﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System;
//using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
//using System.Linq;
//using System.Text;
using log4net;
using System.Reflection;
//using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
//using LS.Data.Utilities;
using System.Collections;
using System.Web;
using System.Xml.Serialization;
//using System.Web.Mvc;
//using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json.Linq;
//using LS.Data.Model;
using System.Xml.Linq;
//using System.Data.Entity.Core.Objects;
using System.Security.Cryptography;
//using LS.Web.Utility;
//using Kendo.DynamicLinq;
//using Stimulsoft.Report;
using System.Text.RegularExpressions;
using System.Diagnostics;
//using Stimulsoft.Report.Export;
//using System.Web.Mvc.Html;
//using System.Web.UI.WebControls;
//using WebGrease.Css.Extensions;
using Missing = System.Reflection.Missing;
//using LS.Data.Model.Reports;
//using LS.Web.Models;
//using System.Transactions;
//using System.Data.Entity.Validation;
//using System.IO.Path;
//using Microsoft.Office.Interop.Excel;
using Infragistics.Documents.Excel;
using System.Web.Configuration;

namespace BatchProcessExport
{
    
   public class Download
    {

        private static ILog _logger = LogManager.GetLogger(typeof(Download));

        // Separate path --- Start

        // To pass dynamic path location from web config   - Start
        string IsServerUpload = WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
        string pathDesignation = WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
        string pathUrl = WebConfigurationManager.AppSettings["ExternalAssetDriveURL"];

        string path = string.Empty;
        string destinationPath;


        string filePath = string.Empty;

        // Seprate Path -- End





        public void ProcessRequest(DataTable ExportTable, DataTable ExportTableFAM, DataTable ExportTableSubProduct, string batchId, string ExportId)
        {
            try
            {
                _logger.Info("Download Start");


                // Get Item# and SubItem# values
                DataTable customerItems = new DataTable();
                Connection connection = new Connection();
                SqlConnection sqlConnection = new SqlConnection();
                //  SqlCommand sqlCommand = new SqlCommand();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();

                var exportFilePath = ConfigurationManager.AppSettings["ExportPath"].ToString();

                _logger.Info("Get Path =" + exportFilePath);
                using (sqlConnection = new SqlConnection(connection.GetConnection()))
                {
                    //sqlConnection.Open();
                    SqlCommand objSqlCommand = new SqlCommand();
                    objSqlCommand.CommandType = CommandType.StoredProcedure;

                    objSqlCommand.Connection = sqlConnection;
                    objSqlCommand.CommandText = "STP_LS_EXPORTBATCHPROCESS";

                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar, 30).Value = "GETCUSTOMERITEMSINFO";
                    objSqlCommand.Parameters.Add("@BATCH_ID", SqlDbType.VarChar, 200).Value = batchId.ToString();
                    objSqlCommand.CommandTimeout = 0;

                    sqlDataAdapter = new SqlDataAdapter(objSqlCommand);

                    sqlDataAdapter.Fill(customerItems);

                }

                ////CustomerItemNo
                //if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null) 
                //{

                //   System.Web.HttpContext.Current.Session["CustomerItemNo"] = homeObj.getCatalogItemNumberValues();
                //}
                //// CustomerSubItemNo
                //if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
                //{
                //    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = homeObj.getCatalogSubItemNumberValues();
                //}


                HttpContext context = HttpContext.Current;
               // context.Response.ContentType = "text/plain";
               // HttpResponse response = context.Response;
               // string filename = HttpContext.Current.Request.QueryString["Path"];

                string filename = "Job_" +Convert.ToString(ExportId);

              //=====  string filename = Convert.ToString(customerItems.Rows[0]["BATCH_ID"]);
                filename = filename + ".xls";
            //    bool EnableSubProduct = Convert.ToBoolean(HttpContext.Current.Request.QueryString["EnableSubProduct"]);

                bool EnableSubProduct = Convert.ToBoolean(customerItems.Rows[0]["ENNABLESUBPRODUCTS"]);

                if (ExportTable != null && filename.ToLower().Contains(".xls"))
                {

                    var workbook = new Workbook();
                    if (ExportTable != null)
                    {
                        var distinctdsFam = ExportTableFAM;
                        distinctdsFam.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                        var tableFam = distinctdsFam.DefaultView.ToTable(true);
                        // table.Columns.Add("sdfsaf");
                        // table.Columns.Add("sdfsaf1");
                        // table.Columns.Add("sdfsaf2");
                        ExportTableFAM = null;

                        //var objExport = new RKLib.ExportData.Export();
                        if (tableFam.Rows.Count > 0)
                        {
                            const string tableName1 = "Families";
                            string tableName = "Families";

                            int rowcntTemp = tableFam.Rows.Count;
                            int j = 0;
                            int runningcnt = 0;
                            do
                            {
                                int rowcnt;
                                if (rowcntTemp <= 65000)
                                {
                                    rowcnt = rowcntTemp;
                                    rowcntTemp = rowcnt - 65000;
                                }
                                else
                                {
                                    rowcnt = 65000;
                                    rowcntTemp = tableFam.Rows.Count - (runningcnt + 65000);
                                }
                                j++;
                                if (j != 1)
                                {
                                    tableName = tableName1 + (j - 1);
                                }
                                var worksheet = workbook.Worksheets.Add(tableName);
                                workbook.WindowOptions.SelectedWorksheet = worksheet;
                                for (int jj = 1; jj <= rowcnt; jj++)

                                    // Create the worksheet to represent this data table


                                    // Create column headers for each column
                                    for (int columnIndex = 0; columnIndex < tableFam.Columns.Count; columnIndex++)
                                    {

                                        if (tableFam.Columns[columnIndex].ColumnName == "CATALOG_ITEM_NO" || tableFam.Columns[columnIndex].ColumnName == "ITEM#")
                                        {
                                            worksheet.Rows[1].Cells[columnIndex].Value = customerItems.Rows[0]["CUSTOMERITEMNO"].ToString();
                                        }
                                        else if (tableFam.Columns[columnIndex].ColumnName == "SUBITEM#")
                                        {
                                            worksheet.Rows[1].Cells[columnIndex].Value = customerItems.Rows[0]["CUSTOMERSUBITEMNO"].ToString();
                                        }
                                        else if (tableFam.Columns[columnIndex].ColumnName != "CATALOG_ITEM_NO" && tableFam.Columns[columnIndex].ColumnName != "ITEM#" && tableFam.Columns[columnIndex].ColumnName != "SUBITEM#")
                                        {
                                            worksheet.Rows[1].Cells[columnIndex].Value = tableFam.Columns[columnIndex].ColumnName;
                                        }
                                    }




                                // Starting at row index 1, copy all data rows in
                                // the data table to the worksheet

                                int rowIndex = 2;
                                int temprunningcnt = runningcnt;
                                for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                                {
                                    var row = worksheet.Rows[rowIndex++];
                                    runningcnt++;
                                    for (int columnIndex = 0; columnIndex < tableFam.Rows[k].ItemArray.Length; columnIndex++)
                                    {
                                        row.Cells[columnIndex].Value = tableFam.Rows[k].ItemArray[columnIndex];
                                    }
                                }

                            }

                            while (rowcntTemp > 0);


                        }
                        else
                        {
                            _logger.Info("text/plain");
                            _logger.Info("No records found.");
                            //context.Response.ContentType = "text/plain";
                            //context.Response.Write("No records found.");
                        }
                    }



                    var distinctds = ExportTable;

                    distinctds.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    var table = distinctds.DefaultView.ToTable(true);
                    // table.Columns.Add("sdfsaf");
                    // table.Columns.Add("sdfsaf1");
                    // table.Columns.Add("sdfsaf2");
                    ExportTable = null;
                    // var workbook = new Workbook();

                    //var objExport = new RKLib.ExportData.Export();
                    if (table.Rows.Count > 0)
                    {
                        const string tableName1 = "Products";
                        string tableName = "Products";

                        int rowcntTemp = table.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        do
                        {
                            int rowcnt;
                            if (rowcntTemp <= 65000)
                            {
                                rowcnt = rowcntTemp;
                                rowcntTemp = rowcnt - 65000;
                            }
                            else
                            {
                                rowcnt = 65000;
                                rowcntTemp = table.Rows.Count - (runningcnt + 65000);
                            }
                            j++;
                            if (j != 1)
                            {
                                tableName = tableName1 + (j - 1);
                            }
                            var worksheet = workbook.Worksheets.Add(tableName);
                            workbook.WindowOptions.SelectedWorksheet = worksheet;
                            for (int jj = 1; jj <= rowcnt; jj++)

                                // Create the worksheet to represent this data table


                                // Create column headers for each column
                                for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                                {
                                    if (table.Columns[columnIndex].ColumnName == "CATALOG_ITEM_NO" || table.Columns[columnIndex].ColumnName == "ITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = customerItems.Rows[0]["CUSTOMERITEMNO"].ToString();
                                    }
                                    else if (table.Columns[columnIndex].ColumnName == "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = customerItems.Rows[0]["CUSTOMERSUBITEMNO"].ToString();
                                    }
                                    else if (table.Columns[columnIndex].ColumnName != "CATALOG_ITEM_NO" && table.Columns[columnIndex].ColumnName != "ITEM#" && table.Columns[columnIndex].ColumnName != "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = table.Columns[columnIndex].ColumnName;
                                    }
                                }

                            // Starting at row index 1, copy all data rows in
                            // the data table to the worksheet

                            int rowIndex = 2;
                            int temprunningcnt = runningcnt;
                            for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                            {
                                var row = worksheet.Rows[rowIndex++];
                                runningcnt++;
                                for (int columnIndex = 0; columnIndex < table.Rows[k].ItemArray.Length; columnIndex++)
                                {
                                    row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex];
                                }
                            }

                        }

                        while (rowcntTemp > 0);


                    }
                    else
                    {
                        var worksheet = workbook.Worksheets.Add("Product");
                        workbook.WindowOptions.SelectedWorksheet = worksheet;

                        // Create column headers for each column
                        for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                        {
                            worksheet.Rows[1].Cells[columnIndex].Value = table.Columns[columnIndex].ColumnName;
                        }
                      //  context.Response.ContentType = "text/plain";
                       // context.Response.Write("No records found.");

                    }
                    if (EnableSubProduct == true)
                    {
                        if (ExportTableSubProduct.Rows.Count > 0)
                        {
                            var tableSP = ExportTableSubProduct;
                            if (!tableSP.Columns.Contains("Action"))
                            {
                                tableSP.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                            }
                            string tableNameSp = "SubProducts";
                            const string tableNameSp1 = "SubProducts";
                            int rowcntTempSP = tableSP.Rows.Count;
                            int jSP = 0;
                            int runningcntSP = 0;
                            do
                            {
                                int rowcntSP;
                                if (rowcntTempSP <= 65000)
                                {
                                    rowcntSP = rowcntTempSP;
                                    rowcntTempSP = rowcntSP - 65000;
                                }
                                else
                                {
                                    rowcntSP = 65000;
                                    rowcntTempSP = tableSP.Rows.Count - (runningcntSP + 65000);
                                }
                                jSP++;

                                if (jSP != 1)
                                {
                                    tableNameSp = tableNameSp1 + (jSP - 1);
                                }
                                var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                                workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                                for (int jj = 1; jj <= rowcntSP; jj++)

                                    // Create the worksheet to represent this data table


                                    // Create column headers for each column
                                    for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                                    {
                                        if (tableSP.Columns[columnIndex].ColumnName == "CATALOG_ITEM_NO" || tableSP.Columns[columnIndex].ColumnName == "ITEM#")
                                        {
                                            worksheetSp.Rows[1].Cells[columnIndex].Value = customerItems.Rows[0]["CUSTOMERITEMNO"].ToString();
                                        }
                                        else if (tableSP.Columns[columnIndex].ColumnName == "SUBITEM#")
                                        {
                                            worksheetSp.Rows[1].Cells[columnIndex].Value = customerItems.Rows[0]["CUSTOMERSUBITEMNO"].ToString();
                                        }
                                        else if (tableSP.Columns[columnIndex].ColumnName != "CATALOG_ITEM_NO" && tableSP.Columns[columnIndex].ColumnName != "ITEM#" && tableSP.Columns[columnIndex].ColumnName != "SUBITEM#")
                                        {


                                            worksheetSp.Rows[1].Cells[columnIndex].Value = tableSP.Columns[columnIndex].ColumnName;

                                        }
                                    }

                                // Starting at row index 1, copy all data rows in
                                // the data table to the worksheet

                                int rowIndex = 2;
                                int temprunningcntSP = runningcntSP;
                                for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                                {
                                    var row = worksheetSp.Rows[rowIndex++];
                                    runningcntSP++;
                                    for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                                    {
                                        row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                                    }
                                }
                            } while (rowcntTempSP > 0);
                        }
                    }
                    if (workbook.Worksheets[0] != null)
                    {
                        var worksheet1 = workbook.Worksheets[0];
                        workbook.WindowOptions.SelectedWorksheet = worksheet1;
                    }

                    string FinalExportPath = SavedPath(filename, ExportId);                 
                    workbook.Save(FinalExportPath);
                    
                    using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        //string SqlString = "update EXPORTBATCHPROCESS set STATUS = 'Completed' where BATCH_ID = '" + batchId + "'";
                        //var cmd = new SqlCommand(SqlString, conn);
                        SqlCommand objSqlCommand = new SqlCommand();
                        objSqlCommand.CommandType = CommandType.StoredProcedure;

                        objSqlCommand.Connection = conn;
                        objSqlCommand.CommandText = "STP_LS_EXPORTBATCHPROCESS";

                        objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar, 30).Value = "ENDPROCESS";
                        objSqlCommand.Parameters.Add("@BATCH_ID", SqlDbType.VarChar, 200).Value = batchId.ToString();
                        objSqlCommand.CommandTimeout = 0;

                        conn.Open();
                        objSqlCommand.ExecuteNonQuery();
                        conn.Close();
                    }
                }
               
                else 
                {
                    _logger.Info("text/plain");
                    _logger.Info("Error occurred during export!.");
                }

            }
            catch (Exception ex)
            {
                
                throw;
            }
            
       

        }


        public string SavedPath(string filename, string ExportId)
        {
            try
            {
                 int ExP_Id = int.Parse(ExportId);
                 String query = "select Comments from Customers where CustomerId in (select CustomerId from Customer_User where  user_Name in (select customer_name from EXPORTBATCHPROCESS where EXPORTID = '" + ExP_Id + "'))";
                 Connection connection = new Connection();
                 string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString();
                 //  SqlCommand sqlCommand = new SqlCommand();  
                 SqlConnection con = new SqlConnection(connectionString);
                 SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
                 SqlCommand cmd1 = new SqlCommand(query, con);
                 con.Open();
                 var user_Name = (string)cmd1.ExecuteScalar();
                 con.Close();

                // To save the path in separate path :

                if (IsServerUpload.ToUpper() == "TRUE")
                {
                    destinationPath = pathDesignation + "\\Content\\ProductImages\\" + user_Name + "\\Export\\";
                    destinationPath = destinationPath.Replace("////", "\\\\");
                    destinationPath = destinationPath.Replace("/", "\\");
                }
                else
                {
                    var exportFilePath = ConfigurationManager.AppSettings["ExportPath"].ToString();                  
                     destinationPath = exportFilePath + "\\Content\\ProductImages\\" + user_Name + "\\Export\\";                               
                }

                if (!Directory.Exists(destinationPath))
                {
                    Directory.CreateDirectory(destinationPath);
                }

                string FinalExportPath = destinationPath + filename;
                _logger.Info("File Saved Path =" + FinalExportPath);

                // To save the path in separate path :
                return FinalExportPath;
            }
            catch (Exception)
            {

                return "Error";
            }





       }


        public void ProcessRequest1()
        {
            HttpContext context = HttpContext.Current;
            //CustomerItemNo
            //if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
            //{

            //    System.Web.HttpContext.Current.Session["CustomerItemNo"] = homeObj.getCatalogItemNumberValues();
            //}
            //// CustomerSubItemNo
            //if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
            //{
            //    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = homeObj.getCatalogSubItemNumberValues();
            //}



            context.Response.ContentType = "text/plain";
            HttpResponse response = context.Response;
            string filename = HttpContext.Current.Request.QueryString["Path"];
            response.ClearContent();
            response.Clear();
            response.ContentType = "text/xls";
            response.AddHeader("Content-Disposition",
                               "attachment; filename=" + filename + ";");
            response.TransmitFile(context.Server.MapPath("~/Content/" + filename));
            response.Flush();
            response.End();

        }






        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
