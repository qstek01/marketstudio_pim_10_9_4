﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace BatchProcess
{
    class Program
    {
        Connection connection = new Connection();
        SqlConnection sqlConnection = new SqlConnection();
        SqlCommand sqlCommand = new SqlCommand();
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();

        private static ILog _logger = LogManager.GetLogger(typeof(Program));

        string sqlString = string.Empty;
        static void Main(string[] args)
        {
            log4net.Config.BasicConfigurator.Configure();
            //  args = new string[3] { "e2dd26d4-1790-41d1-8987-39492f19a96e", "2", "true" };
            try
            {
                // Console.Title = "Import Batch";
                _logger.Info(args.Length.ToString());
                _logger.Info("Start");
                Program prg = new Program();

                int customerId = 0;
                int.TryParse(args[1], out customerId);
                bool schedule = false;
                bool.TryParse(args[2], out schedule);
                prg.ImportProcess(args[0], customerId, schedule);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Main : Program  - ", ex);
            }
        }

        public void ImportProcess(string batchId, int customerId, bool schedule)
        {
            DataSet dsImportDatas = new DataSet();
            DataTable dtImportData = new DataTable();
            DataTable dtImportAttr = new DataTable();
            DataTable dsAllPendingData = new DataTable();
            DataTable dsGetAllRecords = new DataTable();
            DataTable replaceColumn = new DataTable();
            DataTable rowAffected = new DataTable();
            string stpName = connection.GetStpName();
            string importType = string.Empty;
            int allowDup = 0;
            string customerName = string.Empty;
            string logName = string.Empty;

            try
            {
                _logger.Info("Get Records");
                //using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))
                //{
                //    sqlConnection.Open();
                //    using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                //    {
                //        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                //        sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETRECORDS";
                //        sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                //        sqlDataAdapter.Fill(dsGetAllRecords);
                //    }
                //}
                if (schedule)
                {
                    using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))
                    {
                        sqlConnection.Open();
                        using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                        {
                            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                            sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                            sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "SELECT";
                            sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                            sqlCommand.CommandTimeout = 0;
                            sqlDataAdapter.Fill(dsImportDatas);
                        }
                        _logger.Info("Total Records : " + dsImportDatas.Tables.Count.ToString());
                        if (dsImportDatas != null && dsImportDatas.Tables.Count >= 3 && dsImportDatas.Tables[2].Columns.Contains("IMPORTTYPE"))
                        {
                            _logger.Info("Batch Start");
                            importType = dsImportDatas.Tables[2].Rows.Count > 0 ? dsImportDatas.Tables[2].Rows[0][0].ToString() : "CATALOG_ITEM_NO";
                            if (importType.ToUpper() == "CATALOG_ITEM_NO")
                            {
                                string importStatus = catalogItemnoImport(dsImportDatas, importType, batchId);
                                DataTable deletedData = GetFilteredData(dsImportDatas.Tables[0], "DELETE");
                                if (deletedData != null && deletedData.Rows.Count > 0)
                                    ProductDelete(batchId, deletedData, dtImportAttr, importType, allowDup, customerId, customerName, stpName);
                                string importLogName = importStatus.Contains('_') ? importStatus.Split('_')[1] : "";
                                if (importLogName != "")
                                {
                                    if (sqlConnection.State.ToString().ToUpper() == "CLOSED")
                                    {
                                        sqlConnection = new SqlConnection(connection.ConnectionOpen());
                                        sqlConnection.Open();
                                    }
                                    using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                                    {
                                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                        sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                                        sqlCommand.Parameters.Add("@OPTION", SqlDbType.NChar).Value = importLogName;
                                        sqlCommand.CommandTimeout = 0;
                                        sqlCommand.ExecuteNonQuery();
                                        if (logName.Contains("ERROR"))
                                        {
                                            _logger.Info("Import Failed");
                                        }
                                        else
                                        {
                                            _logger.Info("Import Sucess");
                                        }
                                    }
                                }
                            }
                            else if (importType.ToUpper() == "SUBCATALOG_ITEM_NO")
                            {
                                string importStatus = catalogItemnoImport(dsImportDatas, importType, batchId);
                                DataTable deletedData = GetFilteredData(dsImportDatas.Tables[0], "DELETE");
                                if (deletedData != null && deletedData.Rows.Count > 0)
                                    ProductDelete(batchId, deletedData, dtImportAttr, importType, allowDup, customerId, customerName, stpName);
                                string importLogName = importStatus.Contains('_') ? importStatus.Split('_')[1] : "";
                                if (importLogName != "")
                                {
                                    if (sqlConnection.State.ToString().ToUpper() == "CLOSED")
                                    {
                                        sqlConnection = new SqlConnection(connection.ConnectionOpen());
                                        sqlConnection.Open();
                                    }
                                    using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                                    {
                                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                        sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                                        sqlCommand.Parameters.Add("@OPTION", SqlDbType.NChar).Value = importLogName;
                                        sqlCommand.CommandTimeout = 0;
                                        sqlCommand.ExecuteNonQuery();
                                        if (logName.Contains("ERROR"))
                                        {
                                            _logger.Info("Import Failed");
                                        }
                                        else
                                        {
                                            _logger.Info("Import Sucess");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (dsImportDatas != null && dsImportDatas.Tables.Count >= 3 && dsImportDatas.Tables[0].Rows.Count > 0 && dsImportDatas.Tables[1].Rows.Count > 0)
                                {
                                    dtImportData = dsImportDatas.Tables[0];
                                    dtImportAttr = dsImportDatas.Tables[1];
                                    importType = dsImportDatas.Tables[2].Rows.Count > 0 ? dsImportDatas.Tables[2].Rows[0][0].ToString() : "PRODUCT";
                                    int.TryParse(dsImportDatas.Tables[2].Rows.Count > 0 ? dsImportDatas.Tables[2].Rows[0][1].ToString() : "0", out allowDup);
                                    customerName = dsImportDatas.Tables[2].Rows.Count > 0 ? dsImportDatas.Tables[2].Rows[0][3].ToString() : "";
                                    //  customerId = dsImportDatas.Tables[2].Rows.Count > 0 ? int.Parse(dsImportDatas.Tables[2].Rows[0][2].ToString()) : 1;
                                    // MOVE PRODUCT DATA TO TEMP TABLE 
                                    using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                                    {
                                        sqlCommand.CommandType = CommandType.StoredProcedure;
                                        sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = customerId;
                                        sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "RUNPROCESS";
                                        sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NVarChar).Value = batchId;
                                        sqlCommand.CommandTimeout = 0;
                                        sqlCommand.ExecuteNonQuery();
                                    }
                                    if (importType == "FAMILY_IMPORT_WITHOUT_HIERARCHY")
                                    {
                                        int removeFields = dsImportDatas.Tables[0].Columns.Count;
                                        string[] columnsToRemove = { "CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_NAME" };
                                        for (int index = 0; index < dsImportDatas.Tables[0].Columns.Count; index++)
                                        {
                                            if (columnsToRemove.Contains(dsImportDatas.Tables[0].Columns[index].ColumnName.ToUpper()) || dsImportDatas.Tables[0].Columns[index].ColumnName.ToUpper().Contains("SUBCATNAME"))
                                            {
                                                dsImportDatas.Tables[0].Columns.RemoveAt(index);
                                                removeFields = removeFields + 1;
                                                index--;

                                            }
                                        }

                                        string importStatus = catalogFamilyNameImport(dsImportDatas, importType, batchId);
                                        // DataTable dFamily=
                                    }
                                    else if (!importType.ToUpper().Contains("SUB"))
                                    {
                                        if (importType.ToUpper().Contains("FAM"))
                                            _logger.Info("Import Families");
                                        else if (importType.ToUpper().Contains("PROD"))
                                            _logger.Info("Import Product");
                                        DataTable InsertedData = GetFilteredData(dtImportData, "INSERT");
                                        string importStatus = ProductImport(batchId, InsertedData, dtImportAttr, importType, allowDup, customerId, customerName, stpName);
                                        DataTable deletedData = GetFilteredData(dtImportData, "DELETE");
                                        if (deletedData != null && deletedData.Rows.Count > 0)
                                            ProductDelete(batchId, deletedData, dtImportAttr, importType, allowDup, customerId, customerName, stpName);

                                        string importLogName = importStatus.Contains('_') ? importStatus.Split('_')[1] : "";
                                        if (importLogName != "")
                                        {
                                            using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                                            {
                                                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                                sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                                                sqlCommand.Parameters.Add("@OPTION", SqlDbType.NChar).Value = importLogName;
                                                sqlCommand.CommandTimeout = 0;
                                                sqlCommand.ExecuteNonQuery();
                                                if (logName.Contains("ERROR"))
                                                {
                                                    _logger.Info("Import Failed");
                                                }
                                                else
                                                {
                                                    _logger.Info("Import Sucess");
                                                }
                                            }
                                        }

                                    }
                                    else
                                    {
                                        _logger.Info("Import SubProduct");
                                        DataTable InsertedData = GetFilteredData(dtImportData, "INSERT");
                                        string importStatus = SubProductImport(batchId, InsertedData, dtImportAttr, importType, allowDup, customerId, customerName, stpName);

                                        DataTable deletedData = GetFilteredData(dtImportData, "DELETE");
                                        if (deletedData != null && deletedData.Rows.Count > 0)
                                            ProductDelete(batchId, deletedData, dtImportAttr, importType, allowDup, customerId, customerName, stpName);

                                        string importLogName = importStatus.Contains('_') ? importStatus.Split('_')[1] : "";
                                        if (importLogName != "")
                                        {
                                            if (sqlConnection.State.ToString().ToUpper() == "CLOSED")
                                            {
                                                sqlConnection = new SqlConnection(connection.ConnectionOpen());
                                                sqlConnection.Open();
                                            }
                                            using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                                            {
                                                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                                sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                                                sqlCommand.Parameters.Add("@OPTION", SqlDbType.NChar).Value = importLogName;
                                                sqlCommand.CommandTimeout = 0;
                                                sqlCommand.ExecuteNonQuery();
                                                if (logName.Contains("ERROR"))
                                                {
                                                    _logger.Info("Import Failed");
                                                }
                                                else
                                                {
                                                    _logger.Info("Import Sucess");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))
                    {
                        sqlConnection.Open();
                        using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                        {
                            sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                            //sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.NChar).Value = customerId;
                            sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "SELECTPENDINGDATA";
                            sqlCommand.CommandTimeout = 0;
                            sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                            sqlDataAdapter.Fill(dsAllPendingData);
                        }
                    }
                    if (dsAllPendingData != null && dsAllPendingData.Rows.Count > 0)
                    {
                        foreach (DataRow drPending in dsAllPendingData.Rows)
                        {
                            batchId = drPending["BATCHID"].ToString();
                            using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))
                            {
                                sqlConnection.Open();
                                using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                                {
                                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                    sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                                    sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "SELECT";
                                    sqlCommand.CommandTimeout = 0;
                                    sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                                    sqlDataAdapter.Fill(dsImportDatas);
                                }

                                if (dsImportDatas != null && dsImportDatas.Tables[2].Columns.Contains("IMPORTTYPE") && dsImportDatas.Tables[2].Rows.Count > 0)
                                {
                                    importType = dsImportDatas.Tables[2].Rows.Count > 0 ? dsImportDatas.Tables[2].Rows[0][0].ToString() : "CATALOG_ITEM_NO";
                                    if (importType.ToUpper() == "CATALOG_ITEM_NO")
                                    {
                                        string importStatus = catalogItemnoImport(dsImportDatas, importType, batchId);
                                        DataTable deletedData = GetFilteredData(dsImportDatas.Tables[0], "DELETE");
                                        if (deletedData != null && deletedData.Rows.Count > 0)
                                            ProductDelete(batchId, deletedData, dtImportAttr, importType, allowDup, customerId, customerName, stpName);

                                        string importLogName = importStatus.Contains('_') ? importStatus.Split('_')[1] : "";
                                        if (importLogName != "")
                                        {
                                            if (sqlConnection.State.ToString().ToUpper() == "CLOSED")
                                            {
                                                sqlConnection = new SqlConnection(connection.ConnectionOpen());
                                                sqlConnection.Open();
                                            }
                                            using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                                            {
                                                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                                sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                                                sqlCommand.Parameters.Add("@OPTION", SqlDbType.NChar).Value = importLogName;
                                                sqlCommand.CommandTimeout = 0;
                                                sqlCommand.ExecuteNonQuery();
                                                if (logName.Contains("ERROR"))
                                                {
                                                    _logger.Info("Import Failed");
                                                }
                                                else
                                                {
                                                    _logger.Info("Import Sucess");
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        dtImportData = dsImportDatas.Tables[0];
                                        dtImportAttr = dsImportDatas.Tables[1];
                                        importType = dsImportDatas.Tables[2].Rows.Count > 0 ? dsImportDatas.Tables[2].Rows[0][0].ToString() : "PRODUCT";
                                        int.TryParse(dsImportDatas.Tables[2].Rows.Count > 0 ? dsImportDatas.Tables[2].Rows[0][1].ToString() : "0", out allowDup);
                                        customerName = dsImportDatas.Tables[2].Rows.Count > 0 ? dsImportDatas.Tables[2].Rows[0][3].ToString() : "";
                                        //  customerId = dsImportDatas.Tables[2].Rows.Count > 0 ? int.Parse(dsImportDatas.Tables[2].Rows[0][2].ToString()) : 1;
                                        // MOVE PRODUCT DATA TO TEMP TABLE 
                                        if (importType == "FAMILY_IMPORT_WITHOUT_HIERARCHY")
                                        {
                                            bool ItemImport = true;
                                            int removeFields = 0;
                                            string[] columnsToRemove = { "CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_NAME" };
                                            for (int index = 0; index < dtImportData.Columns.Count; index++)
                                            {
                                                if (columnsToRemove.Contains(dtImportData.Columns[index].ColumnName.ToUpper()) || dtImportData.Columns[index].ColumnName.ToUpper().Contains("SUBCATNAME"))
                                                {
                                                    dtImportData.Columns.RemoveAt(index);
                                                    removeFields = removeFields + 1;
                                                }
                                            }

                                            // DataTable dFamily=
                                        }
                                        else if (!importType.ToUpper().Contains("SUB"))
                                        {
                                            _logger.Info("Import Product");
                                            DataTable InsertedData = GetFilteredData(dtImportData, "INSERT");
                                            string importStatus = ProductImport(batchId, InsertedData, dtImportAttr, importType, allowDup, customerId, customerName, stpName);
                                            DataTable deletedData = GetFilteredData(dtImportData, "DELETE");
                                            if (deletedData != null && deletedData.Rows.Count > 0)
                                                ProductDelete(batchId, deletedData, dtImportAttr, importType, allowDup, customerId, customerName, stpName);

                                            string importLogName = importStatus.Contains('_') ? importStatus.Split('_')[1] : "";
                                            if (importLogName != "")
                                            {
                                                if (sqlConnection.State.ToString().ToUpper() == "CLOSED")
                                                {
                                                    sqlConnection = new SqlConnection(connection.ConnectionOpen());
                                                    sqlConnection.Open();
                                                }
                                                using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                                                {
                                                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                                    sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                                                    sqlCommand.Parameters.Add("@OPTION", SqlDbType.NChar).Value = importLogName;
                                                    sqlCommand.CommandTimeout = 0;
                                                    sqlCommand.ExecuteNonQuery();
                                                    if (logName.Contains("ERROR"))
                                                    {
                                                        _logger.Info("Import Failed");
                                                    }
                                                    else
                                                    {
                                                        _logger.Info("Import Sucess");
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            _logger.Info("Import SubProduct");
                                            DataTable InsertedData = GetFilteredData(dtImportData, "INSERT");
                                            string importStatus = SubProductImport(batchId, InsertedData, dtImportAttr, importType, allowDup, customerId, customerName, stpName);
                                            DataTable deletedData = GetFilteredData(dtImportData, "DELETE");
                                            if (deletedData != null && deletedData.Rows.Count > 0)
                                                ProductDelete(batchId, deletedData, dtImportAttr, importType, allowDup, customerId, customerName, stpName);

                                            string importLogName = importStatus.Contains('_') ? importStatus.Split('_')[1] : "";
                                            if (importLogName != "")
                                            {
                                                if (sqlConnection.State.ToString().ToUpper() == "CLOSED")
                                                {
                                                    sqlConnection = new SqlConnection(connection.ConnectionOpen());
                                                    sqlConnection.Open();
                                                }
                                                using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                                                {
                                                    sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                                    sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                                                    sqlCommand.Parameters.Add("@OPTION", SqlDbType.NChar).Value = importLogName;
                                                    sqlCommand.CommandTimeout = 0;
                                                    sqlCommand.ExecuteNonQuery();
                                                    if (logName.Contains("ERROR"))
                                                    {
                                                        _logger.Info("Import Failed");
                                                    }
                                                    else
                                                    {
                                                        _logger.Info("Import Sucess");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLogUpdate(ex.Message, batchId, stpName);
                _logger.Error("Error at Main : Program - ", ex);
            }
        }

        public string ProductImport(string batchId, DataTable dtImportData, DataTable dtImportAttr, string importType, int allowDup, int customerId, string customerName, string stpName)
        {
            try
            {
                //customerId = 0;
                string importTemp;
                importTemp = Guid.NewGuid().ToString();
                DataTable replaceColumn = new DataTable();
                DataTable errorLog = new DataTable();
                foreach (DataColumn dc in dtImportData.Columns)
                {
                    if (dc.ColumnName.ToString().ToUpper().Contains("ACTION"))
                    {
                        dtImportData.Columns.Remove(dc);
                        break;
                    }
                }
                using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))
                {
                    _logger.Info("Fill Temp Table");
                    sqlConnection.Open();
                    sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    SqlCommand _DBCommand = new SqlCommand(sqlString, sqlConnection);
                    _DBCommand.ExecuteNonQuery();
                    sqlString = CreateTable("[##IMPORTTEMP" + importTemp + "]", dtImportData);

                    SqlCommand dbCommandnew = new SqlCommand(sqlString, sqlConnection);
                    dbCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(sqlConnection)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(dtImportData);

                    _logger.Info("Update id columns if exists");
                    // UPDATE ID COLUMN IF ALREADY EXISTS
                    if (importType != "FAMILY_IMPORT_WITHOUT_HIERARCHY")
                    {
                        sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";
                        _DBCommand = new SqlCommand(sqlString, sqlConnection);
                        _DBCommand.CommandTimeout = 0;
                        _DBCommand.ExecuteNonQuery();
                    }
                    // ALTER TEMP TABLE FOR ADDING SOME COLUMNS

                    sqlCommand = new SqlCommand("ALTER TABLE [##IMPORTTEMP" + importTemp + "] ADD Family_Status nvarchar(10),FStatusUI nvarchar(10),StatusUI nvarchar(10),SFSort_Order int,Sort_Order int", sqlConnection);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();

                    _logger.Info("fill attribute table");
                    // MOVE ATTRIBUTE DATA TO TEMP TABLE 
                    sqlCommand = new SqlCommand("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##AttributeTemp" + importTemp + "'')BEGIN DROP TABLE [##AttributeTemp" + importTemp + "] END')", sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand = new SqlCommand();
                    sqlString = CreateTableToImport("[##AttributeTemp" + importTemp + "]", dtImportAttr);
                    sqlCommand = new SqlCommand(sqlString, sqlConnection);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();

                    bulkCopy.DestinationTableName = "[##AttributeTemp" + importTemp + "]";
                    bulkCopy.WriteToServer(dtImportAttr);

                    // UPDATE ATTRIBUTE TEMP TABLE 
                    sqlCommand = new SqlCommand("update [##AttributeTemp" + importTemp + "] set ATTRIBUTE_TYPE = 1 where ATTRIBUTE_TYPE = 10", sqlConnection);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1'') IS NOT NULL  DROP TABLE  ##t1') ", sqlConnection);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand = new SqlCommand("select * into ##t1  from [##AttributeTemp" + importTemp + "] where attribute_type <>0", sqlConnection);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();

                    sqlCommand = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2'') IS NOT NULL  DROP TABLE  ##t2') ", sqlConnection);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand = new SqlCommand("select * into ##t2  from [##importtemp" + importTemp + "]", sqlConnection);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();
                    //FOR REPLACING EMPTY VALUES IN TEMP TABLE 
                    sqlCommand = new SqlCommand("SELECT * FROM [##importtemp" + importTemp + "]", sqlConnection);
                    var daf12 = new SqlDataAdapter(sqlCommand);
                    daf12.Fill(replaceColumn);
                    if (replaceColumn.Rows.Count > 0)
                    {

                        foreach (var item1 in replaceColumn.Columns)
                        {
                            string column_name2 = item1.ToString();
                            if (column_name2.Contains("'"))
                            {
                                column_name2 = column_name2.Replace("'", "''''");
                            }
                            sqlCommand = new SqlCommand("  exec ('UPDATE [##IMPORTTEMP" + importTemp + "] SET  [" + column_name2 + "] = NULL  WHERE cast( [" + column_name2 + "] as nvarchar) = '''' and  [" + column_name2 + "] is not null ')", sqlConnection);
                            sqlCommand.CommandTimeout = 0;
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    _logger.Info("Import Start");
                    // import stp call 
                    if (importType.ToUpper() == "FAMILIES")
                    {
                        _logger.Info("Import Start - Family");
                        var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT_HIERARCHY ''" + importTemp + "'',''" + allowDup + "'', " + customerId + ",''" + customerName + "''')", sqlConnection) { CommandTimeout = 0 };
                        cmd.ExecuteNonQuery();
                        //sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + batchId + "'')BEGIN select count(*) as UpdateCount from [##IMPORTTEMP" + batchId + "]  where Family_Status=''Update'' or StatusUI=''Associate'';select count(*) as InsertCount from [##IMPORTTEMP" + batchId + "]  where Family_Status=''Insert''; END')";
                    }
                    else
                    {
                        _logger.Info("Import Start - Product");
                        var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT ''" + importTemp + "'',''" + allowDup + "'', " + customerId + ",''" + customerName + "''')", sqlConnection) { CommandTimeout = 0 };
                        cmd.ExecuteNonQuery();
                        //sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + batchId + "'')BEGIN select count(*) as UpdateCount from [##IMPORTTEMP" + batchId + "]  where StatusUI=''Update'' or StatusUI=''Associate'';select count(*) as InsertCount from [##IMPORTTEMP" + batchId + "]  where StatusUI=''Insert''; END')";
                    }
                    //sqlCommand = new SqlCommand(sqlString, sqlConnection);
                    //SqlDataAdapter dbAdapter = new SqlDataAdapter(_DBCommand);
                    //dbAdapter.Fill(rowAffected);
                    //if (rowAffected != null && rowAffected.Rows.Count > 0)
                    //{
                    //}

                    sqlString = @" if exists(select NAME from TEMPDB.sys.objects where type='u' and name='##LOGTEMPTABLE" + importTemp + "') BEGIN  select * from [##LOGTEMPTABLE" + importTemp + "] End else  Begin select 'Import Success' End ";
                    DataSet ds = CreateDataSet(sqlString);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        string logName = ds.Tables[0].Columns.Contains("STATUS") &&
                            (ds.Tables[0].Rows[0]["STATUS"].ToString().ToUpper().Contains("UPDATE") || ds.Tables[0].Rows[0]["STATUS"].ToString().ToUpper().Contains("INSERT"))
                            && importType.ToUpper().Contains("SUB") ? "SELECTSUCCESSLOGSUB" : ds.Tables[0].Columns.Contains("STATUS") 
                            && (ds.Tables[0].Rows[0]["STATUS"].ToString().ToUpper().Contains("UPDATE") || ds.Tables[0].Rows[0]["STATUS"].ToString().ToUpper().Contains("INSERT"))
                            && importType.ToUpper().Contains("PRODUCT") ? "SELECTSUCCESSLOG" : ds.Tables[0].Columns.Contains("STATUS")
                            && (ds.Tables[0].Rows[0]["STATUS"].ToString().ToUpper().Contains("UPDATE") || ds.Tables[0].Rows[0]["STATUS"].ToString().ToUpper().Contains("INSERT"))
                            && importType.ToUpper().Contains("FAMIL") ? "SELECTSUCCESSLOGFAM" : "SELECTERRORLOG";

                        if (sqlConnection.State.ToString().ToUpper() == "CLOSED")
                        {
                            sqlConnection = new SqlConnection(connection.ConnectionOpen());
                            sqlConnection.Open();
                            sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + batchId + "'')BEGIN DROP TABLE [##IMPORTTEMP" + batchId + "] END')";
                            _DBCommand = new SqlCommand(sqlString, sqlConnection);
                            _DBCommand.CommandTimeout = 0;
                            _DBCommand.ExecuteNonQuery();

                            sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN select * into  [##IMPORTTEMP" + batchId + "] from [##IMPORTTEMP" + importTemp + "] END')";
                            _DBCommand = new SqlCommand(sqlString, sqlConnection);
                            _DBCommand.CommandTimeout = 0;
                            _DBCommand.ExecuteNonQuery();

                            sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##LOGTEMPTABLE" + batchId + "'')BEGIN DROP TABLE [##LOGTEMPTABLE" + batchId + "] END')";
                            _DBCommand = new SqlCommand(sqlString, sqlConnection);
                            _DBCommand.CommandTimeout = 0;
                            _DBCommand.ExecuteNonQuery();

                            sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##LOGTEMPTABLE" + importTemp + "'')BEGIN select * into  [##LOGTEMPTABLE" + batchId + "] from [##LOGTEMPTABLE" + importTemp + "] END')";
                            _DBCommand = new SqlCommand(sqlString, sqlConnection);
                            _DBCommand.CommandTimeout = 0;
                            _DBCommand.ExecuteNonQuery();
                        }

    

                        // using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                        //{
                        //sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        //sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                        //sqlCommand.Parameters.Add("@OPTION", SqlDbType.NChar).Value = logName;
                        //sqlCommand.CommandTimeout = 0;
                        //sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                        //sqlDataAdapter.Fill(errorLog);
                        if (logName.Contains("ERROR"))
                        {
                            _logger.Info("Import Failed");
                            //   return "Import Failed";
                        }
                        else
                        {
                            _logger.Info("Import Sucess");
                        }
                        //}
                        return "Import Sucess_" + logName;
                    }
                }

                return "Import Sucess";
            }
            catch (Exception ex)
            {
                ErrorLogUpdate(ex.Message, batchId, stpName);
                _logger.Error("Error at ProductImport : Program - ", ex);

                return null;
            }
        }

        public string SubProductImport(string batchId, DataTable dtImportData, DataTable dtImportAttr, string importType, int allowDup, int customerId, string customerName, string stpName)
        {
            try
            {
                string importTemp;
                importTemp = Guid.NewGuid().ToString();
                DataTable replaceColumn = new DataTable();
                DataTable errorLog = new DataTable();
                foreach (DataColumn dc in dtImportData.Columns)
                {
                    if (dc.ColumnName.ToString().ToUpper().Contains("ACTION"))
                    {
                        dtImportData.Columns.Remove(dc);
                        break;
                    }
                }
                using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))
                {
                    _logger.Info("Fill Temp Table");
                    sqlConnection.Open();
                    sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBPRODUCTIMPORTTEMP" + importTemp + "'')BEGIN DROP TABLE [##SUBPRODUCTIMPORTTEMP" + importTemp + "] END')";
                    sqlCommand = new SqlCommand(sqlString, sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                    sqlString = CreateTable("[##SUBPRODUCTIMPORTTEMP" + importTemp + "]", dtImportData);

                    sqlCommand = new SqlCommand(sqlString, sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(sqlConnection)
                    {
                        DestinationTableName = "[##SUBPRODUCTIMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(dtImportData);

                    sqlCommand = new SqlCommand("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBPRODUCTATTRIBUTETEMP" + importTemp + "'')BEGIN DROP TABLE [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] END')", sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand = new SqlCommand();

                    sqlString = CreateTableToImport("[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]", dtImportAttr);
                    sqlCommand = new SqlCommand(sqlString, sqlConnection);
                    sqlCommand.ExecuteNonQuery();

                    bulkCopy.DestinationTableName = "[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]";
                    bulkCopy.WriteToServer(dtImportAttr);
                    sqlCommand = new SqlCommand("update [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] set ATTRIBUTE_TYPE = 1 where ATTRIBUTE_TYPE = 10", sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1Sub'') IS NOT NULL  DROP TABLE  ##t1Sub') ", sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                    // string option = "SUBPRODUCT";
                    sqlCommand = new SqlCommand("select * into ##t1Sub  from [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] where attribute_type <>0", sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2Sub'') IS NOT NULL  DROP TABLE  ##t2Sub') ", sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                    sqlCommand = new SqlCommand("select * into ##t2Sub  from [##SUBPRODUCTIMPORTTEMP" + importTemp + "]", sqlConnection);
                    sqlCommand.ExecuteNonQuery();
                    _logger.Info("Import Start");
                    sqlCommand = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_SUBPRODUCTIMPORT ''" + importTemp + "'',''" + allowDup + "'',''" + customerId + "'',''" + customerName + "''')", sqlConnection) { CommandTimeout = 0 };
                    sqlCommand.ExecuteNonQuery();
                    sqlString = @"select * from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                    var ds = CreateDataSet(sqlString);
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {

                        string logName = ds.Tables[0].Columns.Contains("STATUS") && (ds.Tables[0].Rows[0]["STATUS"].ToString().ToUpper().Contains("UPDATE") || ds.Tables[0].Rows[0]["STATUS"].ToString().ToUpper().Contains("INSERT")) && importType.ToUpper().Contains("SUB") ? "SELECTSUCCESSLOGSUB" : "SELECTERRORLOG";
                        if (sqlConnection.State.ToString().ToUpper() == "CLOSED")
                        {
                            sqlConnection = new SqlConnection(connection.ConnectionOpen());
                            sqlConnection.Open();
                            sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBPRODUCTIMPORTTEMP" + batchId + "'')BEGIN DROP TABLE [##SUBPRODUCTIMPORTTEMP" + batchId + "] END')";
                            sqlCommand = new SqlCommand(sqlString, sqlConnection);
                            sqlCommand.ExecuteNonQuery();

                            sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBPRODUCTIMPORTTEMP" + importTemp + "'')BEGIN select * into  [##SUBPRODUCTIMPORTTEMP" + batchId + "] from [##SUBPRODUCTIMPORTTEMP" + importTemp + "] END')";
                            sqlCommand = new SqlCommand(sqlString, sqlConnection);
                            sqlCommand.ExecuteNonQuery();
                            sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBPRODUCTLOGTEMPTABLE" + batchId + "'')BEGIN DROP TABLE [##SUBPRODUCTLOGTEMPTABLE" + batchId + "] END')";
                            sqlCommand = new SqlCommand(sqlString, sqlConnection);
                            sqlCommand.ExecuteNonQuery();

                            sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBPRODUCTLOGTEMPTABLE" + importTemp + "'')BEGIN select * into  [##SUBPRODUCTLOGTEMPTABLE" + batchId + "] from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "] END')";
                            sqlCommand = new SqlCommand(sqlString, sqlConnection);
                            sqlCommand.ExecuteNonQuery();
                        }

                        // using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                        //{
                        //sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        //sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                        //sqlCommand.Parameters.Add("@OPTION", SqlDbType.NChar).Value = logName;
                        //sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                        //sqlDataAdapter.Fill(errorLog);
                        if (logName.Contains("ERROR"))
                        {
                            _logger.Info("Import Failed");
                        }
                        else
                        {
                            _logger.Info("Import Sucess");
                        }
                        //  }
                        return "Import Sucess_" + logName;
                    }
                    // _logger.Info("Import Sucess");
                }
                return "Import Sucess";
            }
            catch (Exception ex)
            {
                ErrorLogUpdate(ex.Message, batchId, stpName);
                _logger.Error("Error at SubProductImport : Program - ", ex);
                return null;
            }
        }
        public DataSet CreateDataSet(string sqlString)
        {
            var dsReturn = new DataSet();
            using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))

            {
                var dbAdapter = new SqlDataAdapter(sqlString, sqlConnection);
                dbAdapter.SelectCommand.CommandTimeout = 0;
                dbAdapter.Fill(dsReturn);
                dbAdapter.Dispose();
                return dsReturn;
            }
        }
        public string CreateTable(string tableName, DataTable objtable)
        {
            try
            {
                string sqlsc = "CREATE TABLE " + tableName + "(";
                for (int i = 0; i < objtable.Columns.Count; i++)
                {
                    if (!objtable.Columns[i].ColumnName.Contains('['))
                    {
                        sqlsc += "\n [" + objtable.Columns[i].ColumnName + "] ";
                    }
                    else
                    {
                        sqlsc += "\n" + objtable.Columns[i].ColumnName + "";
                    }
                    if (objtable.Columns[i].ColumnName.Contains("CATEGORY_ID"))
                        sqlsc += "nvarchar(50) ";
                    else if (objtable.Columns[i].DataType.ToString().Contains("System.String") ||
                             objtable.Columns[i].ColumnName.Contains("SUBCATID") ||
                             objtable.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                        sqlsc += "varchar(8000) ";
                    else
                        sqlsc += "varchar(8000) ";
                    sqlsc += ",";
                }
                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CreateTable : Program - ", ex);
                throw;
            }
        }
        public string CreateTableToImport(string tableName, DataTable table)
        {
            try
            {
                string sqlsc = "CREATE TABLE " + tableName + "(";
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    if (!table.Columns[i].ColumnName.Contains('['))
                    {
                        sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                    }
                    else
                    {
                        sqlsc += "\n" + table.Columns[i].ColumnName + "";
                    }
                    if (table.Columns[i].ColumnName.Contains("CATEGORY_ID"))
                        sqlsc += "nvarchar(50) ";
                    else if (table.Columns[i].DataType.ToString().Contains("System.String") ||
                             table.Columns[i].ColumnName.Contains("SUBCATID") ||
                             table.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                        sqlsc += "varchar(8000) ";
                    else
                        sqlsc += "varchar(8000) ";
                    sqlsc += ",";
                }
                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CreateTableToImport : Program - ", ex);
                throw;
            }
        }

        public void ErrorLogUpdate(string errorMessagem, string batchId, string stpName)
        {
            try
            {
                using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))
                {
                    sqlConnection.Open();
                    using (sqlCommand = new SqlCommand(stpName, sqlConnection))
                    {
                        sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@ERRORMEAASAGE", SqlDbType.NChar).Value = errorMessagem;
                        sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                        sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "ERRORLOGUPDATE";
                        sqlCommand.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ErrorLogUpdate : Program", ex);
            }
        }

        #region UnPivot DataTable
        /// <summary>
        /// UnPivot DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public DataTable UnPivotTable(DataTable dataItems, string importType)
        {
            try
            {
                string[] columns = dataItems.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                DataTable unPivotTable = new DataTable();
                unPivotTable.Columns.Add("ImportId");
                unPivotTable.Columns["ImportId"].DataType = System.Type.GetType("System.Int32");
                unPivotTable.Columns["ImportId"].AutoIncrement = true;
                unPivotTable.Columns["ImportId"].AutoIncrementSeed = 1;
                unPivotTable.Columns.Add("CATALOG_ITEM_NO");
                if (importType.ToUpper().Contains("SUB"))
                {
                    unPivotTable.Columns.Add("SUBCATALOG_ITEM_NO");
                    unPivotTable.Columns.Add("SUBPRODUCT_ID");
                }
                unPivotTable.Columns.Add("ATTRIBUTENAME");
                unPivotTable.Columns.Add("ATTRIBUTEVALUE");
                unPivotTable.Columns.Add("PRODUCT_ID");
                if (!dataItems.Columns.Contains("PRODUCT_ID"))
                {
                    dataItems.Columns.Add("PRODUCT_ID");
                }
                if (!dataItems.Columns.Contains("PRODUCT_ID"))
                    dataItems.Columns.Add("PRODUCT_ID");
                if (dataItems.Columns.Contains("PRODUCT_ID"))
                    dataItems.Columns["PRODUCT_ID"].SetOrdinal(0);
                if (importType.ToUpper().Contains("SUB") && !dataItems.Columns.Contains("SUBPRODUCT_ID"))
                    dataItems.Columns.Add("SUBPRODUCT_ID");
                if (importType.ToUpper().Contains("SUB") && dataItems.Columns.Contains("SUBPRODUCT_ID"))
                    dataItems.Columns["SUBPRODUCT_ID"].SetOrdinal(2);
                for (int rowIndex = 0; rowIndex < dataItems.Rows.Count; rowIndex++)
                {
                    for (int index = 0; index < columns.Length; index++)
                    {
                        if (!columns[index].ToUpper().Contains("CATALOG_ITEM_NO") && !columns[index].ToUpper().Contains("PRODUCT_ID") && !columns[index].ToUpper().Contains("SUBPRODUCT_ID") && (!columns[index].ToUpper().Contains("ACTION") || importType.Contains("BATCH")))
                        {
                            DataRow dr = unPivotTable.NewRow();
                            dr["CATALOG_ITEM_NO"] = dataItems.Rows[rowIndex]["CATALOG_ITEM_NO"];
                            if (importType.ToUpper().Contains("SUB"))
                            {
                                dr["SUBCATALOG_ITEM_NO"] = dataItems.Rows[rowIndex]["SUBCATALOG_ITEM_NO"];
                                if (dataItems.Columns[columns[index]].ColumnName.ToUpper() == "SUBPRODUCT_ID")
                                {
                                    dataItems.Columns[columns[index]].ColumnName = "SUBPRODUCT_ID";
                                }
                                dr["SUBPRODUCT_ID"] = dataItems.Rows[rowIndex]["SUBPRODUCT_ID"];

                            }
                            dr["ATTRIBUTENAME"] = columns[index];
                            dr["ATTRIBUTEVALUE"] = dataItems.Rows[rowIndex][columns[index]].ToString();
                            if (dataItems.Columns[columns[index]].ColumnName.ToUpper() == "PRODUCT_ID")
                            {
                                dataItems.Columns[columns[index]].ColumnName = "PRODUCT_ID";
                            }
                            dr["PRODUCT_ID"] = dataItems.Rows[rowIndex]["PRODUCT_ID"];
                            if (importType.ToUpper().Contains("BULK"))
                                dr["ROW"] = rowIndex;
                            unPivotTable.Rows.Add(dr);
                        }
                    }
                }
                if (unPivotTable.Columns.Contains("PRODUCT_ID"))
                {
                    unPivotTable.Columns["PRODUCT_ID"].SetOrdinal(unPivotTable.Columns.IndexOf("CATALOG_ITEM_NO"));
                }
                if (unPivotTable.Columns.Contains("SUBPRODUCT_ID"))
                {
                    unPivotTable.Columns["SUBPRODUCT_ID"].SetOrdinal(unPivotTable.Columns.IndexOf("SUBCATALOG_ITEM_NO"));
                }
                return unPivotTable;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        public string catalogItemnoImport(DataSet dsImportDatas, string importtype, string batchId)
        {

            try
            {
                _logger.Info("Item# import Start");
                DataTable insertedData = GetFilteredData(dsImportDatas.Tables[0], "INSERT");
                DataTable importData = UnPivotTable(insertedData, importtype);
                _logger.Info("Get Data");
                DataTable attrType = dsImportDatas.Tables[1];
                dsImportDatas.Tables[0].Columns.Contains("VALUES");
                DataTable AttributeDataBatch = new DataTable();
                AttributeDataBatch.Columns.Add("Id");
                AttributeDataBatch.Columns["Id"].DataType = System.Type.GetType("System.Int32");
                AttributeDataBatch.Columns["Id"].AutoIncrement = true;
                AttributeDataBatch.Columns["Id"].AutoIncrementSeed = 1;
                AttributeDataBatch.Columns.Add("ATTRIBUTENAME");
                AttributeDataBatch.Columns.Add("ATTRIBUTETYPE");
                for (int index = 0; index < attrType.Rows.Count; index++)
                {
                    DataRow dr = AttributeDataBatch.NewRow();
                    dr["ATTRIBUTENAME"] = attrType.Rows[index]["ATTRIBUTE_NAME"].ToString();
                    dr["ATTRIBUTETYPE"] = attrType.Rows[index]["ATTRIBUTE_TYPE"].ToString();
                    AttributeDataBatch.Rows.Add(dr);
                }
                var catalogId = 0;
                int.TryParse(dsImportDatas.Tables[3].Rows[0]["VALUE"].ToString(), out catalogId);
                var customerName = dsImportDatas.Tables[2].Rows[0]["CREATED_USER"].ToString();
                var customerId = 0;
                int.TryParse(dsImportDatas.Tables[2].Rows[0]["CUSTOMERID"].ToString(), out customerId);
                DataSet resultSet1 = new DataSet();
                SqlCommand sqlCommand = new SqlCommand();
                _logger.Info("STP Call for import-Start");
                using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))
                {

                    if (importtype.ToUpper().Contains("SUB"))
                    {
                        sqlCommand = new SqlCommand("STP_CATALOGSTUDIO5_SUBCATALOGITEMNUMBER_IMPORT", sqlConnection);
                    }
                    else
                    {
                        sqlCommand = new SqlCommand("STP_CATALOGSTUDIO5_CATALOGITEMNUMBER_IMPORT", sqlConnection);
                    }
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    sqlCommand.Parameters.Add("@SESSIONID", SqlDbType.NVarChar).Value = batchId.ToString();
                    sqlCommand.Parameters.Add("@CUSTOMERNAME", SqlDbType.VarChar).Value = customerName;
                    sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = customerId;
                    sqlCommand.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = importData;
                    sqlCommand.Parameters.Add("@ATTRTEMP", SqlDbType.Structured).Value = AttributeDataBatch;
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlCommand.CommandTimeout = 0;
                    sqlConnection.Open();
                    sqlDataAdapter.Fill(resultSet1);
                    sqlConnection.Close();
                }
                _logger.Info("STP Call for import-end");
                if (resultSet1 != null && resultSet1.Tables[0].Rows.Count > 0)
                {
                    string logName = resultSet1.Tables[0].Columns.Contains("STATUS") ? "SELECTCATSUCCESSLOG" : "SELECTERRORLOG";
                    //if(logName== "SELECTCATSUCCESSLOG")
                    //{

                    DataTable errorLog = new DataTable();
                    if (sqlConnection.State.ToString().ToUpper() == "CLOSED")
                    {
                        using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))
                        {
                            sqlConnection.Open();
                            if (logName == "SELECTERRORLOG")
                            {
                                sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##LOGTEMPTABLE" + batchId + "'')BEGIN DROP TABLE [##LOGTEMPTABLE" + batchId + "] END')";
                                _logger.Info("Import Failed");
                            }
                            else
                            {
                                _logger.Info("Import Success");
                                sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + batchId + "'')BEGIN DROP TABLE [##IMPORTTEMP" + batchId + "] END')";
                            }
                            SqlCommand _DBCommand = new SqlCommand(sqlString, sqlConnection);
                            _DBCommand.ExecuteNonQuery();
                            if (logName == "SELECTERRORLOG")
                            {
                                sqlString = CreateTable("[##LOGTEMPTABLE" + batchId + "]", resultSet1.Tables[0]);
                            }
                            else
                            {
                                sqlString = CreateTable("[##IMPORTTEMP" + batchId + "]", resultSet1.Tables[0]);
                            }
                            SqlCommand dbCommandnew = new SqlCommand(sqlString, sqlConnection);
                            dbCommandnew.ExecuteNonQuery();
                            if (logName == "SELECTERRORLOG")
                            {
                                var bulkCopy = new SqlBulkCopy(sqlConnection)
                                {
                                    DestinationTableName = "[##LOGTEMPTABLE" + batchId + "]"
                                };
                                bulkCopy.WriteToServer(resultSet1.Tables[0]);
                            }
                            else
                            {
                                var bulkCopy = new SqlBulkCopy(sqlConnection)
                                {
                                    DestinationTableName = "[##IMPORTTEMP" + batchId + "]"
                                };
                                bulkCopy.WriteToServer(resultSet1.Tables[0]);
                            }
                            _logger.Info("Update Status");
                            using (sqlCommand = new SqlCommand("STP_LS_IMPORTBATCH", sqlConnection))
                            {
                                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                                sqlCommand.Parameters.Add("@OPTION", SqlDbType.NChar).Value = "UPDATESTATUS";
                                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                                sqlCommand.CommandTimeout = 0;
                                sqlDataAdapter.Fill(errorLog);

                            }
                        }
                    }
                    return "Import Sucess_" + logName;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Main : Program - ", ex);
            }
            return null;
        }


        #region FamilyImportWithout hierarchy

        #region UnpivotFamilyTable
        public DataTable UnPivotTableFamily(DataTable dataItems)
        {
            try
            {
                string[] columns = dataItems.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                DataTable unPivotTable = new DataTable();
                unPivotTable.Columns.Add("ImportId");
                unPivotTable.Columns["ImportId"].DataType = System.Type.GetType("System.Int32");
                unPivotTable.Columns["ImportId"].AutoIncrement = true;
                unPivotTable.Columns["ImportId"].AutoIncrementSeed = 1;
                unPivotTable.Columns.Add("FAMILY_NAME");
                unPivotTable.Columns.Add("SUBFAMILY_NAME");
                unPivotTable.Columns.Add("SUBFAMILY_ID");
                unPivotTable.Columns.Add("ATTRIBUTENAME");
                unPivotTable.Columns.Add("ATTRIBUTEVALUE");
                unPivotTable.Columns.Add("FAMILY_ID");
                if (!dataItems.Columns.Contains("FAMILY_ID"))
                    dataItems.Columns.Add("FAMILY_ID");
                if (dataItems.Columns.Contains("FAMILY_ID"))
                    dataItems.Columns["FAMILY_ID"].SetOrdinal(0);
                if (!dataItems.Columns.Contains("SUBFAMILY_ID"))
                    dataItems.Columns.Add("SUBFAMILY_ID");
                if (dataItems.Columns.Contains("SUBFAMILY_ID"))
                    dataItems.Columns["SUBFAMILY_ID"].SetOrdinal(2);

                if (!dataItems.Columns.Contains("SUBFAMILY_NAME"))
                    dataItems.Columns.Add("SUBFAMILY_NAME");
                for (int rowIndex = 0; rowIndex < dataItems.Rows.Count; rowIndex++)
                {
                    for (int index = 0; index < columns.Length; index++)
                    {
                        if (!columns[index].ToUpper().Contains("FAMILY_NAME") && !columns[index].ToUpper().Contains("FAMILY_ID") && !columns[index].ToUpper().Contains("SUBFAMILY_ID") && !columns[index].ToUpper().Contains("ACTION"))
                        {
                            DataRow dr = unPivotTable.NewRow();
                            dr["FAMILY_NAME"] = dataItems.Rows[rowIndex]["FAMILY_NAME"];
                            dr["SUBFAMILY_NAME"] = dataItems.Rows[rowIndex]["SUBFAMILY_NAME"];
                            if (dataItems.Columns[columns[index]].ColumnName.ToUpper() == "SUBFAMILY_ID")
                            {
                                dataItems.Columns[columns[index]].ColumnName = "SUBFAMILY_ID";
                            }
                            dr["SUBFAMILY_ID"] = dataItems.Rows[rowIndex]["SUBFAMILY_ID"];


                            dr["ATTRIBUTENAME"] = columns[index];
                            dr["ATTRIBUTEVALUE"] = dataItems.Rows[rowIndex][columns[index]].ToString();
                            if (dataItems.Columns[columns[index]].ColumnName.ToUpper() == "FAMILY_ID")
                            {
                                dataItems.Columns[columns[index]].ColumnName = "FAMILY_ID";
                            }
                            dr["FAMILY_ID"] = dataItems.Rows[rowIndex]["FAMILY_ID"];
                            unPivotTable.Rows.Add(dr);
                        }
                    }
                }
                if (unPivotTable.Columns.Contains("FAMILY_ID"))
                {
                    unPivotTable.Columns["FAMILY_ID"].SetOrdinal(unPivotTable.Columns.IndexOf("FAMILY_NAME"));
                }
                if (unPivotTable.Columns.Contains("SUBFAMILY_ID"))
                {
                    unPivotTable.Columns["SUBFAMILY_ID"].SetOrdinal(unPivotTable.Columns.IndexOf("SUBFAMILY_NAME"));
                }

                return unPivotTable;
            }

            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : UnPivotTable", ex);
                return null;
            }
        }
        #endregion

        public string catalogFamilyNameImport(DataSet dsImportDatas, string importtype, string batchId)
        {

            try
            {
                _logger.Info("Item# import Start");

                DataTable importData = UnPivotTableFamily(dsImportDatas.Tables[0]);
                _logger.Info("Get Data");
                DataTable attrType = dsImportDatas.Tables[1];
                dsImportDatas.Tables[0].Columns.Contains("VALUES");
                DataTable AttributeDataBatch = new DataTable();
                AttributeDataBatch.Columns.Add("Id");
                AttributeDataBatch.Columns["Id"].DataType = System.Type.GetType("System.Int32");
                AttributeDataBatch.Columns["Id"].AutoIncrement = true;
                AttributeDataBatch.Columns["Id"].AutoIncrementSeed = 1;
                AttributeDataBatch.Columns.Add("ATTRIBUTENAME");
                AttributeDataBatch.Columns.Add("ATTRIBUTETYPE");
                for (int index = 0; index < attrType.Rows.Count; index++)
                {
                    //if (!attrType.Rows[index]["ATTRIBUTE_NAME"].ToString().Contains("FAMILY_NAME"))
                    //{
                    DataRow dr = AttributeDataBatch.NewRow();
                    dr["ATTRIBUTENAME"] = attrType.Rows[index]["ATTRIBUTE_NAME"].ToString();
                    dr["ATTRIBUTETYPE"] = attrType.Rows[index]["ATTRIBUTE_TYPE"].ToString();
                    AttributeDataBatch.Rows.Add(dr);
                    //}
                }
                var catalogId = dsImportDatas.Tables[3].Rows[0]["VALUE"];
                var customerName = dsImportDatas.Tables[2].Rows[0]["CREATED_USER"].ToString();
                var customerId = dsImportDatas.Tables[2].Rows[0]["CUSTOMERID"].ToString();
                DataSet resultSet1 = new DataSet();
                SqlCommand sqlCommand = new SqlCommand();
                _logger.Info("STP Call for import-Start");
                using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))
                {
                    sqlCommand = new SqlCommand("STP_CATALOGSTUDIO5_FAMILY_WITHOUT_HIERARCHY_IMPORT", sqlConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    sqlCommand.Parameters.Add("@SESSIONID", SqlDbType.NVarChar).Value = batchId.ToString();
                    sqlCommand.Parameters.Add("@CUSTOMERNAME", SqlDbType.VarChar).Value = customerName;
                    sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.VarChar).Value = customerId;
                    sqlCommand.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = importData;
                    sqlCommand.Parameters.Add("@ATTRTEMP", SqlDbType.Structured).Value = AttributeDataBatch;
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlConnection.Open();
                    sqlDataAdapter.Fill(resultSet1);
                    sqlConnection.Close();
                }
                _logger.Info("STP Call for import-end");
                if (resultSet1 != null && resultSet1.Tables[0].Rows.Count > 0)
                {
                    string logName = resultSet1.Tables[0].Columns.Contains("STATUS") ? "SELECTCATSUCCESSLOG" : "SELECTERRORLOG";
                    //if(logName== "SELECTCATSUCCESSLOG")
                    //{

                    DataTable errorLog = new DataTable();
                    if (sqlConnection.State.ToString().ToUpper() == "CLOSED")
                    {
                        using (sqlConnection = new SqlConnection(connection.ConnectionOpen()))
                        {
                            sqlConnection.Open();
                            if (logName == "SELECTERRORLOG")
                            {
                                sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##LOGTEMPTABLE" + batchId + "'')BEGIN DROP TABLE [##LOGTEMPTABLE" + batchId + "] END')";
                                _logger.Info("Import Failed");
                            }
                            else
                            {
                                _logger.Info("Import Success");
                                sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + batchId + "'')BEGIN DROP TABLE [##IMPORTTEMP" + batchId + "] END')";
                            }
                            SqlCommand _DBCommand = new SqlCommand(sqlString, sqlConnection);
                            _DBCommand.ExecuteNonQuery();
                            if (logName == "SELECTERRORLOG")
                            {
                                sqlString = CreateTable("[##LOGTEMPTABLE" + batchId + "]", resultSet1.Tables[0]);
                            }
                            else
                            {
                                sqlString = CreateTable("[##IMPORTTEMP" + batchId + "]", resultSet1.Tables[0]);
                            }
                            SqlCommand dbCommandnew = new SqlCommand(sqlString, sqlConnection);
                            dbCommandnew.ExecuteNonQuery();
                            if (logName == "SELECTERRORLOG")
                            {
                                var bulkCopy = new SqlBulkCopy(sqlConnection)
                                {
                                    DestinationTableName = "[##LOGTEMPTABLE" + batchId + "]"
                                };
                                bulkCopy.WriteToServer(resultSet1.Tables[0]);
                            }
                            else
                            {
                                var bulkCopy = new SqlBulkCopy(sqlConnection)
                                {
                                    DestinationTableName = "[##IMPORTTEMP" + batchId + "]"
                                };
                                bulkCopy.WriteToServer(resultSet1.Tables[0]);
                            }
                            _logger.Info("Update Status");
                            using (sqlCommand = new SqlCommand("STP_LS_IMPORTBATCH", sqlConnection))
                            {
                                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                                sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NChar).Value = batchId;
                                sqlCommand.Parameters.Add("@OPTION", SqlDbType.NChar).Value = logName;
                                sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                                sqlDataAdapter.Fill(errorLog);

                            }
                        }
                    }
                    return "Import Sucess";
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Main : Program - ", ex);
            }
            return null;
        }
        #endregion

        public string ProductDelete(string batchId, DataTable deletedRecords, DataTable dtImportAttr, string importType, int allowDup, int customerId, string customerName, string stpName)
        {
            try
            {
                //customerId = 0;
                string importTemp;
                importTemp = Guid.NewGuid().ToString();
                DataTable replaceColumn = new DataTable();
                DataTable errorLog = new DataTable();
                string sqlString = string.Empty;
                foreach (DataColumn dc in deletedRecords.Columns)
                {
                    if (dc.ColumnName.ToString().ToUpper().Contains("ACTION"))
                    {
                        deletedRecords.Columns.Remove(dc);
                        break;
                    }
                }
                // DataTable deletedRecords = GetFilteredData(dtImportData, "DELETE");
                DataTable excelData = new DataTable();

                if (importType.ToUpper().Contains("SUBPRODUCT") && deletedRecords.Columns.Contains("SUBITEM#"))
                {
                    deletedRecords.Columns["SUBITEM#"].ColumnName = "SUBCATALOG_ITEM_NO";
                }
                if (importType.ToUpper().Contains("SUBPRODUCT"))
                {
                    if (deletedRecords.Columns.Contains("SUBPRODUCT_ID"))
                    {
                        var productIdaval = deletedRecords.Select("SUBPRODUCT_ID<>'' and  SUBPRODUCT_ID is not null");
                        if (productIdaval.Length == deletedRecords.Rows.Count)
                            excelData = deletedRecords.DefaultView.ToTable(false, "SUBPRODUCT_ID");
                    }
                    excelData = deletedRecords.DefaultView.ToTable(false, "SUBCATALOG_ITEM_NO");
                    excelData.Columns.Add("REMOVE_PRODUCT");
                    excelData.Select("SUBCATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_PRODUCT"] = "YES");
                }
                else
                {
                    if (deletedRecords.Columns.Contains("PRODUCT_ID"))
                    {
                        var productIdaval = deletedRecords.Select("Product_id<>'' and  Product_id is not null");
                        if (productIdaval.Length == deletedRecords.Rows.Count)
                            excelData = deletedRecords.DefaultView.ToTable(false, "PRODUCT_ID");
                    }
                    excelData = deletedRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO");
                    excelData.Columns.Add("REMOVE_PRODUCT");
                    excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_PRODUCT"] = "YES");
                }

                DataTable oattType = new DataTable();
                DataSet replace = new DataSet();

                int ItemVal = Convert.ToInt32(allowDup);
                if (deletedRecords.Rows.Count == 0)
                {
                    return deletedRecords.Rows.Count.ToString();
                }
                using (var conn = new SqlConnection(connection.ConnectionOpen()))
                {
                    conn.Open();
                    if (importType.ToUpper().Contains("SUBPRODUCT"))
                    {

                        sqlString =
                       "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPRODUCTIMPORTTEMP" +
                       importTemp + "]'')BEGIN DROP TABLE [##SUBPRODUCTIMPORTTEMP" + importTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(sqlString, conn);
                        _DBCommand.ExecuteNonQuery();
                        sqlString = CreateTableToImport("[##SUBPRODUCTIMPORTTEMP" + importTemp + "]", excelData);
                        SqlCommand _DBCommandnew = new SqlCommand(sqlString, conn);
                        _DBCommandnew.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##SUBPRODUCTIMPORTTEMP" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(excelData);

                        var cmd1 = new SqlCommand("ALTER TABLE [##SUBPRODUCTIMPORTTEMP" + importTemp + "] ADD STATUS nvarchar(20)", conn);
                        cmd1.ExecuteNonQuery();

                        var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_SUBPRODUCTIMPORT ''" + importTemp + "'',''" + ItemVal + "'',''" + customerId + "'',''" + customerName + "''')", conn) { CommandTimeout = 0 };
                        cmd.ExecuteNonQuery();
                        sqlString = @"select * from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                        var ds = CreateDataSet(sqlString);

                    }
                    else
                    {
                        sqlString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(sqlString, conn);
                        _DBCommand.ExecuteNonQuery();
                        sqlString = CreateTableToImport("[##IMPORTTEMP" + importTemp + "]", excelData);
                        SqlCommand _DBCommandnew = new SqlCommand(sqlString, conn);
                        _DBCommandnew.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(excelData);

                        _DBCommand =
                            new SqlCommand(
                                "ALTER TABLE [##IMPORTTEMP" + importTemp +
                                "] ADD STATUS nvarchar(20)",
                                conn);
                        _DBCommand.ExecuteNonQuery();
                        _DBCommand = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + customerName + "''')", conn) { CommandTimeout = 0 };
                        _DBCommand.ExecuteNonQuery();
                        sqlString = @"select * from [##LOGTEMPTABLE" + importTemp + "]";
                        DataSet ds = CreateDataSet(sqlString);
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            return ds.Tables[0].Rows.Count.ToString();

                    }
                    return deletedRecords.Rows.Count.ToString();
                }

                return "Import Sucess";
            }
            catch (Exception ex)
            {
                ErrorLogUpdate(ex.Message, batchId, stpName);
                _logger.Error("Error at ProductImport : Program - ", ex);

                return null;
            }
        }

        public DataTable GetFilteredData(DataTable filterData, string action)
        {
            try
            {
                DataTable deletedData = new DataTable();
                deletedData = filterData.Clone();
                foreach (DataRow dr in filterData.Rows)
                {
                    if (action == "INSERT")
                    {
                        if (string.IsNullOrEmpty(dr["ACTION"].ToString()))
                            deletedData.Rows.Add(dr.ItemArray);
                    }
                    else
                    {
                        if (dr["ACTION"].ToString().ToUpper() == action)
                            deletedData.Rows.Add(dr.ItemArray);
                    }
                }
                return deletedData;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ProductImport : Program - ", ex);

                return null;
            }
        }

    }
}
