﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace BatchProcess
{
    class Connection 
    {
        SqlConnection sqlConn;
        public string ConnectionOpen()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString();
           

            return connectionString;
        }
        public string GetStpName()
        {
            return ConfigurationManager.AppSettings["STOREDPROCEDURE"].ToString();
        }
    }
}
