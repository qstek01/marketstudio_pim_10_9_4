﻿
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;
//using System.Web.Mvc;
using WebSyncAPI.Models;



namespace WebSyncAPI.Controllers
{
    [RoutePrefix("WebSyncApi")]
    public class WebSyncApiController : ApiController
    {

        private static ILog _logger = LogManager.GetLogger(typeof(WebSyncApiController));
        private WebSyncEntities _webSyncDB = new WebSyncEntities();
        #region CheckConnection
        [Route("~/CheckWebApiConnection")]
        [HttpGet]
        public bool CheckWebApiConnection()
        {
            return true;
        }
        #endregion
        #region GetSessionId
        [Route("~/SessionId")]
        [HttpGet]
        public string SessionId()
        {
            string sessionID = string.Empty;
            DataSet sessoionDataSet = new DataSet();
            string _connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();
            try
            {

                using (SqlConnection _SQLConn = new SqlConnection(_connection))
                {
                    SqlCommand cmd = new SqlCommand("SELECT NEWID()", _SQLConn);
                    SqlDataAdapter Datadapter = new SqlDataAdapter(cmd);
                    Datadapter.Fill(sessoionDataSet);
                    return _connection = sessoionDataSet.Tables[0].Rows[0][0].ToString();
                }

            }
            catch (Exception objexception)
            {
                _logger.Error("Error at GetSyncDataQueue : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "SessionId";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }


                return null;
            }
            return null;
        }

        #endregion
        #region GetSynDataQueue
        [HttpGet]
        [Route("~/GetSyncDataQueue")]
        public System.Web.Mvc.JsonResult GetSyncDataQueue(int catalogId)
        {
            try
            {
                string _Connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();

                if (catalogId != 0)
                {
                    DataTable dt = new DataTable();
                    using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                    {
                        _SQLConn.Open();
                        SqlCommand sqlCommand = new SqlCommand("SPT_Get2WS_SYNCDATAQueue", _SQLConn);
                        sqlCommand.CommandTimeout = 0;
                        sqlCommand.Parameters.Add(new SqlParameter("@Catelog_ID", catalogId));
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                        sqlDataAdapter.Fill(dt);
                    }
                    List<DataRow> list = dt.AsEnumerable().ToList();
                    List<object> QueueList = new List<object>();
                    //var syncDataQueue = _webSyncDB.SPT_Get2WS_SYNCDATAQueue(catalogId).ToList();

                    foreach (var item in list)
                    {
                        QueueList.Add(

                           new CategoryFamilyProduct
                           {
                               Table_Type = Convert.ToString(item["Table_Type"]),
                               Type_Id = Convert.ToString(item["Type_Id"]),
                               Modified_Date = Convert.ToDateTime(item["Modified_Date"])

                           });
                    }
                    return new System.Web.Mvc.JsonResult() { Data = QueueList };

                }
                return null;
            }

            catch (Exception objexception)
            {

                _logger.Error("Error at GetSyncDataQueue : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "SPT_Get2WS_SYNCDATAQueue";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }

                return null;
            }
        }
        #endregion
        #region GetSyncUpdateData
        [AcceptVerbs("GET", "POST")]
        [Route("~/SyncUpdateData")]
        public System.Web.Mvc.JsonResult SyncUpdateData(int catalogId, [FromUri] string[] category_id)
        {
            DataSet DS = new DataSet();
            int pcount = 1;

            string _Connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();
            try
            {
                using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                {

                    if (category_id != null && catalogId != 0 && (category_id[0].Contains("'CAT") || category_id[0].Contains("'~CAT")))
                    {
                        SqlCommand Command = new SqlCommand("SPT_Get2WS_SYNCUPDATEDDATA", _SQLConn);
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add(new SqlParameter("@Catelog_ID", catalogId));
                        foreach (string str in category_id)
                        {
                            Command.Parameters.Add(new SqlParameter("@Category_IDs" + pcount, str.TrimEnd(',')));
                            pcount = pcount + 1;
                        }
                        SqlDataAdapter DataAdapter = new SqlDataAdapter(Command);
                        DataAdapter.Fill(DS);

                        List<object> QueueList = new List<object>();
                        QueueList.Add(DS);
                        return new System.Web.Mvc.JsonResult() { Data = QueueList };



                    }
                }

                return null;
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at SyncUpdateData : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "SyncUpdateData";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }
                return null;
            }
        }
        #endregion
        #region SyncData
        [Route("~/SyncData")]
        [HttpPost]
        public IHttpActionResult SyncData([FromBody]SyncData Xml)
        {
            try
            {
                string _Connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();

                using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                {
                    SqlCommand Command = new SqlCommand("STP_INSERT_SYNCTABLE", _SQLConn);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = 0;
                    Command.Parameters.Add(new SqlParameter("@SESSION_ID", Xml.sessionId));
                    Command.Parameters.Add(new SqlParameter("@XML_PACKET", Xml.xmldata.InnerXml));
                    Command.Parameters.Add(new SqlParameter("@SESSION_STATUS", "Received"));
                    Command.Parameters.Add(new SqlParameter("@JOB_TYPE", Xml.jobType));
                    Command.Parameters.Add(new SqlParameter("@JOB_NAME", Xml.jobName));
                    Command.Parameters.Add(new SqlParameter("@UPDATED_TYPE", Xml.updateType));
                    Command.Parameters.Add(new SqlParameter("@CREATED_USER", Xml.createdUser));
                    Command.Parameters.Add(new SqlParameter("@CREATED_DATE", DateTime.Now));
                    Command.Parameters.Add(new SqlParameter("@ISREMOVED", false));
                    _SQLConn.Open();
                    Command.ExecuteNonQuery();
                    _SQLConn.Close();
                }

                //  var dataPacket = Xml.xmldata.Replace("'", "''");

                //using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                //{
                //    TB2WS_SYNCDATA syncData = new TB2WS_SYNCDATA();
                //    syncData.SESSION_ID = Xml.sessionId;
                //    syncData.XML_PACKET = Xml.xmldata.InnerXml;
                //    syncData.SESSION_STATUS = "Received";
                //    syncData.JOB_TYPE = Xml.jobType;
                //    syncData.JOB_NAME = Xml.jobName;
                //    syncData.UPDATED_TYPE = Xml.updateType;
                //    syncData.CREATED_USER = Xml.createdUser;
                //    syncData.CREATED_DATE = DateTime.Now;
                //    syncData.ISREMOVED = false;
                //    _webSyncDB.TB2WS_SYNCDATA.Add(syncData);
                //    _webSyncDB.SaveChanges();
                //}
                return Ok("");


            }
            catch (Exception objexception)
            {
                _logger.Error("Error at SyncData : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "SyncData";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }
                return null;
            }

        }
        #endregion
        #region GetAttributeGroup
        [Route("~/GetAttributeGroup")]
        [HttpPost]
        public IHttpActionResult GetAttributeGroup([FromBody]SyncData Xml)
        {
            try
            {
                string _Connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();
                string sessionId = Xml.sessionId;

                // var dataPacket = Xml.xmldata.Replace("'", "''");



                using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                {
                    SqlCommand Command = new SqlCommand("STP_INSERT_SYNCTABLE", _SQLConn);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = 0;
                    Command.Parameters.Add(new SqlParameter("@SESSION_ID", Xml.sessionId));
                    Command.Parameters.Add(new SqlParameter("@XML_PACKET", Xml.xmldata.InnerXml));
                    Command.Parameters.Add(new SqlParameter("@SESSION_STATUS", "Received"));
                    Command.Parameters.Add(new SqlParameter("@JOB_TYPE", Xml.jobType));
                    Command.Parameters.Add(new SqlParameter("@JOB_NAME", Xml.jobName));
                    Command.Parameters.Add(new SqlParameter("@UPDATED_TYPE", Xml.updateType));
                    Command.Parameters.Add(new SqlParameter("@CREATED_USER", Xml.createdUser));
                    Command.Parameters.Add(new SqlParameter("@CREATED_DATE", DateTime.Now));
                    Command.Parameters.Add(new SqlParameter("@ISREMOVED", false));
                    _SQLConn.Open();
                    Command.ExecuteNonQuery();
                    _SQLConn.Close();
                }

                //using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                //{
                //    TB2WS_SYNCDATA syncData = new TB2WS_SYNCDATA();
                //    syncData.SESSION_ID = Xml.sessionId;
                //    syncData.XML_PACKET = Xml.xmldata.InnerXml;
                //    syncData.SESSION_STATUS = "Received";
                //    syncData.JOB_TYPE = Xml.jobType;
                //    syncData.JOB_NAME = Xml.jobName;
                //    syncData.UPDATED_TYPE = Xml.updateType;
                //    syncData.CREATED_USER = Xml.createdUser;
                //    syncData.CREATED_DATE = DateTime.Now;
                //    syncData.ISREMOVED = false;
                //    _webSyncDB.TB2WS_SYNCDATA.Add(syncData);
                //    _webSyncDB.SaveChanges();

                //    // var insertAttrGroup = _webSyncDB.STP_CATALOGSTUDIO_XML_ATTRIBUTEGROUP_INSERTION(sessionId);
                //}

                using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                {
                    string websync_STP = string.Empty;

                    if (Xml.jobName == "PICKLIST")
                    {
                        websync_STP = "STP_CATALOGSTUDIO_XML_PICKLIST_INSERTION";
                    }

                    else if (Xml.jobName == "ATTR_GROUP")
                    {
                        websync_STP = "STP_CATALOGSTUDIO_XML_ATTRIBUTEGROUP_INSERTION";
                    }
                    else if (Xml.jobName == "ATTR_GROUP_DELETION")
                    {
                        websync_STP = "STP_CATALOGSTUDIO_XML_ATTRIBUTEGROUP_DELETION";
                    }

                    if (Xml.jobName == "PICKLIST" || Xml.jobName == "ATTR_GROUP" || Xml.jobName == "ATTR_GROUP_DELETION")
                    {
                        SqlCommand Command_Attr = new SqlCommand(websync_STP, _SQLConn);
                        Command_Attr.CommandType = CommandType.StoredProcedure;
                        Command_Attr.Parameters.Add(new SqlParameter("@SESSION_ID", Xml.sessionId));
                        _SQLConn.Open();
                        Command_Attr.ExecuteNonQuery();
                        _SQLConn.Close();
                    }
                }


                return Ok("");


            }
            catch (Exception objexception)
            {
                _logger.Error("Error at GetAttributeGroup : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "GetAttributeGroup";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }
                return null;
            }

        }
        #endregion
        #region CallSyncLiveUpdate

        [HttpPost]
        [Route("~/CallSyncLiveUpdate")]
        public IHttpActionResult CallSyncLiveUpdate(string sessionId)
        {
            try
            {
                string _Connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();
                using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                {
                    SqlCommand Command = new SqlCommand("STP_2WS_XML_INSERTUPDATE", _SQLConn);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = 0;
                    Command.Parameters.Add(new SqlParameter("@PSession_ID", sessionId));
                    _SQLConn.Open();
                    Command.ExecuteNonQuery();
                    _SQLConn.Close();
                }
                //using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                //{
                //    var insertUpdateData = _webSyncDB.STP_2WS_XML_INSERTUPDATE(sessionId);

                //    //  var imageFormatConversion = _webSyncDB.stp_image_format_conversion();
                //}
                return Ok("Updated in destination DB");

            }
            catch (Exception objexception)
            {
                _logger.Error("Error at CallSyncLiveUpdate : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "CallSyncLiveUpdate";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }

                return null;
            }
        }



        #endregion
        #region GetProcErrorLog
        [Route("~/ProcErrorLog")]
        [HttpGet]
        public System.Web.Mvc.JsonResult ProcErrorLog(DateTime fromDate, DateTime toDate)
        {
            try
            {
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {
                    List<object> synDBErrorLog = new List<object>();
                    if (!string.IsNullOrEmpty(fromDate.ToString()) && !string.IsNullOrEmpty(toDate.ToString()))
                    {
                        var getDBErrorLog = _webSyncDB.STP_Get2WS_PROCERRORLOG(fromDate, toDate).ToList();

                        synDBErrorLog.Add(getDBErrorLog);
                    }
                    return new System.Web.Mvc.JsonResult() { Data = synDBErrorLog, };



                }

            }
            catch (Exception objexception)
            {
                _logger.Error("Error at ProcErrorLog : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "ProcErrorLog";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }
                return null;
            }
        }
        #endregion
        #region GetSyncResult
        [Route("~/GetSyncResult")]
        [HttpGet]
        public string GetSyncResult(string sessionId)
        {
            string xmlResultValue = "<tradingbell2ws_productpacket_result>";
            try
            {

                var getSyncResult = _webSyncDB.TB2WS_SYNCDATA.Where(x => x.SESSION_ID == sessionId).Select(x => new
                {
                    x.SESSION_STATUS,
                    x.SYNC_RESULT
                }).ToList();
                foreach (var result in getSyncResult)
                {
                    if (result.SESSION_STATUS.ToString() == "Update" || result.SESSION_STATUS.ToString() == "Failed")
                    {
                        xmlResultValue = xmlResultValue + result.SYNC_RESULT.ToString();
                    }
                    else if (result.SESSION_STATUS.ToString() == "Received")
                    {
                        xmlResultValue = "1";

                    }
                    else if (result.SESSION_STATUS.ToString() == "In Progress")
                    {
                        xmlResultValue = "2";
                    }
                }
                if (xmlResultValue.Contains("tradingbell2ws_productpacket_result"))
                {
                    xmlResultValue = xmlResultValue + "</tradingbell2ws_productpacket_result>";
                }


            }
            catch (Exception objexception)
            {
                _logger.Error("Error at GetSyncResult : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "GetSyncResult";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }

                return null;
            }
            return xmlResultValue;
        }
        #endregion
        #region DeleteSyncLog
        [Route("~/DeleteSyncLog")]
        [HttpPost]
        public string DeleteSyncLog(string PacketIds, string choice)
        {
            try
            {
                var DeleteSyncLog = _webSyncDB.STP_2WSSyncRemoveDelete(PacketIds, choice);
                return "Deleted";
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at DeleteSyncLog : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "DeleteSyncLog";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }

                return null;
            }

        }
        #endregion
        #region  DeleteSyncErrorLog
        [Route("~/DeleteSyncErrorLog")]
        [HttpPost]
        public string DeleteSyncErrorLog(string errorIds, string choice)
        {
            try
            {
                var deleteErrorLog = _webSyncDB.STP_2WSErrorLogRemoveDelete(errorIds, choice);
                return "Deleted";
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at DeleteSyncErrorLog : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "DeleteSyncErrorLog";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }

                return null;
            }
        }

        #endregion
        #region GetXMLData
        [Route("~/GetXMLData")]
        [HttpGet]
        public System.Web.Mvc.JsonResult GetXMLData(int PacketID)
        {
            try
            {
                var xmlData = _webSyncDB.SPT_Get2WS_XMLDATA(PacketID);
                List<object> xmlDatas = new List<object>();
                xmlDatas.Add(xmlData);
                return new System.Web.Mvc.JsonResult() { Data = xmlDatas };

            }
            catch (Exception objException)
            {
                _logger.Error("Error at GetXMLData : WebSyncApiController", objException);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "GetXMLData";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objException.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }

                return null;
            }
        }
        #endregion
        #region syncDataLog
        [Route("~/syncDataLog")]
        [HttpPost]
        public System.Web.Mvc.JsonResult syncDataLog(string Task)
        {
            try
            {
                List<object> synLog = new List<object>();
                if (Task == "All")
                {

                    var getSynDataLog = _webSyncDB.STP_Get2WS_SYNCDATALOG(Task).Select(x => new { x.Id, x.JOB_TYPE, x.RECORD_COUNT, x.UPDATED_DATE }).ToList();
                    synLog.Add(getSynDataLog);
                }
                else
                {
                    var getSynDataLog = _webSyncDB.STP_Get2WS_SYNCDATALOG(Task).Select(x => new { x.Id, x.Status, x.Message, x.UPDATED_DATE }).ToList();
                    synLog.Add(getSynDataLog);

                }
                return new System.Web.Mvc.JsonResult() { Data = synLog };
            }
            catch (Exception objException)
            {
                _logger.Error("Error at syncDataLog : WebSyncApiController", objException);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "syncDataLog";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objException.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }
                return null;
            }
        }
        #endregion
        #region UploadImage
        [Route("~/UploadImage")]
        [HttpPost]
        public IHttpActionResult UploadImage(ImagePDFModel ImageFormat)
        {
            try
            {


                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {
                    System.IO.MemoryStream ImgMemoryStream = new System.IO.MemoryStream(ImageFormat.Image);

                    System.Drawing.Bitmap ImgBitmap = (System.Drawing.Bitmap)Image.FromStream(ImgMemoryStream);
                    string img_path = string.Empty;
                    img_path = ConfigurationManager.AppSettings["IMAGEPATH"].ToString();
                    int fna1 = ImageFormat.ImageName.LastIndexOf("\\");
                    string fna2 = ImageFormat.ImageName.Substring(0, fna1);
                    DirectoryInfo dirInfo = new DirectoryInfo(img_path + fna2);
                    dirInfo.Create();
                    ImgBitmap.Save(@img_path + ImageFormat.ImageName);
                }
                return Ok("");


            }
            catch (Exception objexception)
            {
                _logger.Error("Error at UploadImage : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "UploadImage";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }

                return null;
            }

        }
        #endregion
        #region PutFile
        [Route("~/PutFile")]
        [HttpPost]
        public bool PutFile(PDFattachementModel PDFFormat)
        {
            string pdf_path = string.Empty;
            pdf_path = ConfigurationManager.AppSettings["ImagePDFFile"].ToString();
            int fna1 = PDFFormat.pdf.LastIndexOf("\\");
            string fna2 = PDFFormat.pdf.Substring(0, fna1);
            DirectoryInfo dirInfo = new DirectoryInfo(pdf_path + fna2);
            dirInfo.Create();
            //string pdf_path = string.Empty;
            //pdf_path = ConfigurationManager.AppSettings["PDFPATH"].ToString();
            MemoryStream ms = new MemoryStream(PDFFormat.PDFName);
            System.IO.FileInfo fil1 = new FileInfo(pdf_path + '\\' + PDFFormat.pdf);
            if (fil1.Exists)
            {
                return true;

            }
            else
            {

                FileStream fs = new FileStream(pdf_path + PDFFormat.pdf, FileMode.CreateNew, FileAccess.ReadWrite);
                //FileStream fs = new FileStream(pdf_path + filename, filename, FileMode.Create);
                ms.WriteTo(fs);
                // clean up
                ms.Close();
                fs.Close();
                fs.Dispose();
                return false;
            }
            // return true;

        }
        #endregion
        #region RemoveLog
        [Route("~/RemoveLog")]
        [HttpPost]
        public bool RemoveLog(ImagePDFModel noOfDays)
        {
            try
            {

                string _connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();

                using (SqlConnection _SQLConn = new SqlConnection(_connection))
                {
                    _SQLConn.Open();
                    SqlCommand sqlCommand = new SqlCommand("DeletedSyncData", _SQLConn);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Parameters.Add("@days", SqlDbType.Int).Value = noOfDays.noOfDays;
                    sqlCommand.ExecuteNonQuery();
                    _SQLConn.Close();
                }
                return true;

            }
            catch (Exception objexception)
            {
                //_logger.Error("Error at RemoveLog : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "RemoveLog";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }

                return false;
            }
        }
        #endregion
        #region GetLastRun
        [Route("~/GetLastRun")]
        [HttpGet]
        public string GetLastRun()
        {

            string result = string.Empty;
            string _connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();

            try
            {
                using (SqlConnection _SQLConn = new SqlConnection(_connection))
                {
                    _SQLConn.Open();
                    SqlCommand Command = new SqlCommand("SPT_Get2WS_LASTRUN", _SQLConn);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = 0;
                    Command.ExecuteNonQuery();
                    result = Command.ExecuteScalar().ToString();
                    _SQLConn.Close();
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at GetLastRun : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "GetLastRun";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }
                return null;
            }

            return result;
        }
        #endregion
        #region GetAutoSyncUpdateData
        [AcceptVerbs("GET", "POST")]
        [Route("~/AutoSyncUpdateData")]
        public DataSet AutoSyncUpdateData(AutoWebSyncData UpdateData)
        {
            DataSet DS = new DataSet();

            string _Connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();
            try
            {
                using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                {
                    SqlCommand Command = new SqlCommand("SPT_Get2WSAuto_SYNCUPDATEDDATA", _SQLConn);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add(new SqlParameter("@Catelog_ID", UpdateData.catalogId));
                    Command.Parameters.Add(new SqlParameter("@Category_IDs", UpdateData.categoryId[0]));
                    Command.Parameters.Add(new SqlParameter("@Type", UpdateData.type));
                    SqlDataAdapter DataAdapter = new SqlDataAdapter(Command);
                    DataAdapter.Fill(DS);
                    return DS;
                }

                return null;
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at AutoSyncUpdateData : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "AutoSyncUpdateData";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }
                return null;
            }
        }
        #endregion
        #region AutoCallSyncLiveUpdate

        [HttpPost]
        [Route("~/AutoCallSyncLiveUpdate")]
        public IHttpActionResult AutoCallSyncLiveUpdate([FromBody]string sessionId)
        {
            try
            {
                string _Connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();

                using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                {
                    SqlCommand Command = new SqlCommand("STP_2WS_XML_INSERTUPDATE", _SQLConn);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = 180;
                    Command.Parameters.Add(new SqlParameter("@PSession_ID", sessionId));
                    _SQLConn.Open();
                    Command.ExecuteNonQuery();
                    _SQLConn.Close();

                }
                return Ok("Updated in destination DB");
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at AutoCallSyncLiveUpdate : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "AutoCallSyncLiveUpdate";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }
                return null;
            }
        }

        //[HttpPost]
        //[Route("~/ReturnXmlDocument")]
        //public string ReturnXmlDocument(HttpRequestMessage request)
        //{
        //    var doc = new XmlDocument();

        //    doc.Load(request.Content.ReadAsStreamAsync().Result);
        //    return doc.DocumentElement.OuterXml;
        //}
        [HttpPost]
        [Route("~/ReturnXmlDocument")]
        public string ReturnXmlDocument([FromBody]SyncData doc)
        {
            if (doc == null)
                return "Data is empty";
            return null;
        }

        #endregion


        /// <summary>
        /// IMPORT_TEMPLATE_XML
        /// </summary>
        /// 

        #region GetImportTemplates
        [Route("~/GetImportTemplates")]
        [HttpPost]
        public IHttpActionResult GetImportTemplates([FromBody]SyncData Xml)
        {
            try
            {
                string _Connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();
                string sessionId = Xml.sessionId;

                using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                {
                    SqlCommand Command = new SqlCommand("STP_INSERT_SYNCTABLE", _SQLConn);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = 0;
                    Command.Parameters.Add(new SqlParameter("@SESSION_ID", Xml.sessionId));
                    Command.Parameters.Add(new SqlParameter("@XML_PACKET", Xml.xmldata.InnerXml));
                    Command.Parameters.Add(new SqlParameter("@SESSION_STATUS", "Received"));
                    Command.Parameters.Add(new SqlParameter("@JOB_TYPE", Xml.jobType));
                    Command.Parameters.Add(new SqlParameter("@JOB_NAME", Xml.jobName));
                    Command.Parameters.Add(new SqlParameter("@UPDATED_TYPE", Xml.updateType));
                    Command.Parameters.Add(new SqlParameter("@CREATED_USER", Xml.createdUser));
                    Command.Parameters.Add(new SqlParameter("@CREATED_DATE", DateTime.Now));
                    Command.Parameters.Add(new SqlParameter("@ISREMOVED", false));
                    _SQLConn.Open();
                    Command.ExecuteNonQuery();
                    _SQLConn.Close();
                }

                using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                {
                    SqlCommand Command = new SqlCommand("STP_CATALOGSTUDIO_XML_IMPORT_INSERTION", _SQLConn);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = 180;
                    Command.Parameters.Add(new SqlParameter("@session_id", Xml.sessionId));
                    _SQLConn.Open();
                    Command.ExecuteNonQuery();
                    _SQLConn.Close();

                }

                return Ok("success");
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at GetImportTemplates : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "GetImportTemplates";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }
                return null;
            }

        }

        #endregion


        #region GetExportTemplate
        [Route("~/GetExportTemplate")]
        [HttpPost]
        public IHttpActionResult GetExportTemplate([FromBody]SyncData Xml)
        {
            try
            {
                string _Connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();
                string sessionId = Xml.sessionId;

                using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                {
                    SqlCommand Command = new SqlCommand("STP_INSERT_SYNCTABLE", _SQLConn);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = 0;
                    Command.Parameters.Add(new SqlParameter("@SESSION_ID", Xml.sessionId));
                    Command.Parameters.Add(new SqlParameter("@XML_PACKET", Xml.xmldata.InnerText));
                    Command.Parameters.Add(new SqlParameter("@SESSION_STATUS", "Received"));
                    Command.Parameters.Add(new SqlParameter("@JOB_TYPE", Xml.jobType));
                    Command.Parameters.Add(new SqlParameter("@JOB_NAME", Xml.jobName));
                    Command.Parameters.Add(new SqlParameter("@UPDATED_TYPE", Xml.updateType));
                    Command.Parameters.Add(new SqlParameter("@CREATED_USER", Xml.createdUser));
                    Command.Parameters.Add(new SqlParameter("@CREATED_DATE", DateTime.Now));
                    Command.Parameters.Add(new SqlParameter("@ISREMOVED", false));
                    _SQLConn.Open();
                    Command.ExecuteNonQuery();
                    _SQLConn.Close();
                }

                using (SqlConnection _SQLConn = new SqlConnection(_Connection))
                {
                    SqlCommand Command = new SqlCommand("STP_UPDATEXML_EXPORT_TEMPLATE", _SQLConn);
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.CommandTimeout = 180;
                    Command.Parameters.Add(new SqlParameter("@sessions_id", sessionId));
                    _SQLConn.Open();
                    Command.ExecuteNonQuery();
                    _SQLConn.Close();

                }

                return Ok("success");


            }
            catch (Exception objexception)
            {
                _logger.Error("Error at GetExportTemplate : WebSyncApiController", objexception);
                using (WebSyncEntities _webSyncDB = new WebSyncEntities())
                {

                    TB_ERROR_LOG logError = new TB_ERROR_LOG();
                    logError.ERROR_NUMBER = "1";
                    logError.ERROR_SEVERITY = "1";
                    logError.ERROR_STATE = "1";
                    logError.ERROR_PROCEDURE = "GetExportTemplate";
                    logError.ERROR_LINE = 0;
                    logError.ERROR_MESSAGE = objexception.Message;
                    logError.CREATED_DATE = DateTime.Now;
                    logError.PACKET_ID = 0;
                    _webSyncDB.TB_ERROR_LOG.Add(logError);
                    _webSyncDB.SaveChanges();
                }
                return null;
            }

        }
        #endregion


        //#region AutoCallSyncLiveXmlImportUpdate

        //[HttpPost]
        //[Route("~/AutoCallSyncLiveXmlImportUpdate")]
        //public IHttpActionResult AutoCallSyncLiveXmlImportUpdate([FromBody]string sessionId)
        //{
        //    try
        //    {
        //        string _Connection = ConfigurationManager.ConnectionStrings["WebSynDBConnection"].ToString();

        //        using (SqlConnection _SQLConn = new SqlConnection(_Connection))
        //        {
        //            SqlCommand Command = new SqlCommand("STP_UPDATEXML_IMPORTTEMPLATE", _SQLConn);
        //            Command.CommandType = CommandType.StoredProcedure;
        //            Command.CommandTimeout = 180;
        //            Command.Parameters.Add(new SqlParameter("@sessions_id", sessionId));
        //            _SQLConn.Open();
        //            Command.ExecuteNonQuery();
        //            _SQLConn.Close();

        //        }
        //        return Ok("Updated in destination DB");
        //    }
        //    catch (Exception objexception)
        //    {
        //        _logger.Error("Error at AutoCallSyncLiveXmlImportUpdate : WebSyncApiController", objexception);
        //        using (WebSyncEntities _webSyncDB = new WebSyncEntities())
        //        {

        //            TB_ERROR_LOG logError = new TB_ERROR_LOG();
        //            logError.ERROR_NUMBER = "1";
        //            logError.ERROR_SEVERITY = "1";
        //            logError.ERROR_STATE = "1";
        //            logError.ERROR_PROCEDURE = "AutoCallSyncLiveXmlImportUpdate";
        //            logError.ERROR_LINE = 0;
        //            logError.ERROR_MESSAGE = objexception.Message;
        //            logError.CREATED_DATE = DateTime.Now;
        //            logError.PACKET_ID = 0;
        //            _webSyncDB.TB_ERROR_LOG.Add(logError);
        //            _webSyncDB.SaveChanges();
        //        }
        //        return null;
        //    }
        //}

        //[HttpPost]
        //[Route("~/ReturnXmlimporttmp")]
        //public string ReturnXmlimporttmp([FromBody]SyncData doc)
        //{
        //    if (doc == null)
        //        return "Data is empty";
        //    return null;
        //}

        //#endregion

        public partial class CategoryFamilyProduct
        {
            //public int CatalogId { get; set; }
            public string Table_Type { get; set; }
            public string Type_Id { get; set; }
            public DateTime Modified_Date { get; set; }

        }

    }
}
