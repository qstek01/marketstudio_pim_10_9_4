﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSyncAPI.Models
{
    public class Error_Log
    {
        public string ERROR_NUMBER { get; set; }
        public string ERROR_SEVERITY { get; set; }
        public string ERROR_STATE { get; set; }
        public string ERROR_PROCEDURE { get; set; }
        public int ERROR_LINE { get; set; }
        public string ERROR_MESSAGE { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public int PACKET_ID { get; set; }
        public int ERROR_ID { get; set; }
        public bool ISREMOVED { get; set; }
    }
}