﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSyncAPI.Models
{
    public class AutoWebSyncData
    {
        public int catalogId { get; set; }
        public string[] categoryId { get; set; }
        public string type { get; set; }      
    }
}