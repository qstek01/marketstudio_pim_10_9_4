﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSyncAPI.Models
{
    public class ImagePDFModel
    {

        public string ImageName { get; set; }
        public byte[] Image { get; set; }
        public int noOfDays { get; set; }
    }
}