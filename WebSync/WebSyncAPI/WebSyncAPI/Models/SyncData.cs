﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Xml;

namespace WebSyncAPI.Models
{
    public class SyncData
    {

        public string sessionId { get; set; }
        public string createdUser { get; set; }
        public string updateType { get; set; }
        public string jobType { get; set; }
        public string jobName { get; set; }
        public XmlDocument xmldata { get; set; }
    }
}