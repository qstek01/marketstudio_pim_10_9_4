﻿using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace AutoWebSync
{
    class Program
    {
        static void Main(string[] args)
        {
            //args = "Data Source=SSTEK09\\SQL2017EXP;Initial Catalog=STG_LG_DAL;User ID=tbadmin;Password=data@2019,tbadmin,data@2019,http://10.0.0.123:86/,True,,,LG,8,".Split(',');
            //args = "Data Source=SSDBSVR\\SQLEXP14;Initial Catalog=SAAS_MIGRATION_20th_MARCH_2018;User ID=tbadmin;Password=data@2018,tbadmin,data@2018,http://localhost:11397/,True,2019-05-29 00:00:00,2019-05-30 21:00:00,AutoWebSyncss,4,".Split(',');
            if (args.Length > 0)
            {
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);                            
                config.AppSettings.Settings["ConnString"].Value = args[0].ToString();
                config.AppSettings.Settings["Url"].Value = args[3].ToString();
                config.AppSettings.Settings["categorychangesonly"].Value = args[4].ToString();
                config.AppSettings.Settings["catalogname"].Value = args[7].ToString();
                config.AppSettings.Settings["userId"].Value = args[8].ToString();
                int userId = 0;
                int.TryParse(config.AppSettings.Settings["userId"].Value, out userId);               
                config.Save(ConfigurationSaveMode.Modified);
                string conStr = ConfigurationManager.AppSettings["ConnString"].ToString();
                int _catalogID = 7;
                string fromDate;
                string toDate;
                bool status = false;
                string sessionID = "";
                string complete_familyid = "";
                string completed_Category = "";
                string jobIDs = "";
                string xml = string.Empty;
                string categoryID = string.Empty, familyID = string.Empty, productID = string.Empty, subfamilyId = string.Empty,subProductID;
              string root_categoryId = "0";
                bool final = true;
                bool category_changes = Convert.ToBoolean(ConfigurationManager.AppSettings["categorychangesonly"].ToString());
                string Upd_Type = "Time span";
             
                string fileSyncPath = args[9].ToString();
                Console.WriteLine(args[9].ToString());  
                WebClient client = new WebClient();           
                // For Choosing Catalogs for 

                try
                {
                    if (fileSyncPath != null && fileSyncPath != "")
                    {
                        System.Diagnostics.Process.Start(fileSyncPath);
                    }

                    //client.BaseAddress = new Uri(ConfigurationManager.AppSettings["Url"].ToString());
                    UltraTree ModifiedItemsTreeView = new UltraTree();
                    //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    SqlConnection _SQLConn = new SqlConnection(conStr);
                    //string userId = args[1].ToString(), password = args[2].ToString();
                    SyncData syncData = new SyncData();
                    syncData.ConnectionString = conStr;
                    syncData.GetUrl=args[3].ToString();
                    sessionID = syncData.CreateNewSyncSession();
                    Console.WriteLine("New Session Created");                
                    if (sessionID != string.Empty)
                    {
                       
                        DataSet CatalogDS = new DataSet();
                        try
                        {
                            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/Log"))
                                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/Log");
                            string FirstName = "/Log/log" + DateTime.Now.ToShortDateString().Replace("/", "").Trim() + ".txt";
                            string Path = AppDomain.CurrentDomain.BaseDirectory + FirstName;
                            Path = Path.Replace("\\", "/");
                            if (File.Exists(Path) == false)
                            {
                                FileStream objFileStream = new FileStream(Path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                                objFileStream.Close();
                            }
                            using (StreamWriter objStreamWriterTest = new StreamWriter(Path, true))
                            {
                                objStreamWriterTest.WriteLine("Passed Step 1");

                               
                            } 
                            SqlCommand Command = new SqlCommand();
                            SqlDataAdapter DataAdapter;
                            string receiptStatus;
                            string last_run_date = "";
                            last_run_date = syncData.GetLastRun().ToString();
                            last_run_date = last_run_date.Replace("\"", "");
                           
                            if (args[5].ToString().Trim() != "")
                            {
                                //fromDate = "2018-03-22 23:59:59.000";
                                //toDate = syncData.Getenddate();

                                if (args[5].ToString().Trim().Contains("1980-01-01"))
                                    Upd_Type = "Full";
                                else
                                    Upd_Type = "Time span";
                                if (args[6].ToString().Trim() != "")
                                {
                                    fromDate = args[5].ToString();
                                    toDate = args[6].ToString();
                                }
                                else
                                {
                                    fromDate = args[5].ToString();
                                    toDate = syncData.Getenddate();
                                    
                                }
                            }
                            else if (last_run_date != ""  && last_run_date !="\"\"" )
                            {
                                Upd_Type = "Last update";
                                fromDate = last_run_date;
                                toDate = syncData.Getenddate();
                                //fromDate = fromDate.Replace("\"\""," " );
                                //fromDate.Replace("\"", "");
                            }
                            else
                            {
                                Upd_Type = "Time span";
                                fromDate = syncData.GetStartdate();
                                toDate = syncData.Getenddate();
                               
                            }
                            if (Upd_Type == "Full")
                            {
                                try
                                {
                                    receiptStatus = syncData.RemoveDelete2WS_SyncLog("", "DELETEALL");
                                }
                                catch (Exception) { }
                            }
                            using (StreamWriter objStreamWriterTest1 = new StreamWriter(Path, true))
                            {
                                objStreamWriterTest1.WriteLine("Passed Step 2");
                                objStreamWriterTest1.WriteLine(fromDate, toDate);
                            }

                            DataSet CAT = new DataSet();
                            CAT = syncData.GetCatalogID();
                    
                            Console.WriteLine("Passed Step 33");  
                           string cat_id = CAT.Tables[0].Rows[0]["CATALOG_ID"].ToString();
                           Console.WriteLine("Passed Step 333");  
                            //---------------------------INDESIGN TEMPLATES-------------------------------
                            Console.WriteLine("Sending Indesign template....");
                            syncData.GetXmlIndesignTemplate(fromDate, toDate, userId, sessionID, cat_id);
                            Console.WriteLine("Passed Step 3333");  
                          //--------------------------
                            Console.WriteLine("Sending Deleted Items....");

                            //SEND THE DELETED ITEMS FIRST B4 SENDING MODIFIED ITEMS
                            SqlCommand cmd = new SqlCommand("STP_2WSTRANSACTIONHISTORY", _SQLConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@FROMDATE", SqlDbType.VarChar).Value = fromDate;
                            cmd.Parameters.Add("@TODATE", SqlDbType.VarChar).Value = toDate;
                            cmd.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = cat_id;
                            _SQLConn.Open();
                             SqlDataAdapter da = new SqlDataAdapter(cmd);
                            CatalogDS = new DataSet();
                            da.Fill(CatalogDS);
                            Console.WriteLine("Passed Step 3333");  
                            using (StreamWriter objStreamWriterTest2 = new StreamWriter(Path, true))
                            {
                                objStreamWriterTest2.WriteLine("Passed Step 3");
                                
                            }
                            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\XML"))
                                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\XML");
                            if (CatalogDS.Tables[0].Rows[0][0].ToString().Trim() != string.Empty)
                            {
                                xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" + CatalogDS.Tables[0].Rows[0][0].ToString();
                                receiptStatus = syncData.SyncDatas(sessionID, xml, userId, Upd_Type, "Auto", "DELETE");
                                string Deleted = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\myXmlF[Deleted]" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xml";
                                File.WriteAllText(Deleted, xml);
                              
                                if (receiptStatus != "")
                                {
                                    Command = new SqlCommand("DELETE FROM TB_TRANSACTION_HISTORY", _SQLConn);
                                    int State = Command.ExecuteNonQuery();
                                }
                                xml = "";
                                Console.WriteLine("xml....");
                            }
                            _SQLConn.Close();
                            //SEND MODIFIED ATTRIBUTES NEXT
                            Console.WriteLine("Sending Modified Attributes....");
                            cmd = new SqlCommand("STP_2WS_MODIFIED_ATTRIBUTES", _SQLConn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@FROM_DATE", fromDate));
                            cmd.Parameters.Add(new SqlParameter("@TO_DATE", toDate));
                            _SQLConn.Open();
                            da = new SqlDataAdapter(cmd);
                            CatalogDS = new DataSet();
                            da.Fill(CatalogDS);
                          
                            if (CatalogDS.Tables[0].Rows[0][0].ToString().Trim() != string.Empty)
                            {
                                
                               
                                xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" + CatalogDS.Tables[0].Rows[0][0].ToString();
                                receiptStatus = syncData.SyncDatas(sessionID, xml, userId, Upd_Type, "Auto", "ATTRIBUTES");
                                string AttributeModif = AppDomain.CurrentDomain.BaseDirectory + "\\XML\\myXmlA[Modified]" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xml";
                                File.WriteAllText(AttributeModif, xml);
                                Console.WriteLine(AttributeModif);
                                xml = "";
                            }
                            _SQLConn.Close();
                            ModifiedItemsTreeView.Nodes.Clear();
                            DataSet dc = new DataSet();
                            dc = syncData.GetCatalogID();
                          
                            foreach (DataRow drow in dc.Tables[0].Rows)
                            {
                               
                                Console.WriteLine("Sending modified items of catalog (" + drow[0].ToString() + ")....");
                                ModifiedItemsTreeView.Nodes.Clear();
                                _catalogID = Convert.ToInt32(drow[0].ToString());
                                syncData.GetXmlAttributeGroup(fromDate, toDate, _catalogID, userId, sessionID);
                                syncData.GetXmlPicklist(fromDate, toDate, userId, sessionID);
                                DataSet DsDQueue = new DataSet();
                                DataSet ds = syncData.GetCategoryData(fromDate, toDate, _catalogID, userId);

                                 foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    //if (dr["Category_ID"].ToString() == "BL014")
                                    //{
                                    //    MessageBox.Show("afsaf");
                                    //}
                                    UltraTreeNode treeNode = new UltraTreeNode("`" + dr["Category_ID"].ToString(), "[" + dr["Category_ID"].ToString() + "] " + dr["Category_Name"].ToString());
                                    treeNode.CheckedState = CheckState.Checked;
                                    //DsDQueue = syncData.Get2ws_syncUpdatedData(_catalogID, new string[] { "'" + dr["Category_ID"].ToString() + "'", "", "", "" }, "Category");
                                    //if (DsDQueue != null && DsDQueue.Tables[0].Rows.Count != 0)                      
                                    //{
                                    //    if (DsDQueue.Tables.Count > 0)
                                    //    {
                                    //        if (Upd_Type != "Full")
                                    //        {
                                    //            foreach (DataRow D_tbData in DsDQueue.Tables[0].Rows)
                                    //            {
                                    //                if (D_tbData["CATEGORY_ID"].ToString() == dr["CATEGORY_ID"].ToString()
                                    //                       && Convert.ToDateTime(D_tbData["Modified_Date"].ToString()).ToString() == dr["Modified_Date"].ToString())
                                    //                {
                                    //                    treeNode.CheckedState = CheckState.Unchecked;
                                    //                }
                                    //            }
                                    //        }
                                    //    }
                                    //}

                                    if (treeNode.CheckedState == CheckState.Checked)
                                    {
                                        DataSet ds1 = syncData.GetFamilyData(fromDate, toDate, _catalogID, treeNode.Key.Replace("`", ""), "All");
                                        UltraTreeNode treeNode1;
                                        if (ds1 != null && ds1.Tables.Count > 0)
                                        {
                                            //DsDQueue = syncData.Get2ws_syncUpdatedData(_catalogID, new string[] { "'" + dr["Category_ID"].ToString() + "'", "", "", "" }, "Family");
                                            foreach (DataRow dr1 in ds1.Tables[0].Rows)
                                            {
                                                if (Convert.ToString(dr1["ROOT_CATEGORY"]) != "0") //Cloning families
                                                {
                                                    treeNode1 = new UltraTreeNode("~" + dr1["CATEGORY_ID"].ToString() + "$" + dr1["ROOT_CATEGORY"].ToString() + "#" + dr1["Family_ID"].ToString(), "[" + dr1["Family_ID"].ToString() + "] " + dr1["Family_Name"].ToString());
                                                    treeNode1.CheckedState = CheckState.Checked;
                                                    //if (DsDQueue != null)
                                                    //{
                                                    //    if (DsDQueue.Tables.Count > 0)
                                                    //    {
                                                    //        if (Upd_Type != "Full")
                                                    //        {
                                                    //            foreach (DataRow D_tbData in DsDQueue.Tables[0].Rows)
                                                    //            {
                                                    //                if (D_tbData["FAMILY_ID"].ToString() == dr1["Family_ID"].ToString()
                                                    //                       && Convert.ToDateTime(D_tbData["FModified_Date"].ToString()).ToString() == dr1["Modified_Date"].ToString())
                                                    //                {
                                                    //                    treeNode1.CheckedState = CheckState.Unchecked;
                                                    //                }
                                                    //            }
                                                    //        }
                                                    //    }
                                                    //}
                                                    if (treeNode1.CheckedState == CheckState.Checked)
                                                    {
                                                        string categoryId = treeNode1.Key.Substring(1, treeNode1.Key.LastIndexOf('$') - 1);
                                                        root_categoryId = treeNode1.Key.Substring(treeNode1.Key.LastIndexOf('$'));
                                                        root_categoryId = root_categoryId.Substring(1, (root_categoryId.LastIndexOf('#') - 1));
                                                        string familyId = treeNode1.Key;
                                                        familyId = familyId.Substring(familyId.LastIndexOf('#') + 1);
                                                        DataSet ds2 = new DataSet();
                                                        ds2 = syncData.GetFamilyProducts(fromDate, toDate, _catalogID, familyId, "All");
                                                        if (ds != null && ds2.Tables.Count > 0 && ds2.Tables[0].Rows.Count > 0)
                                                        {
                                                            foreach (DataRow dr2 in ds2.Tables[0].Rows)
                                                            {
                                                                UltraTreeNode treeNode2 = new UltraTreeNode("#" + categoryId + "@" + familyId + "$" + root_categoryId + "+" + dr2["Product_ID"].ToString(), "[Cat#] " + dr2["String_Value"].ToString());
                                                                treeNode2.Tag = dr2["Product_ID"].ToString();
                                                                treeNode1.Nodes.Add(treeNode2);
                                                            }
                                                        }


                                                        DataSet ds3 = syncData.GetSubFamilyData(fromDate, toDate, _catalogID, familyId, categoryId, "All");
                                                        if (ds3 != null && ds3.Tables.Count > 0)
                                                        {
                                                            foreach (DataRow dr3 in ds3.Tables[0].Rows)
                                                            {
                                                                UltraTreeNode treeNode2 = new UltraTreeNode("~" + categoryId + "$" + dr3["ROOT_CATEGORY"].ToString() + "#" + dr3["Family_ID"].ToString(), "[" + dr3["Family_ID"].ToString() + "] " + dr3["Family_Name"].ToString());
                                                                DataSet ds4 = new DataSet();
                                                                categoryId = treeNode2.Key.Substring(1, treeNode2.Key.LastIndexOf('$') - 1);
                                                                root_categoryId = treeNode2.Key.Substring(treeNode2.Key.LastIndexOf('$'));
                                                                root_categoryId = root_categoryId.Substring(1, (root_categoryId.LastIndexOf('#') - 1));
                                                                familyId = treeNode2.Key;
                                                                familyId = familyId.Substring(familyId.LastIndexOf('#') + 1);
                                                                ds4 = syncData.GetFamilyProducts(fromDate, toDate, _catalogID, familyId, "All");
                                                                if (ds4 != null && ds4.Tables.Count > 0 && ds4.Tables[0].Rows.Count > 0)
                                                                {
                                                                    foreach (DataRow dr2 in ds4.Tables[0].Rows)
                                                                    {
                                                                        UltraTreeNode treeNode3 = new UltraTreeNode("#" + categoryId + "@" + familyId + "$" + root_categoryId + "+" + dr2["Product_ID"].ToString(), "[Cat#] " + dr2["String_Value"].ToString());
                                                                        treeNode3.Tag = dr2["Product_ID"].ToString();
                                                                        treeNode2.Nodes.Add(treeNode3);
                                                                    }
                                                                    treeNode1.Nodes.Add(treeNode2);
                                                                }
                                                            }
                                                        }
                                                        treeNode.Nodes.Add(treeNode1);
                                                    }
                                                }
                                                else
                                                {
                                                    treeNode1 = new UltraTreeNode("~" + dr1["Family_ID"].ToString(), "[" + dr1["Family_ID"].ToString() + "] " + dr1["Family_Name"].ToString());
                                                    treeNode1.CheckedState = CheckState.Checked;
                                                    if (DsDQueue != null)
                                                    {
                                                        if (DsDQueue.Tables.Count > 0)
                                                        {
                                                            if (Upd_Type != "Full")
                                                            {
                                                                foreach (DataRow D_tbData in DsDQueue.Tables[0].Rows)
                                                                {
                                                                    if (D_tbData["FAMILY_ID"].ToString() == dr1["Family_ID"].ToString()
                                                                           && Convert.ToDateTime(D_tbData["FModified_Date"].ToString()).ToString() == dr1["Modified_Date"].ToString())
                                                                    {
                                                                        treeNode1.CheckedState = CheckState.Unchecked;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    if (treeNode1.CheckedState == CheckState.Checked)
                                                    {
                                                        DataSet ds2 = new DataSet();
                                                        ds2 = syncData.GetFamilyProducts(fromDate, toDate, _catalogID, treeNode1.Key.Replace("~", ""), "All");
                                                        if (ds != null && ds2.Tables.Count > 0 && ds2.Tables[0].Rows.Count > 0)
                                                        {
                                                            foreach (DataRow dr2 in ds2.Tables[0].Rows)
                                                            {
                                                                UltraTreeNode treeNode2 = new UltraTreeNode("#" + treeNode1.Key.Replace("`", "") + "+" + dr2["Product_ID"].ToString(), "[Cat#] " + dr2["String_Value"].ToString());
                                                                treeNode2.Tag = dr2["Product_ID"].ToString();
                                                                treeNode1.Nodes.Add(treeNode2);

                                                                DataSet ds_subProduct = syncData.GetFamilySubProducts(fromDate, toDate, _catalogID, treeNode1.Key.Replace("~", ""), Convert.ToInt32(dr2["Product_ID"].ToString()), "All");
                                                                foreach (DataRow dr_subProduct in ds_subProduct.Tables[0].Rows)
                                                                {
                                                                   // treeNode1 = new UltraTreeNode("#" + treeNode1.Key.Replace("`", "") + "+" + dr2["Product_ID"].ToString() + "^" + dr_subProduct["SUBPRODUCT_ID"].ToString(), "[Cat#] " + dr_subProduct["String_Value"].ToString());
                                                                    UltraTreeNode treeNodeSubProduct = new UltraTreeNode("#" + treeNode1.Key.Replace("`", "") + "+" + dr2["Product_ID"].ToString() + "^" + dr_subProduct["SUBPRODUCT_ID"].ToString(), "[Cat#] " + dr_subProduct["String_Value"].ToString());
                                                                    treeNodeSubProduct.Tag = dr_subProduct["SUBPRODUCT_ID"].ToString();
                                                                    treeNode2.Nodes.Add(treeNodeSubProduct);
                                                                }


                                                            }
                                                        }
                                                        DataSet ds3 = syncData.GetSubFamilyData(fromDate, toDate, _catalogID, treeNode1.Key.Replace("~", ""), treeNode.Key.Replace("`", ""), "All");
                                                        if (ds3 != null && ds3.Tables.Count > 0)
                                                        {
                                                            foreach (DataRow dr3 in ds3.Tables[0].Rows)
                                                            {
                                                                UltraTreeNode treeNode2 = new UltraTreeNode("~" + dr3["Family_ID"].ToString(), "[" + dr3["Family_ID"].ToString() + "] " + dr3["Family_Name"].ToString());
                                                                DataSet ds4 = new DataSet();
                                                                ds4 = syncData.GetFamilyProducts(fromDate, toDate, _catalogID, treeNode2.Key.Replace("~", ""), "All");
                                                                if (ds4 != null && ds4.Tables.Count > 0 && ds4.Tables[0].Rows.Count > 0)
                                                                {
                                                                    foreach (DataRow dr2 in ds4.Tables[0].Rows)
                                                                    {
                                                                        UltraTreeNode treeNode3 = new UltraTreeNode("#" + treeNode2.Key.Replace("`", "") + "+" + dr2["Product_ID"].ToString(), "[Cat#] " + dr2["String_Value"].ToString());
                                                                        treeNode3.Tag = dr2["Product_ID"].ToString();
                                                                        treeNode2.Nodes.Add(treeNode3);

                                                                        DataSet ds_subProduct = syncData.GetFamilySubProducts(fromDate, toDate, _catalogID, treeNode1.Key.Replace("~", ""), Convert.ToInt32(dr2["Product_ID"].ToString()), "All");
                                                                        foreach (DataRow dr_subProduct in ds_subProduct.Tables[0].Rows)
                                                                        {
                                                                            //treeNode1 = new UltraTreeNode("#" + treeNode1.Key.Replace("`", "") + "+" + dr2["Product_ID"].ToString() + "^" + dr_subProduct["SUBPRODUCT_ID"].ToString(), "[Cat#] " + dr_subProduct["String_Value"].ToString());
                                                                            UltraTreeNode treeNodeSubProduct = new UltraTreeNode("#" + treeNode1.Key.Replace("`", "") + "+" + dr2["Product_ID"].ToString() + "^" + dr_subProduct["SUBPRODUCT_ID"].ToString(), "[Cat#] " + dr_subProduct["String_Value"].ToString());
                                                                            treeNodeSubProduct.Tag = dr_subProduct["SUBPRODUCT_ID"].ToString();
                                                                            treeNode2.Nodes.Add(treeNodeSubProduct);
                                                                        }

                                                                    }
                                                                    treeNode1.Nodes.Add(treeNode2);
                                                                }
                                                                else
                                                                    treeNode1.Nodes.Add(treeNode2);
                                                            }
                                                        }
                                                        treeNode.Nodes.Add(treeNode1);
                                                    }
                                                }

                                            }
                                        }
                                        ModifiedItemsTreeView.Nodes.Add(treeNode);
                                    }
                                }


                                int k = -1;
                                for (int i = 0; i < ModifiedItemsTreeView.Nodes.Count; i++)
                                {
                                    //if (ModifiedItemsTreeView.Nodes[i].Key.Replace("`", "") == "BL014")
                                    //    MessageBox.Show("ADFDSAF");
                                    k = -1;
                                    if (ModifiedItemsTreeView.Nodes[i].CheckedState != CheckState.Unchecked)
                                    {
                                        categoryID = ModifiedItemsTreeView.Nodes[i].Key.Replace("`", "");
                                        root_categoryId = "0";
                                        familyID = "0";
                                        foreach (UltraTreeNode catChildNode in ModifiedItemsTreeView.Nodes[i].Nodes)
                                        {
                                            string prodItems = string.Empty;
                                            string subprodItems = string.Empty;
                                            if (catChildNode.Key.Contains("~"))
                                            {
                                                k++;
                                                if (catChildNode.Key.Contains("#") && catChildNode.Key.Contains("$"))
                                                {
                                                    root_categoryId = catChildNode.Key.Substring(catChildNode.Key.LastIndexOf('$'));
                                                    root_categoryId = root_categoryId.Substring(1, (root_categoryId.LastIndexOf('#') - 1));
                                                    familyID = catChildNode.Key;
                                                    familyID = familyID.Substring(familyID.LastIndexOf('#') + 1);
                                                }
                                                else
                                                {
                                                    familyID = catChildNode.Key.Replace("~", "");
                                                }
                                                foreach (UltraTreeNode familyChildNode in catChildNode.Nodes)
                                                {
                                                    if (familyChildNode.Key.ToString().Contains("+"))
                                                    {
                                                        string produtpath = familyChildNode.Key.ToString();
                                                        if (produtpath.Length > 0)
                                                        {
                                                            produtpath = produtpath.Substring(produtpath.LastIndexOf("+") + 1);
                                                            if (prodItems == string.Empty)
                                                                prodItems = produtpath.ToString();
                                                            else
                                                                prodItems = prodItems + "," + produtpath.ToString();
                                                        }
                                                    }
                                                    foreach(UltraTreeNode productChileNode in familyChildNode.Nodes)
                                                    {
                                                        if (productChileNode.Key.ToString().Contains("^"))
                                                        {
                                                            string subProdutpath = productChileNode.Key.ToString();
                                                            if (subProdutpath.Length > 0)
                                                            {
                                                                subProdutpath = subProdutpath.Substring(subProdutpath.LastIndexOf("^") + 1);
                                                                if (subprodItems == string.Empty)
                                                                    subprodItems = subProdutpath.ToString();
                                                                else
                                                                    subprodItems = subprodItems + "," + subProdutpath.ToString();
                                                            }
                                                        } 
                                                    }
                                                }
                                                Console.WriteLine("Sending family items of catalog (" + drow[0].ToString() + ")....");
                                                productID = prodItems;
                                                subProductID = subprodItems;
                                                _SQLConn.Open();
                                                CatalogDS = new DataSet();
                                                if (categoryID.ToLower() == root_categoryId.ToLower())
                                                {
                                                    root_categoryId = "0";
                                                }
                                                //if (familyID == "299")
                                                //    MessageBox.Show(familyID);
                                                Command = new SqlCommand("STP_CATALOGSTUDIO5_FLATXMLTREE", _SQLConn);
                                                Command.CommandType = CommandType.StoredProcedure;
                                                Command.Parameters.Add(new SqlParameter("@CATALOG_ID", _catalogID));
                                                Command.Parameters.Add(new SqlParameter("@CATEGORYID", categoryID));
                                                Command.Parameters.Add(new SqlParameter("@ROOT_CATEGORYID", root_categoryId));
                                                Command.Parameters.Add(new SqlParameter("@FAMILY_ID", familyID));
                                                Command.Parameters.Add(new SqlParameter("@ALLOW_DUPLICATES", "N"));
                                                Command.Parameters.Add(new SqlParameter("@PRODUCTIDS", productID));
                                                Command.Parameters.Add(new SqlParameter("@SUBPRODUCTIDS", subProductID));
                                                Command.Parameters.Add(new SqlParameter("@FROMDATE", String.Format("{0:yyyyMMdd}", fromDate)));
                                                Command.Parameters.Add(new SqlParameter("@TODATE", String.Format("{0:yyyyMMdd}", toDate)));
                                                DataAdapter = new SqlDataAdapter(Command);
                                                DataAdapter.SelectCommand.CommandTimeout = 0;
                                                CatalogDS = new DataSet();
                                                DataAdapter.Fill(CatalogDS);
                                                complete_familyid = complete_familyid + ',' + familyID;
                                                status = true;
                                                xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" + CatalogDS.Tables[0].Rows[0][0].ToString();
                                                string column_category = "";
                                                if (category_changes)
                                                {
                                                    Command = new SqlCommand();
                                                    Command.CommandText = "SELECT ISNULL(CATEGORY_COLUMNS + ',','') FROM TB_CATEGORY_CHANGE_LIST WHERE CATEGORY_ID = '" + categoryID + "' AND STATUS=0";
                                                    Command.Connection = _SQLConn;
                                                    column_category = Convert.ToString(Command.ExecuteScalar());
                                                    XmlDocument document = new XmlDocument();
                                                    document.LoadXml(xml);

                                                    if ((column_category.Length > 1) && !(column_category.Contains("Copied")))
                                                    {
                                                        foreach (XmlNode child in document.DocumentElement.ChildNodes)
                                                        {
                                                            if (child.NodeType == XmlNodeType.Element)
                                                            {
                                                                if (child.Name.ToString().ToLower().Contains("category"))
                                                                {
                                                                    for (int l = 1; l < child.ChildNodes.Count; l++)
                                                                    {
                                                                        if ((!child.ChildNodes[l].Name.ToUpper().Contains("MODIFIED_DATE")) && (!child.ChildNodes[l].Name.ToUpper().Contains("SORT_ORDER")))
                                                                        {
                                                                            if (!column_category.ToString().ToLower().Contains("," + child.ChildNodes[l].Name.ToLower() + ","))
                                                                            {
                                                                                child.RemoveChild(child.ChildNodes[l]);
                                                                                l--;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        xml = document.InnerXml.ToString();
                                                        Command = new SqlCommand();
                                                        Command.CommandText = "UPDATE TB_CATEGORY_CHANGE_LIST SET STATUS=1,CATEGORY_COLUMNS ='' WHERE CATEGORY_ID = '" + categoryID + "'";
                                                        Command.Connection = _SQLConn;
                                                        Command.ExecuteNonQuery();
                                                    }
                                                    else if (column_category.Contains("Copied"))
                                                    {
                                                        Command = new SqlCommand();
                                                        Command.CommandText = "UPDATE TB_CATEGORY_CHANGE_LIST SET STATUS=1,CATEGORY_COLUMNS ='' WHERE CATEGORY_ID = '" + categoryID + "'";
                                                        Command.Connection = _SQLConn;
                                                        Command.ExecuteNonQuery();
                                                    }
                                                    //else
                                                    //{
                                                    //    DataSet levelDs = new DataSet();
                                                    //    int level = -1;
                                                    //    Command = new SqlCommand("STP_CATALOGSTUDIO5_CategoryLevel", _SQLConn);
                                                    //    Command.CommandType = CommandType.StoredProcedure;
                                                    //    Command.Parameters.Add(new SqlParameter("@CATEGORY_ID", categoryID));
                                                    //    SqlDataAdapter sqlda = new SqlDataAdapter(Command);
                                                    //    sqlda.Fill(levelDs);
                                                    //    foreach (DataRow dr in levelDs.Tables[0].Rows)
                                                    //    {
                                                    //        level++;
                                                    //    }                                                      
                                                    //    column_category = "CATEGORY_ID,CATEGORY_NAME,CATEGORY_SORT_ORDER";
                                                    //    if (level > 0)
                                                    //    {
                                                    //        int l = 1;
                                                    //        while (l <= level)
                                                    //        {
                                                    //            column_category = column_category + ",SUBCAT_L" + l + ",SUBCATNAME_L" + l + ",SUBCAT_L" + level + "_MODIFIED_DATE,SUBCAT_L" + l + "_PUBLISH,SUBCAT_L" + l + "_FLAG_RECYCLE,CUSTOMER_ID,SUBCAT_L" + l + "_CATEGORY_SHORT,SUBCAT_L" + l + "_CATEGORY_PARENT_SHORT,SUBCAT_L" + l + "_SHORT_DESC,SUBCAT_L" + l + "_IMAGE_FILE,SUBCAT_L" + l + "_IMAGE_FILE2,SUBCAT_L" + l + "_IMAGE_TYPE,SUBCAT_L" + l + "_IMAGE_TYPE2,SUBCAT_L" + l + "_IMAGE_NAME,SUBCAT_L" + l + "_IMAGE_NAME2,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD1,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD2,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD3,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD1,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD2,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD3";
                                                    //            l++;
                                                    //        }
                                                    //    }
                                                    //    else
                                                    //    {
                                                    //        column_category = column_category + ",MODIFIED_DATE,PUBLISH";
                                                    //    }
                                                    //    column_category = column_category + ",FLAG_RECYCLE,CUSTOMER_ID,CATEGORY_SHORT,CATEGORY_PARENT_SHORT,SHORT_DESC,IMAGE_FILE,IMAGE_FILE2,IMAGE_TYPE,IMAGE_TYPE2,IMAGE_NAME,IMAGE_NAME2,CUSTOM_NUM_FIELD1,CUSTOM_NUM_FIELD2,CUSTOM_NUM_FIELD3,CUSTOM_TEXT_FIELD1,CUSTOM_TEXT_FIELD2,CUSTOM_TEXT_FIELD3";
                                                    //    foreach (XmlNode child in document.DocumentElement.ChildNodes)
                                                    //    {
                                                    //        if (child.NodeType == XmlNodeType.Element)
                                                    //        {
                                                    //            if (child.Name.ToString().ToLower().Contains("category"))
                                                    //            {
                                                    //                for (int l = 1; l < child.ChildNodes.Count; l++)
                                                    //                {
                                                    //                    if ((!child.ChildNodes[l].Name.ToUpper().Contains("MODIFIED_DATE")) && (!child.ChildNodes[l].Name.ToUpper().Contains("SORT_ORDER")))
                                                    //                    {
                                                    //                        if (!column_category.ToString().ToLower().Contains("," + child.ChildNodes[l].Name.ToLower()))
                                                    //                        {
                                                    //                            child.RemoveChild(child.ChildNodes[l]);
                                                    //                            l--;
                                                    //                        }
                                                    //                    }
                                                    //                }
                                                    //            }
                                                    //        }
                                                    //    }
                                                    //    xml = document.InnerXml.ToString();
                                                    //}
                                                    xml = document.InnerXml.ToString();
                                                }

                                                string Filename2 = Application.StartupPath + "\\XML\\myXmlF[" + (familyID).ToString() + "]" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xml";
                                                File.WriteAllText(Filename2, xml);
                                                Console.WriteLine(Filename2);
                                                //syncData.Url = ConfigurationManager.AppSettings["Url"].ToString();
                                                receiptStatus = syncData.SyncDatas(sessionID, xml, userId, Upd_Type, "Auto", categoryID.ToUpper());
                                                syncData.Timeout = Timeout.Infinite;
                                                _SQLConn.Close();
                                                jobIDs = "," + receiptStatus;
                                                xml = string.Empty;
                                                if (ModifiedItemsTreeView.Nodes[i].HasNodes)
                                                {
                                                    foreach (UltraTreeNode catChildNode1 in ModifiedItemsTreeView.Nodes[i].Nodes[k].Nodes)
                                                    {
                                                        if (catChildNode1.Key.Contains("~"))
                                                        {
                                                            if (catChildNode1.Key.Contains("#") && catChildNode1.Key.Contains("$"))
                                                            {
                                                                root_categoryId = catChildNode1.Key.Substring(catChildNode1.Key.LastIndexOf('$'));
                                                                root_categoryId = root_categoryId.Substring(1, (root_categoryId.LastIndexOf('#') - 1));
                                                                subfamilyId = catChildNode1.Key;
                                                                subfamilyId = subfamilyId.Substring(subfamilyId.LastIndexOf('#') + 1);
                                                                prodItems = "";
                                                                subprodItems="";
                                                                foreach (UltraTreeNode subFamilyChildNode in catChildNode1.Nodes)
                                                                {
                                                                    if (subFamilyChildNode.Key.ToString().Contains("+"))
                                                                    {
                                                                        string produtpath = subFamilyChildNode.Key.ToString();
                                                                        if (produtpath.Length > 0)
                                                                        {
                                                                            produtpath = produtpath.Substring(produtpath.LastIndexOf("+") + 1);
                                                                            if (prodItems == string.Empty)
                                                                                prodItems = produtpath.ToString();
                                                                            else
                                                                                prodItems = prodItems + "," + produtpath.ToString();
                                                                        }
                                                                        productID = prodItems;
                                                                    }
                                                                    foreach (UltraTreeNode subProduct in subFamilyChildNode.Nodes)
                                                                    {
                                                                        if (subProduct.Key.ToString().Contains("^"))
                                                                        {
                                                                            string subprodutpath = subProduct.Key.ToString();
                                                                            if (subprodutpath.Length > 0)
                                                                            {
                                                                                subprodutpath = subprodutpath.Substring(subprodutpath.LastIndexOf("^") + 1);
                                                                                if (subprodItems == string.Empty)
                                                                                    subprodItems = subprodutpath.ToString();
                                                                                else
                                                                                    subprodItems = subprodItems + "," + subprodutpath.ToString();
                                                                            }
                                                                            subProductID = subprodItems;
                                                                        }
                                                                    }
                                                                    
                                                                }
                                                                productID = prodItems;
                                                                subProductID = subprodItems;
                                                               
                                                            }
                                                            else
                                                            {
                                                                if (catChildNode1.Key.ToString().Remove(0, 1) != familyID)
                                                                {
                                                                    subfamilyId = catChildNode1.Key.Replace("~", "");
                                                                    prodItems = "";
                                                                    foreach (UltraTreeNode subFamilyChildNode1 in catChildNode1.Nodes)
                                                                    {
                                                                        if (subFamilyChildNode1.Key.ToString().Contains("+"))
                                                                        {
                                                                            string produtpath = subFamilyChildNode1.Key.ToString();
                                                                            if (produtpath.Length > 0)
                                                                            {
                                                                                produtpath = produtpath.Substring(produtpath.LastIndexOf("+") + 1);
                                                                                if (prodItems == string.Empty)
                                                                                    prodItems = produtpath.ToString();
                                                                                else
                                                                                    prodItems = prodItems + "," + produtpath.ToString();
                                                                            }
                                                                            productID = prodItems;
                                                                        }
                                                                        foreach (UltraTreeNode SubProduct in subFamilyChildNode1.Nodes)
                                                                        {
                                                                            if (SubProduct.Key.ToString().Contains("^"))
                                                                            {
                                                                                string subprodutpath = SubProduct.Key.ToString();
                                                                                if (subprodutpath.Length > 0)
                                                                                {
                                                                                    subprodutpath = subprodutpath.Substring(subprodutpath.LastIndexOf("^") + 1);
                                                                                    if (subprodItems == string.Empty)
                                                                                        subprodItems = subprodutpath.ToString();
                                                                                    else
                                                                                        subprodItems = subprodItems + "," + subprodutpath.ToString();
                                                                                }
                                                                                subProductID = subprodItems;
                                                                            }
                                                                        }
                                                                    }
                                                                   
                                                                }
                                                            }
                                                        }
                                                        if (subfamilyId != "" && familyID != subfamilyId && !subfamilyId.Contains('+'))
                                                        {
                                                            Console.WriteLine("Sending subfamily items of catalog (" + drow[0].ToString() + ")....");
                                                            _SQLConn.Open();
                                                            if (categoryID.ToLower() == root_categoryId.ToLower())
                                                            {
                                                                root_categoryId = "0";
                                                            }
                                                            Command = new SqlCommand("STP_CATALOGSTUDIO5_FLATXMLTREE", _SQLConn);
                                                            Command.CommandType = CommandType.StoredProcedure;
                                                            Command.Parameters.Add(new SqlParameter("@CATALOG_ID", _catalogID));
                                                            Command.Parameters.Add(new SqlParameter("@CATEGORYID", categoryID));
                                                            Command.Parameters.Add(new SqlParameter("@ROOT_CATEGORYID", root_categoryId));
                                                            Command.Parameters.Add(new SqlParameter("@FAMILY_ID", subfamilyId));
                                                            Command.Parameters.Add(new SqlParameter("@ALLOW_DUPLICATES", "N"));
                                                            Command.Parameters.Add(new SqlParameter("@PRODUCTIDS", productID));
                                                            Command.Parameters.Add(new SqlParameter("@SUBPRODUCTIDS", subProductID));
                                                            Command.Parameters.Add(new SqlParameter("@FROMDATE", String.Format("{0:yyyyMMdd}", fromDate)));
                                                            Command.Parameters.Add(new SqlParameter("@TODATE", String.Format("{0:yyyyMMdd}", toDate)));
                                                            DataAdapter = new SqlDataAdapter(Command);
                                                            DataAdapter.SelectCommand.CommandTimeout = 0;
                                                            CatalogDS = new DataSet();
                                                            DataAdapter.Fill(CatalogDS);
                                                            complete_familyid = complete_familyid + ',' + subfamilyId;
                                                            status = true;
                                                            string Filename1 = Application.StartupPath + "\\XML\\myXmlS[" + (subfamilyId).ToString() + "]" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xml";
                                                            xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" + CatalogDS.Tables[0].Rows[0][0].ToString();
                                                            if (category_changes)
                                                            {
                                                                XmlDocument document = new XmlDocument();
                                                                document.LoadXml(xml);
                                                                Command = new SqlCommand();
                                                                Command.CommandText = "SELECT ISNULL(CATEGORY_COLUMNS + ',','') FROM TB_CATEGORY_CHANGE_LIST WHERE CATEGORY_ID = '" + categoryID + "'AND STATUS=0";
                                                                Command.Connection = _SQLConn;
                                                                column_category = Convert.ToString(Command.ExecuteScalar());
                                                                if (column_category.Length > 0)
                                                                {
                                                                    foreach (XmlNode child in document.DocumentElement.ChildNodes)
                                                                    {
                                                                        if (child.NodeType == XmlNodeType.Element)
                                                                        {
                                                                            if (child.Name.ToString().ToLower().Contains("category"))
                                                                            {
                                                                                for (int l = 1; l < child.ChildNodes.Count; l++)
                                                                                {
                                                                                    if ((!child.ChildNodes[l].Name.ToUpper().Contains("MODIFIED_DATE")) && (!child.ChildNodes[l].Name.ToUpper().Contains("SORT_ORDER")))
                                                                                    {
                                                                                        if (!column_category.ToString().ToLower().Contains("," + child.ChildNodes[l].Name.ToLower() + ","))
                                                                                        {
                                                                                            child.RemoveChild(child.ChildNodes[l]);
                                                                                            l--;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    xml = document.InnerXml.ToString();
                                                                    Command = new SqlCommand();
                                                                    Command.CommandText = "UPDATE TB_CATEGORY_CHANGE_LIST SET STATUS=1,CATEGORY_COLUMNS ='' WHERE CATEGORY_ID = '" + categoryID + "'";
                                                                    Command.Connection = _SQLConn;
                                                                    Command.ExecuteNonQuery();
                                                                }
                                                                //else
                                                                //{
                                                                //    DataSet levelDs = new DataSet();
                                                                //    int level = -1;
                                                                //    Command = new SqlCommand("STP_CATALOGSTUDIO5_CategoryLevel", _SQLConn);
                                                                //    Command.CommandType = CommandType.StoredProcedure;
                                                                //    Command.Parameters.Add(new SqlParameter("@CATEGORY_ID", categoryID));
                                                                //    SqlDataAdapter sqlda = new SqlDataAdapter(Command);
                                                                //    sqlda.Fill(levelDs);
                                                                //    foreach (DataRow dr in levelDs.Tables[0].Rows)
                                                                //    {
                                                                //        level++;
                                                                //    }
                                                                //    column_category = "CATEGORY_ID,CATEGORY_NAME,CATEGORY_SORT_ORDER";
                                                                //    if (level > 0)
                                                                //    {
                                                                //        int l = 1;
                                                                //        while (l <= level)
                                                                //        {
                                                                //            column_category = column_category + ",SUBCAT_L" + l + ",SUBCATNAME_L" + l + ",SUBCAT_L" + l + "_SORT_ORDER,SUBCAT_L" + l + "_FLAG_RECYCLE,CUSTOMER_ID,SUBCAT_L" + l + "_CATEGORY_SHORT,SUBCAT_L" + l + "_CATEGORY_PARENT_SHORT,SUBCAT_L" + l + "_SHORT_DESC,SUBCAT_L" + l + "_IMAGE_FILE,SUBCAT_L" + l + "_IMAGE_FILE2,SUBCAT_L" + l + "_IMAGE_TYPE,SUBCAT_L" + l + "_IMAGE_TYPE2,SUBCAT_L" + l + "_IMAGE_NAME,SUBCAT_L" + l + "_IMAGE_NAME2,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD1,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD2,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD3,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD1,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD2,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD3";
                                                                //            l++;
                                                                //        }
                                                                //        column_category = column_category + ",SUBCAT_L" + level + "_MODIFIED_DATE,SUBCAT_L" + level + "_PUBLISH";
                                                                //    }
                                                                //    else
                                                                //    {
                                                                //        column_category = column_category + ",MODIFIED_DATE,PUBLISH";
                                                                //    }
                                                                //    column_category = column_category + ",FLAG_RECYCLE,CUSTOMER_ID,CATEGORY_SHORT,CATEGORY_PARENT_SHORT,SHORT_DESC,IMAGE_FILE,IMAGE_FILE2,IMAGE_TYPE,IMAGE_TYPE2,IMAGE_NAME,IMAGE_NAME2,CUSTOM_NUM_FIELD1,CUSTOM_NUM_FIELD2,CUSTOM_NUM_FIELD3,CUSTOM_TEXT_FIELD1,CUSTOM_TEXT_FIELD2,CUSTOM_TEXT_FIELD3";
                                                                //    foreach (XmlNode child in document.DocumentElement.ChildNodes)
                                                                //    {
                                                                //        if (child.NodeType == XmlNodeType.Element)
                                                                //        {
                                                                //            if (child.Name.ToString().ToLower().Contains("category"))
                                                                //            {
                                                                //                for (int l = 1; l < child.ChildNodes.Count; l++)
                                                                //                {
                                                                //                    if ((!child.ChildNodes[l].Name.ToUpper().Contains("MODIFIED_DATE")) && (!child.ChildNodes[l].Name.ToUpper().Contains("SORT_ORDER")))
                                                                //                    {
                                                                //                        if (!column_category.ToString().ToLower().Contains("," + child.ChildNodes[l].Name.ToLower()))
                                                                //                        {
                                                                //                            child.RemoveChild(child.ChildNodes[l]);
                                                                //                            l--;
                                                                //                        }
                                                                //                    }
                                                                //                }
                                                                //            }
                                                                //        }
                                                                //    }
                                                                   
                                                                //}
                                                                xml = document.InnerXml.ToString();
                                                            }
                                                            File.WriteAllText(Filename1, xml);
                                                            receiptStatus = syncData.SyncDatas(sessionID, xml, userId, Upd_Type, "Auto", categoryID.ToUpper());
                                                            syncData.Timeout = Timeout.Infinite;
                                                            _SQLConn.Close();
                                                            jobIDs = "," + receiptStatus;
                                                            xml = string.Empty;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ((categoryID != "") && (familyID == "0"))
                                    {
                                        //if (xml == string.Empty)
                                        {
                                            Console.WriteLine("Sending category items of catalog (" + drow[0].ToString() + ")....");
                                            completed_Category = completed_Category + "," + categoryID;
                                            familyID = "0";
                                            productID = "0";
                                            root_categoryId = "0";
                                            subProductID = "0";
                                            CatalogDS = new DataSet();
                                            _SQLConn.Open();
                                            Command = new SqlCommand("STP_CATALOGSTUDIO5_FLATXMLTREE", _SQLConn);
                                            Command.CommandType = CommandType.StoredProcedure;
                                            Command.Parameters.Add(new SqlParameter("@CATALOG_ID", _catalogID));
                                            Command.Parameters.Add(new SqlParameter("@CATEGORYID", categoryID));
                                            Command.Parameters.Add(new SqlParameter("@ROOT_CATEGORYID", root_categoryId));
                                            Command.Parameters.Add(new SqlParameter("@FAMILY_ID", familyID));
                                            Command.Parameters.Add(new SqlParameter("@ALLOW_DUPLICATES", "Y"));
                                            Command.Parameters.Add(new SqlParameter("@PRODUCTIDS", productID));
                                            Command.Parameters.Add(new SqlParameter("@SUBPRODUCTIDS", subProductID));
                                            Command.Parameters.Add(new SqlParameter("@FROMDATE", String.Format("{0:yyyyMMdd}", fromDate)));
                                            Command.Parameters.Add(new SqlParameter("@TODATE", String.Format("{0:yyyyMMdd}", toDate)));
                                            DataAdapter = new SqlDataAdapter(Command);
                                            DataAdapter.SelectCommand.CommandTimeout = 0;
                                            DataAdapter.Fill(CatalogDS);
                                            status = true;
                                            xml = "<?xml version=\"1.0\" standalone=\"yes\"?>";
                                            for (int j = 0; j < CatalogDS.Tables[0].Rows.Count; j++)
                                            {
                                                string column_category = "";
                                                string Filename1 = Application.StartupPath + "\\XML\\myXmlC[" + (categoryID).ToString() + "]" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xml";
                                                Console.WriteLine(Filename1);
                                                xml = xml + CatalogDS.Tables[0].Rows[j][0].ToString();
                                                if (category_changes)
                                                {
                                                    XmlDocument document = new XmlDocument();
                                                    document.LoadXml(xml);
                                                    Command = new SqlCommand();
                                                    Command.CommandText = "SELECT ISNULL(CATEGORY_COLUMNS + ',','') FROM TB_CATEGORY_CHANGE_LIST WHERE CATEGORY_ID = '" + categoryID + "'AND STATUS=0";
                                                    Command.Connection = _SQLConn;
                                                    column_category = Convert.ToString(Command.ExecuteScalar());
                                                    if ((column_category.Length > 1) && !(column_category.Contains("Copied")))
                                                    {
                                                        foreach (XmlNode child in document.DocumentElement.ChildNodes)
                                                        {
                                                            if (child.NodeType == XmlNodeType.Element)
                                                            {
                                                                if (child.Name.ToString().ToLower().Contains("category"))
                                                                {
                                                                    for (int l = 1; l < child.ChildNodes.Count; l++)
                                                                    {
                                                                        if ((!child.ChildNodes[l].Name.ToUpper().Contains("MODIFIED_DATE")) && (!child.ChildNodes[l].Name.ToUpper().Contains("SORT_ORDER")))
                                                                        {
                                                                            if (!column_category.ToString().ToLower().Contains("," + child.ChildNodes[l].Name.ToLower() + ","))
                                                                            {
                                                                                child.RemoveChild(child.ChildNodes[l]);
                                                                                l--;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        xml = document.InnerXml.ToString();
                                                        Command = new SqlCommand();
                                                        Command.CommandText = "UPDATE TB_CATEGORY_CHANGE_LIST SET STATUS=1,CATEGORY_COLUMNS ='' WHERE CATEGORY_ID = '" + categoryID + "'";
                                                        Command.Connection = _SQLConn;
                                                        Command.ExecuteNonQuery();
                                                    }
                                                    else if (column_category.Contains("Copied"))
                                                    {
                                                        xml = document.InnerXml.ToString();
                                                        Command = new SqlCommand();
                                                        Command.CommandText = "UPDATE TB_CATEGORY_CHANGE_LIST SET STATUS=1,CATEGORY_COLUMNS ='' WHERE CATEGORY_ID = '" + categoryID + "'";
                                                        Command.Connection = _SQLConn;
                                                        Command.ExecuteNonQuery();
                                                    }
                                                    //else
                                                    //{
                                                    //    DataSet levelDs = new DataSet();
                                                    //    int level = -1;
                                                    //    Command = new SqlCommand("STP_CATALOGSTUDIO5_CategoryLevel", _SQLConn);
                                                    //    Command.CommandType = CommandType.StoredProcedure;
                                                    //    Command.Parameters.Add(new SqlParameter("@CATEGORY_ID", categoryID));
                                                    //    SqlDataAdapter sqlda = new SqlDataAdapter(Command);
                                                    //    sqlda.Fill(levelDs);
                                                    //    foreach (DataRow dr in levelDs.Tables[0].Rows)
                                                    //    {
                                                    //        level++;
                                                    //    }
                                                    //    column_category = "CATEGORY_ID,CATEGORY_NAME,CATEGORY_SORT_ORDER";
                                                    //    if (level > 0)
                                                    //    {
                                                    //        int l = 1;
                                                    //        while (l <= level)
                                                    //        {
                                                    //            column_category = column_category + ",SUBCAT_L" + l + ",SUBCATNAME_L" + l + ",SUBCAT_L" + l + "_SORT_ORDER,SUBCAT_L" + l + "_FLAG_RECYCLE,CUSTOMER_ID,SUBCAT_L" + l + "_CATEGORY_SHORT,SUBCAT_L" + l + "_CATEGORY_PARENT_SHORT,SUBCAT_L" + l + "_SHORT_DESC,SUBCAT_L" + l + "_IMAGE_FILE,SUBCAT_L" + l + "_IMAGE_FILE2,SUBCAT_L" + l + "_IMAGE_TYPE,SUBCAT_L" + l + "_IMAGE_TYPE2,SUBCAT_L" + l + "_IMAGE_NAME,SUBCAT_L" + l + "_IMAGE_NAME2,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD1,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD2,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD3,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD1,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD2,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD3";
                                                    //            l++;
                                                    //        }
                                                    //        column_category = column_category + ",SUBCAT_L" + level + "_MODIFIED_DATE,SUBCAT_L" + level + "_PUBLISH";
                                                    //    }
                                                    //    else
                                                    //    {
                                                    //        column_category = column_category + ",MODIFIED_DATE,PUBLISH";
                                                    //    }
                                                    //    column_category = column_category + ",FLAG_RECYCLE,CUSTOMER_ID,CATEGORY_SHORT,CATEGORY_PARENT_SHORT,SHORT_DESC,IMAGE_FILE,IMAGE_FILE2,IMAGE_TYPE,IMAGE_TYPE2,IMAGE_NAME,IMAGE_NAME2,CUSTOM_NUM_FIELD1,CUSTOM_NUM_FIELD2,CUSTOM_NUM_FIELD3,CUSTOM_TEXT_FIELD1,CUSTOM_TEXT_FIELD2,CUSTOM_TEXT_FIELD3";
                                                    //    foreach (XmlNode child in document.DocumentElement.ChildNodes)
                                                    //    {
                                                    //        if (child.NodeType == XmlNodeType.Element)
                                                    //        {
                                                    //            if (child.Name.ToString().ToLower().Contains("category"))
                                                    //            {
                                                    //                for (int l = 1; l < child.ChildNodes.Count; l++)
                                                    //                {
                                                    //                    if ((!child.ChildNodes[l].Name.ToUpper().Contains("MODIFIED_DATE")) && (!child.ChildNodes[l].Name.ToUpper().Contains("SORT_ORDER")))
                                                    //                    {
                                                    //                        if (!column_category.ToString().ToLower().Contains("," + child.ChildNodes[l].Name.ToLower()))
                                                    //                        {
                                                    //                            child.RemoveChild(child.ChildNodes[l]);
                                                    //                            l--;
                                                    //                        }
                                                    //                    }
                                                    //                }
                                                    //            }
                                                    //        }
                                                    //    }
                                                       
                                                    //}
                                                    xml = document.InnerXml.ToString();
                                                }
                                                File.WriteAllText(Filename1, xml);
                                            }
                                            receiptStatus = syncData.SyncDatas(sessionID, xml, userId, Upd_Type, "Auto", categoryID.ToUpper());
                                            syncData.Timeout = Timeout.Infinite;
                                            _SQLConn.Close();
                                            jobIDs = "," + receiptStatus;
                                            xml = string.Empty;
                                        }
                                    }

                                }
                            }
                        
                        }
                        catch (Exception ex)
                        {
                            final = false;
                            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/Log"))
                                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/Log");
                            string FirstName = "/Log/log" + DateTime.Now.ToShortDateString().Replace("/", "").Trim() + ".txt";
                            string Path = AppDomain.CurrentDomain.BaseDirectory + FirstName;
                            Path = Path.Replace("\\", "/");
                            if (File.Exists(Path) == false)
                            {
                                FileStream objFileStream = new FileStream(Path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                                objFileStream.Close();
                            }
                            StreamWriter objStreamWriter;
                            objStreamWriter = new StreamWriter(Path, true);
                            objStreamWriter.WriteLine("Date        : " + DateTime.Now.ToLongTimeString());
                            objStreamWriter.WriteLine("Time        : " + DateTime.Now.ToShortDateString());
                            objStreamWriter.WriteLine("Computer    : " + Dns.GetHostName().ToString());
                            objStreamWriter.WriteLine("SessionId   : " + sessionID);
                            objStreamWriter.WriteLine("CategoryId  : " + categoryID);
                            objStreamWriter.WriteLine("FamilyId    : " + familyID);
                            objStreamWriter.WriteLine("Status      : Task incompleted");
                            objStreamWriter.WriteLine("Error       : " + ex.Message.ToString().Trim());
                            objStreamWriter.WriteLine("^^-------------------------------------------------------------------^^");
                            objStreamWriter.Flush();
                            objStreamWriter.Close();
                            CatalogDS = null;
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                catch (Exception e)
                {
                    final = false;
                    if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/Log"))
                        Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/Log");
                    string FirstName = "/Log/log" + DateTime.Now.ToShortDateString().Replace("/", "").Trim() + ".txt";
                    string Path = AppDomain.CurrentDomain.BaseDirectory + FirstName;
                    Path = Path.Replace("\\", "/");
                    if (File.Exists(Path) == false)
                    {
                        FileStream objFileStream = new FileStream(Path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                        objFileStream.Close();
                    }
                    StreamWriter objStreamWriter;
                    objStreamWriter = new StreamWriter(Path, true);
                    objStreamWriter.WriteLine("Date        : " + DateTime.Now.ToLongTimeString());
                    objStreamWriter.WriteLine("Time        : " + DateTime.Now.ToShortDateString());
                    objStreamWriter.WriteLine("Computer    : " + Dns.GetHostName().ToString());
                    objStreamWriter.WriteLine("SessionId   : " + sessionID);
                    objStreamWriter.WriteLine("CategoryId  : " + categoryID);
                    objStreamWriter.WriteLine("FamilyId    : " + familyID);
                    objStreamWriter.WriteLine("Status      : Task incompleted");
                    objStreamWriter.WriteLine("Error       : " + e.Message.ToString().Trim());
                    objStreamWriter.WriteLine("^^-------------------------------------------------------------------^^");
                    objStreamWriter.Flush();
                    objStreamWriter.Close();
                }
                finally
                {
                    if (final)
                    {
                        Console.WriteLine("Updating in Destination DB......");
                        //objservice.Timeout = Timeout.Infinite;
                        SyncData syncData = new SyncData();

                        if (syncData.CallSyncLiveUpdate(sessionID, args[3].ToString()) == "Updated in destination DB")
                        {
                            Console.WriteLine("Updated Successfully.....");
                            if (status)
                            {
                                if (jobIDs.Length > 1)
                                {
                                    jobIDs = jobIDs.Remove(0, 1);
                                }
                                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/Log"))
                                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/Log");
                                string FirstName = "/Log/log" + DateTime.Now.ToShortDateString().Replace("/", "").Trim() + ".txt";
                                string Path = AppDomain.CurrentDomain.BaseDirectory + FirstName;
                                Path = Path.Replace("\\", "/");
                                if (File.Exists(Path) == false)
                                {
                                    FileStream objFileStream = new FileStream(Path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                                    objFileStream.Close();
                                }
                                StreamWriter objStreamWriter;
                                objStreamWriter = new StreamWriter(Path, true);
                                objStreamWriter.WriteLine("Date        : " + DateTime.Now.ToLongTimeString());
                                objStreamWriter.WriteLine("Time        : " + DateTime.Now.ToShortDateString());
                                objStreamWriter.WriteLine("Computer    : " + Dns.GetHostName().ToString());
                                objStreamWriter.WriteLine("SessionId   : " + sessionID);
                                objStreamWriter.WriteLine("JobID       : " + jobIDs);
                                objStreamWriter.WriteLine("Status      : Task Completed");
                                objStreamWriter.WriteLine("^^-------------------------------------------------------------------^^");
                                objStreamWriter.Flush();
                                objStreamWriter.Close();
                            }
                        }
                        else
                        {
                            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "/Log"))
                                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "/Log");
                            string FirstName = "/Log/log" + DateTime.Now.ToShortDateString().Replace("/", "").Trim() + ".txt";
                            string Path = AppDomain.CurrentDomain.BaseDirectory + FirstName;
                            Path = Path.Replace("\\", "/");
                            if (File.Exists(Path) == false)
                            {
                                FileStream objFileStream = new FileStream(Path, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                                objFileStream.Close();
                            }
                            StreamWriter objStreamWriter;
                            objStreamWriter = new StreamWriter(Path, true);
                            objStreamWriter.WriteLine("Date        : " + DateTime.Now.ToLongTimeString());
                            objStreamWriter.WriteLine("Time        : " + DateTime.Now.ToShortDateString());
                            objStreamWriter.WriteLine("Computer    : " + Dns.GetHostName().ToString());
                            objStreamWriter.WriteLine("SessionId   : " + sessionID);
                            objStreamWriter.WriteLine("Status      : Task incompleted");
                            objStreamWriter.WriteLine("Error       : Not updated in Destination DB");
                            objStreamWriter.WriteLine("^^-------------------------------------------------------------------^^");
                            objStreamWriter.Flush();
                            objStreamWriter.Close();
                        }
                    }
                }
            }
        }
    }
}
