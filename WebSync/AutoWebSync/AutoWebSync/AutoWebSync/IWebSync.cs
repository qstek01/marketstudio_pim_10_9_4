﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoWebSync
{
    interface IWebSync
    {
        string CreateNewSyncSession();
        string GetLastRun();
        string RemoveDelete2WS_SyncLog(string log, string status);
        string SyncDatas(string sessionID, string xml, int userID, string Upd_Type, string jobType, string job);
        DataSet Get2ws_syncUpdatedData(int _catalogID, string[] categoryies, string flag);
        string CallSyncLiveUpdate(string xml,string URL);

    }
}
