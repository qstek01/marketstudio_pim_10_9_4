﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Windows.Forms;
using System.IO;
//using System.Xml;


namespace AutoWebSync
{
    class SyncData : IWebSync
    {
        string getUrl = string.Empty;
        string user="send";
        public string GetUrl
        {
            set
            {
                getUrl = value;
            }
        }
        int Datetime;
        public int Timeout
        {
            set
            {
                Datetime = value;
            }
        }
        string conStr = string.Empty;
        public string ConnectionString
        {
            set { conStr = value; }
        }
        public string CallSyncLiveUpdate(string xml,string URL)
        {
            try
            {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);
            var values = new Dictionary<string,string>();
            values.Add("xml", '"'+xml+'"');
           // string content = xml;
                StringContent content = new StringContent(JsonConvert.SerializeObject(xml), Encoding.UTF8, "application/json");
                HttpResponseMessage response = client.PostAsync("/AutoCallSyncLiveUpdate", content).Result;
               // HttpResponseMessage response = client.PostAsJsonAsync("api/Values", "Test").Result;
                if (response.IsSuccessStatusCode)
             {
               return "Updated in destination DB";
             }
             return null;
            }
            catch(Exception ex)
            {
                return null;
            }
           
           
        }

        public DataSet Get2ws_syncUpdatedData(int _catalogID, string[] categoryies, string flag)
        {
            try
            {
                var autoUpdateSyncData = new AutoWebSyncData();
                autoUpdateSyncData.catalogId = _catalogID;
                autoUpdateSyncData.categoryId = categoryies;
                autoUpdateSyncData.type = flag;

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(getUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("nl-NL"));
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.PostAsJsonAsync("/AutoSyncUpdateData", autoUpdateSyncData).Result;
                DataSet ds = new DataSet();
              
                if(response.Content != null)
                {
              
                    var responseContent = response.Content.ReadAsStringAsync();
                    var deserialized = JsonConvert.DeserializeObject(responseContent.Result);

                    string json = JsonConvert.SerializeObject(deserialized, Formatting.Indented);

                    var dataSet = JsonConvert.DeserializeObject<DataSet>(json);
                    DataTable table = dataSet.Tables[0];
                     
                    

                    return dataSet;


                   
                 
                }

                return null;
            }
            catch (Exception ex)
            {
                //_logger.Error("Error at InsertSession : CallSyncLiveUpdate", ex);
                return null;
            }
        }

        public string GetLastRun()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(getUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                string response = client.GetStringAsync("/GetLastRun").Result;

                return response.ToString();
            }
            catch (Exception ex)
            {
                //_logger.Error("Error at InsertSession : CallSyncLiveUpdate", ex);
                return null;
            }
        }

        public string RemoveDelete2WS_SyncLog(string log, string status)
        {
            throw new NotImplementedException();
        }

        public string CreateNewSyncSession()
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(getUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.GetStringAsync("/SessionId").Result;

                return response.ToString();
            }
            catch (Exception ex)
            {
                //_logger.Error("Error at InsertSession : CallSyncLiveUpdate", ex);
                return null;
            }
           
          
        }
        public string Attr_SyncDatas(string sessionID, string xml, string user, string Upd_Type, string jobType, string job)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(getUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var syncModel = new SyncDataModel();
                syncModel.sessionId = sessionID;
                 syncModel.xmldata = new System.Xml.XmlDocument();
                 syncModel.xmldata.LoadXml(xml);
               // syncModel.xmldata = xml;
                 syncModel.createdUser = user;
                syncModel.updateType = Upd_Type;
                syncModel.jobType = jobType;
                syncModel.jobName = job;
                //syncModel.CustomerId = CustomerId;
                var response = client.PostAsJsonAsync("/GetAttributeGroup", syncModel).Result;
                //GetAttributeGroup
                //SyncData
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }
                return null;
            }
            catch (Exception ex)
            {
                //_logger.Error("Error at InsertSession : SyncData", ex);
                return null;
            }
        }
       public string SyncDatas(string sessionID, string xml, int userID, string Upd_Type, string jobType, string job)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(getUrl);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var syncModel = new SyncDataModel();
                syncModel.sessionId = sessionID;
                syncModel.xmldata = new System.Xml.XmlDocument();
                syncModel.xmldata.LoadXml(xml);
                // syncModel.xmldata = xml;
                syncModel.createdUser = user;
                syncModel.updateType = Upd_Type;
                syncModel.jobType = jobType;
                syncModel.jobName = job;
                var response = client.PostAsJsonAsync("/SyncData", syncModel).Result;
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }
                return null;
            }
            catch (Exception ex)
            {
                //_logger.Error("Error at InsertSession : SyncData", ex);
                return null;
            }

        }

        public DataSet GetCategoryData(string fromDate, string toDate, int catalogID, int UserID)
        {
            DataSet dst = new DataSet();
            try
            {
                int userId = 0;
               // int.TryParse(UserID, out userId);
                //string sqlStr = "";
                //sqlStr = "SELECT distinct UPPER(TB_CATEGORY.CATEGORY_ID) AS Category_ID, UPPER(TB_CATEGORY.CATEGORY_NAME) AS Category_Name, TB_CATEGORY.PARENT_CATEGORY, TB_CATEGORY.SHORT_DESC, " +
                //                    " TB_CATEGORY.IMAGE_FILE, TB_CATEGORY.IMAGE_TYPE, TB_CATEGORY.IMAGE_NAME, TB_CATEGORY.IMAGE_NAME2, TB_CATEGORY.IMAGE_FILE2, " +
                //                    " TB_CATEGORY.IMAGE_TYPE2, TB_CATEGORY.CUSTOM_NUM_FIELD1, TB_CATEGORY.CUSTOM_NUM_FIELD2, TB_CATEGORY.CUSTOM_NUM_FIELD3, " +
                //                    " TB_CATEGORY.CUSTOM_TEXT_FIELD1, TB_CATEGORY.CUSTOM_TEXT_FIELD2, TB_CATEGORY.CUSTOM_TEXT_FIELD3, TB_CATEGORY.CREATED_USER, " +
                //                    " TB_CATEGORY.CREATED_DATE, TB_CATEGORY.MODIFIED_USER, TB_CATEGORY.MODIFIED_DATE,CATEGORY_SHORT,CATEGORY_PARENT_SHORT" + 
                //                    " FROM TB_CATEGORY INNER JOIN " +
                //                    " TB_CATALOG_SECTIONS ON TB_CATEGORY.CATEGORY_ID = TB_CATALOG_SECTIONS.CATEGORY_ID AND TB_CATALOG_SECTIONS.FLAG_RECYCLE='A' and TB_CATEGORY.FLAG_RECYCLE='A'" +
                //                    "join Customer_Catalog cc on cc.CATALOG_ID=TB_CATALOG_SECTIONS.CATALOG_ID and CUSTOMER_ID=" + UserID +
                //                    " WHERE  CONVERT(NVARCHAR(100), TB_CATEGORY.MODIFIED_DATE,120) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "' AND TB_CATALOG_SECTIONS.CATALOG_ID =" + catalogID + "  UNION " + 
                //                    "SELECT distinct UPPER(TB_CATEGORY.CATEGORY_ID) AS Category_ID, UPPER(TB_CATEGORY.CATEGORY_NAME) AS Category_Name, TB_CATEGORY.PARENT_CATEGORY, TB_CATEGORY.SHORT_DESC, " +
                //                    " TB_CATEGORY.IMAGE_FILE, TB_CATEGORY.IMAGE_TYPE, TB_CATEGORY.IMAGE_NAME, TB_CATEGORY.IMAGE_NAME2, TB_CATEGORY.IMAGE_FILE2, " +
                //                    " TB_CATEGORY.IMAGE_TYPE2, TB_CATEGORY.CUSTOM_NUM_FIELD1, TB_CATEGORY.CUSTOM_NUM_FIELD2, TB_CATEGORY.CUSTOM_NUM_FIELD3, " +
                //                    " TB_CATEGORY.CUSTOM_TEXT_FIELD1, TB_CATEGORY.CUSTOM_TEXT_FIELD2, TB_CATEGORY.CUSTOM_TEXT_FIELD3, TB_CATEGORY.CREATED_USER, " +
                //                    " TB_CATEGORY.CREATED_DATE, TB_CATEGORY.MODIFIED_USER, TB_CATEGORY.MODIFIED_DATE,CATEGORY_SHORT,CATEGORY_PARENT_SHORT" +
                //                    " FROM TB_CATEGORY INNER JOIN " +
                //                    " TB_CATALOG_SECTIONS ON TB_CATEGORY.CATEGORY_ID = TB_CATALOG_SECTIONS.CATEGORY_ID AND TB_CATALOG_SECTIONS.FLAG_RECYCLE='A' and TB_CATEGORY.FLAG_RECYCLE='A'" +
                //                    "join Customer_Catalog cc on cc.CATALOG_ID=TB_CATALOG_SECTIONS.CATALOG_ID and CUSTOMER_ID=" + UserID +
                //                    "join TB_CATALOG_FAMILY TCF on TCF.CATALOG_ID=TB_CATALOG_SECTIONS.CATALOG_ID join TB_FAMILY TF on TF.FAMILY_ID=TCF.FAMILY_ID and TF.CATEGORY_ID=TB_CATEGORY.CATEGORY_ID"+
                //                    " WHERE  CONVERT(NVARCHAR(100), TF.MODIFIED_DATE,120) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "' AND (TB_CATALOG_SECTIONS.CATALOG_ID = " + catalogID + ")  AND  TCF.CATALOG_ID=" + catalogID + " UNION " +
                //                    "SELECT distinct UPPER(TB_CATEGORY.CATEGORY_ID) AS Category_ID, UPPER(TB_CATEGORY.CATEGORY_NAME) AS Category_Name, TB_CATEGORY.PARENT_CATEGORY, TB_CATEGORY.SHORT_DESC, " +
                //                    " TB_CATEGORY.IMAGE_FILE, TB_CATEGORY.IMAGE_TYPE, TB_CATEGORY.IMAGE_NAME, TB_CATEGORY.IMAGE_NAME2, TB_CATEGORY.IMAGE_FILE2, " +
                //                    " TB_CATEGORY.IMAGE_TYPE2, TB_CATEGORY.CUSTOM_NUM_FIELD1, TB_CATEGORY.CUSTOM_NUM_FIELD2, TB_CATEGORY.CUSTOM_NUM_FIELD3, " +
                //                    " TB_CATEGORY.CUSTOM_TEXT_FIELD1, TB_CATEGORY.CUSTOM_TEXT_FIELD2, TB_CATEGORY.CUSTOM_TEXT_FIELD3, TB_CATEGORY.CREATED_USER, " +
                //                    " TB_CATEGORY.CREATED_DATE, TB_CATEGORY.MODIFIED_USER, TB_CATEGORY.MODIFIED_DATE,CATEGORY_SHORT,CATEGORY_PARENT_SHORT" +
                //                    " FROM TB_CATEGORY INNER JOIN " +
                //                    " TB_CATALOG_SECTIONS ON TB_CATEGORY.CATEGORY_ID = TB_CATALOG_SECTIONS.CATEGORY_ID AND TB_CATALOG_SECTIONS.FLAG_RECYCLE='A' and TB_CATEGORY.FLAG_RECYCLE='A'" +
                //                    "join Customer_Catalog cc on cc.CATALOG_ID=TB_CATALOG_SECTIONS.CATALOG_ID and CUSTOMER_ID=" + UserID +
                //                   "join TB_CATALOG_PRODUCT TCP on TCP.CATALOG_ID=TB_CATALOG_SECTIONS.CATALOG_ID "+
                //                   "join TB_PRODUCT TP on TP.PRODUCT_ID=TCP.PRODUCT_ID "+
                //                   "join TB_PROD_FAMILY TPF on TPF.PRODUCT_ID=TP.PRODUCT_ID "+ 
                //                   "join TB_FAMILY TF on TF.FAMILY_ID=TPF.FAMILY_ID and TF.CATEGORY_ID=TB_CATEGORY.CATEGORY_ID"+
                //                   " WHERE  CONVERT(NVARCHAR(100),  TP.MODIFIED_DATE,120) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "' AND (TCP.CATALOG_ID = " + catalogID + ")  order by tb_category.MODIFIED_DATE desc";
                SqlConnection _SQLConn = new SqlConnection(conStr);
                SqlCommand sqlCommand = new SqlCommand( "STP_LS_AUTOWEBSYNC_GETCATEGORYDATA", _SQLConn);
                sqlCommand.CommandType = CommandType.StoredProcedure;                
                sqlCommand.Parameters.Add("@USERID", SqlDbType.Int).Value = UserID;
                sqlCommand.Parameters.Add("@FROM_DATE", SqlDbType.NVarChar).Value = fromDate;
                sqlCommand.Parameters.Add("@TO_DATE", SqlDbType.NVarChar).Value = toDate;
                sqlCommand.Parameters.Add("@CATALOGID", SqlDbType.Int).Value = catalogID;
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                sqlAdapter.Fill(dst);
            }
            catch (Exception ex) { }
            return dst;
        }

        public DataSet GetFamilyData(string fromDate, string toDate, int catalogID, string categoryID, string UserID)
        {
            DataSet dst = new DataSet();
            try
            {
                //string sqlStr = "";
                //sqlStr = "SELECT DISTINCT TB_FAMILY.FAMILY_ID, TB_FAMILY.FAMILY_NAME, TB_FAMILY.FOOT_NOTES, TB_FAMILY.PARENT_FAMILY_ID, TB_FAMILY.ROOT_FAMILY, TB_FAMILY.STATUS, " +
                //          " TB_FAMILY.PRODUCT_TABLE_STRUCTURE, TB_FAMILY.DISPLAY_TABLE_HEADER, TB_CATALOG_FAMILY.CATEGORY_ID, TB_FAMILY.CREATED_USER, " +
                //           " TB_FAMILY.CREATED_DATE, TB_FAMILY.MODIFIED_USER, TB_FAMILY.MODIFIED_DATE,TB_CATALOG_FAMILY.ROOT_CATEGORY  " +
                //          " FROM TB_FAMILY INNER JOIN " +
                //          " TB_CATALOG_FAMILY ON TB_FAMILY.FAMILY_ID = TB_CATALOG_FAMILY.FAMILY_ID and TB_CATALOG_FAMILY.FLAG_RECYCLE='A'  and TB_FAMILY.FLAG_RECYCLE='A' " +
                //          " WHERE TB_FAMILY.ROOT_FAMILY= 1 and (CONVERT(NVARCHAR(100),TB_FAMILY.MODIFIED_DATE,120) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "')" +
                //          "  AND (TB_CATALOG_FAMILY.CATEGORY_ID = '" + categoryID + "') " +
                //         " AND TB_CATALOG_FAMILY.CATALOG_ID = " + catalogID + "  UNION " +
                //         "SELECT DISTINCT TB_FAMILY.FAMILY_ID, TB_FAMILY.FAMILY_NAME, TB_FAMILY.FOOT_NOTES, TB_FAMILY.PARENT_FAMILY_ID, TB_FAMILY.ROOT_FAMILY, TB_FAMILY.STATUS, " +
                //          " TB_FAMILY.PRODUCT_TABLE_STRUCTURE, TB_FAMILY.DISPLAY_TABLE_HEADER, TB_CATALOG_FAMILY.CATEGORY_ID, TB_FAMILY.CREATED_USER, " +
                //           " TB_FAMILY.CREATED_DATE, TB_FAMILY.MODIFIED_USER, TB_FAMILY.MODIFIED_DATE,TB_CATALOG_FAMILY.ROOT_CATEGORY  " +
                //          " FROM     TB_FAMILY INNER JOIN " +
                //          " TB_CATALOG_FAMILY ON TB_FAMILY.FAMILY_ID = TB_CATALOG_FAMILY.FAMILY_ID and TB_CATALOG_FAMILY.FLAG_RECYCLE='A'  and TB_FAMILY.FLAG_RECYCLE='A' " +
                //          "JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID=TB_FAMILY.FAMILY_ID and TPF.FLAG_RECYCLE='A'"+
                //          "join TB_PRODUCT TP ON TP.PRODUCT_ID=TPF.PRODUCT_ID and TP.FLAG_RECYCLE='A'"+
                //          " WHERE  TB_FAMILY.ROOT_FAMILY= 1 and (CONVERT(NVARCHAR(100),TP.MODIFIED_DATE,120) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "')" +
                //          "  AND (TB_CATALOG_FAMILY.CATEGORY_ID = '" + categoryID + "') " +
                //         " AND (TB_CATALOG_FAMILY.CATALOG_ID = " + catalogID +  ") ";


                //sqlStr = "SELECT DISTINCT TB_FAMILY.FAMILY_ID, TB_FAMILY.FAMILY_NAME, TB_FAMILY.FOOT_NOTES, TB_FAMILY.PARENT_FAMILY_ID, TB_FAMILY.ROOT_FAMILY, TB_FAMILY.STATUS, " +
                //             " TB_FAMILY.PRODUCT_TABLE_STRUCTURE, TB_FAMILY.DISPLAY_TABLE_HEADER, TB_CATALOG_FAMILY.CATEGORY_ID, TB_FAMILY.CREATED_USER, " +
                //              " TB_FAMILY.CREATED_DATE, TB_FAMILY.MODIFIED_USER, TB_FAMILY.MODIFIED_DATE,TB_CATALOG_FAMILY.ROOT_CATEGORY  " +
                //             " FROM            TB_FAMILY INNER JOIN  TB_FAMILY F ON  CS.CATEGORY_ID=F.CATEGORY_ID " +
                //                   " AND (((CONVERT(NVARCHAR(10), F.MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') )" +
                //                   " or exists (Select 1 from TB_FAMILY where PARENT_FAMILY_ID=F.FAMILY_ID " +
                //                   " and (CONVERT(NVARCHAR(10), MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') ))" +
                //                   " LEFT OUTER JOIN TB_CATALOG_FAMILY CF ON  CF.FAMILY_ID=F.FAMILY_ID And CF.CATALOG_ID=" + catalogID +
                //                   " LEFT OUTER JOIN TB_PROD_FAMILY PF ON PF.FAMILY_ID=F.FAMILY_ID  AND PF.PRODUCT_ID in (select PRODUCT_ID from [PRODUCTAPPROVALFOR2WS](" + catalogID + ",PF.FAMILY_ID))" +
                //                   " LEFT OUTER JOIN TB_PRODUCT P ON PF.PRODUCT_ID=P.PRODUCT_ID and (CONVERT(NVARCHAR(10), P.MODIFIED_DATE,112) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "') " +
                //                   " LEFT OUTER JOIN TB_CATALOG_PRODUCT CP ON CP.PRODUCT_ID=P.PRODUCT_ID And CP.CATALOG_ID=" + catalogID +
                //                   " WHERE CS.CATALOG_ID=" + catalogID + " And C.CATEGORY_ID in ('" + categoryID + "') AND C.WORKFLOW_STATUS=4 ";

                SqlConnection _SQLConn = new SqlConnection(conStr);
                SqlCommand sqlCommand = new SqlCommand("STP_LS_AUTOWEBSYNC_GETFAMILYDATA", _SQLConn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add("@FROM_DATE", SqlDbType.NVarChar).Value = fromDate;
                sqlCommand.Parameters.Add("@TO_DATE", SqlDbType.NVarChar).Value = toDate;
                sqlCommand.Parameters.Add("@CATALOGID", SqlDbType.Int).Value = catalogID;
                sqlCommand.Parameters.Add("@CATEGORYID", SqlDbType.NVarChar).Value = categoryID;
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                sqlAdapter.Fill(dst);
            }
            catch (Exception ex) { }
            return dst;
        }

        public DataSet GetSubFamilyData(string fromDate, string toDate, int catalogID, string familyID, string categoryID, string UserID)
        {
            DataSet dst = new DataSet();
            try
            {
                //string sqlStr = "";
                //sqlStr = "SELECT DISTINCT TB_FAMILY.FAMILY_ID, TB_FAMILY.FAMILY_NAME, TB_FAMILY.FOOT_NOTES, TB_FAMILY.PARENT_FAMILY_ID, TB_FAMILY.ROOT_FAMILY, TB_FAMILY.STATUS, " +
                //         " TB_FAMILY.PRODUCT_TABLE_STRUCTURE, TB_FAMILY.DISPLAY_TABLE_HEADER, TB_CATALOG_FAMILY.CATEGORY_ID, TB_FAMILY.CREATED_USER, " +
                //          " TB_FAMILY.CREATED_DATE, TB_FAMILY.MODIFIED_USER, TB_FAMILY.MODIFIED_DATE,TB_CATALOG_FAMILY.ROOT_CATEGORY  " +
                //         " FROM            TB_FAMILY INNER JOIN " +
                //         " TB_CATALOG_FAMILY ON TB_FAMILY.FAMILY_ID = TB_CATALOG_FAMILY.FAMILY_ID " +
                //         " WHERE    TB_FAMILY.PARENT_FAMILY_ID= " + familyID + " and (CONVERT(NVARCHAR(100),TB_FAMILY.MODIFIED_DATE,120) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "')" +
                //         " AND (TB_CATALOG_FAMILY.CATEGORY_ID = '" + categoryID + "')" +
                //         " AND (TB_CATALOG_FAMILY.CATALOG_ID = " + catalogID + " UNION "+
                //         "SELECT DISTINCT TB_FAMILY.FAMILY_ID, TB_FAMILY.FAMILY_NAME, TB_FAMILY.FOOT_NOTES, TB_FAMILY.PARENT_FAMILY_ID, TB_FAMILY.ROOT_FAMILY, TB_FAMILY.STATUS, " +
                //         " TB_FAMILY.PRODUCT_TABLE_STRUCTURE, TB_FAMILY.DISPLAY_TABLE_HEADER, TB_CATALOG_FAMILY.CATEGORY_ID, TB_FAMILY.CREATED_USER, " +
                //          " TB_FAMILY.CREATED_DATE, TB_FAMILY.MODIFIED_USER, TB_FAMILY.MODIFIED_DATE,TB_CATALOG_FAMILY.ROOT_CATEGORY  " +
                //         " FROM            TB_FAMILY INNER JOIN " +
                //         " TB_CATALOG_FAMILY ON TB_FAMILY.FAMILY_ID = TB_CATALOG_FAMILY.FAMILY_ID " +
                //          "JOIN TB_PROD_FAMILY TPF ON TPF.FAMILY_ID=TB_FAMILY.FAMILY_ID and TPF.FLAG_RECYCLE='A'" +
                //          "join TB_PRODUCT TP ON TP.PRODUCT_ID=TPF.PRODUCT_ID and TP.FLAG_RECYCLE='A'" +
                //         " WHERE    TB_FAMILY.PARENT_FAMILY_ID= " + familyID + " and (CONVERT(NVARCHAR(100),TP.MODIFIED_DATE,120) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "')" +
                //         " AND (TB_CATALOG_FAMILY.CATEGORY_ID = '" + categoryID + "')" +
                //         " AND (TB_CATALOG_FAMILY.CATALOG_ID = " + catalogID + ")";
                SqlConnection _SQLConn = new SqlConnection(conStr);
                SqlCommand sqlCommand = new SqlCommand("STP_LS_AUTOWEBSYNC_GETSUBFAMILYDATA", _SQLConn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add("@FAMILYID", SqlDbType.Int).Value = familyID;
                sqlCommand.Parameters.Add("@FROM_DATE", SqlDbType.NVarChar).Value = fromDate;
                sqlCommand.Parameters.Add("@TO_DATE", SqlDbType.NVarChar).Value = toDate;
                sqlCommand.Parameters.Add("@CATALOGID", SqlDbType.Int).Value = catalogID;
                sqlCommand.Parameters.Add("@CATEGORYID", SqlDbType.NVarChar).Value = categoryID;
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                sqlAdapter.Fill(dst);
            }
            catch (Exception ex) { }
            return dst;
        }

        public DataSet GetFamilyProducts(string fromDate, string toDate, int catalogID, string familyID, string UserID)
        {
            DataSet dst = new DataSet();
            try
            {
            //    string sqlStr = "";
            //    sqlStr = "SELECT DISTINCT TB_PROD_FAMILY.PRODUCT_ID,TB_PROD_SPECS.STRING_VALUE" +
            //                        " FROM TB_PROD_FAMILY,TB_CATALOG_PRODUCT,TB_PROD_SPECS,TB_PRODUCT" +
            //                        " WHERE TB_PRODUCT.PRODUCT_ID=TB_PROD_FAMILY.PRODUCT_ID AND TB_CATALOG_PRODUCT.PRODUCT_ID=TB_PROD_FAMILY.PRODUCT_ID AND" +
            //                        " TB_PROD_FAMILY.FAMILY_ID=" + familyID + " AND TB_PROD_SPECS.PRODUCT_ID=TB_PROD_FAMILY.PRODUCT_ID AND TB_PROD_SPECS.ATTRIBUTE_ID = 1" +
            //                        " AND TB_PROD_FAMILY.FLAG_RECYCLE='A' and CONVERT(NVARCHAR(100),TB_PROD_SPECS.MODIFIED_DATE,120) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "' ";
                SqlConnection _SQLConn = new SqlConnection(conStr);
                SqlCommand sqlCommand = new SqlCommand("STP_LS_AUTOWEBSYNC_GETPRODUCTDATA", _SQLConn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add("@FAMILYID", SqlDbType.Int).Value = familyID;
                sqlCommand.Parameters.Add("@FROM_DATE", SqlDbType.NVarChar).Value = fromDate;
                sqlCommand.Parameters.Add("@TO_DATE", SqlDbType.NVarChar).Value = toDate;
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                sqlAdapter.Fill(dst);
            }
            catch (Exception ex) { }
            return dst;
        }


        public DataSet GetFamilySubProducts(string fromDate, string toDate, int catalogID, string familyID,int productId, string UserID)
        {
            DataSet dst = new DataSet();
            try
            {
                //string sqlStr = "";
                //sqlStr = "SELECT DISTINCT TB_SUBPRODUCT.SUBPRODUCT_ID,TB_PROD_SPECS.STRING_VALUE" +
                //                    " FROM TB_SUBPRODUCT,TB_PROD_SPECS,TB_PRODUCT,TB_PROD_FAMILY" +
                //                    " WHERE TB_PRODUCT.PRODUCT_ID=TB_SUBPRODUCT.SUBPRODUCT_ID AND TB_PROD_FAMILY.PRODUCT_ID=TB_SUBPRODUCT.PRODUCT_ID and TB_PROD_FAMILY.FAMILY_ID= "+ familyID + " and" +
                //                    " TB_SUBPRODUCT.PRODUCT_ID= "+ productId + "  AND TB_PROD_SPECS.PRODUCT_ID=TB_SUBPRODUCT.SUBPRODUCT_ID AND TB_PROD_SPECS.ATTRIBUTE_ID = 1" +
                //                    " AND CONVERT(NVARCHAR(100),TB_PROD_SPECS.MODIFIED_DATE,120) BETWEEN '" + String.Format("{0:yyyyMMdd}", fromDate) + "' AND  '" + String.Format("{0:yyyyMMdd}", toDate) + "' ";
                SqlConnection _SQLConn = new SqlConnection(conStr);
                SqlCommand sqlCommand = new SqlCommand("STP_LS_AUTOWEBSYNC_GETSUBPRODUCTDATA", _SQLConn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                sqlCommand.Parameters.Add("@FAMILYID", SqlDbType.Int).Value = familyID;
                sqlCommand.Parameters.Add("@PRODUCTID", SqlDbType.Int).Value = productId;
                sqlCommand.Parameters.Add("@FROM_DATE", SqlDbType.NVarChar).Value = fromDate;
                sqlCommand.Parameters.Add("@TO_DATE", SqlDbType.NVarChar).Value = toDate;
                SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                sqlAdapter.Fill(dst);
            }
            catch (Exception ex) { }
            return dst;
        }

        public DataSet GetCatalogID()
        {
            System.Configuration.Configuration AConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            //= ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            DataSet dst = new DataSet();
            //string sqlStr = "";
            try
            {
               // AConfig.AppSettings.Settings["catalogname"].Value != " " && AConfig.AppSettings.Settings["catalogname"].Value != ""
                     string catalogName = AConfig.AppSettings.Settings["catalogname"].Value ;
                     Console.WriteLine(catalogName);
                     SqlConnection _SQLConn = new SqlConnection(conStr);
                     SqlCommand sqlCommand = new SqlCommand("STP_LS_GET_CATALOG_ID", _SQLConn);
                     sqlCommand.CommandType = CommandType.StoredProcedure;
                     sqlCommand.Parameters.Add("@CATALOGNAME", SqlDbType.NVarChar).Value = catalogName;
                     SqlDataAdapter sqlAdapter = new SqlDataAdapter(sqlCommand);
                       sqlAdapter.Fill(dst);

            }
            catch (Exception ex) { }
            return dst;
        }
        public string Getenddate()
        {
            string result = "";
            try
            {
                //string sqlStr = "SELECT  CONVERT(NVARCHAR,CONVERT(date,CONVERT(NVARCHAR,GETDATE(),112),120))+' 23:59:59.000'";
                SqlConnection _SQLConn = new SqlConnection(conStr);
                _SQLConn.Open();
                SqlCommand sqlCommand = new SqlCommand("STP_LS_GET_END_DATE", _SQLConn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                result = sqlCommand.ExecuteScalar().ToString();
                _SQLConn.Close();
              
            }
            catch (Exception ex) { }
            return result;
        }
        public string GetStartdate()
        {
            string result = "";
            try
            {
                //string sqlStr = "SELECT  CONVERT(NVARCHAR,CONVERT(date,CONVERT(NVARCHAR,GETDATE(),112),120))+' 00:00:00.000'";
                SqlConnection _SQLConn = new SqlConnection(conStr);
                _SQLConn.Open();
                SqlCommand sqlCommand = new SqlCommand("STP_LS_GET_START_DATE", _SQLConn);
                sqlCommand.CommandType = CommandType.StoredProcedure;
                result = sqlCommand.ExecuteScalar().ToString();
                _SQLConn.Close();
            }
            catch (Exception ex) { }
            return result;
        }

        public DataTable GetXmlAttributeGroup(string fromDate, string toDate, int _catalogID,int userId,string sessionID)
        {
            DataTable familyXmlDataSet = new DataTable();
            DataTable productXmlDataSet = new DataTable();
          
            DataSet ds = new DataSet();
            try
            {
                using ( SqlConnection _SQLConn = new SqlConnection(conStr))
                {
                    using (SqlCommand cmd = new SqlCommand("STP_CATALOGSTUDIO_XML_ATTRIBUTEGROUP", _SQLConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                        cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                        cmd.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = _catalogID;

                        _SQLConn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(familyXmlDataSet);

                        _SQLConn.Close();

                    }
                    using (SqlCommand cmd = new SqlCommand("STP_CATALOGSTUDIO_XML_ATTRIBUTEGROUPPRODUCT", _SQLConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                        cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                        cmd.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = _catalogID;

                        _SQLConn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(productXmlDataSet);
                        _SQLConn.Close();
                    }
               
                    using (SqlCommand cmd = new SqlCommand("DELETEDATTRIBUTEGRUOP", _SQLConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                        cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                        cmd.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = _catalogID;

                        _SQLConn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(ds);
                        _SQLConn.Close();
                    }

                }
            }
            catch(Exception ex)
            {

            }
            XML(familyXmlDataSet, productXmlDataSet, ds, userId, sessionID);
            return familyXmlDataSet;
        }

        public void GetXmlPicklist(string fromDate, string toDate, int userId, string sessionID)
        {
            try
            {
                DataTable webSync_Picklist = new DataTable();
                DataTable picklist_User = new DataTable();

                string user_Name = string.Empty;

                using (SqlConnection SQLConn_Picklist = new SqlConnection(conStr))
                {
                    SqlCommand cmd_User = new SqlCommand("select User_Name from Customer_User where CustomerId=" + userId + "", SQLConn_Picklist);
                    SqlDataAdapter picklist_Adapter = new SqlDataAdapter(cmd_User);
                    SQLConn_Picklist.Open();
                    user_Name = (string)cmd_User.ExecuteScalar();


                    SqlCommand cmd = new SqlCommand("STP_CATALOGSTUDIO_XML_WEBSYNC_PICKLIST", SQLConn_Picklist);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                    cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                    cmd.Parameters.Add("@USER", SqlDbType.NVarChar).Value = user_Name;


                    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(webSync_Picklist);
                    SQLConn_Picklist.Close();
                }



                Picklist_XML(webSync_Picklist, sessionID, user_Name);
            }
            catch (Exception ex)
            {

            }
        }


        public string Picklist_XML(DataTable webSync_Picklist, string sessionID, string user_Name)
        {
            try
            {
                if (webSync_Picklist.Rows.Count > 0)
                {

                    var picklist_ListValues = webSync_Picklist.AsEnumerable().
                           Select(x => new
                           {
                               PICKLIST_NAME = x.Field<string>("PICKLIST_NAME").ToString(),
                               PICKLIST_DATA_TYPE = x.Field<string>("PICKLIST_DATA_TYPE").ToString(),
                               PICKLIST_DATA = x.Field<string>("PICKLIST_DATA").ToString()

                           }).Distinct().ToList();

                    var xml_PicklistElements = new XElement("PICKLIST",
                     from emp in picklist_ListValues
                     select new XElement("ITEM",
                                  new XAttribute("PICKLIST_NAME", emp.PICKLIST_NAME),
                                  new XAttribute("PICKLIST_DATA_TYPE", emp.PICKLIST_DATA_TYPE),
                                  new XAttribute("PICKLIST_DATA", emp.PICKLIST_DATA),
                                   new XAttribute("CUSTOMER_NAME", user_Name)
                                ));
                    string xml_Picklist = string.Empty;
                    xml_Picklist = xml_PicklistElements.ToString();
                    //if (xml_Picklist != "")
                    //{
                    string File_name1 = Application.StartupPath + "\\XML\\myXmlSPICKLIST" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xml";
                    File.WriteAllText(File_name1, xml_Picklist);

                    Attr_SyncDatas(sessionID, xml_Picklist, user, "BOTH", "Live", "PICKLIST");
                }


                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public string XML(DataTable familyXmlDataSet, DataTable productXmlDataSet, DataSet deletedAttribute,int CustomerId,string sessionID)
        {
           
            try
            {

                //*****************Attribute Group - Start**************//

           

                DataTable deletedAttributeFamily = new DataTable();
                DataTable deletedAttributeProduct = new DataTable();
                deletedAttributeFamily = deletedAttribute.Tables[0];
                deletedAttributeProduct = deletedAttribute.Tables[1];
                string xmlAttributeGroup = "<tradingbell2ws_AttributeGroupPacket>";



                string familAttributeGroup = "<ATTRIBUTE_GROUP TYPE='FAMILY'>";
                string familyGroupName = string.Empty;             


                var group_Name = familyXmlDataSet.AsEnumerable().Select(s => new
                {
                    Name = s.Field<string>("GROUP_NAME").ToString(),
                    Id = s.Field<int>("GROUP_ID").ToString()
                }).Distinct().ToList();

                for (int i = 0; i < group_Name.Count; i++)
                {
                    string family_Group_Name = group_Name[i].Name;

                    var FAMILY = familyXmlDataSet.AsEnumerable().Where(s => s.Field<string>("GROUP_NAME") == family_Group_Name).
                        Select(x => new
                        {
                            catalog_Id = x.Field<int>("CATALOG_ID").ToString(),
                            Name = x.Field<string>("FAMILY_NAME").ToString(),
                            Id = x.Field<int>("FAMILY_Id").ToString(),
                            Level = x.Field<string>("LEVEL").ToString()
                        }).Distinct().ToList();

                    //Select(s => s.Field<string>("FAMILY_NAME").ToString()).ToList();
                    var Attribute = familyXmlDataSet.AsEnumerable().Where(s => s.Field<string>("GROUP_NAME") == family_Group_Name).Select(s => new
                    {
                        Name = s.Field<string>("ATTRIBUTE_NAME").ToString(),
                        Id = s.Field<int>("ATTRIBUTE_ID").ToString(),
                        Sortorder = s.Field<int>("SORT_ORDER").ToString()
                    }).Distinct().ToList();


                    var xEle = new XElement("FAMILY",
                    from emp in FAMILY
                    select new XElement("ITEM",
                                 new XAttribute("CATALOG_ID", emp.catalog_Id),
                                 new XAttribute("NAME", emp.Name),
                                 new XAttribute("ID", emp.Id),
                                  new XAttribute("LEVEL", emp.Level)
                               ), new XElement("ATTRIBUTE",
                                        from emp in Attribute
                                        select new XElement("ITEM",
                                      new XAttribute("NAME", emp.Name),
                                      new XAttribute("ID", emp.Id),
                                      new XAttribute("SORTORDER", emp.Sortorder))));
                    // string A = string.Empty;
                    // if (xEle != null)
                    // {
                    string A = xEle.ToString();
                    familyGroupName = familyGroupName + "<GROUP NAME='" + family_Group_Name + "' ID='" + group_Name[i].Id + "' CUSTOMER_ID='" + CustomerId + "'>" + A + "</GROUP>";


                    // }

                    //  familyGroupName = familyGroupName + "<GROUP NAME='" + group_Name[i] + "'>" + A + "</GROUP>";
                }
                if (familyGroupName != "")
                {
                    familAttributeGroup = familAttributeGroup + familyGroupName + "</ATTRIBUTE_GROUP>";
                }
                else
                {
                    familAttributeGroup = string.Empty;
                }


                string productAttributeGroup = "<ATTRIBUTE_GROUP TYPE='Product'>";            

                string productGroupName = string.Empty;
                var productgroup_Name = productXmlDataSet.AsEnumerable().Select(s => new
                {
                    Name = s.Field<string>("GROUP_NAME").ToString(),
                    Id = s.Field<int>("GROUP_ID").ToString()
                }).Distinct().ToList();

                for (int i = 0; i < productgroup_Name.Count; i++)
                {
                    string product_group_Name = productgroup_Name[i].Name;

                    var FAMILY = productXmlDataSet.AsEnumerable().Where(s => s.Field<string>("GROUP_NAME") == product_group_Name).Select(x => new
                    {
                        catalog_Id = x.Field<int>("CATALOG_ID").ToString(),
                        Name = x.Field<string>("FAMILY_NAME").ToString(),
                        Id = x.Field<int>("FAMILY_Id").ToString(),
                        Level = x.Field<string>("LEVEL").ToString()

                    }).Distinct().ToList();
                    // Select(s => s.Field<string>("FAMILY_NAME").ToString() , .Field<string>("FAMILY_ID").Distinct().ToString()).ToList();
                    var Attribute = productXmlDataSet.AsEnumerable().Where(s => s.Field<string>("GROUP_NAME") == product_group_Name).Select(s => new
                    {
                        Name = s.Field<string>("ATTRIBUTE_NAME").ToString(),
                        Id = s.Field<int>("ATTRIBUTE_ID").ToString(),
                        Sortorder = s.Field<int>("SORT_ORDER").ToString()
                    }).Distinct().ToList();


                    var xEle = new XElement("FAMILY",
                    from emp in FAMILY
                    select new XElement("ITEM",
                                new XAttribute("CATALOG_ID", emp.catalog_Id),
                                new XAttribute("NAME", emp.Name),
                                new XAttribute("ID", emp.Id),
                                 new XAttribute("LEVEL", emp.Level)
                              ), new XElement("ATTRIBUTE",
                                        from emp in Attribute
                                        select new XElement("ITEM",
                                      new XAttribute("NAME", emp.Name),
                                      new XAttribute("ID", emp.Id),
                                      new XAttribute("SORTORDER", emp.Sortorder))));
                  
                    string A = xEle.ToString();
                  

                    productGroupName = productGroupName + "<GROUP NAME='" + product_group_Name + "' ID='" + productgroup_Name[i].Id + "' CUSTOMER_ID='" + CustomerId + "' >" + A + "</GROUP>";
                }
                if (productGroupName != "")
                {
                    productAttributeGroup = productAttributeGroup + productGroupName + "</ATTRIBUTE_GROUP>";
                }
                else
                {
                    productAttributeGroup = string.Empty;
                }

                if (familAttributeGroup != "" || productAttributeGroup != "")

                {
                    xmlAttributeGroup = xmlAttributeGroup + familAttributeGroup + productAttributeGroup + " </tradingbell2ws_AttributeGroupPacket>";
                  
                    string File_name1 = Application.StartupPath + "\\XML\\myXmlSATTRIBUTE" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xml";
                    File.WriteAllText(File_name1, xmlAttributeGroup);

                    Attr_SyncDatas(sessionID, xmlAttributeGroup, user, "BOTH", "Live", "ATTR_GROUP");

                }

                /***********Deleted attr group FAMILY*********/
                string DeleteAttributeGroupName = string.Empty;
                string DeleteProductAttributeGroupName = string.Empty;
                string xmlAttributeGroupDeletion = "<tradingbell2ws_AttributeGroupPacket>";
                var deleteGroup_Name = deletedAttributeFamily.AsEnumerable().Select(x => new
                {
                    GROUP_ID = x.Field<int>("GROUP_ID").ToString(),
                    GROUP_NAME = x.Field<string>("GROUP_NAME").ToString(),
                    catalog_Id = x.Field<int>("CATALOG_ID").ToString()
                }).Distinct().ToList();

                var deleteProductGroup_Name = deletedAttributeProduct.AsEnumerable().Select(x => new
                {
                    GROUP_ID = x.Field<int>("GROUP_ID").ToString(),
                    GROUP_NAME = x.Field<string>("GROUP_NAME").ToString(),
                    catalog_Id = x.Field<int>("CATALOG_ID").ToString()
                }).Distinct().ToList();

                deleteGroup_Name.AddRange(deleteProductGroup_Name);


                if (deleteGroup_Name.Count>0)
                {

                    var Deleted = new XElement("DeletedAttributeGroup",
                     from emp in deleteGroup_Name
                     select new XElement("ITEM",
                                new XAttribute("GROUP_ID", emp.GROUP_ID),
                             new XAttribute("GROUP_NAME", emp.GROUP_NAME),
                              new XAttribute("CATALOG_ID", emp.catalog_Id)
                               ));
                    DeleteAttributeGroupName = Deleted.ToString();

                

                    xmlAttributeGroupDeletion = xmlAttributeGroupDeletion + "<ATTRIBUTE_GROUP>" + DeleteAttributeGroupName + "</ATTRIBUTE_GROUP>" + " </tradingbell2ws_AttributeGroupPacket>";

                    string Filename1 = Application.StartupPath + "\\XML\\myXmlSATTRIBUTEDELETION" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xml";
                    File.WriteAllText(Filename1, xmlAttributeGroupDeletion);
                    Attr_SyncDatas(sessionID, xmlAttributeGroupDeletion, user, "BOTH", "Live", "ATTR_GROUP_DELETION");

                }
                //*****************Attribute Group - End**************//

           
            }
            catch (Exception ex)
            {
            }
            return "ok";

        }

        //-------------------------------------------------------INDESIGN TEMPLATES----------------------------------------------------//
        public DataTable GetXmlIndesignTemplate(string fromDate, string toDate, int userId, string sessionID, string catalogID)
        {
            DataTable ProjectXmlDataSet = new DataTable();
            DataTable reocrdXmlDataSet = new DataTable();

            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection _SQLConn = new SqlConnection(conStr))
                {
                    using (SqlCommand cmd = new SqlCommand("STP_CATALOGSTUDIO_XML_INSDESGN_TEMPLATE", _SQLConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                        cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                        cmd.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = catalogID;
                        _SQLConn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(ProjectXmlDataSet);
                        _SQLConn.Close();
                    }
                }
            }
            catch (Exception ex) { }
            INDESIGNXML(ProjectXmlDataSet, userId, sessionID);
            return ProjectXmlDataSet;
        }
        public string INDESIGNXML(DataTable ProjectXmlDataSet, int CustomerId, string sessionID)
        {

            try
            {


                string Indesigntempxml = "<tradingbell2ws_IndesignPacket>";
                string Projectname = "<PROJECT'>";
                string IndesignProjectname = string.Empty;

                var Project_Name = ProjectXmlDataSet.AsEnumerable().Select(s => new
                {
                    Name = s.Field<string>("PROJECT_NAME").ToString(),
                    Id = s.Field<int>("PROJECT_ID").ToString()
                }).Distinct().ToList();

                for (int i = 0; i < Project_Name.Count; i++)
                {
                    string Indesign_project_Name = Project_Name[i].Name.ToString(); ;
                    var PROJECT = ProjectXmlDataSet.AsEnumerable().Where(s => s.Field<string>("PROJECT_NAME") == Indesign_project_Name).
                        Select(x => new
                        {
                            project_id = x.Field<int>("PROJECT_ID"),
                            Name = x.Field<string>("PROJECT_NAME").ToString(),
                            Record_Id = x.Field<int>("RECORD_ID"),
                            Family_id = x.Field<int>("FAMILY_ID"),
                            Project_type = x.Field<byte>("PROJECT_TYPE"),
                            Workflow_Status = x.Field<int>("WORKFLOW_STATUS"),
                            Catalog_Id = x.Field<int>("CATALOG_ID"),
                            Xml_File_Name = x.Field<string>("XML_FILE_NAME").ToString(),
                            File_No = x.Field<int>("FILE_NO"),
                            Sort_Order = x.Field<int>("SORT_ORDER"),
                            Section_Name = x.Field<string>("SECTION_NAME").ToString(),
                            PUBLISHED_FSTATUS = x.Field<int>("PUBLISHED_FSTATUS"),
                            FILTER_STATUS = x.Field<bool>("FILTER_STATUS"),
                            CATEGORY_ID = x.Field<string>("CATEGORY_ID").ToString()

                        }).Distinct().ToList();

                    var xEle = new XElement("PROJECT",
                    from emp in PROJECT
                    select new XElement("ITEM",
                                 new XElement("PROJECT_ID", emp.project_id),
                                 new XElement("PROJECT_NAME", emp.Name),
                                 new XElement("RECORD_ID", emp.Record_Id),
                                 new XElement("FAMILY_ID", emp.Family_id),
                                 new XElement("PROJECT_TYPE", emp.Project_type),
                                 new XElement("WORKFLOW_STATUS", emp.Workflow_Status),
                                 new XElement("CATALOG_ID", emp.Catalog_Id),
                                 new XElement("XML_FILE_NAME", emp.Xml_File_Name),
                                 new XElement("FILE_NO", emp.File_No),
                                 new XElement("SORT_ORDER", emp.Sort_Order),
                                 new XElement("SECTION_NAME", emp.Section_Name),
                                 new XElement("PUBLISHED_FSTATUS", emp.PUBLISHED_FSTATUS),
                                 new XElement("FILTER_STATUS", emp.FILTER_STATUS),
                                 new XElement("CATEGORY_ID", emp.CATEGORY_ID)
                               ));

                    string xmltmep = xEle.ToString();
                    IndesignProjectname = IndesignProjectname + xmltmep;
                }

                if (Projectname != "")
                {
                    Indesigntempxml = Indesigntempxml + IndesignProjectname + " </tradingbell2ws_IndesignPacket>";
                    string File_name1 = Application.StartupPath + "\\XML\\myXmlINDESIGN" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xml";
                    File.WriteAllText(File_name1, Indesigntempxml);
                    SyncDatas(sessionID, Indesigntempxml, CustomerId, "Time span", "Auto", "INDSIGN");
                }
            }

            catch (Exception ex)
            {
                return null;
            }
            return "ok";

        }
        //-------------------------------------------------------INDESIGN TEMPLATES----------------------------------------------------//

    }
}
