﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using System.IO.Compression;
using Microsoft.Office.Interop;
using System.Web.SessionState;
using LS.Data;
using LS.Web.Controllers;
using LS.Web.Models;
using Newtonsoft.Json;
using DataTable = System.Data.DataTable;
using Workbook = Infragistics.Documents.Excel.Workbook;
using Infragistics.Documents.Excel;

namespace LS.Web
{
    /// <summary>
    /// Summary description for DownloadRoleExport
    /// </summary>
    public class DownloadRoleExport : IHttpHandler, IRequiresSessionState
    {

        private readonly CSEntities _dbcontext = new CSEntities();
        public void ProcessRequest(HttpContext context)
        {
            var roleExport = (DataSet)System.Web.HttpContext.Current.Session["roleExport"];
            DataTable roleExportTable = new DataTable();
            string filename = "RoleExport.xls";
            string[] tableName = { "RoleCatalog", "RoleGroup_name", "RoleAttribute" };
            var workbook = new Workbook();
            int incTableName = 0;
            HttpResponse response = context.Response;
            try
            {

                foreach (DataTable dt in roleExport.Tables)
                {

                    roleExportTable = dt;
                    if (roleExportTable.Rows.Count > 0)
                    {

                        string table = tableName[incTableName];
                        incTableName = incTableName + 1;
                        int rowcntTemp = roleExportTable.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        do
                        {
                            int rowcnt;
                            if (rowcntTemp <= 65000)
                            {
                                rowcnt = rowcntTemp;
                                rowcntTemp = rowcnt - 65000;
                            }
                            else
                            {
                                rowcnt = 65000;
                                rowcntTemp = roleExportTable.Rows.Count - (runningcnt + 65000);
                            }
                            j++;
                            if (j != 1)
                            {
                                table = table + (j - 1);
                            }
                            var worksheet = workbook.Worksheets.Add(table);
                            workbook.WindowOptions.SelectedWorksheet = worksheet;
                            for (int columnIndex = 0; columnIndex < roleExportTable.Columns.Count; columnIndex++)
                            {

                                if (HttpUtility.HtmlDecode(roleExportTable.Columns[columnIndex].ColumnName) != "ID" && HttpUtility.HtmlDecode(roleExportTable.Columns[columnIndex].ColumnName) != "ROLE_ID" && HttpUtility.HtmlDecode(roleExportTable.Columns[columnIndex].ColumnName) != "CATALOGID")
                                {


                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(roleExportTable.Columns[columnIndex].ColumnName);

                                }
                            }
                            int rowIndex = 2;
                            int temprunningcnt = runningcnt;
                            for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                            {
                                var row = worksheet.Rows[rowIndex++];
                                runningcnt++;
                                for (int columnIndex = 0; columnIndex < roleExportTable.Rows[k].ItemArray.Length; columnIndex++)
                                {
                                    row.Cells[columnIndex].Value = roleExportTable.Rows[k].ItemArray[columnIndex];
                                }
                            }

                        }
                        while (rowcntTemp > 0);
                    }
                    else
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("No records found.");
                    }

                }
                string filepath = context.Server.MapPath("~/Content/" + filename);
                if (File.Exists(filepath))
                {
                    File.Delete(filepath);
                }
                workbook.Save(context.Server.MapPath("~/Content/" + filename));
                if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                {
                    response.ClearContent();
                    response.Clear();
                    response.ContentType = "application/vnd.ms-excel";
                    response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", filename));
                    response.TransmitFile(context.Server.MapPath("~/Content/" + filename));

                    response.Flush();
                    response.End();
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("File not found!");
                }
            }
            catch (Exception ex)
            {

            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}