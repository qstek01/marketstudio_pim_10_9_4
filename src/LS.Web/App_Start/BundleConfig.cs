﻿using System.Web;
using System.Web.Optimization;

namespace LS.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/Design/css").Include(
                      "~/Design/bootstrap.css",
                      "~/Design/kendo.common-bootstrap.css",
                      "~/Design/kendo.bootstrap.css",
                      "~/Design/angular-block-ui.css",
                      "~/Design/assets/css/ng-table.css",
                      "~/Design/toaster.css",
                      "~/Design/site.css"));

            bundles.Add(new StyleBundle("~/Design/ls").Include(
                   "~/Design/assets/plugins/bootstrap/css/bootstrap.min.css",
                   "~/Design/assets/fonts/style.css",
                   "~/Design/assets/css/main.css",
                   "~/​Design/assets/css/main-responsive.css",
                   "~/Design/assets/plugins/iCheck/skins/all.css",
                   "~/Design/assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css",
                   "~/Design/assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css",
                   "~/Design/assets/css/theme_light.css"));

            bundles.Add(new StyleBundle("~/Design/lskendo").Include(
                     "~/Design/assets/kendo/styles/kendo.offline.css",
                     "~/Design/assets/kendo/styles/kendo.common.min.css",
                     "~/Design/assets/kendo/styles/kendo.blueopal.min.css",
                     "~/Design/assets/kendo/styles/kendo.custom_style.css",
                     "~/​Design/Lightbox.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                           "~/Scripts/JQuery/jquery-1.10.2.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/Others/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Design/assets/js/bootstrap.min.js",
                      "~/Design/Others/respond.js",
                      "~/Design/Others/linq.js"));

            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
                    "~/Scripts/AngularJs/angular.js",
                    "~/Scripts/AngularJs/angular-route.js",
                    "~/Scripts/Others/kendo.all.min.js",
                    "~/Scripts/Others/angular-block-ui.js",
                    "~/Scripts/AngularJs/angular-animate.js",
                    "~/Scripts/Others/ng-toaster.js",
                    "~/Scripts/Others/stacktrace.0.6.4.js",
                    "~/Scripts/AngularJs/ng-table.js",
                    "~/Scripts/AngularJs/ng-grid.js",
                    "~/Scripts/AngularJs/angular-scrollable-table.js",
                    "~/Design/js/ngDialog.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                     "~/App/routeResolverServices.js",
                     "~/App/app.js",
                     "~/App/controllers/navBarController.js",
                     "~/App/factory/dataFactory.js"));

            //#if Debug
            System.Web.Optimization.BundleTable.EnableOptimizations = false;
            //#else
            //          System.Web.Optimization.BundleTable.EnableOptimizations = true;
            //#endif

        }
    }
}
