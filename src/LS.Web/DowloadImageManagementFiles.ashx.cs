﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace LS.Web
{
    /// <summary>
    /// Summary description for DowloadImageManagementFiles
    /// </summary>
    public class DowloadImageManagementFiles : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            HttpResponse response = context.Response;
            string filePath = HttpContext.Current.Request.QueryString["Path"];
            string fileExtension = HttpContext.Current.Request.QueryString["Type"];
            filePath = filePath.Split(',')[filePath.Split(',').Length - 1];
            string fullPath = Path.GetFullPath(filePath).TrimEnd(Path.DirectorySeparatorChar);
            string filename = fullPath.Split(Path.DirectorySeparatorChar).Last();
            if (HttpContext.Current.Request.QueryString["DownloadType"] != null)
            {
                filePath =HttpContext.Current.Server.MapPath("~/Content/ValidationLog/"+ filePath + fileExtension);
                filename = HttpContext.Current.Request.QueryString["DownloadType"].ToString();
                if (File.Exists(filePath))
                {
                    response.ClearContent();
                    response.Clear();
                    response.ContentType = "text/plain";
                    response.AddHeader("Content-Disposition",
                                       "attachment; filename=" + filename + ";");
                    response.TransmitFile(filePath);
                    response.Flush();
                    response.End();
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("File not found!");
                }
            }
            else
            {
                if (File.Exists(filePath))
                {
                    response.ClearContent();
                    response.Clear();
                    response.ContentType = "text/plain";
                    response.AddHeader("Content-Disposition",
                                       "attachment; filename=" + filename + ";");
                    response.TransmitFile(filePath);
                    response.Flush();
                    response.End();
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("File not found!");
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}