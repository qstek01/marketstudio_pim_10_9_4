﻿using LS.Data;
using LS.Web.Controllers;
using LS.Web.Models;
using System;
using System.IO;
using System.IO.Compression;
using System.Web;
using System.Web.SessionState;
using DataTable = System.Data.DataTable;
using Workbook = Infragistics.Documents.Excel.Workbook;
namespace LS.Web
{
    /// <summary>
    /// Summary description for DownloadFullExport
    /// </summary>
    public class DownloadFullExport : IHttpHandler
    {

        private readonly CSEntities _dbcontext = new CSEntities();

        QueryValues queryValues = new QueryValues();
        HomeApiController homeObj = new HomeApiController();


        public void ProcessRequest(HttpContext context)
        {

            //CustomerItemNo
            if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
            {

                System.Web.HttpContext.Current.Session["CustomerItemNo"] = homeObj.getCatalogItemNumberValues();
            }
            // CustomerSubItemNo
            if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
            {
                System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = homeObj.getCatalogSubItemNumberValues();
            }



            //context.Response.ContentType = "text/plain";
            HttpResponse response = context.Response;
            string filename = HttpContext.Current.Request.QueryString["Path"];
            bool EnableSubProduct = Convert.ToBoolean(HttpContext.Current.Request.QueryString["EnableSubProduct"]);
            bool EnableTableDesigner = Convert.ToBoolean(HttpContext.Current.Request.QueryString["EnableTableDesigner"]);
            string Type = HttpContext.Current.Request.QueryString["Type"];
            FileInfo fi = new FileInfo(filename);
            string extn = fi.Extension;





            if (((context.Session["ExportTable"] != null) || (context.Session["KEYWORDS"] != null) || (context.Session["ExportTableAttributeList"] != null) || (context.Session["ExportTableInDesign"] != null) || (context.Session["Exemptwords"] != null)) && extn.ToLower() == ".xls")
            {

                var workbook = new Workbook();

                if (context.Session["ExportTableCAT"] != null)
                {
                    context.Session["Exemptwords"] = null;
                    var distinctdsCat = (DataTable)context.Session["ExportTableCAT"];

                    //CATEGORY LEVEL EXPORTR & PROTAL REMOVAL
                    if (distinctdsCat.Columns.Contains("CATEGORY_PUBLISH2EXPORT"))
                    {
                        distinctdsCat.Columns.Remove("CATEGORY_PUBLISH2EXPORT");
                    }
                    if (distinctdsCat.Columns.Contains("CATEGORY_PUBLISH2PORTAL"))
                    {
                        distinctdsCat.Columns.Remove("CATEGORY_PUBLISH2PORTAL");
                    }

                    if (!distinctdsCat.Columns.Contains("Action"))
                    {
                        distinctdsCat.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }


                    var tableCat = distinctdsCat.DefaultView.ToTable(true);
                    // table.Columns.Add("sdfsaf");
                    // table.Columns.Add("sdfsaf1");
                    // table.Columns.Add("sdfsaf2");
                    context.Session["ExportTableCAT"] = null;
                    context.Session["PDFCONTENT"] = null;
                    context.Session["KEYWORDS"] = null;
                    context.Session["Exemptwords"] = null;
                    //var objExport = new RKLib.ExportData.Export();
                    if (tableCat.Rows.Count > 0)
                    {
                        const string tableName1 = "Categories";
                        string tableName = "Categories";

                        int rowcntTemp = tableCat.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        do
                        {
                            int rowcnt;
                            if (rowcntTemp <= 65000)
                            {
                                rowcnt = rowcntTemp;
                                rowcntTemp = rowcnt - 65000;
                            }
                            else
                            {
                                rowcnt = 65000;
                                rowcntTemp = tableCat.Rows.Count - (runningcnt + 65000);
                            }
                            j++;
                            if (j != 1)
                            {
                                tableName = tableName1 + (j - 1);
                            }
                            var worksheet = workbook.Worksheets.Add(tableName);
                            workbook.WindowOptions.SelectedWorksheet = worksheet;
                            for (int jj = 1; jj <= rowcnt; jj++)
                                // Create the worksheet to represent this data table
                                for (int columnIndex = 0; columnIndex < tableCat.Columns.Count; columnIndex++)
                                {

                                    if (HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName).Contains("SUBL"))
                                    {
                                        string[] tokens = HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName).Split('-');

                                        worksheet.Rows[1].Cells[columnIndex].Value = tokens[1];
                                    }
                                    else if (HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName);
                                    }
                                }


                            // Create column headers for each column

                            // Starting at row index 1, copy all data rows in
                            // the data table to the worksheet

                            int rowIndex = 2;
                            int temprunningcnt = runningcnt;
                            for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                            {
                                var row = worksheet.Rows[rowIndex++];
                                runningcnt++;
                                for (int columnIndex = 0; columnIndex < tableCat.Rows[k].ItemArray.Length; columnIndex++)
                                {
                                    row.Cells[columnIndex].Value = tableCat.Rows[k].ItemArray[columnIndex];
                                }
                            }

                        }

                        while (rowcntTemp > 0);


                    }
                    else
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("No records found.");
                    }
                }





                if (context.Session["ExportTableFAM"] != null)
                {
                    var distinctdsFam = (DataTable)context.Session["ExportTableFAM"];

                    //FAMILY LEVEL EXPORTR & PROTAL REMOVAL

                    if (distinctdsFam.Columns.Contains("FAMILY_PUBLISH2EXPORT"))
                    {
                        distinctdsFam.Columns.Remove("FAMILY_PUBLISH2EXPORT");
                    }
                    if (distinctdsFam.Columns.Contains("FAMILY_PUBLISH2PORTAL"))
                    {
                        distinctdsFam.Columns.Remove("FAMILY_PUBLISH2PORTAL");
                    }

                    if (!distinctdsFam.Columns.Contains("Action"))
                    {
                        distinctdsFam.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }

                    // New Name changes inside the Sheet column Product(Family)

                    if (distinctdsFam.Columns.Contains("FAMILY_ID"))
                    {
                        distinctdsFam.Columns["FAMILY_ID"].ColumnName = "PRODUCT_ID";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("FAMILY_NAME"))
                    {
                        distinctdsFam.Columns["FAMILY_NAME"].ColumnName = "PRODUCT_NAME";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("SUBFAMILY_ID"))
                    {
                        distinctdsFam.Columns["SUBFAMILY_ID"].ColumnName = "SUBPRODUCT_ID";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("SUBFAMILY_NAME"))
                    {
                        distinctdsFam.Columns["SUBFAMILY_NAME"].ColumnName = "SUBPRODUCT_NAME";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("Product_PdfTemplate"))
                    {
                        distinctdsFam.Columns["Product_PdfTemplate"].ColumnName = "Item_PdfTemplate";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("Family_PdfTemplate"))
                    {
                        distinctdsFam.Columns["Family_PdfTemplate"].ColumnName = "Product_PdfTemplate";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("FAMILY_PUBLISH2PDF"))
                    {
                        distinctdsFam.Columns["FAMILY_PUBLISH2PDF"].ColumnName = "PRODUCT_PUBLISH2PDF";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("FAMILY_PUBLISH2PRINT"))
                    {
                        distinctdsFam.Columns["FAMILY_PUBLISH2PRINT"].ColumnName = "PRODUCT_PUBLISH2PRINT";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("FAMILY_PUBLISH2WEB"))
                    {
                        distinctdsFam.Columns["FAMILY_PUBLISH2WEB"].ColumnName = "PRODUCT_PUBLISH2WEB";
                        distinctdsFam.AcceptChanges();
                    }



                    var tableFam = distinctdsFam.DefaultView.ToTable(true);
                    // table.Columns.Add("sdfsaf");
                    // table.Columns.Add("sdfsaf1");
                    // table.Columns.Add("sdfsaf2");
                    context.Session["ExportTableFAM"] = null;
                    context.Session["PDFCONTENT"] = null;
                    context.Session["KEYWORDS"] = null;
                    context.Session["Exemptwords"] = null;
                    //var objExport = new RKLib.ExportData.Export();
                    if (tableFam.Rows.Count > 0)
                    {
                        const string tableName1 = "Products";
                        string tableName = "Products";

                        int rowcntTemp = tableFam.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        do
                        {
                            int rowcnt;
                            if (rowcntTemp <= 65000)
                            {
                                rowcnt = rowcntTemp;
                                rowcntTemp = rowcnt - 65000;
                            }
                            else
                            {
                                rowcnt = 65000;
                                rowcntTemp = tableFam.Rows.Count - (runningcnt + 65000);
                            }
                            j++;
                            if (j != 1)
                            {
                                tableName = tableName1 + (j - 1);
                            }
                            var worksheet = workbook.Worksheets.Add(tableName);
                            workbook.WindowOptions.SelectedWorksheet = worksheet;
                            for (int jj = 1; jj <= rowcnt; jj++)

                                // Create the worksheet to represent this data table


                                // Create column headers for each column
                                for (int columnIndex = 0; columnIndex < tableFam.Columns.Count; columnIndex++)
                                {

                                    if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "ITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName);
                                    }
                                }




                            // Starting at row index 1, copy all data rows in
                            // the data table to the worksheet

                            int rowIndex = 2;
                            int temprunningcnt = runningcnt;
                            for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                            {
                                var row = worksheet.Rows[rowIndex++];
                                runningcnt++;
                                for (int columnIndex = 0; columnIndex < tableFam.Rows[k].ItemArray.Length; columnIndex++)
                                {
                                    row.Cells[columnIndex].Value = tableFam.Rows[k].ItemArray[columnIndex];
                                }
                            }

                        }

                        while (rowcntTemp > 0);


                    }
                    else
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("No records found.");
                    }
                }


                if ((context.Session["ExportTable"] != null))
                {
                    var distinctds = (DataTable)context.Session["ExportTable"];

                    //product export & portal Removal
                    if (distinctds.Columns.Contains("PRODUCT_PUBLISH2EXPORT"))
                    {
                        distinctds.Columns.Remove("PRODUCT_PUBLISH2EXPORT");
                    }
                    if (distinctds.Columns.Contains("PRODUCT_PUBLISH2PORTAL"))
                    {
                        distinctds.Columns.Remove("PRODUCT_PUBLISH2PORTAL");
                    }


                    if (!distinctds.Columns.Contains("Action"))
                    {
                        distinctds.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }

                    // New Name changes inside the Sheet column Item (Product)

                    if (distinctds.Columns.Contains("PRODUCT_ID"))
                    {
                        distinctds.Columns["PRODUCT_ID"].ColumnName = "ITEM_ID";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("FAMILY_ID"))
                    {
                        distinctds.Columns["FAMILY_ID"].ColumnName = "PRODUCT_ID";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("FAMILY_NAME"))
                    {
                        distinctds.Columns["FAMILY_NAME"].ColumnName = "PRODUCT_NAME";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("SUBFAMILY_ID"))
                    {
                        distinctds.Columns["SUBFAMILY_ID"].ColumnName = "SUBPRODUCT_ID";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("SUBFAMILY_NAME"))
                    {
                        distinctds.Columns["SUBFAMILY_NAME"].ColumnName = "SUBPRODUCT_NAME";
                        distinctds.AcceptChanges();
                    }


                    if (distinctds.Columns.Contains("PRODUCT_PUBLISH2WEB"))
                    {
                        distinctds.Columns["PRODUCT_PUBLISH2WEB"].ColumnName = "ITEM_PUBLISH2WEB";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("PRODUCT_PUBLISH2PRINT"))
                    {
                        distinctds.Columns["PRODUCT_PUBLISH2PRINT"].ColumnName = "ITEM_PUBLISH2PRINT";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("PRODUCT_PUBLISH2PDF"))
                    {
                        distinctds.Columns["PRODUCT_PUBLISH2PDF"].ColumnName = "ITEM_PUBLISH2PDF";
                        distinctds.AcceptChanges();
                    }


                    var table = distinctds.DefaultView.ToTable(true);
                    // table.Columns.Add("sdfsaf");
                    // table.Columns.Add("sdfsaf1");
                    // table.Columns.Add("sdfsaf2");
                    context.Session["ExportTable"] = null;
                    context.Session["PDFCONTENT"] = null;
                    context.Session["KEYWORDS"] = null;
                    context.Session["Exemptwords"] = null;




                    if (table.Rows.Count > 0)
                    {
                        const string tableName1 = "Items";

                        string tableName = "Items";
                        if (System.Web.HttpContext.Current.Session["singlesheet"] == "YES")
                        {

                            tableName = "ProductItems";
                            System.Web.HttpContext.Current.Session["singlesheet"] = "";
                        }
                        else
                        {

                            tableName = "Items";
                        }

                        int rowcntTemp = table.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        do
                        {
                            int rowcnt;
                            if (rowcntTemp <= 65000)
                            {
                                rowcnt = rowcntTemp;
                                rowcntTemp = rowcnt - 65000;
                            }
                            else
                            {
                                rowcnt = 65000;
                                rowcntTemp = table.Rows.Count - (runningcnt + 65000);
                            }
                            j++;
                            if (j != 1)
                            {
                                tableName = tableName1 + (j - 1);
                            }
                            var worksheet = workbook.Worksheets.Add(tableName);
                            workbook.WindowOptions.SelectedWorksheet = worksheet;
                            for (int jj = 1; jj <= rowcnt; jj++)

                                // Create the worksheet to represent this data table


                                // Create column headers for each column
                                for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                                {
                                    if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "ITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                                    }
                                }

                            // Starting at row index 1, copy all data rows in
                            // the data table to the worksheet

                            int rowIndex = 2;
                            int temprunningcnt = runningcnt;
                            for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                            {
                                var row = worksheet.Rows[rowIndex++];
                                runningcnt++;
                                for (int columnIndex = 0; columnIndex < table.Rows[k].ItemArray.Length; columnIndex++)
                                {
                                    row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex];
                                }
                            }

                        }

                        while (rowcntTemp > 0);


                    }
                    else if (table.Rows.Count == 0)
                    {
                        var worksheet = workbook.Worksheets.Add("Items");
                        workbook.WindowOptions.SelectedWorksheet = worksheet;
                        // Create column headers for each column
                        for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                        {
                            worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                        }
                    }
                    else
                    {
                        var worksheet = workbook.Worksheets.Add("ProductItem");
                        workbook.WindowOptions.SelectedWorksheet = worksheet;

                        // Create column headers for each column
                        for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                        {
                            worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                        }
                        // context.Response.ContentType = "text/plain";
                        //context.Response.Write("No records found.");

                    }
                }

                if ((context.Session["ExportTableInDesign"] != null) && (context.Session["ExportTable"] == null))
                {
                    var distinctds = (DataTable)context.Session["ExportTableInDesign"];
                    if (!distinctds.Columns.Contains("Action"))
                    {
                        distinctds.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }
                    var table = distinctds.DefaultView.ToTable(true);
                    context.Session["ExportTableInDesign"] = null;
                    context.Session["PDFCONTENT"] = null;
                    context.Session["KEYWORDS"] = null;
                    context.Session["Exemptwords"] = null;
                    if (table.Rows.Count > 0)
                    {
                        const string tableName1 = "IndesignDetails";
                        string tableName = "IndesignDetails";
                        int rowcntTemp = table.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        do
                        {
                            int rowcnt;
                            if (rowcntTemp <= 65000)
                            {
                                rowcnt = rowcntTemp;
                                rowcntTemp = rowcnt - 65000;
                            }
                            else
                            {
                                rowcnt = 65000;
                                rowcntTemp = table.Rows.Count - (runningcnt + 65000);
                            }
                            j++;
                            if (j != 1)
                            {
                                tableName = tableName1 + (j - 1);
                            }
                            var worksheet = workbook.Worksheets.Add(tableName);
                            workbook.WindowOptions.SelectedWorksheet = worksheet;
                            for (int jj = 1; jj <= rowcnt; jj++)

                                // Create the worksheet to represent this data table
                                // Create column headers for each column
                                for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                                {
                                    if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "ITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                                    }
                                }

                            // Starting at row index 1, copy all data rows in
                            // the data table to the worksheet

                            int rowIndex = 2;
                            int temprunningcnt = runningcnt;
                            for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                            {
                                var row = worksheet.Rows[rowIndex++];
                                runningcnt++;
                                for (int columnIndex = 0; columnIndex < table.Rows[k].ItemArray.Length; columnIndex++)
                                {
                                    row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex];
                                }
                            }

                        }

                        while (rowcntTemp > 0);


                    }
                    else
                    {
                        var worksheet = workbook.Worksheets.Add("Item");
                        workbook.WindowOptions.SelectedWorksheet = worksheet;

                        // Create column headers for each column
                        for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                        {
                            worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                        }
                        // context.Response.ContentType = "text/plain";
                        //context.Response.Write("No records found.");
                    }
                }

                if ((context.Session["ExportTable"] == null) && (context.Session["ExportTableAttributeList"] != null))
                {
                    var distinctds = (DataTable)context.Session["ExportTableAttributeList"];
                    var table = distinctds.DefaultView.ToTable(true);
                    context.Session["ExportTableAttributeList"] = null;
                    context.Session["PDFCONTENT"] = null;
                    context.Session["KEYWORDS"] = null;
                    context.Session["Exemptwords"] = null;
                    if (table.Rows.Count > 0)
                    {
                        const string tableName1 = "AttributeList";
                        string tableName = "AttributeList";
                        int rowcntTemp = table.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        do
                        {
                            int rowcnt;
                            if (rowcntTemp <= 65000)
                            {
                                rowcnt = rowcntTemp;
                                rowcntTemp = rowcnt - 65000;
                            }
                            else
                            {
                                rowcnt = 65000;
                                rowcntTemp = table.Rows.Count - (runningcnt + 65000);
                            }
                            j++;
                            if (j != 1)
                            {
                                tableName = tableName1 + (j - 1);
                            }
                            var worksheet = workbook.Worksheets.Add(tableName);
                            workbook.WindowOptions.SelectedWorksheet = worksheet;
                            for (int jj = 1; jj <= rowcnt; jj++)

                                // Create the worksheet to represent this data table
                                // Create column headers for each column
                                for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                                {
                                    if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "ITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                                    }
                                }

                            // Starting at row index 1, copy all data rows in
                            // the data table to the worksheet

                            int rowIndex = 2;
                            int temprunningcnt = runningcnt;
                            for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                            {
                                var row = worksheet.Rows[rowIndex++];
                                runningcnt++;
                                for (int columnIndex = 0; columnIndex < table.Rows[k].ItemArray.Length; columnIndex++)
                                {
                                    row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex];
                                }
                            }
                        }
                        while (rowcntTemp > 0);
                    }
                    else
                    {
                        var worksheet = workbook.Worksheets.Add("Item");
                        workbook.WindowOptions.SelectedWorksheet = worksheet;

                        // Create column headers for each column
                        for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                        {
                            worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                        }
                        // context.Response.ContentType = "text/plain";
                        //context.Response.Write("No records found.");
                    }
                }

                if (EnableSubProduct == true)
                {
                    if (context.Session["ExportTableSubProduct"] != null)
                    {
                        var tableSP = (DataTable)context.Session["ExportTableSubProduct"];
                        if (!tableSP.Columns.Contains("Action"))
                        {
                            tableSP.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                        }
                        string tableNameSp = "SubItems";
                        const string tableNameSp1 = "SubItems";
                        int rowcntTempSP = tableSP.Rows.Count;
                        int jSP = 0;
                        int runningcntSP = 0;
                        do
                        {
                            int rowcntSP;
                            if (rowcntTempSP <= 65000)
                            {
                                rowcntSP = rowcntTempSP;
                                rowcntTempSP = rowcntSP - 65000;
                            }
                            else
                            {
                                rowcntSP = 65000;
                                rowcntTempSP = tableSP.Rows.Count - (runningcntSP + 65000);
                            }
                            jSP++;

                            if (jSP != 1)
                            {
                                tableNameSp = tableNameSp1 + (jSP - 1);
                            }
                            var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                            workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                            for (int jj = 1; jj <= rowcntSP; jj++)

                                // Create the worksheet to represent this data table


                                // Create column headers for each column
                                for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                                {
                                    if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                                    {
                                        worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                    {
                                        worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                    {


                                        worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);

                                    }
                                }

                            // Starting at row index 1, copy all data rows in
                            // the data table to the worksheet

                            int rowIndex = 2;
                            int temprunningcntSP = runningcntSP;
                            for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                            {
                                var row = worksheetSp.Rows[rowIndex++];
                                runningcntSP++;
                                for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                                {
                                    row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                                }
                            }
                        } while (rowcntTempSP > 0);

                    }
                }

                if (EnableTableDesigner == true)
                {
                    if (context.Session["ExportTableTableDesigner"] != null)
                    {
                        var tableSP = (DataTable)context.Session["ExportTableTableDesigner"];

                        if (!tableSP.Columns.Contains("Action"))
                        {
                            tableSP.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                        }

                        // New Name changes inside the Sheet column at TableDesigner  

                        if (tableSP.Columns.Contains("FAMILY_ID"))
                        {
                            tableSP.Columns["FAMILY_ID"].ColumnName = "PRODUCT_ID";
                            tableSP.AcceptChanges();
                        }

                        if (tableSP.Columns.Contains("FAMILY_NAME"))
                        {
                            tableSP.Columns["FAMILY_NAME"].ColumnName = "PRODUCT_NAME";
                            tableSP.AcceptChanges();
                        }

                        if (tableSP.Columns.Contains("SUBFAMILY_ID"))
                        {
                            tableSP.Columns["SUBFAMILY_ID"].ColumnName = "SUBPRODUCT_ID";
                            tableSP.AcceptChanges();
                        }

                        if (tableSP.Columns.Contains("SUBFAMILY_NAME"))
                        {
                            tableSP.Columns["SUBFAMILY_NAME"].ColumnName = "SUBPRODUCT_NAME";
                            tableSP.AcceptChanges();
                        }

                        if (tableSP.Columns.Contains("FAMILY_TABLE_STRUCTURE"))
                        {
                            tableSP.Columns["FAMILY_TABLE_STRUCTURE"].ColumnName = "PRODUCT_TABLE_STRUCTURE";
                            tableSP.AcceptChanges();
                        }
                        string tableNameSp = "TableDesigner";
                        const string tableNameSp1 = "TableDesigner";
                        int rowcntTempSP = tableSP.Rows.Count;
                        int jSP = 0;
                        int runningcntSP = 0;
                        do
                        {
                            int rowcntSP;
                            if (rowcntTempSP <= 65000)
                            {
                                rowcntSP = rowcntTempSP;
                                rowcntTempSP = rowcntSP - 65000;
                            }
                            else
                            {
                                rowcntSP = 65000;
                                rowcntTempSP = tableSP.Rows.Count - (runningcntSP + 65000);
                            }
                            jSP++;

                            if (jSP != 1)
                            {
                                tableNameSp = tableNameSp1 + (jSP - 1);
                            }
                            var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                            workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                            for (int jj = 1; jj <= rowcntSP; jj++)

                                // Create the worksheet to represent this data table


                                // Create column headers for each column
                                for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                                {
                                    if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                                    {
                                        worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                    {
                                        worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                    {


                                        worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);

                                    }
                                }

                            // Starting at row index 1, copy all data rows in
                            // the data table to the worksheet

                            int rowIndex = 2;
                            int temprunningcntSP = runningcntSP;
                            for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                            {
                                var row = worksheetSp.Rows[rowIndex++];
                                runningcntSP++;
                                for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                                {

                                    if (Convert.ToString(tableSP.Rows[k].ItemArray[columnIndex]).Length > 32700)
                                    {
                                        row.Cells[columnIndex].Value = "";
                                    }
                                    else
                                    {
                                        row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                                    }

                                }
                            }
                        } while (rowcntTempSP > 0);

                    }
                }
                if (context.Session["KEYWORDS"] != null)
                {
                    var tableSP = (DataTable)context.Session["KEYWORDS"];

                    if (!tableSP.Columns.Contains("Action"))
                    {
                        tableSP.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }
                    string tableNameSp = "KEYWORDS";
                    const string tableNameSp1 = "KEYWORDS";
                    int rowcntTempSP = tableSP.Rows.Count;
                    int jSP = 0;
                    int runningcntSP = 0;
                    do
                    {
                        int rowcntSP;
                        if (rowcntTempSP <= 65000)
                        {
                            rowcntSP = rowcntTempSP;
                            rowcntTempSP = rowcntSP - 65000;
                        }
                        else
                        {
                            rowcntSP = 65000;
                            rowcntTempSP = tableSP.Rows.Count - (runningcntSP + 65000);
                        }
                        jSP++;

                        if (jSP != 1)
                        {
                            tableNameSp = tableNameSp1 + (jSP - 1);
                        }
                        var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                        workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                        for (int jj = 1; jj <= rowcntSP; jj++)

                            // Create the worksheet to represent this data table


                            // Create column headers for each column
                            for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                            {
                                if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                                {
                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                {
                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                {


                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);

                                }
                            }

                        // Starting at row index 1, copy all data rows in
                        // the data table to the worksheet

                        int rowIndex = 2;
                        int temprunningcntSP = runningcntSP;
                        for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                        {
                            var row = worksheetSp.Rows[rowIndex++];
                            runningcntSP++;
                            for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                            {
                                row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                            }
                        }
                    } while (rowcntTempSP > 0);

                }

                if (context.Session["Exemptwords"] != null)
                {
                    var tableSP = (DataTable)context.Session["Exemptwords"];

                    if (!tableSP.Columns.Contains("Action"))
                    {
                        tableSP.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }
                    string tableNameSp = "Exemptwords";
                    const string tableNameSp1 = "Exemptwords";
                    int rowcntTempSP = tableSP.Rows.Count;
                    int jSP = 0;
                    int runningcntSP = 0;
                    do
                    {
                        int rowcntSP;
                        if (rowcntTempSP <= 65000)
                        {
                            rowcntSP = rowcntTempSP;
                            rowcntTempSP = rowcntSP - 65000;
                        }
                        else
                        {
                            rowcntSP = 65000;
                            rowcntTempSP = tableSP.Rows.Count - (runningcntSP + 65000);
                        }
                        jSP++;

                        if (jSP != 1)
                        {
                            tableNameSp = tableNameSp1 + (jSP - 1);
                        }
                        var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                        workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                        for (int jj = 1; jj <= rowcntSP; jj++)

                            // Create the worksheet to represent this data table


                            // Create column headers for each column
                            for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                            {
                                if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                                {
                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                {
                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                {


                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);

                                }
                            }

                        // Starting at row index 1, copy all data rows in
                        // the data table to the worksheet

                        int rowIndex = 2;
                        int temprunningcntSP = runningcntSP;
                        for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                        {
                            var row = worksheetSp.Rows[rowIndex++];
                            runningcntSP++;
                            for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                            {
                                row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                            }
                        }
                    } while (rowcntTempSP > 0);

                }
                if (workbook.Worksheets[0] != null)
                {
                    var worksheet1 = workbook.Worksheets[0];
                    workbook.WindowOptions.SelectedWorksheet = worksheet1;
                }

                workbook.Save(context.Server.MapPath("~/Content/" + filename));

                if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                {
                    response.ClearContent();
                    response.Clear();
                    response.ContentType = "application/vnd.ms-excel";
                    //response.AddHeader("Content-Disposition","attachment; filename=" + filename + ";");
                    response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", filename));
                    response.TransmitFile(context.Server.MapPath("~/Content/" + filename));
                    response.Flush();
                    response.End();
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("File not found!");
                }


            }
            else if (((context.Session["ExportTable"] != null) || (context.Session["KEYWORDS"] != null) || (context.Session["ExportTableAttributeList"] != null) || (context.Session["ExportTableInDesign"] != null) || (context.Session["Exemptwords"] != null)) && extn.ToLower() == ".xlsx")
            {
                Infragistics.Documents.Excel.Workbook workbook = new Infragistics.Documents.Excel.Workbook();
                workbook.SetCurrentFormat(Infragistics.Documents.Excel.WorkbookFormat.Excel2007);
                //var workbook = new Workbook();

                if (context.Session["ExportTableCAT"] != null)
                {
                    context.Session["Exemptwords"] = null;
                    var distinctdsCat = (DataTable)context.Session["ExportTableCAT"];

                    if (!distinctdsCat.Columns.Contains("Action"))
                    {
                        distinctdsCat.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }

                    var tableCat = distinctdsCat.DefaultView.ToTable(true);
                    context.Session["ExportTableCAT"] = null;
                    context.Session["PDFCONTENT"] = null;
                    context.Session["KEYWORDS"] = null;
                    context.Session["Exemptwords"] = null;
                    if (tableCat.Rows.Count > 0)
                    {
                        int catorder = 0;
                        int count = catorder;
                        const string tableName1 = "Categories";
                        string tableName = "Categories";

                        int rowcntTemp = tableCat.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        int rowcnt;
                        rowcnt = rowcntTemp;
                        var worksheet = workbook.Worksheets.Add(tableName);
                        workbook.WindowOptions.SelectedWorksheet = worksheet;
                        for (int jj = 1; jj <= rowcnt; jj++)
                            // Create the worksheet to represent this data table
                            for (int columnIndex = 0; columnIndex < tableCat.Columns.Count; columnIndex++)
                            {

                                if (HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName).Contains("SUBL"))
                                {
                                    string[] tokens = HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName).Split('-');

                                    worksheet.Rows[1].Cells[columnIndex].Value = tokens[1];
                                }
                                else if (HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableCat.Columns[columnIndex].ColumnName);
                                }
                            }


                        // Create column headers for each column

                        // Starting at row index 1, copy all data rows in
                        // the data table to the worksheet

                        int rowIndex = 2;
                        int temprunningcnt = runningcnt;
                        for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                        {
                            var row = worksheet.Rows[rowIndex++];
                            runningcnt++;
                            for (int columnIndex = 0; columnIndex < tableCat.Rows[k].ItemArray.Length; columnIndex++)
                            {
                                row.Cells[columnIndex].Value = tableCat.Rows[k].ItemArray[columnIndex];
                            }
                        }

                    }
                    else
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("No records found.");
                    }

                }





                if (context.Session["ExportTableFAM"] != null)
                {
                    var distinctdsFam = (DataTable)context.Session["ExportTableFAM"];

                    if (!distinctdsFam.Columns.Contains("Action"))
                    {
                        distinctdsFam.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }

                    // New Name changes inside the Sheet column Product(Family)


                    if (distinctdsFam.Columns.Contains("FAMILY_ID"))
                    {
                        distinctdsFam.Columns["FAMILY_ID"].ColumnName = "PRODUCT_ID";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("FAMILY_NAME"))
                    {
                        distinctdsFam.Columns["FAMILY_NAME"].ColumnName = "PRODUCT_NAME";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("SUBFAMILY_ID"))
                    {
                        distinctdsFam.Columns["SUBFAMILY_ID"].ColumnName = "SUBPRODUCT_ID";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("SUBFAMILY_NAME"))
                    {
                        distinctdsFam.Columns["SUBFAMILY_NAME"].ColumnName = "SUBPRODUCT_NAME";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("Product_PdfTemplate"))
                    {
                        distinctdsFam.Columns["Product_PdfTemplate"].ColumnName = "Item_PdfTemplate";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("Family_PdfTemplate"))
                    {
                        distinctdsFam.Columns["Family_PdfTemplate"].ColumnName = "Product_PdfTemplate";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("FAMILY_PUBLISH2PDF"))
                    {
                        distinctdsFam.Columns["FAMILY_PUBLISH2PDF"].ColumnName = "PRODUCT_PUBLISH2PDF";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("FAMILY_PUBLISH2PRINT"))
                    {
                        distinctdsFam.Columns["FAMILY_PUBLISH2PRINT"].ColumnName = "PRODUCT_PUBLISH2PRINT";
                        distinctdsFam.AcceptChanges();
                    }

                    if (distinctdsFam.Columns.Contains("FAMILY_PUBLISH2WEB"))
                    {
                        distinctdsFam.Columns["FAMILY_PUBLISH2WEB"].ColumnName = "PRODUCT_PUBLISH2WEB";
                        distinctdsFam.AcceptChanges();
                    }


                    var tableFam = distinctdsFam.DefaultView.ToTable(true);
                    // table.Columns.Add("sdfsaf");
                    // table.Columns.Add("sdfsaf1");
                    // table.Columns.Add("sdfsaf2");
                    context.Session["ExportTableFAM"] = null;
                    context.Session["PDFCONTENT"] = null;
                    context.Session["KEYWORDS"] = null;
                    context.Session["Exemptwords"] = null;
                    //var objExport = new RKLib.ExportData.Export();
                    if (tableFam.Rows.Count > 0)
                    {
                        const string tableName1 = "Products";
                        string tableName = "Products";

                        int rowcntTemp = tableFam.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        int rowcnt;
                        rowcnt = rowcntTemp;
                        var worksheet = workbook.Worksheets.Add(tableName);
                        workbook.WindowOptions.SelectedWorksheet = worksheet;
                        for (int jj = 1; jj <= rowcnt; jj++)

                            // Create the worksheet to represent this data table


                            // Create column headers for each column
                            for (int columnIndex = 0; columnIndex < tableFam.Columns.Count; columnIndex++)
                            {

                                if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "ITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName);
                                }
                            }




                        // Starting at row index 1, copy all data rows in
                        // the data table to the worksheet

                        int rowIndex = 2;
                        int temprunningcnt = runningcnt;
                        for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                        {
                            var row = worksheet.Rows[rowIndex++];
                            runningcnt++;
                            for (int columnIndex = 0; columnIndex < tableFam.Rows[k].ItemArray.Length; columnIndex++)
                            {
                                row.Cells[columnIndex].Value = tableFam.Rows[k].ItemArray[columnIndex];
                            }
                        }




                    }
                    else
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("No records found.");
                    }
                }


                if ((context.Session["ExportTable"] != null))
                {
                    var distinctds = (DataTable)context.Session["ExportTable"];


                    if (!distinctds.Columns.Contains("Action"))
                    {
                        distinctds.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }

                    // New Name changes inside the Sheet column Item (Product)

                    if (distinctds.Columns.Contains("PRODUCT_ID"))
                    {
                        distinctds.Columns["PRODUCT_ID"].ColumnName = "ITEM_ID";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("FAMILY_ID"))
                    {
                        distinctds.Columns["FAMILY_ID"].ColumnName = "PRODUCT_ID";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("FAMILY_NAME"))
                    {
                        distinctds.Columns["FAMILY_NAME"].ColumnName = "PRODUCT_NAME";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("SUBFAMILY_ID"))
                    {
                        distinctds.Columns["SUBFAMILY_ID"].ColumnName = "SUBPRODUCT_ID";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("SUBFAMILY_NAME"))
                    {
                        distinctds.Columns["SUBFAMILY_NAME"].ColumnName = "SUBPRODUCT_NAME";
                        distinctds.AcceptChanges();
                    }


                    if (distinctds.Columns.Contains("PRODUCT_PUBLISH2WEB"))
                    {
                        distinctds.Columns["PRODUCT_PUBLISH2WEB"].ColumnName = "ITEM_PUBLISH2WEB";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("PRODUCT_PUBLISH2PRINT"))
                    {
                        distinctds.Columns["PRODUCT_PUBLISH2PRINT"].ColumnName = "ITEM_PUBLISH2PRINT";
                        distinctds.AcceptChanges();
                    }

                    if (distinctds.Columns.Contains("PRODUCT_PUBLISH2PDF"))
                    {
                        distinctds.Columns["PRODUCT_PUBLISH2PDF"].ColumnName = "ITEM_PUBLISH2PDF";
                        distinctds.AcceptChanges();
                    }


                    var table = distinctds.DefaultView.ToTable(true);
                    // table.Columns.Add("sdfsaf");
                    // table.Columns.Add("sdfsaf1");
                    // table.Columns.Add("sdfsaf2");
                    context.Session["ExportTable"] = null;
                    context.Session["PDFCONTENT"] = null;
                    context.Session["KEYWORDS"] = null;
                    context.Session["Exemptwords"] = null;
                    if (table.Rows.Count > 0)
                    {
                        const string tableName1 = "Items";
                        string tableName = "Items";

                        int rowcntTemp = table.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        int rowcnt;
                        rowcnt = rowcntTemp;

                        var worksheet = workbook.Worksheets.Add(tableName);
                        workbook.WindowOptions.SelectedWorksheet = worksheet;
                        for (int jj = 1; jj <= rowcnt; jj++)

                            // Create the worksheet to represent this data table


                            // Create column headers for each column
                            for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                            {
                                if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "ITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                                }
                            }

                        // Starting at row index 1, copy all data rows in
                        // the data table to the worksheet

                        int rowIndex = 2;
                        int temprunningcnt = runningcnt;
                        for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                        {
                            var row = worksheet.Rows[rowIndex++];
                            runningcnt++;
                            for (int columnIndex = 0; columnIndex < table.Rows[k].ItemArray.Length; columnIndex++)
                            {
                                row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex];
                            }
                        }



                    }
                    else
                    {
                        var worksheet = workbook.Worksheets.Add("Item");
                        workbook.WindowOptions.SelectedWorksheet = worksheet;

                        // Create column headers for each column
                        for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                        {
                            worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                        }
                        // context.Response.ContentType = "text/plain";
                        //context.Response.Write("No records found.");

                    }
                }

                if ((context.Session["ExportTableInDesign"] != null) && (context.Session["ExportTable"] == null))
                {
                    var distinctds = (DataTable)context.Session["ExportTableInDesign"];
                    if (!distinctds.Columns.Contains("Action"))
                    {
                        distinctds.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }
                    var table = distinctds.DefaultView.ToTable(true);
                    context.Session["ExportTableInDesign"] = null;
                    context.Session["PDFCONTENT"] = null;
                    context.Session["KEYWORDS"] = null;
                    context.Session["Exemptwords"] = null;
                    if (table.Rows.Count > 0)
                    {
                        const string tableName1 = "IndesignDetails";
                        string tableName = "IndesignDetails";
                        int rowcntTemp = table.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        int rowcnt;
                        rowcnt = rowcntTemp;

                        var worksheet = workbook.Worksheets.Add(tableName);
                        workbook.WindowOptions.SelectedWorksheet = worksheet;
                        for (int jj = 1; jj <= rowcnt; jj++)

                            // Create the worksheet to represent this data table
                            // Create column headers for each column
                            for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                            {
                                if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "ITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                                }
                            }

                        // Starting at row index 1, copy all data rows in
                        // the data table to the worksheet

                        int rowIndex = 2;
                        int temprunningcnt = runningcnt;
                        for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                        {
                            var row = worksheet.Rows[rowIndex++];
                            runningcnt++;
                            for (int columnIndex = 0; columnIndex < table.Rows[k].ItemArray.Length; columnIndex++)
                            {
                                row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex];
                            }
                        }

                    }
                    else
                    {
                        var worksheet = workbook.Worksheets.Add("Item");
                        workbook.WindowOptions.SelectedWorksheet = worksheet;

                        // Create column headers for each column
                        for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                        {
                            worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                        }
                        // context.Response.ContentType = "text/plain";
                        //context.Response.Write("No records found.");
                    }
                }

                if ((context.Session["ExportTable"] == null) && (context.Session["ExportTableAttributeList"] != null))
                {
                    var distinctds = (DataTable)context.Session["ExportTableAttributeList"];
                    var table = distinctds.DefaultView.ToTable(true);
                    context.Session["ExportTableAttributeList"] = null;
                    context.Session["PDFCONTENT"] = null;
                    context.Session["KEYWORDS"] = null;
                    context.Session["Exemptwords"] = null;
                    if (table.Rows.Count > 0)
                    {
                        const string tableName1 = "AttributeList";
                        string tableName = "AttributeList";
                        int rowcntTemp = table.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        int rowcnt;
                        rowcnt = rowcntTemp;

                        var worksheet = workbook.Worksheets.Add(tableName);
                        workbook.WindowOptions.SelectedWorksheet = worksheet;
                        for (int jj = 1; jj <= rowcnt; jj++)

                            // Create the worksheet to represent this data table
                            // Create column headers for each column
                            for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                            {
                                if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "ITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                                }
                            }

                        // Starting at row index 1, copy all data rows in
                        // the data table to the worksheet

                        int rowIndex = 2;
                        int temprunningcnt = runningcnt;
                        for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                        {
                            var row = worksheet.Rows[rowIndex++];
                            runningcnt++;
                            for (int columnIndex = 0; columnIndex < table.Rows[k].ItemArray.Length; columnIndex++)
                            {
                                row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex];
                            }
                        }

                    }
                    else
                    {
                        var worksheet = workbook.Worksheets.Add("Item");
                        workbook.WindowOptions.SelectedWorksheet = worksheet;

                        // Create column headers for each column
                        for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                        {
                            worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                        }
                        // context.Response.ContentType = "text/plain";
                        //context.Response.Write("No records found.");
                    }
                }

                if (EnableSubProduct == true)
                {
                    if (context.Session["ExportTableSubProduct"] != null)
                    {
                        var tableSP = (DataTable)context.Session["ExportTableSubProduct"];
                        if (!tableSP.Columns.Contains("Action"))
                        {
                            tableSP.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                        }
                        string tableNameSp = "SubItems";
                        const string tableNameSp1 = "SubItems";
                        int rowcntTempSP = tableSP.Rows.Count;
                        int jSP = 0;
                        int runningcntSP = 0;
                        int rowcntSP;
                        rowcntSP = rowcntTempSP;
                        var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                        workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                        for (int jj = 1; jj <= rowcntSP; jj++)

                            // Create the worksheet to represent this data table


                            // Create column headers for each column
                            for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                            {
                                if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                                {
                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                {
                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                {


                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);

                                }
                            }

                        // Starting at row index 1, copy all data rows in
                        // the data table to the worksheet

                        int rowIndex = 2;
                        int temprunningcntSP = runningcntSP;
                        for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                        {
                            var row = worksheetSp.Rows[rowIndex++];
                            runningcntSP++;
                            for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                            {
                                row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                            }
                        }


                    }
                }

                if (EnableTableDesigner == true)
                {
                    if (context.Session["ExportTableTableDesigner"] != null)
                    {
                        var tableSP = (DataTable)context.Session["ExportTableTableDesigner"];

                        if (!tableSP.Columns.Contains("Action"))
                        {
                            tableSP.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                        }

                        // New Name changes inside the Sheet column at TableDesigner 

                        if (tableSP.Columns.Contains("FAMILY_ID"))
                        {
                            tableSP.Columns["FAMILY_ID"].ColumnName = "PRODUCT_ID";
                            tableSP.AcceptChanges();
                        }

                        if (tableSP.Columns.Contains("FAMILY_NAME"))
                        {
                            tableSP.Columns["FAMILY_NAME"].ColumnName = "PRODUCT_NAME";
                            tableSP.AcceptChanges();
                        }

                        if (tableSP.Columns.Contains("SUBFAMILY_ID"))
                        {
                            tableSP.Columns["SUBFAMILY_ID"].ColumnName = "SUBPRODUCT_ID";
                            tableSP.AcceptChanges();
                        }

                        if (tableSP.Columns.Contains("SUBFAMILY_NAME"))
                        {
                            tableSP.Columns["SUBFAMILY_NAME"].ColumnName = "SUBPRODUCT_NAME";
                            tableSP.AcceptChanges();
                        }

                        if (tableSP.Columns.Contains("FAMILY_TABLE_STRUCTURE"))
                        {
                            tableSP.Columns["FAMILY_TABLE_STRUCTURE"].ColumnName = "PRODUCT_TABLE_STRUCTURE";
                            tableSP.AcceptChanges();
                        }

                        string tableNameSp = "TableDesigner";
                        const string tableNameSp1 = "TableDesigner";
                        int rowcntTempSP = tableSP.Rows.Count;
                        int jSP = 0;
                        int runningcntSP = 0;
                        int rowcntSP;
                        rowcntSP = rowcntTempSP;

                        var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                        workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                        for (int jj = 1; jj <= rowcntSP; jj++)

                            // Create the worksheet to represent this data table


                            // Create column headers for each column
                            for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                            {
                                if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                                {
                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                {
                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                {


                                    worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);

                                }
                            }

                        // Starting at row index 1, copy all data rows in
                        // the data table to the worksheet

                        int rowIndex = 2;
                        int temprunningcntSP = runningcntSP;
                        for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                        {
                            var row = worksheetSp.Rows[rowIndex++];
                            runningcntSP++;
                            for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                            {

                                if (Convert.ToString(tableSP.Rows[k].ItemArray[columnIndex]).Length > 32700)
                                {
                                    row.Cells[columnIndex].Value = "";
                                }
                                else
                                {
                                    row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                                }

                            }
                        }


                    }
                }
                if (context.Session["KEYWORDS"] != null)
                {
                    var tableSP = (DataTable)context.Session["KEYWORDS"];

                    if (!tableSP.Columns.Contains("Action"))
                    {
                        tableSP.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }
                    string tableNameSp = "KEYWORDS";
                    const string tableNameSp1 = "KEYWORDS";
                    int rowcntTempSP = tableSP.Rows.Count;
                    int jSP = 0;
                    int runningcntSP = 0;
                    int rowcntSP;
                    rowcntSP = rowcntTempSP;

                    var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                    workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                    for (int jj = 1; jj <= rowcntSP; jj++)

                        // Create the worksheet to represent this data table


                        // Create column headers for each column
                        for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                        {
                            if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                            {
                                worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                            }
                            else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                            {
                                worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                            }
                            else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                            {


                                worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);

                            }
                        }

                    // Starting at row index 1, copy all data rows in
                    // the data table to the worksheet

                    int rowIndex = 2;
                    int temprunningcntSP = runningcntSP;
                    for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                    {
                        var row = worksheetSp.Rows[rowIndex++];
                        runningcntSP++;
                        for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                        {
                            row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                        }
                    }

                }

                if (context.Session["Exemptwords"] != null)
                {
                    var tableSP = (DataTable)context.Session["Exemptwords"];

                    if (!tableSP.Columns.Contains("Action"))
                    {
                        tableSP.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }
                    string tableNameSp = "Exemptwords";
                    const string tableNameSp1 = "Exemptwords";
                    int rowcntTempSP = tableSP.Rows.Count;
                    int jSP = 0;
                    int runningcntSP = 0;
                    int rowcntSP;
                    rowcntSP = rowcntTempSP;

                    var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                    workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                    for (int jj = 1; jj <= rowcntSP; jj++)

                        // Create the worksheet to represent this data table


                        // Create column headers for each column
                        for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                        {
                            if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                            {
                                worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                            }
                            else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                            {
                                worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                            }
                            else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                            {


                                worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);

                            }
                        }

                    // Starting at row index 1, copy all data rows in
                    // the data table to the worksheet

                    int rowIndex = 2;
                    int temprunningcntSP = runningcntSP;
                    for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                    {
                        var row = worksheetSp.Rows[rowIndex++];
                        runningcntSP++;
                        for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                        {
                            row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                        }
                    }


                }
                if (workbook.Worksheets[0] != null)
                {
                    var worksheet1 = workbook.Worksheets[0];
                    workbook.WindowOptions.SelectedWorksheet = worksheet1;
                }

                workbook.Save(context.Server.MapPath("~/Content/" + filename));

                if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                {
                    response.ClearContent();
                    response.Clear();
                    response.ContentType = "application/vnd.ms-excel";
                    //response.AddHeader("Content-Disposition","attachment; filename=" + filename + ";");
                    response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", filename));
                    response.TransmitFile(context.Server.MapPath("~/Content/" + filename));
                    response.Flush();
                    response.End();
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("File not found!");
                }


            }
            else if (filename.ToLower().Contains(".txt") || filename.ToLower().Contains(".csv") || filename.ToLower().Contains(".xml"))
            {
                string subproductcheck = string.Empty;
                string file = Path.GetFileNameWithoutExtension(filename);
                if (Type == "Items")
                {
                    filename = file + "_items.csv";
                }
                else if (Type == "Products")
                {
                    filename = file + "_products.csv";
                }
                else if (Type == "categories")
                {
                    filename = file + "_categories.csv";
                }
                else if (Type == "tabledesigner")
                {
                    filename = file + "_tabledesigner.csv";
                }

                if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                {
                    System.IO.FileStream fs = null;
                    fs = System.IO.File.Open(HttpContext.Current.Server.MapPath("Content/" + filename), System.IO.FileMode.Open);
                    byte[] btFile = new byte[fs.Length];
                    fs.Read(btFile, 0, Convert.ToInt32(fs.Length));
                    fs.Close();
                    response.AddHeader("Content-disposition", "attachment; filename=" + filename);
                    response.ContentType = "application/octet-stream";
                    response.BinaryWrite(btFile);
                    File.Delete(HttpContext.Current.Server.MapPath("Content/" + filename));
                    response.End();

                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("No results found!");
                }
                filename = "Export_items.csv";
                if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                {
                    // format = filepath + "_subproduct" + ".txt";

                    System.IO.FileStream fs = null;
                    fs = System.IO.File.Open(HttpContext.Current.Server.MapPath("Content/" +
                             filename), System.IO.FileMode.Open);
                    byte[] btFile = new byte[fs.Length];
                    fs.Read(btFile, 0, Convert.ToInt32(fs.Length));
                    fs.Close();
                    response.AddHeader("Content-disposition", "attachment; filename=" +
                                       filename);
                    response.ContentType = "application/octet-stream";
                    response.BinaryWrite(btFile);
                    response.End();

                }
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("Error occurred during export!.");
            }


        }
        public void ProcessRequest1(HttpContext context)
        {

            //CustomerItemNo
            if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
            {

                System.Web.HttpContext.Current.Session["CustomerItemNo"] = homeObj.getCatalogItemNumberValues();
            }
            // CustomerSubItemNo
            if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
            {
                System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = homeObj.getCatalogSubItemNumberValues();
            }



            //context.Response.ContentType = "text/plain";
            HttpResponse response = context.Response;
            string filename = HttpContext.Current.Request.QueryString["Path"];
            response.ClearContent();
            response.Clear();
            response.ContentType = "text/xls";
            response.AddHeader("Content-Disposition",
                               "attachment; filename=" + filename + ";");
            response.TransmitFile(context.Server.MapPath("~/Content/" + filename));
            response.Flush();
            response.End();



            //if (context.Session["ExportTable"] != null)
            //{
            //    var table = (DataTable)context.Session["ExportTable"];
            //    // table.Columns.Add("sdfsaf");
            //    // table.Columns.Add("sdfsaf1");
            //    // table.Columns.Add("sdfsaf2");
            //    context.Session["ExportTable"] = null;
            //   //  var objExport = new RKLib.ExportData.Export();
            //    if (table != null && table.Rows.Count > 0)
            //    {
            //      //  objExport.ExportDetails(table, RKLib.ExportData.Export.ExportFormat.Excel, filename);
            //        //context.Response.ClearContent();
            //        //context.Response.Buffer = true;
            //        //context.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Customers.xls"));
            //        //context.Response.ContentType = "application/ms-excel";
            //        //DataTable dt = table;
            //        //string str = string.Empty;
            //        //foreach (DataColumn dtcol in dt.Columns)
            //        //{
            //        //    context.Response.Write(str + dtcol.ColumnName);
            //        //    str = "\t";
            //        //}
            //        //context.Response.Write("\n");
            //        //foreach (DataRow dr in dt.Rows)
            //        //{
            //        //    str = "";
            //        //    for (int j = 0; j < dt.Columns.Count; j++)
            //        //    {
            //        //        context.Response.Write(str + System.Convert.ToString(dr[j]));
            //        //        str = "\t";
            //        //    }
            //        //    context.Response.Write("\n");
            //        //}
            //        //context.Response.End();
            //    }

            //}
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
