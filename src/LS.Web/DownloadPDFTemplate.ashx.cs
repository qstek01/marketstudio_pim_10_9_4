﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using LS.Data.Model;
using LS.Data;
using System.Web.Configuration;
namespace LS.Web
{
    /// <summary>
    /// Summary description for DownloadPDFTemplate
    /// </summary>
    public class DownloadPDFTemplate : IHttpHandler
    {
        private CSEntities _dbcontext = new CSEntities();
        public void ProcessRequest(HttpContext context)
        {
            HttpResponse response = context.Response;
            string userName = HttpContext.Current.Request.QueryString["UserName"];
            string filename = HttpContext.Current.Request.QueryString["FileName"];
            string sourcePath = HttpContext.Current.Request.QueryString["Path"];



            // Separate path --- Start
            var user_Name = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == userName).Select(y => y.c.Comments).FirstOrDefault().ToString();


            // To pass dynamic path location from web config   - Start
            string IsServerUpload = WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
            string pathDesignation = WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
            string pathUrl = WebConfigurationManager.AppSettings["ExternalAssetDriveURL"];

            string path = string.Empty;
            string destinationPath;


            string filePath = string.Empty;



            
            
            
            
            // Seprate Path -- End

         
            if (sourcePath == "Simple")
            {
                filePath = "New Simple Template";
            }
            else if (sourcePath == "Hierarchical")
            {
                filePath = "New Hierarchical Template";
            }
            else if (sourcePath == "Design")
            {
                filePath = "Newly Design Template";
            }
            else if (sourcePath == "Open")
            {
                filePath = "Opened Template";
            }
            else if (sourcePath == "OpenCatalog")
            {
                filePath = "OpenCatalog";
            }
            else if(sourcePath == "Download")
            {
                filePath = "RunTemplates";
            }
            else if (sourcePath == "PDF")
            {
                if (File.Exists(context.Server.MapPath("~/ExportPDF/" + filename)))
                {
                    response.ClearContent();
                    response.Clear();
                    response.ContentType = "application/unknown";
                    response.AddHeader("Content-Disposition",
                                       "attachment; filename=" + filename + ";");
                    response.TransmitFile(context.Server.MapPath("~/ExportPDF/" + filename));
                    response.Flush();
                    response.End();
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("Template File not found!");
                }
            }



            if (IsServerUpload.ToUpper() == "TRUE")
            {
                destinationPath = pathDesignation + "\\Content\\ProductImages\\" + user_Name + "\\Customer Templates\\" + filePath + "\\" + filename;

                destinationPath = destinationPath.Replace("////", "\\\\");

                destinationPath = destinationPath.Replace("/", "\\"); 
            }
            else
            {
                destinationPath = context.Server.MapPath("~/Content/ProductImages/" + user_Name + "/Customer Templates/" + filePath + "/" + filename);
            }

            //  To pass dynamic path location from web config  -  End.



            if (File.Exists(destinationPath))
            {
                response.ClearContent();
                response.Clear();
                response.ContentType = "text/plain";
                response.AddHeader("Content-Disposition",
                                   "attachment; filename=" + filename + ";");
                response.TransmitFile(destinationPath);
                response.Flush();
                response.End();
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("Template File not found!");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}