﻿/// <reference path="../ShoppingCart/ShoppingCart.cshtml" />
LSApp.controller('dashboardController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', 'ngDialog', '$rootScope', 'productService', '$localStorage', 'blockUI', '$sce',
function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, ngDialog, $rootScope, productService, $localStorage, blockUI, $sce) {
    //----------------------------------------------------------start user rights ------------------------------------------------------------------------------------------------    
    $scope.resouceTypeId = '';
    $rootScope.parentCategory = "Home";
    var breadcrumbOption = '';
    $("#usrManage").hide();
    $("#grpManage").hide();
    $scope.IsProduct = false;
    $scope.IsFirstTime = false;
    $scope.contentPrintEnable = false;
    $scope.IsSplitDesign = false;
    $localStorage.parentCategory = 'Home';
    $scope.thumbnailLink = '';
    $rootScope.userRegFirstName = "";
    //----------------------------------------------Declarations for file type identification-------------------------------------------------------------------//
    var pdfArray = ['pdf'];
    var imageArray = ['jpg', 'eps', 'png'];
    var cadArray = ['dwg', 'dxf'];
    var videoArray = ['avi', 'mov', 'mp4', 'wmv'];
    //----------------------------------------------Declarations for file type identification-------------------------------------------------------------------//
    //----------------------------------------------Declarations for new back navigation process-------------------------------------------------------------------//
    //$rootScope.editedUserDetails = [];
    //$rootScope.dataModified = [];
    //$rootScope.IsEdit = false;
    //$rootScope.isUserStatusChanged = false;
    //----------------------------------------------Declarations for new back navigation process-------------------------------------------------------------------//
    var searchData = [];
    var cartData = [];
    if ($localStorage.backNavigationList == undefined && $localStorage.backFlag == undefined) {
        $localStorage.backNavigationList = [];
        $localStorage.backFlag = false;
        $rootScope.backButtonShow = false;
    }
    else {
        if ($localStorage.backNavigationList.length > 1)
            $rootScope.backButtonShow = true;
    }

    $rootScope.GetCategoryList = function () {
        if (!$localStorage.backFlag) {
            if ($localStorage.backNavigationList.length == 0) {
                $localStorage.backNavigationList.push({ PAGE: 'ROOT_CAT', URL: '/', FUNCTION: 'GetCategoryList', PARAMETER1: '', PARAMETER2: '', PARAMETER3: $localStorage.parentCategory });
            }
        }
        else {
            $localStorage.backFlag = false;
        }
        $rootScope.parentCategory = "Home";
        $scope.IsProduct = false;
        var option = 'CATEGORY';
        $scope.resouceTypeId = '';
        $scope.categoryId = '0';
        $rootScope.leftCategoryList = [];
        $scope.breadCrumbData = [];
        $rootScope.mainResourceCategoryList = [];
        dataFactory.getCategoryFamilyList($scope.categoryId, option, $scope.resouceTypeId, 0, $localStorage.userGroupId, $localStorage.UserId).success(function (response) {
            if (response != null) {
                $rootScope.leftCategoryList = $.grep(response.Table, function (v) {
                    return v.PARENT_CATEGORY === '0';
                });
                $rootScope.leftSubCategoryList = $.grep(response.Table, function (v) {
                    return v.PARENT_CATEGORY !== '0';
                });
                $rootScope.mainCategoryList = $rootScope.leftCategoryList;
            }
        });
        $scope.IsSplitDesign = false;
    }
    $scope.IdentifyResource = function (list, id) {
        $scope.identifyList = $.grep(list, function (v) {
            return v.CATEGORY_ID === id;
        });
        return $scope.identifyList;
    }
    $scope.BreadCrumbData = function (categoryId, familyId, productId, breadcrumbOption) {
        dataFactory.getBreadCrumbList(categoryId, familyId, productId, breadcrumbOption).success(function (response) {
            $scope.breadCrumbData = response;
        });

    }

    $rootScope.CategoryNavigation = function (category, e) {
        //
        $rootScope.mainCategoryList = [];
        $rootScope.mainResourceCategoryList = [];
        $rootScope.parentCategory = $sce.trustAsHtml(category.CATEGORY_NAME);
        $localStorage.parentCategory = category.CATEGORY_NAME;
        if (!$localStorage.backFlag) {
            if ($localStorage.backNavigationList.length > 0) {
                var lastVal = $localStorage.backNavigationList[$localStorage.backNavigationList.length - 1];
                var lastRootCheck = lastVal.PARAMETER1.CATEGORY_ID;
                if (lastRootCheck != category.CATEGORY_ID) {
                    $localStorage.backNavigationList.push({ PAGE: 'CAT_NAV', URL: '/', FUNCTION: 'CategoryNavigation', PARAMETER1: category, PARAMETER2: $localStorage.leftNavData, PARAMETER3: $localStorage.parentCategory });
                }
            }
            else {
                $localStorage.backNavigationList.push({ PAGE: 'CAT_NAV', URL: '/', FUNCTION: 'CategoryNavigation', PARAMETER1: category, PARAMETER2: $localStorage.leftNavData, PARAMETER3: $localStorage.parentCategory });
            }
        }
        else {
            $localStorage.backFlag = false;
            localStorage.setItem("backFlag", false);
        }
        $scope.IsProduct = false;
        var option = 'CATEGORYFAMILY';

        if (category.LEVEL == 3) {
            $scope.resouceTypeId = category.CATEGORY_ID;
            $scope.resouceFlag = category.RESOURCE;
            $scope.resouceName = category.CATEGORY_NAME;
        }
        else if (category.LEVEL == 3) {
            $scope.resouceTypeId = "";
            $scope.rootCategory = category.CATEGORY_ID;
        }
        if (category.LEVEL == 1) {
            option = 'RESOURCES_PRODUCT_CATEGORY';
            //  $scope.resouceTypeId = "";
            $scope.rootCategory = category.CATEGORY_ID;
            $scope.resouceName = "";
            $scope.IsSplitDesign = true;
            //  option = 'CATEGORY';
        }
        else {
            $scope.IsSplitDesign = false;
        }
        dataFactory.getCategoryFamilyList(category.CATEGORY_ID, option, $scope.resouceTypeId, 0, $localStorage.userGroupId, $localStorage.UserId).success(function (response) {
            if (response != null) {

                if (category.TYPE == 'C')
                    breadcrumbOption = 'CATEGORY_NAVIGATION';
                var mainList = [];
                mainList = $.grep(response.Table, function (v) {
                    return (v.RESOURCE.toLowerCase() == 'product' || v.RESOURCE.toLowerCase() == 'products');
                });

                $rootScope.backButtonShow = true;
                //$timeout(function () {
                $rootScope.mainCategoryList = mainList;
                //}, 100);
                $rootScope.mainResourceCategoryList = $.grep(response.Table, function (v) {
                    return (v.RESOURCE.toLowerCase() == 'content' || v.RESOURCE.toLowerCase() == 'contents' || v.RESOURCE.toLowerCase() == 'resource' || v.RESOURCE.toLowerCase() == 'resources');
                });
                $timeout(function () {
                    $scope.BreadCrumbData(category.CATEGORY_ID, 0, 0, breadcrumbOption);

                }, 200);
            }

        }
        );
        if (e != undefined)
            e.stopPropagation();
    }

    $rootScope.FamilyNavigation = function (category, e) {
        var option = 'FAMILYPRODUCT';
        $scope.IsProduct = false;
        $scope.IsSplitDesign = false;
        $rootScope.parentCategory = $sce.trustAsHtml(category.CATEGORY_NAME);

        if (!$localStorage.backFlag) {
            if ($localStorage.backNavigationList.length > 0) {
                var lastVal = $localStorage.backNavigationList[$localStorage.backNavigationList.length - 1];
                var lastRootCheck = lastVal.PARAMETER1.CATEGORY_ID;
                if (lastRootCheck != category.CATEGORY_ID) {
                    $localStorage.backNavigationList.push({ PAGE: 'FAM_NAV', URL: '/', FUNCTION: 'FamilyNavigation', PARAMETER1: category, PARAMETER2: $localStorage.leftNavData, PARAMETER3: $localStorage.parentCategory });
                }
            }
            else {
                $localStorage.backNavigationList.push({ PAGE: 'FAM_NAV', URL: '/', FUNCTION: 'FamilyNavigation', PARAMETER1: category, PARAMETER2: $localStorage.leftNavData, PARAMETER3: $localStorage.parentCategory });
            }
        }
        else {
            $localStorage.backFlag = false;
            localStorage.setItem("backFlag", false);
        }
        dataFactory.getCategoryFamilyList(category.PARENT_CATEGORY, option, $scope.resouceTypeId, category.CATEGORY_ID, $localStorage.userGroupId, $localStorage.UserId).success(function (response) {
            if (response != null) {
                if (category.TYPE == 'F')
                    breadcrumbOption = 'FAMILY_NAVIGATION';
                var mainList = [];
                mainList = $.grep(response.Table, function (v) {
                    return (v.RESOURCE.toLowerCase() === 'product' || v.RESOURCE.toLowerCase() === 'products');
                });
                $rootScope.mainCategoryList = mainList;
                var resourceList = [];
                resourceList = $.grep(response.Table, function (v) {
                    return (v.RESOURCE.toLowerCase() == 'content' || v.RESOURCE.toLowerCase() == 'contents' || v.RESOURCE.toLowerCase() == 'resource' || v.RESOURCE.toLowerCase() == 'resources');
                });
                $rootScope.mainResourceCategoryList = resourceList;
                $scope.BreadCrumbData(category.PARENT_CATEGORY, category.CATEGORY_ID, 0, breadcrumbOption);
                $rootScope.backButtonShow = true;
            }

        });
        if (e != undefined)
            e.stopPropagation();
    }

    $rootScope.BreadCrumbClik = function (data, e) {
        if (data.TYPE == 'C') {
            $rootScope.CategoryNavigation(data, e);
        }
        else if (data.TYPE == 'F') {
            $rootScope.FamilyNavigation(data, e);
        }
        else if (data.TYPE == 'P') {
            var list = [({ PARENT_CATEGORY: $scope.resouceTypeId, CATEGORY_ID: data.PARENT_CATEGORY, FAMILY_ID: data.CATEGORY_ID })];
            $scope.ProductNavigation(list[0], e);
        }
    }

    $rootScope.leftMenuNavigation = function (data, event) {
        if (data.TYPE == 'C') {
            $rootScope.CategoryNavigation(data, event);
        }
        else if (data.TYPE == 'F') {
            $rootScope.FamilyNavigation(data, event);
        }
    }
    $scope.downloadAllEnable = false;
    $scope.downloadJpgEnable = false;
    $scope.downloadPsdEnable = false;
    $scope.downloadEnableCurrent = false;
    $scope.downloadJpgEnableCurrent = false;
    $scope.downloadPsdEnableCurrent = false;
    $scope.ProductNavigation = function (data) {
        //
        $scope.IsProduct = true;
        $scope.imageData = "";
        breadcrumbOption = 'PRODUCT_NAVIGATION';
        $scope.BreadCrumbData(data.PARENT_CATEGORY, data.CATEGORY_ID, data.FAMILY_ID, breadcrumbOption);
        if (!$localStorage.backFlag) {
            if ($localStorage.backNavigationList.length > 0) {
                var lastVal = $localStorage.backNavigationList[$localStorage.backNavigationList.length - 1];
                var lastRootCheck = lastVal.PARAMETER1.FAMILY_ID;
                if (lastRootCheck != data.FAMILY_ID) {
                    $localStorage.backNavigationList.push({ PAGE: 'PROD_NAV', URL: '/', FUNCTION: 'ProductNavigation', PARAMETER1: data, PARAMETER2: $localStorage.leftNavData, PARAMETER3: $localStorage.parentCategory });
                }
            }
            else {
                $localStorage.backNavigationList.push({ PAGE: 'PROD_NAV', URL: '/', FUNCTION: 'ProductNavigation', PARAMETER1: data, PARAMETER2: $localStorage.leftNavData, PARAMETER3: $localStorage.parentCategory });
            }
        }
        else {
            $localStorage.backFlag = false;
            localStorage.setItem("backFlag", false);
        }
        dataFactory.getProductDetails(data.PARENT_CATEGORY, data.CATEGORY_ID, data.FAMILY_ID, $localStorage.UserId).success(function (response) {
            if (response != null) {

                $scope.productShortList = response.Table;

                $rootScope.partNumber = $.grep(response.Table, function (v) {
                    return v.ATTRIBUTE_ID === 1;
                });
                if ($rootScope.partNumber.length > 0)
                    $scope.productResourceList = response.Table1;
                //if ($scope.productResourceList.length > 0)
                //    $scope.ResourceThumbnailClick($scope.productResourceList);
                $scope.productImageList = response.Table2;
                //  $scope.ResourceThumbnailClick($scope.productResourceList)
                $rootScope.partNumber[0].STRING_VALUE = $rootScope.partNumber[0].STRING_VALUE.replace(':', '')
                $scope.productDraftDetails = response.Table3;
                if ($scope.productDraftDetails.length > 0) {
                    $scope.DraftProductFlag = $scope.productDraftDetails[0].IS_DRAFT;
                }
                else {
                    $scope.DraftProductFlag = 0;
                }
                $scope.ProductAnalyticsEntry($rootScope.partNumber[0].STRING_VALUE);
                if ($scope.productImageList.length > 0) {
                    angular.forEach($scope.productImageList, function (listVal) {
                        if (listVal.HIGH_RESOLUTION != null && listVal.HIGH_RESOLUTION != undefined && listVal.HIGH_RESOLUTION != '' && listVal.HIGH_RESOLUTION.toLowerCase().indexOf('noimage') <= -1) {
                            $scope.downloadAllEnable = true;
                            $scope.downloadJpgEnable = true;
                        }
                        if (listVal.HIGH_RESOLUTION_PSD != null && listVal.HIGH_RESOLUTION_PSD != undefined && listVal.HIGH_RESOLUTION_PSD != '' && listVal.HIGH_RESOLUTION_PSD.toLowerCase().indexOf('noimage') <= -1) {
                            $scope.downloadAllEnable = true;
                            $scope.downloadPsdEnable = true;
                        }
                    })
                    $scope.imageData = $scope.productImageList[0].STRING_VALUE;
                    $scope.imageDataHiRes = $scope.productImageList[0].HIGH_RESOLUTION;
                    if ($scope.imageDataHiRes != null && $scope.imageDataHiRes != undefined && $scope.imageDataHiRes != '' && $scope.imageDataHiRes.toLowerCase().indexOf('noimage') <= -1) {
                        $scope.downloadEnableCurrent = true;
                        $scope.downloadJpgEnableCurrent = true;
                    }
                    $scope.imageDataHiRes_Psd = $scope.productImageList[0].HIGH_RESOLUTION_PSD;
                    if ($scope.imageDataHiRes_Psd != null && $scope.imageDataHiRes_Psd != undefined && $scope.imageDataHiRes_Psd != '' && $scope.imageDataHiRes_Psd.toLowerCase().indexOf('noimage') <= -1) {
                        $scope.downloadEnableCurrent = true;
                        $scope.downloadPsdEnableCurrent = true;
                    }
                    $scope.GetImageInformation($scope.imageDataHiRes);
                }
                $rootScope.backButtonShow = true;
            }
        })

    }

    var navigation = $location.$$absUrl;
    if (navigation.indexOf("searchNavigation") > -1) {
        var searchStringCatId = $location.$$absUrl.split('parentCategory=');
        var categoryId = searchStringCatId[1].split('&categoryId=')[0];
        var searchStringFamId = searchStringCatId[1].split('&categoryId=')[1];
        var familyId = searchStringFamId.split('&familyId=')[0];
        var productId = searchStringFamId.split('&familyId=')[1];
        $rootScope.mouseHoverCategoryList = $localStorage.leftNavData.Table;
        $rootScope.mouseHoverFamilyList = $localStorage.leftNavData.Table1;
        $scope.familyList = $.grep($rootScope.mouseHoverFamilyList, function (v) {
            return v.CATEGORY_ID === familyId.toString();
        });
        if ($scope.familyList.length > 0) {
            $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
            if ($scope.categoryList[0].LEVEL == 3) {
                $scope.resouceTypeId = $scope.categoryList[0].CATEGORY_ID;
            }
            else if ($scope.categoryList[0].LEVEL == 4) {
                $scope.resouceTypeId = $scope.categoryList[0].PARENT_CATEGORY;
            }
            else if ($scope.categoryList[0].LEVEL == 5) {
                $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
                $scope.resouceTypeId = $scope.categoryList[0].PARENT_CATEGORY;

            }
        }
        searchData = [({ PARENT_CATEGORY: $scope.resouceTypeId, CATEGORY_ID: familyId, FAMILY_ID: productId })];
        $scope.ProductNavigation(searchData[0]);
    }
    else if (navigation.indexOf("cartNavigation") > -1) {
        var searchStringCatId = $location.$$absUrl.split('parentCategory=');
        var categoryId = searchStringCatId[1].split('&categoryId=')[0];
        var searchStringFamId = searchStringCatId[1].split('&categoryId=')[1];
        var familyId = searchStringFamId.split('&familyId=')[0];
        var productId = searchStringFamId.split('&familyId=')[1];
        $scope.cartProductId = productId;
        $rootScope.mouseHoverCategoryList = $localStorage.leftNavData.Table;
        $rootScope.mouseHoverFamilyList = $localStorage.leftNavData.Table1;
        $scope.familyList = $.grep($rootScope.mouseHoverFamilyList, function (v) {
            return v.CATEGORY_ID === familyId.toString();
        });
        if ($scope.familyList.length > 0) {
            $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
            if ($scope.categoryList[0].LEVEL == 3) {
                $scope.resouceTypeId = $scope.categoryList[0].CATEGORY_ID;
            }
            else if ($scope.categoryList[0].LEVEL == 4) {
                $scope.resouceTypeId = $scope.categoryList[0].PARENT_CATEGORY;
            }
            else if ($scope.categoryList[0].LEVEL == 5) {
                $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
                $scope.resouceTypeId = $scope.categoryList[0].PARENT_CATEGORY;

            }
            $rootScope.resouceTypeId = $scope.resouceTypeId;
            dataFactory.cartItemTypeCheck(categoryId, familyId, productId).success(function (itemCheck) {
                if (itemCheck != null) {
                    var itemDetails = itemCheck[0];
                    if (itemDetails.ITEM_TYPE.toLowerCase() == 'content' || itemDetails.ITEM_TYPE.toLowerCase() == 'contents'
                        || itemDetails.ITEM_TYPE.toLowerCase() == 'resource' || itemDetails.ITEM_TYPE.toLowerCase() == 'resources') {
                        cartData = [({ PARENT_CATEGORY: $rootScope.resouceTypeId, CATEGORY_ID: itemDetails.FAMILY_ID, CATEGORY_NAME: itemDetails.CATEGORY_NAME, TYPE: 'F' })];
                        $rootScope.FamilyNavigation(cartData[0]);
                    }
                    else if (itemDetails.ITEM_TYPE.toLowerCase() == 'product' || itemDetails.ITEM_TYPE.toLowerCase() == 'products') {
                        cartData = [({ PARENT_CATEGORY: $rootScope.resouceTypeId, CATEGORY_ID: itemDetails.FAMILY_ID, FAMILY_ID: $scope.cartProductId })];
                        $scope.ProductNavigation(cartData[0]);
                    }
                }
            });
        }
    }

    $scope.ImageReload = function (image) {
        $scope.imageDataHiRes = "";
        $scope.imageDataHiRes_Psd = "";
        $scope.imageData = image.STRING_VALUE;
        $scope.imageDataHiRes = image.HIGH_RESOLUTION;
        $scope.imageDataHiRes_Psd = image.HIGH_RESOLUTION_PSD;
        $scope.GetImageInformation($scope.imageDataHiRes);
    }

    $scope.GetImageInformation = function (image) {
        if (!image.toLowerCase().indexOf('noimage.jpg') > -1) {
            dataFactory.getImageDetails(image).success(function (response) {
                if (response != null) {
                    $scope.imageInfo = response;
                }
                else {
                    $scope.imageInfo = [];
                }
            });
        }
    }

    //===========================================Logged in user data=====================================================//
    dataFactory.getLoggedUserInfo().success(function (userRes) {
        if (userRes != null) {
            if ($localStorage.compareLineCardList != undefined && $localStorage.compareLineCardList.length > 0 && $localStorage.UserId == undefined) {
                $rootScope.compareLineCardList = [];
                $localStorage.compareLineCardList = [];
            }
            $scope.UserDetails = userRes.m_Item1[0];
            $localStorage.hostedUrl = userRes.m_Item2;
            $localStorage.imageUrl = userRes.m_Item3;
            $localStorage.imageSharedPath = userRes.m_Item4;
            $localStorage.UserId = $scope.UserDetails.USER_ID;
            $localStorage.UserEmail = $scope.UserDetails.USER_EMAIL;
            $localStorage.userGroupId = $scope.UserDetails.GROUP_ID;
            $localStorage.usingCatalogId = $scope.UserDetails.CATALOG_ID;
            $localStorage.userRoleName = $scope.UserDetails.ROLE_NAME;
            $rootScope.IS_PRINT = $scope.UserDetails.IS_PRINT;
            if ($localStorage.userGroupId == null || $localStorage.userGroupId == undefined) {
                $localStorage.userGroupId = 0;
            }
            $localStorage.userType = $scope.UserDetails.USER_TYPE;
            $scope.userType = $scope.UserDetails.USER_TYPE;
            $rootScope.UserEmail = $scope.UserDetails.USER_EMAIL;
            $scope.userRoleId = $scope.UserDetails.USER_ROLE;
            $rootScope.userRoleName = $scope.UserDetails.ROLE_NAME;
            $scope.userLoggedFlag = $scope.UserDetails.LOGGED_IN_FLAG;
            $scope.userAgreementData = $scope.UserDetails.USER_AGGREEMENT;
            if ($scope.userLoggedFlag != 1) {
                $(".modal").css("display", "block");
                $("#boxes").show();
                $scope.IsFirstTime = true;
            }
            else {
                $("#boxes").hide();
            }
            $rootScope.MouseHoverCategoryNavigation();
            if ((navigation.toLowerCase().indexOf("search") <= -1) && (navigation.indexOf("?backNavigation") <= -1) && (navigation.indexOf("cartNavigation") <= -1)) {
                $scope.init();
            }
            if ($scope.userRoleId == 1) {
                $("#usrManage").show();
                $("#grpManage").show();
            }
            else {
                $("#usrManage").hide();
                $("#grpManage").hide();
            }
        }
    });
    //===========================================Logged in user data=====================================================//

    $scope.ToPrint = function (resourceDetails) {
        if ($scope.previewFilePath != null && $scope.previewFilePath.trim() != "") {
            //if ($scope.FileCheckFunction($scope.previewFilePath)) {
            dataFactory.checkFileExits($scope.previewFilePath).success(function (response) {
                if (response != null) {
                    if (response == true) {
                        var fileExtention = $scope.previewFilePath.split('.');
                        var fileType = fileExtention[fileExtention.length - 1];
                        if (pdfArray.includes(fileType.toLowerCase()))
                            fileType = 'Pdf';
                        else if (imageArray.includes(fileType.toLowerCase()))
                            fileType = 'Images';
                        else if (cadArray.includes(fileType.toLowerCase()))
                            fileType = 'CAD Drawings';
                        else if (videoArray.includes(fileType.toLowerCase()))
                            fileType = 'Videos';
                        $scope.PDFanalyticsEntry($scope.previewFilePath, 'print', fileType);
                        dataFactory.addToCart($localStorage.UserId, resourceDetails.ATTRIBUTE_ID, resourceDetails.ATTRIBUTE_NAME, resourceDetails.FAMILY_ID, resourceDetails.PRODUCT_ID, $scope.previewFilePath).success(function (cartRes) {
                            if (cartRes != null) {
                                if (cartRes == 'success') {
                                    window.location.href = '/App/ShoppingCart';
                                }
                                else {
                                    $.msgBox({
                                        title: "LG",
                                        content: cartRes,
                                        type: "info"
                                    });
                                }
                            }
                        });

                    }
                    else {
                        //var filePath = "Images/404.png";
                        var filePath = $scope.previewFilePath;
                        $scope.previewWindow(filePath);
                    }
                }
            });
        }
    }

    $scope.ToDownload = function () {
        var fileExtention = $scope.previewFilePath.split('.');
        var fileType = fileExtention[fileExtention.length - 1];
        if (pdfArray.includes(fileType.toLowerCase()))
            fileType = 'Pdf';
        else if (imageArray.includes(fileType.toLowerCase()))
            fileType = 'Images';
        else if (cadArray.includes(fileType.toLowerCase()))
            fileType = 'CAD Drawings';
        else if (videoArray.includes(fileType.toLowerCase()))
            fileType = 'Videos';
        $scope.PDFanalyticsEntry($scope.previewFilePath, 'download', fileType);
        var path = $scope.previewFilePath.replace("&", "~");
        window.open("DownloadFile.ashx?Path=" + path);
    }

    $scope.ToPreview = function () {
        $scope.previewWindow($scope.previewFilePath);
    }

    $scope.PDFGenerationClick = function (resourceData, generateType) {
        var attributeName = resourceData.ATTRIBUTE_NAME;
        var filePath = '';
        var cobrandFlag = 0;
        var fileType = 'Pdf';
        if ($("#cobranding").prop("checked")) {
            cobrandFlag = 1;
        }
        if (attributeName.replace(/ /gi, '').toLowerCase().indexOf('speccard') > -1) {
            dataFactory.resourcePDFGen(resourceData.FAMILY_ID, resourceData.PRODUCT_ID, 'spec_card', generateType, resourceData.ATTRIBUTE_ID, $localStorage.UserId, cobrandFlag).success(function (specCardRes) {
                if (specCardRes != null) {
                    $scope.previewFilePath = specCardRes.split('~')[0];
                    $scope.exceedMsg = specCardRes.split('~')[1];
                    if ($scope.previewFilePath.indexOf('.pdf') > -1)
                        $scope.PDFanalyticsEntry($scope.previewFilePath, 'generate', fileType);
                    var popupContent = "";
                    if ($scope.exceedMsg == 'exceeded4') {
                        popupContent = "Dynamic PDF generation supports only 4 products. So it will take first 4 products from this family.";
                    }
                    else if ($scope.exceedMsg == 'exceeded2') {
                        popupContent = "Dynamic PDF generation supports only 2 products. So it will take first 2 products from this family.";
                    }
                    if (popupContent != "") {
                        $.msgBox({
                            title: "LG",
                            content: popupContent,
                            type: "info"
                        });
                    }

                    if (generateType == 'print') {
                        if ($scope.previewFilePath.toLowerCase().indexOf('.pdf') > -1) {
                            $scope.ToPrint(resourceData);
                        }
                        else {
                            $.msgBox({
                                title: "LG",
                                content: $scope.previewFilePath,
                                type: "info"
                            });
                        }
                    }
                    else if (generateType == 'download') {
                        if ($scope.previewFilePath.toLowerCase().indexOf('.pdf') > -1) {
                            $scope.ToDownload();
                        }
                        else {
                            $.msgBox({
                                title: "LG",
                                content: $scope.previewFilePath,
                                type: "info"
                            });
                        }
                    }
                    else if (generateType == 'preview') {
                        if ($scope.previewFilePath.toLowerCase().indexOf('.pdf') > -1) {
                            $scope.previewWindow($scope.previewFilePath);
                        }
                        else {
                            $.msgBox({
                                title: "LG",
                                content: $scope.previewFilePath,
                                type: "info"
                            });
                        }
                    }

                }
            });
        }
        else if (attributeName.replace(/ /gi, '').toLowerCase().indexOf('specsheet') > -1) {
            dataFactory.resourcePDFGen(resourceData.FAMILY_ID, resourceData.PRODUCT_ID, 'spec_sheet', generateType, resourceData.ATTRIBUTE_ID, $localStorage.UserId, cobrandFlag).success(function (specSheetRes) {
                if (specSheetRes != null) {
                    $scope.previewFilePath = specSheetRes.split('~')[0];
                    $scope.exceedMsg = specSheetRes.split('~')[1];
                    var popupContent = "";
                    if ($scope.previewFilePath.indexOf('.pdf') > -1)
                        $scope.PDFanalyticsEntry($scope.previewFilePath, 'generate', fileType);
                    if ($scope.exceedMsg == 'exceeded4') {
                        popupContent = "Dynamic PDF generation supports only 4 products. So it will take first 4 products from this family.";
                    }
                    else if ($scope.exceedMsg == 'exceeded2') {
                        popupContent = "Dynamic PDF generation supports only 2 products. So it will take first 2 products from this family.";
                    }
                    else if ($scope.exceedMsg == 'nogroup') {
                        popupContent = "Attribute group is not yet set for this template. Kindly contact your administrator for set group order to proceed.";
                        $.msgBox({
                            title: "LG",
                            content: popupContent,
                            type: "info"
                        });
                        return;
                    }
                    if (popupContent != "") {
                        $.msgBox({
                            title: "LG",
                            content: popupContent,
                            type: "info"
                        });
                    }
                    if (generateType == 'print') {
                        if ($scope.previewFilePath.toLowerCase().indexOf('.pdf') > -1) {
                            $scope.ToPrint(resourceData);
                        }
                        else {
                            $.msgBox({
                                title: "LG",
                                content: $scope.previewFilePath,
                                type: "info"
                            });
                        }
                    }
                    else if (generateType == 'download') {
                        if ($scope.previewFilePath.toLowerCase().indexOf('.pdf') > -1) {
                            $scope.ToDownload();
                        }
                        else {
                            $.msgBox({
                                title: "LG",
                                content: $scope.previewFilePath,
                                type: "info"
                            });
                        }
                    }
                    else if (generateType == 'preview') {
                        if ($scope.previewFilePath.toLowerCase().indexOf('.pdf') > -1) {
                            $scope.previewWindow($scope.previewFilePath);
                        }
                        else {
                            $.msgBox({
                                title: "LG",
                                content: $scope.previewFilePath,
                                type: "info"
                            });
                        }
                    }
                    else if (generateType == 'thumbnail') {
                        var Link = $localStorage.hostedUrl + $scope.previewFilePath + "#toolbar=0&navpanes=0";

                        $scope.thumbnailLink = $sce.trustAsResourceUrl(Link);

                    }
                }
            });
        }
    }
    $scope.FileCheckFunction = function (urlToFile) {
        var filePath = $localStorage.hostedUrl + urlToFile;
        dataFactory.checkFileExits(urlToFile).success(function (response) {
            if (response != null) {
                return response;
            }
        });
    }

    $scope.DownLoadFile = function (data) {
        $scope.previewFilePath = data.STRING_VALUE;
        if ($scope.previewFilePath == null || $scope.previewFilePath.trim() == "") {
            $scope.PDFGenerationClick(data, 'download');
        }
        else {
            $scope.ToDownload();
        }
    }
    $scope.DownLoadImage = function (data) {
        $scope.previewFilePath = data;
        var path = $scope.previewFilePath.replace("&", "~");
        window.open("DownloadFile.ashx?Path=" + path);
    }
    $scope.DownLoadImagePsd = function (data) {
        var path = data.replace("&", "~");
        window.open("DownloadFile.ashx?Path=" + path);
    }

    $scope.DownLoadAllFile = function (imagesList, downloadFormat) {
        var files = ""
        dataFactory.getImageCompressDetails($rootScope.partNumber[0].STRING_VALUE, downloadFormat, imagesList).success(function (response) {
            if (response != null) {
                $scope.zipPath = "Compress//LG_" + $rootScope.partNumber[0].STRING_VALUE + "_" + downloadFormat + ".zip";
                window.open("DownloadFile.ashx?Path=" + response);
            }
        });
    }

    $scope.PreviewClick = function (data) {
        $scope.previewFilePath = data.STRING_VALUE;
        if ($scope.previewFilePath == null || $scope.previewFilePath.trim() == "") {
            $scope.PDFGenerationClick(data, 'preview');
        }
        else {
            $scope.ToPreview();
        }
    }

    $scope.IFrameClose = function () {
        $("#dialog").hide();
        $(".ui-dialog").hide();
    }

    $scope.LinkClick = function () {
    }


    $scope.printResourceClick = function (resourceDetails) {
        $scope.previewFilePath = resourceDetails.STRING_VALUE;
        if ($scope.previewFilePath == null || $scope.previewFilePath.trim() == "") {
            $scope.PDFGenerationClick(resourceDetails, 'print');
        }
        if ($scope.previewFilePath != null && $scope.previewFilePath.trim() != "") {
            $scope.ToPrint(resourceDetails);
        }
    }

    $scope.DownLoadContentFile = function (image, e) {
        var files = ""
        var fileExtention = image.HIGH_RESOLUTION.split('.');
        var fileType = fileExtention[fileExtention.length - 1];
        if (pdfArray.includes(fileType.toLowerCase()))
            fileType = 'Pdf';
        else if (imageArray.includes(fileType.toLowerCase()))
            fileType = 'Images';
        else if (cadArray.includes(fileType.toLowerCase()))
            fileType = 'CAD Drawings';
        else if (videoArray.includes(fileType.toLowerCase()))
            fileType = 'Videos';
        $scope.PDFanalyticsEntry(image.HIGH_RESOLUTION, 'download', fileType);
        path = image.HIGH_RESOLUTION.replace("&", "~");
        window.open("DownloadFile.ashx?Path=" + path);
        if (e != undefined) {
            e.stopPropagation();
        }
    }

    $scope.PreviewContentClick = function (data) {
        $scope.previewWindow(data.HIGH_RESOLUTION);
    }

    $scope.ContentClose = function () {
        $("#dialog_content").hide();
        $(".ui-dialog").hide();
    }


    $scope.printContentClick = function (contentDetails, e) {
        $scope.filePath = contentDetails.HIGH_RESOLUTION;
        if (contentDetails.PARENT_FAMILY_ID != undefined) {
            $scope.familyId = contentDetails.PARENT_FAMILY_ID;
            $scope.productId = contentDetails.FAMILY_ID;
        }
        else {
            $scope.familyId = contentDetails.FAMILY_ID;
            $scope.productId = contentDetails.PRODUCT_ID;
        }
        if ($scope.filePath != null && $scope.filePath.trim() != '') {
            dataFactory.checkFileExits($scope.filePath).success(function (response) {
                if (response != null) {
                    if (response == true) {
                        dataFactory.addToCart($localStorage.UserId, 0, 'content', $scope.familyId, $scope.productId, $scope.filePath).success(function (cartRes) {
                            if (cartRes != null) {
                                if (cartRes == 'success') {
                                    window.location.href = '/App/ShoppingCart';
                                }
                                else {
                                    $.msgBox({
                                        title: "LG",
                                        content: cartRes,
                                        type: "info"
                                    });
                                }
                            }
                        });
                    }
                    else {
                        var filePath = $localStorage.hostedUrl + "Images/404.png";
                        $scope.previewWindow(filePath);
                    }
                }
            });
        }
        if (e != undefined) {
            e.stopPropagation();
        }
    };

    $scope.itemSearchStart = function (searchText) {
        if (searchText != undefined && searchText.trim() != "") {
            if (!$localStorage.backFlag) {
                if ($localStorage.backNavigationList.length > 0) {
                    var lastVal = $localStorage.backNavigationList[$localStorage.backNavigationList.length - 1];
                    var lastRootCheck = lastVal.PARAMETER1;
                    if (lastRootCheck != searchText) {
                        $localStorage.backNavigationList.push({ PAGE: 'SEARCH_PAGE', URL: '/', FUNCTION: 'itemSearchStart', PARAMETER1: searchText, PARAMETER2: '', PARAMETER3: $localStorage.parentCategory });
                    }
                }
                else {
                    $localStorage.backNavigationList.push({ PAGE: 'SEARCH_PAGE', URL: '/', FUNCTION: 'itemSearchStart', PARAMETER1: searchText, PARAMETER2: '', PARAMETER3: $localStorage.parentCategory });
                }
            }
            else {
                $localStorage.backFlag = false;
                localStorage.setItem("backFlag", false);
            }
            $localStorage.searchText = searchText;
            window.location = '/App/SearchResult';
        }
    }
    $scope.EnterSearchEvent = function (e, searchText) {
        if (e.which === 13) {
            $scope.searchChangesConfirmation(searchText);
        }
    }
    $scope.init = function () {
        $localStorage.leftNavData = [];
        $rootScope.GetCategoryList();
        $rootScope.MouseHoverCategoryNavigation();
    }
    $scope.IsHover = false;

    $rootScope.MouseHoverCategoryNavigation = function () {
        var option = 'MENU_CATEGORY';
        $scope.categoryId = '0';
        dataFactory.getCategoryFamilyList($scope.categoryId, option, $scope.resouceTypeId, 0, $localStorage.userGroupId, $localStorage.UserId).success(function (responseMouse) {
            if (responseMouse != null) {
                $localStorage.leftNavData = responseMouse;
                $rootScope.mouseHoverCategoryList = responseMouse.Table;
                $rootScope.mouseHoverFamilyList = responseMouse.Table1;
            }
        });
    }

    $scope.IsLinkPreview = false;
    $scope.LinkCreation = function (data) {
        if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
            $('iframe.embed-file').hide();
        }
        $scope.IsLinkPreview = true;
        option = "INSERT";
        var flagType = "Product";
        var objData = []

        $scope.familyList = $.grep($rootScope.mouseHoverFamilyList, function (v) {
            return v.CATEGORY_ID === data[0].FAMILY_ID.toString();
        });
        if ($scope.familyList.length > 0) {
            if ($scope.familyList[0].LEVEL == 3) {
                $scope.resouceTypeId = $scope.familyList[0].CATEGORY_ID;
            }
            else {
                $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
                if ($scope.categoryList[0].LEVEL == 3) {
                    $scope.resouceTypeId = $scope.categoryList[0].CATEGORY_ID;
                }
                else if ($scope.categoryList[0].LEVEL == 4) {
                    $scope.resouceTypeId = $scope.categoryList[0].PARENT_CATEGORY;
                }
                else if ($scope.categoryList[0].LEVEL == 5) {
                    $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
                    $scope.resouceTypeId = $scope.categoryList[0].PARENT_CATEGORY;

                }
            }
        }
        $timeout(function () {
            if ($scope.resouceTypeId != "") {
                objData.push({ CATEGORY_ID: $scope.resouceTypeId, FAMILY_ID: data[0].FAMILY_ID, PRODUCT_ID: data[0].PRODUCT_ID, OPTION: option, USER_ID: $localStorage.UserId, USER_GROUP: $localStorage.userGroupId, IMAGE: $scope.imageData, TYPE: flagType });
                dataFactory.updateLinkDetails(objData).success(function (response) {
                    if (response != null) {
                        $(".modal").css("display", "block");
                        $scope.LinkText = response;
                        var copyDiv = $("#clipboard_div")
                        copyDiv.html($scope.LinkText);
                    }
                });
            }
        }, 100);
    }


    $scope.LinkContentCreation = function (data, e) {
        if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
            $('iframe.embed-file').hide()
        }
        $scope.IsLinkPreview = true;
        option = "INSERT";
        var flagType = "Content";
        var objData = []
        $scope.familyList = $.grep($rootScope.mouseHoverFamilyList, function (v) {
            return v.CATEGORY_ID === data.CATEGORY_ID.toString();
        });
        if ($scope.familyList.length > 0) {
            if ($scope.familyList[0].LEVEL == 3) {
                $scope.resouceTypeId = $scope.familyList[0].CATEGORY_ID;
            }
            else {
                $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
                if ($scope.categoryList[0].LEVEL == 3) {
                    $scope.resouceTypeId = $scope.categoryList[0].CATEGORY_ID;
                }
                else if ($scope.categoryList[0].LEVEL == 5) {
                    $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
                    $scope.resouceTypeId = $scope.categoryList[0].PARENT_CATEGORY;

                }
            }
        }
        $timeout(function () {
            if ($scope.resouceTypeId != "") {

                objData.push({ CATEGORY_ID: $scope.resouceTypeId, FAMILY_ID: data.CATEGORY_ID, PRODUCT_ID: data.FAMILY_ID, OPTION: option, USER_ID: $localStorage.UserId, USER_GROUP: $localStorage.userGroupId, IMAGE: "", TYPE: flagType });
                dataFactory.updateLinkDetails(objData).success(function (response) {
                    if (response != null) {
                        $(".modal").css("display", "block");
                        $scope.LinkText = response;
                        var copyDiv = $("#clipboard_div");
                        copyDiv.html($scope.LinkText);
                    }
                });
            }
        }, 100);
        if (e != undefined) {
            e.stopPropagation();
        }
    }

    $scope.prodListLinkCreation = function (data, e) {
        if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
            $('iframe .embed-file').hide();
        }
        $scope.IsLinkPreview = true;
        option = "INSERT";
        var flagType = "Product";
        var objData = []
        $scope.familyList = $.grep($rootScope.mouseHoverFamilyList, function (v) {
            return v.CATEGORY_ID === data.PARENT_CATEGORY.toString();
        });
        if ($scope.familyList.length > 0) {
            if ($scope.familyList[0].LEVEL == 3) {
                $scope.resouceTypeId = $scope.familyList[0].CATEGORY_ID;
            }
            else {
                $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
                if ($scope.categoryList[0].LEVEL == 3) {
                    $scope.resouceTypeId = $scope.categoryList[0].CATEGORY_ID;
                }
                else if ($scope.categoryList[0].LEVEL == 5) {
                    $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
                    $scope.resouceTypeId = $scope.categoryList[0].PARENT_CATEGORY;
                }
            }
        }
        $timeout(function () {
            if ($scope.resouceTypeId != "") {
                objData.push({ CATEGORY_ID: $scope.resouceTypeId, FAMILY_ID: data.CATEGORY_ID, PRODUCT_ID: data.FAMILY_ID, OPTION: option, USER_ID: $localStorage.UserId, USER_GROUP: $localStorage.userGroupId, IMAGE: data.IMAGE_FILE, TYPE: flagType });
                dataFactory.updateLinkDetails(objData).success(function (response) {
                    if (response != null) {
                        $(".modal").css("display", "block");
                        $scope.LinkText = response;
                        var copyDiv = $("#clipboard_div")
                        copyDiv.html($scope.LinkText);
                    }
                });
            }
        }, 100);
        if (e != undefined) {
            e.stopPropagation();
        }
    }

    $scope.SearchLinkContentCreation = function (data, e) {
        if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
            $('iframe.embed-file').hide()
        }
        $scope.IsLinkPreview = true;
        option = "INSERT";
        var flagType = "Content";
        var objData = []
        $scope.familyList = $.grep($rootScope.mouseHoverFamilyList, function (v) {
            return v.CATEGORY_ID === data.CATEGORY_ID.toString();
        });
        if ($scope.familyList.length > 0) {
            if ($scope.familyList[0].LEVEL == 3) {
                $scope.resouceTypeId = $scope.familyList[0].CATEGORY_ID;
            }
            else {
                $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
                if ($scope.categoryList.length > 0) {
                    if ($scope.categoryList[0].LEVEL == 3) {
                        $scope.resouceTypeId = $scope.categoryList[0].CATEGORY_ID;
                    }
                    else if ($scope.categoryList[0].LEVEL == 5) {
                        $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
                        $scope.resouceTypeId = $scope.categoryList[0].PARENT_CATEGORY;
                    }
                }
                else {
                    if ($scope.familyList[0].LEVEL == 3) {
                        $scope.resouceTypeId = $scope.familyList[0].PARENT_CATEGORY;
                    }
                }
            }
        }
        $timeout(function () {
            if ($scope.resouceTypeId != "") {
                objData.push({ CATEGORY_ID: $scope.resouceTypeId, FAMILY_ID: data.FAMILY_ID, PRODUCT_ID: data.PRODUCT_ID, OPTION: option, USER_ID: $localStorage.UserId, USER_GROUP: $localStorage.userGroupId, IMAGE: "", TYPE: flagType });
                dataFactory.updateLinkDetails(objData).success(function (response) {
                    if (response != null) {
                        $(".modal").css("display", "block");
                        $scope.LinkText = response;
                        var copyDiv = $("#clipboard_div");
                        copyDiv.html($scope.LinkText);
                    }
                });
            }
        }, 100);
        if (e != undefined) {
            e.stopPropagation();
        }
    }

    $scope.searchProdListLinkCreation = function (data, e) {
        if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
            $('iframe.embed-file').hide();
        }
        $scope.IsLinkPreview = true;
        option = "INSERT";
        var flagType = "Product";
        var objData = []
        $scope.familyList = $.grep($rootScope.mouseHoverFamilyList, function (v) {
            return v.CATEGORY_ID === data.CATEGORY_ID.toString();
        });
        if ($scope.familyList.length > 0) {
            if ($scope.familyList[0].LEVEL == 3) {
                $scope.resouceTypeId = $scope.familyList[0].CATEGORY_ID;
            }
            else {
                $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].CATEGORY_ID);
                if ($scope.categoryList[0].LEVEL == 3) {
                    $scope.resouceTypeId = $scope.categoryList[0].CATEGORY_ID;
                }
                else if ($scope.categoryList[0].LEVEL == 5) {
                    $scope.categoryList = $scope.IdentifyResource($rootScope.mouseHoverFamilyList, $scope.familyList[0].PARENT_CATEGORY);
                    $scope.resouceTypeId = $scope.categoryList[0].PARENT_CATEGORY;
                }
                else if ($scope.categoryList[0].LEVEL == 4) {
                    $scope.resouceTypeId = $scope.categoryList[0].PARENT_CATEGORY;
                }
            }
        }
        $timeout(function () {
            if ($scope.resouceTypeId != "") {
                objData.push({ CATEGORY_ID: $scope.resouceTypeId, FAMILY_ID: data.FAMILY_ID, PRODUCT_ID: data.PRODUCT_ID, OPTION: option, USER_ID: $localStorage.UserId, USER_GROUP: $localStorage.userGroupId, IMAGE: data.IMAGE_FILE, TYPE: flagType });
                dataFactory.updateLinkDetails(objData).success(function (response) {
                    if (response != null) {
                        $(".modal").css("display", "block");
                        $scope.LinkText = response;
                        var copyDiv = $("#clipboard_div")
                        copyDiv.html($scope.LinkText);
                    }
                });
            }
        }, 100);
        if (e != undefined) {
            e.stopPropagation();
        }
    }

    if (!('remove' in Element.prototype)) {
        Element.prototype.remove = function () {
            if (this.parentNode) {
                this.parentNode.removeChild(this);
            }
        };
    }

    $scope.CopyText = function () {
        var copyDiv = $("#clipboard_div");
        var textArea = document.createElement("textarea");
        textArea.value = $scope.LinkText;
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("Copy");
        textArea.remove();
        $scope.IsLinkPreview = false;
        if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
            $('iframe.embed-file').show();
        }
    }

    $scope.CloseCopyPopup = function () {
        $scope.IsLinkPreview = false;
        if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
            $('iframe.embed-file').show();
        }
    }

    $scope.CloseAgreementPopup = function () {
        $scope.IsFirstTime = false;
        dataFactory.userAgreementUpdate($localStorage.UserId).success(function (agreeRes) {
            if (agreeRes != null) {

            }
        })
    };

    $scope.backNavigation = function () {
        var userRegFirstName = $rootScope.userRegFirstName;
        var conditionAdded = false;
        if ($rootScope.IsEdit != undefined) {
            conditionAdded = !(($rootScope.editedUserDetails.USER_EMAIL == $rootScope.dataModified.Email) && ($rootScope.editedUserDetails.USER_FNAME == $rootScope.dataModified.FirstName) &&
                ($rootScope.editedUserDetails.USER_LNAME == $rootScope.dataModified.LastName) && ($rootScope.editedUserDetails.USER_PASSWORD == $rootScope.dataModified.Password) &&
                ($rootScope.editedUserDetails.USER_STATUS == $rootScope.dataModified.Status) && ($rootScope.editedUserDetails.COMPANY == $rootScope.dataModified.CompanyName));
            var status = 0;
            if ($rootScope.dataModified.Status)
                status = 1;
        }
        if ($rootScope.isUserStatusChanged != undefined && !conditionAdded) {
            conditionAdded = $rootScope.isUserStatusChanged;
        }
        if ($rootScope.isUserAssoChanged != undefined && !conditionAdded) {
            conditionAdded = $rootScope.isUserAssoChanged;
        }
        if (((document.getElementById('CompanyName_mvc') != null && document.getElementById('CompanyName_mvc').value != "") ||
            (document.getElementById('Email_mvc') != null && document.getElementById('Email_mvc').value != "") ||
            (document.getElementById('LastName_mvc') != null && document.getElementById('LastName_mvc').value != "") ||
            (document.getElementById('FirstName_mvc') != null && document.getElementById('FirstName_mvc').value != "")) && !conditionAdded) {
            conditionAdded = true;
        }
        if (conditionAdded) {
            $.msgBox({
                title: "LG",
                content: "Changes will be lost. Are you sure to continue?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (selection) {
                    if (selection == "Yes") {
                        $scope.backNavigationProcess();
                    }
                }
            })
        }
        else {
            $scope.backNavigationProcess();
        }
    };

    $scope.backNavigationProcess = function () {
        if ($localStorage.backNavigationList.length > 1) {
            $localStorage.backFlag = true;
            localStorage.setItem("backFlag", true);
            $localStorage.backNavigationList.pop();
            list = $localStorage.backNavigationList;
            $localStorage.backUrl = list[list.length - 1].URL;
            $localStorage.function = list[list.length - 1].FUNCTION;
            $localStorage.perameter1 = list[list.length - 1].PARAMETER1;
            $localStorage.perameter2 = list[list.length - 1].PARAMETER2;
            $localStorage.perameter3 = list[list.length - 1].PARAMETER3;
            window.location.href = '/?backNavigation';
        }
        else {
            $rootScope.backButtonShow = false;
            window.location.href = '/';
        }
    }

    if (navigation.indexOf("?backNavigation") > -1) {
        $rootScope.parentCategory = $localStorage.perameter3;
        if ($localStorage.backUrl == '/') {
            if ($localStorage.perameter2 != undefined) {
                $rootScope.mouseHoverCategoryList = $localStorage.perameter2.Table;
                $rootScope.mouseHoverFamilyList = $localStorage.perameter2.Table1;
            }
            if ($localStorage.function == 'itemSearchStart') {
                $scope.itemSearchStart($localStorage.perameter1);
            }
            else if ($localStorage.function == 'ProductNavigation') {
                $scope.ProductNavigation($localStorage.perameter1);
            }
            else if ($localStorage.function == 'FamilyNavigation') {
                $rootScope.FamilyNavigation($localStorage.perameter1);
            }
            else if ($localStorage.function == 'CategoryNavigation') {
                $rootScope.CategoryNavigation($localStorage.perameter1);
            }
            else {
                window.location.href = $localStorage.backUrl;
            }
        }
        else {
            window.location.href = $localStorage.backUrl;
        }
    }

    $scope.logOutClick = function () {
        $localStorage.backNavigationList = [];
        $localStorage.backFlag = false;
        $rootScope.backButtonShow = false;
        $rootScope.compareLineCardList = [];
        $localStorage.compareLineCardList = [];
    }

    $scope.logoClick = function () {
        $localStorage.backNavigationList = [];
        $localStorage.backFlag = false;
        $rootScope.backButtonShow = false;
        window.location.replace('/');
    };

    $scope.sample = function () {
        $("#detailID").dialog();
        $(".ui-dialog").css("display", "block");
        // $("#detailID").dialogExtend("collapse");
    }

    $scope.detailImagePreview = function (imgData) {
        var file = imgData;
        $scope.previewWindow(file);
    }

    $scope.isImagePopUp = false;
    $scope.ImagePopUp = function () {
        $scope.isImagePopUp = true;
        $("#imagepopupdiv").removeClass("fancybox-div");
        $("#imagepopupdiv").css("display", "block !important");
    }

    $scope.ImagePopUpClose = function () {
        $scope.isImagePopUp = false;
        //$scope.imageDataHiRes = "";
        $("#imagepopupdiv").addClass("fancybox-div");
        $("#imagepopupdiv").css("display", "none !important");
    }
    $scope.imageZoomPreview = "";
    $scope.IsImagePreview = false;
    $scope.previewWindow = function (dataUrl) {
        var url = dataUrl;
        dataFactory.checkFileExits(dataUrl).success(function (response) {
            if (response != null) {
                //var height = screen.height / 2;
                //var width = screen.width / 2;
                //var left = width / 2;
                //var top = height / 2;
                var fileExtention = /[^.]+$/.exec(url)[0];
                fileExtention = $.trim(fileExtention);
                if (fileExtention.toLowerCase() != 'dwg' && fileExtention.toLocaleLowerCase() != "png" && fileExtention.toLocaleLowerCase() != "jpg" && fileExtention.toLocaleLowerCase() != "jpeg" && fileExtention.toLocaleLowerCase() != "gif" && fileExtention.toLocaleLowerCase != "ico") {

                    var w = 900;
                    var h = 500;
                    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : window.screenX;
                    var dualScreenTop = window.screenTop != undefined ? window.screenTop : window.screenY;

                    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
                    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

                    var systemZoom = width / window.screen.availWidth;
                    var left = (width - w) / 2 / systemZoom + dualScreenLeft
                    var top = (height - h) / 2 / systemZoom + dualScreenTop
                    var newWindow = window.open('', '', 'scrollbars=no, width=' + w / systemZoom + ', height=' + h / systemZoom + ', top=' + top + ', left=' + left);

                    // Puts focus on the newWindow
                    if (window.focus) {
                        newWindow.focus();

                    }

                    //     var newWindow = window.open('', '', 'top=' + top + ',left=' + left + ',width=' + width + ',height=' + height + ',resizable=1,scrollbars=no');
                    if (response == true) {
                        //newWindow.addEventListener('load', function () {
                        //    setTimeout(function () {
                        //        newWindow.document.title = 'LG 2.0 Preview';
                        //    }, 500);
                        //}, false);
                        //newWindow.onfocus = function () { newWindow.document.title = "hello world"; };
                        //newWindow.document.write('<title>My PDF File Title</title>');
                        //newWindow.document.write('<html><head><title>LG 2.0 Preview</title> <link rel="icon" type="image/png" href="/Design/assets/images/32x32.png" sizes="32x32"> </head><body height="100%" width="100%"><center><iframe src="' + url + '" height="100%" width="100%"></iframe></center></body></html>');
                    }
                    else {
                        url = $localStorage.hostedUrl + "Images/404.png";
                    }
                    var fileExtention = /[^.]+$/.exec(url)[0];
                    if (fileExtention.toLowerCase() == 'dwg' || fileExtention.toLowerCase() == 'dxf' || fileExtention.toLowerCase() == 'stl' || fileExtention.toLowerCase() == 'ai' || fileExtention.toLowerCase() == 'x3d') {
                        url = $localStorage.hostedUrl + "Images/PreviewNotAvailable.png";
                    }
                    if (fileExtention.toLowerCase() == 'mp4' || fileExtention.toLowerCase() == 'avi' || fileExtention.toLowerCase() == 'flv' || fileExtention.toLowerCase() == 'mkv' || fileExtention.toLowerCase() == 'wmv' || fileExtention.toLowerCase() == 'webm') {
                        newWindow.document.write('<html><head><title>LG DAL 2.0 Preview</title> <link rel="icon" type="image/png" href="/Design/assets/images/32x32.png" sizes="32x32"><style>body {text-align:center}</style> </head><body height="100%" width="100%" style="overflow:hidden;background: lightgray;"><embed class="iframepopup" src="' + url + '" height="100%" width="100%" style="overflow:hidden;background: lightgray;"></embed></body></html>');
                    }
                    else {

                        newWindow.document.write('<html><head><title>LG DAL 2.0 Preview</title> <link rel="icon" type="image/png" href="/Design/assets/images/32x32.png" sizes="32x32"><style>body {text-align:center}</style> </head><body height="100%" width="100%" style="overflow:hidden;background: lightgray;"><iframe class="iframepopup" src="' + url + '" height="100%" width="100%" style="overflow:hidden;background: lightgray;"></iframe></body></html>');
                    }
                }
                else {
                    $scope.imageZoomPreview = url;
                    $scope.IsImagePreview = true;
                }
                return true;

            }
        });
    }
    $scope.ResourceThumbnailClick = function (data) {
        $("#cobrandingAttachment").hide();
        $rootScope.thumnailDynamicList = $.grep(data, function (v) {
            return v.ATTRIBUTE_NAME.toLocaleLowerCase() === 'specsheet';
        });
        if ($rootScope.thumnailDynamicList.length > 0 && ($rootScope.thumnailDynamicList[0].STRING_VALUE == '' || $rootScope.thumnailDynamicList[0].STRING_VALUE == null)) {
            $scope.PDFGenerationClick($rootScope.thumnailDynamicList[0], 'thumbnail');

        }
        else if ($rootScope.thumnailDynamicList.length > 0) {

            // $scope.thumbnailLink = $sce.trustAsHtml(lLink);
            $scope.thumbnailLink = $sce.trustAsResourceUrl($rootScope.thumnailDynamicList[0].STRING_VALUE + "#toolbar=0&navpanes=0");
        }
    }
    $scope.ProductAssetLinkCreation = function (data) {
        if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
            $('iframe.embed-file').hide();
        }
        $scope.IsLinkPreview = true;
        option = "INSERT";
        var flagType = "ProductAsset";
        var objData = []
        var cobrandFlag = 0;
        if ($("#cobranding").prop("checked")) {
            cobrandFlag = 1;
        }
        var pdfType = "spec_sheet";
        if (data.ATTRIBUTE_NAME.replace(/ /gi, '').toLowerCase().indexOf('speccard') > -1) {
            pdfType = "spec_card";
        }

        objData.push({ CATEGORY_ID: $scope.resouceTypeId, FAMILY_ID: data.FAMILY_ID, PRODUCT_ID: data.PRODUCT_ID, OPTION: option, USER_ID: $localStorage.UserId, USER_GROUP: $localStorage.userGroupId, IMAGE: $scope.imageData, TYPE: flagType, STRING_VALUE: data.STRING_VALUE, PRINT_CLASS: data.PRINT_CLASS, ATTRIBUTE_ID: data.ATTRIBUTE_ID, CO_BRAND_FLAG: cobrandFlag, ATTRIBUTE_NAME: pdfType });
        dataFactory.updateLinkDetails(objData).success(function (response) {
            if (response != null) {
                $(".modal").css("display", "block");
                $scope.LinkText = response;
                var copyDiv = $("#clipboard_div")
                copyDiv.html($scope.LinkText);
            }
        });
    }

    $scope.cobrandingCheckclick = function (isChecked) {
        if (isChecked) {
            $("#cobrandingAttachment").show();
        }
        else {
            $("#cobrandingAttachment").hide();
        }
    };

    $scope.UploadImage = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        $scope.file = file;
        $rootScope.SelectedFileForUploadnamemain = file.name;
        $http.post("/Guest/UploadCobrandImage?XMLName=" + $scope.newxmlname1 + "", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {
                if (d != null) {
                    $scope.cobrandImagePath = d.split(',')[0];
                }
            }).error(function () {
                $.msgBox({
                    title: "LG",
                    content: 'File upload failed, please try again.',
                    type: "error"
                });
            });
    }

    $scope.selectFileforCobrand = function (file) {
        if (file.length > 0) {
            $scope.attachmentPath = "";
            angular.forEach(file, function (value) {
                $scope.SelectedFileForAttachments = value;
                $scope.UploadImage($scope.SelectedFileForAttachments);
            })
        }
    };

    $scope.layoutNavigationClick = function (redirectURL) {
        var conditionAdded = false;
        if ($rootScope.IsEdit != undefined) {
            conditionAdded = !(($rootScope.editedUserDetails.USER_EMAIL == $rootScope.dataModified.Email) && ($rootScope.editedUserDetails.USER_FNAME == $rootScope.dataModified.FirstName) &&
                ($rootScope.editedUserDetails.USER_LNAME == $rootScope.dataModified.LastName) && ($rootScope.editedUserDetails.USER_PASSWORD == $rootScope.dataModified.Password) &&
                ($rootScope.editedUserDetails.USER_STATUS == $rootScope.dataModified.Status) && ($rootScope.editedUserDetails.COMPANY == $rootScope.dataModified.CompanyName));
            var status = 0;
            if ($rootScope.dataModified.Status)
                status = 1;
        }
        if ($rootScope.isUserStatusChanged != undefined && !conditionAdded) {
            conditionAdded = $rootScope.isUserStatusChanged;
        }
        if ($rootScope.isUserAssoChanged != undefined && !conditionAdded) {
            conditionAdded = $rootScope.isUserAssoChanged;
        }
        if (((document.getElementById('CompanyName_mvc') != null && document.getElementById('CompanyName_mvc').value != "") ||
            (document.getElementById('Email_mvc') != null && document.getElementById('Email_mvc').value != "") ||
            (document.getElementById('LastName_mvc') != null && document.getElementById('LastName_mvc').value != "") ||
            (document.getElementById('FirstName_mvc') != null && document.getElementById('FirstName_mvc').value != "")) && !conditionAdded) {
            conditionAdded = true;
        }
        if (conditionAdded) {
            $.msgBox({
                title: "LG",
                content: "Changes will be lost. Are you sure to continue?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (selection) {
                    if (selection == "Yes") {
                        if (redirectURL == '/') {
                            $scope.logoClick();
                        }
                        window.location.href = redirectURL;
                    }
                }
            })
        }
        else {
            if (redirectURL == '/') {
                $scope.logoClick();
            }
            window.location.href = redirectURL;
        }
    };

    $scope.searchChangesConfirmation = function (searchText) {
        var conditionAdded = false;
        if ($rootScope.IsEdit != undefined) {
            conditionAdded = !(($rootScope.editedUserDetails.USER_EMAIL == $rootScope.dataModified.Email) && ($rootScope.editedUserDetails.USER_FNAME == $rootScope.dataModified.FirstName) &&
                ($rootScope.editedUserDetails.USER_LNAME == $rootScope.dataModified.LastName) && ($rootScope.editedUserDetails.USER_PASSWORD == $rootScope.dataModified.Password) &&
                ($rootScope.editedUserDetails.USER_STATUS == $rootScope.dataModified.Status) && ($rootScope.editedUserDetails.COMPANY == $rootScope.dataModified.CompanyName));
            var status = 0;
            if ($rootScope.dataModified.Status)
                status = 1;
        }
        if ($rootScope.isUserStatusChanged != undefined && !conditionAdded) {
            conditionAdded = $rootScope.isUserStatusChanged;
        }
        if ($rootScope.isUserAssoChanged != undefined && !conditionAdded) {
            conditionAdded = $rootScope.isUserAssoChanged;
        }
        if (((document.getElementById('CompanyName_mvc') != null && document.getElementById('CompanyName_mvc').value != "") ||
            (document.getElementById('Email_mvc') != null && document.getElementById('Email_mvc').value != "") ||
            (document.getElementById('LastName_mvc') != null && document.getElementById('LastName_mvc').value != "") ||
            (document.getElementById('FirstName_mvc') != null && document.getElementById('FirstName_mvc').value != "")) && !conditionAdded) {
            conditionAdded = true;
        }
        if (conditionAdded) {
            $.msgBox({
                title: "LG",
                content: "Changes will be lost. Are you sure to continue?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (selection) {
                    if (selection == "Yes") {
                        $scope.itemSearchStart(searchText);
                    }
                }
            })
        }
        else {
            $scope.itemSearchStart(searchText);
        }
    };
    $scope.GetHoverIdentification = function () {
        if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
            $('iframe.embed-file').hide();
        }
    }
    $scope.GetLeveIdentification = function () {
        if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) {
            $('iframe.embed-file').show();
        }
    }
    //-------------------------------------------Analytics function call---------------------------------------//
    $scope.PDFanalyticsEntry = function (pdfPath, analyticType, fileType) {
        if ($localStorage.UserId != undefined) {
            dataFactory.pdfAnalyticsDetailsEntry(pdfPath, analyticType, $localStorage.UserId, fileType).success(function (analyticRes) {
                if (analyticRes != null) {

                }
            })
        }
    };
    $scope.ProductAnalyticsEntry = function (itemNumber) {
        if ($localStorage.UserId != undefined) {
            dataFactory.productAnalyticsDetailsEntry(itemNumber, $localStorage.UserId).success(function (prodAnalyticRes) {
                if (prodAnalyticRes != null) {

                }
            })
        }
    };
    //-------------------------------------------Analytics function call---------------------------------------//

    //--------------------------------------------Compare Process----------------------------------------------//
    if ($rootScope.compareLineCardList == undefined && $localStorage.compareLineCardList == undefined) {
        $rootScope.compareLineCardList = [];
        $localStorage.compareLineCardList = [];
    }
    else {
        $rootScope.compareLineCardList = $localStorage.compareLineCardList;
    }
    $scope.lineCardCompareEnable = false;
    $scope.compareLineCardListAddClick = function (prodData) {
        var productExistanceCheck = [];
        dataFactory.checkLineCardInputTemplate(prodData.CATEGORY_ID).success(function (inputRes) {
            if (inputRes != null) {
                var categoryId = inputRes[0].CATEGORY_ID;
                var inputTemplateId = inputRes[0].TEMPLATE_ID;
                var lineCardTemplate = inputRes[0].LINECARD_TEMPLATE;
                if (inputTemplateId != 0 && inputTemplateId != null && lineCardTemplate != null && lineCardTemplate != '') {
                    if ($rootScope.compareLineCardList.length > 0) {
                        if ($rootScope.compareLineCardList.length < 16) {
                            if (inputTemplateId == $rootScope.compareLineCardList[0].TEMPLATE_ID) {
                                productExistanceCheck = $.grep($rootScope.compareLineCardList, function (v) {
                                    return v.PRODUCT_ID === prodData.FAMILY_ID;
                                });
                                if (productExistanceCheck.length == 0) {
                                    $rootScope.compareLineCardList.push({ CATEGORY_ID: categoryId, FAMILY_ID: prodData.CATEGORY_ID, PRODUCT_ID: prodData.FAMILY_ID, USER_ID: $localStorage.UserId, IMAGE: prodData.IMAGE_FILE, TEMPLATE_ID: inputTemplateId, ITEM_NO: prodData.FAMILY_NAME });
                                    $localStorage.compareLineCardList = $rootScope.compareLineCardList;
                                }
                                else {
                                    $.msgBox({
                                        title: "LG",
                                        content: "Selected product is already in the LineCard queue.",
                                        type: "info"
                                    });
                                }
                            }
                            else {
                                $.msgBox({
                                    title: "LG",
                                    content: "Dissimilar products cannot be compared.",
                                    type: "info"
                                });
                            }
                        }
                        else {
                            $.msgBox({
                                title: "LG",
                                content: "Kindly select 2-16 items to compare.",
                                type: "info"
                            });
                        }
                    }
                    else {
                        $rootScope.compareLineCardList.push({ CATEGORY_ID: categoryId, FAMILY_ID: prodData.CATEGORY_ID, PRODUCT_ID: prodData.FAMILY_ID, USER_ID: $localStorage.UserId, IMAGE: prodData.IMAGE_FILE, TEMPLATE_ID: inputTemplateId, ITEM_NO: prodData.FAMILY_NAME });
                        $localStorage.compareLineCardList = $rootScope.compareLineCardList;
                    }
                }
                else {
                    $.msgBox({
                        title: "LG",
                        content: "Selected item does not associated with any template.",
                        type: "info"
                    });
                }
            }
        })
    }
    //--------------------------------------------Compare hover popup------------------------------------------//
    $scope.openHoverPopup = false;
    $scope.compareHover = function () {
        if (!$scope.openHoverPopup)
            $scope.openHoverPopup = true;
    }
    $scope.compHoverLeave = function () {
        $scope.openHoverPopup = false;
    }
    $scope.CompareHoverRemove = function (removeProdId) {
        var productRemoveList = [];
        productRemoveList = $.grep($rootScope.compareLineCardList, function (v) {
            return v.PRODUCT_ID != removeProdId;
        });
        $rootScope.compareLineCardList = productRemoveList;
        $localStorage.compareLineCardList = $rootScope.compareLineCardList;
    }
    //----------------------------------------------------End--------------------------------------------------//
    $scope.lineCardPDFGeneationClick = function (generationType) {
        if ($rootScope.compareLineCardList.length > 1 && $rootScope.compareLineCardList.length <= 16) {
            dataFactory.lineCardPDFGeneration(generationType, $rootScope.compareLineCardList).success(function (lineCardRes) {
                if (lineCardRes != null) {
                    $scope.previewFilePath = lineCardRes;
                    if (generationType == 'download') {
                        if ($scope.previewFilePath.toLowerCase().indexOf('.pdf') > -1) {
                            $scope.ToDownload();
                        }
                    }
                    else if (generationType == 'preview') {
                        if ($scope.previewFilePath.toLowerCase().indexOf('.pdf') > -1) {
                            $scope.previewWindow($scope.previewFilePath);
                        }
                    }
                }
            })
        }
        else {
            $.msgBox({
                title: "LG",
                content: "Kindly select 2-16 items to compare.",
                type: "info"
            });
        }
    }
    //--------------------------------------------Compare Process----------------------------------------------//
}]);


