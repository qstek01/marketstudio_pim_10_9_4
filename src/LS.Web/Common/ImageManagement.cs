﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LS.Web.Common
{
    public class ImageManagement
    {
      
        public string FolderName { get; set; }
        public string Extention { get; set; }
        public string Path { get; set; }
        public string CustomerFolder { get; set; }
      
    }
}