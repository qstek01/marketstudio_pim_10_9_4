﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace LS.Web.Common
{
    public enum UseCommandType
    {
        ExecuteNonQuery,
        ExecuteDataTable,
        ExecuteDataSet,
        ExecuteScalar,
        Returnparm,
        Execu
    }
    public class DBAccess
    {

        public SqlConnection objConnection = null;

        public SqlConnection GetConnection()
        {

            string ConnectionString = (ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString());
            objConnection = new SqlConnection(ConnectionString);
            if (objConnection.State != ConnectionState.Open)
            {
                objConnection.Open();

            }
            else
            {
                objConnection.Close();
                objConnection.Open();
            }
            return objConnection;
        }


        public object ExcecuteSTP(string StoredProcedureName, SqlParameter[] parameters, UseCommandType CommandType)
        {
            object output = null;

            try
            {
                GetConnection();

                using (SqlCommand cmd = new SqlCommand(StoredProcedureName, objConnection))
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Clear();
                    if (parameters.Length > 0)
                    {
                        cmd.Parameters.AddRange(parameters);
                    }
                    if (objConnection.State == ConnectionState.Open)
                    {
                        switch (CommandType)
                        {
                            case UseCommandType.ExecuteDataSet:
                                using (SqlDataAdapter objDataAdapter = new SqlDataAdapter(cmd))
                                {
                                    DataSet objDataSet = new DataSet();
                                    objDataAdapter.Fill(objDataSet);
                                    output = objDataSet;
                                    objDataSet.Dispose();
                                }
                                break;

                            case UseCommandType.ExecuteDataTable:
                                using (SqlDataAdapter objDataAdapter = new SqlDataAdapter(cmd))
                                {
                                    DataTable objDataTable = new DataTable();
                                    objDataAdapter.Fill(objDataTable);
                                    output = objDataTable;
                                    objDataTable.Dispose();
                                }
                                break;

                            case UseCommandType.ExecuteNonQuery:
                                output = cmd.ExecuteNonQuery();

                                break;

                            case UseCommandType.ExecuteScalar:
                                output = cmd.ExecuteScalar();
                                break;

                            case UseCommandType.Returnparm:
                                output = cmd.Parameters;
                                break;
                        }
                    }
                    cmd.Parameters.Clear();
                }
            }
            catch (Exception exception)
            {
                // LogUtil.WriteException("DBAccess", "ExcecuteSTP", exception);
            }
            finally
            {
                if (objConnection.State == ConnectionState.Open)
                {
                    objConnection.Close();
                }
                objConnection.Dispose();

            }
            return output;
        }




        public ConnectionState State { get; set; }


    }
}