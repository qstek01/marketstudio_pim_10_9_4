﻿using Infragistics.Documents.Excel;
using LS.Data;
using LS.Web.Controllers;
using LS.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace LS.Web
{
    /// <summary>
    /// Summary description for DownloadInvertedProductsExport
    /// </summary>
    public class DownloadInvertedProductsExport : IHttpHandler
    {

        private readonly CSEntities _dbcontext = new CSEntities();

        QueryValues queryValues = new QueryValues();
        HomeApiController homeObj = new HomeApiController();

        public void ProcessRequest(HttpContext context)
        {
            var a = (DataTable)System.Web.HttpContext.Current.Session["ExportTable"];
            var b = System.Web.HttpContext.Current.Session["seletedAttribute"];
            string s = JsonConvert.SerializeObject(b);
            var CustomizeItemNO = System.Web.HttpContext.Current.Session["CustomizeItemNO"];
            string ItemNO = JsonConvert.SerializeObject(CustomizeItemNO);
            // step 1  
            HttpResponse response = context.Response;
            string folderPath = HttpContext.Current.Request.QueryString["Path"];
            string filename = System.IO.Path.GetFileName(folderPath);
            if (context.Session["ExportTable"] != null)
            {
                //Name Changes In Item List
                if (a.Columns.Contains("PRODUCT_ID"))
                {
                    a.Columns["PRODUCT_ID"].ColumnName = "ITEM_ID";
                    a.AcceptChanges();
                }

                if (a.Columns.Contains("FAMILY_ID"))
                {
                    a.Columns["FAMILY_ID"].ColumnName = "PRODUCT_ID";
                    a.AcceptChanges();
                }

                if (a.Columns.Contains("FAMILY_NAME"))
                {
                    a.Columns["FAMILY_NAME"].ColumnName = "PRODUCT_NAME";
                    a.AcceptChanges();
                }
            }

            if (context.Session["ExportTable"] != null && ((filename.ToLower().Contains(".xls")) || (filename.ToLower().Contains(".xlsx"))))
            {
                //To create the document _Start


                var workbook = new Workbook();

                int desiredSize = 256;

                while (a.Columns.Count > desiredSize)
                {
                    a.Columns.RemoveAt(desiredSize);
                }

                if (a.Rows.Count > 0)
                {
                    const string tableName1 = "SearchExport";
                    string tableName = "Item List";

                    int rowcntTemp = a.Rows.Count;
                    int j = 0;
                    int runningcnt = 0;
                    do
                    {
                        int rowcnt;
                        if (rowcntTemp <= 65000)
                        {
                            rowcnt = rowcntTemp;
                            rowcntTemp = rowcnt - 65000;
                        }
                        else
                        {
                            rowcnt = 65000;
                            rowcntTemp = a.Rows.Count - (runningcnt + 65000);
                        }
                        j++;
                        if (j != 1)
                        {
                            tableName = tableName1 + (j - 1);
                        }
                        var worksheet = workbook.Worksheets.Add(tableName);
                        workbook.WindowOptions.SelectedWorksheet = worksheet;
                        for (int jj = 1; jj <= rowcnt; jj++)

                            // Create the worksheet to represent this data table


                            // Create column headers for each column
                            for (int columnIndex = 0; columnIndex < a.Columns.Count; columnIndex++)
                            {

                                if (HttpUtility.HtmlDecode(a.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(a.Columns[columnIndex].ColumnName) == "ITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(a.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                {
                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                }
                                else if (HttpUtility.HtmlDecode(a.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(a.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(a.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                {
                                    //if ((a.Columns[columnIndex].ColumnName).Contains("_"))
                                    //{
                                    //    a.Columns[columnIndex].ColumnName = (a.Columns[columnIndex].ColumnName).Replace('_', ' ');
                                    //}
                                    if ((a.Columns[columnIndex].ColumnName).Equals("STRING VALUE"))
                                    {

                                        a.Columns[columnIndex].ColumnName = s;
                                        a.Columns[columnIndex].ColumnName = (a.Columns[columnIndex].ColumnName).Replace('"', ' ');
                                    }
                                    if ((a.Columns[columnIndex].ColumnName).Equals("ITEM NO"))
                                    {

                                        a.Columns[columnIndex].ColumnName = ItemNO;
                                        a.Columns[columnIndex].ColumnName = (a.Columns[columnIndex].ColumnName).Replace('"', ' ');
                                    }

                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(a.Columns[columnIndex].ColumnName);
                                }
                            }

                        // Starting at row index 1, copy all data rows in
                        // the data table to the worksheet

                        int rowIndex = 2;
                        int temprunningcnt = runningcnt;
                        for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                        {
                            var row = worksheet.Rows[rowIndex++];
                            runningcnt++;
                            for (int columnIndex = 0; columnIndex < a.Rows[k].ItemArray.Length; columnIndex++)
                            {
                                row.Cells[columnIndex].Value = a.Rows[k].ItemArray[columnIndex];
                            }
                        }

                    }

                    while (rowcntTemp > 0);


                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("No records found.");
                }

                if (filename.ToLower().Contains(".xlsx"))
                {
                    workbook.SetCurrentFormat(WorkbookFormat.Excel2007);
                }
                workbook.Save(context.Server.MapPath("~/Content/" + filename));


                // to download file _ Start

                string _xmlPath = context.Server.MapPath("~/Content/");


                _xmlPath = string.Format("{0}{1}", _xmlPath, folderPath);
                System.Web.HttpContext.Current.Session["ExportTable"] = null;
                if (File.Exists(_xmlPath))
                {
                    response.ClearContent();
                    response.Clear();
                    response.ContentType = "text/plain";
                    response.AddHeader("Content-Disposition",
                                       "attachment; filename=" + filename + ";");
                    // response.TransmitFile((context.Session["Xmlpath"] + filename));
                    response.TransmitFile(_xmlPath);
                    response.Flush();
                    response.End();

                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("File not found!");

                }
                //End

            }

            else if (filename.ToLower().Contains(".txt"))
            {
                //Build the Text file data.
                string txt = string.Empty;

                foreach (DataColumn column in a.Columns)
                {
                    //Add the Header row for Text file.
                    if ((column.ColumnName).Contains("_"))
                    {
                        column.ColumnName = (column.ColumnName).Replace('_', ' ');
                    }
                    if ((column.ColumnName).Equals("STRING VALUE"))
                    {

                        column.ColumnName = s;
                        column.ColumnName = (column.ColumnName).Replace('"', ' ');
                    }
                    if ((column.ColumnName).Equals("ITEM NO"))
                    {

                        column.ColumnName = ItemNO;
                        column.ColumnName = (column.ColumnName).Replace('"', ' ');
                    }
                    txt += column.ColumnName + "\t\t";
                }

                //Add new line.
                txt += "\r\n\n";

                foreach (DataRow row in a.Rows)
                {
                    foreach (DataColumn column in a.Columns)
                    {
                        //Add the Data rows.
                        txt += row[column.ColumnName].ToString() + "\t\t";
                    }

                    //Add new line.
                    txt += "\r\n";
                }

                //Download the Text file.
                System.Web.HttpContext.Current.Session["ExportTable"] = null;
                response.Clear();
                response.Buffer = true;
                response.AddHeader("content-disposition", "attachment;filename=ResponseExport.txt");
                response.Charset = "";
                response.ContentType = "application/text";
                response.Output.Write(txt);
                response.Flush();
                response.End();

            }
            else if ((filename.ToLower().Contains(".csv")))
            {
                //Build the CSV file data as a Comma separated string.
                string csv = string.Empty;

                foreach (DataColumn column in a.Columns)
                {
                    //Add the Header row for CSV file.
                    if ((column.ColumnName).Contains("_"))
                    {
                        column.ColumnName = (column.ColumnName).Replace('_', ' ');
                    }
                    if ((column.ColumnName).Equals("STRING VALUE"))
                    {

                        column.ColumnName = s;
                        column.ColumnName = (column.ColumnName).Replace('"', ' ');
                    }
                    if ((column.ColumnName).Equals("ITEM NO"))
                    {

                        column.ColumnName = ItemNO;
                        column.ColumnName = (column.ColumnName).Replace('"', ' ');
                    }
                    csv += column.ColumnName + ',';
                }

                //Add new line.
                csv += "\r\n";

                foreach (DataRow row in a.Rows)
                {
                    foreach (DataColumn column in a.Columns)
                    {
                        //Add the Data rows.

                        csv += row[column.ColumnName].ToString().Replace(",", ";") + ',';
                    }

                    //Add new line.
                    csv += "\r\n";
                }

                //Download the CSV file.
                response.Clear();
                response.Buffer = true;
                response.AddHeader("content-disposition", "attachment;filename=ResponseExport.csv");
                response.Charset = "";
                response.ContentType = "application/text";
                response.Output.Write(csv);
                response.Flush();
                response.End();

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}