﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using log4net;

namespace LS.Web.Utility
{
    public sealed class ImportPool
    {
        static readonly ILog Logger = null;
        static ImportPool _instance;
        static readonly object Padlock = new object();
        static readonly object Padlock1 = new object();
        List<UserDetail> _listofUserDetails;

        ImportPool()
        {
        }

        public class UserDetail
        {
            public string User { get; set; }

            public bool IsInUse { get; set; }

            public int UsageCount { get; set; }
        }
        public static ImportPool Instance
        {
            get
            {
                lock (Padlock)
                {
                    if (_instance != null) return _instance;
                    _instance = new ImportPool();

                    if (Instance._listofUserDetails != null)
                        return _instance;

                    int noOfMaxUsers = Convert.ToInt16(ConfigurationManager.AppSettings["NoOfMaxUsers"].ToString(CultureInfo.InvariantCulture));

                    Instance._listofUserDetails = new List<UserDetail>();
                    for (int i = 0; i < noOfMaxUsers; i++)
                    {
                        Instance._listofUserDetails.Add(new UserDetail { User = (i + 1).ToString(CultureInfo.InvariantCulture) });
                    }
                }
                return _instance;
            }
        }

        public UserDetail GetUsersDetail()
        {
            try
            {
                while (true)
                {
                    DateTime inTime;
                    lock (Padlock1)
                    {
                        inTime = DateTime.Now;
                        if (_listofUserDetails.Count == 0)
                        {
                            return null;
                        }
                        _listofUserDetails = new List<UserDetail>(_listofUserDetails.OrderBy(x => x.UsageCount));
                        foreach (UserDetail t in _listofUserDetails.Where(t => t != null && !t.IsInUse))
                        {
                            t.IsInUse = true;
                            t.UsageCount++;
                            return t;
                        }
                    }
                   // int maxLockTime = Convert.ToInt16(ConfigurationManager.AppSettings["MaxLockTime"].ToString(CultureInfo.InvariantCulture));
                    //if ((DateTime.Now - inTime).TotalMilliseconds < maxLockTime)
                    //{
                      //  Thread.Sleep(60000);
                   // }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ImportPool - " + "GetUsersDetail", ex);
                return null;
            }
        }

        public void ReleaseUserDetail(string user)
        {
            try
            {
                lock (Padlock1)
                {
                    foreach (var userItem in _listofUserDetails.Where(x => x.User == user && x.IsInUse))
                    {
                        userItem.IsInUse = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("ImportPool - " + "ReleaseUserDetail", ex);
            }
        }
    }
}