﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LS.Data;
using LS.Data.Model;

namespace LS.Web.Utility
{
    public class CurrentUtility
    {

        private static string DefaultPage
        {
            get { return CommonUtility.GetCustomString("DefaultValues", "Defaultpagevalue"); }
        }
        //private static bool UseSubDomain
        //{
        //    get { return MyApplicationState.Instance.UseSubdomain; }
        //}
        public static CustomerModel GetCurrentCustomer()
        {
            //CustomerModel subDomainUser = GetAnonymousDetailsFromClientCookie();
            //user aUser = GetCurrentUser();
            
            return null;
        }
       
        //public static void SetAnonymousCookie(School aSchool, bool isEmbed, bool isNarrowEmbed)
        //{
        //    EduvisionUser oUser = new EduvisionUser
        //    {
        //        SchoolId = aSchool.SchoolId,
        //        SubDomainName = aSchool.SubdomainName.ToLower(),
        //        Email = "",
        //        FirstName = "",
        //        LastName = "",
        //        RoleId = 0,
        //        ID = 0,
        //        LoginID = "",
        //        FromSchoolId = 0,
        //        IsAdminAsSsu = false,
        //        IsNarrowEmbed = isNarrowEmbed,
        //        IsEmbed = isEmbed,
        //        ClientGuid = Guid.NewGuid().ToString(),
        //        IsLiveEventManagementEnabled = 0,
        //        IsByPassUser = false,
        //        CreatedByID = 0,
        //    };
        //    ValidateUserCredentials.SetUserCookie(oUser);
        //}

        //public static School CurrentSchool(SchoolCache oCacheObject)
        //{
        //    School aSchool = new School
        //    {
        //        SchoolName = oCacheObject.SchoolName,
        //        SchoolId = oCacheObject.SchoolId,
        //        Website = oCacheObject.Website,
        //        isActive = oCacheObject.isActive,
        //        isLiveSchool = oCacheObject.isLiveSchool,
        //        SchoolURL = oCacheObject.SchoolURL,
        //        SubdomainName = oCacheObject.SubdomainName,
        //        IsPaymentSchool = oCacheObject.IsPaymentSchool,
        //        VideoPlayTime = oCacheObject.VideoPlayTime,
        //        IsViewerAllowed = oCacheObject.IsViewerAllowed,
        //        EmbedDefaultPageRowCount = oCacheObject.EmbedDefaultPageRowCount,
        //        EmbedPlayerToolTip = oCacheObject.EmbedPlayerToolTip,
        //        DefaultPageRowCount = oCacheObject.DefaultPageRowCount,
        //        IsContactAllowed = oCacheObject.IsContactAllowed,
        //        IsUploadButtonVisible = oCacheObject.IsUploadButtonVisible,
        //        TimeZone = oCacheObject.TimeZone,
        //        IsAdminSchool = oCacheObject.IsAdminSchool,
        //        EmbedUrl = oCacheObject.EmbedUrl,
        //        EnableRating = oCacheObject.EnableRating,
        //        EnableAddThisShare = oCacheObject.EnableAddThisShare,
        //        EnableDownload = oCacheObject.EnableDownload,
        //        EnableShareSA = oCacheObject.EnableShareSA,
        //        EnableEmbedShareSA = oCacheObject.EnableEmbedShareSA,
        //        IsScheduleOverlapAllowed = oCacheObject.IsScheduleOverlapAllowed,
        //        EnableLiveShare = oCacheObject.EnableLiveShare,
        //        EnableLiveShareSA = oCacheObject.EnableLiveShareSA,
        //        EnablePPV = oCacheObject.EnablePPV,
        //        EnablePPVSA = oCacheObject.EnablePPVSA,
        //        EnableCaption = oCacheObject.EnableCaption,
        //        EnableViewCount = oCacheObject.EnableViewCount,
        //        EnableComment = oCacheObject.EnableComment,
        //        EnableModeratedComment = oCacheObject.EnableModeratedComment,
        //        EnableOpenComment = oCacheObject.EnableOpenComment,
        //        EnableCommentSA = oCacheObject.EnableCommentSA,
        //        EnableModeratedCommentSA = oCacheObject.EnableModeratedCommentSA,
        //        EnableOpenCommentSA = oCacheObject.EnableOpenCommentSA,
        //        EnableLiveComment = oCacheObject.EnableLiveComment,
        //        EnableLiveModeratedComment = oCacheObject.EnableLiveModeratedComment,
        //        EnableLiveOpenComment = oCacheObject.EnableLiveOpenComment,
        //        EnableLiveCommentSA = oCacheObject.EnableLiveCommentSA,
        //        EnableLiveModeratedCommentSA = oCacheObject.EnableLiveModeratedCommentSA,
        //        EnableLiveOpenCommentSA = oCacheObject.EnableLiveOpenCommentSA,
        //        EnablePriceDescription = oCacheObject.EnablePriceDescription,
        //        PrivateVideoViewCount = oCacheObject.PrivateVideoViewCount,
        //        PageViewCount = oCacheObject.PageViewCount,
        //        IsBusinessSite = oCacheObject.IsBusinessSite,
        //        TerritoryStatus = oCacheObject.TerritoryStatus,
        //        GSChannel = oCacheObject.GSChannel,
        //        PrepayAccount = oCacheObject.PrepayAccount,
        //        FreeViewingDays = oCacheObject.FreeViewingDays,
        //        IsEduDocsEnabled = oCacheObject.IsEduDocsEnabled,
        //        IsLiveEduDocsEnabled = oCacheObject.IsLiveEduDocsEnabled,
        //        PaymentGateway = oCacheObject.PaymentGateway,
        //        IsPrivateSite = oCacheObject.IsPrivateSite,
        //        IsLiveEnabledBySA = oCacheObject.IsLiveEnabledBySA,
        //        IsDefaultVideoAutoplay = oCacheObject.IsDefaultVideoAutoplay,
        //        ViewCountInterval = oCacheObject.ViewCountInterval,
        //        Vodurl = oCacheObject.Vodurl,
        //        Liveurl = oCacheObject.Liveurl,
        //        PlayerVolume = oCacheObject.PlayerVolume,
        //        ReferalSchoolID = oCacheObject.ReferalSchoolID,
        //        IsRelayAccess = oCacheObject.IsRelayAccess,
        //        IsLiveRecurrenceEnabled = oCacheObject.IsLiveRecurrenceEnabled,
        //        IsCreateChannelForCM = oCacheObject.IsCreateChannelForCM,
        //        AllowPasswordFields = oCacheObject.AllowPasswordFields,
        //        IsPointStreakSite = oCacheObject.IsPointStreakSite,
        //        AllowLiveABR = oCacheObject.AllowLiveABR,
        //        IsMultiTypeUploads = oCacheObject.IsMultiTypeUploads,
        //        IsChannelDefault = oCacheObject.IsChannelDefault,
        //        IsViwerLogin = oCacheObject.IsViwerLogin,
        //        IsSiteEmbedded = oCacheObject.IsSiteEmbedded,
        //        IsSubDomainHideFromGoogle = oCacheObject.IsSubDomainHideFromGoogle,
        //        IsChapterEnabled = oCacheObject.IsChapterEnabled
        //    };
        //    aSchool.IsBusinessSite = oCacheObject.IsBusinessSite;
        //    aSchool.CreatedUser = oCacheObject.CreatedUser;
        //    aSchool.SchoolTheme = oCacheObject.SchoolTheme;
        //    return aSchool;
        //}

        public static string ReadUserMessage(string pageToRead, string messageToRead)
        {
            return CommonUtility.GetCustomString(pageToRead, messageToRead);
        }
       
    }
}