﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LS.Web.Utility
{
    public class MyApplicationState
    {
        public string GetConfigValues(string valueToread)
        {
            return CommonUtility.GetCustomString("WebConfigSettings", valueToread);
        }
    }
}