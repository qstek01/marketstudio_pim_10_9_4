﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace LS.Web.Utility
{
    public class CommonUtility
    {
        public static string GetSubDomain(Uri url)
        {
            if (url.HostNameType != UriHostNameType.Dns) return null;
            string host = url.Host.Replace("www.", "");
            if (host.Split('.').Length <= 2) return null;
            int lastIndex = host.LastIndexOf(".");
            int index = host.LastIndexOf(".", lastIndex - 1);
            return host.Substring(0, index).ToLower().Replace("www.", "");
        }

        public static string GetVirtualPath
        {
            get
            {
                string path = HttpRuntime.AppDomainAppVirtualPath;
                if (path != "/")
                    path += "/";
                return path.ToLower();
            }
        }

        public static string GetDomainName
        {
            get
            {
                return HttpContext.Current.Request.Url.Scheme + "://"; // + MyApplicationState.Instance.DomainName; 
            }
        }

        public static DateTime GetCurrentDateTime
        {
            get { return ConvertToZone(DateTime.Now, GetTimeZone); }
        }

        public static DateTime GetCurrentDate
        {
            get { return DateTime.Now.Date; }
        }

        private static string ListTimeZones(string zoneId)
        {
            var sortedList = new Dictionary<string, string>();
            string timeZones = GetCustomString("TimeZones", "ListtimeZones");
            foreach (string str in timeZones.Split(','))
            {
                sortedList.Add(str.Split(':')[0], str.Split(':')[1]);
            }
            string valueToread;
            sortedList.TryGetValue(zoneId, out valueToread);
            return valueToread;
        }

        public static string GetCustomString(string pageName, string valueToRead)
        {
            // return XMLParser.ReadFromXml(XmlDoc, pageName, valueToRead);
            return null;
        }

        public static bool IsNull(object obj)
        {
            return obj == null;
        }

        private static string GetTimeZone
        {
            get { return (GetCustomString("TimeZones", "CstTimeZones")); }
        }

        public static bool CheckOneHourBefore(DateTime startTime)
        {
            return startTime.AddHours(-1) <= DateTime.Now && DateTime.Now <= startTime;
        }
        public static DateTime CurrentDateTime()
        {
            return DateTime.Now;
        }
        public static DateTime CurrentDate()
        {
            return DateTime.Now;
        }
        public static string ReturnCurrentBasePath()
        {
            return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority +
            VirtualPathUtility.GetDirectory(HttpContext.Current.Request.CurrentExecutionFilePath);
        }
        public static string GetSubDomainUrl(string subDomain)
        {
            return GetDomainName.ToLower().Replace("www", subDomain);
        }
        public static DateTime GetCurrentDateTimeOfSpecificzone(string zoneId)
        {
            return ConvertToZone(DateTime.Now, zoneId);
        }
        public static DateTime GetMinusOneHourTime(object dateTime)
        {
            return ConvertToZone((DateTime)(dateTime), GetTimeZone).AddHours(-1);
        }
        public static DateTime GetDateTimeFromObject(object dateTime)
        {
            return ConvertToZone(Convert.ToDateTime(dateTime), GetTimeZone);
        }
        public static string AddGuid(string fileName)
        {
            if (string.IsNullOrEmpty(fileName)) return "";
            fileName = Path.GetFileName(fileName);
            fileName = fileName.Replace(".", "_" + Guid.NewGuid() + ".");
            fileName = Regex.Replace(fileName, "[^a-zA-Z0-9_.]", "");
            fileName = fileName.Replace(" ", "_").ToLower().Replace("aac", "234");
            return fileName;
        }

        public static string FormatString(string filePath, int cutPoint)
        {
            string formatedValue = filePath;
            if (string.IsNullOrEmpty(filePath))
                return null;
            if (cutPoint <= filePath.Length)
            {
                formatedValue = filePath.Insert(cutPoint, "<br />");
                formatedValue = FormatString(formatedValue, cutPoint + cutPoint);
            }
            return formatedValue;
        }

        public static void Delete(ArrayList files)
        {
            foreach (string fileName in files)
            {
                if (!File.Exists(fileName)) continue;
                File.Delete(fileName);
            }
        }

        public static DateTime ConvertToZone(DateTime dt)
        {
            //if (CurrentUtility.GetCurrentCustomer().TimeZone.Length > 4)
            //    return TimeZoneConverter.ConvertToTimeZone(CurrentUtility.GetCurrentCustomer().TimeZone, dt);
            //return TimeZoneConverter.ConvertToTimeZone(ListTimeZones(CurrentUtility.GetCurrentCustomer().TimeZone), dt);
            return new DateTime();
        }

        public static DateTime ConvertToZone(DateTime dt, string zoneId)
        {
            if (zoneId.Length > 4)
                return TimeZoneConverter.ConvertToTimeZone(zoneId, dt);
            return TimeZoneConverter.ConvertToTimeZone(ListTimeZones(zoneId), dt);
        }

        public static DateTime ConvertToZone(DateTime dt, string sourceZoneId, string destinationZoneId)
        {
            if (destinationZoneId.Length > 4)
                return TimeZoneConverter.ConvertToTimeZone(ListTimeZones(sourceZoneId), destinationZoneId, dt);
            return TimeZoneConverter.ConvertToTimeZone(ListTimeZones(sourceZoneId), ListTimeZones(destinationZoneId), dt);
        }
        
    }
}