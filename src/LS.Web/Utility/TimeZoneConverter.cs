﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace LS.Web.Utility
{
    public class TimeZoneConverter
    {
        public static DateTime ConvertToTimeZone(string zoneId, DateTime dateToConvert)
        {
            if (zoneId == null) return dateToConvert;

            TimeZoneInfo zone = TimeZoneInfo.FindSystemTimeZoneById(zoneId);
            DateTime converted = TimeZoneInfo.ConvertTime(dateToConvert, zone);
            return converted;
        }

        public static DateTime ConvertToTimeZone(string sourceZoneId, string destinationZoneId, DateTime dateToConvert)
        {
            if (sourceZoneId == null) return dateToConvert;
            if (destinationZoneId == null) return dateToConvert;

            TimeZoneInfo sourceZone = TimeZoneInfo.FindSystemTimeZoneById(sourceZoneId);
            TimeZoneInfo destinationZone = TimeZoneInfo.FindSystemTimeZoneById(destinationZoneId);
            DateTime converted = TimeZoneInfo.ConvertTime(dateToConvert, sourceZone, destinationZone);
            return converted;
        }


        public static SortedList<string, string> ListAllTimeZones()
        {
            ReadOnlyCollection<TimeZoneInfo> allZones = TimeZoneInfo.GetSystemTimeZones();
            SortedList<string, string> alltimeZones = new SortedList<string, string> {{"-1", "Select Zone"}};
            foreach (TimeZoneInfo zone in allZones)
            {
                string value = zone.Id.Split(' ')[0].Substring(0, 1) + zone.Id.Split(' ')[1].Substring(0, 1) +
                               zone.Id.Split(' ')[2].Substring(0, 1);
                alltimeZones.Add(zone.Id, (value + "(" + zone.Id + ")"));
            }
            return alltimeZones;
        }

    }
}