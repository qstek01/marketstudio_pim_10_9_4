﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.IO;
using System.IO.Compression;
using Microsoft.Office.Interop;
using System.Web.SessionState;
using LS.Data;
using LS.Web.Controllers;
using LS.Web.Models;
using Newtonsoft.Json;
using DataTable = System.Data.DataTable;
using Workbook = Infragistics.Documents.Excel.Workbook;
using Infragistics.Documents.Excel;

namespace LS.Web
{
    /// <summary>
    /// Summary description for DownloadAttributesExport
    /// </summary>
    public class DownloadAttributesExport : IHttpHandler
    {
        private readonly CSEntities _dbcontext = new CSEntities();

        public void ProcessRequest(HttpContext context)
        {
            var userExportTable = (DataTable)System.Web.HttpContext.Current.Session["AttributeExport"];

            //TO Remove Export & portal 
            if (userExportTable.Columns.Contains("PUBLISH2EXPORT"))
            {
                userExportTable.Columns.Remove("PUBLISH2EXPORT");

            }
            if (userExportTable.Columns.Contains("PUBLISH2PORTAL"))
            {
                userExportTable.Columns.Remove("PUBLISH2PORTAL");

            }

            string filename = "AttributesExportFile.xls";
            HttpResponse response = context.Response;
            try
            {
                var workbook = new Workbook();
                if (userExportTable.Rows.Count > 0)
                {
                    string tableName = "Attribute";
                    int rowcntTemp = userExportTable.Rows.Count;
                    int j = 0;
                    int runningcnt = 0;
                    do
                    {
                        int rowcnt;
                        if (rowcntTemp <= 65000)
                        {
                            rowcnt = rowcntTemp;
                            rowcntTemp = rowcnt - 65000;
                        }
                        else
                        {
                            rowcnt = 65000;
                            rowcntTemp = userExportTable.Rows.Count - (runningcnt + 65000);
                        }
                        j++;
                        if (j != 1)
                        {
                            tableName = tableName + (j - 1);
                        }
                        var worksheet = workbook.Worksheets.Add(tableName);
                        workbook.WindowOptions.SelectedWorksheet = worksheet;
                        for (int columnIndex = 0; columnIndex < userExportTable.Columns.Count; columnIndex++)
                        {

                            // if (HttpUtility.HtmlDecode(userExportTable.Columns[columnIndex].ColumnName) != "ID" && HttpUtility.HtmlDecode(userExportTable.Columns[columnIndex].ColumnName) != "ROLE_ID" && HttpUtility.HtmlDecode(userExportTable.Columns[columnIndex].ColumnName) != "CATALOGID")
                            //  {


                            worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(userExportTable.Columns[columnIndex].ColumnName);

                            //  }
                        }
                        int rowIndex = 2;
                        int temprunningcnt = runningcnt;
                        for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                        {
                            var row = worksheet.Rows[rowIndex++];
                            runningcnt++;
                            for (int columnIndex = 0; columnIndex < userExportTable.Rows[k].ItemArray.Length; columnIndex++)
                            {
                                row.Cells[columnIndex].Value = userExportTable.Rows[k].ItemArray[columnIndex];
                            }
                        }

                    }
                    while (rowcntTemp > 0);
                }
                else
                {

                    context.Response.ContentType = "text/plain";
                    context.Response.Write("No records found.");
                }
                workbook.Save(context.Server.MapPath("~/Content/" + filename));
                if (File.Exists(context.Server.MapPath("~/Content/" + filename)))
                {
                    response.ClearContent();
                    response.Clear();
                    response.ContentType = "application/vnd.ms-excel";
                    response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", filename));
                    response.TransmitFile(context.Server.MapPath("~/Content/" + filename));

                    response.Flush();
                    response.End();
                }
                else
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("File not found!");
                }

            }
            catch (Exception ex)
            {


            }
        }



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}