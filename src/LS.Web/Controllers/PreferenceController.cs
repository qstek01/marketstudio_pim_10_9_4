﻿using LS.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LS.Web.Controllers
{
    public class PreferenceController : Controller
    {
        CSEntities objLS = new CSEntities();
        Customer_Settings objCustomer_Settings = new Customer_Settings();
        public ActionResult SaveBanner(IEnumerable<HttpPostedFileBase> files)
        {
            try
            {
                
                int customerID = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name.Trim()).CustomerId;
                int id = customerID;
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        if (fileName != null)
                        {
                            var physicalPath = Path.Combine(Server.MapPath("~/Images"), fileName);
                            file.SaveAs(physicalPath);
                            var objcatalog = objLS.Customer_Settings.FirstOrDefault(a => a.CustomerId == customerID);
                            objcatalog.bannerpath = fileName; //physicalPath;
                            objLS.SaveChanges();
                        }

                    }
                }

            }
            catch (Exception ex)
            {

            }
            return Content("");
        }
        public ActionResult SaveLogo(IEnumerable<HttpPostedFileBase> files)
        {
            try
            {
                int customerID = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name.Trim()).CustomerId;
                int id = customerID;
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        if (fileName != null)
                        {
                            var physicalPath = Path.Combine(Server.MapPath("~/Images"), fileName);
                            file.SaveAs(physicalPath);
                            var objcatalog = objLS.Customer_Settings.FirstOrDefault(a => a.CustomerId == customerID);
                            objcatalog.logopath = fileName;
                            objLS.SaveChanges();
                        }

                    }
                }

            }
            catch (Exception ex)
            { }
            return Content("");
        }
        public ActionResult ChangeTheme(string SelectedTheme)
        {
            try
            {
                int customerID = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name.Trim()).CustomerId;
                int id = customerID;
                if (customerID != null)
                {
                    var objcatalog = objLS.Customer_Settings.FirstOrDefault(a => a.CustomerId == customerID);
                    objcatalog.Theme = SelectedTheme;
                    objLS.SaveChanges();
                }

            }
            catch (Exception ex)
            { }
            return Content("");
        }
        public ActionResult RemoveBannerImage(string[] fileNames)
        {
            try
            {
                if (fileNames != null)
                {
                    foreach (var fullName in fileNames)
                    {
                        var fileName = Path.GetFileName(fullName);
                        if (fileName != null)
                        {
                            var physicalPath = Path.Combine(Server.MapPath("~/App_Data"), fileName);

                            // TODO: Verify user permissions

                            if (System.IO.File.Exists(physicalPath))
                            {
                                // The files are not actually removed in this demo
                                // System.IO.File.Delete(physicalPath);
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { }
            return Content("");
        }
        // GET: Preference
        public ActionResult Index()
        {
            return View();
        }

        // GET: Preference/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Preference/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Preference/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Preference/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Preference/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Preference/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Preference/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [System.Web.Http.HttpGet]
        public string getmethod()
        {
            try
            {
                int Customer_ID = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                //return objLS.Customer_Settings.Where(a => a.CustomerId == Customer_ID).Select(x => new { x.Theme }).ToList();
                return objLS.Customer_Settings.FirstOrDefault(a => a.CustomerId == Customer_ID).Theme;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
