﻿using log4net;
using LS.Data;

using LS.Data.Model.WebSync;
using LS.Web.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Xml;
using System.Data;
using System.Web.Security;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Configuration;
using System.Diagnostics;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml.Linq;
using LS.Data.Model.WebSync;
using System.Globalization;

namespace LS.Web.Controllers
{
    public class WebSyncApiController : ApiController
    {
        private static ILog _logger = LogManager.GetLogger(typeof(WebSyncController));
        private CSEntities _dbContext = new CSEntities();
        private QueryValues queryValue;
        public string imagemagickpath;
        public string conStr = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        public string freeSync = System.Web.Configuration.WebConfigurationManager.AppSettings["FreeSync"].ToString();


        #region Get UserList
        /// <summary>
        /// Get All users of customer
        /// </summary>
        /// <returns>userList</returns>
        [System.Web.Http.HttpGet]
        public IList GetUserList()
        {
            _dbContext = new CSEntities();

            int CustomerId;
            int Id;
            string User_Name;
            string FirstName;
            var checkAdminUser = _dbContext.vw_aspnet_UsersInRoles.Join(_dbContext.aspnet_Users, aur => aur.UserId, au => au.UserId, (aur, au) => new { aur, au })
                .Join(_dbContext.aspnet_Roles, aurau => aurau.aur.RoleId, ar => ar.RoleId, (aurau, ar) => new { aurau, ar })
                .Where(x => x.aurau.au.UserName == User.Identity.Name).Select(y => y.ar.RoleType).Distinct().FirstOrDefault();

            if (checkAdminUser == 2 || checkAdminUser == 1)
            {

                int customerId = _dbContext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => x.CustomerId).Distinct().FirstOrDefault();
                var USerList = _dbContext.Customer_User.Join(_dbContext.Customer_User, cu => cu.CustomerId, cu1 => cu1.CustomerId, (cu, cu1) => new { cu, cu1 }).Where(x => x.cu.CustomerId == customerId)
                    .Select(y => new { y.cu1.CustomerId, y.cu1.Id, y.cu1.User_Name, y.cu1.FirstName }).Distinct().ToList();
                var USerDetails = _dbContext.Customer_User.Join(_dbContext.Customer_User, cu => cu.CustomerId, cu1 => cu1.CustomerId, (cu, cu1) => new { cu, cu1 }).Where(x => x.cu.CustomerId == customerId).
                    Select(y => new
                    {
                        CustomerId = y.cu1.CustomerId,
                        Id = -1,
                        User_Name = "ALL",
                        FirstName = "ALL"
                    }).Distinct().ToList();
                var userListDetails = USerDetails.Union(USerList);
                return userListDetails.ToList();
            }

            else
            {
                var user = _dbContext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => new { y.CustomerId, y.Id, y.User_Name, y.FirstName }).Distinct().ToList();
                return user;
            }
        }
        #endregion

        #region  Get Category details
        /// <summary>
        /// Get Category details between modified date fromdate and todate based on the username
        /// </summary>
        /// <param name="fromDate">fromdate from user selection in ui</param>
        /// <param name="toDate">todate from user selection in ui</param>
        /// <param name="catalogId">current catalogId</param>
        /// <param name="userName">current user selection in ui</param>
        /// <returns>Category details</returns>
        [System.Web.Http.HttpPost]
        public List<WebSyncCategoryDetails> GetFiltersDetails(DateTime fromDate, DateTime toDate, int catalogId, string userName, string id)
        {
            try
            {

                _dbContext = new CSEntities();
                queryValue = new QueryValues();


                if (userName == null)
                {
                    List<WebSyncCategoryDetails> GetCategoryDetails1 = new List<WebSyncCategoryDetails>()
                    { new WebSyncCategoryDetails {
                        CATEGORY_ID = "",
                        CATEGORY_NAME = "",
                        Parent_Category = "",
                        Modify_Date =DateTime.Now

                    } };
                    GetCategoryDetails1.Remove(GetCategoryDetails1[0]);
                    return GetCategoryDetails1;
                }
                var customerList = _dbContext.Customer_User.Where(x => x.User_Name != userName).Select(y => y.User_Name).Distinct().ToList();
                if (userName.ToUpper() == "ALL")
                {
                    userName = "";
                }
                else
                {
                    userName = "";
                    foreach (var user in customerList)
                    {
                        userName = userName + user + ",";
                    }
                    userName = userName.TrimEnd(',');
                }
                DateTime webSyncFromDate = Convert.ToDateTime(fromDate.ToString());
                DateTime webSyncTodate = Convert.ToDateTime(toDate.ToString());
                webSyncTodate = webSyncTodate.AddDays(1);
                var webSyncToDate = toDate;
                int customerId = 0;
                var customer = _dbContext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).ToList();
                if (customer.Count > 0)
                {
                    int.TryParse(customer[0].ToString(), out customerId);
                }
                int customer_Id;
                if (id == "undefined")
                {
                    if (catalogId != 1)
                    {
                        var customersettings = _dbContext.Customer_Settings.Join(_dbContext.Customer_User, tps => tps.CustomerId,
                            tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                            .Where(x => x.tpf.User_Name == User.Identity.Name)
                            .Select(x => x.tps);
                        var customerSettings = customersettings.FirstOrDefault();
                        if (userName.ToLower() == "tbadmin")
                        {
                            userName = "All";
                        }
                        bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                        var firstOrDefault = customersettings.FirstOrDefault();
                        bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                        customer_Id = Convert.ToInt32(customerSettings.CustomerId);
                        var aa =
                            _dbContext.TB_CATALOG_SECTIONS.Where(
                                a => a.CATALOG_ID == catalogId && _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && !userName.Contains(tc.CREATED_USER) && tc.MODIFIED_DATE >= webSyncFromDate && tc.MODIFIED_DATE <= webSyncTodate && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customerId).Select(tcsel => tcsel.CATEGORY_ID).FirstOrDefault().Contains(a.CATEGORY_ID))
                                .Select(
                                    a =>
                                        new WebSyncCategoryDetails
                                        {
                                            id = a.CATEGORY_ID,
                                            CATEGORY_ID = a.CATEGORY_ID,
                                            CATEGORY_SHORT = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customerId).Select(tcsel => tcsel.CATEGORY_SHORT).FirstOrDefault(),
                                            CUSTOMER_ID = _dbContext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name).CustomerId,
                                            CATEGORY_NAME = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customerId).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                            CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                            CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                            hasChildren = _dbContext.TB_CATEGORY.Join(_dbContext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) || _dbContext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId).Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbContext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)) || _dbContext.TB_CATEGORY.Any(x => x.CATEGORY_ID == a.CATEGORY_ID && x.FLAG_RECYCLE == "A"),
                                            SORT_ORDER = a.SORT_ORDER,
                                            CategoryIdinNavigator = categoryidinnavigator,
                                            FamilyandRelatedFamily = familyandrelatedfamily,
                                            spriteCssClass = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.IS_CLONE).FirstOrDefault() == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryCloneWebSync" : "categoryWebSync",
                                            Modify_Date = a.TB_CATEGORY.MODIFIED_DATE,
                                            STATUS = "UPDATED"
                                        }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();

                        if (aa != null && aa.Count() > 0)

                            return aa;

                        var catTreeData = GetFamilyList(null, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);
                        if (catTreeData != null && catTreeData.Count() > 0)
                            return catTreeData;
                        catTreeData = GetProductList(null, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);
                        if (catTreeData != null && catTreeData.Count() > 0)
                            return catTreeData;
                        //catTreeData = GetSubProductList(webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);
                        //if (catTreeData != null && catTreeData.Count() > 0)
                        //    return catTreeData;

                    }
                    else
                    {
                        var customersettings = _dbContext.Customer_Settings.Join(_dbContext.Customer_User, tps => tps.CustomerId,
                          tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                          .Where(x => x.tpf.User_Name == User.Identity.Name)
                          .Select(x => x.tps);
                        int userroleid = 0;
                        int userroletype = 0;
                        var SuperAdminCheck = _dbContext.vw_aspnet_UsersInRoles
                        .Join(_dbContext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                        .Join(_dbContext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                        .Where(x => x.tcptps.tpf.UserName == User.Identity.Name).ToList();
                        if (SuperAdminCheck.Any())
                        {
                            userroleid = Convert.ToInt16(SuperAdminCheck[0].tcp.Role_id);
                            userroletype = Convert.ToInt16(SuperAdminCheck[0].tcp.RoleType);
                        }

                        var customerSettings = customersettings.FirstOrDefault();
                        bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                        var firstOrDefault = customersettings.FirstOrDefault();
                        bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                        customer_Id = Convert.ToInt32(customerSettings.CustomerId);
                        var aa = _dbContext.TB_CATALOG_SECTIONS.Where(
                             a => a.CATALOG_ID == catalogId && _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && !userName.Contains(tc.CREATED_USER) && tc.MODIFIED_DATE >= webSyncFromDate && tc.MODIFIED_DATE <= webSyncTodate && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.PARENT_CATEGORY).FirstOrDefault().Contains(a.CATEGORY_ID))
                             .Select(
                                 a =>
                                     new WebSyncCategoryDetails
                                     {
                                         id = a.CATEGORY_ID,
                                         CATEGORY_ID = a.CATEGORY_ID,
                                         CATEGORY_SHORT = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_SHORT).FirstOrDefault(),
                                         CUSTOMER_ID = customer_Id,
                                         CATEGORY_NAME = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                         CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                         CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                         //  DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                         // VERSION = a.TB_CATALOG.VERSION,
                                         hasChildren =
                                         _dbContext.TB_CATEGORY.Join(_dbContext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) || _dbContext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId)
                                             .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && c.FLAG_RECYCLE == "A" && _dbContext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)) || _dbContext.TB_CATEGORY.Any(x => x.CATEGORY_ID == a.CATEGORY_ID && x.FLAG_RECYCLE == "A"),
                                         SORT_ORDER = a.SORT_ORDER,
                                         CategoryIdinNavigator = categoryidinnavigator,
                                         FamilyandRelatedFamily = familyandrelatedfamily,
                                         spriteCssClass = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.IS_CLONE).FirstOrDefault() == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryCloneWebSync" : "categoryWebSync",
                                         Modify_Date = a.TB_CATEGORY.MODIFIED_DATE,
                                         @checked = _dbContext.TB_CATALOG_SECTIONS.Any(x => x.CATALOG_ID == catalogId && x.CATEGORY_ID == a.CATEGORY_ID),
                                         STATUS = "UPDATED"
                                     }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();




                        if (userroleid == 1)
                        {
                            var names = _dbContext.TB_CATEGORY
                              .Join(_dbContext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf })
                              .Join(_dbContext.Customer_Catalog, tcptps => tcptps.tpf.CATALOG_ID, tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                              .Join(_dbContext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId, (tcptpscs, cs) => new { tcptpscs, cs })
                              .Where(x => x.cs.User_Name == User.Identity.Name && x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" && x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" && x.tcptpscs.tcptps.tpf.CATALOG_ID != 1).Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);
                            var results = aa.Where(b => names.Contains(b.id)).ToList();
                            return results;
                        }
                        else
                        {
                            if (userroletype == 3)
                            {
                                var names = _dbContext.TB_CATEGORY
                                    .Join(_dbContext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID,
                                        (tps, tpf) => new { tps, tpf })
                                    .Join(_dbContext.Customer_Role_Catalog, tcptps => tcptps.tpf.CATALOG_ID,
                                        tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                                    .Join(_dbContext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId,
                                        (tcptpscs, cs) => new { tcptpscs, cs })
                                    .Where(
                                        x =>
                                            x.cs.User_Name == User.Identity.Name &&
                                            x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" && x.tcptpscs.tcptps.tps.CREATED_USER == userName && x.tcptpscs.tcptps.tps.MODIFIED_DATE >= webSyncFromDate && x.tcptpscs.tcptps.tps.MODIFIED_DATE <= webSyncTodate &&
                                            x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" &&
                                            x.tcptpscs.tcptps.tpf.CATALOG_ID != 1 && x.tcptpscs.tcp.RoleId == userroleid &&
                                            x.tcptpscs.tcp.IsActive == true)
                                    .Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);

                                var results = aa.Where(b => names.Contains(b.id)).ToList();
                                return results;
                            }
                            else
                            {
                                var names = _dbContext.TB_CATEGORY
                                   .Join(_dbContext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID,
                                       (tps, tpf) => new { tps, tpf })
                                   .Join(_dbContext.Customer_Role_Catalog, tcptps => tcptps.tpf.CATALOG_ID,
                                       tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                                   .Join(_dbContext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId,
                                       (tcptpscs, cs) => new { tcptpscs, cs })
                                   .Where(
                                       x =>
                                           x.cs.User_Name == User.Identity.Name &&
                                           x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" && !userName.Contains(x.tcptpscs.tcptps.tps.CREATED_USER) && x.tcptpscs.tcptps.tps.MODIFIED_DATE >= webSyncFromDate && x.tcptpscs.tcptps.tps.MODIFIED_DATE <= webSyncTodate &&
                                           x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" &&
                                           x.tcptpscs.tcptps.tpf.CATALOG_ID != 1 && x.tcptpscs.tcp.RoleId == userroleid)
                                   .Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);

                                var results = aa.Where(b => names.Contains(b.id)).ToList();
                                return results;
                            }
                        }
                    }
                }
                else
                {
                    string productIdExists = string.Empty;
                    string subfamilycategoryid = string.Empty;
                    if (id.Contains("~"))
                    {
                        var catgoryandfamilyid = id.Split('~');
                        foreach (var catids in catgoryandfamilyid)
                        {
                            if (catids.Contains("CAT"))
                            {
                                subfamilycategoryid = catids;
                            }
                        }
                        id = catgoryandfamilyid[catgoryandfamilyid.Length - 1];
                        id = id.Contains("~") ? id.Replace("~", "") : id;
                        if (string.IsNullOrEmpty(catgoryandfamilyid[catgoryandfamilyid.Length - 2].ToString()))
                            productIdExists = catgoryandfamilyid[catgoryandfamilyid.Length - 1];
                    }
                    string productExists = id;
                    if (id.ToUpper().Contains("PRODUCTS"))
                    {
                        id = id.Replace("Products", "");
                    }
                    var customersettings = _dbContext.Customer_Settings.Join(_dbContext.Customer_User, tps => tps.CustomerId,
                          tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                          .Where(x => x.tpf.User_Name == User.Identity.Name)
                          .Select(x => x.tps);
                    var customerSettings = customersettings.FirstOrDefault();

                    bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                    var firstOrDefault = customersettings.FirstOrDefault();
                    bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                    customer_Id = Convert.ToInt32(customerSettings.CustomerId);
                    var aa = _dbContext.TB_CATALOG_SECTIONS.Where(
                        a => a.CATALOG_ID == catalogId && _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && !userName.Contains(tc.CREATED_USER) && tc.MODIFIED_DATE >= webSyncFromDate && tc.MODIFIED_DATE <= webSyncTodate &&
                        tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbContext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.PARENT_CATEGORY).FirstOrDefault() == id)
                        .Select(
                            a =>
                                new WebSyncCategoryDetails
                                {
                                    id = a.CATEGORY_ID,
                                    CATEGORY_ID = a.CATEGORY_ID,
                                    CATEGORY_SHORT = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_SHORT).FirstOrDefault(),
                                    CUSTOMER_ID = customer_Id,
                                    CATEGORY_NAME = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                    CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                    CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                    //DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                    //VERSION = a.TB_CATALOG.VERSION,
                                    hasChildren = _dbContext.TB_CATEGORY.Join(_dbContext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID && x.tps.FLAG_RECYCLE == "A") || _dbContext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId)
                                        .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbContext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1 && x.FLAG_RECYCLE == "A").Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                    SORT_ORDER = a.SORT_ORDER,
                                    CategoryIdinNavigator = categoryidinnavigator,
                                    FamilyandRelatedFamily = familyandrelatedfamily,
                                    spriteCssClass = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && a.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.IS_CLONE).FirstOrDefault() == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryCloneWebSync" : "categoryWebSync",
                                    Modify_Date = a.TB_CATEGORY.MODIFIED_DATE,
                                    //@checked = catalogId == 1 && _dbContext.TB_CATALOG_SECTIONS.Where(x => x.CATALOG_ID == workingCatalogId && x.CATEGORY_ID == id).Any()
                                    @checked = catalogId == 1 && _dbContext.TB_CATEGORY.Join(_dbContext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.CATEGORY_ID == a.CATEGORY_ID && x.tps.FLAG_RECYCLE == "A") ? true : false,
                                    STATUS = "UPDATED"

                                }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);

                    string subProductIds = string.Empty;
                    string productIds = string.Empty;
                    string familyIds = string.Empty;
                    string categoryIds = string.Empty;
                    if (!aa.Any() && id.Contains("CAT"))
                    {
                        //subProductIds = GetSubProductId(webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);

                        //if (!string.IsNullOrEmpty(subProductIds) && string.IsNullOrEmpty(productIds))
                        //    productIds = GetProductId(subProductIds, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);
                        if (!string.IsNullOrEmpty(productIds))
                            productIds = GetProductId(null, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);
                        if (!string.IsNullOrEmpty(productIds) && string.IsNullOrEmpty(familyIds))
                            familyIds = GetFamilyIds(productIds, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);
                        else if (!string.IsNullOrEmpty(familyIds))
                            familyIds = GetFamilyIds(null, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);

                        var childCategoryExist = _dbContext.TB_CATEGORY.Where(x => x.PARENT_CATEGORY == id).Distinct().Count();
                        if (childCategoryExist != 0)
                        {
                            if (!string.IsNullOrEmpty(familyIds) && string.IsNullOrEmpty(categoryIds))
                                categoryIds = GetCategoryIds(familyIds, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);
                            else if (!string.IsNullOrEmpty(categoryIds))
                                categoryIds = GetCategoryIds(null, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);

                            aa = _dbContext.TB_CATALOG_SECTIONS.Where(
                        a => a.CATALOG_ID == catalogId && _dbContext.TB_CATEGORY.Where(tc => !userName.Contains(tc.CREATED_USER) && categoryIds.Contains(tc.CATEGORY_ID) &&
                        tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbContext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.PARENT_CATEGORY).FirstOrDefault() == id)
                        .Select(
                            a =>
                                new WebSyncCategoryDetails
                                {
                                    id = a.CATEGORY_ID,
                                    CATEGORY_ID = a.CATEGORY_ID,
                                    CATEGORY_SHORT = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_SHORT).FirstOrDefault(),
                                    CUSTOMER_ID = customer_Id,
                                    CATEGORY_NAME = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                    CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                    CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                    //DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                    //VERSION = a.TB_CATALOG.VERSION,
                                    hasChildren = _dbContext.TB_CATEGORY.Join(_dbContext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID && x.tps.FLAG_RECYCLE == "A") || _dbContext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId)
                                        .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbContext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1 && x.FLAG_RECYCLE == "A").Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                    SORT_ORDER = a.SORT_ORDER,
                                    CategoryIdinNavigator = categoryidinnavigator,
                                    FamilyandRelatedFamily = familyandrelatedfamily,
                                    spriteCssClass = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && a.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.IS_CLONE).FirstOrDefault() == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryCloneWebSync" : "categoryWebSync",
                                    Modify_Date = a.TB_CATEGORY.MODIFIED_DATE,
                                    //@checked = catalogId == 1 && _dbContext.TB_CATALOG_SECTIONS.Where(x => x.CATALOG_ID == workingCatalogId && x.CATEGORY_ID == id).Any()
                                    @checked = catalogId == 1 && _dbContext.TB_CATEGORY.Join(_dbContext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.CATEGORY_ID == a.CATEGORY_ID && x.tps.FLAG_RECYCLE == "A") ? true : false,
                                    STATUS = "UPDATED"
                                }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);
                        }

                    }
                    var bb = Array.ConvertAll(
                                             _dbContext.TB_CATALOG_FAMILY.Where(
                                                 a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && !userName.Contains(a.TB_FAMILY.CREATED_USER) && a.TB_FAMILY.MODIFIED_DATE >= webSyncFromDate && a.TB_FAMILY.MODIFIED_DATE <= webSyncTodate && a.CATEGORY_ID == id)
                                                 .Select(a
                                                     =>
                                                     new
                                                     {
                                                         a.FAMILY_ID,
                                                         a.TB_FAMILY.FAMILY_NAME,
                                                         a.TB_CATALOG.CATALOG_ID,
                                                         a.TB_CATALOG.CATALOG_NAME,
                                                         //   a.TB_CATALOG.DESCRIPTION,
                                                         //  a.TB_CATALOG.VERSION,
                                                         hasChildren = _dbContext.TB_CATALOG_FAMILY.Join(_dbContext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tpf.PARENT_FAMILY_ID == a.FAMILY_ID && x.tps.CATEGORY_ID == id) ||
                                                         _dbContext.TB_CATALOG_FAMILY.Join(_dbContext.TB_FAMILY, tcf => tcf.FAMILY_ID, tf => tf.FAMILY_ID, (tcf, tf) => new { tcf, tf }).
                                                         Join(_dbContext.TB_PROD_FAMILY, tftcf => tftcf.tf.FAMILY_ID, tpf => tpf.FAMILY_ID, (tftcf, tpf) => new { tftcf, tpf })
                                                         .Join(_dbContext.TB_PROD_SPECS, tpftf => tpftf.tpf.PRODUCT_ID, tps => tps.PRODUCT_ID, (tpftf, tps) => new { tpftf, tps }).Any(x => x.tpftf.tftcf.tcf.CATALOG_ID == catalogId && x.tpftf.tpf.FLAG_RECYCLE == "A" && x.tpftf.tftcf.tcf.CATEGORY_ID == id && x.tps.MODIFIED_DATE >= webSyncFromDate && x.tps.MODIFIED_DATE <= webSyncTodate) || _dbContext.TB_FAMILY_SPECS.Any(x => x.FAMILY_ID == a.FAMILY_ID && x.MODIFIED_DATE >= webSyncFromDate && x.MODIFIED_DATE <= webSyncTodate),
                                                         a.MODIFIED_DATE,
                                                         a.SORT_ORDER
                                                     }).ToArray(),
                                             a =>
                                                 new WebSyncCategoryDetails
                                                 {
                                                     id = _dbContext.TB_CATEGORY.Where(x => x.CATEGORY_ID == _dbContext.TB_CATEGORY.Where(xx => xx.CATEGORY_ID == _dbContext.TB_CATEGORY.Where(z => z.CATEGORY_ID == id && z.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && xx.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() + "~" + _dbContext.TB_CATEGORY.Where(x => x.CATEGORY_ID == _dbContext.TB_CATEGORY.Where(z => z.CATEGORY_ID == id && z.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() + "~" + _dbContext.TB_CATEGORY.Where(x => x.CATEGORY_ID == id && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() + "~" + id + "~" + a.FAMILY_ID.ToString(),
                                                     CATEGORY_ID = "~" + a.FAMILY_ID.ToString(),
                                                     CATEGORY_NAME = familyandrelatedfamily ? a.FAMILY_NAME : a.FAMILY_NAME,
                                                     CATALOG_ID = a.CATALOG_ID,
                                                     CATALOG_NAME = a.CATALOG_NAME,
                                                     // DESCRIPTION = a.DESCRIPTION,
                                                     //VERSION = a.VERSION,
                                                     hasChildren = a.hasChildren,
                                                     SORT_ORDER = a.SORT_ORDER,
                                                     CategoryIdinNavigator = false,
                                                     FamilyandRelatedFamily = familyandrelatedfamily,
                                                     spriteCssClass = _dbContext.TB_CATALOG_FAMILY.Join(_dbContext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tpf.ROOT_FAMILY == 0 && x.tpf.FAMILY_ID == a.FAMILY_ID && x.tps.CATEGORY_ID == id) ?
                                                     "subfamilyWebSync" : "familyWebSync",
                                                     //@checked = catalogId == 1 && _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.FAMILY_ID).Any()
                                                     Modify_Date = a.MODIFIED_DATE,
                                                     @checked = catalogId == 1 && _dbContext.TB_FAMILY.Join(_dbContext.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.FAMILY_ID == a.FAMILY_ID),
                                                     STATUS = "UPDATED"

                                                 }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);

                    if (!bb.Any() && id.Contains("CAT"))
                    {
                        subProductIds = GetSubProductId(webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);
                        productIds = string.Empty;
                        familyIds = string.Empty;
                        categoryIds = string.Empty;
                        if (!string.IsNullOrEmpty(subProductIds) && string.IsNullOrEmpty(productIds))
                            productIds = GetProductId(subProductIds, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);
                        else if (!string.IsNullOrEmpty(productIds))
                            productIds = GetProductId(null, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);
                        if (!string.IsNullOrEmpty(productIds) && string.IsNullOrEmpty(familyIds))
                            familyIds = GetFamilyIds(productIds, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);
                        else if (!string.IsNullOrEmpty(familyIds))
                            familyIds = GetFamilyIds(null, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customerId);

                        var childCategoryExist = _dbContext.TB_CATEGORY.Where(x => x.PARENT_CATEGORY == id).Distinct().Count();
                        if (childCategoryExist == 0)
                        {
                            bb = Array.ConvertAll(
                                            _dbContext.TB_CATALOG_FAMILY.Where(
                                                a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && !userName.Contains(a.TB_FAMILY.CREATED_USER) && familyIds.Contains(a.FAMILY_ID.ToString()) && a.CATEGORY_ID == id)
                                                .Select(a
                                                    =>
                                                    new
                                                    {
                                                        a.FAMILY_ID,
                                                        a.TB_FAMILY.FAMILY_NAME,
                                                        a.TB_CATALOG.CATALOG_ID,
                                                        a.TB_CATALOG.CATALOG_NAME,
                                                        //   a.TB_CATALOG.DESCRIPTION,
                                                        //  a.TB_CATALOG.VERSION,
                                                        hasChildren = _dbContext.TB_CATALOG_FAMILY.Join(_dbContext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tpf.PARENT_FAMILY_ID == a.FAMILY_ID && x.tps.CATEGORY_ID == id) ||
                                                       _dbContext.TB_CATALOG_FAMILY.Join(_dbContext.TB_FAMILY, tcf => tcf.FAMILY_ID, tf => tf.FAMILY_ID, (tcf, tf) => new { tcf, tf }).
                                                       Join(_dbContext.TB_PROD_FAMILY, tftcf => tftcf.tf.FAMILY_ID, tpf => tpf.FAMILY_ID, (tftcf, tpf) => new { tftcf, tpf })
                                                       .Join(_dbContext.TB_PROD_SPECS, tpftf => tpftf.tpf.PRODUCT_ID, tps => tps.PRODUCT_ID, (tpftf, tps) => new { tpftf, tps }).Any(x => x.tpftf.tftcf.tcf.CATALOG_ID == catalogId && x.tpftf.tpf.FLAG_RECYCLE == "A" && x.tpftf.tftcf.tcf.CATEGORY_ID == id && productIds.Contains(x.tps.PRODUCT_ID.ToString())) || _dbContext.TB_FAMILY_SPECS.Any(x => x.FAMILY_ID == a.FAMILY_ID && x.MODIFIED_DATE >= webSyncFromDate && x.MODIFIED_DATE <= webSyncTodate),
                                                        a.MODIFIED_DATE,
                                                        a.SORT_ORDER
                                                    }).ToArray(),
                                            a =>
                                                new WebSyncCategoryDetails
                                                {
                                                    id = _dbContext.TB_CATEGORY.Where(x => x.CATEGORY_ID == _dbContext.TB_CATEGORY.Where(xx => xx.CATEGORY_ID == _dbContext.TB_CATEGORY.Where(z => z.CATEGORY_ID == id && z.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && xx.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() + "~" + _dbContext.TB_CATEGORY.Where(x => x.CATEGORY_ID == _dbContext.TB_CATEGORY.Where(z => z.CATEGORY_ID == id && z.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() + "~" + _dbContext.TB_CATEGORY.Where(x => x.CATEGORY_ID == id && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() + "~" + id + "~" + a.FAMILY_ID.ToString(),
                                                    CATEGORY_ID = "~" + a.FAMILY_ID.ToString(),
                                                    CATEGORY_NAME = familyandrelatedfamily ? a.FAMILY_NAME : a.FAMILY_NAME,
                                                    CATALOG_ID = a.CATALOG_ID,
                                                    CATALOG_NAME = a.CATALOG_NAME,
                                                    // DESCRIPTION = a.DESCRIPTION,
                                                    //VERSION = a.VERSION,
                                                    hasChildren = a.hasChildren,
                                                    SORT_ORDER = a.SORT_ORDER,
                                                    CategoryIdinNavigator = false,
                                                    FamilyandRelatedFamily = familyandrelatedfamily,
                                                    spriteCssClass = _dbContext.TB_CATALOG_FAMILY.Join(_dbContext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tpf.ROOT_FAMILY == 0 && x.tpf.FAMILY_ID == a.FAMILY_ID && x.tps.CATEGORY_ID == id) ?
                                                    "subfamilyWebSync" : "familyWebSync",
                                                    //@checked = catalogId == 1 && _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.FAMILY_ID).Any()
                                                    Modify_Date = a.MODIFIED_DATE,
                                                    @checked = catalogId == 1 && _dbContext.TB_FAMILY.Join(_dbContext.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.FAMILY_ID == a.FAMILY_ID),
                                                    STATUS = "UPDATED"

                                                }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);
                        }

                    }
                    int fid;
                    int.TryParse(id.Trim('~'), out fid);
                    int familyId;
                    int.TryParse(id.Trim('~'), out familyId);
                    var ProductCount = _dbContext.TB_PROD_FAMILY.Join(_dbContext.TB_CATALOG_FAMILY, tpf => tpf.FAMILY_ID, tcf => tcf.FAMILY_ID, (tpf, tcf) => new { tpf, tcf }).Where(x => x.tcf.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tpf.FAMILY_ID == familyId).Select(x => x.tpf.PRODUCT_ID).Count();
                    int maxWebsyncCount = Convert.ToInt16(ConfigurationManager.AppSettings["WebSyncProductCount"].ToString(CultureInfo.InvariantCulture));
                    if (ProductCount >= maxWebsyncCount && productExists.Contains("Products"))
                    {
                        var GetCategoryDetailsProd = new List<WebSyncCategoryDetails>();
                        GetCategoryDetailsProd.Add(new WebSyncCategoryDetails

                        {
                            id = "999999",
                            CATEGORY_ID = "~~" + 999999,
                            CATEGORY_NAME = "More Products...",
                            CATALOG_ID = 99999,
                            CATALOG_NAME = "More Products...",
                            DESCRIPTION = "More Products...",
                            //  VERSION = a.VERSION,
                            SORT_ORDER = 1,
                            CategoryIdinNavigator = false,
                            FamilyandRelatedFamily = false,
                            hasChildren = false,
                            spriteCssClass = "producttreeWebSync",//check2
                            Modify_Date = new DateTime(),
                            //@checked = catalogId == 1 && _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.SUBFAMILY_ID).Any()
                            @checked = false,
                            STATUS = "UPDATED"
                        });
                        return GetCategoryDetailsProd.ToList();
                    }
                    else if (ProductCount >= maxWebsyncCount && !productExists.Contains("Products"))
                    {
                        var GetCategoryDetailsProd = new List<WebSyncCategoryDetails>();
                        GetCategoryDetailsProd.Add(new WebSyncCategoryDetails
                        {
                            id = subfamilycategoryid + "~" + familyId + "Products",
                            CATEGORY_ID = "~" + familyId,
                            CATEGORY_NAME = string.IsNullOrEmpty(productIdExists) ? "Products" : "SubProducts",
                            CATALOG_ID = catalogId,
                            CATALOG_NAME = _dbContext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId).Select(xx => xx.CATALOG_NAME).FirstOrDefault().ToString(),
                            //DESCRIPTION = a.VALUE,
                            //  VERSION = a.VERSION,
                            SORT_ORDER = 1,
                            CategoryIdinNavigator = false,
                            FamilyandRelatedFamily = false,
                            hasChildren = true,
                            spriteCssClass = string.IsNullOrEmpty(productIdExists) ? "producttreeWebSync" : "subproducttreeWebSync",//Both product and subproduct title
                            Modify_Date = DateTime.Now,
                            //@checked = catalogId == 1 && _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.SUBFAMILY_ID).Any()
                            @checked = catalogId == 1 && _dbContext.TB_FAMILY.Join(_dbContext.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.FAMILY_ID == familyId),
                            STATUS = "UPDATED"
                        });
                        return GetCategoryDetailsProd.ToList();
                    }
                    else
                    {
                        var cc = Array.ConvertAll(
                                (_dbContext.TB_PROD_FAMILY.Join(_dbContext.TB_PROD_SPECS, tps => tps.PRODUCT_ID,
                                    tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                                    .Join(_dbContext.TB_CATALOG_FAMILY, tcptps => tcptps.tps.FAMILY_ID, tcp => tcp.FAMILY_ID,
                                        (tcptps, tcp) => new { tcptps, tcp })
                                    .Where(x => x.tcp.CATALOG_ID == catalogId && x.tcp.FLAG_RECYCLE == "A" && x.tcptps.tps.FLAG_RECYCLE == "A" && !userName.Contains(x.tcptps.tpf.CREATED_USER) && x.tcptps.tps.FAMILY_ID == familyId && x.tcptps.tpf.MODIFIED_DATE >= webSyncFromDate && x.tcptps.tpf.MODIFIED_DATE <= webSyncTodate && x.tcp.CATEGORY_ID == subfamilycategoryid)).Distinct().ToArray(),
                                a => new
                                {
                                    FAMILY_ID = a.tcptps.tps.FAMILY_ID.ToString(),
                                    PRODUCT_ID = a.tcptps.tps.PRODUCT_ID,
                                    CATALOG_ID = a.tcp.TB_CATALOG.CATALOG_ID,
                                    a.tcp.TB_CATALOG.CATALOG_NAME,
                                    a.tcp.TB_CATALOG.DESCRIPTION,
                                    a.tcp.TB_CATALOG.VERSION,
                                    a.tcptps.tps.SORT_ORDER,
                                    a.tcptps.tps.MODIFIED_DATE
                                }).Distinct()
                                .Select(a => new WebSyncCategoryDetails
                                {
                                    id = subfamilycategoryid + "~" + familyId + "~~" + a.PRODUCT_ID,
                                    CATEGORY_ID = "~~" + a.PRODUCT_ID,
                                    CATEGORY_NAME = _dbContext.TB_PROD_FAMILY.Join(_dbContext.TB_PROD_SPECS, tpf => tpf.PRODUCT_ID, tps => tps.PRODUCT_ID, (tpf, tps) => new { tpf, tps }).Where(b => b.tps.PRODUCT_ID == a.PRODUCT_ID && b.tpf.FLAG_RECYCLE == "A" && b.tps.ATTRIBUTE_ID == 1).Select(b => b.tps.STRING_VALUE).FirstOrDefault(),
                                    CATALOG_ID = a.CATALOG_ID,
                                    CATALOG_NAME = a.CATALOG_NAME,
                                    DESCRIPTION = a.DESCRIPTION,
                                //  VERSION = a.VERSION,
                                SORT_ORDER = a.SORT_ORDER,
                                    CategoryIdinNavigator = false,
                                    FamilyandRelatedFamily = false,
                                    hasChildren = _dbContext.TB_SUBPRODUCT.Any(x => x.PRODUCT_ID == a.PRODUCT_ID && x.CATALOG_ID == a.CATALOG_ID),
                                    spriteCssClass = "producttreeWebSync",//check2
                                Modify_Date = a.MODIFIED_DATE,
                                //@checked = catalogId == 1 && _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.SUBFAMILY_ID).Any()
                                @checked = catalogId == 1 && _dbContext.TB_PROD_FAMILY.Join(_dbContext.TB_PROD_SPECS, tpf => tpf.PRODUCT_ID, tps => tps.PRODUCT_ID, (tpf, tps) => new { tpf, tps }).Any(b => b.tps.PRODUCT_ID == a.PRODUCT_ID && b.tpf.FLAG_RECYCLE == "A" && b.tps.ATTRIBUTE_ID == 1) ? true : false,
                                    STATUS = "UPDATED"
                                }).Distinct().OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id);

                        var listwebSyncCategoryDetails = queryValue.GetCategoryAttributes(id, catalogId).ToList();
                        //if (aa.Any())
                        //{

                        //    var res = aa.ToList().Union(bb.ToList());
                        //    if (listwebSyncCategoryDetails.Any())
                        //    {
                        //        return listwebSyncCategoryDetails.ToList().Union(res).ToList();
                        //    }
                        //    return res.ToList();
                        //}
                        if (bb.Any())
                        {
                            if (listwebSyncCategoryDetails.Any() && id.Contains("CAT"))
                            {
                                return listwebSyncCategoryDetails.ToList().Union(bb).ToList();
                            }
                            return bb.ToList();
                        }
                        else
                        {
                            if (listwebSyncCategoryDetails.Any() && id.Contains("CAT"))
                            {
                                return listwebSyncCategoryDetails.ToList();
                            }
                        }
                        if (!cc.Any())
                        {
                            var familtAttr = _dbContext.TB_ATTRIBUTE.Join(_dbContext.TB_FAMILY_SPECS, ta => ta.ATTRIBUTE_ID, tfs => tfs.ATTRIBUTE_ID, (ta, tfs) => new { ta, tfs }).Where(x => x.tfs.FAMILY_ID == familyId).Select(
                            y => new
                            {
                                FAMILY_ID = y.tfs.FAMILY_ID,
                                PRODUCT_ID = y.tfs.FAMILY_ID,
                                CATALOG_ID = catalogId,
                                ATTRIBUTE_ID = y.ta.ATTRIBUTE_ID,
                                ATTRIBUTENAME = y.ta.ATTRIBUTE_NAME,
                                VALUE = y.tfs.NUMERIC_VALUE == null ? y.tfs.STRING_VALUE : y.tfs.NUMERIC_VALUE.ToString(),
                                SORT_ORDER = y.ta.ATTRIBUTE_ID,
                                CATALOG_NAME = _dbContext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId).Select(xx => xx.CATALOG_NAME).FirstOrDefault().ToString(),
                                y.tfs.MODIFIED_DATE
                            }).Distinct().ToArray().Select(a => new WebSyncCategoryDetails
                            {
                                id = subfamilycategoryid + "~" + a.FAMILY_ID,
                                CATEGORY_ID = "~" + a.FAMILY_ID,
                                CATEGORY_NAME = a.ATTRIBUTENAME + " (" + a.VALUE + ")",
                                CATALOG_ID = a.CATALOG_ID,
                                CATALOG_NAME = a.CATALOG_NAME,
                            //DESCRIPTION = a.VALUE,
                            //  VERSION = a.VERSION,
                            SORT_ORDER = a.SORT_ORDER,
                                CategoryIdinNavigator = false,
                                FamilyandRelatedFamily = false,
                                hasChildren = false,
                                spriteCssClass = "famspecstreeWebSync",
                                Modify_Date = a.MODIFIED_DATE,
                            //@checked = catalogId == 1 && _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.SUBFAMILY_ID).Any()
                            @checked = catalogId == 1 && _dbContext.TB_PROD_FAMILY.Join(_dbContext.TB_PROD_SPECS, tpf => tpf.PRODUCT_ID, tps => tps.PRODUCT_ID, (tpf, tps) => new { tpf, tps }).Any(b => b.tps.PRODUCT_ID == a.PRODUCT_ID && b.tpf.FLAG_RECYCLE == "A" && b.tps.ATTRIBUTE_ID == 1) ? true : false,
                                STATUS = "UPDATED"

                            }).Distinct().OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id);


                            if (familyId != 0)
                            {
                                if (!productExists.Contains("Products"))
                                {
                                    var GetCategoryDetailsProd = new List<WebSyncCategoryDetails>();
                                    GetCategoryDetailsProd.Add(new WebSyncCategoryDetails
                                    {
                                        id = subfamilycategoryid + "~" + familyId + "Products",
                                        CATEGORY_ID = "~" + familyId,
                                        CATEGORY_NAME = string.IsNullOrEmpty(productIdExists) ? "Products" : "SubProducts",
                                        CATALOG_ID = catalogId,
                                        CATALOG_NAME = _dbContext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId).Select(xx => xx.CATALOG_NAME).FirstOrDefault().ToString(),
                                        //DESCRIPTION = a.VALUE,
                                        //  VERSION = a.VERSION,
                                        SORT_ORDER = 1,
                                        CategoryIdinNavigator = false,
                                        FamilyandRelatedFamily = false,
                                        hasChildren = true,
                                        spriteCssClass = string.IsNullOrEmpty(productIdExists) ? "producttreeWebSync" : "subproducttreeWebSync",//Both product and subproduct title
                                        Modify_Date = DateTime.Now,
                                        //@checked = catalogId == 1 && _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.SUBFAMILY_ID).Any()
                                        @checked = catalogId == 1 && _dbContext.TB_FAMILY.Join(_dbContext.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.FAMILY_ID == familyId),
                                        STATUS = "UPDATED"
                                    });


                                    if (familtAttr.Any() && GetCategoryDetailsProd.Any())
                                    {
                                        return familtAttr.Union(GetCategoryDetailsProd).ToList();
                                    }
                                    else if (GetCategoryDetailsProd.Any())
                                    {
                                        return GetCategoryDetailsProd.ToList();
                                    }
                                }

                                else if (productExists.Contains("Products"))
                                {
                                    cc = Array.ConvertAll(
                               (_dbContext.TB_PROD_FAMILY.Join(_dbContext.TB_PROD_SPECS, tps => tps.PRODUCT_ID,
                                   tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                                   .Join(_dbContext.TB_CATALOG_FAMILY, tcptps => tcptps.tps.FAMILY_ID, tcp => tcp.FAMILY_ID,
                                       (tcptps, tcp) => new { tcptps, tcp })
                                   .Where(x => x.tcp.CATALOG_ID == catalogId && x.tcp.FLAG_RECYCLE == "A" && x.tcptps.tps.FLAG_RECYCLE == "A" && !userName.Contains(x.tcptps.tpf.CREATED_USER) && x.tcptps.tps.FAMILY_ID == familyId && x.tcp.CATEGORY_ID == subfamilycategoryid)).Distinct().ToArray(),
                               a => new
                               {
                                   FAMILY_ID = a.tcptps.tps.FAMILY_ID.ToString(),
                                   PRODUCT_ID = a.tcptps.tps.PRODUCT_ID,
                                   CATALOG_ID = a.tcp.TB_CATALOG.CATALOG_ID,
                                   a.tcp.TB_CATALOG.CATALOG_NAME,
                                   a.tcp.TB_CATALOG.DESCRIPTION,
                                   a.tcp.TB_CATALOG.VERSION,
                                   a.tcptps.tps.SORT_ORDER,
                                   a.tcptps.tps.MODIFIED_DATE
                               }).Distinct()
                               .Select(a => new WebSyncCategoryDetails
                               {
                                   id = subfamilycategoryid + "~" + familyId + "~~" + a.PRODUCT_ID,
                                   CATEGORY_ID = "~~" + a.PRODUCT_ID,
                                   CATEGORY_NAME = _dbContext.TB_PROD_FAMILY.Join(_dbContext.TB_PROD_SPECS, tpf => tpf.PRODUCT_ID, tps => tps.PRODUCT_ID, (tpf, tps) => new { tpf, tps }).Where(b => b.tps.PRODUCT_ID == a.PRODUCT_ID && b.tpf.FLAG_RECYCLE == "A" && b.tps.ATTRIBUTE_ID == 1).Select(b => b.tps.STRING_VALUE).Take(10).FirstOrDefault(),
                                   CATALOG_ID = a.CATALOG_ID,
                                   CATALOG_NAME = a.CATALOG_NAME,
                                   DESCRIPTION = a.DESCRIPTION,
                               //  VERSION = a.VERSION,
                               SORT_ORDER = a.SORT_ORDER,
                                   CategoryIdinNavigator = false,
                                   FamilyandRelatedFamily = false,
                                   hasChildren = _dbContext.TB_SUBPRODUCT.Any(x => x.PRODUCT_ID == a.PRODUCT_ID && x.CATALOG_ID == a.CATALOG_ID),
                                   spriteCssClass = "producttreeWebSync",//Item# icon
                               Modify_Date = a.MODIFIED_DATE,
                               //@checked = catalogId == 1 && _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.SUBFAMILY_ID).Any()
                               @checked = catalogId == 1 && _dbContext.TB_PROD_FAMILY.Join(_dbContext.TB_PROD_SPECS, tpf => tpf.PRODUCT_ID, tps => tps.PRODUCT_ID, (tpf, tps) => new { tpf, tps }).Any(b => b.tps.PRODUCT_ID == a.PRODUCT_ID && b.tpf.FLAG_RECYCLE == "A" && b.tps.ATTRIBUTE_ID == 1) ? true : false,
                                   STATUS = "UPDATED"
                               }).Distinct().OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id);

                                    if (cc.Any())
                                    {
                                        return cc.ToList();
                                    }
                                }
                            }
                            else if (familtAttr.Any())
                            {
                                return familtAttr.ToList();
                            }
                        }
                        if (cc.Any())
                        {
                            if (!productExists.Contains("Products"))
                            {
                                var familtAttr = _dbContext.TB_ATTRIBUTE.Join(_dbContext.TB_FAMILY_SPECS, ta => ta.ATTRIBUTE_ID, tfs => tfs.ATTRIBUTE_ID, (ta, tfs) => new { ta, tfs }).Where(x => x.tfs.FAMILY_ID == familyId).Select(
                                y => new
                                {
                                    FAMILY_ID = y.tfs.FAMILY_ID,
                                    PRODUCT_ID = y.tfs.FAMILY_ID,
                                    CATALOG_ID = catalogId,
                                    ATTRIBUTENAME = y.ta.ATTRIBUTE_NAME,
                                    VALUE = y.tfs.NUMERIC_VALUE == null ? y.tfs.STRING_VALUE : y.tfs.NUMERIC_VALUE.ToString(),
                                    SORT_ORDER = y.ta.ATTRIBUTE_ID,
                                    CATALOG_NAME = _dbContext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId).Select(xx => xx.CATALOG_NAME).FirstOrDefault().ToString(),
                                    y.tfs.MODIFIED_DATE
                                }).Distinct().ToArray().Select(a => new WebSyncCategoryDetails
                                {
                                    id = subfamilycategoryid + "~" + a.FAMILY_ID,
                                    CATEGORY_ID = "~" + a.FAMILY_ID,
                                    CATEGORY_NAME = a.ATTRIBUTENAME + " (" + a.VALUE + ")",
                                    CATALOG_ID = a.CATALOG_ID,
                                    CATALOG_NAME = a.CATALOG_NAME,
                                //DESCRIPTION = a.VALUE,
                                //  VERSION = a.VERSION,
                                SORT_ORDER = a.SORT_ORDER,
                                    CategoryIdinNavigator = false,
                                    FamilyandRelatedFamily = false,
                                    hasChildren = false,
                                    spriteCssClass = "famspecstreeWebSync",
                                    Modify_Date = a.MODIFIED_DATE,
                                //@checked = catalogId == 1 && _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.SUBFAMILY_ID).Any()
                                @checked = catalogId == 1 && _dbContext.TB_PROD_FAMILY.Join(_dbContext.TB_PROD_SPECS, tpf => tpf.PRODUCT_ID, tps => tps.PRODUCT_ID, (tpf, tps) => new { tpf, tps }).Any(b => b.tps.PRODUCT_ID == a.PRODUCT_ID && b.tpf.FLAG_RECYCLE == "A" && b.tps.ATTRIBUTE_ID == 1) ? true : false,
                                    STATUS = "UPDATED"
                                }).Distinct().OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id);

                                var GetCategoryDetailsProd = new List<WebSyncCategoryDetails>();
                                GetCategoryDetailsProd.Add(new WebSyncCategoryDetails
                                {
                                    id = subfamilycategoryid + "~" + familyId + "Products",
                                    CATEGORY_ID = "~" + familyId,
                                    CATEGORY_NAME = "Products",
                                    CATALOG_ID = catalogId,
                                    CATALOG_NAME = _dbContext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId).Select(xx => xx.CATALOG_NAME).FirstOrDefault().ToString(),
                                    //DESCRIPTION = a.VALUE,
                                    //  VERSION = a.VERSION,
                                    SORT_ORDER = 1,
                                    CategoryIdinNavigator = false,
                                    FamilyandRelatedFamily = false,
                                    hasChildren = true,
                                    spriteCssClass = "producttreeWebSync",//check1
                                    Modify_Date = DateTime.Now,
                                    //@checked = catalogId == 1 && _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.SUBFAMILY_ID).Any()
                                    @checked = catalogId == 1 && _dbContext.TB_FAMILY.Join(_dbContext.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.FAMILY_ID == familyId),
                                    STATUS = "UPDATED"
                                });

                                // return familtAttr.ToList().Union(GetCategoryDetails).ToList();

                                if (familtAttr.Any())
                                {
                                    return familtAttr.ToList().Union(GetCategoryDetailsProd).ToList();
                                }
                                else
                                {
                                    return GetCategoryDetailsProd.ToList();
                                }
                            }
                            return cc.ToList();
                        }
                    }
                    //else
                    //{
                    //    int productId;
                    //    int.TryParse(id.Trim('~'), out productId);
                    //    //int fid = Convert.ToInt32(id);
                    //    var dd = Array.ConvertAll(
                    //        (_dbContext.TB_SUBPRODUCT.Join(_dbContext.TB_PROD_SPECS, tps => tps.SUBPRODUCT_ID,
                    //            tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                    //            .Join(_dbContext.TB_CATALOG_PRODUCT, tcptps => tcptps.tps.PRODUCT_ID, tcp => tcp.PRODUCT_ID,
                    //                (tcptps, tcp) => new { tcptps, tcp })
                    //            .Where(x => x.tcp.CATALOG_ID == catalogId && x.tcptps.tps.FLAG_RECYCLE == "A" && !userName.Contains(x.tcptps.tpf.CREATED_USER) && x.tcptps.tps.PRODUCT_ID == productId && x.tcptps.tpf.MODIFIED_DATE >= webSyncFromDate && x.tcptps.tpf.MODIFIED_DATE <= webSyncTodate)).Distinct().ToArray(),
                    //        a => new
                    //        {
                    //            SUBPRODUCT_ID = a.tcptps.tps.SUBPRODUCT_ID,
                    //            PRODUCT_ID = a.tcptps.tps.PRODUCT_ID,
                    //            a.tcp.TB_CATALOG.CATALOG_ID,
                    //            a.tcp.TB_CATALOG.CATALOG_NAME,
                    //            a.tcp.TB_CATALOG.DESCRIPTION,
                    //            a.tcp.TB_CATALOG.VERSION,
                    //            a.tcptps.tps.SORT_ORDER,
                    //            a.tcptps.tps.MODIFIED_DATE
                    //        }).Distinct()
                    //        .Select(a => new WebSyncCategoryDetails
                    //        {
                    //            id = subfamilycategoryid + "~" + familyId + "~~" + a.PRODUCT_ID + "~!" + a.SUBPRODUCT_ID,
                    //            CATEGORY_ID = "~~~" + a.SUBPRODUCT_ID,
                    //            CATEGORY_NAME = _dbContext.TB_SUBPRODUCT.Join(_dbContext.TB_PROD_SPECS, tpf => tpf.SUBPRODUCT_ID, tps => tps.PRODUCT_ID, (tpf, tps) => new { tpf, tps }).Where(b => b.tps.PRODUCT_ID == a.SUBPRODUCT_ID && b.tpf.FLAG_RECYCLE == "A" && b.tps.ATTRIBUTE_ID == 1).Select(b => b.tps.STRING_VALUE).FirstOrDefault(),
                    //            CATALOG_ID = a.CATALOG_ID,
                    //            CATALOG_NAME = a.CATALOG_NAME,
                    //            // DESCRIPTION = a.DESCRIPTION,
                    //            //  VERSION = a.VERSION,
                    //            SORT_ORDER = a.SORT_ORDER,
                    //            CategoryIdinNavigator = false,
                    //            FamilyandRelatedFamily = false,
                    //            hasChildren = false,
                    //            spriteCssClass = "subproducttreeWebSync",
                    //            Modify_Date = a.MODIFIED_DATE,
                    //            //@checked = catalogId == 1 && _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.SUBFAMILY_ID).Any()
                    //            @checked = catalogId == 1 && _dbContext.TB_SUBPRODUCT.Join(_dbContext.TB_PROD_SPECS, tpf => tpf.SUBPRODUCT_ID, tps => tps.PRODUCT_ID, (tpf, tps) => new { tpf, tps }).Any(b => b.tps.PRODUCT_ID == a.PRODUCT_ID && b.tpf.FLAG_RECYCLE == "A" && b.tps.ATTRIBUTE_ID == 1) ? true : false,
                    //            STATUS = "UPDATED"
                    //        }).Distinct().OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id);
                    //    //       CATALOG_ID = a, CATALOG_NAME = a.CATALOG_NAME, DESCRIPTION = a.DESCRIPTION, VERSION = a.VERSION,  , hasChildren = false });
                    //    //var cc = Array.ConvertAll(_dbContext.TB_SUBFAMILY.Where(a => a.FAMILY_ID == fid).ToArray(),
                    //    //    a => new { FAMILY_ID = a.FAMILY_ID.ToString(CultureInfo.InvariantCulture), SUBFAMILY_ID = a.SUBFAMILY_ID,
                    //    //        ProdCnt = _dbContext.TB_PROD_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Distinct().Count()})
                    //    //    .Select(a => new WebSyncCategoryDetails { id = "~" + a.SUBFAMILY_ID, CATEGORY_NAME = _dbContext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault() + "(" + a.ProdCnt + ")",
                    //    //       CATALOG_ID = a, CATALOG_NAME = a.CATALOG_NAME, DESCRIPTION = a.DESCRIPTION, VERSION = a.VERSION,  , hasChildren = false });
                    //    if (listwebSyncCategoryDetails.Any() && id.Contains("CAT"))
                    //    {
                    //        return listwebSyncCategoryDetails.ToList().Union(dd).ToList();
                    //    }
                    //    return dd.ToList();
                    //}
                }

                var GetCategoryDetails = new List<WebSyncCategoryDetails>()
                    { new WebSyncCategoryDetails {
                        CATEGORY_ID = "",
                        CATEGORY_NAME = "",
                        Parent_Category = "",
                        Modify_Date =DateTime.Now

                    } };
                GetCategoryDetails.Remove(GetCategoryDetails[0]);
                return GetCategoryDetails;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at GetFiltersDetails : WebSyncApiController", objException);

                return null;
            }


        }
        #endregion

        #region GetCategoryFamilyProductData
        /// <summary>
        /// GetCategoryFamilyProductData
        /// </summary>
        /// <param name="fromDate">fromdate from user selection in ui</param>
        /// <param name="toDate">todate from user selection in ui</param>
        /// <param name="catalogId">current catalogId</param>
        /// <param name="categoryIds">Category id  between  from date and to date(modified date) </param>
        /// <param name="userName">current user selection in ui</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public List<CategoryFamilyProduct> GetCategoryFamilyProductData(DateTime fromDate, DateTime toDate, int catalogId, string categoryIds, string userName)
        {
            try
            {
                queryValue = new QueryValues();
                DateTime webSyncFromDate = Convert.ToDateTime(fromDate.ToString());
                DateTime webSyncTodate = Convert.ToDateTime(toDate.ToString());
                categoryIds = categoryIds.TrimEnd(',');
                var categoryFamilyProduct = queryValue.GetCategoryFamilyProduct(fromDate, toDate, catalogId, categoryIds, userName);
                return categoryFamilyProduct;

            }
            catch (Exception ex)
            {
                _logger.Error("Error at GetCategoryFamilyProductData : WebSyncApiController", ex);
            }
            return null;
        }

        #endregion

        #region GetCategoryAttribute
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryIds"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]

        public List<WebSyncCategoryDetails> GetCategoryAttributes(string categoryIds, JArray categoryFamilyProduct)
        {
            try
            {
                _dbContext = new CSEntities();
                queryValue = new QueryValues();
                // string[] category_Id = categoryIds.Split(',');
                List<WebSyncCategoryDetails> webSyncCategoryDetails = new List<WebSyncCategoryDetails>();
                var desCategoryFamilyProduct = categoryFamilyProduct[0].Select(x => new CategoryFamilyProduct
                {
                    PRODUCT_ID = int.Parse(x["PRODUCT_ID"].ToString()),
                    CATEGORY_ID = x["CATEGORY_ID"].ToString(),
                    FAMILY_ID = int.Parse(x["FAMILY_ID"].ToString()),
                    FMODIFIED_DATE = DateTime.Parse(x["FMODIFIED_DATE"].ToString()),
                    MODIFIED_DATE = DateTime.Parse(x["MODIFIED_DATE"].ToString()),
                    PMODIFIED_DATE = DateTime.Parse(x["PMODIFIED_DATE"].ToString())
                });
                var srcCategoryFamilyProduct = categoryFamilyProduct[1].Select(x => new CategoryFamilyProduct
                {
                    PRODUCT_ID = int.Parse(x["PRODUCT_ID"].ToString()),
                    CATEGORY_ID = x["CATEGORY_ID"].ToString(),
                    FAMILY_ID = int.Parse(x["FAMILY_ID"].ToString()),
                    FMODIFIED_DATE = DateTime.Parse(x["FMODIFIED_DATE"].ToString()),
                    MODIFIED_DATE = DateTime.Parse(x["MODIFIED_DATE"].ToString()),
                    PMODIFIED_DATE = DateTime.Parse(x["PMODIFIED_DATE"].ToString())
                });
                var listwebSyncCategoryDetails = queryValue.GetCategoryAttributes(categoryIds, 1).ToList();
                var familyList = desCategoryFamilyProduct.Join(srcCategoryFamilyProduct, des => des.FAMILY_ID, src => src.FAMILY_ID, (des, src) => new { des, src }).Join(_dbContext.TB_FAMILY, srcfam => srcfam.src.FAMILY_ID, fam => fam.FAMILY_ID, (srcfam, fam) => new { srcfam, fam }).Where(y => y.srcfam.src.FAMILY_ID != 0).Select(x => new WebSyncCategoryDetails
                {
                    CATEGORY_ID = "~" + x.fam.FAMILY_ID.ToString(),
                    CATEGORY_NAME = x.fam.FAMILY_NAME,
                    @checked = false,
                    id = x.srcfam.src.CATEGORY_ID + "~" + x.fam.FAMILY_ID,
                    hasChildren = false,//_dbContext.TB_PROD_FAMILY.Where(xx=>xx.FAMILY_ID==x.fam.FAMILY_ID && xx.FLAG_RECYCLE=="A") && _dbContext.TB_PROD_SPECS.Join(_dbContext.TB_PROD_FAMILY,tps=>tps.)
                    CATEGORY_SHORT = x.srcfam.src.CATEGORY_ID,
                    encoded = false,
                    FamilyandRelatedFamily = false,
                    Modify_Date = x.srcfam.src.MODIFIED_DATE,
                    Parent_Category = x.srcfam.src.CATEGORY_ID,
                    spriteCssClass = "family"
                });
                //var categoryDetailstesmp = _dbContext.TB_CATEGORY
                //    .Where(x => categoryIds.Contains(x.CATEGORY_ID))
                //    .Select(x => x);
                foreach (WebSyncCategoryDetails item in familyList)
                {
                    listwebSyncCategoryDetails.Add(item);
                }
                return listwebSyncCategoryDetails;


            }
            catch (Exception objException)
            {
                return null;
            }
            return null;

        }
        #endregion

        #region GetDeletedHistory
        [HttpPost]
        public List<string> DeletedHistory(string fromDate, string toDate, int CatalogID)
        {
            try
            {
                _dbContext = new CSEntities();
               
                var deletedHistory = _dbContext.STP_2WSTRANSACTIONHISTORY(fromDate, toDate, CatalogID).ToList();
                return deletedHistory;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at DeletedHistory : WebSyncApiController", ex);
            }
            return null;
        }
        #endregion

        #region InsertSession
        /// <summary>
        /// Insert / Update xml data to destination database
        /// </summary>
        /// <param name="listOfParameter"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public string InsertSession(ListOfParameter listOfParameter)
        {
            try
            {
                string status = string.Empty;
                int noOfDays;

                queryValue = new QueryValues();
                if (listOfParameter.familyId.Replace("~", "") == listOfParameter.productId.Replace("~", "").Replace("~", ""))
                {
                    listOfParameter.productId = "";
                }
                string urlBase = string.Empty;
                string xml, sessionId, send, updateType, jobType, user, jobName, fromDate, toDate;
                urlBase = listOfParameter.urlBase;
                sessionId = listOfParameter.sessionId;//
                send = listOfParameter.send;
                user = listOfParameter.user;
                updateType = listOfParameter.updateType;
                jobType = listOfParameter.jobType;
                jobName = listOfParameter.jobName;
                fromDate = listOfParameter.fromDate;
                int catalogId = listOfParameter.Catalog_Id;
                toDate = listOfParameter.toDate;
                DateTime endTime = DateTime.Now;
                System.Web.HttpContext.Current.Session["BaseUrl"] = urlBase;
                //freeSync = "C:\\ProgramData\\Microsoft\\Windows\\Start Menu\\Programs\\FreeFileSync";

                if (listOfParameter.WebSyncOthers == "True")
                {
                    if (freeSync != null && freeSync != "")
                    {
                        System.Diagnostics.Process.Start(freeSync);
                    }
                }
                //-------------------------------INDESIGN TEMPLATE START------------------------------------------
                if (sessionId != string.Empty && sessionId != "" && sessionId != "undefined" && listOfParameter.IndesignTemp == "True")
                {
                    var test = _dbContext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == "ITEM#").Select(x => x.ATTRIBUTE_ID).FirstOrDefault().ToString();
                    var Customer_Id = _dbContext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => x.CustomerId).FirstOrDefault().ToString();
                    int CustomerId = Int32.Parse(Customer_Id);
                    GetXmlIndesignTemplate(fromDate, toDate, CustomerId, sessionId, catalogId.ToString());
                }
                //-------------------------------INDESIGN TEMPLATE END ------------------------------------------




                RemoveLogDetails();

                if (sessionId != string.Empty && sessionId != "" && sessionId != "undefined")
                {
                    TB2WS_SESSION tb2WsSession = new TB2WS_SESSION();
                    tb2WsSession.SESSION_ID = sessionId;
                    tb2WsSession.SESSION_DATETIME = endTime;
                    tb2WsSession.SESSION_STATUS = send;
                    _dbContext.TB2WS_SESSION.Add(tb2WsSession);
                    _dbContext.SaveChanges();
                    var getdeletedHistory = _dbContext.STP_2WSTRANSACTIONHISTORY(fromDate, toDate, catalogId).ToList();
                    if (getdeletedHistory.Count != 0 && getdeletedHistory[0] != null)
                    {
                        xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" + getdeletedHistory[0];



                        var syncModel = new SyncData();
                        syncModel.createdUser = user;
                        syncModel.sessionId = sessionId;
                        syncModel.xmldata = new XmlDocument();
                        syncModel.xmldata.LoadXml(xml);
                        //syncModel.xmldata = xml;
                        syncModel.updateType = updateType;
                        syncModel.jobType = jobType;
                        syncModel.jobName = jobName;

                        HttpClient client = new HttpClient();
                        client.BaseAddress = new Uri(urlBase);
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var response = client.PostAsJsonAsync("/SyncData", syncModel).Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            return null;
                        }

                        xml = "";
                    }

                    DataTable familyXmlDataSet = new DataTable();
                    DataTable productXmlDataSet = new DataTable();
                    DataSet deletedAttrGrp = new DataSet();


                    try
                    {
                        using (SqlConnection _SQLConn = new SqlConnection(conStr))
                        {
                            using (SqlCommand cmd = new SqlCommand("STP_CATALOGSTUDIO_XML_ATTRIBUTEGROUP", _SQLConn))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                                cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                                cmd.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = catalogId;

                                _SQLConn.Open();
                                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                                adapter.Fill(familyXmlDataSet);

                                _SQLConn.Close();

                            }
                            using (SqlCommand cmd = new SqlCommand("STP_CATALOGSTUDIO_XML_ATTRIBUTEGROUPPRODUCT", _SQLConn))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                                cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                                cmd.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = catalogId;

                                _SQLConn.Open();
                                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                                adapter.Fill(productXmlDataSet);
                                _SQLConn.Close();
                            }

                            using (SqlCommand cmd = new SqlCommand("DELETEDATTRIBUTEGRUOP", _SQLConn))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;

                                cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                                cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                                cmd.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = catalogId;

                                _SQLConn.Open();
                                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                                adapter.Fill(deletedAttrGrp);
                                _SQLConn.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    //int userId = Convert.ToInt32(_dbContext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => x.CustomerId).Single());

                    int CustomerId = Convert.ToInt32(_dbContext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => x.CustomerId).Single());
                    XML(familyXmlDataSet, productXmlDataSet, deletedAttrGrp, sessionId, user, urlBase, CustomerId);
                    if (listOfParameter.WebSyncOthers == "True")
                    {
                        var Customer_Id = _dbContext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => x.CustomerId).FirstOrDefault().ToString();
                        int CustomerId_eXPORT = Int32.Parse(Customer_Id);
                        GetXmlExportTemplate(fromDate, toDate, CustomerId_eXPORT, sessionId, catalogId.ToString());
                        Websync_import_template(fromDate, toDate, catalogId.ToString(), user, urlBase);
                    }

                    if (listOfParameter.WebSyncOthers == "True")
                    {
                        /***********************WEBSYNC PICKLIST-START*****************/

                        DataTable webSync_Picklist = new DataTable();
                        try
                        {
                            using (SqlConnection SQLConn_Picklist = new SqlConnection(conStr))
                            {
                                SqlCommand cmd = new SqlCommand("STP_CATALOGSTUDIO_XML_WEBSYNC_PICKLIST", SQLConn_Picklist);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                                cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                                cmd.Parameters.Add("@USER", SqlDbType.NVarChar).Value = User.Identity.Name;

                                SQLConn_Picklist.Open();
                                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                                adapter.Fill(webSync_Picklist);
                                SQLConn_Picklist.Close();
                            }


                            Picklist_XML(webSync_Picklist, sessionId, user, urlBase);
                        }
                        catch (Exception e)
                        {
                            _logger.Error("Error at WebsyncApiController :  Websync_Picklist", e);
                        }
                        /***********************WEBSYNC PICKLIST-END*****************/
                    }

                    //string categoryId = string.Empty;
                    string familyName = string.Empty;
                    string SyncDataTypeFormat = string.Empty;

                    int barcount = 0;








                    DataTable webSync_getids = new DataTable();
                    using (SqlConnection SQLConn_getids = new SqlConnection(conStr))
                    {
                        SqlCommand cmd = new SqlCommand("STP_WEDSYNC_GETIDS", SQLConn_getids);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                        cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                        cmd.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = listOfParameter.categoryId.ToString().TrimEnd(',');
                        cmd.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar).Value = listOfParameter.Catalog_Id.ToString();

                        SQLConn_getids.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(webSync_getids);
                        SQLConn_getids.Close();
                    }
                    string famids = "";
                    string prdids = "";

                    foreach (DataRow row in webSync_getids.Rows)
                    {
                        for (int i = 0; i < webSync_getids.Columns.Count; i++)
                        {
                            prdids = row.ItemArray[0].ToString();
                            famids = row.ItemArray[1].ToString();
                        }
                    }




















                    //if (barcount == 0)
                    //{
                    int familyId = 0;
                    string familyIds = string.Empty;
                    string productID = "0";
                    string root_categoryId = "0";
                    string subProducts = "0";
                    string categoryIds = listOfParameter.categoryId.ToString().TrimEnd(',');
                    if (listOfParameter.familyId.Contains("~"))
                    {
                        familyIds = listOfParameter.familyId.ToString().TrimEnd(',');

                    }
                    else
                    {
                        familyIds = "~" + famids;
                    }

                    if (listOfParameter.productId.Contains("~~"))
                    {
                        productID = listOfParameter.productId.ToString().TrimEnd(',');
                    }
                    else
                    {
                        productID = "~~" + prdids;
                    }
                    if (listOfParameter.subProductId.Contains("~~~"))
                    {
                        subProducts = listOfParameter.subProductId.ToString().TrimEnd(',');
                    }
                    xml = "";
                    var syncModelCat = new SyncData();
                    syncModelCat.createdUser = user;
                    syncModelCat.sessionId = sessionId;
                    //syncModelCat.xmldata = xml;
                    if (xml == "")
                    {
                        syncModelCat.xmldata = null;
                    }
                    else
                    {
                        syncModelCat.xmldata = new XmlDocument();
                        syncModelCat.xmldata.LoadXml(xml);
                    }

                    syncModelCat.updateType = updateType;
                    syncModelCat.jobType = jobType;
                    syncModelCat.jobName = jobName;
                    #region categoryList
                    if (!string.IsNullOrEmpty(categoryIds))
                    {
                        foreach (string categoryId in categoryIds.Split(','))
                        {
                            var categoryDetails = _dbContext.TB_CATEGORY.Where(x => x.CATEGORY_ID == categoryId).FirstOrDefault();
                            if (categoryId.ToUpper().Contains("ATTR") || categoryDetails == null)
                            {
                                continue;
                            }


                            if (categoryDetails.PARENT_CATEGORY == "0")
                            {
                                barcount = 0;
                            }
                            if (!string.IsNullOrEmpty(familyIds) && familyIds != "undefined" && familyIds != null)
                            {
                                var familyDetails = _dbContext.TB_CATALOG_FAMILY.Where(x => x.CATEGORY_ID == categoryId && familyIds.Contains(x.FAMILY_ID.ToString()) && x.CATALOG_ID == catalogId).Select(y => y.FAMILY_ID).ToList();
                                if (familyDetails != null && familyDetails.Count != 0)
                                {
                                    #region Main family

                                    foreach (var familyDetail in familyDetails)
                                    {
                                        string productIDDetails = "0";
                                        string subProductsId = "0";
                                        int.TryParse(familyDetail.ToString(), out familyId);
                                        if (!string.IsNullOrEmpty(productID) && productID != "undefined" && productID != null && productID != "")
                                        {
                                            List<string> productDetails = _dbContext.TB_PROD_FAMILY.Where(x => x.FAMILY_ID == familyId && productID.Contains(x.PRODUCT_ID.ToString())).Select(y => y.PRODUCT_ID.ToString()).ToList();
                                            if (productDetails.Any())
                                            {
                                                productIDDetails = String.Join(",", productDetails);
                                            }
                                        }
                                        else
                                        {
                                            productIDDetails = "";

                                        }
                                        //var catalogList1 = _dbContext.STP_CATALOGSTUDIO5_FLATXMLTREE(catalogId, categoryId, root_categoryId, familyId, "Y", productID, fromDate, toDate);
                                        //   var catalogList1 =new List<TB_PROD_FAMILY>;
                                        if (!string.IsNullOrEmpty(subProducts) && subProducts != "0")
                                        {
                                            List<string> subProductDetails = productIDDetails != "0" ? _dbContext.TB_SUBPRODUCT.Where(x => productIDDetails.Contains(x.PRODUCT_ID.ToString()) && subProducts.Contains(x.SUBPRODUCT_ID.ToString())).Select(y => y.SUBPRODUCT_ID.ToString()).ToList() : _dbContext.TB_SUBPRODUCT.Where(x => subProducts.Contains(x.SUBPRODUCT_ID.ToString())).Select(y => y.SUBPRODUCT_ID.ToString()).ToList();
                                            if (subProductDetails.Any())
                                            {
                                                subProductsId = String.Join(",", subProductDetails);
                                            }
                                        }
                                        else
                                        {
                                            subProductsId = "";
                                        }
                                        barcount++;
                                        status = GenerateXml(categoryId, categoryDetails, catalogId, root_categoryId, familyId, productIDDetails, subProductsId, fromDate, toDate, xml, urlBase, syncModelCat);
                                    }
                                    #endregion familyList End
                                    // return status;
                                }

                            }

                            else if (barcount == 0)
                            {
                                var subCatDetails = _dbContext.TB_CATEGORY.Where(x => x.PARENT_CATEGORY == categoryId).FirstOrDefault();
                                if (subCatDetails != null && !categoryIds.Contains(subCatDetails.CATEGORY_ID))
                                {
                                    familyId = 0;
                                    productID = "0";
                                    root_categoryId = "0";
                                    subProducts = "0";
                                    if (categoryDetails != null)
                                    {
                                        if (categoryDetails.PARENT_CATEGORY == categoryId)
                                        {
                                            root_categoryId = "0";
                                        }
                                        else
                                        {
                                            root_categoryId = categoryDetails.PARENT_CATEGORY;
                                        }
                                        barcount++;
                                        status = GenerateXml(categoryId, categoryDetails, catalogId, root_categoryId, familyId, productID, subProducts, fromDate, toDate, xml, urlBase, syncModelCat);
                                    }
                                }

                            }
                        }
                        if (status != "")
                        {
                            return status;
                        }
                    }

                    if (barcount == 0 && !string.IsNullOrEmpty(categoryIds))
                    {

                        foreach (string categoryId in categoryIds.Split(','))
                        {
                            familyId = 0;
                            productID = "0";
                            root_categoryId = "0";
                            subProducts = "0";
                            var categoryDetails = _dbContext.TB_CATEGORY.Where(x => x.CATEGORY_ID == categoryId).FirstOrDefault();
                            if (categoryDetails != null)
                            {
                                if (categoryDetails.PARENT_CATEGORY == categoryId)
                                {
                                    root_categoryId = "0";
                                }
                                else
                                {
                                    root_categoryId = categoryDetails.PARENT_CATEGORY;
                                }
                                barcount++;

                                status = GenerateXml(categoryId, categoryDetails, catalogId, root_categoryId, familyId, productID, subProducts, fromDate, toDate, xml, urlBase, syncModelCat);
                            }
                        }
                        return status;
                    }
                    if (string.IsNullOrEmpty(categoryIds) && !string.IsNullOrEmpty(familyIds) && !string.IsNullOrEmpty(productID) && productID != "0")
                    {
                        foreach (var familyDetail in familyIds.Split(',').Distinct())
                        {
                            string productIDDetails = "0";
                            string subProductsIDDetails = "0";
                            familyId = 0;
                            int.TryParse(familyDetail.ToString().Replace("~", ""), out familyId);
                            if (!string.IsNullOrEmpty(productID) && productID != "0" && productID != "undefined" && productID != null)
                            {
                                List<string> productDetails = _dbContext.TB_PROD_FAMILY.Where(x => x.FAMILY_ID == familyId && productID.Contains(x.PRODUCT_ID.ToString())).Select(y => y.PRODUCT_ID.ToString()).ToList();
                                if (productDetails.Any())
                                {
                                    productIDDetails = String.Join(",", productDetails);
                                }
                                //subProducts = listOfParameter.subProductId.ToString().TrimEnd(',');

                            }
                            else
                            {
                                productIDDetails = "";
                            }
                            if (!string.IsNullOrEmpty(subProducts) && subProducts != "0")
                            {
                                subProducts = subProducts.Replace("~~~", "");
                                List<string> subProductDetails = productID != "" ? _dbContext.TB_SUBPRODUCT.Where(x => productID.Contains(x.PRODUCT_ID.ToString()) && subProducts.Contains(x.SUBPRODUCT_ID.ToString())).Select(y => y.SUBPRODUCT_ID.ToString()).ToList() : _dbContext.TB_SUBPRODUCT.Where(x => subProducts.Contains(x.SUBPRODUCT_ID.ToString())).Select(y => y.SUBPRODUCT_ID.ToString()).ToList();
                                if (subProductDetails.Any())
                                {
                                    subProductsIDDetails = String.Join(",", subProductDetails);
                                }
                            }
                            //var catalogList1 = _dbContext.STP_CATALOGSTUDIO5_FLATXMLTREE(catalogId, categoryId, root_categoryId, familyId, "Y", productID, fromDate, toDate);
                            //   var catalogList1 =new List<TB_PROD_FAMILY>;
                            root_categoryId = "0";
                            status = GenerateXml("", null, catalogId, root_categoryId, familyId, productIDDetails, subProductsIDDetails, fromDate, toDate, xml, urlBase, syncModelCat);
                        }




                        //  }




                        return status;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at InsertSession : WebSyncApiController", ex);
            }
            return null;

        }
        #endregion categoryList End

        public string SyncDatas(string sessionID, string xml, string user, string Upd_Type, string jobType, string job, string urlBase)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var syncModel = new SyncDataModel();
                syncModel.sessionId = sessionID;
                syncModel.xmldata = new XmlDocument();
                syncModel.xmldata.LoadXml(xml);
                // syncModel.xmldata = xml;
                syncModel.createdUser = user;
                syncModel.updateType = Upd_Type;
                syncModel.jobType = jobType;
                syncModel.jobName = job;
                //syncModel.CustomerId = CustomerId;
                var response = client.PostAsJsonAsync("/GetAttributeGroup", syncModel).Result;
                //GetAttributeGroup
                //SyncData
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }
                return null;
            }
            catch (Exception ex)
            {
                //_logger.Error("Error at InsertSession : SyncData", ex);
                return null;
            }

        }

        public string Picklist_XML(DataTable webSync_Picklist, string sessionId, string user, string urlBase)
        {
            try
            {
                string CustomerName = User.Identity.Name;

                var picklist_ListValues = webSync_Picklist.AsEnumerable().
                       Select(x => new
                       {
                           PICKLIST_NAME = x.Field<string>("PICKLIST_NAME").ToString(),
                           PICKLIST_DATA_TYPE = x.Field<string>("PICKLIST_DATA_TYPE").ToString(),
                           PICKLIST_DATA = x.Field<string>("PICKLIST_DATA").ToString()

                       }).Distinct().ToList();

                var xml_PicklistElements = new XElement("PICKLIST",
                 from emp in picklist_ListValues
                 select new XElement("ITEM",
                              new XAttribute("PICKLIST_NAME", emp.PICKLIST_NAME),
                              new XAttribute("PICKLIST_DATA_TYPE", emp.PICKLIST_DATA_TYPE),
                              new XAttribute("PICKLIST_DATA", emp.PICKLIST_DATA),
                               new XAttribute("CUSTOMER_NAME", CustomerName)
                            ));
                string xml_Picklist = string.Empty;
                xml_Picklist = xml_PicklistElements.ToString();
                SyncDatas(sessionId, xml_Picklist, user, "BOTH", "Live", "PICKLIST", urlBase);

                return null;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public string XML(DataTable familyXmlDataSet, DataTable productXmlDataSet, DataSet deletedAttribute, string sessionId, string user, string urlBase, int CustomerId)
        {

            try
            {
                DataTable deletedAttributeFamily = new DataTable();
                DataTable deletedAttributeProduct = new DataTable();
                deletedAttributeFamily = deletedAttribute.Tables[0];
                deletedAttributeProduct = deletedAttribute.Tables[1];
                string xmlAttributeGroup = "<tradingbell2ws_AttributeGroupPacket>";



                string familAttributeGroup = "<ATTRIBUTE_GROUP TYPE='FAMILY'>";
                string familyGroupName = string.Empty;

                // var deleteGroup_Name = deletedAttributeFamily.AsEnumerable().Select(x => new
                // {
                //     GROUP_ID = x.Field<int>("GROUP_ID").ToString(),
                //     GROUP_NAME = x.Field<string>("GROUP_NAME").ToString(),
                //     catalog_Id = x.Field<int>("CATALOG_ID").ToString()
                // }).Distinct().ToList();

                // var Deleted = new XElement("DeletedAttributeGroup",
                //from emp in deleteGroup_Name
                //select new XElement("ITEM",
                //            new XAttribute("GROUP_NAME", emp.GROUP_NAME),
                //             new XAttribute("CATALOG_ID", emp.catalog_Id)

                //          ));
                // string DeleteAttributeGroupName = Deleted.ToString();


                var group_Name = familyXmlDataSet.AsEnumerable().Select(s => new
                {
                    Name = s.Field<string>("GROUP_NAME").ToString(),
                    Id = s.Field<int>("GROUP_ID").ToString()
                }).Distinct().ToList();

                for (int i = 0; i < group_Name.Count; i++)
                {
                    string family_Group_Name = group_Name[i].Name;

                    var FAMILY = familyXmlDataSet.AsEnumerable().Where(s => s.Field<string>("GROUP_NAME") == family_Group_Name).
                        Select(x => new
                        {
                            catalog_Id = x.Field<int>("CATALOG_ID").ToString(),
                            Name = x.Field<string>("FAMILY_NAME").ToString(),
                            Id = x.Field<int>("FAMILY_Id").ToString(),
                            Level = x.Field<string>("LEVEL").ToString()
                        }).Distinct().ToList();

                    //Select(s => s.Field<string>("FAMILY_NAME").ToString()).ToList();
                    var Attribute = familyXmlDataSet.AsEnumerable().Where(s => s.Field<string>("GROUP_NAME") == family_Group_Name).Select(s => new
                    {
                        Name = s.Field<string>("ATTRIBUTE_NAME").ToString(),
                        Id = s.Field<int>("ATTRIBUTE_ID").ToString(),
                        Sortorder = s.Field<int>("SORT_ORDER").ToString()
                    }).Distinct().ToList();


                    var xEle = new XElement("FAMILY",
                    from emp in FAMILY
                    select new XElement("ITEM",
                                 new XAttribute("CATALOG_ID", emp.catalog_Id),
                                 new XAttribute("NAME", emp.Name),
                                 new XAttribute("ID", emp.Id),
                                  new XAttribute("LEVEL", emp.Level)
                               ), new XElement("ATTRIBUTE",
                                        from emp in Attribute
                                        select new XElement("ITEM",
                                      new XAttribute("NAME", emp.Name),
                                      new XAttribute("ID", emp.Id),
                                      new XAttribute("SORTORDER", emp.Sortorder))));
                    // string A = string.Empty;
                    // if (xEle != null)
                    // {
                    string A = xEle.ToString();
                    familyGroupName = familyGroupName + "<GROUP NAME='" + family_Group_Name + "' ID='" + group_Name[i].Id + "' CUSTOMER_ID='" + CustomerId + "'>" + A + "</GROUP>";


                    // }

                    //  familyGroupName = familyGroupName + "<GROUP NAME='" + group_Name[i] + "'>" + A + "</GROUP>";
                }
                if (familyGroupName != "")
                {
                    familAttributeGroup = familAttributeGroup + familyGroupName + "</ATTRIBUTE_GROUP>";
                }
                else
                {
                    familAttributeGroup = string.Empty;
                }


                string productAttributeGroup = "<ATTRIBUTE_GROUP TYPE='Product'>";

                //  var deleteProductGroup_Name = deletedAttributeProduct.AsEnumerable().Select(x => new
                //  {
                //      GROUP_ID = x.Field<int>("GROUP_ID").ToString(),
                //      GROUP_NAME = x.Field<string>("GROUP_NAME").ToString(),
                //      catalog_Id = x.Field<int>("CATALOG_ID").ToString()
                //  }).Distinct().ToList();

                //  var DeletedProduct = new XElement("DeletedAttributeGroup",
                //from emp in deleteProductGroup_Name
                //select new XElement("ITEM",
                //            new XAttribute("GROUP_NAME", emp.GROUP_NAME),
                //              new XAttribute("GROUP_NAME", emp.catalog_Id)
                //          ));
                //  string DeleteProductAttributeGroupName = DeletedProduct.ToString();

                string productGroupName = string.Empty;
                var productgroup_Name = productXmlDataSet.AsEnumerable().Select(s => new
                {
                    Name = s.Field<string>("GROUP_NAME").ToString(),
                    Id = s.Field<int>("GROUP_ID").ToString()
                }).Distinct().ToList();

                for (int i = 0; i < productgroup_Name.Count; i++)
                {
                    string product_group_Name = productgroup_Name[i].Name;

                    var FAMILY = productXmlDataSet.AsEnumerable().Where(s => s.Field<string>("GROUP_NAME") == product_group_Name).Select(x => new
                    {
                        catalog_Id = x.Field<int>("CATALOG_ID").ToString(),
                        Name = x.Field<string>("FAMILY_NAME").ToString(),
                        Id = x.Field<int>("FAMILY_Id").ToString(),
                        Level = x.Field<string>("LEVEL").ToString()

                    }).Distinct().ToList();
                    // Select(s => s.Field<string>("FAMILY_NAME").ToString() , .Field<string>("FAMILY_ID").Distinct().ToString()).ToList();
                    var Attribute = productXmlDataSet.AsEnumerable().Where(s => s.Field<string>("GROUP_NAME") == product_group_Name).Select(s => new
                    {
                        Name = s.Field<string>("ATTRIBUTE_NAME").ToString(),
                        Id = s.Field<int>("ATTRIBUTE_ID").ToString(),
                        Sortorder = s.Field<int>("SORT_ORDER").ToString()
                    }).Distinct().ToList();


                    var xEle = new XElement("FAMILY",
                    from emp in FAMILY
                    select new XElement("ITEM",
                                new XAttribute("CATALOG_ID", emp.catalog_Id),
                                new XAttribute("NAME", emp.Name),
                                new XAttribute("ID", emp.Id),
                                 new XAttribute("LEVEL", emp.Level)
                              ), new XElement("ATTRIBUTE",
                                        from emp in Attribute
                                        select new XElement("ITEM",
                                      new XAttribute("NAME", emp.Name),
                                      new XAttribute("ID", emp.Id),
                                      new XAttribute("SORTORDER", emp.Sortorder))));
                    //string A = string.Empty;
                    //if (xEle != null)
                    //{
                    string A = xEle.ToString();
                    // }

                    productGroupName = productGroupName + "<GROUP NAME='" + product_group_Name + "' ID='" + productgroup_Name[i].Id + "' CUSTOMER_ID='" + CustomerId + "' >" + A + "</GROUP>";
                }
                if (productGroupName != "")
                {
                    productAttributeGroup = productAttributeGroup + productGroupName + "</ATTRIBUTE_GROUP>";
                }
                else
                {
                    productAttributeGroup = string.Empty;
                }

                if (familAttributeGroup != "" || productAttributeGroup != "")

                {
                    xmlAttributeGroup = xmlAttributeGroup + familAttributeGroup + productAttributeGroup + " </tradingbell2ws_AttributeGroupPacket>";

                    SyncData syncData = new SyncData();
                    SyncDatas(sessionId, xmlAttributeGroup, user, "BOTH", "Live", "ATTR_GROUP", urlBase);
                }

                /***********Deleted attr group FAMILY*********/
                string DeleteAttributeGroupName = string.Empty;
                string DeleteProductAttributeGroupName = string.Empty;
                string xmlAttributeGroupDeletion = "<tradingbell2ws_AttributeGroupPacket>";
                var deleteGroup_Name = deletedAttributeFamily.AsEnumerable().Select(x => new
                {
                    GROUP_ID = x.Field<int>("GROUP_ID").ToString(),
                    GROUP_NAME = x.Field<string>("GROUP_NAME").ToString(),
                    catalog_Id = x.Field<int>("CATALOG_ID").ToString()
                }).Distinct().ToList();

                var deleteProductGroup_Name = deletedAttributeProduct.AsEnumerable().Select(x => new
                {
                    GROUP_ID = x.Field<int>("GROUP_ID").ToString(),
                    GROUP_NAME = x.Field<string>("GROUP_NAME").ToString(),
                    catalog_Id = x.Field<int>("CATALOG_ID").ToString()
                }).Distinct().ToList();

                deleteGroup_Name.AddRange(deleteProductGroup_Name);


                if (deleteGroup_Name != null)
                {

                    var Deleted = new XElement("DeletedAttributeGroup",
                     from emp in deleteGroup_Name
                     select new XElement("ITEM",
                                new XAttribute("GROUP_ID", emp.GROUP_ID),
                             new XAttribute("GROUP_NAME", emp.GROUP_NAME),
                              new XAttribute("CATALOG_ID", emp.catalog_Id)
                               ));
                    DeleteAttributeGroupName = Deleted.ToString();

                    //var deleted_pdtgrp=new XElement("PRODUCT",
                    //                    from emp in deleteProductGroup_Name
                    //                    select new XElement("ITEM",
                    //           new XAttribute("GROUP_ID", emp.GROUP_ID),
                    //        new XAttribute("GROUP_NAME", emp.GROUP_NAME),
                    //         new XAttribute("CATALOG_ID", emp.catalog_Id)
                    //          ));

                    //DeleteProductAttributeGroupName = deleted_pdtgrp.ToString();
                    //if(deleteGroup_Name==null)
                    //{
                    //    DeleteAttributeGroupName = string.Empty;
                    //}
                    //if (deleteProductGroup_Name == null)
                    //{
                    //    DeleteProductAttributeGroupName = string.Empty;
                    //}

                    xmlAttributeGroupDeletion = xmlAttributeGroupDeletion + "<ATTRIBUTE_GROUP>" + DeleteAttributeGroupName + "</ATTRIBUTE_GROUP>" + " </tradingbell2ws_AttributeGroupPacket>";
                    // SyncData syncData = new SyncData();
                    SyncDatas(sessionId, xmlAttributeGroupDeletion, user, "BOTH", "Live", "ATTR_GROUP_DELETION", urlBase);
                }
                /***********Deleted attr group PRODUCT*********/

            }
            catch (Exception ex)
            {
            }
            return null;
        }


        public string Websync_import_template(string fromDate, string toDate, string catalogId, string user, string urlBase)
        {
            try
            {
                string ImportXmlDatas = string.Empty;

                DataTable dt = new DataTable();
                // string conStr = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;

                using (SqlConnection _SQLConn = new SqlConnection(conStr))
                {
                    using (SqlCommand cmd = new SqlCommand("STP_CATALOGSTUDIO_XML_IMPORT_TEMPLATE", _SQLConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                        cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                        cmd.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = catalogId;

                        _SQLConn.Open();

                        SqlDataAdapter sda = new SqlDataAdapter(cmd);
                        sda.Fill(dt);
                        _SQLConn.Close();
                        DataSet ds = new DataSet();
                        ds.Tables.Add(dt);
                        ImportXmlDatas = ds.GetXml();

                        Guid objGuid = Guid.NewGuid();
                        string sessionID = System.Text.RegularExpressions.Regex.Replace(objGuid.ToString(), "[^a-zA-Z0-9_]+", "");
                        GetImportTemplates(sessionID, ImportXmlDatas, user, "BOTH", "Live", "IMPORT", urlBase);
                    }
                }
            }

            catch (Exception ex)
            {

            }
            return null;
        }


        public string GetImportTemplates(string sessionID, string xml, string user, string Upd_Type, string jobType, string job, string urlBase)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var syncModel = new SyncDataModel();
                syncModel.sessionId = sessionID;
                syncModel.xmldata = new XmlDocument();
                syncModel.xmldata.LoadXml(xml);
                // syncModel.xmldata = xml;
                syncModel.createdUser = user;
                syncModel.updateType = Upd_Type;
                syncModel.jobType = jobType;
                syncModel.jobName = job;
                //syncModel.CustomerId = CustomerId;
                var response = client.PostAsJsonAsync("/GetImportTemplates", syncModel).Result;
                //GetAttributeGroup
                //SyncData
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }
                return null;
            }
            catch (Exception ex)
            {
                //_logger.Error("Error at InsertSession : SyncData", ex);
                return null;
            }

        }


        /// <summary>
        /// Generate xml data and pass to destination
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="categoryDetails"></param>
        /// <param name="catalogId"></param>
        /// <param name="root_categoryId"></param>
        /// <param name="familyId"></param>
        /// <param name="productID"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <param name="xml"></param>
        /// <param name="urlBase"></param>
        /// <param name="syncModel"></param>
        /// <returns></returns>
        public string GenerateXml(string categoryId, TB_CATEGORY categoryDetails, int catalogId, string root_categoryId, int familyId, string productID, string subProductID, string fromDate, string toDate, string xml, string urlBase, SyncData syncModel)
        {
            try
            {
                if (categoryDetails != null)
                {
                    if (categoryDetails.PARENT_CATEGORY == categoryId)
                    {
                        root_categoryId = "0";
                    }
                    else
                    {
                        root_categoryId = categoryDetails.PARENT_CATEGORY;
                    }
                }
                DataTable xmlData = new DataTable();
                xmlData = queryValue.GetXmlData(catalogId, categoryId, root_categoryId, familyId, "N", productID, subProductID, fromDate, toDate);
                //Image PDF Attachement Start
                var allowimagepdfAttachement = _dbContext.Customer_Settings.Join(_dbContext.Customer_User, cs => cs.CustomerId, cu => cu.CustomerId, (cs, cu) => new { cs, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(x => new { ImagePdfAttachemnt = x.cs.ImagePdfAttachemnt, customerId = x.cu.CustomerId }).FirstOrDefault();
                if (allowimagepdfAttachement.ImagePdfAttachemnt == true)
                {
                    string customerPath = _dbContext.Customers.Where(x => x.CustomerId == allowimagepdfAttachement.customerId).Select(y => y.Comments).FirstOrDefault();
                    string path = HttpContext.Current.Server.MapPath("/");
                    string img_path = path + WebConfigurationManager.AppSettings["WebCatImagePath"] + customerPath;
                    string PDFpath = path + WebConfigurationManager.AppSettings["WebCatImagePath"] + customerPath;
                    var imageType = _dbContext.Image_Format_Types.Where(x => x.FORMAT_TYPE == 1).Select(x => x.FORMAT_TYPE_NAME).Distinct().ToList();
                    var imageFileName = _dbContext.TB_CATEGORY.Where(x => x.CATEGORY_ID == categoryId && !x.IMAGE_FILE.Equals(null)).Select(y => y.IMAGE_FILE).FirstOrDefault();
                    if (!string.IsNullOrEmpty(imageFileName))
                    {
                        if (imageFileName.ToString().Contains(".jpg"))
                        {
                            if ((imageFileName.ToString() != ""))
                            {
                                string img_name = imageFileName.ToString().Replace("/", "\\");
                                string ag6 = "";
                                string ag5 = "";
                                int arg7count = 0;
                                string arg1 = img_name.Split('_')[0];
                                int arg2count = img_name.LastIndexOf("\\");
                                string arg2 = img_name.Substring(arg2count + 1);
                                FileInfo imgext = new FileInfo(arg2);
                                if (imgext.Extension.ToUpper() != ".JPG" && imgext.Extension.ToUpper() != ".JPEG")
                                    arg2 = arg2.Replace(imgext.Extension, ".jpg");
                                int ct = 0;
                                for (int j = 0; j < img_name.Length; j++)
                                {

                                    if (img_name[j].ToString() == "\\")
                                        ct = ct + 1;
                                }
                                if (ct > 1)
                                {
                                    ag6 = img_name.Substring(1, img_name.LastIndexOf("\\"));
                                    arg7count = ag6.IndexOf("\\");
                                    ag5 = ag6.Substring(arg7count);
                                }
                                else
                                {
                                    ag5 = "\\";
                                }
                                if (imageType.Count != 0)
                                {
                                    foreach (var drow in imageType)
                                    {
                                        FileInfo imgext1 = new FileInfo(img_name);
                                        if (imgext1.Extension.ToUpper() != ".JPG" && imgext1.Extension.ToUpper() != ".JPEG")
                                        {
                                            img_name = img_name.Replace(imgext1.Extension, ".jpg");
                                        }
                                        string temp_img_name = img_name.ToString().Replace("_Images", drow.ToString());
                                        UploadImageWebservicecall(img_path + "\\" + temp_img_name.Replace(@"\", "\\"), temp_img_name.Replace(@"\", "\\"));
                                        temp_img_name = string.Empty;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(imageFileName.ToString()))
                        {
                            string img_name = imageFileName.ToString().Replace("/", "\\");

                            int arg2count = img_name.LastIndexOf("\\");
                            string arg2 = img_name.Substring(arg2count + 1);
                            string arg1 = img_name.Substring(0, arg2count);
                            FileInfo imgext = new FileInfo(arg2);
                            if (imgext.Extension.ToUpper() != ".JPG" && imgext.Extension.ToUpper() != ".JPEG")
                            {
                                arg2 = arg2.Replace(imgext.Extension, ".jpg");
                            }

                            UploadImageWebservicecall(img_path + img_name.Replace(@"\", "\\"), img_name.Replace(@"\", "\\"));
                            if (imageType.Count != 0)
                            {
                                foreach (var drow in imageType)
                                {
                                    FileInfo imgext1 = new FileInfo(img_name);
                                    if (imgext1.Extension.ToUpper() != ".JPG" && imgext1.Extension.ToUpper() != ".JPEG")
                                    {
                                        img_name = img_name.Replace(imgext1.Extension, ".jpg");
                                    }
                                    string temp_img_name = img_name.ToString().Replace("_Images", drow.ToString());
                                    UploadImageWebservicecall(img_path + temp_img_name.Replace(@"\", "\\"), temp_img_name.Replace(@"\", "\\"));
                                    temp_img_name = string.Empty;
                                }
                            }

                        }
                    }
                    //Start FamilyImageAttachement 
                    if (familyId != 0)
                    {
                        string img_name = string.Empty;
                        var imageattributeId = _dbContext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_TYPE == 9).Select(y => y.ATTRIBUTE_ID).ToList();
                        var familyImageName = _dbContext.TB_FAMILY_SPECS.Where(x => imageattributeId.Contains(x.ATTRIBUTE_ID) && x.FAMILY_ID == familyId && !x.STRING_VALUE.Equals(null)).Select(y => y.STRING_VALUE).ToList();
                        if (familyImageName.Count != 0)
                        {
                            foreach (var imageItem in familyImageName)
                            {
                                if (imageItem.ToLower().Contains(".jpg"))
                                {
                                    if ((imageItem != "") && (imageItem != string.Empty))
                                    {
                                        string ag6 = ""; string ag5 = "";
                                        int arg7count = 0;
                                        img_name = imageItem.ToString();
                                        string arg1 = img_name.Split('_')[0];
                                        int ct = 0;
                                        for (int j = 0; j < img_name.Length; j++)
                                        {
                                            if (img_name[j].ToString() == "\\")
                                            {
                                                ct = ct + 1;
                                            }
                                        }
                                        if (ct > 2)
                                        {
                                            ag6 = img_name.Substring(1, img_name.LastIndexOf("\\"));
                                            arg7count = ag6.IndexOf("\\");
                                            ag5 = ag6.Substring(arg7count);
                                        }
                                        else
                                        {
                                            ag5 = "\\";
                                        }
                                        int arg2count = img_name.LastIndexOf("\\");
                                        string arg2 = img_name.Substring(arg2count + 1);
                                        if (imageType.Count != 0)
                                        {
                                            foreach (var familyimageItem in imageType)
                                            {
                                                FileInfo imgext1 = new FileInfo(img_name);
                                                if (imgext1.Extension.ToUpper() != ".JPG" && imgext1.Extension.ToUpper() != ".JPEG")
                                                {
                                                    img_name = img_name.Replace(imgext1.Extension, ".jpg");
                                                }
                                                string temp_img_name = img_name.ToString().Replace("_Images", familyimageItem.ToString());

                                                UploadImageWebservicecall(img_path + temp_img_name.Replace(@"\", "\\"), temp_img_name.Replace(@"\", "\\"));
                                                temp_img_name = string.Empty;
                                            }
                                        }
                                    }
                                }
                                else if (imageItem.ToString().ToLower().Contains(".pdf"))
                                {


                                    string[] file_name = imageItem.Split('?');

                                    pdfupload(PDFpath + file_name[0].Replace(@"\", "\\"), file_name[0].Replace(@"\", "\\"));
                                }
                                else
                                {
                                    if ((imageItem.ToString() != "") && (imageItem.ToString() != string.Empty))
                                    {
                                        if (familyImageName.Count > 0)
                                        {

                                            img_name = familyImageName[0].ToString();

                                            int arg2count = img_name.LastIndexOf("\\");
                                            string arg2 = img_name.Substring(arg2count + 1);
                                            string arg1 = img_name.Substring(0, arg2count);
                                            FileInfo imgext = new FileInfo(arg2);
                                            if (imgext.Extension.ToUpper() != ".JPG" && imgext.Extension.ToUpper() != ".JPEG")
                                            {
                                                arg2 = arg2.Replace(imgext.Extension, ".jpg");
                                            }
                                            UploadImageWebservicecall(img_path + arg1 + "\\" + arg2, arg1 + "\\" + arg2);
                                            if (imageType != null)
                                            {
                                                foreach (var drow in imageType)
                                                {
                                                    FileInfo imgext1 = new FileInfo(img_name);
                                                    if (imgext1.Extension.ToUpper() != ".JPG" && imgext1.Extension.ToUpper() != ".JPEG")
                                                    {
                                                        img_name = img_name.Replace(imgext1.Extension, ".jpg");
                                                    }

                                                    string temp_img_name = img_name.ToString().Replace("_Images", imageType.ToString());

                                                    UploadImageWebservicecall(img_path + temp_img_name.Replace(@"\", "\\"), temp_img_name.Replace(@"\", "\\"));
                                                    temp_img_name = string.Empty;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(productID))
                    {
                        string prodId = string.Empty;

                        if (!string.IsNullOrEmpty(subProductID))
                        {
                            prodId = productID + "," + subProductID;
                        }
                        string img_name = string.Empty;
                        var imageattributeId = _dbContext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_TYPE == 3).Select(y => y.ATTRIBUTE_ID).ToList();
                        var ProductImageName = _dbContext.TB_PROD_SPECS.Where(x => prodId.Contains(x.PRODUCT_ID.ToString()) && imageattributeId.Contains(x.ATTRIBUTE_ID) && !x.STRING_VALUE.Equals(null)).Select(y => y.STRING_VALUE).ToList();
                        if (ProductImageName.Count != 0)
                        {
                            foreach (var imageItem in ProductImageName)
                            {
                                if (imageItem.ToString().Contains(".jpg") || imageItem.ToString().Contains(".jpeg"))
                                {
                                    if ((imageItem != "") && (imageItem != string.Empty))
                                    {
                                        string ag6 = ""; string ag5 = "";
                                        int arg7count = 0;
                                        img_name = imageItem.ToString();
                                        string arg1 = img_name.Split('_')[0];
                                        int ct = 0;
                                        for (int j = 0; j < img_name.Length; j++)
                                        {
                                            if (img_name[j].ToString() == "\\")
                                            {
                                                ct = ct + 1;
                                            }
                                        }
                                        if (ct > 2)
                                        {
                                            ag6 = img_name.Substring(1, img_name.LastIndexOf("\\"));
                                            arg7count = ag6.IndexOf("\\");
                                            ag5 = ag6.Substring(arg7count);
                                        }
                                        else
                                        {
                                            ag5 = "\\";
                                        }
                                        int arg2count = img_name.LastIndexOf("\\");
                                        string arg2 = img_name.Substring(arg2count + 1);
                                        if (imageType != null)
                                        {
                                            foreach (var productimageItem in imageType)
                                            {
                                                FileInfo imgext1 = new FileInfo(img_name);
                                                if (imgext1.Extension.ToUpper() != ".JPG" && imgext1.Extension.ToUpper() != ".JPEG")
                                                {
                                                    img_name = img_name.Replace(imgext1.Extension, ".jpg");
                                                }
                                                string temp_img_name = img_name.ToString().Replace("_Images", productimageItem.ToString());

                                                UploadImageWebservicecall(img_path + temp_img_name.Replace(@"\", "\\"), temp_img_name.Replace(@"\", "\\"));
                                                temp_img_name = string.Empty;
                                            }
                                        }
                                    }
                                }
                                else if (imageItem.ToString().ToLower().Contains(".pdf"))
                                {


                                    string[] file_name = imageItem.Split('?');

                                    pdfupload(PDFpath + file_name[0].Replace(@"\", "\\"), file_name[0].Replace(@"\", "\\"));
                                }
                                else
                                {
                                    if ((imageItem.ToString() != "") && (imageItem.ToString() != string.Empty))
                                    {
                                        img_name = imageItem.ToString();

                                        int arg2count = img_name.LastIndexOf("\\");
                                        string arg2 = img_name.Substring(arg2count + 1);
                                        string arg1 = img_name.Substring(0, arg2count);
                                        FileInfo imgext = new FileInfo(arg2);
                                        if (imgext.Extension.ToUpper() != ".JPG" && imgext.Extension.ToUpper() != ".JPEG")
                                        {
                                            arg2 = arg2.Replace(imgext.Extension, ".jpg");
                                        }
                                        UploadImageWebservicecall(img_path + arg1 + "\\" + arg2, arg1 + "\\" + arg2);
                                        if (imageType != null)
                                        {
                                            foreach (var drow in imageType)
                                            {
                                                FileInfo imgext1 = new FileInfo(img_name);
                                                if (imgext1.Extension.ToUpper() != ".JPG" && imgext1.Extension.ToUpper() != ".JPEG")
                                                {
                                                    img_name = img_name.Replace(imgext1.Extension, ".jpg");
                                                }

                                                string temp_img_name = img_name.ToString().Replace("_Images", imageType.ToString());

                                                UploadImageWebservicecall(img_path + temp_img_name.Replace(@"\", "\\"), temp_img_name.Replace(@"\", "\\"));
                                                temp_img_name = string.Empty;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    var categoryPDfFileName = _dbContext.TB_CATEGORY.Where(x => x.CATEGORY_ID == categoryId).Select(y => y.IMAGE_FILE2).FirstOrDefault();
                    if (!string.IsNullOrEmpty(categoryPDfFileName))
                    {
                        if (categoryPDfFileName.ToString().Contains(".PDF") || categoryPDfFileName.ToString().Contains(".pdf"))
                        {
                            if (string.IsNullOrEmpty(categoryPDfFileName.ToString()))
                            {
                                string[] file_name = categoryPDfFileName.ToString().Split('?');

                                pdfupload(PDFpath + file_name[0].Replace(@"\", "\\"), file_name[0].Replace(@"\", "\\"));
                            }
                        }
                    }
                }

                //End Image PDF Attachement End

                xml = xmlData != null ? xmlData.Rows.Count > 0 ? "<?xml version=\"1.0\" standalone=\"yes\"?>" + xmlData.Rows[0][0].ToString() : "<?xml version=\"1.0\" standalone=\"yes\"?>" : "<?xml version=\"1.0\" standalone=\"yes\"?>";

                //foreach (var items in catalogList)
                //{
                //    xml = xml + items;
                //}
                string columnCategory = string.Empty;
                var categoryColumnName = _dbContext.TB_CATEGORY_CHANGE_LIST.Where(x => x.CATEGORY_COLUMNS == categoryId && x.STATUS == true).Select(x => x.CATEGORY_COLUMNS).FirstOrDefault();
                XmlDocument document = new XmlDocument();
                document.LoadXml(xml);
                var isClone = _dbContext.TB_CATEGORY.Where(x => x.CATEGORY_ID == categoryId).Select(x => x.IS_CLONE).FirstOrDefault();
                if (isClone.ToString() != "1")
                {
                    if (categoryColumnName != null && categoryColumnName.Length > 1 && !categoryColumnName.Contains("Copied"))
                    {
                        if (categoryColumnName.Length > 0)
                        {
                            foreach (XmlNode child in document.DocumentElement.ChildNodes)
                            {
                                if (child.NodeType == XmlNodeType.Element)
                                {
                                    if (child.Name.ToString().ToLower().Contains("category"))
                                    {
                                        for (int l = 1; l < child.ChildNodes.Count; l++)
                                        {
                                            if ((!child.ChildNodes[l].Name.ToUpper().Contains("MODIFIED_DATE")) && (!child.ChildNodes[l].Name.ToUpper().Contains("SORT_ORDER")))
                                            {
                                                if (!categoryColumnName.ToString().ToLower().Contains("," + child.ChildNodes[l].Name.ToLower() + ","))
                                                {
                                                    child.RemoveChild(child.ChildNodes[l]);
                                                    l--;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            var categoryChangeList = _dbContext.TB_CATEGORY_CHANGE_LIST.Where(x => x.CATEGORY_COLUMNS == categoryId).FirstOrDefault();
                            if (categoryChangeList != null)
                            {
                                categoryChangeList.STATUS = true;
                                categoryChangeList.CATEGORY_COLUMNS = string.Empty;
                                _dbContext.SaveChanges();
                            }
                            xml = document.InnerXml.ToString();
                        }
                    }
                    else if (categoryColumnName != null && categoryColumnName.Contains("Copied"))
                    {
                        var categoryChangeList = _dbContext.TB_CATEGORY_CHANGE_LIST.Where(x => x.CATEGORY_COLUMNS == categoryId).FirstOrDefault();
                        if (categoryChangeList != null)
                        {
                            categoryChangeList.STATUS = true;
                            categoryChangeList.CATEGORY_COLUMNS = string.Empty;
                            _dbContext.SaveChanges();
                        }
                    }
                    //else
                    //{
                    //    int level = -1;
                    //    var categoryColumn = _dbContext.STP_CATALOGSTUDIO5_CategoryLevel(categoryId).ToList();
                    //    foreach (var items in categoryColumn)
                    //    {
                    //        level++;

                    //    }
                    //    columnCategory = "CATEGORY_ID,CATEGORY_NAME,MODIFIED_DATE";
                    //    if (level > 0)
                    //    {
                    //        int l = 1;
                    //        while (l <= level)
                    //        {
                    //            columnCategory = columnCategory + ",SUBCAT_L" + l + ",SUBCATNAME_L" + l + ",SUBCAT_L" + level + "_MODIFIED_DATE,SUBCAT_L" + l + "_PUBLISH,SUBCAT_L" + l + "_FLAG_RECYCLE,CUSTOMER_ID,SUBCAT_L" + l + "_CATEGORY_SHORT,SUBCAT_L" + l + "_CATEGORY_PARENT_SHORT,SUBCAT_L" + l + "_SHORT_DESC,SUBCAT_L" + l + "_IMAGE_FILE,SUBCAT_L" + l + "_IMAGE_FILE2,SUBCAT_L" + l + "_IMAGE_TYPE,SUBCAT_L" + l + "_IMAGE_TYPE2,SUBCAT_L" + l + "_IMAGE_NAME,SUBCAT_L" + l + "_IMAGE_NAME2,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD1,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD2,SUBCAT_L" + l + "_CUSTOM_NUM_FIELD3,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD1,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD2,SUBCAT_L" + l + "_CUSTOM_TEXT_FIELD3";
                    //            l++;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        columnCategory = columnCategory + ",MODIFIED_DATE,PUBLISH";
                    //    }
                    //    columnCategory = columnCategory + ",FLAG_RECYCLE,CUSTOMER_ID,CATEGORY_SHORT,CATEGORY_PARENT_SHORT,SHORT_DESC,IMAGE_FILE,IMAGE_FILE2,IMAGE_TYPE,IMAGE_TYPE2,IMAGE_NAME,IMAGE_NAME2,CUSTOM_NUM_FIELD1,CUSTOM_NUM_FIELD2,CUSTOM_NUM_FIELD3,CUSTOM_TEXT_FIELD1,CUSTOM_TEXT_FIELD2,CUSTOM_TEXT_FIELD3";
                    //    foreach (XmlNode child in document.DocumentElement.ChildNodes)
                    //    {
                    //        if (child.NodeType == XmlNodeType.Element)
                    //        {
                    //            if (child.Name.ToString().ToLower().Contains("category"))
                    //            {
                    //                for (int l = 1; l < child.ChildNodes.Count; l++)
                    //                {
                    //                    if ((!child.ChildNodes[l].Name.ToUpper().Contains("MODIFIED_DATE")) && (!child.ChildNodes[l].Name.ToUpper().Contains("SORT_ORDER")))
                    //                    {
                    //                        if (!columnCategory.ToString().ToLower().Contains("," + child.ChildNodes[l].Name.ToLower()))
                    //                        {
                    //                            child.RemoveChild(child.ChildNodes[l]);
                    //                            l--;
                    //                        }
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }
                    //    xml = document.InnerXml.ToString();

                    //}

                    //syncModel.xmldata = xml;
                    xml = document.InnerXml.ToString();
                    syncModel.xmldata = new XmlDocument();
                    syncModel.xmldata.LoadXml(xml);
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(urlBase);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                    var response = client.PostAsJsonAsync("/SyncData", syncModel).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return "Insert successful_" + syncModel.sessionId;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at InsertSession : WebSyncApiController", ex);
            }
            return null;
        }
        #endregion

        #region Get WebSync url
        /// <summary>
        /// Get websync destination url from customer setting
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public string GetWebSyncUrl()
        {
            try
            {
                _dbContext = new CSEntities();
                var customers = _dbContext.Customer_User.Where(x => x.User_Name == User.Identity.Name).FirstOrDefault();
                if (customers != null)
                {
                    var customerSetting = _dbContext.Customer_Settings.Where(x => x.CustomerId == customers.CustomerId).Select(y => y.WebcatSyncURL).FirstOrDefault();
                    return customerSetting.ToString();
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at InsertSession : WebSyncApiController", ex);
            }
            return null;

        }
        #endregion

        #region CategoryList for Tree
        /// <summary>
        /// Get CategoryList to bind tree
        /// </summary>
        /// <param name="categoryIds"></param>
        /// <param name="catalogId"></param>
        /// <param name="userName"></param>
        /// <param name="categoryidinnavigator"></param>
        /// <param name="familyandrelatedfamily"></param>
        /// <param name="customer_Id"></param>
        /// <returns></returns>
        private List<WebSyncCategoryDetails> GetCategoriesForTree(string categoryIds, int catalogId, string userName, bool categoryidinnavigator, bool familyandrelatedfamily, int customer_Id)
        {
            try
            {
                var CatList = _dbContext.TB_CATALOG_SECTIONS.Where(
                                                a => a.CATALOG_ID == catalogId && _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && !userName.Contains(tc.CREATED_USER) && categoryIds.Contains(tc.CATEGORY_ID) && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbContext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.CATEGORY_ID).FirstOrDefault().Contains(a.CATEGORY_ID))
                                                .Select(
                                                    a =>
                                                        new WebSyncCategoryDetails
                                                        {
                                                            id = a.CATEGORY_ID,
                                                            CATEGORY_ID = a.CATEGORY_ID,
                                                            CATEGORY_SHORT = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbContext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.CATEGORY_SHORT).FirstOrDefault(),
                                                            CUSTOMER_ID = _dbContext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name).CustomerId,
                                                            CATEGORY_NAME = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbContext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                                            CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                                            CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                                            // DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                                            // VERSION = a.TB_CATALOG.VERSION,
                                                            hasChildren = _dbContext.TB_CATEGORY.Join(_dbContext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) || _dbContext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId).Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbContext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                                            SORT_ORDER = a.SORT_ORDER,
                                                            CategoryIdinNavigator = categoryidinnavigator,
                                                            FamilyandRelatedFamily = familyandrelatedfamily,
                                                            spriteCssClass = _dbContext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.IS_CLONE).FirstOrDefault() == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryCloneWebSync" : "categoryWebSync"
                                                        }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();

                return CatList;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at InsertSession : WebSyncApiController", ex);
            }
            return null;

        }
        #endregion

        #region FamilyList for Tree
        /// <summary>
        /// Get FamilyList to bind tree
        /// </summary>
        /// <param name="familyIds"></param>
        /// <param name="webSyncFromDate"></param>
        /// <param name="webSyncTodate"></param>
        /// <param name="catalogId"></param>
        /// <param name="userName"></param>
        /// <param name="categoryidinnavigator"></param>
        /// <param name="familyandrelatedfamily"></param>
        /// <param name="customer_Id"></param>
        /// <returns></returns>
        private List<WebSyncCategoryDetails> GetFamilyList(string familyIds, DateTime webSyncFromDate, DateTime webSyncTodate, int catalogId, string userName, bool categoryidinnavigator, bool familyandrelatedfamily, int customer_Id)
        {
            try
            {
                var familyList = _dbContext.TB_CATALOG_FAMILY.Where(
                                                  a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && !userName.Contains(a.TB_FAMILY.CREATED_USER) && a.TB_FAMILY.MODIFIED_DATE >= webSyncFromDate && a.TB_FAMILY.MODIFIED_DATE <= webSyncTodate)
                                                  .Select(a => a.CATEGORY_ID).Distinct().ToList();


                if (familyIds != null)
                {
                    familyList = _dbContext.TB_CATALOG_FAMILY.Where(
                                                   a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && familyIds.Contains(a.FAMILY_ID.ToString()) && !userName.Contains(a.TB_FAMILY.CREATED_USER))
                                                   .Select(a => a.CATEGORY_ID).Distinct().ToList();
                }
                if (familyList == null || familyList.Count() == 0)
                {
                    return null;
                }
                if (familyList.Any())
                {
                    string catIds = string.Empty;
                    foreach (var famIds in familyList)
                    {
                        catIds = catIds + famIds.ToString() + ",";
                    }
                    catIds = catIds.TrimEnd(',');

                    return GetCategoriesForTree(catIds, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customer_Id);

                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        #endregion

        #region ProductList for Tree
        /// <summary>
        /// Get ProductList to bind tree
        /// </summary>
        /// <param name="productIds"></param>
        /// <param name="webSyncFromDate"></param>
        /// <param name="webSyncTodate"></param>
        /// <param name="catalogId"></param>
        /// <param name="userName"></param>
        /// <param name="categoryidinnavigator"></param>
        /// <param name="familyandrelatedfamily"></param>
        /// <param name="customer_Id"></param>
        /// <returns></returns>
        private List<WebSyncCategoryDetails> GetProductList(string productIds, DateTime webSyncFromDate, DateTime webSyncTodate, int catalogId, string userName, bool categoryidinnavigator, bool familyandrelatedfamily, int customer_Id)
        {
            try
            {
                var familyList = _dbContext.TB_PROD_FAMILY.Join(_dbContext.TB_PROD_SPECS, tsp => tsp.PRODUCT_ID, tps => tps.PRODUCT_ID, (tsp, tps) => new { tsp, tps }).Where(x => x.tps.MODIFIED_DATE >= webSyncFromDate &&
                x.tps.MODIFIED_DATE <= webSyncTodate && x.tsp.FLAG_RECYCLE == "A" && !userName.Contains(x.tps.CREATED_USER)).Select(y => y.tsp.FAMILY_ID).Distinct().ToList();

                if (productIds != null)
                {
                    familyList = _dbContext.TB_CATALOG_FAMILY.Join(_dbContext.TB_PROD_FAMILY, tcf => tcf.FAMILY_ID, TPF => TPF.FAMILY_ID, (tcf, tpf) => new { tcf, tpf }).Where(
                                               a => a.tpf.FLAG_RECYCLE == "A" && productIds.Contains(a.tpf.PRODUCT_ID.ToString()) && a.tcf.CATALOG_ID == catalogId)
                                               .Select(a => a.tpf.FAMILY_ID).ToList();
                }
                if (familyList.Any())
                {
                    string familyIds = string.Empty;
                    foreach (var famIds in familyList)
                    {
                        familyIds = familyIds + famIds.ToString() + ",";
                    }
                    familyIds = familyIds.TrimEnd(',');
                    //  return GetCategoriesForTree(catIds, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customer_Id);
                    return GetFamilyList(familyIds, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customer_Id);
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        #endregion

        #region SubProductList for Tree
        /// <summary>
        ///  Get SubProductList to bind tree
        /// </summary>
        /// <param name="webSyncFromDate"></param>
        /// <param name="webSyncTodate"></param>
        /// <param name="catalogId"></param>
        /// <param name="userName"></param>
        /// <param name="categoryidinnavigator"></param>
        /// <param name="familyandrelatedfamily"></param>
        /// <param name="customer_Id"></param>
        /// <returns></returns>
        private List<WebSyncCategoryDetails> GetSubProductList(DateTime webSyncFromDate, DateTime webSyncTodate, int catalogId, string userName, bool categoryidinnavigator, bool familyandrelatedfamily, int customer_Id)
        {
            try
            {
                var productList = _dbContext.TB_SUBPRODUCT.Join(_dbContext.TB_PROD_SPECS, tsp => tsp.SUBPRODUCT_ID, tps => tps.PRODUCT_ID, (tsp, tps) => new { tsp, tps }).Where(x => x.tps.MODIFIED_DATE >= webSyncFromDate && x.tps.MODIFIED_DATE <= webSyncTodate && !userName.Contains(x.tps.CREATED_USER)).Select(y => y.tsp.PRODUCT_ID).ToList();
                if (productList.Any())
                {
                    string productIds = string.Empty;
                    foreach (var subProdId in productList)
                    {
                        productIds = productIds + subProdId.ToString() + ",";
                    }
                    productIds = productIds.TrimEnd(',');
                    return GetProductList(productIds, webSyncFromDate, webSyncTodate, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customer_Id);

                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        #endregion

        #region Get SubProduct id
        /// <summary>
        ///  GetSubProducts which have a subproducts
        /// </summary>
        /// <param name="webSyncFromDate"></param>
        /// <param name="webSyncTodate"></param>
        /// <param name="catalogId"></param>
        /// <param name="userName"></param>
        /// <param name="categoryidinnavigator"></param>
        /// <param name="familyandrelatedfamily"></param>
        /// <param name="customer_Id"></param>
        /// <returns></returns>
        private string GetSubProductId(DateTime webSyncFromDate, DateTime webSyncTodate, int catalogId, string userName, bool categoryidinnavigator, bool familyandrelatedfamily, int customer_Id)
        {
            try
            {
                var productList = _dbContext.TB_SUBPRODUCT.Join(_dbContext.TB_PROD_SPECS, tsp => tsp.SUBPRODUCT_ID, tps => tps.PRODUCT_ID, (tsp, tps) => new { tsp, tps }).Where(x => x.tps.MODIFIED_DATE >= webSyncFromDate && x.tps.MODIFIED_DATE <= webSyncTodate && !userName.Contains(x.tps.CREATED_USER)).Select(y => y.tps.PRODUCT_ID).ToList();
                if (productList.Any())
                {
                    string productIds = string.Empty;
                    foreach (var subProdId in productList)
                    {
                        productIds = productIds + subProdId.ToString() + ",";
                    }

                    productIds = productIds.TrimEnd(',');
                    return productIds;

                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        #endregion

        #region Get Product id
        /// <summary>
        ///  GetProducts which have a subproducts
        /// </summary>
        /// <param name="webSyncFromDate"></param>
        /// <param name="webSyncTodate"></param>
        /// <param name="catalogId"></param>
        /// <param name="userName"></param>
        /// <param name="categoryidinnavigator"></param>
        /// <param name="familyandrelatedfamily"></param>
        /// <param name="customer_Id"></param>
        /// <returns></returns>
        private string GetProductId(string product_id, DateTime webSyncFromDate, DateTime webSyncTodate, int catalogId, string userName, bool categoryidinnavigator, bool familyandrelatedfamily, int customer_Id)
        {
            try
            {
                var productList = _dbContext.TB_SUBPRODUCT.Join(_dbContext.TB_PROD_SPECS, tsp => tsp.SUBPRODUCT_ID, tps => tps.PRODUCT_ID, (tsp, tps) => new { tsp, tps }).Where(x => x.tps.MODIFIED_DATE >= webSyncFromDate && x.tps.MODIFIED_DATE <= webSyncTodate && !userName.Contains(x.tps.CREATED_USER)).Select(y => y.tsp.PRODUCT_ID).ToList();
                if (!productList.Any())
                {
                    productList = _dbContext.TB_SUBPRODUCT.Join(_dbContext.TB_PROD_SPECS, tsp => tsp.SUBPRODUCT_ID, tps => tps.PRODUCT_ID, (tsp, tps) => new { tsp, tps }).Where(x => product_id.Contains(x.tsp.SUBPRODUCT_ID.ToString()) && !userName.Contains(x.tps.CREATED_USER)).Select(y => y.tsp.PRODUCT_ID).ToList();
                }
                if (productList.Any())
                {
                    string productIds = string.Empty;
                    foreach (var subProdId in productList)
                    {
                        productIds = productIds + subProdId.ToString() + ",";
                    }

                    productIds = productIds.TrimEnd(',');
                    return productIds;

                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        #endregion

        #region Get Family id
        /// <summary>
        /// GetFamilies which have a products
        /// </summary>
        /// <param name="productIds"></param>
        /// <param name="webSyncFromDate"></param>
        /// <param name="webSyncTodate"></param>
        /// <param name="catalogId"></param>
        /// <param name="userName"></param>
        /// <param name="categoryidinnavigator"></param>
        /// <param name="familyandrelatedfamily"></param>
        /// <param name="customer_Id"></param>
        /// <returns></returns>
        private string GetFamilyIds(string productIds, DateTime webSyncFromDate, DateTime webSyncTodate, int catalogId, string userName, bool categoryidinnavigator, bool familyandrelatedfamily, int customer_Id)
        {
            try
            {
                var familyList = _dbContext.TB_PROD_FAMILY.Join(_dbContext.TB_PROD_SPECS, tsp => tsp.PRODUCT_ID, tps => tps.PRODUCT_ID, (tsp, tps) => new { tsp, tps }).Where(x => x.tps.MODIFIED_DATE >= webSyncFromDate &&
                x.tps.MODIFIED_DATE <= webSyncTodate && !userName.Contains(x.tps.CREATED_USER)).Select(y => y.tsp.FAMILY_ID).ToList();

                if (productIds != null)
                {
                    familyList = _dbContext.TB_CATALOG_FAMILY.Join(_dbContext.TB_PROD_FAMILY, tcf => tcf.FAMILY_ID, TPF => TPF.FAMILY_ID, (tcf, tpf) => new { tcf, tpf }).Where(
                                              a => a.tpf.FLAG_RECYCLE == "A" && productIds.Contains(a.tpf.PRODUCT_ID.ToString()) && a.tcf.CATALOG_ID == catalogId)
                                              .Select(a => a.tpf.FAMILY_ID).ToList();
                }
                if (familyList.Any())
                {
                    string familyIds = string.Empty;
                    foreach (var famIds in familyList)
                    {
                        familyIds = familyIds + famIds.ToString() + ",";
                    }
                    familyIds = familyIds.TrimEnd(',');
                    //  return GetCategoriesForTree(catIds, catalogId, userName, categoryidinnavigator, familyandrelatedfamily, customer_Id);
                    return familyIds;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        #endregion

        #region Get Family id
        /// <summary>
        /// GetFamilies which have a products
        /// </summary>
        /// <param name="productIds"></param>
        /// <param name="webSyncFromDate"></param>
        /// <param name="webSyncTodate"></param>
        /// <param name="catalogId"></param>
        /// <param name="userName"></param>
        /// <param name="categoryidinnavigator"></param>
        /// <param name="familyandrelatedfamily"></param>
        /// <param name="customer_Id"></param>
        /// <returns></returns>
        private string GetCategoryIds(string familyId, DateTime webSyncFromDate, DateTime webSyncTodate, int catalogId, string userName, bool categoryidinnavigator, bool familyandrelatedfamily, int customer_Id)
        {
            try
            {
                var familyList = _dbContext.TB_CATALOG_FAMILY.Where(
                                                  a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && !userName.Contains(a.TB_FAMILY.CREATED_USER) && a.TB_FAMILY.MODIFIED_DATE >= webSyncFromDate && a.TB_FAMILY.MODIFIED_DATE <= webSyncTodate)
                                                  .Select(a => a.CATEGORY_ID).Distinct().ToList();


                if (familyId != null)
                {
                    familyList = _dbContext.TB_CATALOG_FAMILY.Where(
                                                   a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && familyId.Contains(a.FAMILY_ID.ToString()) && !userName.Contains(a.TB_FAMILY.CREATED_USER))
                                                   .Select(a => a.CATEGORY_ID).Distinct().ToList();
                }
                if (familyList == null || familyList.Count() == 0)
                {
                    return null;
                }
                string catIds = string.Empty;
                if (familyList.Any())
                {

                    foreach (var famIds in familyList)
                    {
                        catIds = catIds + famIds.ToString() + ",";
                    }
                    catIds = catIds.TrimEnd(',');

                }
                return catIds;
            }
            catch (Exception ex)
            {

            }
            return null;
        }
        #endregion


        public void UploadImageWebservicecall(string ImagePath, string ImageName)
        {
            try
            {
                string new_path = string.Empty;
                FileInfo Fil = new FileInfo(ImagePath);
                string[] ima_name = Fil.Name.Split('.');

                if (ima_name[1].ToString() != "jpg")
                {
                    new_path = imageconverter(Fil.DirectoryName, ima_name[0], ima_name[1]);
                }
                else
                {
                    new_path = ImagePath;
                }
                FileInfo Fil1 = new FileInfo(new_path);

                if (Fil1.Exists)
                {

                    FileStream fs = new FileStream(Fil1.FullName, FileMode.OpenOrCreate, FileAccess.Read);

                    Byte[] img = new Byte[fs.Length];

                    fs.Read(img, 0, Convert.ToInt32(fs.Length));
                    var ImageDetails = new ImagePDFAttachement();
                    ImageDetails.Image = img;
                    ImageDetails.ImageName = @"\\" + ImageName;
                    var urlBase = System.Web.HttpContext.Current.Session["BaseUrl"];
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(urlBase.ToString());
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.PostAsJsonAsync("/UploadImage", ImageDetails).Result;



                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at UploadImageWebservicecall : UploadImageWebservicecall", ex);
            }
        }

        public string imageconverter(string image_path, string img_name, string ext)
        {
            try
            {

                string FolderNamepath = image_path;
                string result = System.IO.Path.GetFileName(FolderNamepath);
                string name1 = img_name + "." + ext;
                string SectionName1 = "";
                string temp1 = image_path + "\\" + name1;
                string webCatImagePath = image_path;
                string imgMagickPath1 = ConfigurationManager.AppSettings["ConvertedImagePath"].ToString();
                string arg81 = "";
                string arg91 = "";
                string ag101 = "";
                string strFilePath = ConfigurationManager.AppSettings["ImageConversionJPG"].ToString() + "\\imageconvertjpg.bat";
                var psi = new System.Diagnostics.ProcessStartInfo(strFilePath);
                psi.CreateNoWindow = true;
                psi.UseShellExecute = false;
                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.WorkingDirectory = ConfigurationManager.AppSettings["ImageConversionJPG"].ToString();
                Process proc = Process.Start(psi);
                proc.StartInfo.Arguments = string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} ", '"' + SectionName1 + '"', '"' + name1 + '"', '"' + temp1 + '"', '"' + webCatImagePath + '"', '"' + SectionName1 + '"', '"' + imgMagickPath1 + '"', '"' + arg81 + '"', '"' + arg91 + '"', '"' + ag101 + '"');
                proc.Start();
                proc.WaitForExit();
                proc.Close();
                string path = image_path + @"\" + img_name + ".jpg";
                return path;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImagesConversion : imageconverter", ex);
            }

            return null;

        }
        public void pdfupload(string filepath, string filename)
        {
            try
            {
                FileInfo Fil;

                Fil = new FileInfo(filepath);

                if (Fil.Exists)
                {

                    FileStream fs = new FileStream(filepath, FileMode.OpenOrCreate, FileAccess.Read);

                    Byte[] fil = new Byte[fs.Length];
                    var PDFDetails = new PDFattachement();
                    PDFDetails.PDFName = fil;
                    PDFDetails.pdf = filename;
                    fs.Read(fil, 0, Convert.ToInt32(fs.Length));
                    var urlBase = System.Web.HttpContext.Current.Session["BaseUrl"];
                    HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri(urlBase.ToString());
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.PostAsJsonAsync("/PutFile", PDFDetails).Result;

                    if (response.IsSuccessStatusCode)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at pdfupload : pdfupload", ex);
            }
        }
        public void RemoveLogDetails()
        {
            try
            {
                int noOfDays;
                int.TryParse(ConfigurationManager.AppSettings["PDFRemoveInDays"].ToString(), out noOfDays);
                _dbContext = new CSEntities();
                var PDFDetails = new ImagePDFAttachement();
                PDFDetails.deletednoOfDays = noOfDays;
                var urlBase = System.Web.HttpContext.Current.Session["BaseUrl"];
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(urlBase.ToString());
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = client.PostAsJsonAsync("/RemoveLog", PDFDetails).Result;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at RemoveLogDetails : RemoveLogDetails", ex);
            }

        }

        //-------------------------------------------------------INDESIGN TEMPLATES----------------------------------------------------//
        public DataTable GetXmlIndesignTemplate(string fromDate, string toDate, int userId, string sessionID, string catalogID)
        {
            DataTable ProjectXmlDataSet = new DataTable();
            DataTable reocrdXmlDataSet = new DataTable();

            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection _SQLConn = new SqlConnection(conStr))
                {
                    //DateTime from_Date_temp = DateTime.ParseExact(fromDate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    //string from_Date = Convert.ToDateTime(from_Date_temp).ToString("yyyy-MM-dd");
                    //DateTime to_Date_temp = DateTime.ParseExact(toDate, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    //string to_Date = Convert.ToDateTime(to_Date_temp).ToString("yyyy-MM-dd");


                    using (SqlCommand cmd = new SqlCommand("STP_CATALOGSTUDIO_XML_INSDESGN_TEMPLATE", _SQLConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                        cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                        cmd.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = catalogID;
                        _SQLConn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(ProjectXmlDataSet);
                        _SQLConn.Close();
                    }
                }
            }
            catch (Exception ex) { }
            INDESIGNXML(ProjectXmlDataSet, userId, sessionID);
            return ProjectXmlDataSet;
        }
        public string INDESIGNXML(DataTable ProjectXmlDataSet, int CustomerId, string sessionID)
        {

            try
            {


                string Indesigntempxml = "<tradingbell2ws_IndesignPacket>";
                string Projectname = "<PROJECT'>";
                string IndesignProjectname = string.Empty;
                string urlBase = System.Web.HttpContext.Current.Session["BaseUrl"].ToString();

                var Project_Name = ProjectXmlDataSet.AsEnumerable().Select(s => new
                {
                    Name = s.Field<string>("PROJECT_NAME").ToString(),
                    Id = s.Field<int>("PROJECT_ID").ToString()
                }).Distinct().ToList();

                for (int i = 0; i < Project_Name.Count; i++)
                {
                    string Indesign_project_Name = Project_Name[i].Name.ToString(); ;
                    var PROJECT = ProjectXmlDataSet.AsEnumerable().Where(s => s.Field<string>("PROJECT_NAME") == Indesign_project_Name).
                        Select(x => new
                        {
                            project_id = x.Field<int>("PROJECT_ID"),
                            Name = x.Field<string>("PROJECT_NAME").ToString(),
                            Record_Id = x.Field<int>("RECORD_ID"),
                            Family_id = x.Field<int>("FAMILY_ID"),
                            Project_type = x.Field<byte>("PROJECT_TYPE"),
                            Workflow_Status = x.Field<int>("WORKFLOW_STATUS"),
                            Catalog_Id = x.Field<int>("CATALOG_ID"),
                            Xml_File_Name = x.Field<string>("XML_FILE_NAME").ToString(),
                            File_No = x.Field<int>("FILE_NO"),
                            Sort_Order = x.Field<int>("SORT_ORDER"),
                            Section_Name = x.Field<string>("SECTION_NAME").ToString(),
                            PUBLISHED_FSTATUS = x.Field<int>("PUBLISHED_FSTATUS"),
                            FILTER_STATUS = x.Field<bool>("FILTER_STATUS"),
                            CATEGORY_ID = x.Field<string>("CATEGORY_ID").ToString()

                        }).Distinct().ToList();

                    var xEle = new XElement("PROJECT",
                    from emp in PROJECT
                    select new XElement("ITEM",
                                 new XElement("PROJECT_ID", emp.project_id),
                                 new XElement("PROJECT_NAME", emp.Name),
                                 new XElement("RECORD_ID", emp.Record_Id),
                                 new XElement("FAMILY_ID", emp.Family_id),
                                 new XElement("PROJECT_TYPE", emp.Project_type),
                                 new XElement("WORKFLOW_STATUS", emp.Workflow_Status),
                                 new XElement("CATALOG_ID", emp.Catalog_Id),
                                 new XElement("XML_FILE_NAME", emp.Xml_File_Name),
                                 new XElement("FILE_NO", emp.File_No),
                                 new XElement("SORT_ORDER", emp.Sort_Order),
                                 new XElement("SECTION_NAME", emp.Section_Name),
                                 new XElement("PUBLISHED_FSTATUS", emp.PUBLISHED_FSTATUS),
                                 new XElement("FILTER_STATUS", emp.FILTER_STATUS),
                                 new XElement("CATEGORY_ID", emp.CATEGORY_ID)
                               ));

                    string xmltmep = xEle.ToString();
                    IndesignProjectname = IndesignProjectname + xmltmep;
                }

                if (Projectname != "")
                {
                    Indesigntempxml = Indesigntempxml + IndesignProjectname + " </tradingbell2ws_IndesignPacket>";
                    //string File_name1 = Application.StartupPath + "\\XML\\myXmlINDESIGN" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xml";
                    //File.WriteAllText(File_name1, Indesigntempxml);
                    //int userid = Int32.Parse(CustomerId);
                    SyncDatas(sessionID, Indesigntempxml, CustomerId.ToString(), "Time span", "Auto", "INDSIGN", urlBase);
                }
            }

            catch (Exception ex)
            {
                return null;
            }
            return "ok";

        }
        //-------------------------------------------------------INDESIGN TEMPLATES----------------------------------------------------//


        //-------------------------------------------------------EXPORT TEMPLATES----------------------------------------------------//
        public DataTable GetXmlExportTemplate(string fromDate, string toDate, int userId, string sessionID, string catalogID)
        {
            DataTable ProjectXmlDataSet = new DataTable();
            DataTable reocrdXmlDataSet = new DataTable();

            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection _SQLConn = new SqlConnection(conStr))
                {



                    using (SqlCommand cmd = new SqlCommand("STP_CATALOGSTUDIO_XML_EXPORT_TEMPLATE", _SQLConn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@FROM_DATE", SqlDbType.VarChar).Value = fromDate;
                        cmd.Parameters.Add("@TO_DATE", SqlDbType.VarChar).Value = toDate;
                        cmd.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = catalogID;
                        _SQLConn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(ProjectXmlDataSet);
                        _SQLConn.Close();
                    }
                }
            }
            catch (Exception ex) { }
            EXPORTXML(ProjectXmlDataSet, userId, sessionID);
            return ProjectXmlDataSet;
        }
        public string EXPORTXML(DataTable ProjectXmlDataSet, int CustomerId, string sessionID)
        {

            try
            {


                string Exporttempxml = "<tradingbell2ws_ExportPacket>";
                string Projectname = "<PROJECT'>";
                string ExportProjectname = string.Empty;
                string urlBase = System.Web.HttpContext.Current.Session["BaseUrl"].ToString();

                var Project_Name = ProjectXmlDataSet.AsEnumerable().Select(s => new
                {
                    Name = s.Field<string>("PROJECT_NAME").ToString(),

                }).Distinct().ToList();

                for (int i = 0; i < Project_Name.Count; i++)
                {
                    string Export_project_Name = Project_Name[i].Name.ToString(); ;
                    var PROJECT = ProjectXmlDataSet.AsEnumerable().Where(s => s.Field<string>("PROJECT_NAME") == Export_project_Name).
                        Select(x => new
                        {
                            project_id = x.Field<int>("PROJECT_ID"),
                            Name = x.Field<string>("PROJECT_NAME").ToString(),
                            Project_type = x.Field<byte>("PROJECT_TYPE"),
                            Workflow_Status = x.Field<int>("WORKFLOW_STATUS"),
                            Catalog_Id = x.Field<int>("CATALOG_ID"),
                            EXPORT_RULES = x.Field<string>("EXPORT_RULES").ToString(),
                            TEMPLATE_ID = x.Field<int>("TEMPLATE_ID"),
                            EXPORT_FORMAT = x.Field<string>("EXPORT_FORMAT").ToString(),
                            EXPORT_ATTRIBUTE = x.Field<string>("EXPORT_ATTRIBUTE").ToString(),
                            HIERARCHY_REQUIRED = x.Field<bool>("HIERARCHY_REQUIRED"),
                            APPLY_FILTER = x.Field<bool>("APPLY_FILTER"),
                            EXPORT_ID_COLUMN = x.Field<bool>("EXPORT_ID_COLUMN"),
                            EXPORT_TYPE = x.Field<string>("EXPORT_TYPE").ToString(),
                            DELIMITER = x.Field<string>("DELIMITER").ToString(),

                        }).Distinct().ToList();

                    var xEle = new XElement("PROJECT",
                    from emp in PROJECT
                    select new XElement("ITEM",
                                 new XElement("PROJECT_ID", emp.project_id),
                                 new XElement("PROJECT_NAME", emp.Name),
                                 new XElement("EXPORT_RULES", emp.EXPORT_RULES),
                                 new XElement("PROJECT_TYPE", emp.Project_type),
                                 new XElement("WORKFLOW_STATUS", emp.Workflow_Status),
                                 new XElement("CATALOG_ID", emp.Catalog_Id),

                                  new XElement("TEMPLATE_ID", emp.TEMPLATE_ID),
                                 new XElement("EXPORT_FORMAT", emp.EXPORT_FORMAT),
                                 new XElement("EXPORT_ATTRIBUTE", emp.EXPORT_ATTRIBUTE),
                                 new XElement("HIERARCHY_REQUIRED", emp.HIERARCHY_REQUIRED),
                                 new XElement("APPLY_FILTER", emp.APPLY_FILTER),
                                 new XElement("EXPORT_ID_COLUMN", emp.EXPORT_ID_COLUMN),

                                  new XElement("EXPORT_TYPE", emp.EXPORT_TYPE),
                                 new XElement("DELIMITER", emp.DELIMITER)

                               ));

                    string xmltmep = xEle.ToString();
                    ExportProjectname = ExportProjectname + xmltmep;
                }

                if (Projectname != "")
                {
                    Exporttempxml = Exporttempxml + ExportProjectname + " </tradingbell2ws_ExportPacket>";
                    //string File_name1 = Application.StartupPath + "\\XML\\myXmlINDESIGN" + System.DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + ".xml";
                    //File.WriteAllText(File_name1, Indesigntempxml);
                    //int userid = Int32.Parse(CustomerId);
                    SyncDatas(sessionID, Exporttempxml, CustomerId.ToString(), "Time span", "Auto", "EXPORT", urlBase);
                }
            }

            catch (Exception ex)
            {
                return null;
            }
            return "ok";

        }
        //-------------------------------------------------------EXPORT TEMPLATES----------------------------------------------------//
    }
}
