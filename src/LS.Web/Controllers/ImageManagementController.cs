﻿//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Web.Script.Serialization;
//using System.Web.Mvc;
//using LS.Data.Model.ProductPreview;
//using Newtonsoft.Json;
//using System.Data.SqlClient;
//using LS.Data.Model.CatalogSectionModels;
//using System.IO;
//using System.Reflection;
//using System.CodeDom;
//using System.CodeDom.Compiler;
//using log4net;
//using System.Collections;
//using System.Xml.Serialization;
//using System.Web;
//using LS.Web.Utility;
//using System.Web.Security;
//using LS.Data;
//using LS.Data.Model;
//using System;
//using System.Collections.Generic;
//using System.Web.Http;
//using System.Net.Http;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Kendo.DynamicLinq;
using LS.Data;
using LS.Data.Model.CatalogSectionModels;
using log4net;
using LS.Data.Model;
using System.Web.Http;
using System.Collections;
using System.Net.Http;
using System.Net;
using LS.Data.Utilities;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using LS.Data.Model.ProductView;
using System.Data.OleDb;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data;
using System;
using System.Globalization;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Linq;
using System.Configuration;
using System.Web.Script.Serialization;
using Infragistics.Documents.Excel;
using System.Xml.Linq;

namespace LS.Web.Controllers
{
    public class ImageManagementController : Controller
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(FamilyController));
        readonly CSEntities _dbcontext = new CSEntities();
        private readonly ILog _logger = LogManager.GetLogger(typeof(AccountsController));
        public string SqlString = "";
        private string _mFamilyId = "2", _mCatalogId = "2", _mCategoryId = "";
        public string CustomerFolder = string.Empty;
        private static readonly char[] SpecialChars = "'#&".ToCharArray();
        public string ImageMagickPath = string.Empty;
        public string Exefile = string.Empty;
        public string convertionPath = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePath"].ToString();
        public string convertionPathREA = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePathREA"].ToString();
        // GET: ImageManagement
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Manage()
        {
            return View();
        }
        public JsonResult SaveFilesManageDrive(string customerFolder)
         {
            try
            {
                string fileName, actualFileName;
                string FilePath = string.Empty;
               string Message = fileName = actualFileName = string.Empty;
                // bool flag = false;
                if (customerFolder == "undefined")
                {
                    customerFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["ServerXMLFileUploadPath"].ToString();
                }
                if (string.IsNullOrEmpty(customerFolder))
               { CustomerFolder = "/STUDIOSOFT"; }
               else
               {
                   CustomerFolder = "/" + customerFolder.Replace("&", "");
               }

                if (Request.Files != null)
                {
                    var file = Request.Files[0];
                    actualFileName = file.FileName;
                    fileName = Path.GetFileName(file.FileName);
                    int size = file.ContentLength;
                    try
                    {
                        if (fileName != null && fileName.IndexOfAny(SpecialChars) == -1)
                        {
                            if (Directory.Exists(customerFolder))
                            {
                                var physicalPath = Path.Combine(customerFolder, fileName);


                              //  physicalPath = string.Concat(@"\\rnd07\D$\IIS\CS10.6_LastestBuild\Design\Login\assets\images\", "nayan-1-1522664756-1532519210.jpg");


                                //var physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages/" + customerFolder), fileName);
                                file.SaveAs(physicalPath);
                                var chkFile =
                                new FileInfo(physicalPath);
                                ImageAndImageSizeConvert(chkFile, physicalPath);
                            }
                            else
                            {
                                //Directory.CreateDirectory(Server.MapPath("~/Content/ProductImages/" + customerFolder));
                                //var physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages/" + customerFolder), fileName);
                                Directory.CreateDirectory(customerFolder);
                                var physicalPath = Path.Combine(customerFolder, fileName);
                                file.SaveAs(physicalPath);
                                var chkFile =
                                new FileInfo(physicalPath);
                                ImageAndImageSizeConvert(chkFile, physicalPath);
                            }

                        }
                        else
                        {
                            Message = "File name have some invalid format";

                        }

                    }
                    catch (Exception)
                    {
                        Message = "File upload failed, please try again";
                    }

                }
                return Json(FilePath, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objex)
            {
                _logger.Error("Error at ImportController : SaveFiles", objex);
                return null;
            }
        }
        public JsonResult SaveXMLFilesManageDrive(string customerFolder,string filename)
        {
            try
            {
                string actualFileName;
                string FilePath = string.Empty;
                string Message  = actualFileName = string.Empty;
                string path = @"D:\Project\Marketstudio Pim\src\LS.Web\Content\ProductImages\DEMO\Indesignxml\";
                if (customerFolder == "undefined")
                {
                    customerFolder = System.Web.Configuration.WebConfigurationManager.AppSettings["ServerXMLFileUploadPath"].ToString();
                }
                if (string.IsNullOrEmpty(customerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                {
                    CustomerFolder = "/" + customerFolder.Replace("&", "");
                }
                string pathName = Path.Combine(path, filename); // location is the string from your form.
                System.IO.File.Copy(pathName, customerFolder+ filename, true);

                return Json(FilePath, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objex)
            {
                _logger.Error("Error at ImportController : SaveFiles", objex);
                return null;
            }
        }
        [System.Web.Http.HttpPost]
        public ActionResult SaveImageManagementFile(IEnumerable<HttpPostedFileBase> files, string customerFolder)
        {
         //   IEnumerable<HttpPostedFileBase>files = file.File;
            if (files != null)
            {
                // CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                {
                    CustomerFolder = "/" + CustomerFolder.Replace("&", "");
                }

                foreach (var file in files)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    if (fileName != null && fileName.IndexOfAny(SpecialChars) == -1)
                    {
                        if (Directory.Exists(customerFolder))
                        {
                            var physicalPath = Path.Combine(customerFolder, fileName);

                            //var physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages/" + customerFolder), fileName);
                            file.SaveAs(physicalPath);
                            var chkFile =
                                new FileInfo(System.Web.HttpContext.Current.Server.MapPath(physicalPath));
                            ImageAndImageSizeConvert(chkFile, physicalPath);
                        }
                        else
                        {
                            //Directory.CreateDirectory(Server.MapPath("~/Content/ProductImages/" + customerFolder));
                            //var physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages/" + customerFolder), fileName);
                            Directory.CreateDirectory(customerFolder);
                            var physicalPath = Path.Combine(customerFolder, fileName);
                            file.SaveAs(physicalPath);
                            var chkFile =
                                new FileInfo(System.Web.HttpContext.Current.Server.MapPath(physicalPath));
                            ImageAndImageSizeConvert(chkFile,physicalPath);
                        }

                    }
                }
            }
            return Content("");
        }

        [System.Web.Http.HttpPost]
        public void ImageAndImageSizeConvert(FileInfo chkFile, string path)
        {
            string convertCustFolder = string.Empty; string convertPath = string.Empty;
            CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
            if (string.IsNullOrEmpty(CustomerFolder))
            { CustomerFolder = "/STUDIOSOFT"; }
            else
            { CustomerFolder = CustomerFolder.Replace("&", ""); }
            int filePosition = chkFile.ToString().LastIndexOf(CustomerFolder);
            convertCustFolder = Convert.ToString(chkFile.DirectoryName.Substring(filePosition));
            if (convertCustFolder.Contains("\\"))
            {
                convertCustFolder = convertCustFolder.Substring(convertCustFolder.IndexOf("\\"));
                convertPath = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile" + convertCustFolder;

                if (!Directory.Exists(convertPath))
                {
                    Directory.CreateDirectory(convertPath);
                }
            }
            else
            {
                convertCustFolder = "";
            }

            string imageFullPath = "";
            string imgFile1 = string.Empty;
            string imgFile2 = string.Empty;
            string imageFile1 = string.Empty;
            string imageFile2 = string.Empty;
            bool imageType = false;
            var imagemagickpath = _dbcontext.Customer_Settings.FirstOrDefault(a => a.CustomerId == 1);
            string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == 1).Select(a => a.ConverterType).SingleOrDefault();
            if (imagemagickpath != null && converttype == "Image Magic")
            {
                ImageMagickPath = convertionPath;
                ImageMagickPath = ImageMagickPath.Replace("\\\\", "\\");
            }
            else
            {
                ImageMagickPath = convertionPathREA;
                ImageMagickPath = ImageMagickPath.Replace("\\\\", "\\");
            }
            try
            {
                #region "Image Convert"

                // ************************************* image convert  *******************************

                if (chkFile != null)
                {
                    if (chkFile.Extension != null && chkFile.Extension.Trim().Length > 0)
                        if (chkFile.Extension.ToUpper() == ".AI" || chkFile.Extension.ToUpper() == ".BMP" ||
                            chkFile.Extension.ToUpper() == ".JPG" || chkFile.Extension.ToUpper() == ".GIF" ||
                            chkFile.Extension.ToUpper() == ".EPS" || chkFile.Extension.ToUpper() == ".PDF" || chkFile.Extension.ToUpper() == ".SVG" ||
                            chkFile.Extension.ToUpper() == ".PSD" || chkFile.Extension.ToUpper() == ".PNG" ||
                            chkFile.Extension.ToUpper() == ".TIF" || chkFile.Extension.ToUpper() == ".RTF" ||
                            chkFile.Extension.ToUpper() == ".TIFF" || chkFile.Extension.ToUpper() == ".DOC")
                        {
                            if (chkFile.Extension.ToLower().EndsWith(".jpg") ||
                                chkFile.Extension.ToLower().EndsWith(".bmp") ||
                                chkFile.Extension.ToLower().EndsWith(".gif"))
                            {
                                imageType = true;
                                ////// imageFullPath = _imagepath + _pivotTable.Rows[i][j];
                                string firstarg = path;
                                string[] imgargs =
                                {
                                    firstarg,
                                    System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile" + convertCustFolder +
                                    chkFile.ToString()
                                        .Substring(chkFile.ToString().LastIndexOf('\\'),
                                            chkFile.ToString().LastIndexOf('.') -
                                            chkFile.ToString().LastIndexOf('\\')) + ".jpg"
                                };

                                imageFullPath = imgargs[0].Replace("&", "");
                                imageFullPath = imageFullPath.Replace("&", "&amp;");
                                System.IO.File.Copy(imgargs[0], imgargs[1], true);
                            }
                            else if (chkFile.Extension.ToLower().EndsWith(".tif") ||
                                     chkFile.Extension.ToLower().EndsWith(".tiff") ||
                                     chkFile.Extension.ToLower().EndsWith(".tga") ||
                                     chkFile.Extension.ToLower().EndsWith(".pcx") ||
                                     chkFile.Extension.ToLower().EndsWith(".png"))
                            {
                                if (Directory.Exists(ImageMagickPath))
                                {
                                    string firstarg = path;
                                    string[] imgargs =
                                    {
                                        firstarg,
                                        System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile" + convertCustFolder+
                                        chkFile.ToString()
                                            .Substring(chkFile.ToString().LastIndexOf('\\'),
                                                chkFile.ToString().LastIndexOf('.') -
                                                chkFile.ToString().LastIndexOf('\\')) + ".jpg"
                                    };
                                    if (converttype == "Image Magic")
                                    { Exefile = "convert.exe"; }
                                    else
                                    { Exefile = "Reaconverter7.exe"; }
                                    var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");

                                    if (System.IO.File.Exists(imgargs[1].Replace("&", "")))
                                    {
                                        System.IO.File.Delete(imgargs[1].Replace("&", ""));
                                    }
                                    pStart.CreateNoWindow = true;
                                    pStart.UseShellExecute = false;
                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                    System.Diagnostics.Process cmdProcess =
                                        System.Diagnostics.Process.Start(pStart);
                                    while (!cmdProcess.HasExited)
                                    {
                                    }
                                }
                                imageType = true;
                                imageFullPath = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) +
                                                "\\temp\\ImageandAttFile" + convertCustFolder +
                                                chkFile.ToString()
                                                    .Substring(chkFile.ToString().LastIndexOf('\\'),
                                                        chkFile.ToString().LastIndexOf('.') -
                                                        chkFile.ToString().LastIndexOf('\\')) + ".jpg";
                                imageFullPath = imageFullPath.Replace("&", "");
                            }
                            else if (chkFile.Extension.ToLower().EndsWith(".psd"))
                            {
                                if (Directory.Exists(ImageMagickPath))
                                {
                                    string firstarg = path;
                                    string[] imgargs =
                                    {
                                        firstarg+"[0]",
                                        System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile" + convertCustFolder+
                                        chkFile.ToString()
                                            .Substring(chkFile.ToString().LastIndexOf('\\'),
                                                chkFile.ToString().LastIndexOf('.') -
                                                chkFile.ToString().LastIndexOf('\\')) + ".jpg"
                                    };
                                    if (converttype == "Image Magic")
                                    { Exefile = "convert.exe"; }
                                    else
                                    { Exefile = "Reaconverter7.exe"; }
                                    var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");

                                    if (System.IO.File.Exists(imgargs[1].Replace("&", "")))
                                    {
                                        System.IO.File.Delete(imgargs[1].Replace("&", ""));
                                    }
                                    pStart.CreateNoWindow = true;
                                    pStart.UseShellExecute = false;
                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                    System.Diagnostics.Process cmdProcess =
                                        System.Diagnostics.Process.Start(pStart);
                                    while (!cmdProcess.HasExited)
                                    {
                                    }
                                }
                                imageType = true;
                                imageFullPath = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) +
                                                "\\temp\\ImageandAttFile" + convertCustFolder +
                                                chkFile.ToString()
                                                    .Substring(chkFile.ToString().LastIndexOf('\\'),
                                                        chkFile.ToString().LastIndexOf('.') -
                                                        chkFile.ToString().LastIndexOf('\\')) + ".jpg";
                                imageFullPath = imageFullPath.Replace("&", "");
                            }
                            else if (chkFile.Extension.ToLower().EndsWith(".eps"))
                            {
                                if (Directory.Exists(ImageMagickPath))
                                {
                                    string firstarg = path;
                                    string[] imgargs =
                                    {
                                        firstarg,
                                        System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) + "\\temp\\ImageandAttFile" + convertCustFolder+
                                        chkFile.ToString()
                                            .Substring(chkFile.ToString().LastIndexOf('\\'),
                                                chkFile.ToString().LastIndexOf('.') -
                                                chkFile.ToString().LastIndexOf('\\')) + ".png"
                                    };

                                    imageFullPath = imgargs[0].Replace("&", "");
                                    if (converttype == "Image Magic")
                                    { Exefile = "convert.exe"; }
                                    else
                                    { Exefile = "Reaconverter7.exe"; }
                                    var pStart = new System.Diagnostics.ProcessStartInfo(ImageMagickPath + "\\" + Exefile, "\"" + imgargs[0] + "\" \"" + imgargs[1].Replace("&", "") + "\"");

                                    if (System.IO.File.Exists(imgargs[1].Replace("&", "")))
                                    {
                                        System.IO.File.Delete(imgargs[1].Replace("&", ""));
                                    }
                                    pStart.CreateNoWindow = true;
                                    pStart.UseShellExecute = false;
                                    pStart.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                    System.Diagnostics.Process cmdProcess =
                                        System.Diagnostics.Process.Start(pStart);
                                    while (!cmdProcess.HasExited)
                                    {



                                    }
                                }
                                imageType = true;
                                imageFullPath = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) +
                                                "\\temp\\ImageandAttFile" + convertCustFolder +
                                                chkFile.ToString()
                                                    .Substring(chkFile.ToString().LastIndexOf('\\'),
                                                        chkFile.ToString().LastIndexOf('.') -
                                                        chkFile.ToString().LastIndexOf('\\')) + ".png";
                                imageFullPath = imageFullPath.Replace("&", "");
                            }
                            else
                            {
                                imageType = true;
                                //// imageFullPath = _imagepath + _pivotTable.Rows[i][j];
                            }

                        }
                    string NEW_FILE = string.Empty;
                    int arg2count = 0;
                    int arg7count = 0;
                    imageFile1 = string.Empty;
                    int index = imgFile1.IndexOf(":");
                    //int vPath = string.Compare(uImagepath, imgFile1.Substring(0, uImagepath.Length), true);
                    //if (vPath == 0)
                    //{
                    //    imageFile1 = imgFile1.Substring(uImagepath.Length, imgFile1.Length - (uImagepath.Length));
                    //}
                    //else
                    //{
                    //    imageFile1 = imgFile1.Substring(index + 1, imgFile1.Length - (index + 1));
                    //}
                    //if ((imageFile1.ToUpper().Contains("SECTION")) && (imageFile1.ToUpper().Contains("_IMAGES")))
                    //{
                    //    image_file2 = imageFile1.Split('_')[0];
                    //    count = (image_file2.LastIndexOf("\\"));
                    //    IMAGE_PATH = imageFile1.Substring(0, count);
                    //    int coun1 = IMAGE_PATH.Length;
                    //    NEW_FILE = imageFile1.Substring(coun1);
                    //    imageFile1 = NEW_FILE;
                    //}
                    //else
                    //{
                    //    NEW_FILE = imageFile1;
                    //}

                    int cont = NEW_FILE.Length;
                    int index1 = NEW_FILE.LastIndexOf("\\");
                    string New_File1 = NEW_FILE.Substring(0, index1 + 1);
                    string arg1 = "CatalogStudio8";
                    string fi_name = Path.GetFileName(imageFullPath);
                    string arg2 = fi_name; // "Uno20Forza.jpg";
                    string arg3 = imageFullPath; //imgargs[1].Replace("&", ""); // "C:\\CatalogStudio8\\Uno20Forza.jpg";
                    string WebCat_Image_Path = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder) +
                                               "\\temp\\ImageandAttFile" + convertCustFolder ;
                    string arg5 = "CatalogStudio8";
                    string arg6 = ImageMagickPath; // "C:\\Program Files\\ImageMagick-7.0.2-Q16";
                    string arg8 = string.Empty;
                    string arg9 = string.Empty;
                    string ag10 = string.Empty;
                    string ag11 = string.Empty;
                    FileInfo extimg = new FileInfo(arg2);
                    arg8 = arg2.Replace(extimg.Extension, ".jpg");
                    arg9 = arg2.Replace(extimg.Extension, ".jpg");
                    ag10 = arg2.Replace(extimg.Extension, ".jpg");
                    ag11 = arg2.Replace(extimg.Extension, ".jpg");


                    string strFilePath = System.Web.HttpContext.Current.Server.MapPath("~/bin") + "\\imageconvertjpg.bat";
                    //Application.StartupPath + "\\imageconvertjpg.bat";
                    System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(strFilePath);
                    psi.CreateNoWindow = true;
                    psi.UseShellExecute = false;
                    psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    psi.WorkingDirectory = (ImageMagickPath);
                    System.Diagnostics.Process proc = System.Diagnostics.Process.Start(psi);
                    proc.StartInfo.Arguments = string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} ",
                        '"' + arg1 + '"', '"' + arg2 + '"', '"' + arg3 + '"', '"' + WebCat_Image_Path + '"',
                        '"' + arg5 + '"', '"' + arg6 + '"', '"' + arg8 + '"', '"' + arg9 + '"', '"' + ag10 + '"',
                        '"' + ag11 + '"');
                    proc.Start();
                    proc.WaitForExit();
                    proc.Close();
                }
                // *********************************** image convert end **********************

                #endregion
            }
            catch (Exception e)
            {

            }
        }

        public ActionResult RemoveImageManagementFile(string[] fileNames, string customerFolder)
        {
            if (fileNames != null)
            {
                // CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }


                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    if (fileName != null)
                    {
                        var physicalPath = Path.Combine(customerFolder, fileName);
                        // var physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages/" + customerFolder), fileName);
                        if (System.IO.File.Exists(physicalPath))
                        {
                            System.IO.File.Delete(physicalPath);
                        }
                    }
                }
            }
            return Content("");
        }

        // [System.Web.Http.HttpGet]
        //public IList GetMultipleImageAttributes(int catalogId, string categoryIds, string attributeType, JArray list)
        //{
        //    try
        //    {
        //        List<string> categoryIdList = new List<string>();
        //        List<int> familyIdList = new List<int>();
        //        List<int> productIdList = new List<int>();
        //        string[] fileformats = { "*.png", "*.gif", "*.jpg" };
        //        string[] category_Ids = categoryIds.Split(',');

                

        //        if (attributeType == "2")
        //            attributeType = "Product";
        //        else if (attributeType == "1")
        //            attributeType = "Family";

        //        foreach (string item in category_Ids)
        //        {
        //            if (item.Contains("CAT"))
        //            {
        //                categoryIdList.Add(item);
        //                var cat_det = _dbcontext.Category_Function(catalogId, Convert.ToString(item));
        //                if (cat_det.Any())
        //                {
        //                    foreach (var category in cat_det)
        //                    {
        //                        string category_id = category.CATEGORY_ID;
        //                        if (!categoryIdList.Contains(category_id))
        //                            categoryIdList.Add(category_id);
        //                    }
        //                }
        //            }
        //            else if (item.Contains("~"))
        //            {
        //                int familyId = 0;
        //                int.TryParse(item.Trim('~'), out familyId);
        //                familyIdList.Add(familyId);
        //            }
        //            else if (item.Contains("*"))
        //            {
        //                int productId = 0;
        //                int.TryParse(item.Trim('~'), out productId);
        //                productIdList.Add(productId);
        //            }
        //        }

        //        if (attributeType.ToLower() == "family" && familyIdList.Count() > 0)
        //        {
        //            var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
        //                                   join familySpecs in _dbcontext.TB_FAMILY_SPECS on catalogFamily.FAMILY_ID equals familySpecs.FAMILY_ID
        //                                   join attributes in _dbcontext.TB_ATTRIBUTE on familySpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
        //                                   where familyIdList.Contains(familySpecs.FAMILY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 9
        //                                   select new
        //                                   {
        //                                       attributes.ATTRIBUTE_ID,
        //                                       attributes.ATTRIBUTE_NAME
        //                                   }).Distinct().ToList();
        //            return imageAttributes;
        //        }
        //        else if (attributeType.ToLower() == "product" && productIdList.Count() > 0)
        //        {
        //            var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
        //                                   join prodFamily in _dbcontext.TB_PROD_FAMILY on catalogFamily.FAMILY_ID equals prodFamily.FAMILY_ID
        //                                   join prodSpecs in _dbcontext.TB_PROD_SPECS on prodFamily.PRODUCT_ID equals prodSpecs.PRODUCT_ID
        //                                   join attributes in _dbcontext.TB_ATTRIBUTE on prodSpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
        //                                   where productIdList.Contains(prodSpecs.PRODUCT_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 3
        //                                   select new
        //                                   {
        //                                       attributes.ATTRIBUTE_ID,
        //                                       attributes.ATTRIBUTE_NAME
        //                                   }).Distinct().ToList();
        //            return imageAttributes;
        //        }
        //        else if (attributeType.ToLower() == "family" && categoryIdList.Count() > 0)
        //        {
        //            var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
        //                                   join familySpecs in _dbcontext.TB_FAMILY_SPECS on catalogFamily.FAMILY_ID equals familySpecs.FAMILY_ID
        //                                   join attributes in _dbcontext.TB_ATTRIBUTE on familySpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
        //                                   where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 9
        //                                   select new
        //                                   {
        //                                       attributes.ATTRIBUTE_ID,
        //                                       attributes.ATTRIBUTE_NAME
        //                                   }).Distinct().ToList();
        //            return imageAttributes;
        //        }
        //        else if (attributeType.ToLower() == "product" && categoryIdList.Count() > 0)
        //        {
        //            var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
        //                                   join prodFamily in _dbcontext.TB_PROD_FAMILY on catalogFamily.FAMILY_ID equals prodFamily.FAMILY_ID
        //                                   join prodSpecs in _dbcontext.TB_PROD_SPECS on prodFamily.PRODUCT_ID equals prodSpecs.PRODUCT_ID
        //                                   join attributes in _dbcontext.TB_ATTRIBUTE on prodSpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
        //                                   where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 3
        //                                   select new
        //                                   {
        //                                       attributes.ATTRIBUTE_ID,
        //                                       attributes.ATTRIBUTE_NAME
        //                                   }).Distinct().ToList();
        //            return imageAttributes;
        //        }
        //        else
        //        {
        //            var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
        //                                   join familySpecs in _dbcontext.TB_FAMILY_SPECS on catalogFamily.FAMILY_ID equals familySpecs.FAMILY_ID
        //                                   join attributes in _dbcontext.TB_ATTRIBUTE on familySpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
        //                                   where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 9
        //                                   select new
        //                                   {
        //                                       attributes.ATTRIBUTE_ID,
        //                                       attributes.ATTRIBUTE_NAME
        //                                   }).Distinct().ToList();
        //            return imageAttributes;
        //        }
        //    }
        //    catch (Exception objException)
        //    {
        //        Logger.Error("Error at ImageManagement : GetImageAttributes", objException);
        //        return null;
        //    }
        //}

        public string FilePath(string customerFolder)
        {
            try
            {
               string path = Server.MapPath("~/Content/ProductImages/" + customerFolder);
               return path;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

    }
}