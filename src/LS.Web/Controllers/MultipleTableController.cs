﻿using LS.Data;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Linq;
using System.Text;
using Kendo.DynamicLinq;
using LS.Data;
using LS.Data.Model.CatalogSectionModels;
using log4net;
using LS.Data.Model;
using System.Web.Http;
using System.Collections;
using System.Net.Http;
using System.Net;
using LS.Data.Utilities;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using LS.Data.Model.ProductView;
using System.Data.OleDb;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data;
using System;
using System.Globalization;
using System.Data.SqlClient;
using System.ComponentModel;



namespace LS.Web.Controllers
{
    public class MultipleTableController : Controller
    {
        //static ILog _logger = LogManager.GetLogger(typeof(HomeApiController));
        // GET: MultipleTable
        static ILog _logger = LogManager.GetLogger(typeof(HomeApiController));
        CSEntities objLS = new CSEntities();
        public ActionResult Index()
        {
            return View();
        }
        public void saveMultipleTableGroupingAttributes(int GroupName, IEnumerable<string> TreeAttributes)
        {
            try
            {
                foreach (string pid in TreeAttributes)
                {
                    string[] prodid = pid.Split(',');
                    int i = 0;
                    foreach (string prid in prodid)
                    {
                        int AttributeID = Convert.ToInt32(prid);
                        var objTB_ATTRIBUTE_GROUP_SPECS = new TB_ATTRIBUTE_GROUP_SPECS();
                        objTB_ATTRIBUTE_GROUP_SPECS.GROUP_ID = GroupName;
                        objTB_ATTRIBUTE_GROUP_SPECS.ATTRIBUTE_ID = AttributeID;
                        objTB_ATTRIBUTE_GROUP_SPECS.SORT_ORDER = i;
                        objTB_ATTRIBUTE_GROUP_SPECS.CREATED_DATE = DateTime.Now;
                        objTB_ATTRIBUTE_GROUP_SPECS.MODIFIED_DATE = DateTime.Now;
                        objTB_ATTRIBUTE_GROUP_SPECS.MODIFIED_USER = User.Identity.Name;
                        objTB_ATTRIBUTE_GROUP_SPECS.CREATED_USER = User.Identity.Name;
                        objLS.TB_ATTRIBUTE_GROUP_SPECS.Add(objTB_ATTRIBUTE_GROUP_SPECS);
                        objLS.SaveChanges();
                        i++;
                    }
                }
                //var objTB_ATTRIBUTE_GROUP_SPECS=new TB_ATTRIBUTE_GROUP_SPECS
                //{
                //    GROUP_ID=GroupName,
                //    ATTRIBUTE_ID=

                //}
                //var Attributes = objLS.TB_ATTRIBUTE.Where(a => a.ATTRIBUTE_TYPE == Attributeid).Select(s => new LS_TB_ATTRIBUTE
                //var Attributes = objLS.TB_ATTRIBUTE.Join(objLS.TB_PROD_FAMILY_ATTR_LIST,a=>a.ATTRIBUTE_ID,b=>b.ATTRIBUTE_ID,(a,b)=>new {a.ATTRIBUTE_TYPE,a.ATTRIBUTE_ID,a.ATTRIBUTE_NAME})
                //var Attributes = objLS.TB_ATTRIBUTE.Join(objLS.TB_PROD_FAMILY_ATTR_LIST, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a.ATTRIBUTE_TYPE, a.ATTRIBUTE_ID, a.ATTRIBUTE_NAME, b.FAMILY_ID })
                //    .Join(objLS.TB_CATALOG_FAMILY, a => a.FAMILY_ID, b => b.FAMILY_ID, (a, b) => new { a.ATTRIBUTE_ID, a.ATTRIBUTE_NAME, a.ATTRIBUTE_TYPE, a.FAMILY_ID, b.CATALOG_ID, b.CATEGORY_ID })
                //    .Join(objLS.TB_ATTRIBUTE_GROUP_SPECS, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a.ATTRIBUTE_ID, a.ATTRIBUTE_NAME, a.ATTRIBUTE_TYPE, a.FAMILY_ID, b.GROUP_ID, a.CATALOG_ID })
                //    .Where(x => x.ATTRIBUTE_TYPE == AttributeID && x.FAMILY_ID == 36 && x.CATALOG_ID == 3)
                //.Select(s => new LS_TB_ATTRIBUTE
                //{
                //    ATTRIBUTE_TYPE = s.ATTRIBUTE_TYPE,
                //    ATTRIBUTE_ID = s.ATTRIBUTE_ID,
                //    ATTRIBUTE_NAME = s.ATTRIBUTE_NAME
                //}).Distinct().ToList();

                //return Json(Attributes, JsonRequestBehavior.AllowGet);

            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading saveMultipleTableGroupingAttributes in MultipleTableController", objException);
            }
        }
        public ActionResult multipleTablePreviewPopUp()
        {
            return View("~/Views/App/Admin/multipleTablePreviewPopUp.cshtml");
        }

        //public IList GetCatalogDetails()
        //{
        //    return GetdropdownCatalog();
        //    // return Json(dbserver, System.Web.Mvc.JsonRequestBehavior.AllowGet);
        //}
        public JsonResult multipletableAttributes(int groupID)
        {
            try
            {
                var multipleAttributeTable = objLS.TB_ATTRIBUTE_GROUP_SPECS.Where(a => a.GROUP_ID == groupID)
                    .Join(objLS.TB_ATTRIBUTE, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a.GROUP_ID, a.ATTRIBUTE_ID, b.ATTRIBUTE_NAME, b.ATTRIBUTE_TYPE, a.SORT_ORDER })
                    .ToList().OrderBy(x => x.SORT_ORDER);
                return Json(multipleAttributeTable, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading multipletableAttributes in MultipleTableController", objException);
                return null;
            }
        }
    }
}