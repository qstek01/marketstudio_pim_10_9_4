﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Http;
using log4net;
using LS.Data;
using System.Reflection;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Diagnostics;
using System.Web.UI.WebControls;
using LS.Web.Models;
//using Ionic.Zip;
using System.IO.Compression;
using LS.Data.Model;
using System.Data.SqlClient;
using System.Configuration;
using LS.Data.Model.ProductView;
using System.Globalization;
using System.Collections;
using System.Net.Http;
using System.Net;
using LS.Data.Utilities;
using System.Text.RegularExpressions;

namespace LS.Web.Controllers
{
    public class ImagManagementApiController : ApiController
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        DataSet objDataSet = new DataSet();
        DataTable objDataTable = new DataTable();
        private static readonly ILog Logger = LogManager.GetLogger(typeof(HomeApiController));
        private readonly CSEntities _dbcontext = new CSEntities();
        public string SqlString = "";
        public string RefID = "";
        private string _sqlString = "";
        private DataSet _dsRmvPmnt = new DataSet();
        private DataSet _recycleDSPerma = new DataSet();
        public string Sqlstr = "";
        public string ImageMagickPath = string.Empty;
        private static string catIds = string.Empty;
        private static string FamilyIds = string.Empty;
        public string CustomerFolder = string.Empty;
        public string Exefile = string.Empty;
        public string converttype = string.Empty;
        AttributeMethods objAttributeMethods = new AttributeMethods();

        //------------------------------------------Seperated path values initializing------------------------------------------------------//
        public string pathCheckFlag = System.Web.Configuration.WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
        public string pathServerValue = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDriveURL"].ToString();
        public string serverPathShareVal = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
        //------------------------------------------Seperated path values initializing------------------------------------------------------//

        public string _imagepath = HttpContext.Current.Server.MapPath("~/Content/ProductImages");
        string img_Constant = HttpContext.Current.Server.MapPath("~/Content/ProductImages");
        private static readonly char[] SpecialChars = "'#&".ToCharArray();

        public void setAttachmentPath()
        {
            try
            {
                if (pathCheckFlag.ToLower() == "true")
                {
                    _imagepath = string.Format("{0}//Content//ProductImages", serverPathShareVal);
                    img_Constant = string.Format("{0}//Content//ProductImages", serverPathShareVal);
                }
            }
            catch (Exception e)
            {

                Logger.Error("Error at ImagManagementApiController : setAttachmentPath", e);
            }
        }

        [System.Web.Http.HttpGet]
        public DataTable GetImageFiles(string catalogId, int customerId, string path, string companyName)
        {
            CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.CustomerId == customerId).Select(x => x.a.Comments).FirstOrDefault();
            setAttachmentPath();
            //  DataRow dr2 = null;
            // var attributename = _dbcontext.TB_ATTRIBUTE.Find(1);
            try
            {

                companyName = DecryptStringAES(companyName);
                path = DecryptStringAES(path);
                DataTable dt = new DataTable();
                DataRow dr = dt.NewRow();
                if (path != "" && path != null)
                {
                    _imagepath = path;
                }
                else
                {
                    _imagepath = _imagepath + "\\" + CustomerFolder;
                }
                dt.Columns.AddRange(new DataColumn[] { new DataColumn("FolderName"), new DataColumn("Extention"), new DataColumn("Path"), new DataColumn("PreviewPath"), new DataColumn("CustomerFolder"), new DataColumn("ModifiedDate"), new DataColumn("Size"), new DataColumn("Customer"), new DataColumn("ExternalDriveFlag") });
                // List<String> files = new List<String>();
                foreach (string file in Directory.GetFiles(_imagepath))
                {
                    FileInfo oFileInfo = new FileInfo(file);
                    string File_Name = System.IO.Path.GetFileName(file);
                    string File_Extention = System.IO.Path.GetExtension(file);
                    string File_Path = file;
                    string replaceSting = string.Empty;
                    string img_preview = string.Empty;
                    if (pathCheckFlag.ToLower() == "false")
                    {
                        replaceSting = file.Replace(img_Constant, "");
                        img_preview = string.Format("{0}/{1}", "Content/ProductImages", replaceSting.Replace("\\", "//"));
                        img_preview = HttpUtility.UrlPathEncode(img_preview);
                        dr = dt.NewRow();
                        dr["FolderName"] = File_Name;
                        dr["Extention"] = File_Extention.ToLower();
                        dr["Path"] = File_Path;
                        dr["PreviewPath"] = img_preview;
                        dr["CustomerFolder"] = _imagepath;
                        dr["ModifiedDate"] = oFileInfo.LastWriteTime;
                        long size = oFileInfo.Length;
                        dr["Size"] = ByteSize(size);
                        dr["Customer"] = CustomerFolder;
                        dr["ExternalDriveFlag"] = pathCheckFlag;
                        dt.Rows.Add(dr);
                    }
                    else
                    {
                        var actionVal = file.Replace("\\", "//");
                        replaceSting = file.Replace(serverPathShareVal, pathServerValue);
                        img_preview = replaceSting.Replace("\\", "//");
                        img_preview = HttpUtility.UrlPathEncode(img_preview);
                        dr = dt.NewRow();
                        dr["FolderName"] = File_Name;
                        dr["Extention"] = File_Extention.ToLower();
                        dr["Path"] = actionVal;
                        dr["PreviewPath"] = img_preview;
                        dr["CustomerFolder"] = _imagepath;
                        dr["ModifiedDate"] = oFileInfo.LastWriteTime;
                        long size = oFileInfo.Length;
                        dr["Size"] = ByteSize(size);
                        dr["Customer"] = CustomerFolder;
                        dr["ExternalDriveFlag"] = pathCheckFlag;
                        dt.Rows.Add(dr);
                    }

                }
                foreach (string folder in Directory.GetDirectories(_imagepath))
                {
                    FileInfo subfolder = new FileInfo(folder);
                    string Folder_Name = subfolder.Name;
                    //new DirectoryInfo(System.IO.Path.GetDirectoryName(folder)).Name;
                    string Folder_Extention = "folder";
                    string Folder_Path = folder;
                    dr = dt.NewRow();
                    dr["FolderName"] = Folder_Name;
                    dr["Extention"] = Folder_Extention;
                    dr["Path"] = Folder_Path;
                    dr["PreviewPath"] = Folder_Path;
                    dr["CustomerFolder"] = _imagepath;
                    dr["ModifiedDate"] = subfolder.LastWriteTime;
                    long size = DirSize(new DirectoryInfo(folder));
                    dr["Size"] = ByteSize(size);
                    dr["Customer"] = CustomerFolder;
                    dr["ExternalDriveFlag"] = pathCheckFlag;
                    dt.Rows.Add(dr);
                }

                return objDataTable = dt;
            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : GetImageFiles", objexception);
                return null;

            }

        }
        public static long DirSize(DirectoryInfo dir)
        {
            return dir.GetFiles().Sum(fi => fi.Length) +
                   dir.GetDirectories().Sum(di => DirSize(di));
        }

        public static string ByteSize(long size)
        {
            string[] sizeSuffixes = {
        "B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

            Debug.Assert(sizeSuffixes.Length > 0);

            const string formatTemplate = "{0}{1:0.#} {2}";

            if (size == 0)
            {
                return string.Format(formatTemplate, null, 0, sizeSuffixes[0]);
            }

            var absSize = Math.Abs((double)size);
            var fpPower = Math.Log(absSize, 1000);
            var intPower = (int)fpPower;
            var iUnit = intPower >= sizeSuffixes.Length
                ? sizeSuffixes.Length - 1
                : intPower;
            var normSize = absSize / Math.Pow(1000, iUnit);

            return string.Format(
                formatTemplate,
                size < 0 ? "-" : null, normSize, sizeSuffixes[iUnit]);
        }

        //New Asset management size based filter Dec 2022

        public class SizeBasedResult
        {
            public string FolderName { get; set; }
            public string Extention { get; set; }
            public string Path { get; set; }
            public string PreviewPath { get; set; }
            public DateTime ModifiedDate { get; set; }
            public string Size { get; set; }
            public string ExtDriveFlag { get; set; }
        }
        public class DateBasedResult
        {
            public string FolderName { get; set; }
            public string Extention { get; set; }
            public string Path { get; set; }
            public string PreviewPath { get; set; }
            public DateTime ModifiedDate { get; set; }
            public string Size { get; set; }
            public string ExtDriveFlag { get; set; }
        }
        
            [System.Web.Http.HttpGet]
        public IList GetAssetDateRange()
        {
            try
            {
                List<String> assetDateRange = new List<String>();
                using (SqlConnection SqlCon = new SqlConnection(connectionString))
                {
                    SqlCon.Open();
                    SqlCommand Sqlcom = new SqlCommand("select DATERANGE from TB_ASSET_DATERANGE", SqlCon);
                    using (SqlDataReader reader = Sqlcom.ExecuteReader())
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                string item = reader.GetString(reader.GetOrdinal("DATERANGE"));
                                assetDateRange.Add(item);
                            }
                        }
                    }
                    Sqlcom.ExecuteNonQuery();
                    SqlCon.Close();
                }


                return assetDateRange.ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("Error at ImagManagementApiController : GetAssetDateRange ", ex);
                return null;
            }
        }


        [System.Web.Http.HttpGet]
        public IList GetAssetSizeRange()
        {
            try
            {
                List<String> assetSizeRange = new List<String>();
                using (SqlConnection SqlCon = new SqlConnection(connectionString))
                {
                    SqlCon.Open();
                    SqlCommand Sqlcom = new SqlCommand("select SIZE_RANG from TB_ASSET_SIZE_RANGE", SqlCon);
                    using (SqlDataReader reader = Sqlcom.ExecuteReader())
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                string item = reader.GetString(reader.GetOrdinal("SIZE_RANG"));
                                assetSizeRange.Add(item);
                            }
                        }
                    }
                    Sqlcom.ExecuteNonQuery();
                    SqlCon.Close();
                }

                
                return assetSizeRange.ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("Error at ImagManagementApiController : GetAssetSizeRange ", ex);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public IList GetAssetFileSize()
        {
            try
            {
                List<String> assetSizeUnit = new List<String>();
                using (SqlConnection SqlCon = new SqlConnection(connectionString))
                {
                    SqlCon.Open();
                    SqlCommand Sqlcom = new SqlCommand("select SIZE_UNIT from TB_ASSET_SIZE_UNIT", SqlCon);
                    using (SqlDataReader reader = Sqlcom.ExecuteReader())
                    {
                        if (reader != null)
                        {
                            while (reader.Read())
                            {
                                string item = reader.GetString(reader.GetOrdinal("SIZE_UNIT"));
                                assetSizeUnit.Add(item);
                            }
                        }
                    }
                    Sqlcom.ExecuteNonQuery();
                    SqlCon.Close();
                }


                return assetSizeUnit.ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("Error at ImagManagementApiController : GetAssetFileSize ", ex);
                return null;
            }
        }
        // Filter the files by give date range
        [System.Web.Http.HttpGet]
        public DataTable GetFilesByDate(string day, string path, string fromRange, string toRange)
        {
            try
            {
                DataTable FilesInDateRange = new DataTable();
                var Allfiles = DirSearch(path);
                var AllFilesIntoList = new List<DateBasedResult>();
                if (day == "Any Time")
                {
                    if (pathCheckFlag.ToLower() == "false")
                    {
                        AllFilesIntoList = Allfiles
                       .Select(y => new DateBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        AllFilesIntoList = Allfiles
                       .Select(y => new DateBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                else if (day == "Today")
                {
                    if (pathCheckFlag.ToLower() == "false")
                    {
                        AllFilesIntoList = Allfiles
                 .Where(x => new FileInfo(x).LastWriteTime.Date == DateTime.Today.Date)
                       .Select(y => new DateBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        AllFilesIntoList = Allfiles
                 .Where(x => new FileInfo(x).LastWriteTime.Date == DateTime.Today.Date)
                       .Select(y => new DateBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                else if (day == "Yesterday")
                {
                    if (pathCheckFlag.ToLower() == "false")
                    {
                        AllFilesIntoList = Allfiles
                 .Where(x => new FileInfo(x).LastWriteTime.Date >= DateTime.Today.Date.AddDays(-1) && new FileInfo(x).CreationTime.Date < DateTime.Today)
                       .Select(y => new DateBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        AllFilesIntoList = Allfiles
                 .Where(x => new FileInfo(x).LastWriteTime.Date >= DateTime.Today.Date.AddDays(-1) && new FileInfo(x).CreationTime.Date < DateTime.Today)
                       .Select(y => new DateBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }

                else if (day == "Last Week")
                {
                    if (pathCheckFlag.ToLower() == "false")
                    {
                        AllFilesIntoList = Allfiles
                 .Where(x => new FileInfo(x).LastWriteTime.Date >= DateTime.Now.AddDays(-7))
                       .Select(y => new DateBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        AllFilesIntoList = Allfiles
                 .Where(x => new FileInfo(x).LastWriteTime.Date >= DateTime.Now.AddDays(-7))
                       .Select(y => new DateBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                else if (day == "Before a week")
                {
                    if (pathCheckFlag.ToLower() == "false")
                    {
                        AllFilesIntoList = Allfiles
                 .Where(x => new FileInfo(x).LastWriteTime.Date < DateTime.Now.AddDays(-7))
                       .Select(y => new DateBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        AllFilesIntoList = Allfiles
                 .Where(x => new FileInfo(x).LastWriteTime.Date < DateTime.Now.AddDays(-7))
                       .Select(y => new DateBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                else if (day == "Custom Date")
                {
                    DateTime exple = DateTime.Parse(fromRange, CultureInfo.InvariantCulture);
                    DateTime to = DateTime.Parse(toRange, CultureInfo.InvariantCulture);
                    if (pathCheckFlag.ToLower() == "false")
                    {
                      
                     AllFilesIntoList = Allfiles
                    .Where(x => ((new FileInfo(x).LastWriteTime.Date >= DateTime.Parse(fromRange, CultureInfo.InvariantCulture)) && (new FileInfo(x).CreationTime.Date <= DateTime.Parse(toRange, CultureInfo.InvariantCulture))))
                    .Select(y => new DateBasedResult
                   {
                       FolderName = new FileInfo(y).Name,
                       Extention = new FileInfo(y).Extension.ToLower(),
                       Path = new FileInfo(y).FullName,
                       PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                       ModifiedDate = new FileInfo(y).LastWriteTime,
                       Size = ByteSize(new FileInfo(y).Length),
                       ExtDriveFlag = pathCheckFlag,
                   })
                       .ToList();
                    }

                    else if (pathCheckFlag.ToLower() == "true")
                    {                        
                        AllFilesIntoList = Allfiles
                       .Where(x => ((new FileInfo(x).LastWriteTime.Date >= DateTime.Parse(fromRange, CultureInfo.InvariantCulture)) && (new FileInfo(x).CreationTime.Date <= DateTime.Parse(toRange, CultureInfo.InvariantCulture))))
                       .Select(y => new DateBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                if (AllFilesIntoList.Count() != 0)
                {
                    ListtoDataTable lsttodt1 = new ListtoDataTable();
                    FilesInDateRange = lsttodt1.RecentListToDataTable(AllFilesIntoList, "FilesBasedOnDate");
                }

                return FilesInDateRange;
            }
            catch(Exception e)
            {
                Logger.Error("Error at ImagManagementApiController : GetFiles by date range ", e);
                return null;
            }
        }


        //Newly add for size based filter

        [System.Web.Http.HttpGet]
        public DataTable GetFilesBasedOnGivenSize(string path, string assetSizeRange, int assetSizeValue, string assetFileSize)
        {
            try
            {
                string customerPath = path;
                long assetSizeinBytes = 0;
                long assetSizeMin = 0;
                long assetSizeMax = 0;
                DataTable filesBasedOnSize = new DataTable();

                if (assetFileSize == "Size")
                {
                    assetFileSize = "";
                    assetSizeValue = 0;
                }


                var files = DirSearch(customerPath);  //getting files present in the mentioned path.

                var filesToList = new List<SizeBasedResult>();  //declaration on variables.

                if (assetSizeRange == "Greater than")
                {
                    if (assetFileSize == "KB")
                    {
                        assetSizeinBytes = assetSizeValue * 1024;
                    }
                    else if (assetFileSize == "MB")
                    {
                        assetSizeinBytes = assetSizeValue * 1048576;
                    }
                    else if (assetFileSize == "GB")
                    {
                        assetSizeinBytes = assetSizeValue * 1073741824;
                    }
                    else
                    {
                        assetSizeinBytes = 0;
                    }

                    if (pathCheckFlag.ToLower() == "false")
                    {
                        filesToList = files
                        .Where(x => new FileInfo(x).Length > assetSizeinBytes)
                        .Select(y => new SizeBasedResult
                        {
                            FolderName = new FileInfo(y).Name,
                            Extention = new FileInfo(y).Extension.ToLower(),
                            Path = new FileInfo(y).FullName,
                            PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                            ModifiedDate = new FileInfo(y).LastWriteTime,
                            Size = ByteSize(new FileInfo(y).Length),
                            ExtDriveFlag = pathCheckFlag,
                        })
                        .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeinBytes)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                else if (assetSizeRange == "Less than")
                {
                    if (assetFileSize == "KB")
                    {
                        assetSizeinBytes = assetSizeValue * 1024;
                    }
                    else if (assetFileSize == "MB")
                    {
                        assetSizeinBytes = assetSizeValue * 1048576;
                    }
                    else if (assetFileSize == "GB")
                    {
                        assetSizeinBytes = assetSizeValue * 1073741824;
                    }
                    else
                    {
                        assetSizeinBytes = 0;
                    }

                    if (pathCheckFlag.ToLower() == "false")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length < assetSizeinBytes)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length < assetSizeinBytes)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                else if (assetSizeRange == "Tiny (0KB - 16KB)")
                {
                    assetSizeMin = 0;
                    assetSizeMax = 16384;
                    if (pathCheckFlag.ToLower() == "false")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeMin)
                       .Where(x => new FileInfo(x).Length < assetSizeMax)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeMin)
                       .Where(x => new FileInfo(x).Length < assetSizeMax)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                else if (assetSizeRange == "Small (16KB - 1MB)")
                {
                    assetSizeMin = 16384;
                    assetSizeMax = 1048576;

                    if (pathCheckFlag.ToLower() == "false")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeMin)
                       .Where(x => new FileInfo(x).Length < assetSizeMax)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeMin)
                       .Where(x => new FileInfo(x).Length < assetSizeMax)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                else if (assetSizeRange == "Medium (1MB - 128MB)")
                {
                    assetSizeMin = 1048576;
                    assetSizeMax = 134217728;

                    if (pathCheckFlag.ToLower() == "false")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeMin)
                       .Where(x => new FileInfo(x).Length < assetSizeMax)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeMin)
                       .Where(x => new FileInfo(x).Length < assetSizeMax)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                else if (assetSizeRange == "Large (128MB - 1GB)")
                {
                    assetSizeMin = 134217728;
                    assetSizeMax = 1073741824;

                    if (pathCheckFlag.ToLower() == "false")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeMin)
                       .Where(x => new FileInfo(x).Length < assetSizeMax)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeMin)
                       .Where(x => new FileInfo(x).Length < assetSizeMax)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                else if (assetSizeRange == "Extra Large (1GB - 4GB)")
                {
                    assetSizeMin = 1073741824;
                    assetSizeMax = 4294967296;
                    if (pathCheckFlag.ToLower() == "false")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeMin)
                       .Where(x => new FileInfo(x).Length > assetSizeMax)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeMin)
                       .Where(x => new FileInfo(x).Length < assetSizeMax)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }
                else if (assetSizeRange == "Huge (Greater than 4GB)")
                {
                    assetSizeMin = 4294967296;

                    if (pathCheckFlag.ToLower() == "false")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length > assetSizeMin)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        filesToList = files
                       .Where(x => new FileInfo(x).Length < assetSizeMin)
                       .Select(y => new SizeBasedResult
                       {
                           FolderName = new FileInfo(y).Name,
                           Extention = new FileInfo(y).Extension.ToLower(),
                           Path = new FileInfo(y).FullName,
                           PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//")),
                           ModifiedDate = new FileInfo(y).LastWriteTime,
                           Size = ByteSize(new FileInfo(y).Length),
                           ExtDriveFlag = pathCheckFlag,
                       })
                       .ToList();
                    }
                }

                if (filesToList.Count() != 0)
                {
                    ListtoDataTable lsttodt = new ListtoDataTable();
                    filesBasedOnSize = lsttodt.RecentListToDataTable(filesToList, "filesBasedOnSize");
                }
                return filesBasedOnSize;
            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : GetFilesBasedOnGivenSize", objexception);
                return null;

            }



        }

        // Newly added for 'Add to Starred'

        [System.Web.Http.HttpPost]
        public string AddtoStarredAssets(string path, string userName, int catalogId, string fileType, int fileCount, JArray list)
        {
            try
            {
                if (userName == User.Identity.Name)
                {
                    DataTable dtable = GetStarredFiles(userName);

                    foreach (var Files in list)
                    {
                        fileType = Convert.ToString(Files["Extention"]).TrimStart('.').ToLower();
                        if (fileType != "folder")
                        {
                            string fileName = Convert.ToString(Files["FolderName"]);
                            string extention = Convert.ToString(Files["Extention"]);
                            path = Convert.ToString(Files["Path"]);
                            string size = Convert.ToString(Files["Size"]);
                            string lastModifiedDate = Convert.ToString(Files["ModifiedDate"]);
                            string previewPath = Convert.ToString(Files["PreviewPath"]);
                            string extDriveFlag = Convert.ToString(Files["ExternalDriveFlag"]);
                           // bool selectedFlag = false;

                            bool exists = dtable.Select().ToList().Exists(row => row["FILE_NAME"].ToString() == fileName);
                            if (exists)
                            {
                                string alreadyExisting = "The " + fileName + " already exist in starred";
                                //return "File already exist in starred";

                                return alreadyExisting;
                            }
                            else
                            {
                                using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                {
                                    SqlCon.Open();
                                    SqlCommand Sqlcom =
                                        new SqlCommand(
                                            "INSERT INTO TB_ASSET_STARRED\r\n ([FILE_NAME],[EXTENSION],[FILE_PATH],[USER_NAME],CATALOG_ID,[SIZE],[LAST_MODIFIED_DATE],[PREVIEW_PATH],[EXTERNAL_DRIVE_FLAG])\r\n values('" +
                                            fileName + "','" + extention + "','" + path + "','" + User.Identity.Name + "'," + catalogId + ",'" + size + "','" + lastModifiedDate + "','" + previewPath + "','" + extDriveFlag + "');",
                                            SqlCon);
                                    Sqlcom.ExecuteNonQuery();
                                    SqlCon.Close();
                                }
                            }
                        }
                        else
                        {
                            return "Starred, folders cannot be added";
                        }


                    }
                }

                if (fileCount == 1)
                {
                    return "File Added to Starred Successfully";
                }
                else
                {
                    return "Files Added to Starred Successfully";
                }

            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : AddtoStarredAssets", objexception);
                return null;

            }
        }

        //Newly Remove from star

        [System.Web.Http.HttpPost]
        public string DeleteFromStarred(string userName, string fileName)
        {
            try
            {
                if (userName == User.Identity.Name)
                {
                    DataTable dtable = GetStarredFiles(userName);
                    bool exists = dtable.Select().ToList().Exists(row => row["FILE_NAME"].ToString() == fileName);
                    if (exists)
                    {
                        using (SqlConnection SqlCon = new SqlConnection(connectionString))
                        {
                            SqlCon.Open();
                            SqlCommand Sqlcom = new SqlCommand("DELETE FROM TB_ASSET_STARRED WHERE FILE_NAME  = '" + fileName + "'", SqlCon);
                            Sqlcom.ExecuteNonQuery();
                            SqlCon.Close();
                        }
                    }
                }
                return "File removed from Starred Successfully";
            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : AddtoStarredAssets", objexception);
                return null;

            }
        }



        //Newly add for getting Starred files

        [System.Web.Http.HttpGet]
        public DataTable GetStarredFiles(string userName)
        {
            try
            {
                DataTable StarredFiles = new DataTable();
                if (userName == User.Identity.Name)
                {
                   
                    using (SqlConnection SqlCon = new SqlConnection(connectionString))
                    {
                        SqlCon.Open();
                        SqlCommand cmd = new SqlCommand("select * from TB_ASSET_STARRED", SqlCon);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(StarredFiles);
                        cmd.ExecuteNonQuery();
                        SqlCon.Close();
                    }
                }
                return StarredFiles;
            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : GetStarredFiles", objexception);
                return null;

            }
        }



        [System.Web.Http.HttpPost]
        public string SaveImageManagementDetails(string path, string folderName, int customerId, string option, string fileType)
        {
            try
            {
                string FolderName = DecryptStringAES(folderName);
                path = DecryptStringAES(path);
                if (option == "Create")
                {
                    if (FolderName != "" && FolderName != null && FolderName != "undefined")
                    {
                        path = path + "/" + FolderName;

                    }
                    else
                    {
                        path = path + "/ NewFolder";
                    }
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                        return "Folder Created Successfully";
                    }
                    else
                    {
                        return "Folder already exist";
                    }
                }
                else
                {
                    if (fileType == "folder")
                    {
                        string fullPath = Path.GetFullPath(path).TrimEnd(Path.DirectorySeparatorChar);
                        string oldFolderName = fullPath.Split(Path.DirectorySeparatorChar).Last();
                        string NewPath = fullPath.Replace("\\" + oldFolderName, "\\" + FolderName);
                        if (File.Exists(NewPath))
                        {
                            //System.IO.File.Delete("newfilename");
                        }
                        //System.IO.File.Move(path, NewPath);
                        Directory.Move(fullPath, NewPath);
                        return "Folder Renamed Successfully";
                    }
                    else
                    {
                        string oldFilePath = path; // Full path of old file
                        string extension = Path.GetExtension(path);
                        string newFileName = FolderName + extension; // Full path of new file
                        string fullPath = Path.GetFullPath(path).TrimEnd(Path.DirectorySeparatorChar);
                        string oldFolderName = fullPath.Split(Path.DirectorySeparatorChar).Last();
                        //------------------youtube files rename process starts ----------------------//
                        if (oldFolderName.StartsWith("YouTube_"))
                        {
                            string[] oldFoldersParts = oldFolderName.Split('_');
                            newFileName = oldFoldersParts[0] + "_" + oldFoldersParts[1] + "_" + newFileName;
                        }
                        //------------------youtube files rename process ends ----------------------//
                        string NewPath = path.Replace(oldFolderName, newFileName);
                        if (File.Exists(NewPath))
                        {
                            // File.Delete(newFilePath);
                        }
                        File.Move(oldFilePath, NewPath);
                        return "File Renamed Successfully";
                    }
                }
            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : SaveImageManagementDetails", objexception);
                return "File Name already Exist";

            }
        }
        [System.Web.Http.HttpPost]
        public string DeleteImageManagementDetails(string path, int customerId, string fileType, int fileCount, JArray list)
        {
            try
            {
                path = DecryptStringAES(path);
                foreach (var Files in list)
                {
                    path = Convert.ToString(Files["Path"]);
                    fileType = Convert.ToString(Files["Extention"]).TrimStart('.').ToLower();
                    if (fileType == "folder")
                    {
                        System.IO.Directory.Delete(path, true);
                    }
                    else
                    {
                        if (File.Exists(path))
                        {
                            File.Delete(path);
                        }
                    }
                }
                if(fileCount == 1)
                {
                    return "File Deleted Successfully";
                }
                else
                {
                    return "Files Deleted Successfully";
                }
                
            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : DeleteImageManagementDetails", objexception);
                return null;

            }
        }
        [System.Web.Http.HttpPost]
        public string CompressImageManagementDetails(string path, int customerId, string fileType, JArray list)
        {
            try
            {
                path = DecryptStringAES(path);
                string fullPath = Path.GetFullPath(path).TrimEnd(Path.DirectorySeparatorChar);
                string oldFolderName = fullPath.Split(Path.DirectorySeparatorChar).Last();
                string FileName = "Compress" + DateTime.Now.ToString("ddMMyyyy 'At' HH mm tt");

                String TempPath = string.Format("{0}\\{1}", fullPath, FileName);

                fileType = "folder";
                if (!Directory.Exists(TempPath))
                {
                    Directory.CreateDirectory(TempPath);
                }
                else
                {
                    System.IO.Directory.Delete(TempPath, true);
                    Directory.CreateDirectory(TempPath);
                }
                foreach (var Files in list)
                {
                    FileInfo fi = new FileInfo(Convert.ToString(Files["Path"]));
                    string destFile = System.IO.Path.Combine(TempPath, Convert.ToString(Files["FolderName"]));
                    if (Convert.ToString(Files["Extention"]).ToLower() != ("folder"))
                    {
                        fi.CopyTo(destFile);
                    }
                    else
                    {
                        string newpath = string.Format("{0}\\{1}", TempPath, Files["FolderName"]);
                        System.IO.Directory.CreateDirectory(newpath);
                        CloneDirectory(Convert.ToString(Files["Path"]), newpath);
                        //File.Move(Convert.ToString(Files["Path"]), TempPath);
                    }
                }
                if (fileType == "folder")
                {

                    //string zipPath = path + ".zip";
                    string zipPath = TempPath + ".zip";

                    //string extractPath = "C:/test/outputdir";

                    // Create zip file by compressing a folder.
                    path = fullPath;
                    //ZipFile.CreateFromDirectory(path, zipPath);
                    using (var zip = new Ionic.Zip.ZipFile())
                    {
                        zip.AddDirectory(TempPath);
                        zip.Save(zipPath);
                    }
                    System.IO.Directory.Delete(TempPath, true);

                    // ZipFile.ExtractToDirectory(zipPath, extractPath);
                    return "Files Compressed Successfully";
                }
                else
                {
                    string oldFilePath = path; // Full path of old file
                    string extension = Path.GetExtension(path);
                    string newFileName = Path.GetFileName(path);
                    newFileName = newFileName.Replace(extension, ".zip"); // Full path of new file
                    string NewPath = path.Replace(oldFolderName, newFileName);
                    using (var zip = new Ionic.Zip.ZipFile())
                    {

                        zip.Save(NewPath);
                    }
                    return "File Compressed Successfully";
                }

            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : CompressImageManagementDetails", objexception);
                return null;

            }
        }
        [System.Web.Http.HttpPost]
        public string ExportCompressImageManagementDetails(string path, int customerId, string fileType, JArray list)
        {
            try
            {
                path = DecryptStringAES(path);
                string fullPath = Path.GetFullPath(path);
                string oldFolderName = fullPath.Split(Path.DirectorySeparatorChar).Last();
                string FileName = "ManageDrive" + DateTime.Now.ToString("ddMMyyyy 'At' HH mm tt");
                String TempPath = string.Format("{0}/{1}", HttpContext.Current.Server.MapPath("~/Compress"), FileName);

                fileType = "folder";
                if (!Directory.Exists(TempPath))
                {
                    Directory.CreateDirectory(TempPath);
                }
                else
                {
                    System.IO.Directory.Delete(TempPath, true);
                    Directory.CreateDirectory(TempPath);
                }
                foreach (var Files in list)
                {
                    FileInfo fi = new FileInfo(Convert.ToString(Files["Path"]));
                    string destFile = System.IO.Path.Combine(TempPath, Convert.ToString(Files["FolderName"]));
                    if (Convert.ToString(Files["Extention"]).ToLower() != ("folder"))
                    {
                        fi.CopyTo(destFile);
                    }
                    else
                    {
                        string newpath = string.Format("{0}/{1}", TempPath, oldFolderName);
                        System.IO.Directory.CreateDirectory(newpath);
                        CloneDirectory(Convert.ToString(Files["Path"]), newpath);
                        //File.Move(Convert.ToString(Files["Path"]), TempPath);
                    }
                }
                if (fileType == "folder")
                {

                    //string zipPath = path + ".zip";
                    string zipPath = TempPath + ".zip";

                    //string extractPath = "C:/test/outputdir";

                    // Create zip file by compressing a folder.
                    path = fullPath.Replace(oldFolderName, "");
                    //ZipFile.CreateFromDirectory(path, zipPath);
                    using (var zip = new Ionic.Zip.ZipFile())
                    {
                        zip.AddDirectory(TempPath);
                        zip.Save(zipPath);
                    }
                    System.IO.Directory.Delete(TempPath, true);

                    // ZipFile.ExtractToDirectory(zipPath, extractPath);
                    return TempPath + ".zip";
                }
                else
                {
                    string oldFilePath = path; // Full path of old file
                    string extension = Path.GetExtension(path);
                    string newFileName = Path.GetFileName(path);
                    newFileName = newFileName.Replace(extension, ".zip"); // Full path of new file
                    string NewPath = path.Replace(oldFolderName, newFileName);
                    using (var zip = new Ionic.Zip.ZipFile())
                    {

                        zip.Save(NewPath);
                    }
                    return TempPath + ".zip";
                }

            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : ExportCompressImageManagementDetails", objexception);
                return null;

            }
        }
        [System.Web.Http.HttpPost]
        public string CutCopyPasteDetails(string path, int customerId, int pasteType, string fileType, JArray list)
        {
            try
            {
                //var data  =new List<ImageManagement>();
                path = DecryptStringAES(path);
                string result = "";
                string checkOldFolderName = path.Split(Path.DirectorySeparatorChar).Last();
                foreach (var Files in list)
                {
                    FileInfo fi = new FileInfo(Convert.ToString(Files["Path"]));

                    string fullPath = Path.GetFullPath(Convert.ToString(Files["Path"])).TrimEnd(Path.DirectorySeparatorChar);
                    string oldFolderName = fullPath.Split(Path.DirectorySeparatorChar).Last();
                    string newpath = path + "/" + oldFolderName;
                    newpath = newpath.Replace("/", "\\");
                    string checkNewFolderName = fullPath.Split(Path.DirectorySeparatorChar).Last();

                    if (Convert.ToString(Files["Extention"]).ToLower() != ("folder"))
                    {

                        if (pasteType == 0)
                        {
                            if (File.Exists(newpath))
                            {
                                if (oldFolderName.Trim().StartsWith("YouTube_"))
                                {
                                    string[] oldFileParts = oldFolderName.Split('_');
                                    newpath = string.Format("{0}/{1}_{2}_{3}", path, oldFileParts[0], oldFileParts[1], "Copy Of " + oldFileParts[2]);
                                }
                                else
                                    newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", oldFolderName);
                                newpath = newpath.Replace("/", "\\");
                                pathCheck3:
                                if (File.Exists(newpath))
                                {
                                    string newFolderName = newpath.Split(Path.DirectorySeparatorChar).Last();
                                    newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", newFolderName);
                                    newpath = newpath.Replace("/", "\\");
                                    goto pathCheck3;
                                }
                            }
                            fi.CopyTo(newpath);
                            result = "Files copied successfully";
                        }
                        else
                        {
                            if (File.Exists(newpath))
                            {
                                if (newpath == newpath)
                                {
                                    newpath = string.Format("{0}{1}{2}", path, "/", oldFolderName);
                                    newpath = newpath.Replace("/", "\\");
                                    result = "Files moved successfully";
                                    break;
                                }
                                //   newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", oldFolderName);
                                //newpath = newpath.Replace("/", "\\");

                                pathCheck2:
                                if (File.Exists(newpath))
                                {
                                    string newFolderName = newpath.Split(Path.DirectorySeparatorChar).Last();
                                    newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", newFolderName);
                                    newpath = newpath.Replace("/", "\\");
                                    goto pathCheck2;
                                }
                            }

                            File.Move(Convert.ToString(Files["Path"]), newpath);
                            result = "Files moved successfully";
                            //  fi.MoveTo(path);

                        }
                    }
                    else
                    {
                        if (checkNewFolderName == checkOldFolderName)
                        {
                            result = "The destination folder is a subfolder of the source folder";
                        }
                        else if (pasteType == 0)
                        {

                            if (System.IO.Directory.Exists(newpath))
                            {
                                int i = 0;
                                newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", oldFolderName);
                                newpath = newpath.Replace("/", "\\");
                                string newFolderName = newpath.Split(Path.DirectorySeparatorChar).Last();

                                pathCheck:
                                if (System.IO.Directory.Exists(newpath))
                                {

                                    newpath = string.Format("{0}{1}{2}{3}{4}{5}", path, "/", newFolderName, "(", i + 1, ")");
                                    newpath = newpath.Replace("/", "\\");
                                    i++;
                                    goto pathCheck;
                                }
                            }
                            System.IO.Directory.CreateDirectory(newpath);
                            CloneDirectory(Convert.ToString(Files["Path"]), newpath);
                            result = "Files copied successfully";
                        }
                        if (pasteType == 1)
                        {




                            if (System.IO.Directory.Exists(newpath))
                            {

                                newpath = string.Format("{0}{1}{2}", path, "/ ", oldFolderName);
                                newpath = newpath.Replace("/", "\\");
                                pathCheck1:
                                if (System.IO.Directory.Exists(newpath))
                                {
                                    string newFolderName = newpath.Split(Path.DirectorySeparatorChar).Last();
                                    newpath = string.Format("{0}{1}{2}", path, "/ ", newFolderName);
                                    newpath = newpath.Replace("/", "\\");
                                    goto pathCheck1;
                                }
                            }
                            if (!System.IO.Directory.Exists(newpath))
                            {
                                if (checkNewFolderName == checkOldFolderName)
                                {
                                    result = "The destination folder is a subfolder of the source folder";
                                }
                                else
                                {


                                    string NewfullPath = Path.GetFullPath(newpath).TrimEnd(Path.DirectorySeparatorChar);
                                    string FolderName = NewfullPath.Split(Path.DirectorySeparatorChar).Last();
                                    string Repath = path + "\\" + FolderName;
                                    string NewPath = Repath.Replace("\\" + FolderName, "\\" + oldFolderName);
                                    //  newpath = string.Format("{0}{1}{2}", path, "/", newFolderName);
                                    // newpath = newpath.Replace("/", "\\");

                                    if (File.Exists(NewPath))
                                    {
                                        //System.IO.File.Delete("newfilename");
                                    }
                                    //System.IO.File.Move(path, NewPath);
                                    System.IO.Directory.CreateDirectory(newpath);
                                    CloneDirectory(Convert.ToString(Files["Path"]), newpath);
                                    //Directory.Move(Convert.ToString(Files["Path"]), NewPath);
                                    System.IO.Directory.Delete(Convert.ToString(Files["Path"]), true);
                                    result = "Files moved successfully";
                                }

                            }

                        }
                    }
                }
                return result;
            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : CutCopyPasteDetails", objexception);
                return null;

            }
        }
        ////public string CutCopyPasteDetails(string path, int customerId, int pasteType, string fileType, JArray list)
        ////{
        ////    try
        ////    {
        ////        //var data  =new List<ImageManagement>();
        ////        path = DecryptStringAES(path);
        ////        string result = "";
        ////        foreach (var Files in list)
        ////        {
        ////            FileInfo fi = new FileInfo(Convert.ToString(Files["Path"]));

        ////            string fullPath = Path.GetFullPath(Convert.ToString(Files["Path"])).TrimEnd(Path.DirectorySeparatorChar);
        ////            string oldFolderName = fullPath.Split(Path.DirectorySeparatorChar).Last();
        ////            string newpath = path + "/" + oldFolderName;
        ////            newpath = newpath.Replace("/", "\\");

        ////            if (Convert.ToString(Files["Extention"]).ToLower() != ("folder"))
        ////            {

        ////                if (pasteType == 0)
        ////                {
        ////                    if (File.Exists(newpath))
        ////                    {
        ////                        newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", oldFolderName);
        ////                        newpath = newpath.Replace("/", "\\");
        ////                    pathCheck3:
        ////                        if (File.Exists(newpath))
        ////                        {
        ////                            string newFolderName = newpath.Split(Path.DirectorySeparatorChar).Last();
        ////                            newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", newFolderName);
        ////                            newpath = newpath.Replace("/", "\\");
        ////                            goto pathCheck3;
        ////                        }
        ////                    }
        ////                    fi.CopyTo(newpath);
        ////                    result = "Files copied successfully";
        ////                }
        ////                else
        ////                {
        ////                    if (File.Exists(newpath))
        ////                    {
        ////                        newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", oldFolderName);
        ////                        newpath = newpath.Replace("/", "\\");
        ////                    pathCheck2:
        ////                        if (File.Exists(newpath))
        ////                        {
        ////                            string newFolderName = newpath.Split(Path.DirectorySeparatorChar).Last();
        ////                            newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", newFolderName);
        ////                            newpath = newpath.Replace("/", "\\");
        ////                            goto pathCheck2;
        ////                        }
        ////                    }

        ////                        File.Move(Convert.ToString(Files["Path"]), newpath);
        ////                        result = "Files moved successfully";
        ////                        //  fi.MoveTo(path);

        ////                }
        ////            }
        ////            else
        ////            {
        ////                if (pasteType == 0)
        ////                {

        ////                    if (System.IO.Directory.Exists(newpath))
        ////                    {

        ////                        newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", oldFolderName);
        ////                        newpath = newpath.Replace("/", "\\");
        ////                    pathCheck:
        ////                        if (System.IO.Directory.Exists(newpath))
        ////                        {
        ////                            string newFolderName = newpath.Split(Path.DirectorySeparatorChar).Last();
        ////                            newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", newFolderName);
        ////                            newpath = newpath.Replace("/", "\\");
        ////                            goto pathCheck;
        ////                        }
        ////                    }
        ////                    System.IO.Directory.CreateDirectory(newpath);
        ////                    CloneDirectory(Convert.ToString(Files["Path"]), newpath);
        ////                    result = "Files copied successfully";
        ////                }
        ////                else if (pasteType == 1)
        ////                {

        ////                    if (System.IO.Directory.Exists(newpath))
        ////                    {
        ////                        newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", oldFolderName);
        ////                        newpath = newpath.Replace("/", "\\");
        ////                    pathCheck1:
        ////                        if (System.IO.Directory.Exists(newpath))
        ////                        {
        ////                            string newFolderName = newpath.Split(Path.DirectorySeparatorChar).Last();
        ////                            newpath = string.Format("{0}{1}{2}", path, "/Copy Of ", newFolderName);
        ////                            newpath = newpath.Replace("/", "\\");
        ////                            goto pathCheck1;
        ////                        }
        ////                    }
        ////                    if (!System.IO.Directory.Exists(newpath))
        ////                    {
        ////                        string NewfullPath = Path.GetFullPath(newpath).TrimEnd(Path.DirectorySeparatorChar);
        ////                        string FolderName = NewfullPath.Split(Path.DirectorySeparatorChar).Last();
        ////                        string Repath = path + "\\" + FolderName;
        ////                        string NewPath = Repath.Replace("\\" + FolderName, "\\" + oldFolderName);
        ////                        if (File.Exists(NewPath))
        ////                        {
        ////                            //System.IO.File.Delete("newfilename");
        ////                        }
        ////                        //System.IO.File.Move(path, NewPath);
        ////                        System.IO.Directory.CreateDirectory(newpath);
        ////                        CloneDirectory(Convert.ToString(Files["Path"]), newpath);
        ////                        //Directory.Move(Convert.ToString(Files["Path"]), NewPath);
        ////                        System.IO.Directory.Delete(Convert.ToString(Files["Path"]), true);

        ////                    }
        ////                    result = "Files moved successfully";
        ////                }
        ////            }
        ////        }
        ////        return result;
        ////    }
        ////    catch (Exception objexception)
        ////    {

        ////        Logger.Error("Error at ImagManagementApiController : CutCopyPasteDetails", objexception);
        ////        return null;

        ////    }
        ////}
        [System.Web.Http.HttpPost]
        public string ExtractDetails(string path, int customerId, JArray list)
        {
            try
            {
                path = DecryptStringAES(path);
                bool check = false;
                foreach (var Files in list)
                {

                    //string fullPath = Path.GetFullPath(Convert.ToString(Files["Path"])).TrimEnd(Path.DirectorySeparatorChar);
                    //System.IO.Compression.ZipFile.ExtractToDirectory(fullPath, path);

                    string fullPath = Path.GetFullPath(Convert.ToString(Files["Path"])).TrimEnd(Path.DirectorySeparatorChar);
                    using (var zip = ZipFile.OpenRead(fullPath))
                    {
                        int totalEntries = zip.Entries.Count;
                        if (totalEntries > 1)
                        {
                            foreach (ZipArchiveEntry e in zip.Entries)
                            {
                                var name = path + "\\" + e.FullName;
                                if (File.Exists(name))
                                {
                                    check = true;
                                }
                            }
                        }
                        else
                        {
                            foreach (ZipArchiveEntry e in zip.Entries)
                            {
                                var name = path + "\\" + e.FullName;
                                if (File.Exists(name))
                                {
                                    check = true;
                                }
                            }
                        }
                    }
                    if (check == false)
                    {
                        ZipFile.ExtractToDirectory(fullPath, path);
                    }
                }
                if (check == false)
                {
                    return "File/Folder extracted successfully.";
                }
                else
                {
                    return "File/Folder already present in this folder.";
                }
            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : DownloadDetails", objexception);
                return "File/Folder cannot extract.";

            }
        }
        [System.Web.Http.HttpGet]
        public void SaveImageManagementFile(IEnumerable<HttpPostedFileBase> files, string customerFolder)
        {
            //   IEnumerable<HttpPostedFileBase>files = file.File;
            if (files != null)
            {

                // CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                {
                    CustomerFolder = "/" + CustomerFolder.Replace("&", "");
                }

                foreach (var file in files)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    if (fileName != null && fileName.IndexOfAny(SpecialChars) == -1)
                    {
                        if (Directory.Exists(customerFolder))
                        {
                            var physicalPath = Path.Combine(customerFolder, fileName);

                            //var physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages/" + customerFolder), fileName);
                            file.SaveAs(physicalPath);
                        }
                        else
                        {
                            //Directory.CreateDirectory(Server.MapPath("~/Content/ProductImages/" + customerFolder));
                            //var physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages/" + customerFolder), fileName);
                            Directory.CreateDirectory(customerFolder);
                            var physicalPath = Path.Combine(customerFolder, fileName);
                            file.SaveAs(physicalPath);
                        }

                    }
                }
            }
            //return Content("");
        }
        [System.Web.Http.HttpPost]
        public DataTable getFileSearchResult(string path, string searchText, int customerId)
        {

            CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.CustomerId == customerId).Select(x => x.a.Comments).FirstOrDefault();
            setAttachmentPath();
            string img_Constant = _imagepath;
            DataTable searchResult = new DataTable();
            DataRow dr = searchResult.NewRow();

            try
            {

                searchText = DecryptStringAES(searchText);
                string[] files = Directory.GetFiles(path, "*" + searchText + "*.*", SearchOption.AllDirectories);
                string[] folders = Directory.GetDirectories(path, searchText + "*", SearchOption.AllDirectories);
                if (path != "" && path != null)
                {
                    _imagepath = path;
                }
                else
                {
                    _imagepath = _imagepath + @"\" + CustomerFolder;
                }
                searchResult.Columns.AddRange(new DataColumn[] { new DataColumn("FolderName"), new DataColumn("Extention"), new DataColumn("Path"), new DataColumn("PreviewPath"), new DataColumn("CustomerFolder"), new DataColumn("ModifiedDate"), new DataColumn("Size"), new DataColumn("Customer"), new DataColumn("ExternalDriveFlag") });
                foreach (string file in files)
                {
                    FileInfo oFileInfo = new FileInfo(file);
                    string File_Name = System.IO.Path.GetFileName(file);
                    string File_Extention = System.IO.Path.GetExtension(file);
                    if (pathCheckFlag.ToLower() == "false")
                    {
                        string File_Path = file;
                        string replaceSting = file.Replace(img_Constant, "");
                        string img_preview = string.Format("{0}/{1}", "Content/ProductImages", replaceSting.Replace("\\", "//"));
                        img_preview = HttpUtility.UrlPathEncode(img_preview);
                        dr = searchResult.NewRow();
                        dr["FolderName"] = File_Name;
                        dr["Extention"] = File_Extention.ToLower();
                        dr["Path"] = File_Path;
                        dr["PreviewPath"] = img_preview;
                        dr["CustomerFolder"] = _imagepath;
                        dr["ModifiedDate"] = oFileInfo.CreationTime;
                        long size = oFileInfo.Length;
                        dr["Size"] = ByteSize(size);
                        dr["Customer"] = CustomerFolder;
                        dr["ExternalDriveFlag"] = pathCheckFlag;
                        searchResult.Rows.Add(dr);
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        string File_Path = file;
                        string replaceSting = file.Replace(serverPathShareVal, pathServerValue);
                        string img_preview = string.Format("{0}", replaceSting.Replace("\\", "//"));
                        img_preview = HttpUtility.UrlPathEncode(img_preview);
                        dr = searchResult.NewRow();
                        dr["FolderName"] = File_Name;
                        dr["Extention"] = File_Extention.ToLower();
                        dr["Path"] = File_Path;
                        dr["PreviewPath"] = img_preview;
                        dr["CustomerFolder"] = _imagepath;
                        dr["ModifiedDate"] = oFileInfo.CreationTime;
                        long size = oFileInfo.Length;
                        dr["Size"] = ByteSize(size);
                        dr["Customer"] = CustomerFolder;
                        dr["ExternalDriveFlag"] = pathCheckFlag;
                        searchResult.Rows.Add(dr);
                    }
                }
                foreach (string folder in folders)
                {
                    FileInfo subfolder = new FileInfo(folder);
                    string Folder_Name = subfolder.Name;
                    //new DirectoryInfo(System.IO.Path.GetDirectoryName(folder)).Name;
                    string Folder_Extention = "folder";
                    string Folder_Path = folder;
                    dr = searchResult.NewRow();
                    dr["FolderName"] = Folder_Name;
                    dr["Extention"] = Folder_Extention;
                    dr["Path"] = Folder_Path;
                    dr["CustomerFolder"] = _imagepath;
                    dr["ModifiedDate"] = subfolder.CreationTime;
                    long size = DirSize(new DirectoryInfo(folder));
                    dr["Size"] = ByteSize(size);
                    dr["Customer"] = CustomerFolder;
                    dr["ExternalDriveFlag"] = pathCheckFlag;
                    searchResult.Rows.Add(dr);
                }
                return searchResult;


            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : getFileSearchResult", objexception);
                return null;

            }
        }
        [System.Web.Http.HttpGet]
        public string getLocation()
        {
            try
            {
                return _imagepath;
            }
            catch (Exception objexception)
            {

                Logger.Error("Error at ImagManagementApiController : getFileSearchResult", objexception);
                return null;

            }
        }

        private static void CloneDirectory(string root, string dest)
        {
            foreach (var directory in Directory.GetDirectories(root))
            {
                string dirName = Path.GetFileName(directory);
                if (!Directory.Exists(Path.Combine(dest, dirName)))
                {
                    Directory.CreateDirectory(Path.Combine(dest, dirName));
                }
                CloneDirectory(directory, Path.Combine(dest, dirName));
            }

            foreach (var file in Directory.GetFiles(root))
            {
                File.Copy(file, Path.Combine(dest, Path.GetFileName(file)));
            }
        }

        public string DecryptStringAES(string cipherText)
        {
            try
            {
                var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
                var iv = Encoding.UTF8.GetBytes("8080808080808080");

                var encrypted = Convert.FromBase64String(cipherText.Replace(" ", "+"));
                var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
                return string.Format(decriptedFromJavascript);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at AdvanceImportApiController : DecryptStringAES", ex);
                return null;
            }
        }
        private string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext = null;

            // Create an RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings  
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.  
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream  
                                // and place them in a string.  
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }

        [System.Web.Http.HttpPost]
        public void RecentOpenedFiles(string path)
        {
            try
            {

                string File_path = DecryptStringAES(path);
                //string File_Name = System.IO.Path.GetFileName(File_path);
                //string File_Extention = System.IO.Path.GetExtension(File_path);
                //string File_Path = File_path;
                //string replaceSting = File_path.Replace(img_Constant, "");
                //string img_preview = string.Format("{0}/{1}", "Content/ProductImages", replaceSting.Replace("\\", "//"));
                //img_preview = HttpUtility.UrlPathEncode(img_preview);

                File.SetLastWriteTime(File_path, DateTime.Now);
                //int check = _dbcontext.TB_RECENT_ASSET_FILES.Where(x => x.FILE_NAME == File_Name && x.FILE_PATH == img_preview).Distinct().Count();
                //TB_RECENT_ASSET_FILES assetFiles = new TB_RECENT_ASSET_FILES();
                //if (check == 0)
                //{
                //    assetFiles.FILE_EXTENTION = File_Extention;
                //    assetFiles.FILE_NAME = File_Name;
                //    assetFiles.FILE_PATH = img_preview;
                //    assetFiles.CREATED_DATE = DateTime.Now;
                //    assetFiles.MODIFIED_DATE = DateTime.Now;
                //    _dbcontext.TB_RECENT_ASSET_FILES.Add(assetFiles);
                //    _dbcontext.SaveChanges();
                //}
                //else
                //{
                //    TB_RECENT_ASSET_FILES alreadyVal = _dbcontext.TB_RECENT_ASSET_FILES.Where(x => x.FILE_NAME == File_Name && x.FILE_PATH == img_preview).FirstOrDefault();
                //    alreadyVal.MODIFIED_DATE = DateTime.Now;
                //    _dbcontext.SaveChanges();
                //}


            }
            catch (System.Data.Entity.Validation.DbEntityValidationException objexception)
            {

                Logger.Error("Error at ImagManagementApiController : RecentOpenedFiles", objexception);
            }
        }

        public class RecentFilesInfo
        {
            public string Path { get; set; }
            public string Extention { get; set; }
            public string FolderName { get; set; }
            public DateTime ModifiedDate { get; set; }
            public string PreviewPath { get; set; }
            public string Size { get; set; }
            public string ExtDriveFlag { get; set; }
        }


        [System.Web.Http.HttpGet]
        public DataSet RecentAssetFiles(string path)
        {
            try
            {
                string customerPath = path;
                DataTable FilesModifiedToday = new DataTable("FilesModifiedToday");
                DataTable FilesModifiedyesterday = new DataTable("FilesModifiedyesterday");
                DataTable OtherFiles = new DataTable("OtherFiles");
                DataSet RecentFiles = new DataSet();

                DateTime Fromdate_Today = DateTime.Now.AddDays(-1);
                DateTime Todate_Today = DateTime.Now;
                DateTime Fromdate_Yesterday = DateTime.Now.AddDays(-2);

                var files = DirSearch(customerPath);  //getting files present in the mentioned path

                var todayFiles = new List<RecentFilesInfo>();  //declaration on variables
                var yesterdayFiles = new List<RecentFilesInfo>();
                var previousFiles = new List<RecentFilesInfo>();
                var serverSharedPath = serverPathShareVal.Replace("//", "\\");

                if (pathCheckFlag.ToLower() == "false")
                {
                    todayFiles = files
                     .Where(x => new FileInfo(x).LastWriteTime > Fromdate_Today && new FileInfo(x).LastWriteTime < Todate_Today)
                     .OrderByDescending(x => new FileInfo(x).LastWriteTime)
                     .Select(y => new RecentFilesInfo
                     {
                         Path = new FileInfo(y).FullName,
                         Extention = new FileInfo(y).Extension.ToLower(),
                         FolderName = new FileInfo(y).Name,
                         ModifiedDate = new FileInfo(y).LastWriteTime,
                         Size = ByteSize(new FileInfo(y).Length),
                         ExtDriveFlag = pathCheckFlag,
                         PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//"))
                     })
                     .ToList();
                    yesterdayFiles = files
                     .Where(x => new FileInfo(x).LastWriteTime > Fromdate_Yesterday && new FileInfo(x).LastWriteTime < Fromdate_Today)
                     .OrderByDescending(x => new FileInfo(x).LastWriteTime)
                     .Select(y => new RecentFilesInfo
                     {
                         Path = new FileInfo(y).FullName,
                         Extention = new FileInfo(y).Extension.ToLower(),
                         FolderName = new FileInfo(y).Name,
                         ModifiedDate = new FileInfo(y).LastWriteTime,
                         Size = ByteSize(new FileInfo(y).Length),
                         ExtDriveFlag = pathCheckFlag,
                         PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//"))
                     })
                     .ToList();
                    previousFiles = files
                      .Where(x => new FileInfo(x).LastWriteTime < Fromdate_Yesterday)
                      .OrderByDescending(x => new FileInfo(x).LastWriteTime)
                      .Select(y => new RecentFilesInfo
                      {
                          Path = new FileInfo(y).FullName,
                          Extention = new FileInfo(y).Extension.ToLower(),
                          FolderName = new FileInfo(y).Name,
                          ModifiedDate = new FileInfo(y).LastWriteTime,
                          Size = ByteSize(new FileInfo(y).Length),
                          ExtDriveFlag = pathCheckFlag,
                          PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(img_Constant, "Content/ProductImages").Replace("\\", "//"))
                      })
                      .ToList();
                }
                else if (pathCheckFlag.ToLower() == "true")
                {
                    todayFiles = files
                     .Where(x => new FileInfo(x).LastWriteTime > Fromdate_Today && new FileInfo(x).LastWriteTime < Todate_Today)
                     .OrderByDescending(x => new FileInfo(x).LastWriteTime)
                     .Select(y => new RecentFilesInfo
                     {
                         Path = new FileInfo(y).FullName,
                         Extention = new FileInfo(y).Extension.ToLower(),
                         FolderName = new FileInfo(y).Name,
                         ModifiedDate = new FileInfo(y).LastWriteTime,
                         Size = ByteSize(new FileInfo(y).Length),
                         ExtDriveFlag = pathCheckFlag,
                         PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(serverSharedPath, pathServerValue).Replace("\\", "//"))
                     })
                     .ToList();
                    yesterdayFiles = files
                     .Where(x => new FileInfo(x).LastWriteTime > Fromdate_Yesterday && new FileInfo(x).LastWriteTime < Fromdate_Today)
                     .OrderByDescending(x => new FileInfo(x).LastWriteTime)
                     .Select(y => new RecentFilesInfo
                     {
                         Path = new FileInfo(y).FullName,
                         Extention = new FileInfo(y).Extension.ToLower(),
                         FolderName = new FileInfo(y).Name,
                         ModifiedDate = new FileInfo(y).LastWriteTime,
                         Size = ByteSize(new FileInfo(y).Length),
                         ExtDriveFlag = pathCheckFlag,
                         PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(serverSharedPath, pathServerValue).Replace("\\", "//"))
                     })
                     .ToList();
                    previousFiles = files
                      .Where(x => new FileInfo(x).LastWriteTime < Fromdate_Yesterday)
                      .OrderByDescending(x => new FileInfo(x).LastWriteTime)
                      .Select(y => new RecentFilesInfo
                      {
                          Path = new FileInfo(y).FullName,
                          Extention = new FileInfo(y).Extension.ToLower(),
                          FolderName = new FileInfo(y).Name,
                          ModifiedDate = new FileInfo(y).LastWriteTime,
                          Size = ByteSize(new FileInfo(y).Length),
                          ExtDriveFlag = pathCheckFlag,
                          PreviewPath = HttpUtility.UrlPathEncode(new FileInfo(y).FullName.Replace(serverSharedPath, pathServerValue).Replace("\\", "//"))
                      })
                      .ToList();
                }
                if (todayFiles.Count() != 0)
                {
                    ListtoDataTable lsttodt = new ListtoDataTable();
                    FilesModifiedToday = lsttodt.RecentListToDataTable(todayFiles, "FilesModifiedToday");
                }

                if (yesterdayFiles.Count() != 0)
                {
                    ListtoDataTable lsttodt1 = new ListtoDataTable();
                    FilesModifiedyesterday = lsttodt1.RecentListToDataTable(yesterdayFiles, "FilesModifiedyesterday");
                }

                if (previousFiles.Count() != 0)
                {
                    ListtoDataTable lsttodt2 = new ListtoDataTable();
                    OtherFiles = lsttodt2.RecentListToDataTable(previousFiles, "OtherFiles");
                }
                RecentFiles.Tables.Add(FilesModifiedToday);
                RecentFiles.Tables.Add(FilesModifiedyesterday);
                RecentFiles.Tables.Add(OtherFiles);

                return RecentFiles;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ImagManagementApiController : getFileSearchResult", objexception);
                return null;
            }

        }

        private List<String> DirSearch(string sDir)
        {
            List<String> files = new List<String>();

            foreach (string f in Directory.GetFiles(sDir))
            {
                files.Add(f);
            }
            foreach (string d in Directory.GetDirectories(sDir))
            {
                files.AddRange(DirSearch(d));
            }
            return files;
        }

        public class ListtoDataTable
        {
            public DataTable RecentListToDataTable<T>(List<T> items, string name)
            {
                DataTable dataTable = new DataTable(name);
                //Get all the properties by using reflection   
                PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (PropertyInfo prop in Props)
                {
                    //Setting column names as Property names  
                    dataTable.Columns.Add(prop.Name);
                }
                foreach (T item in items)
                {
                    var values = new object[Props.Length];
                    for (int i = 0; i < Props.Length; i++)
                    {

                        values[i] = Props[i].GetValue(item, null);
                    }
                    dataTable.Rows.Add(values);
                }

                return dataTable;
            }
        }

        //Find associate Images
        [System.Web.Http.HttpPost]
        public List<ImageManagementFindOption> FindAssociateImage(string selectedImagePath)
        {
            try
            {
                DataTable findTable = new DataTable();
                var objProjectSections = new ImageManagementFindOption();
                string strp = selectedImagePath;
                strp = strp.Substring(strp.IndexOf("/"));
                strp = strp.Replace(@"//", @"\");
                using (
                                        var objSqlConnection =
                                                new SqlConnection(
                                                    ConfigurationManager.ConnectionStrings["LSAppDBConnection"]
                                                        .ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_FIND_ASSOCIATEIAMGES";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@PATH", SqlDbType.NVarChar, 100).Value = strp;
                    objSqlConnection.Open();
                    var objDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objDataAdapter.Fill(findTable);
                }
                IList<ImageManagementFindOption> items = findTable.AsEnumerable().Select(row =>
                 new ImageManagementFindOption
                 {
                     ATTRIBUTE_NAME = row.Field<string>("ATTRIBUTE_NAME"),
                     PRODUCT_ID = row.Field<int>("PRODUCT_ID"),
                     TYPE = row.Field<string>("TYPE"),
                     STRING_VALUE = row.Field<string>("STRING_VALUE"),
                 }).ToList();
                return items.ToList();
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ImagManagementApiController : FindAssociateImage", objexception);
                return null;

            }
        }

        [System.Web.Http.HttpPost]
        public string RemoveAssociation(List<ImageManagementFindOption> arrayImages)
        {
            try
            {
                foreach (var Image in arrayImages)
                {
                    string attr_name = Image.ATTRIBUTE_NAME;
                    string type = Image.TYPE;
                    int prod_id = Image.PRODUCT_ID;

                    using (var cn = new SqlConnection(connectionString))
                    using (var cmd = new SqlCommand())
                    {
                        cn.Open();
                        cmd.Connection = cn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = "select ATTRIBUTE_ID from TB_ATTRIBUTE where ATTRIBUTE_NAME= @Title";
                        cmd.Parameters.Add("@Title", attr_name);
                        int att_id = Convert.ToInt32(cmd.ExecuteScalar());
                        cmd.ExecuteNonQuery();
                        if (type == "Product")
                        {
                            cmd.CommandText = "UPDATE TB_PROD_SPECS SET STRING_VALUE=''  Where ATTRIBUTE_ID = @attrid and PRODUCT_ID = @prodid";
                            cmd.Parameters.Add("@attrid", att_id);
                            cmd.Parameters.Add("@prodid", prod_id);
                            cmd.ExecuteNonQuery();
                        }
                        else if (type == "Family")
                        {
                            cmd.CommandText = "UPDATE TB_FAMILY_SPECS SET STRING_VALUE=''  Where ATTRIBUTE_ID = @attrid and FAMILY_ID = @prodid";
                            cmd.Parameters.Add("@attrid", att_id);
                            cmd.Parameters.Add("@prodid", prod_id);
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                return "success";
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ImagManagementApiController : RemoveAssociation", objexception);
                return null;

            }
        }


        #region Assign Option

        [System.Web.Http.HttpGet]
        public List<ProductView> CategoriesForAssignOption(int catalogId, string categoryId)
        {
            if (catalogId != 0)
            {
                return CategoriesForAssignOptionForTree(catalogId, categoryId, 1);
            }
            else
            {
                var objpProductViews = new List<ProductView>();
                return objpProductViews;
            }
        }

        public List<ProductView> CategoriesForAssignOptionForTree(int catalogId, string id, int workingCatalogId)
        {
            try
            {
                if (id == "undefined")
                {
                    if (catalogId != 1)
                    {
                        var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                            tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                            .Where(x => x.tpf.User_Name == User.Identity.Name)
                            .Select(x => x.tps);
                        var customerSettings = customersettings.FirstOrDefault();
                        bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                        var firstOrDefault = customersettings.FirstOrDefault();
                        bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                        var aa =
                            _dbcontext.TB_CATALOG_SECTIONS.Where(
                                a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && a.TB_CATEGORY.PARENT_CATEGORY == "0" && a.TB_CATEGORY.FLAG_RECYCLE == "A")
                                .Select(
                                    a =>
                                        new ProductView
                                        {
                                            id = a.CATEGORY_ID,
                                            CATEGORY_ID = a.CATEGORY_ID,
                                            CATEGORY_NAME = a.TB_CATEGORY.CATEGORY_NAME,
                                            CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                            CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                            DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                            VERSION = a.TB_CATALOG.VERSION,
                                            hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) ? _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID)
                                                    : _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                                        .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && c.FLAG_RECYCLE == "A" && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                            SORT_ORDER = a.SORT_ORDER,
                                            CategoryIdinNavigator = categoryidinnavigator,
                                            FamilyandRelatedFamily = familyandrelatedfamily,
                                            spriteCssClass = a.TB_CATEGORY.IS_CLONE == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category"
                                        }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();
                        return aa;
                    }
                    else
                    {
                        var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                          tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                          .Where(x => x.tpf.User_Name == User.Identity.Name)
                          .Select(x => x.tps);
                        int userroleid = 0;
                        var SuperAdminCheck = _dbcontext.vw_aspnet_UsersInRoles
                        .Join(_dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                        .Join(_dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                        .Where(x => x.tcptps.tpf.UserName == User.Identity.Name).ToList();
                        if (SuperAdminCheck.Any())
                        {
                            userroleid = Convert.ToInt16(SuperAdminCheck[0].tcp.Role_id);
                        }

                        var customerSettings = customersettings.FirstOrDefault();
                        bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                        var firstOrDefault = customersettings.FirstOrDefault();
                        bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;

                        var aa = _dbcontext.TB_CATALOG_SECTIONS.Where(
                             a => a.CATALOG_ID == catalogId && a.TB_CATEGORY.PARENT_CATEGORY == "0")
                             .Select(
                                 a =>
                                     new ProductView
                                     {
                                         id = a.CATEGORY_ID,
                                         CATEGORY_ID = a.CATEGORY_ID,
                                         CATEGORY_NAME = a.TB_CATEGORY.CATEGORY_NAME,
                                         CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                         CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                         DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                         VERSION = a.TB_CATALOG.VERSION,
                                         hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) ? _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID)
                                                 : _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                                     .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && c.FLAG_RECYCLE == "A" && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                         SORT_ORDER = a.SORT_ORDER,
                                         CategoryIdinNavigator = categoryidinnavigator,
                                         FamilyandRelatedFamily = familyandrelatedfamily,
                                         spriteCssClass = a.TB_CATEGORY.IS_CLONE == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category",

                                     }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();

                        if (userroleid == 1)
                        {
                            var names = _dbcontext.TB_CATEGORY
                              .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf })
                              .Join(_dbcontext.Customer_Catalog, tcptps => tcptps.tpf.CATALOG_ID, tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                              .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId, (tcptpscs, cs) => new { tcptpscs, cs })
                              .Where(x => x.cs.User_Name == User.Identity.Name && x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" && x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" && x.tcptpscs.tcptps.tpf.CATALOG_ID != 1).Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);
                            var results = aa.Where(b => names.Contains(b.id)).ToList();
                            return results;
                        }
                        else
                        {
                            var names = _dbcontext.TB_CATEGORY
                              .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf })
                              .Join(_dbcontext.Customer_Role_Catalog, tcptps => tcptps.tpf.CATALOG_ID, tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                              .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId, (tcptpscs, cs) => new { tcptpscs, cs })
                              .Where(x => x.cs.User_Name == User.Identity.Name && x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" && x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" && x.tcptpscs.tcptps.tpf.CATALOG_ID != 1 && x.tcptpscs.tcp.RoleId == userroleid && x.tcptpscs.tcp.IsActive == true).Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);

                            var results = aa.Where(b => names.Contains(b.id)).ToList();
                            return results;
                        }
                    }
                }
                else
                {
                    string subfamilycategoryid = string.Empty;
                    if (id.Contains("~"))
                    {
                        var catgoryandfamilyid = id.Split('~');
                        subfamilycategoryid = catgoryandfamilyid[0];
                        id = catgoryandfamilyid[1];
                    }
                    var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                          tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                          .Where(x => x.tpf.User_Name == User.Identity.Name)
                          .Select(x => x.tps);
                    var customerSettings = customersettings.FirstOrDefault();
                    bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                    var firstOrDefault = customersettings.FirstOrDefault();
                    bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;


                    var aa = _dbcontext.TB_CATALOG_SECTIONS.Where(
                        a => a.CATALOG_ID == catalogId && a.TB_CATEGORY.PARENT_CATEGORY == id)
                        .Select(
                            a =>
                                new ProductView
                                {
                                    id = a.CATEGORY_ID,
                                    CATEGORY_ID = a.CATEGORY_ID,
                                    CATEGORY_NAME = a.TB_CATEGORY.CATEGORY_NAME,
                                    CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                    CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                    DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                    VERSION = a.TB_CATALOG.VERSION,
                                    hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID && x.tps.FLAG_RECYCLE == "A") ? _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID)
                                            : _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                                .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                    SORT_ORDER = a.SORT_ORDER,
                                    CategoryIdinNavigator = categoryidinnavigator,
                                    FamilyandRelatedFamily = familyandrelatedfamily,
                                    spriteCssClass = a.TB_CATEGORY.IS_CLONE == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category",

                                }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);

                    var bb = Array.ConvertAll(
                                             _dbcontext.TB_CATALOG_FAMILY.Where(
                                                 a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && a.CATEGORY_ID == id && a.TB_FAMILY.FLAG_RECYCLE == "A" && a.TB_FAMILY.ROOT_FAMILY == 1)
                                                 .Select(a
                                                     =>
                                                     new
                                                     {
                                                         a.FAMILY_ID,
                                                         a.TB_FAMILY.FAMILY_NAME,
                                                         a.TB_CATALOG.CATALOG_ID,
                                                         a.TB_CATALOG.CATALOG_NAME,
                                                         a.TB_CATALOG.DESCRIPTION,
                                                         a.TB_CATALOG.VERSION,
                                                         hasChildren = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tps.FLAG_RECYCLE == "A" && x.tpf.PARENT_FAMILY_ID == a.FAMILY_ID && x.tps.CATEGORY_ID == id) ||
                                                         _dbcontext.TB_CATALOG_PRODUCT.Join(_dbcontext.TB_PRODUCT, tps => tps.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                                                         .Join(_dbcontext.TB_PROD_FAMILY, tpstpf => tpstpf.tpf.PRODUCT_ID, tp => tp.PRODUCT_ID, (tpstpf, tp) => new { tpstpf, tp })
                                                         .Any(x => x.tpstpf.tps.CATALOG_ID == catalogId && x.tpstpf.tpf.FLAG_RECYCLE == "A" && x.tp.FLAG_RECYCLE == "A" && x.tp.FAMILY_ID == a.FAMILY_ID),
                                                         ProdCnt =
                                                             a.TB_FAMILY.TB_PROD_FAMILY.Where(b => b.FAMILY_ID == a.FAMILY_ID && b.FLAG_RECYCLE == "A" && b.TB_FAMILY.FLAG_RECYCLE == "A")
                                                                 .Select(b => b.PRODUCT_ID)
                                                                 .Distinct()
                                                                 .Count(),
                                                         SubProdCnt =
                                                             a.TB_FAMILY.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, ts => ts.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (ts, tpf) => new { ts, tpf }).Where(b => b.ts.TB_FAMILY.FAMILY_ID == a.FAMILY_ID && b.tpf.CATALOG_ID == catalogId && b.tpf.FLAG_RECYCLE == "A" && b.ts.TB_FAMILY.FLAG_RECYCLE == "A")
                                                             .Select(b => b.tpf.SUBPRODUCT_ID)
                                                             .Distinct()
                                                             .Count(),
                                                         a.SORT_ORDER
                                                     }).ToArray(),
                                             a =>
                                                 new ProductView
                                                 {
                                                     id = id + "~" + a.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                                     CATEGORY_ID = "~" + a.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                                     CATEGORY_NAME = familyandrelatedfamily ? a.SubProdCnt > 0 ? a.FAMILY_NAME + "(" + a.ProdCnt.ToString(CultureInfo.InvariantCulture) + ")" : a.FAMILY_NAME + "(" + a.ProdCnt.ToString(CultureInfo.InvariantCulture) + ")"
                                                           : a.FAMILY_NAME,
                                                     CATALOG_ID = a.CATALOG_ID,
                                                     CATALOG_NAME = a.CATALOG_NAME,
                                                     DESCRIPTION = a.DESCRIPTION,
                                                     VERSION = a.VERSION,
                                                     hasChildren = a.hasChildren,
                                                     SORT_ORDER = a.SORT_ORDER,
                                                     CategoryIdinNavigator = false,
                                                     FamilyandRelatedFamily = familyandrelatedfamily,
                                                     spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.TB_FAMILY.FLAG_RECYCLE == "A" && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.FAMILY_ID && x.tps.CATEGORY_ID == id) ?
                                                     "familyClone" : "family",
                                                 }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);

                    if (aa.Any())
                    {
                        var res = aa.ToList().Union(bb.ToList());
                        return res.ToList();
                    }
                    if (bb.Any())
                    {
                        return bb.ToList();
                    }
                    else
                    {
                        int fid;
                        int.TryParse(id.Trim('~'), out fid);
                        var cc = Array.ConvertAll(
                            (_dbcontext.TB_SUBFAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID,
                                tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf })
                                .Join(_dbcontext.TB_CATALOG_FAMILY, tcptps => tcptps.tps.SUBFAMILY_ID, tcp => tcp.FAMILY_ID,
                                    (tcptps, tcp) => new { tcptps, tcp })
                                .Where(x => x.tcp.CATALOG_ID == catalogId && x.tcp.FLAG_RECYCLE == "A" && x.tcptps.tps.FAMILY_ID == fid && x.tcp.CATEGORY_ID == subfamilycategoryid)).ToArray(),
                            a => new
                            {
                                FAMILY_ID = a.tcptps.tps.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                SUBFAMILY_ID = a.tcptps.tps.SUBFAMILY_ID,
                                ProdCnt =
                                    _dbcontext.TB_PROD_FAMILY.Where(b => b.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID && b.FLAG_RECYCLE == "A")
                                        .Distinct()
                                        .Count(),
                                SubProdCnt = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, ts => ts.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (ts, tpf) => new { ts, tpf }).Where(b => b.ts.TB_FAMILY.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID && b.tpf.FLAG_RECYCLE == "A" && b.tpf.CATALOG_ID == catalogId)
                                             .Select(b => b.tpf.SUBPRODUCT_ID)
                                             .Distinct()
                                             .Count(),
                                a.tcp.TB_CATALOG.CATALOG_ID,
                                a.tcp.TB_CATALOG.CATALOG_NAME,
                                a.tcp.TB_CATALOG.DESCRIPTION,
                                a.tcp.TB_CATALOG.VERSION,
                                a.tcptps.tps.SORT_ORDER,
                                hasChildren = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tf => tf.FAMILY_ID,
                                    (tps, tf) => new { tps, tf })
                                    .Join(_dbcontext.TB_PROD_FAMILY, tcftf => tcftf.tf.FAMILY_ID, tpf => tpf.FAMILY_ID,
                                    (tcftf, tpf) => new { tcftf, tpf })
                                    .Join(_dbcontext.TB_SUBPRODUCT, tcftftpf => tcftftpf.tpf.PRODUCT_ID, ts => ts.PRODUCT_ID,
                                    (tcftftpf, ts) => new { tcftftpf, ts }).Any(x => x.tcftftpf.tcftf.tps.CATALOG_ID == catalogId && x.tcftftpf.tcftf.tps.FLAG_RECYCLE == "A"
                                    && x.tcftftpf.tcftf.tf.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID
                                    && x.tcftftpf.tcftf.tps.CATEGORY_ID == subfamilycategoryid && x.ts.CATALOG_ID == catalogId) || _dbcontext.TB_CATALOG_PRODUCT.Join(_dbcontext.TB_PRODUCT, tps => tps.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                                                         .Join(_dbcontext.TB_PROD_FAMILY, tpstpf => tpstpf.tpf.PRODUCT_ID, tp => tp.PRODUCT_ID, (tpstpf, tp) => new { tpstpf, tp })
                                                         .Any(x => x.tpstpf.tps.CATALOG_ID == catalogId && x.tpstpf.tpf.FLAG_RECYCLE == "A" && x.tp.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID),
                            })
                            .Select(a => new ProductView
                            {
                                id = subfamilycategoryid + "~" + a.SUBFAMILY_ID,
                                CATEGORY_ID = "~" + a.SUBFAMILY_ID,
                                CATEGORY_NAME = familyandrelatedfamily ? a.SubProdCnt > 0 ?
                                   _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID && b.FLAG_RECYCLE == "A").Select(b => b.FAMILY_NAME).FirstOrDefault() + "(" + a.ProdCnt + ")" : _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault() + "(" + a.ProdCnt + ")" : _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault(),
                                CATALOG_ID = a.CATALOG_ID,
                                CATALOG_NAME = a.CATALOG_NAME,
                                DESCRIPTION = a.DESCRIPTION,
                                VERSION = a.VERSION,
                                SORT_ORDER = a.SORT_ORDER,
                                CategoryIdinNavigator = false,
                                FamilyandRelatedFamily = familyandrelatedfamily,
                                hasChildren = a.hasChildren,
                                spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.SUBFAMILY_ID && x.tps.CATEGORY_ID == subfamilycategoryid && x.tps.FLAG_RECYCLE == "A") ?
                                "subfamilyClone" : "subfamily",
                            }).OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id);

                        var dd = Array.ConvertAll((_dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID,
                              tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf })
                                .Join(_dbcontext.TB_PRODUCT, tcptps => tcptps.tps.PRODUCT_ID, tcp => tcp.PRODUCT_ID,
                                  (tcptps, tcp) => new { tcptps, tcp })
                                  .Join(_dbcontext.TB_PROD_SPECS, tcptps1 => tcptps1.tcp.PRODUCT_ID, tcp => tcp.PRODUCT_ID,
                                  (tcptps1, tcp1) => new { tcptps1, tcp1 })
                              .Where(x => x.tcptps1.tcptps.tps.FAMILY_ID == fid && x.tcp1.ATTRIBUTE_ID == 1 && x.tcptps1.tcptps.tps.FLAG_RECYCLE == "A" && x.tcptps1.tcp.FLAG_RECYCLE == "A")).ToArray(),
                          a => new
                          {
                              FAMILY_ID = a.tcp1.PRODUCT_ID.ToString(CultureInfo.InvariantCulture),
                              SUBFAMILY_ID = a.tcp1.PRODUCT_ID,
                              FAMILY_NAME = a.tcp1.STRING_VALUE,
                              ProdCnt = _dbcontext.TB_PRODUCT.Where(b => b.PRODUCT_ID == a.tcptps1.tcp.PRODUCT_ID && b.FLAG_RECYCLE == "A")
                                      .Distinct()
                                      .Count(),
                              SubProdCnt = _dbcontext.TB_SUBPRODUCT.Where(b => b.PRODUCT_ID == a.tcptps1.tcp.PRODUCT_ID && b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                     .Distinct()
                                     .Count()
                          })
                          .Select(a => new ProductView
                          {
                              id = subfamilycategoryid + "~" + fid + "#" + a.SUBFAMILY_ID,
                              CATEGORY_ID = "#" + a.SUBFAMILY_ID,
                              CATEGORY_NAME = a.FAMILY_NAME + "(" + a.SubProdCnt + ")",
                              CategoryIdinNavigator = false,
                              FamilyandRelatedFamily = familyandrelatedfamily,
                              hasChildren = false,
                              spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.SUBFAMILY_ID && x.tps.CATEGORY_ID == subfamilycategoryid && x.tps.FLAG_RECYCLE == "A") ?
                              "producttreeClone" : "producttree",
                          }).OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id);
                        if (cc.Any())
                        {
                            var res = cc.ToList().Union(dd.ToList());
                            return res.ToList();
                        }
                        if (dd.Any())
                        {
                            var res = dd.ToList();
                            return res.ToList();
                        }
                        return null;
                    }
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : GetCategoriesForTree", objexception);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public Tuple<IList, IList> GetMultipleImageAttributes(int catalogId, string categoryIds, string attributeType, string matchWith, JArray model)
        {
            try
            {
                List<AssignOption> assignOptions = new List<AssignOption>();
                if (matchWith == "1")
                    matchWith = "NONE";
                else if (matchWith == "2")
                    matchWith = "ITEM#";
                else if (matchWith == "3")
                    matchWith = "Family Name";
                if (model != null)
                {
                    foreach (var Files in model)
                    {
                        if (Convert.ToString(Files["Extention"]).ToLower() == ("folder"))
                        {
                            string customerFolder = Convert.ToString(Files["Path"]);
                            DirectoryInfo dir = new DirectoryInfo(customerFolder);
                            FileInfo[] imageFiles = dir.GetFiles("*", SearchOption.AllDirectories);
                            foreach (var item in imageFiles)
                            {
                                AssignOption assignOption = new AssignOption();
                                string file = item.Directory.FullName;
                                file = file + "\\" + item.Name;
                                if (pathCheckFlag.ToLower() == "false")
                                {
                                    string replaceSting = file.Replace(img_Constant, "");
                                    string img_preview = string.Format("{0}/{1}", "Content/ProductImages", replaceSting.Replace("\\", "//"));
                                    img_preview = HttpUtility.UrlPathEncode(img_preview);
                                    assignOption.PATH = img_preview;
                                    assignOption.PREVIEW_PATH = img_preview;
                                }
                                else if (pathCheckFlag.ToLower() == "true")
                                {
                                    string filePathVal = file.Replace("\\", "//");
                                    string replaceSting = file.Replace(serverPathShareVal, pathServerValue);
                                    string img_preview = string.Format("{0}", replaceSting.Replace("\\", "//"));
                                    img_preview = HttpUtility.UrlPathEncode(img_preview);
                                    assignOption.PATH = filePathVal;
                                    assignOption.PREVIEW_PATH = img_preview;
                                }
                                assignOption.FOLDERNAME = item.Name;
                                assignOption.FILENAME = item.Name.Substring(0, item.Name.LastIndexOf('.'));
                                int dotPosition = item.Name.LastIndexOf('.') - 1;
                                int len = item.Name.Length - 1;
                                assignOption.EXTENSION = item.Name.Substring(dotPosition + 1);
                                string PreviewPath = item.Directory.FullName;
                                string Customer = Convert.ToString(Files["Customer"]);
                                int splitPos = PreviewPath.IndexOf(Customer);
                                splitPos = splitPos + Customer.Length;
                                assignOption.FULLPATH = PreviewPath.Substring(splitPos);
                                assignOption.FULLPATH = assignOption.FULLPATH + "\\" + item.Name;
                                assignOptions.Add(assignOption);
                            }
                        }
                        else
                        {
                            AssignOption assignOption = new AssignOption();
                            string fileName = Convert.ToString(Files["FolderName"]);
                            assignOption.FOLDERNAME = fileName;
                            if (pathCheckFlag.ToLower() == "false")
                            {
                                string replaceSting = Convert.ToString(Files["Path"]).Replace(img_Constant, "");
                                string img_preview = string.Format("{0}/{1}", "Content/ProductImages", replaceSting.Replace("\\", "//"));
                                img_preview = HttpUtility.UrlPathEncode(img_preview);
                                assignOption.PATH = img_preview;
                                assignOption.PREVIEW_PATH = img_preview;
                            }
                            else if (pathCheckFlag.ToLower() == "true")
                            {
                                string filePathVal = Convert.ToString(Files["Path"]).Replace("\\", "//");
                                string replaceSting = Convert.ToString(Files["Path"]).Replace(serverPathShareVal, pathServerValue);
                                string img_preview = string.Format("{0}", replaceSting.Replace("\\", "//"));
                                img_preview = HttpUtility.UrlPathEncode(img_preview);
                                assignOption.PATH = replaceSting;
                                assignOption.PREVIEW_PATH = img_preview;
                            }
                            assignOption.FILENAME = fileName.Substring(0, fileName.LastIndexOf('.')); ;
                            assignOption.EXTENSION = Convert.ToString(Files["Extention"]);
                            string PreviewPath = Convert.ToString(Files["PreviewPath"]);
                            string Customer = Convert.ToString(Files["Customer"]);
                            int splitPos = PreviewPath.IndexOf(Customer);
                            splitPos = splitPos + Customer.Length;
                            assignOption.FULLPATH = PreviewPath.Substring(splitPos);
                            assignOption.FULLPATH = assignOption.FULLPATH.Replace("//", "\\");
                            assignOptions.Add(assignOption);
                        }
                    }
                }

                List<string> categoryIdList = new List<string>();
                List<int> familyIdList = new List<int>();
                List<int> productIdList = new List<int>();
                if (categoryIds == null)
                    categoryIds = string.Empty;
                string[] category_Ids = categoryIds.Split(',');

                if (attributeType == "2")
                    attributeType = "Product";
                else if (attributeType == "1")
                    attributeType = "Family";

                foreach (string item in category_Ids)
                {
                    if (item.Contains("CAT"))
                    {
                        categoryIdList.Add(item);
                        var cat_det = _dbcontext.Category_Function(catalogId, Convert.ToString(item));
                        if (cat_det.Any())
                        {
                            foreach (var category in cat_det)
                            {
                                string category_id = category.CATEGORY_ID;
                                if (!categoryIdList.Contains(category_id))
                                    categoryIdList.Add(category_id);
                            }
                        }
                    }
                    else if (item.Contains("~"))
                    {
                        int familyId = 0;
                        int.TryParse(item.Trim('~'), out familyId);
                        if (attributeType.ToLower() == "product")
                        {
                            var products = (from prodFamily in _dbcontext.TB_PROD_FAMILY
                                            join product in _dbcontext.TB_PRODUCT on prodFamily.PRODUCT_ID equals product.PRODUCT_ID
                                            where product.FLAG_RECYCLE == "A" && prodFamily.FAMILY_ID == familyId
                                            select new
                                            {
                                                product.PRODUCT_ID
                                            }).Distinct().ToList();
                            foreach (var prod in products)
                            {
                                if (productIdList != null && !productIdList.Contains(Convert.ToInt32(prod.PRODUCT_ID)))
                                    productIdList.Add(Convert.ToInt32(prod.PRODUCT_ID));
                            }
                        }
                        else
                            familyIdList.Add(familyId);
                    }
                    else if (item.Contains("*"))
                    {
                        int productId = 0;
                        int.TryParse(item.Trim('*'), out productId);
                        if (!productIdList.Contains(productId))
                            productIdList.Add(productId);
                    }
                }

                if (attributeType.ToLower() == "family" && familyIdList.Count() > 0)
                {
                    var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join familySpecs in _dbcontext.TB_FAMILY_SPECS on catalogFamily.FAMILY_ID equals familySpecs.FAMILY_ID
                                           join attributes in _dbcontext.TB_ATTRIBUTE on familySpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                           where familyIdList.Contains(familySpecs.FAMILY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 9
                                           select new
                                           {
                                               attributes.ATTRIBUTE_ID,
                                               attributes.ATTRIBUTE_NAME
                                           }).Distinct().ToList();
                    List<AssignOption> AssignOption_Item = new List<AssignOption>();
                    int fam_ID = 0;
                    if (matchWith == "Family Name")
                    {
                        List<TB_FAMILY> specs = new List<TB_FAMILY>();
                        specs = _dbcontext.TB_FAMILY.Where(s => familyIdList.Contains(s.FAMILY_ID)).ToList();
                        foreach (var image_Attribute in specs)
                        {
                            foreach (AssignOption assign_Option in assignOptions)
                            {
                                if (assign_Option.FILENAME != null && assign_Option.FILENAME.ToLower() == image_Attribute.FAMILY_NAME.ToLower())
                                {
                                    AssignOption_Item.Add(assign_Option);
                                    fam_ID = image_Attribute.FAMILY_ID;
                                }
                            }
                        }

                        var matchWithItem = (from familySpecs in _dbcontext.TB_FAMILY_SPECS
                                             join attributes in _dbcontext.TB_ATTRIBUTE on familySpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                             where attributes.ATTRIBUTE_TYPE == 9 && familySpecs.FAMILY_ID == fam_ID
                                             select new
                                             {
                                                 attributes.ATTRIBUTE_ID,
                                                 attributes.ATTRIBUTE_NAME
                                             }).Distinct().ToList();

                        return new Tuple<IList, IList>(AssignOption_Item, matchWithItem);
                    }

                    return new Tuple<IList, IList>(assignOptions, imageAttributes);
                }
                else if (attributeType.ToLower() == "product" && productIdList.Count() > 0)
                {
                    var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join prodFamily in _dbcontext.TB_PROD_FAMILY on catalogFamily.FAMILY_ID equals prodFamily.FAMILY_ID
                                           join prodSpecs in _dbcontext.TB_PROD_SPECS on prodFamily.PRODUCT_ID equals prodSpecs.PRODUCT_ID
                                           join attributes in _dbcontext.TB_ATTRIBUTE on prodSpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                           where productIdList.Contains(prodSpecs.PRODUCT_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 3
                                           select new
                                           {
                                               attributes.ATTRIBUTE_ID,
                                               attributes.ATTRIBUTE_NAME,
                                           }).Distinct().ToList();

                    List<AssignOption> AssignOption_Item = new List<AssignOption>();
                    int item = 0;
                    if (matchWith == "ITEM#")
                    {

                        List<TB_PROD_SPECS> specs = new List<TB_PROD_SPECS>();
                        specs = _dbcontext.TB_PROD_SPECS.Where(s => s.ATTRIBUTE_ID == 1 && productIdList.Contains(s.PRODUCT_ID)).ToList();
                        foreach (var image_Attribute in specs)
                        {
                            foreach (AssignOption assign_Option in assignOptions)
                            {
                                if (image_Attribute.ATTRIBUTE_ID == 1 && assign_Option.FILENAME != null && assign_Option.FILENAME.ToLower() == image_Attribute.STRING_VALUE.ToLower())
                                {
                                    AssignOption_Item.Add(assign_Option);
                                    item = image_Attribute.PRODUCT_ID;
                                }
                            }
                        }
                        var matchWithItem = (from prodSpecs in _dbcontext.TB_PROD_SPECS
                                             join attributes in _dbcontext.TB_ATTRIBUTE on prodSpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                             where prodSpecs.PRODUCT_ID == item && attributes.ATTRIBUTE_TYPE == 3
                                             select new
                                             {
                                                 attributes.ATTRIBUTE_ID,
                                                 attributes.ATTRIBUTE_NAME,
                                             }).Distinct().ToList();

                        return new Tuple<IList, IList>(AssignOption_Item, matchWithItem);
                    }
                    return new Tuple<IList, IList>(assignOptions, imageAttributes);
                }
                else if (attributeType.ToLower() == "family" && categoryIdList.Count() > 0)
                {
                    var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join familySpecs in _dbcontext.TB_FAMILY_SPECS on catalogFamily.FAMILY_ID equals familySpecs.FAMILY_ID
                                           join attributes in _dbcontext.TB_ATTRIBUTE on familySpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                           where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 9
                                           select new
                                           {
                                               attributes.ATTRIBUTE_ID,
                                               attributes.ATTRIBUTE_NAME
                                           }).Distinct().ToList();
                    List<AssignOption> AssignOption_Item = new List<AssignOption>();
                    int fam_ID = 0;
                    if (matchWith == "Family Name")
                    {
                        var allfamilies = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join family in _dbcontext.TB_FAMILY on catalogFamily.FAMILY_ID equals family.FAMILY_ID
                                           where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && family.FLAG_RECYCLE == "A"
                                           select new
                                           {
                                               family.FAMILY_ID,
                                           }).Distinct().ToList();

                        foreach (var id in allfamilies)
                        {
                            familyIdList.Add(Convert.ToInt32(id.FAMILY_ID));
                        }

                        List<TB_FAMILY> specs = new List<TB_FAMILY>();
                        specs = _dbcontext.TB_FAMILY.Where(s => familyIdList.Contains(s.FAMILY_ID)).ToList();
                        foreach (var image_Attribute in specs)
                        {
                            foreach (AssignOption assign_Option in assignOptions)
                            {
                                if (assign_Option.FILENAME != null && assign_Option.FILENAME.ToLower() == image_Attribute.FAMILY_NAME.ToLower())
                                {
                                    AssignOption_Item.Add(assign_Option);
                                    fam_ID = image_Attribute.FAMILY_ID;
                                }
                            }
                        }

                        var matchWithItem = (from familySpecs in _dbcontext.TB_FAMILY_SPECS
                                             join attributes in _dbcontext.TB_ATTRIBUTE on familySpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                             where attributes.ATTRIBUTE_TYPE == 9 && familySpecs.FAMILY_ID == fam_ID
                                             select new
                                             {
                                                 attributes.ATTRIBUTE_ID,
                                                 attributes.ATTRIBUTE_NAME
                                             }).Distinct().ToList();

                        return new Tuple<IList, IList>(AssignOption_Item, matchWithItem);
                    }
                    return new Tuple<IList, IList>(assignOptions, imageAttributes);
                }
                else if (attributeType.ToLower() == "product" && categoryIdList.Count() > 0)
                {
                    var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join prodFamily in _dbcontext.TB_PROD_FAMILY on catalogFamily.FAMILY_ID equals prodFamily.FAMILY_ID
                                           join prodSpecs in _dbcontext.TB_PROD_SPECS on prodFamily.PRODUCT_ID equals prodSpecs.PRODUCT_ID
                                           join attributes in _dbcontext.TB_ATTRIBUTE on prodSpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                           where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 3
                                           select new
                                           {
                                               attributes.ATTRIBUTE_ID,
                                               attributes.ATTRIBUTE_NAME
                                           }).Distinct().ToList();

                    List<AssignOption> AssignOption_Item = new List<AssignOption>();
                    int item = 0;
                    if (matchWith == "ITEM#")
                    {
                        var allproducts = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join prodFamily in _dbcontext.TB_PROD_FAMILY on catalogFamily.FAMILY_ID equals prodFamily.FAMILY_ID
                                           join product in _dbcontext.TB_PRODUCT on prodFamily.PRODUCT_ID equals product.PRODUCT_ID
                                           where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && product.FLAG_RECYCLE == "A"
                                           select new
                                           {
                                               product.PRODUCT_ID,
                                           }).Distinct().ToList();
                        foreach (var id in allproducts)
                        {
                            productIdList.Add(Convert.ToInt32(id.PRODUCT_ID));
                        }

                        List<TB_PROD_SPECS> specs = new List<TB_PROD_SPECS>();
                        specs = _dbcontext.TB_PROD_SPECS.Where(s => s.ATTRIBUTE_ID == 1 && productIdList.Contains(s.PRODUCT_ID)).ToList();
                        foreach (var image_Attribute in specs)
                        {
                            foreach (AssignOption assign_Option in assignOptions)
                            {
                                if (image_Attribute.ATTRIBUTE_ID == 1 && assign_Option.FILENAME != null && assign_Option.FILENAME.ToLower() == image_Attribute.STRING_VALUE.ToLower())
                                {
                                    AssignOption_Item.Add(assign_Option);
                                    item = image_Attribute.PRODUCT_ID;
                                }
                            }
                        }
                        var matchWithItem = (from prodSpecs in _dbcontext.TB_PROD_SPECS
                                             join attributes in _dbcontext.TB_ATTRIBUTE on prodSpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                             where prodSpecs.PRODUCT_ID == item && attributes.ATTRIBUTE_TYPE == 3
                                             select new
                                             {
                                                 attributes.ATTRIBUTE_ID,
                                                 attributes.ATTRIBUTE_NAME,
                                             }).Distinct().ToList();

                        return new Tuple<IList, IList>(AssignOption_Item, matchWithItem);
                    }

                    return new Tuple<IList, IList>(assignOptions, imageAttributes);
                }
                else
                {
                    if (attributeType == "--- Select ---")
                    {
                        catalogId = 0;
                    }

                    var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join familySpecs in _dbcontext.TB_FAMILY_SPECS on catalogFamily.FAMILY_ID equals familySpecs.FAMILY_ID
                                           join attributes in _dbcontext.TB_ATTRIBUTE on familySpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                           where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 9
                                           select new
                                           {
                                               attributes.ATTRIBUTE_ID,
                                               attributes.ATTRIBUTE_NAME
                                           }).Distinct().ToList();
                    return new Tuple<IList, IList>(assignOptions, imageAttributes);
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at ImagManagementApi : GetMultipleImageAttributes", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public IList GetImageAttributes(int catalogId, string categoryIds, string attributeType, JArray model)
        {
            try
            {
                List<string> categoryIdList = new List<string>();
                List<int> familyIdList = new List<int>();
                List<int> productIdList = new List<int>();
                string[] category_Ids = categoryIds.Split(',');

                if (attributeType == "2")
                    attributeType = "Product";
                else if (attributeType == "1")
                    attributeType = "Family";

                foreach (string item in category_Ids)
                {
                    if (item.Contains("CAT"))
                    {
                        categoryIdList.Add(item);
                        var cat_det = _dbcontext.Category_Function(catalogId, Convert.ToString(item));
                        if (cat_det.Any())
                        {
                            foreach (var category in cat_det)
                            {
                                string category_id = category.CATEGORY_ID;
                                if (!categoryIdList.Contains(category_id))
                                    categoryIdList.Add(category_id);
                            }
                        }
                    }
                    else if (item.Contains("~"))
                    {
                        int familyId = 0;
                        int.TryParse(item.Trim('~'), out familyId);
                        if (attributeType.ToLower() == "product")
                        {
                            var products = (from prodFamily in _dbcontext.TB_PROD_FAMILY
                                            join product in _dbcontext.TB_PRODUCT on prodFamily.PRODUCT_ID equals product.PRODUCT_ID
                                            where product.FLAG_RECYCLE == "A" && prodFamily.FAMILY_ID == familyId
                                            select new
                                            {
                                                product.PRODUCT_ID
                                            }).Distinct().ToList();
                            foreach (var prod in products)
                            {
                                if (productIdList != null && !productIdList.Contains(Convert.ToInt32(prod.PRODUCT_ID)))
                                    productIdList.Add(Convert.ToInt32(prod.PRODUCT_ID));
                            }
                        }
                        else
                            familyIdList.Add(familyId);
                    }
                    else if (item.Contains("*"))
                    {
                        int productId = 0;
                        int.TryParse(item.Trim('*'), out productId);
                        if (!productIdList.Contains(productId))
                            productIdList.Add(productId);
                    }
                }

                if (attributeType.ToLower() == "family" && familyIdList.Count() > 0)
                {
                    var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join familySpecs in _dbcontext.TB_FAMILY_SPECS on catalogFamily.FAMILY_ID equals familySpecs.FAMILY_ID
                                           join attributes in _dbcontext.TB_ATTRIBUTE on familySpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                           where familyIdList.Contains(familySpecs.FAMILY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 9
                                           select new
                                           {
                                               attributes.ATTRIBUTE_ID,
                                               attributes.ATTRIBUTE_NAME
                                           }).Distinct().ToList();
                    return imageAttributes;
                }
                else if (attributeType.ToLower() == "product" && productIdList.Count() > 0)
                {
                    var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join prodFamily in _dbcontext.TB_PROD_FAMILY on catalogFamily.FAMILY_ID equals prodFamily.FAMILY_ID
                                           join prodSpecs in _dbcontext.TB_PROD_SPECS on prodFamily.PRODUCT_ID equals prodSpecs.PRODUCT_ID
                                           join attributes in _dbcontext.TB_ATTRIBUTE on prodSpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                           where productIdList.Contains(prodSpecs.PRODUCT_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 3
                                           select new
                                           {
                                               attributes.ATTRIBUTE_ID,
                                               attributes.ATTRIBUTE_NAME
                                           }).Distinct().ToList();
                    return imageAttributes;
                }
                else if (attributeType.ToLower() == "family" && categoryIdList.Count() > 0)
                {
                    var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join familySpecs in _dbcontext.TB_FAMILY_SPECS on catalogFamily.FAMILY_ID equals familySpecs.FAMILY_ID
                                           join attributes in _dbcontext.TB_ATTRIBUTE on familySpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                           where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 9
                                           select new
                                           {
                                               attributes.ATTRIBUTE_ID,
                                               attributes.ATTRIBUTE_NAME
                                           }).Distinct().ToList();
                    return imageAttributes;
                }
                else if (attributeType.ToLower() == "product" && categoryIdList.Count() > 0)
                {
                    var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join prodFamily in _dbcontext.TB_PROD_FAMILY on catalogFamily.FAMILY_ID equals prodFamily.FAMILY_ID
                                           join prodSpecs in _dbcontext.TB_PROD_SPECS on prodFamily.PRODUCT_ID equals prodSpecs.PRODUCT_ID
                                           join attributes in _dbcontext.TB_ATTRIBUTE on prodSpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                           where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 3
                                           select new
                                           {
                                               attributes.ATTRIBUTE_ID,
                                               attributes.ATTRIBUTE_NAME
                                           }).Distinct().ToList();
                    return imageAttributes;
                }
                else
                {
                    var imageAttributes = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join familySpecs in _dbcontext.TB_FAMILY_SPECS on catalogFamily.FAMILY_ID equals familySpecs.FAMILY_ID
                                           join attributes in _dbcontext.TB_ATTRIBUTE on familySpecs.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                           where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && attributes.ATTRIBUTE_TYPE == 9
                                           select new
                                           {
                                               attributes.ATTRIBUTE_ID,
                                               attributes.ATTRIBUTE_NAME
                                           }).Distinct().ToList();
                    return imageAttributes;
                }
            }
            catch (Exception objException)
            {
                Logger.Error("Error at ImagManagementApi : GetImageAttributes", objException);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage AssociateImages(string matchWith, int catalogId, string AttributeType, int SelectedAttributeId, string category_ids, JArray model)
        {
            try
            {
                List<string> categoryIdList = new List<string>();
                List<int> familyIdList = new List<int>();
                List<int> productIdList = new List<int>();
                string[] category_Ids = category_ids.Split(',');
                string product_Id = string.Empty;
                string family_Id = string.Empty;
                foreach (string item in category_Ids)
                {
                    if (item.Contains("CAT"))
                    {
                        categoryIdList.Add(item);
                        var cat_det = _dbcontext.Category_Function(catalogId, Convert.ToString(item));
                        if (cat_det.Any())
                        {
                            foreach (var category in cat_det)
                            {
                                string category_id = category.CATEGORY_ID;
                                if (!categoryIdList.Contains(category_id))
                                    categoryIdList.Add(category_id);
                            }
                        }
                    }
                    else if (item.Contains("~"))
                    {
                        int familyId = 0;
                        int.TryParse(item.Trim('~'), out familyId);
                        if (AttributeType.ToLower() == "product")
                        {
                            var products = (from prodFamily in _dbcontext.TB_PROD_FAMILY
                                            join product in _dbcontext.TB_PRODUCT on prodFamily.PRODUCT_ID equals product.PRODUCT_ID
                                            where product.FLAG_RECYCLE == "A" && prodFamily.FAMILY_ID == familyId
                                            select new
                                            {
                                                product.PRODUCT_ID
                                            }).Distinct().ToList();
                            foreach (var prod in products)
                            {
                                productIdList.Add(Convert.ToInt32(prod.PRODUCT_ID));
                            }
                        }
                        else
                        {
                            family_Id = family_Id + ',' + familyId;
                            familyIdList.Add(familyId);
                        }
                    }
                    else if (item.Contains("*"))
                    {
                        int productId = 0;
                        int.TryParse(item.Trim('*'), out productId);
                        productIdList.Add(productId);
                    }
                }

                if (matchWith == "1")
                    matchWith = "NONE";
                else if (matchWith == "2")
                    matchWith = "ITEM#";
                else if (matchWith == "3")
                    matchWith = "Family Name";
                if (AttributeType == "2")
                    AttributeType = "Product";
                else if (AttributeType == "1")
                    AttributeType = "Family";
                if (category_ids != null)
                {
                    foreach (var Files in model)
                    {
                        string file_name = string.Empty;
                        //= Convert.ToString(Files["FolderName"]);
                        string extension = Convert.ToString(Files["Extention"]);
                        extension = extension.Substring(1);
                        // file_name = "\\" + file_name;

                        string PreviewPath = Convert.ToString(Files["PreviewPath"]);
                        string Customer = Convert.ToString(Files["Customer"]);
                        int splitPos = PreviewPath.IndexOf(Customer);
                        splitPos = splitPos + Customer.Length;
                        file_name = PreviewPath.Substring(splitPos);
                        file_name = file_name.Replace("//", "\\");

                        if (categoryIdList != null && categoryIdList.Count > 0)
                        {
                            foreach (string category_Id in categoryIdList)
                            {
                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ASSISNOPTION";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add(new SqlParameter("@STRING_VALUE", file_name));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@CATEGORY_ID", category_Id));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_TYPE", AttributeType));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@MATCH_WITH", matchWith));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_ID", SelectedAttributeId));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@EXTENSION", extension));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@CATALOG_ID", catalogId));
                                    objSqlConnection.Open();
                                    objSqlCommand.ExecuteNonQuery();
                                    objSqlConnection.Close();
                                }
                            }
                        }

                        // For Family
                        if (familyIdList != null && familyIdList.Count > 0)
                        {
                            foreach (int famId in familyIdList)
                            {
                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ASSISNOPTION_FAMILY";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add(new SqlParameter("@STRING_VALUE", file_name));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@FAMILY_ID", famId));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@PRODUCT_ID", ""));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_TYPE", AttributeType));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@MATCH_WITH", matchWith));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_ID", SelectedAttributeId));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@EXTENSION", extension));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@CATALOG_ID", catalogId));
                                    objSqlConnection.Open();
                                    objSqlCommand.ExecuteNonQuery();
                                    objSqlConnection.Close();
                                }
                            }
                        }
                        // For Product
                        if (productIdList != null && productIdList.Count > 0)
                        {
                            foreach (int productId in productIdList)
                            {
                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ASSISNOPTION_FAMILY";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add(new SqlParameter("@STRING_VALUE", file_name));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@FAMILY_ID", ""));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@PRODUCT_ID", productId));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_TYPE", AttributeType));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@MATCH_WITH", matchWith));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_ID", SelectedAttributeId));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@EXTENSION", extension));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@CATALOG_ID", catalogId));
                                    objSqlConnection.Open();
                                    objSqlCommand.ExecuteNonQuery();
                                    objSqlConnection.Close();
                                }
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, "Updated Successfully");
            }
            catch (Exception objException)
            {
                Logger.Error("Error at ImagManagementApi : GetImageAttributes", objException);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveImageAttribute(string attributeType, string categoryIds, int catalogId, NewAttribute model)
        {
            try
            {
                List<string> categoryIdList = new List<string>();
                List<int> familyIdList = new List<int>();
                List<int> productIdList = new List<int>();
                string[] category_Ids = categoryIds.Split(',');

                if (attributeType == "2")
                {
                    attributeType = "Product";
                    model.ATTRIBUTE_TYPE = 3;
                }
                else if (attributeType == "1")
                {
                    attributeType = "Family";
                    model.ATTRIBUTE_TYPE = 9;
                }

                foreach (string item in category_Ids)
                {
                    if (item.Contains("CAT"))
                    {
                        categoryIdList.Add(item);
                        var cat_det = _dbcontext.Category_Function(catalogId, Convert.ToString(item));
                        if (cat_det.Any())
                        {
                            foreach (var category in cat_det)
                            {
                                string category_id = category.CATEGORY_ID;
                                if (!categoryIdList.Contains(category_id))
                                    categoryIdList.Add(category_id);
                            }
                        }
                    }
                    else if (item.Contains("~"))
                    {
                        int familyId = 0;
                        int.TryParse(item.Trim('~'), out familyId);
                        if (attributeType.ToLower() == "product")
                        {
                            var products = (from prodFamily in _dbcontext.TB_PROD_FAMILY
                                            join product in _dbcontext.TB_PRODUCT on prodFamily.PRODUCT_ID equals product.PRODUCT_ID
                                            where product.FLAG_RECYCLE == "A" && prodFamily.FAMILY_ID == familyId
                                            select new
                                            {
                                                product.PRODUCT_ID
                                            }).Distinct().ToList();
                            foreach (var prod in products)
                            {
                                if (!productIdList.Contains(Convert.ToInt32(prod.PRODUCT_ID)))
                                    productIdList.Add(Convert.ToInt32(prod.PRODUCT_ID));
                            }
                        }
                        else
                            familyIdList.Add(familyId);
                    }
                    else if (item.Contains("*"))
                    {
                        int productId = 0;
                        int.TryParse(item.Trim('*'), out productId);
                        if (!productIdList.Contains(productId))
                            productIdList.Add(productId);
                    }
                }
                model.ATTRIBUTE_DATARULE = "";
                model.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\r\n\x20-\x7E\u0000-\uFFFF]*$";
                model.ATTRIBUTE_DATATYPE = "Text";
                var customerid = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name);
                if (customerid.Any())
                {
                    var customerIds = customerid.FirstOrDefault();
                    if (customerIds != null)
                    {
                        var attributecount = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                                    tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                    .Count(x => x.tcp.ATTRIBUTE_NAME == model.ATTRIBUTE_NAME && x.tpf.CUSTOMER_ID == customerIds.CustomerId);
                        if (attributecount == 0)
                        {
                            var objattribute = new TB_ATTRIBUTE
                            {
                                ATTRIBUTE_NAME = model.ATTRIBUTE_NAME,
                                ATTRIBUTE_TYPE = model.ATTRIBUTE_TYPE,
                                CREATE_BY_DEFAULT = false,
                                VALUE_REQUIRED = false,
                                STYLE_NAME = "",
                                STYLE_FORMAT = null,
                                DEFAULT_VALUE = null,
                                PUBLISH2PRINT = model.PUBLISH2PRINT,
                                PUBLISH2WEB = model.PUBLISH2WEB,
                                PUBLISH2CDROM = false,
                                PUBLISH2ODP = false,
                                USE_PICKLIST = false,
                                ATTRIBUTE_DATATYPE = model.ATTRIBUTE_DATATYPE,
                                ATTRIBUTE_DATAFORMAT = model.ATTRIBUTE_DATAFORMAT,
                                ATTRIBUTE_DATARULE = "",
                                UOM = null,
                                IS_CALCULATED = false,
                                ATTRIBUTE_CALC_FORMULA = null,
                                PICKLIST_NAME = null,
                                MODIFIED_USER = User.Identity.Name,
                                CREATED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                CREATED_DATE = DateTime.Now,
                                FLAG_RECYCLE = "A",
                                PUBLISH2EXPORT = model.PUBLISH2EXPORT,
                                PUBLISH2PDF = model.PUBLISH2PDF,
                                PUBLISH2PORTAL = model.PUBLISH2PORTAL
                            };
                            _dbcontext.TB_ATTRIBUTE.Add(objattribute);
                            _dbcontext.SaveChanges();
                            int newAttributeId = objattribute.ATTRIBUTE_ID;
                            var firstOrDefault = _dbcontext.TB_ATTRIBUTE.Max(x => x.ATTRIBUTE_ID);
                            var lastcreated = _dbcontext.TB_ATTRIBUTE.FirstOrDefault(x => x.ATTRIBUTE_ID == firstOrDefault);
                            if (lastcreated != null)
                            {
                                var attributeId = lastcreated.ATTRIBUTE_ID;
                                if (catalogId != 1)
                                {
                                    var catalogattributecount =
                                        _dbcontext.TB_CATALOG_ATTRIBUTES.Count(
                                            x => x.ATTRIBUTE_ID == attributeId && x.CATALOG_ID == catalogId);
                                    if (catalogattributecount == 0)
                                    {
                                        var objcatalogattribute = new TB_CATALOG_ATTRIBUTES
                                        {
                                            CATALOG_ID = catalogId,
                                            ATTRIBUTE_ID = attributeId,
                                            MODIFIED_USER = User.Identity.Name,
                                            CREATED_USER = User.Identity.Name,
                                            MODIFIED_DATE = DateTime.Now,
                                            CREATED_DATE = DateTime.Now
                                        };
                                        _dbcontext.TB_CATALOG_ATTRIBUTES.Add(objcatalogattribute);
                                        _dbcontext.SaveChanges();
                                    }
                                }
                                var customAttr =
                                    _dbcontext.CUSTOMERATTRIBUTE.Where(
                                        x => x.ATTRIBUTE_ID == attributeId && x.CUSTOMER_ID == customerIds.CustomerId).Select(z => z.ATTRIBUTE_ID)
                                        .ToList();

                                if (customAttr.Count == 0)
                                {
                                    var objcustomerattribute = new CUSTOMERATTRIBUTE
                                    {
                                        CUSTOMER_ID = customerIds.CustomerId,
                                        ATTRIBUTE_ID = attributeId,
                                        MODIFIED_USER = User.Identity.Name,
                                        CREATED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now,
                                        CREATED_DATE = DateTime.Now
                                    };
                                    _dbcontext.CUSTOMERATTRIBUTE.Add(objcustomerattribute);
                                    _dbcontext.SaveChanges();
                                }
                            }

                            // Insert SPECS tables 
                            if (attributeType.ToLower() == "product")
                            {
                                var allproducts = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                                   join prodFamily in _dbcontext.TB_PROD_FAMILY on catalogFamily.FAMILY_ID equals prodFamily.FAMILY_ID
                                                   join product in _dbcontext.TB_PRODUCT on prodFamily.PRODUCT_ID equals product.PRODUCT_ID
                                                   where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && product.FLAG_RECYCLE == "A"
                                                   select new
                                                   {
                                                       product.PRODUCT_ID,
                                                   }).Distinct().ToList();
                                List<Object> all_Products = (from x in allproducts select (Object)x.PRODUCT_ID).ToList();
                                all_Products.AddRange((from x in productIdList select (Object)x).ToList());
                                foreach (var prod_Id in all_Products)
                                {
                                    int Id = Convert.ToInt32(prod_Id);
                                    var prodSpecs = _dbcontext.TB_PROD_SPECS.Where(s => s.PRODUCT_ID == Id && s.ATTRIBUTE_ID == newAttributeId).FirstOrDefault();
                                    if (prodSpecs == null)
                                    {
                                        TB_PROD_SPECS prod_Specs = new TB_PROD_SPECS();
                                        prod_Specs.ATTRIBUTE_ID = newAttributeId;
                                        prod_Specs.PRODUCT_ID = Id;
                                        prod_Specs.MODIFIED_USER = User.Identity.Name;
                                        prod_Specs.CREATED_USER = User.Identity.Name;
                                        prod_Specs.MODIFIED_DATE = DateTime.Now;
                                        prod_Specs.CREATED_DATE = DateTime.Now;
                                        _dbcontext.TB_PROD_SPECS.Add(prod_Specs);
                                        _dbcontext.SaveChanges();
                                    }
                                }
                            }
                            else if (attributeType.ToLower() == "family")
                            {
                                var allfamilies = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                                   join family in _dbcontext.TB_FAMILY on catalogFamily.FAMILY_ID equals family.FAMILY_ID
                                                   where categoryIdList.Contains(catalogFamily.CATEGORY_ID) && catalogFamily.CATALOG_ID == catalogId && family.FLAG_RECYCLE == "A"
                                                   select new
                                                   {
                                                       family.FAMILY_ID,
                                                   }).Distinct().ToList();
                                List<Object> all_Families = (from x in allfamilies select (Object)x.FAMILY_ID).ToList();
                                all_Families.AddRange((from x in productIdList select (Object)x).ToList());
                                foreach (var fam_Id in all_Families)
                                {
                                    int Id = Convert.ToInt32(fam_Id);
                                    var famSpecs = _dbcontext.TB_FAMILY_SPECS.Where(s => s.FAMILY_ID == Id && s.ATTRIBUTE_ID == newAttributeId).FirstOrDefault();
                                    if (famSpecs == null)
                                    {
                                        TB_FAMILY_SPECS fam_Specs = new TB_FAMILY_SPECS();
                                        fam_Specs.ATTRIBUTE_ID = newAttributeId;
                                        fam_Specs.FAMILY_ID = Convert.ToInt32(fam_Id);
                                        fam_Specs.MODIFIED_USER = User.Identity.Name;
                                        fam_Specs.CREATED_USER = User.Identity.Name;
                                        fam_Specs.MODIFIED_DATE = DateTime.Now;
                                        fam_Specs.CREATED_DATE = DateTime.Now;
                                        _dbcontext.TB_FAMILY_SPECS.Add(fam_Specs);
                                        _dbcontext.SaveChanges();
                                    }
                                }
                            }

                            return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);


                        }

                        return Request.CreateResponse(HttpStatusCode.OK, "Attribute Name already exists, please enter a different Name and create the Attribute");

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, "Attribute Name already exists, please enter a different Name and create the Attribute");
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : SaveNewAttribute", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage AssociateMultipleImages(string matchWith, int catalogId, string AttributeType, string category_ids, JArray model)
        {
            try
            {
                List<string> categoryIdList = new List<string>();
                List<int> familyIdList = new List<int>();
                List<int> productIdList = new List<int>();
                string[] category_Ids = category_ids.Split(',');
                string product_Id = string.Empty;
                string family_Id = string.Empty;
                foreach (string item in category_Ids)
                {
                    if (item.Contains("CAT"))
                    {
                        categoryIdList.Add(item);
                        var cat_det = _dbcontext.Category_Function(catalogId, Convert.ToString(item));
                        if (cat_det.Any())
                        {
                            foreach (var category in cat_det)
                            {
                                string category_id = category.CATEGORY_ID;
                                if (!categoryIdList.Contains(category_id))
                                    categoryIdList.Add(category_id);
                            }
                        }
                    }
                    else if (item.Contains("~"))
                    {
                        int familyId = 0;
                        int.TryParse(item.Trim('~'), out familyId);
                        if (AttributeType.ToLower() == "product")
                        {
                            var products = (from prodFamily in _dbcontext.TB_PROD_FAMILY
                                            join product in _dbcontext.TB_PRODUCT on prodFamily.PRODUCT_ID equals product.PRODUCT_ID
                                            where product.FLAG_RECYCLE == "A" && prodFamily.FAMILY_ID == familyId
                                            select new
                                            {
                                                product.PRODUCT_ID
                                            }).Distinct().ToList();
                            foreach (var prod in products)
                            {
                                productIdList.Add(Convert.ToInt32(prod.PRODUCT_ID));
                            }
                        }
                        else
                        {
                            family_Id = family_Id + ',' + familyId;
                            familyIdList.Add(familyId);
                        }
                    }
                    else if (item.Contains("*"))
                    {
                        int productId = 0;
                        int.TryParse(item.Trim('*'), out productId);
                        productIdList.Add(productId);
                    }
                }

                if (matchWith == "1")
                    matchWith = "NONE";
                else if (matchWith == "2")
                    matchWith = "ITEM#";
                else if (matchWith == "3")
                    matchWith = "Family Name";
                if (AttributeType == "2")
                    AttributeType = "Product";
                else if (AttributeType == "1")
                    AttributeType = "Family";
                if (category_ids != null)
                {
                    foreach (var files in model)
                    {
                        string[] list = files.ToString().Split('*');
                        string attr_Id = list[0];
                        string file_name = list[1];
                        int dotPosition = file_name.LastIndexOf('.');
                        int len = file_name.Length - 1;
                        string extension = file_name.Substring(dotPosition + 1);
                        if (categoryIdList != null && categoryIdList.Count > 0)
                        {
                            foreach (string category_Id in categoryIdList)
                            {
                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ASSISNOPTION";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add(new SqlParameter("@STRING_VALUE", file_name));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@CATEGORY_ID", category_Id));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_TYPE", AttributeType));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@MATCH_WITH", matchWith));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_ID", attr_Id));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@EXTENSION", extension));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@CATALOG_ID", catalogId));
                                    objSqlConnection.Open();
                                    objSqlCommand.ExecuteNonQuery();
                                    objSqlConnection.Close();
                                }
                            }
                        }

                        // For Family
                        if (familyIdList != null && familyIdList.Count > 0)
                        {
                            foreach (int famId in familyIdList)
                            {
                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ASSISNOPTION_FAMILY";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add(new SqlParameter("@STRING_VALUE", file_name));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@FAMILY_ID", famId));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@PRODUCT_ID", ""));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_TYPE", AttributeType));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@MATCH_WITH", matchWith));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_ID", attr_Id));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@EXTENSION", extension));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@CATALOG_ID", catalogId));
                                    objSqlConnection.Open();
                                    objSqlCommand.ExecuteNonQuery();
                                    objSqlConnection.Close();
                                }
                            }
                        }
                        // For Product
                        if (productIdList != null && productIdList.Count > 0)
                        {
                            foreach (int productId in productIdList)
                            {
                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ASSISNOPTION_FAMILY";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add(new SqlParameter("@STRING_VALUE", file_name));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@FAMILY_ID", ""));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@PRODUCT_ID", productId));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_TYPE", AttributeType));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@MATCH_WITH", matchWith));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@ATTRIBUTE_ID", attr_Id));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@EXTENSION", extension));
                                    objSqlCommand.Parameters.Add(new SqlParameter("@CATALOG_ID", catalogId));
                                    objSqlConnection.Open();
                                    objSqlCommand.ExecuteNonQuery();
                                    objSqlConnection.Close();
                                }
                            }
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, "Updated Successfully");
            }
            catch (Exception objException)
            {
                Logger.Error("Error at ImagManagementApi : GetImageAttributes", objException);
                return null;
            }
        }

        #endregion

        #region YouTubelink

        [System.Web.Http.HttpPost]
        public string SaveYoutubeLink(string linkVal, string captionVal, string filePath)
        {
            try
            {
                string GuId = Guid.NewGuid().ToString();
                string[] fileDetail = linkVal.Split('/');
                string sessionId = Regex.Replace(GuId.ToString(), "[^a-zA-Z0-9_]+", "");
                string FileName = "YouTube~" + fileDetail[fileDetail.Length - 1] + "~" + captionVal + ".rtf";
                string saveFilePath = filePath + @"\" + FileName;
                System.IO.File.WriteAllText(saveFilePath, linkVal);
                return "success";
            }
            catch (Exception e)
            {
                Logger.Error("Error at ImagManagementApi : GetImageAttributes", e);
                return null;
            }
        }

        #endregion
    }


}

