﻿using System.Runtime.Remoting.Channels;
using Infragistics.Documents.Excel;
using log4net;
using LS.Data;
using LS.Data.Model;
using LS.Data.Model.CatalogSectionModels;
using LS.Web.Models;
using LS.Web.Utility;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Reflection;
using System.Diagnostics;
using System.ServiceProcess;
using NinjaNye.SearchExtensions;
using Kendo.DynamicLinq;

namespace LS.Web.Controllers
{
    public class AdvanceImportApiController : ApiController
    {
        #region Variables
        private static ILog _logger = LogManager.GetLogger(typeof(AdvanceImportApiController));
        public CSEntities objLS = new CSEntities();
        private string importStatus = string.Empty;
        private string Message = string.Empty;
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        DataSet _dsSheets = new DataSet();
        SqlConnection con = new SqlConnection();
        private readonly CSEntities _dbcontext = new CSEntities();
        private ImportController importController = new ImportController();
        #endregion

        #region enum variable CSF
        public enum CSF
        {
            [Description("CATALOG_ID")]
            CATALOG_ID = 1,
            [Description("CATALOG_NAME")]
            CATALOG_NAME = 2,
            [Description("CATALOG_VERSION")]
            CATALOG_VERSION = 3,
            [Description("CATALOG_DESCRIPTION")]
            CATALOG_DESCRIPTION = 4,



            [Description("CATEGORY_ID")]
            CATEGORY_ID = 101,
            [Description("CATEGORY_NAME")]
            CATEGORY_NAME = 102,
            [Description("SUBCATID_L1")]
            SUBCATID_L1 = 103,
            [Description("SUBCATNAME_L1")]
            SUBCATNAME_L1 = 104,
            [Description("SUBCATID_L2")]
            SUBCATID_L2 = 105,
            [Description("SUBCATNAME_L2")]
            SUBCATNAME_L2 = 106,
            [Description("SUBCATID_L3")]
            SUBCATID_L3 = 107,
            [Description("SUBCATNAME_L3")]
            SUBCATNAME_L3 = 108,
            [Description("SUBCATID_L4")]
            SUBCATID_L4 = 109,
            [Description("SUBCATNAME_L4")]
            SUBCATNAME_L4 = 110,
            [Description("SUBCATID_L5")]
            SUBCATID_L5 = 111,
            [Description("SUBCATNAME_L5")]
            SUBCATNAME_L5 = 112,
            [Description("SUBCATID_L6")]
            SUBCATID_L6 = 113,
            [Description("SUBCATNAME_L6")]
            SUBCATNAME_L6 = 114,
            [Description("SUBCATID_L7")]
            SUBCATID_L7 = 115,
            [Description("SUBCATNAME_L7")]
            SUBCATNAME_L7 = 116,
            [Description("SUBCATID_L8")]
            SUBCATID_L8 = 117,
            [Description("SUBCATNAME_L8")]
            SUBCATNAME_L8 = 118,
            [Description("SUBCATID_L9")]
            SUBCATID_L9 = 119,
            [Description("SUBCATNAME_L9")]
            SUBCATNAME_L9 = 120,
            [Description("CATEGORY_SHORT_DESC")]
            CATEGORY_SHORT_DESC = 151,
            [Description("CATEGORY_IMAGE_FILE")]
            CATEGORY_IMAGE_FILE = 152,
            [Description("CATEGORY_IMAGE_NAME")]
            CATEGORY_IMAGE_NAME = 153,
            [Description("CATEGORY_IMAGE_TYPE")]
            CATEGORY_IMAGE_TYPE = 154,
            [Description("CATEGORY_IMAGE_FILE2")]
            CATEGORY_IMAGE_FILE2 = 155,
            [Description("CATEGORY_IMAGE_NAME2")]
            CATEGORY_IMAGE_NAME2 = 156,
            [Description("CATEGORY_IMAGE_TYPE2")]
            CATEGORY_IMAGE_TYPE2 = 157,
            [Description("CATEGORY_CUSTOM_NUM_FIELD1")]
            CATEGORY_CUSTOM_NUM_FIELD1 = 158,
            [Description("CATEGORY_CUSTOM_NUM_FIELD2")]
            CATEGORY_CUSTOM_NUM_FIELD2 = 159,
            [Description("CATEGORY_CUSTOM_NUM_FIELD3")]
            CATEGORY_CUSTOM_NUM_FIELD3 = 160,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD1")]
            CATEGORY_CUSTOM_TEXT_FIELD1 = 161,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD2")]
            CATEGORY_CUSTOM_TEXT_FIELD2 = 162,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD3")]
            CATEGORY_CUSTOM_TEXT_FIELD3 = 163,
            [Description("CATEGORY_PUBLISH2WEB")]
            CATEGORY_PUBLISH2WEB = 164,
            [Description("CATEGORY_PUBLISH2PDF")]
            CATEGORY_PUBLISH2PDF = 165,
            [Description("CATEGORY_PUBLISH2EXPORT")]
            CATEGORY_PUBLISH2EXPORT = 166,
            [Description("CATEGORY_PUBLISH2PRINT")]
            CATEGORY_PUBLISH2PRINT = 167,
            [Description("CATEGORY_PUBLISH2PORTAL")]
            CATEGORY_PUBLISH2PORTAL = 168,


            [Description("FAMILY_ID")]
            FAMILY_ID = 201,
            [Description("FAMILY_NAME")]
            FAMILY_NAME = 202,
            [Description("SUBFAMILY_ID")]
            SUBFAMILY_ID = 203,
            [Description("SUBFAMILY_NAME")]
            SUBFAMILY_NAME = 204,
            [Description("CATEGORY_ID")]
            FAMCATEGORY_ID = 205,
            [Description("FOOT_NOTES")]
            FOOT_NOTES = 206,
            [Description("STATUS")]
            STATUS = 207,
            [Description("FAMILY_SORT")]
            FAMILY_SORT = 208,
            [Description("FAMILY_PUBLISH2WEB")]
            FAMILY_PUBLISH2WEB = 209,
            [Description("FAMILY_PUBLISH2PDF")]
            FAMILY_PUBLISH2PDF = 210,
            [Description("FAMILY_PUBLISH2EXPORT")]
            FAMILY_PUBLISH2EXPORT = 211,
            [Description("FAMILY_PUBLISH2PRINT")]
            FAMILY_PUBLISH2PRINT = 212,
            [Description("FAMILY_PUBLISH2PORTAL")]
            FAMILY_PUBLISH2PORTAL = 213,


            [Description("PRODUCT_ID")]
            PRODUCT_ID = 301,
            [Description("PRODUCT_PUBLISH2WEB")]
            PRODUCT_PUBLISH2WEB = 302,
            [Description("PRODUCT_PUBLISH2PDF")]
            PRODUCT_PUBLISH2PDF = 303,
            [Description("PRODUCT_PUBLISH2EXPORT")]
            PRODUCT_PUBLISH2EXPORT = 304,
            [Description("PRODUCT_PUBLISH2PRINT")]
            PRODUCT_PUBLISH2PRINT = 305,
            [Description("PRODUCT_PUBLISH2PORTAL")]
            PRODUCT_PUBLISH2PORTAL = 306,

            SUBPRODUCT_ID = 307,
            [Description("SUBPRODUCT_PUBLISH2WEB")]
            SUBPRODUCT_PUBLISH2WEB = 308,
            [Description("SUBPRODUCT_PUBLISH2PDF")]
            SUBPRODUCT_PUBLISH2PDF = 309,
            [Description("SUBPRODUCT_PUBLISH2EXPORT")]
            SUBPRODUCT_PUBLISH2EXPORT = 310,
            [Description("SUBPRODUCT_PUBLISH2PRINT")]
            SUBPRODUCT_PUBLISH2PRINT = 311,
            [Description("SUBPRODUCT_PUBLISH2PORTAL")]
            SUBPRODUCT_PUBLISH2PORTAL = 312,

            [Description("CATALOG_ITEM_NO")]
            CATALOG_ITEM_NO = 302,
            [Description("PRODUCT_SORT")]
            PRODUCT_SORT = 303,
            [Description("REMOVE_PRODUCT")]
            REMOVE_PRODUCT = 304,
            [Description("ATTRIBUTE_ID")]
            ATTRIBUTE_ID = 401,
            /******Product Input screen   ********/
            [Description("FAMILYID")]
            FAMILYID = 402,
            [Description("EXIT_FLAG")]
            EXIT_FLAG = 403,
            /******Product Input screen   ********/

            /****Newly added System field ********/
            [Description("Family_PdfTemplate")]
            Family_PdfTemplate = 404,
            [Description("Catalog_PdfTemplate")]
            Catalog_PdfTemplate = 405,
            [Description("Category_PdfTemplate")]
            Category_PdfTemplate = 406,
            [Description("Product_PdfTemplate")]
            Product_PdfTemplate = 407,
            /****Newly added System field ********/
        };
        #endregion

        #region Get/insert/update/delete templates
        /// <summary>
        /// Get all Templates for import process
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public IList GetTemplateForImport()
        {
            try
            {
                var customerId = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var sampleTemplate = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.CUSTOMER_ID == 0 && x.CATALOG_ID == 0).OrderBy(y => y.TEMPLATE_ID).ToList();
                var withoutSampleTemplate = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.CUSTOMER_ID == customerId || x.CUSTOMER_ID == 0 && x.CATALOG_ID != 0).OrderByDescending(y => y.LAST_MODIFIED_DATE).ToList();
                withoutSampleTemplate = withoutSampleTemplate.Where(x => x.FILE_NAME != null && !x.FILE_NAME.Contains("~")).ToList();
                var templateDetails = sampleTemplate.Union(withoutSampleTemplate);
                return templateDetails.ToList();
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController :GetTemplateForImport ", ex);
                return null;
            }
        }



        /// <summary>
        /// Get template details for particular id for getting catalog ,import input format and mapping details.
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public Tuple<IList, bool> GetTemplateDetails(int templateId)
        {
            IList list = null;
            bool fileNameExistsInFolder = false;
            string str_filePath = string.Empty;

            try
            {
                var customerId = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var templateExists = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == templateId && x.CUSTOMER_ID == customerId);

                if (templateExists.Any())
                {
                    list = templateExists.ToList();

                    var templateFileName = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == templateId && x.CUSTOMER_ID == customerId).Select(x => x.FILE_NAME).FirstOrDefault();

                    if (templateFileName.Any() && templateFileName != null)
                    {
                        str_filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate/") + templateFileName;

                        if (File.Exists(str_filePath))
                        {
                            fileNameExistsInFolder = true;
                        }
                    }

                }
                return new Tuple<IList, bool>(list, fileNameExistsInFolder);
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at AdvanceImportApiController : GetTemplateDetails", objexception);
                return null;
            }
        }




        /// <summary>
        /// Save the template details with below details
        /// </summary>
        /// <param name="templateNameImport"></param>
        /// <param name="catalogId"></param>
        /// <param name="importFormat"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public string SaveTemplateDetails(string templateNameImport, int catalogId, string importFormat, string filePath, string modifyTemplateFlag, string workBookFlag)
        {
            try
            {
                int templateId = 0;
                string str_GetFilePath = null;
                string str_FileName = string.Empty;
                var customerId = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var templateDetailsInsert = new TB_TEMPLATE_DETAILS_IMPORT();
                bool templateNameExists = false;


                if (modifyTemplateFlag == "1")
                {
                    var modifyTemplateDetails = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.TEMPLATE_NAME == templateNameImport && x.CUSTOMER_ID == customerId).FirstOrDefault();
                    templateId = modifyTemplateDetails.TEMPLATE_ID;



                    if (workBookFlag == "1")
                    {

                        //  string str_OldImportFileName = "ImportTemplate~" + templateNameImport + "." + importFormat;

                        string str_OldImportFileName = string.Format("{0}{1}{2}", templateNameImport, ".", importFormat);
                        //      str_GetFilePath = "ImportTemplate~" + Path.GetFileName(filePath);



                        string str_OldDestinationPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate"));
                        string str_OldDestinationFile = Path.Combine(str_OldDestinationPath, str_OldImportFileName);



                        if (File.Exists(str_OldDestinationFile))
                        {
                            File.Delete(str_OldDestinationFile);
                            str_FileName = templateNameImport + '.' + importFormat;
                            string str_DestinationPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate"));
                            string str_DestinationFile = Path.Combine(str_DestinationPath, str_FileName);
                            // File.Move(sourceFile, destinationFile);
                            System.IO.File.Copy(filePath, str_DestinationFile);

                        }

                        if (modifyTemplateDetails != null)
                        {

                            ///**** Added Mapping and sheet details table to delete when there is different workbook in same template name  *****/////
                            IEnumerable<TB_TEMPLATE_MAPPING_DETAILS_IMPORT> list_MappingDetails = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == templateId);
                            IEnumerable<TB_TEMPLATE_SHEET_DETAILS_IMPORT> list_SheetDetails = objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == templateId).ToList();


                            //  var qsMapping = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == templateId);
                            if (list_MappingDetails != null && list_SheetDetails != null)
                            {
                                objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.RemoveRange(list_MappingDetails);
                                objLS.SaveChanges();

                                objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT.RemoveRange(list_SheetDetails);
                                objLS.SaveChanges();

                            }
                        }
                    }

                    else if (workBookFlag == "0")
                    {
                        if (modifyTemplateDetails != null)
                        {


                            string str_OldImportFileName = string.Format("{0}{1}{2}", templateNameImport, ".", importFormat);
                            //      str_GetFilePath = "ImportTemplate~" + Path.GetFileName(filePath);



                            string str_OldDestinationPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate"));
                            string str_OldDestinationFile = Path.Combine(str_OldDestinationPath, str_OldImportFileName);



                            if (File.Exists(str_OldDestinationFile))
                            {
                                File.Delete(str_OldDestinationFile);
                                str_FileName = templateNameImport + '.' + importFormat;
                                string str_DestinationPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate"));
                                string str_DestinationFile = Path.Combine(str_DestinationPath, str_FileName);
                                // File.Move(sourceFile, destinationFile);
                                System.IO.File.Copy(filePath, str_DestinationFile);

                            }

                            else
                            {
                                str_FileName = templateNameImport + '.' + importFormat;
                                string str_DestinationPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate"));
                                string str_DestinationFile = Path.Combine(str_DestinationPath, str_FileName);
                                // File.Move(sourceFile, destinationFile);
                                System.IO.File.Copy(filePath, str_DestinationFile);
                            }

                            var templateNameExists_SheetDetails = objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_DETAILS_IMPORT, TSD => TSD.TEMPLATE_ID, TDI =>
                     TDI.TEMPLATE_ID, (TSD, TDI) => new { TSD, TDI }).Where(X => X.TDI.TEMPLATE_NAME == templateNameImport).FirstOrDefault();


                            if (templateNameExists_SheetDetails != null)
                            {
                                templateNameExists_SheetDetails.TSD.FLAG_MAPPING = false;
                                templateNameExists_SheetDetails.TSD.FLAG_VALIDATION = false;
                                templateNameExists_SheetDetails.TSD.FLAG_IMPORT = false;
                                objLS.SaveChanges();
                            }

                            //var modifyTemplateCustomerSetting = objLS.Customer_Settings.Join(objLS.Customer_User, cs => cs.CustomerId, cu => cu.CustomerId, (cs, cu) => new { cs, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.cs).FirstOrDefault();
                            //if (modifyTemplateCustomerSetting != null)
                            //{
                            //    var picklistValue = modifyTemplateCustomerSetting.ShowCustomAPPS;
                            //    var subCatalog = modifyTemplateCustomerSetting.SubCatalog_Items;
                            //    return "Catalog Valid" + "_YES_PRODUCT_" + picklistValue + "_" + subCatalog + "_" + templateId + "_" + templateNameExists;

                            //}
                        }
                    }

                }

                else if (modifyTemplateFlag == "2")
                {
                    var modifyTemplateDetails = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.TEMPLATE_NAME == templateNameImport && x.CUSTOMER_ID == customerId).FirstOrDefault();
                    templateId = modifyTemplateDetails.TEMPLATE_ID;
                }

                else
                {
                    if (templateNameImport == "" || templateNameImport == null)
                    {
                        string importFileName = Path.GetFileNameWithoutExtension(filePath);
                        templateNameImport = "ImportTemplate~" + importFileName;
                    }

                    var templateExists = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.TEMPLATE_NAME == templateNameImport && x.CUSTOMER_ID == customerId).FirstOrDefault();
                    if (templateExists == null)
                    {
                        /////---------------------------------Import template adding file name start ----------------------------------- 

                        str_GetFilePath = Path.GetFileName(filePath);
                        if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate")))
                        {
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate"));
                        }
                        str_FileName = templateNameImport + '.' + importFormat;
                        string destinationPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate"));
                        string destinationFile = Path.Combine(destinationPath, str_FileName);
                        // File.Move(sourceFile, destinationFile);
                        System.IO.File.Copy(filePath, destinationFile);

                        /////---------------------------------Import template adding file name end----------------------------------- 
                        templateDetailsInsert.CATALOG_ID = catalogId;
                        templateDetailsInsert.CUSTOMER_ID = customerId;
                        templateDetailsInsert.EXCEL_FORMAT = importFormat;
                        templateDetailsInsert.TEMPLATE_NAME = templateNameImport;
                        templateDetailsInsert.CREATED_USER = User.Identity.Name;
                        templateDetailsInsert.CREATED_DATE = DateTime.Now;
                        templateDetailsInsert.FILE_NAME = str_FileName;
                        templateDetailsInsert.LAST_MODIFIED_DATE = DateTime.Now;
                        objLS.TB_TEMPLATE_DETAILS_IMPORT.Add(templateDetailsInsert);
                        objLS.SaveChanges();

                        templateId = templateDetailsInsert.TEMPLATE_ID;
                    }
                    else
                    {
                        //templateExists.CATALOG_ID = catalogId;
                        //templateExists.CUSTOMER_ID = customerId;
                        //templateExists.EXCEL_FORMAT = importFormat;
                        //templateExists.TEMPLATE_NAME = templateNameImport;
                        //templateExists.CREATED_USER = User.Identity.Name;
                        //templateExists.CREATED_DATE = DateTime.Now;
                        templateNameExists = true;
                    }

                }


                var customerSetting = objLS.Customer_Settings.Join(objLS.Customer_User, cs => cs.CustomerId, cu => cu.CustomerId, (cs, cu) => new { cs, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.cs).FirstOrDefault();
                if (customerSetting != null)
                {
                    var picklistValue = customerSetting.ShowCustomAPPS;
                    var subCatalog = customerSetting.SubCatalog_Items;
                    return "Catalog Valid" + "_YES_PRODUCT_" + picklistValue + "_" + subCatalog + "_" + templateId + "_" + templateNameExists;

                }
                return "Catalog Valid" + "_YES_PRODUCT_true_true";
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at AdvanceImportApiController : SaveTemplateDetails", objexception);
                return null;
            }
        }
        /// <summary>
        /// Modify Existing template details
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns>This function returns existing import template file path</returns>
        [System.Web.Http.HttpGet]


        public JsonResult ModifyTemplateDetails(string fileName)
        {
            string importFilePath = string.Empty;
            string filePath = string.Empty;
            string importFileName = string.Empty;

            string sessionId;
            sessionId = Guid.NewGuid().ToString();

            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/Views/App/ImportSheets")))
            {
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/Views/App/ImportSheets"));
            }


            importFileName = sessionId + Path.GetExtension(fileName);

            filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate/") + fileName;

            if (File.Exists(filePath))
            {
                string destinationPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Views/App/ImportSheets/"));
                string destinationFile = Path.Combine(destinationPath, importFileName);
                // File.Move(sourceFile, destinationFile);
                System.IO.File.Copy(filePath, destinationFile);

                importFilePath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Views/App/ImportSheets"), importFileName);
            }
            else
            {
                importFilePath = "0";
            }


            return new JsonResult() { Data = importFilePath };

        }


        //[System.Web.Http.HttpGet]

        //public Tuple<string, byte> SelectedImport(string captionName, int selectedTemplate, string importTypeSelection)
        //{
        //    string attributeName = string.Empty;
        //    byte attributeType = 0;

        //    var attributeId = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == captionName).Select(x => x.ATTRIBUTE_ID).FirstOrDefault();

        //    attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();

        //    attributeType= _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId).Select(x => x.ATTRIBUTE_TYPE).FirstOrDefault();

        //    var attributeCheck = _dbcontext.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.CAPTION == captionName).FirstOrDefault();

        //    if(attributeCheck !=null && attributeId>0)
        //    {
        //        using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString()))
        //        {

        //            SqlCommand insert_MappiingdetailsSqlcommand = sqlConnection.CreateCommand();
        //            insert_MappiingdetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS";
        //            insert_MappiingdetailsSqlcommand.CommandType = CommandType.StoredProcedure;
        //            insert_MappiingdetailsSqlcommand.Connection = sqlConnection;
        //            insert_MappiingdetailsSqlcommand.CommandTimeout = 0;
        //            insert_MappiingdetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "UPDATE";
        //            insert_MappiingdetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.Int).Value = selectedTemplate;
        //            insert_MappiingdetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importTypeSelection;
        //            insert_MappiingdetailsSqlcommand.Parameters.Add("@CAPTION", SqlDbType.NVarChar).Value = captionName;
        //            insert_MappiingdetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.NVarChar).Value = attributeId;

        //            sqlConnection.Open();
        //            insert_MappiingdetailsSqlcommand.ExecuteNonQuery();


        //        }
        //    }
        //    else if (attributeCheck != null && attributeId > 0)
        //    {
        //        using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString()))
        //        {

        //            SqlCommand insert_MappiingdetailsSqlcommand = sqlConnection.CreateCommand();
        //            insert_MappiingdetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS";
        //            insert_MappiingdetailsSqlcommand.CommandType = CommandType.StoredProcedure;
        //            insert_MappiingdetailsSqlcommand.Connection = sqlConnection;
        //            insert_MappiingdetailsSqlcommand.CommandTimeout = 0;
        //            insert_MappiingdetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "INSERT";
        //            insert_MappiingdetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.Int).Value = selectedTemplate;
        //            insert_MappiingdetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importTypeSelection;
        //            insert_MappiingdetailsSqlcommand.Parameters.Add("@CAPTION", SqlDbType.NVarChar).Value = captionName;
        //            insert_MappiingdetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.NVarChar).Value = attributeId;

        //            sqlConnection.Open();
        //            insert_MappiingdetailsSqlcommand.ExecuteNonQuery();


        //        }
        //    }


        //    return new Tuple<string, byte>(attributeName, attributeType);
        //}


        [System.Web.Http.HttpPost]
        public string DeleteTemplate(int templateId)
        {
            try
            {
                var customerId = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var var_ImportTemplateExists = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == templateId && x.CUSTOMER_ID == customerId).FirstOrDefault();

                if (var_ImportTemplateExists != null)
                {
                    ///**** Added Mapping and sheet details table for deletion process  *****/////
                    IEnumerable<TB_TEMPLATE_MAPPING_DETAILS_IMPORT> list_MappingDetails = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == templateId);
                    IEnumerable<TB_TEMPLATE_SHEET_DETAILS_IMPORT> list_SheetDetails = objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == templateId).ToList();


                    //  var qsMapping = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == templateId);
                    if (list_MappingDetails != null && list_SheetDetails != null)
                    {
                        objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.RemoveRange(list_MappingDetails);
                        objLS.SaveChanges();

                        objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT.RemoveRange(list_SheetDetails);
                        objLS.SaveChanges();

                        string str_TemplateFileName = var_ImportTemplateExists.FILE_NAME;

                        string str_DestinationPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate");
                        string str_DestinationFile = Path.Combine(str_DestinationPath, str_TemplateFileName);


                        if ((System.IO.File.Exists(str_DestinationFile)))
                        {
                            System.IO.File.Delete(str_DestinationFile);

                        }

                        objLS.TB_TEMPLATE_DETAILS_IMPORT.Remove(var_ImportTemplateExists);
                        objLS.SaveChanges();

                    }

                }

                //var qsMapping = objLS.QS_IMPORTMAPPING.Where(x => x.TEMPLATE_ID == templateId).FirstOrDefault();
                //var mappingSheetDetails = new TB_TEMPLATE_SHEET_DETAILS_IMPORT();
                return "Import template deleted Successfully";
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at AdvanceImportApiController : GetTemplateDetails", objexception);
                return null;
            }
        }
        #endregion

        [System.Web.Http.HttpGet]
        public JsonResult GetImportSpecs(string SheetName, string ExcelPath, int templateId, string importType, int selectAll)
        {

            DataTable dtable = importSelectionColumnGrid(SheetName, ExcelPath, templateId);
            try
            {

                string[] familyAttr = new string[] { "7", "9", "11", "12", "13", "14" };
                DataTable dtClone = dtable;
                if (!(importType == "Familyproducts"))
                {
                    for (int j = dtable.Rows.Count - 1; j >= 0; j--)
                    {
                        DataRow dr = dtable.Rows[j];
                        if (importType.ToUpper().Contains("FAM") && !familyAttr.Contains(dr["FieldType"].ToString()) && dr["FieldType"].ToString() != "0" && dr["FieldType"].ToString() != "")
                            dr.Delete();
                        else if (importType.ToUpper().Contains("FAM") && (dr["EXCELCOLUMN"].ToString().ToUpper() == "SUBPRODUCT_ID" || dr["EXCELCOLUMN"].ToString().ToUpper() == "SUBITEM#") || dr["EXCELCOLUMN"].ToString().ToUpper() == "SUBCATALOG_ITEM_NO")
                            dr.Delete();
                        else if (!importType.ToUpper().Contains("FAM") && familyAttr.Contains(dr["FieldType"].ToString()) && dr["FieldType"].ToString() != "0" && dr["FieldType"].ToString() != "")
                            dr.Delete();

                    }
                }


                DataColumn dc = new DataColumn("Check");
                dc.DataType = typeof(bool);
                dc.DefaultValue = false;
                dtable.Columns.Add(dc);

                dc = new DataColumn("MapAttributes");
                dc.DataType = typeof(string);
                dc.DefaultValue = "";
                dtable.Columns.Add(dc);

                dc = new DataColumn("AttributeNames");
                dc.DataType = typeof(string);
                dc.DefaultValue = "";
                dtable.Columns.Add(dc);

                if (selectAll == 1)
                {
                    foreach (DataRow dr in dtable.Rows)
                    {
                        string systemField = Convert.ToString(dr["IsSystemField"]);
                        string excelColumn = Convert.ToString(dr["EXCELCOLUMN"]);
                        if (systemField != "True")
                        {
                            if (importType.ToLower() == "families")
                                dr["FieldType"] = 7;
                            else if (importType.ToLower() == "categories")
                                dr["FieldType"] = 25;
                            else
                                dr["FieldType"] = 1;
                            dr["SelectedToImport"] = true;
                            dr["IsSystemField"] = false;
                            dr["Check"] = true;
                            dr["MapAttributes"] = " <<<New Attribute>>>";
                            dr["AttributeNames"] = excelColumn;
                        }
                    }
                }

                var dynamicColumns = new Dictionary<string, string>();
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                JsonWriter jsonWriter = new JsonTextWriter(sw);
                var model = new DashBoardModel();
                using (DataTableReader reader = dtable.CreateDataReader())
                {
                    jsonWriter.WriteStartObject();
                    jsonWriter.WritePropertyName("Data");
                    jsonWriter.WriteStartArray();
                    while (reader.Read())
                    {
                        jsonWriter.WriteStartObject();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            jsonWriter.WritePropertyName(reader.GetName(i));
                            jsonWriter.WriteValue(reader.GetValue(i).ToString());
                            if (!dynamicColumns.ContainsKey(reader.GetName(i)) && !string.IsNullOrEmpty(reader.GetName(i)))
                            {
                                dynamicColumns.Add(reader.GetName(i), reader.GetValue(i).ToString().ToUpper());
                            }
                        }
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WritePropertyName("Columns");
                    jsonWriter.WriteStartArray();
                    foreach (string key in dynamicColumns.Keys)
                    {
                        jsonWriter.WriteStartObject();
                        jsonWriter.WritePropertyName("title");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WritePropertyName("field");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WriteEndObject();
                }
                model.Data = sb.ToString();
                return new JsonResult() { Data = model };

            }
            catch (Exception objexception)
            {
                _logger.Error("Error at ImportApiController : GetProdSpecs", objexception);
                return null;
            }
        }


        [System.Web.Http.HttpGet]
        public DataTable importSelectionColumnGrid(string SheetName, string ExcelPath, int templateId)
        {
            System.Web.HttpContext.Current.Session["ImportTemplate"] = templateId;

            var dt = new DataTable();
            DataTable dtable = new DataTable();
            OleDbConnection conn;
            try
            {
                if (!string.IsNullOrEmpty(ExcelPath))
                {
                    Workbook workbook = Workbook.Load(ExcelPath);
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    string strConn;
                    if (ExcelPath.Substring(ExcelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=2\"";
                    else
                        //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + ExcelPath +
                        //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=2\"";
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelPath +
                                ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=2\"";
                    conn = new OleDbConnection(strConn);
                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }
                    string selectedSheetName = SheetName;
                    if (!SheetName.Contains("$"))
                    {
                        SheetName = SheetName + "$";
                    }
                    dt = new DataTable(SheetName);
                    var dc = new DataColumn("ExcelColumn", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("CatalogField", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("SelectedToImport", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("IsSystemField", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("FieldType", typeof(string));
                    dt.Columns.Add(dc);


                    // For Mapping Attribute'


                    dc = new DataColumn("MappedAttributeName", typeof(string));
                    dt.Columns.Add(dc);

                    // End

                    string sheetNameworkbook = SheetName.Replace("$", "").TrimEnd();

                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells.Count();
                    for (int j = 0; j < iCols; j++)
                    {
                        // Name Changes Product(Family) in November 11 2022
                        if (sheetNameworkbook == "Products")
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null)
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace("PRODUCT_ID", "FAMILY_ID")
                                        .Replace("PRODUCT_NAME", "FAMILY_NAME")
                                        .Replace("SUBPRODUCT_ID", "SUBFAMILY_ID")
                                        .Replace("SUBPRODUCT_NAME", "SUBFAMILY_NAME")
                                        .Replace("Product_PdfTemplate", "Family_PdfTemplate")
                                        .Replace("Item_PdfTemplate", "Product_PdfTemplate")
                                        .Replace("PRODUCT_PUBLISH2PDF", "FAMILY_PUBLISH2PDF")
                                        .Replace("PRODUCT_PUBLISH2PRINT", "FAMILY_PUBLISH2PRINT")
                                        .Replace("PRODUCT_PUBLISH2WEB", "FAMILY_PUBLISH2WEB")
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();

                                if (sColName.ToUpper() != "ACTION")
                                {
                                    dr[0] = sColName;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                        else if (sheetNameworkbook == "Items")
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null)
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace("PRODUCT_ID", "FAMILY_ID")
                                        .Replace("ITEM_ID", "PRODUCT_ID")
                                        .Replace("PRODUCT_NAME", "FAMILY_NAME")
                                        .Replace("SUBPRODUCT_ID", "SUBFAMILY_ID")
                                        .Replace("SUBPRODUCT_NAME", "SUBFAMILY_NAME")
                                        .Replace("ITEM_PUBLISH2WEB", "PRODUCT_PUBLISH2WEB")
                                        .Replace("ITEM_PUBLISH2PRINT", "PRODUCT_PUBLISH2PRINT")
                                        .Replace("ITEM_PUBLISH2PDF", "PRODUCT_PUBLISH2PDF")
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();

                                if (sColName.ToUpper() != "ACTION")
                                {
                                    dr[0] = sColName;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                        else if (sheetNameworkbook == "ProductItems")
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null)
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace("PRODUCT_ID", "FAMILY_ID")
                                        .Replace("ITEM_ID", "PRODUCT_ID")
                                        .Replace("PRODUCT_NAME", "FAMILY_NAME")
                                        .Replace("SUBPRODUCT_ID", "SUBFAMILY_ID")
                                        .Replace("SUBPRODUCT_NAME", "SUBFAMILY_NAME")
                                        .Replace("ITEM_PUBLISH2WEB", "PRODUCT_PUBLISH2WEB")
                                        .Replace("ITEM_PUBLISH2PRINT", "PRODUCT_PUBLISH2PRINT")
                                        .Replace("ITEM_PUBLISH2PDF", "PRODUCT_PUBLISH2PDF")
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();

                                if (sColName.ToUpper() != "ACTION")
                                {
                                    dr[0] = sColName;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                        else if (sheetNameworkbook == "TableDesigner")
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null)
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace("PRODUCT_ID", "FAMILY_ID")
                                        .Replace("PRODUCT_NAME", "FAMILY_NAME")
                                        .Replace("SUBPRODUCT_ID", "SUBFAMILY_ID")
                                        .Replace("SUBPRODUCT_NAME", "SUBFAMILY_NAME")
                                        .Replace("PRODUCT_TABLE_STRUCTURE", "FAMILY_TABLE_STRUCTURE")
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();

                                if (sColName.ToUpper() != "ACTION")
                                {
                                    dr[0] = sColName;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                        else
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null)
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();

                                if (sColName.ToUpper() != "ACTION")
                                {
                                    dr[0] = sColName;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }

                        // Name Changes Product(Family) in November 11 2022 End
                    }


                    _dsSheets.Tables.Add(dt);
                    dt.Dispose();
                    dtable = GetExcelColumn2CatalogMapping(SheetName, templateId, selectedSheetName);
                    conn.Close();
                    conn.Dispose();
                }
                return dtable;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at ImportApiController :importSelectionColumnGrid", objException);
                return null;
                conn.Close();
                conn.Dispose();
            }
        }

        public static Boolean IsCatalogStudioField(String enumFieldName)
        {
            return Enum.GetNames(typeof(CSF)).Any(val => enumFieldName == val);
        }

        DataTable ExcelColumn2CatalogMapping = new DataTable();
        public DataTable GetExcelColumn2CatalogMapping(String xlsSheetName, int templateId, string selectedSheetName)
        {
            try
            {

                DataTable dt = _dsSheets.Tables[xlsSheetName];
                int i = 0;
                dt.Columns.Add("AttrMapping");
                foreach (DataRow dr in dt.Rows)//
                {
                    string sCheckAttributeName = dt.Rows[i][0].ToString();
                    //Check if this is a CatalogStudio system defined field name
                    Boolean vaild = IsCatalogStudioField(sCheckAttributeName);

                    if (vaild) //CatalogStudio Field
                    {
                        dr["CatalogField"] = sCheckAttributeName;
                        dr["IsSystemField"] = true;
                        dr["FieldType"] = 0;
                        dr["SelectedToImport"] = true;

                    }
                    else //User defined attribute name
                    {



                        ///   var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == sCheckAttributeName || x.ATTRIBUTE_NAME == sCheckAttributeName && x.ATTRIBUTE_ID!=1 && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();


                        var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => (x.FLAG_RECYCLE == "A" && x.CAPTION == sCheckAttributeName && x.ATTRIBUTE_ID != 1) || (x.FLAG_RECYCLE == "A" && x.ATTRIBUTE_NAME == sCheckAttributeName && x.ATTRIBUTE_ID != 1)).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                        string sAttributeNameInCatalog;
                        var customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                        if (customerid != null)
                        {
                            var drAttr =
                                objLS.TB_ATTRIBUTE.Join(objLS.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                                    tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                    .Where(
                                        x =>
                                            x.tcp.ATTRIBUTE_NAME == attributeName &&
                                            x.tpf.CUSTOMER_ID == customerid.CustomerId).Select(x => x.tcp).ToList();


                            if (drAttr.Count > 0)
                            {
                                string attrMappingName = "";

                                sAttributeNameInCatalog = drAttr[0].ATTRIBUTE_NAME;
                                int attributeId = 0;

                                if (templateId != 0)
                                {
                                    //var importMapping = objLS.QS_IMPORTMAPPING.Where(x => x.TEMPLATE_ID == templateId && x.Excel_Column == sAttributeNameInCatalog).FirstOrDefault();

                                    // attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == templateId && x.CAPTION == sCheckAttributeName && x.Sheet_Name == selectedSheetName).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());


                                    // attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == templateId && x.CAPTION == sCheckAttributeName && x.Sheet_Name == selectedSheetName).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());

                                    attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                                                           Where(x => x.cs.CAPTION == sCheckAttributeName && x.cs.TEMPLATE_ID == templateId && x.cu.SHEET_NAME == selectedSheetName).Select(x => x.cs.ATTRIBUTE_ID).FirstOrDefault());


                                    if (attributeId == 0)
                                    {
                                        attributeId = Convert.ToInt32(objLS.TB_ATTRIBUTE.Where(x => (x.CAPTION == sCheckAttributeName || x.ATTRIBUTE_NAME == sCheckAttributeName) && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_ID).FirstOrDefault());

                                    }
                                    var mappingAttributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();

                                    //if (importMapping != null)
                                    //    attrMappingName = importMapping.Mapping_Name;

                                    //else
                                    //    attrMappingName = sAttributeNameInCatalog;

                                    if (mappingAttributeName != null && attributeId > 0)
                                    {
                                        attrMappingName = mappingAttributeName;


                                        string sAttributeDatatype = drAttr[0].ATTRIBUTE_DATATYPE;
                                        string sAttrType = drAttr[0].ATTRIBUTE_TYPE.ToString(CultureInfo.InvariantCulture);
                                        if (sAttributeDatatype == "Date and Time")
                                        {
                                            if (sAttrType == "6" || sAttrType == "1" || sAttrType == "3")
                                            {
                                                dr["FieldType"] = 10;
                                                dr["SelectedToImport"] = true;
                                                dr["IsSystemField"] = true;
                                            }
                                            else if (sAttrType == "7" || sAttrType == "11" || sAttrType == "13")
                                            {
                                                dr["FieldType"] = 14;
                                                dr["SelectedToImport"] = true;
                                                dr["IsSystemField"] = true;
                                            }
                                            else if (sAttrType == "21")
                                            {
                                                dr["FieldType"] = 21;
                                                dr["SelectedToImport"] = true;
                                                dr["IsSystemField"] = true;
                                            }

                                        }
                                        else
                                        {
                                            dr["FieldType"] = drAttr[0].ATTRIBUTE_TYPE.ToString();
                                            dr["SelectedToImport"] = true;
                                            dr["IsSystemField"] = true;
                                        }
                                        dr["CatalogField"] = sAttributeNameInCatalog;
                                        dr["AttrMapping"] = attrMappingName;
                                    }
                                }
                            }
                            else
                            {
                                // var mappingAttributeId = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, TMD => TMD.IMPORT_SHEET_ID, TSD => TSD.IMPORT_SHEET_ID, (TMD, TSD) => new { TMD, TSD }).
                                //    Join
                                //(objLS.TB_ATTRIBUTE,TA=>TA.TMD.ATTRIBUTE_ID,TMD=>TMD.ATTRIBUTE_ID,(TA,TMD)=> new { TA, TMD }).
                                // Where(x => x.TMD.CAPTION == sCheckAttributeName && x.TMD.TEMPLATE_ID == templateId && x.TSD.SHEET_NAME == 
                                // selectedSheetName).ToList();



                                TB_TEMPLATE_MAPPING_DETAILS_IMPORT obj_MappingDetails = new TB_TEMPLATE_MAPPING_DETAILS_IMPORT();

                                var lt_MappingColumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, TMD => TMD.IMPORT_SHEET_ID, TSD => TSD.IMPORT_SHEET_ID, (TMD, TSD) => new { TMD, TSD }).Select(TMD => TMD).
                                Where(x => x.TMD.CAPTION == sCheckAttributeName && x.TMD.TEMPLATE_ID == templateId && x.TSD.SHEET_NAME == selectedSheetName).FirstOrDefault();

                                if (lt_MappingColumns != null && lt_MappingColumns.TMD.ATTRIBUTE_ID == 0)
                                {
                                    dr["CatalogField"] = sCheckAttributeName;
                                    dr["FieldType"] = Convert.ToInt32(lt_MappingColumns.TMD.ATTRIBUTE_TYPE);
                                    dr["SelectedToImport"] = lt_MappingColumns.TMD.SelectedToImport;
                                    dr["AttrMapping"] = " <<<New Attribute>>>";
                                    //   dr["IsSystemField"] = true;
                                }

                                else if (lt_MappingColumns != null && lt_MappingColumns.TMD.ATTRIBUTE_ID > 0)
                                {
                                    var new_AttributeName = objLS.TB_ATTRIBUTE.Where(x => x.FLAG_RECYCLE == "A"
                                   && x.ATTRIBUTE_ID == lt_MappingColumns.TMD.ATTRIBUTE_ID).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();

                                    dr["CatalogField"] = sCheckAttributeName;
                                    dr["FieldType"] = Convert.ToInt32(lt_MappingColumns.TMD.ATTRIBUTE_TYPE);
                                    dr["SelectedToImport"] = lt_MappingColumns.TMD.SelectedToImport;
                                    dr["AttrMapping"] = new_AttributeName;

                                }
                                else
                                {
                                    dr["CatalogField"] = sCheckAttributeName;
                                    dr["FieldType"] = "";
                                    dr["SelectedToImport"] = false;
                                }

                                //New added//
                                //dr["FieldType"] = 1;
                                //dr["AttrMapping"] = " <<<New Attribute>>>";
                            }

                        }
                    }
                    i++;
                }

                return dt;

            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading GetExcelColumn2CatalogMapping in Import Controller", objException);
                return null;
            }
        }

        public void ExportDataSetToExcelerrorlog(DataTable finalDs, string fileName)
        {
            try
            {
                if (fileName.Trim() != "")
                {
                    object context;
                    if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                    {
                        var httpContext = context as HttpContextBase;
                        if (httpContext != null && httpContext.Session != null)
                        {
                            if (finalDs.Rows.Count > 0)
                            {
                                httpContext.Session["ExportTable"] = finalDs;
                            }
                        }
                    }
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at AdvanceImportApiController : ExportDataSetToExcelerrorlog", objexception);
            }
        }

        #region Attribute Mapping
        [System.Web.Http.HttpGet]
        public IList Loadaattrmapping(string importType, string attributeType)
        {
            try
            {
                int getAttributeType = 0;
                int customerId = 0;
                int.TryParse(objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault().ToString(), out customerId);
                var attr = objLS.TB_ATTRIBUTE.Select(x => new
                {
                    ATTRIBUTE_ID = 0,
                    ATTRIBUTE_NAME = " <<<New Attribute>>>",
                    ATTRIBUTE_TYPE = (byte)0
                }).Distinct().ToList();
                var attributes = objLS.TB_ATTRIBUTE.Join(objLS.CUSTOMERATTRIBUTE, ta => ta.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (ta, ca) => new { ta, ca }).Where(x => x.ca.CUSTOMER_ID == customerId).Select(e => new { e.ta.ATTRIBUTE_ID, e.ta.ATTRIBUTE_NAME, e.ta.ATTRIBUTE_TYPE }).OrderBy(x => x.ATTRIBUTE_NAME).Distinct().ToList();

                for (int i = 0; i <= attributes.Count - 1; i++)
                {
                    attr.Add(attributes[i]);
                }
                var attributesNew = attr.Where(x => x.ATTRIBUTE_NAME != "").ToList();

                ///***********************Product input screen *****************************/
                if (attributeType != "undefined" && attributeType != null)
                {
                    getAttributeType = Convert.ToInt32(attributeType);

                    if (importType == "Families")
                    {
                        attributesNew = attr.Where(x => x.ATTRIBUTE_NAME != "" && x.ATTRIBUTE_TYPE == getAttributeType || x.ATTRIBUTE_ID == 0).OrderBy(y => y.ATTRIBUTE_TYPE).ToList();
                    }
                    else if (importType == "Products" || importType == "SubProducts")
                    {
                        attributesNew = attr.Where(x => x.ATTRIBUTE_NAME != "" && x.ATTRIBUTE_TYPE == getAttributeType || x.ATTRIBUTE_ID == 0).OrderBy(y => y.ATTRIBUTE_TYPE).ToList();
                    }
                    else if (importType == "Categories")
                    {
                        attributesNew = attr.Where(x => x.ATTRIBUTE_NAME != "" && x.ATTRIBUTE_TYPE == getAttributeType || x.ATTRIBUTE_ID == 0).OrderBy(y => y.ATTRIBUTE_TYPE).ToList();
                    }
                    return attributesNew;
                }
                else
                {

                    //int customerId = 0;
                    //int.TryParse(objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault().ToString(), out customerId);
                    //var attr = objLS.TB_ATTRIBUTE.Select(x => new
                    //{
                    //    ATTRIBUTE_ID = 0,
                    //    ATTRIBUTE_NAME = " <<<New Attribute>>>",
                    //    ATTRIBUTE_TYPE = (byte)0
                    //}).Distinct().ToList();
                    //var attributes = objLS.TB_ATTRIBUTE.Join(objLS.CUSTOMERATTRIBUTE, ta => ta.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (ta, ca) => new { ta, ca }).Where(x => x.ca.CUSTOMER_ID == customerId).Select(e => new { e.ta.ATTRIBUTE_ID, e.ta.ATTRIBUTE_NAME, e.ta.ATTRIBUTE_TYPE }).OrderBy(x => x.ATTRIBUTE_NAME).Distinct().ToList();

                    //for (int i = 0; i <= attributes.Count - 1; i++)
                    //{
                    //    attr.Add(attributes[i]);
                    //}
                    //var attributesNew = attr.Where(x => x.ATTRIBUTE_NAME != "").ToList();
                    if (importType == "Families")
                    {
                        attributesNew = attr.Where(x => x.ATTRIBUTE_NAME != "" && x.ATTRIBUTE_TYPE != 1 && x.ATTRIBUTE_TYPE != 4 && x.ATTRIBUTE_TYPE != 3 && x.ATTRIBUTE_TYPE != 10 && x.ATTRIBUTE_TYPE != 6).OrderBy(y => y.ATTRIBUTE_TYPE).ToList();
                    }
                    else if (importType == "Products" || importType == "SubProducts")
                    {
                        attributesNew = attr.Where(x => x.ATTRIBUTE_NAME != "" && x.ATTRIBUTE_TYPE != 7 && x.ATTRIBUTE_TYPE != 9 && x.ATTRIBUTE_TYPE != 11 && x.ATTRIBUTE_TYPE != 12 && x.ATTRIBUTE_TYPE != 13).OrderBy(y => y.ATTRIBUTE_TYPE).ToList();
                    }
                    else if (importType == "Categories")
                    {
                        attributesNew = attr.Where(x => x.ATTRIBUTE_NAME != "" && x.ATTRIBUTE_TYPE != 1 && x.ATTRIBUTE_TYPE != 4 && x.ATTRIBUTE_TYPE != 3 && x.ATTRIBUTE_TYPE != 10 && x.ATTRIBUTE_TYPE != 6 && x.ATTRIBUTE_TYPE != 7 && x.ATTRIBUTE_TYPE != 9 && x.ATTRIBUTE_TYPE != 11 && x.ATTRIBUTE_TYPE != 12 && x.ATTRIBUTE_TYPE != 13).OrderBy(y => y.ATTRIBUTE_TYPE).ToList();
                    }
                    return attributesNew;
                }
                /***********************Product input screen *****************************/
            }
            catch (Exception objException)
            {
                _logger.Error("Error at AdvanceImportApiController :  Loadaattrmapping", objException);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public IList Loadaattrmappingexp()
        {
            try
            {
                return objLS.TB_ATTRIBUTE.Where(t => t.IS_CALCULATED).Select(e => new { e.ATTRIBUTE_ID, e.ATTRIBUTE_NAME, e.ATTRIBUTE_TYPE }).ToList();

            }
            catch (Exception objException)
            {
                _logger.Error("Error at AdvanceImportApiController :  Loadaattrmappingexp", objException);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public string Updateformulaforattributes(string newmappingname, string oldmaapingattribute)
        {
            try
            {
                var mapping = new QS_IMPORTMAPPING();
                var updatetbattr = objLS.TB_ATTRIBUTE.Single(x => x.ATTRIBUTE_NAME == oldmaapingattribute);
                if (updatetbattr != null)
                {
                    if (!string.IsNullOrEmpty(newmappingname))
                    {
                        updatetbattr.ATTRIBUTE_CALC_FORMULA = newmappingname;
                        objLS.SaveChanges();
                        return "updated";
                    }
                }
                return "failed";

            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : Updateformulaforattributes", ex);
                return "Import Failed~";
            }
        }

        /// <summary>
        /// Save or update the map attribute list with template id
        /// </summary>
        /// <param name="newmappingname"></param>
        /// <param name="oldmaapingattribute"></param>
        /// <param name="templateId"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public string mappingattribute(string newmappingname, string oldmaapingattribute, int templateId, string importType, string sheetName)
        {

            try
            {
                newmappingname = DecryptStringAES(newmappingname);
                oldmaapingattribute = DecryptStringAES(oldmaapingattribute);

                string str_CaptionName = oldmaapingattribute;

                string str_AttributeName = newmappingname;

                string str_UserName = User.Identity.Name;

                var attributeId = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == str_CaptionName || x.ATTRIBUTE_NAME == str_AttributeName && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_ID).FirstOrDefault();


                //var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.FirstOrDefault(c => c.CAPTION == captionName && c.TEMPLATE_ID == selectedTemplate && c.Sheet_Name == sheetName);
                var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                Where(x => x.cs.CAPTION == str_CaptionName && x.cs.TEMPLATE_ID == templateId && x.cu.SHEET_NAME == sheetName).ToList();

                if (checkcolumns.Count > 0)
                {

                    using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString()))
                    {

                        SqlCommand update_MappingdetailsSqlcommand = sqlConnection.CreateCommand();
                        update_MappingdetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS";
                        update_MappingdetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                        update_MappingdetailsSqlcommand.Connection = sqlConnection;
                        update_MappingdetailsSqlcommand.CommandTimeout = 0;
                        update_MappingdetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "UPDATE";
                        update_MappingdetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.Int).Value = templateId;
                        update_MappingdetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importType;
                        update_MappingdetailsSqlcommand.Parameters.Add("@CAPTION", SqlDbType.NVarChar).Value = str_CaptionName;
                        update_MappingdetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.NVarChar).Value = attributeId;
                        update_MappingdetailsSqlcommand.Parameters.Add("@SHEET_NAME", SqlDbType.NVarChar).Value = sheetName;
                        sqlConnection.Open();
                        update_MappingdetailsSqlcommand.ExecuteNonQuery();


                    }

                }
                else
                {
                    using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString()))
                    {

                        SqlCommand insert_MappingdetailsSqlcommand = sqlConnection.CreateCommand();
                        insert_MappingdetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS";
                        insert_MappingdetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                        insert_MappingdetailsSqlcommand.Connection = sqlConnection;
                        insert_MappingdetailsSqlcommand.CommandTimeout = 0;
                        insert_MappingdetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "INSERT";
                        insert_MappingdetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.Int).Value = templateId;
                        insert_MappingdetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importType;
                        insert_MappingdetailsSqlcommand.Parameters.Add("@CAPTION", SqlDbType.NVarChar).Value = str_CaptionName;
                        insert_MappingdetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.NVarChar).Value = attributeId;
                        insert_MappingdetailsSqlcommand.Parameters.Add("@SHEET_NAME", SqlDbType.NVarChar).Value = sheetName;
                        insert_MappingdetailsSqlcommand.Parameters.Add("@USER_NAME", SqlDbType.NVarChar).Value = str_UserName;
                        sqlConnection.Open();
                        insert_MappingdetailsSqlcommand.ExecuteNonQuery();


                    }
                }


                //if (string.IsNullOrEmpty(newmappingname))
                //{ return null; }
                //  var mapping = new QS_IMPORTMAPPING();

                // var attributeId = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == newmappingname).Select(x=> x.ATTRIBUTE_ID).FirstOrDefault();

                //  var checkcolumns = objLS.QS_IMPORTMAPPING.FirstOrDefault(c => c.Excel_Column == oldmaapingattribute && c.TEMPLATE_ID == templateId);
                // var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.FirstOrDefault(c => c.CAPTION == oldmaapingattribute && c.TEMPLATE_ID == templateId);

                //if (checkcolumns != null)
                //{
                //    // objLS.QS_IMPORTMAPPING.Remove(checkcolumns);
                //    // objLS.SaveChanges();

                //    using (SqlConnection con = new SqlConnection(connectionString))
                //    {
                //        con.Open();
                //        var sqlString = "EXEC('update QS_IMPORTMAPPING set Mapping_Name = ''" + newmappingname + "'' where Excel_column = ''" + oldmaapingattribute + "'' and  TEMPLATE_ID = ''" + templateId + "''  ')";
                //        SqlCommand sqlCommand = new SqlCommand(sqlString, con);
                //        sqlCommand.ExecuteNonQuery();
                //        //   con.close();
                //    }



                //}
                //else
                //{

                //    using (SqlConnection con = new SqlConnection(connectionString))
                //    {
                //        con.Open();
                //        var sqlString = "EXEC('insert into QS_IMPORTMAPPING values (''" + oldmaapingattribute + "'',''" + newmappingname + "'',''" + templateId + "'') ')";
                //        SqlCommand sqlCommand = new SqlCommand(sqlString, con);
                //        sqlCommand.ExecuteNonQuery();
                //        // con.close();
                //    }


                //    //mapping.Excel_Column = oldmaapingattribute;
                //    //mapping.Mapping_Name = newmappingname;
                //    //mapping.TEMPLATE_ID = templateId;
                //    //objLS.QS_IMPORTMAPPING.Add(mapping);
                //    //objLS.SaveChanges();
                //}
                return "Mapping successfull";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : mappingattribute", ex);
                return "Import Failed~";
            }
        }



        #endregion
        [System.Web.Http.HttpPost]
        public string saveAllMappingAttributes(int selectedTemplate, string importTypeSelection, string sheetName, string excelPath, JArray model)
        {
            string result = string.Empty;


            try
            {
                string userName = User.Identity.Name;
                if (importTypeSelection.ToUpper() == "TABLEDESIGNER")
                {
                    //  var checkTabledesignerColumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.FirstOrDefault(c => c.TEMPLATE_ID == selectedTemplate && c.Sheet_Name == sheetName);
                    var checkTabledesignerColumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                                           Where(x => x.cs.TEMPLATE_ID == selectedTemplate && x.cu.SHEET_NAME == sheetName).ToList();

                    if (checkTabledesignerColumns.Count == 0)
                    {
                        using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString()))
                        {

                            SqlCommand insert_MappingdetailsSqlcommand = sqlConnection.CreateCommand();
                            insert_MappingdetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS";
                            insert_MappingdetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                            insert_MappingdetailsSqlcommand.Connection = sqlConnection;
                            insert_MappingdetailsSqlcommand.CommandTimeout = 0;
                            insert_MappingdetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "INSERTCAPTION";
                            insert_MappingdetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.Int).Value = selectedTemplate;
                            insert_MappingdetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importTypeSelection;
                            insert_MappingdetailsSqlcommand.Parameters.Add("@CAPTION", SqlDbType.NVarChar).Value = "FAMILY_TABLE_STRUCTURE";
                            insert_MappingdetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.NVarChar).Value = -1;
                            insert_MappingdetailsSqlcommand.Parameters.Add("@SHEET_NAME", SqlDbType.NVarChar).Value = sheetName;
                            insert_MappingdetailsSqlcommand.Parameters.Add("@USER_NAME", SqlDbType.NVarChar).Value = userName;
                            sqlConnection.Open();
                            insert_MappingdetailsSqlcommand.ExecuteNonQuery();


                        }


                    }

                }

                else
                {

                    DataTable excelData = ConvertExcelToDataTable(excelPath, sheetName);

                    DataTable excelDatawithoutheader = ConvertExcelToDataTableWithoutHeader(excelPath, sheetName);


                    if (importTypeSelection.ToUpper().Contains("CAT"))
                    {
                        string colName = null;

                        if (excelData.Rows.Count > 0)
                        {

                            var index = excelData.Rows[0].ItemArray
                          .Select((c, i) => new { c, i })
                          .Where(x => x.c.ToString().Equals("CATEGORY_NAME"))
                          .Select(x => x.i)
                          .ToArray();



                            if (index.Count() > 0)
                            {
                                colName = excelDatawithoutheader.Rows[0][index[0]].ToString().Trim();

                                //if (colName == "" || colName == null)
                                //{
                                //    colName = "CATEGORY_NAME";
                                //}

                            }
                            //else
                            //{

                            //    if (colName == "" || colName == null)
                            //    {
                            //        colName = "CATEGORY_NAME";
                            //    }
                            //}

                            if (colName == "" || colName == null)
                            {
                                colName = "CATEGORY_NAME";
                            }
                        }

                        else
                        {
                            colName = "CATEGORY_NAME";
                        }

                        var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                  Where(x => x.cs.CAPTION == colName && x.cs.TEMPLATE_ID == selectedTemplate && x.cu.SHEET_NAME == sheetName).ToList();

                        if (checkcolumns.Count == 0)
                        {

                            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString()))
                            {

                                SqlCommand insert_MappingdetailsSqlcommand = sqlConnection.CreateCommand();
                                insert_MappingdetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS";
                                insert_MappingdetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                                insert_MappingdetailsSqlcommand.Connection = sqlConnection;
                                insert_MappingdetailsSqlcommand.CommandTimeout = 0;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "INSERTCAPTION";
                                insert_MappingdetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.Int).Value = selectedTemplate;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importTypeSelection;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@CAPTION", SqlDbType.NVarChar).Value = colName;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.NVarChar).Value = -1;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@SHEET_NAME", SqlDbType.NVarChar).Value = sheetName;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@USER_NAME", SqlDbType.NVarChar).Value = userName;
                                sqlConnection.Open();
                                insert_MappingdetailsSqlcommand.ExecuteNonQuery();


                            }
                        }


                    }


                    if (importTypeSelection.ToUpper().Contains("FAM"))
                    {
                        string colName = null;
                        if (excelData.Rows.Count > 0)
                        {

                            var index = excelData.Rows[0].ItemArray
                          .Select((c, i) => new { c, i })
                          .Where(x => x.c.ToString().Equals("FAMILY_NAME"))
                          .Select(x => x.i)
                          .ToArray();



                            if (index.Count() > 0)
                            {
                                colName = excelDatawithoutheader.Rows[0][index[0]].ToString().Trim();

                                //if (colName == "" || colName == null)
                                //{
                                //    colName = "FAMILY_NAME";
                                //}

                            }

                            //else
                            //{
                            //    if (colName == "" || colName == null)
                            //    {
                            //        colName = "FAMILY_NAME";
                            //    }
                            //}



                            if (colName == "" || colName == null)
                            {
                                colName = "FAMILY_NAME";
                            }

                        }
                        else
                        {
                            colName = "FAMILY_NAME";
                        }

                        var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                  Where(x => x.cs.CAPTION == colName && x.cs.TEMPLATE_ID == selectedTemplate && x.cu.SHEET_NAME == sheetName).ToList();

                        if (checkcolumns.Count == 0)
                        {

                            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString()))
                            {

                                SqlCommand insert_MappingdetailsSqlcommand = sqlConnection.CreateCommand();
                                insert_MappingdetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS";
                                insert_MappingdetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                                insert_MappingdetailsSqlcommand.Connection = sqlConnection;
                                insert_MappingdetailsSqlcommand.CommandTimeout = 0;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "INSERTCAPTION";
                                insert_MappingdetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.Int).Value = selectedTemplate;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importTypeSelection;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@CAPTION", SqlDbType.NVarChar).Value = colName;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.NVarChar).Value = -1;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@SHEET_NAME", SqlDbType.NVarChar).Value = sheetName;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@USER_NAME", SqlDbType.NVarChar).Value = userName;
                                sqlConnection.Open();
                                insert_MappingdetailsSqlcommand.ExecuteNonQuery();


                            }

                        }

                    }


                    if (importTypeSelection.ToUpper().Contains("PRO"))
                    {
                        string colName = null;

                        int customerId = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => x.CustomerId).FirstOrDefault();

                        string displayItemnumber = objLS.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();

                        if (excelData.Rows.Count > 0)
                        {


                            var displayItemnumberIndex = excelData.Rows[0].ItemArray
                              .Select((c, i) => new { c, i })
                              .Where(x => x.c.ToString().Equals(displayItemnumber))
                              .Select(x => x.i)
                              .ToArray();

                            var itemNumberIndex = excelData.Rows[0].ItemArray
                             .Select((c, i) => new { c, i })
                             .Where(x => x.c.ToString().Equals("ITEM#"))
                             .Select(x => x.i)
                             .ToArray();


                            if (displayItemnumberIndex.Count() > 0)
                            {
                                colName = excelDatawithoutheader.Rows[0][displayItemnumberIndex[0]].ToString().Trim();

                                if (colName == "" || colName == null)
                                {
                                    colName = excelData.Rows[0][displayItemnumberIndex[0]].ToString().Trim();
                                }
                            }
                            else if (itemNumberIndex.Count() > 0)
                            {
                                colName = excelDatawithoutheader.Rows[0][itemNumberIndex[0]].ToString().Trim();
                                if (colName == "" || colName == null)
                                {
                                    colName = excelData.Rows[0][itemNumberIndex[0]].ToString().Trim();
                                }

                            }

                            else
                            {
                                colName = "ITEM#";
                            }

                        }

                        else
                        {
                            var displayItemnumberIndex = excelDatawithoutheader.Rows[0].ItemArray
                              .Select((c, i) => new { c, i })
                              .Where(x => x.c.ToString().Equals(displayItemnumber))
                              .Select(x => x.i)
                              .ToArray();

                            var itemNumberIndex = excelDatawithoutheader.Rows[0].ItemArray
                             .Select((c, i) => new { c, i })
                             .Where(x => x.c.ToString().Equals("ITEM#"))
                             .Select(x => x.i)
                             .ToArray();


                            if (displayItemnumberIndex.Count() > 0)
                            {
                                colName = excelDatawithoutheader.Rows[0][displayItemnumberIndex[0]].ToString().Trim();

                            }
                            else if (itemNumberIndex.Count() > 0)
                            {
                                colName = excelDatawithoutheader.Rows[0][itemNumberIndex[0]].ToString().Trim();

                            }

                            else
                            {
                                colName = "ITEM#";
                            }
                        }


                        var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
               Where(x => x.cs.CAPTION == colName && x.cs.TEMPLATE_ID == selectedTemplate && x.cu.SHEET_NAME == sheetName).ToList();

                        if (checkcolumns.Count == 0)
                        {


                            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString()))
                            {

                                SqlCommand insert_MappingdetailsSqlcommand = sqlConnection.CreateCommand();
                                insert_MappingdetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS";
                                insert_MappingdetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                                insert_MappingdetailsSqlcommand.Connection = sqlConnection;
                                insert_MappingdetailsSqlcommand.CommandTimeout = 0;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "INSERTCAPTION";
                                insert_MappingdetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.Int).Value = selectedTemplate;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importTypeSelection;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@CAPTION", SqlDbType.NVarChar).Value = colName;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.NVarChar).Value = -1;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@SHEET_NAME", SqlDbType.NVarChar).Value = sheetName;
                                insert_MappingdetailsSqlcommand.Parameters.Add("@USER_NAME", SqlDbType.NVarChar).Value = userName;
                                sqlConnection.Open();
                                insert_MappingdetailsSqlcommand.ExecuteNonQuery();


                            }

                        }



                    }

                    var mappingattributes = model.ToList();

                    DataTable dt_CaptionAttributes = new DataTable();


                    dt_CaptionAttributes = (DataTable)JsonConvert.DeserializeObject(model.ToString(), (typeof(DataTable)));

                    dt_CaptionAttributes.Columns.Add("RowNum", typeof(Int32));

                    dt_CaptionAttributes.Columns["RowNum"].AutoIncrement = true;

                    //Set the Starting or Seed value.
                    dt_CaptionAttributes.Columns["RowNum"].AutoIncrementSeed = 1;

                    //Set the Increment value.
                    dt_CaptionAttributes.Columns["RowNum"].AutoIncrementStep = 1;

                    int rowindex = 0;

                    foreach (DataRow row in dt_CaptionAttributes.Rows)
                        row["RowNum"] = ++rowindex;



                    string sessionId = Guid.NewGuid().ToString();

                    using (var objSqlConnection = new SqlConnection(connectionString))
                    {

                        objSqlConnection.Open();
                        //string sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBCATATTRTEMP" + sessionId + "'')BEGIN DROP TABLE [##SUBCATATTRTEMP" + sessionId + "] END')";
                        string sqlString = "EXEC('IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME like ''##TEMP_CAPTION" + sessionId + "'')BEGIN DROP TABLE [##TEMP_CAPTION" + sessionId + "] END')";
                        SqlCommand insert_MappingdetailsSqlcommand = new SqlCommand(sqlString, objSqlConnection);
                        insert_MappingdetailsSqlcommand.ExecuteNonQuery();


                        sqlString = CreateTable("[##TEMP_CAPTION" + sessionId + "]", dt_CaptionAttributes);

                        SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                        dbCommandnew.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(objSqlConnection)
                        {
                            DestinationTableName = "[##TEMP_CAPTION" + sessionId + "]"
                        };
                        bulkCopy.WriteToServer(dt_CaptionAttributes);


                        insert_MappingdetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS";
                        insert_MappingdetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                        insert_MappingdetailsSqlcommand.Connection = objSqlConnection;
                        insert_MappingdetailsSqlcommand.CommandTimeout = 0;
                        insert_MappingdetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "INSERT";
                        insert_MappingdetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.Int).Value = selectedTemplate;
                        insert_MappingdetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importTypeSelection;
                        insert_MappingdetailsSqlcommand.Parameters.Add("@SHEET_NAME", SqlDbType.NVarChar).Value = sheetName;
                        insert_MappingdetailsSqlcommand.Parameters.Add("@USER_NAME", SqlDbType.NVarChar).Value = userName;
                        insert_MappingdetailsSqlcommand.Parameters.Add("@Session_Id", SqlDbType.NVarChar).Value = sessionId;

                        insert_MappingdetailsSqlcommand.ExecuteNonQuery();

                    }

                    //foreach (var item in mappingattributes)
                    //{
                    //    string captionName = Convert.ToString(item["captionName"]);
                    //    string attributeName = Convert.ToString(item["attributeName"]);


                    //    if (captionName != "" && attributeName != "")
                    //    {

                    //        var attributeId = _dbcontext.TB_ATTRIBUTE.Where(x =>   (x.FLAG_RECYCLE =="A" && x.CAPTION == captionName) || (x.FLAG_RECYCLE == "A" && x.ATTRIBUTE_NAME == attributeName)).Select(x => x.ATTRIBUTE_ID).FirstOrDefault();


                    //        //var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.FirstOrDefault(c => c.CAPTION == captionName && c.TEMPLATE_ID == selectedTemplate && c.Sheet_Name == sheetName);
                    //        var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                    //        Where(x => x.cs.CAPTION == captionName && x.cs.TEMPLATE_ID == selectedTemplate && x.cu.SHEET_NAME == sheetName).ToList();

                    //if (checkcolumns.Count > 0)
                    //{

                    //    using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString()))
                    //    {

                    //        SqlCommand update_MappingdetailsSqlcommand = sqlConnection.CreateCommand();
                    //        update_MappingdetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS";
                    //        update_MappingdetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    //        update_MappingdetailsSqlcommand.Connection = sqlConnection;
                    //        update_MappingdetailsSqlcommand.CommandTimeout = 0;
                    //        update_MappingdetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "UPDATE";
                    //        update_MappingdetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.Int).Value = selectedTemplate;
                    //        update_MappingdetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importTypeSelection;
                    //        update_MappingdetailsSqlcommand.Parameters.Add("@CAPTION", SqlDbType.NVarChar).Value = captionName;
                    //        update_MappingdetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.NVarChar).Value = attributeId;
                    //        update_MappingdetailsSqlcommand.Parameters.Add("@SHEET_NAME", SqlDbType.NVarChar).Value = sheetName;
                    //        sqlConnection.Open();
                    //        update_MappingdetailsSqlcommand.ExecuteNonQuery();


                    //    }

                    //}
                    //else
                    //{


                    //using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString()))
                    //{

                    //    SqlCommand insert_MappingdetailsSqlcommand = sqlConnection.CreateCommand();
                    //    insert_MappingdetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO5_IMPORT_MAPPING_DETAILS";
                    //    insert_MappingdetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    //    insert_MappingdetailsSqlcommand.Connection = sqlConnection;
                    //    insert_MappingdetailsSqlcommand.CommandTimeout = 0;
                    //    insert_MappingdetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "INSERT";
                    //    insert_MappingdetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.Int).Value = selectedTemplate;
                    //    insert_MappingdetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importTypeSelection;
                    //    insert_MappingdetailsSqlcommand.Parameters.Add("@CAPTION", SqlDbType.NVarChar).Value = captionName;
                    //    insert_MappingdetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.NVarChar).Value = attributeId;
                    //    insert_MappingdetailsSqlcommand.Parameters.Add("@SHEET_NAME", SqlDbType.NVarChar).Value = sheetName;
                    //    insert_MappingdetailsSqlcommand.Parameters.Add("@USER_NAME", SqlDbType.NVarChar).Value = userName;
                    //    sqlConnection.Open();
                    //    insert_MappingdetailsSqlcommand.ExecuteNonQuery();


                    //}


                    //  }
                    // }
                    //}
                    //mapping.Excel_Column = oldmaapingattribute;
                    //mapping.Mapping_Name = newmappingname;
                    //mapping.TEMPLATE_ID = templateId;
                    //objLS.QS_IMPORTMAPPING.Add(mapping);
                    //objLS.SaveChanges();

                }
                return "Mapping successfull";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : mappingattribute", ex);
                return "Import Failed~";
            }
        }

        #region models for success and error list
        public class IMPORT_ERROR_LIST
        {
            public string ErrorMessage { get; set; }
            public string ErrorProcedure { get; set; }
            public string ErrorSeverity { get; set; }
            public string ErrorState { get; set; }
            public string ErrorNumber { get; set; }
            public string ErrorLine { get; set; }
        }

        public class IMPORT_ERROR_LISTERROR
        {
            public string ErrorMessage { get; set; }
            public string ErrorProcedure { get; set; }
            public int ErrorSeverity { get; set; }
            public int ErrorState { get; set; }
            public int ErrorNumber { get; set; }
            public int ErrorLine { get; set; }
        }

        public class IMPORT_ERROR_LISTdel
        {
            public string CATALOG_ITEM_NO { get; set; }
            public string STATUS { get; set; }
        }

        public class MissingColumns
        {
            public string CATALOG_NAME { get; set; }
            public string CATEGORY_NAME { get; set; }
            public string FAMILY_NAME { get; set; }
            public string ITEM_NO { get; set; }
            public string MISSING_COLUMNS { get; set; }

        }

        public class IMPORT_SUCCESS_LIST
        {
            public string CATALOG_NAME { get; set; }
            public string CATEGORY_DETAILS { get; set; }
            public string FAMILY_DETAILS { get; set; }
            public string SUBFAMILY_DETAILS { get; set; }
            public string PRODUCT_DETAILS { get; set; }
            public string STATUS { get; set; }
        }

        public class VALIDATEMPORT_SUCCESS_LIST
        {
            public string CATALOG_NAME { get; set; }
            public string CATEGORY_NAME { get; set; }
            public string FAMILY_NAME { get; set; }
            public string CATALOG_ITEM_NO { get; set; }

        }

        #endregion

        [System.Web.Http.HttpGet]
        public IList getmissingcolumns(string SessionId)
        {
            DataSet ds = new DataSet();

            if (SessionId == "undefined")
            {
                return null;
            }
            List<MissingColumns> errorList = new List<MissingColumns>();
            try
            {
                if (!string.IsNullOrEmpty(SessionId))
                {
                    string[] Session = SessionId.Split(',');
                    importController._SQLString = @"select * from [Missingcolumns" + Session[Session.Length - 1] + "]";
                    ds = importController.CreateDataSet();
                    importController._SQLString = "drop table [Missingcolumns" + Session[Session.Length - 1] + "]";
                    var Connection = new SqlConnection(connectionString);
                    var cmd = new SqlCommand(importController._SQLString, Connection);
                    Connection.Open();
                    cmd.CommandText = importController._SQLString;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    var errorList3 = (from DataRow row in ds.Tables[0].Rows
                                      select new
                                      {
                                          CATALOG_NAME = row["CATALOG_NAME"].ToString(),
                                          CATEGORY_NAME = row["CATEGORY_NAME"].ToString(),
                                          FAMILY_NAME = row["FAMILY_NAME"].ToString(),
                                          CATALOG_ITEM_NO = row["CATALOG_ITEM_NO"].ToString(),
                                          Missing_Columns = row["Missing_Columns"].ToString()
                                      }).ToList();
                    return errorList3;
                }
                else
                {
                    return errorList;
                }

            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at AdvanceImportApiController : getFinishImportFailedpicklistResults", ex);
                return errorList;
            }
        }

        [System.Web.Http.HttpGet]
        public string GetImportStatus(string excelPath, string sheetName)
        {
            DataTable importStatus = new DataTable();
            try
            {
                OleDbConnection conn = excelConnection(excelPath);
                DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                DataRow schemaRow = schemaTable.Rows[0];
                string sheet = schemaRow["TABLE_NAME"].ToString();
                if (!sheet.EndsWith("_"))
                {
                    OleDbDataAdapter daexcel = new OleDbDataAdapter("SELECT  * FROM [" + sheetName + "$]", conn);
                    importStatus.Locale = CultureInfo.CurrentCulture;
                    daexcel.Fill(importStatus);
                }
                return importStatus.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at AdvanceImportApiController : GetImportStatus", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public string FinishImport(string sessionId, string SheetName, string allowDuplicate, string importType, int catalogId, string excelPath, bool batch, string scheduleDate, int templateId, JArray model)
        {

            int user = 0;
            ProgressBar progressBar = new ProgressBar();
            string importResult = string.Empty;
            System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
            string validateResults = string.Empty;
            string sqlConn = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
            int updatecount = 0;
            int newInsertedcount = 0;
            string importTemp;
            importTemp = Guid.NewGuid().ToString();
            var tableDesignerFamilyId = new List<string>();
            List<string> familyIdRemove = new List<string>();
            var removeZeroProductRowForFamily = new List<string>();
            if(SheetName == "Products")
            {
                importType = "Families";
            }

            if (SheetName == "TableDesigner")
            {

                stopWatch.Start();
                progressBar.progressVale = "5";


                DataTable excelData = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "");

                AddIdColumnsForTableDesigner(excelData, importType);  //Add ID columns if it not exists


                var excelDistinctCatalogName = excelData.AsEnumerable()
                .Select(s => new
                {
                    catalogName = s.Field<string>("CATALOG_NAME"),
                })
                .Distinct().ToList();

                string catalogName = excelDistinctCatalogName[0].catalogName.ToString();

                string catalogid = _dbcontext.TB_CATALOG.Where(s => s.CATALOG_NAME == catalogName && s.FLAG_RECYCLE == "A").Select(a => a.CATALOG_ID).FirstOrDefault().ToString();

                foreach (DataRow rows in excelData.Rows)
                {
                    rows["CATALOG_ID"] = catalogid;
                }


                DataSet importDataset = new DataSet();


                string sqlString = string.Empty;

                using (var objSqlConnection = new SqlConnection(sqlConn))
                {

                    objSqlConnection.Open();
                    sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    SqlCommand _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                    _DBCommand.ExecuteNonQuery();
                    sqlString = CreateTable("[##IMPORTTEMP" + importTemp + "]", excelData);

                    SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                    dbCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(excelData);

                    sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";
                    _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                    _DBCommand.CommandTimeout = 0;
                    _DBCommand.ExecuteNonQuery();

                    var objSqlDataAdapter = new SqlDataAdapter("select * from [##IMPORTTEMP" + importTemp + "]", objSqlConnection);
                    objSqlDataAdapter.Fill(importDataset);

                }



                DataTable importDatatable = new DataTable();
                importDatatable = importDataset.Tables[0];


                DataTable deletedRecords = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DELETE");
                AddIdColumnsForTableDesigner(deletedRecords, importType);  //Add ID columns if it not exists

                foreach (DataRow rows in deletedRecords.Rows)
                {
                    rows["CATALOG_ID"] = catalogid;
                }
                DataSet deleteImportDataset = new DataSet();


                if (deletedRecords.Rows.Count > 0)
                {


                    using (var objSqlConnection = new SqlConnection(sqlConn))
                    {

                        objSqlConnection.Open();
                        sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                        _DBCommand.ExecuteNonQuery();
                        sqlString = CreateTable("[##IMPORTTEMP" + importTemp + "]", deletedRecords);

                        SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                        dbCommandnew.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(objSqlConnection)
                        {
                            DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(deletedRecords);

                        sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";
                        _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                        _DBCommand.CommandTimeout = 0;
                        _DBCommand.ExecuteNonQuery();

                        var objSqlDataAdapter = new SqlDataAdapter("select * from [##IMPORTTEMP" + importTemp + "]", objSqlConnection);
                        objSqlDataAdapter.Fill(deleteImportDataset);

                    }
                    deletedRecords = deleteImportDataset.Tables[0];

                }


                // Skipped records

                DataTable skippedRecords = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "SKIP");

                if (skippedRecords != null && skippedRecords.Rows.Count > 0)
                {
                    int skippedCount = skippedRecords.Rows.Count;
                    importResult = importResult + "~SkippedRecords:" + skippedCount.ToString();
                }
                else
                {
                    importResult = importResult + "~SkippedRecords:" + "0";
                }

                // Deleted records



                if (deletedRecords != null && deletedRecords.Rows.Count > 0)
                {
                    int deleteCount = deletedRecords.Rows.Count;
                    importResult = importResult + "~DeletedRecords:" + deleteCount.ToString();
                }
                else
                {
                    importResult = importResult + "~DeletedRecords:" + "0";
                }

                foreach (DataRow deleteRow in deletedRecords.Rows)
                {
                    string deleteCatalogId = deleteRow["CATALOG_ID"].ToString();
                    string deleteFamilyId = deleteRow["FAMILY_ID"].ToString();
                    string deleteStructureNamevalue = deleteRow["STRUCTURE_NAME"].ToString();
                    string deleteFamilyTableDesigner = deleteRow["FAMILY_TABLE_STRUCTURE"].ToString();

                    using (var objSqlConnection = new SqlConnection(sqlConn))
                    {
                        string action = "TableDesignerDelete";
                        objSqlConnection.Open();
                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.CommandTimeout = 0;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = deleteCatalogId ?? "";
                        objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                        objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                        objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = deleteFamilyId ?? "";
                        objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                        objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = "";
                        objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 4000).Value = deleteStructureNamevalue ?? "";
                        objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar, 500).Value = "";
                        objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar, -1).Value = deleteFamilyTableDesigner ?? "";
                        objSqlCommand.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action ?? "";
                        objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                        objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                        objSqlCommand.ExecuteNonQuery();
                        objSqlConnection.Close();
                    }
                }


                if (importDatatable.Rows.Count > 0)
                {

                    tableDesignerFamilyId = importDatatable.AsEnumerable()
                   .Select(r => r.Field<string>("FAMILY_ID"))
                   .Distinct().ToList();


                    foreach (var familyId in tableDesignerFamilyId)
                    {
                        DataSet familyProductCountDataSet = new DataSet();

                        using (var objSqlConnection = new SqlConnection(sqlConn))
                        {
                            string action = "ProductCount";
                            objSqlConnection.Open();
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.CommandTimeout = 0;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = "";
                            objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                            objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                            objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = familyId ?? "";
                            objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                            objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = "";
                            objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 4000).Value = "";
                            objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar, 500).Value = "";
                            objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar, -1).Value = "";
                            objSqlCommand.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action ?? "";
                            objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                            objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                            objSqlConnection.Close();
                            var familyProductCountDataAdapter = new SqlDataAdapter(objSqlCommand);
                            familyProductCountDataAdapter.Fill(familyProductCountDataSet);

                            if (familyProductCountDataSet.Tables[0].Rows.Count > 0)
                            {
                                string productCount = familyProductCountDataSet.Tables[0].Rows[0][0].ToString();
                                if (productCount == "0")
                                {
                                    familyIdRemove.Add(familyId);
                                }

                            }
                        }


                    }
                }

                familyIdRemove.RemoveAll(item => item == null);

                DataTable finalImportDatatable = new DataTable();

                var table = importDatatable.AsEnumerable()
                  .Where(r => !familyIdRemove.Contains(r.Field<string>("FAMILY_ID")));

                if (table.Any())
                {
                    importDatatable = table.CopyToDataTable();
                }


                foreach (DataRow row in importDatatable.Rows)
                {

                    var distinctFamilyValues = importDatatable.AsEnumerable()
                   .Where(a => !a.Field<string>("IS_DEFAULT").IsNullOrWhiteSpace() && !a.Field<string>("STRUCTURE_NAME").IsNullOrWhiteSpace()).Select(rows => new
                   {
                       familyName = rows.Field<string>("FAMILY_NAME"),
                       structureName = rows.Field<string>("STRUCTURE_NAME"),
                       isDefault = rows.Field<string>("IS_DEFAULT").ToLower()
                   })
                    .Distinct().ToList();



                    string checkCatalogId = row["CATALOG_ID"].ToString();
                    string familyId = string.Empty;
                    string structureNamevalue = string.Empty;
                    string familyName = string.Empty;
                    string isDefault = row["IS_DEFAULT"].ToString();
                    string familyTableDesigner = row["FAMILY_TABLE_STRUCTURE"].ToString();

                    //---------------------------------------Excel sheet length validation--------------------------------------//
                    if (familyTableDesigner.Length > 32700)
                    {
                        familyTableDesigner = "";
                    }
                    //---------------------------------------Excel sheet length validation--------------------------------------//

                    Dictionary<string, string> ValueDictionaries = new Dictionary<string, string>();

                    string familyIdcolumnName = importDatatable.Columns["FAMILY_ID"].ColumnName.ToString();
                    string subFamilyIdcolumnName = importDatatable.Columns["SUBFAMILY_ID"].ColumnName.ToString();
                    string structureNamecolumnName = importDatatable.Columns["STRUCTURE_NAME"].ColumnName.ToString();
                    string familyNamecolumn = importDatatable.Columns["FAMILY_NAME"].ColumnName.ToString();

                    if (familyNamecolumn == "FAMILY_NAME")
                    {

                        string value = row["FAMILY_NAME"].ToString();
                        if (value != string.Empty)
                            ValueDictionaries.Add(familyNamecolumn, value);
                    }


                    if (familyIdcolumnName == "FAMILY_ID")
                    {
                        string value = row["FAMILY_ID"].ToString();
                        if (value != string.Empty)
                            ValueDictionaries.Add(familyIdcolumnName, value);

                    }

                    if (subFamilyIdcolumnName == "SUBFAMILY_ID")
                    {
                        string value = row["SUBFAMILY_ID"].ToString();
                        if (value != string.Empty)
                            ValueDictionaries.Add(subFamilyIdcolumnName, value);

                    }
                    if (structureNamecolumnName == "STRUCTURE_NAME")
                    {

                        string value = row["STRUCTURE_NAME"].ToString();
                        if (value != string.Empty)
                            ValueDictionaries.Add(structureNamecolumnName, value);
                    }




                    foreach (KeyValuePair<string, string> values in ValueDictionaries)
                    {


                        if (values.Key == "FAMILY_ID" || values.Key == "SUBFAMILY_ID")
                        {
                            familyId = values.Value.ToString();
                        }


                        if (values.Key == "FAMILY_NAME")
                        {
                            familyName = values.Value.ToString();
                        }

                        if (values.Key == "STRUCTURE_NAME")
                        {
                            structureNamevalue = values.Value.ToString();

                        }



                        if (familyId != string.Empty && structureNamevalue != string.Empty && familyName != string.Empty)
                        {
                            using (var objSqlConnection = new SqlConnection(sqlConn))
                            {

                                string checkColumnname = "FAMILY_NAME_IMPORT";
                                objSqlConnection.Open();
                                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                                objSqlCommand.CommandType = CommandType.StoredProcedure;
                                objSqlCommand.CommandTimeout = 0;
                                objSqlCommand.Connection = objSqlConnection;
                                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = checkCatalogId;
                                objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 1000).Value = "";
                                objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 1000).Value = "";
                                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = familyId ?? "";
                                objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 1000).Value = "";
                                objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = checkColumnname ?? "";
                                objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 1000).Value = structureNamevalue ?? "";
                                objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.NVarChar, 500).Value = "";
                                objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.NVarChar, -1).Value = "";
                                objSqlCommand.Parameters.Add("@Action", SqlDbType.NVarChar, 50).Value = "";
                                objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10).Value = "";
                                objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                                using (SqlDataReader rdr = objSqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                                {
                                    while (rdr.Read())
                                    {

                                        validateResults = rdr.GetString(rdr.GetOrdinal("Result"));

                                    }
                                    rdr.Close();
                                }

                            }
                            if (validateResults == "TRUE")
                            {

                                using (var objSqlConnection = new SqlConnection(sqlConn))
                                {
                                    string action = "UPDATE";
                                    objSqlConnection.Open();
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.CommandTimeout = 0;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = checkCatalogId;
                                    objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                                    objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = familyId ?? "";
                                    objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                    objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = "";
                                    objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 4000).Value = structureNamevalue ?? "";
                                    objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar, 500).Value = isDefault ?? "";
                                    objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar, -1).Value = familyTableDesigner ?? "";
                                    objSqlCommand.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action ?? "";
                                    objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                                    objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;

                                    int k = objSqlCommand.ExecuteNonQuery();
                                    if (k != 0)
                                    {

                                    }

                                    objSqlConnection.Close();
                                }
                                updatecount++;
                            }



                            if (validateResults == "FALSE")
                            {
                                using (var objSqlConnection = new SqlConnection(sqlConn))
                                {
                                    string action = "INSERT";
                                    objSqlConnection.Open();
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.CommandTimeout = 0;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = checkCatalogId;
                                    objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                                    objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = familyId ?? "";
                                    objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                    objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = "";
                                    objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 4000).Value = structureNamevalue ?? "";
                                    objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar, 500).Value = isDefault ?? "";
                                    objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar, -1).Value = familyTableDesigner ?? "";
                                    objSqlCommand.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action ?? "";
                                    objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                                    objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;

                                    int k = objSqlCommand.ExecuteNonQuery();
                                    if (k != 0)
                                    {

                                    }

                                    objSqlConnection.Close();
                                }
                                newInsertedcount++;
                            }

                        }
                    }


                }

                importResult = importResult + "~UpdateRecords:" + updatecount + "~InsertRecords:" + newInsertedcount;

                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;

                importResult = importResult + "~" + "TimeElapsed-" + ts.ToString().Split('.')[0];

            }

            else
            {
                try
                {
                    var customerSetting1 = objLS.Customer_Settings.Join(objLS.Customer_User, cs => cs.CustomerId, cu => cu.CustomerId, (cs, cu) => new { cs, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.cs.AllowDuplicateItem_PartNum).ToList();
                    if (customerSetting1.Count > 0)
                    {
                        allowDuplicate = customerSetting1[0].ToString().ToUpper() == "TRUE" ? "1" : "0";
                    }
                    if (batch == true)
                    {
                        FinishImportBatch(sessionId, SheetName, allowDuplicate, importType, catalogId, excelPath, scheduleDate, model);
                        return "Batch in Queue";
                    }
                    // Import Queue
                    int.TryParse(ImportPool.Instance.GetUsersDetail().User, out user);
                    if (user > 0)
                    {
                        stopWatch.Start();
                        progressBar.progressVale = "5";
                        List<string> importTypeProcess = LoadExcelSheet(SheetName, excelPath);

                        var customerSetting = objLS.Customer_Settings.Join(objLS.Customer_User, cs => cs.CustomerId, cu => cu.CustomerId, (cs, cu) => new { cs, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.cs.AllowDuplicateItem_PartNum).ToList();
                        if (customerSetting.Count > 0)
                            allowDuplicate = customerSetting[0].ToString().ToUpper() == "TRUE" ? "1" : "0";
                        if (!importType.ToUpper().Contains("SUBPRODUCT"))
                        {
                            if (SheetName == "FamilyProducts")
                            {
                                importResult = FinishProductImport(SheetName, allowDuplicate, excelPath, catalogId, "FAMILIES", templateId, model);
                                importResult = FinishProductImport(SheetName, allowDuplicate, excelPath, catalogId, "PRODUCTS", templateId, model);
                            }
                            else
                            {
                                importResult = FinishProductImport(SheetName, allowDuplicate, excelPath, catalogId, importType, templateId, model);
                            }
                            DataTable deletedRecords = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DELETE");

                            // To delete the coloums for pdf xpress 
                            if (deletedRecords != null)
                            {
                                if (deletedRecords.Columns.Contains("Catalog_PdfTemplate"))
                                {
                                    deletedRecords.Columns.Remove("Catalog_PdfTemplate");
                                    deletedRecords.Columns.Remove("Category_PdfTemplate");
                                    deletedRecords.Columns.Remove("Family_PdfTemplate");
                                    deletedRecords.Columns.Remove("Product_PdfTemplate");

                                }
                            }

                            if (deletedRecords != null && deletedRecords.Rows.Count > 0)
                            {
                                string deleteCount = DeleteRecords(SheetName, allowDuplicate, excelPath, importType, catalogId, model);
                                importResult = importResult + "~DeletedRecords:" + (deleteCount != "" ? deleteCount != null ? deleteCount : "0" : "0");
                            }
                            else
                            {
                                importResult = importResult + "~DeletedRecords:" + "0";
                            }
                            //---------------------------------------Detach ------------------------------------------
                            DataTable detachRecords = GetDataFromExcelForDetach(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DETACH");

                            if (detachRecords != null && detachRecords.Rows.Count > 0)
                            {
                                string detachCount = DetachRecords(SheetName, allowDuplicate, excelPath, importType, catalogId, model);
                                importResult = importResult + "~DetachedRecords:" + (detachCount != "" ? detachCount != null ? detachCount : "0" : "0");
                            }
                            else
                            {
                                importResult = importResult + "~DetachedRecords:" + "0";
                            }
                            //---------------------------------------Detach ------------------------------------------
                        }
                        else if (importType.ToUpper().Contains("SUBPRODUCT"))
                        {
                            importResult = FinishSubProductImport(SheetName, allowDuplicate, excelPath, catalogId, importType, model, templateId);
                            DataTable deletedRecords = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DELETE");
                            if (deletedRecords != null && deletedRecords.Rows.Count > 0)
                            {
                                importResult = importResult + "~DeletedRecords:" + DeleteRecords(SheetName, allowDuplicate, excelPath, importType, catalogId, model);
                            }
                            else
                            {
                                importResult = importResult + "~DeletedRecords:" + "0";
                            }
                        }
                        stopWatch.Stop();
                        TimeSpan ts = stopWatch.Elapsed;

                        importResult = importResult + "~" + "TimeElapsed-" + ts.ToString().Split('.')[0];
                    }



                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        var sqlString = "EXEC('delete from QS_IMPORTMAPPING')";
                        SqlCommand sqlCommand = new SqlCommand(sqlString, con);
                        sqlCommand.ExecuteNonQuery();
                        con.Dispose();
                    }


                }
                catch (Exception ex)
                {
                    Message = "Import Failed, please try again";
                    _logger.Error("Error at AdvanceImportApiController : finishImport", ex);
                    return null;
                }
                finally
                {
                    if (user > 0)
                        ImportPool.Instance.ReleaseUserDetail(user.ToString(CultureInfo.InvariantCulture));
                }
            }

            return importResult;
        }

        public string CreateTable(string tableName, DataTable objtable)
        {
            try
            {
                string sqlsc = "CREATE TABLE " + tableName + "(";
                for (int i = 0; i < objtable.Columns.Count; i++)
                {
                    if (!objtable.Columns[i].ColumnName.Contains('['))
                    {
                        sqlsc += "\n [" + objtable.Columns[i].ColumnName + "] ";
                    }
                    else
                    {
                        sqlsc += "\n" + objtable.Columns[i].ColumnName + "";
                    }
                    if (objtable.Columns[i].ColumnName.Contains("CATEGORY_ID"))
                        sqlsc += "nvarchar(50) ";
                    else if (objtable.Columns[i].DataType.ToString().Contains("System.String") ||
                             objtable.Columns[i].ColumnName.Contains("SUBCATID") ||
                             objtable.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                        sqlsc += "nvarchar(max) ";
                    else
                        sqlsc += "nvarchar(max) ";
                    sqlsc += ",";
                }
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    var sqlString = "EXEC('delete from QS_IMPORTMAPPING')";
                    SqlCommand sqlCommand = new SqlCommand(sqlString, con);
                    sqlCommand.ExecuteNonQuery();
                    con.Dispose();
                }

                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CreateTable : Program - ", ex);
                throw;
            }
        }
        public string CreateTableToImport(string tableName, DataTable table)
        {
            try
            {
                string sqlsc = "CREATE TABLE " + tableName + "(";
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    if (!table.Columns[i].ColumnName.Contains('['))
                    {
                        sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                    }
                    else
                    {
                        sqlsc += "\n" + table.Columns[i].ColumnName + "";
                    }
                    if (table.Columns[i].ColumnName.Contains("CATEGORY_ID"))
                        sqlsc += "nvarchar(50) ";
                    else if (table.Columns[i].DataType.ToString().Contains("System.String") ||
                             table.Columns[i].ColumnName.Contains("SUBCATID") ||
                             table.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                        sqlsc += "varchar(max) ";
                    else
                        sqlsc += "varchar(max) ";
                    sqlsc += ",";
                }
                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CreateTableToImport : Program - ", ex);
                throw;
            }
        }

        public DataTable AddIdColumnsForTableDesigner(DataTable dtGetTableDesigner, string importType)
        {
            if (dtGetTableDesigner == null || dtGetTableDesigner.Rows.Count == 0)
            {
                return dtGetTableDesigner;
            }
            if (!dtGetTableDesigner.Columns.Contains("CATALOG_ID"))
            {
                dtGetTableDesigner.Columns.Add("CATALOG_ID");
                dtGetTableDesigner.Columns["CATALOG_ID"].SetOrdinal(0);
            }
            if (!dtGetTableDesigner.Columns.Contains("CATALOG_NAME"))
            {
                dtGetTableDesigner.Columns.Add("CATALOG_NAME");
                dtGetTableDesigner.Columns["CATALOG_NAME"].SetOrdinal(1);
            }
            if (!dtGetTableDesigner.Columns.Contains("CATEGORY_ID"))
            {
                dtGetTableDesigner.Columns.Add("CATEGORY_ID");
                dtGetTableDesigner.Columns["CATEGORY_ID"].SetOrdinal(dtGetTableDesigner.Columns.IndexOf("CATEGORY_NAME"));

            }
            int col = 1;
            for (int colindex = 0; colindex < dtGetTableDesigner.Columns.Count; colindex++)
            {
                if (dtGetTableDesigner.Columns[colindex].ColumnName.ToUpper().Contains("SUBCATNAME"))
                {
                    if (!dtGetTableDesigner.Columns.Contains("SUBCATID_L" + col))
                    {
                        dtGetTableDesigner.Columns.Add("SUBCATID_L" + col);
                        if (dtGetTableDesigner.Columns.Contains("SUBCATNAME_L" + col))
                        {
                            dtGetTableDesigner.Columns["SUBCATID_L" + col].SetOrdinal(dtGetTableDesigner.Columns.IndexOf("SUBCATNAME_L" + col));
                        }
                        colindex = colindex + 1;
                    }
                    if (dtGetTableDesigner.Columns.Contains("SUBCATID_L" + col))
                    {
                        col = col + 1;
                    }
                }
            }


            if (!dtGetTableDesigner.Columns.Contains("FAMILY_ID"))
            {
                dtGetTableDesigner.Columns.Add("FAMILY_ID");
                if (dtGetTableDesigner.Columns.Contains("FAMILY_NAME"))
                {
                    dtGetTableDesigner.Columns["FAMILY_ID"].SetOrdinal(dtGetTableDesigner.Columns.IndexOf("FAMILY_NAME"));
                }

            }
            if (!dtGetTableDesigner.Columns.Contains("SUBFAMILY_ID"))
            {
                dtGetTableDesigner.Columns.Add("SUBFAMILY_ID");
                if (dtGetTableDesigner.Columns.Contains("FAMILY_NAME"))
                {
                    dtGetTableDesigner.Columns["SUBFAMILY_ID"].SetOrdinal(dtGetTableDesigner.Columns.IndexOf("FAMILY_NAME") + 1);
                }

            }
            if (!dtGetTableDesigner.Columns.Contains("SUBFAMILY_NAME"))
            {
                dtGetTableDesigner.Columns.Add("SUBFAMILY_NAME");
                dtGetTableDesigner.Columns["SUBFAMILY_NAME"].SetOrdinal(dtGetTableDesigner.Columns.IndexOf("SUBFAMILY_ID") + 1);

            }

            return dtGetTableDesigner;
        }

        public DataSet CreateDataSet(string sqlString)
        {

            var dsReturn = new DataSet();
            using (con = new SqlConnection(connectionString))
            {
                var dbAdapter = new SqlDataAdapter(sqlString, con);
                dbAdapter.SelectCommand.CommandTimeout = 0;
                dbAdapter.Fill(dsReturn);
                dbAdapter.Dispose();
                return dsReturn;
            }
        }

        public string DeleteRecords(string SheetName, string allowDuplicate, string excelPath, string importType, int catalogId, JArray model)
        {
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            List<object> newList = new List<object>();
            DataSet dsGetProdCnt = new DataSet();
            int importProductCount = 0;
            int userProductCount = 0;
            int userSKUProductCount = 0;
            try
            {
                var customerId = 0;
                var skucnt = objLS.TB_PLAN
                   .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                   .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                var SKUcount = skucnt.Select(a => a).ToList();
                customerId = SKUcount[0].CustomerId;

                DataTable dtGetProdCnt = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DELETE");
                if (dtGetProdCnt == null || dtGetProdCnt.Rows.Count == 0)
                {
                    return "0";
                }

                importExcelSheetSelection(excelPath, SheetName);
                DataTable deletedRecords = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DELETE");
                DataTable excelData = new DataTable();
                ImportApiController importApiController = new ImportApiController();

                if (importType.ToUpper().Contains("SUBPRODUCT") && deletedRecords.Columns.Contains("SUBITEM#"))
                {
                    deletedRecords.Columns["SUBITEM#"].ColumnName = "SUBCATALOG_ITEM_NO";
                }
                if (importType.ToUpper().Contains("SUBPRODUCT"))
                {
                    var customerDetails = objLS.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeSubItemNo).FirstOrDefault().ToString();
                    if (deletedRecords.Columns.Contains(customerDetails))
                    {
                        deletedRecords.Columns[customerDetails].ColumnName = "SUBCATALOG_ITEM_NO";
                    }
                    excelData = deletedRecords.DefaultView.ToTable(false, "SUBCATALOG_ITEM_NO");
                    excelData.Columns.Add("REMOVE_PRODUCT");
                    excelData.Select("SUBCATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_PRODUCT"] = "YES");
                }
                else if (importType.ToUpper().Contains("CATEGORIES"))
                {
                    var customerDetails = objLS.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();
                    AddIdColumns(deletedRecords, importType);
                    if (deletedRecords.Columns.Contains(customerDetails))
                    {
                        deletedRecords.Columns[customerDetails].ColumnName = "CATEGORY_ID";
                    }
                    excelData = deletedRecords.DefaultView.ToTable(false, "CATEGORY_ID");
                    excelData.Columns.Add("REMOVE_CATEGORY");
                    excelData.Select("CATEGORY_ID<>''").ToList<DataRow>().ForEach(x => x["REMOVE_CATEGORY"] = "YES");
                }
                else if (importType.ToUpper().Contains("FAMILIES"))
                {
                    //-----------------------------------Family deletion ----------------------------------------------------------
                    int xml_id = 0;
                    int family_id = 0;
                    string productID = "";
                    SqlConnection _SQLCon = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                    DataSet recycleDS1 = new DataSet();
                    SqlCommand cmd = null;
                    _SQLCon.Open();
                    Random random = new Random();
                    xml_id = random.Next(1, 10000);
                    string sqlstr11 = "Delete from TB_RECYCLE_TABLE where 1=2";

                    var customerDetails = objLS.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();
                    AddIdColumns(deletedRecords, importType);
                    excelData.Columns.Add("CATALOG_ID");
                    if (deletedRecords.Columns.Contains(customerDetails))
                    {
                        deletedRecords.Columns[customerDetails].ColumnName = "FAMILY_ID";
                    }
                    excelData = deletedRecords.DefaultView.ToTable(false, "FAMILY_ID", "FAMILY_NAME", "CATALOG_ID");

                    foreach (DataRow dr in excelData.Rows)
                    {
                        string fam_name = dr["FAMILY_NAME"].ToString();
                        if (dr["FAMILY_ID"].ToString() == "")
                        {
                            deletedRecords.Clear();
                            return deletedRecords.Rows.Count.ToString();
                            //var famid = _dbcontext.TB_FAMILY.Join(_dbcontext.TB_CATALOG_FAMILY, ta => ta.FAMILY_ID, ca => ca.FAMILY_ID, (ta, ca) => new { ta, ca }).Where(x => x.ta.FAMILY_NAME.Equals(fam_name) && x.ca.CATALOG_ID == catalogId).Select(z => z.ta.FAMILY_ID).FirstOrDefault();
                            //dr["FAMILY_ID"] = famid;
                        }
                        var famid = dr["FAMILY_ID"];
                        //else
                        //{
                        //    var famid = dr["FAMILY_ID"];
                        //}
                        dr["CATALOG_ID"] = catalogId;
                        var famids = dr["FAMILY_ID"];
                        family_id = Convert.ToInt32(famids);
                        var categoryID = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID.Equals(family_id)).Select(c => c.CATEGORY_ID).FirstOrDefault();
                        sqlstr11 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,[CATALOG_ID],[CATEGORY_ID],[CATEGORY_NAME],[FAMILY_ID],[FAMILY_NAME],[SUB_FAMILY_ID],[SUB_FAMILY_NAME],[PRODUCT_ID],[PRODUCT_NAME],[XML_DOC],[DELETED_USER] ,[CATEGORY_SHORT], [RECYCLE_TYPE]) " +
                                   "VALUES('" + xml_id + "'," + catalogId + ", '" + categoryID + "', '', " + family_id + ", '" + fam_name + "',  '0', '','" + productID + "','','', '" + User.Identity.Name + "','' , 'Family' ) ";
                        cmd = new SqlCommand(sqlstr11, _SQLCon);
                        cmd.ExecuteNonQuery();
                    }
                    _SQLCon.Close();
                    //-----------------------------------Family deletion ----------------------------------------------------------
                    excelData.Columns.Add("REMOVE_FAMILY");
                    excelData.Select("FAMILY_ID<>''").ToList<DataRow>().ForEach(x => x["REMOVE_FAMILY"] = "YES");
                }
                else
                {
                    int xml_id = 0;
                    SqlConnection _SQLCon = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                    SqlCommand cmd = null;
                    Random random = new Random();
                    xml_id = random.Next(1, 10000);
                    _SQLCon.Open();
                    var customerDetails = objLS.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();
                    if (deletedRecords.Columns.Contains(customerDetails))
                    {
                        deletedRecords.Columns[customerDetails].ColumnName = "CATALOG_ITEM_NO";
                    }

                    if (deletedRecords.Columns.Contains("PRODUCT_ID"))
                    {
                        var productIdaval = deletedRecords.Select("Product_id<>'' and  Product_id is not null");
                        if (productIdaval.Length == deletedRecords.Rows.Count)
                        {

                            excelData = deletedRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "PRODUCT_ID", "CATALOG_ID");
                            excelData.Columns.Add("REMOVE_PRODUCT");
                            excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_PRODUCT"] = "YES");
                            foreach (DataRow dr in excelData.Rows)
                            {
                                if (dr["PRODUCT_ID"].ToString() == "")
                                {
                                    deletedRecords.Clear();
                                    return deletedRecords.Rows.Count.ToString();

                                }
                                dr["CATALOG_ID"] = catalogId;
                                var Prodid = Convert.ToInt32(dr["PRODUCT_ID"]);
                                var ProdName = (dr["CATALOG_ITEM_NO"].ToString());
                                var FAMILY_Id = _dbcontext.TB_PROD_FAMILY.Where(x => x.PRODUCT_ID == Prodid).Select(c => c.FAMILY_ID).FirstOrDefault();
                                var categoryID = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID.Equals(FAMILY_Id)).Select(c => c.CATEGORY_ID).FirstOrDefault();
                                string sqlstr11 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,[CATALOG_ID],[CATEGORY_ID],[CATEGORY_NAME],[FAMILY_ID],[FAMILY_NAME],[SUB_FAMILY_ID],[SUB_FAMILY_NAME],[PRODUCT_ID],[PRODUCT_NAME],[XML_DOC],[DELETED_USER] ,[CATEGORY_SHORT], [RECYCLE_TYPE]) " +
                                          "VALUES('" + xml_id + "'," + catalogId + ", '" + categoryID + "', '', " + FAMILY_Id + ", '',  '0', '','" + Prodid + "','" + ProdName + "','', '" + User.Identity.Name + "','' , 'Product' ) ";
                                cmd = new SqlCommand(sqlstr11, _SQLCon);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                    else
                    {
                        excelData = deletedRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO");
                        excelData.Columns.Add("REMOVE_PRODUCT");
                        excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_PRODUCT"] = "YES");
                    }

                    _SQLCon.Close();
                }
                DataTable oattType = new DataTable();
                DataSet replace = new DataSet();

                int ItemVal = Convert.ToInt32(allowDuplicate);
                if (deletedRecords.Rows.Count == 0)
                {
                    return deletedRecords.Rows.Count.ToString();
                }
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    if (importType.ToUpper().Contains("SUBPRODUCT"))
                    {

                        SQLString =
                       "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPRODUCTIMPORTTEMP" +
                       importTemp + "]'')BEGIN DROP TABLE [##SUBPRODUCTIMPORTTEMP" + importTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                        _DBCommand.ExecuteNonQuery();
                        SQLString = importController.CreateTable("[##SUBPRODUCTIMPORTTEMP" + importTemp + "]", excelPath, SheetName, excelData);
                        SqlCommand _DBCommandnew = new SqlCommand(SQLString, conn);
                        _DBCommandnew.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##SUBPRODUCTIMPORTTEMP" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(excelData);

                        oattType = importController.SelectedColumnsToImport(deletedRecords, model);
                        var cmd1 =
                            new SqlCommand(
                                "ALTER TABLE [##SUBPRODUCTIMPORTTEMP" + importTemp +
                                "] ADD STATUS nvarchar(20)",
                                conn);
                        cmd1.ExecuteNonQuery();

                        var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_SUBPRODUCTIMPORT ''" + importTemp + "'',''" + ItemVal + "'',''" + customerId + "'',''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                        cmd.ExecuteNonQuery();
                        importController._SQLString = @"select * from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                        var ds = importController.CreateDataSet();
                        importController._SQLString = @"select * into [tempresult" + importTemp + "] from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                        cmd.CommandText = importController._SQLString;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                    else if (importType.ToUpper().Contains("CATEGORIES"))
                    {

                        SQLString =
                       "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                       importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                        _DBCommand.ExecuteNonQuery();
                        SQLString = importController.CreateTable("[##IMPORTTEMP" + importTemp + "]", excelPath, SheetName, excelData);
                        SqlCommand _DBCommandnew = new SqlCommand(SQLString, conn);
                        _DBCommandnew.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(excelData);

                        oattType = importController.SelectedColumnsToImport(deletedRecords, model);
                        var cmd1 =
                            new SqlCommand(
                                "ALTER TABLE [##IMPORTTEMP" + importTemp +
                                "] ADD STATUS nvarchar(20)",
                                conn);
                        cmd1.ExecuteNonQuery();


                        var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_CATEGORYATTRIBUTEIMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                        cmd.ExecuteNonQuery();
                        importController._SQLString = @"select * from [##LOGTEMPTABLE" + importTemp + "]";
                        DataSet ds = importController.CreateDataSet();
                        //importController._SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp +
                        //                "]";
                        cmd1.CommandText = importController._SQLString;
                        cmd1.CommandType = CommandType.Text;
                        cmd1.ExecuteNonQuery();

                    }
                    else
                    {
                        SQLString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                        _DBCommand.ExecuteNonQuery();
                        SQLString = importController.CreateTable("[##IMPORTTEMP" + importTemp + "]", excelPath, SheetName, excelData);
                        SqlCommand _DBCommandnew = new SqlCommand(SQLString, conn);
                        _DBCommandnew.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(excelData);

                        oattType = importController.SelectedColumnsToImport(deletedRecords, model);
                        var cmd1 =
                            new SqlCommand(
                                "ALTER TABLE [##IMPORTTEMP" + importTemp +
                                "] ADD STATUS nvarchar(20)",
                                conn);
                        cmd1.ExecuteNonQuery();




                        var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                        cmd.ExecuteNonQuery();
                        importController._SQLString = @"select * from [##LOGTEMPTABLE" + importTemp + "]";
                        DataSet ds = importController.CreateDataSet();
                        importController._SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp +
                                        "]";
                        cmd1.CommandText = importController._SQLString;
                        cmd1.CommandType = CommandType.Text;
                        cmd1.ExecuteNonQuery();
                    }
                    return deletedRecords.Rows.Count.ToString();
                }

            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at AdvanceImportApiController : FinishProductImport", ex);
                return null;
            }
        }

        public OleDbConnection excelConnection(string excelPath)
        {

            bool hasHeaders = true;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                          ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            else
                //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                         ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            return conn;
        }

        public List<string> LoadExcelSheet(string SheetName, string excelPath)
        {
            List<string> importTypeProcess = new List<string>();
            DataTable dtexcel = new DataTable();
            try
            {
                OleDbConnection conn = new OleDbConnection();
                using (conn = excelConnection(excelPath))
                {
                    if (!SheetName.EndsWith("_"))
                    {
                        string query = "SELECT  * FROM [" + SheetName + "$]";
                        OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                        dtexcel.Locale = CultureInfo.CurrentCulture;
                        daexcel.Fill(dtexcel);

                    }
                }
                if (dtexcel != null && dtexcel.Columns.Count > 3)
                {
                    if (dtexcel.Rows.Count > 0)
                    {
                        if (dtexcel.Rows[0][0].ToString().ToUpper() == "ACTION")
                        {

                            if (string.IsNullOrEmpty(dtexcel.Columns[1].ToString()))
                                importTypeProcess.Add(dtexcel.Columns[1].ToString());

                            if (string.IsNullOrEmpty(dtexcel.Columns[3].ToString()))
                                importTypeProcess.Add(dtexcel.Columns[3].ToString());

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : LoadExcelSheet", ex);
                return null;
            }
            return importTypeProcess;
        }

        public IList importExcelSheetSelection(string excelPath, string SheetName)
        {
            _dsSheets = new DataSet();
            var dt = new DataTable();
            DataTable dtexcel = new DataTable();
            List<object> newList = new List<object>();
            DataTable newexcelSheet = new DataTable();
            OleDbConnection conn = new OleDbConnection();
            try
            {
                if (!string.IsNullOrEmpty(excelPath))
                {
                    using (conn = excelConnection(excelPath))
                    {
                        DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,
                            new object[] { null, null, null, "TABLE" });
                        int jk = 0;
                        int sheetCount = schemaTable.Rows.Count;
                        int sheetColumnCount = 0;
                        if (!SheetName.Contains("$"))
                        {
                            SheetName = SheetName + "$";
                        }
                        foreach (DataRow rows in schemaTable.Rows)
                        {
                            ImportSheetName objImportSheetName = new ImportSheetName();
                            objImportSheetName.TABLE_NAME = Convert.ToString(rows[2]);
                            objImportSheetName.SHEET_PATH = excelPath;
                            newList.Add(objImportSheetName);
                            jk++;
                        }

                        for (int i = 0; i < sheetCount; i++)
                        {
                            dt = new DataTable(newList[i].ToString());

                            var dc = new DataColumn("ExcelColumn", typeof(string));
                            dt.Columns.Add(dc);

                            dc = new DataColumn("CatalogField", typeof(string));
                            dt.Columns.Add(dc);

                            dc = new DataColumn("SelectedToImport", typeof(bool));
                            dt.Columns.Add(dc);

                            dc = new DataColumn("IsSystemField", typeof(bool));
                            dt.Columns.Add(dc);

                            dc = new DataColumn("FieldType", typeof(string));
                            dt.Columns.Add(dc);

                            dtexcel = new DataTable();
                            DataRow schemaRow = schemaTable.Rows[0];
                            string sheet = schemaRow["TABLE_NAME"].ToString();
                            if (!sheet.EndsWith("_"))
                            {
                                string query = "SELECT  * FROM [" + SheetName + "]";
                                OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                                dtexcel.Locale = CultureInfo.CurrentCulture;
                                daexcel.Fill(dtexcel);
                            }
                            newexcelSheet = SetDataTable(dtexcel, "");
                            sheetColumnCount = newexcelSheet.Columns.Count;

                        }
                        for (int j = 0; j < sheetColumnCount; j++)
                        {
                            if (newexcelSheet.Columns[j].ToString() != null)
                            {
                                String sColName = newexcelSheet.Columns[j].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                DataRow dr = dt.NewRow();
                                dr[0] = sColName;
                                dt.Rows.Add(dr);
                            }
                        }
                        _dsSheets.Tables.Add(dt);
                        importController.GetExcelColumn2CatalogMapping(SheetName);
                        dt.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading importExcelSheetSelection in Import Controller", objException);
                conn.Close();
                conn.Dispose();
                return null;
            }

            return newList;
        }

        public DataTable SetDataTableImport(DataTable oldSheet, string deleteflag)
        {
            DataTable newSheet = new DataTable();
            try
            {
                if (oldSheet.Columns[0].ColumnName.ToString().ToUpper() == "ACTION")
                {
                    foreach (DataRow dtrow in oldSheet.Rows)
                    {
                        DataRow drow = newSheet.NewRow();
                        for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                        {
                            drow[exl] = dtrow.ItemArray[exl];
                        }
                        newSheet.Rows.Add(drow);
                    }
                }
                else
                {
                    foreach (DataRow dtrow in oldSheet.Rows)
                    {
                        if (dtrow.ItemArray[0].ToString().ToUpper() == "ACTION")
                        {
                            foreach (string colName in dtrow.ItemArray)
                            {
                                if (colName.ToUpper() != "ACTION" && !newSheet.Columns.Contains(colName))
                                    newSheet.Columns.Add(colName);
                                else if (deleteflag == "Valid")
                                    newSheet.Columns.Add(colName);
                            }
                        }
                        else
                        {
                            DataRow drow = newSheet.NewRow();
                            for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                drow[exl] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading importExcelSheetSelection in Import Controller", objException);
                return null;
            }
            return newSheet;
        }

        public DataTable SetDataTable(DataTable oldSheet, string deleteflag)
        {
            DataTable newSheet = new DataTable();
            try
            {

                if (oldSheet.Columns[0].ColumnName.ToString().ToUpper() == "ACTION")
                {
                    foreach (DataColumn dcol in oldSheet.Columns)
                    {
                        if (dcol.ColumnName.ToString().ToUpper() != "ACTION" && !newSheet.Columns.Contains(dcol.ColumnName))
                            newSheet.Columns.Add(dcol.ColumnName);
                    }
                    foreach (DataRow dtrow in oldSheet.Rows)
                    {
                        DataRow drow = newSheet.NewRow();
                        if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && deleteflag == "")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (deleteflag == "Valid")
                        {
                            for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count - 1 < exl)
                                {
                                    break;
                                }
                                drow[exl] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() == "DELETE" && deleteflag == "DELETE")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() == "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && dtrow[0].ToString().ToUpper() != "" && deleteflag == "SKIP")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                    }
                    return newSheet;
                }
                foreach (DataRow dtrow in oldSheet.Rows)
                {
                    if (dtrow.ItemArray[0].ToString().ToUpper() == "ACTION")
                    {
                        foreach (var colName in dtrow.ItemArray)
                        {
                            if (colName.ToString().ToUpper() != "ACTION" && !newSheet.Columns.Contains(colName.ToString()) && !string.IsNullOrEmpty(colName.ToString()))
                                newSheet.Columns.Add(colName.ToString());
                            else if (deleteflag == "Valid" && !string.IsNullOrEmpty(colName.ToString()))
                                newSheet.Columns.Add(colName.ToString());
                        }

                    }
                    else
                    {
                        DataRow drow = newSheet.NewRow();

                        if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && deleteflag == "")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (deleteflag == "Valid")
                        {
                            for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count - 1 < exl)
                                {
                                    break;
                                }
                                drow[exl] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() == "DELETE" && deleteflag == "DELETE")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() == "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && dtrow[0].ToString().ToUpper() != "" && deleteflag == "SKIP")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                    }
                }

            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading SetDataTable in AdvanceImportApiController", objException);
                return null;
            }
            return newSheet;
        }

        public DataTable GetDataFromExcel(string SheetName, string excelPath, string query, string deleteflag)
        {
            DataTable dtable1 = new DataTable();
            _dsSheets = new DataSet();
            int ImportTemplateId = Convert.ToInt32(System.Web.HttpContext.Current.Session["ImportTemplate"]);
            dtable1 = importSelectionColumnGrid(SheetName, excelPath, ImportTemplateId);
            if (dtable1 != null)
            {
                DataRow dr;
                dr = dtable1.NewRow();
                dr[0] = "ACTION";
                dtable1.Rows.InsertAt(dr, 0);
            }

            DataTable dtexcel = new DataTable();
            DataTable dtexcelNew = new DataTable();
            try
            {
                var fileName = Path.GetFileName(excelPath).Split('.')[0];
                var path = Path.GetFullPath(excelPath);
                var fileExtention = Path.GetExtension(excelPath);
                string excelPathCopy = path.Replace(Path.GetFileName(excelPath), "") + "//" + fileName + "_Copy" + "." + fileExtention;
                using (OleDbConnection conn = excelConnection(excelPath))
                {
                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    DataTable dtColumns = new DataTable();
                    try
                    {
                        DataRow[] schemaRow = schemaTable.Select("TABLE_NAME like '" + SheetName + "%'");
                        string sheet = schemaRow[0]["TABLE_NAME"].ToString();
                        int selectedIndex = 0;
                        foreach (DataRow dRow in schemaTable.Rows)
                        {

                            if (sheet.ToUpper() == dRow["TABLE_NAME"].ToString().ToUpper())
                                break;
                            selectedIndex++;
                        }

                        DataTable dt_checkHeaderColumns = new DataTable();
                        if (!sheet.EndsWith("_"))
                        {
                            OleDbDataAdapter da_checkHeaderexcel = new OleDbDataAdapter(query, conn);
                            dt_checkHeaderColumns.Locale = CultureInfo.CurrentCulture;


                            da_checkHeaderexcel.Fill(dt_checkHeaderColumns);
                            if (dtable1 != null)
                            {

                                for (int columncount = 1; columncount <= dt_checkHeaderColumns.Columns.Count - 1; columncount++)
                                {

                                    dt_checkHeaderColumns.Columns[columncount].ColumnName = dtable1.Rows[columncount][0].ToString();

                                }
                            }

                        }




                        if (dt_checkHeaderColumns.Rows.Count > 0)
                        {

                            var categoryIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                           .Select((c, i) => new { c, i })
                           .Where(x => x.c.ToString().Equals("CATEGORY_NAME"))
                           .Select(x => x.i)
                           .ToArray();


                            var familyIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                          .Select((c, i) => new { c, i })
                          .Where(x => x.c.ToString().Equals("FAMILY_NAME"))
                          .Select(x => x.i)
                          .ToArray();

                            CSEntities objLS = new CSEntities();


                            int customerId = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => x.CustomerId).FirstOrDefault();

                            string displayItemnumber = objLS.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();


                            var displayItemnumberIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                               .Select((c, i) => new { c, i })
                               .Where(x => x.c.ToString().Equals(displayItemnumber))
                               .Select(x => x.i)
                               .ToArray();

                            var itemNumberIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                             .Select((c, i) => new { c, i })
                             .Where(x => x.c.ToString().Equals("ITEM#"))
                             .Select(x => x.i)
                             .ToArray();


                            if (categoryIndex.Count() > 0 || familyIndex.Count() > 0 || displayItemnumberIndex.Count() > 0 || itemNumberIndex.Count() > 0)
                            {
                                int columnsCount = dt_checkHeaderColumns.Columns.Count;
                                string str_Columnname = ExcelColumnFromNumber(columnsCount);
                                query = "select * from [" + SheetName + "$A2:" + str_Columnname + "65000]";
                            }

                        }



                        if (!sheet.EndsWith("_"))
                        {
                            OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                            dtexcel.Locale = CultureInfo.CurrentCulture;



                            daexcel.Fill(dtColumns);
                            if (dtable1 != null)
                            {
                                for (int columncount = 1; columncount <= dtColumns.Columns.Count - 1; columncount++)
                                {

                                    dtColumns.Columns[columncount].ColumnName = dtable1.Rows[columncount][0].ToString();

                                }
                            }



                        }
                        conn.Close();
                        conn.Dispose();
                        var workbook1 = new Workbook();

                        workbook1 = Workbook.Load(excelPath);


                        for (int i = 0; i < workbook1.Worksheets.Count; i++)
                        {
                            if (workbook1.Worksheets[i].Name.ToUpper() == SheetName.ToUpper())
                            {
                                selectedIndex = i;

                            }
                        }


                        //foreach (WorksheetColumn colSheet in workbook1.Worksheets[1].Columns)
                        //    dt11.Columns.Add(colSheet.ToString());
                        dtexcel = dtColumns.Clone();
                        bool skipFirstRow = false;



                        if (!deleteflag.ToUpper().Contains("DELETE"))
                        {

                            foreach (WorksheetRow rowSheet in workbook1.Worksheets[selectedIndex].Rows)
                            {




                                if (dtColumns.Columns.Contains("ACTION") && !skipFirstRow)
                                {
                                    skipFirstRow = true;
                                    continue;
                                }
                                DataRow dRow = dtexcel.NewRow();

                                for (int col = 0; col < dtexcel.Columns.Count; col++)
                                {

                                    if (rowSheet.Cells[col].Value == null)
                                    {
                                        dRow[col] = DBNull.Value;
                                    }
                                    //else if(rowSheet.Index==0 &&  (categoryIndex.Count()>0 || familyIndex.Count() > 0 || displayItemnumberIndex.Count() > 0 || itemNumberIndex.Count() > 0))
                                    //{
                                    //    dRow[col] = DBNull.Value;
                                    //}
                                    else
                                    {
                                        dRow[col] = rowSheet.Cells[col].Value.ToString();
                                    }


                                }
                                //new
                                if (dRow.ItemArray[0].ToString().ToUpper() != "ACTION")
                                {

                                    dtexcel.Rows.Add(dRow);
                                }
                            }
                        }
                        else
                        {
                            dtexcel = new DataTable();
                            dtexcel = dtColumns;
                            objLS = new CSEntities();
                        }
                        workbook1.Worksheets.Clear();
                    }
                    catch (Exception ex)
                    {
                        dtexcel = new DataTable();
                        dtexcel = dtColumns;
                    }
                }

                //Write the validation code for without import subfamily id and subfamily_name
                //bool checkSubFamily = true;
                //if (dtexcel.Columns.Count != 0)
                //{                  
                //    foreach (DataColumn dc in dtexcel.Columns)
                //    {
                //        if (dc.ColumnName.ToUpper()!="FAMILY_NAME" && (dc.ColumnName.ToUpper() == "SUBFAMILY_ID" || dc.ColumnName.ToUpper() == "SUBFAMILY_NAME") )
                //        {
                //            checkSubFamily = false;
                //        }                       
                //    }
                //    if(checkSubFamily)
                //    {
                //        dtexcel.Columns.Add("SUBFAMILY_ID");
                //        dtexcel.Columns.Add("SUBFAMILY_NAME");
                //    }
                //}


                if (dtexcel.Columns[0].ColumnName.ToString().ToUpper() == "ACTION" && deleteflag.ToUpper() == "VALID")
                {
                    dtexcelNew = dtexcel;
                    goto Skip;
                }
                if ((dtexcel == null || dtexcel.Rows.Count == 0 || dtexcel.Rows.Count == 1) && deleteflag.ToUpper() != "SKIP" && deleteflag.ToUpper() != "DELETE")
                {
                    return dtexcel;
                }
                dtexcelNew = SetDataTable(dtexcel, deleteflag);
                Skip:

                var tbattrcol = objLS.TB_ATTRIBUTE.Find(1);
                Workbook workbook;
                try
                {
                    workbook = Workbook.Load(excelPath);
                    string sheetNameworkbook = SheetName.Replace("$", "").TrimEnd();
                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells.Count();
                    if (deleteflag == "Valid")
                    {
                        for (int i = 0; i < dtexcelNew.Columns.Count; i++)
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value != null)
                            {
                                String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i].ColumnName != sColName && sColName.ToUpper() != "ACTION" && !dtexcelNew.Columns.Contains(sColName))
                                {
                                    dtexcelNew.Columns[i].ColumnName = sColName;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= dtexcelNew.Columns.Count; i++)
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value != null)
                            {
                                String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i - 1].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i - 1].ColumnName != sColName && !dtexcelNew.Columns.Contains(sColName))
                                {
                                    dtexcelNew.Columns[i - 1].ColumnName = sColName;
                                }
                            }
                        }
                    }
                }
                catch (Exception objException)
                {
                    if (deleteflag == "Valid")
                    {
                        for (int i = 0; i < dtexcelNew.Columns.Count; i++)
                        {
                            if (dtexcel.Rows[0][i].ToString() != null)
                            {
                                String sColName = dtexcel.Rows[0][i].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i].ColumnName != sColName && sColName.ToUpper() != "ACTION")
                                {
                                    dtexcelNew.Columns[i].ColumnName = sColName;
                                }
                            }
                        }
                    }
                    else
                    {
                        var validCheck = SheetName.ToUpper().Contains("FAM") ? dtexcelNew.Columns.Contains("FAMILY_ID") : dtexcelNew.Columns.Contains("PRODUCT_ID");
                        if (!validCheck)
                        {
                            for (int i = 1; i <= dtexcelNew.Columns.Count; i++)
                            {
                                if (dtexcel.Rows[0][i].ToString() != null)
                                {
                                    String sColName = dtexcel.Rows[0][i].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                    if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                    {
                                        dtexcelNew.Columns[i - 1].ColumnName = "CATALOG_ITEM_NO";
                                    }
                                    else if (dtexcelNew.Columns[i - 1].ColumnName != sColName)
                                    {
                                        dtexcelNew.Columns[i - 1].ColumnName = sColName;
                                    }
                                }
                            }
                        }
                    }
                }
                if (dtexcelNew.Columns.Count > 0 && dtexcelNew.Columns[0].ColumnName.ToString().ToUpper() == "ACTION" && deleteflag.ToUpper() != "VALID")
                {
                    dtexcelNew.Columns.Remove(dtexcelNew.Columns[0].ColumnName);
                }
                dtexcelNew.AcceptChanges();
            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading GetDataFromExcel in AdvanceImportApiController", objException);
                return null;
            }

            if (dtexcelNew.Columns.Contains("PRODUCT_ID"))
            {
                dtexcelNew.Columns["PRODUCT_ID"].ColumnName = "FAMILY_ID";
                dtexcelNew.AcceptChanges();
            }
            if (dtexcelNew.Columns.Contains("ITEM_ID"))
            {
                dtexcelNew.Columns["ITEM_ID"].ColumnName = "PRODUCT_ID";
                dtexcelNew.AcceptChanges();
            }

            if (dtexcelNew.Columns.Contains("PRODUCT_NAME"))
            {
                dtexcelNew.Columns["PRODUCT_NAME"].ColumnName = "FAMILY_NAME";
                dtexcelNew.AcceptChanges();
            }

            if (dtexcelNew.Columns.Contains("SUBPRODUCT_ID"))
            {
                dtexcelNew.Columns["SUBPRODUCT_ID"].ColumnName = "SUBFAMILY_ID";
                dtexcelNew.AcceptChanges();
            }

            if (dtexcelNew.Columns.Contains("SUBPRODUCT_NAME"))
            {
                dtexcelNew.Columns["SUBPRODUCT_NAME"].ColumnName = "SUBFAMILY_NAME";
                dtexcelNew.AcceptChanges();
            }

            if (dtexcelNew.Columns.Contains("ITEM_PUBLISH2WEB"))
            {
                dtexcelNew.Columns["ITEM_PUBLISH2WEB"].ColumnName = "PRODUCT_PUBLISH2WEB";
                dtexcelNew.AcceptChanges();
            }

            if (dtexcelNew.Columns.Contains("ITEM_PUBLISH2PRINT"))
            {
                dtexcelNew.Columns["ITEM_PUBLISH2PRINT"].ColumnName = "PRODUCT_PUBLISH2PRINT";
                dtexcelNew.AcceptChanges();
            }

            if (dtexcelNew.Columns.Contains("ITEM_PUBLISH2PDF"))
            {
                dtexcelNew.Columns["ITEM_PUBLISH2PDF"].ColumnName = "PRODUCT_PUBLISH2PDF";
                dtexcelNew.AcceptChanges();
            }
            return dtexcelNew;
        }


        //---------------------------------------DETACH PROCESS--------------------------------------------------
        public DataTable GetDataFromExcelForDetach(string SheetName, string excelPath, string query, string deleteflag)
        {
            DataTable dtexcel = new DataTable();
            DataTable dtexcelNew = new DataTable();
            try
            {
                var fileName = Path.GetFileName(excelPath).Split('.')[0];
                var path = Path.GetFullPath(excelPath);
                var fileExtention = Path.GetExtension(excelPath);
                string excelPathCopy = path.Replace(Path.GetFileName(excelPath), "") + "//" + fileName + "_Copy" + "." + fileExtention;
                using (OleDbConnection conn = excelConnection(excelPath))
                {
                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    DataTable dtColumns = new DataTable();
                    try
                    {
                        DataRow[] schemaRow = schemaTable.Select("TABLE_NAME like '" + SheetName + "%'");
                        string sheet = schemaRow[0]["TABLE_NAME"].ToString();
                        int selectedIndex = 0;
                        foreach (DataRow dRow in schemaTable.Rows)
                        {

                            if (sheet.ToUpper() == dRow["TABLE_NAME"].ToString().ToUpper())
                                break;
                            selectedIndex++;
                        }

                        DataTable dt_checkHeaderColumns = new DataTable();
                        if (!sheet.EndsWith("_"))
                        {
                            OleDbDataAdapter da_checkHeaderexcel = new OleDbDataAdapter(query, conn);
                            dt_checkHeaderColumns.Locale = CultureInfo.CurrentCulture;


                            da_checkHeaderexcel.Fill(dt_checkHeaderColumns);

                        }




                        if (dt_checkHeaderColumns.Rows.Count > 0)
                        {

                            var categoryIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                           .Select((c, i) => new { c, i })
                           .Where(x => x.c.ToString().Equals("CATEGORY_NAME"))
                           .Select(x => x.i)
                           .ToArray();


                            var familyIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                          .Select((c, i) => new { c, i })
                          .Where(x => x.c.ToString().Equals("FAMILY_NAME"))
                          .Select(x => x.i)
                          .ToArray();

                            CSEntities objLS = new CSEntities();


                            int customerId = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => x.CustomerId).FirstOrDefault();

                            string displayItemnumber = objLS.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();


                            var displayItemnumberIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                               .Select((c, i) => new { c, i })
                               .Where(x => x.c.ToString().Equals(displayItemnumber))
                               .Select(x => x.i)
                               .ToArray();

                            var itemNumberIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                             .Select((c, i) => new { c, i })
                             .Where(x => x.c.ToString().Equals("ITEM#"))
                             .Select(x => x.i)
                             .ToArray();


                            if (categoryIndex.Count() > 0 || familyIndex.Count() > 0 || displayItemnumberIndex.Count() > 0 || itemNumberIndex.Count() > 0)
                            {
                                int columnsCount = dt_checkHeaderColumns.Columns.Count;
                                string str_Columnname = ExcelColumnFromNumber(columnsCount);
                                query = "select * from [" + SheetName + "$A2:" + str_Columnname + "65000]";
                            }

                        }



                        if (!sheet.EndsWith("_"))
                        {
                            OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                            dtexcel.Locale = CultureInfo.CurrentCulture;



                            daexcel.Fill(dtColumns);

                        }
                        conn.Close();
                        conn.Dispose();
                        var workbook1 = new Workbook();

                        workbook1 = Workbook.Load(excelPath);


                        for (int i = 0; i < workbook1.Worksheets.Count; i++)
                        {
                            if (workbook1.Worksheets[i].Name.ToUpper() == SheetName.ToUpper())
                            {
                                selectedIndex = i;

                            }
                        }


                        //foreach (WorksheetColumn colSheet in workbook1.Worksheets[1].Columns)
                        //    dt11.Columns.Add(colSheet.ToString());
                        dtexcel = dtColumns.Clone();
                        bool skipFirstRow = false;



                        if (!deleteflag.ToUpper().Contains("DETACH"))
                        {

                            foreach (WorksheetRow rowSheet in workbook1.Worksheets[selectedIndex].Rows)
                            {




                                if (dtColumns.Columns.Contains("ACTION") && !skipFirstRow)
                                {
                                    skipFirstRow = true;
                                    continue;
                                }
                                DataRow dRow = dtexcel.NewRow();

                                for (int col = 0; col < dtexcel.Columns.Count; col++)
                                {

                                    if (rowSheet.Cells[col].Value == null)
                                    {
                                        dRow[col] = DBNull.Value;
                                    }
                                    //else if(rowSheet.Index==0 &&  (categoryIndex.Count()>0 || familyIndex.Count() > 0 || displayItemnumberIndex.Count() > 0 || itemNumberIndex.Count() > 0))
                                    //{
                                    //    dRow[col] = DBNull.Value;
                                    //}
                                    else
                                    {
                                        dRow[col] = rowSheet.Cells[col].Value.ToString();
                                    }


                                }
                                //new
                                if (dRow.ItemArray[0].ToString() != "Action")
                                {

                                    dtexcel.Rows.Add(dRow);
                                }
                            }
                        }
                        else
                        {
                            dtexcel = new DataTable();
                            dtexcel = dtColumns;
                            objLS = new CSEntities();
                        }
                        workbook1.Worksheets.Clear();
                    }
                    catch (Exception ex)
                    {
                        dtexcel = new DataTable();
                        dtexcel = dtColumns;
                    }
                }

                //Write the validation code for without import subfamily id and subfamily_name
                //bool checkSubFamily = true;
                //if (dtexcel.Columns.Count != 0)
                //{                  
                //    foreach (DataColumn dc in dtexcel.Columns)
                //    {
                //        if (dc.ColumnName.ToUpper()!="FAMILY_NAME" && (dc.ColumnName.ToUpper() == "SUBFAMILY_ID" || dc.ColumnName.ToUpper() == "SUBFAMILY_NAME") )
                //        {
                //            checkSubFamily = false;
                //        }                       
                //    }
                //    if(checkSubFamily)
                //    {
                //        dtexcel.Columns.Add("SUBFAMILY_ID");
                //        dtexcel.Columns.Add("SUBFAMILY_NAME");
                //    }
                //}


                if (dtexcel.Columns[0].ColumnName.ToString().ToUpper() == "ACTION" && deleteflag.ToUpper() == "VALID")
                {
                    dtexcelNew = dtexcel;
                    goto Skip;
                }
                if ((dtexcel == null || dtexcel.Rows.Count == 0 || dtexcel.Rows.Count == 1) && deleteflag.ToUpper() != "SKIP" && deleteflag.ToUpper() != "DETACH")
                {
                    return dtexcel;
                }
                dtexcelNew = SetDataTableForDetach(dtexcel, deleteflag);
                Skip:

                var tbattrcol = objLS.TB_ATTRIBUTE.Find(1);
                Workbook workbook;
                try
                {
                    workbook = Workbook.Load(excelPath);
                    string sheetNameworkbook = SheetName.Replace("$", "").TrimEnd();
                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells.Count();
                    if (deleteflag == "Valid")
                    {
                        for (int i = 0; i < dtexcelNew.Columns.Count; i++)
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value != null)
                            {
                                String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i].ColumnName != sColName && sColName.ToUpper() != "ACTION" && !dtexcelNew.Columns.Contains(sColName))
                                {
                                    dtexcelNew.Columns[i].ColumnName = sColName;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= dtexcelNew.Columns.Count; i++)
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value != null)
                            {
                                String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i - 1].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i - 1].ColumnName != sColName && !dtexcelNew.Columns.Contains(sColName))
                                {
                                    dtexcelNew.Columns[i - 1].ColumnName = sColName;
                                }
                            }
                        }
                    }
                }
                catch
                {
                    if (deleteflag == "Valid")
                    {
                        for (int i = 0; i < dtexcelNew.Columns.Count; i++)
                        {
                            if (dtexcel.Rows[0][i].ToString() != null)
                            {
                                String sColName = dtexcel.Rows[0][i].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i].ColumnName != sColName && sColName.ToUpper() != "ACTION")
                                {
                                    dtexcelNew.Columns[i].ColumnName = sColName;
                                }
                            }
                        }
                    }
                    else
                    {
                        var validCheck = SheetName.ToUpper().Contains("FAM") ? dtexcelNew.Columns.Contains("FAMILY_ID") : dtexcelNew.Columns.Contains("PRODUCT_ID");
                        if (!validCheck)
                        {
                            for (int i = 1; i <= dtexcelNew.Columns.Count; i++)
                            {
                                if (dtexcel.Rows[0][i].ToString() != null)
                                {
                                    String sColName = dtexcel.Rows[0][i].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                    if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                    {
                                        dtexcelNew.Columns[i - 1].ColumnName = "CATALOG_ITEM_NO";
                                    }
                                    else if (dtexcelNew.Columns[i - 1].ColumnName != sColName)
                                    {
                                        dtexcelNew.Columns[i - 1].ColumnName = sColName;
                                    }
                                }
                            }
                        }
                    }
                }
                if (dtexcelNew.Columns.Count > 0 && dtexcelNew.Columns[0].ColumnName.ToString().ToUpper() == "ACTION" && deleteflag.ToUpper() != "VALID")
                {
                    dtexcelNew.Columns.Remove(dtexcelNew.Columns[0].ColumnName);
                }
                dtexcelNew.AcceptChanges();
            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading GetDataFromExcel in AdvanceImportApiController", objException);
                return null;
            }
            return dtexcelNew;
        }

        public string DetachRecords(string SheetName, string allowDuplicate, string excelPath, string importType, int catalogId, JArray model)
        {
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            List<object> newList = new List<object>();
            DataSet dsGetProdCnt = new DataSet();
            int importProductCount = 0;
            int userProductCount = 0;
            int userSKUProductCount = 0;
            try
            {
                var customerId = 0;
                var skucnt = objLS.TB_PLAN
                   .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                   .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                var SKUcount = skucnt.Select(a => a).ToList();
                customerId = SKUcount[0].CustomerId;

                DataTable dtGetProdCnt = GetDataFromExcelForDetach(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DETACH");
                if (dtGetProdCnt == null || dtGetProdCnt.Rows.Count == 0)
                {
                    return "0";
                }

                importExcelSheetSelection(excelPath, SheetName);
                DataTable detachRecords = GetDataFromExcelForDetach(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DETACH");
                DataTable excelData = new DataTable();
                ImportApiController importApiController = new ImportApiController();

                if (importType.ToUpper().Contains("SUBPRODUCT") && detachRecords.Columns.Contains("SUBITEM#"))
                {
                    detachRecords.Columns["SUBITEM#"].ColumnName = "SUBCATALOG_ITEM_NO";
                }
                if (importType.ToUpper().Contains("SUBPRODUCT"))
                {

                }
                else if (importType.ToUpper().Contains("CATEGORIES"))
                {

                }
                else if (importType.ToUpper().Contains("FAMILIES"))
                {


                }
                else
                {


                    var customerDetails = objLS.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();

                    //With Hierarchy & ID
                    if (detachRecords.Columns.Contains("PRODUCT_ID") && detachRecords.Columns.Contains("FAMILY_ID") && detachRecords.Columns.Contains("CATALOG_ID"))
                    {

                        if (detachRecords.Columns.Contains(customerDetails))
                        {
                            detachRecords.Columns[customerDetails].ColumnName = "CATALOG_ITEM_NO";
                        }
                        excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "CATALOG_ID", "FAMILY_ID", "PRODUCT_ID");
                        excelData.Columns.Add("REMOVE_ASSOCIATE");
                        excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_ASSOCIATE"] = "YES");
                    }
                    //With Hi,without ID
                    else if (!(detachRecords.Columns.Contains("PRODUCT_ID") && detachRecords.Columns.Contains("FAMILY_ID") && detachRecords.Columns.Contains("CATALOG_ID")) && (detachRecords.Columns.Contains("CATALOG_NAME") && detachRecords.Columns.Contains("FAMILY_NAME")))
                    {

                        if (detachRecords.Columns.Contains(customerDetails))
                        {
                            detachRecords.Columns[customerDetails].ColumnName = "CATALOG_ITEM_NO";
                        }
                        // excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO","FAMILY_NAME","CATEGORY_NAME");
                        if (detachRecords.Columns.Contains("SUBCATNAME_L6"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1", "SUBCATNAME_L2", "SUBCATNAME_L3", "SUBCATNAME_L4", "SUBCATNAME_L5", "SUBCATNAME_L6");
                        }
                        else if (detachRecords.Columns.Contains("SUBCATNAME_L5"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1", "SUBCATNAME_L2", "SUBCATNAME_L3", "SUBCATNAME_L4", "SUBCATNAME_L5");
                        }
                        else if (detachRecords.Columns.Contains("SUBCATNAME_L4"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1", "SUBCATNAME_L2", "SUBCATNAME_L3", "SUBCATNAME_L4");
                        }
                        else if (detachRecords.Columns.Contains("SUBCATNAME_L3"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1", "SUBCATNAME_L2", "SUBCATNAME_L3");
                        }
                        else if (detachRecords.Columns.Contains("SUBCATNAME_L2"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1", "SUBCATNAME_L2");
                        }
                        else if (detachRecords.Columns.Contains("SUBCATNAME_L1"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1");
                        }
                        else if (detachRecords.Columns.Contains("CATEGORY_NAME"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME");
                        }


                        excelData.Columns.Add("CATALOG_ID");
                        excelData.Columns.Add("PRODUCT_ID");
                        excelData.Columns.Add("FAMILY_ID");
                        excelData.Columns.Add("REMOVE_ASSOCIATE");
                        excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_ASSOCIATE"] = "YES");
                        foreach (DataRow dr in excelData.Rows)
                        {
                            dr["CATALOG_ID"] = catalogId;
                            string ITEM = dr["CATALOG_ITEM_NO"].ToString();
                            string Famname = dr["FAMILY_NAME"].ToString();
                            string Categoryname = "";
                            if (excelData.Columns.Contains("SUBCATNAME_L6") && (!(dr["SUBCATNAME_L6"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L4"].ToString();
                            }
                            else if (excelData.Columns.Contains("SUBCATNAME_L5") && (!(dr["SUBCATNAME_L5"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L4"].ToString();
                            }
                            else if (excelData.Columns.Contains("SUBCATNAME_L4") && (!(dr["SUBCATNAME_L4"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L4"].ToString();
                            }
                            else if (excelData.Columns.Contains("SUBCATNAME_L3") && (!(dr["SUBCATNAME_L3"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L3"].ToString();
                            }
                            else if (excelData.Columns.Contains("SUBCATNAME_L2") && (!(dr["SUBCATNAME_L2"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L2"].ToString();
                            }
                            else if (excelData.Columns.Contains("SUBCATNAME_L1") && (!(dr["SUBCATNAME_L1"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L1"].ToString();
                            }
                            else if (excelData.Columns.Contains("CATEGORY_NAME") && (!(dr["CATEGORY_NAME"].ToString() == "")))
                            {
                                Categoryname = dr["CATEGORY_NAME"].ToString();
                            }



                            DataTable DETACHIMPORT = new DataTable();
                            string sqlConn = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                            using (var objSqlConnection = new SqlConnection(sqlConn))
                            {
                                objSqlConnection.Open();
                                var objSqlDataAdapter = new SqlDataAdapter("SELECT TPF.FAMILY_ID,TF.FAMILY_NAME,TPF.PRODUCT_ID,TPS.STRING_VALUE,TC.CATEGORY_ID,TC.CATEGORY_NAME    FROM TB_PROD_SPECS TPS JOIN TB_PROD_FAMILY TPF ON TPF.PRODUCT_ID = TPS.PRODUCT_ID JOIN TB_CATALOG_FAMILY TCF ON TCF.FAMILY_ID = TPF.FAMILY_ID AND TCF.CATALOG_ID = " + catalogId + " JOIN TB_FAMILY TF ON TF.FAMILY_ID = TPF.FAMILY_ID AND TF.FAMILY_NAME = '" + Famname + "' JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TC.CATEGORY_NAME = '" + Categoryname + "' JOIN TB_CATALOG_SECTIONS TCS ON TCS.CATEGORY_ID = TC.CATEGORY_ID AND TCS.CATALOG_ID = " + catalogId + "  WHERE TPS.STRING_VALUE = '" + ITEM + "' AND TPS.ATTRIBUTE_ID = 1 ", objSqlConnection);
                                objSqlDataAdapter.Fill(DETACHIMPORT);
                                objSqlConnection.Close();
                            }



                            dr["PRODUCT_ID"] = DETACHIMPORT.Rows[0].ItemArray[2].ToString();
                            dr["FAMILY_ID"] = DETACHIMPORT.Rows[0].ItemArray[0].ToString();
                        }
                        excelData.Columns.Remove("CATEGORY_NAME");
                    }
                    else
                    {
                        if (detachRecords.Columns.Contains(customerDetails))
                        {
                            detachRecords.Columns[customerDetails].ColumnName = "CATALOG_ITEM_NO";

                        }
                        //Without Hi,with productID
                        if (detachRecords.Columns.Contains("PRODUCT_ID"))
                        {
                            detachRecords.Columns[customerDetails].ColumnName = "CATALOG_ITEM_NO";
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "PRODUCT_ID");
                            excelData.Columns.Add("CATALOG_ID");
                            excelData.Columns.Add("REMOVE_ASSOCIATE");
                            excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_ASSOCIATE"] = "YES");
                            foreach (DataRow dr in excelData.Rows)
                            {
                                dr["CATALOG_ID"] = catalogId;
                            }
                        }
                        //Without Hi,ID
                        else
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO");
                            excelData.Columns.Add("PRODUCT_ID");
                            excelData.Columns.Add("CATALOG_ID");
                            excelData.Columns.Add("REMOVE_ASSOCIATE");
                            excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_ASSOCIATE"] = "YES");
                            foreach (DataRow dr in excelData.Rows)
                            {
                                dr["CATALOG_ID"] = catalogId;
                                string ITEM = dr["CATALOG_ITEM_NO"].ToString();
                                var prodid = _dbcontext.TB_PROD_SPECS.Join(_dbcontext.TB_CATALOG_PRODUCT, ta => ta.PRODUCT_ID, ca => ca.PRODUCT_ID, (ta, ca) => new { ta, ca }).Where(x => x.ta.STRING_VALUE.Equals(ITEM) && x.ta.ATTRIBUTE_ID == 1 && x.ca.CATALOG_ID == catalogId).Select(z => z.ta.PRODUCT_ID).FirstOrDefault().ToString();
                                dr["PRODUCT_ID"] = prodid;

                            }
                        }


                    }


                }





                DataTable oattType = new DataTable();
                DataSet replace = new DataSet();

                int ItemVal = Convert.ToInt32(allowDuplicate);
                if (detachRecords.Rows.Count == 0)
                {
                    return detachRecords.Rows.Count.ToString();
                }
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();

                    SQLString =
                    "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                    importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                    _DBCommand.ExecuteNonQuery();
                    SQLString = importController.CreateTable("[##IMPORTTEMP" + importTemp + "]", excelPath, SheetName, excelData);
                    SqlCommand _DBCommandnew = new SqlCommand(SQLString, conn);
                    _DBCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(conn)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(excelData);

                    oattType = importController.SelectedColumnsToImport(detachRecords, model);
                    var cmd1 =
                        new SqlCommand(
                            "ALTER TABLE [##IMPORTTEMP" + importTemp +
                            "] ADD STATUS nvarchar(20)",
                            conn);
                    cmd1.ExecuteNonQuery();




                    var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                    cmd.ExecuteNonQuery();
                    importController._SQLString = @"select * from [##LOGTEMPTABLE" + importTemp + "]";
                    DataSet ds = importController.CreateDataSet();
                    importController._SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp +
                                    "]";
                    cmd1.CommandText = importController._SQLString;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.ExecuteNonQuery();

                    DataTable Detachcount = new DataTable();
                    var cmd1f12 = new SqlCommand("SELECT count(*) FROM [tempresult" + importTemp + "] where STATUS='DETACHED'", conn);
                    var daf12 = new SqlDataAdapter(cmd1f12);
                    daf12.Fill(Detachcount);

                    return Detachcount.Rows[0].ItemArray[0].ToString();
                }

            }
            catch (Exception ex)

            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at AdvanceImportApiController : FinishProductImport", ex);
                return null;
            }
        }

        public DataTable SetDataTableForDetach(DataTable oldSheet, string deleteflag)
        {
            DataTable newSheet = new DataTable();
            try
            {

                if (oldSheet.Columns[0].ColumnName.ToString().ToUpper() == "ACTION")
                {
                    foreach (DataColumn dcol in oldSheet.Columns)
                    {
                        if (dcol.ColumnName.ToString().ToUpper() != "ACTION" && !newSheet.Columns.Contains(dcol.ColumnName))
                            newSheet.Columns.Add(dcol.ColumnName);
                    }
                    foreach (DataRow dtrow in oldSheet.Rows)
                    {
                        DataRow drow = newSheet.NewRow();
                        if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() != "DETACH" && deleteflag == "")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (deleteflag == "Valid")
                        {
                            for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count - 1 < exl)
                                {
                                    break;
                                }
                                drow[exl] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() == "DETACH" && deleteflag == "DETACH")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() == "SKIP" && dtrow[0].ToString().ToUpper() != "DETACH" && dtrow[0].ToString().ToUpper() != "" && deleteflag == "SKIP")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                    }
                    return newSheet;
                }
                foreach (DataRow dtrow in oldSheet.Rows)
                {
                    if (dtrow.ItemArray[0].ToString().ToUpper() == "ACTION")
                    {
                        foreach (var colName in dtrow.ItemArray)
                        {
                            if (colName.ToString().ToUpper() != "ACTION" && !newSheet.Columns.Contains(colName.ToString()) && !string.IsNullOrEmpty(colName.ToString()))
                                newSheet.Columns.Add(colName.ToString());
                            else if (deleteflag == "Valid" && !string.IsNullOrEmpty(colName.ToString()))
                                newSheet.Columns.Add(colName.ToString());
                        }

                    }
                    else
                    {
                        DataRow drow = newSheet.NewRow();

                        if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() != "DETACH" && deleteflag == "")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (deleteflag == "Valid")
                        {
                            for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count - 1 < exl)
                                {
                                    break;
                                }
                                drow[exl] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() == "DETACH" && deleteflag == "DETACH")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() == "SKIP" && dtrow[0].ToString().ToUpper() != "DETACH" && dtrow[0].ToString().ToUpper() != "" && deleteflag == "SKIP")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                    }
                }

            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading SetDataTable in AdvanceImportApiController", objException);
                return null;
            }
            return newSheet;
        }
        //---------------------------------------DETACH PROCESS--------------------------------------------------

        public static string ExcelColumnFromNumber(int column)
        {
            string columnString = "";
            decimal columnNumber = column;
            while (columnNumber > 0)
            {
                decimal currentLetterNumber = (columnNumber - 1) % 26;
                char currentLetter = (char)(currentLetterNumber + 65);
                columnString = currentLetter + columnString;
                columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
            }
            return columnString;
        }
        #region Import
        public DataTable AddIdColumns(DataTable dtGetProdCnt, string importType)
        {
            if (dtGetProdCnt == null || dtGetProdCnt.Rows.Count == 0)
            {
                return dtGetProdCnt;
            }
            if (!dtGetProdCnt.Columns.Contains("CATALOG_ID"))
            {
                dtGetProdCnt.Columns.Add("CATALOG_ID");
                dtGetProdCnt.Columns["CATALOG_ID"].SetOrdinal(0);
            }
            if (!dtGetProdCnt.Columns.Contains("CATALOG_NAME"))
            {
                dtGetProdCnt.Columns.Add("CATALOG_NAME");
                dtGetProdCnt.Columns["CATALOG_NAME"].SetOrdinal(1);
            }
            if (!dtGetProdCnt.Columns.Contains("CATEGORY_ID"))
            {
                dtGetProdCnt.Columns.Add("CATEGORY_ID");
                // dtGetProdCnt.Columns["CATEGORY_ID"].SetOrdinal(dtGetProdCnt.Columns.IndexOf("CATEGORY_NAME"));
                dtGetProdCnt.Columns["CATEGORY_ID"].SetOrdinal(2);

            }
            if (!dtGetProdCnt.Columns.Contains("CATEGORY_NAME"))
            {
                dtGetProdCnt.Columns.Add("CATEGORY_NAME");
                // dtGetProdCnt.Columns["CATEGORY_ID"].SetOrdinal(dtGetProdCnt.Columns.IndexOf("CATEGORY_NAME"));
                dtGetProdCnt.Columns["CATEGORY_NAME"].SetOrdinal(3);

            }
            int col = 1;
            for (int colindex = 0; colindex < dtGetProdCnt.Columns.Count; colindex++)
            {
                if (dtGetProdCnt.Columns[colindex].ColumnName.ToUpper().Contains("SUBCATNAME"))
                {
                    if (!dtGetProdCnt.Columns.Contains("SUBCATID_L" + col))
                    {
                        dtGetProdCnt.Columns.Add("SUBCATID_L" + col);
                        if (dtGetProdCnt.Columns.Contains("SUBCATNAME_L" + col))
                        {
                            dtGetProdCnt.Columns["SUBCATID_L" + col].SetOrdinal(dtGetProdCnt.Columns.IndexOf("SUBCATNAME_L" + col));
                        }
                        colindex = colindex + 1;
                    }
                    if (dtGetProdCnt.Columns.Contains("SUBCATID_L" + col))
                    {
                        col = col + 1;
                    }
                }
            }

            if (importType != "Categories")
            {
                if (!dtGetProdCnt.Columns.Contains("FAMILY_ID"))
                {
                    dtGetProdCnt.Columns.Add("FAMILY_ID");
                    if (dtGetProdCnt.Columns.Contains("FAMILY_NAME"))
                    {
                        dtGetProdCnt.Columns["FAMILY_ID"].SetOrdinal(dtGetProdCnt.Columns.IndexOf("FAMILY_NAME"));
                    }

                }
                if (!dtGetProdCnt.Columns.Contains("SUBFAMILY_ID"))
                {
                    dtGetProdCnt.Columns.Add("SUBFAMILY_ID");
                    if (dtGetProdCnt.Columns.Contains("FAMILY_NAME"))
                    {
                        dtGetProdCnt.Columns["SUBFAMILY_ID"].SetOrdinal(dtGetProdCnt.Columns.IndexOf("FAMILY_NAME") + 1);
                    }

                }
                if (!dtGetProdCnt.Columns.Contains("SUBFAMILY_NAME"))
                {
                    dtGetProdCnt.Columns.Add("SUBFAMILY_NAME");
                    dtGetProdCnt.Columns["SUBFAMILY_NAME"].SetOrdinal(dtGetProdCnt.Columns.IndexOf("SUBFAMILY_ID") + 1);

                }
                if (!dtGetProdCnt.Columns.Contains("PRODUCT_ID"))
                {
                    if (importType == "Products")
                    {
                        dtGetProdCnt.Columns.Add("PRODUCT_ID");
                        if (dtGetProdCnt.Columns.Contains("CATALOG_ITEM_NO") == true)
                        {
                            dtGetProdCnt.Columns["PRODUCT_ID"].SetOrdinal(dtGetProdCnt.Columns.IndexOf("CATALOG_ITEM_NO"));
                        }
                    }

                }
            }
            return dtGetProdCnt;
        }

        public string FinishImportBatch(string sessionId, string SheetName, string allowDuplicate, string importType, int catalogId, string excelPath, string scheduleDate, JArray model)
        {
            try
            {
                #region declaration
                string batchId, sqlString = string.Empty;
                DataTable dtImportData = new DataTable();
                DataTable dtAttributeData = new DataTable();
                DataSet dsGetProdCnt = new DataSet();
                int userSKUProductCount = 0;
                int importProductCount = 0;
                int userProductCount = 0;
                var customerId = 0;
                QueryValues queryValues = new QueryValues();


                // CustomerItemNo
                if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
                {
                    var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    int customerIds;
                    int.TryParse(customerDetails.ToString(), out customerIds);
                    string customerCatalog = queryValues.GetCatalogItemNo(customerIds);
                    System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerCatalog;
                }

                // CustomerSubItemNo
                if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
                {
                    var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    int customerIds;
                    int.TryParse(customerDetails.ToString(), out customerIds);
                    string customerCatalog = queryValues.GetSubCatalogItemNo(customerIds);
                    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerCatalog;
                }


                string customAttributeName = HttpContext.Current.Session["CustomerItemNo"].ToString();
                string subCustomAttributeName = HttpContext.Current.Session["CustomerSubItemNo"].ToString();
                #endregion
                batchId = Guid.NewGuid().ToString(); //To generate a guid 
                var skucnt = objLS.TB_PLAN
                   .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                   .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                var SKUcount = skucnt.Select(a => a).ToList();
                if (importType.ToUpper() == "PRODUCTS")
                {
                    dtImportData = new DataTable();
                    dtImportData = ValidateItemNumber(GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "Valid"));
                    if (dtImportData != null && dtImportData.Rows.Count > 0)
                    {
                        importType = "CATALOG_ITEM_NO";
                    }
                    else
                    {
                        dtImportData = new DataTable();
                        dtImportData = AddIdColumns(GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "Valid"), importType);
                        // if (importType.ToUpper().Contains("CATALOG_ITEM_NO"))
                        //{
                        foreach (DataColumn dcReplaceColumn in dtImportData.Columns)
                        {
                            if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                            {
                                dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                break;
                            }

                        }
                        //}
                    }
                }
                else if (importType.ToUpper() == "FAMILIES")
                {
                    dtImportData = new DataTable();
                    dtImportData = ValidateFamilyData(GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "Valid"));
                    if (dtImportData != null && dtImportData.Rows.Count > 0)
                    {
                        importType = "FAMILY_IMPORT_WITHOUT_HIERARCHY";
                    }
                    dtImportData = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "Valid");

                }
                else if (importType.ToUpper() == "SUBPRODUCTS")
                {
                    dtImportData = new DataTable();
                    dtImportData = ValidateItemNumber(GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "Valid"));
                    if (dtImportData != null && dtImportData.Rows.Count > 0)
                    {
                        importType = "SUBCATALOG_ITEM_NO";
                    }
                    else
                    {
                        dtImportData = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "Valid");

                        foreach (DataColumn dcReplaceColumn in dtImportData.Columns)
                        {
                            if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                            {
                                dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                            }
                            if (dcReplaceColumn.ColumnName.ToUpper() == subCustomAttributeName.ToUpper())
                            {
                                dcReplaceColumn.ColumnName = "SUBCATALOG_ITEM_NO";
                                break;
                            }

                        }
                    }
                }
                else if (importType != "Families" && importType != "FAMILY_IMPORT_WITHOUT_HIERARCHY")
                {
                    if (allowDuplicate == "1")
                    {
                        DataTable dtGetProdCnt = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "");
                        foreach (DataColumn dcReplaceColumn in dtGetProdCnt.Columns)
                        {
                            if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                            {
                                dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                break;
                            }
                        }
                        dsGetProdCnt.Tables.Add(dtGetProdCnt.Select(" isnull(Catalog_item_no,'') <> ''").CopyToDataTable());

                    }
                    else
                    {
                        DataTable dtGetProdCnt = new DataTable();
                        if (importType == "CATALOG_ITEM_NO")
                        {
                            dtGetProdCnt = dtImportData;
                        }
                        else
                        {
                            dtGetProdCnt = AddIdColumns(dtImportData, importType);
                        }
                        DataRow[] drow = dtGetProdCnt.Select("PRODUCT_ID IS NULL  and isnull(Catalog_item_no,'') <> ''");
                        if (drow.Count() > 0)
                        {
                            dsGetProdCnt.Tables.Add(dtGetProdCnt.Select("PRODUCT_ID IS NULL  and isnull(Catalog_item_no,'') <> ''").CopyToDataTable());
                            if (SKUcount.Any())
                            {
                                userSKUProductCount = Convert.ToInt32(SKUcount[0].SKU_COUNT);
                                customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                            }
                            if (Convert.ToInt32(dsGetProdCnt.Tables[0].Rows.Count) > 0)
                            {
                                importProductCount = dsGetProdCnt.Tables[0].Rows.Count;
                                var productcount = objLS.STP_LS_GET_PRODUCTS_BY_USER("PRODUCT", User.Identity.Name, "").ToList();
                                if (productcount.Any())
                                {
                                    userProductCount = Convert.ToInt32(productcount[0]);
                                }
                                if ((userProductCount + importProductCount) > userSKUProductCount)
                                {
                                    return "SKU Exceed~" + batchId;
                                }
                            }
                        }
                    }
                }
                else if (importType != "CATALOG_ITEM_NO" && importType != "FAMILY_IMPORT_WITHOUT_HIERARCHY")
                {
                    dtImportData = AddIdColumns(GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "Valid"), importType);
                    foreach (DataColumn dcReplaceColumn in dtImportData.Columns)
                    {
                        if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                        {
                            dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                            break;
                        }

                    }

                }
                else
                {
                    if (SKUcount.Any())
                    {
                        customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                    }
                }
                if (customerId == 0)
                {
                    if (SKUcount.Any())
                    {
                        customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                    }
                }

                ImportApiController importApiController = new ImportApiController();
                if (importType == "CATALOG_ITEM_NO")
                {
                    dtImportData = importApiController.UpdateCatalogDetails("PRODUCT", catalogId, dtImportData);
                }
                //else if(importType == "FAMILY_IMPORT_WITHOUT_HIERARCHY")
                //{
                //    dtImportData = importApiController.UpdateCatalogDetails(importType, catalogId, dtImportData);
                //}
                else
                {
                    dtImportData = importApiController.UpdateCatalogDetails(importType, catalogId, dtImportData);
                }
                if (dtImportData.Columns.Contains("ACTION"))
                {
                    dtImportData.Columns["ACTION"].SetOrdinal(0);
                }


                if (importType.ToUpper().Contains("PRODUCT") || importType.ToUpper().Contains("CATALOG_ITEM_NO"))
                {
                    //////////////////////////////////////////////////////////////////////////////////



                    if (dtImportData.Columns.Contains("FAMILY_ID"))
                    {
                        if (!dtImportData.Columns.Contains("FAMILY_PUBLISH2WEB"))
                        {
                            dtImportData.Columns.Add("FAMILY_PUBLISH2WEB");
                            dtImportData.Columns["FAMILY_PUBLISH2WEB"].SetOrdinal(dtImportData.Columns.IndexOf("SUBFAMILY_ID") + 1);
                        }
                        if (!dtImportData.Columns.Contains("FAMILY_PUBLISH2PDF"))
                        {
                            dtImportData.Columns.Add("FAMILY_PUBLISH2PDF");
                            dtImportData.Columns["FAMILY_PUBLISH2PDF"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_PUBLISH2WEB"));
                        }
                        if (!dtImportData.Columns.Contains("FAMILY_PUBLISH2EXPORT"))
                        {
                            dtImportData.Columns.Add("FAMILY_PUBLISH2EXPORT");
                            dtImportData.Columns["FAMILY_PUBLISH2EXPORT"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_PUBLISH2PDF"));
                        }
                        if (!dtImportData.Columns.Contains("FAMILY_PUBLISH2PRINT"))
                        {
                            dtImportData.Columns.Add("FAMILY_PUBLISH2PRINT");
                            dtImportData.Columns["FAMILY_PUBLISH2PRINT"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_PUBLISH2EXPORT"));
                        }
                        if (!dtImportData.Columns.Contains("FAMILY_PUBLISH2PORTAL"))
                        {
                            dtImportData.Columns.Add("FAMILY_PUBLISH2PORTAL");
                            dtImportData.Columns["FAMILY_PUBLISH2PORTAL"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_PUBLISH2PRINT"));
                        }
                        if (!dtImportData.Columns.Contains("CATEGORY_PUBLISH2WEB"))
                        {
                            dtImportData.Columns.Add("CATEGORY_PUBLISH2WEB");
                            dtImportData.Columns["CATEGORY_PUBLISH2WEB"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_ID"));
                        }
                        if (!dtImportData.Columns.Contains("CATEGORY_PUBLISH2PDF"))
                        {
                            dtImportData.Columns.Add("CATEGORY_PUBLISH2PDF");
                            dtImportData.Columns["CATEGORY_PUBLISH2PDF"].SetOrdinal(dtImportData.Columns.IndexOf("CATEGORY_PUBLISH2WEB"));
                        }
                        if (!dtImportData.Columns.Contains("CATEGORY_PUBLISH2EXPORT"))
                        {
                            dtImportData.Columns.Add("CATEGORY_PUBLISH2EXPORT");
                            dtImportData.Columns["CATEGORY_PUBLISH2EXPORT"].SetOrdinal(dtImportData.Columns.IndexOf("CATEGORY_PUBLISH2PDF"));
                        }
                        if (!dtImportData.Columns.Contains("CATEGORY_PUBLISH2PRINT"))
                        {
                            dtImportData.Columns.Add("CATEGORY_PUBLISH2PRINT");
                            dtImportData.Columns["CATEGORY_PUBLISH2PRINT"].SetOrdinal(dtImportData.Columns.IndexOf("CATEGORY_PUBLISH2EXPORT"));
                        }
                        if (!dtImportData.Columns.Contains("CATEGORY_PUBLISH2PORTAL"))
                        {
                            dtImportData.Columns.Add("CATEGORY_PUBLISH2PORTAL");
                            dtImportData.Columns["CATEGORY_PUBLISH2PORTAL"].SetOrdinal(dtImportData.Columns.IndexOf("CATEGORY_PUBLISH2PRINT"));
                        }
                        ////
                        if (!dtImportData.Columns.Contains("PRODUCT_PUBLISH2WEB"))
                        {
                            dtImportData.Columns.Add("PRODUCT_PUBLISH2WEB");
                            dtImportData.Columns["PRODUCT_PUBLISH2WEB"].SetOrdinal(dtImportData.Columns.IndexOf("CATALOG_ITEM_NO"));
                        }
                        if (!dtImportData.Columns.Contains("PRODUCT_PUBLISH2PDF"))
                        {
                            dtImportData.Columns.Add("PRODUCT_PUBLISH2PDF");
                            dtImportData.Columns["PRODUCT_PUBLISH2PDF"].SetOrdinal(dtImportData.Columns.IndexOf("PRODUCT_PUBLISH2WEB"));
                        }
                        if (!dtImportData.Columns.Contains("PRODUCT_PUBLISH2EXPORT"))
                        {
                            dtImportData.Columns.Add("PRODUCT_PUBLISH2EXPORT");
                            dtImportData.Columns["PRODUCT_PUBLISH2EXPORT"].SetOrdinal(dtImportData.Columns.IndexOf("PRODUCT_PUBLISH2PDF"));
                        }
                        if (!dtImportData.Columns.Contains("PRODUCT_PUBLISH2PRINT"))
                        {
                            dtImportData.Columns.Add("PRODUCT_PUBLISH2PRINT");
                            dtImportData.Columns["PRODUCT_PUBLISH2PRINT"].SetOrdinal(dtImportData.Columns.IndexOf("PRODUCT_PUBLISH2EXPORT"));
                        }
                        if (!dtImportData.Columns.Contains("PRODUCT_PUBLISH2PORTAL"))
                        {
                            dtImportData.Columns.Add("PRODUCT_PUBLISH2PORTAL");
                            dtImportData.Columns["PRODUCT_PUBLISH2PORTAL"].SetOrdinal(dtImportData.Columns.IndexOf("PRODUCT_PUBLISH2PRINT"));
                        }
                    }
                    else
                    {

                        if (!dtImportData.Columns.Contains("PRODUCT_PUBLISH2WEB"))
                        {
                            dtImportData.Columns.Add("PRODUCT_PUBLISH2WEB");
                            dtImportData.Columns["PRODUCT_PUBLISH2WEB"].SetOrdinal(dtImportData.Columns.IndexOf("CATALOG_ITEM_NO"));
                        }
                        if (!dtImportData.Columns.Contains("PRODUCT_PUBLISH2PDF"))
                        {
                            dtImportData.Columns.Add("PRODUCT_PUBLISH2PDF");
                            dtImportData.Columns["PRODUCT_PUBLISH2PDF"].SetOrdinal(dtImportData.Columns.IndexOf("PRODUCT_PUBLISH2WEB"));
                        }
                        if (!dtImportData.Columns.Contains("PRODUCT_PUBLISH2EXPORT"))
                        {
                            dtImportData.Columns.Add("PRODUCT_PUBLISH2EXPORT");
                            dtImportData.Columns["PRODUCT_PUBLISH2EXPORT"].SetOrdinal(dtImportData.Columns.IndexOf("PRODUCT_PUBLISH2PDF"));
                        }
                        if (!dtImportData.Columns.Contains("PRODUCT_PUBLISH2PRINT"))
                        {
                            dtImportData.Columns.Add("PRODUCT_PUBLISH2PRINT");
                            dtImportData.Columns["PRODUCT_PUBLISH2PRINT"].SetOrdinal(dtImportData.Columns.IndexOf("PRODUCT_PUBLISH2EXPORT"));
                        }
                        if (!dtImportData.Columns.Contains("PRODUCT_PUBLISH2PORTAL"))
                        {
                            dtImportData.Columns.Add("PRODUCT_PUBLISH2PORTAL");
                            dtImportData.Columns["PRODUCT_PUBLISH2PORTAL"].SetOrdinal(dtImportData.Columns.IndexOf("PRODUCT_PUBLISH2PRINT"));
                        }
                    }

                    ////////////////////////////////////////////////////////////////////////////////////
                }
                else if (importType.ToUpper().Contains("SUB"))
                {
                    if (!dtImportData.Columns.Contains("SUBPRODUCT_PUBLISH2WEB"))
                    {
                        dtImportData.Columns.Add("SUBPRODUCT_PUBLISH2WEB");
                        dtImportData.Columns["SUBPRODUCT_PUBLISH2WEB"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_ID"));
                    }
                    if (!dtImportData.Columns.Contains("SUBPRODUCT_PUBLISH2PDF"))
                    {
                        dtImportData.Columns.Add("SUBPRODUCT_PUBLISH2PDF");
                        dtImportData.Columns["SUBPRODUCT_PUBLISH2PDF"].SetOrdinal(dtImportData.Columns.IndexOf("SUBPRODUCT_PUBLISH2WEB"));
                    }
                    if (!dtImportData.Columns.Contains("SUBPRODUCT_PUBLISH2EXPORT"))
                    {
                        dtImportData.Columns.Add("SUBPRODUCT_PUBLISH2EXPORT");
                        dtImportData.Columns["SUBPRODUCT_PUBLISH2EXPORT"].SetOrdinal(dtImportData.Columns.IndexOf("SUBPRODUCT_PUBLISH2PDF"));
                    }
                    if (!dtImportData.Columns.Contains("SUBPRODUCT_PUBLISH2PRINT"))
                    {
                        dtImportData.Columns.Add("SUBPRODUCT_PUBLISH2PRINT");
                        dtImportData.Columns["SUBPRODUCT_PUBLISH2PRINT"].SetOrdinal(dtImportData.Columns.IndexOf("SUBPRODUCT_PUBLISH2EXPORT"));
                    }
                    if (!dtImportData.Columns.Contains("SUBPRODUCT_PUBLISH2PORTAL"))
                    {
                        dtImportData.Columns.Add("SUBPRODUCT_PUBLISH2PORTAL");
                        dtImportData.Columns["SUBPRODUCT_PUBLISH2PORTAL"].SetOrdinal(dtImportData.Columns.IndexOf("SUBPRODUCT_PUBLISH2PRINT"));
                    }
                }
                else
                {

                    if (!dtImportData.Columns.Contains("FAMILY_PUBLISH2WEB"))
                    {
                        dtImportData.Columns.Add("FAMILY_PUBLISH2WEB");
                        dtImportData.Columns["FAMILY_PUBLISH2WEB"].SetOrdinal(dtImportData.Columns.IndexOf("SUBFAMILY_ID") + 1);
                    }
                    if (!dtImportData.Columns.Contains("FAMILY_PUBLISH2PDF"))
                    {
                        dtImportData.Columns.Add("FAMILY_PUBLISH2PDF");
                        dtImportData.Columns["FAMILY_PUBLISH2PDF"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_PUBLISH2WEB"));
                    }
                    if (!dtImportData.Columns.Contains("FAMILY_PUBLISH2EXPORT"))
                    {
                        dtImportData.Columns.Add("FAMILY_PUBLISH2EXPORT");
                        dtImportData.Columns["FAMILY_PUBLISH2EXPORT"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_PUBLISH2PDF"));
                    }
                    if (!dtImportData.Columns.Contains("FAMILY_PUBLISH2PRINT"))
                    {
                        dtImportData.Columns.Add("FAMILY_PUBLISH2PRINT");
                        dtImportData.Columns["FAMILY_PUBLISH2PRINT"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_PUBLISH2EXPORT"));
                    }
                    if (!dtImportData.Columns.Contains("FAMILY_PUBLISH2PORTAL"))
                    {
                        dtImportData.Columns.Add("FAMILY_PUBLISH2PORTAL");
                        dtImportData.Columns["FAMILY_PUBLISH2PORTAL"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_PUBLISH2PRINT"));
                    }
                    if (!dtImportData.Columns.Contains("CATEGORY_ID"))
                    {
                        dtImportData.Columns.Add("CATEGORY_ID");
                        dtImportData.Columns["CATEGORY_ID"].SetOrdinal(dtImportData.Columns.IndexOf("CATEGORY_NAME"));

                    }
                    if (importType != "Categories")
                    {
                        if (!dtImportData.Columns.Contains("FAMILY_ID"))
                        {
                            dtImportData.Columns.Add("FAMILY_ID");
                            dtImportData.Columns["FAMILY_ID"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_NAME"));
                        }
                        if (!dtImportData.Columns.Contains("SUBFAMILY_ID"))
                        {
                            dtImportData.Columns.Add("SUBFAMILY_ID");
                            dtImportData.Columns["SUBFAMILY_ID"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_NAME") + 1);
                        }
                        if (!dtImportData.Columns.Contains("SUBFAMILY_NAME"))
                        {
                            dtImportData.Columns.Add("SUBFAMILY_NAME");
                            dtImportData.Columns["SUBFAMILY_NAME"].SetOrdinal(dtImportData.Columns.IndexOf("SUBFAMILY_ID") + 1);
                        }
                    }
                    if (!dtImportData.Columns.Contains("CATEGORY_PUBLISH2WEB"))
                    {
                        dtImportData.Columns.Add("CATEGORY_PUBLISH2WEB");
                        dtImportData.Columns["CATEGORY_PUBLISH2WEB"].SetOrdinal(dtImportData.Columns.IndexOf("FAMILY_ID"));
                    }
                    if (!dtImportData.Columns.Contains("CATEGORY_PUBLISH2PDF"))
                    {
                        dtImportData.Columns.Add("CATEGORY_PUBLISH2PDF");
                        dtImportData.Columns["CATEGORY_PUBLISH2PDF"].SetOrdinal(dtImportData.Columns.IndexOf("CATEGORY_PUBLISH2WEB"));
                    }
                    if (!dtImportData.Columns.Contains("CATEGORY_PUBLISH2EXPORT"))
                    {
                        dtImportData.Columns.Add("CATEGORY_PUBLISH2EXPORT");
                        dtImportData.Columns["CATEGORY_PUBLISH2EXPORT"].SetOrdinal(dtImportData.Columns.IndexOf("CATEGORY_PUBLISH2PDF"));
                    }
                    if (!dtImportData.Columns.Contains("CATEGORY_PUBLISH2PRINT"))
                    {
                        dtImportData.Columns.Add("CATEGORY_PUBLISH2PRINT");
                        dtImportData.Columns["CATEGORY_PUBLISH2PRINT"].SetOrdinal(dtImportData.Columns.IndexOf("CATEGORY_PUBLISH2EXPORT"));
                    }
                    if (!dtImportData.Columns.Contains("CATEGORY_PUBLISH2PORTAL"))
                    {
                        dtImportData.Columns.Add("CATEGORY_PUBLISH2PORTAL");
                        dtImportData.Columns["CATEGORY_PUBLISH2PORTAL"].SetOrdinal(dtImportData.Columns.IndexOf("CATEGORY_PUBLISH2PRINT"));
                    }

                }
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTBATCHPRODUCT_" + batchId + "'')BEGIN DROP TABLE [##IMPORTBATCHPRODUCT_" + batchId + "] END')";
                    SqlCommand sqlCommand = new SqlCommand(sqlString, con);
                    sqlCommand.ExecuteNonQuery();
                    sqlString = importController.CreateTable("[##IMPORTBATCHPRODUCT_" + batchId + "]", excelPath, SheetName, dtImportData);
                    sqlCommand = new SqlCommand(sqlString, con);
                    sqlCommand.ExecuteNonQuery();
                    //var sqlBulkCopy = new SqlBulkCopy(con)
                    //{
                    //    DestinationTableName = "[##IMPORTBATCHPRODUCT_" + batchId + "]"
                    //};
                    //sqlBulkCopy.WriteToServer(dtImportData);
                    DataTable importData = new DataTable();
                    if (importType == "FAMILY_IMPORT_WITHOUT_HIERARCHY" || importType.ToUpper() == "FAMILIES")
                    {
                        importData = UnPivotTableFamily(dtImportData, "FAMILIES");
                    }
                    else
                    {
                        importData = UnPivotTable(dtImportData, importType == "SUBCATALOG_ITEM_NO" ? "SUBPRODUCTS" + "_BATCH_BULK" : importType + "_BATCH_BULK");
                    }

                    SqlCommand dbCommandTemp = new SqlCommand();
                    if (importType == "FAMILY_IMPORT_WITHOUT_HIERARCHY" || importType.ToUpper() == "FAMILIES")
                    {
                        dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTFAMILYTEMPDATA", con) { CommandTimeout = 0 };
                    }
                    else
                    {
                        if (importType.ToUpper().Contains("SUB"))
                            dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTSUBTEMPDATABULK", con) { CommandTimeout = 0 };
                        else if (importType.ToUpper().Contains("PROD") || importType.ToUpper().Contains("CATALOG_ITEM_NO"))
                            dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTTEMPDATA", con) { CommandTimeout = 0 };
                    }
                    dbCommandTemp.CommandType = CommandType.StoredProcedure;
                    dbCommandTemp.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = importData;
                    dbCommandTemp.Parameters.Add("@TABLENAME", SqlDbType.NChar).Value = "[##IMPORTBATCHPRODUCT_" + batchId + "]";
                    dbCommandTemp.ExecuteNonQuery();
                    sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTBATCHATTRIBUTE_" + batchId + "'')BEGIN DROP TABLE [##IMPORTBATCHATTRIBUTE_" + batchId + "] END')";
                    sqlCommand = new SqlCommand(sqlString, con);
                    sqlCommand.ExecuteNonQuery();
                    dtAttributeData = importController.SelectedColumnsToImport(dtImportData, model);
                    sqlString = importController.CreateTable("[##IMPORTBATCHATTRIBUTE_" + batchId + "]", excelPath, SheetName, dtAttributeData);
                    sqlCommand = new SqlCommand(sqlString, con);
                    sqlCommand.ExecuteNonQuery();
                    var sqlBulkCopy = new SqlBulkCopy(con)
                    {
                        DestinationTableName = "[##IMPORTBATCHATTRIBUTE_" + batchId + "]"
                    };
                    sqlBulkCopy.WriteToServer(dtAttributeData);
                    sqlString = "EXEC('STP_LS_IMPORTBATCH ''" + batchId + "''," + customerId + "," + allowDuplicate + ",''" + importType + "'',''" + User.Identity.Name + "'',''" + sessionId + "''," + catalogId + ",''INSERT'',''" + scheduleDate + "''')";
                    sqlCommand = new SqlCommand(sqlString, con);
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.ExecuteNonQuery();

                    var batchPath = ConfigurationManager.AppSettings["Batchprocess"].ToString();
                    Process[] runningProcess = Process.GetProcessesByName("BatchProcess");
                    if (runningProcess.Length > 0)
                    {
                        return "Import is already running.Please wait for few minutes";
                    }
                    else
                    {
                        return "Import started";
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : FinishImportBatch", ex);
                return null;
            }
        }

        /// <summary>
        /// Product import
        /// </summary>
        /// <param name="SheetName">Selected sheet from imported excel sheet</param>
        /// <param name="allowDuplicate">(0/1)1-Insert,0-Associate</param>
        /// <param name="excelPath">imported excel sheet path</param>
        /// <param name="catalogId">current catalog id</param>
        /// <param name="importType">selected import type</param>
        /// <param name="model">Attribute list with name and type</param>
        /// <returns>import status - success/Failure with session id</returns>
        public string FinishProductImport(string SheetName, string allowDuplicate, string excelPath, int catalogId, string importType, int templateId, JArray model)
        {
            ProgressBar progressBar = new ProgressBar();
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            List<object> newList = new List<object>();
            DataSet dsGetProdCnt = new DataSet();
            int importCount = 0;
            importController.ImportStatus("Called");
            int importProductCount = 0;
            int userProductCount = 0;
            int userSKUProductCount = 0;
            QueryValues queryValues = new QueryValues();
            // CustomerItemNo
            if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
            {
                var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                int customerIds;
                int.TryParse(customerDetails.ToString(), out customerIds);
                string customerCatalog = queryValues.GetCatalogItemNo(customerIds);
                System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerCatalog;
            }
            // CustomerSubItemNo
            if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
            {
                var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                int customerId;
                int.TryParse(customerDetails.ToString(), out customerId);
                string customerCatalog = queryValues.GetSubCatalogItemNo(customerId);
                System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerCatalog;
            }
            try
            {
                var customerId = 0;
                using (objLS = new CSEntities())
                {
                    var skucnt = objLS.TB_PLAN
                       .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                       .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                       .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                    var SKUcount = skucnt.Select(a => a).ToList();
                    //Item# import Process Start
                    string customAttributeName = HttpContext.Current.Session["CustomerItemNo"].ToString();
                    DataTable attrName = importController.SelectedColumnsToImport(new DataTable(), model);
                    string[] fieldNames = attrName == null ? null : attrName.Rows.Count == 0 ? null : attrName.AsEnumerable().Select(x => x.Field<string>("ATTRIBUTE_NAME")).ToArray();
                    DataTable itemImport = ValidateItemNumber(GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", ""));

                    // To delete the coloums for pdf xpress 

                    if (itemImport != null)
                    {
                        if (itemImport.Columns.Contains("Catalog_PdfTemplate"))
                        {
                            itemImport.Columns.Remove("Catalog_PdfTemplate");
                            itemImport.Columns.Remove("Category_PdfTemplate");
                            itemImport.Columns.Remove("Family_PdfTemplate");
                            itemImport.Columns.Remove("Product_PdfTemplate");

                        }
                    }

                    if (itemImport != null && itemImport.Rows.Count > 0)
                    {
                        foreach (string fieldName in fieldNames)
                        {
                            foreach (DataColumn colName in itemImport.Columns)
                            {
                                if (colName.ColumnName.ToString().ToUpper() == fieldName.ToUpper().Replace(".", "#"))
                                {
                                    colName.ColumnName = fieldName;
                                }
                            }
                        }
                        return CatalogItemNumberImport(itemImport, SheetName, allowDuplicate, excelPath, catalogId, importType, model, importTemp, templateId);
                    }
                    //Item# import process stop
                    ////Familyname import starts
                    DataTable attrNameFamily = importController.SelectedFamilyColumnsToImport(new DataTable(), model);
                    string[] fieldNamesFamily = attrNameFamily == null ? null : attrNameFamily.Rows.Count == 0 ? null : attrNameFamily.AsEnumerable().Select(x => x.Field<string>("ATTRIBUTE_NAME")).ToArray();
                    DataTable familyNameImport = ValidateFamilyData(GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", ""));

                    // To delete the coloums for pdf xpress 
                    if (familyNameImport != null)
                    {
                        if (familyNameImport.Columns.Contains("Catalog_PdfTemplate"))
                        {
                            familyNameImport.Columns.Remove("Catalog_PdfTemplate");
                            familyNameImport.Columns.Remove("Category_PdfTemplate");
                            familyNameImport.Columns.Remove("Family_PdfTemplate");
                            familyNameImport.Columns.Remove("Product_PdfTemplate");

                        }
                    }






                    if (familyNameImport != null && familyNameImport.Rows.Count > 0)
                    {
                        foreach (string fieldName in fieldNamesFamily)
                        {
                            foreach (DataColumn colName in familyNameImport.Columns)
                            {
                                if (colName.ColumnName.ToString().ToUpper() == fieldName.ToUpper().Replace(".", "#"))
                                {
                                    colName.ColumnName = fieldName;
                                }
                            }
                        }
                        return CatalogFamilyNameImport(familyNameImport, SheetName, allowDuplicate, excelPath, catalogId, importType, model, importTemp, templateId);
                    }
                    ////Familyname import stop
                    if (importType != "Families" && importType != "Categories")
                    {
                        if (allowDuplicate == "1")
                        {
                            DataTable dtGetProdCnt = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "");
                            if (importType.ToUpper().Contains("PRODUCT") && dtGetProdCnt.Columns.Contains("ITEM#"))
                            {
                                dtGetProdCnt.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                            }
                            if (importType.ToUpper().Contains("PRODUCT"))
                            {
                                foreach (DataColumn dcReplaceColumn in dtGetProdCnt.Columns)
                                {
                                    if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                                    {
                                        dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                        break;
                                    }
                                }
                            }
                            dsGetProdCnt.Tables.Add(dtGetProdCnt.Select(" isnull(Catalog_item_no,'') <> ''").CopyToDataTable());
                        }
                        else
                        {
                            DataTable dtGetProdCnt = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "");
                            if (importType.ToUpper().Contains("PRODUCT"))
                            {
                                foreach (DataColumn dcReplaceColumn in dtGetProdCnt.Columns)
                                {
                                    if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                                    {
                                        dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                        break;
                                    }
                                }
                            }
                            if (!dtGetProdCnt.Columns.Contains("CATALOG_ID"))
                            {
                                dtGetProdCnt.Columns.Add("CATALOG_ID");
                                dtGetProdCnt.Columns["CATALOG_ID"].SetOrdinal(0);
                            }
                            if (!dtGetProdCnt.Columns.Contains("CATEGORY_ID"))
                            {
                                dtGetProdCnt.Columns.Add("CATEGORY_ID");
                                dtGetProdCnt.Columns["CATEGORY_ID"].SetOrdinal(2);
                            }
                            if (!dtGetProdCnt.Columns.Contains("FAMILY_ID"))
                            {
                                dtGetProdCnt.Columns.Add("FAMILY_ID");
                                dtGetProdCnt.Columns["FAMILY_ID"].SetOrdinal(4);

                            }
                            if (!dtGetProdCnt.Columns.Contains("SUBFAMILY_ID"))
                            {
                                dtGetProdCnt.Columns.Add("SUBFAMILY_ID");
                                dtGetProdCnt.Columns["SUBFAMILY_ID"].SetOrdinal(6);

                            }
                            if (!dtGetProdCnt.Columns.Contains("PRODUCT_ID"))
                            {
                                dtGetProdCnt.Columns.Add("PRODUCT_ID");
                                dtGetProdCnt.Columns["PRODUCT_ID"].SetOrdinal(8);
                            }
                            //if (importType.ToUpper().Contains("FAM"))
                            //{
                            //    if (!dtGetProdCnt.Columns.Contains("CATEGORY_PUBLISH2WEB"))
                            //        dtGetProdCnt.Columns.Add("CATEGORY_PUBLISH2WEB");
                            //    dtGetProdCnt.Columns["CATEGORY_PUBLISH2WEB"].SetOrdinal(dtGetProdCnt.Columns.IndexOf("FAMILY_ID"));
                            //    if (!dtGetProdCnt.Columns.Contains("CATEGORY_PUBLISH2PDF"))
                            //        dtGetProdCnt.Columns.Add("CATEGORY_PUBLISH2PDF");
                            //    dtGetProdCnt.Columns["CATEGORY_PUBLISH2PDF"].SetOrdinal(dtGetProdCnt.Columns.IndexOf("CATEGORY_PUBLISH2WEB"));
                            //    if (!dtGetProdCnt.Columns.Contains("CATEGORY_PUBLISH2EXPORT"))
                            //        dtGetProdCnt.Columns.Add("CATEGORY_PUBLISH2EXPORT");
                            //    dtGetProdCnt.Columns["CATEGORY_PUBLISH2EXPORT"].SetOrdinal(dtGetProdCnt.Columns.IndexOf("CATEGORY_PUBLISH2PDF"));
                            //    if (!dtGetProdCnt.Columns.Contains("CATEGORY_PUBLISH2PRINT"))
                            //        dtGetProdCnt.Columns.Add("CATEGORY_PUBLISH2PRINT");
                            //    dtGetProdCnt.Columns["CATEGORY_PUBLISH2PRINT"].SetOrdinal(dtGetProdCnt.Columns.IndexOf("CATEGORY_PUBLISH2EXPORT"));
                            //    if (!dtGetProdCnt.Columns.Contains("CATEGORY_PUBLISH2PORTAL"))
                            //        dtGetProdCnt.Columns.Add("CATEGORY_PUBLISH2PORTAL");
                            //    dtGetProdCnt.Columns["CATEGORY_PUBLISH2PORTAL"].SetOrdinal(dtGetProdCnt.Columns.IndexOf("CATEGORY_PUBLISH2PRINT"));
                            //}
                            // string item = dtGetProdCnt.Columns["ITEM#"].ToString();//Converting Item# into ITEM# in sheet
                            if (importType.ToUpper().Contains("PRODUCT"))
                            {
                                foreach (DataColumn dcRename in dtGetProdCnt.Columns)
                                {
                                    if (dcRename.ColumnName.ToUpper() == "ITEM#")
                                    {
                                        dtGetProdCnt.Columns[dcRename.ColumnName].ColumnName = "CATALOG_ITEM_NO";
                                        break;
                                    }
                                }
                            }
                            DataRow[] drow = dtGetProdCnt.Select("PRODUCT_ID IS NULL  and isnull(Catalog_item_no,'') <> ''");
                            if (drow.Count() > 0)
                            {
                                dsGetProdCnt.Tables.Add(dtGetProdCnt.Select("PRODUCT_ID IS NULL  and isnull(Catalog_item_no,'') <> ''").CopyToDataTable());
                                if (SKUcount.Any())
                                {
                                    userSKUProductCount = Convert.ToInt32(SKUcount[0].SKU_COUNT);
                                    customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                                }

                                if (Convert.ToInt32(dsGetProdCnt.Tables[0].Rows.Count) > 0)
                                {
                                    importProductCount = dsGetProdCnt.Tables[0].Rows.Count;
                                    var productcount = objLS.STP_LS_GET_PRODUCTS_BY_USER("PRODUCT", User.Identity.Name, "").ToList();

                                    if (productcount.Any())
                                    {
                                        userProductCount = Convert.ToInt32(productcount[0]);
                                    }

                                    if ((userProductCount + importProductCount) > userSKUProductCount)
                                    {

                                        return "SKU Exceed~" + importTemp;

                                    }
                                }
                                progressBar.progressVale = "10";
                                importCount = dtGetProdCnt.Rows.Count;
                            }
                            else
                            {
                                importCount = dtGetProdCnt.Rows.Count;
                            }

                        }
                    }
                    else
                    {
                        if (SKUcount.Any())
                        {
                            customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                        }
                    }
                    if (customerId == 0)
                    {
                        if (SKUcount.Any())
                        {
                            customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                        }
                    }
                    importExcelSheetSelection(excelPath, SheetName);
                    DataTable excelData = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "");

                    // To delete the coloums for pdf xpress 
                    if (excelData != null)
                    {
                        if (excelData.Columns.Contains("Catalog_PdfTemplate"))
                        {
                            excelData.Columns.Remove("Catalog_PdfTemplate");
                            excelData.Columns.Remove("Category_PdfTemplate");
                            excelData.Columns.Remove("Family_PdfTemplate");
                            excelData.Columns.Remove("Product_PdfTemplate");

                        }
                    }




                    if (importType.ToUpper().Contains("PRODUCT"))
                    {
                        foreach (DataColumn dcReplaceColumn in excelData.Columns)
                        {
                            if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                            {
                                dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                break;
                            }

                        }
                    }
                    if (!excelData.Columns.Contains("CATALOG_ID"))
                    {
                        excelData.Columns.Add("CATALOG_ID");
                        excelData.Columns["CATALOG_ID"].SetOrdinal(0);
                    }
                    if (!excelData.Columns.Contains("CATALOG_NAME"))
                    {
                        excelData.Columns.Add("CATALOG_NAME");
                        excelData.Columns["CATALOG_NAME"].SetOrdinal(1);
                    }
                    if (!excelData.Columns.Contains("CATEGORY_ID"))
                    {
                        excelData.Columns.Add("CATEGORY_ID");
                        excelData.Columns["CATEGORY_ID"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_NAME"));

                    }
                    int col = 1;
                    for (int colindex = 0; colindex < excelData.Columns.Count; colindex++)
                    {
                        if (excelData.Columns[colindex].ColumnName.ToUpper().Contains("SUBCATNAME"))
                        {
                            if (!excelData.Columns.Contains("SUBCATID_L" + col))
                            {
                                excelData.Columns.Add("SUBCATID_L" + col);
                                excelData.Columns["SUBCATID_L" + col].SetOrdinal(excelData.Columns.IndexOf("SUBCATNAME_L" + col));
                                colindex = colindex + 1;
                            }
                            if (excelData.Columns.Contains("SUBCATID_L" + col))
                            {
                                col = col + 1;
                            }
                        }
                    }
                    if (importType != "Categories")
                    {
                        if (!excelData.Columns.Contains("FAMILY_ID"))
                        {
                            excelData.Columns.Add("FAMILY_ID");
                            excelData.Columns["FAMILY_ID"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_NAME"));
                        }
                        if (!excelData.Columns.Contains("SUBFAMILY_ID"))
                        {
                            excelData.Columns.Add("SUBFAMILY_ID");
                            excelData.Columns["SUBFAMILY_ID"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_NAME") + 1);
                        }
                        if (!excelData.Columns.Contains("SUBFAMILY_NAME"))
                        {
                            excelData.Columns.Add("SUBFAMILY_NAME");
                            excelData.Columns["SUBFAMILY_NAME"].SetOrdinal(excelData.Columns.IndexOf("SUBFAMILY_ID") + 1);
                        }
                    }
                    if (!excelData.Columns.Contains("PRODUCT_ID"))
                    {
                        if (importType == "Products")
                        {
                            excelData.Columns.Add("PRODUCT_ID");
                            if (excelData.Columns.Contains("CATALOG_ITEM_NO") == true)
                            {
                                excelData.Columns["PRODUCT_ID"].SetOrdinal(excelData.Columns.IndexOf("CATALOG_ITEM_NO"));
                            }
                        }
                    }
                    //if (importType.ToUpper().Contains("FAM"))
                    //{
                    if (importType != "Categories")
                    {
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2WEB"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2WEB");
                            excelData.Columns["FAMILY_PUBLISH2WEB"].SetOrdinal(excelData.Columns.IndexOf("SUBFAMILY_ID") + 1);
                        }
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2PDF"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2PDF");
                            excelData.Columns["FAMILY_PUBLISH2PDF"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_PUBLISH2WEB"));
                        }
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2EXPORT"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2EXPORT");
                            excelData.Columns["FAMILY_PUBLISH2EXPORT"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_PUBLISH2PDF"));
                        }
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2PRINT"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2PRINT");
                            excelData.Columns["FAMILY_PUBLISH2PRINT"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_PUBLISH2EXPORT"));
                        }
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2PORTAL"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2PORTAL");
                            excelData.Columns["FAMILY_PUBLISH2PORTAL"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_PUBLISH2PRINT"));
                        }
                    }
                    if (importType != "Categories")
                    {
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2WEB"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2WEB");
                            excelData.Columns["CATEGORY_PUBLISH2WEB"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_ID"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PDF"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PDF");
                            excelData.Columns["CATEGORY_PUBLISH2PDF"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2WEB"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2EXPORT"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2EXPORT");
                            excelData.Columns["CATEGORY_PUBLISH2EXPORT"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PDF"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PRINT"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PRINT");
                            excelData.Columns["CATEGORY_PUBLISH2PRINT"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2EXPORT"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PORTAL"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PORTAL");
                            excelData.Columns["CATEGORY_PUBLISH2PORTAL"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PRINT"));
                        }
                    }
                    else
                    {
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2WEB"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2WEB");
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PDF"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PDF");
                            excelData.Columns["CATEGORY_PUBLISH2PDF"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2WEB"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2EXPORT"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2EXPORT");
                            excelData.Columns["CATEGORY_PUBLISH2EXPORT"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PDF"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PRINT"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PRINT");
                            excelData.Columns["CATEGORY_PUBLISH2PRINT"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2EXPORT"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PORTAL"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PORTAL");
                            excelData.Columns["CATEGORY_PUBLISH2PORTAL"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PRINT"));
                        }
                    }

                    if (importType.ToUpper().Contains("PRODUCT"))
                    {
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2EXPORT"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2EXPORT");
                        //excelData.Columns["PRODUCT_PUBLISH2EXPORT"].SetOrdinal(excelData.Columns.IndexOf("PRODUCT_PUBLISH2PDF"));
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2PORTAL"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2PORTAL");
                        excelData.Columns["PRODUCT_PUBLISH2PORTAL"].SetOrdinal(excelData.Columns.IndexOf("PRODUCT_PUBLISH2EXPORT"));
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2WEB"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2WEB");
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2PRINT"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2PRINT");
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2PDF"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2PDF");
                    }
                    // }
                    ImportApiController importApiController = new ImportApiController();
                    if (importType.ToUpper().Contains("PRODUCT") && excelData.Columns.Contains("ITEM#"))
                    {
                        excelData.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                    }
                    excelData = importApiController.UpdateCatalogDetails(importType, catalogId, excelData);
                    DataTable oattType = new DataTable();
                    DataSet replace = new DataSet();
                    int ItemVal = Convert.ToInt32(allowDuplicate);
                    using (var conn = new SqlConnection(connectionString))
                    {
                        conn.Open();
                        SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                        _DBCommand.ExecuteNonQuery();


                        //Mapping attribute -Start-------------------------------------------------------------------

                        //var Mappingattributes = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                        //if (Mappingattributes.Count != 0)
                        //{
                        //    for (int i = 0; i < Mappingattributes.Count; i++)
                        //    {
                        //        for (int j = 0; j < excelData.Columns.Count; j++)
                        //        {
                        //            if (Mappingattributes[i].Excel_Column == excelData.Columns[j].ColumnName)
                        //            {
                        //                excelData.Columns[j].ColumnName = Mappingattributes[i].Mapping_Name;
                        //            }
                        //        }
                        //    }
                        //}


                        // var importTempMappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Select(x => x).Where(x => x.TEMPLATE_ID == templateId).ToList();

                        string str_GetImportType = null;

                        if (importType.ToUpper().Contains("CAT"))
                        {
                            str_GetImportType = "Category";
                        }
                        else if (importType.ToUpper().Contains("FAM"))
                        {
                            str_GetImportType = "Family";
                        }
                        else if (importType.ToUpper().Contains("PRO"))
                        {
                            str_GetImportType = "Product";
                        }


                        bool mappingFlag = false;

                        List<string> lt_MissingMappingAttributes = new List<string>();

                        DataTable dt_MappingAttributesErrorLog = new DataTable();

                        dt_MappingAttributesErrorLog.Columns.Add("SheetName");
                        dt_MappingAttributesErrorLog.Columns.Add("ImportType");
                        dt_MappingAttributesErrorLog.Columns.Add("Attribute(s)");

                        var importTempMappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                      Where(x => x.cs.TEMPLATE_ID == templateId && x.cu.IMPORT_TYPE == str_GetImportType).ToList();

                        if (importTempMappingattributesValue.Count != 0)
                        {
                            for (int j = 0; j < excelData.Columns.Count; j++)
                            {
                                string excelColumnname = Convert.ToString(excelData.Columns[j].ColumnName);
                                //string attributeType = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_TYPE"]);



                                //var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"])  && x.ATTRIBUTE_TYPE!=0).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                                int attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.CAPTION == excelColumnname && x.ATTRIBUTE_ID >= 0 && x.TEMPLATE_ID == templateId).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());
                                if (attributeId != 0)
                                {
                                    var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();

                                    if (attributeName != null)
                                    {
                                        excelData.Columns[j].ColumnName = attributeName;
                                    }

                                    else
                                    {
                                        // excelData.Columns[j].ColumnName = excelColumnname;
                                        _logger.Info("Missing Attributes/Caption in Mapping table: " + excelColumnname);
                                        // return "Import Failed" + "~MappingAttribute:" + "~" + excelColumnname;
                                        lt_MissingMappingAttributes.Add(excelColumnname);
                                        DataRow row = dt_MappingAttributesErrorLog.NewRow();
                                        row["SheetName"] = SheetName;
                                        row["ImportType"] = importType;
                                        row["Attribute(s)"] = excelColumnname;
                                        dt_MappingAttributesErrorLog.Rows.Add(row);
                                        mappingFlag = true;
                                    }
                                }


                            }

                        }

                        //Mapping Attribute - End ---------------------------------------------------------------------

                        SQLString = importController.CreateTable("[##IMPORTTEMP" + importTemp + "]", excelPath, SheetName, excelData);
                        SqlCommand dbCommandnew = new SqlCommand(SQLString, conn);
                        dbCommandnew.ExecuteNonQuery();
                        //bulkCopy.WriteToServer(excelData);

                        // importData = UnPivotTable(excelData, importType + "_bulk");
                        // dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTTEMPDATA", conn) { CommandTimeout = 0 };

                        DataTable importData = new DataTable();
                        SqlCommand dbCommandTemp = new SqlCommand();
                        if (importType.ToUpper() == "FAMILIES")
                        {
                            //importData = UnPivotTableFamily(excelData, importType);
                            dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTFAMILYTEMPDATA", conn) { CommandTimeout = 0 };
                        }
                        else if (importType.ToUpper() == "CATEGORIES")
                        {
                            //importData = UnPivotTableCategory(excelData, importType);
                            dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTCATEGORYTEMPDATA", conn) { CommandTimeout = 0 };
                        }
                        else
                        {
                            //importData = UnPivotTable(excelData, importType + "_bulk");
                            dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTTEMPDATA", conn) { CommandTimeout = 0 };
                        }

                        dbCommandTemp.CommandType = CommandType.StoredProcedure;
                        // dbCommandTemp.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = excelData;
                        dbCommandTemp.Parameters.Add("@TABLENAME", SqlDbType.NChar).Value = "[##IMPORTTEMP" + importTemp + "]";
                        dbCommandTemp.ExecuteNonQuery();

                        DataColumn newCol = new DataColumn("rowImportid", typeof(string));
                        excelData.Columns.Add(newCol);
                        int i = 0;
                        foreach (DataRow row in excelData.Rows)
                        {
                            i++;
                            row["rowImportid"] = i.ToString();
                        }

                        importStatus = "20";
                        using (SqlBulkCopy bulkCopyss = new SqlBulkCopy(conn))
                        {
                            bulkCopyss.DestinationTableName = "[##IMPORTTEMP" + importTemp + "]";


                            foreach (var excelColumn in excelData.Columns)
                            {

                                bulkCopyss.ColumnMappings.Add(excelColumn.ToString(), excelColumn.ToString());

                            }

                            bulkCopyss.WriteToServer(excelData);
                        }
                        using (var connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            SQLString = string.Format("CREATE CLUSTERED INDEX IX_TestTableone ON [##IMPORTTEMP{0}] (rowImportid ASC)", importTemp);
                            SqlCommand sqlCommandIndexing = new SqlCommand(SQLString, connection);
                            sqlCommandIndexing.ExecuteNonQuery();
                        }
                        if (importType.ToUpper() == "CATEGORIES")
                        {
                            SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCATEGORYCOLUMNS ''''" + importTemp + "'''''');END')";

                        }
                        else
                        {
                            SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";

                        }
                        _DBCommand = new SqlCommand(SQLString, conn) { CommandTimeout = 0 };
                        _DBCommand.ExecuteNonQuery();

                        oattType = importController.SelectedColumnsToImport(excelData, model);


                        //Mapping attribute -Start-------------------------------------------------------------------

                        //var MappingattributesValue = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                        //if (MappingattributesValue.Count != 0)
                        //{
                        //    for (int i = 0; i < MappingattributesValue.Count; i++)
                        //    {
                        //        for (int j = 0; j < oattType.Rows.Count; j++)
                        //        {
                        //            if (MappingattributesValue[i].Excel_Column.ToString() == oattType.Rows[j]["ATTRIBUTE_NAME"].ToString())
                        //            {
                        //                oattType.Rows[j][1] = MappingattributesValue[i].Mapping_Name;
                        //            }
                        //        }
                        //    }
                        //}


                        //  // var MappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Select(x => x).Where(x=>x.TEMPLATE_ID== templateId).ToList();

                        //  var MappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                        //Where(x => x.cs.TEMPLATE_ID == templateId && x.cu.IMPORT_TYPE == str_GetImportType).ToList();


                        //  if (MappingattributesValue.Count != 0)
                        //  {
                        //      for (int j = 0; j < oattType.Rows.Count; j++)
                        //      {
                        //          string excelColumnname = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"]);
                        //          string attributeType = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_TYPE"]);

                        //          if (attributeType != "0")
                        //          {

                        //              //var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"])  && x.ATTRIBUTE_TYPE!=0).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                        //              int attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.CAPTION == excelColumnname && x.ATTRIBUTE_ID >= 0 && x.TEMPLATE_ID == templateId).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());

                        //              if (attributeId > 0)
                        //              {
                        //                  var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                        //                  oattType.Rows[j][1] = attributeName;
                        //              }
                        //              else
                        //              {
                        //                  //var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.CAPTION == excelColumnname).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                        //                  //if (attributeName != "")
                        //                  //{ oattType.Rows[j][1] = attributeName; }
                        //                  //else
                        //                  //{
                        //                  _logger.Info("Missing Attributes/Caption in Mapping table: " + excelColumnname);
                        //                  return "Import Failed";
                        //                  //}
                        //              }
                        //          }
                        //      }

                        //  }



                        var MappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                                            Where(x => x.cs.TEMPLATE_ID == templateId && x.cu.IMPORT_TYPE == str_GetImportType).ToList();


                        if (MappingattributesValue.Count != 0)
                        {
                            for (int j = 0; j < oattType.Rows.Count; j++)
                            {
                                string excelColumnname = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"]);
                                string attributeType = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_TYPE"]);

                                if (attributeType != "0")
                                {

                                    //var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"])  && x.ATTRIBUTE_TYPE!=0).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();

                                    var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                         Where(x => x.cs.CAPTION == excelColumnname && x.cs.TEMPLATE_ID == templateId && x.cu.SHEET_NAME == SheetName).ToList();


                                    if (checkcolumns.Count > 0)
                                    {
                                        int attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.CAPTION == excelColumnname && x.ATTRIBUTE_ID >= 0 && x.TEMPLATE_ID == templateId).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());

                                        if (attributeId > 0)
                                        {
                                            var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                                            oattType.Rows[j][1] = attributeName;
                                        }
                                        else if (attributeId == 0)
                                        {
                                            oattType.Rows[j][1] = excelColumnname;
                                        }

                                    }

                                    else
                                    {
                                        //var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.CAPTION == excelColumnname).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                                        //if (attributeName != "")
                                        //{ oattType.Rows[j][1] = attributeName; }
                                        //else
                                        //{
                                        _logger.Info("Missing Attributes/Caption in Mapping table: " + excelColumnname);
                                        // return "Import Failed" + "~MappingAttribute:" + "~" + excelColumnname;
                                        lt_MissingMappingAttributes.Add(excelColumnname);
                                        DataRow row = dt_MappingAttributesErrorLog.NewRow();
                                        row["SheetName"] = SheetName;
                                        row["ImportType"] = importType;
                                        row["Attribute(s)"] = excelColumnname;
                                        dt_MappingAttributesErrorLog.Rows.Add(row);
                                        mappingFlag = true;
                                        //}
                                    }
                                }
                            }

                        }


                        //Mapping Attribute - End ---------------------------------------------------------------------


                        if (mappingFlag == true)
                        {
                            HttpContext.Current.Session["MissingMappingAttribute"] = lt_MissingMappingAttributes;
                            HttpContext.Current.Session["MappingAttributeErrorLog"] = dt_MappingAttributesErrorLog;
                            return "Import Failed" + "~MappingAttribute";

                        }

                        var cmd1 =
                            new SqlCommand(
                                "ALTER TABLE [##IMPORTTEMP" + importTemp +
                                "] ADD Family_Status nvarchar(10),FStatusUI nvarchar(10),StatusUI nvarchar(10),SFSort_Order int,Sort_Order int",
                                conn);
                        cmd1.ExecuteNonQuery();
                        var sqlstring1 =
                            new SqlCommand(
                                "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##AttributeTemp" +
                                importTemp + "]'')BEGIN DROP TABLE [##AttributeTemp" + importTemp + "] END')", conn);
                        sqlstring1.ExecuteNonQuery();
                        var cmd2 = new SqlCommand();
                        cmd2.Connection = conn;
                        SQLString = importController.CreateTableToImport("[##AttributeTemp" + importTemp + "]", oattType);
                        cmd2.CommandText = SQLString;
                        cmd2.CommandType = CommandType.Text;
                        cmd2.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##AttributeTemp" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(oattType);
                        var cmbpk10 = new SqlCommand("update [##AttributeTemp" + importTemp + "] set ATTRIBUTE_TYPE = 1 where ATTRIBUTE_TYPE = 10", conn);
                        cmbpk10.ExecuteNonQuery();

                        var cmbpk1 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1'') IS NOT NULL  DROP TABLE  ##t1') ", conn);
                        cmbpk1.ExecuteNonQuery();
                        var cmbpk2 = new SqlCommand("select * into ##t1  from [##AttributeTemp" + importTemp + "] where attribute_type <>0", conn);
                        cmbpk2.ExecuteNonQuery();
                        var cmbpk3 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2'') IS NOT NULL  DROP TABLE  ##t2') ", conn);
                        cmbpk3.ExecuteNonQuery();
                        var cmbpk4 = new SqlCommand("select * into ##t2  from [##importtemp" + importTemp + "]", conn);
                        cmbpk4.ExecuteNonQuery();
                        //---------------------------------------FOR REPLACING EMPTY VALUES IN TEMP TABLE ---------------------FEB-2017 ------------//
                        var cmd1f12 = new SqlCommand("SELECT * FROM [##importtemp" + importTemp + "]", conn);
                        var daf12 = new SqlDataAdapter(cmd1f12);
                        daf12.Fill(replace);
                        progressBar.progressVale = "30";
                        if (replace.Tables[0].Rows.Count > 0)
                        {
                            foreach (var item1 in replace.Tables[0].Columns)
                            {
                                string column_name2 = item1.ToString();
                                if (column_name2.Contains("'"))
                                {
                                    column_name2 = column_name2.Replace("'", "''''");
                                }
                                var cmbpk41 = new SqlCommand("  exec ('UPDATE [##IMPORTTEMP" + importTemp + "] SET  [" + column_name2 + "] = NULL  WHERE cast( [" + column_name2 + "] as nvarchar) = '''' and  [" + column_name2 + "] is not null ')", conn);
                                cmbpk41.ExecuteNonQuery();
                            }
                        }
                        _logger.Error("Error at Import Start : FinishProductImport");
                        progressBar.progressVale = "50";
                        if (importType.ToUpper() == "FAMILIES")
                        {
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT_HIERARCHY ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                            cmd.ExecuteNonQuery();
                        }
                        else if (importType.ToUpper() == "CATEGORIES")
                        {
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_CATEGORYATTRIBUTEIMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            _logger.Error("Error at Get STP : FinishProductImport");
                            var customers = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                            int maximumSheetLength = objLS.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.ImportSheetProductCount).FirstOrDefault();
                            //int maximumSheetLength = Int32.Parse(ConfigurationManager.AppSettings["ImportSheetProductCount"].ToString());
                            int currentSheetLength = replace.Tables[0].Rows.Count;
                            int noOfIteration = (currentSheetLength / maximumSheetLength) + 1;

                            int importStartCount = 0;
                            int importEndCount = maximumSheetLength;

                            SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";
                            SqlCommand sqlCommand = new SqlCommand(SQLString, conn) { CommandTimeout = 0 };
                            sqlCommand.ExecuteNonQuery();

                            SQLString = string.Format("EXEC('IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE=''U'' AND NAME=''##IMPORTALLDATA{0}'') BEGIN DROP TABLE [##IMPORTALLDATA{0}] END SELECT ROW_NUMBER() OVER (ORDER BY (select 100)) AS RowID,* INTO [##IMPORTALLDATA{0}] FROM [##IMPORTTEMP{0}]')", importTemp);
                            sqlCommand = new SqlCommand(SQLString, conn);
                            sqlCommand.ExecuteNonQuery();

                            while (noOfIteration >= 1)
                            {
                                SQLString = string.Format("EXEC('IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE=''U'' AND NAME=''##IMPORTTEMP{0}'') BEGIN DROP TABLE [##IMPORTTEMP{0}] END')", importTemp);
                                sqlCommand = new SqlCommand(SQLString, conn);
                                sqlCommand.ExecuteNonQuery();

                                SQLString = string.Format("SELECT * INTO [##IMPORTTEMP{0}] FROM [##IMPORTALLDATA{0}] WHERE RowID  between {1} and {2}", importTemp, importStartCount, importEndCount);
                                sqlCommand = new SqlCommand(SQLString, conn);
                                sqlCommand.ExecuteNonQuery();

                                //SQLString = string.Format("CREATE CLUSTERED INDEX IX_TestTableone ON [##IMPORTTEMP{0}] (rowImportid ASC)", importTemp);
                                //sqlCommand = new SqlCommand(SQLString, conn);
                                //sqlCommand.ExecuteNonQuery();

                                SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";
                                sqlCommand = new SqlCommand(SQLString, conn) { CommandTimeout = 0 };
                                sqlCommand.ExecuteNonQuery();
                                _logger.Error("Error at STP start : FinishProductImport");
                                var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                                cmd.ExecuteNonQuery();
                                _logger.Error("Error at End start : FinishProductImport");
                                noOfIteration = noOfIteration - 1;
                                importStartCount = importEndCount + 1;
                                importEndCount = importEndCount + maximumSheetLength;

                            }
                        }
                        DataSet rowaffected = new DataSet();
                        SQLString = string.Empty;
                        if (importType.ToUpper() == "FAMILIES")
                        {
                            SQLString =
                         "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" +
                         importTemp + "'')BEGIN select count(*) as UpdateCount from [##IMPORTTEMP" + importTemp + "]  where Family_Status=''Update'' or Family_Status=''Associate'';select count(*) as InsertCount from [##IMPORTTEMP" + importTemp + "]  where Family_Status=''Insert'' ; END')";
                        }
                        else if (importType.ToUpper() == "CATEGORIES")
                        {
                            SQLString =
                          "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" +
                         importTemp + "'')BEGIN select Count (distinct CATEGORY_ID) as UpdateCount from [##CATEGORYCOUNTDETAILS" + importTemp + "] where FLAG=''U'' ;select Count (distinct CATEGORY_ID) as InsertCount from [##CATEGORYCOUNTDETAILS" + importTemp + "] where FLAG =''I''; END')";
                        }
                        else
                        {
                            _logger.Error("Error at NEW start : FinishProductImport");
                            SQLString =
                         "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" +
                         importTemp + "'')BEGIN select count(*) as UpdateCount from [##IMPORTTEMP" + importTemp + "]  where StatusUI=''Update'' or StatusUI=''Associate'';select count(*) as InsertCount from [##IMPORTTEMP" + importTemp + "]  where StatusUI=''Insert''; END')";
                        }
                        _DBCommand = new SqlCommand(SQLString, conn);
                        SqlDataAdapter dbAdapter = new SqlDataAdapter(_DBCommand);
                        dbAdapter.Fill(rowaffected);
                        progressBar.progressVale = "90";
                        _logger.Error("Error at NEWEND start : FinishProductImport");
                        #region Attribute Pack
                        DataSet attributePackFamily = new DataSet();
                        string QueryString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##TEMP_ATTRIBUTE_PACK_FAMILY" +
                        importTemp + "'')BEGIN select * from [##TEMP_ATTRIBUTE_PACK_FAMILY" + importTemp + "]; END')";
                        _DBCommand = new SqlCommand(QueryString, conn);
                        SqlDataAdapter dbAdapter_AP = new SqlDataAdapter(_DBCommand);
                        dbAdapter_AP.Fill(attributePackFamily);
                        _logger.Error("Error at NEWEND1 start : FinishProductImport");
                        DataSet attributePackCategory = new DataSet();
                        string QueryString_Category =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##TEMP_ATTRIBUTE_PACK_CATEGORY" +
                        importTemp + "'')BEGIN select * from [##TEMP_ATTRIBUTE_PACK_CATEGORY" + importTemp + "]; END')";
                        _DBCommand = new SqlCommand(QueryString_Category, conn);
                        SqlDataAdapter dbAdapter_APCategory = new SqlDataAdapter(_DBCommand);
                        dbAdapter_APCategory.Fill(attributePackCategory);
                        _logger.Error("Error at NEWEND2 start : FinishProductImport");
                        DataSet attributePackProduct = new DataSet();
                        string QueryString_Product =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##TEMP_ATTRIBUTE_PACK_PRODUCT" +
                        importTemp + "'')BEGIN select * from [##TEMP_ATTRIBUTE_PACK_PRODUCT" + importTemp + "]; END')";
                        _DBCommand = new SqlCommand(QueryString_Product, conn);
                        SqlDataAdapter dbAdapter_APProduct = new SqlDataAdapter(_DBCommand);
                        dbAdapter_APProduct.Fill(attributePackProduct);
                        _logger.Error("Error at NEWEND3 start : FinishProductImport");
                        // for category
                        if (attributePackCategory != null && attributePackCategory.Tables.Count > 0 && attributePackCategory.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dtRow in attributePackCategory.Tables[0].Rows)
                            {
                                string category_Short = Convert.ToString(dtRow["CATEGORY_ID"]);
                                string category_Id = objLS.TB_CATEGORY.Where(s => s.CATEGORY_SHORT == category_Short).FirstOrDefault().CATEGORY_ID;
                                List<string> categoryIds = new List<string>();
                                TB_CATEGORY tb_category = new TB_CATEGORY();
                                categoryIds.Add(category_Id);
                                var ds_AP = new DataSet();

                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESIGNEXPORT_IMPORT";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETPARENT";
                                    objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.VarChar).Value = category_Id;
                                    objSqlConnection.Open();
                                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                    objSqlDataAdapter.Fill(ds_AP);
                                }

                                if (ds_AP != null && ds_AP.Tables.Count > 0 && ds_AP.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow dtrow in ds_AP.Tables[0].Rows)
                                    {
                                        if (dtrow["CATEGORY_ID"].ToString() != null)
                                        {
                                            categoryIds.Add(dtrow["CATEGORY_ID"].ToString());
                                        }
                                    }
                                }

                                string catalogIdstring = Convert.ToString(catalogId);
                                categoryIds.Add(catalogIdstring);
                                HomeApiController objHomeApiController = new HomeApiController();
                                List<TB_ATTRIBUTE_HIERARCHY> attributeHierarchyList = objLS.TB_ATTRIBUTE_HIERARCHY.Where(s => categoryIds.Contains(s.ASSIGN_TO)).ToList();
                                foreach (var attributeHierarchy in attributeHierarchyList)
                                {
                                    int packId = Convert.ToInt32(attributeHierarchy.PACK_ID);
                                    objHomeApiController.AssociateAttributePackaging(category_Id, packId, catalogId);
                                }

                            }
                        }

                        // For Family
                        if (attributePackFamily != null && attributePackFamily.Tables.Count > 0 && attributePackFamily.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dtRow in attributePackFamily.Tables[0].Rows)
                            {
                                string category_Id = Convert.ToString(dtRow["CATEGORY_ID"]);
                                int family_Id = Convert.ToInt32(dtRow["FAMILY_ID"]);
                                string familyIdString = Convert.ToString(family_Id);
                                string family_ID = "~" + familyIdString;
                                List<string> categoryIds = new List<string>();
                                TB_CATEGORY tb_category = new TB_CATEGORY();
                                categoryIds.Add(category_Id);
                                var ds_AP = new DataSet();

                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESIGNEXPORT_IMPORT";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETPARENT";
                                    objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.VarChar).Value = category_Id;
                                    objSqlConnection.Open();
                                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                    objSqlDataAdapter.Fill(ds_AP);
                                }

                                if (ds_AP != null && ds_AP.Tables.Count > 0 && ds_AP.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow dtrow in ds_AP.Tables[0].Rows)
                                    {
                                        if (dtrow["CATEGORY_ID"].ToString() != null)
                                        {
                                            categoryIds.Add(dtrow["CATEGORY_ID"].ToString());
                                        }
                                    }
                                }

                                string catalogIdstring = Convert.ToString(catalogId);
                                categoryIds.Add(catalogIdstring);
                                HomeApiController objHomeApiController = new HomeApiController();
                                List<TB_ATTRIBUTE_HIERARCHY> attributeHierarchyList = objLS.TB_ATTRIBUTE_HIERARCHY.Where(s => categoryIds.Contains(s.ASSIGN_TO)).ToList();
                                foreach (var attributeHierarchy in attributeHierarchyList)
                                {
                                    int packId = Convert.ToInt32(attributeHierarchy.PACK_ID);
                                    objHomeApiController.AssociateAttributePackaging(family_ID, packId, catalogId);
                                }
                            }
                        }

                        // For Product
                        if (attributePackProduct != null && attributePackProduct.Tables.Count > 0 && attributePackProduct.Tables[0].Rows.Count > 0)
                        {
                            DataView view = new DataView(attributePackProduct.Tables[0]);
                            DataTable distinctValues = view.ToTable(true, "FAMILY_ID");

                            foreach (DataRow dtRow in distinctValues.Rows)
                            {
                                int family_Id = Convert.ToInt32(dtRow["FAMILY_ID"]);
                                string category_Id = objLS.TB_CATALOG_FAMILY.Where(s => s.CATALOG_ID == catalogId && s.FAMILY_ID == family_Id).FirstOrDefault().CATEGORY_ID;
                                string familyIdString = Convert.ToString(family_Id);
                                string family_ID = "~" + familyIdString;
                                List<string> categoryIds = new List<string>();
                                TB_CATEGORY tb_category = new TB_CATEGORY();
                                categoryIds.Add(category_Id);
                                var ds_AP = new DataSet();

                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESIGNEXPORT_IMPORT";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETPARENT";
                                    objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.VarChar).Value = category_Id;
                                    objSqlConnection.Open();
                                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                    objSqlDataAdapter.Fill(ds_AP);
                                }

                                if (ds_AP != null && ds_AP.Tables.Count > 0 && ds_AP.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow dtrow in ds_AP.Tables[0].Rows)
                                    {
                                        if (dtrow["CATEGORY_ID"].ToString() != null)
                                        {
                                            categoryIds.Add(dtrow["CATEGORY_ID"].ToString());
                                        }
                                    }
                                }

                                string catalogIdstring = Convert.ToString(catalogId);
                                categoryIds.Add(catalogIdstring);
                                categoryIds.Add(familyIdString);
                                HomeApiController objHomeApiController = new HomeApiController();
                                List<TB_ATTRIBUTE_HIERARCHY> attributeHierarchyList = objLS.TB_ATTRIBUTE_HIERARCHY.Where(s => categoryIds.Contains(s.ASSIGN_TO)).ToList();
                                foreach (var attributeHierarchy in attributeHierarchyList)
                                {
                                    int packId = Convert.ToInt32(attributeHierarchy.PACK_ID);
                                    objHomeApiController.AssociateAttributePackaging(family_ID, packId, catalogId);
                                }
                            }
                        }

                        #endregion

                        int insertRecords = 0;
                        int updateRecords = 0;
                        DataTable detachRecords = GetDataFromExcelForDetach(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DETACH");
                        int detachcount = detachRecords.Rows.Count;
                        if (rowaffected != null && rowaffected.Tables.Count > 1)
                        {
                            if (rowaffected.Tables[0].Rows.Count > 0)
                            {
                                updateRecords = int.Parse(rowaffected.Tables[0].Rows[0][0].ToString());
                            }
                            if (rowaffected.Tables[1].Rows.Count > 0)
                            {
                                insertRecords = int.Parse(rowaffected.Tables[1].Rows[0][0].ToString());
                            }
                        }
                        //if (!(importCount == 0))
                        //{
                        //    updateRecords = importCount - detachcount;
                        //}

                        //if (updateRecords == 0 && insertRecords == 0)
                        //{
                        //    updateRecords = importCount;
                        //}
                        importController._SQLString = @" if exists(select NAME from TEMPDB.sys.objects where type='u' and name='##LOGTEMPTABLE" + importTemp + "') BEGIN  select * from [##LOGTEMPTABLE" + importTemp + "] End else  Begin select 'Import Success' End ";
                        DataSet ds = importController.CreateDataSet();
                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0][0].ToString() != "Import Success")
                            {
                                importController._SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp + "]";
                                cmd1.CommandText = importController._SQLString;
                                cmd1.CommandType = CommandType.Text;
                                cmd1.ExecuteNonQuery();
                            }
                        }
                        if (ds.Tables[0].Columns[0].ColumnName == "ErrorMessage")
                        {
                            return "Import Failed~" + importTemp;
                        }
                        else
                        {
                            if (importType.ToUpper() == "PRODUCTS")
                            {
                                var picklistvaluecreation = objLS.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerId);//To check Picklist validation in enabled or not in customer setting

                                if (picklistvaluecreation != null && picklistvaluecreation.ShowCustomAPPS)
                                {
                                    PickListUpdate(importTemp);
                                }
                            }
                            DataTable skipped = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "SKIP");

                            // To delete the coloums for pdf xpress 

                            if (skipped != null)
                            {
                                if (skipped.Columns.Contains("Catalog_PdfTemplate"))
                                {
                                    skipped.Columns.Remove("Catalog_PdfTemplate");
                                    skipped.Columns.Remove("Category_PdfTemplate");
                                    skipped.Columns.Remove("Family_PdfTemplate");
                                    skipped.Columns.Remove("Product_PdfTemplate");

                                }
                            }



                            // To implement the Pdf xpress Import.  -- Start Mariyvijayan


                            DataTable dt_PdfXpressData = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "");

                            FullImportPdfXpress(dt_PdfXpressData);

                            // End





                            if (skipped != null && skipped.Rows.Count > 0)
                            {
                                return "Import Success~" + importTemp + "~InsertRecords:" + insertRecords + "~" + "UpdateRecords:" + updateRecords + "~SkippedRecords:" + skipped.Rows.Count;
                            }
                            else
                            {
                                return "Import Success~" + importTemp + "~InsertRecords:" + insertRecords + "~" + "UpdateRecords:" + updateRecords + "~SkippedRecords:0";
                            }
                        }
                        if (conn.State.ToString().ToUpper() == "OPEN")
                            conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at AdvanceImportApiController : FinishProductImport", ex);
                return null;
            }
        }

        /// <summary>
        /// In this method used to insert a pdf xpress data into correspoding values.
        /// </summary>
        /// <param name="dt_PdfXpressData">
        /// by :Mariyvijayan  26/10/2018
        /// </param>
        public void FullImportPdfXpress(DataTable dt_PdfXpressData)
        {
            try
            {
                string customer_Name = User.Identity.Name;
                var userId = objLS.Customer_User.Join(objLS.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();
                string catalogName = dt_PdfXpressData.AsEnumerable().Select(x => x.Field<string>("CATALOG_NAME")).FirstOrDefault();
                int CatalogId = objLS.TB_CATALOG.Where(x => x.CATALOG_NAME == catalogName).Select(x => x.CATALOG_ID).FirstOrDefault();

                var rows_PdfValue = objLS.TB_PDFXPRESS_HIERARCHY.Where(x => x.CATALOG_ID == CatalogId).Select(r => r.CATALOG_ID).FirstOrDefault();





                //var rows_PdfValue = dt_PdfXpressData.AsEnumerable()
                //    .Where(r => r.Field<string>("Catalog_PdfTemplate") != null)
                //    .Select(x => x.Field<string>("Catalog_PdfTemplate"))
                //    .FirstOrDefault();

                if (rows_PdfValue != null)
                {

                    if (dt_PdfXpressData.Columns.Contains("Category_PdfTemplate"))
                    {
                        //  PdfXpress_CatalogImport(dt_PdfXpressData, customer_Name, userId, CatalogId);
                        PdfXpress_CategoryImport(dt_PdfXpressData, customer_Name, userId, CatalogId);
                    }

                    if (dt_PdfXpressData.Columns.Contains("Family_PdfTemplate"))
                    {
                        PdfXpress_FamilyImport(dt_PdfXpressData, customer_Name, userId, CatalogId);
                        PdfXpress_ProductImport(dt_PdfXpressData, customer_Name, userId, CatalogId);
                    }

                }
            }
            catch (Exception ex)
            {

                // Got Error;
            }

        }



        /// <summary>
        /// In this method to insert catalog pdf xpress values in to pdfxpress tab;e
        /// </summary>
        /// <param name="dt_PdfXpressData"></param>
        /// <param name="customer_Name"></param>
        /// <param name="userId"></param>
        /// <param name="CatalogId"></param>
        public void PdfXpress_CatalogImport(DataTable dt_PdfXpressData, string customer_Name, string userId, int CatalogId)
        {

            string catalog_Id = CatalogId.ToString();
            string flePath = string.Empty;
            string fileName = string.Empty;
            string fullFileName = string.Empty;
            DataTable CatalogPdfXpress = new DataTable();

            CatalogPdfXpress = dt_PdfXpressData.AsEnumerable().Where(r => r.Field<string>("Catalog_PdfTemplate") != null).Select(x => x).CopyToDataTable();


            // Savedt_PdfXpressDatas("Catalog", CatalogPdfXpress);

            flePath = dt_PdfXpressData.AsEnumerable().Where(x => x.Field<string>("Catalog_PdfTemplate") != null).Select(x => x.Field<string>("Catalog_PdfTemplate")).FirstOrDefault();
            if (flePath != null)


                fullFileName = flePath.ToString().Replace(@"PDF_Template\", "");

            fileName = CatalogId + "_" + fullFileName;
            flePath = flePath.Replace(fullFileName, fileName);


            if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template")))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template"));
            }
            // When the values is already in a table
            var pdfXpressdet = objLS.TB_PDFXPRESS_HIERARCHY.FirstOrDefault(a => a.CATALOG_ID == CatalogId && a.ASSIGN_TO == catalog_Id && a.CUSTOMER_ID == 0 && a.TYPE == "Catalog" && a.TEMPLATE_NAME == fileName && a.TEMPLATE_PATH == flePath);
            if (pdfXpressdet == null)
            {
                var pdfXpressdetValues = objLS.TB_PDFXPRESS_HIERARCHY.FirstOrDefault(a => a.CATALOG_ID == CatalogId && a.ASSIGN_TO == catalog_Id && a.CUSTOMER_ID == 0 && a.TYPE == "Catalog");
                if (pdfXpressdetValues != null)
                {
                    objLS.TB_PDFXPRESS_HIERARCHY.Remove(pdfXpressdetValues);
                    objLS.SaveChanges();
                }
                var objPdfXpress = new TB_PDFXPRESS_HIERARCHY();
                objPdfXpress.PROJECT_ID = 0;
                objPdfXpress.TEMPLATE_NAME = fileName;
                objPdfXpress.TEMPLATE_PATH = flePath;
                objPdfXpress.TYPE = "Catalog";
                objPdfXpress.ASSIGN_TO = catalog_Id;
                objPdfXpress.CUSTOMER_ID = 0;
                objPdfXpress.CATALOG_ID = CatalogId;
                objPdfXpress.CREATED_USER = customer_Name;
                objPdfXpress.CREATED_DATE = DateTime.Now;
                objPdfXpress.MODIFIED_USER = customer_Name;
                objPdfXpress.MODIFIED_DATE = DateTime.Now;
                objLS.TB_PDFXPRESS_HIERARCHY.Add(objPdfXpress);
                objLS.SaveChanges();
            }
        }

        /// <summary>
        ///  In this method to insert category pdf xpress values in to pdfxpress table
        /// </summary>
        /// <param name="dt_PdfXpressData"></param>
        /// <param name="customer_Name"></param>
        /// <param name="userId"></param>
        /// <param name="CatalogId">
        /// by :Mariyvijayan  26/10/2018
        /// </param>
        public void PdfXpress_CategoryImport(DataTable dt_PdfXpressData, string customer_Name, string userId, int CatalogId)
        {

            DataTable CategoryPdfXpress = new DataTable();
            string flePath = string.Empty;
            string fileName = string.Empty;
            string categorySort = string.Empty;
            string Id = string.Empty;
            string cat_Id = CatalogId.ToString();
            string categoryName = string.Empty;


            CategoryPdfXpress = dt_PdfXpressData.AsEnumerable()
           .Where(p => p.Field<string>("Category_PdfTemplate") != null)
           .GroupBy(x => x.Field<string>("CATEGORY_NAME"))
           .Select(x => x.First()).CopyToDataTable();


            if (!CategoryPdfXpress.Columns.Contains("CATEGORY_ID"))
            {
                CategoryPdfXpress.Columns.Add("CATEGORY_ID");
            }


            if (CategoryPdfXpress.Rows.Count > 0)
            {
                foreach (DataRow dr in CategoryPdfXpress.Rows)
                {

                    flePath = dr["Category_PdfTemplate"].ToString();
                    fileName = flePath.ToString().Replace(@"PDF_Template\", "");
                    categoryName = dr["CATEGORY_NAME"].ToString();

                    if (string.IsNullOrEmpty(dr["CATEGORY_ID"].ToString()))
                    {

                        categorySort = objLS.TB_CATEGORY.Join(objLS.TB_CATALOG_SECTIONS, F => F.CATEGORY_ID, c => c.CATEGORY_ID, (F, C) => new { F, C }).
                               Where(x => x.F.CATEGORY_NAME == categoryName && x.C.CATALOG_ID == CatalogId).
                               Select(x => x.F.CATEGORY_SHORT).FirstOrDefault();

                    }
                    else
                    {
                        categorySort = dr["CATEGORY_ID"].ToString();
                    }

                    Id = objLS.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == categorySort).Select(r => r.CATEGORY_ID).SingleOrDefault();

                    // When the values is already in a table
                    var pdfXpressdet = objLS.TB_PDFXPRESS_HIERARCHY.FirstOrDefault(a => a.CATALOG_ID == CatalogId && a.ASSIGN_TO == Id && a.CUSTOMER_ID == 0 && a.TYPE == "Category" && a.TEMPLATE_NAME == fileName && a.TEMPLATE_PATH == flePath);
                    if (pdfXpressdet == null)
                    {
                        var pdfXpressdetValues = objLS.TB_PDFXPRESS_HIERARCHY.FirstOrDefault(a => a.CATALOG_ID == CatalogId && a.ASSIGN_TO == Id && a.CUSTOMER_ID == 0 && a.TYPE == "Category");
                        if (pdfXpressdetValues != null)
                        {
                            objLS.TB_PDFXPRESS_HIERARCHY.Remove(pdfXpressdetValues);
                            objLS.SaveChanges();
                        }

                        var objPdfXpress = new TB_PDFXPRESS_HIERARCHY();
                        objPdfXpress.PROJECT_ID = 0;
                        objPdfXpress.TEMPLATE_NAME = fileName;
                        objPdfXpress.TEMPLATE_PATH = flePath;
                        objPdfXpress.TYPE = "Category";
                        objPdfXpress.ASSIGN_TO = Id;
                        objPdfXpress.CUSTOMER_ID = 0;
                        objPdfXpress.CATALOG_ID = CatalogId;
                        objPdfXpress.CREATED_USER = customer_Name;
                        objPdfXpress.CREATED_DATE = DateTime.Now;
                        objPdfXpress.MODIFIED_USER = customer_Name;
                        objPdfXpress.MODIFIED_DATE = DateTime.Now;
                        objLS.TB_PDFXPRESS_HIERARCHY.Add(objPdfXpress);
                        objLS.SaveChanges();

                    }


                }
            }



        }




        /// <summary>
        ///  In this method to insert Family pdf xpress values in to pdfxpress table
        /// </summary>
        /// <param name="dt_PdfXpressData"></param>
        /// <param name="customer_Name"></param>
        /// <param name="userId"></param>
        /// <param name="CatalogId">
        /// by :Mariyvijayan  26/10/2018
        /// </param>
        public void PdfXpress_FamilyImport(DataTable dt_PdfXpressData, string customer_Name, string userId, int CatalogId)
        {

            DataTable FamilyPdfXpress = new DataTable();
            string flePath = string.Empty;
            string fileName = string.Empty;
            string categorySort = string.Empty;
            string categoryName = string.Empty;
            string familyName = string.Empty;
            string Id = string.Empty;
            string cat_Id = CatalogId.ToString();
            List<string> categoryLevels = new List<string> { "SUBCATNAME_L7", "SUBCATNAME_L6", "SUBCATNAME_L5", "SUBCATNAME_L4", "SUBCATNAME_L3", "SUBCATNAME_L2", "SUBCATNAME_L1", "CATEGORY_NAME" };

            foreach (string val in categoryLevels.ToList())
            {
                if (!dt_PdfXpressData.Columns.Contains(val))
                {
                    categoryLevels.Remove(val);
                }
            }

            FamilyPdfXpress = dt_PdfXpressData.AsEnumerable()
                              .Where(x => x.Field<string>("Family_PdfTemplate") != null)
                              .GroupBy(g => g.Field<string>("FAMILY_NAME"))
                              .Select(x => x.First()).CopyToDataTable();


            if (FamilyPdfXpress.Columns.Contains("FAMILY_ID"))
            {
                var checkFamilyIdValue = FamilyPdfXpress.AsEnumerable().Where(x => x.Field<string>("FAMILY_ID") != null).Select(x => x.Field<string>("FAMILY_NAME")).FirstOrDefault();

                if (checkFamilyIdValue == null)
                {
                    FamilyPdfXpress.Columns.Remove("FAMILY_ID");
                }
            }


            if (!FamilyPdfXpress.Columns.Contains("FAMILY_ID"))
            {
                // To add a family_id and update the family_id by category and familyname.
                FamilyPdfXpress.Columns.Add("FAMILY_ID", typeof(int));
                foreach (DataRow dr in FamilyPdfXpress.Rows)
                {
                    foreach (var CategoryVal in categoryLevels)
                    {
                        if (!string.IsNullOrEmpty(dr[CategoryVal].ToString()))
                        {
                            categoryName = dr[CategoryVal].ToString();
                            break;
                        }
                    }

                    familyName = dr["FAMILY_NAME"].ToString();
                    dr["FAMILY_ID"] = objLS.TB_FAMILY.Join(objLS.TB_CATEGORY, F => F.CATEGORY_ID, c => c.CATEGORY_ID, (F, C) => new { F, C }).Where(x => x.F.FAMILY_NAME == familyName && x.C.CATEGORY_NAME == categoryName).Select(x => x.F.FAMILY_ID).FirstOrDefault();
                }
            }


            // ready To import.


            if (FamilyPdfXpress.Rows.Count > 0)
            {
                foreach (DataRow dr in FamilyPdfXpress.Rows)
                {

                    flePath = dr["Family_PdfTemplate"].ToString();
                    fileName = flePath.ToString().Replace(@"PDF_Template\", "");

                    Id = dr["FAMILY_ID"].ToString();

                    // When the values is already in a table
                    var pdfXpressdet = objLS.TB_PDFXPRESS_HIERARCHY.FirstOrDefault(a => a.CATALOG_ID == CatalogId && a.ASSIGN_TO == Id && a.CUSTOMER_ID == 0 && a.TYPE == "Family" && a.TEMPLATE_NAME == fileName && a.TEMPLATE_PATH == flePath);
                    if (pdfXpressdet == null)
                    {
                        var pdfXpressdetValues = objLS.TB_PDFXPRESS_HIERARCHY.FirstOrDefault(a => a.CATALOG_ID == CatalogId && a.ASSIGN_TO == Id && a.CUSTOMER_ID == 0 && a.TYPE == "Family");
                        if (pdfXpressdetValues != null)
                        {
                            objLS.TB_PDFXPRESS_HIERARCHY.Remove(pdfXpressdetValues);
                            objLS.SaveChanges();
                        }

                        var objPdfXpress = new TB_PDFXPRESS_HIERARCHY();
                        objPdfXpress.PROJECT_ID = 0;
                        objPdfXpress.TEMPLATE_NAME = fileName;
                        objPdfXpress.TEMPLATE_PATH = flePath;
                        objPdfXpress.TYPE = "Family";
                        objPdfXpress.ASSIGN_TO = Id;
                        objPdfXpress.CUSTOMER_ID = 0;
                        objPdfXpress.CATALOG_ID = CatalogId;
                        objPdfXpress.CREATED_USER = customer_Name;
                        objPdfXpress.CREATED_DATE = DateTime.Now;
                        objPdfXpress.MODIFIED_USER = customer_Name;
                        objPdfXpress.MODIFIED_DATE = DateTime.Now;
                        objLS.TB_PDFXPRESS_HIERARCHY.Add(objPdfXpress);
                        objLS.SaveChanges();

                    }
                }
            }
        }


        /// <summary>
        ///  In this method to insert Product pdf xpress values in to pdfxpress table
        /// </summary>
        /// <param name="dt_PdfXpressData"></param>
        /// <param name="customer_Name"></param>
        /// <param name="userId"></param>
        /// <param name="CatalogId">
        /// by :Mariyvijayan  26/10/2018
        /// </param>
        public void PdfXpress_ProductImport(DataTable dt_PdfXpressData, string customer_Name, string userId, int CatalogId)
        {
            DataTable ProductPdfXpres = new DataTable();
            string flePath = string.Empty;
            string fileName = string.Empty;
            string categorySort = string.Empty;
            string categoryName = string.Empty;
            string familyName = string.Empty;
            string Id = string.Empty;
            string cat_Id = CatalogId.ToString();



            List<string> categoryLevels = new List<string> { "SUBCATNAME_L7", "SUBCATNAME_L6", "SUBCATNAME_L5", "SUBCATNAME_L4", "SUBCATNAME_L3", "SUBCATNAME_L2", "SUBCATNAME_L1", "CATEGORY_NAME" };

            foreach (string val in categoryLevels.ToList())
            {
                if (!dt_PdfXpressData.Columns.Contains(val))
                {
                    categoryLevels.Remove(val);
                }
            }



            ProductPdfXpres = dt_PdfXpressData.AsEnumerable()
                                        .Where(x => x.Field<string>("Product_PdfTemplate") != null)
                                        .GroupBy(g => g.Field<string>("FAMILY_NAME"))
                                        .Select(x => x.First()).CopyToDataTable();



            if (ProductPdfXpres.Columns.Contains("FAMILY_ID"))
            {
                var checkFamilyIdValue = ProductPdfXpres.AsEnumerable().Where(x => x.Field<string>("FAMILY_ID") != null).Select(x => x.Field<string>("FAMILY_NAME")).FirstOrDefault();

                if (checkFamilyIdValue == null)
                {
                    ProductPdfXpres.Columns.Remove("FAMILY_ID");
                }

            }


            if (!ProductPdfXpres.Columns.Contains("FAMILY_ID"))
            {
                // To add a family_id and update the family_id by category and familyname.
                ProductPdfXpres.Columns.Add("FAMILY_ID", typeof(int));
                foreach (DataRow dr in ProductPdfXpres.Rows)
                {

                    foreach (var CategoryVal in categoryLevels)
                    {
                        if (!string.IsNullOrEmpty(dr[CategoryVal].ToString()))
                        {
                            categoryName = dr[CategoryVal].ToString();
                            break;
                        }
                    }

                    familyName = dr["FAMILY_NAME"].ToString();
                    dr["FAMILY_ID"] = objLS.TB_FAMILY.Join(objLS.TB_CATEGORY, F => F.CATEGORY_ID, c => c.CATEGORY_ID, (F, C) => new { F, C }).Where(x => x.F.FAMILY_NAME == familyName && x.C.CATEGORY_NAME == categoryName).Select(x => x.F.FAMILY_ID).FirstOrDefault();
                }
            }

            if (ProductPdfXpres.Rows.Count > 0)
            {
                foreach (DataRow dr in ProductPdfXpres.Rows)
                {

                    flePath = dr["Product_PdfTemplate"].ToString();
                    fileName = flePath.ToString().Replace(@"PDF_Template\", "");

                    Id = dr["FAMILY_ID"].ToString();



                    // When the values is already in a table
                    var pdfXpressdet = objLS.TB_PDFXPRESS_HIERARCHY.FirstOrDefault(a => a.CATALOG_ID == CatalogId && a.ASSIGN_TO == Id && a.CUSTOMER_ID == 0 && a.TYPE == "Product" && a.TEMPLATE_NAME == fileName && a.TEMPLATE_PATH == flePath);
                    if (pdfXpressdet == null)
                    {
                        var pdfXpressdetValues = objLS.TB_PDFXPRESS_HIERARCHY.FirstOrDefault(a => a.CATALOG_ID == CatalogId && a.ASSIGN_TO == Id && a.CUSTOMER_ID == 0 && a.TYPE == "Product");
                        if (pdfXpressdetValues != null)
                        {
                            objLS.TB_PDFXPRESS_HIERARCHY.Remove(pdfXpressdetValues);
                            objLS.SaveChanges();
                        }

                        var objPdfXpress = new TB_PDFXPRESS_HIERARCHY();
                        objPdfXpress.PROJECT_ID = 0;
                        objPdfXpress.TEMPLATE_NAME = fileName;
                        objPdfXpress.TEMPLATE_PATH = flePath;
                        objPdfXpress.TYPE = "Product";
                        objPdfXpress.ASSIGN_TO = Id;
                        objPdfXpress.CUSTOMER_ID = 0;
                        objPdfXpress.CATALOG_ID = CatalogId;
                        objPdfXpress.CREATED_USER = customer_Name;
                        objPdfXpress.CREATED_DATE = DateTime.Now;
                        objPdfXpress.MODIFIED_USER = customer_Name;
                        objPdfXpress.MODIFIED_DATE = DateTime.Now;
                        objLS.TB_PDFXPRESS_HIERARCHY.Add(objPdfXpress);
                        objLS.SaveChanges();

                    }


                }
            }
        }



        public string FinishSubProductImport(string sheetName, string allowDuplicate, string excelPath, int catalogId, string importType, JArray model, int templateId)
        {
            string importTemp;

            QueryValues queryValues = new QueryValues();

            // CustomerItemNo
            if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
            {
                var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                int customerIds;
                int.TryParse(customerDetails.ToString(), out customerIds);
                string customerCatalog = queryValues.GetCatalogItemNo(customerIds);
                System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerCatalog;
            }
            // CustomerSubItemNo
            if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
            {
                var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                int customerId;
                int.TryParse(customerDetails.ToString(), out customerId);
                string customerCatalog = queryValues.GetSubCatalogItemNo(customerId);
                System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerCatalog;
            }
            importTemp = Guid.NewGuid().ToString();
            try
            {
                int itemVal = Convert.ToInt32(allowDuplicate);
                using (var conn = new SqlConnection(connectionString))
                {
                    var customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                    if (customerid != null)
                    {
                        conn.Open();
                        string sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPRODUCTIMPORTTEMP" + importTemp + "]'')BEGIN DROP TABLE [##SUBPRODUCTIMPORTTEMP" + importTemp + "] END')";

                        var dbCommand = new SqlCommand(sqlString, conn);
                        dbCommand.ExecuteNonQuery();
                        importExcelSheetSelection(excelPath, sheetName);
                        DataTable excelData = GetDataFromExcel(sheetName, excelPath, " SELECT * from [" + sheetName + "$]", "");
                        //Item# import Process Start
                        DataTable itemImport = ValidateItemNumber(excelData);

                        if (excelData.Columns.Contains(HttpContext.Current.Session["CustomerItemNo"].ToString()))
                        {
                            excelData.Columns[HttpContext.Current.Session["CustomerItemNo"].ToString()].ColumnName = "CATALOG_ITEM_NO";
                        }
                        if (excelData.Columns.Contains(HttpContext.Current.Session["CustomerSubItemNo"].ToString()))
                        {
                            excelData.Columns[HttpContext.Current.Session["CustomerSubItemNo"].ToString()].ColumnName = "SUBCATALOG_ITEM_NO";
                        }

                        if (itemImport != null && itemImport.Rows.Count > 0)
                        {
                            return CatalogItemNumberImport(itemImport, sheetName, allowDuplicate, excelPath, catalogId, importType, model, importTemp, templateId);
                        }
                        //Item# import process stop
                        //----------------------------------------------------For with out ID column Import ----------------------
                        excelData = GetDataFromExcel(sheetName, excelPath, " SELECT * from [" + sheetName + "$]", "");
                        if (excelData.Columns.Contains(HttpContext.Current.Session["CustomerItemNo"].ToString()))
                        {
                            excelData.Columns[HttpContext.Current.Session["CustomerItemNo"].ToString()].ColumnName = "CATALOG_ITEM_NO";
                        }
                        if (excelData.Columns.Contains(HttpContext.Current.Session["CustomerSubItemNo"].ToString()))
                        {
                            excelData.Columns[HttpContext.Current.Session["CustomerSubItemNo"].ToString()].ColumnName = "SUBCATALOG_ITEM_NO";
                        }
                        var catalogName = objLS.TB_CATALOG.Where(xx => xx.CATALOG_ID == catalogId).Select(y => y.CATALOG_NAME).FirstOrDefault();
                        if (!excelData.Columns.Contains("CATALOG_ID"))
                        {
                            excelData.Columns.Add("CATALOG_ID");
                            excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["CATALOG_ID"] = catalogId);
                        }
                        else if (excelData.Columns.Contains("CATALOG_ID"))
                        {
                            excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["CATALOG_ID"] = catalogId);
                        }
                        if (!excelData.Columns.Contains("CATALOG_NAME"))
                        {
                            excelData.Columns.Add("CATALOG_NAME");
                            excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["CATALOG_NAME"] = catalogName);
                        }
                        else
                        {
                            excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["CATALOG_NAME"] = catalogName);
                        }
                        if (!excelData.Columns.Contains("CATALOG_ID"))
                        {
                            excelData.Columns.Add("CATALOG_ID");
                            excelData.Columns["CATALOG_ID"].SetOrdinal(0);
                        }
                        if (!excelData.Columns.Contains("FAMILY_ID"))
                        {
                            excelData.Columns.Add("FAMILY_ID");
                            excelData.Columns["FAMILY_ID"].SetOrdinal(4);
                        }
                        if (!excelData.Columns.Contains("PRODUCT_ID"))
                        {
                            excelData.Columns.Add("PRODUCT_ID");
                            excelData.Columns["PRODUCT_ID"].SetOrdinal(6);
                        }
                        if (!excelData.Columns.Contains("SUBPRODUCT_ID"))
                        {
                            excelData.Columns.Add("SUBPRODUCT_ID");
                            excelData.Columns["SUBPRODUCT_ID"].SetOrdinal(8);
                        }
                        if (!excelData.Columns.Contains("SUBPRODUCT_PUBLISH2WEB"))
                        {
                            excelData.Columns.Add("SUBPRODUCT_PUBLISH2WEB");
                            excelData.Columns["SUBPRODUCT_PUBLISH2WEB"].SetOrdinal(excelData.Columns.IndexOf("SUBPRODUCT_ID") + 1);
                        }
                        if (!excelData.Columns.Contains("SUBPRODUCT_PUBLISH2PDF"))
                        {
                            excelData.Columns.Add("SUBPRODUCT_PUBLISH2PDF");
                            excelData.Columns["SUBPRODUCT_PUBLISH2PDF"].SetOrdinal(excelData.Columns.IndexOf("SUBPRODUCT_PUBLISH2WEB"));
                        }
                        if (!excelData.Columns.Contains("SUBPRODUCT_PUBLISH2EXPORT"))
                        {
                            excelData.Columns.Add("SUBPRODUCT_PUBLISH2EXPORT");
                            excelData.Columns["SUBPRODUCT_PUBLISH2EXPORT"].SetOrdinal(excelData.Columns.IndexOf("SUBPRODUCT_PUBLISH2PDF"));
                        }
                        if (!excelData.Columns.Contains("SUBPRODUCT_PUBLISH2PRINT"))
                        {
                            excelData.Columns.Add("SUBPRODUCT_PUBLISH2PRINT");
                            excelData.Columns["SUBPRODUCT_PUBLISH2PRINT"].SetOrdinal(excelData.Columns.IndexOf("SUBPRODUCT_PUBLISH2EXPORT"));
                        }
                        if (!excelData.Columns.Contains("SUBPRODUCT_PUBLISH2PORTAL"))
                        {
                            excelData.Columns.Add("SUBPRODUCT_PUBLISH2PORTAL");
                            excelData.Columns["SUBPRODUCT_PUBLISH2PORTAL"].SetOrdinal(excelData.Columns.IndexOf("SUBPRODUCT_PUBLISH2PRINT"));
                        }



                        //Mapping attribute -Start-------------------------------------------------------------------

                        var Mappingattributes = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                        if (Mappingattributes.Count != 0)
                        {
                            for (int i = 0; i < Mappingattributes.Count; i++)
                            {
                                for (int j = 0; j < excelData.Columns.Count; j++)
                                {
                                    if (Mappingattributes[i].Excel_Column == excelData.Columns[j].ColumnName)
                                    {
                                        excelData.Columns[j].ColumnName = Mappingattributes[i].Mapping_Name;
                                    }
                                }
                            }
                        }

                        //Mapping Attribute - End ---------------------------------------------------------------------

                        //------------------------------------------------------------------End-------------------------------------------------------
                        sqlString = importController.CreateTable("[##SUBPRODUCTIMPORTTEMP" + importTemp + "]", excelPath, sheetName, excelData);
                        dbCommand = new SqlCommand(sqlString, conn);
                        dbCommand.ExecuteNonQuery();
                        //var bulkCopy = new SqlBulkCopy(conn)
                        //{
                        //    DestinationTableName = "[##SUBPRODUCTIMPORTTEMP" + importTemp + "]"
                        //};
                        //bulkCopy.WriteToServer(excelData);
                        DataTable importData = UnPivotTable(excelData, importType);
                        SqlCommand dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTSUBTEMPDATA", conn);
                        dbCommandTemp.CommandType = CommandType.StoredProcedure;
                        dbCommandTemp.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = importData;
                        dbCommandTemp.Parameters.Add("@TABLENAME", SqlDbType.NChar).Value = "[##SUBPRODUCTIMPORTTEMP" + importTemp + "]";
                        dbCommandTemp.ExecuteNonQuery();

                        DataTable oattType = importController.SelectedColumnsToImport(excelData, model);




                        //Mapping attribute -Start-------------------------------------------------------------------

                        var MappingattributesValue = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                        if (MappingattributesValue.Count != 0)
                        {
                            for (int i = 0; i < MappingattributesValue.Count; i++)
                            {
                                for (int j = 0; j < oattType.Rows.Count; j++)
                                {
                                    if (MappingattributesValue[i].Excel_Column.ToString() == oattType.Rows[j]["ATTRIBUTE_NAME"].ToString())
                                    {
                                        oattType.Rows[j][1] = MappingattributesValue[i].Mapping_Name;
                                    }
                                }
                            }
                        }

                        //Mapping Attribute - End ---------------------------------------------------------------------
                        var sqlstring1 = new SqlCommand("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]'')BEGIN DROP TABLE [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] END')", conn);
                        sqlstring1.ExecuteNonQuery();
                        var cmd2 = new SqlCommand();
                        cmd2.Connection = conn;
                        sqlString = importController.CreateTableToImport("[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]", oattType);
                        cmd2.CommandText = sqlString;
                        cmd2.CommandType = CommandType.Text;
                        cmd2.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn) { DestinationTableName = "[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]" };
                        bulkCopy.WriteToServer(oattType);
                        var cmbpk10 = new SqlCommand("update [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] set ATTRIBUTE_TYPE = 1 where ATTRIBUTE_TYPE = 10", conn);
                        cmbpk10.ExecuteNonQuery();
                        var cmbpk1 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1Sub'') IS NOT NULL  DROP TABLE  ##t1Sub') ", conn);
                        cmbpk1.ExecuteNonQuery();
                        var cmbpk2 = new SqlCommand("select * into ##t1Sub  from [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] where attribute_type <>0", conn);
                        cmbpk2.ExecuteNonQuery();
                        var cmbpk3 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2Sub'') IS NOT NULL  DROP TABLE  ##t2Sub') ", conn);
                        cmbpk3.ExecuteNonQuery();
                        var cmbpk4 = new SqlCommand("select * into ##t2Sub  from [##SUBPRODUCTIMPORTTEMP" + importTemp + "]", conn);
                        cmbpk4.ExecuteNonQuery();

                        var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_SUBPRODUCTIMPORT ''" + importTemp + "'',''" + itemVal + "'',''" + customerid.CustomerId + "'',''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                        cmd.ExecuteNonQuery();
                        importController._SQLString = @"select * from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                        var ds = importController.CreateDataSet();
                        importController._SQLString = @"select * into [tempresult" + importTemp + "] from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                        cmd.CommandText = importController._SQLString;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                        DataSet rowaffected = new DataSet();
                        string SQLString = string.Empty;
                        SQLString =
                          "exec('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBPRODUCTIMPORTTEMP" + importTemp + "'') BEGIN if exists(select NAME from TEMPDB.sys.objects where type = ''u'' and name = ''##SUBPRODUCTLOGTEMPTABLE" + importTemp + "'') " +
                          "Begin select Count(*) UpdatedRecords from(select distinct temp1.* from[##SUBPRODUCTLOGTEMPTABLE" + importTemp + "] temp join [##SUBPRODUCTIMPORTTEMP" + importTemp + "] temp1 on SUBSTRING(temp.PRODUCT_DETAILS, 1, CHARINDEX(''('', temp.PRODUCT_DETAILS) - 1) = temp1.CATALOG_ITEM_NO  where STATUS like ''Update%'' or STATUS like ''Associate%''" +
                           ")  UpdateDetails select Count(*) as InsertedRecords from(select distinct temp1.* from[##SUBPRODUCTLOGTEMPTABLE" + importTemp + "] temp join [##SUBPRODUCTIMPORTTEMP" + importTemp + "] temp1 on SUBSTRING(temp.PRODUCT_DETAILS, 1, CHARINDEX(''('', temp.PRODUCT_DETAILS) - 1) = temp1.CATALOG_ITEM_NO  where STATUS like ''insert%''" +
                           ")  InsertDetails END END')";
                        SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                        SqlDataAdapter dbAdapter = new SqlDataAdapter(_DBCommand);
                        dbAdapter.Fill(rowaffected);

                        int insertRecords = 0;
                        int updateRecords = 0;
                        if (rowaffected != null && rowaffected.Tables.Count > 1)
                        {
                            if (rowaffected.Tables[0].Rows.Count > 0)
                            {
                                updateRecords = int.Parse(rowaffected.Tables[0].Rows[0][0].ToString());
                            }
                            if (rowaffected.Tables[1].Rows.Count > 0)
                            {
                                insertRecords = int.Parse(rowaffected.Tables[1].Rows[0][0].ToString());
                            }
                        }
                        if (ds.Tables[0].Columns[0].ColumnName == "ErrorMessage")
                        {

                            return "Import Failed~" + importTemp;
                        }
                        else
                        {
                            //--------------------Update Picklist values in Picklist table---------------------------------------------
                            var picklistvaluecreation = objLS.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerid.CustomerId);//To check Picklist validation in enabled or not in customer setting

                            PickListUpdate(importTemp);
                            DataTable skipped = GetDataFromExcel(sheetName, excelPath, " SELECT * from [" + sheetName + "$]", "SKIP");
                            if (skipped != null && skipped.Rows.Count > 0)
                            {
                                return "Import Success~" + importTemp + "~InsertRecords:" + insertRecords + "~" + "UpdateRecords:" + updateRecords + "~SkippedRecords:" + skipped.Rows.Count;
                            }
                            else
                            {
                                return "Import Success~" + importTemp + "~InsertRecords:" + insertRecords + "~" + "UpdateRecords:" + updateRecords + "~SkippedRecords:0";

                            }
                        }
                    }
                    else
                    {
                        return "Invalid Customer~" + importTemp;

                    }

                }


            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at AdvanceImportApiController : FinishSubProductImport", ex);
                return null;
            }
        }

        public string GetSkuCount(int prodCount)
        {
            int userProductCount = 0, userSKUProductCount = 0;
            var skucnt = objLS.TB_PLAN
                  .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                  .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                  .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
            var SKUcount = skucnt.Select(a => a).ToList();
            if (SKUcount.Any())
            {
                userSKUProductCount = Convert.ToInt32(SKUcount[0].SKU_COUNT);
            }

            var productcount = objLS.STP_LS_GET_PRODUCTS_BY_USER("PRODUCT", User.Identity.Name, "").ToList();

            if (productcount.Any())
            {
                userProductCount = Convert.ToInt32(productcount[0]);
            }

            if ((userProductCount + prodCount) > userSKUProductCount)
            {

                return "SKU Exceed";
            }
            return "SKU Available";

        }

        public string PickListUpdate(string importTemp)
        {
            try
            {
                DataSet checkPICK = new DataSet();
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    var cmd1f = new SqlCommand(
                    "SELECT * FROM [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]", conn);
                    var daf = new SqlDataAdapter(cmd1f);
                    daf.Fill(checkPICK);
                    string check = string.Empty;
                    if (checkPICK.Tables[0].Rows.Count > 0)
                        foreach (DataColumn column1 in checkPICK.Tables[0].Columns)
                        {
                            if (column1.ToString() == "STATUS")
                            {
                                check = checkPICK.Tables[0].Rows[0][column1.ToString()].ToString();//"Action completed";

                            }
                        }
                    if (check == "Success" || check == "UPDATED")
                    {

                        DataSet pkname = new DataSet();
                        SqlDataAdapter daa =
                            new SqlDataAdapter(
                                "select PICKLIST_NAME,ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select attribute_name  from ##t1sub where attribute_type <>0) and USE_PICKLIST =1",
                                conn);
                        daa.Fill(pkname);
                        if (pkname.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow vr in pkname.Tables[0].Rows)
                            {
                                DataTable _pickListValue = new DataTable();
                                DataSet newsheetval = new DataSet();
                                _pickListValue.Columns.Add("Value");
                                string picklistname = vr["PICKLIST_NAME"].ToString();
                                DataSet dpicklistdata = new DataSet();
                                if (!string.IsNullOrEmpty(picklistname))
                                {
                                    var pickList = objLS.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                                    {
                                        PICKLIST_DATA = s.PICKLIST_DATA,
                                        PICKLIST_NAME = s.PICKLIST_NAME,
                                        PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                                    }).FirstOrDefault(d => d.PICKLIST_NAME == picklistname);
                                    var objPick = new List<PickList>();
                                    if (pickList != null && !string.IsNullOrEmpty(pickList.PICKLIST_DATA))
                                    {
                                        var pkxmlData = pickList.PICKLIST_DATA;

                                        if (pkxmlData.Contains("<?xml version"))
                                        {
                                            pkxmlData = pkxmlData.Replace("\\r\\n", "");
                                            pkxmlData = pkxmlData.Replace("\r\n", "");
                                            pkxmlData = pkxmlData.Replace("\\\"", "\"");
                                            pkxmlData =
                                                pkxmlData.Replace(
                                                    "<?xml version=\"1.0\" standalone=\"yes\"?>",
                                                    "");

                                            XDocument objXml = XDocument.Parse(pkxmlData);

                                            objPick =
                                                objXml.Descendants("NewDataSet")
                                                    .Descendants("Table1")
                                                    .Select(d =>
                                                    {
                                                        var xElement = d.Element("ListItem");
                                                        return xElement != null
                                                            ? new PickList
                                                            {
                                                                ListItem = xElement.Value
                                                            }
                                                            : null;
                                                    }).ToList();
                                        }
                                    }
                                    SqlDataAdapter daa2 = new SqlDataAdapter("Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  ['+ @colName+'],CATALOG_ITEM_NO   from [##t2sub]') ", conn);
                                    daa2.Fill(newsheetval);
                                    SqlCommand cvb = new SqlCommand("DELETE TOP(1) From ##t1sub", conn);
                                    cvb.ExecuteNonQuery();
                                    DataSet picklistdataDS = new DataSet();
                                    DataTable newTable = new DataTable();
                                    for (int rowCount = 0; rowCount < newsheetval.Tables[0].Rows.Count; rowCount++)
                                    {
                                        if (newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim() != "")
                                        {
                                            var picklistvalue = newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim();
                                            PickList objlist = new PickList();
                                            objlist.ListItem = picklistvalue;
                                            if (!objPick.Contains(objlist))
                                            {
                                                objPick.Add(objlist);
                                            }
                                        }
                                    }
                                    var picklistdatas = objPick.DistinctBy(x => x.ListItem).ToList();
                                    foreach (var item in picklistdatas)
                                    {
                                        if (string.IsNullOrEmpty(item.ListItem))
                                        {
                                            picklistdatas.Remove(item);
                                        }
                                        else if (item.ListItem.Trim() == "")
                                        {
                                            picklistdatas.Remove(item);
                                        }
                                        else
                                        {
                                            item.ListItem = item.ListItem.Trim();
                                        }
                                    }
                                    var serializer1 = new JavaScriptSerializer();
                                    var json = serializer1.Serialize(picklistdatas);
                                    XmlDocument doc = JsonConvert.DeserializeXmlNode(
                                        "{\"Table1\":" + json + "}", "NewDataSet");
                                    string picklistdataxml =
                                        "<?xml version=\"1.0\" standalone=\"yes\"?>" +
                                        doc.InnerXml;
                                    var objPicklist =
                                        objLS.TB_PICKLIST.FirstOrDefault(
                                            s => s.PICKLIST_NAME == picklistname);
                                    if (objPicklist != null)
                                        objPicklist.PICKLIST_DATA = picklistdataxml;
                                    objLS.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : PickListUpdate", ex);
                return "Import Failed~" + importTemp;
            }
            return "";
        }

        public string CheckPickList(string importTemp, string importType)
        {
            try
            {
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    DataSet pkname = new DataSet();
                    SqlDataAdapter daa = new SqlDataAdapter("select PICKLIST_NAME,ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select attribute_name  from ##t1 where attribute_type <>0) and USE_PICKLIST =1", conn);
                    daa.Fill(pkname);
                    if (pkname.Tables[0].Rows.Count > 0)
                    {
                        DataTable Errortable = new DataTable();
                        Errortable.Columns.Add("STATUS");
                        Errortable.Columns.Add("ITEM_NO");
                        Errortable.Columns.Add("ATTRIBUTE_NAME");
                        Errortable.Columns.Add("PICKLIST_VALUE");

                        foreach (DataRow vr in pkname.Tables[0].Rows)
                        {
                            DataTable _pickListValue = new DataTable();
                            DataSet newsheetval = new DataSet();
                            _pickListValue.Columns.Add("Value");
                            string picklistname = vr["PICKLIST_NAME"].ToString();
                            DataSet dpicklistdata = new DataSet();
                            if (!string.IsNullOrEmpty(picklistname))
                            {
                                var pickList = objLS.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                                {
                                    PICKLIST_DATA = s.PICKLIST_DATA,
                                    PICKLIST_NAME = s.PICKLIST_NAME,
                                    PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                                }).FirstOrDefault(d => d.PICKLIST_NAME == picklistname);
                                var objPick = new List<PickList>();
                                if (pickList != null && !string.IsNullOrEmpty(pickList.PICKLIST_DATA))
                                {
                                    var pkxmlData = pickList.PICKLIST_DATA;

                                    if (pkxmlData.Contains("<?xml version"))
                                    {
                                        pkxmlData = pkxmlData.Replace("\\r\\n", "");
                                        pkxmlData = pkxmlData.Replace("\r\n", "");
                                        pkxmlData = pkxmlData.Replace("\\\"", "\"");
                                        pkxmlData = pkxmlData.Replace("<?xml version=\"1.0\" standalone=\"yes\"?>", "");
                                        XDocument objXml = XDocument.Parse(pkxmlData);

                                        objPick =
                                            objXml.Descendants("NewDataSet")
                                                .Descendants("Table1")
                                                .Select(d =>
                                                {
                                                    var xElement = d.Element("ListItem");
                                                    return xElement != null
                                                        ? new PickList
                                                        {
                                                            ListItem = xElement.Value
                                                        }
                                                        : null;
                                                }).ToList();
                                    }
                                }
                                string sqlString = "Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  ['+ @colName+'],CATALOG_ITEM_NO   from [##t2] where  ['+ @colName+']<>''@null''') ";
                                if (importType.ToUpper() == "SUBPRODUCT")
                                {
                                    sqlString = "Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  ['+ @colName+'],[SUBITEM#] as SUBITEM   from [##t2]  where  ['+ @colName+']<>''@null''') ";
                                }
                                SqlDataAdapter daa2 =
                                        new SqlDataAdapter(sqlString, conn);
                                daa2.Fill(newsheetval);
                                SqlCommand cvb = new SqlCommand("DELETE TOP(1) From ##t1", conn);
                                cvb.ExecuteNonQuery();
                                DataSet picklistdataDS = new DataSet();
                                DataTable newTable = new DataTable();
                                for (int rowCount = 0; rowCount < newsheetval.Tables[0].Rows.Count; rowCount++)
                                {
                                    if (newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim() != "")
                                    {
                                        var picklistvalue = newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim();
                                        var dd = objPick.Where(x => x.ListItem == picklistvalue);
                                        if (!dd.Any())
                                        {
                                            string CAT = newsheetval.Tables[0].Rows[rowCount][1].ToString();
                                            Errortable.Rows.Add("Import failed due to invalid Pick List value.", CAT, vr["ATTRIBUTE_NAME"], picklistvalue);
                                        }

                                    }
                                }


                            }
                        }
                        if (Errortable.Rows.Count > 0)
                        {
                            string sqlString = importController.CreateTableToImport("[Picklistlog" + importTemp + "]", Errortable);
                            SqlCommand cmdPick = new SqlCommand();
                            cmdPick.Connection = conn;
                            cmdPick.CommandText = sqlString;
                            cmdPick.CommandType = CommandType.Text;
                            cmdPick.ExecuteNonQuery();
                            var bulkCopysp = new SqlBulkCopy(conn)
                            {
                                DestinationTableName = "[Picklistlog" + importTemp + "]"
                            };
                            bulkCopysp.WriteToServer(Errortable);
                            var removetemp = new SqlCommand("IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'errorlog" + importTemp + "')Begin drop table [errorlog" + importTemp + "] End ", conn);
                            SqlCommand cmbpk1 = new SqlCommand();
                            removetemp.ExecuteNonQuery();
                            return "Validation Failed";
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : CheckPickList", ex);
                return null;
            }

            return "Validation Success";
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage importlogs(string Errorlogoutputformat)
        {
            try
            {

                if (Errorlogoutputformat.ToLower() == "csv")
                {

                    return Request.CreateResponse(HttpStatusCode.OK, "Validation_log.csv");
                }
                else if (Errorlogoutputformat.ToLower() == "text")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Validation_log.txt");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Validation_log.xls");
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at AdvanceImportApiController : importlogs", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [System.Web.Http.HttpPost]
        public string CheckCatalogDetails(string excelPath, string sheetName, int catalogId)
        {
            try
            {

                List<string> importTypeProcess = LoadExcelSheet(sheetName, excelPath);


                if (catalogId == 0)
                {
                    return "Catalog Valid" + "_" + importTypeProcess[0] + "_" + importTypeProcess[1];
                }
                OleDbConnection con = excelConnection(excelPath);

                DataTable excelData = GetDataFromExcel(sheetName, excelPath, "select * from [" + sheetName + "$]", "");
                var catalogName = objLS.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId).Select(y => y.CATALOG_NAME).FirstOrDefault();
                var customerSetting = objLS.Customer_Settings.Join(objLS.Customer_User, cs => cs.CustomerId, cu => cu.CustomerId, (cs, cu) => new { cs, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.cs).FirstOrDefault();
                if (customerSetting != null && importTypeProcess.Count == 0)
                {
                    var picklistValue = customerSetting.ShowCustomAPPS;
                    var subCatalog = customerSetting.SubCatalog_Items;
                    if (importTypeProcess.Count == 0)
                    {
                        return "Catalog Valid" + "_YES_PRODUCT_" + picklistValue + "_" + subCatalog;
                    }
                }
                else if (importTypeProcess.Count == 0)
                {
                    return null;
                }
                if (excelData != null && excelData.Rows.Count > 0)
                {
                    ImportApiController importapiController = new ImportApiController();
                    excelData = importapiController.UpdateCatalogDetails(importTypeProcess[1], catalogId, excelData);
                    DataRow[] dRow = excelData.Select("CATALOG_NAME='" + catalogName + "'");
                    if (dRow.Length == 0)
                    {
                        var masterCatalog = objLS.Customer_Settings.Join(objLS.TB_CATALOG, cs => cs.Customer_MasterCatalog, tc => tc.CATALOG_ID, (cs, tc) => new { cs, tc }).Where(x => x.tc.CATALOG_ID == catalogId).Select(y => y.tc.CATALOG_NAME).FirstOrDefault();

                        if (string.IsNullOrEmpty(masterCatalog))
                        {
                            return customerSetting != null ? "Catalog Invalid" + "_" + importTypeProcess[0] + "_" + importTypeProcess[1] + "_" + customerSetting.ShowCustomAPPS + "_" + customerSetting.SubCatalog_Items : "Catalog Invalid" + "_" + importTypeProcess[0] + "_" + importTypeProcess[1];
                        }
                        else
                        {
                            return customerSetting != null ? "Catalog Valid" + "_" + importTypeProcess[0] + "_" + importTypeProcess[1] + "_" + customerSetting.ShowCustomAPPS + "_" + customerSetting.SubCatalog_Items : "Catalog Valid" + "_" + importTypeProcess[0] + "_" + importTypeProcess[1];

                        }
                    }
                    else
                    {
                        return customerSetting != null ? "Catalog Valid" + "_" + importTypeProcess[0] + "_" + importTypeProcess[1] + "_" + customerSetting.ShowCustomAPPS + "_" + customerSetting.SubCatalog_Items : "Catalog Valid" + "_" + importTypeProcess[0] + "_" + importTypeProcess[1];
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : CheckCatalogDetails", ex);
                return null;
            }
            return null;
        }

        public string GetSheetCount(string excelPath)
        {
            try
            {
                string[] importSheets = new string[10];
                string sheetName = "Sub products";
                List<object> newList = new List<object>();
                if (HttpContext.Current.Session["ImportSheet"] != null)
                {

                    importSheets = HttpContext.Current.Session["ImportSheet"].ToString().Split(',');
                }
                var sheetList = importController.importExcelSheetSelection(excelPath, sheetName);
                OleDbConnection conn = excelConnection(excelPath);
                DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,
                        new object[] { null, null, null, "TABLE" });
                int jk = 0;
                int sheetCount = schemaTable.Rows.Count;
                int sheetColumnCount = 0;
                if (!sheetName.Contains("$"))
                {
                    sheetName = sheetName + "$";
                }
                foreach (DataRow rows in schemaTable.Rows)
                {
                    ImportSheetName objImportSheetName = new ImportSheetName();
                    if (!rows[2].ToString().EndsWith("_"))
                    {
                        objImportSheetName.TABLE_NAME = Convert.ToString(rows[2]);
                        objImportSheetName.SHEET_PATH = excelPath;
                        newList.Add(objImportSheetName);
                    }
                    jk++;
                }
                DataRow[] sheetRows = schemaTable.Select("TABLE_NAME like '%" + sheetName + "%'");
                foreach (string importSheet in importSheets)
                {
                    if (schemaTable.Rows.Contains(importSheet))
                    {

                    }
                }
                return schemaTable.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : GetSheetCount", ex);
                return null;
            }
        }
        #endregion

        #region Encrypt/Decrypt
        public string DecryptStringAES(string cipherText)
        {
            try
            {
                var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
                var iv = Encoding.UTF8.GetBytes("8080808080808080");

                var encrypted = Convert.FromBase64String(cipherText.Replace(" ", "+"));
                var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
                return string.Format(decriptedFromJavascript);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : DecryptStringAES", ex);
                return null;
            }
        }

        private string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext = null;

            // Create an RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings  
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.  
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream  
                                // and place them in a string.  
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }
        #endregion

        #region Batch view log

        public List<ImportBatch> ConvertDataTable(DataTable dt)
        {
            List<ImportBatch> data = new List<ImportBatch>();
            foreach (DataRow row in dt.Rows)
            {
                var importBatch = new ImportBatch();
                importBatch.BatchId = row.ItemArray[0].ToString();
                importBatch.CatalogName = row.ItemArray[1].ToString();
                importBatch.FileName = row.ItemArray[2].ToString();
                importBatch.ImportType = row.ItemArray[3].ToString();
                if (importBatch.ImportType.Equals("CATALOG_ITEM_NO"))
                {
                    importBatch.ImportType = "ITEM#";
                }
                importBatch.AllowDuplicate = int.Parse(row.ItemArray[4].ToString());
                importBatch.InsertRecords = int.Parse(row.ItemArray[5].ToString());
                importBatch.DeleteRecords = int.Parse(row.ItemArray[6].ToString());
                importBatch.SkipRecords = int.Parse(row.ItemArray[7].ToString());
                importBatch.Status = row.ItemArray[8].ToString();
                importBatch.ElapsedTime = row.ItemArray[9].ToString();

                importBatch.scheduleTime = row.ItemArray[10].ToString();
                importBatch.NoOfRecords = row.ItemArray[11].ToString();
                importBatch.ISAvailable = Convert.ToBoolean(row.ItemArray[12].ToString());
                importBatch.CustomStatus = row.ItemArray[13].ToString();

                if (dt.Columns.Contains("UPDATEDRECORDS"))
                {
                    if (row.ItemArray[14].ToString() != "")
                        importBatch.UPDATEDRECORDS = int.Parse(row.ItemArray[14].ToString());
                    else
                        importBatch.UPDATEDRECORDS = 0;
                }
                else
                {
                    importBatch.UPDATEDRECORDS = 0;
                }

                data.Add(importBatch);
            }
            return data;
        }
        public IList GetBatchLogList()
        {
            DataTable dtGetLogDetails = new DataTable();
            try
            {
                var customerId = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).ToList();
                int _customerId = 0;
                int.TryParse(customerId[0].ToString(), out _customerId);
                Process[] runningProcess = Process.GetProcessesByName("BatchProcess");
                if (runningProcess.Length == 0)
                {

                    using (var con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        var sqlCommand = new SqlCommand("STP_LS_IMPORTBATCH", con);
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = _customerId;
                        sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "UPDATESTATUS";
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                        sqlDataAdapter.Fill(dtGetLogDetails);
                    }
                }

                using (var con = new SqlConnection(connectionString))
                {
                    con.Open();
                    var sqlCommand = new SqlCommand("STP_LS_IMPORTBATCH", con);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = _customerId;
                    sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "SELECTLOGCDETAILS";
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dtGetLogDetails);
                }


                var listLog = ConvertDataTable(dtGetLogDetails).ToList();
                return listLog;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : GetBatchLogList", ex);
            }
            return null;
        }

        [System.Web.Http.HttpGet]
        public string RunImportProcess(string batchId)
        {
            try
            {
                var batchPath = ConfigurationManager.AppSettings["Batchprocess"].ToString();
                var customers = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).ToList();
                string customerId = string.Empty;
                if (customers.Count > 0)
                {
                    customerId = customers[0].ToString();
                }
                Process[] runningProcess = Process.GetProcessesByName("BatchProcess");
                if (runningProcess.Length > 0)
                {
                    return "Import is already running.Please wait for few minutes";
                }
                else
                {
                    using (var con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        var sqlCommand = new SqlCommand("STP_LS_IMPORTBATCH", con);
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = customerId;
                        sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "RUNPROCESS";
                        sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NVarChar).Value = batchId;
                        sqlCommand.ExecuteNonQuery();
                    }
                    //     string[] args = { batchId, customerId, "true" };
                    //     Process.Start(batchPath, string.Join(" ", args));
                    Process proc = new Process();
                    proc.StartInfo.Arguments = string.Format("{0} {1} {2}", batchId, customerId, "true");
                    proc.StartInfo.FileName = batchPath;
                    proc.Start();
                    return "Import started";
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : RunImportProcess", ex);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public string PauseImportProcess(string batchId)
        {
            try
            {
                var batchPath = ConfigurationManager.AppSettings["Batchprocess"].ToString();
                var customers = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).ToList();
                string customerId = string.Empty;
                if (customers.Count > 0)
                {
                    customerId = customers[0].ToString();
                }
                Process[] runningProcess = Process.GetProcessesByName("BatchProcess");

                if (runningProcess.Length > 0)
                {
                    return "Import is already running.Please wait for few minutes";
                }
                else
                {
                    using (var con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        var sqlCommand = new SqlCommand("STP_LS_IMPORTBATCH", con);
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = customerId;
                        sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "PAUSEPROCESS";
                        sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NVarChar).Value = batchId;
                        sqlCommand.ExecuteNonQuery();
                    }
                    string[] args = { batchId, customerId, "true" };
                    return "Import Paused";
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : RunImportProcess", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public string ClearLog(List<string> BathchValues)
        {
            try
            {
                DataTable dtDeletedData = new DataTable();
                var customerId = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).ToList();
                bool running = false;
                using (var con = new SqlConnection(connectionString))
                {
                    con.Open();

                    for (int i = 0; BathchValues.Count > i; i++)
                    {
                        var sqlCommand = new SqlCommand("STP_LS_IMPORTBATCH", con);
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = customerId[0];
                        sqlCommand.Parameters.Add("@BATCHID", SqlDbType.VarChar).Value = BathchValues[i];
                        sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "DELETELOGCDETAILS";
                        SqlDataAdapter da = new SqlDataAdapter(sqlCommand);
                        da.Fill(dtDeletedData);

                    }

                }
                return "Deleted Successfully";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : ClearLog", ex);
                return "Error";
            }
        }
        #endregion

        [System.Web.Http.HttpGet]
        public HttpResponseMessage DownLoadErrorLog(string batchId)
        {
            DataTable dtGetLogDetails = new DataTable();
            try
            {
                var customerId = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).ToList();
                using (var con = new SqlConnection(connectionString))
                {
                    con.Open();
                    var sqlCommand = new SqlCommand("STP_LS_IMPORTBATCH", con);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@BATCHID", SqlDbType.NVarChar).Value = batchId;
                    sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = customerId[0];
                    sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "ERRORLIST";
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlDataAdapter.Fill(dtGetLogDetails);
                }
                if (dtGetLogDetails != null && dtGetLogDetails.Rows.Count > 0)
                {
                    object context;
                    if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                    {
                        var httpContext = context as HttpContextBase;
                        if (httpContext != null && httpContext.Session != null)
                        {
                            httpContext.Session["ExportErrorTable"] = dtGetLogDetails;
                        }
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, "errorLog.xls");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "errorLog.xls");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : DownLoadErrorLog", ex);
                return null;
            }
        }

        #region Get Error list of item# import
        /// <summary>
        /// GetItemImportErrorlist
        /// </summary>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public IList GetItemImportErrorlist(HttpContext context)
        {
            try
            {
                List<IMPORT_ERROR_LIST> errorList = new List<IMPORT_ERROR_LIST>();
                DataTable errorDataTable = HttpContext.Current.Session["ITEMIMPORTSESSION"] as DataTable;
                errorList = errorDataTable.AsEnumerable().Select(x => new IMPORT_ERROR_LIST
                {
                    ErrorMessage = x.Field<string>("ErrorMessage").ToString(),
                    ErrorLine = x.Field<string>("ErrorLine").ToString(),
                    ErrorNumber = x.Field<string>("ErrorNumber").ToString(),
                    ErrorProcedure = x.Field<string>("ErrorProcedure").ToString(),
                    ErrorSeverity = x.Field<string>("ErrorSeverity").ToString(),
                    ErrorState = x.Field<string>("ErrorState").ToString()
                }).ToList();
                return errorList;
            }
            catch (Exception ex)
            {
                try
                {
                    List<IMPORT_ERROR_LISTERROR> errorList1 = new List<IMPORT_ERROR_LISTERROR>();
                    DataTable errorDataTable = HttpContext.Current.Session["ITEMIMPORTSESSION"] as DataTable;
                    errorList1 = errorDataTable.AsEnumerable().Select(x => new IMPORT_ERROR_LISTERROR
                    {
                        ErrorMessage = x.Field<string>("ErrorMessage").ToString(),
                        ErrorLine = x.Field<int>("ErrorLine"),
                        ErrorNumber = x.Field<int>("ErrorNumber"),
                        ErrorProcedure = x.Field<string>("ErrorProcedure").ToString(),
                        ErrorSeverity = x.Field<int>("ErrorSeverity"),
                        ErrorState = x.Field<int>("ErrorState")
                    }).ToList();
                    return errorList1;
                }
                catch (Exception ex1)
                {
                    _logger.Error("Error at AdvanceImportApiController : GetItemImportErrorlist", ex1);
                    return null;
                }
            }
        }
        #endregion

        #region Item# impprt process
        // Item# import process start
        #region  Check Import type(Item#)
        /// <summary>
        /// Check weather the sheet is Product/SubProduct import or Item# import
        /// </summary>
        /// <param name="itemImport"></param>
        /// <returns></returns>
        public DataTable ValidateItemNumber(DataTable itemImport)
        {
            try
            {
                QueryValues queryValues = new QueryValues();

                // CustomerItemNo
                if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
                {
                    var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    int customerIds;
                    int.TryParse(customerDetails.ToString(), out customerIds);
                    string customerCatalog = queryValues.GetCatalogItemNo(customerIds);
                    System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerCatalog;
                }

                // CustomerSubItemNo
                if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
                {
                    var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    int customerId;
                    int.TryParse(customerDetails.ToString(), out customerId);
                    string customerCatalog = queryValues.GetSubCatalogItemNo(customerId);
                    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerCatalog;
                }

                bool ItemImport = true;
                string[] columnsToRemove = { "CATEGORY_ID", "CATEGORY_NAME", "FAMILY_ID", "FAMILY_NAME", "SUBFAMILY_ID", "SUBFAMILY_NAME" };
                int removeFields = 0;
                for (int index = 0; index < itemImport.Columns.Count; index++)
                {
                    if ((columnsToRemove.Contains(itemImport.Columns[index].ColumnName.ToUpper()) && itemImport.Columns[index].ColumnName.ToUpper().Contains("NAME")) || itemImport.Columns[index].ColumnName.ToUpper().Contains("SUBCATNAME"))
                    {
                        var emptyRowCount = itemImport.AsEnumerable().Where(x => string.IsNullOrEmpty(x.Field<string>(itemImport.Columns[index].ColumnName))).Select(y => y.Field<string>(itemImport.Columns[index].ColumnName)).Distinct().Count();
                        if (emptyRowCount == itemImport.Rows.Count)
                        {
                            itemImport.Columns.RemoveAt(index);
                            removeFields = removeFields + 1;
                        }
                        else
                        {
                            ItemImport = false;
                        }
                    }
                }
                if (removeFields > 1)
                {
                    for (int index = 0; index < itemImport.Columns.Count; index++)
                    {
                        if (columnsToRemove.Contains(itemImport.Columns[index].ColumnName.ToUpper()) || itemImport.Columns[index].ColumnName.ToUpper().Contains("SUBCAT"))
                        {
                            itemImport.Columns.RemoveAt(index);
                        }
                    }
                }
                if (ItemImport == false)
                {
                    return null;
                }
                if (itemImport.Columns.Contains(HttpContext.Current.Session["CustomerItemNo"].ToString()))
                {
                    itemImport.Columns[HttpContext.Current.Session["CustomerItemNo"].ToString()].ColumnName = "CATALOG_ITEM_NO";
                }
                if (!itemImport.Columns.Contains("PRODUCT_ID") && itemImport.Columns.Contains("CATALOG_ITEM_NO"))
                {
                    itemImport.Columns.Add("PRODUCT_ID");
                }

                if (itemImport.Columns.Contains(HttpContext.Current.Session["CustomerSubItemNo"].ToString()))
                {
                    itemImport.Columns[HttpContext.Current.Session["CustomerSubItemNo"].ToString()].ColumnName = "SUBCATALOG_ITEM_NO";
                }
                if (!itemImport.Columns.Contains("SUBPRODUCT_ID") && itemImport.Columns.Contains("SUBCATALOG_ITEM_NO"))
                {
                    itemImport.Columns.Add("SUBPRODUCT_ID");
                }


                return itemImport;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : ValidateItemNumber", ex);
                return itemImport;
            }
        }
        #endregion

        #region CatalogItemNumberImport
        /// <summary>
        /// Import the data based on the item#
        /// </summary>
        /// <param name="itemImport">Datatable from import data</param>
        /// <param name="SheetName">sheet name from imported excel sheet name</param>
        /// <param name="allowDuplicate">allow duplication(0/1)</param>
        /// <param name="excelPath">imported excel sheet path</param>
        /// <param name="catalogId">Current catalog id</param>
        /// <param name="importType">selected import type</param>
        /// <param name="model">Attribute list with name and type</param>
        /// <returns></returns>
        public string CatalogItemNumberImport(DataTable itemImport, string SheetName, string allowDuplicate, string excelPath, int catalogId, string importType, JArray model, string importTemp, int templateId)
        {
            try
            {
                QueryValues queryValues = new QueryValues();


                // CustomerItemNo
                if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
                {
                    var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    int customerIds;
                    int.TryParse(customerDetails.ToString(), out customerIds);
                    string customerCatalog = queryValues.GetCatalogItemNo(customerIds);
                    System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerCatalog;
                }
                // CustomerSubItemNo
                if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
                {
                    var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    int customerId;
                    int.TryParse(customerDetails.ToString(), out customerId);
                    string customerCatalog = queryValues.GetSubCatalogItemNo(customerId);
                    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerCatalog;
                }


                foreach (DataColumn dcItem in itemImport.Columns)
                {
                    if (dcItem.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerSubItemNo"].ToString())
                        dcItem.ColumnName = "SUBCATALOG_ITEM_NO";
                    if (dcItem.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerItemNo"].ToString())
                        dcItem.ColumnName = "CATALOG_ITEM_NO";
                }
                if (importType.ToUpper().Contains("SUB"))
                {
                    if (!itemImport.Columns.Contains("SUBPRODUCT_PUBLISH2WEB"))
                    {
                        itemImport.Columns.Add("SUBPRODUCT_PUBLISH2WEB");
                        itemImport.Columns["SUBPRODUCT_PUBLISH2WEB"].SetOrdinal(itemImport.Columns.IndexOf("SUBCATALOG_ITEM_NO"));
                    }
                    if (!itemImport.Columns.Contains("SUBPRODUCT_PUBLISH2PDF"))
                    {
                        itemImport.Columns.Add("SUBPRODUCT_PUBLISH2PDF");
                        itemImport.Columns["SUBPRODUCT_PUBLISH2PDF"].SetOrdinal(itemImport.Columns.IndexOf("SUBPRODUCT_PUBLISH2WEB"));
                    }
                    if (!itemImport.Columns.Contains("SUBPRODUCT_PUBLISH2EXPORT"))
                    {
                        itemImport.Columns.Add("SUBPRODUCT_PUBLISH2EXPORT");
                        itemImport.Columns["SUBPRODUCT_PUBLISH2EXPORT"].SetOrdinal(itemImport.Columns.IndexOf("SUBPRODUCT_PUBLISH2PDF"));
                    }
                    if (!itemImport.Columns.Contains("SUBPRODUCT_PUBLISH2PRINT"))
                    {
                        itemImport.Columns.Add("SUBPRODUCT_PUBLISH2PRINT");
                        itemImport.Columns["SUBPRODUCT_PUBLISH2PRINT"].SetOrdinal(itemImport.Columns.IndexOf("SUBPRODUCT_PUBLISH2EXPORT"));
                    }
                    if (!itemImport.Columns.Contains("SUBPRODUCT_PUBLISH2PORTAL"))
                    {
                        itemImport.Columns.Add("SUBPRODUCT_PUBLISH2PORTAL");
                        itemImport.Columns["SUBPRODUCT_PUBLISH2PORTAL"].SetOrdinal(itemImport.Columns.IndexOf("SUBPRODUCT_PUBLISH2PRINT"));
                    }
                }
                else
                {
                    if (!itemImport.Columns.Contains("PRODUCT_PUBLISH2WEB"))
                    {
                        itemImport.Columns.Add("PRODUCT_PUBLISH2WEB");
                        itemImport.Columns["PRODUCT_PUBLISH2WEB"].SetOrdinal(itemImport.Columns.IndexOf("CATALOG_ITEM_NO"));
                    }
                    if (!itemImport.Columns.Contains("PRODUCT_PUBLISH2PDF"))
                    {
                        itemImport.Columns.Add("PRODUCT_PUBLISH2PDF");
                        itemImport.Columns["PRODUCT_PUBLISH2PDF"].SetOrdinal(itemImport.Columns.IndexOf("PRODUCT_PUBLISH2WEB"));
                    }
                    //if (!itemImport.Columns.Contains("PRODUCT_PUBLISH2EXPORT"))
                    //{
                    //    itemImport.Columns.Add("PRODUCT_PUBLISH2EXPORT");
                    //    itemImport.Columns["PRODUCT_PUBLISH2EXPORT"].SetOrdinal(itemImport.Columns.IndexOf("PRODUCT_PUBLISH2PDF"));
                    //}
                    if (!itemImport.Columns.Contains("PRODUCT_PUBLISH2PRINT"))
                    {
                        itemImport.Columns.Add("PRODUCT_PUBLISH2PRINT");
                        itemImport.Columns["PRODUCT_PUBLISH2PRINT"].SetOrdinal(itemImport.Columns.IndexOf("PRODUCT_PUBLISH2PDF"));
                    }
                    //if (!itemImport.Columns.Contains("PRODUCT_PUBLISH2PORTAL"))
                    //{
                    //    itemImport.Columns.Add("PRODUCT_PUBLISH2PORTAL");
                    //    itemImport.Columns["PRODUCT_PUBLISH2PORTAL"].SetOrdinal(itemImport.Columns.IndexOf("PRODUCT_PUBLISH2PRINT"));
                    //}
                }

                var specs = importType.ToUpper().Contains("SUBPRODUCT") ?
                    itemImport.AsEnumerable().Select(item => item.Field<String>("SUBCATALOG_ITEM_NO")).Distinct().ToArray() : itemImport.AsEnumerable().Select(item => item.Field<String>("CATALOG_ITEM_NO")).Distinct().ToArray();
                var product = importType.ToUpper().Contains("SUBPRODUCT") ? objLS.TB_SUBPRODUCT.Join(objLS.TB_PROD_SPECS, ts => ts.SUBPRODUCT_ID, tps => tps.PRODUCT_ID, (ts, tps) => new { ts, tps }).Where(y => y.ts.CATALOG_ID == catalogId && y.tps.ATTRIBUTE_ID == 1 && specs.Contains(y.tps.STRING_VALUE)).Select(x => x.tps.PRODUCT_ID).Distinct().Count() :
                    objLS.TB_CATALOG_PRODUCT.Join(objLS.TB_PROD_SPECS, ts => ts.PRODUCT_ID, tps => tps.PRODUCT_ID, (ts, tps) => new { ts, tps }).Where(y => y.ts.CATALOG_ID == catalogId && y.tps.ATTRIBUTE_ID == 1 && specs.Contains(y.tps.STRING_VALUE)).Select(x => x.tps.PRODUCT_ID).Distinct().Count();

                if (GetSkuCount(product) == "SKU Exceed")
                {
                    return "SKU Exceed~" + importTemp;
                }
                var customers = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();

                //-----------------------------------------------------------Import sheet count handling starting------------------------------------------------------//
                int maximumSheetLength = objLS.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.ImportSheetProductCount).FirstOrDefault();
                //int maximumSheetLength = Int32.Parse(ConfigurationManager.AppSettings["ImportSheetProductCount"].ToString());
                int currentSheetLength = itemImport.Rows.Count;
                int noOfIteration = (currentSheetLength / maximumSheetLength) + 1;

                int importStartCount = 0;
                int importEndCount = maximumSheetLength;

                DataTable resultSet = new DataTable();
                DataSet resultSet1 = new DataSet();

                while (noOfIteration >= 1)
                {
                    DataTable copyItemImportValues = itemImport.Copy();
                    DataTable loopImportData = new DataTable();
                    string sqlString = string.Empty;
                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        sqlString = string.Format("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTDATA{0}'')BEGIN DROP TABLE [##IMPORTDATA{0}] END')", importTemp);
                        SqlCommand newCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlConnection.Open();
                        newCommand.ExecuteNonQuery();

                        sqlString = CreateTable("[##IMPORTDATA" + importTemp + "]", copyItemImportValues);
                        newCommand = new SqlCommand(sqlString, sqlConnection);
                        newCommand.ExecuteNonQuery();

                        var bulkCopy = new SqlBulkCopy(sqlConnection)
                        {
                            DestinationTableName = "[##IMPORTDATA" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(copyItemImportValues);

                        sqlString = string.Format("IF OBJECT_ID('IMPORTTEMPB4IMPORT{0}', 'U') IS NOT NULL   DROP TABLE [IMPORTTEMPB4IMPORT{0}] select IDENTITY(INT,1,1) AS RowID,* into [IMPORTTEMPB4IMPORT{0}] from [##IMPORTDATA{0}]", importTemp);
                        newCommand = new SqlCommand(sqlString, sqlConnection);
                        newCommand.ExecuteNonQuery();

                        sqlString = string.Format("select * from [IMPORTTEMPB4IMPORT{0}] where RowID  between {1} and {2}", importTemp, importStartCount, importEndCount);
                        newCommand = new SqlCommand(sqlString, sqlConnection);
                        SqlDataAdapter importDataDatatable = new SqlDataAdapter(newCommand);
                        importDataDatatable.Fill(loopImportData);
                        sqlConnection.Close();
                    }

                    DataTable importData = UnPivotTable(loopImportData, importType);

                    DataTable attrType = importController.SelectedColumnsToImport(itemImport, model);
                    DataTable AttributeData = new DataTable();
                    AttributeData.Columns.Add("AttrtId");
                    AttributeData.Columns["AttrtId"].DataType = System.Type.GetType("System.Int32");
                    AttributeData.Columns["AttrtId"].AutoIncrement = true;
                    AttributeData.Columns["AttrtId"].AutoIncrementSeed = 1;
                    AttributeData.Columns.Add("ATTRIBUTENAME");
                    AttributeData.Columns.Add("ATTRIBUTETYPE");
                    for (int index = 0; index < attrType.Rows.Count; index++)
                    {
                        DataRow dr = AttributeData.NewRow();
                        dr["ATTRIBUTENAME"] = attrType.Rows[index]["ATTRIBUTE_NAME"].ToString();
                        dr["ATTRIBUTETYPE"] = attrType.Rows[index]["ATTRIBUTE_TYPE"].ToString();
                        AttributeData.Rows.Add(dr);
                    }

                    // -------------------------------------------------------- Mapping - Attributes - Start.                    

                    //var Mappingattributes = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                    //if (Mappingattributes.Count != 0)
                    //{
                    //    for (int i = 0; i < Mappingattributes.Count; i++)
                    //    {
                    //        for (int j = 0; j < importData.Rows.Count; j++)
                    //        {
                    //            if (Mappingattributes[i].Excel_Column.ToString() == importData.Rows[j]["ATTRIBUTENAME"].ToString())
                    //            {
                    //                importData.Rows[j]["ATTRIBUTENAME"] = Mappingattributes[i].Mapping_Name;
                    //            }
                    //        }
                    //    }
                    //}

                    ////Mapping Attribute - End ---------------------------------------------------------------------
                    //var MappingattributesValue = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                    //if (MappingattributesValue.Count != 0)
                    //{
                    //    for (int i = 0; i < MappingattributesValue.Count; i++)
                    //    {
                    //        for (int j = 0; j < AttributeData.Rows.Count; j++)
                    //        {
                    //            if (Mappingattributes[i].Excel_Column.ToString() == AttributeData.Rows[j]["ATTRIBUTENAME"].ToString())
                    //            {
                    //                AttributeData.Rows[j]["ATTRIBUTENAME"] = Mappingattributes[i].Mapping_Name;
                    //            }
                    //        }
                    //    }
                    //}



                    string str_GetImportType = null;

                    if (importType.ToUpper().Contains("CAT"))
                    {
                        str_GetImportType = "Category";
                    }
                    else if (importType.ToUpper().Contains("FAM"))
                    {
                        str_GetImportType = "Family";
                    }
                    else if (importType.ToUpper().Contains("PRO"))
                    {
                        str_GetImportType = "Product";
                    }

                    bool mappingFlag = false;

                    List<string> lt_MissingMappingAttributes = new List<string>();

                    DataTable dt_MappingAttributesErrorLog = new DataTable();

                    dt_MappingAttributesErrorLog.Columns.Add("SheetName");
                    dt_MappingAttributesErrorLog.Columns.Add("ImportType");
                    dt_MappingAttributesErrorLog.Columns.Add("Attribute(s)");



                    var importTempMappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                  Where(x => x.cs.TEMPLATE_ID == templateId && x.cu.IMPORT_TYPE == str_GetImportType).ToList();

                    if (importTempMappingattributesValue.Count != 0)
                    {
                        for (int j = 0; j < importData.Columns.Count; j++)
                        {
                            string excelColumnname = Convert.ToString(importData.Columns[j].ColumnName);
                            //string attributeType = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_TYPE"]);



                            //var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"])  && x.ATTRIBUTE_TYPE!=0).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                            int attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.CAPTION == excelColumnname && x.ATTRIBUTE_ID >= 0 && x.TEMPLATE_ID == templateId).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());
                            if (attributeId != 0)
                            {
                                var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();

                                if (attributeName.Count() > 0)
                                {
                                    importData.Columns[j].ColumnName = attributeName;
                                }
                                else
                                {
                                    // excelData.Columns[j].ColumnName = excelColumnname;
                                    _logger.Info("Missing Attributes/Caption in Mapping table: " + excelColumnname);
                                    //return "Import Failed" + "~MappingAttribute:"+ "~" + excelColumnname;
                                    lt_MissingMappingAttributes.Add(excelColumnname);
                                    DataRow row = dt_MappingAttributesErrorLog.NewRow();
                                    row["SheetName"] = SheetName;
                                    row["ImportType"] = importType;
                                    row["Attribute(s)"] = excelColumnname;
                                    dt_MappingAttributesErrorLog.Rows.Add(row);

                                    mappingFlag = true;
                                }
                            }


                        }

                    }




                    var MappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                                        Where(x => x.cs.TEMPLATE_ID == templateId && x.cu.IMPORT_TYPE == str_GetImportType).ToList();


                    if (MappingattributesValue.Count != 0)
                    {
                        for (int j = 0; j < AttributeData.Rows.Count; j++)
                        {
                            string excelColumnname = Convert.ToString(AttributeData.Rows[j]["ATTRIBUTENAME"]);
                            string attributeType = Convert.ToString(AttributeData.Rows[j]["ATTRIBUTETYPE"]);

                            if (attributeType != "0")
                            {

                                //var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"])  && x.ATTRIBUTE_TYPE!=0).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();

                                var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                     Where(x => x.cs.CAPTION == excelColumnname && x.cs.TEMPLATE_ID == templateId && x.cu.SHEET_NAME == SheetName).ToList();


                                if (checkcolumns.Count > 0)
                                {
                                    int attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.CAPTION == excelColumnname && x.ATTRIBUTE_ID >= 0 && x.TEMPLATE_ID == templateId).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());

                                    if (attributeId > 0)
                                    {
                                        var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                                        AttributeData.Rows[j][1] = attributeName;
                                    }
                                    else if (attributeId == 0)
                                    {
                                        AttributeData.Rows[j][1] = excelColumnname;
                                    }

                                }

                                else
                                {
                                    //var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.CAPTION == excelColumnname).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                                    //if (attributeName != "")
                                    //{ oattType.Rows[j][1] = attributeName; }
                                    //else
                                    //{
                                    _logger.Info("Missing Attributes/Caption in Mapping table: " + excelColumnname);
                                    // return "Import Failed" + "~MappingAttribute:" + "~" + excelColumnname;
                                    lt_MissingMappingAttributes.Add(excelColumnname);
                                    DataRow row = dt_MappingAttributesErrorLog.NewRow();
                                    row["SheetName"] = SheetName;
                                    row["ImportType"] = importType;
                                    row["Attribute(s)"] = excelColumnname;
                                    dt_MappingAttributesErrorLog.Rows.Add(row);
                                    mappingFlag = true;
                                    //}
                                }
                            }
                        }

                    }

                    //Mapping Attribute - End ---------------------------------------------------------------------




                    if (mappingFlag == true)
                    {
                        HttpContext.Current.Session["MissingMappingAttribute"] = lt_MissingMappingAttributes;
                        HttpContext.Current.Session["MappingAttributeErrorLog"] = dt_MappingAttributesErrorLog;
                        return "Import Failed" + "~MappingAttribute";

                    }

                    SqlCommand sqlCommand = new SqlCommand();
                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        if (importType.ToUpper().Contains("SUBPRODUCT"))
                        {
                            sqlCommand = new SqlCommand("STP_CATALOGSTUDIO5_SUBCATALOGITEMNUMBER_IMPORT", sqlConnection);
                        }
                        else
                        {
                            sqlCommand = new SqlCommand("STP_CATALOGSTUDIO5_CATALOGITEMNUMBER_IMPORT", sqlConnection);
                        }
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.CommandTimeout = 0;
                        sqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                        sqlCommand.Parameters.Add("@SESSIONID", SqlDbType.NVarChar).Value = importTemp;
                        sqlCommand.Parameters.Add("@CUSTOMERNAME", SqlDbType.VarChar).Value = User.Identity.Name;
                        sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.VarChar).Value = customers.ToString();
                        sqlCommand.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = importData;
                        sqlCommand.Parameters.Add("@ATTRTEMP", SqlDbType.Structured).Value = AttributeData;
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                        sqlConnection.Open();
                        sqlDataAdapter.Fill(resultSet1);
                        sqlConnection.Close();
                    }

                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        sqlString = string.Format("IF OBJECT_ID('IMPORTTEMPB4IMPORT{0}', 'U') IS NOT NULL   DROP TABLE [IMPORTTEMPB4IMPORT{0}]", importTemp);
                        SqlCommand newCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlConnection.Open();
                        newCommand.ExecuteNonQuery();
                        sqlConnection.Close();
                    }

                    noOfIteration = noOfIteration - 1;
                    importStartCount = importEndCount + 1;
                    importEndCount = importEndCount + maximumSheetLength;
                }
                //-----------------------------------------------------------Import sheet count handling ends------------------------------------------------------//

                resultSet = resultSet1.Tables[0];

                //using (var sqlConnection = new SqlConnection(connectionString))
                //{
                //    sqlConnection.Open();
                //    string sqlString33 = string.Empty;    


                //    sqlString33 = CreateTable("[importTemp]", resultSet);
                //    SqlCommand newCommand33 = new SqlCommand(sqlString33, sqlConnection);
                //    newCommand33.ExecuteNonQuery();

                //    var bulkCopy = new SqlBulkCopy(sqlConnection)
                //    {
                //        DestinationTableName = "[importTemp]"
                //    };
                //    bulkCopy.WriteToServer(resultSet);
                //    sqlConnection.Close();
                //}



                DataTable skipped = GetDataFromExcel(SheetName, excelPath, "SELECT * from [" + SheetName + "$]", "SKIP");
                if (resultSet != null && resultSet.Columns.Contains("STATUS"))
                {
                    if (resultSet.Rows.Count > 0)
                    {
                        _logger.Error("New Count");
                        _logger.Error(resultSet.Rows.Count);
                        int insertedRecourds = importType.ToUpper().Contains("SUBPRODUCT") ? resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "INSERT").Select(cat => cat.Field<string>("SUBCATALOG_ITEM_NO")).Distinct().Count() : resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "INSERT").Select(cat => cat.Field<string>("CATALOG_ITEM_NO")).Distinct().Count();
                        int updatedRecourds = importType.ToUpper().Contains("SUBPRODUCT") ? resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "UPDATE").Select(cat => cat.Field<string>("SUBCATALOG_ITEM_NO")).Distinct().Count() : resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "UPDATE").Select(cat => cat.Field<int>("PRODUCT_ID")).Distinct().Count();
                        DataTable detachRecords = GetDataFromExcelForDetach(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DETACH");
                        int detachcount = detachRecords.Rows.Count;
                        if (!(updatedRecourds == 0) && !(updatedRecourds == null))
                        { updatedRecourds = updatedRecourds - detachcount; }

                        _logger.Error("New Update Count");
                        _logger.Error(updatedRecourds);
                        _logger.Error(skipped.Rows.Count);

                        // int updatedRecourds = importType.ToUpper().Contains("SUBPRODUCT") ? resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "UPDATE" || string.IsNullOrEmpty(x.Field<string>("STATUS"))).Select(cat => cat.Field<string>("SUBCATALOG_ITEM_NO")).Distinct().Count() : resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "UPDATE" || string.IsNullOrEmpty(x.Field<string>("STATUS"))).Select(cat => cat.Field<string>("CATALOG_ITEM_NO")).Distinct().Count();
                        if (skipped != null && skipped.Rows.Count > 0)
                        {
                            return "Import Success~" + importTemp + "~InsertRecords:" + insertedRecourds + "~" + "UpdateRecords:" + updatedRecourds + "~SkippedRecords:" + skipped.Rows.Count + "~ITEM#";
                        }
                        else
                        {
                            return "Import Success~" + importTemp + "~InsertRecords:" + insertedRecourds + "~" + "UpdateRecords:" + updatedRecourds + "~SkippedRecords:0" + "~ITEM#";
                        }
                    }
                    else
                    {
                        if (skipped != null && skipped.Rows.Count > 0)
                        {
                            return "Import Success~" + importTemp + "~InsertRecords:0~" + "UpdateRecords:0~SkippedRecords:" + skipped.Rows.Count + "~ITEM#";
                        }
                        else
                        {
                            return "Import Success~" + importTemp + "~InsertRecords:0~" + "UpdateRecords:0~SkippedRecords:0" + "~ITEM#";
                        }
                    }
                }
                else
                {
                    if (resultSet != null && resultSet.Rows.Count > 0)
                    {
                        HttpContext.Current.Session["ITEMIMPORTSESSION"] = resultSet;
                        return "Import Failed~" + importTemp + "~ITEM#";
                    }
                    return "Import Failed~" + importTemp + "~ITEMEMPTY";
                }


            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : CatalogItemNumberImport", ex);
                return "Import failed";
            }
        }
        #endregion

        #region UnPivot DataTable
        /// <summary>
        /// UnPivot DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public DataTable UnPivotTable(DataTable dataItems, string importType)
        {
            try
            {
                string[] columns = dataItems.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                DataTable unPivotTable = new DataTable();
                unPivotTable.Columns.Add("ImportId");
                unPivotTable.Columns["ImportId"].DataType = System.Type.GetType("System.Int32");
                unPivotTable.Columns["ImportId"].AutoIncrement = true;
                unPivotTable.Columns["ImportId"].AutoIncrementSeed = 1;
                if (importType.ToUpper().Contains("BULK"))
                    unPivotTable.Columns.Add("ROW");
                unPivotTable.Columns.Add("CATALOG_ITEM_NO");
                if (importType.ToUpper().Contains("SUBPRODUCT"))
                {
                    unPivotTable.Columns.Add("SUBCATALOG_ITEM_NO");
                    unPivotTable.Columns.Add("SUBPRODUCT_ID");
                }

                unPivotTable.Columns.Add("ATTRIBUTENAME");
                unPivotTable.Columns.Add("ATTRIBUTEVALUE");
                unPivotTable.Columns.Add("PRODUCT_ID");
                if (!dataItems.Columns.Contains("PRODUCT_ID"))
                    dataItems.Columns.Add("PRODUCT_ID");
                if (dataItems.Columns.Contains("PRODUCT_ID"))
                    dataItems.Columns["PRODUCT_ID"].SetOrdinal(0);
                if (importType.ToUpper().Contains("SUBPROD") && !dataItems.Columns.Contains("SUBPRODUCT_ID"))
                    dataItems.Columns.Add("SUBPRODUCT_ID");
                if (importType.ToUpper().Contains("SUBPROD") && dataItems.Columns.Contains("SUBPRODUCT_ID"))
                    dataItems.Columns["SUBPRODUCT_ID"].SetOrdinal(2);
                for (int rowIndex = 0; rowIndex < dataItems.Rows.Count; rowIndex++)
                {
                    for (int index = 0; index < columns.Length; index++)
                    {
                        if (!columns[index].ToUpper().Contains("CATALOG_ITEM_NO") && !columns[index].ToUpper().Contains("PRODUCT_ID") && !columns[index].ToUpper().Contains("SUBPRODUCT_ID") && (!columns[index].ToUpper().Contains("ACTION") || importType.Contains("BATCH")))
                        {
                            DataRow dr = unPivotTable.NewRow();
                            dr["CATALOG_ITEM_NO"] = dataItems.Rows[rowIndex]["CATALOG_ITEM_NO"];
                            if (importType.ToUpper().Contains("SUBPRODUCT"))
                            {
                                dr["SUBCATALOG_ITEM_NO"] = dataItems.Rows[rowIndex]["SUBCATALOG_ITEM_NO"];
                                if (dataItems.Columns[columns[index]].ColumnName.ToUpper() == "SUBPRODUCT_ID")
                                {
                                    dataItems.Columns[columns[index]].ColumnName = "SUBPRODUCT_ID";
                                }
                                dr["SUBPRODUCT_ID"] = dataItems.Rows[rowIndex]["SUBPRODUCT_ID"];

                            }
                            dr["ATTRIBUTENAME"] = columns[index];
                            dr["ATTRIBUTEVALUE"] = dataItems.Rows[rowIndex][columns[index]].ToString();
                            if (dataItems.Columns[columns[index]].ColumnName.ToUpper() == "PRODUCT_ID")
                            {
                                dataItems.Columns[columns[index]].ColumnName = "PRODUCT_ID";
                            }
                            dr["PRODUCT_ID"] = dataItems.Rows[rowIndex]["PRODUCT_ID"];
                            if (importType.ToUpper().Contains("BULK"))
                                dr["ROW"] = rowIndex;
                            unPivotTable.Rows.Add(dr);
                        }
                    }
                }
                if (unPivotTable.Columns.Contains("PRODUCT_ID"))
                {
                    unPivotTable.Columns["PRODUCT_ID"].SetOrdinal(unPivotTable.Columns.IndexOf("CATALOG_ITEM_NO"));
                }
                if (unPivotTable.Columns.Contains("SUBPRODUCT_ID"))
                {
                    unPivotTable.Columns["SUBPRODUCT_ID"].SetOrdinal(unPivotTable.Columns.IndexOf("SUBCATALOG_ITEM_NO"));
                }

                return unPivotTable;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : UnPivotTable", ex);
                return null;
            }
        }





        #endregion
        // Item# import process start
        #endregion

        #region check without family level hierarchy
        public DataTable ValidateFamilyData(DataTable familyImport)
        {
            try
            {
                bool FamilyImport = true;
                int removeFields = 0;
                string[] CheckcolumnName = { "CATEGORY_ID", "CATEGORY_NAME" };
                for (int index = 0; index < familyImport.Columns.Count; index++)
                {
                    if ((CheckcolumnName.Contains(familyImport.Columns[index].ColumnName.ToUpper()) && familyImport.Columns[index].ColumnName.ToUpper().Contains("NAME")) || familyImport.Columns[index].ColumnName.ToUpper().Contains("SUBCATNAME"))
                    {
                        var emptyRowCount = familyImport.AsEnumerable().Where(x => string.IsNullOrEmpty(x.Field<string>(familyImport.Columns[index].ColumnName))).Select(y => y.Field<string>(familyImport.Columns[index].ColumnName)).Distinct().Count();
                        if (emptyRowCount == familyImport.Rows.Count)
                        {
                            familyImport.Columns.RemoveAt(index);
                            removeFields = removeFields + 1;
                        }
                        else
                        {
                            FamilyImport = false;
                        }
                    }

                }
                if (removeFields > 1)
                {
                    for (int index = 0; index < familyImport.Columns.Count; index++)
                    {
                        if (CheckcolumnName.Contains(familyImport.Columns[index].ColumnName.ToUpper()) || familyImport.Columns[index].ColumnName.ToUpper().Contains("SUBCAT"))
                        {
                            familyImport.Columns.RemoveAt(index);
                        }
                    }
                }
                if (FamilyImport == false)
                {
                    return null;
                }
                return familyImport;

            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : ValidateItemNumber", ex);
                return familyImport;
            }
        }
        #region UnPivotTableFamily
        /// <summary>
        /// UnPivot DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public DataTable UnPivotTableFamily(DataTable dataItems, string importType)
        {
            try
            {
                string[] columns = dataItems.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                DataTable unPivotTable = new DataTable();
                unPivotTable.Columns.Add("ImportId");
                unPivotTable.Columns["ImportId"].DataType = System.Type.GetType("System.Int32");
                unPivotTable.Columns["ImportId"].AutoIncrement = true;
                unPivotTable.Columns["ImportId"].AutoIncrementSeed = 1;
                if (importType.ToUpper().Contains("BULK"))
                    unPivotTable.Columns.Add("ROW");
                unPivotTable.Columns.Add("FAMILY_NAME");
                unPivotTable.Columns.Add("SUBFAMILY_NAME");
                unPivotTable.Columns.Add("SUBFAMILY_ID");
                unPivotTable.Columns.Add("ATTRIBUTENAME");
                unPivotTable.Columns.Add("ATTRIBUTEVALUE");
                unPivotTable.Columns.Add("FAMILY_ID");
                if (!dataItems.Columns.Contains("FAMILY_ID"))
                    dataItems.Columns.Add("FAMILY_ID");
                if (dataItems.Columns.Contains("FAMILY_ID"))
                    dataItems.Columns["FAMILY_ID"].SetOrdinal(0);
                if (!dataItems.Columns.Contains("SUBFAMILY_ID"))
                    dataItems.Columns.Add("SUBFAMILY_ID");
                if (dataItems.Columns.Contains("SUBFAMILY_ID"))
                    dataItems.Columns["SUBFAMILY_ID"].SetOrdinal(2);

                if (!dataItems.Columns.Contains("SUBFAMILY_NAME"))
                    dataItems.Columns.Add("SUBFAMILY_NAME");
                for (int rowIndex = 0; rowIndex < dataItems.Rows.Count; rowIndex++)
                {
                    for (int index = 0; index < columns.Length; index++)
                    {
                        if (!columns[index].ToUpper().Contains("FAMILY_NAME") && !columns[index].ToUpper().Contains("FAMILY_ID") && !columns[index].ToUpper().Contains("SUBFAMILY_ID") && !columns[index].ToUpper().Contains("ACTION"))
                        {
                            DataRow dr = unPivotTable.NewRow();
                            dr["FAMILY_NAME"] = dataItems.Rows[rowIndex]["FAMILY_NAME"];
                            dr["SUBFAMILY_NAME"] = dataItems.Rows[rowIndex]["SUBFAMILY_NAME"];
                            if (dataItems.Columns[columns[index]].ColumnName.ToUpper() == "SUBFAMILY_ID")
                            {
                                dataItems.Columns[columns[index]].ColumnName = "SUBFAMILY_ID";
                            }
                            dr["SUBFAMILY_ID"] = dataItems.Rows[rowIndex]["SUBFAMILY_ID"];


                            dr["ATTRIBUTENAME"] = columns[index];
                            dr["ATTRIBUTEVALUE"] = dataItems.Rows[rowIndex][columns[index]].ToString();
                            if (dataItems.Columns[columns[index]].ColumnName.ToUpper() == "FAMILY_ID")
                            {
                                dataItems.Columns[columns[index]].ColumnName = "FAMILY_ID";
                            }
                            dr["FAMILY_ID"] = dataItems.Rows[rowIndex]["FAMILY_ID"];
                            if (importType.ToUpper().Contains("BULK"))
                                dr["ROW"] = rowIndex;
                            unPivotTable.Rows.Add(dr);
                        }
                    }
                }
                if (unPivotTable.Columns.Contains("FAMILY_ID"))
                {
                    unPivotTable.Columns["FAMILY_ID"].SetOrdinal(unPivotTable.Columns.IndexOf("FAMILY_NAME"));
                }
                if (unPivotTable.Columns.Contains("SUBFAMILY_ID"))
                {
                    unPivotTable.Columns["SUBFAMILY_ID"].SetOrdinal(unPivotTable.Columns.IndexOf("SUBFAMILY_NAME"));
                }

                return unPivotTable;
            }

            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : UnPivotTable", ex);
                return null;
            }
        }

        #endregion
        #endregion

        #region check without category level hierarchy
        /// <summary>
        /// 
        /// </summary>
        /// <param name="categoryImport"></param>
        /// <returns></returns>
        public DataTable ValidateCategoryData(DataTable categoryImport)
        {
            try
            {
                bool categoryImportValidation = true;
                int removeFields = 0;
                string[] CheckcolumnName = { "CATEGORY_ID", "CATEGORY_NAME" };
                for (int index = 0; index < categoryImport.Columns.Count; index++)
                {
                    if ((CheckcolumnName.Contains(categoryImport.Columns[index].ColumnName.ToUpper()) && categoryImport.Columns[index].ColumnName.ToUpper().Contains("NAME")) || categoryImport.Columns[index].ColumnName.ToUpper().Contains("SUBCATNAME"))
                    {
                        var emptyRowCount = categoryImport.AsEnumerable().Where(x => string.IsNullOrEmpty(x.Field<string>(categoryImport.Columns[index].ColumnName))).Select(y => y.Field<string>(categoryImport.Columns[index].ColumnName)).Distinct().Count();
                        if (emptyRowCount == categoryImport.Rows.Count)
                        {
                            categoryImport.Columns.RemoveAt(index);
                            removeFields = removeFields + 1;
                        }
                        else
                        {
                            categoryImportValidation = false;
                        }
                    }

                }
                if (removeFields > 1)
                {
                    for (int index = 0; index < categoryImport.Columns.Count; index++)
                    {
                        if (CheckcolumnName.Contains(categoryImport.Columns[index].ColumnName.ToUpper()) || categoryImport.Columns[index].ColumnName.ToUpper().Contains("SUBCAT"))
                        {
                            categoryImport.Columns.RemoveAt(index);
                        }
                    }
                }
                if (categoryImportValidation == false)
                {
                    return null;
                }
                return categoryImport;

            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : ValidateItemNumber", ex);
                return categoryImport;
            }

        }

        #endregion
        #region UnPivotTableCategory
        /// <summary>
        /// UnPivot DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public DataTable UnPivotTableCategory(DataTable dataItems, string importType)
        {
            try
            {
                string[] columns = dataItems.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                DataTable unPivotTable = new DataTable();
                unPivotTable.Columns.Add("ImportId");
                unPivotTable.Columns["ImportId"].DataType = System.Type.GetType("System.Int32");
                unPivotTable.Columns["ImportId"].AutoIncrement = true;
                unPivotTable.Columns["ImportId"].AutoIncrementSeed = 1;
                if (importType.ToUpper().Contains("BULK"))
                    unPivotTable.Columns.Add("ROW");
                unPivotTable.Columns.Add("CATEGORY_NAME");
                unPivotTable.Columns.Add("ATTRIBUTENAME");
                unPivotTable.Columns.Add("ATTRIBUTEVALUE");
                unPivotTable.Columns.Add("CATEGORY_ID");
                if (!dataItems.Columns.Contains("CATEGORY_ID"))
                    dataItems.Columns.Add("CATEGORY_ID");
                if (dataItems.Columns.Contains("CATEGORY_ID"))
                    dataItems.Columns["CATEGORY_ID"].SetOrdinal(0);
                for (int rowIndex = 0; rowIndex < dataItems.Rows.Count; rowIndex++)
                {
                    for (int index = 0; index < columns.Length; index++)
                    {
                        if (!columns[index].ToUpper().Contains("CATEGORY_NAME") && !columns[index].ToUpper().Contains("CATEGORY_ID") && !columns[index].ToUpper().Contains("ACTION"))
                        {
                            DataRow dr = unPivotTable.NewRow();
                            dr["CATEGORY_NAME"] = dataItems.Rows[rowIndex]["CATEGORY_NAME"];
                            dr["ATTRIBUTENAME"] = columns[index];
                            dr["ATTRIBUTEVALUE"] = dataItems.Rows[rowIndex][columns[index]].ToString();
                            if (dataItems.Columns[columns[index]].ColumnName.ToUpper() == "CATEGORY_ID")
                            {
                                dataItems.Columns[columns[index]].ColumnName = "CATEGORY_ID";
                            }
                            dr["CATEGORY_ID"] = dataItems.Rows[rowIndex]["CATEGORY_ID"];
                            if (importType.ToUpper().Contains("BULK"))
                                dr["ROW"] = rowIndex;
                            unPivotTable.Rows.Add(dr);
                        }
                    }
                }
                if (unPivotTable.Columns.Contains("CATEGORY_ID"))
                {
                    unPivotTable.Columns["CATEGORY_ID"].SetOrdinal(unPivotTable.Columns.IndexOf("CATEGORY_NAME"));
                }

                return unPivotTable;
            }

            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : UnPivotTable", ex);
                return null;
            }
        }

        #endregion

        public string CatalogFamilyNameImport(DataTable familyImport, string SheetName, string allowDuplicate, string excelPath, int catalogId, string importType, JArray model, string importTemp, int templateId)
        {
            try
            {

                var specs = familyImport.AsEnumerable().Select(item => item.Field<String>("FAMILY_NAME")).Distinct().ToArray();
                var product = objLS.TB_CATALOG_FAMILY.Join(objLS.TB_FAMILY_SPECS, ts => ts.FAMILY_ID, tps => tps.FAMILY_ID, (ts, tps) => new { ts, tps }).Where(y => y.ts.CATALOG_ID == catalogId && y.tps.ATTRIBUTE_ID == 1 && specs.Contains(y.tps.STRING_VALUE)).Select(x => x.tps.FAMILY_ID).Distinct().Count();

                if (!familyImport.Columns.Contains("FAMILY_PUBLISH2WEB"))
                {
                    familyImport.Columns.Add("FAMILY_PUBLISH2WEB");
                    familyImport.Columns["FAMILY_PUBLISH2WEB"].SetOrdinal(familyImport.Columns.IndexOf("FAMILY_ID"));
                }
                if (!familyImport.Columns.Contains("FAMILY_PUBLISH2PDF"))
                {
                    familyImport.Columns.Add("FAMILY_PUBLISH2PDF");
                    familyImport.Columns["FAMILY_PUBLISH2PDF"].SetOrdinal(familyImport.Columns.IndexOf("FAMILY_PUBLISH2WEB"));
                }
                //if (!familyImport.Columns.Contains("FAMILY_PUBLISH2EXPORT"))
                //{
                //    familyImport.Columns.Add("FAMILY_PUBLISH2EXPORT");
                //    familyImport.Columns["FAMILY_PUBLISH2EXPORT"].SetOrdinal(familyImport.Columns.IndexOf("FAMILY_PUBLISH2PDF"));
                //}
                if (!familyImport.Columns.Contains("FAMILY_PUBLISH2PRINT"))
                {
                    familyImport.Columns.Add("FAMILY_PUBLISH2PRINT");
                    familyImport.Columns["FAMILY_PUBLISH2PRINT"].SetOrdinal(familyImport.Columns.IndexOf("FAMILY_PUBLISH2EXPORT"));
                }
                //if (!familyImport.Columns.Contains("FAMILY_PUBLISH2PORTAL"))
                //{
                //    familyImport.Columns.Add("FAMILY_PUBLISH2PORTAL");
                //    familyImport.Columns["FAMILY_PUBLISH2PORTAL"].SetOrdinal(familyImport.Columns.IndexOf("FAMILY_PUBLISH2PRINT"));
                //}

                var customers = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                DataTable importData = UnPivotTableFamily(familyImport, importType);

                DataTable attrType = importController.SelectedFamilyColumnsToImport(familyImport, model);
                DataTable AttributeData = new DataTable();
                AttributeData.Columns.Add("AttrtId");
                AttributeData.Columns["AttrtId"].DataType = System.Type.GetType("System.Int32");
                AttributeData.Columns["AttrtId"].AutoIncrement = true;
                AttributeData.Columns["AttrtId"].AutoIncrementSeed = 1;
                AttributeData.Columns.Add("ATTRIBUTENAME");
                AttributeData.Columns.Add("ATTRIBUTETYPE");
                for (int index = 0; index < attrType.Rows.Count; index++)
                {
                    DataRow dr = AttributeData.NewRow();
                    dr["ATTRIBUTENAME"] = attrType.Rows[index]["ATTRIBUTE_NAME"].ToString();
                    dr["ATTRIBUTETYPE"] = attrType.Rows[index]["ATTRIBUTE_TYPE"].ToString();
                    AttributeData.Rows.Add(dr);
                }



                // -------------------------------------------------------- Mapping - Attributes - Start.

                //Mapping attribute -Start-------------------------------------------------------------------

                //var Mappingattributes = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                //if (Mappingattributes.Count != 0)
                //{
                //    for (int i = 0; i < Mappingattributes.Count; i++)
                //    {
                //        for (int j = 0; j < importData.Rows.Count; j++)
                //        {
                //            if (Mappingattributes[i].Excel_Column.ToString() == importData.Rows[j]["ATTRIBUTENAME"].ToString())
                //            {
                //                importData.Rows[j]["ATTRIBUTENAME"] = Mappingattributes[i].Mapping_Name;
                //            }

                //        }
                //    }
                //}

                string str_GetImportType = null;

                if (importType.ToUpper().Contains("CAT"))
                {
                    str_GetImportType = "Category";
                }
                else if (importType.ToUpper().Contains("FAM"))
                {
                    str_GetImportType = "Family";
                }
                else if (importType.ToUpper().Contains("PRO"))
                {
                    str_GetImportType = "Product";
                }

                bool mappingFlag = false;

                List<string> lt_MissingMappingAttributes = new List<string>();

                DataTable dt_MappingAttributesErrorLog = new DataTable();

                dt_MappingAttributesErrorLog.Columns.Add("SheetName");
                dt_MappingAttributesErrorLog.Columns.Add("ImportType");
                dt_MappingAttributesErrorLog.Columns.Add("Attribute(s)");

                var importTempMappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
              Where(x => x.cs.TEMPLATE_ID == templateId && x.cu.IMPORT_TYPE == str_GetImportType).ToList();

                if (importTempMappingattributesValue.Count != 0)
                {
                    for (int j = 0; j < importData.Columns.Count; j++)
                    {
                        string excelColumnname = Convert.ToString(importData.Columns[j].ColumnName);
                        //string attributeType = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_TYPE"]);



                        //var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"])  && x.ATTRIBUTE_TYPE!=0).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                        int attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.CAPTION == excelColumnname && x.ATTRIBUTE_ID >= 0 && x.TEMPLATE_ID == templateId).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());
                        if (attributeId != 0)
                        {
                            var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();

                            if (attributeName.Count() > 0)
                            {
                                importData.Columns[j].ColumnName = attributeName;
                            }
                            else
                            {
                                // excelData.Columns[j].ColumnName = excelColumnname;
                                _logger.Info("Missing Attributes/Caption in Mapping table: " + excelColumnname);
                                // return "Import Failed" + "~MappingAttribute:" + "~" + excelColumnname;
                                lt_MissingMappingAttributes.Add(excelColumnname);
                                DataRow row = dt_MappingAttributesErrorLog.NewRow();
                                row["SheetName"] = SheetName;
                                row["ImportType"] = importType;
                                row["Attribute(s)"] = excelColumnname;
                                dt_MappingAttributesErrorLog.Rows.Add(row);
                                mappingFlag = true;
                            }
                        }


                    }

                }


                //Mapping Attribute - End ---------------------------------------------------------------------



                //var MappingattributesValue = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                //if (MappingattributesValue.Count != 0)
                //{
                //    for (int i = 0; i < MappingattributesValue.Count; i++)
                //    {
                //        for (int j = 0; j < AttributeData.Rows.Count; j++)
                //        {
                //            if (Mappingattributes[i].Excel_Column.ToString() == AttributeData.Rows[j]["ATTRIBUTENAME"].ToString())
                //            {
                //                AttributeData.Rows[j]["ATTRIBUTENAME"] = Mappingattributes[i].Mapping_Name;
                //            }
                //        }
                //    }
                //}


                var MappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                                    Where(x => x.cs.TEMPLATE_ID == templateId && x.cu.IMPORT_TYPE == str_GetImportType).ToList();


                if (MappingattributesValue.Count != 0)
                {
                    for (int j = 0; j < AttributeData.Rows.Count; j++)
                    {
                        string excelColumnname = Convert.ToString(AttributeData.Rows[j]["ATTRIBUTENAME"]);
                        string attributeType = Convert.ToString(AttributeData.Rows[j]["ATTRIBUTETYPE"]);

                        if (attributeType != "0")
                        {

                            //var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"])  && x.ATTRIBUTE_TYPE!=0).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();

                            var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                 Where(x => x.cs.CAPTION == excelColumnname && x.cs.TEMPLATE_ID == templateId && x.cu.SHEET_NAME == SheetName).ToList();


                            if (checkcolumns.Count > 0)
                            {
                                int attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.CAPTION == excelColumnname && x.ATTRIBUTE_ID >= 0 && x.TEMPLATE_ID == templateId).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());

                                if (attributeId > 0)
                                {
                                    var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                                    AttributeData.Rows[j][1] = attributeName;
                                }
                                else if (attributeId == 0)
                                {
                                    AttributeData.Rows[j][1] = excelColumnname;
                                }

                            }

                            else
                            {
                                //var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.CAPTION == excelColumnname).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                                //if (attributeName != "")
                                //{ oattType.Rows[j][1] = attributeName; }
                                //else
                                //{
                                _logger.Info("Missing Attributes/Caption in Mapping table: " + excelColumnname);
                                // return "Import Failed" + "~MappingAttribute:" + "~" + excelColumnname;
                                lt_MissingMappingAttributes.Add(excelColumnname);
                                DataRow row = dt_MappingAttributesErrorLog.NewRow();
                                row["SheetName"] = SheetName;
                                row["ImportType"] = importType;
                                row["Attribute(s)"] = excelColumnname;
                                dt_MappingAttributesErrorLog.Rows.Add(row);
                                mappingFlag = true;
                                //}
                            }
                        }
                    }

                }

                //Mapping Attribute - End ---------------------------------------------------------------------



                if (mappingFlag == true)
                {
                    HttpContext.Current.Session["MissingMappingAttribute"] = lt_MissingMappingAttributes;
                    HttpContext.Current.Session["MappingAttributeErrorLog"] = dt_MappingAttributesErrorLog;
                    return "Import Failed" + "~MappingAttribute";

                }



                DataTable resultSet = new DataTable();
                DataSet resultSet1 = new DataSet();
                SqlCommand sqlCommand = new SqlCommand();
                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    sqlCommand = new SqlCommand("STP_CATALOGSTUDIO5_FAMILY_WITHOUT_HIERARCHY_IMPORT", sqlConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    sqlCommand.Parameters.Add("@SESSIONID", SqlDbType.NVarChar).Value = importTemp;
                    sqlCommand.Parameters.Add("@CUSTOMERNAME", SqlDbType.VarChar).Value = User.Identity.Name;
                    sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.VarChar).Value = customers.ToString();
                    sqlCommand.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = importData;
                    sqlCommand.Parameters.Add("@ATTRTEMP", SqlDbType.Structured).Value = AttributeData;
                    sqlCommand.CommandTimeout = 0;
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlConnection.Open();
                    sqlDataAdapter.Fill(resultSet1);
                    sqlConnection.Close();
                }
                resultSet = resultSet1.Tables[0];
                DataTable skipped = GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "SKIP");








                if (resultSet != null && resultSet.Columns.Contains("STATUS"))
                {
                    if (resultSet.Rows.Count > 0)
                    {
                        int insertedRecourds = resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "INSERT").Select(cat => cat.Field<string>("FAMILY_NAME")).Distinct().Count();
                        int updatedRecourds = resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "UPDATE").Select(cat => cat.Field<string>("FAMILY_NAME")).Distinct().Count();
                        if (skipped != null && skipped.Rows.Count > 0)
                        {
                            return "Import Success~" + importTemp + "~InsertRecords:" + insertedRecourds + "~" + "UpdateRecords:" + updatedRecourds + "~SkippedRecords:" + skipped.Rows.Count + "~FAMILY_NAME";
                        }
                        else
                        {
                            return "Import Success~" + importTemp + "~InsertRecords:" + insertedRecourds + "~" + "UpdateRecords:" + updatedRecourds + "~SkippedRecords:0" + "~FAMILY_NAME";
                        }
                    }
                    else
                    {
                        if (skipped != null && skipped.Rows.Count > 0)
                        {
                            return "Import Success~" + importTemp + "~InsertRecords:0~" + "UpdateRecords:0~SkippedRecords:" + skipped.Rows.Count + "~ITEM#";
                        }
                        else
                        {
                            return "Import Success~" + importTemp + "~InsertRecords:0~" + "UpdateRecords:0~SkippedRecords:0" + "~ITEM#";
                        }
                    }
                }
                else
                {
                    if (resultSet != null && resultSet.Rows.Count > 0)
                    {
                        HttpContext.Current.Session["ITEMIMPORTSESSION"] = resultSet;
                        return "Import Failed~" + importTemp + "~FAMILY_NAME";
                    }
                    return "Import Failed~" + importTemp + "~ITEMEMPTY";
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : CatalogFamilyNameImport", ex);
                return "Import failed";
            }
        }

        public string FinishImportFamilyLevelTableDesigner(string excelSheetPath, string sheetName)
        {
            try
            {
                int user = 0;
                ImportApiController importApiController = new ImportApiController();
                ProgressBar progressBar = new ProgressBar();
                string importType = "TableDesigner";
                string SheetName = sheetName;
                string importResult = string.Empty;
                string allowDuplicate = string.Empty;
                System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
                string validateResults = string.Empty;
                string sqlConn = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                int updatecount = 0;
                int newInsertedcount = 0;
                string importTemp;
                importTemp = Guid.NewGuid().ToString();
                var tableDesignerFamilyId = new List<string>();
                List<string> familyIdRemove = new List<string>();
                var removeZeroProductRowForFamily = new List<string>();
                if (importType == "TableDesigner")
                {
                    stopWatch.Start();
                    progressBar.progressVale = "5";
                    DataTable excelData = GetDataFromExcel(SheetName, excelSheetPath, " SELECT * from [" + SheetName + "$]", "");
                    importApiController.AddIdColumnsForTableDesigner(excelData, importType);  //Add ID columns if it not exists
                    var excelDistinctCatalogName = excelData.AsEnumerable()
                    .Select(s => new
                    {
                        catalogName = s.Field<string>("CATALOG_NAME"),
                    })
                    .Distinct().ToList();
                    string catalogName = excelDistinctCatalogName[0].catalogName.ToString();
                    string catalogid = objLS.TB_CATALOG.Where(s => s.CATALOG_NAME == catalogName && s.FLAG_RECYCLE == "A").Select(a => a.CATALOG_ID).SingleOrDefault().ToString();
                    foreach (DataRow rows in excelData.Rows)
                    {
                        rows["CATALOG_ID"] = catalogid;
                    }
                    DataSet importDataset = new DataSet();
                    string sqlString = string.Empty;
                    using (var objSqlConnection = new SqlConnection(sqlConn))
                    {

                        objSqlConnection.Open();
                        sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                        _DBCommand.ExecuteNonQuery();
                        sqlString = CreateTable("[##IMPORTTEMP" + importTemp + "]", excelData);
                        SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                        dbCommandnew.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(objSqlConnection)
                        {
                            DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(excelData);
                        sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";
                        _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                        _DBCommand.CommandTimeout = 0;
                        _DBCommand.ExecuteNonQuery();
                        var objSqlDataAdapter = new SqlDataAdapter("select * from [##IMPORTTEMP" + importTemp + "]", objSqlConnection);
                        objSqlDataAdapter.Fill(importDataset);
                    }
                    DataTable importDatatable = new DataTable();
                    importDatatable = importDataset.Tables[0];
                    DataTable deletedRecords = GetDataFromExcel(SheetName, excelSheetPath, " SELECT * from [" + SheetName + "$]", "DELETE");
                    importApiController.AddIdColumnsForTableDesigner(deletedRecords, importType);  //Add ID columns if it not exists
                    foreach (DataRow rows in deletedRecords.Rows)
                    {
                        rows["CATALOG_ID"] = catalogid;
                    }
                    DataSet deleteImportDataset = new DataSet();
                    if (deletedRecords.Rows.Count > 0)
                    {
                        using (var objSqlConnection = new SqlConnection(sqlConn))
                        {
                            objSqlConnection.Open();
                            sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                            SqlCommand _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                            _DBCommand.ExecuteNonQuery();
                            sqlString = CreateTable("[##IMPORTTEMP" + importTemp + "]", deletedRecords);
                            SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                            dbCommandnew.ExecuteNonQuery();
                            var bulkCopy = new SqlBulkCopy(objSqlConnection)
                            {
                                DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                            };
                            bulkCopy.WriteToServer(deletedRecords);
                            sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";
                            _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                            _DBCommand.CommandTimeout = 0;
                            _DBCommand.ExecuteNonQuery();
                            var objSqlDataAdapter = new SqlDataAdapter("select * from [##IMPORTTEMP" + importTemp + "]", objSqlConnection);
                            objSqlDataAdapter.Fill(deleteImportDataset);
                        }
                        deletedRecords = deleteImportDataset.Tables[0];
                    }


                    // Skipped records
                    DataTable skippedRecords = GetDataFromExcel(SheetName, excelSheetPath, " SELECT * from [" + SheetName + "$]", "SKIP");
                    if (skippedRecords != null && skippedRecords.Rows.Count > 0)
                    {
                        int skippedCount = skippedRecords.Rows.Count;
                        importResult = importResult + "~SkippedRecords:" + skippedCount.ToString();
                    }
                    else
                    {
                        importResult = importResult + "~SkippedRecords:" + "0";
                    }

                    // Deleted records
                    if (deletedRecords != null && deletedRecords.Rows.Count > 0)
                    {
                        int deleteCount = deletedRecords.Rows.Count;
                        importResult = importResult + "~DeletedRecords:" + deleteCount.ToString();
                    }
                    else
                    {
                        importResult = importResult + "~DeletedRecords:" + "0";
                    }

                    foreach (DataRow deleteRow in deletedRecords.Rows)
                    {
                        string deleteCatalogId = deleteRow["CATALOG_ID"].ToString();
                        string deleteFamilyId = deleteRow["FAMILY_ID"].ToString();
                        string deleteStructureNamevalue = deleteRow["STRUCTURE_NAME"].ToString();
                        string deleteFamilyTableDesigner = deleteRow["FAMILY_TABLE_STRUCTURE"].ToString();
                        using (var objSqlConnection = new SqlConnection(sqlConn))
                        {
                            string action = "TableDesignerDelete";
                            objSqlConnection.Open();
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.CommandTimeout = 0;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = deleteCatalogId ?? "";
                            objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                            objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                            objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = deleteFamilyId ?? "";
                            objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                            objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = "";
                            objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 4000).Value = deleteStructureNamevalue ?? "";
                            objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar, 500).Value = "";
                            objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar, -1).Value = deleteFamilyTableDesigner ?? "";
                            objSqlCommand.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action ?? "";
                            objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                            objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                            objSqlCommand.ExecuteNonQuery();
                            objSqlConnection.Close();
                        }
                    }
                    if (importDatatable.Rows.Count > 0)
                    {
                        tableDesignerFamilyId = importDatatable.AsEnumerable()
                       .Select(r => r.Field<string>("FAMILY_ID"))
                       .Distinct().ToList();
                        foreach (var familyId in tableDesignerFamilyId)
                        {
                            DataSet familyProductCountDataSet = new DataSet();
                            using (var objSqlConnection = new SqlConnection(sqlConn))
                            {
                                string action = "ProductCount";
                                objSqlConnection.Open();
                                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                                objSqlCommand.CommandType = CommandType.StoredProcedure;
                                objSqlCommand.CommandTimeout = 0;
                                objSqlCommand.Connection = objSqlConnection;
                                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = "";
                                objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                                objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = familyId ?? "";
                                objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = "";
                                objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 4000).Value = "";
                                objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar, 500).Value = "";
                                objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar, -1).Value = "";
                                objSqlCommand.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action ?? "";
                                objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                                objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                                objSqlConnection.Close();
                                var familyProductCountDataAdapter = new SqlDataAdapter(objSqlCommand);
                                familyProductCountDataAdapter.Fill(familyProductCountDataSet);
                                if (familyProductCountDataSet.Tables[0].Rows.Count > 0)
                                {
                                    string productCount = familyProductCountDataSet.Tables[0].Rows[0][0].ToString();
                                    if (productCount == "0")
                                    {
                                        familyIdRemove.Add(familyId);
                                    }
                                }
                            }
                        }
                    }
                    familyIdRemove.RemoveAll(item => item == null);
                    DataTable finalImportDatatable = new DataTable();
                    var table = importDatatable.AsEnumerable()
                      .Where(r => !familyIdRemove.Contains(r.Field<string>("FAMILY_ID")));
                    if (table.Any())
                    {
                        importDatatable = table.CopyToDataTable();
                    }
                    importDatatable.Columns.Add("ImportResult");
                    foreach (DataRow row in importDatatable.Rows)
                    {
                        var distinctFamilyValues = importDatatable.AsEnumerable()
                       .Where(a => !a.Field<string>("IS_DEFAULT").IsNullOrWhiteSpace() && !a.Field<string>("STRUCTURE_NAME").IsNullOrWhiteSpace()).Select(rows => new
                       {
                           familyName = rows.Field<string>("FAMILY_NAME"),
                           structureName = rows.Field<string>("STRUCTURE_NAME"),
                           isDefault = rows.Field<string>("IS_DEFAULT").ToLower()
                       })
                        .Distinct().ToList();
                        string checkCatalogId = row["CATALOG_ID"].ToString();
                        string familyId = string.Empty;
                        string structureNamevalue = string.Empty;
                        string familyName = string.Empty;
                        string isDefault = row["IS_DEFAULT"].ToString();
                        string familyTableDesigner = row["FAMILY_TABLE_STRUCTURE"].ToString();

                        //---------------------------------------Excel sheet length validation--------------------------------------//
                        if (familyTableDesigner.Length > 32700)
                        {
                            familyTableDesigner = "";
                        }
                        //---------------------------------------Excel sheet length validation--------------------------------------//

                        Dictionary<string, string> ValueDictionaries = new Dictionary<string, string>();
                        string familyIdcolumnName = importDatatable.Columns["FAMILY_ID"].ColumnName.ToString();
                        string subFamilyIdcolumnName = importDatatable.Columns["SUBFAMILY_ID"].ColumnName.ToString();
                        string structureNamecolumnName = importDatatable.Columns["STRUCTURE_NAME"].ColumnName.ToString();
                        string familyNamecolumn = importDatatable.Columns["FAMILY_NAME"].ColumnName.ToString();
                        if (familyNamecolumn == "FAMILY_NAME")
                        {
                            string value = row["FAMILY_NAME"].ToString();
                            if (value != string.Empty)
                                ValueDictionaries.Add(familyNamecolumn, value);
                        }
                        if (familyIdcolumnName == "FAMILY_ID")
                        {
                            string value = row["FAMILY_ID"].ToString();
                            if (value != string.Empty)
                                ValueDictionaries.Add(familyIdcolumnName, value);
                        }
                        if (subFamilyIdcolumnName == "SUBFAMILY_ID")
                        {
                            string value = row["SUBFAMILY_ID"].ToString();
                            if (value != string.Empty)
                                ValueDictionaries.Add(subFamilyIdcolumnName, value);
                        }
                        if (structureNamecolumnName == "STRUCTURE_NAME")
                        {
                            string value = row["STRUCTURE_NAME"].ToString();
                            if (value != string.Empty)
                                ValueDictionaries.Add(structureNamecolumnName, value);
                        }
                        foreach (KeyValuePair<string, string> values in ValueDictionaries)
                        {
                            if (values.Key == "FAMILY_ID" || values.Key == "SUBFAMILY_ID")
                            {
                                familyId = values.Value.ToString();
                            }
                            if (values.Key == "FAMILY_NAME")
                            {
                                familyName = values.Value.ToString();
                            }
                            if (values.Key == "STRUCTURE_NAME")
                            {
                                structureNamevalue = values.Value.ToString();

                            }
                            if (familyId != string.Empty && structureNamevalue != string.Empty && familyName != string.Empty)
                            {
                                using (var objSqlConnection = new SqlConnection(sqlConn))
                                {
                                    string checkColumnname = "FAMILY_NAME_IMPORT";
                                    objSqlConnection.Open();
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.CommandTimeout = 0;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = checkCatalogId;
                                    objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 1000).Value = "";
                                    objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 1000).Value = "";
                                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = familyId ?? "";
                                    objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 1000).Value = "";
                                    objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = checkColumnname ?? "";
                                    objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 1000).Value = structureNamevalue ?? "";
                                    objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.NVarChar, 500).Value = "";
                                    objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.NVarChar, -1).Value = "";
                                    objSqlCommand.Parameters.Add("@Action", SqlDbType.NVarChar, 50).Value = "";
                                    objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10).Value = "";
                                    objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                                    using (SqlDataReader rdr = objSqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                                    {
                                        while (rdr.Read())
                                        {
                                            validateResults = rdr.GetString(rdr.GetOrdinal("Result"));
                                        }
                                        rdr.Close();
                                    }
                                }
                                if (validateResults == "TRUE")
                                {
                                    using (var objSqlConnection = new SqlConnection(sqlConn))
                                    {
                                        string action = "UPDATE";
                                        objSqlConnection.Open();
                                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                        objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                                        objSqlCommand.CommandTimeout = 0;
                                        objSqlCommand.Connection = objSqlConnection;
                                        objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = checkCatalogId;
                                        objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                                        objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                        objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = familyId ?? "";
                                        objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                        objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = "";
                                        objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 4000).Value = structureNamevalue ?? "";
                                        objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar, 500).Value = isDefault ?? "";
                                        objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar, -1).Value = familyTableDesigner ?? "";
                                        objSqlCommand.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action ?? "";
                                        objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                                        objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                                        int k = objSqlCommand.ExecuteNonQuery();
                                        if (k != 0)
                                        {

                                        }
                                        objSqlConnection.Close();
                                    }
                                    row["ImportResult"] = "Update";
                                    updatecount++;
                                }
                                if (validateResults == "FALSE")
                                {
                                    using (var objSqlConnection = new SqlConnection(sqlConn))
                                    {
                                        string action = "INSERT";
                                        objSqlConnection.Open();
                                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                        objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                                        objSqlCommand.CommandTimeout = 0;
                                        objSqlCommand.Connection = objSqlConnection;
                                        objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = checkCatalogId;
                                        objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                                        objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                        objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = familyId ?? "";
                                        objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                        objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = "";
                                        objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 4000).Value = structureNamevalue ?? "";
                                        objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar, 500).Value = isDefault ?? "";
                                        objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar, -1).Value = familyTableDesigner ?? "";
                                        objSqlCommand.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action ?? "";
                                        objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                                        objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                                        int k = objSqlCommand.ExecuteNonQuery();
                                        if (k != 0)
                                        {

                                        }
                                        objSqlConnection.Close();
                                    }
                                    row["ImportResult"] = "Insert";
                                    newInsertedcount++;
                                }
                            }
                        }
                    }
                    string emptycolumn = string.Empty;
                    foreach (DataColumn checkEmptyValues in importDatatable.Columns)
                    {
                        int count = 0;
                        foreach (DataRow dr in importDatatable.Rows)
                        {
                            string checkNullValue = dr[checkEmptyValues].ToString();
                            if (checkNullValue != "")
                            {
                                count = count + 1;
                            }
                        }

                        if (count == 0)
                        {
                            if (emptycolumn == "")
                            {
                                emptycolumn = checkEmptyValues.ToString();
                            }
                            else
                            {
                                emptycolumn = emptycolumn + "~" + (checkEmptyValues.ToString());
                            }

                        }
                    }
                    string[] emptyColumnValues = emptycolumn.Split('~');
                    for (int i = 0; i < emptyColumnValues.Length; i++)
                    {
                        if (emptyColumnValues[i] != "")
                        {
                            importDatatable.Columns.Remove(emptyColumnValues[i]);
                        }
                    }
                    importDatatable.AcceptChanges();
                    System.Web.HttpContext.Current.Session["importDatatable"] = importDatatable;
                    System.Web.HttpContext.Current.Session["downloadLogFamilyPage"] = importDatatable;
                    importResult = importResult + "~UpdateRecords:" + updatecount + "~InsertRecords:" + newInsertedcount;
                    stopWatch.Stop();
                    TimeSpan ts = stopWatch.Elapsed;
                    importResult = importResult + "~" + "TimeElapsed-" + ts.ToString().Split('.')[0];
                }
                return importResult;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : FinishImportFamilyLevelTableDesigner", ex);
                return null;
            }
        }

        //public string CreateTable(string tableName, DataTable objtable)
        //{
        //    try
        //    {
        //        string sqlsc = "CREATE TABLE " + tableName + "(";
        //        for (int i = 0; i < objtable.Columns.Count; i++)
        //        {
        //            if (!objtable.Columns[i].ColumnName.Contains('['))
        //            {
        //                sqlsc += "\n [" + objtable.Columns[i].ColumnName + "] ";
        //            }
        //            else
        //            {
        //                sqlsc += "\n" + objtable.Columns[i].ColumnName + "";
        //            }
        //            if (objtable.Columns[i].ColumnName.Contains("CATEGORY_ID"))
        //                sqlsc += "nvarchar(50) ";
        //            else if (objtable.Columns[i].DataType.ToString().Contains("System.String") ||
        //                     objtable.Columns[i].ColumnName.Contains("SUBCATID") ||
        //                     objtable.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
        //                sqlsc += "varchar(8000) ";
        //            else
        //                sqlsc += "varchar(8000) ";
        //            sqlsc += ",";
        //        }
        //        return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at CreateTable", ex);
        //        throw;
        //    }
        //}

        [System.Web.Http.HttpPost]
        public DataTable GetCurrentCatalogGroupNames(int catalogId, string importType, int templateId, DataSourceRequest request)
        {
            try
            {

                DataTable dt_GroupNames = new DataTable();

                using (var objSqlConnection = new SqlConnection(connectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand get_GroupNames_DetailsSqlcommand = new SqlCommand();

                    get_GroupNames_DetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    get_GroupNames_DetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    get_GroupNames_DetailsSqlcommand.Connection = objSqlConnection;
                    get_GroupNames_DetailsSqlcommand.CommandTimeout = 0;
                    get_GroupNames_DetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "GETGROUPNAMES";
                    get_GroupNames_DetailsSqlcommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    get_GroupNames_DetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importType;
                    get_GroupNames_DetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    SqlDataAdapter get_Attributenames_DetailsSqlDataAdapter = new SqlDataAdapter(get_GroupNames_DetailsSqlcommand);
                    get_Attributenames_DetailsSqlDataAdapter.Fill(dt_GroupNames);

                    //  return new JsonResult() { Data = dt_AttributeNames };

                    return dt_GroupNames;
                }
                //  HomeApiController homeObj = new HomeApiController();
                //var lt_GroupNamesForTemplateName = (from attrHeirarchy in _dbcontext.TB_ATTRIBUTE_HIERARCHY
                //                                join attributePack in _dbcontext.TB_PACKAGE_MASTER on attrHeirarchy.PACK_ID equals attributePack.GROUP_ID
                //                                join packageDetails in  _dbcontext.TB_PACKAGE_DETAILS on attributePack.GROUP_ID equals packageDetails.GROUP_ID  
                //                                join mappingDetails in _dbcontext.TB_TEMPLATE_MAPPING_DETAILS_IMPORT on packageDetails.ATTRIBUTE_ID equals mappingDetails.ATTRIBUTE_ID
                //                             //   join templateAttributeGroup in _dbcontext.TB_IMPORT_TEMPLATE_ATTRIBUTE_GROUP on packageDetails.GROUP_ID equals templateAttributeGroup.GROUP_ID
                //                                where attributePack.CATALOG_ID == catalogId && attributePack.IS_FAMILY.ToLower() == importType && attributePack.FLAG_RECYCLE == "A"
                //                                && mappingDetails.TEMPLATE_ID== templateId
                //                                select new
                //                                {

                //                                    attributePack.GROUP_ID,
                //                                    attributePack.GROUP_NAME,
                //                                    ISAvailable = true,

                //                                }).Distinct().OrderBy(x=>x.GROUP_NAME).ToList();

                //var templateGroupNames = _dbcontext.TB_IMPORT_TEMPLATE_ATTRIBUTE_GROUP.Join(_dbcontext.TB_PACKAGE_MASTER, TAG => TAG.GROUP_ID,
                // TPM=> TPM.GROUP_ID, (TAG, TPM) => new { TAG, TPM }).Join
                //    (_dbcontext.TB_PACKAGE_DETAILS, TPD => TPD.TAG.GROUP_ID, TPDD => TPDD.GROUP_ID, (TPD, TPDD) => new { TPD, TPDD })
                //   .Where(X => X.TPD.TAG.TEMPLATE_ID == templateId ).Select(x => new { x.TPD.TAG.GROUP_ID }).Distinct().ToList();

                //List<AttributePack> attributePackList = new List<AttributePack>();

                //foreach (var groupId in lt_GroupNamesForTemplateName)
                //{
                //    bool isGroupNameExists = true;
                //    isGroupNameExists = templateGroupNames.Any(x => x.GROUP_ID == groupId.GROUP_ID);
                //    AttributePack attributePack = new AttributePack();
                //    attributePack.CATALOG_ID = catalogId;
                //    attributePack.GROUP_ID = groupId.GROUP_ID;
                //    attributePack.GROUP_NAME = groupId.GROUP_NAME;
                //    attributePack.ISAvailable = true;
                //    if (isGroupNameExists==false)
                //    {
                //        attributePack.ISAvailable = false;
                //    }
                //    attributePackList.Add(attributePack);

                //}



                //var allProductAttributePacks = _dbcontext.TB_PACKAGE_MASTER.Where(s => s.CATALOG_ID == catalogId && s.IS_FAMILY.ToLower() == importType && s.FLAG_RECYCLE == "A").Select(z => new
                //{
                //    GROUP_ID = z.GROUP_ID,
                //    GROUP_NAME = z.GROUP_NAME,
                //    ISAvailable = false
                //}).Distinct().ToList();

                //List<AttributePack> attributePackList = new List<AttributePack>();

                //foreach (var allProductAttributePack in allProductAttributePacks)
                //{
                //    bool isContain = false;
                //    isContain = cat_ProductAttributePack.Any(s => s.GROUP_ID == allProductAttributePack.GROUP_ID);
                //    AttributePack attributePack = new AttributePack();
                //    attributePack.GROUP_ID = allProductAttributePack.GROUP_ID;
                //    attributePack.GROUP_NAME = allProductAttributePack.GROUP_NAME;
                //    if (isContain)
                //        attributePack.ISAvailable = true;
                //    else
                //        attributePack.ISAvailable = false;
                //    attributePackList.Add(attributePack);
                //}

                //return attributePackList.AsQueryable().ToDataSourceResult(request);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CategoryApiController : GetProductPacksforCategory", ex);
                return null;
            }
        }


        [System.Web.Http.HttpGet]
        public DataTable GetAllGroupAttributesByGroupId(int catalogId, int groupId, string importType, int templateId, bool isAvailable)
        {
            try
            {

                DataTable dt_AttributeNames = new DataTable();

                using (var objSqlConnection = new SqlConnection(connectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand get_Attributenames_DetailsSqlcommand = new SqlCommand();

                    get_Attributenames_DetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    get_Attributenames_DetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    get_Attributenames_DetailsSqlcommand.Connection = objSqlConnection;
                    get_Attributenames_DetailsSqlcommand.CommandTimeout = 0;
                    get_Attributenames_DetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "GETATRIBUTENAMES";
                    get_Attributenames_DetailsSqlcommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    get_Attributenames_DetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importType;
                    get_Attributenames_DetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    get_Attributenames_DetailsSqlcommand.Parameters.Add("@GROUP_ID", SqlDbType.Int).Value = groupId;
                    SqlDataAdapter get_Attributenames_DetailsSqlDataAdapter = new SqlDataAdapter(get_Attributenames_DetailsSqlcommand);
                    get_Attributenames_DetailsSqlDataAdapter.Fill(dt_AttributeNames);

                    //  return new JsonResult() { Data = dt_AttributeNames };

                    return dt_AttributeNames;
                }

                //var allAttributeNames = (from catalogAttributes in _dbcontext.TB_CATALOG_ATTRIBUTES
                //                         join attributes in _dbcontext.TB_ATTRIBUTE on catalogAttributes.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                //                         where catalogAttributes.CATALOG_ID == catalogId && attributes.FLAG_RECYCLE == "A" && attributes.ATTRIBUTE_NAME != "ITEM#"
                //                         select new
                //                         {
                //                             catalogAttributes.CATALOG_ID,
                //                             attributes.ATTRIBUTE_ID,
                //                             attributes.ATTRIBUTE_NAME,
                //                             attributes.ATTRIBUTE_TYPE,
                //                             ISAvailable = false,
                //                         }).Distinct().ToList();


                //if (isAvailable == true)
                //{



                //    var attributeGroup = (from packageDetails in _dbcontext.TB_PACKAGE_DETAILS
                //                          join master in _dbcontext.TB_PACKAGE_MASTER on packageDetails.GROUP_ID equals master.GROUP_ID
                //                          join catalogAttributes in _dbcontext.TB_CATALOG_ATTRIBUTES on packageDetails.ATTRIBUTE_ID equals catalogAttributes.ATTRIBUTE_ID
                //                          join attributes in _dbcontext.TB_ATTRIBUTE on catalogAttributes.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                //                          join templateAttributeGroup in _dbcontext.TB_IMPORT_TEMPLATE_ATTRIBUTE_GROUP on packageDetails.GROUP_ID equals templateAttributeGroup.GROUP_ID
                //                          where master.CATALOG_ID == catalogId && attributes.FLAG_RECYCLE == "A" && packageDetails.GROUP_ID == groupId && master.IS_FAMILY == importType
                //                          select new
                //                          {
                //                              master.CATALOG_ID,
                //                              attributes.ATTRIBUTE_ID,
                //                              attributes.ATTRIBUTE_NAME,
                //                              attributes.ATTRIBUTE_TYPE,
                //                              ISAvailable = true,
                //                              attributes.CAPTION,

                //                          }).Distinct().ToList();


                //    var templateGroupAttributes = _dbcontext.TB_IMPORT_TEMPLATE_ATTRIBUTE_GROUP_DETAILS.Join(_dbcontext.TB_IMPORT_TEMPLATE_ATTRIBUTE_GROUP, TAGD => TAGD.GROUP_REFERENCE_ID,
                // TAG => TAG.GROUP_REFERENCE_ID, (TAGD, TAG) => new { TAGD, TAG }).Join
                //    (_dbcontext.TB_PACKAGE_DETAILS, TPD => TPD.TAGD.ATTRIBUTE_ID, TAGD => TAGD.ATTRIBUTE_ID, (TPD, TAGD) => new { TPD, TAGD })
                //   .Where(X => X.TPD.TAG.TEMPLATE_ID == templateId && X.TPD.TAG.GROUP_ID == groupId).Select(x => new { x.TAGD.ATTRIBUTE_ID }).Distinct().ToList();

                //    List<AttributePublish> attributePublishList = new List<AttributePublish>();


                //    foreach (var attributes in attributeGroup)
                //    {

                //        bool isAttributeNameExists = true;
                //        isAttributeNameExists = templateGroupAttributes.Any(x => x.ATTRIBUTE_ID == attributes.ATTRIBUTE_ID);
                //        AttributePublish attributePublish = new AttributePublish();
                //        attributePublish.CATALOG_ID = Convert.ToInt32(attributes.CATALOG_ID);
                //        attributePublish.ATTRIBUTE_ID = attributes.ATTRIBUTE_ID;
                //        attributePublish.ATTRIBUTE_NAME = attributes.ATTRIBUTE_NAME;
                //        attributePublish.ATTRIBUTE_TYPE = attributes.ATTRIBUTE_TYPE;
                //        attributePublish.CAPTION = attributes.CAPTION;
                //        attributePublish.ISAvailable = attributes.ISAvailable;
                //        if (isAttributeNameExists == false)
                //        {
                //            attributePublish.ISAvailable = false;
                //        }
                //        attributePublishList.Add(attributePublish);

                //    }

                //    return attributePublishList.OrderBy(y => y.ATTRIBUTE_NAME).ToList();
                //}

                //else
                //{

                //    var attributeGroup = (from packageDetails in _dbcontext.TB_PACKAGE_DETAILS
                //                          join master in _dbcontext.TB_PACKAGE_MASTER on packageDetails.GROUP_ID equals master.GROUP_ID
                //                          join catalogAttributes in _dbcontext.TB_CATALOG_ATTRIBUTES on packageDetails.ATTRIBUTE_ID equals catalogAttributes.ATTRIBUTE_ID
                //                          join attributes in _dbcontext.TB_ATTRIBUTE on catalogAttributes.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                //                          where master.CATALOG_ID == catalogId && attributes.FLAG_RECYCLE == "A" && packageDetails.GROUP_ID == groupId && master.IS_FAMILY == importType
                //                          select new
                //                          {
                //                              master.CATALOG_ID,
                //                              attributes.ATTRIBUTE_ID,
                //                              attributes.ATTRIBUTE_NAME,
                //                              attributes.ATTRIBUTE_TYPE,
                //                              ISAvailable = false,
                //                              attributes.CAPTION,
                //                          }).Distinct().ToList();

                //    return attributeGroup.OrderBy(y => y.ATTRIBUTE_NAME).ToList();

                //}



                //var mappingAttributes = _dbcontext.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(_dbcontext.TB_TEMPLATE_SHEET_DETAILS_IMPORT, TMD => TMD.IMPORT_SHEET_ID, TSD => TSD.IMPORT_SHEET_ID, (TMD, TSD) => new { TMD, TSD }).Join
                //    (_dbcontext.TB_TEMPLATE_DETAILS_IMPORT, TDI => TDI.TSD.TEMPLATE_ID, TSD => TSD.TEMPLATE_ID, (TDI, TSD) => new { TDI, TSD })
                //    .Where(X => X.TDI.TSD.TEMPLATE_ID == templateId && X.TDI.TMD.ATTRIBUTE_ID>0).Select(x => new { x.TDI.TMD.ATTRIBUTE_ID }).Distinct().ToList();




                //foreach (var attributes in attributeGroup)
                //{
                //    bool isAvaliable = false;
                //    isAvaliable = mappingAttributes.Any(x => x.ATTRIBUTE_ID == attributes.ATTRIBUTE_ID);

                //    if (isAvaliable)
                //    {
                //        attributes.ISAvailable = true;
                //    }


                //}


                //     List<AttributePublish> attributePublishList = new List<AttributePublish>();

                //if (groupId != 0)
                //{
                //foreach (var allAttributeName in attributeGroup)
                //{
                //    bool isContain = false;
                //    isContain = attributeGroup.Any(s => s.ATTRIBUTE_ID == allAttributeName.ATTRIBUTE_ID);
                //    if (!isContain)
                //    {

                //        attributePublish.CATALOG_ID = allAttributeName.CATALOG_ID;
                //        attributePublish.ATTRIBUTE_ID = allAttributeName.ATTRIBUTE_ID;
                //        attributePublish.ATTRIBUTE_NAME = allAttributeName.ATTRIBUTE_NAME;
                //        attributePublish.ATTRIBUTE_TYPE = allAttributeName.ATTRIBUTE_TYPE;
                //        attributePublish.ISAvailable = false;
                //        attributePublishList.Add(attributePublish);
                //    }

                //}

                //if (importType == "Family")
                //{
                //    int[] attributetypes = new int[] { 7, 9, 11, 12, 13 };
                //    List<int> attributeTypes = new List<int>();
                //    attributeTypes = attributetypes.ToList();
                //    attributePublishList = attributePublishList.Where(s => attributeTypes.Contains(s.ATTRIBUTE_TYPE)).ToList();
                //}
                //else if (importType == "Product")
                //{
                //    int[] attributetypes = new int[] { 1, 3, 4, 6 };
                //    List<int> attributeTypes = new List<int>();
                //    attributeTypes = attributetypes.ToList();
                //    attributePublishList = attributePublishList.Where(s => attributeTypes.Contains(s.ATTRIBUTE_TYPE)).ToList();
                //}


                //}
                //else
                //    return attributePublishList = new List<AttributePublish>();
            }
            catch (Exception objException)
            {
                _logger.Error("Error at HomeApiController : GetAllAttributesForGrouping", objException);
                return null;
            }
        }


        [System.Web.Http.HttpPost]
        public string SaveGroupNamesByTemplateId(int catalogId, string importType, int templateId, JArray model)
        {
            string str_Result = string.Empty;

            DataTable dt_GroupName = new DataTable();

            try
            {
                dt_GroupName = (DataTable)JsonConvert.DeserializeObject(model.ToString(), (typeof(DataTable)));

                dt_GroupName.Columns.Add("RowNum", typeof(Int32));

                dt_GroupName.Columns["RowNum"].AutoIncrement = true;

                //Set the Starting or Seed value.
                dt_GroupName.Columns["RowNum"].AutoIncrementSeed = 1;

                //Set the Increment value.
                dt_GroupName.Columns["RowNum"].AutoIncrementStep = 1;

                int rowindex = 0;

                foreach (DataRow row in dt_GroupName.Rows)
                    row["RowNum"] = ++rowindex;

                string sessionId = Guid.NewGuid().ToString();

                using (var objSqlConnection = new SqlConnection(connectionString))
                {

                    objSqlConnection.Open();
                    //string sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBCATATTRTEMP" + sessionId + "'')BEGIN DROP TABLE [##SUBCATATTRTEMP" + sessionId + "] END')";
                    string sqlString = "EXEC('IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME like ''##TEMP_GROUP_NAME" + sessionId + "'')BEGIN DROP TABLE [##TEMP_GROUP_NAME" + sessionId + "] END')";
                    SqlCommand insert_GroupNameDetailsSqlcommand = new SqlCommand(sqlString, objSqlConnection);
                    insert_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    sqlString = CreateTable("[##TEMP_GROUP_NAME" + sessionId + "]", dt_GroupName);

                    SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                    dbCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##TEMP_GROUP_NAME" + sessionId + "]"
                    };
                    bulkCopy.WriteToServer(dt_GroupName);


                    insert_GroupNameDetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    insert_GroupNameDetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    insert_GroupNameDetailsSqlcommand.Connection = objSqlConnection;
                    insert_GroupNameDetailsSqlcommand.CommandTimeout = 0;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "INSERT";
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importType;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@Session_Id", SqlDbType.NVarChar).Value = sessionId;

                    insert_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    string str_DropTempTable = "EXEC('IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME like ''##TEMP_GROUP_NAME" + sessionId + "'')BEGIN DROP TABLE [##TEMP_GROUP_NAME" + sessionId + "] END')";
                    SqlCommand drop_GroupNameDetailsSqlcommand = new SqlCommand(str_DropTempTable, objSqlConnection);
                    drop_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    str_Result = "Group details inserted successfully";

                    return str_Result;

                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at AdvanceImportApiController : SaveGroupNamesByTemplateId", objException);
                return null;
            }


        }

        [System.Web.Http.HttpPost]
        public string DeleteGroupNamesByTemplateId(int catalogId, string importType, int templateId, JArray model)
        {
            string str_Result = string.Empty;

            DataTable dt_DeleteGroupName = new DataTable();

            try
            {

                string sessionId = Guid.NewGuid().ToString();

                dt_DeleteGroupName = (DataTable)JsonConvert.DeserializeObject(model.ToString(), (typeof(DataTable)));

                using (var objSqlConnection = new SqlConnection(connectionString))
                {
                    objSqlConnection.Open();
                    //string sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBCATATTRTEMP" + sessionId + "'')BEGIN DROP TABLE [##SUBCATATTRTEMP" + sessionId + "] END')";
                    string sqlString = "EXEC('IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME like ''##TEMP_GROUP_NAME_DELETE" + sessionId + "'')BEGIN DROP TABLE [##TEMP_GROUP_NAME_DELETE" + sessionId + "] END')";

                    SqlCommand insert_GroupNameDetailsSqlcommand = new SqlCommand(sqlString, objSqlConnection);
                    insert_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    sqlString = CreateTable("[##TEMP_GROUP_NAME_DELETE" + sessionId + "]", dt_DeleteGroupName);

                    SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                    dbCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##TEMP_GROUP_NAME_DELETE" + sessionId + "]"
                    };
                    bulkCopy.WriteToServer(dt_DeleteGroupName);

                    insert_GroupNameDetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    insert_GroupNameDetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    insert_GroupNameDetailsSqlcommand.Connection = objSqlConnection;
                    insert_GroupNameDetailsSqlcommand.CommandTimeout = 0;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "DELETEGROUP";
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importType;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@Session_Id", SqlDbType.NVarChar).Value = sessionId;
                    insert_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    string str_DropTempTable = "EXEC('IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME like ''##TEMP_GROUP_NAME_DELETE" + sessionId + "'')BEGIN DROP TABLE [##TEMP_GROUP_NAME_DELETE" + sessionId + "] END')";
                    SqlCommand drop_GroupNameDetailsSqlcommand = new SqlCommand(str_DropTempTable, objSqlConnection);
                    drop_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    str_Result = "Group details deleted successfully";

                    return str_Result;
                }
            }

            catch (Exception objException)
            {
                _logger.Error("Error at AdvanceImportApiController : DeleteGroupNamesByTemplateId", objException);
                return null;
            }


        }

        [System.Web.Http.HttpPost]
        public string UpdateAttributeNamesByTemplateId(int catalogId, string importType, int templateId, int groupId, bool isAvailable, int attributeId)
        {
            string str_Result = string.Empty;

            if (isAvailable == false)
            {
                using (var objSqlConnection = new SqlConnection(connectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand insert_GroupNameDetailsSqlcommand = new SqlCommand();

                    insert_GroupNameDetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    insert_GroupNameDetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    insert_GroupNameDetailsSqlcommand.Connection = objSqlConnection;
                    insert_GroupNameDetailsSqlcommand.CommandTimeout = 0;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "DELETEATTRIBUTE";
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importType;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@GROUP_ID", SqlDbType.Int).Value = groupId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.Int).Value = attributeId;
                    insert_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    str_Result = "Attribute Name details deleted successfully";

                }
            }

            else
            {

                using (var objSqlConnection = new SqlConnection(connectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand insert_GroupNameDetailsSqlcommand = new SqlCommand();

                    insert_GroupNameDetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    insert_GroupNameDetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    insert_GroupNameDetailsSqlcommand.Connection = objSqlConnection;
                    insert_GroupNameDetailsSqlcommand.CommandTimeout = 0;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "INSERTATTRIBUTE";
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importType;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@GROUP_ID", SqlDbType.Int).Value = groupId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.Int).Value = attributeId;
                    insert_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    str_Result = "Attribute Name details inserted successfully";

                }
            }


            return str_Result;
        }

        [System.Web.Http.HttpPost]
        public string SaveAttributeNamesByTemplateId(int templateId, int groupId, JArray model)
        {
            string str_Result = string.Empty;

            DataTable dt_GroupName = new DataTable();

            try
            {
                dt_GroupName = (DataTable)JsonConvert.DeserializeObject(model.ToString(), (typeof(DataTable)));

                string sessionId = Guid.NewGuid().ToString();

                using (var objSqlConnection = new SqlConnection(connectionString))
                {

                    objSqlConnection.Open();
                    //string sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBCATATTRTEMP" + sessionId + "'')BEGIN DROP TABLE [##SUBCATATTRTEMP" + sessionId + "] END')";
                    string sqlString = "EXEC('IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME like ''##TEMP_ATTRIBUTE_NAME" + sessionId + "'')BEGIN DROP TABLE [##TEMP_ATTRIBUTE_NAME" + sessionId + "] END')";
                    SqlCommand insert_GroupNameDetailsSqlcommand = new SqlCommand(sqlString, objSqlConnection);
                    insert_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    sqlString = CreateTable("[##TEMP_ATTRIBUTE_NAME" + sessionId + "]", dt_GroupName);

                    SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                    dbCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##TEMP_ATTRIBUTE_NAME" + sessionId + "]"
                    };
                    bulkCopy.WriteToServer(dt_GroupName);


                    insert_GroupNameDetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    insert_GroupNameDetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    insert_GroupNameDetailsSqlcommand.Connection = objSqlConnection;
                    insert_GroupNameDetailsSqlcommand.CommandTimeout = 0;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "INSERTGROUPATTRIBUTE";
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@GROUP_ID", SqlDbType.Int).Value = groupId;
                    insert_GroupNameDetailsSqlcommand.Parameters.Add("@Session_Id", SqlDbType.NVarChar).Value = sessionId;

                    insert_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    string str_DropTempTable = "EXEC('IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME like ''##TEMP_ATTRIBUTE_NAME" + sessionId + "'')BEGIN DROP TABLE [##TEMP_ATTRIBUTE_NAME" + sessionId + "] END')";
                    SqlCommand drop_GroupNameDetailsSqlcommand = new SqlCommand(str_DropTempTable, objSqlConnection);
                    drop_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    str_Result = "Attribute details inserted successfully";

                    return str_Result;

                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at AdvanceImportApiController : SaveAttributeNamesByTemplateId", objException);
                return null;
            }


        }

        [System.Web.Http.HttpPost]
        public string DeleteAttributeNamesByTemplateId(int templateId, JArray model)
        {
            string str_Result = string.Empty;

            DataTable dt_Delete_AttributeGroupName = new DataTable();

            try
            {
                dt_Delete_AttributeGroupName = (DataTable)JsonConvert.DeserializeObject(model.ToString(), (typeof(DataTable)));

                string sessionId = Guid.NewGuid().ToString();

                using (var objSqlConnection = new SqlConnection(connectionString))
                {

                    objSqlConnection.Open();
                    //string sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##SUBCATATTRTEMP" + sessionId + "'')BEGIN DROP TABLE [##SUBCATATTRTEMP" + sessionId + "] END')";
                    string sqlString = "EXEC('IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME like ''##TEMP_ATTRIBUTE_NAME_DELETE" + sessionId + "'')BEGIN DROP TABLE [##TEMP_ATTRIBUTE_NAME_DELETE" + sessionId + "] END')";
                    SqlCommand delete_AttributeGroupNameDetailsSqlcommand = new SqlCommand(sqlString, objSqlConnection);
                    delete_AttributeGroupNameDetailsSqlcommand.ExecuteNonQuery();

                    sqlString = CreateTable("[##TEMP_ATTRIBUTE_NAME_DELETE" + sessionId + "]", dt_Delete_AttributeGroupName);

                    SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                    dbCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##TEMP_ATTRIBUTE_NAME_DELETE" + sessionId + "]"
                    };
                    bulkCopy.WriteToServer(dt_Delete_AttributeGroupName);

                    delete_AttributeGroupNameDetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    delete_AttributeGroupNameDetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    delete_AttributeGroupNameDetailsSqlcommand.Connection = objSqlConnection;
                    delete_AttributeGroupNameDetailsSqlcommand.CommandTimeout = 0;
                    delete_AttributeGroupNameDetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "DELETEGROUPATTRIBUTE";
                    delete_AttributeGroupNameDetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    delete_AttributeGroupNameDetailsSqlcommand.Parameters.Add("@Session_Id", SqlDbType.NVarChar).Value = sessionId;

                    delete_AttributeGroupNameDetailsSqlcommand.ExecuteNonQuery();

                    string str_DropTempTable = "EXEC('IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME like ''##TEMP_ATTRIBUTE_NAME_DELETE" + sessionId + "'')BEGIN DROP TABLE [##TEMP_ATTRIBUTE_NAME_DELETE" + sessionId + "] END')";
                    SqlCommand drop_GroupNameDetailsSqlcommand = new SqlCommand(str_DropTempTable, objSqlConnection);
                    drop_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    str_Result = "Attribute details deleted successfully";

                    return str_Result;

                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at AdvanceImportApiController : DeleteAttributeNamesByTemplateId", objException);
                return null;
            }


        }

        [System.Web.Http.HttpPost]
        public string AttributeSortOrderMoveUp(int attributeId, int groupId, int templateId)
        {
            string str_Result = string.Empty;

            try
            {
                using (var objSqlConnection = new SqlConnection(connectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand sortOrder_AttributeNameDetailsSqlcommand = new SqlCommand();

                    sortOrder_AttributeNameDetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    sortOrder_AttributeNameDetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    sortOrder_AttributeNameDetailsSqlcommand.Connection = objSqlConnection;
                    sortOrder_AttributeNameDetailsSqlcommand.CommandTimeout = 0;
                    sortOrder_AttributeNameDetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "ATTRIBUTESORT_MOVEUP";
                    sortOrder_AttributeNameDetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    sortOrder_AttributeNameDetailsSqlcommand.Parameters.Add("@GROUP_ID", SqlDbType.Int).Value = groupId;
                    sortOrder_AttributeNameDetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.Int).Value = attributeId;
                    sortOrder_AttributeNameDetailsSqlcommand.ExecuteNonQuery();

                    str_Result = "Attribute sort order updated successfully";

                }

                return str_Result;
            }

            catch (Exception ex)
            {
                _logger.Error("Error in AdvanceimportApicontroller:AttributeSortOrderMoveUp", ex);
                return null;
            }


        }


        [System.Web.Http.HttpPost]
        public string AttributeNameMoveDownClick(int attributeId, int groupId, int templateId)
        {
            string str_Result = string.Empty;

            try
            {


                using (var objSqlConnection = new SqlConnection(connectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand sortOrder_AttributeNameDetailsSqlcommand = new SqlCommand();

                    sortOrder_AttributeNameDetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    sortOrder_AttributeNameDetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    sortOrder_AttributeNameDetailsSqlcommand.Connection = objSqlConnection;
                    sortOrder_AttributeNameDetailsSqlcommand.CommandTimeout = 0;
                    sortOrder_AttributeNameDetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "ATTRIBUTESORT_MOVEDOWN";
                    sortOrder_AttributeNameDetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    sortOrder_AttributeNameDetailsSqlcommand.Parameters.Add("@GROUP_ID", SqlDbType.Int).Value = groupId;
                    sortOrder_AttributeNameDetailsSqlcommand.Parameters.Add("@ATTRIBUTE_ID", SqlDbType.Int).Value = attributeId;
                    sortOrder_AttributeNameDetailsSqlcommand.ExecuteNonQuery();

                    str_Result = "Attribute sort order updated successfully";
                }

                return str_Result;
            }

            catch (Exception ex)
            {
                _logger.Error("Error in AdvanceimportApicontroller:AttributeNameMoveDownClick", ex);
                return null;
            }


        }

        [System.Web.Http.HttpPost]
        public string GroupNameMoveUp(int templateId, int groupId, string importType, int catalogId)
        {
            string str_Result = string.Empty;

            try
            {

                using (var objSqlConnection = new SqlConnection(connectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand sortOrder_GroupNameDetailsSqlcommand = new SqlCommand();

                    sortOrder_GroupNameDetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    sortOrder_GroupNameDetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    sortOrder_GroupNameDetailsSqlcommand.Connection = objSqlConnection;
                    sortOrder_GroupNameDetailsSqlcommand.CommandTimeout = 0;
                    sortOrder_GroupNameDetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "GROUPSORT_MOVEUP";
                    sortOrder_GroupNameDetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    sortOrder_GroupNameDetailsSqlcommand.Parameters.Add("@GROUP_ID", SqlDbType.Int).Value = groupId;
                    sortOrder_GroupNameDetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importType;
                    sortOrder_GroupNameDetailsSqlcommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    sortOrder_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    str_Result = "Group Name sort order updated successfully";

                }
                return str_Result;
            }

            catch (Exception ex)
            {
                _logger.Error("Error in AdvanceimportApicontroller:GroupNameMoveUp", ex);

                return null;
            }

        }


        [System.Web.Http.HttpPost]
        public string GroupNameMoveDown(int templateId, int groupId, string importType, int catalogId)
        {
            string str_Result = string.Empty;

            try
            {
                using (var objSqlConnection = new SqlConnection(connectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand sortOrder_GroupNameDetailsSqlcommand = new SqlCommand();

                    sortOrder_GroupNameDetailsSqlcommand.CommandText = "STP_CATALOGSTUDIO_IMPORT_GROUP_ATTRIBUTE_DETAILS";
                    sortOrder_GroupNameDetailsSqlcommand.CommandType = CommandType.StoredProcedure;
                    sortOrder_GroupNameDetailsSqlcommand.Connection = objSqlConnection;
                    sortOrder_GroupNameDetailsSqlcommand.CommandTimeout = 0;
                    sortOrder_GroupNameDetailsSqlcommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "GROUPSORT_MOVEDOWN";
                    sortOrder_GroupNameDetailsSqlcommand.Parameters.Add("@TEMPLATE_ID", SqlDbType.NVarChar).Value = templateId;
                    sortOrder_GroupNameDetailsSqlcommand.Parameters.Add("@GROUP_ID", SqlDbType.Int).Value = groupId;
                    sortOrder_GroupNameDetailsSqlcommand.Parameters.Add("@IMPORT_TYPE", SqlDbType.NVarChar).Value = importType;
                    sortOrder_GroupNameDetailsSqlcommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    sortOrder_GroupNameDetailsSqlcommand.ExecuteNonQuery();

                    str_Result = "Group Name sort order updated successfully";

                }
                return str_Result;

            }

            catch (Exception ex)
            {
                _logger.Error("Error in AdvanceimportApicontroller:GroupNameMoveDown", ex);
                return null;
            }


        }

        /// <summary>
        /// Added by Aswin kumar.M
        /// </summary>
        /// <param name="templateNameImport"></param>
        /// <param name="catalogId"></param>
        /// <param name="importFormat"></param>
        /// <param name="filePath"></param>
        /// <param name="templateId"></param>
        /// <returns>It returns template id and template already exists or not</returns>
        [System.Web.Http.HttpPost]
        public string UpdateTemplateDetails(string templateNameImport, int catalogId, string importFormat, string filePath, int templateId)
        {
            try
            {

                string str_GetFilePath = string.Empty;
                string str_FileName = string.Empty;
                var customerId = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var obj_InsertTemplateDetails = new TB_TEMPLATE_DETAILS_IMPORT();
                bool templateNameExists = false;

                var templateExists = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.TEMPLATE_NAME == templateNameImport && x.CUSTOMER_ID == customerId).FirstOrDefault();
                if (templateExists == null)
                {
                    /////---------------------------------Import template update file name ----------------------------------- 
                    string str_ImportFileName = Path.GetFileNameWithoutExtension(filePath);
                    str_ImportFileName = "ImportTemplate~" + str_ImportFileName;
                    str_GetFilePath = "ImportTemplate~" + Path.GetFileName(filePath);

                    obj_InsertTemplateDetails = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.FILE_NAME == str_GetFilePath && x.TEMPLATE_NAME == str_ImportFileName && x.CUSTOMER_ID == customerId).FirstOrDefault();


                    string str_OldDestinationPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate"));
                    string str_OldDestinationFile = Path.Combine(str_OldDestinationPath, str_GetFilePath);



                    if (File.Exists(str_OldDestinationFile))
                    {
                        str_FileName = templateNameImport + '.' + importFormat;
                        string str_DestinationPath = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ImportTemplate"));
                        string str_DestinationFile = Path.Combine(str_DestinationPath, str_FileName);
                        // File.Move(sourceFile, destinationFile);
                        System.IO.File.Copy(filePath, str_DestinationFile);
                        File.Delete(str_OldDestinationFile);
                    }

                    /////---------------------------------Import template update file name end----------------------------------- 
                    obj_InsertTemplateDetails.CATALOG_ID = catalogId;
                    obj_InsertTemplateDetails.CUSTOMER_ID = customerId;
                    obj_InsertTemplateDetails.EXCEL_FORMAT = importFormat;
                    obj_InsertTemplateDetails.TEMPLATE_NAME = templateNameImport;
                    obj_InsertTemplateDetails.CREATED_USER = User.Identity.Name;
                    obj_InsertTemplateDetails.CREATED_DATE = DateTime.Now;
                    obj_InsertTemplateDetails.FILE_NAME = str_FileName;
                    obj_InsertTemplateDetails.LAST_MODIFIED_DATE = DateTime.Now;

                    objLS.SaveChanges();

                    templateId = obj_InsertTemplateDetails.TEMPLATE_ID;
                }
                else
                {
                    //templateExists.CATALOG_ID = catalogId;
                    //templateExists.CUSTOMER_ID = customerId;
                    //templateExists.EXCEL_FORMAT = importFormat;
                    //templateExists.TEMPLATE_NAME = templateNameImport;
                    //templateExists.CREATED_USER = User.Identity.Name;
                    //templateExists.CREATED_DATE = DateTime.Now;
                    templateNameExists = true;
                }

                return templateId + "_" + templateNameExists;

            }
            catch (Exception objexception)
            {
                _logger.Error("Error at AdvanceImportApiController : UpdateTemplateDetails", objexception);
                return null;
            }
        }
        #region Restore Catalog

        public string FinishRestore(string allowDuplicate, int catalogId, string importType, DataTable dt, JArray model)
        {
            ProgressBar progressBar = new ProgressBar();
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            List<object> newList = new List<object>();
            DataSet dsGetProdCnt = new DataSet();
            int importCount = 0;
            importController.ImportStatus("Called");
            int importProductCount = 0;
            int userProductCount = 0;
            int userSKUProductCount = 0;
            QueryValues queryValues = new QueryValues();
            // CustomerItemNo
            if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
            {
                var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                int customerIds;
                int.TryParse(customerDetails.ToString(), out customerIds);
                string customerCatalog = queryValues.GetCatalogItemNo(customerIds);
                System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerCatalog;
            }
            // CustomerSubItemNo
            if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
            {
                var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                int customerId;
                int.TryParse(customerDetails.ToString(), out customerId);
                string customerCatalog = queryValues.GetSubCatalogItemNo(customerId);
                System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerCatalog;
            }
            try
            {
                var customerId = 0;
                using (objLS = new CSEntities())
                {
                    var skucnt = objLS.TB_PLAN
                       .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                       .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                       .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                    var SKUcount = skucnt.Select(a => a).ToList();
                    //Item# import Process Start
                    string customAttributeName = HttpContext.Current.Session["CustomerItemNo"].ToString();
                    DataTable attrName = importController.SelectedColumnsToImport(new DataTable(), model);
                    string[] fieldNames = attrName == null ? null : attrName.Rows.Count == 0 ? null : attrName.AsEnumerable().Select(x => x.Field<string>("ATTRIBUTE_NAME")).ToArray();
                    DataTable itemImport = ValidateItemNumber(dt);
                    // To delete the coloums for pdf xpress 

                    if (itemImport != null)
                    {
                        if (itemImport.Columns.Contains("Catalog_PdfTemplate"))
                        {
                            itemImport.Columns.Remove("Catalog_PdfTemplate");
                            itemImport.Columns.Remove("Category_PdfTemplate");
                            itemImport.Columns.Remove("Family_PdfTemplate");
                            itemImport.Columns.Remove("Product_PdfTemplate");

                        }
                    }
                    if (itemImport != null && itemImport.Rows.Count > 0)
                    {
                        foreach (string fieldName in fieldNames)
                        {
                            foreach (DataColumn colName in itemImport.Columns)
                            {
                                if (colName.ColumnName.ToString().ToUpper() == fieldName.ToUpper().Replace(".", "#"))
                                {
                                    colName.ColumnName = fieldName;
                                }
                            }
                        }
                        return CatalogItemNumberRestore(itemImport, "", allowDuplicate, catalogId, importType, model, importTemp);
                    }
                    //Item# import process stop
                    ////Familyname import starts
                    DataTable attrNameFamily = importController.SelectedFamilyColumnsToImport(new DataTable(), model);
                    string[] fieldNamesFamily = attrNameFamily == null ? null : attrNameFamily.Rows.Count == 0 ? null : attrNameFamily.AsEnumerable().Select(x => x.Field<string>("ATTRIBUTE_NAME")).ToArray();
                    DataTable familyNameImport = ValidateFamilyData(dt);
                    // To delete the coloums for pdf xpress 
                    if (familyNameImport != null)
                    {
                        if (familyNameImport.Columns.Contains("Catalog_PdfTemplate"))
                        {
                            familyNameImport.Columns.Remove("Catalog_PdfTemplate");
                            familyNameImport.Columns.Remove("Category_PdfTemplate");
                            familyNameImport.Columns.Remove("Family_PdfTemplate");
                            familyNameImport.Columns.Remove("Product_PdfTemplate");

                        }
                    }
                    if (familyNameImport != null && familyNameImport.Rows.Count > 0)
                    {
                        foreach (string fieldName in fieldNamesFamily)
                        {
                            foreach (DataColumn colName in familyNameImport.Columns)
                            {
                                if (colName.ColumnName.ToString().ToUpper() == fieldName.ToUpper().Replace(".", "#"))
                                {
                                    colName.ColumnName = fieldName;
                                }
                            }
                        }
                        return CatalogItemNumberRestore(familyNameImport, "", allowDuplicate, catalogId, importType, model, importTemp);
                    }
                    ////Familyname import stop
                    if (importType != "Families" && importType != "Categories")
                    {
                        if (allowDuplicate == "1")
                        {
                            DataTable dtGetProdCnt = dt;
                            if (importType.ToUpper().Contains("PRODUCT") && dtGetProdCnt.Columns.Contains("ITEM#"))
                            {
                                dtGetProdCnt.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                            }
                            if (importType.ToUpper().Contains("PRODUCT"))
                            {
                                foreach (DataColumn dcReplaceColumn in dtGetProdCnt.Columns)
                                {
                                    if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                                    {
                                        dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                        break;
                                    }
                                }
                            }
                            dsGetProdCnt.Tables.Add(dtGetProdCnt.Select(" isnull(Catalog_item_no,'') <> ''").CopyToDataTable());
                        }
                        else
                        {
                            DataTable dtGetProdCnt = dt;
                            if (importType.ToUpper().Contains("PRODUCT"))
                            {
                                foreach (DataColumn dcReplaceColumn in dtGetProdCnt.Columns)
                                {
                                    if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                                    {
                                        dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                        break;
                                    }
                                }
                            }
                            if (!dtGetProdCnt.Columns.Contains("CATALOG_ID"))
                            {
                                dtGetProdCnt.Columns.Add("CATALOG_ID");
                                dtGetProdCnt.Columns["CATALOG_ID"].SetOrdinal(0);
                            }
                            if (!dtGetProdCnt.Columns.Contains("CATEGORY_ID"))
                            {
                                dtGetProdCnt.Columns.Add("CATEGORY_ID");
                                dtGetProdCnt.Columns["CATEGORY_ID"].SetOrdinal(2);
                            }
                            if (!dtGetProdCnt.Columns.Contains("FAMILY_ID"))
                            {
                                dtGetProdCnt.Columns.Add("FAMILY_ID");
                                dtGetProdCnt.Columns["FAMILY_ID"].SetOrdinal(4);

                            }
                            if (!dtGetProdCnt.Columns.Contains("SUBFAMILY_ID"))
                            {
                                dtGetProdCnt.Columns.Add("SUBFAMILY_ID");
                                dtGetProdCnt.Columns["SUBFAMILY_ID"].SetOrdinal(6);

                            }
                            if (!dtGetProdCnt.Columns.Contains("PRODUCT_ID"))
                            {
                                dtGetProdCnt.Columns.Add("PRODUCT_ID");
                                dtGetProdCnt.Columns["PRODUCT_ID"].SetOrdinal(8);
                            }

                            if (importType.ToUpper().Contains("PRODUCT"))
                            {
                                foreach (DataColumn dcRename in dtGetProdCnt.Columns)
                                {
                                    if (dcRename.ColumnName.ToUpper() == "ITEM#")
                                    {
                                        dtGetProdCnt.Columns[dcRename.ColumnName].ColumnName = "CATALOG_ITEM_NO";
                                        break;
                                    }
                                }
                            }
                            DataRow[] drow = dtGetProdCnt.Select("PRODUCT_ID IS NULL  and isnull(Catalog_item_no,'') <> ''");
                            if (drow.Count() > 0)
                            {
                                dsGetProdCnt.Tables.Add(dtGetProdCnt.Select("PRODUCT_ID IS NULL  and isnull(Catalog_item_no,'') <> ''").CopyToDataTable());
                                if (SKUcount.Any())
                                {
                                    userSKUProductCount = Convert.ToInt32(SKUcount[0].SKU_COUNT);
                                    customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                                }

                                if (Convert.ToInt32(dsGetProdCnt.Tables[0].Rows.Count) > 0)
                                {
                                    importProductCount = dsGetProdCnt.Tables[0].Rows.Count;
                                    var productcount = objLS.STP_LS_GET_PRODUCTS_BY_USER("PRODUCT", User.Identity.Name, "").ToList();

                                    if (productcount.Any())
                                    {
                                        userProductCount = Convert.ToInt32(productcount[0]);
                                    }

                                    if ((userProductCount + importProductCount) > userSKUProductCount)
                                    {

                                        return "SKU Exceed~" + importTemp;

                                    }
                                }
                                progressBar.progressVale = "10";
                                importCount = dtGetProdCnt.Rows.Count;
                            }
                            else
                            {
                                importCount = dtGetProdCnt.Rows.Count;
                            }

                        }
                    }
                    else
                    {
                        if (SKUcount.Any())
                        {
                            customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                        }
                    }
                    if (customerId == 0)
                    {
                        if (SKUcount.Any())
                        {
                            customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                        }
                    }
                    //importExcelSheetSelection(excelPath, SheetName);
                    DataTable excelData = dt;
                    // To delete the coloums for pdf xpress 
                    if (excelData != null)
                    {
                        if (excelData.Columns.Contains("Catalog_PdfTemplate"))
                        {
                            excelData.Columns.Remove("Catalog_PdfTemplate");
                            excelData.Columns.Remove("Category_PdfTemplate");
                            excelData.Columns.Remove("Family_PdfTemplate");
                            excelData.Columns.Remove("Product_PdfTemplate");

                        }
                    }
                    if (importType.ToUpper().Contains("PRODUCT"))
                    {
                        foreach (DataColumn dcReplaceColumn in excelData.Columns)
                        {
                            if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                            {
                                dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                break;
                            }

                        }
                    }
                    if (!excelData.Columns.Contains("CATALOG_ID"))
                    {
                        excelData.Columns.Add("CATALOG_ID");
                        excelData.Columns["CATALOG_ID"].SetOrdinal(0);
                    }
                    if (!excelData.Columns.Contains("CATALOG_NAME"))
                    {
                        excelData.Columns.Add("CATALOG_NAME");
                        excelData.Columns["CATALOG_NAME"].SetOrdinal(1);
                    }
                    if (!excelData.Columns.Contains("CATEGORY_ID"))
                    {
                        excelData.Columns.Add("CATEGORY_ID");
                        excelData.Columns["CATEGORY_ID"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_NAME"));

                    }
                    int col = 1;
                    for (int colindex = 0; colindex < excelData.Columns.Count; colindex++)
                    {
                        if (excelData.Columns[colindex].ColumnName.ToUpper().Contains("SUBCATNAME"))
                        {
                            if (!excelData.Columns.Contains("SUBCATID_L" + col))
                            {
                                excelData.Columns.Add("SUBCATID_L" + col);
                                excelData.Columns["SUBCATID_L" + col].SetOrdinal(excelData.Columns.IndexOf("SUBCATNAME_L" + col));
                                colindex = colindex + 1;
                            }
                            if (excelData.Columns.Contains("SUBCATID_L" + col))
                            {
                                col = col + 1;
                            }
                        }
                    }
                    if (importType != "Categories")
                    {
                        if (!excelData.Columns.Contains("FAMILY_ID"))
                        {
                            excelData.Columns.Add("FAMILY_ID");
                            excelData.Columns["FAMILY_ID"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_NAME"));
                        }
                        if (!excelData.Columns.Contains("SUBFAMILY_ID"))
                        {
                            excelData.Columns.Add("SUBFAMILY_ID");
                            excelData.Columns["SUBFAMILY_ID"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_NAME") + 1);
                        }
                        if (!excelData.Columns.Contains("SUBFAMILY_NAME"))
                        {
                            excelData.Columns.Add("SUBFAMILY_NAME");
                            excelData.Columns["SUBFAMILY_NAME"].SetOrdinal(excelData.Columns.IndexOf("SUBFAMILY_ID") + 1);
                        }
                    }
                    if (!excelData.Columns.Contains("PRODUCT_ID"))
                    {
                        if (importType == "Products")
                        {
                            excelData.Columns.Add("PRODUCT_ID");
                            if (excelData.Columns.Contains("CATALOG_ITEM_NO") == true)
                            {
                                excelData.Columns["PRODUCT_ID"].SetOrdinal(excelData.Columns.IndexOf("CATALOG_ITEM_NO"));
                            }
                        }
                    }
                    if (importType != "Categories")
                    {
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2WEB"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2WEB");
                            excelData.Columns["FAMILY_PUBLISH2WEB"].SetOrdinal(excelData.Columns.IndexOf("SUBFAMILY_ID") + 1);
                        }
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2PDF"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2PDF");
                            excelData.Columns["FAMILY_PUBLISH2PDF"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_PUBLISH2WEB"));
                        }
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2EXPORT"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2EXPORT");
                            excelData.Columns["FAMILY_PUBLISH2EXPORT"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_PUBLISH2PDF"));
                        }
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2PRINT"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2PRINT");
                            excelData.Columns["FAMILY_PUBLISH2PRINT"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_PUBLISH2EXPORT"));
                        }
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2PORTAL"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2PORTAL");
                            excelData.Columns["FAMILY_PUBLISH2PORTAL"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_PUBLISH2PRINT"));
                        }
                    }
                    if (!excelData.Columns.Contains("CATEGORY_PUBLISH2WEB"))
                    {
                        excelData.Columns.Add("CATEGORY_PUBLISH2WEB");
                        excelData.Columns["CATEGORY_PUBLISH2WEB"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_ID"));
                    }
                    if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PDF"))
                    {
                        excelData.Columns.Add("CATEGORY_PUBLISH2PDF");
                        excelData.Columns["CATEGORY_PUBLISH2PDF"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2WEB"));
                    }
                    if (!excelData.Columns.Contains("CATEGORY_PUBLISH2EXPORT"))
                    {
                        excelData.Columns.Add("CATEGORY_PUBLISH2EXPORT");
                        excelData.Columns["CATEGORY_PUBLISH2EXPORT"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PDF"));
                    }
                    if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PRINT"))
                    {
                        excelData.Columns.Add("CATEGORY_PUBLISH2PRINT");
                        excelData.Columns["CATEGORY_PUBLISH2PRINT"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2EXPORT"));
                    }
                    if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PORTAL"))
                    {
                        excelData.Columns.Add("CATEGORY_PUBLISH2PORTAL");
                        excelData.Columns["CATEGORY_PUBLISH2PORTAL"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PRINT"));
                    }
                    if (importType.ToUpper().Contains("PRODUCT"))
                    {
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2WEB"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2WEB");
                        excelData.Columns["PRODUCT_PUBLISH2WEB"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_ID"));
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2PDF"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2PDF");
                        excelData.Columns["PRODUCT_PUBLISH2PDF"].SetOrdinal(excelData.Columns.IndexOf("PRODUCT_PUBLISH2WEB"));
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2EXPORT"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2EXPORT");
                        excelData.Columns["PRODUCT_PUBLISH2EXPORT"].SetOrdinal(excelData.Columns.IndexOf("PRODUCT_PUBLISH2PDF"));
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2PRINT"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2PRINT");
                        excelData.Columns["PRODUCT_PUBLISH2PRINT"].SetOrdinal(excelData.Columns.IndexOf("PRODUCT_PUBLISH2EXPORT"));
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2PORTAL"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2PORTAL");
                        excelData.Columns["PRODUCT_PUBLISH2PORTAL"].SetOrdinal(excelData.Columns.IndexOf("PRODUCT_PUBLISH2PRINT"));
                    }
                    // }
                    ImportApiController importApiController = new ImportApiController();
                    if (importType.ToUpper().Contains("PRODUCT") && excelData.Columns.Contains("ITEM#"))
                    {
                        excelData.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                    }
                    excelData = importApiController.UpdateCatalogDetails(importType, catalogId, excelData);
                    DataTable oattType = new DataTable();
                    DataSet replace = new DataSet();
                    int ItemVal = Convert.ToInt32(allowDuplicate);
                    using (var conn = new SqlConnection(connectionString))
                    {
                        conn.Open();
                        SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                        _DBCommand.ExecuteNonQuery();


                        //Mapping attribute -Start-------------------------------------------------------------------

                        var Mappingattributes = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                        if (Mappingattributes.Count != 0)
                        {
                            for (int i = 0; i < Mappingattributes.Count; i++)
                            {
                                for (int j = 0; j < excelData.Columns.Count; j++)
                                {
                                    if (Mappingattributes[i].Excel_Column == excelData.Columns[j].ColumnName)
                                    {
                                        excelData.Columns[j].ColumnName = Mappingattributes[i].Mapping_Name;
                                    }
                                }
                            }
                        }

                        //Mapping Attribute - End ---------------------------------------------------------------------
                        int col_New = 1;
                        if (importType.ToLower() == "products")
                        {
                            if (excelData != null && excelData.Rows.Count > 0)
                            {
                                for (int colindex = 0; colindex < excelData.Columns.Count; colindex++)
                                {
                                    if (excelData.Columns[colindex].ColumnName.ToUpper().Contains("SUBCATNAME"))
                                    {
                                        if (excelData.Columns.Contains("SUBCATID_L" + col_New))
                                        {
                                            if (col_New == 1)
                                            {
                                                excelData.Columns["SUBCATID_L" + col_New].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PORTAL"));
                                                excelData.Columns["SUBCATNAME_L" + col_New].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PORTAL"));
                                            }
                                            else
                                            {
                                                excelData.Columns["SUBCATNAME_L" + col_New].SetOrdinal(excelData.Columns.IndexOf("SUBCATNAME_L" + (col_New - 1)));
                                                excelData.Columns["SUBCATNAME_L" + col_New].SetOrdinal(excelData.Columns.IndexOf("SUBCATNAME_L" + (col_New - 1)));
                                                excelData.Columns["SUBCATID_L" + col_New].SetOrdinal(excelData.Columns.IndexOf("SUBCATNAME_L" + col_New));
                                            }
                                            colindex = colindex + 1;
                                            col_New = col_New + 1;
                                        }
                                    }
                                    //else if (excelData.Columns.Contains("SUBCATID_L" + (col_New+1)))
                                    //{
                                    //    excelData.Columns.Remove("SUBCATID_L" + (col_New + 1));
                                    //}
                                }

                                int index_family = 0;
                                index_family = excelData.Columns.IndexOf("FAMILY_ID");
                                excelData.Columns["FAMILY_NAME"].SetOrdinal(index_family + 1);
                                int index_Prod = 0;
                                index_Prod = excelData.Columns.IndexOf("PRODUCT_ID");
                                excelData.Columns["SUBFAMILY_NAME"].SetOrdinal(index_Prod);
                                if (excelData.Columns.Contains("Action"))
                                {
                                    excelData.Columns.Remove("Action");
                                }
                                int index_categoryId = 0;
                                int index_categoryName = 0;
                                index_categoryId = excelData.Columns.IndexOf("CATEGORY_ID");
                                index_categoryName = excelData.Columns.IndexOf("CATEGORY_NAME");
                                if ((index_categoryId + 1) != index_categoryName)
                                    excelData.Columns["CATEGORY_NAME"].SetOrdinal(index_categoryId + 1);
                            }
                        }

                        SQLString = importController.CreateTable("[##IMPORTTEMP" + importTemp + "]", "", "", excelData);
                        SqlCommand dbCommandnew = new SqlCommand(SQLString, conn);
                        dbCommandnew.ExecuteNonQuery();

                        DataTable importData = new DataTable();
                        SqlCommand dbCommandTemp = new SqlCommand();
                        if (importType.ToUpper() == "FAMILIES")
                        {
                            importData = UnPivotTableFamily(excelData, importType);
                            dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTFAMILYTEMPDATA", conn) { CommandTimeout = 0 };
                        }
                        else if (importType.ToUpper() == "CATEGORIES")
                        {
                            importData = UnPivotTableCategory(excelData, importType);
                            dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTCATEGORYTEMPDATA", conn) { CommandTimeout = 0 };
                        }
                        else
                        {
                            importData = UnPivotTable(excelData, importType + "_bulk");
                            dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTTEMPDATA", conn) { CommandTimeout = 0 };
                        }

                        dbCommandTemp.CommandType = CommandType.StoredProcedure;
                        dbCommandTemp.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = importData;
                        dbCommandTemp.Parameters.Add("@TABLENAME", SqlDbType.NChar).Value = "[##IMPORTTEMP" + importTemp + "]";
                        dbCommandTemp.ExecuteNonQuery();


                        importStatus = "20";
                        if (importType.ToUpper() == "CATEGORIES")
                        {
                            SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCATEGORYCOLUMNS ''''" + importTemp + "'''''');END')";

                        }
                        else
                        {
                            SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";

                        }
                        _DBCommand = new SqlCommand(SQLString, conn);
                        _DBCommand.ExecuteNonQuery();

                        oattType = importController.SelectedColumnsToImport(excelData, model);


                        //Mapping attribute -Start-------------------------------------------------------------------

                        var MappingattributesValue = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                        if (MappingattributesValue.Count != 0)
                        {
                            for (int i = 0; i < MappingattributesValue.Count; i++)
                            {
                                for (int j = 0; j < oattType.Rows.Count; j++)
                                {
                                    if (MappingattributesValue[i].Excel_Column.ToString() == oattType.Rows[j]["ATTRIBUTE_NAME"].ToString())
                                    {
                                        oattType.Rows[j][1] = MappingattributesValue[i].Mapping_Name;
                                    }
                                }
                            }
                        }
                        //Mapping Attribute - End ---------------------------------------------------------------------
                        var cmd1 =
                            new SqlCommand(
                                "ALTER TABLE [##IMPORTTEMP" + importTemp +
                                "] ADD Family_Status nvarchar(10),FStatusUI nvarchar(10),StatusUI nvarchar(10),SFSort_Order int,Sort_Order int",
                                conn);
                        cmd1.ExecuteNonQuery();
                        var sqlstring1 =
                            new SqlCommand(
                                "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##AttributeTemp" +
                                importTemp + "]'')BEGIN DROP TABLE [##AttributeTemp" + importTemp + "] END')", conn);
                        sqlstring1.ExecuteNonQuery();
                        var cmd2 = new SqlCommand();
                        cmd2.Connection = conn;
                        SQLString = importController.CreateTableToImport("[##AttributeTemp" + importTemp + "]", oattType);
                        cmd2.CommandText = SQLString;
                        cmd2.CommandType = CommandType.Text;
                        cmd2.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##AttributeTemp" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(oattType);
                        var cmbpk10 = new SqlCommand("update [##AttributeTemp" + importTemp + "] set ATTRIBUTE_TYPE = 1 where ATTRIBUTE_TYPE = 10", conn);
                        cmbpk10.ExecuteNonQuery();

                        var cmbpk1 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1'') IS NOT NULL  DROP TABLE  ##t1') ", conn);
                        cmbpk1.ExecuteNonQuery();
                        var cmbpk2 = new SqlCommand("select * into ##t1  from [##AttributeTemp" + importTemp + "] where attribute_type <>0", conn);
                        cmbpk2.ExecuteNonQuery();
                        var cmbpk3 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2'') IS NOT NULL  DROP TABLE  ##t2') ", conn);
                        cmbpk3.ExecuteNonQuery();
                        var cmbpk4 = new SqlCommand("select * into ##t2  from [##importtemp" + importTemp + "]", conn);
                        cmbpk4.ExecuteNonQuery();
                        //---------------------------------------FOR REPLACING EMPTY VALUES IN TEMP TABLE ---------------------FEB-2017 ------------//
                        var cmd1f12 = new SqlCommand("SELECT * FROM [##importtemp" + importTemp + "]", conn);
                        var daf12 = new SqlDataAdapter(cmd1f12);
                        daf12.Fill(replace);
                        progressBar.progressVale = "30";
                        if (replace.Tables[0].Rows.Count > 0)
                        {
                            foreach (var item1 in replace.Tables[0].Columns)
                            {
                                string column_name2 = item1.ToString();
                                if (column_name2.Contains("'"))
                                {
                                    column_name2 = column_name2.Replace("'", "''''");
                                }
                                var cmbpk41 = new SqlCommand("  exec ('UPDATE [##IMPORTTEMP" + importTemp + "] SET  [" + column_name2 + "] = NULL  WHERE cast( [" + column_name2 + "] as nvarchar) = '''' and  [" + column_name2 + "] is not null ')", conn);
                                cmbpk41.ExecuteNonQuery();
                            }
                        }
                        progressBar.progressVale = "50";
                        if (importType.ToUpper() == "FAMILIES")
                        {
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT_HIERARCHY ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                            cmd.ExecuteNonQuery();
                        }
                        else if (importType.ToUpper() == "CATEGORIES")
                        {
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_CATEGORYATTRIBUTEIMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                            cmd.ExecuteNonQuery();
                        }
                        DataSet rowaffected = new DataSet();
                        SQLString = string.Empty;
                        if (importType.ToUpper() == "FAMILIES")
                        {
                            SQLString =
                         "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" +
                         importTemp + "'')BEGIN select count(*) as UpdateCount from [##IMPORTTEMP" + importTemp + "]  where Family_Status=''Update'' or Family_Status=''Associate'';select count(*) as InsertCount from [##IMPORTTEMP" + importTemp + "]  where Family_Status=''Insert'' ; END')";
                        }
                        else
                        {
                            SQLString =
                          "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" +
                          importTemp + "'')BEGIN select count(*) as UpdateCount from [##IMPORTTEMP" + importTemp + "]  where StatusUI=''Update'' or StatusUI=''Associate'';select count(*) as InsertCount from [##IMPORTTEMP" + importTemp + "]  where StatusUI=''Insert''; END')";
                        }
                        _DBCommand = new SqlCommand(SQLString, conn);
                        SqlDataAdapter dbAdapter = new SqlDataAdapter(_DBCommand);
                        dbAdapter.Fill(rowaffected);
                        progressBar.progressVale = "90";

                        int insertRecords = 0;
                        int updateRecords = 0;
                        if (rowaffected != null && rowaffected.Tables.Count > 1)
                        {
                            if (rowaffected.Tables[0].Rows.Count > 0)
                            {
                                updateRecords = int.Parse(rowaffected.Tables[0].Rows[0][0].ToString());
                            }
                            if (rowaffected.Tables[1].Rows.Count > 0)
                            {
                                insertRecords = int.Parse(rowaffected.Tables[1].Rows[0][0].ToString());
                            }
                        }
                        if (updateRecords == 0 && insertRecords == 0)
                        {
                            updateRecords = importCount;
                        }
                        importController._SQLString = @" if exists(select NAME from TEMPDB.sys.objects where type='u' and name='##LOGTEMPTABLE" + importTemp + "') BEGIN  select * from [##LOGTEMPTABLE" + importTemp + "] End else  Begin select 'Import Success' End ";
                        DataSet ds = importController.CreateDataSet();
                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0][0].ToString() != "Import Success")
                            {
                                importController._SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp + "]";
                                cmd1.CommandText = importController._SQLString;
                                cmd1.CommandType = CommandType.Text;
                                cmd1.ExecuteNonQuery();
                            }
                        }
                        if (ds.Tables[0].Columns[0].ColumnName == "ErrorMessage")
                        {
                            return "Import Failed~" + importTemp;
                        }
                        else
                        {
                            if (importType.ToUpper() == "PRODUCT")
                            {
                                var picklistvaluecreation = objLS.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerId);//To check Picklist validation in enabled or not in customer setting

                                if (picklistvaluecreation != null && picklistvaluecreation.ShowCustomAPPS)
                                {
                                    PickListUpdate(importTemp);
                                }
                            }

                            return "Import Success~" + importTemp + "~InsertRecords:" + insertRecords + "~" + "UpdateRecords:" + updateRecords + "~SkippedRecords:0";
                        }
                        if (conn.State.ToString().ToUpper() == "OPEN")
                            conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at AdvanceImportApiController : FinishProductImport", ex);
                return null;
            }
        }

        public string CatalogItemNumberRestore(DataTable itemImport, string SheetName, string allowDuplicate, int catalogId, string importType, JArray model, string importTemp)
        {
            try
            {
                QueryValues queryValues = new QueryValues();


                // CustomerItemNo
                if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
                {
                    var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    int customerIds;
                    int.TryParse(customerDetails.ToString(), out customerIds);
                    string customerCatalog = queryValues.GetCatalogItemNo(customerIds);
                    System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerCatalog;
                }
                // CustomerSubItemNo
                if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
                {
                    var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    int customerId;
                    int.TryParse(customerDetails.ToString(), out customerId);
                    string customerCatalog = queryValues.GetSubCatalogItemNo(customerId);
                    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerCatalog;
                }


                foreach (DataColumn dcItem in itemImport.Columns)
                {
                    if (dcItem.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerSubItemNo"].ToString())
                        dcItem.ColumnName = "SUBCATALOG_ITEM_NO";
                    if (dcItem.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerItemNo"].ToString())
                        dcItem.ColumnName = "CATALOG_ITEM_NO";
                }
                if (importType.ToUpper().Contains("SUB"))
                {
                    if (!itemImport.Columns.Contains("SUBPRODUCT_PUBLISH2WEB"))
                    {
                        itemImport.Columns.Add("SUBPRODUCT_PUBLISH2WEB");
                        itemImport.Columns["SUBPRODUCT_PUBLISH2WEB"].SetOrdinal(itemImport.Columns.IndexOf("SUBCATALOG_ITEM_NO"));
                    }
                    if (!itemImport.Columns.Contains("SUBPRODUCT_PUBLISH2PDF"))
                    {
                        itemImport.Columns.Add("SUBPRODUCT_PUBLISH2PDF");
                        itemImport.Columns["SUBPRODUCT_PUBLISH2PDF"].SetOrdinal(itemImport.Columns.IndexOf("SUBPRODUCT_PUBLISH2WEB"));
                    }
                    if (!itemImport.Columns.Contains("SUBPRODUCT_PUBLISH2EXPORT"))
                    {
                        itemImport.Columns.Add("SUBPRODUCT_PUBLISH2EXPORT");
                        itemImport.Columns["SUBPRODUCT_PUBLISH2EXPORT"].SetOrdinal(itemImport.Columns.IndexOf("SUBPRODUCT_PUBLISH2PDF"));
                    }
                    if (!itemImport.Columns.Contains("SUBPRODUCT_PUBLISH2PRINT"))
                    {
                        itemImport.Columns.Add("SUBPRODUCT_PUBLISH2PRINT");
                        itemImport.Columns["SUBPRODUCT_PUBLISH2PRINT"].SetOrdinal(itemImport.Columns.IndexOf("SUBPRODUCT_PUBLISH2EXPORT"));
                    }
                    if (!itemImport.Columns.Contains("SUBPRODUCT_PUBLISH2PORTAL"))
                    {
                        itemImport.Columns.Add("SUBPRODUCT_PUBLISH2PORTAL");
                        itemImport.Columns["SUBPRODUCT_PUBLISH2PORTAL"].SetOrdinal(itemImport.Columns.IndexOf("SUBPRODUCT_PUBLISH2PRINT"));
                    }
                }
                else
                {
                    if (!itemImport.Columns.Contains("PRODUCT_PUBLISH2WEB"))
                    {
                        itemImport.Columns.Add("PRODUCT_PUBLISH2WEB");
                        itemImport.Columns["PRODUCT_PUBLISH2WEB"].SetOrdinal(itemImport.Columns.IndexOf("CATALOG_ITEM_NO"));
                    }
                    if (!itemImport.Columns.Contains("PRODUCT_PUBLISH2PDF"))
                    {
                        itemImport.Columns.Add("PRODUCT_PUBLISH2PDF");
                        itemImport.Columns["PRODUCT_PUBLISH2PDF"].SetOrdinal(itemImport.Columns.IndexOf("PRODUCT_PUBLISH2WEB"));
                    }
                    if (!itemImport.Columns.Contains("PRODUCT_PUBLISH2EXPORT"))
                    {
                        itemImport.Columns.Add("PRODUCT_PUBLISH2EXPORT");
                        itemImport.Columns["PRODUCT_PUBLISH2EXPORT"].SetOrdinal(itemImport.Columns.IndexOf("PRODUCT_PUBLISH2PDF"));
                    }
                    if (!itemImport.Columns.Contains("PRODUCT_PUBLISH2PRINT"))
                    {
                        itemImport.Columns.Add("PRODUCT_PUBLISH2PRINT");
                        itemImport.Columns["PRODUCT_PUBLISH2PRINT"].SetOrdinal(itemImport.Columns.IndexOf("PRODUCT_PUBLISH2EXPORT"));
                    }
                    if (!itemImport.Columns.Contains("PRODUCT_PUBLISH2PORTAL"))
                    {
                        itemImport.Columns.Add("PRODUCT_PUBLISH2PORTAL");
                        itemImport.Columns["PRODUCT_PUBLISH2PORTAL"].SetOrdinal(itemImport.Columns.IndexOf("PRODUCT_PUBLISH2PRINT"));
                    }
                }

                var specs = importType.ToUpper().Contains("SUBPRODUCT") ?
                    itemImport.AsEnumerable().Select(item => item.Field<String>("SUBCATALOG_ITEM_NO")).Distinct().ToArray() : itemImport.AsEnumerable().Select(item => item.Field<String>("CATALOG_ITEM_NO")).Distinct().ToArray();
                var product = importType.ToUpper().Contains("SUBPRODUCT") ? objLS.TB_SUBPRODUCT.Join(objLS.TB_PROD_SPECS, ts => ts.SUBPRODUCT_ID, tps => tps.PRODUCT_ID, (ts, tps) => new { ts, tps }).Where(y => y.ts.CATALOG_ID == catalogId && y.tps.ATTRIBUTE_ID == 1 && specs.Contains(y.tps.STRING_VALUE)).Select(x => x.tps.PRODUCT_ID).Distinct().Count() :
                    objLS.TB_CATALOG_PRODUCT.Join(objLS.TB_PROD_SPECS, ts => ts.PRODUCT_ID, tps => tps.PRODUCT_ID, (ts, tps) => new { ts, tps }).Where(y => y.ts.CATALOG_ID == catalogId && y.tps.ATTRIBUTE_ID == 1 && specs.Contains(y.tps.STRING_VALUE)).Select(x => x.tps.PRODUCT_ID).Distinct().Count();

                if (GetSkuCount(product) == "SKU Exceed")
                {
                    return "SKU Exceed~" + importTemp;
                }
                var customers = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                DataTable importData = UnPivotTable(itemImport, importType);

                DataTable attrType = importController.SelectedColumnsToImport(itemImport, model);
                DataTable AttributeData = new DataTable();
                AttributeData.Columns.Add("AttrtId");
                AttributeData.Columns["AttrtId"].DataType = System.Type.GetType("System.Int32");
                AttributeData.Columns["AttrtId"].AutoIncrement = true;
                AttributeData.Columns["AttrtId"].AutoIncrementSeed = 1;
                AttributeData.Columns.Add("ATTRIBUTENAME");
                AttributeData.Columns.Add("ATTRIBUTETYPE");
                for (int index = 0; index < attrType.Rows.Count; index++)
                {
                    DataRow dr = AttributeData.NewRow();
                    dr["ATTRIBUTENAME"] = attrType.Rows[index]["ATTRIBUTE_NAME"].ToString();
                    dr["ATTRIBUTETYPE"] = attrType.Rows[index]["ATTRIBUTE_TYPE"].ToString();
                    AttributeData.Rows.Add(dr);
                }

                // -------------------------------------------------------- Mapping - Attributes - Start.

                //Mapping attribute -Start-------------------------------------------------------------------

                var Mappingattributes = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                if (Mappingattributes.Count != 0)
                {
                    for (int i = 0; i < Mappingattributes.Count; i++)
                    {
                        for (int j = 0; j < importData.Rows.Count; j++)
                        {
                            if (Mappingattributes[i].Excel_Column.ToString() == importData.Rows[j]["ATTRIBUTENAME"].ToString())
                            {
                                importData.Rows[j]["ATTRIBUTENAME"] = Mappingattributes[i].Mapping_Name;
                            }

                        }
                    }
                }

                //Mapping Attribute - End ---------------------------------------------------------------------

                var MappingattributesValue = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                if (MappingattributesValue.Count != 0)
                {
                    for (int i = 0; i < MappingattributesValue.Count; i++)
                    {
                        for (int j = 0; j < AttributeData.Rows.Count; j++)
                        {
                            if (Mappingattributes[i].Excel_Column.ToString() == AttributeData.Rows[j]["ATTRIBUTENAME"].ToString())
                            {
                                AttributeData.Rows[j]["ATTRIBUTENAME"] = Mappingattributes[i].Mapping_Name;
                            }
                        }
                    }
                }

                //Mapping Attribute - End ---------------------------------------------------------------------

                //---------------------------------------------------------- Mapping - Attributes - End.
                DataTable resultSet = new DataTable();
                DataSet resultSet1 = new DataSet();
                SqlCommand sqlCommand = new SqlCommand();
                using (var sqlConnection = new SqlConnection(connectionString))
                {
                    if (importType.ToUpper().Contains("SUBPRODUCT"))
                    {
                        sqlCommand = new SqlCommand("STP_CATALOGSTUDIO5_SUBCATALOGITEMNUMBER_IMPORT", sqlConnection);
                    }
                    else
                    {
                        sqlCommand = new SqlCommand("STP_CATALOGSTUDIO5_CATALOGITEMNUMBER_IMPORT", sqlConnection);
                    }
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.CommandTimeout = 0;
                    sqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    sqlCommand.Parameters.Add("@SESSIONID", SqlDbType.NVarChar).Value = importTemp;
                    sqlCommand.Parameters.Add("@CUSTOMERNAME", SqlDbType.VarChar).Value = User.Identity.Name;
                    sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.VarChar).Value = customers.ToString();
                    sqlCommand.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = importData;
                    sqlCommand.Parameters.Add("@ATTRTEMP", SqlDbType.Structured).Value = AttributeData;
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                    sqlConnection.Open();
                    sqlDataAdapter.Fill(resultSet1);
                    sqlConnection.Close();
                }
                resultSet = resultSet1.Tables[0];
                if (resultSet != null && resultSet.Columns.Contains("STATUS"))
                {
                    if (resultSet.Rows.Count > 0)
                    {
                        int insertedRecourds = importType.ToUpper().Contains("SUBPRODUCT") ? resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "INSERT").Select(cat => cat.Field<string>("SUBCATALOG_ITEM_NO")).Distinct().Count() : resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "INSERT").Select(cat => cat.Field<string>("CATALOG_ITEM_NO")).Distinct().Count();
                        int updatedRecourds = importType.ToUpper().Contains("SUBPRODUCT") ? resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "UPDATE" || string.IsNullOrEmpty(x.Field<string>("STATUS"))).Select(cat => cat.Field<string>("SUBCATALOG_ITEM_NO")).Distinct().Count() : resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "UPDATE" || string.IsNullOrEmpty(x.Field<string>("STATUS"))).Select(cat => cat.Field<string>("CATALOG_ITEM_NO")).Distinct().Count();
                        return "Import Success~" + importTemp + "~InsertRecords:" + insertedRecourds + "~" + "UpdateRecords:" + updatedRecourds + "~SkippedRecords:0" + "~ITEM#";
                    }
                    else
                    {
                        return "Import Success~" + importTemp + "~InsertRecords:0~" + "UpdateRecords:0~SkippedRecords:0" + "~ITEM#";
                    }
                }
                else
                {
                    if (resultSet != null && resultSet.Rows.Count > 0)
                    {
                        HttpContext.Current.Session["ITEMIMPORTSESSION"] = resultSet;
                        return "Import Failed~" + importTemp + "~ITEM#";
                    }
                    return "Import Failed~" + importTemp + "~ITEMEMPTY";
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : CatalogItemNumberImport", ex);
                return "Import failed";
            }
        }

        public DataTable GetImportSpecsForRestore(DataTable dtable, string importType)
        {
            try
            {
                DataTable dtable1 = importSelectionColumnGridForrestore(importType, dtable);

                string[] familyAttr = new string[] { "7", "9", "11", "12", "13", "" };
                DataTable dtClone = dtable1;
                for (int j = dtable1.Rows.Count - 1; j >= 0; j--)
                {
                    DataRow dr = dtable1.Rows[j];
                    if (importType.ToUpper().Contains("FAM") && !familyAttr.Contains(dr["FieldType"].ToString()) && dr["FieldType"].ToString() != "0" && dr["FieldType"].ToString() != "")
                        dr.Delete();
                    else if (importType.ToUpper().Contains("FAM") && (dr["EXCELCOLUMN"].ToString().ToUpper() == "SUBPRODUCT_ID" || dr["EXCELCOLUMN"].ToString().ToUpper() == "SUBITEM#") || dr["EXCELCOLUMN"].ToString().ToUpper() == "SUBCATALOG_ITEM_NO")
                        dr.Delete();
                    else if (!importType.ToUpper().Contains("FAM") && familyAttr.Contains(dr["FieldType"].ToString()) && dr["FieldType"].ToString() != "0" && dr["FieldType"].ToString() != "")
                        dr.Delete();

                }
                //var dynamicColumns = new Dictionary<string, string>();
                //var sb = new StringBuilder();
                //var sw = new StringWriter(sb);
                //JsonWriter jsonWriter = new JsonTextWriter(sw);
                //var model = new DashBoardModel();
                //using (DataTableReader reader = dtable1.CreateDataReader())
                //{
                //    jsonWriter.WriteStartObject();
                //    jsonWriter.WritePropertyName("Data");
                //    jsonWriter.WriteStartArray();
                //    while (reader.Read())
                //    {
                //        jsonWriter.WriteStartObject();
                //        for (int i = 0; i < reader.FieldCount; i++)
                //        {
                //            jsonWriter.WritePropertyName(reader.GetName(i));
                //            jsonWriter.WriteValue(reader.GetValue(i).ToString());
                //            if (!dynamicColumns.ContainsKey(reader.GetName(i)) && !string.IsNullOrEmpty(reader.GetName(i)))
                //            {
                //                dynamicColumns.Add(reader.GetName(i), reader.GetValue(i).ToString().ToUpper());
                //            }
                //        }
                //        jsonWriter.WriteEndObject();
                //    }
                //    jsonWriter.WriteEndArray();
                //    jsonWriter.WritePropertyName("Columns");
                //    jsonWriter.WriteStartArray();
                //    foreach (string key in dynamicColumns.Keys)
                //    {
                //        jsonWriter.WriteStartObject();
                //        jsonWriter.WritePropertyName("title");
                //        jsonWriter.WriteValue(key);
                //        jsonWriter.WritePropertyName("field");
                //        jsonWriter.WriteValue(key);
                //        jsonWriter.WriteEndObject();
                //    }
                //    jsonWriter.WriteEndArray();
                //    jsonWriter.WriteEndObject();
                //}
                //model.Data = sb.ToString();
                //return sb.ToString();
                return dtable1;

            }
            catch (Exception objexception)
            {
                _logger.Error("Error at ImportApiController : GetProdSpecsforRestore", objexception);
                return null;
            }
        }

        public DataTable importSelectionColumnGridForrestore(string sheetName, DataTable dtTable)
        {
            var dt = new DataTable();
            DataTable dtable = new DataTable();
            try
            {
                if (dtTable != null && dtTable.Rows.Count > 0)
                {

                    if (!sheetName.Contains("$"))
                    {
                        sheetName = sheetName + "$";
                    }
                    dt = new DataTable(sheetName);
                    var dc = new DataColumn("ExcelColumn", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("CatalogField", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("SelectedToImport", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("IsSystemField", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("FieldType", typeof(string));
                    dt.Columns.Add(dc);

                    // For Mapping Attribute'

                    dc = new DataColumn("MappedAttributeName", typeof(string));
                    dt.Columns.Add(dc);

                    // End

                    foreach (DataColumn column in dtTable.Columns)
                    {
                        String sColName = column.ColumnName.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                        DataRow dr = dt.NewRow();

                        if (sColName.ToUpper() != "ACTION")
                        {
                            dr[0] = sColName;
                            dt.Rows.Add(dr);
                        }
                    }

                    _dsSheets.Tables.Add(dt);
                    dt.Dispose();
                    dtable = GetExcelColumn(dt);
                }
                return dtable;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at ImportApiController :importSelectionColumnGrid", objException);
                return null;
            }
        }

        public DataTable GetExcelColumn(DataTable dt)
        {
            try
            {

                // DataTable dt = _dsSheets.Tables[xlsSheetName];
                int i = 0;
                dt.Columns.Add("AttrMapping");
                foreach (DataRow dr in dt.Rows)
                {
                    string sCheckAttributeName = dt.Rows[i][0].ToString().ToUpper();
                    //Check if this is a CatalogStudio system defined field name
                    Boolean vaild = IsCatalogStudioField(sCheckAttributeName);

                    if (vaild) //CatalogStudio Field
                    {
                        dr["CatalogField"] = sCheckAttributeName;
                        dr["IsSystemField"] = true;
                        dr["FieldType"] = 0;
                        dr["SelectedToImport"] = true;

                    }
                    else //User defined attribute name
                    {
                        CSEntities _dbcontext = new CSEntities();

                        string sAttributeNameInCatalog;

                        var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                        if (customerid != null)
                        {
                            var drAttr =
                                _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                                    tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                    .Where(
                                        x =>
                                            x.tcp.ATTRIBUTE_NAME == sCheckAttributeName &&
                                            x.tpf.CUSTOMER_ID == customerid.CustomerId).Select(x => x.tcp).ToList();


                            if (drAttr.Count > 0)
                            {
                                string attrMappingName = "";

                                sAttributeNameInCatalog = drAttr[0].ATTRIBUTE_NAME;
                                //if (templateId != 0)
                                //{
                                //    var importMapping = objLS.QS_IMPORTMAPPING.Where(x => x.TEMPLATE_ID == templateId && x.Excel_Column == sAttributeNameInCatalog).FirstOrDefault();
                                //    if (importMapping != null)
                                //        attrMappingName = importMapping.Mapping_Name;
                                //    else
                                //        attrMappingName = sAttributeNameInCatalog;
                                //}

                                string sAttributeDatatype = drAttr[0].ATTRIBUTE_DATATYPE;
                                string sAttrType = drAttr[0].ATTRIBUTE_TYPE.ToString(CultureInfo.InvariantCulture);
                                if (sAttributeDatatype == "Date and Time")
                                {
                                    if (sAttrType == "6" || sAttrType == "1" || sAttrType == "3")
                                    {
                                        dr["FieldType"] = 10;
                                        dr["SelectedToImport"] = true;
                                        dr["IsSystemField"] = true;
                                    }
                                    else if (sAttrType == "7" || sAttrType == "11" || sAttrType == "13")
                                    {
                                        dr["FieldType"] = 14;
                                        dr["SelectedToImport"] = true;
                                        dr["IsSystemField"] = true;
                                    }
                                }
                                else
                                {
                                    dr["FieldType"] = drAttr[0].ATTRIBUTE_TYPE.ToString();
                                    dr["SelectedToImport"] = true;
                                    dr["IsSystemField"] = true;
                                }
                                dr["CatalogField"] = sAttributeNameInCatalog;
                                dr["AttrMapping"] = attrMappingName;

                            }
                            else
                            {
                                dr["CatalogField"] = sCheckAttributeName;
                                dr["FieldType"] = "";
                                dr["SelectedToImport"] = false;
                            }

                        }
                    }
                    i++;
                }
                return dt;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading GetExcelColumn2CatalogMapping in Import Controller", objException);
                return null;
            }
        }


        /// <summary>
        /// Added by Aswin kumar
        /// </summary>
        /// <param name="selectedTemplateID"></param>
        /// <param name="importTypeSelection"></param>
        /// <param name="validationStatus"></param>
        /// <param name="sheetName"></param>
        /// <returns>This function returns import sheet names</returns>
        [System.Web.Http.HttpPost]

        public JsonResult UpdateImportValidationFlag(int selectedTemplateID, string importTypeSelection, string validationStatus, string sheetName)
        {
            string result = string.Empty;


            var mappingExists = new TB_TEMPLATE_SHEET_DETAILS_IMPORT();

            ImportSheetName importSheetNamesList = new ImportSheetName();
            try
            {

                //if(importTypeSelection)
                string[] importTypes = new string[] { "CATEGORY", "PRODUCT", "ITEM", "TABLEDESIGNER" };

                //if (importTypeSelection == "Product" && sheetName == "Products")
                //{
                //    importTypes[1] = "FAMILY";
                //}

                //if (importTypeSelection == "Item" && sheetName == "Items")
                //{
                //    importTypes[2] = "PRODUCT";
                //}

                //  var mappingExists = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Select(y => y).Where(x => x.TEMPLATE_ID == selectedTemplate && x.IMPORT_TYPE.Trim() == importTypeSelection && x.Sheet_Name == sheetName).ToList();


                mappingExists = objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT.Select(y => y).Where(x => x.TEMPLATE_ID == selectedTemplateID && x.IMPORT_TYPE.Trim() == importTypeSelection && x.SHEET_NAME == sheetName).FirstOrDefault();



                if (mappingExists != null)
                {

                    mappingExists.FLAG_VALIDATION = true;
                    objLS.SaveChanges();

                }

                var import_Sheets = _dbcontext.TB_TEMPLATE_SHEET_DETAILS_IMPORT.Search(t => t.IMPORT_TYPE)
                          .Containing(importTypes).Where(x => x.TEMPLATE_ID == selectedTemplateID && x.FLAG_VALIDATION == true && x.FLAG_MAPPING == true && x.FLAG_IMPORT == false)
                          .Select(t => new { t.SHEET_NAME, t.IMPORT_TYPE, t.IMPORT_SHEET_ID }).OrderBy(x => x.IMPORT_SHEET_ID)
                          .ToList();

                return new JsonResult() { Data = import_Sheets };
            }

            catch (Exception ex)
            {
                _logger.Error("Error at in AdvanceImportApiController function UpdateImportValidationFlag", ex);
                return null;
            }

        }


        /// <summary>
        /// Added by Aswin kumar
        /// </summary>
        /// <param name="selectedTemplateID"></param>
        /// <param name="importTypeSelection"></param>
        /// <param name="sheetName"></param>
        /// <returns>This function returns import type</returns>
        [System.Web.Http.HttpGet]

        public string UpdateImportMappingFlag(int selectedTemplateID, string importTypeSelection, string sheetName)
        {
            string str_Result = string.Empty;


            try
            {
                var templateExists = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == selectedTemplateID).FirstOrDefault();
                var mappingExists = objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT.Where(x => x.TEMPLATE_ID == selectedTemplateID && x.IMPORT_TYPE.Trim() == importTypeSelection && x.SHEET_NAME == sheetName).FirstOrDefault();

                if (templateExists != null && mappingExists != null)
                {

                    switch (importTypeSelection.ToUpper())
                    {
                        case "CATEGORIES":
                            str_Result = "Categories";
                            break;
                        case "PRODUCT":
                            str_Result = "Families";
                            break;
                        case "ITEM":
                            str_Result = "Products";
                            break;
                        case "TABLEDESIGNER":
                            str_Result = "TableDesigner";
                            break;
                    }
                }
            }

            catch (Exception ex)
            {
                _logger.Error("Error at in AdvanceImportApiController :  UpdateImporMappingFlag", ex);
            }

            return str_Result;

        }

        /// <summary>
        /// Added by Aswin kumar
        /// </summary>
        /// <param name="selectedTemplateID"></param>
        /// <param name="importTypeSelection"></param>
        /// <param name="validationStatus"></param>
        /// <param name="sheetName"></param>
        /// <returns>This function returns import flag whether it is saved in datatatable or  not</returns>
        [System.Web.Http.HttpPost]

        public string UpdateImportFlag(int selectedTemplateID, string importTypeSelection, string validationStatus, string sheetName)
        {
            string str_Result = string.Empty;


            var updateimportFlag = new TB_TEMPLATE_SHEET_DETAILS_IMPORT();


            try
            {

                //  var mappingExists = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Select(y => y).Where(x => x.TEMPLATE_ID == selectedTemplate && x.IMPORT_TYPE.Trim() == importTypeSelection && x.Sheet_Name == sheetName).ToList();


                updateimportFlag = objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT.Select(y => y).Where(x => x.TEMPLATE_ID == selectedTemplateID && x.IMPORT_TYPE.Trim() == importTypeSelection && x.SHEET_NAME == sheetName).FirstOrDefault();


                if (updateimportFlag != null)
                {

                    updateimportFlag.FLAG_IMPORT = true;


                    objLS.SaveChanges();


                }

                return str_Result = "1";
            }

            catch (Exception ex)
            {
                _logger.Error("Error at in AdvanceImportApiController function UpdateImportValidationFlag", ex);
                return null;
            }

        }

        public static DataTable ConvertExcelToDataTable(string FileName, string Sheetname)
        {
            DataTable dataTableResult = null;
            int totalSheet = 0; //No of sheets on excel file  
            try
            {
                string strConn;
                if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName +
                              ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1\"";
                else
                    //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName +
                    //          ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"";
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName +
                           ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=1\"";
                // using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=Yes;IMEX=1;';"))
                using (OleDbConnection objConn = new OleDbConnection(strConn))
                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet oleDataSet = new DataSet();
                    DataTable oleDatatable = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = string.Empty;
                    if (oleDatatable != null)
                    {
                        var tempDataTable = (from dataRow in oleDatatable.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();
                        oleDatatable = tempDataTable;
                        totalSheet = oleDatatable.Rows.Count;
                        sheetName = Sheetname + "$";
                    }
                    cmd.Connection = objConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "] ";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(oleDataSet, "excelData");
                    dataTableResult = oleDataSet.Tables["excelData"];
                    return dataTableResult; //Returning Dattable  
                }
            }

            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : ConvertExcelToDataTable", ex);
                return null;
            }
        }

        /// <summary>
        /// Added by Aswin kumar
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="Sheetname"></param>
        /// <returns>This function converts datatable without header</returns>
        public static DataTable ConvertExcelToDataTableWithoutHeader(string FileName, string Sheetname)
        {
            DataTable dataTableResult = null;
            int totalSheet = 0; //No of sheets on excel file  
            try
            {
                string strConn;
                if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName +
                              ";Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1\"";
                else
                    //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName +
                    //          ";Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1\"";
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName +
                             ";Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1\"";
                // using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=No;IMEX=1;';"))
                using (OleDbConnection objConn = new OleDbConnection(strConn))
                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet oleDataSet = new DataSet();
                    DataTable oleDatatable = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = string.Empty;
                    if (oleDatatable != null)
                    {
                        var tempDataTable = (from dataRow in oleDatatable.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();
                        oleDatatable = tempDataTable;
                        totalSheet = oleDatatable.Rows.Count;
                        sheetName = Sheetname + "$";
                    }
                    cmd.Connection = objConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "] ";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(oleDataSet, "excelData");
                    dataTableResult = oleDataSet.Tables["excelData"];
                    return dataTableResult; //Returning Dattable  
                }
            }

            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : ConvertExcelToDataTable", ex);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public List<string> ImportMissingMappingAttributeResult()
        {
            List<string> lt_MissingMappingAttributes = new List<string>();
            lt_MissingMappingAttributes = (List<string>)HttpContext.Current.Session["MissingMappingAttribute"];


            return lt_MissingMappingAttributes;
        }
        #endregion
    }


}

