﻿using Kendo.DynamicLinq;
using log4net;
using LS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;

namespace LS.Web.Controllers
{
    public class DashboardAPIController : ApiController
    {
        //-----------------Common fields--------------------------
        private static readonly ILog Logger = LogManager.GetLogger(typeof(DashboardAPIController));
        private readonly CSEntities _dbcontext = new CSEntities();


        //-------------------End Common Fields------------------------


        // Get Count _ Start 

        [System.Web.Http.HttpPost]
        public DataSet GetDashboardCountDetails(int catalogId)
        {
            try
            {
                var Modified_User = User.Identity.Name;
                var getcatalogDetails = _dbcontext.Customer_User.Join(_dbcontext.Customer_Catalog, cu => cu.CustomerId, cc => cc.CustomerId, (cu, cc) => new { cu, cc }).Where(x => x.cu.User_Name == User.Identity.Name).ToList();
                int customerId = 0;
                int.TryParse(getcatalogDetails[0].cc.CustomerId.ToString(), out customerId);
                DataSet getAllCount = new DataSet();




                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    con.Open();
                    SqlCommand cmd = con.CreateCommand();
                    cmd.Connection = con;
                    cmd.CommandText = "STP_LS_GET_DASHBOARD_DATA_COUNT";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                    cmd.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = Modified_User;
                    cmd.Parameters.Add("@MODIFIED_USER_ID", SqlDbType.Int).Value = customerId;
                    var AllCount = new SqlDataAdapter(cmd);
                    AllCount.Fill(getAllCount);
                    return getAllCount;
                }

            }
            catch (Exception EX)
            {
                return null;

            }
        }


        // Get Count _End 


        [System.Web.Http.HttpPost]
        public DataSourceResult GetRecentlyModifiedCategories(int catalogId, DateTime startDate, DateTime endDate, int option, DataSourceRequest request)
        {

            try
            {

                DataTable recentlyModifiedCategory = new DataTable();
                var Modified_User = User.Identity.Name;
                _dbcontext.Database.CommandTimeout = 0;
                DateTime tday = DateTime.Today;
                endDate = endDate.AddDays(+1);
                DateTime Day1 = DateTime.Today.AddDays(-1);



                if (option == 8)
                {
                    using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        SqlCommand cmd = con.CreateCommand();
                        cmd.Connection = con;
                        cmd.CommandText = "STP_LS_GET_RECENTLYMODIFIEDCATEGORY";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        cmd.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                        cmd.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;
                        cmd.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;
                        cmd.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = Modified_User;
                        //  cmd.Parameters.Add("@SATRTDATE", SqlDbType.DateTime).Value = startDate;
                        //  cmd.Parameters.Add("@ENDDATE", SqlDbType.DateTime).Value = endDate;

                        var Category = new SqlDataAdapter(cmd);
                        Category.Fill(recentlyModifiedCategory);

                        // var queries = recentlyModifiedCategory.AsEnumerable().AsQueryable().ToDataSourceResult(request);
                        var queries = recentlyModifiedCategory.AsEnumerable().Select(x => new
                        {
                            CATEGORY_ID = x.Field<string>("CATEGORY_ID"),
                            CATALOG_ID = x.Field<int>("CATALOG_ID"),
                            PARENT_CATEGORY = x.Field<string>("PARENT_CATEGORY"),
                            CATEGORY_NAME = x.Field<string>("CATEGORY_NAME"),
                            MODIFIED_DATE = x.Field<DateTime>("MODIFIED_DATE"),
                            MODIFIED_USER = x.Field<string>("MODIFIED_USER")
                        });

                        return queries.AsEnumerable().AsQueryable().ToDataSourceResult(request);

                        //  var jsonString = JsonConvert.SerializeObject(recentlyModifiedCategory);
                        // JArray categoryModified = JArray.Parse(jsonString);


                        // return categoryModified.AsEnumerable().AsQueryable().ToDataSourceResult(request);
                    }

                }

                return null;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at DashboardAPIController : GetRecentlyModifiedCategories", objException);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public DataSourceResult GetRecentlyModifiedFamily(int catalogId, DateTime startDate, DateTime endDate, int option, DataSourceRequest request)
        {
            try
            {
                var Modified_User = User.Identity.Name;
                _dbcontext.Database.CommandTimeout = 0;

                DateTime Day1 = DateTime.Today.AddDays(-1);
                DateTime tday = DateTime.Today;
                endDate = endDate.AddDays(+1);


                if (option == 8)
                {

                    DataTable RecentltModifiedFamily = new DataTable();

                    using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = conn;
                        cmd.CommandText = "STP_LS_GET_RECENTLYMODIFIEDFAMILY";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        cmd.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                        cmd.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = Modified_User;
                        cmd.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;
                        cmd.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;
                        var Family = new SqlDataAdapter(cmd);
                        Family.Fill(RecentltModifiedFamily);


                        var queries = RecentltModifiedFamily.AsEnumerable().Select(x => new
                        {
                            FAMILY_ID = x.Field<int>("FAMILY_ID"),
                            FAMILY_NAME = x.Field<string>("FAMILY_NAME"),
                            CATEGORY_ID = x.Field<string>("CATEGORY_ID"),
                            CATALOG_ID = x.Field<int>("CATALOG_ID"),
                            CATALOG_NAME = x.Field<string>("CATALOG_NAME"),
                            DESCRIPTION = x.Field<string>("DESCRIPTION"),
                            VERSION = x.Field<string>("VERSION"),
                            SORT_ORDER = x.Field<int>("SORT_ORDER"),

                            MODIFIED_DATE = x.Field<DateTime>("MODIFIED_DATE"),
                            MODIFIED_USER = x.Field<string>("MODIFIED_USER")
                        });

                        return queries.AsEnumerable().AsQueryable().ToDataSourceResult(request);




                        //var JsonObject = JsonConvert.SerializeObject(RecentltModifiedFamily);
                        //JArray FamilyResult = JArray.Parse(JsonObject);

                        //return FamilyResult.AsEnumerable().AsQueryable().ToDataSourceResult(request);

                    }


                }

                return null;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at DashboardAPIController : GetRecentlyModifiedFamily", objException);

                return null;
            }

        }

        [System.Web.Http.HttpPost]
        public DataSourceResult GetRecentlyModifiedProducts(int catalogId, DateTime startDate, DateTime endDate, int option, string pageload, DataSourceRequest request)
        {
            try
            {
                DataTable recentlyModifiedProduct = new DataTable();
                var Modified_User = User.Identity.Name;
                _dbcontext.Database.CommandTimeout = 0;
                DateTime tday = DateTime.Today;
                DateTime Day1 = DateTime.Today.AddDays(-1);
                endDate = endDate.AddDays(+1);

                var getcatalogDetails = _dbcontext.Customer_User.Join(_dbcontext.Customer_Catalog, cu => cu.CustomerId, cc => cc.CustomerId, (cu, cc) => new { cu, cc }).Where(x => x.cu.User_Name == User.Identity.Name).ToList();
                int customerId = 0;
                int.TryParse(getcatalogDetails[0].cc.CustomerId.ToString(), out customerId);

                if (option == 8)
                {
                    //var queries = _dbcontext.STP_LS_GET_RECENTLYMODIFIEDPRODUCTS(catalogId, startDate, endDate, customerId, option).ToList();
                    //return queries.AsQueryable().ToDataSourceResult(request);
                    using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        SqlCommand cmd = con.CreateCommand();
                        cmd.Connection = con;
                        cmd.CommandText = "STP_LS_GET_RECENTLYMODIFIEDPRODUCTS";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        cmd.Parameters.Add("@CatalogID", SqlDbType.Int).Value = catalogId;
                        cmd.Parameters.Add("@Customer_id", SqlDbType.Int).Value = customerId;

                         cmd.Parameters.Add("@modifiedUser", SqlDbType.NVarChar, 100).Value = Modified_User;
                        cmd.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;
                        cmd.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;
                        cmd.Parameters.Add("@loadOption", SqlDbType.NVarChar).Value = pageload.ToUpper();
                        var subProduct = new SqlDataAdapter(cmd);
                        subProduct.Fill(recentlyModifiedProduct);



                        var queries = recentlyModifiedProduct.AsEnumerable().Select(x => new
                        {
                            PRODUCT_ID = x.Field<int>("PRODUCT_ID"),
                            STRING_VALUE = x.Field<string>("STRING_VALUE"),
                            MODIFIED_DATE = x.Field<DateTime>("MODIFIED_DATE"),
                            MODIFIED_USER = x.Field<string>("MODIFIED_USER")
                        });

                        return queries.AsEnumerable().AsQueryable().ToDataSourceResult(request);

                    }
                }

                return null;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at DashboardAPIController : GetRecentlyModifiedProducts", objException);
                var objrecentlymofidyproduct = new List<STP_LS_RECENTLYMODIFIEDPRODUCTS_Result>();
                return null;
            }

        }

        [System.Web.Http.HttpPost]
        public DataSourceResult GetRecentlyModifiedSubProducts(int catalogId, DateTime startDate, DateTime endDate, int option, DataSourceRequest request)
        {
            try
            {
                // return null;
                DataTable recentlyModifiedSubproduct = new DataTable();
                var Modified_User = User.Identity.Name;
                _dbcontext.Database.CommandTimeout = 0;
                DateTime tday = DateTime.Today;
                endDate = endDate.AddDays(+1);
                DateTime Day1 = DateTime.Today.AddDays(-1);

                var getcatalogDetails = _dbcontext.Customer_User.Join(_dbcontext.Customer_Catalog, cu => cu.CustomerId, cc => cc.CustomerId, (cu, cc) => new { cu, cc }).Where(x => x.cu.User_Name == User.Identity.Name).ToList();
                int customerId = 0;
                int.TryParse(getcatalogDetails[0].cc.CustomerId.ToString(), out customerId);


                if (option == 8)
                {
                    //var queries = _dbcontext.STP_LS_GET_RECENTLYMODIFIEDSUBPRODUCTS(catalogId, startDate, endDate, customerId, option).ToList();
                    //return queries.AsQueryable().ToDataSourceResult(request);
                    using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        SqlCommand cmd = con.CreateCommand();
                        cmd.Connection = con;
                        cmd.CommandText = "STP_LS_GET_RECENTLYMODIFIEDSUBPRODUCTS";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        cmd.Parameters.Add("@CatalogID", SqlDbType.Int).Value = catalogId;
                        cmd.Parameters.Add("@Customer_id", SqlDbType.Int).Value = customerId;

                        // cmd.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = Modified_User;
                        cmd.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDate;
                        cmd.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDate;
                        

                        var subProduct = new SqlDataAdapter(cmd);
                        subProduct.Fill(recentlyModifiedSubproduct);

                        // var queries = recentlyModifiedCategory.AsEnumerable().AsQueryable().ToDataSourceResult(request);
                        var queries = recentlyModifiedSubproduct.AsEnumerable().Select(x => new
                        {
                            SUBPRODUCT_ID = x.Field<int>("SUBPRODUCT_ID"),
                            STRING_VALUE = x.Field<string>("STRING_VALUE"),
                            CustomerId = x.Field<int>("CustomerId"),
                            CATALOG_ID = x.Field<int>("CATALOG_ID"),


                            MODIFIED_DATE = x.Field<DateTime>("MODIFIED_DATE"),
                            MODIFIED_USER = x.Field<string>("MODIFIED_USER")
                        });

                        return queries.AsEnumerable().AsQueryable().ToDataSourceResult(request);

                        //var jsonString = JsonConvert.SerializeObject(recentlyModifiedSubproduct);
                        //JArray subProductModified = JArray.Parse(jsonString);


                        //return subProductModified.AsEnumerable().AsQueryable().ToDataSourceResult(request);
                    }
                }

                return null;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at DashboardAPIController : GetRecentlyModifiedSubProducts", objException);
                var objrecentlymofidyproduct = new List<STP_LS_RECENTLYMODIFIEDPRODUCTS_Result>();
                return null;
            }

        }
    }
}
