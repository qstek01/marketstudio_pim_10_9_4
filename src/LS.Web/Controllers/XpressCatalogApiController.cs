﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Net;
using LS.Data.Utilities;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Drawing.Printing;
using Stimulsoft.Report;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;
using LS.Data;
using System.Data;
using log4net;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Web.Http;
using LS.Data.Model.XpressCatalog;
using System.Configuration;
using System.Net.Http;
using System.Globalization;
using System.Windows.Forms;
using LS.Data.Model;
using System.IO;
using Kendo.DynamicLinq;
using System.Web;
using System.Web.Configuration;
using LS.Web.Helpers;

namespace LS.Web.Controllers
{
    public class XpressCatalogApiController : ApiController
    {
        private static int projectId;
        public static JArray selectedTreeNodes;
        public static JArray selectedGroupByTreeNodes;
        private readonly PrinterSettings _printerSetting = new PrinterSettings();
        private ILog _logger = LogManager.GetLogger(typeof(AccountsController));
        CSEntities _dbcontext = new CSEntities();
        XpressCatalog objXpress = new XpressCatalog();
        public string _SQLString = "";
        private readonly int[] _fieldCount = new int[20];
        private int _field;
        private string _uPath = string.Empty;
        private string _userImagepath = string.Empty;
        public bool Opensimple;
        public bool Formopen;
        private bool _runedTemplate;
        private StiReport _openedReport = new StiReport();
        public StiReport report = new StiReport();
        public StiReport simpleCatalogReport = new StiReport();
        private static int _workingCatalogID;
        private StiDataBand _dataBand;
        private StiCrossDataBand _crossDataBand;
        private double _leftValue;
        private bool _fBand;
        private bool _cBand;
        private bool _familyAttachmentBool;
        private readonly double[] _widthValue = new double[10];
        private bool _pTablebool;
        private int _familyDescription;
        string multipleTables = "", referenceTables = "";
        private string _connectionString = string.Empty;
        private string strval = string.Empty;
        private string datatype = string.Empty;
        private int noofdecimalplace = 0;
        string sysAttr;
        private static string ColumnNumericEditor = "";
        private static string ColumnWidthNumericEditor = "";
        private static string ColumnGapNumericEditor = "";
        private static string LeftNumericEditor = "";
        private static string RightNumericEditor = "";
        private static string TopNumericEditor = "";
        private static string BottomNumericEditor = "";
        private static string PaperSizeCombo = "";
        private static string WidthNumericEditor = "";
        private static string HeightNumericEditor = "";
        private static string sWidthNumericEditor = "";
        private static string sHeightNumericEditor = "";
        private static int pageformatOptionSet = 0;
        private static int typeOptionSet_Tab1 = 0;
        private static int CoverPageOptionSet = 0;
        private static int TOCOptionSet = 0;
        private static int TOC_Cat_Fam_OptionSet = 0;
        private static int IndexPageOptionSet = 0;
        private static int Index_Cat_No_OptionSet = 0;
        private static string IndexColumns_NumericEditor = "";
        private static int workingCatalogID;

        private static string selectedCategoryId = "";
        private static string fullCategoryIds = "";


        #region  PdfXpress Separate path
        // To pass dynamic path location from web config   - Start
        string IsServerUpload = WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
        string pathDesignation = WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
        string pathUrl = WebConfigurationManager.AppSettings["ExternalAssetDriveURL"];
        #endregion


        [System.Web.Http.HttpGet]
        public string getCategoryId(string categoryId)
        {
            selectedCategoryId = categoryId;
            fullCategoryIds = "";
            var categoryIds = selectedCategoryId.Split(',');
            TB_CATEGORY tb_category = new TB_CATEGORY();
            foreach (string category_Id in categoryIds)
            {
                var tb_categoryList = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category_Id).ToList();
                fullCategoryIds = fullCategoryIds + "," + category_Id;
                foreach (var category in tb_categoryList)
                {
                    fullCategoryIds = fullCategoryIds + "," + category.CATEGORY_ID;
                    //categoryIds.Add(category.CATEGORY_ID);
                    var tb_categoryLists = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category.CATEGORY_ID).ToList();
                    foreach (var categorys in tb_categoryLists)
                    {
                        fullCategoryIds = fullCategoryIds + "," + categorys.CATEGORY_ID;
                        //categoryIds.Add(categorys.CATEGORY_ID);
                        string category_ID = categorys.CATEGORY_ID;
                        do
                        {
                            tb_category = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category_ID).FirstOrDefault();
                            if (tb_category != null)
                            {
                                category_ID = tb_category.CATEGORY_ID;
                                fullCategoryIds = fullCategoryIds + "," + category_ID;
                                //categoryIds.Add(tb_category.CATEGORY_ID);
                            }
                        } while (tb_category != null);

                    }
                }
            }
            fullCategoryIds = fullCategoryIds.Substring(1);
            System.Web.HttpContext.Current.Session["SelectedCategories"] = fullCategoryIds;
            return fullCategoryIds;
        }


        [System.Web.Http.HttpGet]
        public int getCatalogID(int catalogId)
        {
            workingCatalogID = catalogId;
            _workingCatalogID = catalogId;

            System.Web.HttpContext.Current.Session["CURRENTCATALOGID"] = catalogId;
            return catalogId;
        }
        [System.Web.Http.HttpGet]
        public IList GetPaperSize()
        {
            try
            {
                var objPageSizeSettings = _dbcontext.PageSizeSettings;
                return objPageSizeSettings.ToList();
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApiController GetpaperSize", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetPaperWidth(string sizename)
        {
            try
            {
                var widthlst = new List<PageSizeSetting>();
                var objPageSizeSettings = _dbcontext.PageSizeSettings;
                var settings = new List<PageSizeSetting>();
                settings = objPageSizeSettings.ToList();
                for (int count = 0; count <= settings.Count - 1; count++)
                {
                    if (settings[count].PaperName.Equals(sizename))
                    {
                        widthlst.Add(settings[count]);
                    }
                }
                return widthlst.ToList();
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApiController GetPaperWidth", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetPDFCatalogattributes(int catalogId, int projectId, string type)
        {
            try
            {
                XpressCatalog objXpress = new XpressCatalog();
                var c = GetCatalogAttr(catalogId, type);

                if (projectId == 0)
                {
                    return c;
                }
                else
                {
                    TB_PROJECT projectdetails = _dbcontext.TB_PROJECT.Find(projectId);
                    var pdfDetails = projectdetails.XPRESSCATALOG_CONFIG;
                    List<PDF_PROJECT_SETTINGS> objprojectSettings = objXpress.ProjectXmlDeserializefunction(pdfDetails);
                    string[] allcustomAttribute = null;
                    string[] allsystemAttribute = null;
                    foreach (var item in objprojectSettings)
                    {
                        string customAttribute = item.CUSTOM_ATTRIBUTE;
                        string systemAttribute = item.SYSTEM_ATTRIBUTE;
                        allcustomAttribute = customAttribute.Split(',');
                        allsystemAttribute = systemAttribute.Split(',');
                    }


                    for (int i = 0; i < allcustomAttribute.Count(); i++)
                    {
                        int attrid = 0;
                        int.TryParse(allcustomAttribute[i].Trim('~'), out attrid);
                        var obj = c.FirstOrDefault(x => x.ATTRIBUTE_ID == attrid);
                        if (obj != null)
                        {
                            c.Remove(obj);
                        }
                    }
                    for (int i = 0; i < allsystemAttribute.Count(); i++)
                    {
                        var obj = c.FirstOrDefault(x => x.ATTRIBUTE_NAME == allsystemAttribute[i]);
                        if (obj != null)
                        {
                            c.Remove(obj);
                        }
                    }
                    return c;
                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApi : GetPDFCatalogattributes", objException);
                return null;
            }
        }

        public List<QS_CATALOGATTRIBUTE_Result> GetCatalogAttr(int catalogId, string type)
        {
            try
            {
                String[] categoryLevel = new String[10];
                String[] listofamilyselectedattrs = new String[2];
                String[] listmultiple1 = new String[1];
                String[] listrefrnce = new String[1];

                if (type.Equals("simple"))
                {
                    string[] categoryLevel1 = { "CATALOG_VERSION", "CATALOG_DESCRIPTION", "CATALOG_FAMILY_FILTERS", "CATALOG_PRODUCT_FILTERS" };
                    categoryLevel = categoryLevel1;
                    String[] listofamilyselectedattrs1 = { "FOOT_NOTES", "STATUS" };
                    listofamilyselectedattrs = listofamilyselectedattrs1;
                    var listmultitable = _dbcontext.TB_ATTRIBUTE_GROUP_SECTIONS.Where(x => x.CATALOG_ID == catalogId)
                        .Join(_dbcontext.TB_ATTRIBUTE_GROUP, a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { b.GROUP_ID, b.GROUP_NAME }).ToList();

                    var listreference = _dbcontext.TB_REFERENCE_SECTIONS.Where(x => x.CATALOG_ID == catalogId).Join
                        (_dbcontext.TB_REFERENCE_TABLE, a => a.TABLE_ID, b => b.TABLE_ID, (a, b) => new { b.TABLE_ID, b.TABLE_NAME }).Distinct().ToList();
                    List<QS_CATALOGATTRIBUTE_Result> objqsCatalog = categoryLevel.Select(listofselectedattr => new QS_CATALOGATTRIBUTE_Result
                    {
                        ATTRIBUTE_ID = 0,
                        ATTRIBUTE_TYPE = 0,
                        ISAvailable = 0,
                        ATTRIBUTE_NAME = listofselectedattr
                    }).ToList();

                    objqsCatalog.AddRange(listofamilyselectedattrs.Select(listofselectedattr => new QS_CATALOGATTRIBUTE_Result
                    {
                        ATTRIBUTE_ID = 0,
                        ATTRIBUTE_TYPE = 2,
                        ISAvailable = 0,
                        ATTRIBUTE_NAME = listofselectedattr
                    }));

                    foreach (var multival in listmultitable)
                    {
                        QS_CATALOGATTRIBUTE_Result objQS_CATALOGALLATTRIBUTES_Result = new QS_CATALOGATTRIBUTE_Result();
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_ID = multival.GROUP_ID;
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_TYPE = 14;
                        objQS_CATALOGALLATTRIBUTES_Result.ISAvailable = 0;
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_NAME = multival.GROUP_NAME;
                        objqsCatalog.Add(objQS_CATALOGALLATTRIBUTES_Result);
                    }
                    foreach (var referenceVal in listreference)
                    {
                        QS_CATALOGATTRIBUTE_Result objQS_CATALOGALLATTRIBUTES_Result = new QS_CATALOGATTRIBUTE_Result();
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_ID = Convert.ToInt32(referenceVal.TABLE_ID);
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_TYPE = 15;
                        objQS_CATALOGALLATTRIBUTES_Result.ISAvailable = 0;
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_NAME = referenceVal.TABLE_NAME;
                        objqsCatalog.Add(objQS_CATALOGALLATTRIBUTES_Result);
                    }
                    // List<QS_CATALOGATTRIBUTE_Result> catalogs = _dbcontext.QS_CATALOGATTRIBUTE(catalogId).ToList();

                    var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);

                    #region For Category Attributes
                    var categoryAttributes = (from categorySpecs in _dbcontext.TB_CATEGORY_SPECS
                                              join attribute in _dbcontext.TB_ATTRIBUTE on categorySpecs.ATTRIBUTE_ID equals attribute.ATTRIBUTE_ID
                                              join customerAttribute in _dbcontext.CUSTOMERATTRIBUTE on attribute.ATTRIBUTE_ID equals customerAttribute.ATTRIBUTE_ID
                                              where customerAttribute.CUSTOMER_ID == customerid.CustomerId && categorySpecs.CATALOG_ID == catalogId
                                              select new
                                              {
                                                  attribute.ATTRIBUTE_ID,
                                                  attribute.ATTRIBUTE_NAME,
                                                  attribute.ATTRIBUTE_TYPE,
                                                  attribute.PUBLISH2CDROM,
                                                  attribute.PUBLISH2ODP,
                                                  attribute.PUBLISH2PRINT,
                                                  attribute.PUBLISH2WEB
                                              }).Distinct().ToList();

                    foreach (var categoryAttribute in categoryAttributes)
                    {
                        QS_CATALOGATTRIBUTE_Result objQS_CATALOGALLATTRIBUTES_Result = new QS_CATALOGATTRIBUTE_Result();
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_ID = Convert.ToInt32(categoryAttribute.ATTRIBUTE_ID);
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_TYPE = categoryAttribute.ATTRIBUTE_TYPE;
                        objQS_CATALOGALLATTRIBUTES_Result.ISAvailable = 0;
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_NAME = categoryAttribute.ATTRIBUTE_NAME;
                        objQS_CATALOGALLATTRIBUTES_Result.DISPLAY_NAME = categoryAttribute.ATTRIBUTE_NAME;
                        objQS_CATALOGALLATTRIBUTES_Result.PUBLISH2CDROM = categoryAttribute.PUBLISH2CDROM;
                        objQS_CATALOGALLATTRIBUTES_Result.PUBLISH2ODP = categoryAttribute.PUBLISH2ODP;
                        objQS_CATALOGALLATTRIBUTES_Result.PUBLISH2PRINT = categoryAttribute.PUBLISH2PRINT;
                        objQS_CATALOGALLATTRIBUTES_Result.PUBLISH2WEB = categoryAttribute.PUBLISH2WEB;
                        objqsCatalog.Add(objQS_CATALOGALLATTRIBUTES_Result);
                    }

                    List<int> categoryAttrTypes = new List<int>();
                    categoryAttrTypes.Add(21);
                    categoryAttrTypes.Add(23);
                    categoryAttrTypes.Add(25);

                    #endregion

                    if (customerid != null)
                    {
                        List<QS_CATALOGATTRIBUTE_Result> catalogs = _dbcontext.QS_CATALOGATTRIBUTE(catalogId).Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                            .Join(_dbcontext.TB_ATTRIBUTE, ca => ca.tcp.ATTRIBUTE_ID, ta => ta.ATTRIBUTE_ID, (ca, ta) => new { ca, ta })
                                .Where(x => x.ca.tpf.CUSTOMER_ID == customerid.CustomerId && x.ta.PUBLISH2PDF == true).Select(x => x.ca.tcp).ToList();
                        catalogs = catalogs.Where(s => !categoryAttrTypes.Contains(s.ATTRIBUTE_TYPE)).ToList();
                        return !catalogs.Any() ? objqsCatalog : catalogs.Union(objqsCatalog).ToList();
                    }
                    else
                    {
                        return objqsCatalog;
                    }
                    //if (!catalogs.Any())
                    //{
                    //    return objqsCatalog;
                    //}
                    //else
                    //{
                    //    return catalogs.Union(objqsCatalog).ToList();
                    //}
                }
                else
                {

                    string[] categoryLevel1 = { "CATALOG_ID", "CATALOG_NAME", "PARENT_CATEGORY", "SHORT_DESC",
                                                  "IMAGE_FILE", "IMAGE_TYPE", "IMAGE_NAME",
                                                  //"IMAGE_NAME2", "IMAGE_FILE2", 
                                                  "IMAGE_NAME2", "IMAGE_TYPE2","SORT_ORDER", "CUSTOM_TEXT_FIELD1",
                                                  "CUSTOM_TEXT_FIELD2", "CUSTOM_TEXT_FIELD3",
                                                  "CUSTOM_NUM_FIELD1", "CUSTOM_NUM_FIELD2",
                                                  "CUSTOM_NUM_FIELD3","CATALOG_CREATED_USER","CATALOG_CREATED_DATE","CATALOG_MODIFIED_USER","CATALOG_MODIFIED_DATE",
                                              "CATEGORY_CREATED_USER","CATEGORY_CREATED_DATE","CATEGORY_MODIFIED_USER","CATEGORY_MODIFIED_DATE"};

                    categoryLevel = categoryLevel1;
                    string sqlStr = "";
                    sqlStr = "SELECT NAME FROM SYSCOLUMNS WHERE ID IN (SELECT ID FROM SYSOBJECTS  WHERE NAME='Category Attributes')";
                    _SQLString = sqlStr;
                    DataSet CategoryAttr = CreateDataSet();
                    sqlStr = "SELECT NAME FROM SYSCOLUMNS WHERE ID IN (SELECT ID FROM SYSOBJECTS  WHERE NAME='Family')";
                    _SQLString = sqlStr;
                    DataSet Family = CreateDataSet();
                    sqlStr = "SELECT NAME FROM SYSCOLUMNS WHERE ID IN (SELECT ID FROM SYSOBJECTS  WHERE NAME='Family Description')";
                    _SQLString = sqlStr;
                    DataSet FamilyDescription = CreateDataSet();
                    sqlStr = "SELECT NAME FROM SYSCOLUMNS WHERE ID IN (SELECT ID FROM SYSOBJECTS  WHERE NAME='Family Attachment')";
                    _SQLString = sqlStr;
                    DataSet FamilyAttachment = CreateDataSet();
                    sqlStr = "SELECT NAME FROM SYSCOLUMNS WHERE ID IN (SELECT ID FROM SYSOBJECTS  WHERE NAME='Product Family')";
                    _SQLString = sqlStr;
                    DataSet ProductFamily = CreateDataSet();
                    sqlStr = "SELECT NAME FROM SYSCOLUMNS WHERE ID IN (SELECT ID FROM SYSOBJECTS  WHERE NAME='Product Specification')";
                    _SQLString = sqlStr;
                    DataSet ProductSpecification = CreateDataSet();


                    //   List<QS_CATALOGATTRIBUTE_Result> objqsCatalog = categoryLevel.Select(listofselectedattr => new QS_CATALOGATTRIBUTE_Result
                    //{
                    //    ATTRIBUTE_ID = 0,
                    //    ATTRIBUTE_TYPE = 0,
                    //    ISAvailable = 0,
                    //    ATTRIBUTE_NAME = listofselectedattr
                    //}).ToList();
                    List<QS_CATALOGATTRIBUTE_Result> objqsCatalog = new List<QS_CATALOGATTRIBUTE_Result>();
                    for (int row = 1; row < CategoryAttr.Tables[0].Rows.Count; row++)
                    {
                        QS_CATALOGATTRIBUTE_Result objQS_CATALOGALLATTRIBUTES_Result = new QS_CATALOGATTRIBUTE_Result();
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_ID = 0;// Convert.ToInt32(multival.TABLE_ID);
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_TYPE = 23;
                        objQS_CATALOGALLATTRIBUTES_Result.ISAvailable = 0;
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_NAME = CategoryAttr.Tables[0].Rows[row][0].ToString();// multival.TABLE_NAME;                        
                        objqsCatalog.Add(objQS_CATALOGALLATTRIBUTES_Result);
                    }
                    for (int row = 2; row < Family.Tables[0].Rows.Count; row++)
                    {
                        QS_CATALOGATTRIBUTE_Result objQS_CATALOGALLATTRIBUTES_Result = new QS_CATALOGATTRIBUTE_Result();
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_ID = 0;// Convert.ToInt32(multival.TABLE_ID);
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_TYPE = 2;
                        objQS_CATALOGALLATTRIBUTES_Result.ISAvailable = 0;
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_NAME = Family.Tables[0].Rows[row][0].ToString();// multival.TABLE_NAME;
                        if (objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_NAME != "FAMILY_NAME")
                            objqsCatalog.Add(objQS_CATALOGALLATTRIBUTES_Result);
                    }
                    for (int row = 2; row < FamilyDescription.Tables[0].Rows.Count; row++)
                    {
                        QS_CATALOGATTRIBUTE_Result objQS_CATALOGALLATTRIBUTES_Result = new QS_CATALOGATTRIBUTE_Result();
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_ID = 0;// Convert.ToInt32(multival.TABLE_ID);
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_TYPE = 7;
                        objQS_CATALOGALLATTRIBUTES_Result.ISAvailable = 0;
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_NAME = FamilyDescription.Tables[0].Rows[row][0].ToString();// multival.TABLE_NAME;
                        objqsCatalog.Add(objQS_CATALOGALLATTRIBUTES_Result);
                    }
                    for (int row = 2; row < FamilyAttachment.Tables[0].Rows.Count; row++)
                    {
                        QS_CATALOGATTRIBUTE_Result objQS_CATALOGALLATTRIBUTES_Result = new QS_CATALOGATTRIBUTE_Result();
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_ID = 0;// Convert.ToInt32(multival.TABLE_ID);
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_TYPE = 9;
                        objQS_CATALOGALLATTRIBUTES_Result.ISAvailable = 0;
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_NAME = FamilyAttachment.Tables[0].Rows[row][0].ToString();// multival.TABLE_NAME;
                        objqsCatalog.Add(objQS_CATALOGALLATTRIBUTES_Result);
                    }
                    for (int row = 2; row < ProductFamily.Tables[0].Rows.Count; row++)
                    {
                        QS_CATALOGATTRIBUTE_Result objQS_CATALOGALLATTRIBUTES_Result = new QS_CATALOGATTRIBUTE_Result();
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_ID = 0;// Convert.ToInt32(multival.TABLE_ID);
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_TYPE = 16;
                        objQS_CATALOGALLATTRIBUTES_Result.ISAvailable = 0;
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_NAME = ProductFamily.Tables[0].Rows[row][0].ToString();// multival.TABLE_NAME;
                        if (objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_NAME != "CATALOG_ITEM_NUMBER")
                            objqsCatalog.Add(objQS_CATALOGALLATTRIBUTES_Result);
                    }

                    for (int row = 2; row < ProductSpecification.Tables[0].Rows.Count; row++)
                    {
                        QS_CATALOGATTRIBUTE_Result objQS_CATALOGALLATTRIBUTES_Result = new QS_CATALOGATTRIBUTE_Result();
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_ID = 0;// Convert.ToInt32(multival.TABLE_ID);
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_TYPE = 17;
                        objQS_CATALOGALLATTRIBUTES_Result.ISAvailable = 0;
                        objQS_CATALOGALLATTRIBUTES_Result.ATTRIBUTE_NAME = ProductSpecification.Tables[0].Rows[row][0].ToString();// multival.TABLE_NAME;
                        objqsCatalog.Add(objQS_CATALOGALLATTRIBUTES_Result);

                    }
                    return objqsCatalog.ToList();
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at XpressApiController : GetCatalogAttr", objexception);
                return null;
            }
        }
        public DataSet CreateDataSet()
        {
            DataSet dsReturn = new DataSet();
            using (
                var objSqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlDataAdapter _DBAdapter = new SqlDataAdapter(_SQLString, objSqlConnection);
                _DBAdapter.SelectCommand.CommandTimeout = 0;
                _DBAdapter.Fill(dsReturn);
                _DBAdapter.Dispose();
                return dsReturn;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetPDFSelectedobjects()
        {
            try
            {
                XpressCatalog objXpress = new XpressCatalog();
                String[] listofselectedattrs = new String[4];

                String[] listofselectedattrs1 = { };
                listofselectedattrs = listofselectedattrs1;
                var objqsCatalog = listofselectedattrs.Select(listofselectedattr => new QS_CATALOGATTRIBUTE_Result
                {
                    ATTRIBUTE_ID = 0,
                    ATTRIBUTE_TYPE = 0,
                    ISAvailable = 0,
                    ATTRIBUTE_NAME = listofselectedattr
                }).ToList();
                return objqsCatalog;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApi : GetPDFSelectedobjects", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetPDFSelectedattributes(int catalogId, int projectId, string type)
        {
            try
            {
                String[] listofselectedattrs = new String[4];
                String[] listofamilyselectedattrs = new String[4];
                String[] listofproductselectedattrs = new String[4];
                if (type.Equals("simple"))
                {
                    String[] listofselectedattrs1 = { "CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_NAME" };
                    listofselectedattrs = listofselectedattrs1;
                    String[] listofamilyselectedattrs1 = { "FAMILY_ID", "FAMILY_NAME", "SUBFAMILY_ID", "SUBFAMILY_NAME" };
                    listofamilyselectedattrs = listofamilyselectedattrs1;
                    String[] listofproductselectedattrs1 = { "PRODUCT_ID" };
                    listofproductselectedattrs = listofproductselectedattrs1;
                }
                else
                {
                    String[] listofselectedattrs1 = { "CATEGORY_ID", "CATEGORY_NAME" };
                    listofselectedattrs = listofselectedattrs1;
                    String[] listofamilyselectedattrs1 = { "FAMILY_NAME" };
                    listofamilyselectedattrs = listofamilyselectedattrs1;
                    String[] listofproductselectedattrs1 = { "CATALOG_ITEM_NUMBER" };
                    listofproductselectedattrs = listofproductselectedattrs1;
                }

                var objqsCatalog = listofselectedattrs.Select(listofselectedattr => new QS_CATALOGATTRIBUTE_Result
                {
                    ATTRIBUTE_ID = 0,
                    ATTRIBUTE_TYPE = 0,
                    ISAvailable = 0,
                    ATTRIBUTE_NAME = listofselectedattr
                }).ToList();

                objqsCatalog.AddRange(listofamilyselectedattrs.Select(listofselectedattr => new QS_CATALOGATTRIBUTE_Result
                {
                    ATTRIBUTE_ID = 0,
                    ATTRIBUTE_TYPE = 2,
                    ISAvailable = 0,
                    ATTRIBUTE_NAME = listofselectedattr
                }));

                objqsCatalog.AddRange(listofproductselectedattrs.Select(listofselectedattr => new QS_CATALOGATTRIBUTE_Result
                {
                    ATTRIBUTE_ID = 0,
                    ATTRIBUTE_TYPE = 16,
                    ISAvailable = 0,
                    ATTRIBUTE_NAME = listofselectedattr
                }));
                if (projectId == 0)
                {
                    return objqsCatalog;
                }
                else
                {
                    TB_PROJECT projectdetails = _dbcontext.TB_PROJECT.Find(projectId);
                    var pdfDetails = projectdetails.XPRESSCATALOG_CONFIG;
                    List<PDF_PROJECT_SETTINGS> objprojectSettings = objXpress.ProjectXmlDeserializefunction(pdfDetails);
                    string[] allcustomAttribute = null;
                    string[] allsystemAttribute = null;
                    foreach (var item in objprojectSettings)
                    {
                        string customAttribute = item.CUSTOM_ATTRIBUTE;
                        string systemAttribute = item.SYSTEM_ATTRIBUTE;
                        allcustomAttribute = customAttribute.Split(',');
                        allsystemAttribute = systemAttribute.Split(',');
                    }

                    var c = GetCatalogAttr(catalogId, type);
                    for (int i = 0; i < allcustomAttribute.Count(); i++)
                    {
                        int attrid = 0;
                        int.TryParse(allcustomAttribute[i].Trim('~'), out attrid);
                        var objexistattr = objqsCatalog.Join(_dbcontext.TB_ATTRIBUTE, cat => cat.ATTRIBUTE_ID, ta => ta.ATTRIBUTE_ID, (cat, ta) => new { cat, ta }).Where(x => x.ta.PUBLISH2PDF == true).Select(y => y.cat).Distinct().FirstOrDefault();
                        if (objexistattr == null)
                        {
                            var objselectedattr = c.FirstOrDefault(x => x.ATTRIBUTE_ID == attrid);
                            if (objselectedattr != null)
                            {
                                objqsCatalog.Add(objselectedattr);
                            }
                        }
                    }
                    for (int i = 0; i < allsystemAttribute.Count(); i++)
                    {
                        //var objexistattr = objqsCatalog.FirstOrDefault(x => x.ATTRIBUTE_NAME == allsystemAttribute[i]);
                        var objexistattr = objqsCatalog.Join(_dbcontext.TB_ATTRIBUTE, cat => cat.ATTRIBUTE_ID, ta => ta.ATTRIBUTE_ID, (cat, ta) => new { cat, ta }).Select(y => y.cat).Distinct().FirstOrDefault(x => x.ATTRIBUTE_NAME == allsystemAttribute[i]);
                        if (objexistattr == null)
                        {
                            var objselectedattr = c.FirstOrDefault(x => x.ATTRIBUTE_NAME == allsystemAttribute[i]);
                            if (objselectedattr != null)
                            {
                                objqsCatalog.Add(objselectedattr);
                            }
                        }
                    }
                    return objqsCatalog;
                }

            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApi : GetPDFSelectedattributes", objException);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public IList GetPDFCatalogobjects(int catalogId)
        {
            try
            {
                XpressCatalog objXpress = new XpressCatalog();
                var objects = GetCatalogObjects(catalogId);
                return objects;


            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApi : GetPDFCatalogobjects", objException);
                return null;
            }
        }
        public List<QS_CATALOGATTRIBUTE_Result> GetCatalogObjects(int catalogId)
        {
            try
            {
                CatalogID = catalogId;
                _workingCatalogID = catalogId;
                workingCatalogID = catalogId;
                string custAttrName = string.Empty;
                string selAttrname = string.Empty;
                var SelectedFieldsTree = (selectedTreeNodes[0]).Select(x => x).ToList();
                foreach (var itemm in SelectedFieldsTree)
                {
                    if (itemm["ATTRIBUTE_ID"].ToString() != "0" && itemm["ATTRIBUTE_TYPE"].ToString() != "14" && itemm["ATTRIBUTE_TYPE"].ToString() != "15")
                    {
                        custAttrName += "," + itemm["ATTRIBUTE_ID"];
                    }

                }
                foreach (var attname in SelectedFieldsTree)
                {
                    if (attname.ToString() != "" && attname.ToString() != null && attname["ATTRIBUTE_ID"].ToString() == "0")
                    {
                        selAttrname += "," + attname["ATTRIBUTE_NAME"];
                    }
                    if (selAttrname.Contains("CATEGORY_SHORT_DESC"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_SHORT_DESC", "SHORT_DESC");
                    }
                    if (selAttrname.Contains("CATEGORY_CUSTOM_NUM_FIELD1"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_CUSTOM_NUM_FIELD1", "CUSTOM_NUM_FIELD1");
                    }
                    if (selAttrname.Contains("CATEGORY_CUSTOM_NUM_FIELD2"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_CUSTOM_NUM_FIELD2", "CUSTOM_NUM_FIELD2");
                    }
                    if (selAttrname.Contains("CATEGORY_CUSTOM_NUM_FIELD3"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_CUSTOM_NUM_FIELD3", "CUSTOM_NUM_FIELD3");
                    }
                    if (selAttrname.Contains("CATEGORY_CUSTOM_TEXT_FIELD1"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_CUSTOM_TEXT_FIELD1", "CUSTOM_TEXT_FIELD1");
                    }
                    if (selAttrname.Contains("CATEGORY_CUSTOM_TEXT_FIELD2"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_CUSTOM_TEXT_FIELD2", "CUSTOM_TEXT_FIELD2");
                    }
                    if (selAttrname.Contains("CATEGORY_CUSTOM_TEXT_FIELD3"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_CUSTOM_TEXT_FIELD3", "CUSTOM_TEXT_FIELD3");
                    }
                    if (selAttrname.Contains("CATEGORY_IMAGE_FILE"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_IMAGE_FILE", "IMAGE_FILE");
                    }
                    if (selAttrname.Contains("CATEGORY_IMAGE_TYPE"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_IMAGE_TYPE", "IMAGE_TYPE");
                    }
                    if (selAttrname.Contains("CATEGORY_IMAGE_NAME"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_IMAGE_NAME", "IMAGE_NAME");
                    }
                    if (selAttrname.Contains("CATEGORY_IMAGE_FILE2"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_IMAGE_FILE2", "IMAGE_FILE2");
                    }
                    if (selAttrname.Contains("CATEGORY_IMAGE_TYPE2"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_IMAGE_TYPE2", "IMAGE_TYPE2");
                    }
                    if (selAttrname.Contains("CATEGORY_IMAGE_NAME2"))
                    {
                        selAttrname = selAttrname.Replace("CATEGORY_IMAGE_NAME2", "IMAGE_NAME2");
                    }
                }

                var selAttrName = new string[0];
                var catattName = new string[0];
                if (custAttrName != "")
                    selAttrName = custAttrName.Substring(1).Split(',');
                if (selAttrname != "")
                    catattName = selAttrname.Substring(1).Split(',');

                DataSet flatDataset = FunctionAttr(selAttrName, catattName);
                var objqsCatalogobjects = new List<QS_CATALOGATTRIBUTE_Result>();
                foreach (DataColumn item in flatDataset.Tables[0].Columns)
                {
                    var objnewqsCatalogobjects = new QS_CATALOGATTRIBUTE_Result();

                    objnewqsCatalogobjects.ATTRIBUTE_ID = 0;
                    objnewqsCatalogobjects.ATTRIBUTE_TYPE = 0;
                    objnewqsCatalogobjects.ISAvailable = 0;
                    objnewqsCatalogobjects.ATTRIBUTE_NAME = item.Caption;
                    objqsCatalogobjects.Add(objnewqsCatalogobjects);
                };
                //String[] selectedcategoryLevel = new String[10];
                //string[] selectedcategoryLevel1 = { "CATALOG_NAME", "CATALOG_ID", "CATEGORY_ID", "CATEGORY_NAME",
                //                                  "SUBCATID_L1", "SUBCATNAME_L1", "SUBCATID_L2", "SUBCATNAME_L2", "SUBCATID_L3", 
                //                                  "SUBCATNAME_L3", "SUBCATID_L4","SUBCATNAME_L4", "FAMILY_ID",
                //                                  "FAMILY_NAME", "SUBFAMILY_ID",
                //                                  "SUBFAMILY_NAME", "FAMILY_SORTORDER",
                //                                  "PRODUCT_ID","PRODUCT_SORTORDER"};
                //selectedcategoryLevel = selectedcategoryLevel1;
                //List<QS_CATALOGATTRIBUTE_Result> objqsCatalogobjects = selectedcategoryLevel.Select(listofselectedattrobj => new QS_CATALOGATTRIBUTE_Result
                //{
                //    ATTRIBUTE_ID = 0,
                //    ATTRIBUTE_TYPE = 0,
                //    ISAvailable = 0,
                //    ATTRIBUTE_NAME = listofselectedattrobj
                //}).ToList();

                return objqsCatalogobjects;
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at XpressApiController : GetCatalogObjects", objexception);
                return null;
            }
        }
        public HttpResponseMessage UpdateXpressProject(int projectId, string projectName, string category_Ids, string family_Ids, string comment, int catalogId,
            string catalogName, string type, string pagesize, string widthValue, string heightValue, string segwidth, string segheight, string leftmrgn,
            string rightmrgn, string topmrgn, string bottommrgn, string cols, string colswidth, string colsgap, string pageformat,
            string cvrpg, string tocpg, string cattoc, string gindex, string catindex, string indexcols, JArray model)
        {
            try
            {
                selectedTreeNodes = model;
                if (type == "simple")
                {
                    type = "0";
                }
                else
                {
                    type = "1";
                }
                if (pageformat == "Portrait")
                {
                    pageformat = "0";
                }
                else
                {
                    pageformat = "1";
                }
                if (cvrpg == "Yes")
                { cvrpg = "0"; }
                else { cvrpg = "1"; }
                if (tocpg == "Yes")
                { tocpg = "0"; }
                else { tocpg = "1"; }
                if (cattoc == "Yes")
                { cattoc = "0"; }
                else { cattoc = "1"; }
                if (catindex == "Yes")
                { catindex = "0"; }
                else { catindex = "1"; }
                if (gindex == "Yes")
                { gindex = "0"; }
                else { gindex = "1"; }
                typeOptionSet_Tab1 = Convert.ToInt16(type);
                ColumnNumericEditor = cols;
                ColumnWidthNumericEditor = colswidth;
                ColumnGapNumericEditor = colsgap;
                LeftNumericEditor = leftmrgn;
                RightNumericEditor = rightmrgn;
                TopNumericEditor = topmrgn;
                BottomNumericEditor = bottommrgn;
                PaperSizeCombo = pagesize;
                WidthNumericEditor = widthValue;
                HeightNumericEditor = heightValue;
                sWidthNumericEditor = segwidth;
                sHeightNumericEditor = segheight;
                pageformatOptionSet = Convert.ToInt16(pageformat);
                CoverPageOptionSet = Convert.ToInt16(cvrpg);
                TOCOptionSet = Convert.ToInt16(tocpg);
                TOC_Cat_Fam_OptionSet = Convert.ToInt16(cattoc);
                IndexPageOptionSet = Convert.ToInt16(gindex);
                Index_Cat_No_OptionSet = Convert.ToInt16(catindex);
                IndexColumns_NumericEditor = indexcols;
                xpressSimpleCatalogController xscp = new xpressSimpleCatalogController();
                xscp.simpleCatalogCall(model);
                xscp.getCurrentProjectId(projectId, projectName);
                xscp.getPageSettings(cvrpg, tocpg, cattoc, gindex, catindex, indexcols);
                if (type == "0")
                {
                    type = "Simple Catalog";
                }
                else
                {
                    type = "Hierarchical Catalog";
                }
                if (pageformat == "0")
                {
                    pageformat = "Portrait";
                }
                else
                {
                    pageformat = "Landscape";
                }
                if (cvrpg == "0")
                { cvrpg = "Yes"; }
                else { cvrpg = "No"; }
                if (tocpg == "0")
                { tocpg = "Yes"; }
                else { tocpg = "No"; }
                if (cattoc == "0")
                { cattoc = "Yes"; }
                else { cattoc = "No"; }
                if (catindex == "0")
                { catindex = "Yes"; }
                else { catindex = "No"; }
                if (gindex == "0")
                { gindex = "Yes"; }
                else { gindex = "No"; }
                XpressCatalog objXpress = new XpressCatalog();
                var projectCount = _dbcontext.TB_PROJECT.Count(x => x.PROJECT_ID == projectId);
                if (projectCount <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
                }
                TB_PROJECT projectPDF = _dbcontext.TB_PROJECT.Find(projectId);
                string xpressRules = objXpress.XpressConfig(projectId, projectName, category_Ids, family_Ids, comment, catalogId, catalogName, type
                    , pagesize, widthValue, heightValue, segwidth, segheight, leftmrgn,
            rightmrgn, topmrgn, bottommrgn, cols, colswidth, colsgap, pageformat,
            cvrpg, tocpg, cattoc, gindex, catindex, indexcols, model);
                xpressRules = xpressRules.Replace("encoding", "standalone");
                xpressRules = xpressRules.Replace("utf-16", "yes");
                projectPDF.XPRESSCATALOG_CONFIG = xpressRules;
                _dbcontext.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at XpressCatalogApiController : UpdateXpressProject", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        public HttpResponseMessage UpdateGroupByFields(int projectId, JArray model)
        {
            try
            {
                selectedGroupByTreeNodes = model;
                xpressSimpleCatalogController xscp = new xpressSimpleCatalogController();
                xscp.simpleCatalogGroupByFields(model);
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at XpressCatalogApiController : UpdateGroupByFields", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetPDFSummary(int projectId)
        {
            try
            {
                //var c = new List<QS_CATALOGATTRIBUTE_Result>();
                TB_PROJECT projectdetails = _dbcontext.TB_PROJECT.Find(projectId);
                var pdfDetails = projectdetails.XPRESSCATALOG_CONFIG;
                List<PDF_PROJECT_SETTINGS> objprojectSettings = objXpress.ProjectXmlDeserializefunction(pdfDetails);
                return objprojectSettings;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApi : GetPDFSummary", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList getTemplate()
        {

            try
            {

                string userName = User.Identity.Name;



                string path = string.Empty;
                string destinationPath;
                string filePath = string.Empty;

                var user_Name = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == userName).Select(y => y.c.Comments).FirstOrDefault().ToString();


                if (IsServerUpload.ToUpper() == "TRUE")
                {
                    destinationPath = pathDesignation + "\\Content\\ProductImages\\" + user_Name + "\\Customer Templates\\";

                    destinationPath = destinationPath.Replace("////", "\\\\");

                    destinationPath = destinationPath.Replace("/", "\\");
                }
                else
                {
                    destinationPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + user_Name + "/Customer Templates");
                }












                var objTemplateList = new List<TemplateList>();
                if (Directory.Exists(destinationPath + "/New Simple Template"))
                {
                    string[] gPdf = Directory.GetFiles(destinationPath + "/New Simple Template", "*.mrt");
                    for (int i = 0; i < gPdf.Count(); i++)
                    {
                        var objTemplateList1 = new TemplateList
                        {
                            FILENAME = gPdf[i].Replace(destinationPath + "/New Simple Template\\", ""),
                            SOURCE = "Simple",
                            USERNAME = userName,
                            MODIFIED_DATE = File.GetCreationTimeUtc(gPdf[i])
                        };
                        objTemplateList.Add(objTemplateList1);
                    }
                }
                if (Directory.Exists(destinationPath + "/New Hierarchical Template"))
                {
                    string[] gPdf = Directory.GetFiles(destinationPath + "/New Hierarchical Template", "*.mrt");
                    for (int i = 0; i < gPdf.Count(); i++)
                    {
                        var objTemplateList2 = new TemplateList
                        {
                            FILENAME = gPdf[i].Replace(destinationPath + "/New Hierarchical Template\\", ""),
                            SOURCE = "Hierarchical",
                            USERNAME = userName,
                            MODIFIED_DATE = File.GetCreationTimeUtc(gPdf[i])
                        };
                        objTemplateList.Add(objTemplateList2);
                    }

                }
                if (Directory.Exists(destinationPath + "/Newly Design Template"))
                {
                    string[] gPdf = Directory.GetFiles(destinationPath + "/Newly Design Template", "*.mrt");
                    for (int i = 0; i < gPdf.Count(); i++)
                    {
                        var objTemplateList3 = new TemplateList
                        {
                            FILENAME = gPdf[i].Replace(destinationPath + "/Newly Design Template\\", ""),
                            SOURCE = "Design",
                            USERNAME = userName,
                            MODIFIED_DATE = File.GetCreationTimeUtc(gPdf[i])
                        };
                        objTemplateList.Add(objTemplateList3);
                    }

                }
                if (Directory.Exists(destinationPath + "/Opened Template"))
                {
                    string[] gPdf = Directory.GetFiles(destinationPath + "/Opened Template", "*.mrt");
                    for (int i = 0; i < gPdf.Count(); i++)
                    {
                        var objTemplateList4 = new TemplateList
                        {
                            FILENAME = gPdf[i].Replace(destinationPath + "/Opened Template\\", ""),
                            SOURCE = "Open",
                            USERNAME = userName,
                            MODIFIED_DATE = File.GetCreationTimeUtc(gPdf[i])
                        };
                        objTemplateList.Add(objTemplateList4);
                    }

                }
                if (Directory.Exists(destinationPath + "/OpenCatalog"))
                {
                    string[] gPdf = Directory.GetFiles(destinationPath + "/OpenCatalog", "*.mdc");
                    for (int i = 0; i < gPdf.Count(); i++)
                    {
                        var objTemplateList5 = new TemplateList
                        {
                            FILENAME = gPdf[i].Replace(destinationPath + "/OpenCatalog\\", ""),
                            SOURCE = "OpenCatalog",
                            USERNAME = userName,
                            MODIFIED_DATE = File.GetCreationTimeUtc(gPdf[i])
                        };
                        objTemplateList.Add(objTemplateList5);
                    }

                }

                if (Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/Customer Templates/" + userName + "/RunTemplates")))
                {
                    string[] gPdf = Directory.GetFiles(System.Web.Hosting.HostingEnvironment.MapPath("~/Customer Templates/" + userName + "/RunTemplates"), "*.mrt");
                    for (int i = 0; i < gPdf.Count(); i++)
                    {
                        var objTemplateList6 = new TemplateList
                        {
                            FILENAME = gPdf[i].Replace(System.Web.Hosting.HostingEnvironment.MapPath("~/Customer Templates/" + userName + "/RunTemplates\\"), ""),
                            SOURCE = "Download",
                            USERNAME = userName,
                            MODIFIED_DATE = File.GetCreationTimeUtc(gPdf[i])
                        };
                        objTemplateList.Add(objTemplateList6);
                    }
                }
                // objTemplateList= objTemplateList.OrderBy(x => x.MODIFIED_DATE).ToList();
                return objTemplateList.OrderByDescending(x => x.MODIFIED_DATE).ToList();
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApiController, getTemplate", objException);
                return null;
            }
        }
        public ActionResult GetReportTemplate()
        {
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                StiReport report = new StiReport();
                report.Load(System.Web.Hosting.HostingEnvironment.MapPath("~/Views/App/XpressCatalog/Content/Reports/Report.mrt"));
                report.ReportCacheMode = StiReportCacheMode.Off;
                StiOptions.Engine.ReportCache.CachePath = "~/Views/App/XpressCatalog/Content/Temp";
                StiOptions.Engine.ImageCache.Enabled = false;
                StiOptions.Engine.ImageCache.CachePath = "~/Views/App/XpressCatalog/Content/Temp";
                report.ReportUnit = Stimulsoft.Report.StiReportUnitType.Inches;
                report.Dictionary.Load(System.Web.Hosting.HostingEnvironment.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct"));
                report.Dictionary.Synchronize();
                report.Dictionary.Databases.Clear();
                report.Dictionary.Databases.Add(new Stimulsoft.Report.Dictionary.StiSqlDatabase("CatalogStudio", connectionString));
                //  FamilyFilter(workingCatalogID, report);
                // ProductFilter(workingCatalogID, report);
                report.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
                report.Dictionary.DataSources["Category"].Parameters["CATEGORY_ID"].Value = fullCategoryIds.ToString();

                report.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
                report.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
                report.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
                report.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
                report.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
                report.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
                //  return StiMvcDesignerFx.GetReportTemplateResult(report);
                return null;
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-GetReportTemplate", exp);
                Console.WriteLine(exp.Message);
                return null;
            }
        }
        public ActionResult SaveReportTemplate()
        {
            try
            {
                StiReport report = StiMvcDesigner.GetReportObject();
                string packedReport = report.SavePackedReportToString();
                return StiMvcDesigner.SaveReportResult();
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-SaveReportTemplate", exp);
                Console.WriteLine(exp.Message);
                return null;
            }
        }
        public ActionResult SaveReportTemplaterun()
        {
            try
            {
                simpleCatalogReport = StiMvcDesigner.GetReportObject();
                string packedReport = simpleCatalogReport.SavePackedReportToString();
                simpleCatalogReport.Save(System.Web.Hosting.HostingEnvironment.MapPath("~/Views/App/XpressCatalog/Content/Reports/Report_simple.mrt"));
                return StiMvcDesigner.SaveReportResult();
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-SaveReportTemplate", exp);
                Console.WriteLine(exp.Message);
                return null;
            }
        }

        public ActionResult GetReportSnapshotPreview()
        {
            GetReportSnapshotPreviewrun();
            report.Load(System.Web.Hosting.HostingEnvironment.MapPath("~/Views/App/XpressCatalog/Content/Reports/Report.mrt"));
            report.Dictionary.Databases.Clear();
            report.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Expression = workingCatalogID.ToString();
            report.Dictionary.DataSources["Category"].Parameters["CATEGORY_ID"].Expression = fullCategoryIds.ToString();

            report.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Expression = workingCatalogID.ToString();
            report.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Expression = workingCatalogID.ToString();
            report.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Expression = workingCatalogID.ToString();
            report.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Expression = workingCatalogID.ToString();
            report.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Expression = workingCatalogID.ToString();
            report.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Expression = workingCatalogID.ToString();
            report.Dictionary.Synchronize();
            report.Save(System.Web.Hosting.HostingEnvironment.MapPath("~/Views/App/XpressCatalog/Content/Reports/Report.mrt"));
            //return StiMvcViewerFx.GetReportSnapshotResult(report);
            return null;
        }
        public ActionResult GetReportSnapshotPreviewrun()
        {
            simpleCatalogReport.Load(System.Web.Hosting.HostingEnvironment.MapPath("~/Views/App/XpressCatalog/Content/Reports/Report_simple.mrt"));
            sysAttr = "CATALOG_ID,CATALOG_NAME,CATEGORY_ID,CATEGORY_NAME,Family ID,Family Name,SubFamily ID,SubFamily Name";
            StiOptions.Engine.ImageCache.Enabled = false;
            simpleCatalogReport.IsModified = true;
            simpleCatalogReport.PreviewMode = StiPreviewMode.Standard;
            CustomFields(ref(simpleCatalogReport));
            // return StiMvcViewerFx.GetReportSnapshotResult(simpleCatalogReport);
            return null;
        }
        #region Methods
        public ActionResult HierarchicalCatalog()
        {
            StiPage coverPageTemp = null, tocPageTemp = null, indexPageTemp = null;
            bool productTable;
            DataTable AttrTable_Names = new DataTable("AttrTable_Names");
            AttrTable_Names.Columns.Add(new DataColumn("code", typeof(int)));
            AttrTable_Names.Columns.Add(new DataColumn("Name", typeof(string)));
            AttrTable_Names.Rows.Add(0, "Category");
            AttrTable_Names.Rows.Add(1, "Product Specifications");
            AttrTable_Names.Rows.Add(3, "Product Image / Attachment");
            AttrTable_Names.Rows.Add(4, "Product Price");
            AttrTable_Names.Rows.Add(7, "Family Description");
            AttrTable_Names.Rows.Add(6, "Product Key");

            try
            {
                string dsName = string.Empty;
                _pTablebool = false;
                double tempWidth = 0.0;
                StiReport newReport = new StiReport(); //new StiReport();
                newReport.ReportCacheMode = StiReportCacheMode.Auto;
                //  StiOptions.Engine.ReportCache.CachePath = _settingMembers.GetValue(SystemSettingsCollection.SettingsList.TEMPORARYPATH.ToString());
                StiOptions.Engine.ImageCache.Enabled = false;
                //  StiOptions.Engine.ImageCache.CachePath = _settingMembers.GetValue(SystemSettingsCollection.SettingsList.TEMPORARYPATH.ToString());
                //////if (System.IO.File.Exists(Application.StartupPath + @"\\Report_Config.Config") == true)
                //////{
                //////    StiConfig.Load(Application.StartupPath + "\\Report_Config.Config");
                //////}
                newReport.ReportUnit = Stimulsoft.Report.StiReportUnitType.Inches;
                newReport.IsModified = true;
                newReport.Pages.Clear();
                StiPage coverPage = new StiPage(newReport);
                coverPage.Name = "Cover";
                newReport.Pages.Add(coverPage);
                StiPage tocPage = new StiPage(newReport);
                tocPage.Name = "TOC";
                newReport.Pages.Add(tocPage);
                StiPage page1 = new StiPage(newReport);
                page1.Name = "Page1";
                newReport.Pages.Add(page1);
                StiPage indexPage = new StiPage(newReport);
                indexPage.Name = "Index";
                newReport.Pages.Add(indexPage);
                if (pageformatOptionSet == 0) //pageformatOptionSet.CheckedIndex
                {
                    newReport.Pages["Page1"].Orientation = StiPageOrientation.Portrait;
                }
                else
                {
                    newReport.Pages["Page1"].Orientation = StiPageOrientation.Landscape;
                }
                newReport.Pages["Page1"].Columns = Convert.ToInt32(ColumnNumericEditor); // ColumnNumericEditor.Value.ToString()
                newReport.Pages["Page1"].ColumnWidth = Convert.ToDouble(ColumnNumericEditor); //ColumnNumericEditor.Value.ToString()
                newReport.Pages["Page1"].ColumnGaps = Convert.ToDouble(ColumnGapNumericEditor); //ColumnGapNumericEditor.Value.ToString()
                newReport.Pages["Page1"].Margins = new Stimulsoft.Report.Components.StiMargins(Convert.ToDouble(LeftNumericEditor), Convert.ToDouble(RightNumericEditor), Convert.ToDouble(TopNumericEditor), Convert.ToDouble(BottomNumericEditor)); // keft rifht top bottom
                if (pageformatOptionSet == 0) //pageformatOptionSet.CheckedIndex
                {
                    newReport.Pages["Page1"].Width = double.Parse(WidthNumericEditor) - 0.7799999999999994; //WidthNumericEditor.Value.ToString()
                    newReport.Pages["Page1"].Height = double.Parse(HeightNumericEditor) - 0.7799999999999994; //HeightNumericEditor.Value.ToString()
                }
                else
                {
                    newReport.Pages["Page1"].Width = Convert.ToDouble(WidthNumericEditor) - 0.7799999999999994; //WidthNumericEditor.Value.ToString()
                    newReport.Pages["Page1"].Height = Convert.ToDouble(HeightNumericEditor) - 0.7799999999999994; //HeightNumericEditor.Value.ToString()
                }
                newReport.Pages["Page1"].SegmentPerWidth = Convert.ToInt32(sWidthNumericEditor); //sWidthNumericEditor.Value.ToString()
                newReport.Pages["Page1"].SegmentPerHeight = Convert.ToInt32(sHeightNumericEditor); //sHeightNumericEditor.Value.ToString()
                tempWidth = (newReport.Pages["Page1"].Width / newReport.Pages["Page1"].Columns) - newReport.Pages["Page1"].ColumnGaps;
                if (newReport.Pages["Page1"].ColumnWidth > 0)
                {
                    tempWidth = newReport.Pages["Page1"].ColumnWidth;
                }
                _connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString; // _connection.BuildConnectionString().ToString();
                // _userImagepath = _settingMembers.GetValue(SystemSettingsCollection.SettingsList.USERIMAGEPATH.ToString());
                //_workingCatalogID = 2;//Convert.ToInt32(_settingMembers.GetValue(SystemSettingsCollection.SettingsList.DEFAULTCATALOG.ToString()));
                if (System.IO.File.Exists("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct") == false)
                {
                    //  _exception.ShowCustomMessage("E045", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    ImagePathXpc();
                    newReport.Dictionary.Load("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct");
                    newReport.Dictionary.Synchronize();
                    newReport.Dictionary.Databases.Clear();
                    newReport.Dictionary.Databases.Add(new Stimulsoft.Report.Dictionary.StiSqlDatabase("CatalogStudio",
                        _connectionString));
                    ////////newReport.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value = cmboCatalog.Value.ToString();
                    ////////newReport.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value = cmboCatalog.Value.ToString();
                    ////////newReport.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value = cmboCatalog.Value.ToString();
                    ////////newReport.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value = cmboCatalog.Value.ToString();
                    ////////newReport.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value = cmboCatalog.Value.ToString();
                    ////////newReport.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value = cmboCatalog.Value.ToString();
                    ////////newReport.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value = cmboCatalog.Value.ToString();
                    var attachmentPath = ConfigurationManager.AppSettings["AttachmentPath"];
                    newReport.Dictionary.Variables["AttachmentPath"].Value = System.Web.Hosting.HostingEnvironment.MapPath(attachmentPath);
                    //newReport.Dictionary.Variables["AttachmentPath"].Value = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/ProductImages");
                    if (1 == 1) //typeOptionSet_Tab1.CheckedIndex
                    {
                        StiHierarchicalBand categoryBand = new StiHierarchicalBand();
                        StiHierarchicalBand familyBand = new StiHierarchicalBand();
                        StiDataBand productFamily = null;
                        if (1 != 0) //HeaderTextEditor.Text.Trim().Length
                        {
                            StiPageHeaderBand pageHeader = new StiPageHeaderBand();
                            pageHeader.Name = "Page_Header_Band";
                            pageHeader.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.2,
                                newReport.Pages["Page1"].Width, 0.5);
                            pageHeader.Restrictions = Stimulsoft.Report.Components.StiRestrictions.All;
                            pageHeader.Border =
                                new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None,
                                    System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4,
                                    new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                            pageHeader.Brush =
                                new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                            newReport.Pages["Page1"].Components.Add(pageHeader);
                            StiText pageHeaderText = new StiText();
                            pageHeaderText.Name = "Page_Header_Text";
                            pageHeaderText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.1,
                                newReport.Pages["Page1"].Width, 0.3);
                            pageHeaderText.Text = "HeaderTextEditor.Text"; //HeaderTextEditor.Text;
                            pageHeaderText.CanGrow = true;
                            pageHeaderText.CanShrink = true;
                            pageHeaderText.VertAlignment = StiVertAlignment.Center;
                            pageHeaderText.Border =
                                new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None,
                                    System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4,
                                    new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                            pageHeaderText.Brush =
                                new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                            pageHeaderText.Font = new System.Drawing.Font("Arial Unicode MS", 16F);
                            pageHeaderText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                            pageHeaderText.TextBrush =
                                new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.SteelBlue);
                            pageHeaderText.TextFormat =
                                new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                            pageHeaderText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true,
                                0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                            pageHeader.Components.Add(pageHeaderText);
                        }
                        if (1 != 0) //FooterTextEditor.Text.Trim().Length
                        {
                            StiPageFooterBand pageFooter = new StiPageFooterBand();
                            pageFooter.Name = "Page_Footer_Band";
                            pageFooter.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.2,
                                newReport.Pages["Page1"].Width, 0.5);
                            pageFooter.Restrictions = Stimulsoft.Report.Components.StiRestrictions.All;
                            pageFooter.Border =
                                new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None,
                                    System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4,
                                    new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                            pageFooter.Brush =
                                new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                            newReport.Pages["Page1"].Components.Add(pageFooter);
                            StiText pageFooterText = new StiText();
                            pageFooterText.Name = "Page_Footer_Text";
                            pageFooterText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.2,
                                newReport.Pages["Page1"].Width, 0.3);
                            pageFooterText.Text = ""; // FooterTextEditor.Text.Trim();
                            pageFooterText.CanGrow = true;
                            pageFooterText.CanShrink = true;
                            pageFooterText.VertAlignment = StiVertAlignment.Center;
                            pageFooterText.Border =
                                new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None,
                                    System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4,
                                    new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                            pageFooterText.Brush =
                                new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                            pageFooterText.Font = new System.Drawing.Font("Arial Unicode MS", 14F);
                            pageFooterText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                            pageFooterText.TextBrush =
                                new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                            pageFooterText.TextFormat =
                                new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                            pageFooterText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true,
                                0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                            pageFooter.Components.Add(pageFooterText);
                        }
                        categoryBand.Name = "CategoryBand";
                        categoryBand.Indent = 0;
                        categoryBand.Height = 0.5;
                        categoryBand.DataSourceName = "Category";
                        categoryBand.KeyDataColumn = "CATEGORY_ID";
                        categoryBand.MasterKeyDataColumn = "PARENT_CATEGORY";
                        categoryBand.RenderingEvent.Script =
                            "{AddAnchor(Category.CATALOG_ID+Category.CATEGORY_ID+Category.CATEGORY_NAME);}";
                        categoryBand.ParentValue = "0";
                        categoryBand.MasterComponent = null;
                        categoryBand.PrintIfDetailEmpty = true;
                        categoryBand.Sort = new System.String[] { "ASC", "SORT_ORDER" };
                        familyBand.Name = "FamilyBand";
                        familyBand.Height = 0.5;
                        familyBand.DataSourceName = "Family";
                        familyBand.DataRelationName = "Category_Family";
                        familyBand.Indent = 0;
                        familyBand.KeepGroupTogether = true;
                        familyBand.KeyDataColumn = "FAMILY_ID";
                        familyBand.MasterKeyDataColumn = "PARENT_FAMILY_ID";
                        familyBand.RenderingEvent.Script =
                            "AddAnchor(Family.CATALOG_ID+Family.CATEGORY_ID+Family.FAMILY_ID+Family.FAMILY_NAME);";
                        familyBand.ParentValue = "0";
                        familyBand.PrintIfDetailEmpty = true;
                        familyBand.Sort = new System.String[] { "ASC", "FAMILY_SORT_ORDER", "ASC", "SUBFAMILY_SORT_ORDER" };
                        categoryBand.Enabled = false;
                        familyBand.Enabled = false;
                        _leftValue = 0;
                        //for (int pNodeCount = 0; pNodeCount < SelectedFieldsTree.Nodes.Count; pNodeCount++)
                        //{
                        //    _field = 0;
                        //    for (int cNodeCount = 0; cNodeCount < SelectedFieldsTree.Nodes[pNodeCount].Nodes.Count; cNodeCount++)
                        //    {
                        //        if (groupname == "Family Attachment")
                        //        {
                        //            if (SelectedFieldsTree.Nodes[pNodeCount].Nodes[cNodeCount].Text != @"STRING_VALUE")
                        //                _field = _field + 1;
                        //        }
                        //        else
                        //        {
                        //            _field = _field + 1;
                        //        }
                        //    }
                        //    _fieldCount[pNodeCount] = _field;
                        //}
                        //for (int pNodeCount = 0; pNodeCount < SelectedFieldsTree.Nodes.Count; pNodeCount++)
                        //{
                        //    if (SelectedFieldsTree.Nodes[pNodeCount].Text == @"Family Attachment")
                        //    {
                        //        if (SelectedFieldsTree.Nodes[pNodeCount].Nodes.Count >= 3)
                        //        {
                        //            _familyAttachmentBool = true;
                        //        }
                        //        for (int cNodeCount = 0; cNodeCount < SelectedFieldsTree.Nodes[pNodeCount].Nodes.Count; cNodeCount++)
                        //        {
                        //            if (SelectedFieldsTree.Nodes[pNodeCount].Nodes[cNodeCount].Text != @"STRING_VALUE" && SelectedFieldsTree.Nodes[pNodeCount].Nodes[cNodeCount].Text != @"OBJECT_NAME")
                        //            {
                        //                faNodeCount++;
                        //            }
                        //        }
                        //    }
                        //}
                        //**************************   Converted Start   *********************************************
                        var SelectedFieldsTree = (selectedTreeNodes[0]).Select(x => x).ToList();

                        for (int pNodeCount = 0; pNodeCount < 20; pNodeCount++)
                        {
                            _field = 0;
                            foreach (var item in SelectedFieldsTree)
                            {
                                if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == pNodeCount)
                                {
                                    _field = _field + 1;
                                }

                            }
                            _fieldCount[pNodeCount] = _field;
                        }
                        //int result = _fieldCount.Count(i => i != 0);

                        for (int pNodeCount = 0; pNodeCount < 20; pNodeCount++)
                        {
                            if (_fieldCount[pNodeCount] != 0)
                            {
                                _widthValue[pNodeCount] = tempWidth / _fieldCount[pNodeCount];
                            }
                            else
                            {
                                _widthValue[pNodeCount] = 0;
                            }
                        }
                        int faNodeCount = 0;


                        foreach (var item in SelectedFieldsTree)
                        {
                            if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 7)
                            {
                                if (_fieldCount[7] > 3)
                                {
                                    _familyAttachmentBool = true;
                                }
                            }
                            ////////if (SelectedFieldsTree.Nodes[pNodeCount].Nodes[cNodeCount].Text != @"STRING_VALUE" && SelectedFieldsTree.Nodes[pNodeCount].Nodes[cNodeCount].Text != @"OBJECT_NAME")
                            ////////{
                            ////////    faNodeCount++;
                            ////////}
                        }

                        if (faNodeCount >= 1)
                        {
                            _familyAttachmentBool = true;
                        }
                        int psNodeCount = 0;

                        foreach (var item in SelectedFieldsTree)
                        {
                            DataRow dr = AttrTable_Names.Select("code = " + 16).First();
                            if (dr["Name"].ToString() == @"Product Family")
                            //SelectedFieldsTree.Nodes[pNodeCount].Text == @"Product Family"
                            {

                                if (Convert.ToString(item["ATTRIBUTE_NAME"]) != @"PRODUCT_ID")
                                {
                                    productTable = false;
                                }

                                if (_fieldCount[16] > 1)
                                {
                                    productTable = false;
                                }
                            }
                            DataRow dr1 = AttrTable_Names.Select("code = " + 17).First();
                            if (dr1["Name"].ToString() == @"Product Specification")
                            //(SelectedFieldsTree.Nodes[pNodeCount].Text == @"Product Specification")
                            {

                                if (Convert.ToString(item["ATTRIBUTE_NAME"]) != @"ATTRIBUTE_NAME" &&
                                    Convert.ToString(item["ATTRIBUTE_NAME"]) != @"STRING_VALUE" &&
                                    Convert.ToString(item["ATTRIBUTE_NAME"]) != @"NUMERIC_VALUE")
                                {
                                    psNodeCount++;
                                }

                            }
                        }
                        if (psNodeCount >= 1)
                        {
                            productTable = false;
                        }
                        if (true == true) //DisplayProductTable.Checked
                        {
                            productTable = true;
                        }
                        else
                        {
                            productTable = false;
                        }
                        DataTable pNodeTree = new DataTable("pNodeTree");
                        pNodeTree.Columns.Add("index", typeof(int));
                        pNodeTree.Columns.Add("ATTRIBUTE_TYPE", typeof(int));
                        pNodeTree.Columns.Add("ATTRIBUTE_ID", typeof(string));
                        pNodeTree.Columns.Add("ATTRIBUTE_NAME", typeof(string));
                        int treeIndex = 0;
                        foreach (var item in SelectedFieldsTree)
                        {
                            pNodeTree.Rows.Add(treeIndex, item["ATTRIBUTE_TYPE"], item["ATTRIBUTE_ID"], item["ATTRIBUTE_NAME"]);
                            treeIndex++;
                        }
                        DataTable uniquepNodeTree = pNodeTree.DefaultView.ToTable(true, "ATTRIBUTE_TYPE");
                        //**************************   Converted end   *********************************************
                        for (int pNodeCount = 0; pNodeCount < uniquepNodeTree.Rows.Count; pNodeCount++) //SelectedFieldsTree.Nodes.Count
                        {
                            _leftValue = 0;
                            //   for (int cNodeCount = 0; cNodeCount < SelectedFieldsTree.Nodes[pNodeCount].Nodes.Count; cNodeCount++)
                            //  DataTable cNodeTree = pNodeTree.Select(ATTRIBUTE_TYPE = uniquepNodeTree.DefaultView.RowFilter( , null, DataViewRowState.CurrentRows);
                            string _sqlWhere = "ATTRIBUTE_TYPE = " + pNodeCount;
                            string _sqlOrder = "ATTRIBUTE_TYPE DESC";

                            DataTable _newDataTable = pNodeTree.Select(_sqlWhere, _sqlOrder).CopyToDataTable();
                            for (int cNodeCount = 0; cNodeCount < _newDataTable.Rows.Count; cNodeCount++) // foreach (var item in SelectedFieldsTree)
                            {

                                DataRow dr1 = AttrTable_Names.Select("code = " + Convert.ToInt32(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_TYPE"])).First();
                                if (dr1["Name"].ToString() == "Category") // && categoryBand.Enabled == false) //SelectedFieldsTree.Nodes[pNodeCount].Text.ToString() == "Category"
                                {
                                    newReport.Pages["Page1"].Components.Add(categoryBand);
                                    categoryBand.Enabled = true;
                                    categoryBand.MasterComponent = null;
                                    categoryBand.DataRelationName = null;
                                    _cBand = true;
                                }
                                if (dr1["Name"].ToString() == "Family" && familyBand.Enabled == false) //SelectedFieldsTree.Nodes[pNodeCount].Text.ToString() == "Family"
                                {
                                    newReport.Pages["Page1"].Components.Add(familyBand);
                                    familyBand.Enabled = true;
                                    if (_cBand == true) familyBand.MasterComponent = categoryBand;
                                    familyBand.DataRelationName = "Category_Family";
                                    _fBand = true;
                                }
                                if (dsName != dr1["Name"].ToString())
                                {
                                    if (dr1["Name"].ToString() != "Category" && dr1["Name"].ToString() != "Family")
                                    {
                                        if (dr1["Name"].ToString() == "Product Family" && productTable == true)
                                        {
                                            StiHeaderBand pTableheaderBand = new StiHeaderBand();
                                            pTableheaderBand.Name = dr1["Name"].ToString() + "Header";
                                            pTableheaderBand.Height = 0.3;
                                            newReport.Pages["Page1"].Components.Add(pTableheaderBand);
                                            _crossDataBand = new StiCrossDataBand();
                                            _crossDataBand.Name = "Product_Specification_AttributeName_Band";
                                            _crossDataBand.DataSourceName = "Product Specification";
                                            _crossDataBand.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.1, 0, 1, 0.2);
                                            _crossDataBand.DataRelationName = "Product_Product_Specification";
                                            _crossDataBand.Sort = new System.String[] { "ASC", "COLUMN_SORT_ORDER" };
                                            pTableheaderBand.Components.Add(_crossDataBand);
                                            StiText dataTextPsa = new StiText();
                                            dataTextPsa.Name = "Product_Specification_ATTRIBUTE_NAME";
                                            dataTextPsa.CanGrow = true;
                                            dataTextPsa.CanShrink = true;
                                            dataTextPsa.HorAlignment = StiTextHorAlignment.Center;
                                            dataTextPsa.VertAlignment = StiVertAlignment.Center;
                                            dataTextPsa.DockStyle = StiDockStyle.Fill;
                                            dataTextPsa.Top = 0;
                                            dataTextPsa.Left = 0;
                                            dataTextPsa.Width = 1;
                                            dataTextPsa.Height = 0.2;
                                            dataTextPsa.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.Black, 0.5, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                            dataTextPsa.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Gray);
                                            dataTextPsa.Font = new System.Drawing.Font("Arial Unicode MS", 8F, System.Drawing.FontStyle.Bold);
                                            dataTextPsa.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                            dataTextPsa.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.White);
                                            dataTextPsa.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                            dataTextPsa.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                            _crossDataBand.Components.Add(dataTextPsa);
                                            dataTextPsa.Text = "{Product_Specification.ATTRIBUTE_NAME}";
                                            _pTablebool = true;
                                        }
                                        _dataBand = new StiDataBand();
                                        _dataBand.Name = dr1["Name"].ToString() + "_DataBand";
                                        _dataBand.Height = 0.5;
                                        _dataBand.DataSourceName = dr1["Name"].ToString();
                                        if (_crossDataBand != null)
                                        {
                                            if (_crossDataBand.Name.ToString() == "Product_Specification_AttributeName_Band" && _dataBand.DataSourceName == "Product Family")
                                                _crossDataBand.MasterComponent = _dataBand;
                                        }
                                        if (dr1["Name"].ToString() == "Family Description")
                                        {
                                            _dataBand.DataRelationName = "Family_Family_Description";
                                        }
                                        if (dr1["Name"].ToString() == "Family Attachment")
                                        {
                                            _dataBand.DataRelationName = "Family_Family_Image";
                                        }
                                        if (dr1["Name"].ToString() == "Product Family")
                                        {
                                            _dataBand.DataRelationName = "Family_Product_Family";
                                        }
                                        if (dr1["Name"].ToString() == "Product Specification")
                                        {
                                            _dataBand.DataRelationName = "Product_Product_Specification";
                                        }
                                        if (_pTablebool == true)
                                        {
                                            if (dr1["Name"].ToString() != "Product Specification")
                                            {
                                                newReport.Pages["Page1"].Components.Add(_dataBand);
                                            }
                                        }
                                        else
                                        {
                                            newReport.Pages["Page1"].Components.Add(_dataBand);
                                        }
                                        if (dr1["Name"].ToString() == "Product Family")
                                        {
                                            _dataBand.RenderingEvent.Script = "{AddAnchor(Product_Family.PRODUCT_ID+Product_Family.CATALOG_ITEM_NUMBER);}";
                                            productFamily = _dataBand;
                                        }
                                        if (dr1["Name"].ToString() != "Product Specification" && dr1["Name"].ToString() != "Customer List" && dr1["Name"].ToString() != "Campaign" && dr1["Name"].ToString() != "Campaign Details,Attribute")
                                        {
                                            if (_fBand == true)
                                                _dataBand.MasterComponent = familyBand;
                                        }
                                        else if (dr1["Name"].ToString() == "Product Specification")
                                        {
                                            _dataBand.MasterComponent = productFamily;
                                        }
                                    }
                                }
                                dsName = dr1["Name"].ToString();
                                if (dr1["Name"].ToString() == "Family Attachment") //&& SelectedFieldsTree.Nodes[pNodeCount].Nodes[cNodeCount].Text.ToString() == "STRING_VALUE")
                                {
                                    StiImage dataImage = new StiImage();
                                    dataImage.AspectRatio = true;
                                    if (_familyAttachmentBool == false)
                                    {
                                        dataImage.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.1, 0.1, 1.2, 0.9);
                                    }
                                    else
                                    {
                                        dataImage.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.1, 0.4, 1.2, 0.9);
                                    }
                                    dataImage.Name = "Family_Attachment_Image";
                                    dataImage.HorAlignment = Stimulsoft.Base.Drawing.StiHorAlignment.Center;
                                    dataImage.Stretch = true;
                                    dataImage.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.Transparent, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                    dataImage.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                    dataImage.BeforePrintEvent.Script = "if(Family_Attachment.STRING_VALUE==\"\")\n" +
                                                                        "{\n" +
                                                                        "Family_Attachment_Image.Enabled=false;\n" +
                                                                        "}\n" +
                                                                        "else\n" +
                                                                        "{\n" +
                                                                        "Family_Attachment_Image.Enabled=true;\n" +
                                                                        "string filetype=Family_Attachment.OBJECT_TYPE.ToString().Trim();\n" +

                                                                        "Family_Attachment_Image.File=Application.StartupPath + @" + "\"\\Images\\unsupportedImageformat.jpg\";\n" +
                                                                        "if(System.IO.File.Exists(AttachmentPath+Family_Attachment.STRING_VALUE)==true)\n" +
                                                                        "{\n" +
                                                                        "if(string.Compare(filetype,\"jpg\",StringComparison.OrdinalIgnoreCase)==0 || string.Compare(filetype,\"gif\",StringComparison.OrdinalIgnoreCase)==0 || string.Compare(filetype,\"jpeg\",StringComparison.OrdinalIgnoreCase)==0 || string.Compare(filetype,\"bmp\",StringComparison.OrdinalIgnoreCase)==0)\n" +
                                                                        "{\n" +
                                                                            "Family_Attachment_Image.File=AttachmentPath+Family_Attachment.STRING_VALUE;\n" +
                                                                        "}\n" +
                                                                        "}\n" +
                                                                        "}\n";
                                    if (_familyAttachmentBool == false)
                                    {
                                        if (newReport.Pages["Page1"].Columns == 1)
                                        {
                                            if (newReport.Pages["Page1"].Width >= 7)
                                            {
                                                _dataBand.Columns = 6;
                                                _dataBand.ColumnDirection = Stimulsoft.Report.Components.StiColumnDirection.DownThenAcross;
                                                _dataBand.ColumnDirection = Stimulsoft.Report.Components.StiColumnDirection.AcrossThenDown;
                                            }
                                        }
                                    }
                                    _dataBand.Height = 1.4;
                                    _dataBand.Components.Add(dataImage);
                                }
                                StiText dataText = new StiText();
                                dataText.Name = Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]); //  dataText.Name = SelectedFieldsTree.Nodes[pNodeCount].Nodes[cNodeCount].Text.ToString(); //****ERROR****
                                dataText.Text = "{" + dr1["Name"].ToString() + "." + Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]) + "}";  //****ERROR****
                                if (dataText.Name == "CATEGORY_NAME")
                                {
                                    categoryBand.BeforePrintEvent.Script = " if (Category.PARENT_CATEGORY != " + "\"0\"" + ")\n" +
                                                                                           "{ Category_CATEGORY_NAME.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Wheat);\n" +
                                                                                               "Category_CATEGORY_NAME.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);\n" +
                                                                                               "Category_CATEGORY_NAME.Font = new System.Drawing.Font(" + "\"Arial Unicode MS\"" + ", 16F, System.Drawing.FontStyle.Bold);\n" +
                                                                                           "}\n" +
                                                                                           "else\n" +
                                                                                           "{\n" +
                                                                                               "Category_CATEGORY_NAME.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.LightSalmon);\n" +
                                                                                               "Category_CATEGORY_NAME.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.White);\n" +
                                                                                               "Category_CATEGORY_NAME.Font = new System.Drawing.Font(" + "\"Arial Unicode MS\"" + ", 18F, System.Drawing.FontStyle.Bold);\n" +

                                                                                           "}\n";
                                }
                                if (dataText.Name == "FAMILY_NAME")
                                {
                                    familyBand.BeforePrintEvent.Script = "if(Family.PARENT_FAMILY_ID!=0)" +
                                                                                           "{" +
                                                                                           "Family_FAMILY_NAME.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Blue);" +
                                                                                           "Family_FAMILY_NAME.Font = new System.Drawing.Font(" + "\"Arial Unicode MS\"" + ", 12F, System.Drawing.FontStyle.Bold);" +
                                                                                           "}" +
                                                                                           "else" +
                                                                                           "{" +
                                                                                           "Family_FAMILY_NAME.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.White);" +
                                                                                           "Family_FAMILY_NAME.Font = new System.Drawing.Font(" + "\"Arial Unicode MS\"" + ", 14F, System.Drawing.FontStyle.Bold);" +
                                                                                           "}";
                                }
                                if (dr1["Name"].ToString() == "Product Family")
                                {
                                    dataText.Text = "{Product_Family." + Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]) + "}"; //****ERROR****
                                }
                                if (dr1["Name"].ToString() == "Product Specification")
                                {
                                    dataText.Text = "{Product_Specification." + Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]) + "}";  //****ERROR****
                                }
                                if (dr1["Name"].ToString() == "Family Description")
                                {
                                    dataText.Text = "{Family_Description." + Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]) + "}";  //****ERROR****
                                }
                                if (dr1["Name"].ToString() == "Family Attachment")
                                {
                                    dataText.Text = "{Family_Attachment." + Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]) + "}";  //****ERROR****
                                }
                                if (dr1["Name"].ToString() == "Customer List")
                                {
                                    dataText.Text = "{Customer_List." + Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]) + "}";  //****ERROR****
                                }
                                if (dr1["Name"].ToString() == "Campaign Details")
                                {
                                    dataText.Text = "{Campaign_Details." + Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]) + "}"; //****ERROR****
                                }
                                dataText.CanGrow = true;
                                dataText.CanShrink = true;
                                if (dataText.Text.ToString() != "{Family_Attachment.STRING_VALUE}")
                                {
                                    dataText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(_leftValue, 0.1, _widthValue[pNodeCount], 0.2);
                                    _leftValue = _leftValue + dataText.Width;
                                }
                                dataText.Name = dr1["Name"].ToString() + "_" + Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]);  //****ERROR****
                                if (dr1["Name"].ToString() == "Family Attachment")
                                {
                                    if (Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]) == "OBJECT_NAME")    //****ERROR****
                                    {
                                        dataText.HorAlignment = StiTextHorAlignment.Center;
                                    }
                                }
                                dataText.VertAlignment = StiVertAlignment.Center;
                                dataText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                dataText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                dataText.Font = new System.Drawing.Font("Arial Unicode MS", 8F);
                                dataText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                dataText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                                dataText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                dataText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                if (dr1["Name"].ToString() == "Category")
                                {
                                    if (dataText.Name == "Category_CATEGORY_NAME")
                                    {
                                        dataText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Wheat);
                                    }
                                    categoryBand.Components.Add(dataText);
                                }
                                else if (dr1["Name"].ToString() == "Family")
                                {
                                    if (dataText.Name == "Family_FAMILY_NAME")
                                    {
                                        dataText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.DimGray);
                                        dataText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.White);
                                    }
                                    familyBand.Components.Add(dataText);
                                }
                                else
                                {
                                    if (dr1["Name"].ToString() != "Family Attachment" && Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]) != "STRING_VALUE")   //****ERROR****
                                    {
                                        if (_pTablebool == false)
                                            _dataBand.Components.Add(dataText);
                                        else
                                        {
                                            if (dr1["Name"].ToString() != "Product Family")
                                            {
                                                _dataBand.Components.Add(dataText);
                                            }
                                            else
                                            {
                                                if (_fBand == true)
                                                    _dataBand.MasterComponent = familyBand;
                                                StiCrossDataBand crossDataBand1 = new StiCrossDataBand();
                                                crossDataBand1.Name = "Product_SpecificationBand";
                                                crossDataBand1.DataSourceName = "Product Specification";
                                                crossDataBand1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.1, 0, 1, 0.2);
                                                crossDataBand1.DataRelationName = "Product_Product_Specification";
                                                crossDataBand1.MasterComponent = _dataBand;
                                                crossDataBand1.Sort = new System.String[] { "ASC", "COLUMN_SORT_ORDER" };
                                                crossDataBand1.BeforePrintEvent.Script = "Product_Specification_STRING_VALUE.Enabled=true;\n" +
                                                                                        "Product_Specification_NUMERIC_VALUE.Enabled=false;\n" +
                                                                                        "Product_Image.Enabled=false; \n" +

                                                                                        "if(Product_Specification.NUMERIC_VALUE!=null && Product_Specification.NUMERIC_VALUE.ToString().Length>0)\n" +
                                                                                        "{\n" +
                                                                                          "Product_Image.Enabled=false;\n" +
                                                                                          "Product_Specification_STRING_VALUE.Enabled=false;\n" +
                                                                                          "Product_Specification_NUMERIC_VALUE.Enabled=true;\n" +
                                                                                         "}\n" +


                                                                                        "if(Product_Specification.ATTRIBUTE_TYPE==3)//Product Image\n" +
                                                                                        "{\n" +
                                                                                          "Product_Specification_STRING_VALUE.Enabled=false;\n" +
                                                                                          "Product_Specification_NUMERIC_VALUE.Enabled=false;\n" +
                                                                                          "Product_Image.Enabled=true;\n" +
                                                                                          "string extension = Product_Specification.OBJECT_TYPE;\n" +
                                                                                          "if (Product_Specification.STRING_VALUE != null && Product_Specification.STRING_VALUE.ToString().Length > 0)\n" +
                                                                                          "{\n" +
                                                                                            "if (string.Compare(extension, " + "\"jpg\"" + ", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(extension," + "\"gif\"" + ", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(extension, " + "\"jpeg\"" + ", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(extension," + "\"bmp\"" + ", StringComparison.OrdinalIgnoreCase) == 0)\n" +
                                                                                              "{\n" +
                                                                                                   "Product_Image.File = AttachmentPath + Product_Specification.STRING_VALUE;\n" +
                                                                                              "}\n" +
                                                                                              "else\n" +
                                                                                              "{\n" +
                                                                                                    "Product_Image.File =Application.StartupPath + @" + "\"\\Images\\unsupportedImageformat.jpg\";\n" +
                                                                                              "}\n" +
                                                                                          "}\n" +
                                                                                          "else\n" +
                                                                                          "{\n" +
                                                                                             "Product_Image.Enabled = false;\n" +
                                                                                             "Product_Specification_STRING_VALUE.Enabled = true;\n" +
                                                                                          "}\n" +

                                                                                        "}\n";
                                                _dataBand.Components.Add(crossDataBand1);
                                                _dataBand.Sort = new System.String[] { "ASC", "ROW_SORT_ORDER" };
                                                _dataBand.Height = 0.2;
                                                _dataBand.BeforePrintEvent.Script = "if(Line%2==0)\n" +
                                                                                   "{\n" +
                                                                                     "Product_Specification_STRING_VALUE.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.WhiteSmoke);\n" +
                                                                                    "}\n" +
                                                                                    "else\n" +
                                                                                    "{\n" +
                                                                                    "Product_Specification_STRING_VALUE.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);\n" +
                                                                                    "}\n";
                                                dataText.Name = "Product_Specification_STRING_VALUE";
                                                dataText.CanGrow = true;
                                                dataText.CanShrink = true;
                                                dataText.HorAlignment = StiTextHorAlignment.Left;
                                                dataText.VertAlignment = StiVertAlignment.Center;
                                                dataText.DockStyle = StiDockStyle.Fill;
                                                dataText.Top = 0;
                                                dataText.Left = 0;
                                                dataText.Width = 1;
                                                dataText.Height = 0.2;
                                                dataText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.DarkGray, 0.5, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                                dataText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                                dataText.Font = new System.Drawing.Font("Arial Unicode MS", 8F);
                                                dataText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                                dataText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                                                dataText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                                dataText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                                dataText.GetValueEvent.Script = "if(Product_Specification.STRING_VALUE.Length==0)\n" +
                                                                               "{\n" +
                                                                               "e.Value=" + "\"-\";" +
                                                                               "}";
                                                crossDataBand1.Components.Add(dataText);
                                                dataText.Text = "{Product_Specification.STRING_VALUE}";
                                                StiText ndataText = new StiText();
                                                ndataText.Name = "Product_Specification_NUMERIC_VALUE";
                                                ndataText.CanGrow = true;
                                                ndataText.CanShrink = true;
                                                ndataText.HorAlignment = StiTextHorAlignment.Right;
                                                ndataText.VertAlignment = StiVertAlignment.Center;
                                                ndataText.DockStyle = StiDockStyle.Fill;
                                                ndataText.Top = 0;
                                                ndataText.Left = 0;
                                                ndataText.Width = 1;
                                                ndataText.Height = 0.2;
                                                ndataText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.DarkGray, 0.5, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                                ndataText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                                ndataText.Font = new System.Drawing.Font("Arial Unicode MS", 8F);
                                                ndataText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                                ndataText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                                                ndataText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                                ndataText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                                ndataText.GetValueEvent.Script = "try\n" +
                                                                                "{\n" +
                                                                                        "string style = Product_Specification.Attribute_Relation.STYLE_FORMAT.ToString();\n" +
                                                                                        "int index = 0;\n" +
                                                                                         "index = style.IndexOf(\"[\");\n" +
                                                                                        "if(index!=-1)\n" +
                                                                                        "style = style.Substring(0, index-1);\n" +

                                                                                        "double dt = System.Convert.ToDouble(e.Value.ToString());\n" +
                                                                                        "e.Value = dt.ToString(style.Trim());\n" +

                                                                                        "if (Product_Specification.Attribute_Relation.ATTRIBUTE_DATARULE.ToString().Length > 0)\n" +
                                                                                        "{\n" +
                                                                                          " string nvalue=e.Value.ToString();\n" +
                                                                                            "System.Xml.XmlDocument xmlDOc = new System.Xml.XmlDocument();\n" +
                                                                                            "xmlDOc.LoadXml(Product_Specification.Attribute_Relation.ATTRIBUTE_DATARULE.ToString());\n" +
                                                                                            "System.Xml.XmlNode rNode = xmlDOc.DocumentElement;\n" +

                                                                                                "if (rNode.ChildNodes.Count > 0)\n" +
                                                                                                "{\n" +
                                                                                                    "for (int i = 0; i < rNode.ChildNodes.Count; i++)\n" +
                                                                                                    "{\n" +
                                                                                                         "System.Xml.XmlNode TableDataSetNode = rNode.ChildNodes[i];\n" +

                                                                                                        "if (TableDataSetNode.HasChildNodes)\n" +
                                                                                                        "{\n" +
                                                                                                            "string prefix = TableDataSetNode.ChildNodes[0].InnerText.ToString();\n" +
                                                                                                            "string sufix = TableDataSetNode.ChildNodes[1].InnerText.ToString();\n" +
                                                                                                            "string condition = TableDataSetNode.ChildNodes[2].InnerText.ToString();\n" +
                                                                                                            "string customValue = TableDataSetNode.ChildNodes[3].InnerText.ToString();\n" +
                                                                                                            "if (condition.Length != 0)\n" +
                                                                                                            "{\n" +
                                                                                                                        "if (nvalue == condition)\n" +
                                                                                                                        "{\n" +
                                                                                                                            "if (nvalue.Length == 0)\n" +
                                                                                                                                "if (condition == \"Empty\" || condition == \"Null\")\n" +
                                                                                                                                "{\n" +
                                                                                                                                    "nvalue = customValue;\n" +
                                                                                                                                "}\n" +
                                                                                                                            "if (nvalue.Length > 0)\n" +
                                                                                                                            "{\n" +
                                                                                                                                "nvalue = prefix + customValue + sufix;\n" +
                                                                                                                            "}\n" +

                                                                                                                        "}\n" +
                                                                                                                        "else\n" +
                                                                                                                        "{\n" +
                                                                                                                            "if (nvalue.Length != 0)\n" +
                                                                                                                            "{\n" +
                                                                                                                                "nvalue = prefix + nvalue + sufix;\n" +
                                                                                                                            "}\n" +
                                                                                                                            "else\n" +
                                                                                                                            "{\n" +
                                                                                                                                "nvalue = customValue;\n" +
                                                                                                                            "}\n" +
                                                                                                                        "}\n" +
                                                                                                            "}\n" +
                                                                                                            "else\n" +
                                                                                                            "{\n" +


                                                                                                                "{\n" +
                                                                                                                    "if (nvalue.Length > 0)\n" +
                                                                                                                        "nvalue = prefix + nvalue + sufix;\n" +

                                                                                                                "}\n" +
                                                                                                            "}\n" +
                                                                                                        "}\n" +
                                                                                                    "}\n" +
                                                                                                "}\n" +
                                                                                            "e.Value = nvalue;\n" +
                                                                                            "if (e.Value.ToString().Length==0)\n" +
                                                                                            "{\n" +
                                                                                                "e.Value=" + "\"-\";" +
                                                                                            "}\n" +
                                                                                        "}\n" +

                                                                                "}\n" +
                                                                                "catch (Exception) { }";
                                                crossDataBand1.Components.Add(ndataText);
                                                ndataText.Text = "{Product_Specification.NUMERIC_VALUE}";
                                                StiImage pdataImage = new StiImage();
                                                pdataImage.Name = "Product_Image";
                                                pdataImage.CanGrow = true;
                                                pdataImage.CanShrink = true;
                                                pdataImage.VertAlignment = StiVertAlignment.Center;
                                                pdataImage.DockStyle = StiDockStyle.Fill;
                                                pdataImage.Top = 0;
                                                pdataImage.Left = 0;
                                                pdataImage.Width = 1;
                                                pdataImage.Height = 0.2;
                                                pdataImage.Stretch = true;
                                                pdataImage.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.All, System.Drawing.Color.DarkGray, 0.5, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                                pdataImage.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                                crossDataBand1.Components.Add(pdataImage);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (dr1["Name"].ToString() == "Family Attachment")
                                        {
                                            if (Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]) != "STRING_VALUE")   //****ERROR****
                                                _dataBand.Components.Add(dataText);
                                        }
                                        else
                                        {
                                            if (dataText.Text.ToString() == "{Family_Description.STRING_VALUE}")
                                            {
                                                _familyDescription++;
                                            }
                                            if (_familyDescription == 1 && _dataBand.Name.ToString() == "Family Description_DataBand")
                                            {
                                                _familyDescription = 0;
                                                for (int i = 0; i < _dataBand.Components.Count; i++)
                                                {
                                                    if (_dataBand.BeforePrintEvent.Script.ToString().Length == 0 && _dataBand.Components[i].Name.ToString() == "Family Description_ATTRIBUTE_NAME")
                                                    {
                                                        _dataBand.BeforePrintEvent.Script = "Family_Description_ATTRIBUTE_NAME.Enabled = true;\n" +
                                                                                            "Family_Description_STRING_VALUE.Enabled = true;\n" +
                                                                                            "if (Family_Description.STRING_VALUE.Length== 0 || Family_Description.ATTRIBUTE_TYPE == 9)\n" +
                                                                                            "{\n" +
                                                                                               "Family_Description_ATTRIBUTE_NAME.Enabled = false;\n" +
                                                                                                "Family_Description_STRING_VALUE.Enabled = false;\n" +
                                                                                                "Family_Description_DataBand.Height = 0;\n" +
                                                                                            "}\n";
                                                    }
                                                }
                                            }
                                            _dataBand.Components.Add(dataText);
                                        }
                                    }
                                }
                                if (dr1["Name"].ToString() == "Family Attachment" && Convert.ToString(_newDataTable.Rows[cNodeCount]["ATTRIBUTE_NAME"]) == "OBJECT_NAME" && _familyAttachmentBool == false)  //****ERROR****
                                {
                                    dataText.Left = 0.1;
                                    dataText.Top = 1.1;
                                    dataText.Width = 1.2;
                                }
                            }
                        }  // main for ends here  *****************
                        StiPageFooterBand pageFooterBand1 = new Stimulsoft.Report.Components.StiPageFooterBand();
                        pageFooterBand1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, newReport.Pages["Page1"].Height - 0.98, newReport.Pages["Page1"].Width, 0.2);
                        pageFooterBand1.DockStyle = StiDockStyle.Bottom;
                        pageFooterBand1.Name = "PageFooterBand1";
                        pageFooterBand1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                        pageFooterBand1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                        newReport.Pages["Page1"].Components.Add(pageFooterBand1);
                        StiSystemText systemText1 = new Stimulsoft.Report.Components.StiSystemText();
                        systemText1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, newReport.Pages["Page1"].Width, 0.2);
                        systemText1.DockStyle = StiDockStyle.Fill;
                        systemText1.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Right;
                        systemText1.Name = "SystemText1";
                        systemText1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.Top, System.Drawing.Color.DimGray, 0.5, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                        systemText1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                        systemText1.Font = new System.Drawing.Font("Arial Unicode MS", 8F);
                        systemText1.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                        systemText1.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                        systemText1.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                        systemText1.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                        systemText1.Text = "{PageNofM}";
                        systemText1.BeforePrintEvent.Script = "if(PageNumber%2==0)\n" +
                                                            "{\n" +
                                                            "SystemText1.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Right;\n" +
                                                            "}\n" +
                                                            "else\n" +
                                                            "{\n" +
                                                            "SystemText1.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Left;\n" +
                                                            "}\n";
                        pageFooterBand1.Components.Add(systemText1);
                        newReport.Pages["Page1"].Name = "Catalog";
                        if (0 == 0) //CoverPageOptionSet.CheckedIndex
                        {
                            coverPageTemp = newReport.Pages["Cover"];
                            if (System.IO.File.Exists(Application.StartupPath + "\\Images\\PDFCatalogCover.jpg"))
                            {
                                StiImage cPageImage = new StiImage();
                                cPageImage.Name = "Cover_Image";
                                cPageImage.Top = 0;
                                cPageImage.Left = 0;
                                cPageImage.Width = coverPage.Width;
                                cPageImage.Height = coverPage.Height;
                                cPageImage.MultipleFactor = 1.22;
                                cPageImage.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.Top, System.Drawing.Color.MintCream, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                cPageImage.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                cPageImage.Image = null;
                                cPageImage.BeforePrintEvent.Script = @"Cover_Image.File = Application.StartupPath+@" + "\"\\Images\\PDFCatalogCover.jpg\"";
                                newReport.Pages["Cover"].Components.Add(cPageImage);
                            }
                            else
                            {
                                StiText coverPageText = new StiText();
                                StiText coverPageTextCatalogName = new StiText();
                                coverPageText = new Stimulsoft.Report.Components.StiText();
                                coverPageText.CanGrow = true;
                                coverPageText.CanShrink = true;
                                coverPageText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.9, 5.3, 1.59, 0.31);
                                coverPageText.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Left;
                                coverPageText.Name = "CoverPageText";
                                coverPageText.VertAlignment = Stimulsoft.Base.Drawing.StiVertAlignment.Center;
                                coverPageText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                coverPageText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                coverPageText.Font = new System.Drawing.Font("Arial Unicode MS", 14F, System.Drawing.FontStyle.Bold);
                                coverPageText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                coverPageText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                                coverPageText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                coverPageText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                coverPageTextCatalogName = new Stimulsoft.Report.Components.StiText();
                                coverPageTextCatalogName.CanGrow = true;
                                coverPageTextCatalogName.CanShrink = true;
                                coverPageTextCatalogName.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(2.5, 5.3, newReport.Pages["Cover"].Width - 2.5, 0.31);
                                coverPageTextCatalogName.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Left;
                                coverPageTextCatalogName.Name = "CoverPageTextCatalogName";
                                coverPageTextCatalogName.VertAlignment = Stimulsoft.Base.Drawing.StiVertAlignment.Center;
                                coverPageTextCatalogName.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                coverPageTextCatalogName.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                coverPageTextCatalogName.Font = new System.Drawing.Font("Arial Unicode MS", 14F, System.Drawing.FontStyle.Bold);
                                coverPageTextCatalogName.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                coverPageTextCatalogName.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                                coverPageTextCatalogName.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                coverPageTextCatalogName.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                coverPageText.Text = "Cover Page for";
                                coverPageTextCatalogName.Text = "{Category.CATALOG_NAME}";
                                newReport.Pages["Cover"].Components.Add(coverPageText);
                                newReport.Pages["Cover"].Components.Add(coverPageTextCatalogName);
                            }
                        }
                        if (0 == 0) //TOCOptionSet.CheckedIndex
                        {
                            if (0 == 0) //pageformatOptionSet.CheckedIndex
                            {
                                tocPage.Orientation = StiPageOrientation.Portrait;
                            }
                            else
                            {
                                tocPage.Orientation = StiPageOrientation.Landscape;
                            }
                            tocPage.Margins = new Stimulsoft.Report.Components.StiMargins(Convert.ToDouble(LeftNumericEditor), Convert.ToDouble(RightNumericEditor), Convert.ToDouble(TopNumericEditor), Convert.ToDouble(BottomNumericEditor));
                            if (0 == 0) //pageformatOptionSet.CheckedIndex
                            {
                                tocPage.Width = double.Parse(WidthNumericEditor) - 0.7799999999999994; //WidthNumericEditor.Value.ToString()
                                tocPage.Height = double.Parse("11.67") - 0.7799999999999994; //HeightNumericEditor.Value.ToString()
                            }
                            else
                            {
                                tocPage.Width = Convert.ToDouble(WidthNumericEditor) - 0.7799999999999994;
                                tocPage.Height = Convert.ToDouble("11.67") - 0.7799999999999994;
                            }
                            tocPage.SegmentPerWidth = Convert.ToInt32("1");
                            tocPage.SegmentPerHeight = Convert.ToInt32("1");
                            StiReportTitleBand reportTitleBand1 = new StiReportTitleBand();
                            reportTitleBand1.Name = "Report_Title_Band";
                            reportTitleBand1.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.2, tocPage.Width, 0.55);
                            reportTitleBand1.DockStyle = StiDockStyle.Top;
                            reportTitleBand1.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                            reportTitleBand1.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                            newReport.Pages["TOC"].Components.Add(reportTitleBand1);
                            StiText reportText = new StiText();
                            reportText = new Stimulsoft.Report.Components.StiText();
                            reportText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, tocPage.Width, 0.55);
                            reportText.DockStyle = StiDockStyle.Fill;
                            reportText.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Center;
                            reportText.Name = "Report_Text";
                            reportText.Text = "Contents";
                            reportText.VertAlignment = Stimulsoft.Base.Drawing.StiVertAlignment.Center;
                            reportText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                            reportText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                            reportText.Font = new System.Drawing.Font("Arial Unicode MS", 24F, System.Drawing.FontStyle.Bold);
                            reportText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                            reportText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                            reportText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                            reportText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                            reportTitleBand1.Components.Add(reportText);
                            if (true == true) // TOC_Cat_Fam_OptionSet.CheckedIndex == 0 && TOC_Cat_Fam_OptionSet.Enabled
                            {
                                StiHierarchicalBand tocCategoryBand1 = new StiHierarchicalBand();
                                StiHierarchicalBand tocFamilyBand1 = new StiHierarchicalBand();
                                tocCategoryBand1.Name = "TOC_Category_Band";
                                tocCategoryBand1.Indent = 0;
                                tocCategoryBand1.Height = 0.2;
                                tocCategoryBand1.DataSourceName = "Category";
                                tocCategoryBand1.KeyDataColumn = "CATEGORY_ID";
                                tocCategoryBand1.MasterKeyDataColumn = "PARENT_CATEGORY";
                                tocCategoryBand1.ParentValue = "0";
                                tocCategoryBand1.MasterComponent = null;
                                tocCategoryBand1.PrintIfDetailEmpty = true;
                                tocCategoryBand1.Sort = new System.String[] { "ASC", "SORT_ORDER" };
                                tocCategoryBand1.BeforePrintEvent.Script = " if (Category.PARENT_CATEGORY != " + "\"0\"" + ")\n" +
                                                                                                   "{ TOC_Category_Name.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.DarkSeaGreen);\n" +
                                                                                                       "TOC_Category_Name.Font = new System.Drawing.Font(" + "\"Arial Unicode MS\"" + ", 14F, System.Drawing.FontStyle.Bold);\n" +
                                                                                                   "}\n" +
                                                                                                   "else\n" +
                                                                                                   "{\n" +

                                                                                                       "TOC_Category_Name.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);\n" +
                                                                                                       "TOC_Category_Name.Font = new System.Drawing.Font(" + "\"Arial Unicode MS\"" + ", 18F, System.Drawing.FontStyle.Bold);\n" +

                                                                                                   "}\n";

                                tocFamilyBand1.Name = "TOC_Family_Band";
                                tocFamilyBand1.Height = 0.2;
                                tocFamilyBand1.DataSourceName = "Family";
                                tocFamilyBand1.DataRelationName = "Category_Family";
                                tocFamilyBand1.MasterComponent = tocCategoryBand1;
                                tocFamilyBand1.Indent = 0;
                                tocFamilyBand1.KeepGroupTogether = true;
                                tocFamilyBand1.KeyDataColumn = "FAMILY_ID";
                                tocFamilyBand1.MasterKeyDataColumn = "PARENT_FAMILY_ID";
                                tocFamilyBand1.ParentValue = "0";
                                tocFamilyBand1.BeforePrintEvent.Script = "if(Family.PARENT_FAMILY_ID!=0)\n" +
                                                                        "{\n" +
                                                                            "TOC_Family_Name.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.LightGreen);\n" +
                                                                        "}\n" +
                                                                         "else\n" +
                                                                         "{\n" +
                                                                            "TOC_Family_Name.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.DarkOrchid);\n" +
                                                                          "}";
                                tocFamilyBand1.PrintIfDetailEmpty = true;
                                tocFamilyBand1.Sort = new System.String[] { "ASC", "FAMILY_SORT_ORDER", "ASC", "SUBFAMILY_SORT_ORDER" };
                                newReport.Pages["TOC"].Components.Add(tocCategoryBand1);
                                newReport.Pages["TOC"].Components.Add(tocFamilyBand1);
                                StiText tocCategoryNameText = new Stimulsoft.Report.Components.StiText();
                                tocCategoryNameText.CanGrow = true;
                                tocCategoryNameText.CanShrink = true;
                                tocCategoryNameText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, tocPage.Width / 2, 0.3);
                                tocCategoryNameText.Name = "TOC_Category_Name";
                                tocCategoryNameText.Text = "{Category.CATEGORY_NAME}";
                                tocCategoryNameText.VertAlignment = Stimulsoft.Base.Drawing.StiVertAlignment.Center;
                                tocCategoryNameText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.MintCream, 0.5, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                tocCategoryNameText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                tocCategoryNameText.Font = new System.Drawing.Font("Arial Unicode MS", 14F, System.Drawing.FontStyle.Bold);
                                tocCategoryNameText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                tocCategoryNameText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.DarkBlue);
                                tocCategoryNameText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                tocCategoryNameText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                tocCategoryBand1.Components.Add(tocCategoryNameText);
                                StiText categoryPageNumberText = new Stimulsoft.Report.Components.StiText();
                                categoryPageNumberText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(tocCategoryNameText.Width, 0, tocPage.Width / 2, 0.3);
                                categoryPageNumberText.HideZeros = true;
                                categoryPageNumberText.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Right;
                                categoryPageNumberText.Name = "Category_Page_Number";
                                categoryPageNumberText.ProcessAtEnd = true;
                                StiTagExpression tagEx = new StiTagExpression();
                                tagEx.Value = "{Category.CATALOG_ID}{Category.CATEGORY_ID}{Category.CATEGORY_NAME}";
                                categoryPageNumberText.Tag = tagEx;
                                categoryPageNumberText.Text = "{GetAnchorPageNumber(sender.TagValue)}";
                                categoryPageNumberText.VertAlignment = Stimulsoft.Base.Drawing.StiVertAlignment.Center;
                                categoryPageNumberText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.MintCream, 0.5, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Silver));
                                categoryPageNumberText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                categoryPageNumberText.Font = new System.Drawing.Font("Arial Unicode MS", 10F);
                                categoryPageNumberText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                categoryPageNumberText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                                categoryPageNumberText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                categoryPageNumberText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                tocCategoryBand1.Components.Add(categoryPageNumberText);
                                StiText tocFamilyNameText = new Stimulsoft.Report.Components.StiText();
                                tocFamilyNameText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.1, 0, tocPage.Width / 2, 0.2);
                                tocFamilyNameText.Name = "TOC_Family_Name";
                                tocFamilyNameText.Text = "{Family.FAMILY_NAME}";
                                tocFamilyNameText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.Top, System.Drawing.Color.MintCream, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                tocFamilyNameText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                tocFamilyNameText.Font = new System.Drawing.Font("Arial Unicode MS", 10F);
                                tocFamilyNameText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                tocFamilyNameText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.SteelBlue);
                                tocFamilyNameText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                tocFamilyNameText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                tocFamilyBand1.Components.Add(tocFamilyNameText);
                                StiText familyPageNumberText = new Stimulsoft.Report.Components.StiText();
                                familyPageNumberText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(tocFamilyNameText.Width, 0, tocPage.Width / 2, 0.2);
                                familyPageNumberText.HideZeros = true;
                                familyPageNumberText.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Right;
                                familyPageNumberText.Name = "Family_Page_Number";
                                familyPageNumberText.ProcessAtEnd = true;
                                familyPageNumberText.Text = "{GetAnchorPageNumber(sender.TagValue)}";
                                StiTagExpression tagEx1 = new StiTagExpression();
                                tagEx1.Value = "{Family.CATALOG_ID}{Family.CATEGORY_ID}{Family.FAMILY_ID}{Family.FAMILY_NAME}";
                                familyPageNumberText.Tag = tagEx1;
                                familyPageNumberText.VertAlignment = Stimulsoft.Base.Drawing.StiVertAlignment.Center;
                                familyPageNumberText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.LightGray, 3, Stimulsoft.Base.Drawing.StiPenStyle.Dot, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Silver));
                                familyPageNumberText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                familyPageNumberText.Font = new System.Drawing.Font("Arial Unicode MS", 10F);
                                familyPageNumberText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                familyPageNumberText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                                familyPageNumberText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                familyPageNumberText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                tocFamilyBand1.Components.Add(familyPageNumberText);
                            }
                            tocPageTemp = tocPage;
                        }
                        if (0 == 0) //IndexPageOptionSet.CheckedIndex
                        {
                            if (0 == 0) //pageformatOptionSet.CheckedIndex
                            {
                                indexPage.Orientation = StiPageOrientation.Portrait;
                            }
                            else
                            {
                                indexPage.Orientation = StiPageOrientation.Landscape;
                            }
                            indexPage.Margins = new Stimulsoft.Report.Components.StiMargins(Convert.ToDouble(LeftNumericEditor), Convert.ToDouble(RightNumericEditor), Convert.ToDouble(TopNumericEditor), Convert.ToDouble(BottomNumericEditor));
                            if (0 == 0) //pageformatOptionSet.CheckedIndex
                            {
                                indexPage.Width = double.Parse(WidthNumericEditor) - 0.7799999999999994; //WidthNumericEditor.Value.ToString()
                                indexPage.Height = double.Parse(HeightNumericEditor) - 0.7799999999999994; //HeightNumericEditor.Value.ToString()
                            }
                            else
                            {
                                indexPage.Width = Convert.ToDouble(WidthNumericEditor) - 0.7799999999999994;
                                indexPage.Height = Convert.ToDouble(HeightNumericEditor) - 0.7799999999999994;
                            }
                            indexPage.SegmentPerWidth = Convert.ToInt32("1");
                            indexPage.SegmentPerHeight = Convert.ToInt32("1");
                            indexPage.Columns = Convert.ToInt32("1");
                            StiPageHeaderBand pageHeaderBand = new Stimulsoft.Report.Components.StiPageHeaderBand();
                            pageHeaderBand.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0.2, 7.5, 0.5);
                            pageHeaderBand.DockStyle = StiDockStyle.Top;
                            pageHeaderBand.Name = "Index_Header_Band";
                            pageHeaderBand.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                            pageHeaderBand.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                            newReport.Pages["Index"].Components.Add(pageHeaderBand);
                            StiText indexText = new Stimulsoft.Report.Components.StiText();
                            indexText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0, 0, indexPage.Width, 0.3);
                            indexText.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Center;
                            indexText.Name = "Index_Text";
                            indexText.Text = "Index";
                            indexText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.Black, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                            indexText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                            indexText.Font = new System.Drawing.Font("Arial Unicode MS", 18F, System.Drawing.FontStyle.Bold);
                            indexText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                            indexText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                            indexText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                            indexText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                            pageHeaderBand.Components.Add(indexText);
                            if (true == true) //Index_Cat_No_OptionSet.CheckedIndex == 0 && Index_Cat_No_OptionSet.Enabled
                            {
                                StiHierarchicalBand indexCategoryBand1 = new StiHierarchicalBand();
                                StiHierarchicalBand indexFamilyBand1 = new StiHierarchicalBand();
                                indexCategoryBand1.Name = "Index_Category_Band";
                                indexCategoryBand1.Indent = 0;
                                indexCategoryBand1.Height = 0;
                                indexCategoryBand1.DataSourceName = "Category";
                                indexCategoryBand1.KeyDataColumn = "CATEGORY_ID";
                                indexCategoryBand1.MasterKeyDataColumn = "PARENT_CATEGORY";
                                indexCategoryBand1.ParentValue = "0";
                                indexCategoryBand1.MasterComponent = null;
                                indexCategoryBand1.PrintIfDetailEmpty = true;
                                indexCategoryBand1.Sort = new System.String[] { "ASC", "SORT_ORDER" };
                                indexFamilyBand1.Name = "Index_Family_Band";
                                indexFamilyBand1.Height = 0;
                                indexFamilyBand1.DataSourceName = "Family";
                                indexFamilyBand1.DataRelationName = "Category_Family";
                                indexFamilyBand1.MasterComponent = indexCategoryBand1;
                                indexFamilyBand1.Indent = 0;
                                indexFamilyBand1.KeepGroupTogether = true;
                                indexFamilyBand1.KeyDataColumn = "FAMILY_ID";
                                indexFamilyBand1.MasterKeyDataColumn = "PARENT_FAMILY_ID";
                                indexFamilyBand1.ParentValue = "0";
                                indexFamilyBand1.PrintIfDetailEmpty = true;
                                indexFamilyBand1.Sort = new System.String[] { "ASC", "FAMILY_SORT_ORDER", "ASC", "SUBFAMILY_SORT_ORDER" };
                                newReport.Pages["Index"].Components.Add(indexCategoryBand1);
                                newReport.Pages["Index"].Components.Add(indexFamilyBand1);
                                StiDataBand indexProdFamily = new StiDataBand();
                                indexProdFamily.Name = "Index_Product_Family_Band";
                                indexProdFamily.Height = 0.2;
                                indexProdFamily.DataSourceName = "Product Family";
                                indexProdFamily.DataRelationName = "Family_Product_Family";
                                indexProdFamily.MasterComponent = indexFamilyBand1;
                                indexProdFamily.KeepGroupTogether = true;
                                indexProdFamily.PrintIfDetailEmpty = true;
                                indexProdFamily.Sort = new System.String[] { "ASC", "ROW_SORT_ORDER" };
                                newReport.Pages["Index"].Components.Add(indexProdFamily);
                                StiText indexItemText = new Stimulsoft.Report.Components.StiText();
                                indexItemText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(0.1, 0, (newReport.Pages["Index"].Width / indexPage.Columns) / 2, 0.2);
                                indexItemText.Name = "Index_Catalog_Item_No_Text";
                                indexItemText.CanGrow = true;
                                indexItemText.CanShrink = true;
                                indexItemText.Text = "{Product_Family.CATALOG_ITEM_NUMBER}";
                                indexItemText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.Top, System.Drawing.Color.MintCream, 1, Stimulsoft.Base.Drawing.StiPenStyle.Solid, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black));
                                indexItemText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                indexItemText.Font = new System.Drawing.Font("Arial Unicode MS", 10F);
                                indexItemText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                indexItemText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.SteelBlue);
                                indexItemText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                indexItemText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                indexProdFamily.Components.Add(indexItemText);
                                StiText indexPageNumberText = new Stimulsoft.Report.Components.StiText();
                                indexPageNumberText.ClientRectangle = new Stimulsoft.Base.Drawing.RectangleD(indexItemText.Width, 0, indexItemText.Width, 0.2);
                                indexPageNumberText.HideZeros = true;
                                indexPageNumberText.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Right;
                                indexPageNumberText.Name = "Index_Page_Number";
                                indexPageNumberText.ProcessAtEnd = true;
                                indexPageNumberText.Text = "{GetAnchorPageNumber(sender.TagValue)}";
                                StiTagExpression tagEx2 = new StiTagExpression();
                                tagEx2.Value = "{Product_Family.PRODUCT_ID}{Product_Family.CATALOG_ITEM_NUMBER}";
                                indexPageNumberText.Tag = tagEx2;
                                indexPageNumberText.VertAlignment = Stimulsoft.Base.Drawing.StiVertAlignment.Center;
                                indexPageNumberText.Border = new Stimulsoft.Base.Drawing.StiBorder(Stimulsoft.Base.Drawing.StiBorderSides.None, System.Drawing.Color.LightGray, 3, Stimulsoft.Base.Drawing.StiPenStyle.Dot, false, 4, new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Silver));
                                indexPageNumberText.Brush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Transparent);
                                indexPageNumberText.Font = new System.Drawing.Font("Arial Unicode MS", 10F);
                                indexPageNumberText.Margins = new Stimulsoft.Report.Components.StiMargins(0, 0, 0, 0);
                                indexPageNumberText.TextBrush = new Stimulsoft.Base.Drawing.StiSolidBrush(System.Drawing.Color.Black);
                                indexPageNumberText.TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService();
                                indexPageNumberText.TextOptions = new Stimulsoft.Base.Drawing.StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None, System.Drawing.StringTrimming.None);
                                indexProdFamily.Components.Add(indexPageNumberText);
                            }
                            indexPageTemp = indexPage;
                        }
                        StiPagesCollection collection = newReport.Pages;
                        if (coverPageTemp == null)
                        {
                            for (int n = 0; n < collection.Count; n++)
                            {
                                if (collection[n].Name == "Cover")
                                {
                                    newReport.Pages.RemoveAt(n);
                                }
                            }
                        }
                        if (tocPageTemp == null)
                        {
                            for (int n = 0; n < collection.Count; n++)
                            {
                                if (collection[n].Name == "TOC")
                                {
                                    newReport.Pages.RemoveAt(n);
                                }
                            }
                        }
                        if (indexPageTemp == null)
                        {
                            for (int n = 0; n < collection.Count; n++)
                            {
                                if (collection[n].Name == "Index")
                                {
                                    newReport.Pages.RemoveAt(n);
                                }
                            }
                        }
                        FamilyFilter(Convert.ToInt32(CatalogID), newReport);
                        ProductFilter(Convert.ToInt32(CatalogID), newReport);
                        //newReport.Design(this.Owner);
                    }
                }
                //new return StiMvcDesignerFx.GetReportTemplateResult(newReport);
                return null;
                //    }
                //}
                //return null;
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at XpressCatalogApiController : HierarchicalCatalog", objexception);
                return null;
            }
        }
        private void FamilyFilter(int catalogID, StiReport stiReport)
        {
            Boolean dsFamily = false;
            for (int dsCount = 0; dsCount < stiReport.Dictionary.DataSources.Count; dsCount++)
            {
                if (stiReport.Dictionary.DataSources[dsCount].Name == "Family")
                {
                    dsFamily = true;
                    break;
                }
            }
            if (dsFamily)
            {
                var sqLstring = new StringBuilder();
                var ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);

                _SQLString = " SELECT FAMILY_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + catalogID + " ";

                DataSet oDsFamilyFilter = CreateDataSet();
                if (oDsFamilyFilter.Tables[0].Rows.Count > 0 && oDsFamilyFilter.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                {
                    string sFamilyFilter = oDsFamilyFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(sFamilyFilter);
                    XmlNode rNode = xmlDOc.DocumentElement;

                    if (rNode != null && rNode.ChildNodes.Count > 0)
                    {
                        for (int i = 0; i < rNode.ChildNodes.Count; i++)
                        {
                            XmlNode tableDataSetNode = rNode.ChildNodes[i];

                            if (tableDataSetNode.HasChildNodes)
                            {
                                if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                                {
                                    tableDataSetNode.ChildNodes[2].InnerText = "=";
                                }
                                if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                                {
                                    tableDataSetNode.ChildNodes[0].InnerText = "0";
                                }
                                string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                                if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                {
                                    sqLstring.Append("SELECT DISTINCT FAMILY_ID FROM [FAMILY DESCRIPTION](" + catalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                }
                                else
                                {
                                    sqLstring.Append("SELECT DISTINCT FAMILY_ID FROM [FAMILY DESCRIPTION](" + catalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                }
                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                            {

                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                            {
                                sqLstring.Append(" INTERSECT \n");
                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                            {
                                sqLstring.Append(" UNION \n");
                            }
                        }
                    }
                }
                string familyFiltersql = sqLstring.ToString();

                if (familyFiltersql.Length > 0)
                {
                    string s = "\nWHERE CATALOG_ID=" + catalogID + " AND FAMILY_ID IN\n" +
                          "(\n";
                    familyFiltersql = s + familyFiltersql + "\n)";
                    var tableSource = stiReport.DataSources["Family"] as StiSqlSource;
                    if (tableSource != null)
                    {

                        if (tableSource.SqlCommand.Contains("@") || tableSource.SqlCommand.Contains("=") || tableSource.SqlCommand.Contains("where"))
                        {
                            tableSource.SqlCommand = tableSource.SqlCommand + " where family_id in( Select family_id from family(" + CatalogID + ") " + familyFiltersql + ")";
                        }
                        else
                        {
                            tableSource.SqlCommand = "Select * from family(" + CatalogID + ") " + familyFiltersql;
                        }
                    }
                }
            }
        }
        private void ProductFilter(int catalogID, StiReport stiReport)
        {
            Boolean dsPFamily = false;

            for (int dsCount = 0; dsCount < stiReport.Dictionary.DataSources.Count; dsCount++)
            {
                if (stiReport.Dictionary.DataSources[dsCount].Name == "Product Family")
                {
                    dsPFamily = true;
                    break;
                }
            }

            if (dsPFamily)
            {
                var sqLstring = new StringBuilder();
                var ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                _SQLString = " SELECT PRODUCT_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + catalogID + " ";
                DataSet oDsProductFilter = CreateDataSet();
                if (oDsProductFilter.Tables[0].Rows.Count > 0 && oDsProductFilter.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                {
                    string sProductFilter = oDsProductFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(sProductFilter);
                    XmlNode rNode = xmlDOc.DocumentElement;

                    if (rNode != null && rNode.ChildNodes.Count > 0)
                    {
                        for (int i = 0; i < rNode.ChildNodes.Count; i++)
                        {
                            XmlNode tableDataSetNode = rNode.ChildNodes[i];

                            if (tableDataSetNode.HasChildNodes)
                            {
                                if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                                {
                                    tableDataSetNode.ChildNodes[2].InnerText = "=";
                                }
                                if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                                {
                                    tableDataSetNode.ChildNodes[0].InnerText = "0";
                                }
                                string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                                _SQLString = " SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE  ATTRIBUTE_ID = " + Convert.ToInt32(tableDataSetNode.ChildNodes[0].InnerText) + " ";
                                DataSet attribuetypeDs = CreateDataSet();
                                if (attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("TEX") || attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("DATE"))
                                {

                                    if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                    {
                                        sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                    }
                                    else
                                    {
                                        sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogID + ") WHERE (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                    }
                                }
                                else if (attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("DECI") || attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("NUM"))
                                {
                                    if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                    {
                                        sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogID + ") WHERE  (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                    }
                                    else
                                    {
                                        sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogID + ") WHERE  (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                    }
                                }
                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                            {

                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                            {
                                sqLstring.Append(" INTERSECT \n");
                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                            {
                                sqLstring.Append(" UNION \n");
                            }
                        }
                    }
                }
                string productFiltersql = sqLstring.ToString();

                if (productFiltersql.Length > 0)
                {
                    string s = "\nWHERE CATALOG_ID=" + catalogID + " AND PRODUCT_ID IN\n" +
                          "(\n";
                    productFiltersql = s + productFiltersql + "\n)";
                    var tableSource = stiReport.DataSources["Product Family"] as StiSqlSource;
                    if (tableSource != null)
                    {
                        if ((tableSource.SqlCommand.Contains("order by") != true) && (tableSource.SqlCommand.Contains("@") || tableSource.SqlCommand.Contains("=") || tableSource.SqlCommand.Contains("where")))
                        {
                            tableSource.SqlCommand = tableSource.SqlCommand + " where product_id in( Select product_id from [Product Family](" + CatalogID + ") " + productFiltersql + ")";
                        }
                        else
                        {
                            tableSource.SqlCommand = "Select * from [Product Family](" + CatalogID + ") " + productFiltersql;
                        }
                    }
                }
            }
        }
        public StiReport SimpleCatalog(ref StiReport simpleReport)
        {
            return simpleReport;
        }
        public ActionResult SimpleCatalog()
        {



            CatalogID = 2;
            sysAttr = "CATALOG_ID,CATALOG_NAME,CATEGORY_ID,CATEGORY_NAME,Family ID,Family Name,SubFamily ID,SubFamily Name";
            ////  var simpleCatalogReport = new StiReport { ReportCacheMode = StiReportCacheMode.Auto };
            /////   StiOptions.Engine.ReportCache.CachePath = _settingMembers.GetValue(SystemSettingsCollection.SettingsList.TEMPORARYPATH.ToString());
            StiOptions.Engine.ImageCache.Enabled = false;
            //////  StiOptions.Engine.ImageCache.CachePath = _settingMembers.GetValue(SystemSettingsCollection.SettingsList.TEMPORARYPATH.ToString());
            //////if (File.Exists(Application.StartupPath + @"\\Report_Config.Config"))
            //////{
            //////    StiConfig.Load(Application.StartupPath + "\\Report_Config.Config");
            //////}
            simpleCatalogReport.IsModified = true;
            simpleCatalogReport.PreviewMode = StiPreviewMode.Standard;
            CustomFields(ref(simpleCatalogReport));
            // simpleCatalogReport.Save(Server.MapPath("~/Views/App/XpressCatalog/Content/Reports/Report_simple.mrt"));

            //NEw return StiMvcDesigner.GetReportTemplateResult(simpleCatalogReport, httpContext);
            return null;
        }


        public void SimpleCatalogReportDesign(StiReport newReport, string[] selectedFields)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
            double textWidth = 0;
            double[] leftValue = { 0 };
            newReport.Pages.Clear();
            var coverPage = new StiPage(newReport) { Name = "Cover" };
            newReport.Pages.Add(coverPage);
            var tocPage = new StiPage(newReport) { Name = "TOC" };
            newReport.Pages.Add(tocPage);
            var page1 = new StiPage(newReport) { Name = "Page1" };
            newReport.Pages.Add(page1);
            var indexPage = new StiPage(newReport) { Name = "Index" };
            newReport.Pages.Add(indexPage);
            newReport.Pages["Page1"].Components.Clear();
            newReport.ReportUnit = StiReportUnitType.Inches;
            newReport.Pages["Page1"].Orientation = pageformatOptionSet == 0 ? StiPageOrientation.Portrait : StiPageOrientation.Landscape;
            newReport.Pages["Page1"].Columns = Convert.ToInt32(ColumnNumericEditor); ///// Convert.ToInt32(ColumnNumericEditor.Value.ToString());
            newReport.Pages["Page1"].ColumnWidth = Convert.ToDouble(ColumnWidthNumericEditor);
            newReport.Pages["Page1"].ColumnGaps = Convert.ToDouble(ColumnGapNumericEditor);
            newReport.Pages["Page1"].Margins = new StiMargins(Convert.ToDouble(LeftNumericEditor), Convert.ToDouble(RightNumericEditor), Convert.ToDouble(TopNumericEditor), Convert.ToDouble(BottomNumericEditor));
            if (pageformatOptionSet == 0)
            {
                newReport.Pages["Page1"].Width = double.Parse(WidthNumericEditor) - 0.7799999999999994;
                newReport.Pages["Page1"].Height = double.Parse(HeightNumericEditor) - 0.7799999999999994;
            }
            else
            {
                newReport.Pages["Page1"].Width = Convert.ToDouble(WidthNumericEditor) - 0.7799999999999994;
                newReport.Pages["Page1"].Height = Convert.ToDouble(HeightNumericEditor) - 0.7799999999999994;
            }
            newReport.Pages["Page1"].SegmentPerWidth = Convert.ToInt32(sWidthNumericEditor);
            newReport.Pages["Page1"].SegmentPerHeight = Convert.ToInt32(sHeightNumericEditor);
            double tempWidth = (newReport.Pages["Page1"].Width / newReport.Pages["Page1"].Columns) - newReport.Pages["Page1"].ColumnGaps;
            if (newReport.Pages["Page1"].ColumnWidth > 0)
            {
                tempWidth = newReport.Pages["Page1"].ColumnWidth;
            }
            if (selectedFields.Length > 0)
            {
                textWidth = tempWidth / selectedFields.Length;
                if (selectedFields.Any(t => t == "Product ID"))
                {
                    textWidth = tempWidth / (selectedFields.Length - 1);
                }
            }
            StiPage coverPageTemp = null, tocPageTemp = null, indexPageTemp = null;
            string custAttrName = string.Empty;
            foreach (var item in selectedGroupByTreeNodes)
            {

                TB_ATTRIBUTE str_attribute_id = _dbcontext.TB_ATTRIBUTE.Find(item["ATTRIBUTE_NAME"]);
                custAttrName += "," + str_attribute_id.ATTRIBUTE_ID;


            }
            //if (0 == 0)//with groupheader
            //{
            //    //if (GroupSelectedFieldsTree.Nodes.Count > 0 && GroupSelectedFieldsTree.Nodes[0].Nodes.Count > 0)
            //    //{
            //    //for (int fieldCount = 0; fieldCount < GroupSelectedFieldsTree.Nodes[0].Nodes.Count; fieldCount++)
            //    //{
            //    int fieldCount = 0;
            //    foreach (var item in selectedGroupByTreeNodes)
            //    {

            //        var groupHeaderBand = new StiGroupHeaderBand
            //        {
            //            ClientRectangle = new RectangleD(0, 0, 0, 0.1),
            //            DockStyle = StiDockStyle.Top,
            //            Name = "GroupHeaderBand" + fieldCount,
            //            Border =
            //                new StiBorder(StiBorderSides.None, System.Drawing.Color.Black, 1, StiPenStyle.Solid,
            //                              false, 4, new StiSolidBrush(System.Drawing.Color.Black)),
            //            Brush = new StiSolidBrush(System.Drawing.Color.Transparent),
            //            Condition =
            //            {
            //                Value =
            //                    "{Catalog_Objects." +
            //                    ReplaceCharacters(Convert.ToString(item["ATTRIBUTE_NAME"])) + "}" //GroupSelectedFieldsTree.Nodes[0].Nodes[fieldCount].Text
            //            }
            //        };
            //        newReport.Pages["Page1"].Components.Add(groupHeaderBand);
            //        fieldCount ++;
            //    }
            //    //}
            //}
            var dataBand = new StiDataBand
            {
                Name = "Catalog_Objects_DataBand",
                Height = 0.5,
                DataSourceName = "Catalog Objects"
            };
            newReport.Pages["Page1"].Components.Add(dataBand);
            foreach (var textBox in from t in selectedFields
                                    where t != "Product ID"
                                    select new StiText
                                    {
                                        Text = "{Catalog_Objects" + "." + ReplaceCharacters(t) + "}",
                                        Name = "Catalog_Objects" + "_" + ReplaceCharacters(t),
                                        ClientRectangle = new RectangleD(leftValue[0], 0.1, textWidth, 0.2),
                                        CanGrow = true,
                                        CanShrink = true,
                                        WordWrap = true,
                                        Border =
                                            new StiBorder(StiBorderSides.Top, System.Drawing.Color.MintCream, 1, StiPenStyle.Solid,
                                                          false, 4, new StiSolidBrush(System.Drawing.Color.Black)),
                                        Brush = new StiSolidBrush(System.Drawing.Color.Transparent),
                                        Font = new System.Drawing.Font("Arial Unicode MS", 10F),
                                        Margins = new StiMargins(0, 0, 0, 0),
                                        TextBrush = new StiSolidBrush(System.Drawing.Color.Black),
                                        TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService(),
                                        TextOptions =
                                            new StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None,
                                                               System.Drawing.StringTrimming.None)
                                    })
            {
                leftValue[0] = leftValue[0] + textBox.Width;
                dataBand.Components.Add(textBox);
            }
            foreach (string t in selectedFields.Where(t => 0 == 0 &&
                                                           (
                                                               t.ToLower().Replace(" ", "_").Replace(".", "_").Contains("cat#") ||
                                                               t.ToLower().Replace(" ", "_").Replace(".", "_").Contains("cat_no") ||
                                                               t.ToLower().Replace(" ", "_").Replace(".", "_").Contains("catalog_item_no") ||
                                                               t.ToLower().Replace(" ", "_").Replace(".", "_").Contains("cat_item_no") ||
                                                               t.ToLower().Replace(" ", "_").Replace(".", "_").Contains("catalog_item_number") ||
                                                               t.ToLower().Replace(" ", "_").Replace(".", "_").Contains("catalogitemno") ||
                                                               t.ToLower().Replace(" ", "_").Replace(".", "_").Contains("item_no")
                                                           )))
            {
                dataBand.RenderingEvent.Script = "AddAnchor(Catalog_Objects." + ReplaceCharacters(t) + ")";
            }
            //if (GroupOptionSet.CheckedIndex == 0)//with groupfooter
            //{
            //    if (GroupSelectedFieldsTree.Nodes.Count > 0 && GroupSelectedFieldsTree.Nodes[0].Nodes.Count > 0)
            //    {
            //        for (int fieldCount = 0; fieldCount < GroupSelectedFieldsTree.Nodes[0].Nodes.Count; fieldCount++)
            //        {
            //            var groupFooterBand = new StiGroupFooterBand
            //            {
            //                ClientRectangle = new RectangleD(0, 0, 0, 0.1),
            //                DockStyle = StiDockStyle.Top,
            //                Name = "GroupFooterBand" + fieldCount,
            //                Border =
            //                    new StiBorder(StiBorderSides.None, System.Drawing.Color.Black, 1,
            //                                  StiPenStyle.Solid, false, 4, new StiSolidBrush(System.Drawing.Color.Black)),
            //                Brush = new StiSolidBrush(System.Drawing.Color.Transparent)
            //            };
            //            newReport.Pages["Page1"].Components.Add(groupFooterBand);
            //        }
            //    }
            //}
            //PageNumber
            var pageFooterBand1 = new StiPageFooterBand
            {
                ClientRectangle =
                    new RectangleD(0, newReport.Pages["Page1"].Height - 0.98, newReport.Pages["Page1"].Width, 0.2),
                DockStyle = StiDockStyle.Bottom,
                Name = "PageFooterBand1",
                Border =
                    new StiBorder(StiBorderSides.None, System.Drawing.Color.Black, 1, StiPenStyle.Solid, false, 4,
                                  new StiSolidBrush(System.Drawing.Color.Black)),
                Brush = new StiSolidBrush(System.Drawing.Color.Transparent)
            };
            newReport.Pages["Page1"].Components.Add(pageFooterBand1);
            var systemText1 = new StiSystemText
            {
                ClientRectangle = new RectangleD(0, 0, newReport.Pages["Page1"].Width, 0.2),
                DockStyle = StiDockStyle.Fill,
                HorAlignment = StiTextHorAlignment.Right,
                Name = "SystemText1",
                Border =
                    new StiBorder(StiBorderSides.Top, System.Drawing.Color.DimGray, 0.5, StiPenStyle.Solid, false, 4,
                                  new StiSolidBrush(System.Drawing.Color.Black)),
                Brush = new StiSolidBrush(System.Drawing.Color.Transparent),
                Font = new System.Drawing.Font("Arial Unicode MS", 8F),
                Margins = new StiMargins(0, 0, 0, 0),
                TextBrush = new StiSolidBrush(System.Drawing.Color.Black),
                TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService(),
                TextOptions =
                    new StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None,
                                       System.Drawing.StringTrimming.None),
                Text = "{PageNofM}",
                BeforePrintEvent =
                {
                    Script = "if(PageNumber%2==0)\n" +
                             "{\n" +
                             "SystemText1.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Right;\n" +
                             "}\n" +
                             "else\n" +
                             "{\n" +
                             "SystemText1.HorAlignment = Stimulsoft.Base.Drawing.StiTextHorAlignment.Left;\n" +
                             "}\n"
                }
            };
            pageFooterBand1.Components.Add(systemText1);
            newReport.Pages["Page1"].Name = "Catalog";
            if (0 == 0)
            {
                coverPageTemp = newReport.Pages["Cover"];
                //if (File.Exists(Application.StartupPath + "\\Images\\PDFCatalogCover.jpg"))
                //{
                //    var cPageImage = new StiImage
                //    {
                //        Name = "Cover_Image",
                //        Top = 0,
                //        Left = 0,
                //        Width = coverPage.Width,
                //        Height = coverPage.Height,
                //        MultipleFactor = 1.22,
                //        Border =
                //            new StiBorder(StiBorderSides.Top, System.Drawing.Color.MintCream, 1, StiPenStyle.Solid,
                //                          false, 4, new StiSolidBrush(System.Drawing.Color.Black)),
                //        Brush = new StiSolidBrush(System.Drawing.Color.Transparent),
                //        Image = null,
                //        BeforePrintEvent =
                //        {
                //            Script =
                //                @"Cover_Image.File = Application.StartupPath+@" + "\"\\Images\\PDFCatalogCover.jpg\""
                //        }
                //    };

                //    newReport.Pages["Cover"].Components.Add(cPageImage);
                //}
                //else
                //{
                var coverPageText = new StiText
                {
                    CanGrow = true,
                    CanShrink = true,
                    ClientRectangle = new RectangleD(0.9, 5.3, 1.59, 0.31),
                    HorAlignment = StiTextHorAlignment.Left,
                    Name = "CoverPageText",
                    VertAlignment = StiVertAlignment.Center,
                    Border =
                        new StiBorder(StiBorderSides.None, System.Drawing.Color.Black, 1,
                                      StiPenStyle.Solid, false, 4, new StiSolidBrush(System.Drawing.Color.Black)),
                    Brush = new StiSolidBrush(System.Drawing.Color.Transparent),
                    Font = new System.Drawing.Font("Arial Unicode MS", 14F, System.Drawing.FontStyle.Bold),
                    Margins = new StiMargins(0, 0, 0, 0),
                    TextBrush = new StiSolidBrush(System.Drawing.Color.Black),
                    TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService(),
                    TextOptions =
                        new StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None,
                                           System.Drawing.StringTrimming.None)
                };
                var coverPageTextCatalogName = new StiText
                {
                    CanGrow = true,
                    CanShrink = true,
                    ClientRectangle = new RectangleD(2.5, 5.3, newReport.Pages["Cover"].Width - 2.5, 0.31),
                    HorAlignment = StiTextHorAlignment.Left,
                    Name = "CoverPageTextCatalogName",
                    VertAlignment = StiVertAlignment.Center,
                    Border =
                        new StiBorder(StiBorderSides.None, System.Drawing.Color.Black, 1, StiPenStyle.Solid, false,
                                      4, new StiSolidBrush(System.Drawing.Color.Black)),
                    Brush = new StiSolidBrush(System.Drawing.Color.Transparent),
                    Font = new System.Drawing.Font("Arial Unicode MS", 14F, System.Drawing.FontStyle.Bold),
                    Margins = new StiMargins(0, 0, 0, 0),
                    TextBrush = new StiSolidBrush(System.Drawing.Color.Black),
                    TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService(),
                    TextOptions =
                        new StiTextOptions(false, false, true, 0F, System.Drawing.Text.HotkeyPrefix.None,
                                           System.Drawing.StringTrimming.None)
                };
                coverPageText.Text = "Cover Page for";
                coverPageTextCatalogName.Text = "{Catalog_Objects.CATALOG_NAME}";
                newReport.Pages["Cover"].Components.Add(coverPageText);
                newReport.Pages["Cover"].Components.Add(coverPageTextCatalogName);
                //}
            }
            if (TOCOptionSet == 0) //TOCOptionSet.CheckedIndex
            {
                tocPage.Margins = new StiMargins(Convert.ToDouble(LeftNumericEditor), Convert.ToDouble(RightNumericEditor), Convert.ToDouble(TopNumericEditor), Convert.ToDouble(BottomNumericEditor));
                if (pageformatOptionSet == 0) //pageformatOptionSet.CheckedIndex
                {
                    tocPage.Width = double.Parse(WidthNumericEditor) - 0.7799999999999994;
                    tocPage.Height = double.Parse(HeightNumericEditor) - 0.7799999999999994;
                }
                else
                {
                    tocPage.Width = Convert.ToDouble(WidthNumericEditor) - 0.7799999999999994;
                    tocPage.Height = Convert.ToDouble(HeightNumericEditor) - 0.7799999999999994;
                }
                tocPage.SegmentPerWidth = Convert.ToInt32(sWidthNumericEditor);
                tocPage.SegmentPerHeight = Convert.ToInt32(sHeightNumericEditor);
                var reportTitleBand1 = new StiReportTitleBand
                {
                    Name = "Report_Title_Band",
                    ClientRectangle = new RectangleD(0, 0.2, 7.49, 0.55),
                    DockStyle = StiDockStyle.Top,
                    Border =
                        new StiBorder(StiBorderSides.None, System.Drawing.Color.Black, 1, StiPenStyle.Solid, false, 4,
                                      new StiSolidBrush(System.Drawing.Color.Black)),
                    Brush = new StiSolidBrush(System.Drawing.Color.Transparent)
                };
                newReport.Pages["TOC"].Components.Add(reportTitleBand1);
                var reportText = new StiText
                {
                    ClientRectangle = new RectangleD(0, 0, 7.49, 0.55),
                    DockStyle = StiDockStyle.Fill,
                    HorAlignment = StiTextHorAlignment.Center,
                    Name = "Report_Text",
                    Text = "Contents",
                    VertAlignment = StiVertAlignment.Center,
                    Border =
                        new StiBorder(StiBorderSides.None, System.Drawing.Color.Black, 1, StiPenStyle.Solid, false, 4,
                                      new StiSolidBrush(System.Drawing.Color.Black)),
                    Brush = new StiSolidBrush(System.Drawing.Color.Transparent),
                    Font = new System.Drawing.Font("Arial Unicode MS", 24F, System.Drawing.FontStyle.Bold),
                    Margins = new StiMargins(0, 0, 0, 0),
                    TextBrush = new StiSolidBrush(System.Drawing.Color.Black),
                    TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService(),
                    TextOptions =
                        new StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None,
                                           System.Drawing.StringTrimming.None)
                };
                reportTitleBand1.Components.Add(reportText);
                if (TOC_Cat_Fam_OptionSet == 0) //TOC_Cat_Fam_OptionSet.CheckedIndex == 0 && TOC_Cat_Fam_OptionSet.Enabled
                {
                    //    foreach (UltraTreeNode t in from UltraTreeNode t in GroupAvailableFieldsTree.Nodes[0].Nodes where t.Text.Contains("cate") || t.Text.Contains("subcat") select t)
                    //    {
                    //        t.CheckedState = CheckState.Checked;
                    //    }
                }
                tocPageTemp = tocPage;
            }
            if (IndexPageOptionSet == 0) //IndexPageOptionSet.CheckedIndex
            {

                indexPage.Margins = new StiMargins(Convert.ToDouble(LeftNumericEditor), Convert.ToDouble(RightNumericEditor), Convert.ToDouble(TopNumericEditor), Convert.ToDouble(BottomNumericEditor));
                if (pageformatOptionSet == 0) //pageformatOptionSet.CheckedIndex
                {

                    indexPage.Width = double.Parse(WidthNumericEditor) - 0.7799999999999994;
                    indexPage.Height = double.Parse(HeightNumericEditor) - 0.7799999999999994;

                }
                else
                {
                    indexPage.Width = Convert.ToDouble(WidthNumericEditor) - 0.7799999999999994;
                    indexPage.Height = Convert.ToDouble(HeightNumericEditor) - 0.7799999999999994;
                }
                indexPage.SegmentPerWidth = Convert.ToInt32(sWidthNumericEditor);
                indexPage.SegmentPerHeight = Convert.ToInt32(sHeightNumericEditor);
                indexPage.Columns = Convert.ToInt32("1");


                var pageHeaderBand = new StiPageHeaderBand
                {
                    ClientRectangle = new RectangleD(0, 0.2, 7.5, 0.5),
                    DockStyle = StiDockStyle.Top,
                    Name = "Index_Header_Band",
                    Border =
                        new StiBorder(StiBorderSides.None, System.Drawing.Color.Black, 1, StiPenStyle.Solid, false, 4,
                                      new StiSolidBrush(System.Drawing.Color.Black)),
                    Brush = new StiSolidBrush(System.Drawing.Color.Transparent)
                };
                newReport.Pages["Index"].Components.Add(pageHeaderBand);
                if (Index_Cat_No_OptionSet == 0) //Index_Cat_No_OptionSet.CheckedIndex == 0 && Index_Cat_No_OptionSet.Enabled
                {
                    var indexText = new StiText
                    {
                        ClientRectangle = new RectangleD(0, 0, 7.5, 0.3),
                        HorAlignment = StiTextHorAlignment.Center,
                        Name = "Index_Text",
                        Text = "Index",
                        Border =
                            new StiBorder(StiBorderSides.None, System.Drawing.Color.Black, 1, StiPenStyle.Solid, false,
                                          4, new StiSolidBrush(System.Drawing.Color.Black)),
                        Brush = new StiSolidBrush(System.Drawing.Color.Transparent),
                        Font = new System.Drawing.Font("Arial Unicode MS", 18F, System.Drawing.FontStyle.Bold),
                        Margins = new StiMargins(0, 0, 0, 0),
                        TextBrush = new StiSolidBrush(System.Drawing.Color.Black),
                        TextFormat = new Stimulsoft.Report.Components.TextFormats.StiGeneralFormatService(),
                        TextOptions =
                            new StiTextOptions(false, false, false, 0F, System.Drawing.Text.HotkeyPrefix.None,
                                               System.Drawing.StringTrimming.None)
                    };
                    pageHeaderBand.Components.Add(indexText);
                }
                indexPageTemp = indexPage;
            }
            StiPagesCollection collection = newReport.Pages;
            if (coverPageTemp == null)
            {
                for (int n = 0; n < collection.Count; n++)
                {
                    if (collection[n].Name == "Cover")
                    {
                        newReport.Pages.RemoveAt(n);
                    }
                }
            }
            if (tocPageTemp == null)
            {
                for (int n = 0; n < collection.Count; n++)
                {
                    if (collection[n].Name == "TOC")
                    {
                        newReport.Pages.RemoveAt(n);
                    }
                }
            }
            if (indexPageTemp == null)
            {
                for (int n = 0; n < collection.Count; n++)
                {
                    if (collection[n].Name == "Index")
                    {
                        newReport.Pages.RemoveAt(n);
                    }
                }
            }
            // return StiMvcDesignerFx.GetReportTemplateResult(newReport);

            //StiReport report = new StiReport();
            //report.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Reports/Report.mrt"));
            //report.ReportCacheMode = StiReportCacheMode.Off;
            //StiOptions.Engine.ReportCache.CachePath = "~/Views/App/XpressCatalog/Content/Temp";
            //StiOptions.Engine.ImageCache.Enabled = false;
            //StiOptions.Engine.ImageCache.CachePath = "~/Views/App/XpressCatalog/Content/Temp";
            //report.ReportUnit = Stimulsoft.Report.StiReportUnitType.Inches;
            //report.Dictionary.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct"));
            //report.Dictionary.Synchronize();
            //report.Dictionary.Databases.Clear();
            //report.Dictionary.Databases.Add(new Stimulsoft.Report.Dictionary.StiSqlDatabase("CatalogStudio", connectionString));
            ////  FamilyFilter(workingCatalogID, report);
            //// ProductFilter(workingCatalogID, report);
            //report.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
            //report.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
            //report.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
            //report.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
            //report.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
            //report.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();
            //report.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value = workingCatalogID.ToString();

        }

        private void CustomFields(ref StiReport simpleReport)
        {
            string custAttrName = string.Empty;
            var SelectedFieldsTree = (selectedTreeNodes[0]).Select(x => x).ToList();
            foreach (var item in SelectedFieldsTree)
            {
                if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 2 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 3 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 4)
                {
                    TB_ATTRIBUTE str_attribute_id = _dbcontext.TB_ATTRIBUTE.Find(item["ATTRIBUTE_NAME"]);
                    custAttrName += "," + str_attribute_id.ATTRIBUTE_ID;
                }

            }
            var selAttrName = new string[0];
            var catattName = new string[0];
            if (custAttrName != "")
                selAttrName = custAttrName.Substring(1).Split(',');

            DataSet flatDataset = FunctionAttr(selAttrName, catattName);

            DataSet flatFamily = FamilyFilterFlatTable(flatDataset);
            DataSet finalDs = ProductFilterFlatTable(flatFamily);
            finalDs.DataSetName = "CatalogStudio";
            DataTable dtFamilyDetails = finalDs.Tables[0].DefaultView.ToTable(true, "FAMILY_ID", "FAMILY_NAME");
            for (int i = 0; i < finalDs.Tables.Count; i++)
            {
                if (i == 0)
                    finalDs.Tables[0].TableName = "Catalog Objects";
                else if (i == 1)
                    finalDs.Tables[1].TableName = "Multiple Tables";
                else if (i >= 2)
                    if (finalDs.Tables[i].Rows.Count > 0)
                        finalDs.Tables[i].TableName = "Reference Tables" + finalDs.Tables[i].Rows[0][1].ToString();
            }
            finalDs.Tables.Add(dtFamilyDetails);
            finalDs.Tables[finalDs.Tables.Count - 1].TableName = "Family Details";
            for (int mainCount = 0; mainCount < finalDs.Tables.Count; mainCount++)
            {
                for (int cCount = 0; cCount < finalDs.Tables[mainCount].Columns.Count; cCount++)
                {
                    try
                    {
                        finalDs.Tables[0].Columns[cCount].Caption = finalDs.Tables[0].Columns[cCount].ColumnName;
                        finalDs.Tables[0].Columns[cCount].ColumnName =
                            ReplaceCharacters(finalDs.Tables[0].Columns[cCount].ColumnName);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            DataSet finalDataSource = PrefixSufixFlatTable(finalDs, selAttrName);
            simpleReport.RegData(finalDataSource);
            ImagePathXpc();
            ProjectID = 970;
            simpleReport.Dictionary.Variables.Add(new StiVariable("", "AttachmentPath", "AttachmentPath", "",
                                                                  typeof(string), "", true, false));
            var attachmentPath = ConfigurationManager.AppSettings["AttachmentPath"];
            simpleReport.Dictionary.Variables["AttachmentPath"].Value = System.Web.Hosting.HostingEnvironment.MapPath(attachmentPath);
            simpleReport.Dictionary.Variables.Add(new StiVariable("", "ProjectID", "ProjectID", "", typeof(string), "",
                                                                  true, false));
            simpleReport.Dictionary.Variables["ProjectID"].Value = ProjectID.ToString(CultureInfo.InvariantCulture);
            simpleReport.Dictionary.Synchronize();
            custAttrName = ",Product ID";
            foreach (var item in SelectedFieldsTree)
            {
                if (Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 2 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 3 || Convert.ToInt32(item["ATTRIBUTE_TYPE"]) == 4)
                {
                    custAttrName += "," + item["ATTRIBUTE_NAME"];
                }

            }
            if (custAttrName != "")
                selAttrName = custAttrName.Substring(1).Split(',');
            if (Opensimple == false && _runedTemplate == false)
            {
                SimpleCatalogReportDesign(simpleReport, selAttrName);
            }
            else
            {
                _openedReport.RegData(finalDataSource);
            }
            StiDataRelation relation = new StiDataRelation("FamilyRelation", "FamilySource", "FamilyRelation", simpleReport.Dictionary.DataSources["Family Details"], simpleReport.Dictionary.DataSources["Catalog Objects"], new System.String[] { "FAMILY_ID" }, new System.String[] { "FAMILY_ID" });
            simpleReport.Dictionary.Relations.Add(relation);
            relation = new StiDataRelation("MultipleTableRelation", "MultipleTableSource", "MultipleTableRelation", simpleReport.Dictionary.DataSources["Family Details"], simpleReport.Dictionary.DataSources["Multiple Tables"], new System.String[] { "FAMILY_ID" }, new System.String[] { "FAMILY_ID" });
            simpleReport.Dictionary.Relations.Add(relation);
            relation = new StiDataRelation("ReferenceTableRelation", "ReferenceTableSource", "ReferenceTableRelation", simpleReport.Dictionary.DataSources["Family Details"], simpleReport.Dictionary.DataSources["Reference Tables"], new System.String[] { "FAMILY_ID" }, new System.String[] { "FAMILY_ID" });
            simpleReport.Dictionary.Relations.Add(relation);
            simpleReport.Dictionary.Synchronize();
            if (Opensimple == false && _runedTemplate == false)
            {
                try
                {
                    simpleReport.Design(this.Formopen);
                }
                catch (Exception exception)
                {
                }
            }
        }

        private DataSet FunctionAttr(string[] attrNames, string[] catattName)
        {
            try
            {
                CatalogID = _workingCatalogID;
                foreach (var dt in catattName)
                {
                    sysAttr += "," + dt.ToString();
                }
                //sysAttr = "CATALOG_ID,CATALOG_NAME,CATEGORY_ID,CATEGORY_NAME,Family ID,Family Name,SubFamily ID,SubFamily Name";
                string conValue = "", attrHeader = "";
                var ocnn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                const string attrRowCol = "Column";
                string SQLString = conValue != ""
                    ? "[STP_CATALOGSTUDIO5_FlatTable] '" + CatalogID + "' , '" +
                      conValue.Substring(0, conValue.Length - 1).Replace("'", "''") + "' , '" + attrHeader + "' , " +
                      Convert.ToString(attrNames.Length) + ", '" + attrRowCol + "'"
                    : "[STP_CATALOGSTUDIO5_FlatTable] '" + CatalogID + "' , '''' , '' , " +
                      Convert.ToString(attrNames.Length) + ", '" + attrRowCol + "'";

                var attrList = new int[attrNames.Length];
                for (int incVal = 0; incVal < attrNames.Length; incVal++)
                    attrList[incVal] = Convert.ToInt32(attrNames[incVal]);
                var dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                var SelectedFieldsTree = (selectedTreeNodes[0]).Select(x => x).ToList();
                foreach (var itemm in SelectedFieldsTree)
                {
                    if (itemm["ATTRIBUTE_ID"].ToString() != "0" && itemm["ATTRIBUTE_TYPE"].ToString() == "14")
                    {
                        multipleTables += "," + itemm["ATTRIBUTE_ID"];
                    }
                    if (itemm["ATTRIBUTE_ID"].ToString() != "0" && itemm["ATTRIBUTE_TYPE"].ToString() == "15")
                    {
                        referenceTables += "," + itemm["ATTRIBUTE_ID"];
                    }
                }
                if (string.IsNullOrEmpty(multipleTables))
                {
                    multipleTables = ",";
                }
                if (string.IsNullOrEmpty(referenceTables))
                {
                    referenceTables = ",";
                }
                DataSet attrDtds1 = CatalogPdfx(CatalogID, 0, attrList, sysAttr, ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString, "", multipleTables.Substring(1), referenceTables.Substring(1));
                var attrDtds2 = new DataSet();
                for (int i = 0; i < attrDtds1.Tables.Count; i++)
                {
                    attrDtds2.Tables.Add(attrDtds1.Tables[i].Copy());
                }
                DataSet attrDtds = ApplyStyleFormat(attrDtds2);
                return attrDtds;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at FamilyApiController: FunctionAttr", ex);
                return null;
            }
        }

        private DataSet FamilyFilterFlatTable(DataSet flatDataset)
        {
            var sqLstring = new StringBuilder();

            _SQLString = " SELECT FAMILY_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + CatalogID + " ";

            DataSet oDsFamilyFilter = CreateDataSet();
            if (oDsFamilyFilter.Tables[0].Rows.Count > 0 && oDsFamilyFilter.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
            {
                string sFamilyFilter = oDsFamilyFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                var xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(sFamilyFilter);
                XmlNode rNode = xmlDOc.DocumentElement;
                if (rNode != null && rNode.ChildNodes.Count > 0)
                {
                    for (int i = 0; i < rNode.ChildNodes.Count; i++)
                    {
                        XmlNode tableDataSetNode = rNode.ChildNodes[i];
                        if (tableDataSetNode.HasChildNodes)
                        {
                            if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[2].InnerText = "=";
                            }
                            if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[0].InnerText = "0";
                            }
                            string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                            if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                            {
                                sqLstring.Append("SELECT DISTINCT FAMILY_ID FROM [FAMILY DESCRIPTION](" + CatalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                            }
                            else
                            {
                                sqLstring.Append("SELECT DISTINCT FAMILY_ID FROM [FAMILY DESCRIPTION](" + CatalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                            }
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                        {

                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                        {
                            sqLstring.Append(" INTERSECT \n");
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                        {
                            sqLstring.Append(" UNION \n");
                        }
                    }
                }
            }
            string familyFiltersql = sqLstring.ToString();
            if (familyFiltersql.Length > 0)
            {
                string s = "SELECT FAMILY_ID FROM FAMILY(" + CatalogID + ") WHERE FAMILY_ID IN\n" +
                      "(\n";
                familyFiltersql = s + familyFiltersql + "\n)";
                _SQLString = familyFiltersql;
                oDsFamilyFilter = CreateDataSet();
                for (int j = 0; j < flatDataset.Tables[0].Rows.Count; j++)
                {
                    DataRow odr = flatDataset.Tables[0].Rows[j];
                    bool available = oDsFamilyFilter.Tables[0].Rows.Cast<DataRow>().Count(dr => dr["FAMILY_ID"].ToString() == odr["Family_ID"].ToString() || dr["FAMILY_ID"].ToString() == odr["SubFamily_ID"].ToString()) > 0;
                    if (available == false)
                    {
                        odr.Delete();
                        flatDataset.AcceptChanges();
                        j = -1;
                    }
                }
            }
            return flatDataset;

        }

        private DataSet ProductFilterFlatTable(DataSet flatDataset)
        {
            var sqLstring = new StringBuilder();
            _SQLString = " SELECT PRODUCT_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + CatalogID + " ";

            DataSet oDsProductFilter = CreateDataSet();
            if (oDsProductFilter.Tables[0].Rows.Count > 0 && oDsProductFilter.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
            {
                string sProductFilter = oDsProductFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                var xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(sProductFilter);
                XmlNode rNode = xmlDOc.DocumentElement;
                if (rNode != null && rNode.ChildNodes.Count > 0)
                {
                    for (int i = 0; i < rNode.ChildNodes.Count; i++)
                    {
                        XmlNode tableDataSetNode = rNode.ChildNodes[i];
                        if (tableDataSetNode.HasChildNodes)
                        {
                            if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[2].InnerText = "=";
                            }
                            if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[0].InnerText = "0";
                            }
                            string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                            _SQLString = " SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE  ATTRIBUTE_ID = " + Convert.ToInt32(tableDataSetNode.ChildNodes[0].InnerText) + " ";
                            DataSet attribuetypeDs = CreateDataSet();
                            if (attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("TEX") || attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("DATE"))
                            {
                                if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + CatalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                }
                                else
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + CatalogID + ") WHERE (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                }
                            }
                            else if (attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("DECI") || attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("NUM"))
                            {
                                if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + CatalogID + ") WHERE (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                }
                                else
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + CatalogID + ") WHERE  (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                }
                            }
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                        {

                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                        {
                            sqLstring.Append(" INTERSECT \n");
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                        {
                            sqLstring.Append(" UNION \n");
                        }
                    }
                }
            }
            string productFiltersql = sqLstring.ToString();
            if (productFiltersql.Length > 0)
            {
                string s = "SELECT PRODUCT_ID FROM [PRODUCT FAMILY](" + CatalogID + ") WHERE PRODUCT_ID IN\n" +
                      "(\n";
                productFiltersql = s + productFiltersql + "\n)";

                _SQLString = productFiltersql;
                oDsProductFilter = CreateDataSet();
                var dataDs = new DataSet();
                DataTable table = flatDataset.Tables[0].Clone();
                dataDs.Tables.Add(table);
                foreach (DataRow odr in from DataRow odr in flatDataset.Tables[0].Rows from DataRow dr in oDsProductFilter.Tables[0].Rows where dr["PRODUCT_ID"].ToString() == odr["Product_ID"].ToString() select odr)
                {
                    table.ImportRow(odr);
                }
                flatDataset.Tables.Clear();
                flatDataset = dataDs.Copy();
            }
            return flatDataset;

        }

        private DataSet PrefixSufixFlatTable(DataSet finalDs, IEnumerable<string> selAttrName)
        {
            var oDsAttributeDatarule = new DataSet();

            foreach (string t in selAttrName)
            {
                oDsAttributeDatarule.Clear();
                _SQLString = string.Empty;
                _SQLString = "SELECT ATTRIBUTE_DATARULE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = '" + t + "' ";
                oDsAttributeDatarule = CreateDataSet();
                if (oDsAttributeDatarule.Tables[0].Rows.Count > 0 && oDsAttributeDatarule.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                {
                    string sAttributeDataRule = oDsAttributeDatarule.Tables[0].Rows[0].ItemArray[0].ToString();
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(sAttributeDataRule);
                    XmlNode rNode = xmlDOc.DocumentElement;
                    if (rNode != null && rNode.ChildNodes.Count > 0)
                    {
                        for (int i = 0; i < rNode.ChildNodes.Count; i++)
                        {
                            XmlNode tableDataSetNode = rNode.ChildNodes[i];
                            if (tableDataSetNode.HasChildNodes)
                            {
                                string prefix = tableDataSetNode.ChildNodes[0].InnerText;
                                string sufix = tableDataSetNode.ChildNodes[1].InnerText;
                                string condition = tableDataSetNode.ChildNodes[2].InnerText;
                                string customValue = tableDataSetNode.ChildNodes[3].InnerText;
                                _SQLString = string.Empty;
                                _SQLString = "SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = '" + t + "' ";
                                DataSet attrName = CreateDataSet();
                                string columnName = ReplaceCharacters(attrName.Tables[0].Rows[0].ItemArray[0].ToString());
                                if (condition.Length != 0)
                                {
                                    foreach (DataRow dr in finalDs.Tables[0].Rows)
                                    {
                                        if (condition.Length == 0)
                                        {
                                            if (dr[columnName].ToString().Length != 0)
                                            {
                                                dr[columnName] = prefix + dr[columnName] + sufix;
                                            }
                                        }
                                        if (condition.Length != 0)
                                        {
                                            if (dr[columnName].ToString() == condition)
                                            {
                                                if (dr[columnName].ToString().Length == 0)
                                                    if (condition == "Empty" || condition == "Null")
                                                    {
                                                        dr[columnName] = customValue;
                                                    }
                                                if (dr[columnName].ToString().Length > 0)
                                                {
                                                    dr[columnName] = prefix + customValue + sufix;
                                                }
                                            }
                                            else
                                            {
                                                if (dr[columnName].ToString().Length != 0)
                                                {
                                                    dr[columnName] = prefix + dr[columnName] + sufix;
                                                }
                                                else
                                                {
                                                    dr[columnName] = customValue;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (DataRow dr in finalDs.Tables[0].Rows)
                                    {
                                        try
                                        {
                                            if (dr[columnName].ToString().Length > 0)
                                                dr[columnName] = prefix + dr[columnName] + sufix;
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var selectedMultipleTables = multipleTables.Substring(1).Split(',');
            foreach (string t in selectedMultipleTables)
            {
                oDsAttributeDatarule.Clear();
                _SQLString = string.Empty;
                _SQLString = "SELECT ATTRIBUTE_DATARULE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = '" + t + "' ";
                oDsAttributeDatarule = CreateDataSet();
                if (oDsAttributeDatarule.Tables[0].Rows.Count > 0 && oDsAttributeDatarule.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                {
                    string sAttributeDataRule = oDsAttributeDatarule.Tables[0].Rows[0].ItemArray[0].ToString();
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(sAttributeDataRule);
                    XmlNode rNode = xmlDOc.DocumentElement;
                    if (rNode != null && rNode.ChildNodes.Count > 0)
                    {
                        for (int i = 0; i < rNode.ChildNodes.Count; i++)
                        {
                            XmlNode tableDataSetNode = rNode.ChildNodes[i];
                            if (tableDataSetNode.HasChildNodes)
                            {
                                string prefix = tableDataSetNode.ChildNodes[0].InnerText;
                                string sufix = tableDataSetNode.ChildNodes[1].InnerText;
                                string condition = tableDataSetNode.ChildNodes[2].InnerText;
                                string customValue = tableDataSetNode.ChildNodes[3].InnerText;
                                _SQLString = string.Empty;
                                _SQLString = "SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = '" + t + "' ";
                                DataSet attrName = CreateDataSet();
                                string columnName = ReplaceCharacters(attrName.Tables[0].Rows[0].ItemArray[0].ToString());
                                if (condition.Length != 0)
                                {
                                    foreach (DataRow dr in finalDs.Tables[1].Rows)
                                    {
                                        if (condition.Length == 0)
                                        {
                                            if (dr[columnName].ToString().Length != 0)
                                            {
                                                dr[columnName] = prefix + dr[columnName] + sufix;
                                            }
                                        }
                                        if (condition.Length != 0)
                                        {
                                            if (dr[columnName].ToString() == condition)
                                            {
                                                if (dr[columnName].ToString().Length == 0)
                                                    if (condition == "Empty" || condition == "Null")
                                                    {
                                                        dr[columnName] = customValue;
                                                    }
                                                if (dr[columnName].ToString().Length > 0)
                                                {
                                                    dr[columnName] = prefix + customValue + sufix;
                                                }
                                            }
                                            else
                                            {
                                                if (dr[columnName].ToString().Length != 0)
                                                {
                                                    dr[columnName] = prefix + dr[columnName] + sufix;
                                                }
                                                else
                                                {
                                                    dr[columnName] = customValue;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (DataRow dr in finalDs.Tables[1].Rows)
                                    {
                                        try
                                        {
                                            if (dr[columnName].ToString().Length > 0)
                                                dr[columnName] = prefix + dr[columnName] + sufix;
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return finalDs;

        }

        public DataSet CatalogPdfx(int catalogID, int familyID, int[] attributeList, string SysAttrIds, string connvalue, string catId, string multipleTables, string referenceTables)
        {
            var joinedDs = new DataSet();
            string custAttrName = string.Empty;
            #region For Category level Attributes

            List<int> categoryAttributes = new List<int>();
            categoryAttributes = attributeList.ToList();
            var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);

            var category_Attributes = (from categorySpecs in _dbcontext.TB_CATEGORY_SPECS
                                       join attribute in _dbcontext.TB_ATTRIBUTE on categorySpecs.ATTRIBUTE_ID equals attribute.ATTRIBUTE_ID
                                       join customerAttribute in _dbcontext.CUSTOMERATTRIBUTE on attribute.ATTRIBUTE_ID equals customerAttribute.ATTRIBUTE_ID
                                       where customerAttribute.CUSTOMER_ID == customerid.CustomerId && categorySpecs.CATALOG_ID == catalogID && categoryAttributes.Contains(customerAttribute.ATTRIBUTE_ID)
                                       select new
                                       {
                                           attribute.ATTRIBUTE_ID,
                                           attribute.ATTRIBUTE_NAME,
                                           attribute.ATTRIBUTE_TYPE,
                                           attribute.PUBLISH2CDROM,
                                           attribute.PUBLISH2ODP,
                                           attribute.PUBLISH2PRINT,
                                           attribute.PUBLISH2WEB
                                       }).Distinct().ToList();

            #endregion
            if (attributeList.Length > 0)
            {
                custAttrName = attributeList.Select(a => a.ToString()).Aggregate((a, b) => a + "," + b);
            }
            else
            {
                custAttrName = "0";
            }
            if (!string.IsNullOrEmpty(multipleTables))
            {
                custAttrName = custAttrName + "|" + multipleTables;
            }
            else
            {
                custAttrName = custAttrName + "|0";
            }
            if (!string.IsNullOrEmpty(referenceTables))
            {
                custAttrName = custAttrName + "|" + referenceTables;
            }
            else
            {
                custAttrName = custAttrName + "|0";
            }
            using (var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                sqlConn.Open();
                try
                {
                    var cmd = new SqlCommand("STP_CATALOGSTUDIO5_StimulSoftFlatDatas", sqlConn)
                    {
                        CommandTimeout = 0,
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@SESSID", "CS1");
                    cmd.Parameters.AddWithValue("@CATALOG_ID", CatalogID);
                    cmd.Parameters.AddWithValue("@ATTRIBUTE_IDS", custAttrName);
                    cmd.Parameters.AddWithValue("@SYSATTR_IDS", SysAttrIds);
                    cmd.Parameters.AddWithValue("@CATEGORY_ID", "ALL");
                    cmd.Parameters.AddWithValue("@PATH", "d:\\");
                    var da = new SqlDataAdapter(cmd);
                    da.Fill(joinedDs);
                    joinedDs = HtmlSanitizer.Sanitize(joinedDs);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                }

                #region For Category level Attributes

                if (joinedDs != null && joinedDs.Tables.Count > 0 && joinedDs.Tables[0].Rows.Count > 0)
                {
                    DataColumnCollection columns = joinedDs.Tables[0].Columns;
                    int index = 0;
                    if (columns.Contains("FAMILY_ID"))
                    {
                        index = columns.IndexOf("FAMILY_ID");
                    }
                    foreach (var item in category_Attributes)
                    {
                        joinedDs.Tables[0].Columns.Add(item.ATTRIBUTE_NAME).SetOrdinal(index);
                        index = index + 1;
                    }

                    joinedDs.AcceptChanges();

                    foreach (DataRow dtRow in joinedDs.Tables[0].Rows)
                    {
                        foreach (var item in category_Attributes)
                        {
                            string category_Id = Convert.ToString(dtRow["CATEGORY_ID"]);
                            string string_value = string.Empty;
                            TB_CATEGORY_SPECS categorySpecs = _dbcontext.TB_CATEGORY_SPECS.Where(s => s.CATEGORY_ID == category_Id && s.ATTRIBUTE_ID == item.ATTRIBUTE_ID && s.CATALOG_ID == catalogID).FirstOrDefault();
                            if (categorySpecs != null && categorySpecs.ID > 0)
                                string_value = categorySpecs.STRING_VALUE;
                            if (joinedDs.Tables[0].Columns.Contains(item.ATTRIBUTE_NAME))
                            {
                                dtRow[item.ATTRIBUTE_NAME] = string_value;
                            }
                        }
                    }

                }

                #endregion

                #region "Decimal Place construction"
                foreach (DataRow dr in joinedDs.Tables[0].Rows)
                {
                    //if ((dr["A"].ToString() != null && dr["B"].ToString() != null && dr["C"].ToString() != null) || (dr["A"].ToString() != "" && dr["B"].ToString() != "" && dr["C"].ToString() != ""))
                    {
                        if (dr["PRODUCT_ID"].ToString() != null && dr["PRODUCT_ID"].ToString() != "")
                        {
                            var cmd = new SqlCommand("select ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE  from TB_PROD_SPECS aps join TB_ATTRIBUTE b on aps.ATTRIBUTE_ID=b.ATTRIBUTE_ID where PRODUCT_ID=" + dr["PRODUCT_ID"].ToString() + " and ATTRIBUTE_DATATYPE like 'Num%' union select ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE  from TB_FAMILY_SPECS aps join TB_ATTRIBUTE b on aps.ATTRIBUTE_ID=b.ATTRIBUTE_ID where FAMILY_ID=" + dr["FAMILY_ID"].ToString() + " and ATTRIBUTE_DATATYPE like 'Num%'", sqlConn);
                            DataSet temp = new DataSet();
                            var da = new SqlDataAdapter(cmd);
                            da.Fill(temp);
                            if (temp.Tables[0].Rows.Count > 0)
                            {
                                for (int i = 0; i < joinedDs.Tables[0].Columns.Count; i++)
                                {
                                    for (int j = 0; j < temp.Tables[0].Rows.Count; j++)
                                    {
                                        if (temp.Tables[0].Rows[j].ItemArray[0].ToString() == joinedDs.Tables[0].Columns[i].ToString())
                                        {
                                            if (dr[joinedDs.Tables[0].Columns[i]].ToString() != null && dr[joinedDs.Tables[0].Columns[i]].ToString() != "")
                                            {
                                                strval = dr[joinedDs.Tables[0].Columns[i].ToString()].ToString();
                                                datatype = temp.Tables[0].Rows[j].ItemArray[1].ToString();
                                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                                noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                    strval = strval.Remove(strval.IndexOf('.') + 1 + noofdecimalplace);
                                                if (noofdecimalplace == 0)
                                                {
                                                    strval = strval.TrimEnd('.');
                                                }
                                                dr[joinedDs.Tables[0].Columns[i]] = strval;
                                                //int a = temp["ATTRIBUTE_DATATYPE"];
                                                #region "Decimal Place Finding"
                                                //            string afterDecimal = string.Empty;
                                                //            string decimalPrecision = dr["A"].ToString();
                                                //            int _afterDecimalVal = 0;
                                                //            for (int i = 0; i < decimalPrecision.Length; i++)
                                                //            {
                                                //                if (decimalPrecision[i] == ',')
                                                //                {
                                                //                    int j = i + 1;
                                                //                    while (decimalPrecision[j] != ')')
                                                //                    {
                                                //                        afterDecimal = afterDecimal + decimalPrecision[j].ToString();
                                                //                        j++;
                                                //                    }
                                                //                }
                                                //            }

                                                #endregion
                                                #region "Trimming as per decimal place"
                                                //if(
                                                //string attr = "A";

                                                //string strvalue = dr["A"].ToString();
                                                //int noofdecimal = strvalue.IndexOf(".", StringComparison.Ordinal);
                                                //if (strvalue.IndexOf(".", StringComparison.Ordinal) > 0)
                                                //{
                                                //    if ((strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1) > 0)
                                                //    {

                                                //        if (Convert.ToDecimal(strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1)) == 0)
                                                //        {
                                                //            strvalue = strvalue.Substring(0, strvalue.IndexOf(".", StringComparison.Ordinal));
                                                //            string retVal = "";
                                                //            for (int preadd = 0; preadd < noofdecimal; preadd++)
                                                //            { retVal = retVal + "0"; }
                                                //            if (retVal != "")
                                                //            {
                                                //                dr["A"] = strvalue + "." + retVal;
                                                //            }
                                                //        }
                                                //        else if (Convert.ToDecimal(strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1)) > 0)
                                                //        {
                                                //            if (strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1).Length >= noofdecimal)
                                                //                dr["A"] = strvalue.Substring(0, strvalue.IndexOf(".", StringComparison.Ordinal)) + "." + (strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, noofdecimal));
                                                //        }
                                                //    }
                                                //}
                                                #endregion

                                            }
                                        }
                                    }
                                }

                            }

                        }
                    }
                }
                #endregion
                #region "Date and Time Formatting"
                //string Attribute_IDS = Convert.ToString(attributeList);

                if (joinedDs.Tables.Count > 0)
                    if (joinedDs.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < attributeList.Count(); i++)
                        {
                            //Ocon.SQLString = "SELECT ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID =" + Attribute_IDS[i] + "";
                            var cmd = new SqlCommand("SELECT ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID =" + attributeList[i].ToString() + "", sqlConn);
                            DataSet AttrName = new DataSet();
                            var da = new SqlDataAdapter(cmd);
                            da.Fill(AttrName);

                            if (AttrName.Tables.Count > 0)
                                if (AttrName.Tables[0].Rows.Count > 0)
                                    if (AttrName.Tables[0].Rows[0][1].ToString().StartsWith("Date"))
                                    {
                                        foreach (DataColumn dc in joinedDs.Tables[0].Columns)
                                        {
                                            if (dc.ColumnName == AttrName.Tables[0].Rows[0][0].ToString())
                                            {
                                                foreach (DataRow dr in joinedDs.Tables[0].Rows)
                                                {
                                                    if (AttrName.Tables[0].Rows[0][2].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
                                                    {
                                                        string tempdate = dr[dc.ColumnName].ToString();
                                                        if (tempdate != "" && tempdate != null)
                                                        {
                                                            dr[dc.ColumnName] = tempdate.Substring(3, 2) + "/" + tempdate.Substring(0, 2) + "/" + tempdate.Substring(6, 4);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                        }
                    }

                #endregion
                sqlConn.Close();
            }
            try
            {
                joinedDs.Tables[0].Columns.Remove("FAMILY_ID_1");
            }
            catch (Exception) { }
            bool supplierInfo = false;
            for (int i = 0; i < attributeList.Length; i++)
            {
                if (attributeList[i].ToString(CultureInfo.InvariantCulture) == "3")
                {
                    supplierInfo = true;
                    break;
                }
            }
            if (supplierInfo)
            {
                var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                string sql = "SELECT	distinct TB_CATALOG_FAMILY.CATALOG_ID,TB_PROD_FAMILY.FAMILY_ID,TB_PROD_FAMILY.PRODUCT_ID," +
                            " SUPPLIER_COMPANY_NAME,ADDRESS_LINE_1 as SUPPLIER_ADDRESS_LINE_1,ADDRESS_LINE_2 as SUPPLIER_ADDRESS_LINE_2," +
                            " ADDRESS_LINE_3 as SUPPLIER_ADDRESS_LINE_3,CITY as SUPPLIER_CITY,STATE AS SUPPLIER_STATE,ZIP AS SUPPLIER_ZIP,COUNTRY AS SUPPLIER_COUNTRY,PHONE_1 AS SUPPLIER_PHONE_1," +
                            " FAX AS SUPPLIER_FAX,EMAIL AS SUPPLIER_EMAIL,URL AS SUPPLIER_URL,LOGO_IMAGE_FILE AS SUPPLIER_LOGO_IMAGE_FILE,LOGO_IMAGE_TYPE AS SUPPLIER_LOGO_IMAGE_TYPE" +
                            " FROM	TB_CATALOG_FAMILY, TB_PROD_FAMILY, TB_CATALOG_PRODUCT, TB_PROD_SPECS,TB_SUPPLIER" +
                            " WHERE	TB_CATALOG_FAMILY.FAMILY_ID = TB_PROD_FAMILY.FAMILY_ID AND" +
                            " TB_PROD_FAMILY.PRODUCT_ID =  TB_CATALOG_PRODUCT.PRODUCT_ID AND" +
                            " TB_CATALOG_FAMILY.CATALOG_ID=" + catalogID + " AND" +
                            " TB_PROD_SPECS.ATTRIBUTE_ID = 3 AND TB_PROD_SPECS.PRODUCT_ID = TB_CATALOG_PRODUCT.PRODUCT_ID  AND" +
                            " TB_CATALOG_FAMILY.CATALOG_ID=TB_CATALOG_PRODUCT.CATALOG_ID AND TB_PROD_FAMILY.PUBLISH=1 AND" +
                            " TB_PROD_SPECS.STRING_VALUE=TB_SUPPLIER.SUPPLIER_NAME";
                DataSet supplierDs = new DataSet();
                SqlDataAdapter _DBAdapter = new SqlDataAdapter(sql, objSqlConnection);
                _DBAdapter.SelectCommand.CommandTimeout = 0;
                _DBAdapter.Fill(supplierDs);
                _DBAdapter.Dispose();


                if (supplierDs != null && supplierDs.Tables[0].Rows.Count > 0)
                {
                    DataTable newTable = JoinSupplierDS(joinedDs.Tables[0], supplierDs.Tables[0]);
                    var joinedDs1 = new DataSet();
                    joinedDs1.Tables.Add(newTable);
                    joinedDs1 = HtmlSanitizer.Sanitize(joinedDs1);
                    return joinedDs1;
                }
            }
            joinedDs = HtmlSanitizer.Sanitize(joinedDs);
            return joinedDs;
        }

        private DataTable JoinSupplierDS(DataTable first, DataTable second)
        {

            var table = new DataTable("JoinedTable");

            foreach (DataColumn dc in first.Columns)
            {
                table.Columns.Add(dc.ColumnName, dc.DataType);
            }
            foreach (DataColumn dc in second.Columns.Cast<DataColumn>().Where(dc => dc.ColumnName != "CATALOG_ID" && dc.ColumnName != "FAMILY_ID" && dc.ColumnName != "PRODUCT_ID"))
            {
                table.Columns.Add(dc.ColumnName, dc.DataType);
            }
            foreach (DataRow dr in first.Rows)
            {
                DataRow[] supplierRow = null;
                if (dr["PRODUCT_ID"].ToString().Length > 0)
                {
                    supplierRow = second.Select("CATALOG_ID=" + dr["CATALOG_ID"] + " AND FAMILY_ID=" + dr["FAMILY_ID"] + " AND PRODUCT_ID=" + dr["PRODUCT_ID"]);
                }
                DataRow newRow = table.NewRow();

                for (int i = 0; i < first.Columns.Count; i++)
                {
                    newRow[i] = dr[i];
                }
                if (supplierRow != null && supplierRow.Length > 0)
                {
                    for (int i = 0; i < second.Columns.Count; i++)
                    {
                        if (second.Columns[i].ToString() != "CATALOG_ID" && second.Columns[i].ToString() != "FAMILY_ID" && second.Columns[i].ToString() != "PRODUCT_ID")
                        {
                            newRow[second.Columns[i].ColumnName] = supplierRow[0][second.Columns[i].ColumnName];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < second.Columns.Count; i++)
                    {
                        if (second.Columns[i].ToString() != "CATALOG_ID" && second.Columns[i].ToString() != "FAMILY_ID" && second.Columns[i].ToString() != "PRODUCT_ID")
                        {
                            newRow[second.Columns[i].ToString()] = DBNull.Value;
                        }
                    }
                }
                table.Rows.Add(newRow);

            }
            return table;

        }

        private DataSet ApplyStyleFormat(DataSet finalDs)
        {
            for (int mainCount = 0; mainCount < finalDs.Tables.Count; mainCount++)
            {
                for (int cName = 0; cName < finalDs.Tables[mainCount].Columns.Count; cName++)
                {
                    if (finalDs.Tables[mainCount].Columns[cName].ColumnName != "CATALOG_ID")
                    {
                        DataSet styleDs = new DataSet();
                        var oConn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                        {
                            string SQLString =
                                "SELECT STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_NAME = '" +
                                finalDs.Tables[mainCount].Columns[cName].ColumnName.Trim().Replace("'", "''") + "'";


                            SqlDataAdapter _DBAdapter = new SqlDataAdapter(SQLString, oConn);
                            _DBAdapter.SelectCommand.CommandTimeout = 0;
                            _DBAdapter.Fill(styleDs);

                            if (styleDs.Tables[0].Rows.Count > 0)
                            {
                                string style = styleDs.Tables[0].Rows[0].ItemArray[0].ToString();
                                if (style.Length > 0)
                                {
                                    int index = style.IndexOf("[", StringComparison.Ordinal);
                                    if (index != -1)
                                        style = style.Substring(0, index - 1);
                                    foreach (DataRow dr in finalDs.Tables[mainCount].Rows)
                                    {
                                        try
                                        {
                                            double dt = Convert.ToDouble(dr[finalDs.Tables[mainCount].Columns[cName].ColumnName]);
                                            dr[finalDs.Tables[mainCount].Columns[cName].ColumnName] = dt.ToString(style.Trim());
                                        }
                                        catch (Exception)
                                        {

                                        }
                                    }
                                }
                            }
                        };
                    }
                }
            }
            return finalDs;
        }
        private void ImagePathXpc()
        {
            _uPath = string.Empty;
            if (_userImagepath.Length == 2)
            {
                _userImagepath = _userImagepath + "\\";
            }

            foreach (char t in _userImagepath)
            {
                if (t == '\\')
                {
                    _uPath = _uPath + "\\\\";
                }
                else
                {
                    _uPath = _uPath + t;
                }
            }
        }

        private string ReplaceCharacters(string input)
        {
            byte[] ascii = Encoding.ASCII.GetBytes(input);
            string ret = string.Empty;
            for (int i = 0; i < ascii.Length; i++)
            {
                int a = ascii[i];
                if ((a >= 48 && a <= 57) || (a >= 65 && a <= 90) || (a >= 97 && a <= 122))
                {
                    ret += input[i];
                }
                else
                {
                    ret += "_";
                    ascii[i] = 95;
                }
            }
            return ret;
        }

        public int ProjectID { private get; set; }
        public int CatalogID { private get; set; }
        #endregion


        [System.Web.Http.HttpGet]
        public IList GetOpenPDFCatalogattributes(int catalogId)
        {
            try
            {
                int projectId = 52;
                XpressCatalog objXpress = new XpressCatalog();
                var c = GetCatalogAttr(catalogId, "simple");
                object context;
                if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                {
                    var httpContext = context as HttpContextBase;
                    if (httpContext != null && httpContext.Session != null)
                    {
                        StiReport frmStiReport = new StiReport();
                        frmStiReport.Load(httpContext.Session["TemplatePath"].ToString());
                        try
                        {
                            projectId = Convert.ToInt32(frmStiReport.Dictionary.Variables["ProjectID"].Value);
                        }
                        catch (NullReferenceException)
                        {

                        }
                    }
                }
                if (projectId == 0)
                {
                    return c;
                }
                else
                {
                    TB_PROJECT projectdetails = _dbcontext.TB_PROJECT.Find(projectId);
                    var pdfDetails = projectdetails.XPRESSCATALOG_CONFIG;
                    List<PDF_PROJECT_SETTINGS> objprojectSettings = objXpress.ProjectXmlDeserializefunction(pdfDetails);
                    string[] allcustomAttribute = null;
                    string[] allsystemAttribute = null;
                    foreach (var item in objprojectSettings)
                    {
                        string customAttribute = item.CUSTOM_ATTRIBUTE;
                        string systemAttribute = item.SYSTEM_ATTRIBUTE;
                        allcustomAttribute = customAttribute.Split(',');
                        allsystemAttribute = systemAttribute.Split(',');
                    }


                    for (int i = 0; i < allcustomAttribute.Count(); i++)
                    {
                        int attrid = 0;
                        int.TryParse(allcustomAttribute[i].Trim('~'), out attrid);
                        var obj = c.FirstOrDefault(x => x.ATTRIBUTE_ID == attrid);
                        if (obj != null)
                        {
                            c.Remove(obj);
                        }
                    }
                    for (int i = 0; i < allsystemAttribute.Count(); i++)
                    {
                        var obj = c.FirstOrDefault(x => x.ATTRIBUTE_NAME == allsystemAttribute[i]);
                        if (obj != null)
                        {
                            c.Remove(obj);
                        }
                    }
                    return c;
                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApi : GetOpenPDFCatalogattributes", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetOpenPDFSelectedattributes(int catalogId)
        {
            try
            {
                int projectId = 52;
                XpressCatalog objXpress = new XpressCatalog();
                String[] listofselectedattrs = new String[4];
                String[] listofamilyselectedattrs = new String[4];
                String[] listofproductselectedattrs = new String[4];

                String[] listofselectedattrs1 = { "CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_SHORT", "CATEGORY_NAME" };
                listofselectedattrs = listofselectedattrs1;
                String[] listofamilyselectedattrs1 = { "FAMILY_ID", "FAMILY_NAME", "SUBFAMILY_ID", "SUBFAMILY_NAME" };
                listofamilyselectedattrs = listofamilyselectedattrs1;
                String[] listofproductselectedattrs1 = { "PRODUCT_ID" };
                listofproductselectedattrs = listofproductselectedattrs1;


                var objqsCatalog = listofselectedattrs.Select(listofselectedattr => new QS_CATALOGATTRIBUTE_Result
                {
                    ATTRIBUTE_ID = 0,
                    ATTRIBUTE_TYPE = 0,
                    ISAvailable = 0,
                    ATTRIBUTE_NAME = listofselectedattr
                }).ToList();

                objqsCatalog.AddRange(listofamilyselectedattrs.Select(listofselectedattr => new QS_CATALOGATTRIBUTE_Result
                {
                    ATTRIBUTE_ID = 0,
                    ATTRIBUTE_TYPE = 2,
                    ISAvailable = 0,
                    ATTRIBUTE_NAME = listofselectedattr
                }));

                objqsCatalog.AddRange(listofproductselectedattrs.Select(listofselectedattr => new QS_CATALOGATTRIBUTE_Result
                {
                    ATTRIBUTE_ID = 0,
                    ATTRIBUTE_TYPE = 1,
                    ISAvailable = 0,
                    ATTRIBUTE_NAME = listofselectedattr
                }));
                object context;
                if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                {
                    var httpContext = context as HttpContextBase;
                    if (httpContext != null && httpContext.Session != null)
                    {
                        StiReport frmStiReport = new StiReport();
                        frmStiReport.Load(httpContext.Session["TemplatePath"].ToString());
                        try
                        {
                            projectId = Convert.ToInt32(frmStiReport.Dictionary.Variables["ProjectID"].Value);
                        }
                        catch (NullReferenceException)
                        {

                        }
                    }
                }
                if (projectId == 0)
                {
                    return objqsCatalog;
                }
                else
                {
                    TB_PROJECT projectdetails = _dbcontext.TB_PROJECT.Find(projectId);
                    var pdfDetails = projectdetails.XPRESSCATALOG_CONFIG;
                    List<PDF_PROJECT_SETTINGS> objprojectSettings = objXpress.ProjectXmlDeserializefunction(pdfDetails);
                    string[] allcustomAttribute = null;
                    string[] allsystemAttribute = null;
                    foreach (var item in objprojectSettings)
                    {
                        string customAttribute = item.CUSTOM_ATTRIBUTE;
                        string systemAttribute = item.SYSTEM_ATTRIBUTE;
                        allcustomAttribute = customAttribute.Split(',');
                        allsystemAttribute = systemAttribute.Split(',');
                    }

                    var c = GetCatalogAttr(catalogId, "simple");
                    for (int i = 0; i < allcustomAttribute.Count(); i++)
                    {
                        int attrid = 0;
                        int.TryParse(allcustomAttribute[i].Trim('~'), out attrid);
                        var objexistattr = objqsCatalog.FirstOrDefault(x => x.ATTRIBUTE_ID == attrid);
                        if (objexistattr == null)
                        {
                            var objselectedattr = c.FirstOrDefault(x => x.ATTRIBUTE_ID == attrid);
                            if (objselectedattr != null)
                            {
                                objqsCatalog.Add(objselectedattr);
                            }
                        }
                    }
                    for (int i = 0; i < allsystemAttribute.Count(); i++)
                    {
                        var objexistattr = objqsCatalog.FirstOrDefault(x => x.ATTRIBUTE_NAME == allsystemAttribute[i]);
                        if (objexistattr == null)
                        {
                            var objselectedattr = c.FirstOrDefault(x => x.ATTRIBUTE_NAME == allsystemAttribute[i]);
                            if (objselectedattr != null)
                            {
                                objqsCatalog.Add(objselectedattr);
                            }
                        }
                    }
                    return objqsCatalog;
                }

            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApi : GetOpenPDFSelectedattributes", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetOpenPDFSummary()
        {
            try
            {
                object context;
                if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                {
                    var httpContext = context as HttpContextBase;
                    if (httpContext != null && httpContext.Session != null)
                    {
                        StiReport frmStiReport = new StiReport();
                        frmStiReport.Load(httpContext.Session["TemplatePath"].ToString());
                        try
                        {
                            projectId = Convert.ToInt32(frmStiReport.Dictionary.Variables["ProjectID"].Value);
                        }
                        catch (NullReferenceException)
                        {

                        }
                    }
                }
                var c = new List<QS_CATALOGATTRIBUTE_Result>();
                TB_PROJECT projectdetails = _dbcontext.TB_PROJECT.Find(projectId);
                var pdfDetails = projectdetails.XPRESSCATALOG_CONFIG;
                List<PDF_PROJECT_SETTINGS> objprojectSettings = objXpress.ProjectXmlDeserializefunction(pdfDetails);
                return objprojectSettings;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApi : GetOpenPDFSummary", objException);
                return null;
            }
        }


        public HttpResponseMessage OpenUpdateXpressProject(JArray model)
        {
            try
            {
                object context;
                if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                {
                    var httpContext = context as HttpContextBase;
                    if (httpContext != null && httpContext.Session != null)
                    {
                        StiReport frmStiReport = new StiReport();
                        frmStiReport.Load(httpContext.Session["TemplatePath"].ToString());
                        try
                        {
                            projectId = Convert.ToInt32(frmStiReport.Dictionary.Variables["ProjectID"].Value);
                        }
                        catch (NullReferenceException)
                        {

                        }
                    }
                }
                selectedTreeNodes = model;
                var projectCount = _dbcontext.TB_PROJECT.Count(x => x.PROJECT_ID == projectId);
                if (projectCount <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
                }
                TB_PROJECT projectPDF = _dbcontext.TB_PROJECT.Find(projectId);
                string xpressRules = objXpress.OpenXpressConfig(projectId, model);
                if (xpressRules != string.Empty)
                {
                    xpressRules = xpressRules.Replace("encoding", "standalone");
                    xpressRules = xpressRules.Replace("utf-16", "yes");
                    projectPDF.XPRESSCATALOG_CONFIG = xpressRules;
                    _dbcontext.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at XpressCatalogApiController : OpenUpdateXpressProject", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [System.Web.Http.HttpGet]
        public HttpResponseMessage FindProjectType()
        {
            try
            {
                projectId = 0;
                object context;
                if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                {
                    var httpContext = context as HttpContextBase;
                    if (httpContext != null && httpContext.Session != null)
                    {
                        StiReport frmStiReport = new StiReport();
                        frmStiReport.Load(httpContext.Session["TemplatePath"].ToString());
                        try
                        {
                            if (frmStiReport.Dictionary.Variables.Contains("ProjectID"))
                                projectId = Convert.ToInt32(frmStiReport.Dictionary.Variables["ProjectID"].Value);
                            HttpContext.Current.Session["CURRENTPROJECTID"] = projectId;
                        }
                        catch (NullReferenceException)
                        {

                        }
                    }
                }
                if (projectId != 0)
                {
                    var c = new List<QS_CATALOGATTRIBUTE_Result>();
                    TB_PROJECT projectdetails = _dbcontext.TB_PROJECT.Find(projectId);
                    var pdfDetails = projectdetails.XPRESSCATALOG_CONFIG;
                    List<PDF_PROJECT_SETTINGS> objprojectSettings = objXpress.ProjectXmlDeserializefunction(pdfDetails);
                    if (objprojectSettings.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, objprojectSettings[0].PUBLICATION_TYPE);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Error");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Hierarchical Catalog");
                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XpressCatalogApi : GetOpenPDFSummary", objException);
                return Request.CreateResponse(HttpStatusCode.OK, "Error");
            }
        }


        // *******************************************       Methods moved from CatalogController  ***********************
    }
}