﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using log4net;
using LS.Data;
using System.IO;
using System.Data;
using Infragistics.Documents.Excel;
using System.Data.OleDb;
using LS.Web.Utility;
using System.Collections.Generic;
using LS.Data.Model.CatalogSectionModels;
using Newtonsoft.Json;

namespace LS.Web.Controllers
{
    public class PDFIndexController : Controller
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(CatalogController));
        readonly CSEntities _dbcontext = new CSEntities();
        private ImportController IC = new ImportController();
        HomeApiController homeController = new HomeApiController();
        XpressCatalogController obj_XpressCatalogController = new XpressCatalogController();
        DataTable logtable = new DataTable();
        DataTable IndeXpdflogtable = new DataTable();

        // GET: PDFIndex
        public ActionResult ExemptedWords()
        {
            return View();
        }

        public ActionResult ContentLibrary()
        {
            return View();
        }

        public ActionResult IndexGeneration()
        {
            return View();
        }

        public DataTable GetDataFromExcel(string SheetName, string excelPath, string query, string deleteflag)
        {
            DataTable dtexcel = new DataTable();
            DataTable dtexcelNew = new DataTable();
            try
            {
                DataTable dtColumns = new DataTable(); string ExcelSheetName, sheet = string.Empty;
                var fileName = Path.GetFileName(excelPath).Split('.')[0];
                var path = Path.GetFullPath(excelPath);
                var fileExtention = Path.GetExtension(excelPath);
                string excelPathCopy = path.Replace(Path.GetFileName(excelPath), "") + "//" + fileName + "_Copy" + "." + fileExtention;
                using (System.Data.OleDb.OleDbConnection conn = excelConnection(excelPath))
                {
                    DataTable schemaTable = conn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    for (int i = 0; i < 1; i++)
                    {
                        ExcelSheetName = schemaTable.Rows[i]["Table_Name"].ToString();
                        ExcelSheetName = ExcelSheetName.Replace("$", "");
                        if (ExcelSheetName == SheetName)
                        {
                            SheetName = ExcelSheetName;
                        }
                    }
                    query = " SELECT * from [" + SheetName + "$]";

                    if (!sheet.EndsWith("_"))
                    {

                        System.Data.OleDb.OleDbDataAdapter daexcel = new System.Data.OleDb.OleDbDataAdapter(query, conn);
                        dtexcel.Locale = System.Globalization.CultureInfo.CurrentCulture;


                        daexcel.Fill(dtColumns);

                    }
                    conn.Close();
                    conn.Dispose();
                    var workbook1 = new Workbook();
                    workbook1 = Workbook.Load(excelPath);

                }
                return dtColumns;
            }

            catch (Exception ex)
            {
                dtexcel = new DataTable();
                return dtexcel;
            }
        }



        #region Index PDF Import validation process
        public JsonResult ValidateProcess(string excelPath, string sheetName)
        {
            List<string> resultsReference = new List<string>();
            List<string> Message1 = new List<string>();
            try
            {
                List<string> validationResult = new List<string>();
                long returnTableId = 0;
                string fileName, actualFileName, Message;
                string FilePath = string.Empty;
                Message = fileName = actualFileName = string.Empty;
                DataTable objDatatable = GetDataFromExcel(sheetName, excelPath, " SELECT * from [" + sheetName + "$]", "");

                validationResult = IndexPdfvalidation(objDatatable, sheetName);
                List<string> nameList = Message1.Select(p => p.ToString()).ToList();
                resultsReference.Add(nameList.ToString());
                // }
                return Json(validationResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexController : ValidateProcess", objexception);
                return Json("Please upload valid file", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region Validation process
        public List<string> IndexPdfvalidation(DataTable dt, string sheetName)
        {
            DataTable dtt = new DataTable();
            List<string> validationResult = new List<string>();
            List<string> errorlistMissing = new List<string>();
            List<string> errorlistDuplicate = new List<string>();
            List<string> errorlistNewColumn = new List<string>();
            errorlistMissing = ValidateMissingColumn(dt, sheetName);
            errorlistDuplicate = ValidateDuplicateColumn(dt, sheetName);
            errorlistNewColumn = ValidateNewColumn(dt, sheetName);
            if (!(errorlistMissing.Count == 0))
                validationResult.Add("Missing Column");
            if (!(errorlistDuplicate.Count == 0))
                validationResult.Add("Duplicate");
            if (!(errorlistNewColumn.Count == 0))
                validationResult.Add("NewColumn");
            return validationResult;
        }

        #endregion

        #region Missing column validation
        public List<string> ValidateMissingColumn(DataTable dt, string sheetName)
        {
            var errorlist = new List<string>();
            IndeXpdflogtable.Columns.Add("ValidationType");
            IndeXpdflogtable.Columns.Add("Message");
            try
            {
                int totalRows = dt.Rows.Count;
                if (sheetName == "PDFCONTENT")
                {
                    for (int i = 0; i < totalRows; i++)
                    {
                        if (!dt.Columns.Contains("TEMPLATE_NAME"))
                        {
                            errorlist.Add("Missing-TEMPLATE_NAME");
                            return errorlist;
                        }
                    }
                }
                else if (sheetName == "KEYWORDS")
                {
                    if (totalRows==0)
                    {
                        IndeXpdflogtable.Rows.Add();
                        IndeXpdflogtable.Rows[IndeXpdflogtable.Rows.Count - 1]["ValidationType"] = "Sheet selection";
                        IndeXpdflogtable.Rows[IndeXpdflogtable.Rows.Count - 1]["Message"] = "Invalid sheet selected ";
                        errorlist.Add("Invalid sheet");
                        System.Web.HttpContext.Current.Session["IndexPDFvalidateLogTable"] = IndeXpdflogtable;
                        return errorlist;
                    }
                    for (int i = 0; i < totalRows; i++)
                    {
                        if (dt.Columns.Count == 4)
                        {
                            if (!dt.Columns.Contains("KEYWORD_LIST") || !dt.Columns.Contains("KEYWORD_NAME") || !dt.Columns.Contains("ID"))
                            {
                                IndeXpdflogtable.Rows.Add();
                                IndeXpdflogtable.Rows[IndeXpdflogtable.Rows.Count - 1]["ValidationType"] = "Missing Columns";
                                IndeXpdflogtable.Rows[IndeXpdflogtable.Rows.Count - 1]["Message"] = "Column 'KEYWORD_LIST' is missing ";
                                errorlist.Add("Missing-KEYWORD_LIST");
                                System.Web.HttpContext.Current.Session["IndexPDFvalidateLogTable"] = IndeXpdflogtable;
                                return errorlist;
                            }
                        }
                        else
                        {
                            IndeXpdflogtable.Rows.Add();
                            IndeXpdflogtable.Rows[IndeXpdflogtable.Rows.Count - 1]["ValidationType"] = "Missing Columns";
                            IndeXpdflogtable.Rows[IndeXpdflogtable.Rows.Count - 1]["Message"] = "Invalid sheet selected ";
                            errorlist.Add("Invalid sheet");
                            System.Web.HttpContext.Current.Session["IndexPDFvalidateLogTable"] = IndeXpdflogtable;
                            return errorlist;
                        }
                    }
                }
                return errorlist;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexController : ValidateMissingColumn", objexception);
                return errorlist;
            }
        }
        #endregion

        #region Duplicate Item
        public List<string> ValidateDuplicateColumn(DataTable dt, string sheetName)
        {
            var errorlist = new List<string>();

            try
            {
                string myresult = string.Empty;
                string duplicatevalues = string.Empty;
                int totalRows = dt.Rows.Count;
                DataView dv = dt.DefaultView;
                DataTable DT1 = new DataTable();
                int newcount = 0;
                int oldcount = 0;
                oldcount = dt.Rows.Count;
                //List the column names whose values are to be distinct
                if (sheetName == "PDFCONTENT")
                {
                    string[] Columns = { "ID", "PAGE_NO", "PAGE_DATA" };
                    DT1 = dv.ToTable(true, Columns);
                }
                else if (sheetName == "KEYWORDS")
                {
                    string[] Columns = { "KEYWORD_NAME" };
                    DT1 = dv.ToTable(true, Columns);
                }
                newcount = DT1.Rows.Count;
                if (oldcount != newcount)
                {
                    errorlist.Add("Dup-Duplicate records found in sheet");
                }

                var duplicate = dt.AsEnumerable().GroupBy(x => x["KEYWORD_NAME"]).Where(x => x.Count() > 1).Select(g => g.Key);
                foreach (var item in duplicate)
                {
                    myresult = item.ToString();
                    if (duplicatevalues != "")
                        duplicatevalues = duplicatevalues + "," + myresult;
                    else
                        duplicatevalues = myresult;


                }

                if (duplicate != null && duplicate.Count() > 0)
                {
                    IndeXpdflogtable.Rows.Add();
                    IndeXpdflogtable.Rows[IndeXpdflogtable.Rows.Count - 1]["ValidationType"] = "Duplicate Items";
                    IndeXpdflogtable.Rows[IndeXpdflogtable.Rows.Count - 1]["Message"] = duplicatevalues + "  " + "are duplicated.";
                    System.Web.HttpContext.Current.Session["IndexPDFvalidateLogTable"] = IndeXpdflogtable;
                }
                return errorlist;

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexController : ValidateDuplicateColumn", objexception);
                return errorlist;
            }
        }
        #endregion

        #region New column Item
        public List<string> ValidateNewColumn(DataTable dt, string sheetName)
        {
            var errorlist = new List<string>();

            try
            {
                int count = dt.Columns.Count;
                string[] ColumnsPDFCONTENT = { "TEMPLATE_NAME", "ID", "PAGE_NO", "PAGE_DATA", "Action" };
                string[] ColumnsKEYWORDS = { "Action", "ID", "KEYWORD_NAME", "KEYWORD_LIST" };
                if (sheetName == "PDFCONTENT")
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        string checkColumn = dt.Columns[i].ColumnName.ToString();
                        if (!ColumnsPDFCONTENT.Contains(checkColumn))
                        {
                            errorlist.Add("Newcolumn");
                            return errorlist;
                        }
                    }
                    //if (count != ColumnsPDFCONTENT.Count())
                    //{ 
                    //    errorlist.Add("Newcolumn");
                    //    return errorlist;
                    //}
                }
                else if (sheetName == "KEYWORDS")
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        string checkColumn = dt.Columns[i].ColumnName.ToString();
                        if (!ColumnsKEYWORDS.Contains(checkColumn))
                        {
                            IndeXpdflogtable.Rows.Add();
                            IndeXpdflogtable.Rows[IndeXpdflogtable.Rows.Count - 1]["ValidationType"] = "New Column";
                            IndeXpdflogtable.Rows[IndeXpdflogtable.Rows.Count - 1]["Message"] = "New Column '" + checkColumn + "' is found in sheet";
                            errorlist.Add("Newcolumn");
                            System.Web.HttpContext.Current.Session["IndexPDFvalidateLogTable"] = IndeXpdflogtable;
                            errorlist.Add("Newcolumn");
                            return errorlist;
                        }
                    }
                    //if (count != ColumnsKEYWORDS.Count())
                    //{
                    //    errorlist.Add("Newcolumn");
                    //    return errorlist;
                    //}
                }
                return errorlist;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexController : ValidateNewColumn", objexception);
                return errorlist;
            }
        }
        #endregion

        public DataTable GetDataFromExcel1(string SheetName, string excelPath, string query, string deleteflag)
        {
            DataTable dtexcel = new DataTable();
            DataTable dtexcelNew = new DataTable();
            try
            {
                var fileName = Path.GetFileName(excelPath).Split('.')[0];
                var path = Path.GetFullPath(excelPath);
                var fileExtention = Path.GetExtension(excelPath);
                string excelPathCopy = path.Replace(Path.GetFileName(excelPath), "") + "//" + fileName + "_Copy" + "." + fileExtention;
                using (System.Data.OleDb.OleDbConnection conn = excelConnection(excelPath))
                {
                    DataTable schemaTable = conn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    string ExcelSheetName = schemaTable.Rows[0]["Table_Name"].ToString();
                    DataTable dtColumns = new DataTable();
                    SheetName = ExcelSheetName;
                    SheetName = SheetName.Replace("$", "");
                    try
                    {

                        DataRow[] schemaRow = schemaTable.Select("TABLE_NAME like '" + SheetName + "%'");
                        string sheet = schemaRow[0]["TABLE_NAME"].ToString();
                        int selectedIndex = 0;
                        foreach (DataRow dRow in schemaTable.Rows)
                        {

                            if (sheet.ToUpper() == dRow["TABLE_NAME"].ToString().ToUpper())
                                break;
                            selectedIndex++;
                        }
                        query = " SELECT * from [" + SheetName + "$]";

                        if (!sheet.EndsWith("_"))
                        {

                            System.Data.OleDb.OleDbDataAdapter daexcel = new System.Data.OleDb.OleDbDataAdapter(query, conn);
                            dtexcel.Locale = System.Globalization.CultureInfo.CurrentCulture;


                            daexcel.Fill(dtColumns);

                        }
                        conn.Close();
                        conn.Dispose();
                        var workbook1 = new Workbook();
                        workbook1 = Workbook.Load(excelPath);
                        //foreach (WorksheetColumn colSheet in workbook1.Worksheets[1].Columns)
                        //    dt11.Columns.Add(colSheet.ToString());
                        dtexcel = dtColumns.Clone();
                        bool skipFirstRow = false;
                        if (!deleteflag.ToUpper().Contains("DELETE"))
                        {
                            foreach (WorksheetRow rowSheet in workbook1.Worksheets[selectedIndex].Rows)
                            {
                                if (dtColumns.Columns.Contains("ACTION") && !skipFirstRow)
                                {
                                    skipFirstRow = true;
                                    continue;
                                }
                                DataRow dRow = dtexcel.NewRow();
                                for (int col = 0; col < dtexcel.Columns.Count; col++)
                                    dRow[col] = rowSheet.Cells[col].Value == null ? null : rowSheet.Cells[col].Value.ToString();
                                dtexcel.Rows.Add(dRow);
                            }
                        }
                        else
                        {
                            dtexcel = new DataTable();
                            dtexcel = dtColumns;
                            // objLS = new CSEntities();
                        }


                        if (dtexcel.Rows[0][0] == "") // For indesging import change made here only
                        {
                            dtexcel.Rows.RemoveAt(0);
                        }


                        workbook1.Worksheets.Clear();
                    }
                    catch (Exception ex)
                    {
                        dtexcel = new DataTable();
                        dtexcel = dtColumns;
                    }
                }
                return dtexcel;
            }

            catch (Exception ex)
            {
                dtexcel = new DataTable();
                return dtexcel;
            }
        }

        #region ExtemptList PDF Import validation process
        public JsonResult ValidateExemptsheet()
        {
            List<string> resultsReference = new List<string>();
            List<string> Message1 = new List<string>();
            try
            {
                List<string> validationResult = new List<string>();
                long returnTableId = 0;
                string fileName, actualFileName, Message;
                string FilePath = string.Empty;
                Message = fileName = actualFileName = string.Empty;
                if (Request.Files != null)
                {

                    var file = Request.Files[0];
                    actualFileName = file.FileName;
                    fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    int size = file.ContentLength;
                    //  try
                    //  {
                    string physicalPath = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName);
                    file.SaveAs(physicalPath);
                    DataTable objDatatable = GetDataFromExcel1(actualFileName, physicalPath, " SELECT * from [" + actualFileName + "$]", "");

                    validationResult = Extemptvalidation(objDatatable);
                    List<string> nameList = Message1.Select(p => p.ToString()).ToList();
                    resultsReference.Add(nameList.ToString());
                }
                return Json(validationResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexController : ValidateExemptsheet", objexception);
                return Json("Please upload valid file", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Extempt Validation process
        public List<string> Extemptvalidation(DataTable dt)
        {
            DataTable dtt = new DataTable();
            List<string> validationResult = new List<string>();
            List<string> errorlistMissingExtempt = new List<string>();
            List<string> errorlistDuplicateExtempt = new List<string>();
            List<string> errorlistNewColumnExtempt = new List<string>();
            errorlistMissingExtempt = ValidateMissingColumnExtempt(dt);
            errorlistDuplicateExtempt = ValidateDuplicateColumnExtempt(dt);
            errorlistNewColumnExtempt = ValidateNewColumnExtempt(dt);
            if (!(errorlistMissingExtempt.Count == 0))
                validationResult.Add("Missing Column");
            if (!(errorlistDuplicateExtempt.Count == 0))
                validationResult.Add("Duplicate");
            if (!(errorlistNewColumnExtempt.Count == 0))
                validationResult.Add("NewColumn");
            return validationResult;
        }

        #endregion

        #region Extempt Missing column validation
        public List<string> ValidateMissingColumnExtempt(DataTable dt)
        {
            var errorlist = new List<string>();

            try
            {
                int totalRows = dt.Rows.Count;

                logtable.Columns.Add("ValidationType");
                logtable.Columns.Add("Message");
                if (totalRows == 0)
                {
                    logtable.Rows.Add();
                    logtable.Rows[logtable.Rows.Count - 1]["ValidationType"] = "Sheet selection";
                    logtable.Rows[logtable.Rows.Count - 1]["Message"] = "Invalid sheet selected ";
                    errorlist.Add("Invalid sheet");
                    System.Web.HttpContext.Current.Session["IndexPDFvalidateLogTable"] = IndeXpdflogtable;
                    return errorlist;
                }
                for (int i = 0; i < totalRows; i++)
                {
                    if (dt.Columns.Count == 3)
                    {
                        if (!dt.Columns.Contains("WORD") || !dt.Columns.Contains("ID"))
                        {
                            logtable.Rows.Add();
                            logtable.Rows[i]["ValidationType"] = "Missing Column";
                            logtable.Rows[i]["Message"] = "Column 'Word'  is missing";
                            errorlist.Add("Missing-WORD");
                            System.Web.HttpContext.Current.Session["IndexPDFLogTable"] = logtable;
                            return errorlist;
                        }
                    }
                    else
                    {
                        logtable.Rows.Add();
                        logtable.Rows[logtable.Rows.Count - 1]["ValidationType"] = "Sheet selection";
                        logtable.Rows[logtable.Rows.Count - 1]["Message"] = "Invalid sheet selected ";
                        errorlist.Add("Invalid sheet");
                        System.Web.HttpContext.Current.Session["IndexPDFvalidateLogTable"] = IndeXpdflogtable;
                        return errorlist;
                    }
                }
                return errorlist;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexController : ValidateMissingColumnExtempt", objexception);
                return errorlist;
            }
        }
        #endregion

        #region Extempt Duplicate Item
        public List<string> ValidateDuplicateColumnExtempt(DataTable dt)
        {
            var errorlist = new List<string>();

            try
            {
                string myresult = string.Empty;
                string duplicatevalues = string.Empty;
                int totalRows = dt.Rows.Count;
                DataView dv = dt.DefaultView;
                DataTable DT1 = new DataTable();
                int newcount = 0;
                int oldcount = 0;
                oldcount = dt.Rows.Count;
                //List the column names whose values are to be distinct

                var dupli = dt.AsEnumerable().GroupBy(x => x["WORD"]).Where(x => x.Count() > 1).Select(g => g.Key).ToList();
                foreach (var item in dupli)
                {
                    myresult = item.ToString();
                    if (duplicatevalues != "")
                        duplicatevalues = duplicatevalues + "," + myresult;
                    else
                        duplicatevalues = myresult;


                }
                if (dupli != null && dupli.Count() > 0)
                {
                    logtable.Rows.Add();
                    logtable.Rows[logtable.Rows.Count - 1]["ValidationType"] = "Duplicate Items";
                    logtable.Rows[logtable.Rows.Count - 1]["Message"] = duplicatevalues + "  " + "are duplicated.";
                    System.Web.HttpContext.Current.Session["IndexPDFLogTable"] = logtable;
                }
                string[] Columns = { "WORD", "ID" };
                DT1 = dv.ToTable(true, Columns);
                newcount = DT1.Rows.Count;
                if (oldcount != newcount)
                {
                    errorlist.Add("Dup-Duplicate records found in sheet");
                }
                return errorlist;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexController : ValidateDuplicateColumnExtempt", objexception);
                return errorlist;
            }
        }
        #endregion

        #region Extempt New column Item
        public List<string> ValidateNewColumnExtempt(DataTable dt)
        {
            var errorlist = new List<string>();

            try
            {
                int count = dt.Columns.Count;
                string[] Columns = { "Action", "ID", "WORD" };
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    string temp2 = dt.Columns[i].ColumnName.ToString();
                    if (!Columns.Contains(temp2))
                    {
                        logtable.Rows.Add();
                        logtable.Rows[logtable.Rows.Count - 1]["ValidationType"] = "New Column";
                        logtable.Rows[logtable.Rows.Count - 1]["Message"] = "New Column '" + temp2 + "' is found in sheet";
                        errorlist.Add("Newcolumn");
                        System.Web.HttpContext.Current.Session["IndexPDFLogTable"] = logtable;
                        return errorlist;
                    }
                }
                //if (count != Columns.Count())
                //    {
                //        errorlist.Add("Newcolumn");
                //        return errorlist;
                //    }
                return errorlist;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexController : ValidateNewColumnExtempt", objexception);
                return errorlist;
            }
        }
        #endregion



        public JsonResult ExemptViewLog()
        {
            try
            {
                DashBoardModel model = new DashBoardModel();
                var LogTable = (DataTable)System.Web.HttpContext.Current.Session["IndexPDFLogTable"];
                foreach (DataColumn columns in LogTable.Columns)
                {
                    var objPTColumns = new PTColumns();
                    objPTColumns.Caption = columns.Caption;
                    objPTColumns.ColumnName = columns.ColumnName;
                    model.Columns.Add(objPTColumns);
                }
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(LogTable);
                model.Data = JSONString;
                return new JsonResult() { Data = model };
            }
            catch (Exception ex)
            {
                Logger.Error("Error at RoleViewLog ", ex);
                return null;
            }

        }

        public JsonResult IndexpdfvalidationViewLog()
        {
            try
            {
                DashBoardModel model = new DashBoardModel();


                var IndexpdfLogTable = (DataTable)System.Web.HttpContext.Current.Session["IndexPDFvalidateLogTable"];
                foreach (DataColumn columns in IndexpdfLogTable.Columns)
                {
                    var objPTColumns = new PTColumns();
                    objPTColumns.Caption = columns.Caption;
                    objPTColumns.ColumnName = columns.ColumnName;
                    model.Columns.Add(objPTColumns);
                }
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(IndexpdfLogTable);
                model.Data = JSONString;
                return new JsonResult() { Data = model };
            }
            catch (Exception ex)
            {
                Logger.Error("Error at RoleViewLog ", ex);
                return null;
            }

        }

        public System.Data.OleDb.OleDbConnection excelConnection(string excelPath)
        {
            bool hasHeaders = true;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                          ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            else
                //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                         ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection(strConn);
            conn.Open();
            var workbook1 = new Workbook();
            string sheetName;
            foreach (Microsoft.Office.Interop.Excel.Worksheet wSheet in workbook1.Worksheets)
            {
                sheetName = wSheet.Name.ToString();
            }
            return conn;
        }

        #region ImportIndexPdf process
        private string Message;
        public string ImportIndexPdf(string excelPath, string sheetName)
        {
            try
            {
                //Logger.Info("Inside  at ImportIndexPdf : 1");
                DataTable dt = new DataTable();
                string fileName, actualFileName;
                string FilePath = string.Empty;
                Message = fileName = actualFileName = string.Empty;
                string physicalPath = string.Empty;
                string strConn;
                DataSet ds = new DataSet();
                string SheetName = string.Empty;
                //Logger.Info("Inside  at ImportIndexPdf : 2");
                if (excelPath != null)
                {

                    FileInfo file = new FileInfo(excelPath);
                    string Extention = file.Extension;
                    var file_name = sheetName;
                    actualFileName = file_name;
                    fileName = Guid.NewGuid() + Extention;
                    long size = file.Length;
                    //Logger.Info("Inside  at ImportIndexPdf : 3");
                    physicalPath = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName);
                    file.CopyTo(physicalPath);
                    AdvanceImportApiController adImport = new AdvanceImportApiController();
                    dt = GetDataFromExcel(actualFileName, physicalPath, " SELECT * from [" + actualFileName + "$]", "");
                    //Logger.Info("Inside  at ImportIndexPdf : " + dt.Rows.Count + "Rows");
                    //Logger.Info("Inside  at ImportIndexPdf : 4");
                    bool hasHeaders = true;
                    //Logger.Info("Inside  at ImportIndexPdf : 4.1");
                    string HDR = hasHeaders ? "Yes" : "No";
                    //Logger.Info("Inside  at ImportIndexPdf : 4.2");
                    if (excelPath.ToLower().Contains(".xlsx"))
                    {
                        //Logger.Info("Inside  at ImportIndexPdf : 4.3");
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + physicalPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    }
                    else
                    {
                        //Logger.Info("Inside  at ImportIndexPdf : 4.4");
                        //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + physicalPath +
                        //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + physicalPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    }
                    //Logger.Info("Inside  at ImportIndexPdf : 4.5");
                    OleDbConnection conn = new OleDbConnection(strConn);
                    //Logger.Info("Inside  at ImportIndexPdf : 4.6");
                    conn.Open();
                    //Logger.Info("Inside  at ImportIndexPdf : 5");
                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,
                        new object[] { null, null, null, "TABLE" });
                    DataRow schemaRow = schemaTable.Rows[0];
                    string ExcelSheetName = schemaRow["Table_Name"].ToString();
                    //Logger.Info("Inside  at ImportIndexPdf : 6");
                    if (ExcelSheetName.Length > 0)
                        SheetName = ExcelSheetName.Replace("$", "");
                    else
                        SheetName = actualFileName;
                    //if (ExcelSheetName == actualFileName)
                    //{
                    //    SheetName = ExcelSheetName;
                    //}                    
                    //SheetName = schemaRow["TABLE_NAME"].ToString();
                }
                string importTemp = string.Empty;
                string SQLStringCheck = string.Empty;
                string SQLStringCreate = string.Empty;
                importTemp = Guid.NewGuid().ToString();
                _dbcontext.Database.CommandTimeout = 0;
                string action = string.Empty;
                int totalRows = dt.Rows.Count;
                //Logger.Info("Inside  at ImportIndexPdf : " + totalRows +"Rows");
                DataSet resultSet1 = new DataSet();
                //Logger.Info("Inside  at ImportIndexPdf : 7");
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    //Logger.Info("Inside  at ImportIndexPdf : 7.1");
                    objSqlConnection.Open();
                    //Logger.Info("Inside  at ImportIndexPdf : 7.2");
                    SQLStringCheck =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    //Logger.Info("Inside  at ImportIndexPdf : 7.3");
                    SqlCommand _DBCommandnewCheck = new SqlCommand(SQLStringCheck, objSqlConnection);
                    //Logger.Info("Inside  at ImportIndexPdf : 7.4");
                    _DBCommandnewCheck.ExecuteNonQuery();
                    //Logger.Info("Inside  at ImportIndexPdf : 7.5");
                    SQLStringCreate = IC.CreateTable("[##IMPORTTEMP" + importTemp + "]", physicalPath, SheetName, dt);
                    //Logger.Info("Inside  at ImportIndexPdf : 7.6");
                    SqlCommand _DBCommandnewCreate = new SqlCommand(SQLStringCreate, objSqlConnection);
                    //Logger.Info("Inside  at ImportIndexPdf : 7.7");
                    _DBCommandnewCreate.ExecuteNonQuery();
                    //Logger.Info("Inside  at ImportIndexPdf : 7.8");
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    //Logger.Info("Inside  at ImportIndexPdf : 7.9");
                    bulkCopy.WriteToServer(dt);
                    //Logger.Info("Inside  at ImportIndexPdf : 8");
                    var sqlstring1exist =
                      new SqlCommand(
                          "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''tempresult" +
                          importTemp + "'')BEGIN DROP TABLE [tempresult" + importTemp + "] END')", objSqlConnection);
                    sqlstring1exist.ExecuteNonQuery();                    

                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO_INDEXPDF";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.AddWithValue("@SID", importTemp);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(objSqlCommand);                    
                    sqlDataAdapter.Fill(resultSet1);
                    //Logger.Info("Inside  at ImportIndexPdf : 9");
                    IC._SQLString = @"select * from [##LOGTEMPTABLE" + importTemp + "]";
                    ds = IC.CreateDataSet();
                    IC._SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp +
                                    "]";                  

                    objSqlCommand.CommandText = IC._SQLString;
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.ExecuteNonQuery();
                    objSqlConnection.Close();
                    //Logger.Info("Inside  at ImportIndexPdf : 10");
                    if (resultSet1.Tables[0].Rows.Count > 0 && resultSet1.Tables[1].Rows.Count > 0 && resultSet1.Tables[2].Rows.Count > 0)
                    {
                        string insertedCount = resultSet1.Tables[0].Rows[0][0].ToString();
                        string updatedCount = resultSet1.Tables[1].Rows[0][0].ToString();
                        string deletedCount = resultSet1.Tables[2].Rows[0][0].ToString(); ;
                        return "Import success~" + importTemp + "~" + insertedCount + "~" + updatedCount + "~" + deletedCount;

                    }
                    else
                    {
                        return "Import Failed~" + importTemp;
                    }                    
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                //throw raise;
                Logger.Error("Error at CatalogController :ImportIndesgin", dbEx);
                return null;
            }
        }
        #endregion

        
        private string Message1;

        #region Import ExcemptSheet

        public string ImportExemptsheet()
        {
            try
            {

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                string fileName, actualFileName;
                string FilePath = string.Empty;
                Message1 = fileName = actualFileName = string.Empty;
                string physicalPath = string.Empty;
                string strConn;
                string SheetName = string.Empty;

                if (Request.Files != null)
                {

                    var file = Request.Files[0];
                    actualFileName = file.FileName;
                    fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    int size = file.ContentLength;
                    physicalPath = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName);
                    file.SaveAs(physicalPath);
                    AdvanceImportApiController adImport = new AdvanceImportApiController();
                    dt = GetDataFromExcel1(actualFileName, physicalPath, " SELECT * from [" + actualFileName + "$]", "");
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    if (physicalPath.Substring(physicalPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + physicalPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    else
                        //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + physicalPath +
                        //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + physicalPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,
                        new object[] { null, null, null, "TABLE" });
                    DataRow schemaRow = schemaTable.Rows[0];
                    SheetName = schemaRow["TABLE_NAME"].ToString();
                }
                string importTemp, SQLString = string.Empty;
                importTemp = Guid.NewGuid().ToString();
                _dbcontext.Database.CommandTimeout = 0;
                string action = string.Empty;
                int totalRows = dt.Rows.Count;
                DataSet resultSet1 = new DataSet();
                using (
                      var objSqlConnection =
                          new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SQLString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    SQLString = IC.CreateTable("[##IMPORTTEMP" + importTemp + "]", physicalPath, SheetName, dt);
                    SqlCommand _DBCommandnew = new SqlCommand(SQLString, objSqlConnection);
                    _DBCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(dt);

                    var sqlstring1exist =
                      new SqlCommand(
                          "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''tempresult" +
                          importTemp + "'')BEGIN DROP TABLE [tempresult" + importTemp + "] END')", objSqlConnection);
                    sqlstring1exist.ExecuteNonQuery();

                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_EXCEMPTLIST";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.AddWithValue("@SID", importTemp);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    sqlDataAdapter.Fill(resultSet1);
                    //---------------------------------LOG DISPLAY-------------------------------//
                    IC._SQLString = @"select * from [##LOGTEMPTABLE" + importTemp + "]";
                    ds = IC.CreateDataSet();
                    IC._SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp +
                                    "]";

                    objSqlCommand.CommandText = IC._SQLString;
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.ExecuteNonQuery();
                    //---------------------------------LOG DISPLAY-------------------------------//

                    objSqlConnection.Close();
                    if (resultSet1.Tables[0].Rows.Count > 0)
                    {
                        return "Import Failed~" + importTemp;
                    }
                    else
                    {
                        string insertedCount = resultSet1.Tables[1].Rows[0][0].ToString();
                        string updatedCount = resultSet1.Tables[2].Rows[0][0].ToString();
                        string deleteCount = resultSet1.Tables[3].Rows[0][0].ToString();
                        return "Import success~" + importTemp + "~" + insertedCount + "~" + updatedCount + "~" + deleteCount;
                    }
                }
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message1 = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message1, raise);
                    }
                }
                throw raise;
                //Logger.Error("Error at CatalogController :ImportIndesgin", objException);
                return null;
            }
        }

        #endregion
    }
}