﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using Kendo.DynamicLinq;
using LS.Data;
using LS.Data.Model.CatalogSectionModels;
using log4net;
using LS.Data.Model;
using System.Web.Http;
using System.Collections;
using System.Net.Http;
using System.Net;
using LS.Data.Utilities;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using LS.Data.Model.ProductPreview;
using System.Globalization;
using System.Data.Entity.Core.Objects;

namespace LS.Web.Controllers
{
    public class RecycleBinApiController : ApiController
    {
        static ILog _logger = LogManager.GetLogger(typeof(RecycleBinApiController));
        
        CSEntities objLS = new CSEntities();
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        } 

        [HttpGet]
        public IHttpActionResult GetRecycleBinDataSource(string SelectType, string UserId, string FromDate, string ToDate)
        {
            try
            {
                //if (!selectType.IsNullOrWhiteSpace() && !userId.IsNullOrWhiteSpace() && !fromDate.IsNullOrWhiteSpace() &&
                //    !toDate.IsNullOrWhiteSpace())
                //{
                
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    if (FromDate.IsNullOrWhiteSpace())
                        FromDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd") + " 00:00:00";
                    if (ToDate.IsNullOrWhiteSpace())
                        ToDate = DateTime.Now.ToString("yyyy-MM-dd") + " 23:59:59";
                    if (UserId.IsNullOrWhiteSpace())
                        UserId = User.Identity.Name;
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_TRASH_ITEMS";
                    objSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    if (string.IsNullOrEmpty(SelectType))
                        SelectType = "All";
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = SelectType;
                    objSqlCommand.Parameters.Add("@USER_ID", SqlDbType.NVarChar, 100).Value = UserId;
                    objSqlCommand.Parameters.Add("@FROM_DATE", SqlDbType.NVarChar, 100).Value = FromDate;
                    objSqlCommand.Parameters.Add("@TO_DATE", SqlDbType.NVarChar, 100).Value = ToDate;
                    var dt = new DataTable();
                    objSqlConnection.Open();
                    var myList = new List<RecycledItems>();
                    using (SqlDataReader reader = objSqlCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var itemSearch = new RecycledItems();
                            itemSearch.RESTORE = Convert.ToInt32(reader["RESTORE"]);
                            itemSearch.XML_ID = Convert.ToInt32(reader["XML_ID"]);
                            itemSearch.ROOT_CATEGORY_ID = Convert.ToString(reader["ROOT_CATEGORY_ID"]);
                            itemSearch.CATEGORY_SHORT = Convert.ToString(reader["CATEGORY_SHORT"]);
                            itemSearch.REMOVED_CATEGORY_NAME = Convert.ToString(reader["REMOVED_CATEGORY_NAME"]);
                            itemSearch.REMOVED_FAMILY = Convert.ToString(reader["REMOVED_FAMILY"]);
                            itemSearch.REMOVED_SUB_FAMILY = Convert.ToString(reader["REMOVED_SUB_FAMILY"]);
                            itemSearch.REMOVED_PRODUCT = Convert.ToString(reader["REMOVED_PRODUCT"]);
                            itemSearch.ATTRIBUTE_ID = string.IsNullOrEmpty(Convert.ToString(reader["ATTRIBUTE_ID"]))
                                ? 0
                                : Convert.ToInt32(reader["ATTRIBUTE_ID"]);
                            itemSearch.ATTRIBUTE_NAME = Convert.ToString(reader["ATTRIBUTE_NAME"]);
                            itemSearch.DELETED_USER = Convert.ToString(reader["DELETED_USER"]);
                            itemSearch.ACTION_DELETED_DATEADD = Convert.ToString(String.Format("{0:g}", reader["ACTION_DELETED_DATEADD"]));  //  item.Field<string>("ACTION_DELETED_DATEADD"),
                            itemSearch.CATALOG_ID = Convert.ToInt32(reader["CATALOG_ID"]);
                            itemSearch.FAMILY_ID = Convert.ToInt32(reader["FAMILY_ID"]);
                            itemSearch.SUBFAMILY_ID = Convert.ToInt32(reader["SUB_FAMILY_ID"]);
                            itemSearch.PRODUCT_ID = reader["PRODUCT_ID"] != null ? !string.IsNullOrEmpty(reader["PRODUCT_ID"].ToString()) ? Convert.ToInt32(reader["PRODUCT_ID"]) : 0 : 0;
                            itemSearch.SUBPRODUCT_ID = Convert.ToInt32(reader["SUBPRODUCT_ID"]);
                            itemSearch.RECYCLE_TYPE = Convert.ToString(reader["RECYCLE_TYPE"]);
                            myList.Add(itemSearch);
                        }
                        // dt.Load(reader);

                    }
                    myList = myList.GroupBy(x => x.XML_ID).Select(x => x.FirstOrDefault()).ToList();
                    return Ok(myList);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at GetRecycleBinDataSource", ex);
                return null;
            }
            //return null;
        }
        [System.Web.Http.HttpPost]
        public string RecycleBinRestoreSelectedItemsdetails(object model)
        {
            string result = "0";
             
            int userProductCount = 0;
            int userSKUProductCount = 0;

            try
            {


                var restoreSelectedItems = ((JArray)model).Select(x => x).ToList();
                string Arr_XML_ID = "0";

                foreach (var items in restoreSelectedItems)
                {
                    if (items["RESTORE"].ToString().ToUpper() == "TRUE")
                    {
                        Arr_XML_ID = Arr_XML_ID + "," + Convert.ToString(items["XML_ID"].ToString());
                        if (result=="0")
                            result = "1";//Convert.ToString(items["RESTORE"]);
                    }
                    
                }
               // string[] XML_IDs = Arr_XML_ID.Split(',');
                IEnumerable<int> XML_IDs = Arr_XML_ID.Split(',').Select(x => int.Parse(x));
         
                //= objLS.TB_RECYCLE_TABLE.Where(x => x.PRODUCT_ID != null && XML_IDs.Contains(x.XML_ID)).Select(x => new { x.PRODUCT_ID }).Distinct().Count();
                //int restoreprodcnt =  objLS.STP_LS_RESTORE_PRODUCTS_COUNT(Arr_XML_ID);
                int restoreprodcnt = 0;// 
                DataTable objDtRestoreList = new DataTable();
                using (
                          var objSqlConnection =
                              new SqlConnection(
                                  ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_RESTORE_PRODUCTS_COUNT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@XML_ID", SqlDbType.NVarChar).Value = Arr_XML_ID;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDtRestoreList);
                }

                if (objDtRestoreList != null)
                {
                    restoreprodcnt = Convert.ToInt16(objDtRestoreList.Rows[0][0]);
                }
                var skucnt = objLS.TB_PLAN
                           .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                           .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                           .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => x.tcptps.tps.SKU_COUNT);
                var SKUcount = skucnt.Select(a => a).ToList();
                if (SKUcount.Any())
                {
                    userSKUProductCount = Convert.ToInt32(SKUcount[0]);
                }

                var productcount = objLS.STP_LS_GET_PRODUCTS_BY_USER("PRODUCT", User.Identity.Name, "").ToList();

                if (productcount.Any())
                {
                    userProductCount = Convert.ToInt32(productcount[0]);
                }

                if ((userProductCount + restoreprodcnt) > userSKUProductCount)
                {

                    return "SKU Exceed";
                }

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_RESTORE_DELETED_ITEMS";
                    objSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@XML_ID", SqlDbType.NVarChar, 5000).Value = Arr_XML_ID;
                    objSqlCommand.Parameters.Add("@USER_ID", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.ExecuteNonQuery();

                    SqlCommand objSqlCommand1 = objSqlConnection.CreateCommand();
                    objSqlCommand1.CommandText = "STP_LS_RESTORE_DELETED_ATTRIBUTES";
                    objSqlCommand1.CommandType = System.Data.CommandType.StoredProcedure;
                    objSqlCommand1.Connection = objSqlConnection;
                    objSqlCommand1.Parameters.Add("@XML_ID", SqlDbType.NVarChar, 5000).Value = Arr_XML_ID;
                    objSqlCommand1.Parameters.Add("@USER_ID", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand1.CommandTimeout = 0;
                    objSqlCommand1.ExecuteNonQuery();


                }
                return result;
                // return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Delete Catalog", ex);
                return null;
                //  return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        [System.Web.Http.HttpPost]
        public string RecycleBinRestoreSelectedTrashItemsdetails(object model)
        {
            string result = "0";

            int userProductCount = 0;
            int userSKUProductCount = 0;

            try
            {


                var restoreSelectedItems = ((JArray)model).Select(x => x).ToList();
                string Arr_XML_ID = "0";

                foreach (var items in restoreSelectedItems)
                {
                    if (items["RESTORE"].ToString().ToUpper() == "TRUE")
                    {
                        Arr_XML_ID = Arr_XML_ID + "," + Convert.ToString(items["XML_ID"].ToString());
                        if (result == "0")
                            result = "1";//Convert.ToString(items["RESTORE"]);
                    }

                }
                // string[] XML_IDs = Arr_XML_ID.Split(',');
                IEnumerable<int> XML_IDs = Arr_XML_ID.Split(',').Select(x => int.Parse(x));

                //= objLS.TB_RECYCLE_TABLE.Where(x => x.PRODUCT_ID != null && XML_IDs.Contains(x.XML_ID)).Select(x => new { x.PRODUCT_ID }).Distinct().Count();
                //int restoreprodcnt =  objLS.STP_LS_RESTORE_PRODUCTS_COUNT(Arr_XML_ID);
                int restoreprodcnt = 0;// 
                DataTable objDtRestoreList = new DataTable();
                using (
                          var objSqlConnection =
                              new SqlConnection(
                                  ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_RESTORE_PRODUCTS_COUNT_FOR_TRASH";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@XML_ID", SqlDbType.NVarChar).Value = Arr_XML_ID;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDtRestoreList);
                }

                if (objDtRestoreList != null)
                {
                    restoreprodcnt = Convert.ToInt16(objDtRestoreList.Rows[0][0]);
                }
                var skucnt = objLS.TB_PLAN
                           .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                           .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                           .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => x.tcptps.tps.SKU_COUNT);
                var SKUcount = skucnt.Select(a => a).ToList();
                if (SKUcount.Any())
                {
                    userSKUProductCount = Convert.ToInt32(SKUcount[0]);
                }

                var productcount = objLS.STP_LS_GET_PRODUCTS_BY_USER("PRODUCT", User.Identity.Name, "").ToList();

                if (productcount.Any())
                {
                    userProductCount = Convert.ToInt32(productcount[0]);
                }

                if ((userProductCount + restoreprodcnt) > userSKUProductCount)
                {

                    return "SKU Exceed";
                }

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_RESTORE_TRASH_ITEMS";
                    objSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@XML_ID", SqlDbType.NVarChar, 5000).Value = Arr_XML_ID;
                    objSqlCommand.Parameters.Add("@USER_ID", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.ExecuteNonQuery();

                  
                    //SqlCommand objSqlCommand2 = objSqlConnection.CreateCommand();
                    //objSqlCommand2.CommandText = "STP_LS_RESTORE_DELETED_ITEMS";
                    //objSqlCommand2.CommandType = System.Data.CommandType.StoredProcedure;
                    //objSqlCommand2.Connection = objSqlConnection;
                    //objSqlCommand2.Parameters.Add("@XML_ID", SqlDbType.NVarChar, 5000).Value = Arr_XML_ID;
                    //objSqlCommand2.Parameters.Add("@USER_ID", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    //objSqlCommand2.CommandTimeout = 0;
                    //objSqlCommand2.ExecuteNonQuery();

                    SqlCommand objSqlCommand1 = objSqlConnection.CreateCommand();
                    objSqlCommand1.CommandText = "STP_LS_RESTORE_DELETED_ATTRIBUTES";
                    objSqlCommand1.CommandType = System.Data.CommandType.StoredProcedure;
                    objSqlCommand1.Connection = objSqlConnection;
                    objSqlCommand1.Parameters.Add("@XML_ID", SqlDbType.NVarChar, 5000).Value = Arr_XML_ID;
                    objSqlCommand1.Parameters.Add("@USER_ID", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand1.CommandTimeout = 0;
                    objSqlCommand1.ExecuteNonQuery();


                }
                return result;
                // return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Delete Catalog", ex);
                return null;
                //  return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        [System.Web.Http.HttpPost]
        public string RecycleBinPermanentlyRemoveSelecteddetails(object model)
        {
            string result = string.Empty;
            try
            {
                using (var db = new CSEntities())
                {
                    var permanentlyRemoveSelected = ((JArray)model).Select(x => x).ToList();
                    var returnValue = new ObjectParameter("REFINT", typeof(Int32));
                    // var arr = (JArray)((JObject.Parse(model.ToString())).SelectToken("data")).SelectToken("models");
                    //var permanentlyRemoveSelected = ((JArray)arr).Select(x => new TB_RECYCLE_BIN { RESTORE = (bool)x["RESTORE"], XML_ID = (int)x["XML_ID"] }).ToList();
                    foreach (var items in permanentlyRemoveSelected)
                    {
                        if (items["RESTORE"].ToString().ToUpper() == "TRUE")
                        {
                            // int userworkflowcount = objLS.TB_ROLE_FUNCTIONS.Count(x => x.FUNCTION_ID == dynTFA.FUNCTION_ID);
                            var ss = Convert.ToBoolean(items["RESTORE"]);
                            string catalogID = Convert.ToString(items["CATALOG_ID"]);
                            string categoryID = Convert.ToString(items["ROOT_CATEGORY_ID"]);
                            string familyID = Convert.ToString(items["FAMILY_ID"]);
                            string subfamilyID = Convert.ToString(items["SUBFAMILY_ID"]);
                            string productID = Convert.ToString(items["PRODUCT_ID"]);
                            string subproductID = Convert.ToString(items["SUBPRODUCT_ID"]);
                            string recycleType = Convert.ToString(items["RECYCLE_TYPE"]).ToUpper();
                            var s1s = items["XML_ID"];
                            //TB_RECYCLE_TABLE objTbRecycleTable = objLS.TB_RECYCLE_TABLE.Find(items["XML_ID"].ToString());
                            //objLS.TB_RECYCLE_TABLE.Remove(objTbRecycleTable);
                            //objLS.SaveChanges();
                            if (recycleType == "CATEGORY")
                            {
                                objLS.STP_CATALOGSTUDIO5_Remove(catalogID, categoryID, "0", "0", "0", "CATEGORY", "", 0, 1, returnValue);
                            }
                            if (recycleType == "FAMILY")
                            {
                                objLS.STP_CATALOGSTUDIO5_Remove(catalogID, categoryID, familyID, "0", "0", "Family", "", 0, 1, returnValue);
                            }
                            if (recycleType == "SUBFAMILY")
                            {
                                objLS.STP_CATALOGSTUDIO5_Remove(catalogID, categoryID, subfamilyID, "0", "0", "Family", "", 0, 1, returnValue);
                            }
                            if (recycleType == "PRODUCT")
                            {
                                objLS.STP_CATALOGSTUDIO5_Remove(catalogID, categoryID, productID, items["PRODUCT_ID"].ToString(), "0", "Product", "", 0, 1, returnValue);
                            }
                            if (recycleType == "SUBPRODUCT")
                            {
                                objLS.STP_CATALOGSTUDIO5_Remove(catalogID, categoryID, subproductID, items["SUBPRODUCT_ID"].ToString(), "0", "Product", "", 0, 1, returnValue);
                            }
                            //TB_RECYCLE_FOR_ATTRIBUTES objTbRecycleFor_Attributes = objLS.TB_RECYCLE_FOR_ATTRIBUTES.Find(items["XML_ID"].ToString());
                            //objLS.TB_RECYCLE_FOR_ATTRIBUTES.Remove(objTbRecycleFor_Attributes);
                            //objLS.SaveChanges();
                            string deleted_query = "DELETE FROM TB_RECYCLE_TABLE WHERE XML_ID IN(" + s1s + ");DELETE FROM TB_RECYCLE_FOR_ATTRIBUTES WHERE XML_ID IN(" + s1s + ")";
                            var objProductLayout = new ProductLayout();
                            objProductLayout.Checkproductfilter(deleted_query);
                           
                        }
                        if (result.ToUpper() != "TRUE")
                        {
                            result = Convert.ToString(items["RESTORE"]);
                        }
                    }
                    //}
                    return result;
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error at Delete Catalog", ex);
                return null;

            }
        }

        [System.Web.Http.HttpPost]
        public void EmptyRecycleBindetails(object model)
        {

            string result = string.Empty;
            try
            {
                using (var db = new CSEntities())
                {
                    var permanentlyRemoveSelected = ((JArray)model).Select(x => x).ToList();
                    var returnValue = new ObjectParameter("REFINT", typeof(Int32));
                    // var arr = (JArray)((JObject.Parse(model.ToString())).SelectToken("data")).SelectToken("models");
                    //var permanentlyRemoveSelected = ((JArray)arr).Select(x => new TB_RECYCLE_BIN { RESTORE = (bool)x["RESTORE"], XML_ID = (int)x["XML_ID"] }).ToList();
                    foreach (var items in permanentlyRemoveSelected)
                    {
                        
                            // int userworkflowcount = objLS.TB_ROLE_FUNCTIONS.Count(x => x.FUNCTION_ID == dynTFA.FUNCTION_ID);
                            var ss = Convert.ToBoolean(items["RESTORE"]);
                            string catalogID = Convert.ToString(items["CATALOG_ID"]);
                            string categoryID = Convert.ToString(items["ROOT_CATEGORY_ID"]);
                            string familyID = Convert.ToString(items["FAMILY_ID"]);
                            string subfamilyID = Convert.ToString(items["SUBFAMILY_ID"]);
                            string productID = Convert.ToString(items["PRODUCT_ID"]);
                            string subproductID = Convert.ToString(items["SUBPRODUCT_ID"]);
                            string recycleType = Convert.ToString(items["RECYCLE_TYPE"]).ToUpper();
                            var s1s = items["XML_ID"];
                            //TB_RECYCLE_TABLE objTbRecycleTable = objLS.TB_RECYCLE_TABLE.Find(items["XML_ID"].ToString());
                            //objLS.TB_RECYCLE_TABLE.Remove(objTbRecycleTable);
                            //objLS.SaveChanges();
                            if (recycleType == "CATEGORY")
                            {
                                objLS.STP_CATALOGSTUDIO5_Remove(catalogID, categoryID, "0", "0", "0", "CATEGORY", "", 0, 1, returnValue);
                            }
                            if (recycleType == "FAMILY")
                            {
                                objLS.STP_CATALOGSTUDIO5_Remove(catalogID, categoryID, familyID, "0", "0", "Family", "", 0, 1, returnValue);
                            }
                            if (recycleType == "SUBFAMILY")
                            {
                                objLS.STP_CATALOGSTUDIO5_Remove(catalogID, categoryID, subfamilyID, "0", "0", "Family", "", 0, 1, returnValue);
                            }
                            if (recycleType == "PRODUCT")
                            {
                                objLS.STP_CATALOGSTUDIO5_Remove(catalogID, categoryID, productID, items["PRODUCT_ID"].ToString(), "0", "Product", "", 0, 1, returnValue);
                            }
                            if (recycleType == "SUBPRODUCT")
                            {
                                objLS.STP_CATALOGSTUDIO5_Remove(catalogID, categoryID, subproductID, items["SUBPRODUCT_ID"].ToString(), "0", "Product", "", 0, 1, returnValue);
                            }
                            //TB_RECYCLE_FOR_ATTRIBUTES objTbRecycleFor_Attributes = objLS.TB_RECYCLE_FOR_ATTRIBUTES.Find(items["XML_ID"].ToString());
                            //objLS.TB_RECYCLE_FOR_ATTRIBUTES.Remove(objTbRecycleFor_Attributes);
                            //objLS.SaveChanges();
                            string deleted_query = "DELETE FROM TB_RECYCLE_TABLE WHERE XML_ID IN(" + s1s + ");DELETE FROM TB_RECYCLE_FOR_ATTRIBUTES WHERE XML_ID IN(" + s1s + ")";
                            var objProductLayout = new ProductLayout();
                            objProductLayout.Checkproductfilter(deleted_query);

                         
                    }
                    //}
                    
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error at Delete Catalog", ex);
              

            }
        }

    }
    public class TB_RECYCLE_BIN
    {

        public bool RESTORE { get; set; }
        public int XML_ID { get; set; }
        //public int ROOT_CATEGORY_ID { get; set; }
        //public string REMOVED_CATEGORY_NAME { get; set; }
        //public string REMOVED_FAMILY { get; set; }
        //public string REMOVED_SUB_FAMILY { get; set; }
        //public string REMOVED_PRODUCT { get; set; }
        //public int ATTRIBUTE_ID { get; set; }
        //public string ATTRIBUTE_NAME { get; set; }
        //public string DELETED_USER { get; set; }
        //public string DELETED_DATE { get; set; }
    }
}