﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace LS.Web.Controllers
{
    public class SeparatePathApiController : ApiController
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(SeparatePathApiController));


        [HttpPost]
        public string ServerUploadFile(HttpPostedFileBase file)
        {
            try
            {
                string userPath = WebConfigurationManager.AppSettings["ExternalAssetDrivePath"];
                string Server_FileName = System.IO.Path.GetFileName(file.FileName);
                string Server_path = string.Concat(userPath, Server_FileName);   //userName +""+ Server_FileName);
                file.SaveAs(Server_path);
                return "File Uploaded Success";
            }
            catch (Exception EX)
            {
                Logger.Error("Error at SepreatePathApiController : UploadFile", EX);
                return "File upload failed!!";

            }
        }
        /// <summary>
        /// To Get the saved path from web config
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public  List<string> GetServerFilePath()
        {
            string isServerOrLocal = WebConfigurationManager.AppSettings["UseExternalAssetDrive"];
            string userPath = WebConfigurationManager.AppSettings["ExternalAssetDrivePath"];
            string serverIpPath = WebConfigurationManager.AppSettings["ExternalAssetDriveURL"];
            string path = string.Empty;
            List<string> returnValues = new List<string>();



            if (isServerOrLocal == "true")
            {
                path = serverIpPath;
            }else
            {
                path = "../";
            }

            returnValues.Add(path);
            returnValues.Add(isServerOrLocal);


            return returnValues;
        }

    }
}
