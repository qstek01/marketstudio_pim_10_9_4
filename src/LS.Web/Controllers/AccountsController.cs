﻿                                                                                                                                                                                                                using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using LS.Data.Model.Accounts;
using log4net;
using LS.Data;
using System.Data.Entity.SqlServer;
using System.Configuration;
using System.IO;



namespace LS.Web.Controllers
{
    public class AccountsController : Controller
    {
        private ILog _logger = LogManager.GetLogger(typeof(AccountsController));
        readonly CSEntities _dbcontext = new CSEntities();
        AnalyticsApiController Analytics = new AnalyticsApiController();
        //------------------------------------------Seperated path values initializing------------------------------------------------------//
        public string pathCheckFlag = System.Web.Configuration.WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
        public string pathServerValue = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDriveURL"].ToString();
        public string serverPathShareVal = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
        //------------------------------------------Seperated path values initializing------------------------------------------------------//


        //Active Directory
        //Modified by jothipriya on APril 20 2022
        public ActionResult LogOn()
        {
            try
            {
                //To Retrive the ProductTitle Value from Webconfig and assign into Session.
                Session["ProductTitle"] = ConfigurationManager.AppSettings["productTitle"];

                if (string.IsNullOrEmpty(User.Identity.Name) || User.Identity.Name != "")
                {
                    return View();
                }
                //return RedirectToAction("AdminDashboard", "App");
                return RedirectToAction("Dashboard", "App");
            }
            catch (Exception ex)
            {
                _logger.Error("Error at LogOn", ex);
                throw;
            }
        }

        ////Active directory windows verification
        //private void WindowsVerification()
        //{

        //    string getUserDomainName = Environment.UserDomainName;
        //    string getUserName = Environment.UserName;
        //    string getWindowsIDName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
        //    using (StreamWriter sw = new StreamWriter("C:\\WindowsLog\\Log.txt", append: true))
        //    {
        //        string logMessage = string.Format("Env. User Domain:{0}; Env.UserName :{1}; Windows Identity Name:{2}", getUserDomainName, getUserName, getWindowsIDName);
        //        sw.WriteLine();
        //        sw.Close();
        //        sw.Dispose();
        //    }
        //}


        //public String GetUserEmail_FromSSO()
        //{
        //    /*To get the System Environment Fields*/
        //    string DomainName = Environment.UserDomainName, UserName = Environment.UserName;

        //    var objUserSSO = _dbcontext.QSWS_USER_SSO
        //                                     .Where(x => x.UserDomain == DomainName && x.UserName == UserName)
        //                                     .Select(y => new { y.UserEmail }).ToList().FirstOrDefault();

        //    return objUserSSO != null ? objUserSSO.UserEmail : "";

        //}

        //Active Directory
        // END
        //Modified by jothipriya on APril 20 2022



        // POST: /Logon/LogOn
        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string User_id = string.Empty;

                    //MAil login
                    string displayname = string.Empty;
                    var user_name = _dbcontext.aspnet_Membership
                                             .Join(_dbcontext.aspnet_Users, cc => cc.UserId, tc => tc.UserId,
                                                 (cc, tc) => new { cc, tc })
                                             .Where(
                                                 x =>
                                                     x.cc.Email == model.UserName)
                                             .Select(y => new { y.tc.UserName }).ToArray();
                    if (user_name.Any())
                    {
                        model.UserName = user_name[0].UserName.ToString();
                    }
                    var USER_ID1 = _dbcontext.aspnet_Membership.Where(x => x.Email == model.UserName).Select(x => x.UserId).ToArray();
                    if (USER_ID1.Any())
                    {
                          User_id = USER_ID1[0].ToString();
                    }
                    string RoleId = string.Empty;
                    string session_id = HttpContext.Session.SessionID.ToString();
                    // To get check the flag that user can choose the files are saved local or server.  [  Start ] 
                    try
                    {
                        if (pathCheckFlag == "true")
                        {
                            CreateTheServerPath(model.UserName);
                        }
                    }

                    catch (Exception e)
                    {
                        _logger.Error("Error at SeparatePathCreation", e);
                    }
                    // To get check the flag that user can choose the files are saved local or server.  [  End ] 

                    if (Membership.ValidateUser(model.UserName, model.Password))
                    {
                        FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                        _logger.Info("Redirects to App");
                        var SuperAdminCheck = _dbcontext.vw_aspnet_UsersInRoles
                        .Join(_dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                        .Join(_dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                        .Where(x => x.tcptps.tpf.UserName == model.UserName && x.tcp.RoleType == 1).ToList();

                        if (SuperAdminCheck.Any())
                        {
                            return RedirectToAction("Dashboard", "App");
                        }
                        var userActivationCheck = _dbcontext.aspnet_Users.Where(
                                          z => z.UserName == model.UserName && z.IsAnonymous == false)
                                          .Select(x => x.UserName)
                                          .ToList();

                        if (userActivationCheck.Any())
                        {
                            var userIsActive = _dbcontext.Customers.Join(_dbcontext.Customer_User, C => C.CustomerId, CU => CU.CustomerId, (C, CU) => new { C, CU }).Where(x => x.CU.User_Name == model.UserName && x.C.IsActive == true)

                                        .Select(y => y.C.IsActive)
                                        .ToList();
                            if (userIsActive.Any())
                            {
                                var chkSubscription =
                                    _dbcontext.STP_LS_CHECK_SUBSCRIPTION_BY_USER(model.UserName, "").ToList();
                                if (chkSubscription.Any())
                                {
                                    var chkSubscriptions = chkSubscription.FirstOrDefault();

                                    var chkSubscription1 = _dbcontext.TB_STORE_SESSION
                                        .Join(_dbcontext.Customer_User, tps => tps.ID, tpf => tpf.CustomerId,
                                            (tps, tpf) => new { tps, tpf })
                                        .Where(x => x.tpf.User_Name == model.UserName).ToList();
                                    if (chkSubscription1.Any())
                                    {
                                        int skucount =
                                            Convert.ToInt32(
                                                LS.Data.Utilities.CommonUtil.Decrypt(
                                                    chkSubscription1.FirstOrDefault().tps.COL1));
                                        int Noofusers =
                                            Convert.ToInt32(
                                                LS.Data.Utilities.CommonUtil.Decrypt(
                                                    chkSubscription1.FirstOrDefault().tps.COL2));
                                        int NoofSubscriptions =
                                            Convert.ToInt32(
                                                LS.Data.Utilities.CommonUtil.Decrypt(
                                                    chkSubscription1.FirstOrDefault().tps.COL3));
                                        string activationdate =
                                            LS.Data.Utilities.CommonUtil.Decrypt(chkSubscription1.FirstOrDefault().tps.COL4);
                                        DateTime dtStore = Convert.ToDateTime(activationdate);
                                        string actStoreDate = String.Format("{0:dd-MMM-yyyy}", dtStore);

                                        DateTime dtPlan = Convert.ToDateTime(chkSubscription[0].ActivationDate);
                                        string actPlanDate = String.Format("{0:dd-MMM-yyyy}", dtPlan);

                                        if (chkSubscription[0].NO_OF_USERS == Noofusers &&
                                            chkSubscription[0].SUBSCRIPTION_IN_MONTHS == NoofSubscriptions &&
                                            actPlanDate == actStoreDate)
                                        {
                                            var UserList =
                                                _dbcontext.aspnet_Users.Where(
                                                    z => z.UserName == model.UserName && z.IsAnonymous == false)
                                                    .Select(x => x.UserName)
                                                    .ToList();
                                            if (UserList.Any())
                                            {
                                                if (Roles.IsUserInRole(model.UserName, "Admin") ||
                                                    Roles.IsUserInRole(model.UserName, "AdminNormalUser") ||
                                                    Roles.IsUserInRole(model.UserName, "SuperAdmin"))
                                                {
                                                    //return RedirectToAction("AdminDashboard", "App");
                                                    Analytics.getUser(User_id, RoleId, model.UserName, session_id);
                                                    return RedirectToAction("Dashboard", "App");
                                                }

                                                var cataloglist = _dbcontext.Customer_User
                                                    .Join(_dbcontext.Customer_Catalog, cc => cc.CustomerId,
                                                        tc => tc.CustomerId,
                                                        (cc, tc) => new { cc, tc })
                                                    .Where(
                                                        x =>
                                                            x.cc.User_Name == model.UserName &&
                                                            x.cc.CustomerId == x.tc.CustomerId)
                                                    .Select(x => new { x.cc.CustomerId, x.cc.FirstName, x.cc.LastName }).ToList().FirstOrDefault();

                                                //   Session["Welcomename"] = Convert.ToString(cataloglist.FirstName) + " " + Convert.ToString(cataloglist.LastName);
                                                //if (cataloglist.Any())
                                                //{

                                                //    //return RedirectToAction("CustomerDashboard", "App");
                                                //    return RedirectToAction("Dashboard", "App");

                                                //}
                                                //else
                                                {
                                                    //  return RedirectToAction("Catalog", "App");
                                                    //return RedirectToAction("CustomerDashboard", "App");
                                                    Session["passWord"] = EnryptString(model.Password);
                                                    Analytics.getUser(User_id, RoleId, model.UserName, session_id);
                                                    return RedirectToAction("Dashboard", "App");
                                                }
                                            }
                                        }

                                    }


                                }
                                else
                                {
                                    Session.Abandon();
                                    FormsAuthentication.SignOut();
                                    ViewBag.Message = "“Your subscription is currently inactive , please contact support@questudio.com for activation / query.";
                                    return View("~/Views/UserAdmin/ThankyouPage.cshtml");
                                }
                            }
                            else
                            {
                                Session.Abandon();
                                FormsAuthentication.SignOut();
                                ViewBag.Message = "“Your subscription is currently inactive , please contact support@questudio.com for activation / query.";
                                return View("~/Views/UserAdmin/ThankyouPage.cshtml");

                            }
                        }
                        else
                        {
                            //Session.Abandon();
                            //FormsAuthentication.SignOut();
                            //ViewBag.Message = "Your Subscription has not been activated yet.";
                            //return View("~/Views/UserAdmin/ThankyouPage.cshtml");
                            Session.Abandon();
                            FormsAuthentication.SignOut();
                            ViewBag.Message = "“Your subscription is currently inactive , please contact support@questudio.com for activation / query.";
                            return View("~/Views/UserAdmin/ThankyouPage.cshtml");
                        }
                    }
                    else
                    {
                        MembershipUser memCheckUser = Membership.GetUser(model.UserName);
                        if (memCheckUser != null && memCheckUser.IsLockedOut == true)
                        {
                            _logger.Error("Error While Logging");
                            ModelState.AddModelError("Error", "Your Account was locked.");
                        }
                        else
                        {
                            _logger.Error("Error While Logging");
                            ModelState.AddModelError("Error", "Please enter correct Email Id/ Password.");
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                _logger.Error("Error at LogOn", exp);
                Console.WriteLine(exp.Message);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public string EnryptString(string strEncrypted)
        {
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(strEncrypted);
            string encrypted = Convert.ToBase64String(b);
            return encrypted;
        }
        public string DecryptString(string encrString)
        {
            byte[] b;
            string decrypted;
            try
            {
                b = Convert.FromBase64String(encrString);
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(b);
            }
            catch (FormatException fe)
            {
                decrypted = "";
            }
            return decrypted;
        }
        //[Authorize(Roles = "Admin")]
        public ActionResult Manage()
        {
            return View("~/Views/App/Admin/Manage.cshtml");
        }
        public ActionResult Error()
        {
            return View("~/Views/Accounts/Error.cshtml");
        }
        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            try
            {
                _logger.Info("Inside  at AccountController - LogOff");
                Analytics.getLogoutDetails();
                Session.Abandon();
                FormsAuthentication.SignOut();
                //WebSecurity.Logout();

                return RedirectToAction("Account", "LogOn");
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AccountController - LogOff", ex);
                throw;
            }
        }

        //  [Authorize(Roles = "Admin")]
        public ActionResult UserList()
        {
            return View();
        }

        //[Authorize(Roles = "Admin")]
        public ActionResult Register()
        {
            return View(new UserModel());
        }

        //
        // POST: /Account/Register
        [HttpPost]
        //  [Authorize(Roles = "Admin")]
        public ActionResult Register(UserModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserName = model.Email;
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    foreach (string role in Roles.GetAllRoles())
                    {
                        if (!model.UserRoles.Contains(role) && Roles.IsUserInRole(model.UserName, role))
                        {
                            Roles.RemoveUserFromRole(model.UserName, role);
                        }
                    }

                    foreach (string role in model.UserRoles)
                    {
                        if (!Roles.IsUserInRole(model.UserName, role))
                        {
                            Roles.AddUsersToRole(new[] { model.UserName }, role);
                        }
                    }

                    //add profile information
                    var profile = ProfileModel.GetProfile(model.UserName);
                    profile.CustomerId = model.CustomerId;
                    profile.FirstName = model.FirstName;
                    profile.LastName = model.LastName;
                    profile.Title = model.Title;
                    profile.Address1 = model.Address1;
                    profile.Address2 = model.Address2;
                    profile.City = model.City;
                    profile.State = model.State;
                    profile.Zip = model.Zip;
                    profile.Country = model.Country;
                    profile.Phone = model.Phone;
                    profile.Fax = model.Fax;
                    profile.EMail = model.Email;
                    profile.Comments = model.Comments;
                    profile.DateUpdated = DateTime.Now;
                    profile.DateCreated = DateTime.Now;
                    profile.Save();
                    return RedirectToAction("UserList");
                }
                ModelState.AddModelError("", ErrorCodeToString(createStatus));
            }

            // If we got this far, something failed, redisplay form

            return View(model);
        }


        // [Authorize(Roles = "CustomerAdmin")]
        public ActionResult CustomerUserList()
        {
            return View();
        }

        //  [Authorize(Roles = "CustomerAdmin")]
        public ActionResult AddCustomerUser()
        {
            int? customerId = AccountApiController.GetCustomerId(User.Identity.Name);
            if (customerId.HasValue)
            {
                //   Customer customer = UserAccountsRepository.DC.Customers.Where(x => x.CustomerId == customerId.Value).FirstOrDefault();
                // return View(new UserModel() { CustomerId = customerId, CustomerName = customer.CustomerName });
                return null;
            }
            throw new Exception("Invalid Request");
        }

        //
        // POST: /Account/Register

        [HttpPost]
        //  [Authorize(Roles = "CustomerAdmin")]
        public ActionResult AddCustomerUser(UserModel model)
        {
            if (ModelState.IsValid)
            {
                if (!model.CustomerId.HasValue)
                {
                    throw new Exception("Invalid Request");
                }
                // Attempt to register the user
                MembershipCreateStatus createStatus;
                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    foreach (string role in Roles.GetAllRoles().Where(role => !model.UserRoles.Contains(role) && Roles.IsUserInRole(model.UserName, role)))
                    {
                        Roles.RemoveUserFromRole(model.UserName, role);
                    }

                    foreach (string role in model.UserRoles.Where(role => !Roles.IsUserInRole(model.UserName, role)))
                    {
                        Roles.AddUsersToRole(new[] { model.UserName }, role);
                    }

                    //add profile information
                    var profile = ProfileModel.GetProfile(model.UserName);
                    profile.CustomerId = model.CustomerId;
                    profile.FirstName = model.FirstName;
                    profile.LastName = model.LastName;
                    profile.Title = model.Title;
                    profile.Address1 = model.Address1;
                    profile.Address2 = model.Address2;
                    profile.City = model.City;
                    profile.State = model.State;
                    profile.Zip = model.Zip;
                    profile.Country = model.Country;
                    profile.Phone = model.Phone;
                    profile.Fax = model.Fax;
                    profile.EMail = model.Email;
                    profile.Comments = model.Comments;
                    profile.DateUpdated = DateTime.Now;
                    profile.DateCreated = DateTime.Now;
                    profile.Save();
                    return RedirectToAction("UserList");
                }
                ModelState.AddModelError("", ErrorCodeToString(createStatus));
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/RegisterSuccess
        public ActionResult RegisterSuccess()
        {
            return View();
        }

        // GET: /Account/EditUserProfile
        // [Authorize(Roles = "Admin,Administrators,ProjectManager")]
        public ActionResult EditUserProfile()
        {
            MembershipUser user = Membership.GetUser(Request.QueryString["UserName"]);
            if (user != null)
            {
                var profile = ProfileModel.GetProfile(user.UserName);

                var model = new UserRoleModel
                {
                    UserName = user.UserName,
                    CustomerId = profile.CustomerId,
                    FirstName = profile.FirstName,
                    LastName = profile.LastName,
                    Title = profile.Title,
                    Address1 = profile.Address1,
                    Address2 = profile.Address2,
                    City = profile.City,
                    State = profile.State,
                    Zip = profile.Zip,
                    Country = profile.Country,
                    Phone = profile.Phone,
                    Fax = profile.Fax,
                    Email = profile.EMail,
                    Comments = profile.Comments,
                    DateUpdated = profile.DateUpdated,
                    DateCreated = profile.DateCreated,
                };

                if (profile.CustomerId.HasValue)
                {
                    //Customer customer = UserAccountsRepository.DC.Customers.Where(x => x.CustomerId == profile.CustomerId.Value).FirstOrDefault();
                    //model.CustomerName = customer.CustomerName;
                }
                model.UserRoles = Roles.GetRolesForUser(model.UserName);
                return View(model);
            }
            return null;
        }

        //
        // GET: /Account/EditUserProfile
        // [Authorize(Roles = "Admin,Administrators,ProjectManager")]
        [HttpPost]
        public ActionResult EditUserProfile(UserRoleModel model)
        {
            if (ModelState.IsValid)
            {
                bool updateSucceeded;
                try
                {
                    MembershipUser currentUser = Membership.GetUser(model.UserName);
                    if (currentUser != null)
                    {
                        currentUser.Email = model.Email;
                        Membership.UpdateUser(currentUser);
                        foreach (string role in Roles.GetAllRoles().Where(role => !model.UserRoles.Contains(role) && Roles.IsUserInRole(currentUser.UserName, role)))
                        {
                            Roles.RemoveUserFromRole(currentUser.UserName, role);
                        }
                        foreach (string role in model.UserRoles.Where(role => !Roles.IsUserInRole(currentUser.UserName, role)))
                        {
                            Roles.AddUsersToRole(new[] { currentUser.UserName }, role);
                        }
                    }

                    //update profile information
                    var profile = ProfileModel.GetProfile(model.UserName);
                    profile.CustomerId = model.CustomerId;
                    profile.FirstName = model.FirstName;
                    profile.LastName = model.LastName;
                    profile.Title = model.Title;
                    profile.Address1 = model.Address1;
                    profile.Address2 = model.Address2;
                    profile.City = model.City;
                    profile.State = model.State;
                    profile.Zip = model.Zip;
                    profile.Country = model.Country;
                    profile.Phone = model.Phone;
                    profile.Fax = model.Fax;
                    profile.EMail = model.Email;
                    profile.Comments = model.Comments;
                    profile.DateUpdated = DateTime.Now;
                    profile.Save();
                    updateSucceeded = true;
                }
                catch (Exception)
                {
                    updateSucceeded = false;
                }

                if (updateSucceeded)
                {
                    return RedirectToAction("UserList");
                }
                ModelState.AddModelError("", @"Unable to update user profile. Please contact administrator");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult EditUserProfileSuccess()
        {
            return View();
        }

        //public ActionResult ChangePassword()
        //{
        //    var model = new ChangePasswordModel() { UserName = Request.QueryString["UserName"] };
        //    return View(model);
        //}

        //[HttpPost]
        //public ActionResult ChangePassword(ChangePasswordModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        bool changePasswordSucceeded = false;
        //        try
        //        {
        //            MembershipUser currentUser = Membership.GetUser(model.UserName);
        //            if (currentUser != null)
        //            {
        //                string oldPassword = currentUser.ResetPassword();
        //                changePasswordSucceeded = currentUser.ChangePassword(oldPassword, model.NewPassword);
        //                currentUser.UnlockUser();
        //            }

        //            if (changePasswordSucceeded)
        //            {
        //                return RedirectToAction("ChangePasswordSuccess");
        //            }
        //            else
        //            {
        //                ModelState.AddModelError("", "Unable to change the password. Please contact administrator");
        //            }
        //        }
        //        catch (Exception)
        //        {
        //            changePasswordSucceeded = false;
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        //public ActionResult ChangePasswordSuccess()
        //{
        //    return View();
        //}

        //[Authorize(Users = "*")]
        //[AllowAnonymous]
        //public ActionResult ForgotPassword()
        //{
        //    ForgorPasswordModel fm = new ForgorPasswordModel();
        //    return View(fm);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult ForgotPassword(ForgorPasswordModel model)
        //{
        //    try
        //    {
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //    return View();
        //}


        //[Authorize(Roles = "Admin")]
        //public ActionResult RolesList(string searchWord, GridSortOptions gridSortOptions, int? page)
        //{
        //    IQueryable<RoleModel> rolesView = (from s in Roles.GetAllRoles()
        //                                       where string.IsNullOrEmpty(searchWord) || s.Contains(searchWord)
        //                                       select new RoleModel() { RoleName = s }).ToList().AsQueryable();
        //    var pagedViewModel = new PagedViewModel<RoleModel>
        //    {
        //        ViewData = ViewData,
        //        Query = rolesView,
        //        GridSortOptions = gridSortOptions,
        //        DefaultSortColumn = "RoleName",
        //        Page = page,
        //        PageSize = 5,
        //    }
        //    .AddFilter("searchWord", searchWord)
        //    .Setup();

        //    return View(pagedViewModel);
        //}

        //[Authorize(Roles = "Admin")]
        //public ActionResult AddNewRole()
        //{
        //    return View();
        //}

        //[HttpPost]
        //[Authorize(Roles = "Admin")]
        //public ActionResult AddNewRole(RoleModel model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        // Attempt to register the user
        //        Roles.CreateRole(model.RoleName);

        //        if (Roles.GetAllRoles().Contains(model.RoleName))
        //        {
        //            return RedirectToAction("RolesList");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "Unable to create role.");
        //        }
        //    }

        //    // If we got this far, something failed, redisplay form
        //    return View(model);
        //}

        //public ActionResult AddNewRoleSuccess()
        //{
        //    return View();
        //}

        [AllowAnonymous]
        public string CheckUserName(string input)
        {
            MembershipUser user = Membership.GetUser(input);
            // bool ifuser = aspnet_Users
            return user == null ? "Available" : "Not Available";
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists, please enter a different user name";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists, please enter a different e-mail address";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid, please enter a valid password value";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address entered is invalid, please check the value and try again";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid, please check the value and try again";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid, please check the value and try again";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid, please check the value and try again";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        public void CreateTheServerPath(string userName)
        {
            try
            {
                var getCustomerPath = _dbcontext.Customers.Join(_dbcontext.Customer_User, C => C.CustomerId, CU => CU.CustomerId, (C, CU) => new { C, CU }).Where(x => x.CU.User_Name == userName && x.C.IsActive == true)
                                           .Select(y => y.C.Comments)
                                           .FirstOrDefault();
                if (getCustomerPath != null)
                {
                    string serverUploadContentPath = string.Format("{0}//Content//ProductImages//{1}", serverPathShareVal, getCustomerPath);
                    // If directory does not exist, create it.
                    if (!Directory.Exists(serverUploadContentPath))
                    {
                        Directory.CreateDirectory(serverUploadContentPath);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.Error("Error at LogOn", e);
            }
            
        }
    }

}