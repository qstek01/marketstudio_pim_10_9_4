﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Web;
using log4net;
using LS.Data;
using DataColumn = System.Data.DataColumn;
using LS.Data.Model.ProductPreview;
using LS.Data.Model.ProductView;
using System.Transactions;
using System.Diagnostics;
using Newtonsoft.Json.Linq;


namespace LS.Web.Controllers
{
    public class TableDesignerApiController : ApiController
    {
        private List<string> _mRowFields;
        private List<string> _mLeftRowFields;
        private List<string> _mRightRowFields;
        private List<string> _mColumnFields;
        private List<string> _mSummaryFields;
        private List<XmlNode> _mLeftRowNodes;
        private List<XmlNode> _mRightRowNodes;
        private List<XmlNode> _mColumnNodes;
        private List<XmlNode> _mSummaryNodes;
        private string _mSummaryGroupField, _mTableGroupField;
        private string _mFamilyID = "1063", _mCatalogID = "133";
        private bool _mShowRowHeaders;
        private bool _mShowColumnHeaders;
        private bool _mShowSummaryHeaders;
        private bool _mMergeRowHeaders;
        private bool _mMergeSummaryFields;
        private bool _mVerticalTable;
        private string _mPlaceHolderText, _mPivotHeaderText, _mTableLayoutName;
        private DataTable _pivotTable, _attrTablePivot, _rowTable, _leftRowTable, _rightRowTable, _columnTable, _sourceTable, _attributeTable, _groupTable;

        string _prefix = string.Empty; string _suffix = string.Empty; string _emptyCondition = string.Empty; string _replaceText = string.Empty; string _headeroptions = string.Empty; string _fornumeric = string.Empty;
        private string _connectionString = string.Empty;
        private string _imagePath = string.Empty;
        private string _applicationStartupPath = string.Empty;
        private XmlNode _leftRowNode, _rightRowNode;
        private Image _chkSize;
        private int _headerCnt;

        public int[] _attributeIdList;
        private readonly bool _mergeFamilyWithProductTable;
        private readonly bool _loadXFamily;
        private bool _loadXProductTable;
        private int[] _AttributeIdList;
        public bool PDFCatalog = false;
        public string _SQLString = "";
        static readonly ILog Logger = LogManager.GetLogger(typeof(TableDesignerApiController));
        readonly CSEntities _dbcontext = new CSEntities();
        string sqlConn = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        public void SuperColumnLeft(XmlNode childNodes, List<TreeViewData> mLeftRowvalues, List<STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES_Result> objStplsAttributes, string attrId, string rowfield, int catalogid, int familyid)
        {
            for (int j = 0; j < childNodes.ChildNodes.Count; j++)
            {
                if (childNodes.ChildNodes[j].Name == rowfield)
                {
                    if (childNodes.ChildNodes[j].Attributes != null)
                    {
                        if (childNodes.Attributes["AttrName"].Value == attrId)
                        {
                            if (
                                childNodes.ChildNodes[j].Attributes["AttrID"].Value != "Super")
                            {
                                int attributeid;
                                string color = "Blue";
                                string attributeId = string.Empty;
                                if (childNodes.ChildNodes[j].Attributes["AttrID"].Value.Contains("nAttr"))
                                {
                                    attributeId =
                                      childNodes.ChildNodes[j].Attributes["AttrID"]
                                          .Value
                                          .Remove(0, 6);
                                    int.TryParse(attributeId, out attributeid);
                                }
                                else
                                {
                                    attributeId =
                                   childNodes.ChildNodes[j].Attributes["AttrID"]
                                       .Value
                                       .Remove(0, 5);
                                    int.TryParse(attributeId, out attributeid);
                                }

                                var checkattr = objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                //if (checkattr.Any())
                                //{
                                //    color = "Red";
                                //}
                                if (!checkattr.Any())
                                {
                                    var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                      _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                      tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                      .Where(x => x.tps.CATALOG_ID == catalogid
                                                                  && x.tps.ATTRIBUTE_ID == attributeid &&
                                                                  x.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                  x.tpf.FAMILY_ID == familyid);
                                    if (catalogattrchk.Any())
                                    {


                                        mLeftRowvalues.Add(new TreeViewData
                                        {
                                            id = 2,
                                            ATTRIBUTE_ID = attributeid,
                                            Merge =
                                                childNodes.ChildNodes[j].Attributes["Merge"]
                                                    .Value,
                                            ATTRIBUTE_NAME =
                                                GetAttributeNameFromAttributeID(attributeId),
                                            COLOR = color
                                        });
                                    }
                                }

                            }
                            else
                            {
                                mLeftRowvalues.Add(new TreeViewData
                                {
                                    id = 1,
                                    ATTRIBUTE_ID = -1,
                                    //Merge = childNodes[i].Attributes["Merge"].Value,
                                    ATTRIBUTE_NAME =
                                        childNodes.ChildNodes[j].Attributes["AttrName"].Value,
                                    COLOR = "Black",
                                    hasChildren =
                                        childNodes.ChildNodes[j].ChildNodes.Count > 0
                                });
                                // SuperColumn(childNodes.ChildNodes[j], mLeftRowvalues, objStplsAttributes, attrId);
                            }
                        }
                        else
                        {
                            SuperColumnLeft(childNodes.ChildNodes[j], mLeftRowvalues, objStplsAttributes, attrId, rowfield, catalogid, familyid);
                        }

                    }
                }

            }
        }
        public void SuperColumnRight(XmlNode childNodes, List<TreeViewData> mLeftRowvalues, List<STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES_Result> objStplsAttributes, string attrId, string rowfield, int catalogid, int familyid)
        {
            for (int j = 0; j < childNodes.ChildNodes.Count; j++)
            {
                if (childNodes.ChildNodes[j].Name == rowfield)
                {
                    if (childNodes.ChildNodes[j].Attributes != null)
                    {
                        if (childNodes.Attributes["AttrName"].Value == attrId)
                        {
                            if (
                                childNodes.ChildNodes[j].Attributes["AttrID"].Value != "Super")
                            {
                                int attributeid;
                                string color = "Blue";
                                string attributeId = string.Empty;
                                if (childNodes.ChildNodes[j].Attributes["AttrID"].Value.Contains("nAttr"))
                                {
                                    attributeId =
                                      childNodes.ChildNodes[j].Attributes["AttrID"]
                                          .Value
                                          .Remove(0, 6);
                                    int.TryParse(attributeId, out attributeid);
                                }
                                else
                                {
                                    attributeId =
                                   childNodes.ChildNodes[j].Attributes["AttrID"]
                                       .Value
                                       .Remove(0, 5);
                                    int.TryParse(attributeId, out attributeid);
                                }

                                var checkattr = objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                //if (checkattr.Any())
                                //{
                                //    color = "Red";
                                //}
                                if (!checkattr.Any())
                                {
                                    var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                     _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                     tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                     .Where(x => x.tps.CATALOG_ID == catalogid
                                                                 && x.tps.ATTRIBUTE_ID == attributeid &&
                                                                 x.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                 x.tpf.FAMILY_ID == familyid);
                                    if (catalogattrchk.Any())
                                    {
                                        mLeftRowvalues.Add(new TreeViewData
                                        {
                                            id = 2,
                                            ATTRIBUTE_ID = attributeid,
                                            Merge =
                                                childNodes.ChildNodes[j].Attributes["Merge"]
                                                    .Value,
                                            ATTRIBUTE_NAME =
                                                GetAttributeNameFromAttributeID(attributeId),
                                            COLOR = color
                                        });
                                    }
                                }
                            }
                            else
                            {
                                mLeftRowvalues.Add(new TreeViewData
                                {
                                    id = 1,
                                    ATTRIBUTE_ID = -1,
                                    //Merge = childNodes[i].Attributes["Merge"].Value,
                                    ATTRIBUTE_NAME =
                                        childNodes.ChildNodes[j].Attributes["AttrName"].Value,
                                    COLOR = "Black",
                                    hasChildren =
                                        childNodes.ChildNodes[j].ChildNodes.Count > 0
                                });
                                // SuperColumn(childNodes.ChildNodes[j], mLeftRowvalues, objStplsAttributes, attrId);
                            }
                        }
                        else
                        {
                            SuperColumnRight(childNodes.ChildNodes[j], mLeftRowvalues, objStplsAttributes, attrId, rowfield, catalogid, familyid);
                        }

                    }
                }

            }
        }
        public void SuperColumnSummary(XmlNode childNodes, List<TreeViewData> mLeftRowvalues, List<STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES_Result> objStplsAttributes, string attrId, string rowfield, int catalogid, int familyid)
        {
            for (int j = 0; j < childNodes.ChildNodes.Count; j++)
            {
                if (childNodes.ChildNodes[j].Name == rowfield)
                {
                    if (childNodes.ChildNodes[j].Attributes != null)
                    {
                        if (childNodes.Attributes["AttrName"].Value == attrId)
                        {
                            if (
                                childNodes.ChildNodes[j].Attributes["AttrID"].Value != "Super")
                            {
                                int attributeid;
                                string color = "Blue";
                                string attributeId = string.Empty;
                                if (childNodes.ChildNodes[j].Attributes["AttrID"].Value.Contains("nAttr"))
                                {
                                    attributeId =
                                      childNodes.ChildNodes[j].Attributes["AttrID"]
                                          .Value
                                          .Remove(0, 6);
                                    int.TryParse(attributeId, out attributeid);
                                }
                                else
                                {
                                    attributeId =
                                   childNodes.ChildNodes[j].Attributes["AttrID"]
                                       .Value
                                       .Remove(0, 5);
                                    int.TryParse(attributeId, out attributeid);
                                }

                                var checkattr = objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                //if (checkattr.Any())
                                //{
                                //    color = "Red";
                                //}
                                if (!checkattr.Any())
                                {
                                    var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                     _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                     tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                     .Where(x => x.tps.CATALOG_ID == catalogid
                                                                 && x.tps.ATTRIBUTE_ID == attributeid &&
                                                                 x.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                 x.tpf.FAMILY_ID == familyid);
                                    if (catalogattrchk.Any())
                                    {

                                        mLeftRowvalues.Add(new TreeViewData
                                        {
                                            id = 2,
                                            ATTRIBUTE_ID = attributeid,
                                            Merge =
                                                childNodes.ChildNodes[j].Attributes["Merge"]
                                                    .Value,
                                            ATTRIBUTE_NAME =
                                                GetAttributeNameFromAttributeID(attributeId),
                                            COLOR = color
                                        });
                                    }
                                }

                            }
                            else
                            {
                                mLeftRowvalues.Add(new TreeViewData
                                {
                                    id = 1,
                                    ATTRIBUTE_ID = -1,
                                    //Merge = childNodes[i].Attributes["Merge"].Value,
                                    ATTRIBUTE_NAME =
                                        childNodes.ChildNodes[j].Attributes["AttrName"].Value,
                                    COLOR = "Black",
                                    hasChildren =
                                        childNodes.ChildNodes[j].ChildNodes.Count > 0
                                });
                                // SuperColumn(childNodes.ChildNodes[j], mLeftRowvalues, objStplsAttributes, attrId);
                            }
                        }
                        else
                        {
                            SuperColumnSummary(childNodes.ChildNodes[j], mLeftRowvalues, objStplsAttributes, attrId, rowfield, catalogid, familyid);
                        }

                    }
                }

            }
        }


        [HttpPost]
        public List<TreeViewData> DecodeLayoutXmlLeftTree(string structureName, string mCatalogId, string familyId, string attrId, string flag, int groupId,int packId, object model)
        {
            //var lefttreeitems = new List<NodeViewModel>();
            //   xmlStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?><TradingBell TableType=\"SuperTable\"><LeftRowField AttrID=\"Super\" AttrName=\"TEST\" Level=\"0\"> <LeftRowField AttrID=\"Attr:42\" Merge=\"Unchecked\" Level=\"1\"></LeftRowField><LeftRowField AttrID=\"Attr:1\" Merge=\"Unchecked\" Level=\"1\"></LeftRowField></LeftRowField><SummaryGroupField>[none]</SummaryGroupField><TableGroupField>[none]</TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>False</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>";
            try
            {
                string option = "GETUNPUBLISHEDATTRS";
                int familyid;
                int.TryParse(familyId, out familyid);
                int catalogid;
                int.TryParse(mCatalogId, out catalogid);
                var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                var mLeftRowvalues = new List<TreeViewData>();
                _mLeftRowNodes = new List<XmlNode>();
                _leftRowNode = null;
                string xmlStr = Convert.ToString(model);

                #region Move AvailableAttr to LeftRow
                if (structureName == "Default Layout")
                {
                    TB_FAMILY_TABLE_STRUCTURE fTS = new TB_FAMILY_TABLE_STRUCTURE();
                    int catalogId = Convert.ToInt32(mCatalogId);
                    int family_id = Convert.ToInt32(familyId);
                    fTS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(s => s.CATALOG_ID == catalogId && s.FAMILY_ID == family_id && s.STRUCTURE_NAME == "Default Layout").FirstOrDefault();
                    string structName = DateTime.Now.ToString("yyyyMMddHHmmss");

                    if (fTS != null)
                    {
                        string tableStructure = "<?xml version=\"1.0\" encoding=\"utf-8\"?><TradingBell TableType=\"SuperTable\"><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>";
                        if (fTS.FAMILY_TABLE_STRUCTURE.Contains("\r\n     "))
                        {
                            fTS.FAMILY_TABLE_STRUCTURE = fTS.FAMILY_TABLE_STRUCTURE.Replace("\r\n     ", "");
                        }
                        if (fTS.FAMILY_TABLE_STRUCTURE == tableStructure)
                        {
                            string categoryId = "";
                            categoryId = _dbcontext.TB_CATALOG_FAMILY.Where(s => s.CATALOG_ID == catalogId && s.FAMILY_ID == family_id).FirstOrDefault().CATEGORY_ID;
                            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                            {
                                var sqlCommand = new SqlCommand();
                                objSqlConnection.Open();
                                sqlCommand.CommandText = "STP_CATALOGSTUDIO5_InsertFamilyTableStructure";
                                sqlCommand.CommandType = CommandType.StoredProcedure;
                                sqlCommand.Connection = objSqlConnection;
                                sqlCommand.Parameters.Add("@CATEGORYID", SqlDbType.NVarChar);
                                sqlCommand.Parameters["@CATEGORYID"].Value = categoryId;
                                sqlCommand.Parameters.Add("@CATALOGID", SqlDbType.Int);
                                sqlCommand.Parameters["@CATALOGID"].Value = catalogId;
                                sqlCommand.Parameters.Add("@FAMILYID", SqlDbType.Int);
                                sqlCommand.Parameters["@FAMILYID"].Value = family_id;
                                sqlCommand.Parameters.Add("@STRUCTURENAME", SqlDbType.NVarChar, 500);
                                sqlCommand.Parameters["@STRUCTURENAME"].Value = structName;
                                sqlCommand.Parameters.Add("@TABLETYPE", SqlDbType.NVarChar, 12);
                                sqlCommand.Parameters["@TABLETYPE"].Value = "Horizontal";
                                sqlCommand.Parameters.Add("@ISDEFAULT", SqlDbType.Bit);
                                sqlCommand.Parameters["@ISDEFAULT"].Value = 1;
                                sqlCommand.ExecuteNonQuery();
                                objSqlConnection.Close();
                            }
                        }
                    }

                    TB_FAMILY_TABLE_STRUCTURE fTS1 = new TB_FAMILY_TABLE_STRUCTURE();
                    fTS1 = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(s => s.CATALOG_ID == catalogId && s.FAMILY_ID == family_id && s.STRUCTURE_NAME == structName).FirstOrDefault();
                    if (fTS1 != null)
                    {
                        xmlStr = fTS1.FAMILY_TABLE_STRUCTURE;
                        fTS.FAMILY_TABLE_STRUCTURE = fTS1.FAMILY_TABLE_STRUCTURE;
                        _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Remove(fTS1);
                        _dbcontext.SaveChanges();
                    }
                }
                #endregion

                #region Attribute Pack in Table Designer

                if (packId > 0)
                {
                    TB_FAMILY_TABLE_STRUCTURE fTS = new TB_FAMILY_TABLE_STRUCTURE();
                    int family_id = Convert.ToInt32(familyId);
                    fTS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(s => s.CATALOG_ID == catalogid && s.FAMILY_ID == family_id && s.STRUCTURE_NAME == structureName).FirstOrDefault();
                    TB_ATTRIBUTE_PACK_TABLE_STRUCTURE apTableStructure = new TB_ATTRIBUTE_PACK_TABLE_STRUCTURE();
                    if (fTS != null && fTS.ID > 0)
                    {
                        apTableStructure = _dbcontext.TB_ATTRIBUTE_PACK_TABLE_STRUCTURE.Where(s => s.PACK_ID == packId && s.FAMILY_STRUCTURE_ID == fTS.ID).FirstOrDefault();
                    }
                    if (apTableStructure != null && apTableStructure.ID > 0)
                    {
                        xmlStr = apTableStructure.TABLE_STRUCTURE;
                    }
                }

                #endregion


                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();

                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "LeftRowField")
                            {
                                _leftRowNode = rootNode.Clone();
                                if (childNodes[i].Attributes != null)
                                {
                                    if (attrId != "undefined")
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {
                                            //// DecodeRowAttr(childNodes[i]);
                                            //mLeftRowvalues.Add(new TreeViewData
                                            //{
                                            //    id = 1,
                                            //    ATTRIBUTE_ID = -1,
                                            //    //Merge = childNodes[i].Attributes["Merge"].Value,
                                            //    ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            //    COLOR = "Black",
                                            //    hasChildren = childNodes[i].ChildNodes.Count > 0
                                            //});

                                            SuperColumnLeft(childNodes[i], mLeftRowvalues, objStplsAttributes, attrId,
                                                "LeftRowField", catalogid, familyid);
                                        }
                                    }
                                    else
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {
                                            //// DecodeRowAttr(childNodes[i]);
                                            mLeftRowvalues.Add(new TreeViewData
                                            {
                                                id = 0,
                                                ATTRIBUTE_ID = -1,
                                                //Merge = childNodes[i].Attributes["Merge"].Value,
                                                ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                                COLOR = "Black",
                                                hasChildren = childNodes[i].ChildNodes.Count > 0
                                            });

                                        }
                                        else
                                        {
                                            int attributeid;
                                            string color = "Blue";
                                            string attributeId = string.Empty;
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 6);
                                                int.TryParse(attributeId, out attributeid);
                                            }
                                            else
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                                int.TryParse(attributeId, out attributeid);
                                            }
                                            // var attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                            // int.TryParse(attributeId, out attributeid);
                                            var checkattr = objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                            if (checkattr.Any())
                                            {
                                                color = "Red";
                                            }
                                            if (color == "Red")
                                            {
                                                if (flag != "MultipleTable")
                                                { 
                                                mLeftRowvalues.Add(new TreeViewData
                                                {
                                                    id = 1,
                                                    ATTRIBUTE_ID = attributeid,
                                                    Merge = childNodes[i].Attributes["Merge"].Value,
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                    COLOR = color,
                                                    hasChildren = false
                                                });
                                                }
                                            }
                                            else
                                            {
                                                var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                    _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                    tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                    .Where(x => x.tps.CATALOG_ID == catalogid
                                                                && x.tps.ATTRIBUTE_ID == attributeid &&
                                                                x.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                x.tpf.FAMILY_ID == familyid);
                                                if (catalogattrchk.Any())
                                                {
                                                    int Attribute_ID = Convert.ToInt32(attributeId);
                                                    var TestResult = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == Attribute_ID 
                                                        && x.ATTRIBUTE_TYPE !=7
                                                        && x.ATTRIBUTE_TYPE != 9
                                                        && x.ATTRIBUTE_TYPE != 11
                                                        && x.ATTRIBUTE_TYPE != 12
                                                        && x.ATTRIBUTE_TYPE != 13
                                                        && x.FLAG_RECYCLE =="A"
                                                        ).ToList();

                                                    if (TestResult.Count != 0)
                                                    {

                                                        mLeftRowvalues.Add(new TreeViewData
                                                        {
                                                            id = 1,
                                                            ATTRIBUTE_ID = attributeid,
                                                            Merge = childNodes[i].Attributes["Merge"].Value,
                                                            ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                            COLOR = color,
                                                            hasChildren = false
                                                        });
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }



                        }
                    }
                }


                //  feilds[0] = _mLeftRowFields.ToArray();
                //if (_mLeftRowFields.Count > 0)
                //{
                //    if (attrId == "undefined")
                //    {
                //        foreach (var item in _mLeftRowFields)
                //        {
                //            if (item.id == 0)
                //            {
                //                var root = new NodeViewModel { id = "1", ATTRIBUTE_ID = item.ATTRIBUTE_ID, Merge = item.Merge, ATTRIBUTE_NAME = item.ATTRIBUTE_NAME, COLOR = item.COLOR, hasChildren = false };
                //                lefttreeitems.Add(root);
                //            }
                //            else if (item.id == 1)
                //            {
                //                var root = new NodeViewModel { id = "0", ATTRIBUTE_ID = item.ATTRIBUTE_ID, Merge = item.Merge, ATTRIBUTE_NAME = item.ATTRIBUTE_NAME, COLOR = item.COLOR, hasChildren = true };
                //                lefttreeitems.Add(root);
                //            }
                //        }
                //    }
                //    else
                //    {
                //        foreach (var item in _mLeftRowFields)
                //        {
                //            if (item.id == 2)
                //            {

                //                var root = new NodeViewModel { id = "Super", ATTRIBUTE_ID = item.ATTRIBUTE_ID, Merge = item.Merge, ATTRIBUTE_NAME = item.ATTRIBUTE_NAME, COLOR = item.COLOR, hasChildren = false };
                //                lefttreeitems.Add(root);
                //            }
                //            else if (item.id == 1)
                //            {
                //                var root = new NodeViewModel { id = "0", ATTRIBUTE_ID = item.ATTRIBUTE_ID, Merge = item.Merge, ATTRIBUTE_NAME = item.ATTRIBUTE_NAME, COLOR = item.COLOR, hasChildren = true };
                //                lefttreeitems.Add(root);
                //            }
                //        }


                //    }
                //}
                //else
                //{

                //    lefttreeitems = new List<NodeViewModel>();
                //    return lefttreeitems;
                //}
                ////if (flag == "MultipleTable")
                ////{
                ////    option = "MULTIPLEALL";
                ////   // int familyid;
                ////   // int.TryParse(familyId, out familyid);
                ////   int catalogid1;
                ////    int.TryParse(mCatalogId, out catalogid1);
                ////    var objStplsAttributesMultiple = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid1, familyid, groupId, option).ToList();
                ////    //var leftTreeItems = mLeftRowvalues;
                ////    for (int i = 0; i < mLeftRowvalues.Count; i++)
                ////    {
                ////        if (mLeftRowvalues[i].ATTRIBUTE_ID != objStplsAttributesMultiple[i].ATTRIBUTE_ID)
                ////        {
                ////            mLeftRowvalues.Remove(mLeftRowvalues[i]);
                ////        }
                        
                ////    }
                ////    //foreach (var objleftrowfields in mLeftRowvalues)
                ////    //{
                ////    //    var objall = objStplsAttributesMultiple.FirstOrDefault(x => x.ATTRIBUTE_ID == objleftrowfields.ATTRIBUTE_ID);
                ////    //    if (objall != null)
                ////    //    {
                ////    //        objStplsAttributesMultiple.Remove(objall);
                           
                ////    //    }
                ////    //}
                    
                ////    //var mleftrowfiled = objStplsAttributesMultiple.Select(x => new TreeViewData
                ////    //{
                ////    //    ATTRIBUTE_ID = x.ATTRIBUTE_ID,
                ////    //    ATTRIBUTE_NAME = x.ATTRIBUTE_NAME,
                ////    //    hasChildren = false,
                ////    //    COLOR = x.COLOR
                ////    //}).ToList();
                ////    //return mleftrowfiled;
                ////   // mLeftRowvalues = mleftrowfiled;
                ////}
                return mLeftRowvalues;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at TableDesignerApiController: DecodeLayoutXmlLeftTree", ex);
                var mLeftRowv = new List<TreeViewData>(); ;
                return mLeftRowv;
            }

        }

        [HttpPost]
        public List<TreeViewData> DecodeLayoutXmlRightTree(string structureName, string mCatalogId, string familyId, string attrId, string flag, int groupId,int packId, object model)
        {
            try
            {
                string option = "GETUNPUBLISHEDATTRS";
                int familyid;
                int.TryParse(familyId, out familyid);
                int catalogid;
                int.TryParse(mCatalogId, out catalogid);
                var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                var mLeftRowvalues = new List<TreeViewData>();
                _mRightRowNodes = new List<XmlNode>();
                _rightRowNode = null;

                string xmlStr = Convert.ToString(model);

                #region Attribute Pack in Table Designer

                if (packId > 0)
                {
                    TB_FAMILY_TABLE_STRUCTURE fTS = new TB_FAMILY_TABLE_STRUCTURE();
                    fTS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(s => s.CATALOG_ID == catalogid && s.FAMILY_ID == familyid && s.STRUCTURE_NAME == structureName).FirstOrDefault();
                    TB_ATTRIBUTE_PACK_TABLE_STRUCTURE apTableStructure = new TB_ATTRIBUTE_PACK_TABLE_STRUCTURE();
                    if (fTS != null && fTS.ID > 0)
                    {
                        apTableStructure = _dbcontext.TB_ATTRIBUTE_PACK_TABLE_STRUCTURE.Where(s => s.PACK_ID == packId && s.FAMILY_STRUCTURE_ID == fTS.ID).FirstOrDefault();
                    }
                    if (apTableStructure != null && apTableStructure.ID > 0)
                    {
                        xmlStr = apTableStructure.TABLE_STRUCTURE;
                    }
                }

                #endregion
                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();

                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "RightRowField")
                            {
                                _rightRowNode = rootNode.Clone();
                                if (childNodes[i].Attributes != null)
                                {
                                    if (attrId != "undefined")
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {
                                            //// DecodeRowAttr(childNodes[i]);
                                            //mLeftRowvalues.Add(new TreeViewData
                                            //{
                                            //    id = 1,
                                            //    ATTRIBUTE_ID = -1,
                                            //    //Merge = childNodes[i].Attributes["Merge"].Value,
                                            //    ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            //    COLOR = "Black",
                                            //    hasChildren = childNodes[i].ChildNodes.Count > 0
                                            //});

                                            SuperColumnRight(childNodes[i], mLeftRowvalues, objStplsAttributes, attrId, "RightRowField", catalogid, familyid);
                                        }
                                    }
                                    else
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {
                                            //// DecodeRowAttr(childNodes[i]);
                                            mLeftRowvalues.Add(new TreeViewData
                                            {
                                                id = 0,
                                                ATTRIBUTE_ID = -1,
                                                //Merge = childNodes[i].Attributes["Merge"].Value,
                                                ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                                COLOR = "Black",
                                                hasChildren = childNodes[i].ChildNodes.Count > 0
                                            });

                                        }
                                        else
                                        {
                                            int attributeid;
                                            string color = "Blue";
                                            //var attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                            // int.TryParse(attributeId, out attributeid);
                                            string attributeId = string.Empty;
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 6);
                                                int.TryParse(attributeId, out attributeid);
                                            }
                                            else
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                                int.TryParse(attributeId, out attributeid);
                                            }
                                            var checkattr = objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                            if (checkattr.Any())
                                            {
                                                color = "Red";
                                            }

                                            if (color == "Red")
                                            {
                                                if (flag != "MultipleTable")
                                                {
                                                    mLeftRowvalues.Add(new TreeViewData
                                                    {
                                                        id = 1,
                                                        ATTRIBUTE_ID = attributeid,
                                                        Merge = childNodes[i].Attributes["Merge"].Value,
                                                        ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                        COLOR = color,
                                                        hasChildren = false
                                                    });
                                                }
                                            }
                                            else
                                            {
                                                var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                      _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                      tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                      .Join(_dbcontext.TB_ATTRIBUTE, TCPF => TCPF.tpf.ATTRIBUTE_ID, TA => TA.ATTRIBUTE_ID, (TCPF, TA) => new { TCPF, TA })
                                                      .Where(x => x.TCPF.tps.CATALOG_ID == catalogid
                                                                  && x.TCPF.tps.ATTRIBUTE_ID == attributeid &&
                                                                  x.TCPF.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                  x.TCPF.tpf.FAMILY_ID == familyid
                                                                  && x.TA.FLAG_RECYCLE == "A");


                                                if (catalogattrchk.Any())
                                                {
                                                    mLeftRowvalues.Add(new TreeViewData
                                                    {
                                                        id = 1,
                                                        ATTRIBUTE_ID = attributeid,
                                                        Merge = childNodes[i].Attributes["Merge"].Value,
                                                        ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                        COLOR = color,
                                                        hasChildren = false
                                                    });
                                                }
                                            }
                                        }


                                    }



                                }
                            }



                        }
                    }
                }

                return mLeftRowvalues;

            }
            catch (Exception ex)
            {
                Logger.Error("Error at TableDesignerApiController: DecodeLayoutXmlRightTree", ex);
                var mLeftRowv = new List<TreeViewData>(); ;
                return mLeftRowv;
            }

        }

        [HttpPost]
        public List<TreeViewData> DecodeLayoutXmlSummaryTree(string structureName, string mCatalogId, string familyId, string attrId, string flag, int groupId,int packId, object model)
        {
            try
            {
                string option = "GETUNPUBLISHEDATTRS";
                int familyid;
                int.TryParse(familyId, out familyid);
                int catalogid;
                int.TryParse(mCatalogId, out catalogid);
                var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                var mLeftRowvalues = new List<TreeViewData>();
                _mSummaryNodes = new List<XmlNode>();

                string xmlStr = Convert.ToString(model);

                #region Attribute Pack in Table Designer

                if (packId > 0)
                {
                    TB_FAMILY_TABLE_STRUCTURE fTS = new TB_FAMILY_TABLE_STRUCTURE();
                    fTS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(s => s.CATALOG_ID == catalogid && s.FAMILY_ID == familyid && s.STRUCTURE_NAME == structureName).FirstOrDefault();
                    TB_ATTRIBUTE_PACK_TABLE_STRUCTURE apTableStructure = new TB_ATTRIBUTE_PACK_TABLE_STRUCTURE();
                    if (fTS != null && fTS.ID > 0)
                    {
                        apTableStructure = _dbcontext.TB_ATTRIBUTE_PACK_TABLE_STRUCTURE.Where(s => s.PACK_ID == packId && s.FAMILY_STRUCTURE_ID == fTS.ID).FirstOrDefault();
                    }
                    if (apTableStructure != null && apTableStructure.ID > 0)
                    {
                        xmlStr = apTableStructure.TABLE_STRUCTURE;
                    }
                } 

                #endregion


                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();

                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "SummaryField")
                            {

                                if (childNodes[i].Attributes != null)
                                {
                                    if (attrId != "undefined")
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {

                                            SuperColumnSummary(childNodes[i], mLeftRowvalues, objStplsAttributes, attrId, "SummaryField", catalogid, familyid);
                                        }
                                    }
                                    else
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {
                                            //// DecodeRowAttr(childNodes[i]);
                                            mLeftRowvalues.Add(new TreeViewData
                                            {
                                                id = 0,
                                                ATTRIBUTE_ID = -1,
                                                //Merge = childNodes[i].Attributes["Merge"].Value,
                                                ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                                COLOR = "Black",
                                                hasChildren = childNodes[i].ChildNodes.Count > 0
                                            });

                                        }
                                        else
                                        {
                                            int attributeid;
                                            string color = "Blue";
                                            //var attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                            // int.TryParse(attributeId, out attributeid);
                                            string attributeId = string.Empty;
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 6);
                                                int.TryParse(attributeId, out attributeid);
                                            }
                                            else
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                                int.TryParse(attributeId, out attributeid);
                                            }
                                            var checkattr = objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                            if (checkattr.Any())
                                            {
                                                color = "Red";
                                            }

                                            if (color == "Red")
                                            {
                                                mLeftRowvalues.Add(new TreeViewData
                                                {
                                                    id = 1,
                                                    ATTRIBUTE_ID = attributeid,
                                                    Merge = childNodes[i].Attributes["Merge"].Value,
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                    COLOR = color,
                                                    hasChildren = false
                                                });
                                            }
                                            else
                                            {
                                                //var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                //     _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                //     tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                //     .Where(x => x.tps.CATALOG_ID == catalogid
                                                //                 && x.tps.ATTRIBUTE_ID == attributeid &&
                                                //                 x.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                //                 x.tpf.FAMILY_ID == familyid);
                                                     var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                     _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                     tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                     .Join(_dbcontext.TB_ATTRIBUTE, TCPF => TCPF.tpf.ATTRIBUTE_ID, TA => TA.ATTRIBUTE_ID, (TCPF, TA) => new { TCPF, TA })
                                                     .Where(x => x.TCPF.tps.CATALOG_ID == catalogid
                                                                 && x.TCPF.tps.ATTRIBUTE_ID == attributeid &&
                                                                 x.TCPF.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                 x.TCPF.tpf.FAMILY_ID == familyid
                                                                 && x.TA.FLAG_RECYCLE == "A");




                                                if (catalogattrchk.Any())
                                                {
                                                    mLeftRowvalues.Add(new TreeViewData
                                                    {
                                                        id = 1,
                                                        ATTRIBUTE_ID = attributeid,
                                                        Merge = childNodes[i].Attributes["Merge"].Value,
                                                        ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                        COLOR = color,
                                                        hasChildren = false
                                                    });
                                                }
                                            }

                                        }
                                    }
                                }
                            }



                        }
                    }
                }



                return mLeftRowvalues;

            }
            catch (Exception ex)
            {
                Logger.Error("Error at TableDesignerApiController: DecodeLayoutXmlSummaryTree", ex);
                var mLeftRowv = new List<TreeViewData>(); ;
                return mLeftRowv;
            }
        }

        [HttpPost]
        public List<NodeViewModel> DecodeLayoutXmlColumnTree(string structureName, string mCatalogId, string familyId, string attrId, string flag, int groupId,int packId, object model)
        {
            var Columntreeitems = new List<NodeViewModel>();
            //   xmlStr = "<?xml version=\"1.0\" encoding=\"utf-8\"?><TradingBell TableType=\"SuperTable\"><ColumnRowField AttrID=\"Super\" AttrName=\"TEST\" Level=\"0\"> <ColumnRowField AttrID=\"Attr:42\" Merge=\"Unchecked\" Level=\"1\"></ColumnRowField><ColumnRowField AttrID=\"Attr:1\" Merge=\"Unchecked\" Level=\"1\"></ColumnRowField></ColumnRowField><ColumnGroupField>[none]</ColumnGroupField><TableGroupField>[none]</TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>False</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplayColumnHeader>False</DisplayColumnHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeColumnFields></MergeColumnFields></TradingBell>";
            try
            {
                string option = "GETUNPUBLISHEDATTRS";
                int familyid;
                int.TryParse(familyId, out familyid);
                int catalogid;
                int.TryParse(mCatalogId, out catalogid);
                var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                var _mColumnRowFields = new List<TreeViewData>();
                _mColumnNodes = new List<XmlNode>();

                string xmlStr = Convert.ToString(model);

                #region Attribute Pack in Table Designer
                if (packId > 0)
                {
                    TB_FAMILY_TABLE_STRUCTURE fTS = new TB_FAMILY_TABLE_STRUCTURE();
                    fTS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(s => s.CATALOG_ID == catalogid && s.FAMILY_ID == familyid && s.STRUCTURE_NAME == structureName).FirstOrDefault();
                    TB_ATTRIBUTE_PACK_TABLE_STRUCTURE apTableStructure = new TB_ATTRIBUTE_PACK_TABLE_STRUCTURE();
                    if (fTS != null && fTS.ID > 0)
                    {
                        apTableStructure = _dbcontext.TB_ATTRIBUTE_PACK_TABLE_STRUCTURE.Where(s => s.PACK_ID == packId && s.FAMILY_STRUCTURE_ID == fTS.ID).FirstOrDefault();
                    }
                    if (apTableStructure != null && apTableStructure.ID > 0)
                    {
                        xmlStr = apTableStructure.TABLE_STRUCTURE;
                    }
                }
                #endregion

                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();

                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "ColumnField")
                            {

                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        //// DecodeRowAttr(childNodes[i]);
                                        _mColumnRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });

                                        for (int j = 0; j < childNodes[i].ChildNodes.Count; j++)
                                        {
                                            if (childNodes[i].ChildNodes[j].Name == "ColumnField")
                                            {
                                                if (childNodes[i].ChildNodes[j].Attributes != null)
                                                {
                                                    if (childNodes[i].Attributes["AttrName"].Value == attrId)
                                                    {
                                                        int attributeid;
                                                        string color = "Blue";
                                                        //var attributeId =
                                                        //    childNodes[i].ChildNodes[j].Attributes["AttrID"].Value
                                                        //        .Remove(0, 5);
                                                        //int.TryParse(attributeId, out attributeid);
                                                        string attributeId = string.Empty;
                                                        if (childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Contains("nAttr"))
                                                        {
                                                            attributeId = childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 6);
                                                            int.TryParse(attributeId, out attributeid);
                                                        }
                                                        else
                                                        {
                                                            attributeId = childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 5);
                                                            int.TryParse(attributeId, out attributeid);
                                                        }
                                                        var checkattr =
                                                            objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                                        if (checkattr.Any())
                                                        {
                                                            color = "Red";
                                                        }
                                                        if (color == "Red")
                                                        {
                                                            _mColumnRowFields.Add(new TreeViewData
                                                            {
                                                                id = 2,
                                                                ATTRIBUTE_ID = attributeid,
                                                                Merge = childNodes[i].ChildNodes[j].Attributes["Merge"].Value,
                                                                ATTRIBUTE_NAME =
                                                                    GetAttributeNameFromAttributeID(attributeId),
                                                                COLOR = color
                                                            });
                                                        }
                                                        else
                                                        {
                                                            var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                      _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                      tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                      .Join(_dbcontext.TB_ATTRIBUTE,TCPF=>TCPF.tpf.ATTRIBUTE_ID,TA=>TA.ATTRIBUTE_ID,(TCPF, TA) => new { TCPF, TA })
                                                      .Where(x => x.TCPF.tps.CATALOG_ID == catalogid
                                                                  && x.TCPF.tps.ATTRIBUTE_ID == attributeid &&
                                                                  x.TCPF.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                  x.TCPF.tpf.FAMILY_ID == familyid
                                                                  && x.TA.FLAG_RECYCLE=="A");
                                                            if (catalogattrchk.Any())
                                                            {
                                                                _mColumnRowFields.Add(new TreeViewData
                                                                {
                                                                    id = 2,
                                                                    ATTRIBUTE_ID = attributeid,
                                                                    Merge = childNodes[i].ChildNodes[j].Attributes["Merge"].Value,
                                                                    ATTRIBUTE_NAME =
                                                                        GetAttributeNameFromAttributeID(attributeId),
                                                                    COLOR = color
                                                                });
                                                            }
                                                        }

                                                    }
                                                }
                                            }

                                        }
                                    }
                                    else
                                    {
                                        int attributeid;
                                        string color = "Blue";
                                        // int.TryParse(, out attributeid);
                                        //var attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                        //int.TryParse(attributeId, out attributeid);
                                        string attributeId = string.Empty;
                                        if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                        {
                                            attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 6);
                                            int.TryParse(attributeId, out attributeid);
                                        }
                                        else
                                        {
                                            attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                            int.TryParse(attributeId, out attributeid);
                                        }
                                        var checkattr = objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                        if (checkattr.Any())
                                        {
                                            color = "Red";
                                        }
                                        //_mColumnRowFields.Add(new TreeViewData
                                        //{
                                        //    id = 0,
                                        //    ATTRIBUTE_ID = attributeid,
                                        //    Merge = childNodes[i].Attributes["Merge"].Value,
                                        //    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                        //    COLOR = color
                                        //});
                                        var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                      _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                      tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                      .Join(_dbcontext.TB_ATTRIBUTE, TCPF => TCPF.tpf.ATTRIBUTE_ID, TA => TA.ATTRIBUTE_ID, (TCPF, TA) => new { TCPF, TA })
                                                      .Where(x => x.TCPF.tps.CATALOG_ID == catalogid
                                                                  && x.TCPF.tps.ATTRIBUTE_ID == attributeid &&
                                                                  x.TCPF.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                  x.TCPF.tpf.FAMILY_ID == familyid
                                                                  && x.TA.FLAG_RECYCLE == "A");
                                        if (catalogattrchk.Any())
                                        {
                                            _mColumnRowFields.Add(new TreeViewData
                                            {
                                                id = 0,
                                                ATTRIBUTE_ID = attributeid,
                                                Merge = childNodes[i].Attributes["Merge"].Value,
                                                ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                COLOR = color
                                            });
                                        }
                                    }

                                }
                            }



                        }
                    }
                }


                //  feilds[0] = _mColumnRowFields.ToArray();
                if (_mColumnRowFields.Count > 0)
                {
                    if (attrId == "undefined")
                    {
                        foreach (var item in _mColumnRowFields)
                        {
                            if (item.id == 0)
                            {
                                var root = new NodeViewModel { id = "1", ATTRIBUTE_ID = item.ATTRIBUTE_ID, Merge = item.Merge, ATTRIBUTE_NAME = item.ATTRIBUTE_NAME, COLOR = item.COLOR, hasChildren = false };
                                Columntreeitems.Add(root);
                            }
                            else if (item.id == 1)
                            {
                                var root = new NodeViewModel { id = "0", ATTRIBUTE_ID = item.ATTRIBUTE_ID, Merge = item.Merge, ATTRIBUTE_NAME = item.ATTRIBUTE_NAME, COLOR = item.COLOR, hasChildren = true };
                                Columntreeitems.Add(root);
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in _mColumnRowFields)
                        {
                            if (item.id == 2)
                            {

                                var root = new NodeViewModel { id = "Super", ATTRIBUTE_ID = item.ATTRIBUTE_ID, Merge = item.Merge, ATTRIBUTE_NAME = item.ATTRIBUTE_NAME, COLOR = item.COLOR, hasChildren = false };
                                Columntreeitems.Add(root);
                            }
                        }


                    }
                }
                else
                {

                    Columntreeitems = new List<NodeViewModel>();
                    return Columntreeitems;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return Columntreeitems;
        }

        [HttpPost]
        public List<STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES_Result> GettableGroupHeaderDataSource(string catalogId, string familyId, string flag, int groupId, int packId, string structureName, object model)
        {
            try
            {
                var _mLeftRowFields = new List<TreeViewData>();
                string xmlStr = Convert.ToString(model);

                #region Attribute Pack in table Deisgner

                if (packId > 0)
                {
                    int c_id = Convert.ToInt32(catalogId);
                    int f_Id = Convert.ToInt32(familyId);
                    TB_FAMILY_TABLE_STRUCTURE fTS = new TB_FAMILY_TABLE_STRUCTURE();
                    fTS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(s => s.CATALOG_ID == c_id && s.FAMILY_ID == f_Id && s.STRUCTURE_NAME == structureName).FirstOrDefault();
                    TB_ATTRIBUTE_PACK_TABLE_STRUCTURE apTableStructure = new TB_ATTRIBUTE_PACK_TABLE_STRUCTURE();
                    if (fTS != null && fTS.ID > 0)
                    {
                        apTableStructure = _dbcontext.TB_ATTRIBUTE_PACK_TABLE_STRUCTURE.Where(s => s.PACK_ID == packId && s.FAMILY_STRUCTURE_ID == fTS.ID).FirstOrDefault();
                    }
                    if (apTableStructure != null && apTableStructure.ID > 0)
                    {
                        xmlStr = apTableStructure.TABLE_STRUCTURE;
                    }
                }


                #endregion

                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "LeftRowField")
                            {
                                // _leftRowNode = rootNode.Clone();
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        //  DecodeRowAttr(childNodes[i]);

                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            //Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });

                                        Allattrs(childNodes[i], _mLeftRowFields, "LeftRowField");
                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + catalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {

                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            else
                                            {

                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                            // _mLeftRowNodes.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "RightRowField")
                            {
                                // if (rightRowNode == null)
                                // rightRowNode = childNodes[i].CloneNode(true);
                                _rightRowNode = rootNode.Clone();
                                _rightRowNode.AppendChild(childNodes[i].CloneNode(true));
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            // Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });
                                        Allattrs(childNodes[i], _mLeftRowFields, "RightRowField");

                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + catalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {

                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                            else
                                            {
                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                            // _mRightRowNodes.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "ColumnField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    string attrID =
                                        childNodes[i].Attributes["AttrID"].Value.Substring(
                                            childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                StringComparison.Ordinal) + 1,
                                            childNodes[i].Attributes["AttrID"].Value.Length -
                                            (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                StringComparison.Ordinal) + 1));
                                    _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                 " AND CATALOG_ID = " + catalogId + "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                    {
                                        var attributeName = childNodes[i].Attributes["AttrID"].Value.Contains("nAttr")
                                            ? GetAttributeNameFromAttributeID(
                                                childNodes[i].Attributes["AttrID"].Value.Remove(0, 6))
                                            : GetAttributeNameFromAttributeID(
                                                childNodes[i].Attributes["AttrID"].Value.Remove(0, 5));
                                        var attributeID = childNodes[i].Attributes["AttrID"].Value.Contains("nAttr") ? childNodes[i].Attributes["AttrID"].Value.Remove(0, 6) : childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            ATTRIBUTE_ID = Convert.ToInt32(attributeID),
                                            ATTRIBUTE_NAME = attributeName,
                                            COLOR = "BLUE"
                                        });
                                        //_mColumnNodes.Add(childNodes[i].Clone());
                                    }
                                }
                            }

                            if (childNodes[i].Name == "SummaryField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            //Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });

                                        Allattrs(childNodes[i], _mLeftRowFields, "SummaryField");

                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + catalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            else
                                            {
                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            //  _mLeftRowFields.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }


                        }
                    }
                }
                //string option = "getPublishedAttrs";
                //int familyid;
                //int.TryParse(familyId, out familyid);
                //int catalogid;
                //int.TryParse(catalogId, out catalogid);
                //var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                //foreach (var objleftrowfields in _mLeftRowFields)
                //{
                //    var objall = objStplsAttributes.FirstOrDefault(x => x.ATTRIBUTE_ID == objleftrowfields.ATTRIBUTE_ID);
                //    if (objall != null)
                //    {
                //        objStplsAttributes.Remove(objall);

                //    }

                //}

                string option = "GETPUBLISHEDATTRS";
                if (flag == "MultipleTable")
                {
                    option = "MULTIPLEALL";
                    int familyid;
                    int.TryParse(familyId, out familyid);
                    int catalogid;
                    int.TryParse(catalogId, out catalogid);
                    var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                    foreach (var objleftrowfields in _mLeftRowFields)
                    {
                        var objall = objStplsAttributes.FirstOrDefault(x => x.ATTRIBUTE_ID == objleftrowfields.ATTRIBUTE_ID);
                        if (objall != null)
                        {
                            objStplsAttributes.Remove(objall);

                        }

                    }
                    objStplsAttributes.RemoveAll(x => x.COLOR == "RED");
                    return objStplsAttributes;
                }
                else
                {
                    int familyid;
                    int.TryParse(familyId, out familyid);
                    int catalogid;
                    int.TryParse(catalogId, out catalogid);
                    if (packId > 0)
                        option = "ATTRIBUTEPACK";
                    else
                        packId = 0;
                    var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, packId, option).ToList();
                    foreach (var objleftrowfields in _mLeftRowFields)
                    {
                        var objall = objStplsAttributes.FirstOrDefault(x => x.ATTRIBUTE_ID == objleftrowfields.ATTRIBUTE_ID);
                        if (objall != null)
                        {
                            objStplsAttributes.Remove(objall);

                        }

                    }
                    objStplsAttributes.RemoveAll(x => x.COLOR == "RED");
                    return objStplsAttributes;

                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GettableGroupHeaderDataSource", ex);
                return null;
            }
        }


        [HttpPost]
        public List<TreeViewData> GettableGroupSummaryDataSource(string catalogId, string familyId, string flag, int groupId, int packId, string structureName, object model)
        {
            try
            {
                var mLeftRowFields = new List<TreeViewData>();
                string xmlStr = Convert.ToString(model);

                #region Attribute Pack in Table Designer

                if (packId > 0)
                {
                    int c_id = Convert.ToInt32(catalogId);
                    int f_Id = Convert.ToInt32(familyId);
                    TB_FAMILY_TABLE_STRUCTURE fTS = new TB_FAMILY_TABLE_STRUCTURE();
                    fTS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(s => s.CATALOG_ID == c_id && s.FAMILY_ID == f_Id && s.STRUCTURE_NAME == structureName).FirstOrDefault();
                    TB_ATTRIBUTE_PACK_TABLE_STRUCTURE apTableStructure = new TB_ATTRIBUTE_PACK_TABLE_STRUCTURE();
                    if (fTS != null && fTS.ID > 0)
                    {
                        apTableStructure = _dbcontext.TB_ATTRIBUTE_PACK_TABLE_STRUCTURE.Where(s => s.PACK_ID == packId && s.FAMILY_STRUCTURE_ID == fTS.ID).FirstOrDefault();
                    }
                    if (apTableStructure != null && apTableStructure.ID > 0)
                    {
                        xmlStr = apTableStructure.TABLE_STRUCTURE;
                    }
                }

                #endregion

                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "SummaryField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        //mLeftRowFields.Add(new TreeViewData
                                        //{
                                        //    id = 1,
                                        //    ATTRIBUTE_ID = -1,
                                        //    //Merge = childNodes[i].Attributes["Merge"].Value,
                                        //    ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                        //    COLOR = "BLACK"
                                        //});

                                        AllSummaryattrs(childNodes[i], mLeftRowFields, "SummaryField");

                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + catalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            else
                                            {
                                                mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            //  _mLeftRowFields.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }


                        }
                    }
                }
                //string option = "getPublishedAttrs";
                //int familyid;
                //int.TryParse(familyId, out familyid);
                //int catalogid;
                //int.TryParse(catalogId, out catalogid);
                //var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, option).ToList();
                //foreach (var objleftrowfields in _mLeftRowFields)
                //{
                //    var objall = objStplsAttributes.FirstOrDefault(x => x.ATTRIBUTE_ID == objleftrowfields.ATTRIBUTE_ID);
                //    if (objall != null)
                //    {
                //        objStplsAttributes.Remove(objall);

                //    }

                //}
                //objStplsAttributes.RemoveAll(x => x.COLOR == "RED");
                return mLeftRowFields;

            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GettableGroupSummaryDataSource", ex);
                return null;
            }
        }

        public void Allattrs(XmlNode childNodes, List<TreeViewData> _mLeftRowFields, string nodefield)
        {
            for (int j = 0; j < childNodes.ChildNodes.Count; j++)
            {
                if (childNodes.ChildNodes[j].Name == nodefield)
                {
                    if (childNodes.ChildNodes[j].Attributes != null)
                    {
                        if (
                            childNodes.ChildNodes[j].Attributes["AttrID"].Value != "Super")
                        {
                            int attributeid = 0;
                            string attributeId = string.Empty;
                            if (childNodes.ChildNodes[j].Attributes["AttrID"].Value.Contains("nAttr"))
                            {
                                attributeId = childNodes.ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 6);
                                int.TryParse(attributeId, out attributeid);
                            }
                            else
                            {
                                attributeId = childNodes.ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 5);
                                int.TryParse(attributeId, out attributeid);
                            }
                            _mLeftRowFields.Add(new TreeViewData
                            {
                                id = 2,
                                ATTRIBUTE_ID = attributeid,
                                // Merge = childNodes[i].Attributes["Merge"].Value,
                                ATTRIBUTE_NAME =
                                    GetAttributeNameFromAttributeID(attributeId),
                                COLOR = "BLUE"
                            });
                        }
                        else
                        {
                            _mLeftRowFields.Add(new TreeViewData
                            {
                                id = 1,
                                ATTRIBUTE_ID = -1,
                                //Merge = childNodes[i].Attributes["Merge"].Value,
                                ATTRIBUTE_NAME = childNodes.ChildNodes[j].Attributes["AttrName"].Value,
                                COLOR = "BLACK"
                            });

                            Allattrs(childNodes.ChildNodes[j], _mLeftRowFields, nodefield);
                        }
                    }
                }

            }


        }

        public void AllSummaryattrs(XmlNode childNodes, List<TreeViewData> _mLeftRowFields, string nodefield)
        {
            for (int j = 0; j < childNodes.ChildNodes.Count; j++)
            {
                if (childNodes.ChildNodes[j].Name == nodefield)
                {
                    if (childNodes.ChildNodes[j].Attributes != null)
                    {
                        if (
                            childNodes.ChildNodes[j].Attributes["AttrID"].Value != "Super")
                        {
                            int attributeid = 0;
                            string attributeId = string.Empty;
                            if (childNodes.ChildNodes[j].Attributes["AttrID"].Value.Contains("nAttr"))
                            {
                                attributeId = childNodes.ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 6);
                                int.TryParse(attributeId, out attributeid);
                            }
                            else
                            {
                                attributeId = childNodes.ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 5);
                                int.TryParse(attributeId, out attributeid);
                            }
                            _mLeftRowFields.Add(new TreeViewData
                            {
                                id = 2,
                                ATTRIBUTE_ID = attributeid,
                                // Merge = childNodes[i].Attributes["Merge"].Value,
                                ATTRIBUTE_NAME =
                                    GetAttributeNameFromAttributeID(attributeId),
                                COLOR = "BLUE"
                            });
                        }
                        else
                        {
                            //_mLeftRowFields.Add(new TreeViewData
                            //{
                            //    id = 1,
                            //    ATTRIBUTE_ID = -1,
                            //    //Merge = childNodes[i].Attributes["Merge"].Value,
                            //    ATTRIBUTE_NAME = childNodes.ChildNodes[j].Attributes["AttrName"].Value,
                            //    COLOR = "BLACK"
                            //});

                            AllSummaryattrs(childNodes.ChildNodes[j], _mLeftRowFields, nodefield);
                        }
                    }
                }

            }


        }

        [HttpPost]
        public List<TreeViewData> GetAllAttributeDataSource(string structureName, string catalogId, string familyId, string flag, int groupId, int packId, object model)
        {
            try
            {
                var _mLeftRowFields = new List<TreeViewData>();
                string xmlStr = Convert.ToString(model);

                if (structureName == "Default Layout")
                {
                    string tableStructure = "<?xml version=\"1.0\" encoding=\"utf-8\"?><TradingBell TableType=\"SuperTable\"><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>";
                    if (xmlStr.Contains("\r\n     "))
                    {
                        xmlStr = xmlStr.Replace("\r\n     ", "");
                    }
                    if (xmlStr == tableStructure)
                    {
                        TB_FAMILY_TABLE_STRUCTURE fTS = new TB_FAMILY_TABLE_STRUCTURE();
                        int catalogid = Convert.ToInt32(catalogId);
                        int family_id = Convert.ToInt32(familyId);
                        fTS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(s => s.CATALOG_ID == catalogid && s.FAMILY_ID == family_id && s.STRUCTURE_NAME == "Default Layout").FirstOrDefault();
                        if (fTS != null)
                        {
                            if (fTS.FAMILY_TABLE_STRUCTURE != xmlStr)
                            {
                                xmlStr = fTS.FAMILY_TABLE_STRUCTURE;
                            }
                        }
                    }
                }

                if (packId > 0)
                {
                    TB_FAMILY_TABLE_STRUCTURE fTS = new TB_FAMILY_TABLE_STRUCTURE();
                    int catalogid = Convert.ToInt32(catalogId);
                    int family_id = Convert.ToInt32(familyId);
                    fTS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(s => s.CATALOG_ID == catalogid && s.FAMILY_ID == family_id && s.STRUCTURE_NAME == structureName).FirstOrDefault();
                    TB_ATTRIBUTE_PACK_TABLE_STRUCTURE apTableStructure = new TB_ATTRIBUTE_PACK_TABLE_STRUCTURE();
                    if (fTS != null && fTS.ID > 0)
                    {
                        apTableStructure = _dbcontext.TB_ATTRIBUTE_PACK_TABLE_STRUCTURE.Where(s => s.PACK_ID == packId && s.FAMILY_STRUCTURE_ID == fTS.ID).FirstOrDefault();
                    }
                    if (apTableStructure != null && apTableStructure.ID > 0)
                    {
                        xmlStr = apTableStructure.TABLE_STRUCTURE;
                    }
                }

                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "LeftRowField")
                            {
                                // _leftRowNode = rootNode.Clone();
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        //  DecodeRowAttr(childNodes[i]);

                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            //Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });

                                        Allattrs(childNodes[i], _mLeftRowFields, "LeftRowField");
                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + catalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {

                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            else
                                            {

                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                            // _mLeftRowNodes.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "RightRowField")
                            {
                                // if (rightRowNode == null)
                                // rightRowNode = childNodes[i].CloneNode(true);
                                _rightRowNode = rootNode.Clone();
                                _rightRowNode.AppendChild(childNodes[i].CloneNode(true));
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            // Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });
                                        Allattrs(childNodes[i], _mLeftRowFields, "RightRowField");

                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + catalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {

                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                            else
                                            {
                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                            // _mRightRowNodes.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "ColumnField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    string attrID =
                                        childNodes[i].Attributes["AttrID"].Value.Substring(
                                            childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                StringComparison.Ordinal) + 1,
                                            childNodes[i].Attributes["AttrID"].Value.Length -
                                            (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                StringComparison.Ordinal) + 1));
                                    _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                 " AND CATALOG_ID = " + catalogId + "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                    {
                                        var attributeName = childNodes[i].Attributes["AttrID"].Value.Contains("nAttr")
                                            ? GetAttributeNameFromAttributeID(
                                                childNodes[i].Attributes["AttrID"].Value.Remove(0, 6))
                                            : GetAttributeNameFromAttributeID(
                                                childNodes[i].Attributes["AttrID"].Value.Remove(0, 5));
                                        var attributeID = childNodes[i].Attributes["AttrID"].Value.Contains("nAttr") ? childNodes[i].Attributes["AttrID"].Value.Remove(0, 6) : childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            ATTRIBUTE_ID = Convert.ToInt32(attributeID),
                                            ATTRIBUTE_NAME = attributeName,
                                            COLOR = "BLUE"
                                        });
                                        //_mColumnNodes.Add(childNodes[i].Clone());
                                    }
                                }
                            }

                            if (childNodes[i].Name == "SummaryField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            //Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });

                                        Allattrs(childNodes[i], _mLeftRowFields, "SummaryField");

                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + catalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            else
                                            {
                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            //  _mLeftRowFields.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }


                        }
                    }
                }
                string option = "ALL";
                if (flag == "MultipleTable")
                {
                    option = "MULTIPLEALL";
                    int familyid;
                    int.TryParse(familyId, out familyid);
                    int catalogid;
                    int.TryParse(catalogId, out catalogid);
                    var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                    foreach (var objleftrowfields in _mLeftRowFields)
                    {
                        var objall = objStplsAttributes.FirstOrDefault(x => x.ATTRIBUTE_ID == objleftrowfields.ATTRIBUTE_ID);
                        if (objall != null)
                        {
                            objStplsAttributes.Remove(objall);

                        }
                    }
                    // new List<TreeViewData>();
                    var mleftrowfiled = objStplsAttributes.Select(x => new TreeViewData
                    {
                        ATTRIBUTE_ID = x.ATTRIBUTE_ID,
                        ATTRIBUTE_NAME = x.ATTRIBUTE_NAME,
                        hasChildren = false,
                        COLOR = x.COLOR
                    }).ToList();
                    return mleftrowfiled;
                }
                else
                {
                    int familyid;
                    int.TryParse(familyId, out familyid);
                    int catalogid;
                    int.TryParse(catalogId, out catalogid);
                    if (packId > 0)
                        option = "ATTRIBUTEPACK";
                    else
                        packId = 0;

                    var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, packId, option).ToList();
                    foreach (var objleftrowfields in _mLeftRowFields)
                    {
                        var objall = objStplsAttributes.FirstOrDefault(x => x.ATTRIBUTE_ID == objleftrowfields.ATTRIBUTE_ID);
                        if (objall != null)
                        {
                            objStplsAttributes.Remove(objall);

                        }

                    }

                    var FinalResult = objStplsAttributes.AsEnumerable().Join(_dbcontext.TB_ATTRIBUTE, ASEN => ASEN.ATTRIBUTE_ID, TA => TA.ATTRIBUTE_ID, (ASEN, TA) => new { ASEN, TA }

    ).Where(x => x.TA.ATTRIBUTE_TYPE != 7
                 && x.TA.ATTRIBUTE_TYPE != 9
                 && x.TA.ATTRIBUTE_TYPE != 11
                 && x.TA.ATTRIBUTE_TYPE != 12
                  && x.TA.ATTRIBUTE_TYPE != 13
                  && x.TA.FLAG_RECYCLE=="A"
    ).Select(x => x.ASEN).ToList();
                    

                    // new List<TreeViewData>();
                    var mleftrowfiled = FinalResult.Select(x => new TreeViewData
                    {
                        ATTRIBUTE_ID = x.ATTRIBUTE_ID,
                        ATTRIBUTE_NAME = x.ATTRIBUTE_NAME,
                        hasChildren = false,
                        COLOR = x.COLOR
                    }).ToList();

            return mleftrowfiled;

                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetAllAttributeDataSource", ex);
                return null;
            }
        }


        [HttpPost]
        public IHttpActionResult DecodeLayoutXml(string structureName, string mCatalogId, object model)
        {
            var feilds = new Array[13];
            try
            {
                string xmlStr = Convert.ToString(model);
                var _mRowFields = new List<TreeViewData>();
                var _mSummaryFields = new List<TreeViewData>();
                var _mColumnFields = new List<TreeViewData>();
                var _mLeftRowFields = new List<TreeViewData>();
                var _mRightRowFields = new List<TreeViewData>();
                var _mSummaryGroupFieldSet = new List<string>();
                var _mTableGroupFieldSet = new List<string>();
                var _mPlaceHolderTextSet = new List<string>();
                var _mShowRowHeadersSet = new List<bool>();
                var _mShowColumnHeadersSet = new List<bool>();
                var _mShowSummaryHeadersSet = new List<bool>();
                var _mPivotHeaderTextSet = new List<string>();
                var _mVerticalTableSet = new List<bool>();
                _mLeftRowNodes = new List<XmlNode>();
                _mRightRowNodes = new List<XmlNode>();
                _mSummaryNodes = new List<XmlNode>();
                _mColumnNodes = new List<XmlNode>();
                _mPlaceHolderText = "";
                _mSummaryGroupField = "";
                _mTableGroupField = "";
                _mShowColumnHeaders = false;
                _mShowRowHeaders = false;
                _mShowSummaryHeaders = false;
                _mMergeRowHeaders = false;
                _mMergeSummaryFields = false;
                _leftRowNode = null;
                _rightRowNode = null;
                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "LeftRowField")
                            {
                                _leftRowNode = rootNode.Clone();
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        //  DecodeRowAttr(childNodes[i]);

                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            //Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });

                                        for (int j = 0; j < childNodes[i].ChildNodes.Count; j++)
                                        {
                                            if (childNodes[i].ChildNodes[j].Name == "LeftRowField")
                                            {
                                                if (childNodes[i].ChildNodes[j].Attributes != null)
                                                {

                                                    _mLeftRowFields.Add(new TreeViewData
                                                    {
                                                        id = 2,
                                                        ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 5)),
                                                        // Merge = childNodes[i].Attributes["Merge"].Value,
                                                        ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 5)),
                                                        COLOR = "BLUE"
                                                    });
                                                }
                                            }

                                        }
                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + mCatalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                _mRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });
                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            else
                                            {
                                                _mRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });
                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                            _mLeftRowNodes.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "RightRowField")
                            {
                                // if (rightRowNode == null)
                                // rightRowNode = childNodes[i].CloneNode(true);
                                _rightRowNode = rootNode.Clone();
                                _rightRowNode.AppendChild(childNodes[i].CloneNode(true));
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        _mRightRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            // Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });

                                        for (int j = 0; j < childNodes[i].ChildNodes.Count; j++)
                                        {
                                            if (childNodes[i].ChildNodes[j].Name == "RightRowField")
                                            {
                                                if (childNodes[i].ChildNodes[j].Attributes != null)
                                                {

                                                    _mRightRowFields.Add(new TreeViewData
                                                    {
                                                        id = 2,
                                                        ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 5)),
                                                        // Merge = childNodes[i].Attributes["Merge"].Value,
                                                        ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                            childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 5)),
                                                        COLOR = "BLUE"
                                                    });
                                                }
                                            }

                                        }
                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + mCatalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                _mRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });
                                                _mRightRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                            else
                                            {
                                                _mRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });
                                                _mRightRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                            _mRightRowNodes.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "ColumnField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    string attrID =
                                        childNodes[i].Attributes["AttrID"].Value.Substring(
                                            childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                StringComparison.Ordinal) + 1,
                                            childNodes[i].Attributes["AttrID"].Value.Length -
                                            (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                StringComparison.Ordinal) + 1));
                                    _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                 " AND CATALOG_ID = " + mCatalogId + "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                    {
                                        var attribute_name = childNodes[i].Attributes["AttrID"].Value.Contains("nAttr")
                                            ? GetAttributeNameFromAttributeID(
                                                childNodes[i].Attributes["AttrID"].Value.Remove(0, 6))
                                            : GetAttributeNameFromAttributeID(
                                                childNodes[i].Attributes["AttrID"].Value.Remove(0, 5));
                                        var attribute_id = childNodes[i].Attributes["AttrID"].Value.Contains("nAttr") ? childNodes[i].Attributes["AttrID"].Value.Remove(0, 6) : childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                        _mColumnFields.Add(new TreeViewData
                                        {
                                            ATTRIBUTE_ID = Convert.ToInt32(attribute_id),
                                            ATTRIBUTE_NAME = attribute_name,
                                            COLOR = "BLUE"
                                        });
                                        _mColumnNodes.Add(childNodes[i].Clone());
                                    }
                                }
                            }

                            if (childNodes[i].Name == "SummaryField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        _mSummaryFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            //Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });

                                        for (int j = 0; j < childNodes[i].ChildNodes.Count; j++)
                                        {
                                            if (childNodes[i].ChildNodes[j].Name == "SummaryField")
                                            {
                                                if (childNodes[i].ChildNodes[j].Attributes != null)
                                                {

                                                    _mSummaryFields.Add(new TreeViewData
                                                    {
                                                        id = 2,
                                                        ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 5)),
                                                        //Merge = childNodes[i].Attributes["Merge"].Value,
                                                        ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                            childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 5)),
                                                        COLOR = "BLUE"
                                                    });
                                                }
                                            }

                                        }
                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + mCatalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                _mSummaryFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });
                                                objGroupedSummaryAttrs.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)), "None");
                                            }
                                            else
                                            {
                                                _mSummaryFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });
                                                objGroupedSummaryAttrs.Add(
                                                    GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)), "None");
                                            }
                                            _mSummaryNodes.Add(childNodes[i].Clone());
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "SummaryGroupField")
                            {
                                _mSummaryGroupField = "";
                                if (childNodes[i].InnerText.Trim() != string.Empty &&
                                    childNodes[i].InnerText.Trim() != "[none]")
                                {
                                    string attrID = childNodes[i].InnerText.Remove(0, 5);
                                    _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                 " AND CATALOG_ID = " + mCatalogId + "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                    {
                                        _mSummaryGroupField =
                                            GetAttributeNameFromAttributeID(childNodes[i].InnerText.Remove(0, 5));
                                        _mSummaryGroupFieldSet.Add(childNodes[i].InnerText.Remove(0, 5));
                                    }
                                }
                            }

                            if (childNodes[i].Name == "TableGroupField")
                            {
                                _mTableGroupField = "";
                                if (childNodes[i].InnerText.Trim() != string.Empty &&
                                    childNodes[i].InnerText.Trim() != "[none]")
                                {
                                    string attrID = childNodes[i].InnerText.Remove(0, 5);
                                    _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                 "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        _mTableGroupField =
                                            GetAttributeNameFromAttributeID(childNodes[i].InnerText.Remove(0, 5));
                                    _mTableGroupFieldSet.Add(childNodes[i].InnerText.Remove(0, 5));
                                }
                            }

                            if (childNodes[i].Name == "PlaceHolderText")
                                _mPlaceHolderText = childNodes[i].InnerText;
                            if (_mPlaceHolderText != "")
                            {
                                if (_mPlaceHolderTextSet.Count == 0)
                                {
                                    _mPlaceHolderTextSet.Add(_mPlaceHolderText);
                                }
                            }
                            if (childNodes[i].Name == "DisplayRowHeader")
                            {
                                _mShowRowHeaders = Convert.ToBoolean(childNodes[i].InnerText);
                                if (_mPlaceHolderText != "")
                                {
                                    if (_mShowRowHeadersSet.Count == 0)
                                    {
                                        _mShowRowHeadersSet.Add(_mShowRowHeaders);
                                    }
                                }
                            }
                            if (childNodes[i].Name == "DisplayColumnHeader")
                            {
                                _mShowColumnHeaders = Convert.ToBoolean(childNodes[i].InnerText);
                                if (_mPlaceHolderText != "")
                                {
                                    if (_mShowColumnHeadersSet.Count == 0)
                                    {
                                        _mShowColumnHeadersSet.Add(_mShowColumnHeaders);
                                    }
                                }

                            }
                            if (childNodes[i].Name == "DisplaySummaryHeader")
                            {
                                _mShowSummaryHeaders = Convert.ToBoolean(childNodes[i].InnerText);
                                if (_mPlaceHolderText != "")
                                {
                                    if (_mShowSummaryHeadersSet.Count == 0)
                                    {
                                        _mShowSummaryHeadersSet.Add(_mShowSummaryHeaders);
                                    }
                                }
                            }
                            if (childNodes[i].Name == "PivotHeaderText")
                            {
                                _mPivotHeaderText = childNodes[i].InnerText;
                                if (_mPlaceHolderText != "")
                                {
                                    if (_mPivotHeaderTextSet.Count == 0)
                                    {
                                        _mPivotHeaderTextSet.Add(_mPivotHeaderText);
                                    }
                                }

                            }
                            if (childNodes[i].Name == "VerticalTable")
                            {
                                _mVerticalTable = Convert.ToBoolean(childNodes[i].InnerText);
                                if (_mPlaceHolderText != "")
                                {
                                    if (_mVerticalTableSet.Count == 0)
                                    {
                                        _mVerticalTableSet.Add(_mVerticalTable);
                                    }
                                }

                            }
                        }
                    }
                }
                feilds[0] = _mRowFields.ToArray();
                feilds[1] = _mLeftRowFields.ToArray();
                feilds[2] = _mRightRowFields.ToArray();
                feilds[3] = _mColumnFields.ToArray();
                feilds[4] = _mSummaryFields.ToArray();
                feilds[5] = _mSummaryGroupFieldSet.ToArray();
                feilds[6] = _mTableGroupFieldSet.ToArray();
                feilds[7] = _mPlaceHolderTextSet.ToArray();
                feilds[8] = _mShowRowHeadersSet.ToArray();
                feilds[9] = _mShowColumnHeadersSet.ToArray();
                feilds[10] = _mShowSummaryHeadersSet.ToArray();
                feilds[11] = _mPivotHeaderTextSet.ToArray();
                feilds[12] = _mVerticalTableSet.ToArray();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return Ok(feilds);
        }

        [HttpPost]
        public Tabledesignernodes DecodeLayoutXmls(string structureName, string mCatalogId,string familyId, string flag, int groupId,int packId, object model)
        {
            // var feilds = new Array[13];
            var objTabledesignernodes = new Tabledesignernodes();
            try
            {
                string xmlStr = Convert.ToString(model);

                #region Attribute Pack in Table Designer

                if (packId > 0)
                {
                    int catalogid = Convert.ToInt32(mCatalogId);
                    int familyid = Convert.ToInt32(familyId);
                    TB_FAMILY_TABLE_STRUCTURE fTS = new TB_FAMILY_TABLE_STRUCTURE();
                    fTS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(s => s.CATALOG_ID == catalogid && s.FAMILY_ID == familyid && s.STRUCTURE_NAME == structureName).FirstOrDefault();
                    TB_ATTRIBUTE_PACK_TABLE_STRUCTURE apTableStructure = new TB_ATTRIBUTE_PACK_TABLE_STRUCTURE();
                    if (fTS != null && fTS.ID > 0)
                    {
                        apTableStructure = _dbcontext.TB_ATTRIBUTE_PACK_TABLE_STRUCTURE.Where(s => s.PACK_ID == packId && s.FAMILY_STRUCTURE_ID == fTS.ID).FirstOrDefault();
                    }
                    if (apTableStructure != null && apTableStructure.ID > 0)
                    {
                        xmlStr = apTableStructure.TABLE_STRUCTURE;
                    }
                }

                #endregion

                _mLeftRowNodes = new List<XmlNode>();
                _mRightRowNodes = new List<XmlNode>();
                _mSummaryNodes = new List<XmlNode>();
                _mColumnNodes = new List<XmlNode>();
                _mPlaceHolderText = "";
                _mSummaryGroupField = "";
                _mTableGroupField = "";
                _mShowColumnHeaders = false;
                _mShowRowHeaders = false;
                _mShowSummaryHeaders = false;
                _mMergeRowHeaders = false;
                _mMergeSummaryFields = false;
                _leftRowNode = null;
                _rightRowNode = null;
                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "SummaryGroupField")
                            {
                                int attributeId = 0;
                                _mSummaryGroupField = "[none]";
                                if (childNodes[i].InnerText.Trim() != string.Empty && childNodes[i].InnerText.Trim() != "[none]" && !childNodes[i].InnerText.Contains("? string: ?"))
                                {

                                    string attrID = childNodes[i].InnerText.Remove(0, 5);
                                    _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                 " AND CATALOG_ID = " + mCatalogId + "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                    {
                                        _mSummaryGroupField =
                                            GetAttributeNameFromAttributeID(childNodes[i].InnerText.Remove(0, 5));
                                        //_mSummaryGroupFieldSet.Add(childNodes[i].InnerText.Remove(0, 5));
                                    }
                                    int.TryParse(attrID.Trim('~'), out attributeId);
                                }

                                objTabledesignernodes.SummaryGroupField = _mSummaryGroupField;
                                objTabledesignernodes.SummaryGroupFieldValue = attributeId;

                            }
                            if (childNodes[i].Name == "TableGroupField")
                            {
                                int attributeId = 0;
                                _mTableGroupField = "[none]";
                                if (childNodes[i].InnerText.Trim() != string.Empty &&
                                    childNodes[i].InnerText.Trim() != "[none]")
                                {
                                    string attrID = childNodes[i].InnerText.Remove(0, 5);
                                    _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                 "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        _mTableGroupField =
                                            GetAttributeNameFromAttributeID(childNodes[i].InnerText.Remove(0, 5));
                                    // _mTableGroupFieldSet.Add(childNodes[i].InnerText.Remove(0, 5));
                                    int.TryParse(attrID.Trim('~'), out attributeId);
                                }

                                objTabledesignernodes.TableGroupField = _mTableGroupField;
                                objTabledesignernodes.TableGroupFieldValue = attributeId;
                            }

                            if (childNodes[i].Name == "PlaceHolderText")
                            {
                                _mPlaceHolderText = childNodes[i].InnerText;
                                objTabledesignernodes.PlaceHolderText = _mPlaceHolderText;
                            }
                            //if (_mPlaceHolderText != "")
                            //{
                            //    if (_mPlaceHolderTextSet.Count == 0)
                            //    {
                            //       // _mPlaceHolderTextSet.Add(_mPlaceHolderText);
                            //    }
                            //}
                            if (childNodes[i].Name == "DisplayRowHeader")
                            {
                                _mShowRowHeaders = Convert.ToBoolean(childNodes[i].InnerText);
                                objTabledesignernodes.DisplayRowHeader = _mShowRowHeaders;
                                //if (_mPlaceHolderText != "")
                                //{
                                //    if (_mShowRowHeadersSet.Count == 0)
                                //    {
                                //        _mShowRowHeadersSet.Add(_mShowRowHeaders);
                                //    }
                                //}
                            }
                            if (childNodes[i].Name == "DisplayColumnHeader")
                            {
                                _mShowColumnHeaders = Convert.ToBoolean(childNodes[i].InnerText);
                                objTabledesignernodes.DisplayColumnHeader = _mShowColumnHeaders;
                                //if (_mPlaceHolderText != "")
                                //{
                                //    if (_mShowColumnHeadersSet.Count == 0)
                                //    {
                                //        _mShowColumnHeadersSet.Add(_mShowColumnHeaders);
                                //    }
                                //}

                            }
                            if (childNodes[i].Name == "DisplaySummaryHeader")
                            {
                                _mShowSummaryHeaders = Convert.ToBoolean(childNodes[i].InnerText);
                                objTabledesignernodes.DisplaySummaryHeader = _mShowSummaryHeaders;
                                //if (_mPlaceHolderText != "")
                                //{
                                //    if (_mShowSummaryHeadersSet.Count == 0)
                                //    {
                                //        _mShowSummaryHeadersSet.Add(_mShowSummaryHeaders);
                                //    }
                                //}
                            }
                            if (childNodes[i].Name == "PivotHeaderText")
                            {
                                _mPivotHeaderText = childNodes[i].InnerText;
                                objTabledesignernodes.PivotHeaderText = _mPivotHeaderText;
                                //if (_mPlaceHolderText != "")
                                //{
                                //    if (_mPivotHeaderTextSet.Count == 0)
                                //    {
                                //        _mPivotHeaderTextSet.Add(_mPivotHeaderText);
                                //    }
                                //}

                            }
                            if (childNodes[i].Name == "VerticalTable")
                            {
                                _mVerticalTable = Convert.ToBoolean(childNodes[i].InnerText);
                                objTabledesignernodes.VerticalTable = _mVerticalTable;
                                //if (_mPlaceHolderText != "")
                                //{
                                //    if (_mVerticalTableSet.Count == 0)
                                //    {
                                //        _mVerticalTableSet.Add(_mVerticalTable);
                                //    }
                                //}

                            }
                        }
                    }
                }
                //feilds[0] = _mRowFields.ToArray();
                //feilds[1] = _mLeftRowFields.ToArray();
                //feilds[2] = _mRightRowFields.ToArray();
                //feilds[3] = _mColumnFields.ToArray();
                //feilds[4] = _mSummaryFields.ToArray();
                //feilds[5] = _mSummaryGroupFieldSet.ToArray();
                //feilds[6] = _mTableGroupFieldSet.ToArray();
                //feilds[7] = _mPlaceHolderTextSet.ToArray();
                //feilds[8] = _mShowRowHeadersSet.ToArray();
                //feilds[9] = _mShowColumnHeadersSet.ToArray();
                //feilds[10] = _mShowSummaryHeadersSet.ToArray();
                //feilds[11] = _mPivotHeaderTextSet.ToArray();
                //feilds[12] = _mVerticalTableSet.ToArray();
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            //  return new JsonResult() { Data = objTabledesignernodes };
            return objTabledesignernodes;
        }


        [HttpGet]
        public List<NodeViewModel> CategoriesTD(int catalogId, string categoryId)
        {
            if (catalogId != 0)
            {
                return GetCategoriesForTDTree(catalogId, categoryId);
            }
            else
            {
                var objpProductViews = new List<NodeViewModel>();
                return objpProductViews;
            }
        }
        public List<NodeViewModel> GetCategoriesForTDTree(int catalogId, string id)
        {

            var treeitems = new List<NodeViewModel>();
            try
            {


                if (id == "undefined")
                {

                    var root = new NodeViewModel { id = "1", ATTRIBUTE_ID = 1, ATTRIBUTE_NAME = "Root", hasChildren = true };
                    treeitems.Add(root);
                }
                else
                {
                    var root = new NodeViewModel { id = "2", ATTRIBUTE_ID = 2, ATTRIBUTE_NAME = "One", hasChildren = false };
                    treeitems.Add(root);
                    var root1 = new NodeViewModel { id = "3", ATTRIBUTE_ID = 3, ATTRIBUTE_NAME = "Two", hasChildren = false };
                    treeitems.Add(root1);
                }





                return treeitems; ;

            }

            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : GetCategoriesForTree", objexception);
                return null;
            }

        }

        Dictionary<string, string> objGroupedSummaryAttrs = new Dictionary<string, string>();

        public int SaveTableLayout(string xmlTxt)
        {
            // this.DecodeLayoutXML(xmlTxt);
            string xmlString = xmlTxt;//ConstructLayoutXml();
            _mTableLayoutName = "test";
            string sqlStr = @"UPDATE TB_FAMILY_TABLE_STRUCTURE SET FAMILY_TABLE_STRUCTURE = '" + xmlString.Replace("'", "''") + "' WHERE FAMILY_ID = " + _mFamilyID + " AND CATALOG_ID=" + _mCatalogID + "";
            _SQLString = sqlStr;
            using (
                var objSqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                DataSet ds = new DataSet();

                objSqlConnection.Open();
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = _SQLString;
                objSqlCommand.CommandType = System.Data.CommandType.Text;
                objSqlCommand.Connection = objSqlConnection;
                //   SqlCommand dbCommand = new SqlCommand(_SQLString, objSqlConnection);
                return objSqlCommand.ExecuteNonQuery();
            }
            //return ExecuteSQLQuery();

        }
        public void SetPreview(string htmlPreviewString)
        {
            if (htmlPreviewString.Contains("&amp;lt;") && htmlPreviewString.Contains("&amp;gt;"))
            {
                htmlPreviewString = htmlPreviewString.Replace("&amp;lt;", "&lt;").Replace("&amp;gt;", "&gt;");
            }
            //if (htmlPreviewString.Contains("&lt;") && htmlPreviewString.Contains("&gt;"))
            //{
            //    htmlPreviewString = htmlPreviewString.Replace("&lt;", "<");
            //    htmlPreviewString = htmlPreviewString.Replace("&gt;", ">");
            //}
            if (htmlPreviewString.Contains("&nbsp;"))
            {
                htmlPreviewString = htmlPreviewString.Replace("&nbsp;", "\n");
            }
            string FamilyName = "Family Name";
            // Text = @"Table Preview - " + LayoutName;
            //var textFile = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/css/images/master.css"));
            // string fileContents = textFile.ReadToEnd();
            // textFile.Close();
            var fileContents = "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head>";
            htmlPreviewString = htmlPreviewString.Replace("<head />", fileContents);
            htmlPreviewString = htmlPreviewString.Replace("<h2 />", "<h2>" + FamilyName + "</h2>");
            var encOutput = System.Text.Encoding.UTF8;
            var filePath = HttpContext.Current.Server.MapPath("~/Views/App/Partials/SuperColumnPreview.html");
            // string filePath = Application.StartupPath + "\\SuperColumnPreview.html";
            var fileHtml = new FileStream(filePath, FileMode.Create);
            var strwriter = new StreamWriter(fileHtml, encOutput);
            strwriter.Write(htmlPreviewString);
            strwriter.Close();
            fileHtml.Close();
            //webBrowser1.Navigate(filePath);

        }
        public string RetrieveTableLayout()
        {
            string layoutXml = string.Empty;
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                DataSet ds = new DataSet();
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText =
                    "SELECT STRUCTURE_NAME,FAMILY_TABLE_STRUCTURE FROM TB_FAMILY_TABLE_STRUCTURE WHERE FAMILY_ID = " +
                    _mFamilyID + " AND CATALOG_ID=" + _mCatalogID + " AND IS_DEFAULT=1";
                objSqlCommand.CommandType = System.Data.CommandType.Text;
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                var da = new SqlDataAdapter(objSqlCommand);
                var fs = new DataSet();
                da.Fill(ds);
                //string sqlStr = "SELECT STRUCTURE_NAME,FAMILY_TABLE_STRUCTURE FROM TB_FAMILY_TABLE_STRUCTURE WHERE FAMILY_ID = " + _mFamilyID + " AND CATALOG_ID=" + _mCatalogID + " AND IS_DEFAULT=1";
                // SQLString = sqlStr;
                //  DataSet ds2 =  CreateDataSet();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    //layoutXml = ds.Tables[0].Rows[0]["FAMILY_TABLE_STRUCTURE"].ToString();
                    //Text = @"Table Designer - " + ds.Tables[0].Rows[0]["STRUCTURE_NAME"];
                    //_mTableLayoutName = ds.Tables[0].Rows[0]["STRUCTURE_NAME"].ToString();
                    //cmbStructureName.Text = ds.Tables[0].Rows[0]["STRUCTURE_NAME"].ToString();
                    layoutXml = ds.Tables[0].Rows[0]["FAMILY_TABLE_STRUCTURE"].ToString();
                }
            }
            return layoutXml;
            // return null;
        }
        public DataSet CatalogProductDetailsX(int catalogID, int familyID, int[] attributeList, bool mainFamily, string connvalue)
        {
            //var asdf = new Connection();
            //asdf.ConnSettings(connvalue);
            var asdf =
                new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            //  ProductCatalog(catalogID, familyID, true, true, attributeList, true, "");
            DataSet ds = GetProductCatalogX(mainFamily);
            ds.Tables.Add(ExtendedProperties(catalogID, familyID));
            ds.Tables[0].Columns.Remove("FAMILYID");
            ds.Tables[1].Columns.Remove("FAMILYID");
            return ds;
        }
        private DataTable ExtendedProperties(int catalogID, int familyID)
        {
            var et = new DataTable("ExtendedProperties");
            var iAttrList = new int[0];

            //for (int i = 0; i < iAttrList.Length - 1; i++)
            //{
            //    //iAttrList[i] = (int) grdSelect.Rows[i].Cells["ATTRIBUTE_ID"].Value;
            //}
            //var csPt = new CSDBProviderEX.ProductTable(catalogID, familyID, iAttrList, "");
            DataTable eet = GetProductTableExProperties(true);
            foreach (DataColumn dc in eet.Columns)
                et.Columns.Add(dc.Caption, dc.DataType);
            foreach (DataRow dr in eet.Rows)
                et.ImportRow(dr);
            return et;
        }
        public DataTable GetProductTableExProperties(bool mainFamily)
        {
            DataTable tblprod = new DataTable("tblprod");
            DataTable tblcpa = new DataTable("tblcpa");
            DataTable taprod = new DataTable("taprod");

            //var taprod = new CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESTableAdapter();
            //var tblprod = new CSDBProvider.CSDS.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESDataTable();
            //var tblcpa = new CSDBProvider.CSDS.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTDataTable();

            //taprod.FillByCatalogFamilyExProperties(tblprod, _mCatalogID, _mFamilyID);
            string cmdstr1 = "SELECT DISTINCT CAST(TB_ATTRIBUTE.ATTRIBUTE_ID AS nvarchar(MAX)) AS STRING_VALUE, - 3 AS PRODUCT_ID, TB_PARTS_KEY.ATTRIBUTE_ID as ATTRIBUTE_ID, TB_PARTS_KEY.ATTRIBUTE_ID AS NUMERIC_VALUE, null as OBJECT_TYPE, null as OBJECT_NAME, " +
                         " TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID FROM            TB_PROD_FAMILY INNER JOIN                          TB_PARTS_KEY ON TB_PROD_FAMILY.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN TB_ATTRIBUTE ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <> " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") " +
                         " UNION SELECT DISTINCT  CAST(TB_ATTRIBUTE.ATTRIBUTE_ID AS nvarchar(MAX)) AS STRING_VALUE, - 3 AS PRODUCT_ID, TB_PROD_SPECS.ATTRIBUTE_ID as ATTRIBUTE_ID,  TB_ATTRIBUTE.ATTRIBUTE_ID AS NUMERIC_VALUE,  null as OBJECT_TYPE, null as OBJECT_NAME,  TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID FROM            TB_PROD_FAMILY INNER JOIN                          TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN " +
                         " TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <> " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") UNION SELECT DISTINCT  CAST(TB_ATTRIBUTE.ATTRIBUTE_TYPE AS nvarchar(MAX)) AS STRING_VALUE, - 2 AS PRODUCT_ID, TB_PROD_SPECS.ATTRIBUTE_ID as ATTRIBUTE_ID,  TB_ATTRIBUTE.ATTRIBUTE_TYPE AS NUMERIC_VALUE,  null as OBJECT_TYPE, null as OBJECT_NAME,  TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID FROM            TB_PROD_FAMILY INNER JOIN " +
                         " TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID  WHERE        (-1 <> " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") UNION SELECT DISTINCT  TB_ATTRIBUTE.STYLE_NAME AS STRING_VALUE, - 1 AS PRODUCT_ID, TB_PROD_SPECS.ATTRIBUTE_ID  as ATTRIBUTE_ID,  null  AS NUMERIC_VALUE,  null as OBJECT_TYPE, null as OBJECT_NAME, TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID FROM            TB_PROD_FAMILY INNER JOIN " +
                         " TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <> " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") union SELECT DISTINCT  CAST(TB_ATTRIBUTE.ATTRIBUTE_TYPE AS nvarchar(MAX)) AS STRING_VALUE, - 2 AS PRODUCT_ID, TB_PARTS_KEY.ATTRIBUTE_ID as ATTRIBUTE_ID,  TB_ATTRIBUTE.ATTRIBUTE_TYPE AS NUMERIC_VALUE, null as OBJECT_TYPE, null as OBJECT_NAME,  TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID " +
                         " FROM            TB_PROD_FAMILY INNER JOIN  TB_PARTS_KEY ON TB_PROD_FAMILY.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN TB_ATTRIBUTE ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <> " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") UNION SELECT DISTINCT  TB_ATTRIBUTE.STYLE_NAME AS STRING_VALUE, - 1 AS PRODUCT_ID, TB_PARTS_KEY.ATTRIBUTE_ID  as ATTRIBUTE_ID,  null  AS NUMERIC_VALUE, null as OBJECT_TYPE, null as OBJECT_NAME, " +
                         " TB_PROD_FAMILY.FAMILY_ID, " + _mCatalogID + " AS CATALOG_ID FROM            TB_PROD_FAMILY INNER JOIN                          TB_PARTS_KEY ON TB_PROD_FAMILY.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN TB_ATTRIBUTE ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <>  " + _mCatalogID + ") AND (TB_PROD_FAMILY.FAMILY_ID = " + _mFamilyID + ") ORDER BY PRODUCT_ID, ATTRIBUTE_ID ";
            var Ocon1 = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            SqlCommand sqlcmd1 = new SqlCommand(cmdstr1, Ocon1);
            sqlcmd1.CommandTimeout = 0;
            SqlDataAdapter _DBAdapter1 = new SqlDataAdapter(sqlcmd1);
            _DBAdapter1.Fill(taprod);
            _DBAdapter1.Fill(tblprod);
            string cmdstr = "SELECT 0 AS ATTRIBUTE_ID, 'Sort' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 5 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT - 1 AS ATTRIBUTE_ID, 'Publish' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 4 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT -2 AS ATTRIBUTE_ID, 'Publish2Print' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, -3  AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT -3 AS ATTRIBUTE_ID, 'Publish2CD' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 2 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                         " UNION " +
                            " SELECT -4 AS ATTRIBUTE_ID, 'WorkFlowStat' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 1 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                         "UNION " +
                         " SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_PROD_FAMILY_ATTR_LIST.SORT_ORDER, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE " +
                          "FROM TB_PROD_FAMILY INNER JOIN " +
                        "TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN " +
                        "TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID INNER JOIN " +
                        "TB_PROD_FAMILY_ATTR_LIST ON TB_PROD_FAMILY.FAMILY_ID = TB_PROD_FAMILY_ATTR_LIST.FAMILY_ID AND " +
                        "TB_PROD_SPECS.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST.ATTRIBUTE_ID INNER JOIN " +
                        "TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND " +
                        "TB_CATALOG_ATTRIBUTES.CATALOG_ID =" + _mCatalogID + "  WHERE        (- 1 <> 2) AND (TB_PROD_FAMILY.FAMILY_ID =" + _mFamilyID + ") " +
                         "UNION " +
                       "SELECT DISTINCT " +
                        "TB_ATTRIBUTE_5.ATTRIBUTE_ID, TB_ATTRIBUTE_5.ATTRIBUTE_NAME, TB_ATTRIBUTE_5.ATTRIBUTE_TYPE,  " +
                        "TB_PROD_FAMILY_ATTR_LIST_3.SORT_ORDER, TB_ATTRIBUTE_5.ATTRIBUTE_DATATYPE " +
                          "FROM            TB_PROD_FAMILY AS TB_PROD_FAMILY_5 INNER JOIN " +
                        "TB_PARTS_KEY ON TB_PROD_FAMILY_5.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN " +
                        "TB_ATTRIBUTE AS TB_ATTRIBUTE_5 ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE_5.ATTRIBUTE_ID INNER JOIN TB_PROD_FAMILY_ATTR_LIST AS TB_PROD_FAMILY_ATTR_LIST_3 ON " +
                        "TB_PROD_FAMILY_5.FAMILY_ID = TB_PROD_FAMILY_ATTR_LIST_3.FAMILY_ID AND " +
                        "TB_PARTS_KEY.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST_3.ATTRIBUTE_ID INNER JOIN " +
                        "TB_CATALOG_ATTRIBUTES AS TB_CATALOG_ATTRIBUTES_5 ON TB_CATALOG_ATTRIBUTES_5.ATTRIBUTE_ID = TB_ATTRIBUTE_5.ATTRIBUTE_ID AND " +
                        "TB_CATALOG_ATTRIBUTES_5.CATALOG_ID =" + _mCatalogID + "" +
"  WHERE        (TB_PROD_FAMILY_5.FAMILY_ID =" + _mFamilyID + ") AND (- 1 <> 2) " +
"UNION " +
"select ATTRIBUTE_ID,ATTRIBUTE_NAME,ATTRIBUTE_TYPE,9999 as sort_order,ATTRIBUTE_DATATYPE from TB_ATTRIBUTE where " +
"ATTRIBUTE_ID in(select  CA.ATTRIBUTE_ID from TB_PROD_SPECS PS JOIN TB_CATALOG_ATTRIBUTES CA ON PS.ATTRIBUTE_ID=CA.ATTRIBUTE_ID where PRODUCT_ID in(select PRODUCT_ID from TB_PROD_FAMILY where FAMILY_ID=" + _mFamilyID + " ) AND CATALOG_ID=" + _mCatalogID + " UNION " +
                           "SELECT DISTINCT CA.ATTRIBUTE_ID " +
                           "FROM  TB_PARTS_KEY AS TB_PARTS_KEY_1 JOIN TB_CATALOG_ATTRIBUTES CA ON TB_PARTS_KEY_1.ATTRIBUTE_ID=CA.ATTRIBUTE_ID " +
                           "WHERE (FAMILY_ID =" + _mFamilyID + ") AND TB_PARTS_KEY_1.CATALOG_ID=" + _mCatalogID + " ) " +
                           "AND (ATTRIBUTE_ID NOT IN " +
                           "(SELECT DISTINCT ATTRIBUTE_ID " +
                           "FROM TB_PROD_FAMILY_ATTR_LIST AS TB_PROD_FAMILY_ATTR_LIST_1 " +
                           "WHERE (FAMILY_ID =" + _mFamilyID + "))) ORDER BY SORT_ORDER ";
            var Ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            SqlCommand sqlcmd = new SqlCommand(cmdstr, Ocon);
            sqlcmd.CommandTimeout = 0;
            SqlDataAdapter _DBAdapter = new SqlDataAdapter(sqlcmd);
            _DBAdapter.Fill(tblcpa);
            //tacpa.FillByCatalogFamily(tblcpa, _CatalogId, _FamilyId);   
            // tacpa.FillByCatalogFamily(tblcpa, _CatalogId, _FamilyId);

            DataTable dt = GetTransposedProductTable(tblprod, tblcpa);

            dt.Columns.Add("FAMILYID", System.Type.GetType("System.Int32"));

            return dt;
        }
        public DataSet GetProductCatalogX(bool mainFamily)
        {
            var dsPf = new DataSet { EnforceConstraints = false };
            if (_loadXProductTable)
            {
                //Load product table
                _AttributeIdList = new int[] { };

                DataTable dtProductTable = GetProductTable(mainFamily);
                dtProductTable.TableName = "ProductTable";
                dsPf.Tables.Add(dtProductTable);
            }
            return dsPf;
        }
        public DataTable GetProductTable(bool mainFamily)
        {
            DataTable dt = new DataTable("Product");
            DataTable dt2 = new DataTable("Product2");
            DataTable tblprod = new DataTable("Product2");
            DataTable tblcpa = new DataTable("Product2");
            string category_id = string.Empty;
            int catlogId = Convert.ToInt32(_mCatalogID);
            int familyId = Convert.ToInt32(_mFamilyID);
            // TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESTableAdapter taprod = new TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESTableAdapter();
            //  TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESDataTable tblprod = new TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESDataTable();


            //  TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTTableAdapter tacpa = new TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTTableAdapter();
            // TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTDataTable tblcpa = new TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTDataTable();
            var catedet = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(a => a.CATALOG_ID == catlogId && a.FAMILY_ID == familyId);
            if (catedet != null)
            {
                category_id = catedet.CATEGORY_ID;
            }
            string catfamquery = "EXEC STP_CATALOGSTUDIO5_FILLBYCATALOG_FAMILIY " + _mFamilyID + "," + _mCatalogID + "," + "," + category_id + "'";
            // var conn = new TradingBell.CatalogX.CSDBProvider.Connection();
            //var ocon = new SqlConnection(conn.GetConnSettings());
            var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            var ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            var comm = new SqlCommand(catfamquery, conn);
            comm.CommandTimeout = 0;
            var dbAdapter = new SqlDataAdapter(comm);
            dbAdapter.Fill(tblprod);
            //taprod.FillByCatalogFamily(tblprod, _FamilyId, _CatalogId);
            string cmdstr = "SELECT 0 AS ATTRIBUTE_ID, 'Sort' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 5 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT - 1 AS ATTRIBUTE_ID, 'Publish' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 4 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT -2 AS ATTRIBUTE_ID, 'Publish2Print' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, -3  AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT -3 AS ATTRIBUTE_ID, 'Publish2CD' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 2 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                            " SELECT -4 AS ATTRIBUTE_ID, 'WorkFlowStat' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, -1  AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                            " UNION " +
                         " SELECT DISTINCT TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_PROD_FAMILY_ATTR_LIST.SORT_ORDER, TB_ATTRIBUTE.ATTRIBUTE_DATATYPE " +
                          "FROM TB_PROD_FAMILY INNER JOIN " +
                        "TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN " +
                        "TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID INNER JOIN " +
                        "TB_PROD_FAMILY_ATTR_LIST ON TB_PROD_FAMILY.FAMILY_ID = TB_PROD_FAMILY_ATTR_LIST.FAMILY_ID AND " +
                        "TB_PROD_SPECS.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST.ATTRIBUTE_ID INNER JOIN " +
                        "TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND " +
                        "TB_CATALOG_ATTRIBUTES.CATALOG_ID =" + _mCatalogID + "  WHERE        (- 1 <> 2) AND (TB_PROD_FAMILY.FAMILY_ID =" + _mFamilyID + ") " +
                         "UNION " +
                       "SELECT DISTINCT " +
                        "TB_ATTRIBUTE_5.ATTRIBUTE_ID, TB_ATTRIBUTE_5.ATTRIBUTE_NAME, TB_ATTRIBUTE_5.ATTRIBUTE_TYPE,  " +
                        "TB_PROD_FAMILY_ATTR_LIST_3.SORT_ORDER, TB_ATTRIBUTE_5.ATTRIBUTE_DATATYPE " +
                          "FROM            TB_PROD_FAMILY AS TB_PROD_FAMILY_5 INNER JOIN " +
                        "TB_PARTS_KEY ON TB_PROD_FAMILY_5.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN " +
                        "TB_ATTRIBUTE AS TB_ATTRIBUTE_5 ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE_5.ATTRIBUTE_ID INNER JOIN TB_PROD_FAMILY_ATTR_LIST AS TB_PROD_FAMILY_ATTR_LIST_3 ON " +
                        "TB_PROD_FAMILY_5.PRODUCT_ID = TB_PROD_FAMILY_ATTR_LIST_3.PRODUCT_ID AND " +
                        "TB_PROD_FAMILY_5.FAMILY_ID = TB_PROD_FAMILY_ATTR_LIST_3.FAMILY_ID AND " +
                        "TB_PARTS_KEY.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST_3.ATTRIBUTE_ID INNER JOIN " +
                        "TB_CATALOG_ATTRIBUTES AS TB_CATALOG_ATTRIBUTES_5 ON TB_CATALOG_ATTRIBUTES_5.ATTRIBUTE_ID = TB_ATTRIBUTE_5.ATTRIBUTE_ID AND " +
                        "TB_CATALOG_ATTRIBUTES_5.CATALOG_ID =" + _mCatalogID + "" +
"  WHERE        (TB_PROD_FAMILY_5.FAMILY_ID =" + _mFamilyID + ") AND (- 1 <> 2) " +
"UNION " +
"select ATTRIBUTE_ID,ATTRIBUTE_NAME,ATTRIBUTE_TYPE,9999 as sort_order,ATTRIBUTE_DATATYPE from TB_ATTRIBUTE where " +
"ATTRIBUTE_ID in(select  CA.ATTRIBUTE_ID from TB_PROD_SPECS PS JOIN TB_CATALOG_ATTRIBUTES CA ON PS.ATTRIBUTE_ID=CA.ATTRIBUTE_ID where PRODUCT_ID in(select PRODUCT_ID from TB_PROD_FAMILY where FAMILY_ID=" + _mFamilyID + " ) AND CATALOG_ID=" + _mCatalogID + " UNION " +
                           "SELECT DISTINCT CA.ATTRIBUTE_ID " +
                           "FROM  TB_PARTS_KEY AS TB_PARTS_KEY_1 JOIN TB_CATALOG_ATTRIBUTES CA ON TB_PARTS_KEY_1.ATTRIBUTE_ID=CA.ATTRIBUTE_ID " +
                           "WHERE (FAMILY_ID =" + _mFamilyID + ") AND TB_PARTS_KEY_1.CATALOG_ID=" + _mCatalogID + " ) " +
                           "AND (ATTRIBUTE_ID NOT IN " +
                           "(SELECT DISTINCT ATTRIBUTE_ID " +
                           "FROM TB_PROD_FAMILY_ATTR_LIST AS TB_PROD_FAMILY_ATTR_LIST_1 " +
                           "WHERE (FAMILY_ID =" + _mFamilyID + "))) ORDER BY SORT_ORDER ";
            //CSDBProvider.Connection conn = new TradingBell.CatalogX.CSDBProvider.Connection();
            //SqlConnection Ocon = new SqlConnection(conn.getConnSettings());

            var sqlcmd = new SqlCommand(cmdstr, ocon);
            sqlcmd.CommandTimeout = 0;
            var dbAdapter1 = new SqlDataAdapter(sqlcmd);
            dbAdapter1.Fill(tblcpa);
            //tacpa.FillByCatalogFamily(tblcpa, _CatalogId, _FamilyId);  
            #region "changes for time format changing "

            #endregion
            dt = GetTransposedProductTable(tblprod, tblcpa);

            dt.Columns.Add("FAMILYID", typeof(int));

            foreach (DataRow dr in dt.Rows)
            {
                dr["FAMILYID"] = dr["FAMILY_ID"];
            }
            return dt;
        }
        private DataTable GetTransposedProductTable(DataTable productInfo, DataTable productAttributes)
        {
            try
            {
                var dtnew = new DataTable();
                dtnew.Columns.Add("CATALOG_ID", typeof(int));
                dtnew.Columns.Add("FAMILY_ID", typeof(int));
                dtnew.Columns.Add("PRODUCT_ID", typeof(int));
                if (PDFCatalog)
                    dtnew.Columns.Add("SORT_ORDER", typeof(int));

                var dcpks = new DataColumn[3];
                if (PDFCatalog)
                {
                    dcpks = new DataColumn[4];
                    dcpks[0] = dtnew.Columns["CATALOG_ID"];
                    dcpks[1] = dtnew.Columns["FAMILY_ID"];
                    dcpks[2] = dtnew.Columns["PRODUCT_ID"];
                    dcpks[3] = dtnew.Columns["SORT_ORDER"];
                }
                else
                {
                    dcpks[0] = dtnew.Columns["CATALOG_ID"];
                    dcpks[1] = dtnew.Columns["FAMILY_ID"];
                    dcpks[2] = dtnew.Columns["PRODUCT_ID"];
                }

                dtnew.Constraints.Add("PK", dcpks, true);
                foreach (DataRow dr in productAttributes.Rows)
                {

                    if (_AttributeIdList.Length > 0)
                    {
                        bool exists = CheckExists(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));
                        if (exists)
                        {
                            //_AttributeIdList[iCtr]
                            if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                            {
                                productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                            }
                            else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            {
                                productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString(), typeof(Double));
                            }
                            else
                            {
                                productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                            }


                            //enhance - Make sure to check if the column exists in this table

                            //OLD CODE------------
                            //DataRow[] drs = ProductInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"], "FAMILY_ID,SORT_ORDER");
                            //DataTable dtsel;
                            //dtsel = ProductInfo.Clone();
                            //for (int i = 0; i < drs.Length; i++)
                            //{
                            //    dtsel.ImportRow(drs[i]);
                            //}
                            //-------------------
                            DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"],
                                "FAMILY_ID,SORT_ORDER");
                            var dtsel = new DataTable();
                            if (drs.Any())
                            {
                                dtsel = drs.CopyToDataTable();
                            }

                            try
                            {
                                if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                    if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" &&
                                        dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                                    {
                                        bool stringValue =
                                            dtsel.Rows.Cast<DataRow>()
                                                .Any(dr1 => dr1["STRING_VALUE"].ToString().Length == 0);
                                        if (stringValue == false)
                                        {
                                            dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                            dtsel.Columns["STRING_VALUE"].ColumnName =
                                                dr["ATTRIBUTE_NAME"].ToString();
                                        }
                                    }
                            }
                            catch (Exception)
                            {
                            }
                            dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                            dtsel.Dispose();
                            if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                                dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                            {
                                productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                            }
                            else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                            else
                                productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                        }
                    }
                    else
                    {
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                            dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        {
                            productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                        }
                        else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                        else
                            productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();




                        //enhance - Make sure to check if the column exists in this table

                        dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                        DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"]);
                        DataTable dtsel;
                        dtsel = productInfo.Clone();
                        for (int i = 0; i < drs.Length; i++)
                        {
                            dtsel.ImportRow(drs[i]);
                            //    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                            //dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                            //    {
                            //        dtsel.Rows[i].ItemArray[7] = 1;// Convert.ToDecimal(dtsel.Rows[i].ItemArray[0]);
                            //    }
                        }
                        // int i = 0;
                        //foreach (DataRow dr2 in dtnew.Rows)
                        //{
                        //    foreach (DataRow dr1 in dtsel.Rows)
                        //    {
                        //        dr2[dr["ATTRIBUTE_NAME"].ToString()] = dr1["STRING_VALUE"].ToString();
                        //        break;
                        //    }
                        //}
                        try
                        {
                            if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" &&
                                    dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                                {
                                    bool stringValue = false;
                                    foreach (DataRow dr1 in dtsel.Rows)
                                    {
                                        if (dr1["STRING_VALUE"].ToString().Length == 0)
                                        {
                                            stringValue = true;
                                            break;
                                        }
                                    }
                                    if (stringValue == false)
                                    {
                                        dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                        dtsel.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                    }
                                }
                        }
                        catch (Exception)
                        {
                        }
                        dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                        dtsel.Dispose();
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                            dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        {
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                        }
                        else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                        else
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";

                    }
                }
                return dtnew;
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.Message);
                _groupTable = new DataTable();
                _sourceTable = new DataTable();
                _rowTable = new DataTable();
                _columnTable = new DataTable();
                _attributeTable = new DataTable();
                return null;
            }


        }
        public string ImagePath
        {
            private get
            {
                return _imagePath;
            }
            set
            {
                _imagePath = value;
            }
        }
        private bool CheckExists(int iattrID)
        {
            if (iattrID == 0)
            {
                //  return true;
            }
            return _AttributeIdList.Any(t => iattrID == t);
        }
        public string SQLString
        {
            get
            {
                return _SQLString;
            }
            set
            {
                _SQLString = value;
            }
        }
        public DataSet CreateDataSet()
        {
            DataSet dsReturn = new DataSet();
            using (
                var objSqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlDataAdapter _DBAdapter = new SqlDataAdapter(_SQLString, objSqlConnection);
                _DBAdapter.SelectCommand.CommandTimeout = 0;
                _DBAdapter.Fill(dsReturn);
                _DBAdapter.Dispose();
                return dsReturn;
            }
        }
        public bool Isnumber(string refValue)
        {
            if (refValue == "")
            {
                return false;
            }
            //const string strReg =
            //    @"^((\d?)|(([-+]?\d+\.?\d*)|([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d+\.?\d*))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d+\.?\d*)))$";
            var retval = false;
            //var res = new Regex(strReg);
            //if (res.IsMatch(refValue))
            //{
            //    retval = true;
            //}
            //return retval;
            const string strRegx = @"^[0-9]*(\.)?[0-9]+$";
            var re = new Regex(strRegx);
            if (re.IsMatch(refValue))
            {
                retval = true;
            }
            return retval; 
        }
        public string ApplicationStartupPath
        {
            get
            {
                return _applicationStartupPath;
            }
            set
            {
                _applicationStartupPath = value;
            }
        }
        public string TableLayoutName
        {
            private get
            {
                return _mTableLayoutName;
            }
            set
            {
                _mTableLayoutName = value;
            }
        }
        public string ConnectionString
        {
            get
            {
                return _connectionString;
            }
            set
            {
                _connectionString = value;
            }
        }
        private string GetAttributeNameFromAttributeID(string attrID)
        {

            string attrName = string.Empty;
            try
            {
                string sqlStr = "SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID  = " + attrID + "  ";
                _SQLString = sqlStr;
                DataSet ds = CreateDataSet();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    attrName = ds.Tables[0].Rows[0]["ATTRIBUTE_NAME"].ToString();
                }

            }

            catch (Exception ex)
            {
                Logger.Error("Error at TableDesignerApiController: DecodeLayoutXmlLeftTree", ex);
                //var mLeftRowv = new List<TreeViewData>(); ;
                //return mLeftRowv;
            }
            return attrName;
        }

        #region Attribute Pack

        public List<TreeViewData> GetXmlLeftTree(string mCatalogId, string familyId, string attrId, string flag, int groupId, string xmlStr)
        {
            try
            {
                string option = "GETUNPUBLISHEDATTRS";
                int familyid;
                int.TryParse(familyId, out familyid);
                int catalogid;
                int.TryParse(mCatalogId, out catalogid);
                var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                var mLeftRowvalues = new List<TreeViewData>();
                _mLeftRowNodes = new List<XmlNode>();
                _leftRowNode = null;


                //    string xmlStr = Convert.ToString(model);

                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();

                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "LeftRowField")
                            {
                                _leftRowNode = rootNode.Clone();
                                if (childNodes[i].Attributes != null)
                                {
                                    if (attrId != "undefined")
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {
                                            SuperColumnLeft(childNodes[i], mLeftRowvalues, objStplsAttributes, attrId,
                                                "LeftRowField", catalogid, familyid);
                                        }
                                    }
                                    else
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {
                                            mLeftRowvalues.Add(new TreeViewData
                                            {
                                                id = 0,
                                                ATTRIBUTE_ID = -1,
                                                ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                                COLOR = "Black",
                                                hasChildren = childNodes[i].ChildNodes.Count > 0
                                            });

                                        }
                                        else
                                        {
                                            int attributeid;
                                            string color = "Blue";
                                            string attributeId = string.Empty;
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 6);
                                                int.TryParse(attributeId, out attributeid);
                                            }
                                            else
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                                int.TryParse(attributeId, out attributeid);
                                            }

                                            var checkattr = objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                            if (checkattr.Any())
                                            {
                                                color = "Red";
                                            }
                                            if (color == "Red")
                                            {
                                                if (flag != "MultipleTable")
                                                {
                                                    mLeftRowvalues.Add(new TreeViewData
                                                    {
                                                        id = 1,
                                                        ATTRIBUTE_ID = attributeid,
                                                        Merge = childNodes[i].Attributes["Merge"].Value,
                                                        ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                        COLOR = color,
                                                        hasChildren = false
                                                    });
                                                }
                                            }
                                            else
                                            {
                                                var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                    _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                    tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                    .Where(x => x.tps.CATALOG_ID == catalogid
                                                                && x.tps.ATTRIBUTE_ID == attributeid &&
                                                                x.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                x.tpf.FAMILY_ID == familyid);
                                                if (catalogattrchk.Any())
                                                {
                                                    int Attribute_ID = Convert.ToInt32(attributeId);
                                                    var TestResult = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == Attribute_ID
                                                        && x.ATTRIBUTE_TYPE != 7
                                                        && x.ATTRIBUTE_TYPE != 9
                                                        && x.ATTRIBUTE_TYPE != 11
                                                        && x.ATTRIBUTE_TYPE != 12
                                                        && x.ATTRIBUTE_TYPE != 13
                                                        ).ToList();

                                                    if (TestResult.Count != 0)
                                                    {

                                                        mLeftRowvalues.Add(new TreeViewData
                                                        {
                                                            id = 1,
                                                            ATTRIBUTE_ID = attributeid,
                                                            Merge = childNodes[i].Attributes["Merge"].Value,
                                                            ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                            COLOR = color,
                                                            hasChildren = false
                                                        });
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                }

                return mLeftRowvalues;
            }
            catch (Exception ex)
            {
                var mLeftRowv = new List<TreeViewData>(); ;
                return mLeftRowv;
            }

        }

        public List<TreeViewData> GetXmlRightTree(string mCatalogId, string familyId, string attrId, string flag, int groupId, string xmlStr)
        {
            try
            {
                string option = "GETUNPUBLISHEDATTRS";
                int familyid;
                int.TryParse(familyId, out familyid);
                int catalogid;
                int.TryParse(mCatalogId, out catalogid);
                var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                var mLeftRowvalues = new List<TreeViewData>();
                _mRightRowNodes = new List<XmlNode>();
                _rightRowNode = null;

                //  string xmlStr = Convert.ToString(model);

                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();

                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "RightRowField")
                            {
                                _rightRowNode = rootNode.Clone();
                                if (childNodes[i].Attributes != null)
                                {
                                    if (attrId != "undefined")
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {
                                            SuperColumnRight(childNodes[i], mLeftRowvalues, objStplsAttributes, attrId, "RightRowField", catalogid, familyid);
                                        }
                                    }
                                    else
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {
                                            mLeftRowvalues.Add(new TreeViewData
                                            {
                                                id = 0,
                                                ATTRIBUTE_ID = -1,
                                                ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                                COLOR = "Black",
                                                hasChildren = childNodes[i].ChildNodes.Count > 0
                                            });

                                        }
                                        else
                                        {
                                            int attributeid;
                                            string color = "Blue";
                                            string attributeId = string.Empty;
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 6);
                                                int.TryParse(attributeId, out attributeid);
                                            }
                                            else
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                                int.TryParse(attributeId, out attributeid);
                                            }
                                            var checkattr = objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                            if (checkattr.Any())
                                            {
                                                color = "Red";
                                            }

                                            if (color == "Red")
                                            {
                                                if (flag != "MultipleTable")
                                                {
                                                    mLeftRowvalues.Add(new TreeViewData
                                                    {
                                                        id = 1,
                                                        ATTRIBUTE_ID = attributeid,
                                                        Merge = childNodes[i].Attributes["Merge"].Value,
                                                        ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                        COLOR = color,
                                                        hasChildren = false
                                                    });
                                                }
                                            }
                                            else
                                            {
                                                var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                    _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                    tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                    .Where(x => x.tps.CATALOG_ID == catalogid
                                                                && x.tps.ATTRIBUTE_ID == attributeid &&
                                                                x.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                x.tpf.FAMILY_ID == familyid);
                                                if (catalogattrchk.Any())
                                                {
                                                    mLeftRowvalues.Add(new TreeViewData
                                                    {
                                                        id = 1,
                                                        ATTRIBUTE_ID = attributeid,
                                                        Merge = childNodes[i].Attributes["Merge"].Value,
                                                        ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                        COLOR = color,
                                                        hasChildren = false
                                                    });
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

                return mLeftRowvalues;

            }
            catch (Exception ex)
            {
                var mLeftRowv = new List<TreeViewData>(); ;
                return mLeftRowv;
            }

        }

        public List<TreeViewData> GetXmlSummaryTree(string mCatalogId, string familyId, string attrId, string flag, int groupId, string xmlStr)
        {
            try
            {
                string option = "GETUNPUBLISHEDATTRS";
                int familyid;
                int.TryParse(familyId, out familyid);
                int catalogid;
                int.TryParse(mCatalogId, out catalogid);
                var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                var mLeftRowvalues = new List<TreeViewData>();
                _mSummaryNodes = new List<XmlNode>();

                //  string xmlStr = Convert.ToString(model);

                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "SummaryField")
                            {

                                if (childNodes[i].Attributes != null)
                                {
                                    if (attrId != "undefined")
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {
                                            SuperColumnSummary(childNodes[i], mLeftRowvalues, objStplsAttributes, attrId, "SummaryField", catalogid, familyid);
                                        }
                                    }
                                    else
                                    {
                                        if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                        {
                                            mLeftRowvalues.Add(new TreeViewData
                                            {
                                                id = 0,
                                                ATTRIBUTE_ID = -1,
                                                ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                                COLOR = "Black",
                                                hasChildren = childNodes[i].ChildNodes.Count > 0
                                            });

                                        }
                                        else
                                        {
                                            int attributeid;
                                            string color = "Blue";
                                            string attributeId = string.Empty;
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 6);
                                                int.TryParse(attributeId, out attributeid);
                                            }
                                            else
                                            {
                                                attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                                int.TryParse(attributeId, out attributeid);
                                            }
                                            var checkattr = objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                            if (checkattr.Any())
                                            {
                                                color = "Red";
                                            }

                                            if (color == "Red")
                                            {
                                                mLeftRowvalues.Add(new TreeViewData
                                                {
                                                    id = 1,
                                                    ATTRIBUTE_ID = attributeid,
                                                    Merge = childNodes[i].Attributes["Merge"].Value,
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                    COLOR = color,
                                                    hasChildren = false
                                                });
                                            }
                                            else
                                            {
                                                var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                     _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                     tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                     .Where(x => x.tps.CATALOG_ID == catalogid
                                                                 && x.tps.ATTRIBUTE_ID == attributeid &&
                                                                 x.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                 x.tpf.FAMILY_ID == familyid);
                                                if (catalogattrchk.Any())
                                                {
                                                    mLeftRowvalues.Add(new TreeViewData
                                                    {
                                                        id = 1,
                                                        ATTRIBUTE_ID = attributeid,
                                                        Merge = childNodes[i].Attributes["Merge"].Value,
                                                        ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                                        COLOR = color,
                                                        hasChildren = false
                                                    });
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return mLeftRowvalues;
            }
            catch (Exception ex)
            {
                var mLeftRowv = new List<TreeViewData>(); ;
                return mLeftRowv;
            }
        }

        public List<NodeViewModel> GetXmlColumnTree(string mCatalogId, string familyId, string attrId, string flag, int groupId, string xmlStr)
        {
            var Columntreeitems = new List<NodeViewModel>();
            try
            {
                string option = "GETUNPUBLISHEDATTRS";
                int familyid;
                int.TryParse(familyId, out familyid);
                int catalogid;
                int.TryParse(mCatalogId, out catalogid);
                var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                var _mColumnRowFields = new List<TreeViewData>();
                _mColumnNodes = new List<XmlNode>();

                // string xmlStr = Convert.ToString(model);

                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();

                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "ColumnField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        _mColumnRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });

                                        for (int j = 0; j < childNodes[i].ChildNodes.Count; j++)
                                        {
                                            if (childNodes[i].ChildNodes[j].Name == "ColumnField")
                                            {
                                                if (childNodes[i].ChildNodes[j].Attributes != null)
                                                {
                                                    if (childNodes[i].Attributes["AttrName"].Value == attrId)
                                                    {
                                                        int attributeid;
                                                        string color = "Blue";
                                                        string attributeId = string.Empty;
                                                        if (childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Contains("nAttr"))
                                                        {
                                                            attributeId = childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 6);
                                                            int.TryParse(attributeId, out attributeid);
                                                        }
                                                        else
                                                        {
                                                            attributeId = childNodes[i].ChildNodes[j].Attributes["AttrID"].Value.Remove(0, 5);
                                                            int.TryParse(attributeId, out attributeid);
                                                        }
                                                        var checkattr =
                                                            objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                                        if (checkattr.Any())
                                                        {
                                                            color = "Red";
                                                        }
                                                        if (color == "Red")
                                                        {
                                                            _mColumnRowFields.Add(new TreeViewData
                                                            {
                                                                id = 2,
                                                                ATTRIBUTE_ID = attributeid,
                                                                Merge = childNodes[i].ChildNodes[j].Attributes["Merge"].Value,
                                                                ATTRIBUTE_NAME =
                                                                    GetAttributeNameFromAttributeID(attributeId),
                                                                COLOR = color
                                                            });
                                                        }
                                                        else
                                                        {
                                                            var catalogattrchk = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(
                                                      _dbcontext.TB_PROD_FAMILY_ATTR_LIST, tps => tps.ATTRIBUTE_ID,
                                                      tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                                                      .Where(x => x.tps.CATALOG_ID == catalogid
                                                                  && x.tps.ATTRIBUTE_ID == attributeid &&
                                                                  x.tps.TB_ATTRIBUTE.PUBLISH2PRINT &&
                                                                  x.tpf.FAMILY_ID == familyid);
                                                            if (catalogattrchk.Any())
                                                            {
                                                                _mColumnRowFields.Add(new TreeViewData
                                                                {
                                                                    id = 2,
                                                                    ATTRIBUTE_ID = attributeid,
                                                                    Merge = childNodes[i].ChildNodes[j].Attributes["Merge"].Value,
                                                                    ATTRIBUTE_NAME =
                                                                        GetAttributeNameFromAttributeID(attributeId),
                                                                    COLOR = color
                                                                });
                                                            }
                                                        }

                                                    }
                                                }
                                            }

                                        }
                                    }
                                    else
                                    {
                                        int attributeid;
                                        string color = "Blue";
                                        string attributeId = string.Empty;
                                        if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                        {
                                            attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 6);
                                            int.TryParse(attributeId, out attributeid);
                                        }
                                        else
                                        {
                                            attributeId = childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                            int.TryParse(attributeId, out attributeid);
                                        }
                                        var checkattr = objStplsAttributes.Where(x => x.ATTRIBUTE_ID == attributeid);
                                        if (checkattr.Any())
                                        {
                                            color = "Red";
                                        }
                                        _mColumnRowFields.Add(new TreeViewData
                                        {
                                            id = 0,
                                            ATTRIBUTE_ID = attributeid,
                                            Merge = childNodes[i].Attributes["Merge"].Value,
                                            ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(attributeId),
                                            COLOR = color
                                        });
                                    }
                                }
                            }
                        }
                    }
                }

                if (_mColumnRowFields.Count > 0)
                {
                    if (attrId == "undefined")
                    {
                        foreach (var item in _mColumnRowFields)
                        {
                            if (item.id == 0)
                            {
                                var root = new NodeViewModel { id = "1", ATTRIBUTE_ID = item.ATTRIBUTE_ID, Merge = item.Merge, ATTRIBUTE_NAME = item.ATTRIBUTE_NAME, COLOR = item.COLOR, hasChildren = false };
                                Columntreeitems.Add(root);
                            }
                            else if (item.id == 1)
                            {
                                var root = new NodeViewModel { id = "0", ATTRIBUTE_ID = item.ATTRIBUTE_ID, Merge = item.Merge, ATTRIBUTE_NAME = item.ATTRIBUTE_NAME, COLOR = item.COLOR, hasChildren = true };
                                Columntreeitems.Add(root);
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in _mColumnRowFields)
                        {
                            if (item.id == 2)
                            {
                                var root = new NodeViewModel { id = "Super", ATTRIBUTE_ID = item.ATTRIBUTE_ID, Merge = item.Merge, ATTRIBUTE_NAME = item.ATTRIBUTE_NAME, COLOR = item.COLOR, hasChildren = false };
                                Columntreeitems.Add(root);
                            }
                        }
                    }
                }
                else
                {
                    Columntreeitems = new List<NodeViewModel>();
                    return Columntreeitems;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return Columntreeitems;
        }

        public Tabledesignernodes GetOtherDetails(string mCatalogId, string familyId, string flag, int groupId, string xmlStr)
        {
            var objTabledesignernodes = new Tabledesignernodes();
            try
            {
                _mLeftRowNodes = new List<XmlNode>();
                _mRightRowNodes = new List<XmlNode>();
                _mSummaryNodes = new List<XmlNode>();
                _mColumnNodes = new List<XmlNode>();
                _mPlaceHolderText = "";
                _mSummaryGroupField = "";
                _mTableGroupField = "";
                _mShowColumnHeaders = false;
                _mShowRowHeaders = false;
                _mShowSummaryHeaders = false;
                _mMergeRowHeaders = false;
                _mMergeSummaryFields = false;
                _leftRowNode = null;
                _rightRowNode = null;
                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "SummaryGroupField")
                            {
                                int attributeId = 0;
                                _mSummaryGroupField = "[none]";
                                if (childNodes[i].InnerText.Trim() != string.Empty && childNodes[i].InnerText.Trim() != "[none]" && !childNodes[i].InnerText.Contains("? string: ?"))
                                {

                                    string attrID = childNodes[i].InnerText.Remove(0, 5);
                                    _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                 " AND CATALOG_ID = " + mCatalogId + "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                    {
                                        _mSummaryGroupField =
                                            GetAttributeNameFromAttributeID(childNodes[i].InnerText.Remove(0, 5));
                                    }
                                    int.TryParse(attrID.Trim('~'), out attributeId);
                                }

                                objTabledesignernodes.SummaryGroupField = _mSummaryGroupField;
                                objTabledesignernodes.SummaryGroupFieldValue = attributeId;

                            }
                            if (childNodes[i].Name == "TableGroupField")
                            {
                                int attributeId = 0;
                                _mTableGroupField = "[none]";
                                if (childNodes[i].InnerText.Trim() != string.Empty &&
                                    childNodes[i].InnerText.Trim() != "[none]")
                                {
                                    string attrID = childNodes[i].InnerText.Remove(0, 5);
                                    _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                 "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        _mTableGroupField =
                                            GetAttributeNameFromAttributeID(childNodes[i].InnerText.Remove(0, 5));
                                    int.TryParse(attrID.Trim('~'), out attributeId);
                                }

                                objTabledesignernodes.TableGroupField = _mTableGroupField;
                                objTabledesignernodes.TableGroupFieldValue = attributeId;
                            }

                            if (childNodes[i].Name == "PlaceHolderText")
                            {
                                _mPlaceHolderText = childNodes[i].InnerText;
                                objTabledesignernodes.PlaceHolderText = _mPlaceHolderText;
                            }

                            if (childNodes[i].Name == "DisplayRowHeader")
                            {
                                _mShowRowHeaders = Convert.ToBoolean(childNodes[i].InnerText);
                                objTabledesignernodes.DisplayRowHeader = _mShowRowHeaders;

                            }
                            if (childNodes[i].Name == "DisplayColumnHeader")
                            {
                                _mShowColumnHeaders = Convert.ToBoolean(childNodes[i].InnerText);
                                objTabledesignernodes.DisplayColumnHeader = _mShowColumnHeaders;

                            }
                            if (childNodes[i].Name == "DisplaySummaryHeader")
                            {
                                _mShowSummaryHeaders = Convert.ToBoolean(childNodes[i].InnerText);
                                objTabledesignernodes.DisplaySummaryHeader = _mShowSummaryHeaders;

                            }
                            if (childNodes[i].Name == "PivotHeaderText")
                            {
                                _mPivotHeaderText = childNodes[i].InnerText;
                                objTabledesignernodes.PivotHeaderText = _mPivotHeaderText;

                            }
                            if (childNodes[i].Name == "VerticalTable")
                            {
                                _mVerticalTable = Convert.ToBoolean(childNodes[i].InnerText);
                                objTabledesignernodes.VerticalTable = _mVerticalTable;

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return objTabledesignernodes;
        }

        public List<STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES_Result> GettableGroupHeader(string catalogId, string familyId, string flag, int groupId, string xmlStr)
        {
            try
            {
                var _mLeftRowFields = new List<TreeViewData>();

                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "LeftRowField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });

                                        Allattrs(childNodes[i], _mLeftRowFields, "LeftRowField");
                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + catalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {

                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            else
                                            {

                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "RightRowField")
                            {
                                _rightRowNode = rootNode.Clone();
                                _rightRowNode.AppendChild(childNodes[i].CloneNode(true));
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });
                                        Allattrs(childNodes[i], _mLeftRowFields, "RightRowField");

                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + catalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {

                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                            else
                                            {
                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME =
                                                        GetAttributeNameFromAttributeID(
                                                            childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });
                                            }
                                        }
                                    }
                                }
                            }

                            if (childNodes[i].Name == "ColumnField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    string attrID =
                                        childNodes[i].Attributes["AttrID"].Value.Substring(
                                            childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                StringComparison.Ordinal) + 1,
                                            childNodes[i].Attributes["AttrID"].Value.Length -
                                            (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                StringComparison.Ordinal) + 1));
                                    _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " + attrID +
                                                 " AND CATALOG_ID = " + catalogId + "";
                                    DataSet ds = CreateDataSet();
                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                    {
                                        var attributeName = childNodes[i].Attributes["AttrID"].Value.Contains("nAttr")
                                            ? GetAttributeNameFromAttributeID(
                                                childNodes[i].Attributes["AttrID"].Value.Remove(0, 6))
                                            : GetAttributeNameFromAttributeID(
                                                childNodes[i].Attributes["AttrID"].Value.Remove(0, 5));
                                        var attributeID = childNodes[i].Attributes["AttrID"].Value.Contains("nAttr") ? childNodes[i].Attributes["AttrID"].Value.Remove(0, 6) : childNodes[i].Attributes["AttrID"].Value.Remove(0, 5);
                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            ATTRIBUTE_ID = Convert.ToInt32(attributeID),
                                            ATTRIBUTE_NAME = attributeName,
                                            COLOR = "BLUE"
                                        });
                                    }
                                }
                            }

                            if (childNodes[i].Name == "SummaryField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        _mLeftRowFields.Add(new TreeViewData
                                        {
                                            id = 1,
                                            ATTRIBUTE_ID = -1,
                                            ATTRIBUTE_NAME = childNodes[i].Attributes["AttrName"].Value,
                                            COLOR = "BLACK"
                                        });

                                        Allattrs(childNodes[i], _mLeftRowFields, "SummaryField");

                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + catalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            else
                                            {
                                                _mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                        }
                                    }
                                }
                            }


                        }
                    }
                }

                string option = "GETPUBLISHEDATTRS";
                if (flag == "MultipleTable")
                {
                    option = "MULTIPLEALL";
                    int familyid;
                    int.TryParse(familyId, out familyid);
                    int catalogid;
                    int.TryParse(catalogId, out catalogid);
                    var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, groupId, option).ToList();
                    foreach (var objleftrowfields in _mLeftRowFields)
                    {
                        var objall = objStplsAttributes.FirstOrDefault(x => x.ATTRIBUTE_ID == objleftrowfields.ATTRIBUTE_ID);
                        if (objall != null)
                        {
                            objStplsAttributes.Remove(objall);
                        }
                    }
                    objStplsAttributes.RemoveAll(x => x.COLOR == "RED");
                    return objStplsAttributes;
                }
                else
                {
                    int familyid;
                    int.TryParse(familyId, out familyid);
                    int catalogid;
                    int.TryParse(catalogId, out catalogid);
                    var objStplsAttributes = _dbcontext.STP_LS_TABLE_DESIGNER_GET_ALL_ATTRIBUTES(catalogid, familyid, 0, option).ToList();
                    foreach (var objleftrowfields in _mLeftRowFields)
                    {
                        var objall = objStplsAttributes.FirstOrDefault(x => x.ATTRIBUTE_ID == objleftrowfields.ATTRIBUTE_ID);
                        if (objall != null)
                        {
                            objStplsAttributes.Remove(objall);

                        }

                    }
                    objStplsAttributes.RemoveAll(x => x.COLOR == "RED");
                    return objStplsAttributes;

                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GettableGroupHeaderDataSource", ex);
                return null;
            }
        }

        public List<TreeViewData> GettableGroupSummary(string catalogId, string familyId, string flag, int groupId, string xmlStr)
        {
            try
            {
                var mLeftRowFields = new List<TreeViewData>();

                if (xmlStr.Trim() != string.Empty)
                {
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(xmlStr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    if (rootNode != null)
                    {
                        XmlNodeList childNodes = rootNode.ChildNodes;
                        for (int i = 0; i < childNodes.Count; i++)
                        {
                            if (childNodes[i].Name == "SummaryField")
                            {
                                if (childNodes[i].Attributes != null)
                                {
                                    if (childNodes[i].Attributes["AttrID"].Value == "Super")
                                    {
                                        AllSummaryattrs(childNodes[i], mLeftRowFields, "SummaryField");

                                    }
                                    else
                                    {
                                        string attrID =
                                            childNodes[i].Attributes["AttrID"].Value.Substring(
                                                childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1,
                                                childNodes[i].Attributes["AttrID"].Value.Length -
                                                (childNodes[i].Attributes["AttrID"].Value.IndexOf(":",
                                                    StringComparison.Ordinal) + 1));
                                        _SQLString = @"select * from TB_CATALOG_ATTRIBUTES where ATTRIBUTE_ID = " +
                                                     attrID +
                                                     " AND CATALOG_ID = " + catalogId + "";
                                        DataSet ds = CreateDataSet();
                                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        {
                                            if (childNodes[i].Attributes["AttrID"].Value.Contains("nAttr"))
                                            {
                                                mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 6)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                            else
                                            {
                                                mLeftRowFields.Add(new TreeViewData
                                                {
                                                    ATTRIBUTE_ID = Convert.ToInt32(childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    ATTRIBUTE_NAME = GetAttributeNameFromAttributeID(
                                                        childNodes[i].Attributes["AttrID"].Value.Remove(0, 5)),
                                                    COLOR = "BLUE"
                                                });

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return mLeftRowFields;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GettableGroupSummaryDataSource", ex);
                return null;
            }
        }

        #endregion

        #region Multiple selected table designer

        /// <summary>
        /// Added by Aswin kumar.M
        /// </summary>
        /// <param name="catalogId"></param>
        /// <param name="categoryId"></param>
        /// <param name="workingCatalogId"></param>
        /// <returns>It returns tree view data category, families and sub families</returns>

        [System.Web.Http.HttpGet]
        public List<ProductView> TabledesignerAllCategories(int catalogId, string categoryId, int workingCatalogId)
        {
            try
            {
                if (catalogId != 0)
                {
                    return GetCategoriesForTree(catalogId, categoryId, workingCatalogId);
                }
                else
                {
                    var objpProductViews = new List<ProductView>();
                    return objpProductViews;
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at TableDesignerApiController : TabledesignerAllCategories", objexception);
                return null;
            }
        }

        /// <summary>
        /// Added by Aswin kumar.M
        /// </summary>
        /// <param name="catalogId"></param>
        /// <param name="id"></param>
        /// <param name="workingCatalogId"></param>
        /// <returns>It returns the categories tree view</returns>
        public List<ProductView> GetCategoriesForTree(int catalogId, string id, int workingCatalogId)
        {
            try
            {
                using (var tnso = new TransactionScope(TransactionScopeOption.Required,
                new TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
                {
                    int customer_Id = 2;
                    if (id == "undefined")
                    {
                        if (catalogId != 1)
                        {
                            Stopwatch sw = Stopwatch.StartNew();
                            sw.Stop();

                            var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                                tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                                .Where(x => x.tpf.User_Name == User.Identity.Name)
                                .Select(x => x.tps);
                            var customerSettings = customersettings.FirstOrDefault();
                            bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                            var firstOrDefault = customersettings.FirstOrDefault();
                            bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                            customer_Id = Convert.ToInt32(customerSettings.CustomerId);
                            var aa =
                                _dbcontext.TB_CATALOG_SECTIONS.Where(
                                    a => a.CATALOG_ID == catalogId && _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.PARENT_CATEGORY).FirstOrDefault() == "0")
                                    .Select(
                                        a =>
                                            new ProductView
                                            {
                                                id = a.CATEGORY_ID,
                                                CATEGORY_ID = a.CATEGORY_ID,
                                                CATEGORY_SHORT = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.CATEGORY_SHORT).FirstOrDefault(),
                                                CUSTOMER_ID = _dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name).CustomerId,
                                                CATEGORY_NAME = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                                CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                                CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                                // DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                                // VERSION = a.TB_CATALOG.VERSION,
                                                hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) || _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A").Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                                SORT_ORDER = a.SORT_ORDER,
                                                CategoryIdinNavigator = categoryidinnavigator,
                                                FamilyandRelatedFamily = familyandrelatedfamily,
                                                spriteCssClass = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.IS_CLONE).FirstOrDefault() == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category"
                                            }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();

                            return aa;
                        }
                        else
                        {

                            var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                              tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                              .Where(x => x.tpf.User_Name == User.Identity.Name)
                              .Select(x => x.tps);
                            //string[] names;
                            //DataSet _dscatids = new DataSet();
                            //string _sqlCatLevel = "select c.category_id from TB_CATEGORY c join TB_CATALOG_SECTIONS cs on cs.CATEGORY_ID = c.CATEGORY_ID " +
                            //                      "and c.PARENT_CATEGORY = '0' join Customer_Catalog cc on cc.CATALOG_ID = cs.CATALOG_ID and cc.CustomerId = 11";
                            //_SQLString = _sqlCatLevel;
                            //_dscatids = CreateDataSet();
                            //names = new string[_dscatids.Tables[0].Rows.Count];
                            //for (int a = 0; a < _dscatids.Tables[0].Rows.Count; a++)
                            //{
                            //    names[a] = _dscatids.Tables[0].Rows[a]["category_id"].ToString();
                            //}


                            int userroleid = 0;
                            int userroletype = 0;
                            var SuperAdminCheck = _dbcontext.vw_aspnet_UsersInRoles
                            .Join(_dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                            .Join(_dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                            .Where(x => x.tcptps.tpf.UserName == User.Identity.Name).ToList();
                            if (SuperAdminCheck.Any())
                            {
                                userroleid = Convert.ToInt16(SuperAdminCheck[0].tcp.Role_id);
                                userroletype = Convert.ToInt16(SuperAdminCheck[0].tcp.RoleType);
                            }

                            var customerSettings = customersettings.FirstOrDefault();
                            bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                            var firstOrDefault = customersettings.FirstOrDefault();
                            bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                            customer_Id = Convert.ToInt32(customerSettings.CustomerId);
                            var aa = _dbcontext.TB_CATALOG_SECTIONS.Where(
                                 a => a.CATALOG_ID == catalogId && _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.PARENT_CATEGORY).FirstOrDefault() == "0")
                                 .Select(
                                     a =>
                                         new ProductView
                                         {
                                             id = a.CATEGORY_ID,
                                             CATEGORY_ID = a.CATEGORY_ID,
                                             CATEGORY_SHORT = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_SHORT).FirstOrDefault(),
                                             CUSTOMER_ID = customer_Id,
                                             CATEGORY_NAME = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                             CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                             CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                             //  DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                             // VERSION = a.TB_CATALOG.VERSION,
                                             hasChildren =
                                             _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) || _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                                 .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && c.FLAG_RECYCLE == "A" && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                             SORT_ORDER = a.SORT_ORDER,
                                             CategoryIdinNavigator = categoryidinnavigator,
                                             FamilyandRelatedFamily = familyandrelatedfamily,
                                             spriteCssClass = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.IS_CLONE).FirstOrDefault() == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category",
                                             @checked = _dbcontext.TB_CATALOG_SECTIONS.Any(x => x.CATALOG_ID == workingCatalogId && x.CATEGORY_ID == a.CATEGORY_ID)
                                         }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();




                            if (userroleid == 1)
                            {
                                var names = _dbcontext.TB_CATEGORY
                                  .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf })
                                  .Join(_dbcontext.Customer_Catalog, tcptps => tcptps.tpf.CATALOG_ID, tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                                  .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId, (tcptpscs, cs) => new { tcptpscs, cs })
                                  .Where(x => x.cs.User_Name == User.Identity.Name && x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" && x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" && x.tcptpscs.tcptps.tpf.CATALOG_ID != 1).Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);
                                var results = aa.Where(b => names.Contains(b.id)).ToList();
                                return results;
                            }
                            else
                            {
                                if (userroletype == 3)
                                {
                                    var names = _dbcontext.TB_CATEGORY
                                        .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID,
                                            (tps, tpf) => new { tps, tpf })
                                        .Join(_dbcontext.Customer_Role_Catalog, tcptps => tcptps.tpf.CATALOG_ID,
                                            tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                                        .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId,
                                            (tcptpscs, cs) => new { tcptpscs, cs })
                                        .Where(
                                            x =>
                                                x.cs.User_Name == User.Identity.Name &&
                                                x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" &&
                                                x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" &&
                                                x.tcptpscs.tcptps.tpf.CATALOG_ID != 1 && x.tcptpscs.tcp.RoleId == userroleid &&
                                                x.tcptpscs.tcp.IsActive == true)
                                        .Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);

                                    var results = aa.Where(b => names.Contains(b.id)).ToList();
                                    return results;
                                }
                                else
                                {
                                    var names = _dbcontext.TB_CATEGORY
                                       .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID,
                                           (tps, tpf) => new { tps, tpf })
                                       .Join(_dbcontext.Customer_Role_Catalog, tcptps => tcptps.tpf.CATALOG_ID,
                                           tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                                       .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId,
                                           (tcptpscs, cs) => new { tcptpscs, cs })
                                       .Where(
                                           x =>
                                               x.cs.User_Name == User.Identity.Name &&
                                               x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" &&
                                               x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" &&
                                               x.tcptpscs.tcptps.tpf.CATALOG_ID != 1 && x.tcptpscs.tcp.RoleId == userroleid)
                                       .Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);

                                    var results = aa.Where(b => names.Contains(b.id)).ToList();
                                    return results;
                                }
                            }
                        }
                    }
                    else
                    {
                        string subfamilycategoryid = string.Empty;
                        if (id.Contains("~"))
                        {
                            var catgoryandfamilyid = id.Split('~');
                            subfamilycategoryid = catgoryandfamilyid[0];
                            id = catgoryandfamilyid[1];
                        }
                        var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                              tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                              .Where(x => x.tpf.User_Name == User.Identity.Name)
                              .Select(x => x.tps);
                        var customerSettings = customersettings.FirstOrDefault();
                        bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                        var firstOrDefault = customersettings.FirstOrDefault();
                        bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                        customer_Id = Convert.ToInt32(customerSettings.CustomerId);
                        var aa = _dbcontext.TB_CATALOG_SECTIONS.Where(

                            a => a.CATALOG_ID == catalogId && _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.PARENT_CATEGORY).FirstOrDefault() == id)
                            .Select(
                                a =>
                                    new ProductView
                                    {
                                        id = a.CATEGORY_ID,
                                        CATEGORY_ID = a.CATEGORY_ID,
                                        CATEGORY_SHORT = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_SHORT).FirstOrDefault(),
                                        CUSTOMER_ID = customer_Id,
                                        CATEGORY_NAME = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                        CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                        CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                        //DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                        //VERSION = a.TB_CATALOG.VERSION,
                                        hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID && x.tps.FLAG_RECYCLE == "A") || _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                            .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1 && x.FLAG_RECYCLE == "A").Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                        SORT_ORDER = a.SORT_ORDER,
                                        CategoryIdinNavigator = categoryidinnavigator,
                                        FamilyandRelatedFamily = familyandrelatedfamily,
                                        spriteCssClass = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && a.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.IS_CLONE).FirstOrDefault() == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category",
                                        //@checked = catalogId == 1 && _dbcontext.TB_CATALOG_SECTIONS.Where(x => x.CATALOG_ID == workingCatalogId && x.CATEGORY_ID == id).Any()
                                        @checked = catalogId == 1 && _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == workingCatalogId && x.tps.CATEGORY_ID == a.CATEGORY_ID && x.tps.FLAG_RECYCLE == "A") ? true : false

                                    }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);


                        //var parentCategory = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == id && x.PARENT_CATEGORY!="0").Select(y => y.PARENT_CATEGORY).FirstOrDefault();
                        //if(parentCategory!="" && parentCategory!=null)
                        //{
                        //    id = parentCategory + "|" + id;
                        //}
                        var bb = Array.ConvertAll(
                                                 _dbcontext.TB_CATALOG_FAMILY.Where(
                                                     a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && a.CATEGORY_ID == id && a.TB_FAMILY.ROOT_FAMILY == 1 && a.TB_FAMILY.FLAG_RECYCLE == "A")
                                                     .Select(a
                                                         =>
                                                         new
                                                         {
                                                             a.FAMILY_ID,
                                                             a.TB_FAMILY.FAMILY_NAME,
                                                             a.TB_CATALOG.CATALOG_ID,
                                                             a.TB_CATALOG.CATALOG_NAME,
                                                             //   a.TB_CATALOG.DESCRIPTION,
                                                             //  a.TB_CATALOG.VERSION,
                                                             hasChildren = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Join(_dbcontext.TB_SUBFAMILY, tcftf => tcftf.tpf.FAMILY_ID, tsf => tsf.SUBFAMILY_ID, (tcftf, tsf) => new { tcftf, tsf }).Any(x => x.tcftf.tps.CATALOG_ID == catalogId && x.tcftf.tpf.FLAG_RECYCLE == "A" && x.tcftf.tpf.PARENT_FAMILY_ID == a.FAMILY_ID && x.tcftf.tps.CATEGORY_ID == id && x.tcftf.tps.FLAG_RECYCLE == "A"),
                                                             ProdCnt =
                                                                 a.TB_FAMILY.TB_PROD_FAMILY.Join(_dbcontext.TB_PRODUCT, tpf => tpf.PRODUCT_ID, tp => tp.PRODUCT_ID, (tpf, tp) => new { tpf, tp }).Where(b => b.tpf.FAMILY_ID == a.FAMILY_ID && b.tpf.FLAG_RECYCLE == "A" && b.tp.FLAG_RECYCLE == "A")
                                                                     .Select(b => b.tpf.PRODUCT_ID)
                                                                     .Distinct()
                                                                     .Count(),
                                                             SubProdCnt =
                                                                 a.TB_FAMILY.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, ts => ts.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (ts, tpf) => new { ts, tpf }).Where(b => b.ts.TB_FAMILY.FAMILY_ID == a.FAMILY_ID && b.tpf.CATALOG_ID == catalogId && b.tpf.FLAG_RECYCLE == "A")
                                                                 .Select(b => b.tpf.SUBPRODUCT_ID)
                                                                 .Distinct()
                                                                 .Count(),
                                                             a.SORT_ORDER
                                                         }).ToArray(),
                                                 a =>
                                                     new ProductView
                                                     {
                                                         id = id + "~" + a.FAMILY_ID.ToString(CultureInfo.InvariantCulture) + "~!" + _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == _dbcontext.TB_CATEGORY.Where(xx => xx.CATEGORY_ID == _dbcontext.TB_CATEGORY.Where(z => z.CATEGORY_ID == id && z.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && xx.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() + "~" + _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == _dbcontext.TB_CATEGORY.Where(z => z.CATEGORY_ID == id && z.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() + "~" + _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == id && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault(),
                                                         CATEGORY_ID = "~" + a.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                                         CATEGORY_NAME = familyandrelatedfamily ? a.SubProdCnt > 0 ? a.FAMILY_NAME /*+ " (" + a.ProdCnt.ToString(CultureInfo.InvariantCulture) + ")" */: a.FAMILY_NAME /*+ " (" + a.ProdCnt.ToString(CultureInfo.InvariantCulture) + ")"*/
                                                               : a.FAMILY_NAME,
                                                         CATALOG_ID = a.CATALOG_ID,
                                                         CATALOG_NAME = a.CATALOG_NAME,
                                                         // DESCRIPTION = a.DESCRIPTION,
                                                         //VERSION = a.VERSION,
                                                         hasChildren = a.hasChildren,
                                                         SORT_ORDER = a.SORT_ORDER,
                                                         CategoryIdinNavigator = false,
                                                         FamilyandRelatedFamily = familyandrelatedfamily,
                                                         spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.FAMILY_ID && x.tps.CATEGORY_ID == id && x.tps.FLAG_RECYCLE == "A") ?
                                                       "familyClone" : _dbcontext.TB_CATALOG.Join(_dbcontext.TB_CATALOG_FAMILY, tc => tc.CATALOG_ID, tcfa => tcfa.CATALOG_ID, (tc, tcfa) => new { tc, tcfa }).Any(x => x.tc.CATALOG_ID == catalogId && x.tcfa.CATEGORY_ID == id && x.tc.DEFAULT_FAMILY == a.FAMILY_ID) ? "defaultfamily" : "family",
                                                         //@checked = catalogId == 1 && _dbcontext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.FAMILY_ID).Any()
                                                         @checked = catalogId == 1 && _dbcontext.TB_FAMILY.Join(_dbcontext.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == workingCatalogId && x.tps.FLAG_RECYCLE == "A" && x.tpf.FLAG_RECYCLE == "A" && x.tps.FAMILY_ID == a.FAMILY_ID)


                                                     }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);

                        if (aa.Any())
                        {
                            var res = aa.ToList().Union(bb.ToList());
                            return res.ToList();
                        }
                        if (bb.Any())
                        {

                            return bb.ToList();
                        }
                        else
                        {
                            int fid;
                            int.TryParse(id.Trim('~'), out fid);
                            //int fid = Convert.ToInt32(id);
                            var cc = Array.ConvertAll(
                                (_dbcontext.TB_SUBFAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID,
                                    tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf })
                                    .Join(_dbcontext.TB_CATALOG_FAMILY, tcptps => tcptps.tps.SUBFAMILY_ID, tcp => tcp.FAMILY_ID,
                                        (tcptps, tcp) => new { tcptps, tcp })
                                    .Where(x => x.tcp.CATALOG_ID == catalogId && x.tcp.FLAG_RECYCLE == "A" && x.tcptps.tpf.FLAG_RECYCLE == "A" && x.tcptps.tps.FAMILY_ID == fid && x.tcp.CATEGORY_ID == subfamilycategoryid)).ToArray(),
                                a => new
                                {
                                    FAMILY_ID = a.tcptps.tps.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                    SUBFAMILY_ID = a.tcptps.tps.SUBFAMILY_ID,
                                    ProdCnt =
                                        _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_PRODUCT, tpf => tpf.PRODUCT_ID, tp => tp.PRODUCT_ID, (tpf, tp) => new { tpf, tp }).Where(b => b.tpf.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID && b.tpf.FLAG_RECYCLE == "A" && b.tp.FLAG_RECYCLE == "A").Select(x => x.tpf.PRODUCT_ID)
                                            .Distinct()
                                            .Count(),
                                    SubProdCnt = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, ts => ts.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (ts, tpf) => new { ts, tpf }).Where(b => b.ts.TB_FAMILY.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID && b.tpf.CATALOG_ID == catalogId && b.tpf.FLAG_RECYCLE == "A")
                                                 .Select(b => b.tpf.SUBPRODUCT_ID)
                                                 .Distinct()
                                                 .Count(),
                                    a.tcp.TB_CATALOG.CATALOG_ID,
                                    a.tcp.TB_CATALOG.CATALOG_NAME,
                                    a.tcp.TB_CATALOG.DESCRIPTION,
                                    a.tcp.TB_CATALOG.VERSION,
                                    a.tcptps.tps.SORT_ORDER
                                })
                                .Select(a => new ProductView
                                {
                                    id = subfamilycategoryid + "~" + a.SUBFAMILY_ID,
                                    CATEGORY_ID = "~" + a.SUBFAMILY_ID,
                                    CATEGORY_NAME = familyandrelatedfamily ? a.SubProdCnt > 0 ?
                                       _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID && b.FLAG_RECYCLE == "A").Select(b => b.FAMILY_NAME).FirstOrDefault() /*+ " (" + a.ProdCnt + ")"*/ : _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID && b.FLAG_RECYCLE == "A").Select(b => b.FAMILY_NAME).FirstOrDefault()/* + " (" + a.ProdCnt + ")"*/ : _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault(),
                                    CATALOG_ID = a.CATALOG_ID,
                                    CATALOG_NAME = a.CATALOG_NAME,
                                    // DESCRIPTION = a.DESCRIPTION,
                                    //  VERSION = a.VERSION,
                                    SORT_ORDER = a.SORT_ORDER,
                                    CategoryIdinNavigator = false,
                                    FamilyandRelatedFamily = familyandrelatedfamily,
                                    hasChildren = false,
                                    spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.SUBFAMILY_ID && x.tps.CATEGORY_ID == subfamilycategoryid && x.tpf.FLAG_RECYCLE == "A") ?
                                    "subfamilyClone" : _dbcontext.TB_CATALOG.Join(_dbcontext.TB_CATALOG_FAMILY, tc => tc.DEFAULT_FAMILY, tcf => tcf.FAMILY_ID, (tc, tcf) => new { tc, tcf }).Any(x => x.tc.DEFAULT_FAMILY == a.SUBFAMILY_ID && x.tcf.FLAG_RECYCLE == "A") ? "defaultsubfamily" : "subfamily",
                                    //@checked = catalogId == 1 && _dbcontext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == workingCatalogId && x.FAMILY_ID == a.SUBFAMILY_ID).Any()
                                    @checked = catalogId == 1 && _dbcontext.TB_FAMILY.Join(_dbcontext.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == workingCatalogId && x.tps.FLAG_RECYCLE == "A" && x.tpf.FLAG_RECYCLE == "A" && x.tps.FAMILY_ID == a.SUBFAMILY_ID) ? true : false
                                });
                            //       CATALOG_ID = a, CATALOG_NAME = a.CATALOG_NAME, DESCRIPTION = a.DESCRIPTION, VERSION = a.VERSION,  , hasChildren = false });
                            //var cc = Array.ConvertAll(_dbcontext.TB_SUBFAMILY.Where(a => a.FAMILY_ID == fid).ToArray(),
                            //    a => new { FAMILY_ID = a.FAMILY_ID.ToString(CultureInfo.InvariantCulture), SUBFAMILY_ID = a.SUBFAMILY_ID,
                            //        ProdCnt = _dbcontext.TB_PROD_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Distinct().Count()})
                            //    .Select(a => new ProductView { id = "~" + a.SUBFAMILY_ID, CATEGORY_NAME = _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault() + "(" + a.ProdCnt + ")",
                            //       CATALOG_ID = a, CATALOG_NAME = a.CATALOG_NAME, DESCRIPTION = a.DESCRIPTION, VERSION = a.VERSION,  , hasChildren = false });
                            return cc.OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id).ToList();
                        }
                    }
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at TableDesignerApiController : GetCategoriesForTree", objexception);
                return null;
            }

        }

        /// <summary>
        /// Added by Aswin kumar.M
        /// </summary>
        /// <param name="familyId"></param>
        /// <param name="structureName"></param>
        /// <param name="catalogId"></param>
        /// <param name="data"></param>
        /// <returns>It returns '1' if it is successful else it returns '0'</returns>

        [System.Web.Http.HttpPost]
        public string multipleTableDesignerSelectionFamilyIds(string familyId, string structureName, string catalogId, string IsdefaultStructurename,string IsUpdateMasterCatalog, JArray data)
        {
            string result = string.Empty;

            var categorylistFamilyId = data.ToList();
            List<string> categoryFamilyIdSplitList = new List<string>();
            List<string> removeCategoryList = new List<string>();
            List<string> tildefamilyId = new List<string>();


            foreach (var item in categorylistFamilyId)
            {
                string[] splitCategoryFamilyId = item.ToString().Split('!');
              
                categoryFamilyIdSplitList.Add(splitCategoryFamilyId[0]);

                if (item.ToString().Contains("~"))
                {
                    tildefamilyId.Add(item.ToString());
                }
            }



            string[] getFamilyId = new string[] { };
            List<string> listFamilyId = new List<string>();
            int getcurrentCatalogId = Convert.ToInt32(catalogId);

            //foreach (var items in list)
            //{
            //    if (items.Contains("~"))
            //    {
            //        string[] splitCategoryId = items.ToString().Split('~');
            //        if(!removeList.Contains(splitCategoryId[0]))
            //        {
            //            removeList.Add(splitCategoryId[0]);
            //        }

            //    }
            //}

            //foreach(var removeCategoryId in removeList)
            //{
            //    foreach(var categoryId in list.ToList())
            //    {
            //        if (removeCategoryId== categoryId)
            //        {
            //            list.Remove(categoryId);
            //        }
            //    }
            //}




            foreach (var functionAlloweditem in categoryFamilyIdSplitList)
            {
                string functionAllowed = Convert.ToString(functionAlloweditem);
                if (!(functionAllowed.Contains('~')) && (!tildefamilyId.Any(str => str.Contains(functionAlloweditem))) && (!removeCategoryList.Any(str => str.Contains(functionAlloweditem))))
                {
                    List<string> categoryIds = new List<string>();
                    string category_Id = Convert.ToString(functionAllowed);
                    TB_CATEGORY tb_category = new TB_CATEGORY();

                    var tb_categoryList = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category_Id).ToList();
                    categoryIds.Add(category_Id);
                    foreach (var category in tb_categoryList)
                    {
                        categoryIds.Add(category.CATEGORY_ID);
                        var tb_categoryLists = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category.CATEGORY_ID).ToList();
                        foreach (var categorys in tb_categoryLists)
                        {
                            categoryIds.Add(categorys.CATEGORY_ID);
                            string category_ID = categorys.CATEGORY_ID;

                            var tb_category_list = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category_ID).ToList();

                            foreach (var categories in tb_category_list)
                            {
                                categoryIds.Add(categories.CATEGORY_ID);
                            }

                            do
                            {
                                tb_category = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category_ID).FirstOrDefault();
                                if (tb_category != null)
                                {
                                    category_ID = tb_category.CATEGORY_ID;
                                    categoryIds.Add(tb_category.CATEGORY_ID);
                                }
                            } while (tb_category != null);

                        }
                    }




                    var catalogFamilies = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                           join family in _dbcontext.TB_FAMILY on catalogFamily.FAMILY_ID equals family.FAMILY_ID
                                           where catalogFamily.CATALOG_ID == getcurrentCatalogId && family.FLAG_RECYCLE == "A" && catalogFamily.FLAG_RECYCLE == "A" && categoryIds.Contains(catalogFamily.CATEGORY_ID)
                                           select new
                                           {
                                               catalogFamily.FAMILY_ID,
                                               catalogFamily.CATALOG_ID,
                                               catalogFamily.CATEGORY_ID,
                                               catalogFamily.FLAG_RECYCLE,
                                               catalogFamily.SORT_ORDER
                                           }).Distinct().ToList();

                    catalogFamilies = catalogFamilies.OrderBy(s => s.SORT_ORDER).ToList();



                    foreach (var categoryId in categoryIds)
                    {
                        removeCategoryList.Add(categoryId);
                    }

                    foreach (var familyIds in catalogFamilies)
                    {
                        listFamilyId.Add(familyIds.FAMILY_ID.ToString());
                    }
                }


                else if (functionAllowed.Contains("~"))
                {
                    int getCurrentFamilyId = 0;

                    getFamilyId = functionAllowed.Split('~');

                    getCurrentFamilyId = Convert.ToInt32(getFamilyId[1]);

                    listFamilyId.Add(getCurrentFamilyId.ToString());

                    var catalogSubFamilies = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                              join family in _dbcontext.TB_FAMILY on catalogFamily.FAMILY_ID equals family.FAMILY_ID
                                              where catalogFamily.CATALOG_ID == getcurrentCatalogId && family.FLAG_RECYCLE == "A" && catalogFamily.FLAG_RECYCLE == "A" && family.PARENT_FAMILY_ID == getCurrentFamilyId
                                              select new
                                              {
                                                  catalogFamily.FAMILY_ID,
                                                  catalogFamily.CATALOG_ID,
                                                  catalogFamily.CATEGORY_ID,
                                                  catalogFamily.FLAG_RECYCLE,
                                                  catalogFamily.SORT_ORDER
                                              }).Distinct().ToList();

                    catalogSubFamilies = catalogSubFamilies.OrderBy(s => s.SORT_ORDER).ToList();

                    foreach (var familyIds in catalogSubFamilies)
                    {
                        listFamilyId.Add(familyIds.FAMILY_ID.ToString());
                    }
                }

            }




            if (listFamilyId.Count > 0)
            {

                int getcurrentFamilyId = Convert.ToInt32(familyId);

                string structureNameIsExists = string.Empty;
                var currentFamilyTableStructure = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(x => x.FAMILY_ID == getcurrentFamilyId && x.STRUCTURE_NAME == structureName && x.CATALOG_ID == getcurrentCatalogId).Select(v => v.FAMILY_TABLE_STRUCTURE).FirstOrDefault();


                //for (int i = 0; i < categorylistFamilyId.Count; i++)
                //{
                //    replaceCategoryFamilyId = categorylistFamilyId[i].ToString().Replace("~!~~!", "");
                //    replaceCategoryFamilyId = categorylistFamilyId[i].ToString().Replace("!", "");
                //    //categorylistFamilyId[i] = replaceCategoryFamilyId;

                //    if (replaceCategoryFamilyId.Contains("~"))
                //    {
                //        getFamilyId = replaceCategoryFamilyId.Split('~');
                //        listFamilyId.Add(getFamilyId[1]);
                //    }
                //}

                foreach (var getEachFamilyId in listFamilyId)
                {
                    DataSet checkStructureNameDataset = new DataSet();
                    try
                    {
                        using (var objMultipleTabledesignerSqlConnection = new SqlConnection(sqlConn))
                        {
                            objMultipleTabledesignerSqlConnection.Open();
                            SqlCommand objMultipleTabledesignerSqlCommand = objMultipleTabledesignerSqlConnection.CreateCommand();
                            objMultipleTabledesignerSqlCommand.CommandText = "STP_LS_TABLE_DESIGNER_MULTIPLE_FAMILIES";
                            objMultipleTabledesignerSqlCommand.CommandType = CommandType.StoredProcedure;
                            objMultipleTabledesignerSqlCommand.CommandTimeout = 0;
                            objMultipleTabledesignerSqlCommand.Connection = objMultipleTabledesignerSqlCommand.Connection;
                            objMultipleTabledesignerSqlCommand.Parameters.Add("@ACTION", SqlDbType.VarChar).Value = "CheckTabledesignerStructurename";
                            objMultipleTabledesignerSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = catalogId ?? "";
                            objMultipleTabledesignerSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar).Value = structureName ?? "";
                            objMultipleTabledesignerSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar).Value = getEachFamilyId ?? "";
                            var objMultipleTabledesignerSqlAdapter = new SqlDataAdapter(objMultipleTabledesignerSqlCommand);
                            objMultipleTabledesignerSqlAdapter.Fill(checkStructureNameDataset);
                            objMultipleTabledesignerSqlConnection.Close();
                        }

                        if (checkStructureNameDataset.Tables[0].Rows.Count > 0)
                        {
                            structureNameIsExists = checkStructureNameDataset.Tables[0].Rows[0][0].ToString();

                            using (var objMultipleTabledesignerSqlConnection = new SqlConnection(sqlConn))
                            {
                                objMultipleTabledesignerSqlConnection.Open();
                                SqlCommand objMultipleTabledesignerSqlCommand = objMultipleTabledesignerSqlConnection.CreateCommand();
                                objMultipleTabledesignerSqlCommand.CommandText = "STP_LS_TABLE_DESIGNER_MULTIPLE_FAMILIES";
                                objMultipleTabledesignerSqlCommand.CommandType = CommandType.StoredProcedure;
                                objMultipleTabledesignerSqlCommand.CommandTimeout = 0;
                                objMultipleTabledesignerSqlCommand.Connection = objMultipleTabledesignerSqlCommand.Connection;
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@ACTION", SqlDbType.VarChar).Value = "UpdateTabledesignerStructurename";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = catalogId ?? "";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar).Value = structureName ?? "";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar).Value = getEachFamilyId ?? "";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.NVarChar).Value = currentFamilyTableStructure ?? "";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar).Value = IsdefaultStructurename ?? "0";
                                objMultipleTabledesignerSqlCommand.ExecuteNonQuery();
                                objMultipleTabledesignerSqlConnection.Close();
                            }
                        }
                        else
                        {
                            using (var objMultipleTabledesignerSqlConnection = new SqlConnection(sqlConn))
                            {
                                objMultipleTabledesignerSqlConnection.Open();
                                SqlCommand objMultipleTabledesignerSqlCommand = objMultipleTabledesignerSqlConnection.CreateCommand();
                                objMultipleTabledesignerSqlCommand.CommandText = "STP_LS_TABLE_DESIGNER_MULTIPLE_FAMILIES";
                                objMultipleTabledesignerSqlCommand.CommandType = CommandType.StoredProcedure;
                                objMultipleTabledesignerSqlCommand.CommandTimeout = 0;
                                objMultipleTabledesignerSqlCommand.Connection = objMultipleTabledesignerSqlCommand.Connection;
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@ACTION", SqlDbType.VarChar).Value = "InsertTabledesignerStructurename";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = catalogId ?? "";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar).Value = structureName ?? "";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar).Value = getEachFamilyId ?? "";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.NVarChar).Value = currentFamilyTableStructure ?? "";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar).Value = IsdefaultStructurename ?? "0";
                                objMultipleTabledesignerSqlCommand.ExecuteNonQuery();
                                objMultipleTabledesignerSqlConnection.Close();
                            }
                        }


                        result = "1";
                    }

                    catch (Exception ex)
                    {
                        Logger.Error("Error at TableDesignerApiController : multipleTableDesignerSelectionFamilyIds", ex);
                        return "0";
                    }
                }
            }

            if (IsUpdateMasterCatalog == "true")
            {


                var tableDesignerDataset = new DataSet();
                Guid objGuid = Guid.NewGuid();
                string sessionid = Regex.Replace(objGuid.ToString(), "[^a-zA-Z0-9_]+", "");
                string selectedCategory = string.Empty;
                string selectedFamily = string.Empty;
                List<string> categorySplitList = new List<string>();

                //foreach (var item in categorylistFamilyId)
                //{
                //    string[] splitCategoryFamilyId = item.ToString().Split('!');

                //    for (int k = 0; k < splitCategoryFamilyId.Count(); k++)
                //    {
                //        if (splitCategoryFamilyId[k].Contains('~'))
                //        {
                //            string splitCategoryId = splitCategoryFamilyId[k].Split('~')[0];

                //            categorySplitList.Add(splitCategoryId);
                //        }
                //        else
                //        {
                //            categorySplitList.Add(splitCategoryFamilyId[k]);
                //        }
                //    }

                //}


                for (int i = 0; i <= categoryFamilyIdSplitList.Count - 1; i++)
                {
                    var categoryId = categoryFamilyIdSplitList[i].ToString().Split('~');


                    if (!(selectedCategory.Contains(categoryId[0].ToString())))
                    {
                        if (categoryFamilyIdSplitList.Count - 1 == i)
                        {
                            selectedCategory = selectedCategory + categoryId[0];
                        }
                        else
                        {
                            selectedCategory = selectedCategory + categoryId[0] + ",";
                        }
                    }


                }



                selectedCategory = string.Join(",", selectedCategory.Split(',').Select(x => string.Format("'{0}'", x)));

                var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                var customerSetting_MasterCatalogId = _dbcontext.Customer_Settings.Where(x => x.CustomerId == customerid.CustomerId).Select(y => y.Customer_MasterCatalog).ToList();

                int masterCatalogId = Convert.ToInt32(customerSetting_MasterCatalogId[0]);
             
                using (var tableDesignerExportSqlConnection = new SqlConnection(sqlConn))
                {
                    tableDesignerExportSqlConnection.Open();
                    SqlCommand tableDesignerExportSqlCommand = tableDesignerExportSqlConnection.CreateCommand();
                    tableDesignerExportSqlCommand.CommandText = "STP_CATALOGSTUDIO5_Table_Designer_Multiple_Families";
                    tableDesignerExportSqlCommand.CommandType = CommandType.StoredProcedure;
                    tableDesignerExportSqlCommand.CommandTimeout = 0;
                    tableDesignerExportSqlCommand.Connection = tableDesignerExportSqlConnection;
                    tableDesignerExportSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 10).Value = catalogId;
                    tableDesignerExportSqlCommand.Parameters.Add("@SESSID", SqlDbType.NVarChar, 500).Value = sessionid;
                    tableDesignerExportSqlCommand.Parameters.Add("@ATTRIBUTE_IDS", SqlDbType.NVarChar).Value = "";
                    tableDesignerExportSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = selectedCategory ?? "All";

                    var objSqlDataAdapter1 = new SqlDataAdapter(tableDesignerExportSqlCommand);
                    objSqlDataAdapter1.Fill(tableDesignerDataset);

                }

                DataTable dtTableDesigner = new DataTable();

                if (tableDesignerDataset.Tables[0].Rows.Count > 0)
                {
                    dtTableDesigner = (DataTable)tableDesignerDataset.Tables[0];
                }


                if (dtTableDesigner.Rows.Count > 0)
                {
                    dtTableDesigner.Columns.Remove("FAMILY_ID_1");
                    dtTableDesigner.Columns.Remove("ff");
                }


                DataTable filteredTableForSelectedSubCatalog = new DataTable();

                //filteredTableForSelectedSubCatalog = (from n in dtTableDesigner.AsEnumerable()
                //                                      where n.Field<Int32>("FAMILY_ID").Equals(85)
                //                                      select n).CopyToDataTable();

                //filteredTableForSelectedSubCatalog = filteredTableForSelectedSubCatalog.AsEnumerable()
                //.Where(row => listFamilyId.Contains(row.Field<string>("FAMILY_ID");


                //var rows = (from n in dtTableDesigner.AsEnumerable()
                //            where n.Field<string>("FAMILY_ID").Contains("85")
                //            select n);

                //if (rows.Any())
                //    filteredTableForSelectedSubCatalog = rows.CopyToDataTable();

                DataView dv_FilterFamilyIds = new DataView(dtTableDesigner);

                DataView dv_FilterSubFamilyIds = new DataView(dtTableDesigner);

             
                //for(int i=0;i< listFamilyId.Count;i++)
                //{
                //    int family_IDs = Convert.ToInt32( listFamilyId[i]);

                //    dv_FilterFamilyIds.RowFilter = "FAMILY_ID IN("+family_IDs+")";
                //    dv_FilterFamilyIds.RowFilter = "SUBFAMILY_ID IN(" + family_IDs + ")";

                //}

                foreach (var _familyId in listFamilyId)
                {
                    // int family_IDs = Convert.ToInt32(listFamilyId[i]);

                 
                        if (selectedFamily==string.Empty)
                        {
                            selectedFamily = selectedFamily + _familyId + ",";
                        }
                        else if (!(_familyId.Contains(selectedFamily)))
                    
                        {
                        
                            selectedFamily = selectedFamily + _familyId + ",";
                        }
                    

                }


                dv_FilterFamilyIds.RowFilter = string.Format("FAMILY_ID IN({0})", selectedFamily);
                dv_FilterSubFamilyIds.RowFilter = string.Format("SUBFAMILY_ID IN({0})", selectedFamily);

                DataTable datatableMerge = dv_FilterFamilyIds.ToTable();
                datatableMerge.Merge(dv_FilterSubFamilyIds.ToTable());

                List<string> columnToRemoveFromTableDesigner = new List<string>();

                //System.Type type = dtTableDesigner.Rows[0][4].GetType();
              
                //filteredTableForSelectedSubCatalog = dtTableDesigner.AsEnumerable()
                //.Where(i => i.Field<int>("FAMILY_ID").Equals(listFamilyId)).CopyToDataTable();


                foreach (DataColumn column in dtTableDesigner.Columns)
                {

                    if (column.ColumnName.Contains("ID"))
                    {
                        columnToRemoveFromTableDesigner.Add(column.ColumnName);
                    }
                }

                foreach (var columns in columnToRemoveFromTableDesigner)
                {
                    string columnName = columns;

                    dtTableDesigner.Rows.OfType<DataRow>().ToList().ForEach(r =>
                    {
                        r[columnName] = DBNull.Value;

                    });

                }


                string catalogName = _dbcontext.TB_CATALOG.Where(s => s.CATALOG_ID == masterCatalogId && s.FLAG_RECYCLE == "A").Select(a => a.CATALOG_NAME).FirstOrDefault().ToString();

                dtTableDesigner.Rows.OfType<DataRow>().ToList().ForEach(r =>
                {
                    r["CATALOG_ID"] = masterCatalogId;

                    r["CATALOG_NAME"] = catalogName;
                });

                DataTable dt_Filter_FamilyDatatable = new DataTable();


                dt_Filter_FamilyDatatable = dtTableDesigner.Copy();
                dt_Filter_FamilyDatatable = dtTableDesigner.AsEnumerable()
                                 .GroupBy(r => r.Field<string>("FAMILY_NAME")).Select(g=>g.First())
                                 .CopyToDataTable();



                DataTable dt_Filter_SubFamilyDatatable = new DataTable();


                dt_Filter_SubFamilyDatatable = dtTableDesigner.AsEnumerable()
                                .GroupBy(r => r.Field<string>("SUBFAMILY_NAME"))
                                .Select(g => g.First())
                                .CopyToDataTable();

                DataTable dt_Filter_Datatable = new DataTable();

                dt_Filter_Datatable = dt_Filter_FamilyDatatable.Copy();
                dt_Filter_Datatable.Merge(dt_Filter_SubFamilyDatatable);


                //dt_Filter_Datatable = (from n in dt_Filter_Datatable.AsEnumerable()
                //                                      where !(n.Field<string>("FAMILY_NAME").Contains("")) && n.Field<string>("SUBFAMILY_NAME").Contains("") ||
                //                                      !(n.Field<string>("FAMILY_NAME").Contains("")) && !(n.Field<string>("SUBFAMILY_NAME").Contains(""))
                //                       select n).CopyToDataTable();





                DataSet importDataset = new DataSet();
                string importTemp;
                importTemp = Guid.NewGuid().ToString();

                string sqlString = string.Empty;

                using (var objSqlConnection = new SqlConnection(sqlConn))
                {

                    objSqlConnection.Open();
                    sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    SqlCommand _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                    _DBCommand.ExecuteNonQuery();
                    sqlString = CreateTable("[##IMPORTTEMP" + importTemp + "]", dt_Filter_Datatable);

                    SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                    dbCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(dt_Filter_Datatable);

                    sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";
                    _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                    _DBCommand.CommandTimeout = 0;
                    _DBCommand.ExecuteNonQuery();

                    var objSqlDataAdapter = new SqlDataAdapter("select * from [##IMPORTTEMP" + importTemp + "]", objSqlConnection);
                    objSqlDataAdapter.Fill(importDataset);

                }

                DataTable importDataTable = (DataTable)importDataset.Tables[0];


                List<string> mastercatalogFamilyList = new List<string>();
                List<string> mastercatalogSubFamilyList = new List<string>();


                mastercatalogFamilyList = importDataTable.AsEnumerable()
                          .Select(r => r.Field<string>("FAMILY_ID"))
                          .Distinct().ToList();

                mastercatalogFamilyList.RemoveAll(item => item == null);

                mastercatalogSubFamilyList = importDataTable.AsEnumerable()
                       .Select(r => r.Field<string>("SUBFAMILY_ID"))
                       .Distinct().ToList();
                mastercatalogSubFamilyList.RemoveAll(item => item == null);

                mastercatalogFamilyList.AddRange(mastercatalogSubFamilyList);

                if (mastercatalogFamilyList.Count > 0)
                {

                    int getcurrentFamilyId = Convert.ToInt32(familyId);

                    string structureNameIsExists = string.Empty;
                    var currentFamilyTableStructure = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(x => x.FAMILY_ID == getcurrentFamilyId && x.STRUCTURE_NAME == structureName && x.CATALOG_ID == getcurrentCatalogId).Select(v => v.FAMILY_TABLE_STRUCTURE).FirstOrDefault();


                    //for (int i = 0; i < categorylistFamilyId.Count; i++)
                    //{
                    //    replaceCategoryFamilyId = categorylistFamilyId[i].ToString().Replace("~!~~!", "");
                    //    replaceCategoryFamilyId = categorylistFamilyId[i].ToString().Replace("!", "");
                    //    //categorylistFamilyId[i] = replaceCategoryFamilyId;

                    //    if (replaceCategoryFamilyId.Contains("~"))
                    //    {
                    //        getFamilyId = replaceCategoryFamilyId.Split('~');
                    //        listFamilyId.Add(getFamilyId[1]);
                    //    }

                    //}

                    foreach (var getEachFamilyId in mastercatalogFamilyList)
                    {
                        DataSet checkStructureNameDataset = new DataSet();
                        try
                        {
                            using (var objMultipleTabledesignerSqlConnection = new SqlConnection(sqlConn))
                            {
                                objMultipleTabledesignerSqlConnection.Open();
                                SqlCommand objMultipleTabledesignerSqlCommand = objMultipleTabledesignerSqlConnection.CreateCommand();
                                objMultipleTabledesignerSqlCommand.CommandText = "STP_LS_TABLE_DESIGNER_MULTIPLE_FAMILIES";
                                objMultipleTabledesignerSqlCommand.CommandType = CommandType.StoredProcedure;
                                objMultipleTabledesignerSqlCommand.CommandTimeout = 0;
                                objMultipleTabledesignerSqlCommand.Connection = objMultipleTabledesignerSqlCommand.Connection;
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@ACTION", SqlDbType.VarChar).Value = "CheckTabledesignerStructurename";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = masterCatalogId.ToString() ?? "";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar).Value = structureName ?? "";
                                objMultipleTabledesignerSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar).Value = getEachFamilyId ?? "";
                                var objMultipleTabledesignerSqlAdapter = new SqlDataAdapter(objMultipleTabledesignerSqlCommand);
                                objMultipleTabledesignerSqlAdapter.Fill(checkStructureNameDataset);
                                objMultipleTabledesignerSqlConnection.Close();
                            }

                            if (checkStructureNameDataset.Tables[0].Rows.Count > 0)
                            {
                                structureNameIsExists = checkStructureNameDataset.Tables[0].Rows[0][0].ToString();

                                using (var objMultipleTabledesignerSqlConnection = new SqlConnection(sqlConn))
                                {
                                    objMultipleTabledesignerSqlConnection.Open();
                                    SqlCommand objMultipleTabledesignerSqlCommand = objMultipleTabledesignerSqlConnection.CreateCommand();
                                    objMultipleTabledesignerSqlCommand.CommandText = "STP_LS_TABLE_DESIGNER_MULTIPLE_FAMILIES";
                                    objMultipleTabledesignerSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objMultipleTabledesignerSqlCommand.CommandTimeout = 0;
                                    objMultipleTabledesignerSqlCommand.Connection = objMultipleTabledesignerSqlCommand.Connection;
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@ACTION", SqlDbType.VarChar).Value = "UpdateTabledesignerStructurename";
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = masterCatalogId.ToString() ?? "";
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar).Value = structureName ?? "";
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar).Value = getEachFamilyId ?? "";
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.NVarChar).Value = currentFamilyTableStructure ?? "";
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar).Value = IsdefaultStructurename ?? "0";
                                    objMultipleTabledesignerSqlCommand.ExecuteNonQuery();
                                    objMultipleTabledesignerSqlConnection.Close();
                                }
                            }
                            else
                            {
                                using (var objMultipleTabledesignerSqlConnection = new SqlConnection(sqlConn))
                                {
                                    objMultipleTabledesignerSqlConnection.Open();
                                    SqlCommand objMultipleTabledesignerSqlCommand = objMultipleTabledesignerSqlConnection.CreateCommand();
                                    objMultipleTabledesignerSqlCommand.CommandText = "STP_LS_TABLE_DESIGNER_MULTIPLE_FAMILIES";
                                    objMultipleTabledesignerSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objMultipleTabledesignerSqlCommand.CommandTimeout = 0;
                                    objMultipleTabledesignerSqlCommand.Connection = objMultipleTabledesignerSqlCommand.Connection;
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@ACTION", SqlDbType.VarChar).Value = "InsertTabledesignerStructurename";
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.VarChar).Value = masterCatalogId.ToString() ?? "";
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar).Value = structureName ?? "";
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar).Value = getEachFamilyId ?? "";
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.NVarChar).Value = currentFamilyTableStructure ?? "";
                                    objMultipleTabledesignerSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar).Value = IsdefaultStructurename ?? "0";
                                    objMultipleTabledesignerSqlCommand.ExecuteNonQuery();
                                    objMultipleTabledesignerSqlConnection.Close();
                                }
                            }


                            result = "1";
                        }




                        catch (Exception ex)
                        {
                            Logger.Error("Error at TableDesignerApiController : multipleTableDesignerSelectionFamilyIds", ex);
                            return "0";
                        }
                    }
                }
            }

          return result;
                
        }

        #endregion

        public string CreateTable(string tableName, DataTable objtable)
        {
            try
            {
                string sqlsc = "CREATE TABLE " + tableName + "(";
                for (int i = 0; i < objtable.Columns.Count; i++)
                {
                    if (!objtable.Columns[i].ColumnName.Contains('['))
                    {
                        sqlsc += "\n [" + objtable.Columns[i].ColumnName + "] ";
                    }
                    else
                    {
                        sqlsc += "\n" + objtable.Columns[i].ColumnName + "";
                    }
                    if (objtable.Columns[i].ColumnName.Contains("CATEGORY_ID"))
                        sqlsc += "nvarchar(50) ";
                    else if (objtable.Columns[i].DataType.ToString().Contains("System.String") ||
                             objtable.Columns[i].ColumnName.Contains("SUBCATID") ||
                             objtable.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                        sqlsc += "nvarchar(max) ";
                    else
                        sqlsc += "nvarchar(max) ";
                    sqlsc += ",";
                }
                using (SqlConnection con = new SqlConnection(sqlConn))
                {
                    con.Open();
                    var sqlString = "EXEC('delete from QS_IMPORTMAPPING')";
                    SqlCommand sqlCommand = new SqlCommand(sqlString, con);
                    sqlCommand.ExecuteNonQuery();
                    con.Dispose();
                }

                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CreateTable : Program - ", ex);
                throw;
            }
        }

    }

   
    public class XmlTransformer
    {
        public string CustomerFolder = string.Empty;
        readonly CSEntities _dbcontext = new CSEntities();
        public string TransformToInDesignXml(string srcXml, string UserName)
        {
            string output = string.Empty;
            try
            {
                CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == UserName).Select(x => x.a.Comments).FirstOrDefault();
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }

                var stream = new MemoryStream(Encoding.Unicode.GetBytes(srcXml));
                var document = new XPathDocument(stream);
                var writer = new StringWriter();
                var transform = new XslCompiledTransform();
                var templatepath = HttpContext.Current.Server.MapPath("~/Content/ProductImages" + CustomerFolder);
                transform.Load(templatepath + "\\XMLTemplate.xsl");
                transform.Transform(document, null, writer);
                output = writer.ToString();
            }
            catch
            { }
            return output;
            //  return null;
        }

        public string TransformToHtml(string srcXml)
        {
            string output = string.Empty;
            try
            {
                var stream = new MemoryStream(Encoding.Unicode.GetBytes(srcXml));
                var document = new XPathDocument(stream);
                var writer = new StringWriter();
                var transform = new XslCompiledTransform();
                transform.Load(Application.StartupPath + "\\HTMLTable.xsl");
                transform.Transform(document, null, writer);
                output = writer.ToString();
            }
            catch
            { }
            return output;

        }

    }
    public class SystemSettingsAttributes
    {
        #region "Declarations..."

        private string _Key;
        private string _Value;


        #endregion

        #region "Constructors..."

        public SystemSettingsAttributes()
        {

        }

        public SystemSettingsAttributes(string _MemberKey, string _MemberValue)
        {
            this._Key = _MemberKey;
            this._Value = _MemberValue;
        }

        #endregion

        #region "Properties..."

        [ConfigurationProperty("Key")]
        public string MemberKey
        {
            get
            {
                return _Key;
            }
            set
            {
                _Key = value;
            }
        }

        [ConfigurationProperty("Value")]

        public string MemberValue
        {
            get
            {
                return _Value;
            }
            set
            {
                _Value = value;
            }
        }
        #endregion

    }
    public class TREENODEDATA
    {

        public string ATTRIBUTE_ID { get; set; }
        public string ATTRIBUTE_NAME { get; set; }

    }



    public class Tabledesignernodes
    {
        public string SummaryGroupField { get; set; }
        public string TableGroupField { get; set; }
        public int SummaryGroupFieldValue { get; set; }
        public int TableGroupFieldValue { get; set; }
        public string PlaceHolderText { get; set; }
        public string PivotHeaderText { get; set; }

        public bool DisplayRowHeader { get; set; }
        public bool DisplayColumnHeader { get; set; }
        public bool DisplaySummaryHeader { get; set; }
        public bool VerticalTable { get; set; }
    }
}