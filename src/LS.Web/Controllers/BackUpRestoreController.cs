﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Reflection;
using System.Diagnostics;
using System.ServiceProcess;
using log4net;
using System.IO.Compression;
using LS.Data.Model;
using LS.Web.Models;
using LS.Data;
//using Microsoft.SqlServer.Management.Common;
//using Microsoft.SqlServer.Management.Smo;
using Newtonsoft.Json;
using Stimulsoft.Base.Json.Linq;
namespace LS.Web.Controllers
{
    public class BackUpRestoreController : ApiController
    {
        //------------------------------------------Seperated path values initializing------------------------------------------------------//





        public string pathCheckFlag = System.Web.Configuration.WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
        public string pathServerValue = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDriveURL"].ToString();
        public string serverPathShareVal = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
        //------------------------------------------Seperated path values initializing------------------------------------------------------//

        #region Global Declaration

        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        public string connectionBackupString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        private static readonly ILog Logger = LogManager.GetLogger(typeof(BackUpRestoreController));
        private readonly CSEntities _dbcontext = new CSEntities();

        //public string databaseBackUpPath = ConfigurationManager.AppSettings["BackUpDataBasePathe"];
        //string backUppathe = ConfigurationManager.AppSettings["BackUpMdfPath"];
      

        #endregion

        #region GetCustomer
        /// <summary>
        /// Get Customer Catalog Name and Customer Folder
        /// </summary>
        /// <param name="CatalogId"></param>
        /// <param name="CustomerID"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public string GetCustomerCatalog(int CatalogId, int CustomerID)
        {
            try
            {
                var customers = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                string attachmentPath = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.BackUpAttachement).FirstOrDefault();


                string getCatalogId_CustomerFolder = string.Empty;
                if (CatalogId != 0)
                {

                    var getCatalogName = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_ID == CatalogId).Select(y => y.CATALOG_NAME).FirstOrDefault();
                    var getCustomerPath = _dbcontext.Customers.Where(x => x.CustomerId == CustomerID).Select(y => y.Comments).FirstOrDefault();
                   // var attachmentPath = ConfigurationManager.AppSettings["BackUpAttachement"] + getCustomerPath;
                    getCatalogId_CustomerFolder = string.Format("{0},{1}", getCatalogName, attachmentPath);
                }
                return getCatalogId_CustomerFolder;

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : GetCustomerCatalog", objexception);
                return "GetCustomerCatalog Failed";
            }
        }
        #endregion
        #region   Full BackUp Path
        /// <summary>
        /// Full BackUp Path
        /// </summary>
        /// <param name="catalogId"></param>
        /// <param name="databaseName"></param>
        /// <param name="pathName"></param>
        /// <param name="seletedBackupItems"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public string NewDatabseBackupFile(int catalogId, string databaseName, string pathName, string seletedBackupItems, object templateDetails)
        {
            try
            {
                var customers = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                string backUppathe = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.BackUpMdfPath).FirstOrDefault();
                string customerFolder = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();
                
                int template_id = SaveTemplate(templateDetails);
                if (template_id != -1)
                {
                    string createTable = string.Empty;
                    if (!string.IsNullOrEmpty(pathName))
                    {
                        //-------------------------------------Separate path changes------------------------------------//
                        var fullPathName = "";
                        if (pathCheckFlag == "false")
                            fullPathName = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + pathName +"//"+customerFolder+  "//BacupFiles";
                        else
                            fullPathName = serverPathShareVal +"//"+ pathName.Replace("\\","//")+"//"+customerFolder+ "//BacupFiles";
                        if (!Directory.Exists(fullPathName))
                            Directory.CreateDirectory(fullPathName);
                        //-------------------------------------Separate path changes------------------------------------//
                        string date = DateTime.Now.ToString("yyyyMMddHHmmss");
                        databaseName = string.Format("{0}{1}", databaseName, date);
                        if (seletedBackupItems == "All")
                        {
                            if (!string.IsNullOrEmpty(databaseName))
                            {
                                string createdDatabse = CreateDatabse(databaseName, backUppathe);
                                if (!string.IsNullOrEmpty(createdDatabse) && createdDatabse == "Created")
                                {
                                    createTable = CreateTable(databaseName, catalogId, fullPathName, seletedBackupItems, templateDetails);
                                }
                                else
                                {
                                    createTable = "Create action not completed.";
                                }
                            }
                        }
                    }
                    else
                    {
                        createTable = "Please select Backup path";
                    }
                    return createTable + '~' + template_id;
                }
                else
                {
                    return "Template name already exists" + '~' + 0;
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : NewDatabseBackupFile", objexception);
                return "Create action not completed";
            }
        }

        /// <summary>
        /// Full BackUp Path
        /// </summary>
        /// <param name="catalogId"></param>
        /// <param name="databaseName"></param>
        /// <param name="pathName"></param>
        /// <param name="seletedBackupItems"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public string NewDatabseBackupforFiles(int catalogId, string databaseName, string pathName, string seletedBackupItems, object templateDetails)
        {
            try
            {
                var customers = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                string backUppathe = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.BackUpMdfPath).FirstOrDefault();
                int template_id = SaveTemplate(templateDetails);
                if (template_id != -1)
                {
                    string fileName = DateTime.Now.ToString("yyyyMMddHHmmssf");
                    fileName = "Files" + fileName;
                    System.Web.HttpContext.Current.Session["FileName"] = fileName;
                    string createTable = string.Empty;
                    if (!string.IsNullOrEmpty(pathName))
                    {
                        //var fullPathName = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + pathName;
                        //-------------------------------------Separate path changes------------------------------------//
                        var fullPathName = "";
                        if (pathCheckFlag == "false")
                            fullPathName = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + pathName;
                        else
                            fullPathName = serverPathShareVal + "//" + pathName.Replace("\\", "//");
                        //-------------------------------------Separate path changes------------------------------------//
                        string date = DateTime.Now.ToString("yyyyMMddHHmmss");
                        databaseName = string.Format("{0}{1}", databaseName, date);
                        if (seletedBackupItems != "All")
                        {
                            if (!string.IsNullOrEmpty(databaseName))
                            {
                                string createdDatabse = CreateDatabse(databaseName, backUppathe);
                                if (!string.IsNullOrEmpty(createdDatabse) && createdDatabse == "Created")
                                {
                                    createTable = CreateTable(databaseName, catalogId, fullPathName, seletedBackupItems, templateDetails);
                                }
                            }
                        }
                    }
                    return createTable + '~' + template_id;
                }
                else
                {
                    return "Template name already exists" + '~' + 0;
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : NewDatabseBackupforFiles", objexception);
                return "Create action not completed";
            }
        }

        #endregion

        #region Create Database
        /// <summary>
        /// Create Database 
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="pathName"></param>
        /// <returns></returns>
        public string CreateDatabse(string databaseName, string backUpMdFPath)
        {
            try
            {
                
                if (System.Web.HttpContext.Current.Session["CreateDatabasePath"] != null)
                {
                   string backUppathe = Convert.ToString(System.Web.HttpContext.Current.Session["CreateDatabasePath"]);
                }
                else
                {
                    var customers = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                   string backUppathe = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.BackUpMdfPath).FirstOrDefault();
                   // backUppathe = ConfigurationManager.AppSettings["BackUpMdfPath"];
                }

                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    var customers = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    string backUppathe = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.BackUpMdfPath).FirstOrDefault();
                    SqlCommand myCommand = new SqlCommand();
                    myCommand.CommandText = "CreateNewDatabaseBackup";
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.Connection = con;
                    myCommand.Parameters.Add("@CreatedPath", SqlDbType.NVarChar, 1000).Value = backUppathe;
                    myCommand.Parameters.Add("@DatabaseName", SqlDbType.NVarChar, 1000).Value = databaseName;
                    con.Open();
                    myCommand.ExecuteNonQuery();
                    con.Close();
                    return "Created";
                }

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : CreateDatabse", objexception);
                return "Create action not completed";
            }


        }
        #endregion
        #region CreateTable
        /// <summary>
        /// CreateTable 
        /// </summary>
        /// <returns></returns>
        public string CreateTable(string databaseName, int catalogId, string fullPathName, string seletedBackupItems, object templateDetails)
        {
            try
            {
                List<string> selectedItems = new List<string>();
                List<string> selectedItems_User = new List<string>();
                string[] userAndRole = new string[] { "UserDetails", "RoleCatalog", "RoleGroup_name", "RoleAttribute" };
                selectedItems_User = userAndRole.ToList();
                bool PRODUCTS_WITH_HIERARCHY = false;
                bool ATTRIBUTE_LIST = false;
                bool PICKLIST = false;
                bool USER_AND_ROLE = false;
                bool INDESIGN_PROJECTS = false;
                bool TABLE_DESIGNER = false;
                if (seletedBackupItems == "All")
                {
                    PRODUCTS_WITH_HIERARCHY = true;
                    ATTRIBUTE_LIST = true;
                    PICKLIST = true;
                    USER_AND_ROLE = true;
                    INDESIGN_PROJECTS = true;
                    TABLE_DESIGNER = true;
                }
                else
                {
                    var arr = JObject.Parse(templateDetails.ToString());
                    PRODUCTS_WITH_HIERARCHY = Convert.ToBoolean(arr["PRODUCTS_WITH_HIERARCHY"]);
                    ATTRIBUTE_LIST = Convert.ToBoolean(arr["ATTRIBUTE_LIST"]);
                    PICKLIST = Convert.ToBoolean(arr["PICKLIST"]);
                    USER_AND_ROLE = Convert.ToBoolean(arr["USER_AND_ROLE"]);
                    INDESIGN_PROJECTS = Convert.ToBoolean(arr["INDESIGN_PROJECTS"]);
                    TABLE_DESIGNER = Convert.ToBoolean(arr["TABLE_DESIGNER"]);
                }
                string SQLString = string.Empty;
                BackUpRestoreController BackupController = new BackUpRestoreController();
                var con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionBackupString);
                string UserId = builder.UserID;
                string Password = builder.Password;
                SqlConnectionStringBuilder myBuilder = new SqlConnectionStringBuilder();
                myBuilder.DataSource = con.DataSource;
                myBuilder.InitialCatalog = databaseName;
                myBuilder.UserID = UserId;
                myBuilder.Password = Password;
                myBuilder.MultipleActiveResultSets = true;
                myBuilder.ConnectTimeout = 30;
                con.ConnectionString = myBuilder.ConnectionString;
                if (ATTRIBUTE_LIST)
                {
                    AttributeListTable(con.ConnectionString, catalogId);
                    selectedItems.Add("ATTRIBUTELIST");
                }
                if (USER_AND_ROLE)
                {
                    UserListTable(con.ConnectionString, catalogId);
                    RoleListTable(con.ConnectionString, catalogId);
                    selectedItems.AddRange(selectedItems_User);
                }
                if (PRODUCTS_WITH_HIERARCHY && TABLE_DESIGNER)
                {
                    ExportTable(con.ConnectionString, catalogId);
                    selectedItems.Add("Category");
                    selectedItems.Add("FAMILY");
                    selectedItems.Add("PRODUCT");
                    selectedItems.Add("TableDesigner");
                }
                else if (PRODUCTS_WITH_HIERARCHY && TABLE_DESIGNER == false)
                {
                    ExportTable(con.ConnectionString, catalogId);
                    selectedItems.Add("Category");
                    selectedItems.Add("FAMILY");
                    selectedItems.Add("PRODUCT");
                }
                else if (PRODUCTS_WITH_HIERARCHY == false && TABLE_DESIGNER)
                {
                    ExportTable(con.ConnectionString, catalogId);
                    selectedItems.Add("TableDesigner");
                }

                if (INDESIGN_PROJECTS)
                {
                    IndesignTable(con.ConnectionString, catalogId);
                    selectedItems.Add("IndesignDetails");
                }
                if (PICKLIST)
                {
                    Picklist(con.ConnectionString, catalogId);
                    selectedItems.Add("PickList");
                }
                string BuackupResult = string.Empty;
                if (seletedBackupItems == "All")
                {
                    BuackupResult = BackupController.BackupFile(databaseName, fullPathName, con.ConnectionString.ToString());
                    if (BuackupResult == "BackUp Completed")
                    {
                        BackupController.ZipFile(databaseName, fullPathName);
                        BackupController.DeletedFiles(databaseName, fullPathName, con.ConnectionString.ToString());
                    }
                }
                else
                {
                    using (var sqlCon = new SqlConnection(con.ConnectionString.ToString()))
                    {
                        sqlCon.Open();
                        foreach (string table in selectedItems)
                        {
                            var val = "";
                            string query = "SELECT * FROM " + table;
                            System.Web.HttpContext.Current.Session[table] = null;

                            var cmd = new SqlCommand("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'" + table + " '", sqlCon);
                            cmd.CommandTimeout = 0;
                            val = Convert.ToString(cmd.ExecuteScalar());
                            if (val != null && val != "")
                            {
                                var Passtable = new DataTable();
                                SqlCommand objSqlCommand = sqlCon.CreateCommand();
                                objSqlCommand.CommandText = query;
                                objSqlCommand.CommandType = CommandType.Text;
                                objSqlCommand.Connection = sqlCon;
                                var da = new SqlDataAdapter(objSqlCommand);
                                da.Fill(Passtable);
                                System.Web.HttpContext.Current.Session[table] = Passtable;
                            }
                        }
                        sqlCon.Close();
                    }
                    System.Web.HttpContext.Current.Session["Files"] = selectedItems;
                    BackupController.DeletedFiles(databaseName, fullPathName, con.ConnectionString.ToString());
                }

                return BuackupResult;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : CreateNewCategory", objexception);
                return "Create action not completed";
            }

        }
        #endregion
        #region AttributeListTable
        public void AttributeListTable(string connectionString, int catalogId)
        {
            string SQLString = string.Empty;
            BackUpRestoreController BackupController = new BackUpRestoreController();
            try
            {
                DataTable AttributeLIst = new DataTable();
                AttributeApiController Attribute = new AttributeApiController();
                Attribute.ExportAttributesList(catalogId);
                using (var conn = new SqlConnection(connectionString.ToString()))
                {
                    if (System.Web.HttpContext.Current.Session["ExportTableAttributeList"] != null)
                    {
                        conn.Open();
                        SQLString = "EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''AttributeLIst'')BEGIN DROP TABLE [AttributeLIst] END')";
                        SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                        _DBCommand.ExecuteNonQuery();
                        var distinctdsCat = (DataTable)System.Web.HttpContext.Current.Session["ExportTableAttributeList"];
                        SQLString = BackupController.CreateAllTable("AttributeLIst", distinctdsCat);
                        SqlCommand dbCommandnew = new SqlCommand(SQLString, conn);
                        dbCommandnew.ExecuteNonQuery();
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                        {
                            sqlBulkCopy.DestinationTableName = "AttributeLIst";

                            foreach (var column in distinctdsCat.Columns)
                                sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                            sqlBulkCopy.WriteToServer(distinctdsCat);
                        }

                    }
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : AttributeListTable", objexception);
            }

        }
        #endregion
        #region UserListTable
        public void UserListTable(string connectionString, int catalogId)
        {
            try
            {
                BackUpRestoreController BackupController = new BackUpRestoreController();
                UserAdminApiController UserDetails = new UserAdminApiController();
                UserDetails.GetUserExportDetails(1);

                using (var conn = new SqlConnection(connectionString.ToString()))
                {
                    if (System.Web.HttpContext.Current.Session["userExportTable"] != null)
                    {
                        conn.Open();
                        string userExport = "EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''UserDetails'')BEGIN DROP TABLE [UserDetails] END')";
                        SqlCommand cmd_UserDetails = new SqlCommand(userExport, conn);
                        cmd_UserDetails.ExecuteNonQuery();

                        var userTabel = (DataTable)System.Web.HttpContext.Current.Session["userExportTable"];

                        string userCreateExport = BackupController.CreateAllTable("UserDetails", userTabel);
                        SqlCommand cmd_userCreateExport = new SqlCommand(userCreateExport, conn);
                        cmd_userCreateExport.ExecuteNonQuery();


                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                        {
                            sqlBulkCopy.DestinationTableName = "UserDetails";

                            foreach (var column in userTabel.Columns)
                                sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                            sqlBulkCopy.WriteToServer(userTabel);
                        }

                    }
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : UserListTable", objexception);
            }


        }
        #endregion
        #region IndesignTable
        public void IndesignTable(string connectionString, int catalogId)
        {
            try
            {
                BackUpRestoreController BackupController = new BackUpRestoreController();
                InDesignApiController IndesignExport = new InDesignApiController();
                IndesignExport.InDesigExport(catalogId, true, "true", 0);

                using (var conn = new SqlConnection(connectionString.ToString()))
                {
                    if (System.Web.HttpContext.Current.Session["ExportTableInDesign"] != null)
                    {
                        conn.Open();
                        string indesignExportTable = "EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''IndesignDetails'')BEGIN DROP TABLE [IndesignDetails] END')";
                        SqlCommand cmd_IndesignExportTable = new SqlCommand(indesignExportTable, conn);
                        cmd_IndesignExportTable.ExecuteNonQuery();

                        var indesignTabel = (DataTable)System.Web.HttpContext.Current.Session["ExportTableInDesign"];

                        string indesignCreateTable = BackupController.CreateAllTable("IndesignDetails", indesignTabel);
                        SqlCommand cmd_indesignCreateTable = new SqlCommand(indesignCreateTable, conn);
                        cmd_indesignCreateTable.ExecuteNonQuery();


                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                        {
                            sqlBulkCopy.DestinationTableName = "IndesignDetails";

                            foreach (var column in indesignTabel.Columns)
                                sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                            sqlBulkCopy.WriteToServer(indesignTabel);
                        }

                    }
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : UserListTable", objexception);
            }


        }
        #endregion
        #region Picklist
        public void Picklist(string connectionString, int catalogId)
        {
            try
            {
                BackUpRestoreController BackupController = new BackUpRestoreController();
                HomeApiController picklistExport = new HomeApiController();
                string SelectedPickListDataType = "";
                string catalog_Id = Convert.ToString(catalogId);
                bool DisplayIdcolumns = true;
                picklistExport.GetPicklistExportData(SelectedPickListDataType, catalog_Id, DisplayIdcolumns);
                using (var conn = new SqlConnection(connectionString.ToString()))
                {
                    if (System.Web.HttpContext.Current.Session["PicklistExport"] != null)
                    {
                        conn.Open();
                        string picklistSqlString = "EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''PickList'')BEGIN DROP TABLE [PickList] END')";
                        SqlCommand _DBCommand = new SqlCommand(picklistSqlString, conn);
                        _DBCommand.ExecuteNonQuery();

                        var picklistCreateTable = (DataTable)System.Web.HttpContext.Current.Session["PicklistExport"];
                        picklistSqlString = BackupController.CreateAllTable("PickList", picklistCreateTable);
                        SqlCommand cmd_PicklistSqlString = new SqlCommand(picklistSqlString, conn);
                        cmd_PicklistSqlString.ExecuteNonQuery();

                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                        {
                            sqlBulkCopy.DestinationTableName = "PickList";

                            foreach (var column in picklistCreateTable.Columns)
                                sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                            sqlBulkCopy.WriteToServer(picklistCreateTable);
                        }

                    }
                }

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : Picklist", objexception);
            }
        }
        #endregion
        #region RoleListTable
        public void RoleListTable(string connectionString, int catalogId)
        {
            try
            {
                BackUpRestoreController BackupController = new BackUpRestoreController();
                UserAdminApiController UserDetails = new UserAdminApiController();
                UserDetails.GetRoleExport();

                using (var conn = new SqlConnection(connectionString.ToString()))
                {
                    if (System.Web.HttpContext.Current.Session["roleExport"] != null)
                    {
                        conn.Open();
                        string roleCatalogExport = "EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''RoleCatalog'')BEGIN DROP TABLE [RoleCatalog] END')";
                        SqlCommand cmd_roleCatalogDetails = new SqlCommand(roleCatalogExport, conn);
                        cmd_roleCatalogDetails.ExecuteNonQuery();

                        string roleGroup_NameExport = "EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''RoleGroup_name'')BEGIN DROP TABLE [RoleGroup_name] END')";
                        SqlCommand cmd_roleGroup_NameDetails = new SqlCommand(roleGroup_NameExport, conn);
                        cmd_roleGroup_NameDetails.ExecuteNonQuery();


                        string RoleAttributeExport = "EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''RoleAttribute'')BEGIN DROP TABLE [RoleAttribute] END')";
                        SqlCommand cmd_RoleAttributeDetails = new SqlCommand(RoleAttributeExport, conn);
                        cmd_RoleAttributeDetails.ExecuteNonQuery();

                        var roleTabel = (DataSet)System.Web.HttpContext.Current.Session["roleExport"];
                        DataTable roleCatalogTable = roleTabel.Tables[0];
                        DataTable roleGroup_NameTable = roleTabel.Tables[1];
                        DataTable RoleAttributeTable = roleTabel.Tables[2];


                        string CatalogExport = BackupController.CreateAllTable("RoleCatalog", roleCatalogTable);
                        SqlCommand cmd_CatalogExport = new SqlCommand(CatalogExport, conn);
                        cmd_CatalogExport.ExecuteNonQuery();

                        string Group_NameExport = BackupController.CreateAllTable("RoleGroup_name", roleGroup_NameTable);
                        SqlCommand cmd_GroupNameExport = new SqlCommand(Group_NameExport, conn);
                        cmd_GroupNameExport.ExecuteNonQuery();

                        string AttributeExport = BackupController.CreateAllTable("RoleAttribute", RoleAttributeTable);
                        SqlCommand cmd_AttributeExport = new SqlCommand(AttributeExport, conn);
                        cmd_AttributeExport.ExecuteNonQuery();


                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                        {
                            sqlBulkCopy.DestinationTableName = "RoleCatalog";

                            foreach (var column in roleCatalogTable.Columns)
                                sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                            sqlBulkCopy.WriteToServer(roleCatalogTable);
                        }
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                        {
                            sqlBulkCopy.DestinationTableName = "RoleGroup_name";

                            foreach (var column in roleGroup_NameTable.Columns)
                                sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                            sqlBulkCopy.WriteToServer(roleGroup_NameTable);
                        }
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                        {
                            sqlBulkCopy.DestinationTableName = "RoleAttribute";

                            foreach (var column in RoleAttributeTable.Columns)
                                sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                            sqlBulkCopy.WriteToServer(RoleAttributeTable);
                        }

                    }
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : RoleListTable", objexception);
            }


        }
        #endregion
        #region ExportTable
        public void ExportTable(string connectionString, int catalogId)
        {
            string SQLString = string.Empty;
            BackUpRestoreController BackupController = new BackUpRestoreController();
            try
            {
                string model = "[{\"PROJECT_NAME\":\"\",\"COMMENTS\":\"\",\"exportAttributes\":\"\",\"applyFilter\":\"\",\"familyFilter\":\"\",\"subProduct\":\"\"}]";
                HomeApiController ExportAllProduct = new HomeApiController();
                var AttributeNames = (ExportAllProduct.GetSelectedattributes(catalogId, 0));

                var allAttributeNames = (from catalogAttributes in _dbcontext.TB_CATALOG_ATTRIBUTES
                                         join attributes in _dbcontext.TB_ATTRIBUTE on catalogAttributes.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                         join pfal in _dbcontext.TB_PROD_FAMILY_ATTR_LIST on attributes.ATTRIBUTE_ID equals pfal.ATTRIBUTE_ID
                                         where catalogAttributes.CATALOG_ID == catalogId && attributes.FLAG_RECYCLE == "A" && attributes.ATTRIBUTE_NAME != "ITEM#"
                                         select new
                                         {
                                             catalogAttributes.CATALOG_ID,
                                             attributes.ATTRIBUTE_ID,
                                             attributes.ATTRIBUTE_NAME,
                                             attributes.ATTRIBUTE_TYPE,
                                             ISAvailable = false,
                                         }).Distinct().ToList();

                foreach (var listofselectedattr in allAttributeNames)
                {
                    QS_CATALOGATTRIBUTE_Result alls = new QS_CATALOGATTRIBUTE_Result();
                    alls.ATTRIBUTE_ID = listofselectedattr.ATTRIBUTE_ID;
                    alls.ATTRIBUTE_TYPE = listofselectedattr.ATTRIBUTE_TYPE;
                    alls.ISAvailable = 0;
                    alls.ATTRIBUTE_NAME = listofselectedattr.ATTRIBUTE_NAME;
                    alls.DISPLAY_NAME = listofselectedattr.ATTRIBUTE_NAME;
                    AttributeNames.Add(alls);
                }

                var updatedJson = AddObjectsToJson(model, AttributeNames);
                Newtonsoft.Json.Linq.JArray gastosArray = (Newtonsoft.Json.Linq.JArray)Newtonsoft.Json.JsonConvert.DeserializeObject(updatedJson);
                ExportAllProduct.ExportXls(catalogId, "Column", "No", "No", null, "All", 0, gastosArray, "BackUp", "All", "", "XLS", "", "", "", "Yes", "Yes", "Export.XLS","");
                using (var conn = new SqlConnection(connectionString.ToString()))
                {
                    conn.Open();
                    if (System.Web.HttpContext.Current.Session["ExportTableTableDesigner"] != null)
                    {
                        SQLString = "EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''TableDesigner'')BEGIN DROP TABLE [TableDesigner] END')";
                        SqlCommand TableDesigner_DBCommand = new SqlCommand(SQLString, conn);
                        TableDesigner_DBCommand.ExecuteNonQuery();
                        var table_TableDesigner = (DataTable)System.Web.HttpContext.Current.Session["ExportTableTableDesigner"];
                        SQLString = BackupController.CreateAllTable("TableDesigner", table_TableDesigner);
                        SqlCommand TableDesigner_cmd = new SqlCommand(SQLString, conn);
                        TableDesigner_cmd.ExecuteNonQuery();
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                        {
                            sqlBulkCopy.DestinationTableName = "TableDesigner";

                            foreach (var column in table_TableDesigner.Columns)
                                sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                            sqlBulkCopy.WriteToServer(table_TableDesigner);
                        }

                    }
                    if (System.Web.HttpContext.Current.Session["ExportTableCAT"] != null)
                    {

                        SQLString = "EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''Category'')BEGIN DROP TABLE [Category] END')";
                        SqlCommand Category_DBCommand = new SqlCommand(SQLString, conn);
                        Category_DBCommand.ExecuteNonQuery();
                        var category_Table = (DataTable)System.Web.HttpContext.Current.Session["ExportTableCAT"];
                        SQLString = BackupController.CreateAllTable("Category", category_Table);
                        SqlCommand Category_Cmd = new SqlCommand(SQLString, conn);
                        Category_Cmd.ExecuteNonQuery();
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                        {
                            sqlBulkCopy.DestinationTableName = "Category";

                            foreach (var column in category_Table.Columns)
                                sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                            sqlBulkCopy.WriteToServer(category_Table);
                        }

                    }
                    if (System.Web.HttpContext.Current.Session["ExportTableFAM"] != null)
                    {
                        SQLString = "EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''FAMILY'')BEGIN DROP TABLE [FAMILY] END')";
                        SqlCommand family_DBCommand = new SqlCommand(SQLString, conn);
                        family_DBCommand.ExecuteNonQuery();
                        var family_Table = (DataTable)System.Web.HttpContext.Current.Session["ExportTableFAM"];
                        SQLString = BackupController.CreateAllTable("FAMILY", family_Table);
                        SqlCommand family_Cmd = new SqlCommand(SQLString, conn);
                        family_Cmd.ExecuteNonQuery();
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                        {
                            sqlBulkCopy.DestinationTableName = "FAMILY";

                            foreach (var column in family_Table.Columns)
                                sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                            sqlBulkCopy.WriteToServer(family_Table);
                        }

                    }
                    if (System.Web.HttpContext.Current.Session["ExportTable"] != null)
                    {
                        SQLString = "EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''PRODUCT'')BEGIN DROP TABLE [PRODUCT] END')";
                        SqlCommand product_DBCommand = new SqlCommand(SQLString, conn);
                        product_DBCommand.ExecuteNonQuery();
                        var product_Table = (DataTable)System.Web.HttpContext.Current.Session["ExportTable"];
                        SQLString = BackupController.CreateAllTable("PRODUCT", product_Table);
                        SqlCommand product_Cmd = new SqlCommand(SQLString, conn);
                        product_Cmd.ExecuteNonQuery();
                        using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(conn))
                        {
                            sqlBulkCopy.DestinationTableName = "PRODUCT";

                            foreach (var column in product_Table.Columns)
                                sqlBulkCopy.ColumnMappings.Add(column.ToString(), column.ToString());

                            sqlBulkCopy.WriteToServer(product_Table);
                        }

                    }


                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : ExportTable", objexception);
            }

        }
        #endregion

        #region CreateAllTable
        /// <summary>
        /// CreateAllTable
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="objtable"></param>
        /// <returns></returns>
        public string CreateAllTable(string tableName, DataTable objtable)
        {

            string sqlsc = "CREATE TABLE " + tableName + "(";
            for (int i = 0; i < objtable.Columns.Count; i++)
            {
                if (!objtable.Columns[i].ColumnName.Contains('['))
                {
                    sqlsc += "\n [" + objtable.Columns[i].ColumnName + "] ";
                }
                else
                {
                    sqlsc += "\n" + objtable.Columns[i].ColumnName + "";
                }
                if (objtable.Columns[i].ColumnName.Contains("CATALOG_ID"))
                    sqlsc += "nvarchar(50) ";
                else if (objtable.Columns[i].DataType.ToString().Contains("System.String") ||
                         objtable.Columns[i].ColumnName.Contains("SUBCATID") ||
                         objtable.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                    sqlsc += "varchar(8000) ";
                else
                    sqlsc += "varchar(8000) ";
                sqlsc += ",";
            }
            return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
        }
        #endregion
        #region Create Backup (bak file)
        /// <summary>
        /// Create Backup File
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="fullPathName"></param>
        /// <param name="ConnectionString"></param>
        /// <returns></returns>
        public string BackupFile(string databaseName, string fullPathName, string ConnectionString)
        {
            try
            {
                var customers = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                string backUppathe = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.BackUpMdfPath).FirstOrDefault();

                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand();
                    SqlDataReader reader;
                    string query_BackUpBakFile = string.Format("BACKUP DATABASE [{0}] TO DISK='{1}{0}.bak'", databaseName, backUppathe);
                    cmd.CommandText = query_BackUpBakFile;
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = conn;
                    reader = cmd.ExecuteReader();
                    conn.Close();

                }
                return "BackUp Completed";
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : BackupFile", objexception);
                return "Create action not completed";
            }
        }
        #endregion
        #region ZipFile
        public void ZipFile(string databaseName, string fullPathName)
        {
            try
            {
                var customers = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                string databaseBackUpPath = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.BackUpDataBasePath).FirstOrDefault();

                string bak_backupPath = string.Format(@"{0}{1}.bak", databaseBackUpPath, databaseName);
                string bak_StoreLocalPath = string.Format(@"{0}\{1}.bak", fullPathName, databaseName);
                string sZipFile = string.Format(@"{0}\{1}.zip", fullPathName, databaseName);
                byte[] localDatabase;
                MemoryStream ms = new MemoryStream();
                localDatabase = File.ReadAllBytes(bak_backupPath);
                File.WriteAllBytes(bak_StoreLocalPath, localDatabase);
                ms.Close();
                // FileStream stream = File.OpenRead(bak_StoreLocalPath);
                //byte[] fileBytes = new byte[stream.Length];
                //stream.Read(fileBytes, 0, fileBytes.Length);
                //stream.Close();
                //using (FileStream __fStream = File.Open(sZipFile, FileMode.Create))
                //{
                //    GZipStream obj = new GZipStream(__fStream, CompressionMode.Compress);

                //    byte[] bt = File.ReadAllBytes(bak_StoreLocalPath);
                //    obj.Write(bt, 0, bt.Length);

                //    obj.Close();
                //    obj.Dispose();
                //}

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : ZipFile", objexception);
            }
        }
        #endregion
        #region DeletedFiles
        public void DeletedFiles(string databaseName, string fullPathName, string ConnectionString)
        {
            try
            {
                var customers = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                string databaseBackUpPath = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.BackUpDataBasePath).FirstOrDefault();
                string bak_StoreLocalPath = string.Format(@"{0}{1}.bak", databaseBackUpPath, databaseName);
                string local_StorePath = string.Format(@"{0}\{1}.bak", fullPathName, databaseName);
                string sDataBase = string.Format(@"{0}[{1}].mdf", databaseBackUpPath, databaseName);
                string sDataBaseLog = string.Format(@"{0}[{1}].ldf", databaseBackUpPath, databaseName);
                using (SqlConnection conn = new SqlConnection(ConnectionString))
                {
                    conn.Open();
                    var AlterStr = "ALTER DATABASE [" + databaseName + " ] SET OFFLINE WITH ROLLBACK IMMEDIATE";
                    var AlterCmd = new SqlCommand(AlterStr, conn);
                    var DropStr = "DROP DATABASE [" + databaseName + "] ";
                    var DropCmd = new SqlCommand(DropStr, conn);
                    AlterCmd.ExecuteNonQuery();
                    DropCmd.ExecuteNonQuery();
                }

                if ((System.IO.File.Exists(bak_StoreLocalPath)))
                {
                    System.IO.File.Delete(bak_StoreLocalPath);
                }
                //if ((System.IO.File.Exists(local_StorePath)))
                //{
                //    System.IO.File.Delete(local_StorePath);
                //}
                if ((System.IO.File.Exists(sDataBase)))
                {
                    System.IO.File.Delete(sDataBase);
                }
                if ((System.IO.File.Exists(sDataBaseLog)))
                {
                    System.IO.File.Delete(sDataBaseLog);
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : DeletedFiles", objexception);
            }
        }
        #endregion


        public string AddObjectsToJson(string json, IList objects)
        {
            IList list = Newtonsoft.Json.JsonConvert.DeserializeObject<IList>(json);
            list.Add(objects);
            list.Add("[]");
            list.Add(null);
            list.Add("[{\"ATTRIBUTE_ID\":1,\"ATTRIBUTE_NAME\":\"ITEM#\",\"DISPLAY_NAME\":\"ITEM#\",\"ATTRIBUTE_TYPE\":1,\"PUBLISH2CDROM\":false,\"PUBLISH2ODP\":false,\"PUBLISH2PRINT\":false,\"PUBLISH2WEB\":false,\"ISAvailable\":0}]");
            return Newtonsoft.Json.JsonConvert.SerializeObject(list);
        }

        #region Get Template Details

        [System.Web.Http.HttpGet]
        public IList GetAllTemplateDetails(int catalogId)
        {
            try
            {
                return _dbcontext.TB_TEMPLATE_BACKUP.Where(s => s.CATALOG_ID == catalogId).Select(s => new { s.TEMPLATE_ID, s.TEMPLATE_NAME }).ToList();
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : GetAllTemplateDetails", objexception);
                return null;
            }
        }

        public int SaveTemplate(object templateDetails)
        {
            try
            {
                string json = JsonConvert.SerializeObject(templateDetails);
                TB_TEMPLATE_BACKUP _template = new TB_TEMPLATE_BACKUP();
                var arr = JObject.Parse(templateDetails.ToString());
                string template_Name = Convert.ToString(arr["Template_Name"]);
                if (template_Name != null && template_Name != "" && template_Name != null)
                {
                    int catalog_Id = Convert.ToInt32(arr["Catalog_Id"]);
                    TB_TEMPLATE_BACKUP template = _dbcontext.TB_TEMPLATE_BACKUP.Where(s => s.TEMPLATE_NAME.ToLower() == template_Name.ToLower() && s.CATALOG_ID == catalog_Id).FirstOrDefault();
                    if (template == null)
                    {
                       //// string Local_or_FTP = arr["Local_or_FTP"].ToString();
                       // if (Local_or_FTP.ToLower() == "no")
                       //     _template.IS_LOCAL = true;
                       // else
                       //     _template.IS_LOCAL = false;
                       // string startDate = arr["FROM_DATE"].ToString();

                       // if (startDate == null || startDate == "")
                       //     _template.FROM_DATE = DateTime.Now;
                       // else
                       // {
                       //     string[] DateFormating = startDate.Split('/');
                       //     DateTime date1 = new DateTime(int.Parse(DateFormating[2]), int.Parse(DateFormating[0]), int.Parse(DateFormating[1]));
                       //     _template.FROM_DATE = date1;
                       // }
                       // string endDate = arr["TO_DATE"].ToString();

                       // if (endDate == null || endDate == "")
                       //     _template.TO_DATE = DateTime.Now;
                       // else
                       // {
                       //     string[] DateFormating = endDate.Split('/');
                       //     DateTime date1 = new DateTime(int.Parse(DateFormating[2]), int.Parse(DateFormating[0]), int.Parse(DateFormating[1]));
                       //     _template.TO_DATE = date1;
                       // }
                       // // 2:30 AM
                       // if (arr["TIME"].ToString() != null && arr["TIME"].ToString() != "")
                       // {
                       //     string[] time = arr["TIME"].ToString().Split(':');
                       //     string[] time1 = time[1].ToString().Split(' ');
                       //     TimeSpan tooBig = new TimeSpan(int.Parse(time[0]), int.Parse(time1[0]), 0);
                       //     _template.TIME = tooBig;
                       // }
                        _template.LOCAL_PATH = arr["Local_Path"].ToString();
                        _template.TEMPLATE_NAME = arr["Template_Name"].ToString();
                        if (arr["Export_As"].ToString() == "1")
                            _template.EXPORT_AS = "Data";
                        else
                            _template.EXPORT_AS = "File";
                        _template.PRODUCTS_WITH_HIERARCHY = Convert.ToBoolean(arr["PRODUCTS_WITH_HIERARCHY"]);
                        _template.ATTRIBUTE_LIST = Convert.ToBoolean(arr["ATTRIBUTE_LIST"]);
                        _template.PICKLIST = Convert.ToBoolean(arr["PICKLIST"]);
                        _template.USER_AND_ROLE = Convert.ToBoolean(arr["USER_AND_ROLE"]);
                        _template.INDESIGN_PROJECTS = Convert.ToBoolean(arr["INDESIGN_PROJECTS"]);
                        _template.TABLE_DESIGNER = Convert.ToBoolean(arr["TABLE_DESIGNER"]);
                        //_template.FTP_URL = Convert.ToString(arr["FTP_URL"]);
                        //_template.FTP_USER_NAME = Convert.ToString(arr["FTP_USER_NAME"]);
                        //_template.FTP_PASSWORD = Convert.ToString(arr["FTP_PASSWORD"]);
                        //_template.CATALOG_ID = Convert.ToInt32(arr["Catalog_Id"]);
                        //_template.INTERVAL = Convert.ToString(arr["Interval"]);
                        _dbcontext.TB_TEMPLATE_BACKUP.Add(_template);
                        _dbcontext.SaveChanges();
                        return _template.TEMPLATE_ID;
                    }
                    else
                    {
                        return -1;
                    }
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at BackUpRestoreController : GetAllTemplateDetails", ex);
                return -1;
            }
        }

        [System.Web.Http.HttpGet]
        public IList GetTemplateDetails(int templateId)
        {
            try
            {
                var customerId = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var templateExists = _dbcontext.TB_TEMPLATE_BACKUP.Where(s => s.TEMPLATE_ID == templateId).ToList();
                return templateExists;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at BackUpRestoreController : GetTemplateDetails", objexception);
                return null;
            }
        }
        #endregion
    }
}
