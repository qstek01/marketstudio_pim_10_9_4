﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Http;
using log4net;
using LS.Data;
using LS.Data.Model.CatalogSectionModels;
using LS.Data.Model.Export;
using LS.Data.Model.ProductPreview;
using LS.Data.Model.ProductView;
using System.Reflection;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using LS.Data.Utilities;
using System.Collections;
using System.Web;
using System.Xml.Serialization;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json.Linq;
using LS.Data.Model;
using System.Xml.Linq;
using System.Data.Entity.Core.Objects;
using System.Security.Cryptography;
using LS.Web.Utility;
using Kendo.DynamicLinq;
using Stimulsoft.Report;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Stimulsoft.Report.Export;
using System.Web.Mvc.Html;
using System.Web.UI.WebControls;
using WebGrease.Css.Extensions;
using Missing = System.Reflection.Missing;
using LS.Data.Model.Reports;
using LS.Web.Models;
using System.Transactions;
using System.Data.Entity.Validation;
using Microsoft.Office.Interop.Excel;
using DataTable = System.Data.DataTable;
using Workbook = Infragistics.Documents.Excel.Workbook;

namespace LS.Web.Controllers
{
    public class ExportApiController : ApiController
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(HomeApiController));
        private readonly CSEntities _dbcontext = new CSEntities();
        HomeApiController homeObj = new HomeApiController();
        SqlConnection sqlConnection = new SqlConnection();
         SqlCommand sqlCommand = new SqlCommand();
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();

        public void batchExport(int catalogid, string attrBy, string filter, string familyFilter, string catalogname, string categoryId, int familyId,
     JArray model, string projectName, string selcategory_id, string selfamily_idd, string outputformat, string Delimiters, string fromdate, string todate, string exportids, string hierarchy, string fileName, string scheduleDateTime, bool EnableSubProduct, string ftp, string url, string username, string passwordencrpt, string exporttype)
        {
            try
            {
                string password = null;
                bool is_ftp;
                var customerName = User.Identity.Name;
                if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
                {

                    System.Web.HttpContext.Current.Session["CustomerItemNo"] = homeObj.getCatalogItemNumberValues();
                }
                // CustomerSubItemNo
                if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
                {
                    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = homeObj.getCatalogSubItemNumberValues();
                }

                if (ftp == "1")
                {
                    is_ftp = true;
                    password = DecryptStringAES(passwordencrpt);
                }
                else
                {
                    is_ftp = false;
                    url = "";
                    username = "";
                    password = "";
                }


                //Get Enable SubProduct
            

                DataTable ExportResult = new DataTable();

                var exportBatchId = Guid.NewGuid().ToString();
                //string SqlQuery = "EXEC(STP_LS_EXPORTBATCHPROCESS 0,'" + exportBatchId + "','" + catalogid + "','" + catalogname + "','" 
                //    + attrBy + "','" + filter + "','" + familyFilter + "','" + categoryId + "','" + familyId + "','" + projectName + "','" + selcategory_id + "','" + selfamily_idd + "','" 
                //    + outputformat + "','" + Delimiters + "','" + fromdate + "','" + todate + "','" + exportids + "','" + hierarchy + "','" + fileName + "','" + scheduleDateTime + "','INSERT','')"; /// ,'" + model + "'
               
                // To insert the all value into EXPORTBATCHPROCESS table    - 1 
                
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = con.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_EXPORTBATCHPROCESS";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Connection = con;
                    objSqlCommand.Parameters.Add("@EXPORTID", SqlDbType.Int).Value = 0;
                    objSqlCommand.Parameters.Add("@BATCH_ID", SqlDbType.NVarChar, 200).Value = exportBatchId;
                    objSqlCommand.Parameters.Add("@CATALOGID", SqlDbType.Int).Value = catalogid;
                    objSqlCommand.Parameters.Add("@CATALOGNAME", SqlDbType.NVarChar, 100).Value = catalogname == null ? "" : catalogname;
                    objSqlCommand.Parameters.Add("@ATTRIBUTEBY", SqlDbType.NVarChar, 50).Value = attrBy;
                    objSqlCommand.Parameters.Add("@FILTER", SqlDbType.NVarChar,50).Value = filter;
                    objSqlCommand.Parameters.Add("@FAMILYFILTER", SqlDbType.NVarChar, 50).Value = familyFilter;
                    objSqlCommand.Parameters.Add("@FAMILYID", SqlDbType.Int).Value = familyId;
                    objSqlCommand.Parameters.Add("@PROJECTNAME", SqlDbType.NVarChar,100).Value = projectName;
                    objSqlCommand.Parameters.Add("@CATEGORYIDLIST", SqlDbType.NVarChar).Value = selcategory_id;
                    objSqlCommand.Parameters.Add("@FAMILYIDLIST", SqlDbType.NVarChar).Value = selfamily_idd;
                    objSqlCommand.Parameters.Add("@EXPORT_OUTPUT", SqlDbType.NVarChar, 10).Value = outputformat;
                    objSqlCommand.Parameters.Add("@MODEL", SqlDbType.NVarChar).Value = "";
                    objSqlCommand.Parameters.Add("@DELIMITER", SqlDbType.NVarChar, 10).Value = Delimiters == null ? "" : Delimiters;
                    objSqlCommand.Parameters.Add("@FROMDATE ", SqlDbType.NVarChar, 30).Value = fromdate == null ? "" : fromdate;
                    objSqlCommand.Parameters.Add("@TODATE", SqlDbType.NVarChar, 30).Value = todate == null ? "" : todate;
                    objSqlCommand.Parameters.Add("@EXPORTIDS", SqlDbType.NVarChar).Value = exportids;
                    objSqlCommand.Parameters.Add("@FILENAME", SqlDbType.NVarChar, 255).Value = fileName.ToString();
                    objSqlCommand.Parameters.Add("@SCEDULETIME",SqlDbType.NVarChar,100).Value = scheduleDateTime;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar,30).Value = "INSERT";
                    objSqlCommand.Parameters.Add("@STATUS", SqlDbType.NVarChar, 10).Value = 0;
                    objSqlCommand.Parameters.Add("@ENNABLESUBPRODUCTS", SqlDbType.Bit).Value = EnableSubProduct;
                    objSqlCommand.Parameters.Add("@HIERARCHY",SqlDbType.NVarChar,20).Value = hierarchy;
                    objSqlCommand.Parameters.Add("@CUSTOMER_NAME", SqlDbType.NVarChar, 50).Value = customerName;
                    objSqlCommand.Parameters.Add("@CUSTOMERITEMNO", SqlDbType.NVarChar, 50).Value = System.Web.HttpContext.Current.Session["CustomerItemNo"];
                    objSqlCommand.Parameters.Add("@CUSTOMERSUBITEMNO", SqlDbType.NVarChar, 50).Value = System.Web.HttpContext.Current.Session["CustomerSubItemNo"];
                    objSqlCommand.Parameters.Add("@IS_FTP", SqlDbType.Bit).Value = is_ftp;
                    objSqlCommand.Parameters.Add("@FTP_URL", SqlDbType.NVarChar, 200).Value = url;
                    objSqlCommand.Parameters.Add("@FTP_USERNAME", SqlDbType.NVarChar, 200).Value = username;
                    objSqlCommand.Parameters.Add("@FTP_PASSWORD", SqlDbType.NVarChar, 200).Value = password;
                    var ExportDetails = new SqlDataAdapter(objSqlCommand);
                    ExportDetails.Fill(ExportResult);
                     
                }

                if(ExportResult.Rows.Count > 0)
                {
                    var ExportId = ExportResult.Rows[0]["EXPORTID"];
                   // = ExportResult.AsEnumerable().AsQueryable().ToList().Select(x => x).ToList();  
            


                // To insert model values into corresponding table.======================================================


                   //Model [0] Insert into table - Start
                 #region model[0]
                    var modelvalue = (model[0]).Select(x => x).ToList();
                    #endregion
                //  Model [0] Insert into table - Start


                  //Model [1] Insert into table - Start
                 #region MODEL[1]

                var functionAlloweditem = (model[1]).Select(p => new EXPORTBATCHALLOWEDITEM()
                {
                    ATTRIBUTE_ID = (string)p["ATTRIBUTE_ID"],
                    ATTRIBUTE_NAME = (string)p["ATTRIBUTE_NAME"],
                    DISPLAY_NAME = (string)p["DISPLAY_NAME"],
                    ATTRIBUTE_TYPE = (string)p["ATTRIBUTE_TYPE"],
                    PUBLISH2CDROM = (bool)p["PUBLISH2CDROM"],
                    PUBLISH2ODP = (bool)p["PUBLISH2ODP"],
                    PUBLISH2PRINT = (bool)p["PUBLISH2PRINT"],
                    PUBLISH2WEB = (bool)p["PUBLISH2WEB"],
                    ISAvailable = (bool)p["ISAvailable"]
                }).ToList();
                foreach (EXPORTBATCHALLOWEDITEM obj in functionAlloweditem )
                {
                    using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                       {

                           string SqlString = "Insert into EXPORTBATCHALLOWEDITEM(ATTRIBUTE_ID,ATTRIBUTE_NAME,DISPLAY_NAME,ATTRIBUTE_TYPE,PUBLISH2CDROM,PUBLISH2ODP,PUBLISH2PRINT,PUBLISH2WEB,ISAvailable,EXPORTID) VALUES  ('" + obj.ATTRIBUTE_ID + "','" + obj.ATTRIBUTE_NAME + "','" + obj.DISPLAY_NAME + "','" + obj.ATTRIBUTE_TYPE + "','" + obj.PUBLISH2CDROM + "','" + obj.PUBLISH2ODP + "','" + obj.PUBLISH2PRINT + "','" + obj.PUBLISH2WEB + "','" + obj.ISAvailable + "','" + ExportId + "')";
                           var cmd = new SqlCommand(SqlString, conn);
                           conn.Open();
                           cmd.ExecuteNonQuery();
                           conn.Close();
                      }
                }
                 #endregion
                   //Model [1] Insert into table - End


                //Model [2] Insert into table - Start
                #region Model[2]
                var selectedAttributes = (model[2]).Select(x => x).ToList();
                #endregion
                //Model [2] Insert into table - end


                //Model [3] Insert into table - Start
                #region Model [3]
                string selfamily_id = model[3].ToString().Replace("[", "").Replace("]", "");
                if (selfamily_id.Length != 0)
                {
                    using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        string SqlString = "Insert into EXPORTBATCHSELFAMILYID(EXPORTID,FAMILYID) VALUES ('" + ExportId + "','" + selfamily_id + "')";
                        var cmd = new SqlCommand(SqlString, conn);
                        conn.Open();
                        cmd.ExecuteNonQuery();
                         conn.Close();
                    }
                }
                #endregion

                //Model [3] Insert into table - End


                //Model [4] Insert into table - Start

                #region Model[4]

                JavaScriptSerializer js = new JavaScriptSerializer();
                QS_CATALOGATTRIBUTE_Result[] dislayattributes = js.Deserialize<QS_CATALOGATTRIBUTE_Result[]>(model[4].ToString());
                foreach (var obj in dislayattributes)
                {
                    using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        string SqlString = "Insert into EXPORTBATCHDISPLAYATTRIBUTES(ATTRIBUTE_ID,ATTRIBUTE_NAME,DISPLAY_NAME,ATTRIBUTE_TYPE,PUBLISH2CDROM,PUBLISH2ODP,PUBLISH2PRINT,PUBLISH2WEB,ISAvailable,EXPORTID) VALUES  ('" + obj.ATTRIBUTE_ID + "','" + obj.ATTRIBUTE_NAME + "','" + obj.DISPLAY_NAME + "','" + obj.ATTRIBUTE_TYPE + "','" + obj.PUBLISH2CDROM + "','" + obj.PUBLISH2ODP + "','" + obj.PUBLISH2PRINT + "','" + obj.PUBLISH2WEB + "','" + obj.ISAvailable + "','" + ExportId + "')";
                        var cmd = new SqlCommand(SqlString, conn);
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
                #endregion
                    //Model [4] Insert into table - End

                }
            }
            catch (Exception ex)
            {
                
               
            }

        }

        [System.Web.Http.HttpGet]
        public IList GetBatchExportLogList()
        {
            try
            {
                DataTable ExportBatchlog = new DataTable();

                using (sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    sqlConnection.Open();
                    SqlCommand objSqlCommand = new SqlCommand();
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = sqlConnection;
                    objSqlCommand.CommandText = "STP_LS_EXPORTBATCHPROCESS";
                    objSqlCommand.Parameters.Add("@CUSTOMER_NAME", SqlDbType.NVarChar, 50).Value = User.Identity.Name;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar, 30).Value = "SELECTBATCHLOG";
                    objSqlCommand.CommandTimeout = 0;

                    sqlDataAdapter = new SqlDataAdapter(objSqlCommand);

                    sqlDataAdapter.Fill(ExportBatchlog);

                    var listLog = ConvertDataTable(ExportBatchlog).ToList();


                    return listLog;
                }  
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ExportApiController : GetBatchExportLogList", objexception);
                return null;
               
            }
      
        }

        public class ExportBatch
        {
            public int EXPORTID { get; set; }
            public string EXPORT_NAME { get; set; }
            public string PROJECT_NAME { get; set; }
            public string STATUS { get; set; }
            public string SCEDULETIME { get; set; }
            public string CUSTOMER_NAME { get; set; }
            public bool ISAvailable { get; set; }
            public string BATCH_ID { get; set; }
        }


        public List<ExportBatch> ConvertDataTable (DataTable ExportBatchlog)
        {

            List<ExportBatch> data = new List<ExportBatch>();

            try
            {
                if (ExportBatchlog.Rows.Count > 0)
                {

                    foreach (DataRow row in ExportBatchlog.Rows)
                    {
                        var ExportBatch = new ExportBatch();
                        ExportBatch.EXPORT_NAME = row.ItemArray[0].ToString();
                        ExportBatch.PROJECT_NAME = row.ItemArray[1].ToString();
                        ExportBatch.STATUS = row.ItemArray[4].ToString();
                        ExportBatch.SCEDULETIME = row.ItemArray[2].ToString();
                        ExportBatch.CUSTOMER_NAME = row.ItemArray[3].ToString();
                        ExportBatch.ISAvailable = bool.Parse(row.ItemArray[5].ToString());
                        ExportBatch.EXPORTID = int.Parse(row.ItemArray[6].ToString());
                        ExportBatch.BATCH_ID = row.ItemArray[7].ToString();
                        data.Add(ExportBatch);

                        //ExportBatch.EXPORTID = row.ItemArray[0];EXPORTID
                        //ExportBatch.BATCH_ID = 
                    }
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at ExportApiController : ConvertDataTable", objexception);
                return null;
               
            }



            return data;
        }

        [System.Web.Http.HttpGet]
        public string RunExportProcess(string BATCH_ID)
        {
            try
            {
                var ExportPath = ConfigurationManager.AppSettings["ExportBatchProcess"].ToString();
                var customers = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).ToList();
                string customerId = string.Empty;
                if (customers.Count > 0)
                {
                    customerId = customers[0].ToString();
                }
                string fileName = Path.GetFileName(ExportPath);
                Process[] runningProcess = Process.GetProcessesByName(fileName.Substring(0, fileName.LastIndexOf('.')));

                if (runningProcess.Length > 0)
                {
                    return "Export is already running.Please wait for few minutes";
                }
                else
                {
 
                     Process proc = new Process();

                     proc.StartInfo.Arguments = string.Format("{0} {1} {2}", BATCH_ID, customerId, "true");
                     proc.StartInfo.FileName = ExportPath;
                     proc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                     proc.StartInfo.CreateNoWindow = true;
                     proc.Start();


                    //To update the status pending into inprogress.


                    using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        string updateQuery = "update EXPORTBATCHPROCESS set STATUS = 'Inprogress' where BATCH_ID = '" + BATCH_ID + "'";
                       var cmd = new SqlCommand(updateQuery,con);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }


                    return "Export started";
                }
            }
            catch (Exception ex)
            {

                return "Error";
            }
 
        }

          [System.Web.Http.HttpGet]
        public string DeleteExportBatchById(string EXPORTID)
        {
            try
            {
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    //string SqlString = "delete from EXPORTBATCHPROCESS where BATCH_ID = '" + BATCH_ID + "'";
                    //var cmd = new SqlCommand(SqlString, conn);
                    //conn.Open();
                    //cmd.ExecuteNonQuery();
                    //conn.Close();


                    SqlCommand objSqlCommand = new SqlCommand();
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = conn;
                    objSqlCommand.CommandText = "STP_LS_EXPORTBATCHPROCESS";
                    objSqlCommand.Parameters.Add("EXPORTID", SqlDbType.Int).Value = int.Parse(EXPORTID);
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar, 30).Value = "DELETEATCHLOG";

                    conn.Open();
                    objSqlCommand.ExecuteNonQuery();
                    conn.Close();


                }
                return "DeletedSuccessfully";
            }
            catch (Exception )
            {
                return "DeletedSuccessfully";
                throw;
            }

        }
         [System.Web.Http.HttpPost]
        public string ClearLogExport(List<string> BathchValues)
          {
              try
              {
                  for (int i = 0; i < BathchValues.Count; i++)
                  {
                      using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                      {

                          SqlCommand objSqlCommand = new SqlCommand();
                          objSqlCommand.CommandType = CommandType.StoredProcedure;
                          objSqlCommand.Connection = conn;
                          objSqlCommand.CommandText = "STP_LS_EXPORTBATCHPROCESS";
                          objSqlCommand.Parameters.Add("EXPORTID", SqlDbType.Int).Value = int.Parse(BathchValues[i]);
                          objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar, 30).Value = "DELETEATCHLOG";

                          conn.Open();
                          objSqlCommand.ExecuteNonQuery();
                          conn.Close();


                      }
                  }
                  return "DeletedSuccessfully";
              }
              catch (Exception)
              {
                  return "Deleted Not Successfully";
                  throw;
              }

          }

        //----------

         [System.Web.Http.HttpGet]
         public string UpdateProjectForBatch(string BatchProjectId, string ScheduleProjectName)
         {
             try
             {
                 
                 using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                 {

                     string updateProjectName = " update TB_PROJECT set PROJECT_NAME = '" + ScheduleProjectName + "' where PROJECT_ID = '" + int.Parse(BatchProjectId) + "'";

                     var cmd = new SqlCommand(updateProjectName, conn);
                     conn.Open();
                     cmd.ExecuteNonQuery();
                     conn.Close();
                 }
               
                 return "DeletedSuccessfully";
             }
             catch (Exception)
             {
                 return "AlreadyExist";
                
             }

         }
        //--------------


        #region FTP Export

         //--------------new-------
         //download the excel sheet in the content folder server format .xls
         [System.Web.Http.HttpPost]
         public void DownloadExportSheet(string FileName, bool EnableSubProduct, string Url, string Username, string Passwordencrpt, string templateName)
         {


             if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
             {

                 System.Web.HttpContext.Current.Session["CustomerItemNo"] = homeObj.getCatalogItemNumberValues();
             }

             if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
             {
                 System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = homeObj.getCatalogSubItemNumberValues();
             }

             string filename = FileName;
             var newmappingname = HttpContext.Current.Request.QueryString["Passwordencrpt"];
             var Password = DecryptStringAES(newmappingname);


             if (System.Web.HttpContext.Current.Session["ExportTable"] != null && filename.ToLower().Contains(".xls"))
             {

                 var workbook = new Workbook();
                 if (System.Web.HttpContext.Current.Session["ExportTableFAM"] != null)
                 {
                     var distinctdsFam = (DataTable)System.Web.HttpContext.Current.Session["ExportTableFAM"];

                     if (!distinctdsFam.Columns.Contains("Action"))
                     {
                         distinctdsFam.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                     }


                     var tableFam = distinctdsFam.DefaultView.ToTable(true);

                     System.Web.HttpContext.Current.Session["ExportTableFAM"] = null;


                     if (tableFam.Rows.Count > 0)
                     {
                         const string tableName1 = "Families";
                         string tableName = "Families";

                         int rowcntTemp = tableFam.Rows.Count;
                         int j = 0;
                         int runningcnt = 0;
                         do
                         {
                             int rowcnt;
                             if (rowcntTemp <= 65000)
                             {
                                 rowcnt = rowcntTemp;
                                 rowcntTemp = rowcnt - 65000;
                             }
                             else
                             {
                                 rowcnt = 65000;
                                 rowcntTemp = tableFam.Rows.Count - (runningcnt + 65000);
                             }
                             j++;
                             if (j != 1)
                             {
                                 tableName = tableName1 + (j - 1);
                             }
                             var worksheet = workbook.Worksheets.Add(tableName);
                             workbook.WindowOptions.SelectedWorksheet = worksheet;
                             for (int jj = 1; jj <= rowcnt; jj++)


                                 for (int columnIndex = 0; columnIndex < tableFam.Columns.Count; columnIndex++)
                                 {

                                     if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "ITEM#")
                                     {
                                         worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                     }
                                     else if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                     {
                                         worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                     }
                                     else if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                     {
                                         worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName);
                                     }
                                 }
                             int rowIndex = 2;
                             int temprunningcnt = runningcnt;
                             for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                             {
                                 var row = worksheet.Rows[rowIndex++];
                                 runningcnt++;
                                 for (int columnIndex = 0; columnIndex < tableFam.Rows[k].ItemArray.Length; columnIndex++)
                                 {
                                     row.Cells[columnIndex].Value = tableFam.Rows[k].ItemArray[columnIndex];
                                 }
                             }

                         } while (rowcntTemp > 0);


                     }

                 }
                 var distinctds = (DataTable)System.Web.HttpContext.Current.Session["ExportTable"];


                 if (!distinctds.Columns.Contains("Action"))
                 {
                     distinctds.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                 }


                 var table = distinctds.DefaultView.ToTable(true);

                 System.Web.HttpContext.Current.Session["ExportTable"] = null;

                 if (table.Rows.Count > 0)
                 {
                     const string tableName1 = "Products";
                     string tableName = "Products";

                     int rowcntTemp = table.Rows.Count;
                     int j = 0;
                     int runningcnt = 0;
                     do
                     {
                         int rowcnt;
                         if (rowcntTemp <= 65000)
                         {
                             rowcnt = rowcntTemp;
                             rowcntTemp = rowcnt - 65000;
                         }
                         else
                         {
                             rowcnt = 65000;
                             rowcntTemp = table.Rows.Count - (runningcnt + 65000);
                         }
                         j++;
                         if (j != 1)
                         {
                             tableName = tableName1 + (j - 1);
                         }
                         var worksheet = workbook.Worksheets.Add(tableName);
                         workbook.WindowOptions.SelectedWorksheet = worksheet;
                         for (int jj = 1; jj <= rowcnt; jj++)


                             for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                             {
                                 if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "ITEM#")
                                 {
                                     worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                 }
                                 else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                 {
                                     worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                 }
                                 else if (HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                 {
                                     worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                                 }
                             }
                         int rowIndex = 2;
                         int temprunningcnt = runningcnt;
                         for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                         {
                             var row = worksheet.Rows[rowIndex++];
                             runningcnt++;
                             for (int columnIndex = 0; columnIndex < table.Rows[k].ItemArray.Length; columnIndex++)
                             {
                                 row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex];
                             }
                         }

                     } while (rowcntTemp > 0);


                 }
                 else
                 {
                     var worksheet = workbook.Worksheets.Add("Product");
                     workbook.WindowOptions.SelectedWorksheet = worksheet;
                     for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                     {
                         worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                     }


                 }
                 if (EnableSubProduct == true)
                 {
                     if (System.Web.HttpContext.Current.Session["ExportTableSubProduct"] != null)
                     {
                         var tableSP = (DataTable)System.Web.HttpContext.Current.Session["ExportTableSubProduct"];
                         if (!tableSP.Columns.Contains("Action"))
                         {
                             tableSP.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                         }
                         string tableNameSp = "SubProducts";
                         const string tableNameSp1 = "SubProducts";
                         int rowcntTempSP = tableSP.Rows.Count;
                         int jSP = 0;
                         int runningcntSP = 0;
                         do
                         {
                             int rowcntSP;
                             if (rowcntTempSP <= 65000)
                             {
                                 rowcntSP = rowcntTempSP;
                                 rowcntTempSP = rowcntSP - 65000;
                             }
                             else
                             {
                                 rowcntSP = 65000;
                                 rowcntTempSP = tableSP.Rows.Count - (runningcntSP + 65000);
                             }
                             jSP++;

                             if (jSP != 1)
                             {
                                 tableNameSp = tableNameSp1 + (jSP - 1);
                             }
                             var worksheetSp = workbook.Worksheets.Add(tableNameSp);
                             workbook.WindowOptions.SelectedWorksheet = worksheetSp;
                             for (int jj = 1; jj <= rowcntSP; jj++)


                                 for (int columnIndex = 0; columnIndex < tableSP.Columns.Count; columnIndex++)
                                 {
                                     if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "ITEM#")
                                     {
                                         worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                     }
                                     else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                     {
                                         worksheetSp.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                     }
                                     else if (HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                     {


                                         worksheetSp.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableSP.Columns[columnIndex].ColumnName);

                                     }
                                 }



                             int rowIndex = 2;
                             int temprunningcntSP = runningcntSP;
                             for (int k = runningcntSP; k < (temprunningcntSP + rowcntSP); k++)
                             {
                                 var row = worksheetSp.Rows[rowIndex++];
                                 runningcntSP++;
                                 for (int columnIndex = 0; columnIndex < tableSP.Rows[k].ItemArray.Length; columnIndex++)
                                 {
                                     row.Cells[columnIndex].Value = tableSP.Rows[k].ItemArray[columnIndex];
                                 }
                             }
                         } while (rowcntTempSP > 0);

                     }
                 }
                 if (workbook.Worksheets[0] != null)
                 {
                     var worksheet1 = workbook.Worksheets[0];
                     workbook.WindowOptions.SelectedWorksheet = worksheet1;
                 }

                 string localpath = HttpContext.Current.Server.MapPath("~/Content/" + filename);
                 workbook.Save(@localpath);
                 FTPFileExsist(Url, Username, Password, FileName, templateName);


             }
             else if (filename.ToLower().Contains(".txt") || filename.ToLower().Contains(".csv") || filename.ToLower().Contains(".xml"))
             {
                 string subproductcheck = string.Empty;
                 string localpath = HttpContext.Current.Server.MapPath("~/Content/" + filename);

                 FTPFileExsist(Url, Username, Password, FileName, templateName);




             }



         }

         /// <summary>
         /// Check the FTP file is exsists or not  
         /// </summary>
         /// <author>kowshikkumar</author>
         /// <param name="ftpURL">Use to pass ftp url</param>
         /// <param name="ftpUserName"> pass the FTP user name </param>
         /// <param name="ftp password"></param>
         /// <param name="file name">Pass file name *** file name should not contains Special char *** </param>
         /// <returns></returns>

         public bool FTPFileExsist(string url, string username, string password, string filename, string templateName)
         {
             string oldfilename = string.Empty;
             try
             {
                 string localpath = HttpContext.Current.Server.MapPath("~/Content/" + filename);
                 int inc = 1;
                 string[] filenameSplit = filename.Split('.');
                 string newfilename = string.Empty;
                 oldfilename = templateName + "." + filenameSplit[1];
                 url = url + "//" + templateName + "." + filenameSplit[1];
                 try
                 {
                     do
                     {
                         FtpWebRequest req = (FtpWebRequest)FtpWebRequest.Create(url);
                         req.Proxy = null;
                         req.Method = WebRequestMethods.Ftp.UploadFile;
                         req.Credentials = new NetworkCredential(username, password);
                         req.Method = WebRequestMethods.Ftp.GetFileSize;
                         req.UseBinary = true;
                         req.UsePassive = true;
                         FtpWebResponse res = (FtpWebResponse)req.GetResponse();
                         newfilename = filenameSplit[0] + "(" + inc + ")" + "." + filenameSplit[1];
                         inc = inc + 1;
                         url = url.Replace(filename, newfilename);
                         filename = newfilename;
                     } while (filename != null);
                     return true;
                 }
                 catch (Exception Ex)
                 {
                     UploadToFTP(url, username, password, filename, oldfilename);
                     return true;
                 }



             }

             catch (Exception objexception)
             {
                 Logger.Error("Error at FTPFileExsist", objexception);
                 return false;
             }

         }


         public bool UploadToFTP(string url, string username, string password, string filename, string oldfilename)
         {

             try
             {
                 string localpath = HttpContext.Current.Server.MapPath("~/Content/" + oldfilename);
                 FtpWebRequest req = (FtpWebRequest)FtpWebRequest.Create(url);
                 req.Proxy = null;
                 req.Method = WebRequestMethods.Ftp.UploadFile;
                 req.Credentials = new NetworkCredential(username, password);
                 req.UseBinary = true;
                 req.UsePassive = true;
                 byte[] data = File.ReadAllBytes(localpath);
                 req.ContentLength = data.Length;
                 Stream stream = req.GetRequestStream();
                 stream.Write(data, 0, data.Length);
                 stream.Close();
                 FtpWebResponse res = (FtpWebResponse)req.GetResponse();
                 return true;
             }

             catch (Exception objexception)
             {
                 Logger.Error("Error at FTPFileExsist", objexception);
                 return false;
             }

         }
         //decrypt the password 
         public string DecryptStringAES(string cipherText)
         {
             try
             {
                 var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
                 var iv = Encoding.UTF8.GetBytes("8080808080808080");

                 var encrypted = Convert.FromBase64String(cipherText.Replace(" ", "+"));
                 var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
                 return string.Format(decriptedFromJavascript);
             }
             catch (Exception ex)
             {

                 return null;
             }
         }
         //decrypt the password 
         private string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
         {

             if (cipherText == null || cipherText.Length <= 0)
             {
                 throw new ArgumentNullException("cipherText");
             }
             if (key == null || key.Length <= 0)
             {
                 throw new ArgumentNullException("key");
             }
             if (iv == null || iv.Length <= 0)
             {
                 throw new ArgumentNullException("key");
             }
             string plaintext = null;
             using (var rijAlg = new RijndaelManaged())
             {

                 rijAlg.Mode = CipherMode.CBC;
                 rijAlg.Padding = PaddingMode.PKCS7;
                 rijAlg.FeedbackSize = 128;

                 rijAlg.Key = key;
                 rijAlg.IV = iv;


                 var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                 try
                 {

                     using (var msDecrypt = new MemoryStream(cipherText))
                     {
                         using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                         {

                             using (var srDecrypt = new StreamReader(csDecrypt))
                             {
                                 plaintext = srDecrypt.ReadToEnd();

                             }

                         }
                     }
                 }
                 catch
                 {
                     plaintext = "keyError";
                 }
             }

             return plaintext;
         }


        #endregion

         public bool TableDesignerFamilyExportDownload(DataTable tableDesignerFamilyExportTable, string familyId)
         {
             try
             {
                 int integerFamilyId = int.Parse(familyId);
                 string filename = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID == integerFamilyId).Select(x => x.FAMILY_NAME).FirstOrDefault().ToString();
                 filename = filename + ".xls";
                 if (tableDesignerFamilyExportTable.Columns.Contains("FAMILY_ID_1"))
                 {
                     tableDesignerFamilyExportTable.Columns.Remove("FAMILY_ID_1");
                 }
                 if (tableDesignerFamilyExportTable.Columns.Contains("ff"))
                 {
                     tableDesignerFamilyExportTable.Columns.Remove("ff");
                 }
                 tableDesignerFamilyExportTable.Columns.Add("ACTION").SetOrdinal(0);
                 tableDesignerFamilyExportTable.AcceptChanges();
                 System.Web.HttpContext.Current.Session["tableDesignerFamilyName"] = filename;
                 System.Web.HttpContext.Current.Session["tableDesignerFamilyExportTable"] = tableDesignerFamilyExportTable;
                 var workbook = new Workbook();
                 if (tableDesignerFamilyExportTable.Rows.Count > 0)
                 {

                     string tableName = "Tabledesigner";
                     int rowcntTemp = tableDesignerFamilyExportTable.Rows.Count;
                     int j = 0;
                     int runningcnt = 0;
                     do
                     {
                         int rowcnt;
                         if (rowcntTemp <= 65000)
                         {
                             rowcnt = rowcntTemp;
                             rowcntTemp = rowcnt - 65000;
                         }
                         else
                         {
                             rowcnt = 65000;
                             rowcntTemp = tableDesignerFamilyExportTable.Rows.Count - (runningcnt + 65000);
                         }
                         j++;
                         if (j != 1)
                         {
                             tableName = tableName + (j - 1);
                         }
                         var worksheet = workbook.Worksheets.Add(tableName);
                         workbook.WindowOptions.SelectedWorksheet = worksheet;
                         for (int columnIndex = 0; columnIndex < tableDesignerFamilyExportTable.Columns.Count; columnIndex++)
                         {

                             if (HttpUtility.HtmlDecode(tableDesignerFamilyExportTable.Columns[columnIndex].ColumnName) != "ID" && HttpUtility.HtmlDecode(tableDesignerFamilyExportTable.Columns[columnIndex].ColumnName) != "ROLE_ID" && HttpUtility.HtmlDecode(tableDesignerFamilyExportTable.Columns[columnIndex].ColumnName) != "CATALOGID")
                             {


                                 worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableDesignerFamilyExportTable.Columns[columnIndex].ColumnName);

                             }
                         }
                         int rowIndex = 2;
                         int temprunningcnt = runningcnt;
                         for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                         {
                             var row = worksheet.Rows[rowIndex++];
                             runningcnt++;
                             for (int columnIndex = 0; columnIndex < tableDesignerFamilyExportTable.Rows[k].ItemArray.Length; columnIndex++)
                             {
                                 row.Cells[columnIndex].Value = tableDesignerFamilyExportTable.Rows[k].ItemArray[columnIndex];
                             }
                         }

                     }
                     while (rowcntTemp > 0);
                 }
                 else
                 {
                     return false;
                 }
                 string filepath = HttpContext.Current.Server.MapPath("~/Content/" + filename);
                 if (File.Exists(filepath))
                 {
                     File.Delete(filepath);
                 }
                 workbook.Save(filepath);
                 return true;
             }
             catch (Exception ex)
             {
                 Logger.Error("Error at TableDesignerFamilyExportDownload ", ex);
                 return false;

             }
         }

    }
}
