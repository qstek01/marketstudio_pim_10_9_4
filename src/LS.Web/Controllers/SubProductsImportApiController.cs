﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using LS.Data;
using LS.Data.Model.CatalogSectionModels;
using log4net;
using LS.Data.Model;
using System.Web.Http;
using System.Collections;
using LS.Data.Model.CommonValue;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Data.OleDb;
using System.Web.Mvc;
using System.Globalization;
using LS.Data.Model.ProductView;
using Infragistics.Documents.Excel;
using System.Web;
using System.Xml.Linq;
using Microsoft.Ajax.Utilities;
using System.Web.Script.Serialization;
using System.Xml;

namespace LS.Web.Controllers
{
    public class SubProductsImportApiController : ApiController
    {
        DataTable _dtable;
        DataTable _objdatatable;
        static readonly ILog Logger = LogManager.GetLogger(typeof(SubProductsImportApiController));
        readonly CSEntities _dbcontext = new CSEntities();
        readonly DataSet _dsSheets = new DataSet();
        string _message;
        public CSEntities objLS = new CSEntities();
        readonly string _connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        //static ILog _logger = LogManager.GetLogger(typeof(FamilyApiController));
        readonly ImportController _ic = new ImportController();
        // GET: ImportApi
        public enum CSF
        {
            [Description("CATALOG_ID")]
            CATALOG_ID = 1,
            [Description("CATALOG_NAME")]
            CATALOG_NAME = 2,
            [Description("CATALOG_VERSION")]
            CATALOG_VERSION = 3,
            [Description("CATALOG_DESCRIPTION")]
            CATALOG_DESCRIPTION = 4,



            [Description("CATEGORY_ID")]
            CATEGORY_ID = 101,
            [Description("CATEGORY_NAME")]
            CATEGORY_NAME = 102,
            [Description("SUBCATID_L1")]
            SUBCATID_L1 = 103,
            [Description("SUBCATNAME_L1")]
            SUBCATNAME_L1 = 104,
            [Description("SUBCATID_L2")]
            SUBCATID_L2 = 105,
            [Description("SUBCATNAME_L2")]
            SUBCATNAME_L2 = 106,
            [Description("SUBCATID_L3")]
            SUBCATID_L3 = 107,
            [Description("SUBCATNAME_L3")]
            SUBCATNAME_L3 = 108,
            [Description("SUBCATID_L4")]
            SUBCATID_L4 = 109,
            [Description("SUBCATNAME_L4")]
            SUBCATNAME_L4 = 110,
            [Description("SUBCATID_L5")]
            SUBCATID_L5 = 111,
            [Description("SUBCATNAME_L5")]
            SUBCATNAME_L5 = 112,
            [Description("SUBCATID_L6")]
            SUBCATID_L6 = 113,
            [Description("SUBCATNAME_L6")]
            SUBCATNAME_L6 = 114,
            [Description("SUBCATID_L7")]
            SUBCATID_L7 = 115,
            [Description("SUBCATNAME_L7")]
            SUBCATNAME_L7 = 116,
            [Description("SUBCATID_L8")]
            SUBCATID_L8 = 117,
            [Description("SUBCATNAME_L8")]
            SUBCATNAME_L8 = 118,
            [Description("SUBCATID_L9")]
            SUBCATID_L9 = 119,
            [Description("SUBCATNAME_L9")]
            SUBCATNAME_L9 = 120,
            [Description("CATEGORY_SHORT_DESC")]
            CATEGORY_SHORT_DESC = 151,
            [Description("CATEGORY_IMAGE_FILE")]
            CATEGORY_IMAGE_FILE = 152,
            [Description("CATEGORY_IMAGE_NAME")]
            CATEGORY_IMAGE_NAME = 153,
            [Description("CATEGORY_IMAGE_TYPE")]
            CATEGORY_IMAGE_TYPE = 154,
            [Description("CATEGORY_IMAGE_FILE2")]
            CATEGORY_IMAGE_FILE2 = 155,
            [Description("CATEGORY_IMAGE_NAME2")]
            CATEGORY_IMAGE_NAME2 = 156,
            [Description("CATEGORY_IMAGE_TYPE2")]
            CATEGORY_IMAGE_TYPE2 = 157,
            [Description("CATEGORY_CUSTOM_NUM_FIELD1")]
            CATEGORY_CUSTOM_NUM_FIELD1 = 158,
            [Description("CATEGORY_CUSTOM_NUM_FIELD2")]
            CATEGORY_CUSTOM_NUM_FIELD2 = 159,
            [Description("CATEGORY_CUSTOM_NUM_FIELD3")]
            CATEGORY_CUSTOM_NUM_FIELD3 = 160,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD1")]
            CATEGORY_CUSTOM_TEXT_FIELD1 = 161,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD2")]
            CATEGORY_CUSTOM_TEXT_FIELD2 = 162,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD3")]
            CATEGORY_CUSTOM_TEXT_FIELD3 = 163,



            [Description("FAMILY_ID")]
            FAMILY_ID = 201,
            [Description("FAMILY_NAME")]
            FAMILY_NAME = 202,
            [Description("SUBFAMILY_ID")]
            SUBFAMILY_ID = 203,
            [Description("SUBFAMILY_NAME")]
            SUBFAMILY_NAME = 204,
            [Description("CATEGORY_ID")]
            FAMCATEGORY_ID = 205,
            [Description("FOOT_NOTES")]
            FOOT_NOTES = 206,
            [Description("STATUS")]
            STATUS = 207,
            [Description("FAMILY_SORT")]
            FAMILY_SORT = 208,

            [Description("PRODUCT_ID")]
            PRODUCT_ID = 301,
            [Description("CATALOG_ITEM_NO")]
            //[Description("CAT#")]
            CATALOG_ITEM_NO = 302,

            [Description("SUBPRODUCT_ID")]
            SUBPRODUCT_ID = 303,
            [Description("SUBCATALOG_ITEM_NO")]
            SUBCATALOG_ITEM_NO = 304,


            [Description("PRODUCT_SORT")]
            PRODUCT_SORT = 305,
            [Description("REMOVE_PRODUCT")]
            REMOVE_PRODUCT = 306,
            [Description("ATTRIBUTE_ID")]
            ATTRIBUTE_ID = 307,

        };
        // DataSet _dsSheets = new DataSet();


        [System.Web.Http.HttpGet]
        public IList importExcelSheetSelection(string excelPath)
        {
            List<ImportSheetName> newList = new List<ImportSheetName>();
            //  excelPath = "C:\\Users\\Rakesh\\Downloads\\LS Export.xls";
            //var myList = new List<ImportSheetName>();
            try
            {
                if (!string.IsNullOrEmpty(excelPath))
                {
                    // bool hasHeaders = true;
                    //  string HDR = hasHeaders ? "Yes" : "No";
                    // string strConn;
                    //if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                    //    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                    //              ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    //else
                    //    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                    //              ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";

                    Infragistics.Documents.Excel.Workbook workbook = Infragistics.Documents.Excel.Workbook.Load(excelPath);
                    // OleDbConnection conn = new OleDbConnection(strConn);
                    // conn.Open();
                    //  DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,null);
                    //int i = 0;
                    // string SheetName = string.Empty;
                    foreach (Worksheet rows in workbook.Worksheets)
                    {

                        //SheetName = Convert.ToString(rows[2]);
                        // if (!SheetName.EndsWith("_"))
                        // {
                        // if (SheetName.Contains("$"))
                        // {
                        //     SheetName = SheetName.Replace("$", "");
                        // }
                        // if (SheetName.Contains("'"))
                        //  {
                        //      SheetName = SheetName.Replace("'", "");
                        //  }

                        ImportSheetName objImportSheetName = new ImportSheetName();
                        objImportSheetName.TABLE_NAME = rows.Name;
                        objImportSheetName.SHEET_PATH = excelPath;
                        newList.Add(objImportSheetName); //sheetnames[i] = schemaTable.Rows[i];
                        //  i++;
                    }
                }
                // conn.Close();


            }

            catch (Exception objException)
            {
                Logger.Error("Error at loading importExcelSheetSelection in Import Controller", objException);
                return null;
            }
            return newList;
        }



        [System.Web.Http.HttpGet]
        public DataTable ImportSelectionColumnGrid(string sheetName, string excelPath)
        {
            var dt = new DataTable();
            try
            {
                if (!string.IsNullOrEmpty(excelPath))
                {
                    Workbook workbook = Workbook.Load(excelPath);
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    string strConn;
                    if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    else
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    // DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    // int sheetCount = schemaTable.Rows.Count;
                    //  int sheetColumnCount = 0;

                    if (!sheetName.Contains("$"))
                    {
                        sheetName = sheetName + "$";
                    }
                    dt = new DataTable(sheetName);
                    var dc = new DataColumn("ExcelColumn", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("CatalogField", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("SelectedToImport", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("IsSystemField", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("FieldType", typeof(string));
                    dt.Columns.Add(dc);
                    //DataRow schemaRow = schemaTable.Rows[0];
                    //string sheet = schemaRow["TABLE_NAME"].ToString();
                    //if (!sheet.EndsWith("_"))
                    //{
                    //    string query = "SELECT  * FROM [" + SheetName + "]";
                    //    OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                    //    dtexcel.Locale = CultureInfo.CurrentCulture;
                    //    daexcel.Fill(dtexcel);
                    //}
                    //sheetColumnCount = dtexcel.Columns.Count;

                    //int iCols = sheetColumnCount;
                    string sheetNameworkbook = sheetName.Replace("$", "").TrimEnd();
                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[0].Cells.Count();
                    for (int j = 0; j < iCols; j++)
                    {
                        if (workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[j].Value != null)
                        {
                            String sColName = workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[j].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                            DataRow dr = dt.NewRow();
                            dr[0] = sColName;
                            dt.Rows.Add(dr);
                        }
                    }

                    _dsSheets.Tables.Add(dt);
                    dt.Dispose();
                    _dtable = GetExcelColumn2CatalogMapping(sheetName);
                    conn.Close();
                }
                return _dtable;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at ImportApiController :importSelectionColumnGrid", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public DataTable ImportSelectionColumnGridforInvertedProduct(string sheetName, string excelPath)
        {
            var dt = new DataTable();
            try
            {
                if (!string.IsNullOrEmpty(excelPath))
                {
                    Workbook workbook = Workbook.Load(excelPath);
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    string strConn;
                    if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    else
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    // DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    // int sheetCount = schemaTable.Rows.Count;
                    //  int sheetColumnCount = 0;

                    if (!sheetName.Contains("$"))
                    {
                        sheetName = sheetName + "$";
                    }
                    dt = new DataTable(sheetName);
                    var dc = new DataColumn("ExcelColumn", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("CatalogField", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("SelectedToImport", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("IsSystemField", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("FieldType", typeof(string));
                    dt.Columns.Add(dc);
                    //DataRow schemaRow = schemaTable.Rows[0];
                    //string sheet = schemaRow["TABLE_NAME"].ToString();
                    //if (!sheet.EndsWith("_"))
                    //{
                    //    string query = "SELECT  * FROM [" + SheetName + "]";
                    //    OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                    //    dtexcel.Locale = CultureInfo.CurrentCulture;
                    //    daexcel.Fill(dtexcel);
                    //}
                    //sheetColumnCount = dtexcel.Columns.Count;

                    //int iCols = sheetColumnCount;
                    string sheetNameworkbook = sheetName.Replace("$", "").TrimEnd();
                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells.Count();
                    for (int j = 0; j < iCols; j++)
                    {
                        if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null)
                        {
                            String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                            DataRow dr = dt.NewRow();
                            dr[0] = sColName;
                            dt.Rows.Add(dr);
                        }
                    }

                    _dsSheets.Tables.Add(dt);
                    dt.Dispose();
                    _dtable = GetExcelColumn2CatalogMappingforProducts(sheetName);
                    conn.Close();
                }
                return _dtable;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at ImportApiController :importSelectionColumnGrid", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public JsonResult GetImportSpecs(string sheetName, string excelPath)
        {
            // ExcelPath = "C:\\Users\\Rakesh\\Downloads\\LS Export.xls";
            DataTable dtable = ImportSelectionColumnGrid(sheetName, excelPath);
            try
            {
                var dynamicColumns = new Dictionary<string, string>();
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                JsonWriter jsonWriter = new JsonTextWriter(sw);
                var model = new DashBoardModel();
                using (DataTableReader reader = dtable.CreateDataReader())
                {
                    jsonWriter.WriteStartObject();
                    jsonWriter.WritePropertyName("Data");
                    jsonWriter.WriteStartArray();
                    while (reader.Read())
                    {
                        jsonWriter.WriteStartObject();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            jsonWriter.WritePropertyName(reader.GetName(i));
                            jsonWriter.WriteValue(reader.GetValue(i).ToString());
                            if (!dynamicColumns.ContainsKey(reader.GetName(i)) && !string.IsNullOrEmpty(reader.GetName(i)))
                            {
                                dynamicColumns.Add(reader.GetName(i), reader.GetValue(i).ToString());
                            }
                        }
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WritePropertyName("Columns");
                    jsonWriter.WriteStartArray();
                    foreach (string key in dynamicColumns.Keys)
                    {
                        jsonWriter.WriteStartObject();
                        jsonWriter.WritePropertyName("title");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WritePropertyName("field");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WriteEndObject();
                }
                model.Data = sb.ToString();
                return new JsonResult { Data = model };

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : GetProdSpecs", objexception);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public JsonResult GetImportSpecsforinvertedproductsimport(string sheetName, string excelPath)
        {
            // ExcelPath = "C:\\Users\\Rakesh\\Downloads\\LS Export.xls";
            DataTable dtable = ImportSelectionColumnGridforInvertedProduct(sheetName, excelPath);
            try
            {
                var dynamicColumns = new Dictionary<string, string>();
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                JsonWriter jsonWriter = new JsonTextWriter(sw);
                var model = new DashBoardModel();
                using (DataTableReader reader = dtable.CreateDataReader())
                {
                    jsonWriter.WriteStartObject();
                    jsonWriter.WritePropertyName("Data");
                    jsonWriter.WriteStartArray();
                    while (reader.Read())
                    {
                        jsonWriter.WriteStartObject();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            jsonWriter.WritePropertyName(reader.GetName(i));
                            jsonWriter.WriteValue(reader.GetValue(i).ToString());
                            if (!dynamicColumns.ContainsKey(reader.GetName(i)) && !string.IsNullOrEmpty(reader.GetName(i)))
                            {
                                dynamicColumns.Add(reader.GetName(i), reader.GetValue(i).ToString());
                            }
                        }
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WritePropertyName("Columns");
                    jsonWriter.WriteStartArray();
                    foreach (string key in dynamicColumns.Keys)
                    {
                        jsonWriter.WriteStartObject();
                        jsonWriter.WritePropertyName("title");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WritePropertyName("field");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WriteEndObject();
                }
                model.Data = sb.ToString();
                return new JsonResult { Data = model };

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : GetImportSpecsforinvertedproductsimport", objexception);
                return null;
            }
        }

        public string DataTableToJsonObj(DataTable dt)
        {
            try
            {
                var ds = new DataSet();
                ds.Merge(dt);
                var jsonString = new StringBuilder();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    jsonString.Append("[");
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        jsonString.Append("{");
                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {
                            if (j < ds.Tables[0].Columns.Count - 1)
                            {
                                jsonString.Append("\"" + ds.Tables[0].Columns[j].ColumnName + "\":" + "\"" + ds.Tables[0].Rows[i][j] + "\",");
                            }
                            else if (j == ds.Tables[0].Columns.Count - 1)
                            {
                                jsonString.Append("\"" + ds.Tables[0].Columns[j].ColumnName + "\":" + "\"" + ds.Tables[0].Rows[i][j] + "\"");
                            }
                        }
                        if (i == ds.Tables[0].Rows.Count - 1)
                        {
                            jsonString.Append("}");
                        }
                        else
                        {
                            jsonString.Append("},");
                        }
                    }
                    jsonString.Append("]");
                    return jsonString.ToString();
                }
                return null;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at loading DataTableToJsonObj in Import Controller", objException);
                return null;
            }
        }
        public static Boolean IsCatalogStudioField(String enumFieldName)
        {
            if (enumFieldName.ToUpper() == "ITEM#")
            {
                enumFieldName = "ITEM_";
            }
            if (enumFieldName.ToUpper() == "SUBITEM#")
            {
                enumFieldName = "SUBITEM_";
            }
            return Enum.GetNames(typeof(CSF)).Any(val => enumFieldName == val);
        }
        public DataTable GetExcelColumn2CatalogMappingforProducts(String xlsSheetName)
        {

            try
            {
                DataTable dt = _dsSheets.Tables[xlsSheetName];
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    string sCheckAttributeName = dt.Rows[i][0].ToString().ToUpper();
                    //Check if this is a CatalogStudio system defined field name
                    Boolean vaild = IsCatalogStudioField(sCheckAttributeName);
                    var customerName = User.Identity.Name;
                    var customerDetails = objLS.Customers.Where(x => x.EMail == customerName).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();
                    if (sCheckAttributeName == "SUBITEM#" || sCheckAttributeName == customerDetails.ToUpper())
                    {
                        vaild = true;
                    }
                    if (vaild) //CatalogStudio Field
                    {
                        dr["CatalogField"] = sCheckAttributeName;
                        dr["IsSystemField"] = true;
                        dr["FieldType"] = 1;
                        dr["SelectedToImport"] = true;
                    }
                    else //User defined attribute name
                    {
                        var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                        if (customerid != null)
                        {
                            var drAttr =
                                _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                .Where(x => x.tcp.ATTRIBUTE_NAME == sCheckAttributeName && x.tpf.CUSTOMER_ID == customerid.CustomerId).Select(x => x.tcp).ToList();


                            if (drAttr.Count > 0)
                            {
                                string sAttributeNameInCatalog = drAttr[0].ATTRIBUTE_NAME;
                                string sAttributeDatatype = drAttr[0].ATTRIBUTE_DATATYPE;
                                string sAttrType = drAttr[0].ATTRIBUTE_TYPE.ToString(CultureInfo.InvariantCulture);
                                if (sAttributeDatatype == "Date and Time")
                                {
                                    if (sAttrType == "6" || sAttrType == "1" || sAttrType == "3")
                                    {
                                        dr["FieldType"] = 10;
                                        dr["SelectedToImport"] = true;
                                        dr["IsSystemField"] = true;
                                    }
                                    else if (sAttrType == "7" || sAttrType == "11" || sAttrType == "13")
                                    {
                                        dr["FieldType"] = 14;
                                        dr["SelectedToImport"] = true;
                                        dr["IsSystemField"] = true;
                                    }
                                }
                                else
                                {
                                    dr["FieldType"] = drAttr[0].ATTRIBUTE_TYPE.ToString(CultureInfo.InvariantCulture);
                                    dr["SelectedToImport"] = true;
                                    dr["IsSystemField"] = true;
                                }
                                dr["CatalogField"] = sAttributeNameInCatalog;
                                //dr["IsSystemField"] = false;

                            }
                            else
                            {
                                dr["FieldType"] = "";
                                dr["SelectedToImport"] = false;
                            }
                        }
                    }
                    i++;
                }

                return dt;

            }
            catch (Exception objException)
            {
                Logger.Error("Error at loading GetExcelColumn2CatalogMappingforProducts in Import Controller", objException);
                return null;
            }
        }

        public DataTable GetExcelColumn2CatalogMapping(String xlsSheetName)
        {
            try
            {
                DataTable dt = _dsSheets.Tables[xlsSheetName];
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    string sCheckAttributeName = dt.Rows[i][0].ToString().ToUpper();
                    //Check if this is a CatalogStudio system defined field name
                    Boolean vaild = IsCatalogStudioField(sCheckAttributeName);

                    if (sCheckAttributeName == "SUBITEM#" || sCheckAttributeName == "ITEM#")
                    {
                        vaild = true;
                    }
                    if (vaild) //CatalogStudio Field
                    {
                        dr["CatalogField"] = sCheckAttributeName;
                        dr["IsSystemField"] = true;
                        dr["FieldType"] = 0;
                        dr["SelectedToImport"] = true;
                    }
                    else //User defined attribute name
                    {
                        var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                        if (customerid != null)
                        {
                            var drAttr =
                                _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                .Where(x => x.tcp.ATTRIBUTE_NAME == sCheckAttributeName && x.tpf.CUSTOMER_ID == customerid.CustomerId).Select(x => x.tcp).ToList();


                            if (drAttr.Count > 0)
                            {
                                string sAttributeNameInCatalog = drAttr[0].ATTRIBUTE_NAME;
                                string sAttributeDatatype = drAttr[0].ATTRIBUTE_DATATYPE;
                                string sAttrType = drAttr[0].ATTRIBUTE_TYPE.ToString(CultureInfo.InvariantCulture);
                                if (sAttributeDatatype == "Date and Time")
                                {
                                    if (sAttrType == "6" || sAttrType == "1" || sAttrType == "3")
                                    {
                                        dr["FieldType"] = 10;
                                        dr["SelectedToImport"] = true;
                                        dr["IsSystemField"] = true;
                                    }
                                    else if (sAttrType == "7" || sAttrType == "11" || sAttrType == "13")
                                    {
                                        dr["FieldType"] = 14;
                                        dr["SelectedToImport"] = true;
                                        dr["IsSystemField"] = true;
                                    }
                                }
                                else
                                {
                                    dr["FieldType"] = drAttr[0].ATTRIBUTE_TYPE.ToString(CultureInfo.InvariantCulture);
                                    dr["SelectedToImport"] = true;
                                    dr["IsSystemField"] = true;
                                }
                                dr["CatalogField"] = sAttributeNameInCatalog;
                                //dr["IsSystemField"] = false;

                            }
                            else
                            {
                                dr["FieldType"] = "";
                                dr["SelectedToImport"] = false;
                            }
                        }
                    }
                    i++;
                }

                return dt;

            }
            catch (Exception objException)
            {
                Logger.Error("Error at loading GetExcelColumn2CatalogMapping in Import Controller", objException);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public string FinishImport(string sheetName, string allowDuplicate, string excelPath, JArray model)
        {
            string importTemp;
            importTemp = Guid.NewGuid().ToString();
            int userProductCount = 0;
            int userSkuProductCount = 0;
            try
            {
                int itemVal = Convert.ToInt32(allowDuplicate);
                using (var conn = new SqlConnection(_connectionString))
                {
                    //conn.Open();
                    var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                    if (customerid != null)
                    {
                        conn.Open();
                        string sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPRODUCTIMPORTTEMP" + importTemp + "]'')BEGIN DROP TABLE [##SUBPRODUCTIMPORTTEMP" + importTemp + "] END')";
                        _ic.importExcelSheetSelection(excelPath, sheetName);
                        var dbCommand = new SqlCommand(sqlString, conn);
                        dbCommand.ExecuteNonQuery();
                        _objdatatable = _ic.exceldata(excelPath, sheetName);
                        //----------------------------------------------------For with out ID column Import ----------------------Vinothkumar March 17
                        if (!_objdatatable.Columns.Contains("CATALOG_ID"))
                        {
                            _objdatatable.Columns.Add("CATALOG_ID");
                            _objdatatable.Columns["CATALOG_ID"].SetOrdinal(0);
                        }

                        if (!_objdatatable.Columns.Contains("FAMILY_ID"))
                        {
                            _objdatatable.Columns.Add("FAMILY_ID");
                            _objdatatable.Columns["FAMILY_ID"].SetOrdinal(4);

                        }

                        if (!_objdatatable.Columns.Contains("PRODUCT_ID"))
                        {

                            _objdatatable.Columns.Add("PRODUCT_ID");
                            _objdatatable.Columns["PRODUCT_ID"].SetOrdinal(6);
                        }

                        if (!_objdatatable.Columns.Contains("SUBPRODUCT_ID"))
                        {

                            _objdatatable.Columns.Add("SUBPRODUCT_ID");
                            _objdatatable.Columns["SUBPRODUCT_ID"].SetOrdinal(8);
                        }
                        //------------------------------------------------------------------End-------------------------------------------------------
                        sqlString = _ic.CreateTable("[##SUBPRODUCTIMPORTTEMP" + importTemp + "]", excelPath, sheetName, _objdatatable);
                        dbCommand = new SqlCommand(sqlString, conn);
                        dbCommand.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##SUBPRODUCTIMPORTTEMP" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(_objdatatable);

                        DataTable oattType = _ic.SelectedColumnsToImport(_objdatatable, model);
                        //var cmd1 = new SqlCommand("ALTER TABLE [##SUBPRODUCTIMPORTTEMP" + importTemp + "] ADD Family_Status nvarchar(10),FStatusUI nvarchar(10),StatusUI nvarchar(10),SFSort_Order int,Sort_Order int", conn);
                        //cmd1.ExecuteNonQuery();
                        var sqlstring1 = new SqlCommand("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]'')BEGIN DROP TABLE [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] END')", conn);
                        sqlstring1.ExecuteNonQuery();
                        var cmd2 = new SqlCommand();
                        cmd2.Connection = conn;
                        sqlString = _ic.CreateTableToImport("[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]", oattType);
                        cmd2.CommandText = sqlString;
                        cmd2.CommandType = CommandType.Text;
                        cmd2.ExecuteNonQuery();
                        bulkCopy.DestinationTableName = "[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]";
                        bulkCopy.WriteToServer(oattType);
                        var cmbpk10 = new SqlCommand("update [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] set ATTRIBUTE_TYPE = 1 where ATTRIBUTE_TYPE = 10", conn);
                        cmbpk10.ExecuteNonQuery();
                        var cmbpk1 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1Sub'') IS NOT NULL  DROP TABLE  ##t1Sub') ", conn);
                        cmbpk1.ExecuteNonQuery();
                        var cmbpk2 = new SqlCommand("select * into ##t1Sub  from [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] where attribute_type <>0", conn);
                        cmbpk2.ExecuteNonQuery();
                        var cmbpk3 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2Sub'') IS NOT NULL  DROP TABLE  ##t2Sub') ", conn);
                        cmbpk3.ExecuteNonQuery();
                        var cmbpk4 = new SqlCommand("select * into ##t2Sub  from [##SUBPRODUCTIMPORTTEMP" + importTemp + "]", conn);
                        cmbpk4.ExecuteNonQuery();
                        //--------------------Check Picklist values in valid or not---------------------------------------------
                        var Checkpicklistupdate = _dbcontext.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerid.CustomerId); //To check Picklist validation in enabled or not in customer setting
                        if (Checkpicklistupdate != null && !Checkpicklistupdate.ShowCustomAPPS)
                        {
                            string pickValidation = PickListValidate(importTemp);//Call picklist validation function
                            if (pickValidation.Contains("Import failed due to invalid Pick List"))
                            {
                                return pickValidation;//If picklist validation is invalid it return error with log
                            }

                        }
                        var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_SUBPRODUCTIMPORT ''" + importTemp + "'',''" + itemVal + "'',''" + customerid.CustomerId + "'',''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                        cmd.ExecuteNonQuery();
                        _ic._SQLString = @"select * from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                        var ds = _ic.CreateDataSet();
                        _ic._SQLString = @"select * into [tempresult" + importTemp + "] from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                        cmd.CommandText = _ic._SQLString;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();

                        if (ds.Tables[0].Columns[0].ColumnName == "ErrorMessage")
                        {

                            return "Import Failed~" + importTemp;
                        }
                        else
                        {
                            //--------------------Update Picklist values in Picklist table---------------------------------------------
                            var picklistvaluecreation = _dbcontext.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerid.CustomerId);//To check Picklist validation in enabled or not in customer setting

                            if (picklistvaluecreation != null && picklistvaluecreation.ShowCustomAPPS)
                            {
                                PickListUpdate(importTemp);
                            }
                            return "Import Success~" + importTemp;
                        }
                    }
                    else
                    {
                        return "Invalid Customer~" + importTemp;

                    }

                }


            }

            catch (Exception ex)
            {
                Logger.Error("Error at ImportApiController : FinishImport", ex);
                return "Import Failed~" + importTemp;
            }

        }

        public string PickListValidate(string importTemp)
        {
            try
            {

                using (var conn = new SqlConnection(_connectionString))
                {

                    conn.Open();
                    DataSet pkname = new DataSet();
                    SqlDataAdapter daa = new SqlDataAdapter("select PICKLIST_NAME,ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select attribute_name  from ##t1Sub where attribute_type <>0) and USE_PICKLIST =1", conn);
                    daa.Fill(pkname);
                    if (pkname.Tables[0].Rows.Count > 0)
                    {
                        DataTable Errortable = new DataTable();
                        Errortable.Columns.Add("STATUS");
                        Errortable.Columns.Add("ITEM_NO");
                        Errortable.Columns.Add("ATTRIBUTE_NAME");
                        Errortable.Columns.Add("PICKLIST_VALUE");

                        foreach (DataRow vr in pkname.Tables[0].Rows)
                        {
                            DataTable _pickListValue = new DataTable();
                            DataSet newsheetval = new DataSet();
                            _pickListValue.Columns.Add("Value");
                            string picklistname = vr["PICKLIST_NAME"].ToString();
                            DataSet dpicklistdata = new DataSet();
                            if (!string.IsNullOrEmpty(picklistname))
                            {
                                var pickList = _dbcontext.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                                {
                                    PICKLIST_DATA = s.PICKLIST_DATA,
                                    PICKLIST_NAME = s.PICKLIST_NAME,
                                    PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                                }).FirstOrDefault(d => d.PICKLIST_NAME == picklistname);
                                var objPick = new List<PickList>();
                                if (pickList != null && !string.IsNullOrEmpty(pickList.PICKLIST_DATA))
                                {
                                    var pkxmlData = pickList.PICKLIST_DATA;

                                    if (pkxmlData.Contains("<?xml version"))
                                    {
                                        pkxmlData = pkxmlData.Replace("\\r\\n", "");
                                        pkxmlData = pkxmlData.Replace("\r\n", "");
                                        pkxmlData = pkxmlData.Replace("\\\"", "\"");
                                        pkxmlData = pkxmlData.Replace("<?xml version=\"1.0\" standalone=\"yes\"?>", "");
                                        XDocument objXml = XDocument.Parse(pkxmlData);

                                        objPick =
                                            objXml.Descendants("NewDataSet")
                                                .Descendants("Table1")
                                                .Select(d =>
                                                {
                                                    var xElement = d.Element("ListItem");
                                                    return xElement != null
                                                        ? new PickList
                                                        {
                                                            ListItem = xElement.Value
                                                        }
                                                        : null;
                                                }).ToList();
                                    }
                                }
                                SqlDataAdapter daa2 =
                                        new SqlDataAdapter("Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  ['+ @colName+'],CATALOG_ITEM_NO   from [##t2Sub]') ",
                                            conn);
                                daa2.Fill(newsheetval);
                                var cvb = new SqlCommand("DELETE TOP(1) From ##t1Sub", conn);

                                cvb.ExecuteNonQuery();
                                DataSet picklistdataDS = new DataSet();
                                DataTable newTable = new DataTable();
                                // DataSet newsheetval1 = new DataSet();
                                for (int rowCount = 0; rowCount < newsheetval.Tables[0].Rows.Count; rowCount++)
                                {
                                    if (newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim() != "")
                                    {
                                        var picklistvalue = newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim();
                                        var dd = objPick.Where(x => x.ListItem == picklistvalue);
                                        if (!dd.Any())
                                        {

                                            // SqlDataAdapter daa3 =
                                            //  new SqlDataAdapter("Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  CATALOG_ITEM_NO   from [##t2] where  ['+ @colName+'] = ''" + picklistvalue + "''') ",
                                            //    conn);
                                            //       //  daa3.Fill(newsheetval1);
                                            string CAT = string.Empty;
                                            //if (newsheetval1.Tables[0].Rows.Count > 0)
                                            //{
                                            CAT = newsheetval.Tables[0].Rows[rowCount][1].ToString();
                                            // }

                                            Errortable.Rows.Add("Import failed due to invalid Pick List value.", CAT, vr["ATTRIBUTE_NAME"], picklistvalue);
                                            // return "Import failed due to invalid Pick List value\nAttribute Name : " + vr["ATTRIBUTE_NAME"] + "\nPicklist Value : " + picklistvalue + "\nCat # : " + CAT;
                                        }

                                    }
                                }


                            }
                        }
                        if (Errortable.Rows.Count > 0)
                        {
                            string sqlString = _ic.CreateTableToImport("[Picklistlog" + importTemp + "]", Errortable);
                            SqlCommand cmdPick = new SqlCommand();
                            cmdPick.Connection = conn;
                            cmdPick.CommandText = sqlString;
                            cmdPick.CommandType = CommandType.Text;
                            cmdPick.ExecuteNonQuery();
                            var bulkCopysp = new SqlBulkCopy(conn)
                            {
                                DestinationTableName = "[Picklistlog" + importTemp + "]"
                            };
                            bulkCopysp.WriteToServer(Errortable);

                            return "Import failed due to invalid Pick List value~" + importTemp;
                        }
                    }

                }
            }

            catch (Exception ex)
            {
                Logger.Error("Error at ImportApiController : PickListUpdate", ex);
                return "Import Failed~" + importTemp;
            }
            return "";
        }
        public string PickListUpdate(string importTemp)
        {
            try
            {
                DataSet checkPICK = new DataSet();
                using (var conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    var cmd1f = new SqlCommand(
                    "SELECT * FROM [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]", conn);
                    var daf = new SqlDataAdapter(cmd1f);
                    daf.Fill(checkPICK);
                    string check = string.Empty;
                    if (checkPICK.Tables[0].Rows.Count > 0)
                        foreach (DataColumn column1 in checkPICK.Tables[0].Columns)
                        {
                            if (column1.ToString() == "STATUS")
                            {
                                check = checkPICK.Tables[0].Rows[0][column1.ToString()].ToString();//"Action completed";

                            }
                        }
                    if (check == "Success" || check == "UPDATED")
                    {
                        // if (checkPICK.Tables[0].Rows[0][0].ToString() == "UPDATED")
                        // {

                        DataSet pkname = new DataSet();
                        SqlDataAdapter daa =
                            new SqlDataAdapter(
                                "select PICKLIST_NAME,ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select attribute_name  from ##t1sub where attribute_type <>0) and USE_PICKLIST =1",
                                conn);
                        daa.Fill(pkname);
                        if (pkname.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow vr in pkname.Tables[0].Rows)
                            {
                                DataTable _pickListValue = new DataTable();
                                DataSet newsheetval = new DataSet();
                                _pickListValue.Columns.Add("Value");
                                string picklistname = vr["PICKLIST_NAME"].ToString();
                                DataSet dpicklistdata = new DataSet();
                                if (!string.IsNullOrEmpty(picklistname))
                                {
                                    var pickList = _dbcontext.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                                    {
                                        PICKLIST_DATA = s.PICKLIST_DATA,
                                        PICKLIST_NAME = s.PICKLIST_NAME,
                                        PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                                    }).FirstOrDefault(d => d.PICKLIST_NAME == picklistname);
                                    var objPick = new List<PickList>();
                                    if (pickList != null && !string.IsNullOrEmpty(pickList.PICKLIST_DATA))
                                    {
                                        var pkxmlData = pickList.PICKLIST_DATA;

                                        if (pkxmlData.Contains("<?xml version"))
                                        {
                                            // pkxmlData = pkxmlData.Remove(0, 1);
                                            //pkxmlData = pkxmlData.Remove(pkxmlData.Length - 1);
                                            pkxmlData = pkxmlData.Replace("\\r\\n", "");
                                            pkxmlData = pkxmlData.Replace("\r\n", "");
                                            pkxmlData = pkxmlData.Replace("\\\"", "\"");
                                            pkxmlData =
                                                pkxmlData.Replace(
                                                    "<?xml version=\"1.0\" standalone=\"yes\"?>",
                                                    "");
                                            //PKXMLData = PKXMLData.Replace("ListItem", "Value");

                                            XDocument objXml = XDocument.Parse(pkxmlData);

                                            objPick =
                                                objXml.Descendants("NewDataSet")
                                                    .Descendants("Table1")
                                                    .Select(d =>
                                                    {
                                                        var xElement = d.Element("ListItem");
                                                        return xElement != null
                                                            ? new PickList
                                                            {
                                                                ListItem = xElement.Value
                                                            }
                                                            : null;
                                                    }).ToList();
                                        }
                                    }
                                    SqlDataAdapter daa2 =
                              new SqlDataAdapter("Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  ['+ @colName+'],CATALOG_ITEM_NO   from [##t2sub]') ",
                                  conn);
                                    daa2.Fill(newsheetval);
                                    SqlCommand cvb = new SqlCommand("DELETE TOP(1) From ##t1sub", conn);
                                    cvb.ExecuteNonQuery();
                                    DataSet picklistdataDS = new DataSet();
                                    DataTable newTable = new DataTable();
                                    // newTable.Columns.Add("value");
                                    for (int rowCount = 0;
                                        rowCount < newsheetval.Tables[0].Rows.Count;
                                        rowCount++)
                                    {
                                        //DataRow DRow = _pickListValue.NewRow();
                                        if (newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim() != "")
                                        {
                                            var picklistvalue =
                                                newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim();
                                            PickList objlist = new PickList();
                                            objlist.ListItem = picklistvalue;
                                            if (!objPick.Contains(objlist))
                                            {
                                                objPick.Add(objlist);
                                            }
                                        }
                                    }
                                    var picklistdatas = objPick.DistinctBy(x => x.ListItem).ToList();
                                    foreach (var item in picklistdatas)
                                    {
                                        if (string.IsNullOrEmpty(item.ListItem))
                                        {
                                            picklistdatas.Remove(item);
                                        }
                                        else if (item.ListItem.Trim() == "")
                                        {
                                            picklistdatas.Remove(item);
                                        }
                                        else
                                        {
                                            item.ListItem = item.ListItem.Trim();
                                        }
                                    }
                                    var serializer1 = new JavaScriptSerializer();
                                    var json = serializer1.Serialize(picklistdatas);
                                    XmlDocument doc = JsonConvert.DeserializeXmlNode(
                                        "{\"Table1\":" + json + "}", "NewDataSet");
                                    string picklistdataxml =
                                        "<?xml version=\"1.0\" standalone=\"yes\"?>" +
                                        doc.InnerXml;
                                    var objPicklist =
                                        _dbcontext.TB_PICKLIST.FirstOrDefault(
                                            s => s.PICKLIST_NAME == picklistname);
                                    if (objPicklist != null)
                                        objPicklist.PICKLIST_DATA = picklistdataxml;
                                    _dbcontext.SaveChanges();
                                }
                            }
                            //DataTable distinctTable =
                            //    _pickListValue.DefaultView.ToTable( /*distinct*/ true);
                            //picklistdataDS.Tables.Add(distinctTable);
                            // picklistdataDS.WriteXml(Application.StartupPath + "\\tempxml.xml");
                            //FileStream fileStream =
                            //    new FileStream(Application.StartupPath + "\\tempxml.xml",
                            //        FileMode.Open);
                            //System.IO.StreamReader streamWriter = new System.IO.StreamReader(fileStream);
                            //string dataString = streamWriter.ReadToEnd();
                            //streamWriter.Close();
                            //fileStream.Close();
                        }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at ImportApiController : PickListUpdate", ex);
                return "Import Failed~" + importTemp;
            }
            return "";
        }

        public void CreateCSVFile(DataTable dt_DataTable, string strFilePath)
        {
            // Create the CSV file to which grid data will be exported.
            StreamWriter sw = new StreamWriter(strFilePath, false);
            //First we will write the headers.
            int iColCount = dt_DataTable.Columns.Count;
            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dt_DataTable.Columns[i]);
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
            // Now write all the rows.
            foreach (DataRow dr in dt_DataTable.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }
                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();



        }

        public void CreateTextFile(DataTable dt_DataTable, string strFilePath, string Delimeter)
        {
            // Create the Text file to which grid data will be exported.    

            if (Delimeter == "\\t")
            {
                Delimeter = "\t";
            }
            else if (Delimeter == "\\s")
            {
                Delimeter = "  ";
            }

            StreamWriter sw = new StreamWriter(strFilePath, false);
            //First we will write the headers.
            int iColCount = dt_DataTable.Columns.Count;
            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dt_DataTable.Columns[i]);
                if (i < iColCount - 1)
                {
                    sw.Write(Delimeter);
                }
            }
            sw.Write(sw.NewLine);

            // Now write all the rows.

            foreach (DataRow dr in dt_DataTable.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }
                    if (i < iColCount - 1)
                    {
                        sw.Write(Delimeter);
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();



            //if (FTPdetails[0] != null && FTPdetails[1] != null && FTPdetails[2] != null)
            //{
            //    UploadFTP(strFilePath, FTPdetails);
            //}
        }
        [System.Web.Http.HttpPost]
        public string validatesubproductsImport(string sheetName, string allowDuplicate, string excelPath, string Errorlogoutputformatsub, JArray model)
        {
            string importTemp;
            importTemp = Guid.NewGuid().ToString();
            int userProductCount = 0;
            int userSkuProductCount = 0;
            string sqlString = string.Empty;
            try
            {
                int itemVal = Convert.ToInt32(allowDuplicate);
                using (var conn = new SqlConnection(_connectionString))
                {
                    var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                    if (customerid != null)
                    {

                        conn.Open();
                        sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPRODUCTIMPORTTEMP" + importTemp + "]'')BEGIN DROP TABLE [##SUBPRODUCTIMPORTTEMP" + importTemp + "] END')";
                        _ic.importExcelSheetSelection(excelPath, sheetName);
                        var dbCommand = new SqlCommand(sqlString, conn);
                        dbCommand.ExecuteNonQuery();
                        _objdatatable = _ic.exceldata(excelPath, sheetName);

                        sqlString = _ic.CreateTable("[##SUBPRODUCTIMPORTTEMP" + importTemp + "]", excelPath, sheetName, _objdatatable);
                        dbCommand = new SqlCommand(sqlString, conn);
                        dbCommand.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##SUBPRODUCTIMPORTTEMP" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(_objdatatable);
                        var Itemnumbervalidation = _dbcontext.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerid.CustomerId);
                        //------------------Created by vinothkumar for Validation based on catalogsettings June 2017 -------------------------------

                        string validation = Itemnumbervalidation.SubCatalog_Items.ToString();
                        if (Itemnumbervalidation != null && validation == "1")
                        {
                            string cat = _dbcontext.Customer_Settings.Where(x => x.CustomerId == customerid.CustomerId).Select(x => x.Customer_MasterCatalog).FirstOrDefault().ToString();
                            DataSet validate_subitems = new DataSet();
                            sqlString = "SELECT CATALOG_NAME as CATALOG_NAME,CATEGORY_NAME as CATEGORY_NAME,FAMILY_NAME as FAMILY_NAME,CATALOG_ITEM_NO,SUBITEM# as SUBITEM into [validatedresult" + importTemp + "] FROM [##SUBPRODUCTIMPORTTEMP" + importTemp + "] WHERE SUBITEM# IN " +

                                " (SELECT SUBITEM# FROM [##SUBPRODUCTIMPORTTEMP" + importTemp + "] " +
                                         " except " +
                                         " select distinct tps.STRING_VALUE from TB_PROD_SPECS tps " +
                                         " join TB_CATALOG_PRODUCT tc on tc.PRODUCT_ID = tps.PRODUCT_ID " +
                                         " join TB_SUBPRODUCT ts on ts.SUBPRODUCT_ID = tps.PRODUCT_ID " +
                                          " where tps.ATTRIBUTE_ID = 1 and ts.CATALOG_ID = " + cat + " )";
                            SqlCommand _DBCommandvv = new SqlCommand(sqlString, conn);
                            _DBCommandvv.ExecuteNonQuery();
                            var cmd1fsub = new SqlCommand("select * from [validatedresult" + importTemp + "]", conn);
                            var dafa = new SqlDataAdapter(cmd1fsub);
                            dafa.Fill(validate_subitems);
                            if (validate_subitems.Tables.Count > 0 && validate_subitems.Tables[0].Rows.Count > 0)
                            {
                                string validationfilename = "Validation_log";
                                string filepath = HttpContext.Current.Server.MapPath("~/Content/" + validationfilename + "");
                                string format = filepath + ".csv";
                                CreateCSVFile(validate_subitems.Tables[0], format);
                                string txtformat = filepath + ".txt";
                                CreateTextFile(validate_subitems.Tables[0], txtformat, "\t");
                                ExportDataSetToExcel(validate_subitems, validationfilename);
                                return "validation Failed~" + importTemp;
                            }
                            else
                            {
                                return "validation passed~" + importTemp;
                            }
                        }
                    }
                    else
                    {
                        return "Import Success~" + importTemp;

                    }

                }
                return "Import Success~" + importTemp;

            }

            catch (Exception ex)
            {
                Logger.Error("Error at ImportApiController : FinishImport", ex);
                return "Import Failed~" + importTemp;
            }

        }

        public void ExportDataSetToExcel(DataSet finalDs, string fileName)
        {
            try
            {

                if (finalDs.Tables[0].Columns.Contains("FAMILY_ID_1"))
                {
                    finalDs.Tables[0].Columns.Remove("FAMILY_ID_1");
                }
                if (finalDs.Tables[0].Columns.Contains("FAMILY_FOOT_NOTES"))
                {
                    finalDs.Tables[0].Columns.Remove("FAMILY_FOOT_NOTES");
                }
                if (finalDs.Tables[0].Columns.Contains("FAMILY_STATUS"))
                {
                    finalDs.Tables[0].Columns.Remove("FAMILY_STATUS");
                }

                if (fileName.Trim() != "")
                {
                    object context;
                    if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                    {
                        var httpContext = context as HttpContextBase;
                        if (httpContext != null && httpContext.Session != null)
                        {
                            if (finalDs.Tables[0].Rows.Count > 0)
                            {
                                httpContext.Session["ExportTable"] = finalDs.Tables[0];
                            }

                        }
                    }
                }

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at Export : ExportXls", objexception);
                //return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        public string FinishImportTemplate(DataSet dsSet, string excelPath)
        {
            string importTemp;
            importTemp = Guid.NewGuid().ToString();
            int userProductCount = 0;
            int userSkuProductCount = 0;
            string sheetName = "Sheet1$";
            try
            {
                string allowDuplicate = dsSet.Tables[2].Rows[0][0].ToString();
                int itemVal = Convert.ToInt32(allowDuplicate);
                using (var conn = new SqlConnection(_connectionString))
                {
                    var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                    if (customerid != null)
                    {
                        conn.Open();
                        string sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPRODUCTIMPORTTEMP" + importTemp + "]'')BEGIN DROP TABLE [##SUBPRODUCTIMPORTTEMP" + importTemp + "] END')";
                        _ic.importExcelSheetSelection(excelPath, sheetName);
                        var dbCommand = new SqlCommand(sqlString, conn);
                        dbCommand.ExecuteNonQuery();
                        _objdatatable = _ic.exceldata(excelPath, sheetName);
                        sqlString = _ic.CreateTable("[##SUBPRODUCTIMPORTTEMP" + importTemp + "]", excelPath, sheetName, _objdatatable);
                        dbCommand = new SqlCommand(sqlString, conn);
                        dbCommand.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##SUBPRODUCTIMPORTTEMP" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(_objdatatable);

                        DataTable oattType = _ic.SelectedColumnsToImportTemplate(_objdatatable, dsSet.Tables[0]);
                        //var cmd1 = new SqlCommand("ALTER TABLE [##SUBPRODUCTIMPORTTEMP" + importTemp + "] ADD Family_Status nvarchar(10),FStatusUI nvarchar(10),StatusUI nvarchar(10),SFSort_Order int,Sort_Order int", conn);
                        //cmd1.ExecuteNonQuery();
                        var sqlstring1 = new SqlCommand("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]'')BEGIN DROP TABLE [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] END')", conn);
                        sqlstring1.ExecuteNonQuery();
                        var cmd2 = new SqlCommand();
                        cmd2.Connection = conn;
                        sqlString = _ic.CreateTableToImport("[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]", oattType);
                        cmd2.CommandText = sqlString;
                        cmd2.CommandType = CommandType.Text;
                        cmd2.ExecuteNonQuery();
                        bulkCopy.DestinationTableName = "[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]";
                        bulkCopy.WriteToServer(oattType);
                        var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_SUBPRODUCTIMPORT ''" + importTemp + "'',''" + itemVal + "'',''" + customerid.CustomerId + "'',''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                        cmd.ExecuteNonQuery();
                        _ic._SQLString = @"select * from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                        var ds = _ic.CreateDataSet();
                        _ic._SQLString = @"select * into [tempresult" + importTemp + "] from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                        cmd.CommandText = _ic._SQLString;
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();

                        if (ds.Tables[0].Columns[0].ColumnName == "ErrorMessage")
                        {

                            return "Import Failed~" + importTemp;
                        }
                        else
                        {

                            return "Import Success~" + importTemp;
                        }
                    }
                    else
                    {
                        return "Import Success~" + importTemp;

                    }

                }


            }

            catch (Exception ex)
            {
                Logger.Error("Error at ImportApiController : FinishImport", ex);
                return "Import Failed~" + importTemp;
            }

        }
        public class IMPORT_ERROR_LIST
        {
            public string ErrorMessage { get; set; }
            public string ErrorProcedure { get; set; }
            public string ErrorSeverity { get; set; }
            public string ErrorState { get; set; }
            public string ErrorNumber { get; set; }
            public string ErrorLine { get; set; }
        }

        public class IMPORT_SUCCESS_LIST
        {
            public string CATALOG_NAME { get; set; }
            public string CATEGORY_DETAILS { get; set; }
            public string FAMILY_DETAILS { get; set; }
            public string SUBFAMILY_DETAILS { get; set; }
            public string PRODUCT_DETAILS { get; set; }
            public string STATUS { get; set; }
        }
        [System.Web.Http.HttpGet]
        public IList GetFinishImportResults(string sessionId)
        {
            var errorList = new List<IMPORT_ERROR_LIST>();
            try
            {
                if (!string.IsNullOrEmpty(sessionId))
                {
                    string[] session = sessionId.Split(',');
                    _ic._SQLString = @"select * from [tempresult" + session[1] + "]";
                    DataSet ds = _ic.CreateDataSet();
                    _ic._SQLString = "drop table [tempresult" + session[1] + "]";
                    var connection = new SqlConnection(_connectionString);
                    var cmd = new SqlCommand(_ic._SQLString, connection);
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                    errorList = (from DataRow row in ds.Tables[0].Rows

                                 select new IMPORT_ERROR_LIST
                                 {
                                     ErrorMessage = row["ErrorMessage"].ToString(),
                                     ErrorProcedure = row["ErrorProcedure"].ToString(),
                                     ErrorSeverity = row["ErrorSeverity"].ToString(),
                                     ErrorState = row["ErrorState"].ToString(),
                                     ErrorNumber = row["ErrorNumber"].ToString(),
                                     ErrorLine = row["ErrorLine"].ToString()

                                 }).ToList();


                    return errorList;
                }
                return errorList;
            }

            catch (Exception ex)
            {
                _message = "Import Failed, please try again";
                Logger.Error("Error at ImportController : getFinishImportResults", ex);
                return errorList;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetFinishImportSuccessResults(string sessionId)
        {
            var successList = new List<IMPORT_SUCCESS_LIST>();
            try
            {
                if (!string.IsNullOrEmpty(sessionId))
                {

                    string[] session = sessionId.Split(',');
                    _ic._SQLString = @"select * from [tempresult" + session[1] + "]";
                    DataSet ds = _ic.CreateDataSet();
                    _ic._SQLString = "drop table [tempresult" + session[1] + "]";
                    var connection = new SqlConnection(_connectionString);
                    var cmd = new SqlCommand(_ic._SQLString, connection);
                    connection.Open();
                    cmd.CommandText = _ic._SQLString;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    return (from DataRow row in ds.Tables[0].Rows

                            select new
                            {
                                CATALOG_NAME = row["CATALOG_NAME"].ToString(),
                                PRODUCT_DETAILS = row["PRODUCT_DETAILS"].ToString(),
                                SUB_PRODUCT_DETAILS = row["SUB_PRODUCT_DETAILS"].ToString(),
                                STATUS = row["STATUS"].ToString()

                            }).ToList();

                }
                //JsonResult jsonResult = new JsonResult();

                //jsonResult.MaxJsonLength = int.MaxValue;
                //jsonResult = Json(successList, JsonRequestBehavior.AllowGet);
                ////return jsonResult;
                //return Json(jsonResult, JsonRequestBehavior.AllowGet);
                return successList;
            }

            catch (Exception ex)
            {
                _message = "Import Failed, please try again";
                Logger.Error("Error at ImportController : getFinishImportResults", ex);
                return successList;
            }
        }



        [System.Web.Http.HttpGet]

        public IList GetAllcommonvaluesubproducts(int familyid, string categoryId, int catalogId, string id, int parentid)
        {
            try
            {
                if (catalogId != 0)
                {
                    return GetAllcommonvaluesubproductsTree(familyid, categoryId, catalogId, id, parentid);
                }
                else
                {
                    var objpProductViews = new List<CommonValue>();
                    return objpProductViews;
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at SubProductsImportApiController : GetAllcommonvalueproducts", objexception);
                return null;
            }
        }

        [System.Web.Http.HttpGet]

        public bool GetAllcommonvaluesubproductsCheck(int familyid, string categoryId, int catalogId)
        {
            bool status = false;
            try
            {
                if (catalogId != 0)
                {
                    var productdetails = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_PROD_SPECS, tps => tps.PRODUCT_ID,
                        cs => cs.PRODUCT_ID, (tps, cs) => new { tps, cs })
                         .Join(_dbcontext.TB_SUBPRODUCT, tcptps => tcptps.tps.PRODUCT_ID, ts => ts.PRODUCT_ID,
                      (tcptpscs, ts) => new { tcptpscs, ts }).
                        Where(x =>



                            x.tcptpscs.cs.ATTRIBUTE_ID == 1 &&
                            x.tcptpscs.tps.FAMILY_ID == familyid
                            );
                    if (productdetails.Any())
                    {
                        status = true;
                    }


                }
                else
                {

                    status = false;
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at SubProductsImportApiController : GetAllcommonvalueproducts", objexception);
                return false;
            }
            return status;
        }

        public IList GetAllcommonvaluesubproductsTree(int familyid, string categoryId, int catalogId, string id, int parentid)
        {
            try
            {
                if (id == "undefined")
                {
                    if (!string.IsNullOrEmpty(categoryId))
                    {
                        var ids = categoryId.Split('~');
                        categoryId = ids[0];
                    }
                    else
                    {
                        categoryId = "";
                    }

                    var familydetails =
                        _dbcontext.TB_CATALOG_FAMILY.Where(
                            x => x.TB_FAMILY.FAMILY_ID == familyid && x.CATALOG_ID == catalogId &&
                                 x.CATEGORY_ID == categoryId);

                    var productdetails = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_PROD_SPECS, tps => tps.PRODUCT_ID,
                        cs => cs.PRODUCT_ID, (tps, cs) => new { tps, cs })
                         .Join(_dbcontext.TB_SUBPRODUCT, tcptps => tcptps.tps.PRODUCT_ID, ts => ts.PRODUCT_ID,
                      (tcptpscs, ts) => new { tcptpscs, ts }).
                        Where(x =>



                            x.tcptpscs.cs.ATTRIBUTE_ID == 1 &&
                            x.ts.PRODUCT_ID == parentid
                            );


                    if (productdetails.Any())
                    {
                        var ss = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_PROD_SPECS, tps => tps.PRODUCT_ID,
                    cs => cs.PRODUCT_ID, (tps, cs) => new { tps, cs })
                     .Join(_dbcontext.TB_SUBPRODUCT, tcptps => tcptps.tps.PRODUCT_ID, ts => ts.PRODUCT_ID,
                  (tcptpscs, ts) => new { tcptpscs, ts }).
                    Where(x =>

                      x.ts.PRODUCT_ID == parentid &&

                        x.tcptpscs.cs.ATTRIBUTE_ID == 1);
                        var ss1 = ss.GroupBy(x => x.tcptpscs.tps.PRODUCT_ID).Select(grp => grp.FirstOrDefault());


                        return ss1.Select(x => new CommonValue
                        {
                            id = x.tcptpscs.cs.PRODUCT_ID,
                            FAMILY_NAME = x.tcptpscs.cs.STRING_VALUE,
                            hasChildren = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, tps => tps.PRODUCT_ID,
                               tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                               .Any(y => y.tpf.CATALOG_ID == catalogId
                                         ),
                            spriteCssClass = "family",

                        }).ToList();
                    }


                    else
                    {
                        var objpProductViews = new List<CommonValue>();
                        return objpProductViews;
                    }
                    // 
                }
                else
                {
                    if (!string.IsNullOrEmpty(categoryId))
                    {
                        var ids = categoryId.Split('~');
                        categoryId = ids[0];
                    }
                    else
                    {
                        categoryId = "";
                    }
                    int family = 0;
                    if (!string.IsNullOrEmpty(id))
                    {
                        family = Convert.ToInt32(id);
                    }
                    int fid;
                    int.TryParse(id.Trim('~'), out fid);
                    var productdetails = _dbcontext.TB_PROD_FAMILY.Where(x => x.FAMILY_ID == familyid);


                    //if (productdetails.Any())
                    //{
                    //    return productdetails.Select(x => new CommonValue
                    //    {
                    //        id = x.PRODUCT_ID,

                    //        hasChildren = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, tps => tps.PRODUCT_ID,
                    //            tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                    //            .Any(y => y.tpf.CATALOG_ID == catalogId
                    //                      ),

                    //    }).ToList();
                    //}
                    int prod_id = int.Parse(id);
                    var names = _dbcontext.TB_PROD_FAMILY
                        .Join(_dbcontext.TB_SUBPRODUCT, tps => tps.PRODUCT_ID, tpf => tpf.PRODUCT_ID,
                            (tps, tpf) => new { tps, tpf })
                   .Join(_dbcontext.TB_PROD_SPECS, tcptps => tcptps.tpf.SUBPRODUCT_ID, cs => cs.PRODUCT_ID,
                      (tcptpscs, cs) => new { tcptpscs, cs })
                  .Where(
                      x =>
                          x.cs.ATTRIBUTE_ID == 1 &&
                          x.tcptpscs.tps.FAMILY_ID == familyid &&
                          x.tcptpscs.tpf.CATALOG_ID == catalogId &&
                          x.tcptpscs.tpf.PRODUCT_ID == prod_id

                         )
                  .Select(y => new CommonValue
                  {
                      id = y.cs.PRODUCT_ID,

                      FAMILY_NAME = y.cs.STRING_VALUE,

                      spriteCssClass = "producttree"
                  });


                    return names.ToList();
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at SubProductsImportApiController : GetAllcommonvalueproductsTree", objexception);
                return null;
            }

        }
        //-----------------------------------------CommonValue--------------------------------------------
        [System.Web.Http.HttpGet]
        public IList GetAllcommonvalueproducts(int familyid, string categoryId, int catalogId, string id)
        {
            try
            {
                if (catalogId != 0)
                {
                    return GetAllcommonvalueproductsTree(familyid, categoryId, catalogId, id);
                }
                else
                {
                    var objpProductViews = new List<CommonValue>();
                    return objpProductViews;
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at SubProductsImportApiController : GetAllcommonvalueproducts", objexception);
                return null;
            }
        }


        public IList GetAllcommonvalueproductsTree(int familyid, string categoryId, int catalogId, string id)
        {
            try
            {
                if (id == "undefined")
                {
                    if (!string.IsNullOrEmpty(categoryId))
                    {
                        var ids = categoryId.Split('~');
                        categoryId = ids[0];
                    }
                    else
                    {
                        categoryId = "";
                    }
                    var familydetails =
                        _dbcontext.TB_CATALOG_FAMILY.Where(
                            x => x.TB_FAMILY.FAMILY_ID == familyid && x.CATALOG_ID == catalogId &&
                                 x.CATEGORY_ID == categoryId);
                    if (familydetails.Any())
                    {
                        var familydetailss = familydetails.Select(x => new CommonValue
                        {
                            id = x.FAMILY_ID,
                            FAMILY_ID = x.FAMILY_ID,
                            FAMILY_NAME = x.TB_FAMILY.FAMILY_NAME,
                            CATALOG_ID = x.CATALOG_ID,
                            CATALOG_NAME = x.TB_CATALOG.CATALOG_NAME,
                            hasChildren =
                                _dbcontext.TB_CATALOG_PRODUCT.Join(_dbcontext.TB_PROD_FAMILY, tps => tps.PRODUCT_ID,
                                    tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                                    .Any(y => y.tps.CATALOG_ID == catalogId
                                              && y.tpf.FAMILY_ID == familyid),
                            spriteCssClass = "family"
                        });

                        return familydetailss.ToList();
                    }
                    else
                    {
                        var objpProductViews = new List<CommonValue>();
                        return objpProductViews;
                    }
                    // 
                }
                else
                {
                    if (!string.IsNullOrEmpty(categoryId))
                    {
                        var ids = categoryId.Split('~');
                        categoryId = ids[0];
                    }
                    else
                    {
                        categoryId = "";
                    }
                    int family = 0;
                    if (!string.IsNullOrEmpty(id))
                    {
                        family = Convert.ToInt32(id);
                    }
                    int fid;
                    int.TryParse(id.Trim('~'), out fid);
                    // var list = new List<TB_PROD_SPECS>();
                    //list.AddRange(_dbcontext.TB_PROD_SPECS.Where(a => a.ATTRIBUTE_ID == 1));

                    var names = _dbcontext.TB_CATALOG_FAMILY
                        .Join(_dbcontext.TB_CATALOG_PRODUCT, tps => tps.CATALOG_ID, tpf => tpf.CATALOG_ID,
                            (tps, tpf) => new { tps, tpf })
                        .Join(_dbcontext.TB_PROD_FAMILY, tcptps => tcptps.tpf.PRODUCT_ID, tcp => tcp.PRODUCT_ID,
                            (tcptps, tcp) => new { tcptps, tcp })
                        .Join(_dbcontext.TB_PROD_SPECS, tcptps => tcptps.tcp.PRODUCT_ID, cs => cs.PRODUCT_ID,
                            (tcptpscs, cs) => new { tcptpscs, cs })
                        .Where(
                            x =>
                                x.cs.ATTRIBUTE_ID == 1 && x.tcptpscs.tcp.FAMILY_ID == x.tcptpscs.tcptps.tps.FAMILY_ID && x.tcptpscs.tcptps.tpf.CATALOG_ID == catalogId &&
                                x.tcptpscs.tcptps.tps.FAMILY_ID == family &&
                                x.tcptpscs.tcptps.tps.CATEGORY_ID == categoryId)
                        .Select(y => new CommonValue
                        {
                            id = y.cs.PRODUCT_ID,
                            FAMILY_ID = y.cs.PRODUCT_ID,
                            FAMILY_NAME = y.cs.STRING_VALUE,
                            CATALOG_ID = catalogId,
                            CATALOG_NAME = y.tcptpscs.tcptps.tps.TB_CATALOG.CATALOG_NAME,
                            hasChildren = false,
                            spriteCssClass = "family"
                        });
                    return names.ToList();
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at SubProductsImportApiController : GetAllcommonvalueproductsTree", objexception);
                return null;
            }

        }

        [System.Web.Http.HttpGet]
        public IList Getsubprodattributes(int familyid, string categoryId, int catalogId)
        {
            try
            {
                if (catalogId != 0)
                {
                    var ss = _dbcontext.STP_LS_SubProductPivotTableAttributes_commonvalueupdate(familyid, catalogId, User.Identity.Name).Where(x => x.IS_CALCULATED == false).ToList();
                    var ss1 = ss.FirstOrDefault(x => x.ATTRIBUTE_ID == 1);
                    ss.Remove(ss1);
                    return ss;
                }
                else
                {
                    var objpProductViews = new List<STP_LS_ProductPivotTableAttributes_Result>();
                    return objpProductViews;
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at SubProductsImportApiController : Getprodattributes", objexception);
                return null;
            }
        }


        [System.Web.Http.HttpGet]
        public IList Getprodattributes(int familyid, string categoryId, int catalogId)
        {
            try
            {
                if (catalogId != 0)
                {
                    var ss = _dbcontext.STP_LS_ProductPivotTableAttributes(familyid, catalogId, User.Identity.Name).Where(x => x.IS_CALCULATED == false).ToList();
                    var ss1 = ss.FirstOrDefault(x => x.ATTRIBUTE_ID == 1);
                    ss.Remove(ss1);
                    return ss;
                }
                else
                {
                    var objpProductViews = new List<STP_LS_ProductPivotTableAttributes_Result>();
                    return objpProductViews;
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at SubProductsImportApiController : Getprodattributes", objexception);
                return null;
            }
        }


        [System.Web.Http.HttpPost]
        public void SaveCommonProductSpecs(string selectedprodids, int catalogId, int familyid, string categoryid, string commonvalue, int attributeid, string imageCaption, object model)
        {
            var productid = selectedprodids.Split(',');
            var string_val = JsonConvert.DeserializeObject<Dictionary<string, string>>(model.ToString());
            string value = string_val["STRING_VALUE"];
            foreach (var s in productid)
            {
                int prodid = Convert.ToInt32(s);
                SaveProductSpecs(prodid, catalogId, familyid, categoryid, value, attributeid, imageCaption);
            }
        }


        public void SaveProductSpecs(int productId, int catalogId, int familyId, string categoryid, string commonvalue, int attributeid, string imageCaption)
        {
            try
            {
                bool publish = true;
                bool publish2Print = true;
                int workflowstatus = 0;
                int preVal = 0;
                int orgVal = 0;
                string categoryId = string.Empty;

                if (categoryid.Trim() != "")
                {
                    var id = categoryid.Split('~');
                    categoryId = Convert.ToString(id[0]);

                }
                var objproductpublish = _dbcontext.TB_PROD_FAMILY.FirstOrDefault(p => p.FAMILY_ID == familyId && p.PRODUCT_ID == productId);
                var attributeId = attributeid;
                var attributedetails = _dbcontext.TB_ATTRIBUTE.Find(attributeId);
                string attributeName = attributedetails.ATTRIBUTE_NAME;
                var attributeType = attributedetails.ATTRIBUTE_TYPE;
                var attributeDatatype = attributedetails.ATTRIBUTE_DATATYPE;
                var attributeDataformat = attributedetails.ATTRIBUTE_DATAFORMAT;
                var stringvalue = commonvalue;
                string attributesize = Convert.ToString(attributedetails.ATTRIBUTE_DATATYPE);
                int textLength = 0;
                //if (!string.IsNullOrEmpty(stringvalue))
                //{
                //    stringvalue = stringvalue.Replace("\n", "");
                //    if (stringvalue.ToLower().StartsWith("<p>") && stringvalue.ToLower().EndsWith("</p>"))
                //    {
                //        stringvalue = stringvalue.Replace("<p>", "");
                //        stringvalue = stringvalue.Replace("</p>", "");
                //    }
                //}
                if (!string.IsNullOrEmpty(stringvalue))
                {
                    if (stringvalue.ToLower().EndsWith("\n"))
                    {
                        int sd = stringvalue.LastIndexOf("\n", StringComparison.Ordinal);
                        stringvalue = stringvalue.Remove(sd);
                    }
                    if (stringvalue.ToLower().EndsWith("\r"))
                    {
                        int sd = stringvalue.LastIndexOf("\r", StringComparison.Ordinal);
                        stringvalue = stringvalue.Remove(sd);
                    }
                    if (stringvalue.ToLower().StartsWith("<p>") && stringvalue.ToLower().EndsWith("</p>"))
                    {
                        int count = 0;
                        int i = 0;
                        while ((i = stringvalue.IndexOf("<p>", i, System.StringComparison.Ordinal)) != -1)
                        {
                            i += "<p>".Length;
                            count++;
                        }
                        if (count == 1)
                        {
                            int endp = stringvalue.LastIndexOf("</p>", StringComparison.Ordinal);
                            stringvalue = stringvalue.Remove(endp);
                            int p = stringvalue.IndexOf("<p>", StringComparison.Ordinal);
                            stringvalue = stringvalue.Remove(p, 3);
                        }
                    }
                    stringvalue = stringvalue.Replace("\r", "");
                    stringvalue = stringvalue.Replace("\n\n", "\n");
                    stringvalue = stringvalue.Replace("\n", "\r\n");
                    //stringvalue = stringvalue.Replace("<br />", "\r\n");
                    textLength = stringvalue.Length;
                }

                if (attributeType != 6)
                {
                    var objproductSpecs = _dbcontext.TB_PROD_SPECS.FirstOrDefault(s => s.PRODUCT_ID == productId && s.ATTRIBUTE_ID == attributeId);
                    var subproductcheck = _dbcontext.TB_SUBPRODUCT.Where(a => a.SUBPRODUCT_ID == productId);
                    if (objproductSpecs == null)
                    {
                        SaveProdSpecforNull(stringvalue, attributeType, attributeDatatype, productId, attributeId, attributesize, textLength, attributeDataformat, imageCaption, string.Empty);
                    }
                    else
                    {
                        if (attributeType == 4)
                        {
                            if (string.IsNullOrEmpty(stringvalue))
                            {
                                objproductSpecs.NUMERIC_VALUE = null;
                                _dbcontext.SaveChanges();
                            }
                            else
                            {
                                decimal numericvalue;
                                Decimal.TryParse(stringvalue, out numericvalue);
                                objproductSpecs.NUMERIC_VALUE = numericvalue;
                                _dbcontext.SaveChanges();
                            }
                        }
                        else if (attributeType == 3)
                        {
                            if (stringvalue == "")
                            {
                                objproductSpecs.STRING_VALUE = imageCaption;
                            }

                            else if (stringvalue.Contains(@"//Content") || stringvalue.Contains(@"//ImageandAttFile"))
                            {
                                objproductSpecs.STRING_VALUE = objproductSpecs.STRING_VALUE;
                            }
                            else
                            {
                                objproductSpecs.STRING_VALUE = stringvalue;
                            }
                            objproductSpecs.OBJECT_NAME = imageCaption;
                            _dbcontext.SaveChanges();

                        }
                        else
                        {
                            if (attributeDatatype.ToLower().Contains("number"))
                            {
                                if (string.IsNullOrEmpty(stringvalue))
                                {
                                    objproductSpecs.NUMERIC_VALUE = null;
                                    _dbcontext.SaveChanges();
                                }
                                else
                                {
                                    decimal numericvalue;
                                    Decimal.TryParse(stringvalue, out numericvalue);
                                    objproductSpecs.NUMERIC_VALUE = numericvalue;
                                    _dbcontext.SaveChanges();
                                }
                            }
                            else
                            {
                                if (attributesize.Contains('('))
                                {
                                    string[] sizearray = attributesize.Split('(');
                                    string[] size = sizearray[1].Split(')');
                                    if (textLength <= Convert.ToInt32(size[0]))
                                    {
                                        objproductSpecs.STRING_VALUE = stringvalue;
                                    }
                                }
                                else
                                {
                                    if (attributeDatatype.Contains("Date"))
                                    {
                                        if (attributeDataformat == @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))"
                                            || attributeDataformat == @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2}\s\w{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))")
                                        {
                                            var val = stringvalue.Split('M');
                                            if (val[1].Trim() == "")
                                            {
                                                objproductSpecs.STRING_VALUE = stringvalue;
                                            }
                                        }
                                        else if (attributeDataformat == @"(?=dd(-|/|.)dd(-|/|.)d{4})(?=.{0}(?:0[1-9]|[12]d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(-|/|.)02(-|/|.)...[13579])(?!29(-|/|.)02(-|/|.)..[13579][048])(?!29(-|/|.)02(-|/|.)..[02468][26])(?!29(-|/|.)02(-|/|.).[13579]00)(?!29(-|/|.)02(-|/|.)[13579][048]00)(?!29(-|/|.)02(-|/|.)[02468][26]00)")
                                        {
                                            objproductSpecs.STRING_VALUE = stringvalue;
                                        }
                                        else if (attributeDataformat == "System default settings")
                                        {
                                            objproductSpecs.STRING_VALUE = stringvalue;
                                        }
                                        else
                                        {

                                            objproductSpecs.STRING_VALUE = stringvalue;
                                        }
                                    }
                                    else
                                    {
                                        objproductSpecs.STRING_VALUE = stringvalue;
                                    }

                                }
                            }
                            objproductSpecs.MODIFIED_USER = User.Identity.Name;
                            objproductSpecs.MODIFIED_DATE = DateTime.Now;
                            _dbcontext.SaveChanges();
                        }
                    }
                }

                else
                {
                    var subproductCheck = _dbcontext.TB_SUBPRODUCT.Where(a => a.PRODUCT_ID == productId && a.SUBPRODUCT_ID == productId);
                    if (!subproductCheck.Any())
                    {
                        var objpartskey = _dbcontext.TB_PARTS_KEY.FirstOrDefault(x => x.PRODUCT_ID == productId && x.ATTRIBUTE_ID == attributeId && x.FAMILY_ID == familyId && x.CATALOG_ID == catalogId && x.CATEGORY_ID == categoryId);
                        if (objpartskey != null)
                        {
                            objpartskey.ATTRIBUTE_VALUE = stringvalue;
                            _dbcontext.SaveChanges();

                        }
                        else
                        {
                            var objparts = new TB_PARTS_KEY
                            {
                                ATTRIBUTE_ID = attributeId,
                                PRODUCT_ID = productId,
                                FAMILY_ID = familyId,
                                ATTRIBUTE_VALUE = stringvalue,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                CATALOG_ID = catalogId,
                                CATEGORY_ID = categoryId
                            };
                            _dbcontext.TB_PARTS_KEY.Add(objparts);
                            _dbcontext.SaveChanges();
                        }
                    }

                    else
                    {
                        var objsubprokeyCheck = _dbcontext.TB_SUBPRODUCT_KEY.FirstOrDefault(a => a.CATALOG_ID == catalogId && a.CATEGORY_ID == categoryId && a.FAMILY_ID == familyId && a.PRODUCT_ID == productId && a.SUBPRODUCT_ID == productId && a.ATTRIBUTE_ID == attributeId);
                        if (objsubprokeyCheck != null)
                        {
                            objsubprokeyCheck.ATTRIBUTE_VALUE = stringvalue;
                            _dbcontext.SaveChanges();
                        }
                        else
                        {
                            var objparts = new TB_SUBPRODUCT_KEY
                            {
                                ATTRIBUTE_ID = attributeId,
                                PRODUCT_ID = productId,
                                SUBPRODUCT_ID = productId,
                                FAMILY_ID = familyId,
                                ATTRIBUTE_VALUE = stringvalue,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                CATALOG_ID = catalogId,
                                CATEGORY_ID = categoryId
                            };
                            _dbcontext.TB_SUBPRODUCT_KEY.Add(objparts);
                            _dbcontext.SaveChanges();
                        }
                    }
                }

                // }
                // }

                //  }
                //SaveCalculatedProdSpecs(productId, familyId, catalogId);
                //publish
                if (objproductpublish != null)
                {

                    objproductpublish.PUBLISH = publish;
                    objproductpublish.PUBLISH2PRINT = publish2Print;
                    objproductpublish.WORKFLOW_STATUS = workflowstatus;
                    objproductpublish.MODIFIED_USER = User.Identity.Name;
                    objproductpublish.MODIFIED_DATE = DateTime.Now;
                    _dbcontext.SaveChanges();

                    //var objfamilySpecs = _dbcontext.TB_FAMILY_SPECS.FirstOrDefault(p => p.FAMILY_ID == familyId);
                    //objfamilySpecs.MODIFIED_DATE = DateTime.Now;
                    //objfamilySpecs.MODIFIED_USER = User.Identity.Name;
                    //_dbcontext.SaveChanges();

                    var objfamily = _dbcontext.TB_FAMILY.FirstOrDefault(p => p.FAMILY_ID == familyId);
                    objfamily.MODIFIED_DATE = DateTime.Now;
                    objfamily.MODIFIED_USER = User.Identity.Name;
                    _dbcontext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: SaveProductSpecs", ex);
            }
        }
        [System.Web.Http.HttpPost]
        public void SaveCommonSubProductSpecs(string selectedprodids, int catalogId, int familyid, string categoryid, string commonvalue, int attributeid, string imageCaption, object model)
        {
            var productid = selectedprodids.Split(',');
            var string_val = JsonConvert.DeserializeObject<Dictionary<string, string>>(model.ToString());
            string value = string_val["STRING_VALUE"];
            foreach (var s in productid)
            {
                int prodid = Convert.ToInt32(s);
                SaveSubProductSpecs(prodid, catalogId, familyid, categoryid, value, attributeid, imageCaption);
            }
        }


        public void SaveSubProductSpecs(int productId, int catalogId, int familyId, string categoryid, string commonvalue, int attributeid, string imageCaption)
        {
            try
            {
                bool publish = true;
                bool publish2Print = true;
                int workflowstatus = 0;
                int preVal = 0;
                int orgVal = 0;
                string categoryId = string.Empty;

                if (categoryid.Trim() != "")
                {
                    var id = categoryid.Split('~');
                    categoryId = Convert.ToString(id[0]);

                }
                // var objproductpublish = _dbcontext.TB_PROD_FAMILY.FirstOrDefault(p => p.FAMILY_ID == familyId && p.PRODUCT_ID == productId);
                var attributeId = attributeid;
                var attributedetails = _dbcontext.TB_ATTRIBUTE.Find(attributeId);
                string attributeName = attributedetails.ATTRIBUTE_NAME;
                var attributeType = attributedetails.ATTRIBUTE_TYPE;
                var attributeDatatype = attributedetails.ATTRIBUTE_DATATYPE;
                var attributeDataformat = attributedetails.ATTRIBUTE_DATAFORMAT;
                var stringvalue = commonvalue;
                string attributesize = Convert.ToString(attributedetails.ATTRIBUTE_DATATYPE);
                int textLength = 0;
                //if (!string.IsNullOrEmpty(stringvalue))
                //{
                //    stringvalue = stringvalue.Replace("\n", "");
                //    if (stringvalue.ToLower().StartsWith("<p>") && stringvalue.ToLower().EndsWith("</p>"))
                //    {
                //        stringvalue = stringvalue.Replace("<p>", "");
                //        stringvalue = stringvalue.Replace("</p>", "");
                //    }
                //}
                if (!string.IsNullOrEmpty(stringvalue))
                {
                    if (stringvalue.ToLower().EndsWith("\n"))
                    {
                        int sd = stringvalue.LastIndexOf("\n", StringComparison.Ordinal);
                        stringvalue = stringvalue.Remove(sd);
                    }
                    if (stringvalue.ToLower().EndsWith("\r"))
                    {
                        int sd = stringvalue.LastIndexOf("\r", StringComparison.Ordinal);
                        stringvalue = stringvalue.Remove(sd);
                    }
                    if (stringvalue.ToLower().StartsWith("<p>") && stringvalue.ToLower().EndsWith("</p>"))
                    {
                        int count = 0;
                        int i = 0;
                        while ((i = stringvalue.IndexOf("<p>", i, System.StringComparison.Ordinal)) != -1)
                        {
                            i += "<p>".Length;
                            count++;
                        }
                        if (count == 1)
                        {
                            int endp = stringvalue.LastIndexOf("</p>", StringComparison.Ordinal);
                            stringvalue = stringvalue.Remove(endp);
                            int p = stringvalue.IndexOf("<p>", StringComparison.Ordinal);
                            stringvalue = stringvalue.Remove(p, 3);
                        }
                    }
                    stringvalue = stringvalue.Replace("\r", "");
                    stringvalue = stringvalue.Replace("\n\n", "\n");
                    stringvalue = stringvalue.Replace("\n", "\r\n");
                    //stringvalue = stringvalue.Replace("<br />", "\r\n");
                    textLength = stringvalue.Length;
                }

                if (attributeType != 6)
                {
                    var objproductSpecs = _dbcontext.TB_PROD_SPECS.FirstOrDefault(s => s.PRODUCT_ID == productId && s.ATTRIBUTE_ID == attributeId);
                    var subproductcheck = _dbcontext.TB_SUBPRODUCT.Where(a => a.SUBPRODUCT_ID == productId);
                    if (objproductSpecs == null && subproductcheck.Any())
                    {
                        SaveProdSpecforNull(stringvalue, attributeType, attributeDatatype, productId, attributeId, attributesize, textLength, attributeDataformat, imageCaption, string.Empty);
                    }
                    else
                    {
                        if (attributeType == 4)
                        {
                            if (string.IsNullOrEmpty(stringvalue))
                            {
                                objproductSpecs.NUMERIC_VALUE = null;
                                _dbcontext.SaveChanges();
                            }
                            else
                            {
                                decimal numericvalue;
                                Decimal.TryParse(stringvalue, out numericvalue);
                                objproductSpecs.NUMERIC_VALUE = numericvalue;
                                _dbcontext.SaveChanges();
                            }
                        }
                        else if (attributeType == 3)
                        {
                            if (stringvalue == "")
                            {
                                objproductSpecs.STRING_VALUE = imageCaption;
                            }
                            else if (stringvalue.Contains(@"//Content") || stringvalue.Contains(@"//ImageandAttFile"))
                            {
                                objproductSpecs.STRING_VALUE = objproductSpecs.STRING_VALUE;
                            }
                            else
                            {
                                objproductSpecs.STRING_VALUE = stringvalue;
                            }
                            objproductSpecs.OBJECT_NAME = imageCaption;
                            _dbcontext.SaveChanges();

                        }
                        else
                        {
                            if (attributeDatatype.ToLower().Contains("number"))
                            {
                                if (string.IsNullOrEmpty(stringvalue))
                                {
                                    objproductSpecs.NUMERIC_VALUE = null;
                                    _dbcontext.SaveChanges();
                                }
                                else
                                {
                                    decimal numericvalue;
                                    Decimal.TryParse(stringvalue, out numericvalue);
                                    objproductSpecs.NUMERIC_VALUE = numericvalue;
                                    _dbcontext.SaveChanges();
                                }
                            }
                            else
                            {
                                if (attributesize.Contains('('))
                                {
                                    string[] sizearray = attributesize.Split('(');
                                    string[] size = sizearray[1].Split(')');
                                    if (textLength <= Convert.ToInt32(size[0]))
                                    {
                                        objproductSpecs.STRING_VALUE = stringvalue;
                                    }
                                }
                                else
                                {
                                    if (attributeDatatype.Contains("Date"))
                                    {
                                        if (attributeDataformat == @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))"
                                            || attributeDataformat == @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2}\s\w{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))")
                                        {
                                            var val = stringvalue.Split('M');
                                            if (val[1].Trim() == "")
                                            {
                                                objproductSpecs.STRING_VALUE = stringvalue;
                                            }
                                        }
                                        else if (attributeDataformat == @"(?=dd(-|/|.)dd(-|/|.)d{4})(?=.{0}(?:0[1-9]|[12]d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(-|/|.)02(-|/|.)...[13579])(?!29(-|/|.)02(-|/|.)..[13579][048])(?!29(-|/|.)02(-|/|.)..[02468][26])(?!29(-|/|.)02(-|/|.).[13579]00)(?!29(-|/|.)02(-|/|.)[13579][048]00)(?!29(-|/|.)02(-|/|.)[02468][26]00)")
                                        {
                                            objproductSpecs.STRING_VALUE = stringvalue;
                                        }
                                        else if (attributeDataformat == "System default settings")
                                        {
                                            objproductSpecs.STRING_VALUE = stringvalue;
                                        }
                                        else
                                        {

                                            objproductSpecs.STRING_VALUE = stringvalue;
                                        }
                                    }
                                    else
                                    {
                                        objproductSpecs.STRING_VALUE = stringvalue;
                                    }

                                }
                            }
                            objproductSpecs.MODIFIED_USER = User.Identity.Name;
                            objproductSpecs.MODIFIED_DATE = DateTime.Now;
                            _dbcontext.SaveChanges();
                        }
                    }
                }

                else
                {
                    var subproductCheck = _dbcontext.TB_SUBPRODUCT.Where(a => a.SUBPRODUCT_ID == productId);
                    if (!subproductCheck.Any())
                    {
                        var objpartskey = _dbcontext.TB_SUBPRODUCT_KEY.FirstOrDefault(x => x.PRODUCT_ID == productId && x.ATTRIBUTE_ID == attributeId && x.FAMILY_ID == familyId && x.CATALOG_ID == catalogId && x.CATEGORY_ID == categoryId);
                        if (objpartskey != null)
                        {
                            objpartskey.ATTRIBUTE_VALUE = stringvalue;
                            _dbcontext.SaveChanges();

                        }
                        else
                        {
                            var objparts = new TB_PARTS_KEY
                            {
                                ATTRIBUTE_ID = attributeId,
                                PRODUCT_ID = productId,
                                FAMILY_ID = familyId,
                                ATTRIBUTE_VALUE = stringvalue,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                CATALOG_ID = catalogId,
                                CATEGORY_ID = categoryId
                            };
                            _dbcontext.TB_PARTS_KEY.Add(objparts);
                            _dbcontext.SaveChanges();
                        }
                    }

                    else
                    {
                        var objsubprokeyCheck = _dbcontext.TB_SUBPRODUCT_KEY.FirstOrDefault(a => a.CATALOG_ID == catalogId && a.CATEGORY_ID == categoryId && a.FAMILY_ID == familyId && a.SUBPRODUCT_ID == productId && a.ATTRIBUTE_ID == attributeId);
                        if (objsubprokeyCheck != null)
                        {
                            objsubprokeyCheck.ATTRIBUTE_VALUE = stringvalue;
                            _dbcontext.SaveChanges();
                        }
                        else
                        {
                            var objparts = new TB_SUBPRODUCT_KEY
                            {
                                ATTRIBUTE_ID = attributeId,
                                PRODUCT_ID = subproductCheck.Max(a => a.PRODUCT_ID),
                                SUBPRODUCT_ID = productId,
                                FAMILY_ID = familyId,
                                ATTRIBUTE_VALUE = stringvalue,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                CATALOG_ID = catalogId,
                                CATEGORY_ID = categoryId
                            };
                            _dbcontext.TB_SUBPRODUCT_KEY.Add(objparts);
                            _dbcontext.SaveChanges();
                        }
                    }
                }
                var sub_prod_attrb = _dbcontext.TB_SUBPRODUCT_ATTR_LIST.Where(a => a.SUBPRODUCT_ID == 0);
                int sort_order_attrlist = 0;
                if (sub_prod_attrb.Any())
                {


                    var attrlist = _dbcontext.TB_SUBPRODUCT_ATTR_LIST.Where(a => a.SUBPRODUCT_ID == 0 && a.ATTRIBUTE_ID == attributeId);
                    if (!attrlist.Any())
                    {
                        sort_order_attrlist = sub_prod_attrb.Max(a => a.SORT_ORDER);
                        sort_order_attrlist++;




                        var objProdFamilyAttrList = new TB_SUBPRODUCT_ATTR_LIST
                        {
                            ATTRIBUTE_ID = attributeId,
                            SORT_ORDER = sort_order_attrlist,
                            SUBPRODUCT_ID = 0,
                            PRODUCT_ID = sub_prod_attrb.Max(a => a.PRODUCT_ID),
                            CREATED_USER = User.Identity.Name,
                            CREATED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now

                        };

                        _dbcontext.TB_SUBPRODUCT_ATTR_LIST.Add(objProdFamilyAttrList);
                    }



                }

                // }
                // }

                //  }
                //SaveCalculatedProdSpecs(productId, familyId, catalogId);
                //publish
                //if (objproductpublish != null)
                //{

                //    objproductpublish.PUBLISH = publish;
                //    objproductpublish.PUBLISH2PRINT = publish2Print;
                //    objproductpublish.WORKFLOW_STATUS = workflowstatus;
                //    objproductpublish.MODIFIED_USER = User.Identity.Name;
                //    objproductpublish.MODIFIED_DATE = DateTime.Now;
                //    _dbcontext.SaveChanges();

                //    //var objfamilySpecs = _dbcontext.TB_FAMILY_SPECS.FirstOrDefault(p => p.FAMILY_ID == familyId);
                //    //objfamilySpecs.MODIFIED_DATE = DateTime.Now;
                //    //objfamilySpecs.MODIFIED_USER = User.Identity.Name;
                //    //_dbcontext.SaveChanges();

                //    var objfamily = _dbcontext.TB_FAMILY.FirstOrDefault(p => p.FAMILY_ID == familyId);
                //    objfamily.MODIFIED_DATE = DateTime.Now;
                //    objfamily.MODIFIED_USER = User.Identity.Name;
                _dbcontext.SaveChanges();
                //}
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: SaveProductSpecs", ex);
            }
        }
        public void SaveProdSpecforNull(string stringvalue, int attributeType, string attributeDatatype, int productId, int attributeId, string attributesize, int textLength, string attributeDataformat, string imageCaption, string value)
        {
            if (attributeType == 1)
            {
                if (attributeDatatype.ToLower().Contains("number"))
                {
                    if (string.IsNullOrEmpty(stringvalue))
                    {
                        var objprodspecs = new TB_PROD_SPECS
                        {
                            STRING_VALUE = null,
                            NUMERIC_VALUE = null,
                            PRODUCT_ID = productId,
                            ATTRIBUTE_ID = attributeId,
                            CREATED_DATE = DateTime.Now,
                            CREATED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name
                        };
                        _dbcontext.TB_PROD_SPECS.Add(objprodspecs);
                    }
                    else
                    {
                        decimal numericvalue;
                        Decimal.TryParse(stringvalue, out numericvalue);
                        var objprodspecs = new TB_PROD_SPECS
                        {
                            STRING_VALUE = null,
                            NUMERIC_VALUE = numericvalue,
                            PRODUCT_ID = productId,
                            ATTRIBUTE_ID = attributeId,
                            CREATED_DATE = DateTime.Now,
                            CREATED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name
                        };
                        _dbcontext.TB_PROD_SPECS.Add(objprodspecs);
                    }
                }
                else
                {
                    if (attributesize.Contains('('))
                    {
                        string[] sizearray = attributesize.Split('(');
                        string[] size = sizearray[1].Split(')');
                        if (textLength <= Convert.ToInt32(size[0]))
                        {
                            var objprodspecs = new TB_PROD_SPECS
                            {
                                STRING_VALUE = stringvalue,
                                NUMERIC_VALUE = null,
                                PRODUCT_ID = productId,
                                ATTRIBUTE_ID = attributeId,
                                CREATED_DATE = DateTime.Now,
                                CREATED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name
                            };
                            _dbcontext.TB_PROD_SPECS.Add(objprodspecs);
                        }
                    }
                    else
                    {
                        if (attributeDatatype.Contains("Date"))
                        {
                            if (attributeDataformat == @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))"
                                || attributeDataformat == @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2}\s\w{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))")
                            {
                                var val = stringvalue.Split('M');
                                if (val[1].Trim() == "")
                                {
                                    var objprodspecs = new TB_PROD_SPECS
                                    {
                                        STRING_VALUE = stringvalue,
                                        NUMERIC_VALUE = null,
                                        PRODUCT_ID = productId,
                                        ATTRIBUTE_ID = attributeId,
                                        CREATED_DATE = DateTime.Now,
                                        CREATED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now,
                                        MODIFIED_USER = User.Identity.Name
                                    };
                                    _dbcontext.TB_PROD_SPECS.Add(objprodspecs);
                                }
                            }
                            else if (attributeDataformat == @"(?=dd(-|/|.)dd(-|/|.)d{4})(?=.{0}(?:0[1-9]|[12]d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(-|/|.)02(-|/|.)...[13579])(?!29(-|/|.)02(-|/|.)..[13579][048])(?!29(-|/|.)02(-|/|.)..[02468][26])(?!29(-|/|.)02(-|/|.).[13579]00)(?!29(-|/|.)02(-|/|.)[13579][048]00)(?!29(-|/|.)02(-|/|.)[02468][26]00)")
                            {
                                var objprodspecs = new TB_PROD_SPECS
                                {
                                    STRING_VALUE = stringvalue,
                                    NUMERIC_VALUE = null,
                                    PRODUCT_ID = productId,
                                    ATTRIBUTE_ID = attributeId,
                                    CREATED_DATE = DateTime.Now,
                                    CREATED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now,
                                    MODIFIED_USER = User.Identity.Name
                                };
                                _dbcontext.TB_PROD_SPECS.Add(objprodspecs);
                            }
                            else if (attributeDataformat == "System default settings")
                            {
                                var objprodspecs = new TB_PROD_SPECS
                                {
                                    STRING_VALUE = stringvalue,
                                    NUMERIC_VALUE = null,
                                    PRODUCT_ID = productId,
                                    ATTRIBUTE_ID = attributeId,
                                    CREATED_DATE = DateTime.Now,
                                    CREATED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now,
                                    MODIFIED_USER = User.Identity.Name
                                };
                                _dbcontext.TB_PROD_SPECS.Add(objprodspecs);
                            }
                            else
                            {

                                var objprodspecs = new TB_PROD_SPECS
                                {
                                    STRING_VALUE = stringvalue,
                                    NUMERIC_VALUE = null,
                                    PRODUCT_ID = productId,
                                    ATTRIBUTE_ID = attributeId,
                                    CREATED_DATE = DateTime.Now,
                                    CREATED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now,
                                    MODIFIED_USER = User.Identity.Name
                                };
                                _dbcontext.TB_PROD_SPECS.Add(objprodspecs);
                            }
                        }
                        else
                        {
                            var objprodspecs = new TB_PROD_SPECS
                            {
                                STRING_VALUE = stringvalue,
                                NUMERIC_VALUE = null,
                                PRODUCT_ID = productId,
                                ATTRIBUTE_ID = attributeId,
                                CREATED_DATE = DateTime.Now,
                                CREATED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name
                            };
                            _dbcontext.TB_PROD_SPECS.Add(objprodspecs);
                        }

                    }
                }
            }
            else if (attributeType == 4)
            {
                decimal numericvalue;
                Decimal.TryParse(stringvalue, out numericvalue);
                var objprodspecs = new TB_PROD_SPECS
                {
                    STRING_VALUE = null,
                    NUMERIC_VALUE = numericvalue,
                    PRODUCT_ID = productId,
                    ATTRIBUTE_ID = attributeId,
                    CREATED_DATE = DateTime.Now,
                    CREATED_USER = User.Identity.Name,
                    MODIFIED_DATE = DateTime.Now,
                    MODIFIED_USER = User.Identity.Name
                };
                _dbcontext.TB_PROD_SPECS.Add(objprodspecs);
            }
            else if (attributeType == 3)
            {

                if (stringvalue.Contains(@"//Content") || stringvalue.Contains(@"//ImageandAttFile"))
                {
                    var objprodspecs = new TB_PROD_SPECS
                    {
                        STRING_VALUE = null,
                        NUMERIC_VALUE = null,
                        PRODUCT_ID = productId,
                        ATTRIBUTE_ID = attributeId,
                        CREATED_DATE = DateTime.Now,
                        CREATED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now,
                        MODIFIED_USER = User.Identity.Name,
                        OBJECT_NAME = imageCaption
                    };
                    _dbcontext.TB_PROD_SPECS.Add(objprodspecs);
                }
                else
                {
                    var objprodspecs = new TB_PROD_SPECS
                    {
                        STRING_VALUE = stringvalue,
                        NUMERIC_VALUE = null,
                        PRODUCT_ID = productId,
                        ATTRIBUTE_ID = attributeId,
                        CREATED_DATE = DateTime.Now,
                        CREATED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now,
                        MODIFIED_USER = User.Identity.Name
                    };
                    _dbcontext.TB_PROD_SPECS.Add(objprodspecs);
                }
            }

            _dbcontext.SaveChanges();
        }


        [System.Web.Http.HttpGet]
        public List<ProductView> GetCategoriessubProducts(int catalogid, string categoryId)
        {
            try
            {
                return GetCategoriesForTree(catalogid, categoryId, 1);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : WizardCategories", objexception);
                return null;
            }
        }
        public List<ProductView> GetSubCategoriesForTree(int catalogId, string id, int workingCatalogId)
        {
            try
            {
                if (id == "undefined")
                {
                    if (catalogId != 1)
                    {
                        var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                            tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                            .Where(x => x.tpf.User_Name == User.Identity.Name)
                            .Select(x => x.tps);
                        var customerSettings = customersettings.FirstOrDefault();
                        bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                        var firstOrDefault = customersettings.FirstOrDefault();
                        bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                        var aa =
                            _dbcontext.TB_CATALOG_SECTIONS.Where(
                                a => a.CATALOG_ID == catalogId && a.TB_CATEGORY.PARENT_CATEGORY == "0")
                                .Select(
                                    a =>
                                        new ProductView
                                        {
                                            id = a.CATEGORY_ID,
                                            CATEGORY_ID = a.CATEGORY_ID,
                                            CATEGORY_NAME = a.TB_CATEGORY.CATEGORY_NAME,
                                            CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                            CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                            DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                            VERSION = a.TB_CATALOG.VERSION,
                                            hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID,
                                                (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) ||
                                                _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId)
                                                    .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbcontext.TB_FAMILY.Join(_dbcontext.TB_PROD_FAMILY, tf => tf.FAMILY_ID,
                                                        tpf => tpf.FAMILY_ID, (tf, tpf) => new { tf, tpf }).Join(_dbcontext.TB_SUBPRODUCT, tftpf => tftpf.tpf.PRODUCT_ID,
                                                        ts => ts.PRODUCT_ID, (tftpf, ts) => new { tftpf, ts }).Where(x => x.ts.CATALOG_ID == catalogId).Select(b => b.tftpf.tf.FAMILY_ID).Contains(c.FAMILY_ID)),
                                            SORT_ORDER = a.SORT_ORDER,
                                            CategoryIdinNavigator = categoryidinnavigator,
                                            FamilyandRelatedFamily = familyandrelatedfamily,
                                            spriteCssClass = a.TB_CATEGORY.IS_CLONE == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category"
                                        }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();
                        return aa;
                    }
                    else
                    {

                        var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                          tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                          .Where(x => x.tpf.User_Name == User.Identity.Name)
                          .Select(x => x.tps);
                        //string[] names;
                        //DataSet _dscatids = new DataSet();
                        //string _sqlCatLevel = "select c.category_id from TB_CATEGORY c join TB_CATALOG_SECTIONS cs on cs.CATEGORY_ID = c.CATEGORY_ID " +
                        //                      "and c.PARENT_CATEGORY = '0' join Customer_Catalog cc on cc.CATALOG_ID = cs.CATALOG_ID and cc.CustomerId = 11";
                        //_SQLString = _sqlCatLevel;
                        //_dscatids = CreateDataSet();
                        //names = new string[_dscatids.Tables[0].Rows.Count];
                        //for (int a = 0; a < _dscatids.Tables[0].Rows.Count; a++)
                        //{
                        //    names[a] = _dscatids.Tables[0].Rows[a]["category_id"].ToString();
                        //}


                        int userroleid = 0;
                        var SuperAdminCheck = _dbcontext.vw_aspnet_UsersInRoles
                        .Join(_dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                        .Join(_dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                        .Where(x => x.tcptps.tpf.UserName == User.Identity.Name).ToList();
                        if (SuperAdminCheck.Any())
                        {
                            userroleid = Convert.ToInt16(SuperAdminCheck[0].tcp.Role_id);
                        }

                        var customerSettings = customersettings.FirstOrDefault();
                        bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                        var firstOrDefault = customersettings.FirstOrDefault();
                        bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;

                        var aa = _dbcontext.TB_CATALOG_SECTIONS.Where(
                             a => a.CATALOG_ID == catalogId && a.TB_CATEGORY.PARENT_CATEGORY == "0")
                             .Select(
                                 a =>
                                     new ProductView
                                     {
                                         id = a.CATEGORY_ID,
                                         CATEGORY_ID = a.CATEGORY_ID,
                                         CATEGORY_NAME = a.TB_CATEGORY.CATEGORY_NAME,
                                         CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                         CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                         DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                         VERSION = a.TB_CATALOG.VERSION,
                                         hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID,
                                             (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) ||
                                             _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId)
                                                 .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbcontext.TB_FAMILY.Join(_dbcontext.TB_PROD_FAMILY, tf => tf.FAMILY_ID,
                                                        tpf => tpf.FAMILY_ID, (tf, tpf) => new { tf, tpf }).Join(_dbcontext.TB_SUBPRODUCT, tftpf => tftpf.tpf.PRODUCT_ID,
                                                        ts => ts.PRODUCT_ID, (tftpf, ts) => new { tftpf, ts }).Where(x => x.ts.CATALOG_ID == catalogId).Select(b => b.tftpf.tf.FAMILY_ID).Contains(c.FAMILY_ID)),
                                         SORT_ORDER = a.SORT_ORDER,
                                         CategoryIdinNavigator = categoryidinnavigator,
                                         FamilyandRelatedFamily = familyandrelatedfamily,
                                         spriteCssClass = a.TB_CATEGORY.IS_CLONE == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category",

                                     }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();

                        if (userroleid == 1)
                        {
                            var names = _dbcontext.TB_CATEGORY
                              .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf })
                              .Join(_dbcontext.Customer_Catalog, tcptps => tcptps.tpf.CATALOG_ID, tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                              .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId, (tcptpscs, cs) => new { tcptpscs, cs })
                              .Where(x => x.cs.User_Name == User.Identity.Name && x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" && x.tcptpscs.tcptps.tpf.CATALOG_ID != 1).Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);
                            var results = aa.Where(b => names.Contains(b.id)).ToList();
                            return results;
                        }
                        else
                        {
                            var names = _dbcontext.TB_CATEGORY
                              .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf })
                              .Join(_dbcontext.Customer_Role_Catalog, tcptps => tcptps.tpf.CATALOG_ID, tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                              .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId, (tcptpscs, cs) => new { tcptpscs, cs })
                              .Where(x => x.cs.User_Name == User.Identity.Name && x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" && x.tcptpscs.tcptps.tpf.CATALOG_ID != 1 && x.tcptpscs.tcp.RoleId == userroleid && x.tcptpscs.tcp.IsActive == true).Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);

                            var results = aa.Where(b => names.Contains(b.id)).ToList();
                            return results;
                        }



                    }
                }
                else
                {
                    string subfamilycategoryid = string.Empty;
                    if (id.Contains("~"))
                    {
                        var catgoryandfamilyid = id.Split('~');
                        subfamilycategoryid = catgoryandfamilyid[0];
                        id = catgoryandfamilyid[1];
                    }
                    var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                          tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                          .Where(x => x.tpf.User_Name == User.Identity.Name)
                          .Select(x => x.tps);
                    var customerSettings = customersettings.FirstOrDefault();
                    bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                    var firstOrDefault = customersettings.FirstOrDefault();
                    bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;


                    var aa = _dbcontext.TB_CATALOG_SECTIONS.Where(
                        a => a.CATALOG_ID == catalogId && a.TB_CATEGORY.PARENT_CATEGORY == id)
                        .Select(
                            a =>
                                new ProductView
                                {
                                    id = a.CATEGORY_ID,
                                    CATEGORY_ID = a.CATEGORY_ID,
                                    CATEGORY_NAME = a.TB_CATEGORY.CATEGORY_NAME,
                                    CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                    CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                    DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                    VERSION = a.TB_CATALOG.VERSION,
                                    hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID,
                                        (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) ||
                                        _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId)
                                            .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbcontext.TB_FAMILY.Join(_dbcontext.TB_PROD_FAMILY, tf => tf.FAMILY_ID,
                                                        tpf => tpf.FAMILY_ID, (tf, tpf) => new { tf, tpf }).Join(_dbcontext.TB_SUBPRODUCT, tftpf => tftpf.tpf.PRODUCT_ID,
                                                        ts => ts.PRODUCT_ID, (tftpf, ts) => new { tftpf, ts }).Where(x => x.ts.CATALOG_ID == catalogId).Select(b => b.tftpf.tf.FAMILY_ID).Contains(c.FAMILY_ID)),
                                    SORT_ORDER = a.SORT_ORDER,
                                    CategoryIdinNavigator = categoryidinnavigator,
                                    FamilyandRelatedFamily = familyandrelatedfamily,
                                    spriteCssClass = a.TB_CATEGORY.IS_CLONE == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category",

                                }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);

                    var bb = Array.ConvertAll(_dbcontext.TB_CATALOG_FAMILY.Where(
                                                 a => a.CATALOG_ID == catalogId && a.CATEGORY_ID == id && a.TB_FAMILY.ROOT_FAMILY == 1)
                                                 .Select(a
                                                     =>
                                                     new
                                                     {
                                                         a.FAMILY_ID,
                                                         a.TB_FAMILY.FAMILY_NAME,
                                                         a.TB_CATALOG.CATALOG_ID,
                                                         a.TB_CATALOG.CATALOG_NAME,
                                                         a.TB_CATALOG.DESCRIPTION,
                                                         a.TB_CATALOG.VERSION,
                                                         hasChildren = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tf => tf.FAMILY_ID,
                                                         (tps, tf) => new { tps, tf })
                                                         .Join(_dbcontext.TB_PROD_FAMILY, tcftf => tcftf.tf.FAMILY_ID, tpf => tpf.FAMILY_ID,
                                                         (tcftf, tpf) => new { tcftf, tpf })
                                                         .Join(_dbcontext.TB_SUBPRODUCT, tcftftpf => tcftftpf.tpf.PRODUCT_ID, ts => ts.PRODUCT_ID,
                                                         (tcftftpf, ts) => new { tcftftpf, ts }).Any(x => x.tcftftpf.tcftf.tps.CATALOG_ID == catalogId && x.tcftftpf.tcftf.tf.PARENT_FAMILY_ID == a.FAMILY_ID
                                                             && x.tcftftpf.tcftf.tps.CATEGORY_ID == id && x.ts.CATALOG_ID == catalogId),
                                                         ProdCnt =
                                                             a.TB_FAMILY.TB_PROD_FAMILY.Where(b => b.FAMILY_ID == a.FAMILY_ID)
                                                                 .Select(b => b.PRODUCT_ID)
                                                                 .Distinct()
                                                                 .Count(),
                                                         SubProdCnt =
                                                             a.TB_FAMILY.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, ts => ts.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (ts, tpf) => new { ts, tpf }).Where(b => b.ts.TB_FAMILY.FAMILY_ID == a.FAMILY_ID && b.tpf.CATALOG_ID == catalogId)
                                                             .Select(b => b.tpf.SUBPRODUCT_ID)
                                                             .Distinct()
                                                             .Count(),
                                                         a.SORT_ORDER
                                                     }).ToArray(),
                                             a =>
                                                 new ProductView
                                                 {
                                                     id = id + "~" + a.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                                     CATEGORY_ID = "~" + a.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                                     CATEGORY_NAME = familyandrelatedfamily ? a.SubProdCnt > 0 ? a.FAMILY_NAME + "(" + a.ProdCnt.ToString(CultureInfo.InvariantCulture) + "^)" : a.FAMILY_NAME + "(" + a.ProdCnt.ToString(CultureInfo.InvariantCulture) + ")"
                                                           : a.FAMILY_NAME,
                                                     CATALOG_ID = a.CATALOG_ID,
                                                     CATALOG_NAME = a.CATALOG_NAME,
                                                     DESCRIPTION = a.DESCRIPTION,
                                                     VERSION = a.VERSION,
                                                     hasChildren = a.hasChildren,
                                                     SORT_ORDER = a.SORT_ORDER,
                                                     CategoryIdinNavigator = false,
                                                     FamilyandRelatedFamily = familyandrelatedfamily,
                                                     spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.FAMILY_ID && x.tps.CATEGORY_ID == id) ?
                                                     "familyClone" : "family",


                                                 }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);

                    if (aa.Any())
                    {
                        var res = aa.ToList().Union(bb.ToList());
                        return res.ToList();
                    }
                    if (bb.Any())
                    {

                        return bb.ToList();
                    }
                    else
                    {
                        int fid;
                        int.TryParse(id.Trim('~'), out fid);
                        //int fid = Convert.ToInt32(id);
                        var cc = Array.ConvertAll((_dbcontext.TB_SUBFAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID,
                                tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf })
                                .Join(_dbcontext.TB_CATALOG_FAMILY, tcptps => tcptps.tps.SUBFAMILY_ID, tcp => tcp.FAMILY_ID,
                                    (tcptps, tcp) => new { tcptps, tcp })
                                .Where(x => x.tcp.CATALOG_ID == catalogId && x.tcptps.tps.FAMILY_ID == fid && x.tcp.CATEGORY_ID == subfamilycategoryid)).ToArray(),
                            a => new
                            {
                                FAMILY_ID = a.tcptps.tps.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                SUBFAMILY_ID = a.tcptps.tps.SUBFAMILY_ID,
                                ProdCnt =
                                    _dbcontext.TB_PROD_FAMILY.Where(b => b.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID)
                                        .Distinct()
                                        .Count(),
                                SubProdCnt = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, ts => ts.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (ts, tpf) => new { ts, tpf }).Where(b => b.ts.TB_FAMILY.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID && b.tpf.CATALOG_ID == catalogId)
                                             .Select(b => b.tpf.SUBPRODUCT_ID)
                                             .Distinct()
                                             .Count(),
                                a.tcp.TB_CATALOG.CATALOG_ID,
                                a.tcp.TB_CATALOG.CATALOG_NAME,
                                a.tcp.TB_CATALOG.DESCRIPTION,
                                a.tcp.TB_CATALOG.VERSION,
                                a.tcptps.tps.SORT_ORDER,
                                hasChildren = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tf => tf.FAMILY_ID,
                                                         (tps, tf) => new { tps, tf })
                                                         .Join(_dbcontext.TB_PROD_FAMILY, tcftf => tcftf.tf.FAMILY_ID, tpf => tpf.FAMILY_ID,
                                                         (tcftf, tpf) => new { tcftf, tpf })
                                                         .Join(_dbcontext.TB_SUBPRODUCT, tcftftpf => tcftftpf.tpf.PRODUCT_ID, ts => ts.PRODUCT_ID,
                                                         (tcftftpf, ts) => new { tcftftpf, ts }).Any(x => x.tcftftpf.tcftf.tps.CATALOG_ID == catalogId
                                                             && x.tcftftpf.tcftf.tf.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID
                                                             && x.tcftftpf.tcftf.tps.CATEGORY_ID == id && x.ts.CATALOG_ID == catalogId),
                            })
                            .Select(a => new ProductView
                            {
                                id = subfamilycategoryid + "~" + a.SUBFAMILY_ID,
                                CATEGORY_ID = "~" + a.SUBFAMILY_ID,
                                CATEGORY_NAME = familyandrelatedfamily ? a.SubProdCnt > 0 ?
                                   _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault() + "(" + a.ProdCnt + "^)" : _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault() + "(" + a.ProdCnt + ")" : _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault(),
                                CATALOG_ID = a.CATALOG_ID,
                                CATALOG_NAME = a.CATALOG_NAME,
                                DESCRIPTION = a.DESCRIPTION,
                                VERSION = a.VERSION,
                                SORT_ORDER = a.SORT_ORDER,
                                CategoryIdinNavigator = false,
                                FamilyandRelatedFamily = familyandrelatedfamily,
                                hasChildren = a.hasChildren,
                                spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.SUBFAMILY_ID && x.tps.CATEGORY_ID == subfamilycategoryid) ?
                                "subfamilyClone" : "subfamily",
                            }).OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id);


                        var dd = Array.ConvertAll((_dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID,
                                tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf })
                                .Join(_dbcontext.TB_SUBPRODUCT, tcptps => tcptps.tps.PRODUCT_ID, tcp => tcp.PRODUCT_ID,
                                    (tcptps, tcp) => new { tcptps, tcp })
                                    .Join(_dbcontext.TB_PROD_SPECS, tcptps1 => tcptps1.tcp.SUBPRODUCT_ID, tcp => tcp.PRODUCT_ID,
                                    (tcptps1, tcp1) => new { tcptps1, tcp1 })
                                .Where(x => x.tcptps1.tcp.CATALOG_ID == catalogId && x.tcptps1.tcptps.tps.FAMILY_ID == fid && x.tcp1.ATTRIBUTE_ID == 1)).ToArray(),
                            a => new
                            {
                                FAMILY_ID = a.tcp1.PRODUCT_ID.ToString(CultureInfo.InvariantCulture),
                                SUBFAMILY_ID = a.tcp1.PRODUCT_ID,
                                FAMILY_NAME = a.tcp1.STRING_VALUE,
                                ProdCnt = _dbcontext.TB_SUBPRODUCT.Where(b => b.PRODUCT_ID == a.tcptps1.tcp.PRODUCT_ID)
                                        .Distinct()
                                        .Count(),
                                SubProdCnt = _dbcontext.TB_SUBPRODUCT.Where(b => b.PRODUCT_ID == a.tcptps1.tcp.PRODUCT_ID)
                                        .Distinct()
                                        .Count(),
                                a.tcptps1.tcp.TB_CATALOG.CATALOG_ID,
                                a.tcptps1.tcp.TB_CATALOG.CATALOG_NAME,
                                a.tcptps1.tcp.TB_CATALOG.DESCRIPTION,
                                a.tcptps1.tcp.TB_CATALOG.VERSION,
                                a.tcptps1.tcp.SORT_ORDER
                            })
                            .Select(a => new ProductView
                            {
                                id = subfamilycategoryid + "~" + a.SUBFAMILY_ID,
                                CATEGORY_ID = "~" + a.SUBFAMILY_ID,
                                CATEGORY_NAME = a.FAMILY_NAME + "(" + a.ProdCnt + "^)",
                                CATALOG_ID = a.CATALOG_ID,
                                CATALOG_NAME = a.CATALOG_NAME,
                                DESCRIPTION = a.DESCRIPTION,
                                VERSION = a.VERSION,
                                SORT_ORDER = a.SORT_ORDER,
                                CategoryIdinNavigator = false,
                                FamilyandRelatedFamily = familyandrelatedfamily,
                                hasChildren = false,
                                spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.SUBFAMILY_ID && x.tps.CATEGORY_ID == subfamilycategoryid) ?
                                "subfamilyClone" : "subfamily",


                            }).OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id);
                        //       CATALOG_ID = a, CATALOG_NAME = a.CATALOG_NAME, DESCRIPTION = a.DESCRIPTION, VERSION = a.VERSION,  , hasChildren = false });
                        //var cc = Array.ConvertAll(_dbcontext.TB_SUBFAMILY.Where(a => a.FAMILY_ID == fid).ToArray(),
                        //    a => new { FAMILY_ID = a.FAMILY_ID.ToString(CultureInfo.InvariantCulture), SUBFAMILY_ID = a.SUBFAMILY_ID,
                        //        ProdCnt = _dbcontext.TB_PROD_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Distinct().Count()})
                        //    .Select(a => new ProductView { id = "~" + a.SUBFAMILY_ID, CATEGORY_NAME = _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault() + "(" + a.ProdCnt + ")",
                        //       CATALOG_ID = a, CATALOG_NAME = a.CATALOG_NAME, DESCRIPTION = a.DESCRIPTION, VERSION = a.VERSION,  , hasChildren = false });
                        if (cc.Any())
                        {
                            var res = cc.ToList().Union(dd.ToList());
                            return res.ToList();
                        }
                        if (dd.Any())
                        {
                            var res = dd.ToList();
                            return res.ToList();
                        }
                        return null;
                    }
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : GetCategoriesForTree", objexception);
                return null;
            }

        }



        public List<ProductView> GetCategoriesForTree(int catalogId, string id, int workingCatalogId)
        {
            try
            {
                if (id == "undefined")
                {
                    if (catalogId != 1)
                    {
                        var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                            tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                            .Where(x => x.tpf.User_Name == User.Identity.Name)
                            .Select(x => x.tps);
                        var customerSettings = customersettings.FirstOrDefault();
                        bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                        var firstOrDefault = customersettings.FirstOrDefault();
                        bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                        var aa =
                            _dbcontext.TB_CATALOG_SECTIONS.Where(
                                a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && a.TB_CATEGORY.PARENT_CATEGORY == "0" && a.TB_CATEGORY.FLAG_RECYCLE == "A")
                                .Select(
                                    a =>
                                        new ProductView
                                        {
                                            id = a.CATEGORY_ID,
                                            CATEGORY_ID = a.CATEGORY_ID,
                                            CATEGORY_NAME = a.TB_CATEGORY.CATEGORY_NAME,
                                            CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                            CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                            DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                            VERSION = a.TB_CATALOG.VERSION,
                                            hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) ? _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID)
                                                    : _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                                        .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && c.FLAG_RECYCLE == "A" && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                            SORT_ORDER = a.SORT_ORDER,
                                            CategoryIdinNavigator = categoryidinnavigator,
                                            FamilyandRelatedFamily = familyandrelatedfamily,
                                            spriteCssClass = a.TB_CATEGORY.IS_CLONE == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category"
                                        }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();
                        return aa;
                    }
                    else
                    {

                        var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                          tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                          .Where(x => x.tpf.User_Name == User.Identity.Name)
                          .Select(x => x.tps);
                        //string[] names;
                        //DataSet _dscatids = new DataSet();
                        //string _sqlCatLevel = "select c.category_id from TB_CATEGORY c join TB_CATALOG_SECTIONS cs on cs.CATEGORY_ID = c.CATEGORY_ID " +
                        //                      "and c.PARENT_CATEGORY = '0' join Customer_Catalog cc on cc.CATALOG_ID = cs.CATALOG_ID and cc.CustomerId = 11";
                        //_SQLString = _sqlCatLevel;
                        //_dscatids = CreateDataSet();
                        //names = new string[_dscatids.Tables[0].Rows.Count];
                        //for (int a = 0; a < _dscatids.Tables[0].Rows.Count; a++)
                        //{
                        //    names[a] = _dscatids.Tables[0].Rows[a]["category_id"].ToString();
                        //}


                        int userroleid = 0;
                        var SuperAdminCheck = _dbcontext.vw_aspnet_UsersInRoles
                        .Join(_dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                        .Join(_dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                        .Where(x => x.tcptps.tpf.UserName == User.Identity.Name).ToList();
                        if (SuperAdminCheck.Any())
                        {
                            userroleid = Convert.ToInt16(SuperAdminCheck[0].tcp.Role_id);
                        }

                        var customerSettings = customersettings.FirstOrDefault();
                        bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                        var firstOrDefault = customersettings.FirstOrDefault();
                        bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;

                        var aa = _dbcontext.TB_CATALOG_SECTIONS.Where(
                             a => a.CATALOG_ID == catalogId && a.TB_CATEGORY.PARENT_CATEGORY == "0")
                             .Select(
                                 a =>
                                     new ProductView
                                     {
                                         id = a.CATEGORY_ID,
                                         CATEGORY_ID = a.CATEGORY_ID,
                                         CATEGORY_NAME = a.TB_CATEGORY.CATEGORY_NAME,
                                         CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                         CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                         DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                         VERSION = a.TB_CATALOG.VERSION,
                                         hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) ? _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID)
                                                 : _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                                     .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && c.FLAG_RECYCLE == "A" && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                         SORT_ORDER = a.SORT_ORDER,
                                         CategoryIdinNavigator = categoryidinnavigator,
                                         FamilyandRelatedFamily = familyandrelatedfamily,
                                         spriteCssClass = a.TB_CATEGORY.IS_CLONE == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category",

                                     }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();

                        if (userroleid == 1)
                        {
                            var names = _dbcontext.TB_CATEGORY
                              .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf })
                              .Join(_dbcontext.Customer_Catalog, tcptps => tcptps.tpf.CATALOG_ID, tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                              .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId, (tcptpscs, cs) => new { tcptpscs, cs })
                              .Where(x => x.cs.User_Name == User.Identity.Name && x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" && x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" && x.tcptpscs.tcptps.tpf.CATALOG_ID != 1).Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);
                            var results = aa.Where(b => names.Contains(b.id)).ToList();
                            return results;
                        }
                        else
                        {
                            var names = _dbcontext.TB_CATEGORY
                              .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf })
                              .Join(_dbcontext.Customer_Role_Catalog, tcptps => tcptps.tpf.CATALOG_ID, tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                              .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId, (tcptpscs, cs) => new { tcptpscs, cs })
                              .Where(x => x.cs.User_Name == User.Identity.Name && x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" && x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" && x.tcptpscs.tcptps.tpf.CATALOG_ID != 1 && x.tcptpscs.tcp.RoleId == userroleid && x.tcptpscs.tcp.IsActive == true).Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);

                            var results = aa.Where(b => names.Contains(b.id)).ToList();
                            return results;
                        }



                    }
                }
                else
                {
                    string subfamilycategoryid = string.Empty;
                    if (id.Contains("~"))
                    {
                        var catgoryandfamilyid = id.Split('~');
                        subfamilycategoryid = catgoryandfamilyid[0];
                        id = catgoryandfamilyid[1];
                    }
                    var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                          tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                          .Where(x => x.tpf.User_Name == User.Identity.Name)
                          .Select(x => x.tps);
                    var customerSettings = customersettings.FirstOrDefault();
                    bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                    var firstOrDefault = customersettings.FirstOrDefault();
                    bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;


                    var aa = _dbcontext.TB_CATALOG_SECTIONS.Where(
                        a => a.CATALOG_ID == catalogId && a.TB_CATEGORY.PARENT_CATEGORY == id)
                        .Select(
                            a =>
                                new ProductView
                                {
                                    id = a.CATEGORY_ID,
                                    CATEGORY_ID = a.CATEGORY_ID,
                                    CATEGORY_NAME = a.TB_CATEGORY.CATEGORY_NAME,
                                    CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                    CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                    DESCRIPTION = a.TB_CATALOG.DESCRIPTION,
                                    VERSION = a.TB_CATALOG.VERSION,
                                    hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID && x.tps.FLAG_RECYCLE == "A") ? _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID)
                                            : _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                                .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                    SORT_ORDER = a.SORT_ORDER,
                                    CategoryIdinNavigator = categoryidinnavigator,
                                    FamilyandRelatedFamily = familyandrelatedfamily,
                                    spriteCssClass = a.TB_CATEGORY.IS_CLONE == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category",

                                }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);

                    var bb = Array.ConvertAll(
                                             _dbcontext.TB_CATALOG_FAMILY.Where(
                                                 a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && a.CATEGORY_ID == id && a.TB_FAMILY.ROOT_FAMILY == 1)
                                                 .Select(a
                                                     =>
                                                     new
                                                     {
                                                         a.FAMILY_ID,
                                                         a.TB_FAMILY.FAMILY_NAME,
                                                         a.TB_CATALOG.CATALOG_ID,
                                                         a.TB_CATALOG.CATALOG_NAME,
                                                         a.TB_CATALOG.DESCRIPTION,
                                                         a.TB_CATALOG.VERSION,
                                                         hasChildren = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tpf.PARENT_FAMILY_ID == a.FAMILY_ID && x.tps.CATEGORY_ID == id) ||
                                                         _dbcontext.TB_CATALOG_PRODUCT.Join(_dbcontext.TB_PRODUCT, tps => tps.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                                                         .Join(_dbcontext.TB_PROD_FAMILY, tpstpf => tpstpf.tpf.PRODUCT_ID, tp => tp.PRODUCT_ID, (tpstpf, tp) => new { tpstpf, tp })
                                                         .Any(x => x.tpstpf.tps.CATALOG_ID == catalogId && x.tpstpf.tpf.FLAG_RECYCLE == "A" && x.tp.FAMILY_ID == a.FAMILY_ID),
                                                         ProdCnt =
                                                             a.TB_FAMILY.TB_PROD_FAMILY.Where(b => b.FAMILY_ID == a.FAMILY_ID && b.FLAG_RECYCLE == "A")
                                                                 .Select(b => b.PRODUCT_ID)
                                                                 .Distinct()
                                                                 .Count(),
                                                         SubProdCnt =
                                                             a.TB_FAMILY.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, ts => ts.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (ts, tpf) => new { ts, tpf }).Where(b => b.ts.TB_FAMILY.FAMILY_ID == a.FAMILY_ID && b.tpf.CATALOG_ID == catalogId && b.tpf.FLAG_RECYCLE == "A")
                                                             .Select(b => b.tpf.SUBPRODUCT_ID)
                                                             .Distinct()
                                                             .Count(),
                                                         a.SORT_ORDER
                                                     }).ToArray(),
                                             a =>
                                                 new ProductView
                                                 {
                                                     id = id + "~" + a.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                                     CATEGORY_ID = "~" + a.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                                     CATEGORY_NAME = familyandrelatedfamily ? a.SubProdCnt > 0 ? a.FAMILY_NAME + "(" + a.ProdCnt.ToString(CultureInfo.InvariantCulture) + ")" : a.FAMILY_NAME + "(" + a.ProdCnt.ToString(CultureInfo.InvariantCulture) + ")"
                                                           : a.FAMILY_NAME,
                                                     CATALOG_ID = a.CATALOG_ID,
                                                     CATALOG_NAME = a.CATALOG_NAME,
                                                     DESCRIPTION = a.DESCRIPTION,
                                                     VERSION = a.VERSION,
                                                     hasChildren = a.hasChildren,
                                                     SORT_ORDER = a.SORT_ORDER,
                                                     CategoryIdinNavigator = false,
                                                     FamilyandRelatedFamily = familyandrelatedfamily,
                                                     spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.FAMILY_ID && x.tps.CATEGORY_ID == id) ?
                                                     "familyClone" : "family",




                                                 }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);

                    if (aa.Any())
                    {
                        var res = aa.ToList().Union(bb.ToList());
                        return res.ToList();
                    }
                    if (bb.Any())
                    {

                        return bb.ToList();
                    }
                    else
                    {
                        int fid;
                        int.TryParse(id.Trim('~'), out fid);
                        //var sda=_dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tf => tf.FAMILY_ID,
                        //          (tps, tf) => new {tps, tf})
                        //          .Join(_dbcontext.TB_PROD_FAMILY, tcftf => tcftf.tf.FAMILY_ID, tpf => tpf.FAMILY_ID,
                        //              (tcftf, tpf) => new {tcftf, tpf})
                        //          .Join(_dbcontext.TB_SUBPRODUCT, tcftftpf => tcftftpf.tpf.PRODUCT_ID, ts => ts.PRODUCT_ID,
                        //              (tcftftpf, ts) => new {tcftftpf, ts})
                        //          .Where(x => x.tcftftpf.tcftf.tps.CATALOG_ID == catalogId
                        //                    && x.tcftftpf.tcftf.tf.FAMILY_ID == fid
                        //                    && x.tcftftpf.tcftf.tps.CATEGORY_ID == subfamilycategoryid && x.ts.CATALOG_ID == catalogId);
                        //int fid = Convert.ToInt32(id);
                        var cc = Array.ConvertAll(
                            (_dbcontext.TB_SUBFAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID,
                                tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf })
                                .Join(_dbcontext.TB_CATALOG_FAMILY, tcptps => tcptps.tps.SUBFAMILY_ID, tcp => tcp.FAMILY_ID,
                                    (tcptps, tcp) => new { tcptps, tcp })
                                .Where(x => x.tcp.CATALOG_ID == catalogId && x.tcp.FLAG_RECYCLE == "A" && x.tcptps.tps.FAMILY_ID == fid && x.tcp.CATEGORY_ID == subfamilycategoryid)).ToArray(),
                            a => new
                            {
                                FAMILY_ID = a.tcptps.tps.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                SUBFAMILY_ID = a.tcptps.tps.SUBFAMILY_ID,
                                ProdCnt =
                                    _dbcontext.TB_PROD_FAMILY.Where(b => b.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID && b.FLAG_RECYCLE == "A")
                                        .Distinct()
                                        .Count(),
                                SubProdCnt = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, ts => ts.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (ts, tpf) => new { ts, tpf }).Where(b => b.ts.TB_FAMILY.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID && b.tpf.FLAG_RECYCLE == "A" && b.tpf.CATALOG_ID == catalogId)
                                             .Select(b => b.tpf.SUBPRODUCT_ID)
                                             .Distinct()
                                             .Count(),
                                a.tcp.TB_CATALOG.CATALOG_ID,
                                a.tcp.TB_CATALOG.CATALOG_NAME,
                                a.tcp.TB_CATALOG.DESCRIPTION,
                                a.tcp.TB_CATALOG.VERSION,
                                a.tcptps.tps.SORT_ORDER,
                                hasChildren = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tf => tf.FAMILY_ID,
                                    (tps, tf) => new { tps, tf })
                                    .Join(_dbcontext.TB_PROD_FAMILY, tcftf => tcftf.tf.FAMILY_ID, tpf => tpf.FAMILY_ID,
                                    (tcftf, tpf) => new { tcftf, tpf })
                                    .Join(_dbcontext.TB_SUBPRODUCT, tcftftpf => tcftftpf.tpf.PRODUCT_ID, ts => ts.PRODUCT_ID,
                                    (tcftftpf, ts) => new { tcftftpf, ts }).Any(x => x.tcftftpf.tcftf.tps.CATALOG_ID == catalogId && x.tcftftpf.tcftf.tps.FLAG_RECYCLE == "A"
                                    && x.tcftftpf.tcftf.tf.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID
                                    && x.tcftftpf.tcftf.tps.CATEGORY_ID == subfamilycategoryid && x.ts.CATALOG_ID == catalogId) || _dbcontext.TB_CATALOG_PRODUCT.Join(_dbcontext.TB_PRODUCT, tps => tps.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                                                         .Join(_dbcontext.TB_PROD_FAMILY, tpstpf => tpstpf.tpf.PRODUCT_ID, tp => tp.PRODUCT_ID, (tpstpf, tp) => new { tpstpf, tp })
                                                         .Any(x => x.tpstpf.tps.CATALOG_ID == catalogId && x.tpstpf.tpf.FLAG_RECYCLE == "A" && x.tp.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID),
                            })
                            .Select(a => new ProductView
                            {
                                id = subfamilycategoryid + "~" + a.SUBFAMILY_ID,
                                CATEGORY_ID = "~" + a.SUBFAMILY_ID,
                                CATEGORY_NAME = familyandrelatedfamily ? a.SubProdCnt > 0 ?
                                   _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID && b.FLAG_RECYCLE == "A").Select(b => b.FAMILY_NAME).FirstOrDefault() + "(" + a.ProdCnt + ")" : _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault() + "(" + a.ProdCnt + ")" : _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault(),
                                CATALOG_ID = a.CATALOG_ID,
                                CATALOG_NAME = a.CATALOG_NAME,
                                DESCRIPTION = a.DESCRIPTION,
                                VERSION = a.VERSION,
                                SORT_ORDER = a.SORT_ORDER,
                                CategoryIdinNavigator = false,
                                FamilyandRelatedFamily = familyandrelatedfamily,
                                hasChildren = a.hasChildren,
                                spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.SUBFAMILY_ID && x.tps.CATEGORY_ID == subfamilycategoryid && x.tps.FLAG_RECYCLE == "A") ?
                                "subfamilyClone" : "subfamily",


                            }).OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id);

                        var dd = Array.ConvertAll((_dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID,
                              tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf })
                                .Join(_dbcontext.TB_PRODUCT, tcptps => tcptps.tps.PRODUCT_ID, tcp => tcp.PRODUCT_ID,
                                  (tcptps, tcp) => new { tcptps, tcp })
                                  .Join(_dbcontext.TB_PROD_SPECS, tcptps1 => tcptps1.tcp.PRODUCT_ID, tcp => tcp.PRODUCT_ID,
                                  (tcptps1, tcp1) => new { tcptps1, tcp1 })
                              .Where(x => x.tcptps1.tcptps.tps.FAMILY_ID == fid && x.tcp1.ATTRIBUTE_ID == 1 && x.tcptps1.tcptps.tps.FLAG_RECYCLE == "A")).ToArray(),
                          a => new
                          {
                              FAMILY_ID = a.tcp1.PRODUCT_ID.ToString(CultureInfo.InvariantCulture),
                              SUBFAMILY_ID = a.tcp1.PRODUCT_ID,
                              FAMILY_NAME = a.tcp1.STRING_VALUE,
                              ProdCnt = _dbcontext.TB_PRODUCT.Where(b => b.PRODUCT_ID == a.tcptps1.tcp.PRODUCT_ID && b.FLAG_RECYCLE == "A")
                                      .Distinct()
                                      .Count(),
                              SubProdCnt = _dbcontext.TB_SUBPRODUCT.Where(b => b.PRODUCT_ID == a.tcptps1.tcp.PRODUCT_ID && b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                     .Distinct()
                                     .Count()
                              //        ,a.tcptps1.tcp.TB_CATALOG.CATALOG_ID,
                              //a.tcptps1.tcp.TB_CATALOG.CATALOG_NAME,
                              //a.tcptps1.tcp.TB_CATALOG.DESCRIPTION,
                              //a.tcptps1.tcp.TB_CATALOG.VERSION,
                              //a.tcptps1.tcp.SORT_ORDER
                          })
                          .Select(a => new ProductView
                          {
                              id = subfamilycategoryid + "~" + fid + "#" + a.SUBFAMILY_ID,
                              CATEGORY_ID = "#" + a.SUBFAMILY_ID,
                              CATEGORY_NAME = a.FAMILY_NAME + "(" + a.SubProdCnt + ")",
                              //CATALOG_ID = a.CATALOG_ID,
                              //CATALOG_NAME = a.CATALOG_NAME,
                              //DESCRIPTION = a.DESCRIPTION,
                              //VERSION = a.VERSION,
                              //SORT_ORDER = a.SORT_ORDER,
                              CategoryIdinNavigator = false,
                              FamilyandRelatedFamily = familyandrelatedfamily,
                              hasChildren = false,
                              spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.SUBFAMILY_ID && x.tps.CATEGORY_ID == subfamilycategoryid && x.tps.FLAG_RECYCLE == "A") ?
                              "producttreeClone" : "producttree",



                          }).OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id);
                        //       CATALOG_ID = a, CATALOG_NAME = a.CATALOG_NAME, DESCRIPTION = a.DESCRIPTION, VERSION = a.VERSION,  , hasChildren = false });
                        //var cc = Array.ConvertAll(_dbcontext.TB_SUBFAMILY.Where(a => a.FAMILY_ID == fid).ToArray(),
                        //    a => new { FAMILY_ID = a.FAMILY_ID.ToString(CultureInfo.InvariantCulture), SUBFAMILY_ID = a.SUBFAMILY_ID,
                        //        ProdCnt = _dbcontext.TB_PROD_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Distinct().Count()})
                        //    .Select(a => new ProductView { id = "~" + a.SUBFAMILY_ID, CATEGORY_NAME = _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault() + "(" + a.ProdCnt + ")",
                        //       CATALOG_ID = a, CATALOG_NAME = a.CATALOG_NAME, DESCRIPTION = a.DESCRIPTION, VERSION = a.VERSION,  , hasChildren = false });
                        if (cc.Any())
                        {
                            var res = cc.ToList().Union(dd.ToList());
                            return res.ToList();
                        }
                        if (dd.Any())
                        {
                            var res = dd.ToList();
                            return res.ToList();
                        }
                        return null;
                    }
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : GetCategoriesForTree", objexception);
                return null;
            }

        }

        //---------------------END
    }
}