﻿using System;
using System.IO;
using System.Linq;
using Kendo.DynamicLinq;
using LS.Data;
using log4net;
using System.Web.Http;
using LS.Data.Model;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using Stimulsoft.Report;
using FunctionModel = LS.Data.Model.FunctionModel;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Web.Security;
using System.Web;
using System.Net.Http;
using System.Net;
using LS.Data.Utilities;
using LS.Web.Resources;
using System.Configuration;
using System.Data.SqlClient;
using DataTable = System.Data.DataTable;
using Workbook = Infragistics.Documents.Excel.Workbook;
using Infragistics.Documents.Excel;
using Microsoft.Office.Interop.Excel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using LS.Data.Model.Accounts;
using System.Net.Mail;
using DataEncrypterDecrypter;
using System.Globalization;
using Dapper;
using WebCat.CatalogDB;
using LS.Web.Common;

namespace LS.Web.Controllers
{
    public class UserAdminApiController : ApiController
    {
        // GET: UserAdminApiController
        private static ILog _logger = LogManager.GetLogger(typeof(UserAdminApiController));
        private CSEntities dbcontext = new CSEntities();
        DBAccess objDBAccess = new DBAccess();

        [HttpGet]
        public IHttpActionResult GetUsersFunctionAllowedData(string username)
        {
            try
            {
                if (username == "undefined" || username == null)
                    username = User.Identity.Name;
                var firstOrDefault = dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == username.Trim());
                if (firstOrDefault != null)
                {
                    var planids = dbcontext.Customers.Find(firstOrDefault.CustomerId);
                    var planID = planids.PlanId;
                    var newGridPlanFuctiontable = new List<PlanFunctionModel>();
                    var functionList =
                        ((from x in dbcontext.TB_FUNCTION select x).ToList()).Select(FunctionModel.GetModelForEmptyPlan)
                            .ToList();

                    int intRoleId = 0;
                    var getroleid = dbcontext.vw_aspnet_UsersInRoles
                        .Join(dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                        .Join(dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                            (tcptps, tcp) => new { tcptps, tcp })
                        .Where(x => x.tcptps.tpf.UserName == username).ToList();
                    if (getroleid.Any())
                    {
                        var sd = getroleid.FirstOrDefault();
                        intRoleId = Convert.ToInt32(sd.tcp.Role_id);
                    }

                    var planFunctionModelList =
                        dbcontext.GET_USER_FUNCTION_ALLOWED_VIEW.Where(a => a.ROLE_ID == intRoleId).ToList();

                    return Ok(planFunctionModelList);
                }
                return null;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }
        }

        [HttpGet]
        public IHttpActionResult GetPlanFunctionAllowedData(int planId)
        {
            try
            {
                List<FunctionModel> functionList =
                    ((from x in dbcontext.TB_FUNCTION select x).ToList()).Select(FunctionModel.GetModelForEmptyPlan)
                        .ToList();

                List<PlanFunctionModel> planFunctionModelList =
                    ((from x in dbcontext.TB_PLAN_FUNCTIONS where x.PLAN_ID == planId select x).ToList()).Select(
                        PlanFunctionModel.GetModel).ToList();
                if (planFunctionModelList.Count > 0)
                {
                    foreach (var planFunctionModel in planFunctionModelList)
                    {
                        var functionUpdatedItem =
                            functionList.FirstOrDefault(x => x.FUNCTION_NAME == planFunctionModel.FUNCTION_NAME);
                        if (functionUpdatedItem == null) continue;
                        functionUpdatedItem.ACTION_REMOVE = planFunctionModel.ACTION_REMOVE;
                        functionUpdatedItem.ACTION_ADD = planFunctionModel.ACTION_ADD;
                        functionUpdatedItem.ACTION_MODIFY = planFunctionModel.ACTION_MODIFY;
                        functionUpdatedItem.ACTION_VIEW = planFunctionModel.ACTION_VIEW;
                    }
                }
                return Ok(functionList);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }
        }

        [HttpGet]
        public IHttpActionResult GetNewPlanFunctionAllowedData()
        {
            try
            {
                //var result = (from x in dbcontext.TB_FUNCTION select x).ToList().
                //    Select(i => new FunctionModel
                //    {
                //        FUNCTION_ID = i.FUNCTION_ID,
                //        FUNCTION_NAME = i.FUNCTION_NAME,
                //        ACTION_ADD = false,
                //        ACTION_VIEW = false,
                //        ACTION_MODIFY = false,
                //        ACTION_REMOVE = false
                //    }).ToList();
                return Ok(((from x in dbcontext.TB_FUNCTION select x).ToList()).
                    Select(tbFunction => FunctionModel.GetModelForEmptyPlan(tbFunction)).ToList());

            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }
        }


        [HttpGet]
        public IHttpActionResult GetUsersRolesFunctionAllowedData(string roleId)
        {
            try
            {
                if (roleId.IsNullOrWhiteSpace())
                    return Ok((from x in dbcontext.GET_USER_FUNCTION_ALLOWED_VIEW select x).ToList());
                int intRoleId;
                bool res = int.TryParse(roleId, out intRoleId);

                var userFunctionAllowedByRole =
                    dbcontext.GET_USER_FUNCTION_ALLOWED_VIEW.Where(a => a.ROLE_ID == intRoleId).ToList();
                return Ok(userFunctionAllowedByRole);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public DataSourceResult GetUsersRolesFunctionAllowedDatamodified(string roleId, DataSourceRequest request)
        {
            if (request.Sort == null || !request.Sort.Any())
            {
                request.Sort = new List<Sort> { new Sort { Field = "FUNCTION_NAME", Dir = "asc" } };
            }
            try
            {
                if (roleId.IsNullOrWhiteSpace())
                {
                    // return Ok((from x in dbcontext.GET_USER_FUNCTION_ALLOWED_VIEW select x).ToList());
                    var userFunctionAllowedByRole = dbcontext.GET_USER_FUNCTION_ALLOWED_VIEW.Select(x => x);
                    return userFunctionAllowedByRole.ToDataSourceResult(request);
                }
                else
                {
                    int intRoleId;
                    bool res = int.TryParse(roleId, out intRoleId);
                    var userFunctionAllowedByRole =
                        dbcontext.GET_USER_FUNCTION_ALLOWED_VIEW.Where(a => a.ROLE_ID == intRoleId);
                    //  var ss = userFunctionAllowedByRole.ToDataSourceResult(request);
                    return userFunctionAllowedByRole.ToDataSourceResult(request);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public DataSourceResult GetUsersRolesFunctionAllowedDatamodifiedforAttribute(string roleId,
            DataSourceRequest filter)
        {
            if (filter.Sort == null || !filter.Sort.Any())
            {
                filter.Sort = new List<Sort> { new Sort { Field = "attribute_name", Dir = "asc" } };
            }
            try
            {
                var customerid = dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                if (customerid != null)
                {
                    int intRoleId;
                    bool res = int.TryParse(roleId, out intRoleId);

                    var sdffsdf = dbcontext.VIEW_CATALOGSTUDIO5_ReadonlyAttribute.Join(dbcontext.CUSTOMERATTRIBUTE,
                        tcp => tcp.attribute_id,
                        tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf }).Join(dbcontext.Customer_User,
                        tcptpf => tcptpf.tpf.CUSTOMER_ID, cu => cu.CustomerId, (tcptpf, cu) => new { tcptpf, cu }
                        )

                        .Where(x => x.tcptpf.tpf.CUSTOMER_ID == customerid.CustomerId && x.tcptpf.tpf.CREATED_USER == User.Identity.Name)
                        .GroupJoin(
                            dbcontext.TB_READONLY_ATTRIBUTES,
                            v => new { ATTRIBUTE_ID = v.tcptpf.tcp.attribute_id, ROLE_ID = v.tcptpf.tcp.role_id },
                            tra => new { tra.ATTRIBUTE_ID, tra.ROLE_ID }, (v, tra_join) => new { v, tra_join })
                        .SelectMany(@t => @t.tra_join.DefaultIfEmpty(), (@t, tra) => new { @t, tra })
                        .Where(@t => @t.@t.v.tcptpf.tcp.role_id == intRoleId && @t.t.v.cu.User_Name == User.Identity.Name)
                        .OrderBy(@t => @t.@t.v.tcptpf.tcp.attribute_name)
                        .Select(@t => new
                        {
                            @t.@t.v.tcptpf.tcp.role_id,
                            @t.@t.v.tcptpf.tcp.role_name,
                            @t.@t.v.tcptpf.tcp.attribute_name,
                            @t.@t.v.tcptpf.tcp.attribute_id,
                            @readonly = (System.Int64)((System.Int32?)@t.tra.ROLE_ID ?? (System.Int32?)0) == 0 ? true : false
                        });




                    return sdffsdf.ToDataSourceResult(filter);
                }
                else
                {
                    return null;
                }
            }

            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }
        }

        //[HttpGet]
        //public IHttpActionResult GetUsersRolesFunctionAllowedDatamodifiedforAttribute(string roleId)
        //{
        //    try
        //    {
        //        int rid = Convert.ToInt32(roleId);
        //        var listattributes = dbcontext.STP_LS_GET_READONLY_ATTRIBUTES_LIST(rid).ToList();
        //        return Ok(listattributes);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at UserAdminApiController: GetUsersRolesFunctionAllowedDatamodifiedforAttribute", ex);
        //        return null;
        //    }
        //    // return null;
        //}

        public string UpdateUserRolesAllowedFunction(UserRolesList userRolesList)
        {
            try
            {
                // DB insert needs to be written - To Victor
                return "Save action completed";
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }
            return null;
        }

        //Active Directory Update Process
        //Added by Jothipriya on April 20 2022
        public DataTable GetWindowsUserDetails(string User_Name)
        {
            try
            {
                DataTable ds_announceList = new DataTable();
                List<SqlParameter> objParameterList = new List<SqlParameter>();
                SqlParameter objparameter = new SqlParameter();
                objparameter = new SqlParameter("@USER_NAME", SqlDbType.VarChar) { Value = User_Name };
                objParameterList.Add(objparameter);
                objparameter = new SqlParameter("@OPTION", SqlDbType.VarChar) { Value = "Select" };
                objParameterList.Add(objparameter);

                object obj_Result = objDBAccess.ExcecuteSTP("STP_CATALOGSTUDIO_Get_Windows_User_Details", objParameterList.ToArray(), UseCommandType.ExecuteDataTable);
                if (obj_Result != null)
                {
                    ds_announceList = (DataTable)obj_Result;
                }

                return ds_announceList;

            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }
        }

        //Added by Jothipriya on April 20 2022
        [System.Web.Http.HttpGet]
        public string SaveWindowsUserDetails(string USER_NAME, string SysUser_Name, string SysDomain_Name, string User_Status)
        {
            try
            {
                List<SqlParameter> objParameterList = new List<SqlParameter>();
                SqlParameter objparameter = new SqlParameter();


                objparameter = new SqlParameter("@USER_NAME", SqlDbType.VarChar) { Value = USER_NAME };
                objParameterList.Add(objparameter);

                objparameter = new SqlParameter("@SysUser_Name", SqlDbType.VarChar) { Value = SysUser_Name };
                objParameterList.Add(objparameter);

                objparameter = new SqlParameter("@SysDomain_Name", SqlDbType.VarChar) { Value = SysDomain_Name };
                objParameterList.Add(objparameter);

                objparameter = new SqlParameter("@User_Status", SqlDbType.VarChar) { Value = User_Status };
                objParameterList.Add(objparameter);

                objparameter = new SqlParameter("@Option", SqlDbType.VarChar) { Value = "INSERT" };
                objParameterList.Add(objparameter);


                objDBAccess.ExcecuteSTP("STP_CATALOGSTUDIO_Get_Windows_User_Details", objParameterList.ToArray(), UseCommandType.ExecuteNonQuery);

                return "Success";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ProductApiController : AddJobQuoteDetails", ex);
                return null;
            }
        }
        //Active Directory Update Process
        //END


        [System.Web.Http.HttpPost]
        public IHttpActionResult SaveUserFunctionAllowed(object model)
        {
            try
            {
                using (var db = new CSEntities())
                {
                    var planID = dbcontext.Customers.FirstOrDefault(x => x.EMail == User.Identity.Name.Trim()).PlanId;
                    var arr = (JArray)((JObject.Parse(model.ToString())).SelectToken("data")).SelectToken("models");
                    var functionAlloweditems = ((JArray)arr).Select(x => new TB_USER_FUNCTION_ALLOWED
                    {
                        FUNCTION_ID = (int)x["FUNCTION_ID"],
                        FUNCTION_NAME = (string)x["FUNCTION_NAME"],
                        ROLE_ID = (int)x["ROLE_ID"],
                        ACTION_VIEW = (bool)x["ACTION_VIEW"],
                        ACTION_MODIFY = (bool)x["ACTION_MODIFY"],
                        ACTION_ADD = (bool)x["ACTION_ADD"],
                        ACTION_REMOVE = (bool)x["ACTION_REMOVE"]
                    }).ToList();
                    foreach (var item in functionAlloweditems)
                    {
                        //var objuserFunctionAllowed = db.TB_ROLE_FUNCTIONS.FirstOrDefault(x => x.FUNCTION_ID == item.FUNCTION_ID && x.ROLE_ID == 1);//item.ROLE_ID
                        var objuserFunctionAllowed =
                            db.TB_PLAN_FUNCTIONS.FirstOrDefault(
                                x => x.FUNC_ID == item.FUNCTION_ID && x.PLAN_ID == planID); //item.ROLE_ID
                        if (objuserFunctionAllowed != null)
                        {
                            objuserFunctionAllowed.ACTION_VIEW = Convert.ToBoolean(item.ACTION_VIEW);
                            objuserFunctionAllowed.ACTION_MODIFY = Convert.ToBoolean(item.ACTION_MODIFY);
                            objuserFunctionAllowed.ACTION_ADD = Convert.ToBoolean(item.ACTION_ADD);
                            objuserFunctionAllowed.ACTION_REMOVE = Convert.ToBoolean(item.ACTION_REMOVE);
                        }
                        db.SaveChanges();
                    }
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at SaveOrUpdateAppUser", ex);
            }
            return null;
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult SaveUserRoleFunctions(PlanModel plan)
        {
            try
            {
                foreach (var objFunModel in plan.FunctionModels)
                {
                    List<TB_ROLE_FUNCTIONS> objPlanFunctions;
                    if (objFunModel.FUNCTION_ID != 0)
                    {
                        objPlanFunctions = dbcontext.TB_ROLE_FUNCTIONS.Where(
                            x => x.ROLE_ID == plan.PLAN_ID &&
                                 x.FUNCTION_GROUP_ID == objFunModel.FUNCTION_GROUP_ID &&
                                 x.FUNCTION_ID == objFunModel.FUNCTION_ID).ToList();
                    }
                    else
                    {
                        objPlanFunctions = dbcontext.TB_ROLE_FUNCTIONS.Where(
                            x => x.ROLE_ID == plan.PLAN_ID &&
                                 x.FUNCTION_GROUP_ID == objFunModel.FUNCTION_GROUP_ID).ToList();

                    }
                    if (objPlanFunctions.Count > 0)
                    {
                        objPlanFunctions.ForEach(a =>
                        {
                            a.ACTION_ADD = objFunModel.ACTION_ADD;
                            a.ACTION_MODIFY = objFunModel.ACTION_MODIFY;
                            a.ACTION_REMOVE = objFunModel.ACTION_REMOVE;
                            a.ACTION_VIEW = objFunModel.ACTION_VIEW;
                            a.ACTION_DETACH = objFunModel.ACTION_DETACH;

                        });

                        dbcontext.SaveChanges();
                    }


                }
                //var rolefunctionAlloweditems = ((JArray)model).Select(x => new TB_ROLE_FUNCTIONS()
                //{
                //    FUNCTION_ID = (int)x["FUNCTION_ID"],
                //    ROLE_ID = (int)x["ROLE_ID"],
                //    ACTION_VIEW = (bool)x["ACTION_VIEW"],
                //    ACTION_MODIFY = (bool)x["ACTION_MODIFY"],
                //    ACTION_ADD = (bool)x["ACTION_ADD"],
                //    ACTION_REMOVE = (bool)x["ACTION_REMOVE"]

                //}).ToList();


                //foreach (var item in rolefunctionAlloweditems)
                //{
                //    var objuserFunctionAllowed = dbcontext.TB_ROLE_FUNCTIONS.FirstOrDefault(x => x.FUNCTION_ID == item.FUNCTION_ID && x.ROLE_ID == item.ROLE_ID);//item.ROLE_ID
                //    if (objuserFunctionAllowed != null)
                //    {
                //        objuserFunctionAllowed.ACTION_VIEW = Convert.ToBoolean(item.ACTION_VIEW);
                //        objuserFunctionAllowed.ACTION_MODIFY = Convert.ToBoolean(item.ACTION_MODIFY);
                //        objuserFunctionAllowed.ACTION_ADD = Convert.ToBoolean(item.ACTION_ADD);
                //        objuserFunctionAllowed.ACTION_REMOVE = Convert.ToBoolean(item.ACTION_REMOVE);
                //    }
                //    dbcontext.SaveChanges();
                //}
                return Ok("Update completed");

            }
            catch (Exception ex)
            {
                _logger.Error("Error at UserAdminApi / SaveUserRoleFunctionAllowed", ex);
            }
            return null;
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult saveUserRoleList(int FuncId, int RoleId, string Option, bool Value)
        {
            try
            {
                string[] multipleOption = null;
                if (Option.Contains(","))
                {
                    multipleOption = Option.Split(',');
                }
                if (multipleOption != null)
                {
                    foreach (var item in multipleOption)
                    {
                        updateFunction(item.ToString(), RoleId, FuncId, Value);
                    }
                }
                else
                {
                    if (Option == "view")
                    {
                        var functions =
                            dbcontext.TB_ROLE_FUNCTIONS.FirstOrDefault(
                                a => a.ROLE_ID == RoleId && a.FUNCTION_ID == FuncId);
                        if (functions != null)
                        {
                            functions.ACTION_VIEW = Value;
                            dbcontext.SaveChanges();
                        }
                    }
                    else if (Option == "modify")
                    {
                        var functions =
                            dbcontext.TB_ROLE_FUNCTIONS.FirstOrDefault(
                                a => a.ROLE_ID == RoleId && a.FUNCTION_ID == FuncId);
                        if (functions != null)
                        {
                            functions.ACTION_MODIFY = Value;
                            dbcontext.SaveChanges();
                        }
                    }
                    else if (Option == "add")
                    {
                        var functions =
                            dbcontext.TB_ROLE_FUNCTIONS.FirstOrDefault(
                                a => a.ROLE_ID == RoleId && a.FUNCTION_ID == FuncId);
                        if (functions != null)
                        {
                            functions.ACTION_ADD = Value;
                            dbcontext.SaveChanges();
                        }
                    }
                    else
                    {
                        var functions =
                            dbcontext.TB_ROLE_FUNCTIONS.FirstOrDefault(
                                a => a.ROLE_ID == RoleId && a.FUNCTION_ID == FuncId);
                        if (functions != null)
                        {
                            functions.ACTION_REMOVE = Value;
                            dbcontext.SaveChanges();
                        }
                    }
                }
                return Ok();

            }
            catch (Exception ex)
            {
                _logger.Error("Error at SaveOrUpdateAppUser", ex);
            }
            return null;
        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult updateReadonlyattributes(int RoleId, int Attrid, bool Value)
        {
            try
            {
                if (Value)
                {
                    var list =
                        dbcontext.TB_READONLY_ATTRIBUTES.FirstOrDefault(
                            a => a.ROLE_ID == RoleId && a.ATTRIBUTE_ID == Attrid);
                    if (list != null)
                    {
                        //var readonlyattributes = dbcontext.TB_READONLY_ATTRIBUTES.Find(list);
                        dbcontext.TB_READONLY_ATTRIBUTES.Remove(list);
                        dbcontext.SaveChanges();
                    }
                }
                else
                {
                    var list =
                        dbcontext.TB_READONLY_ATTRIBUTES.Where(a => a.ROLE_ID == RoleId && a.ATTRIBUTE_ID == Attrid);
                    if (!list.Any())
                    {
                        var readonlyattributes = new TB_READONLY_ATTRIBUTES
                        {
                            ATTRIBUTE_ID = Attrid,
                            ROLE_ID = RoleId,
                            CREATED_USER = User.Identity.Name,
                            CREATED_DATE = DateTime.Now,
                            MODIFIED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name
                        };
                        dbcontext.TB_READONLY_ATTRIBUTES.Add(readonlyattributes);
                        dbcontext.SaveChanges();
                    }
                }
                return Ok();

            }
            catch (Exception ex)
            {
                _logger.Error("Error at SaveOrUpdateAppUser", ex);
            }
            return null;
        }


        [System.Web.Http.HttpGet]
        public IHttpActionResult deleteRole(int RoleId)
        {
            try
            {
                if (RoleId != null)
                {
                    if (RoleId != 1 && RoleId != 2 && RoleId != 3 && RoleId != 4)
                    {
                        var UserInRoleCheck = dbcontext.vw_aspnet_UsersInRoles
                            .Join(dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId,
                                (tps, tpf) => new { tps, tpf })
                            .Join(dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                                (tcptps, tcp) => new { tcptps, tcp })
                            .Where(x => x.tcp.Role_id == RoleId).ToList();
                        if (!UserInRoleCheck.Any())
                        {

                            var details = dbcontext.TB_ROLE.FirstOrDefault(a => a.ROLE_ID == RoleId);
                            if (details != null)
                            {
                                var findRole = dbcontext.TB_ROLE.Find(details.ROLE_ID);
                                dbcontext.TB_ROLE.Remove(findRole);

                                var aspnetroles = dbcontext.aspnet_Roles.FirstOrDefault(x => x.Role_id == RoleId);
                                if (aspnetroles != null)
                                {
                                    var findRoleaspnet = dbcontext.aspnet_Roles.Find(aspnetroles.RoleId);
                                    dbcontext.aspnet_Roles.Remove(findRoleaspnet);
                                }

                                dbcontext.SaveChanges();
                            }
                        }
                        else
                        {
                            return Ok("You are not allowed to delete this role. It is already assigned to some Users.");
                        }
                    }
                    else
                    {
                        return Ok("Default roles cannot be deleted");
                    }
                }
                return Ok("Deleted successfully.");

            }
            catch (Exception ex)
            {
                _logger.Error("Error at deleteRole", ex);
            }
            return null;
        }


        public void updateFunction(string Option, int RoleId, int FuncId, bool Value)
        {
            if (Option == "view")
            {
                var functions =
                    dbcontext.TB_ROLE_FUNCTIONS.FirstOrDefault(a => a.ROLE_ID == RoleId && a.FUNCTION_ID == FuncId);
                if (functions != null)
                {
                    functions.ACTION_VIEW = Value;
                    dbcontext.SaveChanges();
                }
            }
            else if (Option == "modify")
            {
                var functions =
                    dbcontext.TB_ROLE_FUNCTIONS.FirstOrDefault(a => a.ROLE_ID == RoleId && a.FUNCTION_ID == FuncId);
                if (functions != null)
                {
                    functions.ACTION_MODIFY = Value;
                    dbcontext.SaveChanges();
                }
            }
            else if (Option == "add")
            {
                var functions =
                    dbcontext.TB_ROLE_FUNCTIONS.FirstOrDefault(a => a.ROLE_ID == RoleId && a.FUNCTION_ID == FuncId);
                if (functions != null)
                {
                    functions.ACTION_ADD = Value;
                    dbcontext.SaveChanges();
                }
            }
            else
            {
                var functions =
                    dbcontext.TB_ROLE_FUNCTIONS.FirstOrDefault(a => a.ROLE_ID == RoleId && a.FUNCTION_ID == FuncId);
                if (functions != null)
                {
                    functions.ACTION_REMOVE = Value;
                    dbcontext.SaveChanges();
                }
            }
        }

        [System.Web.Http.HttpPost]
        public void SaveUserRoleFunctionAllowed(object model)
        {
            try
            {
                var roles = JObject.Parse(model.ToString()).SelectToken("models").SelectToken("data");
                var functionAlloweditems = (roles).Select(x => new userRoleModel
                {
                    FUNCTION_ID = (int)x["FUNCTION_ID"],
                    FUNCTION_NAME = (string)x["FUNCTION_NAME"],
                    ROLE_ID = (int)x["ROLE_ID"],
                    ACTION_VIEW = (bool)x["ACTION_VIEW"],
                    ACTION_MODIFY = (bool)x["ACTION_MODIFY"],
                    ACTION_ADD = (bool)x["ACTION_ADD"],
                    ACTION_REMOVE = (bool)x["ACTION_REMOVE"]
                }).ToList();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
        }



        [HttpGet]
        public IList getUserRoleRights(int functionId, int catalogId)
        {
            try
            {
                var SuperAdminCheck = dbcontext.vw_aspnet_UsersInRoles
                    .Join(dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
                if (SuperAdminCheck.Any())
                {
                    List<STP_LS_GET_USER_ROLE_FUNCTION_Result> rolefunctionresults = null;
                    rolefunctionresults =
                        dbcontext.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name.ToString(), functionId).ToList();
                    if (rolefunctionresults.Count == 0)
                    {
                        return null;
                    }
                    else
                    {

                        var userrole = rolefunctionresults.Select(a => new userRoleModel
                        {
                            ACTION_ADD = a.ACTION_ADD == 1,
                            ACTION_MODIFY = a.ACTION_MODIFY == 1,
                            ACTION_REMOVE = a.ACTION_REMOVE == 1,
                            ACTION_VIEW = a.ACTION_VIEW == 1
                        }).ToList();
                        return userrole;
                    }
                }
                var chkSubscription =
                    dbcontext.STP_LS_CHECK_SUBSCRIPTION_BY_USER(User.Identity.Name.ToString(), "").ToList();
                if (chkSubscription.Any())
                {
                    var chkSubscriptions = chkSubscription.FirstOrDefault();

                    var chkSubscription1 = dbcontext.TB_STORE_SESSION
                        .Join(dbcontext.Customer_User, tps => tps.ID, tpf => tpf.CustomerId,
                            (tps, tpf) => new { tps, tpf })
                        .Where(x => x.tpf.User_Name == User.Identity.Name.ToString()).ToList();
                    if (chkSubscription1.Any())
                    {
                        int skucount =
                            Convert.ToInt32(
                                LS.Data.Utilities.CommonUtil.Decrypt(chkSubscription1.FirstOrDefault().tps.COL1));
                        int Noofusers =
                            Convert.ToInt32(
                                LS.Data.Utilities.CommonUtil.Decrypt(chkSubscription1.FirstOrDefault().tps.COL2));
                        int NoofSubscriptions =
                            Convert.ToInt32(
                                LS.Data.Utilities.CommonUtil.Decrypt(chkSubscription1.FirstOrDefault().tps.COL3));
                        string activationdate =
                            LS.Data.Utilities.CommonUtil.Decrypt(chkSubscription1.FirstOrDefault().tps.COL4);
                        DateTime dtStore = Convert.ToDateTime(activationdate);
                        string actStoreDate = String.Format("{0:dd-MMM-yyyy}", dtStore);

                        DateTime dtPlan = Convert.ToDateTime(chkSubscription[0].ActivationDate);
                        string actPlanDate = String.Format("{0:dd-MMM-yyyy}", dtPlan);

                        if (chkSubscription[0].NO_OF_USERS == Noofusers &&
                            chkSubscription[0].SUBSCRIPTION_IN_MONTHS == NoofSubscriptions &&
                            actPlanDate == actStoreDate)
                        {
                            List<STP_LS_GET_USER_ROLE_FUNCTION_Result> rolefunctionresults = null;
                            rolefunctionresults =
                                dbcontext.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name.ToString(), functionId)
                                    .ToList();
                            if (rolefunctionresults.Count == 0)
                            {
                                return null;
                            }
                            else
                            {
                                if (catalogId != 0)
                                {
                                    var customerId = dbcontext.aspnet_Users.Join(dbcontext.vw_aspnet_UsersInRoles, au => au.UserId, aur => aur.UserId, (au, aur) => new { au, aur }).Join(dbcontext.aspnet_Roles, users => users.aur.RoleId, roles => roles.RoleId, (users, roles) => new { users, roles }).Where(x => x.users.au.UserName == User.Identity.Name).FirstOrDefault();
                                    var customerwiseCatalog = dbcontext.Customer_Role_Catalog.Where(x => x.CATALOG_ID == catalogId && x.RoleId == customerId.roles.Role_id && x.CustomerId == (dbcontext.Customer_User.Where(cu => cu.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault())).FirstOrDefault();
                                    if (customerwiseCatalog != null && customerwiseCatalog.IsRead == true)
                                    {
                                        var customerwiseuserrole = rolefunctionresults.Select(a => new userRoleModel
                                        {
                                            ACTION_ADD = false,
                                            ACTION_MODIFY = false,
                                            ACTION_REMOVE = false,
                                            ACTION_VIEW = true,
                                            FUNCTION_ID = a.FUNC_ID
                                        }).ToList();
                                        return customerwiseuserrole;
                                    }
                                }
                                var userrole = rolefunctionresults.Select(a => new userRoleModel
                                {
                                    ACTION_ADD = a.ACTION_ADD == 1,
                                    ACTION_MODIFY = a.ACTION_MODIFY == 1,
                                    ACTION_REMOVE = a.ACTION_REMOVE == 1,
                                    ACTION_VIEW = a.ACTION_VIEW == 1
                                }).ToList();
                                return userrole;
                            }

                        }
                        else
                        {
                            HttpContext.Current.Session.Abandon();
                            FormsAuthentication.SignOut();
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at UserAdminApiController : getUserRoleRights", objexception);
                return null;
            }

        }


        [HttpGet]
        public IList GetUserWiseCatalog(int catalogId)
        {
            List<STP_LS_GET_USER_ROLE_FUNCTION_Result> rolefunctionresults = null;
            rolefunctionresults =
                dbcontext.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name.ToString(), 0).ToList();
            if (rolefunctionresults.Count == 0)
            {
                return null;
            }
            else
            {
                if (catalogId != 0)
                {
                    var customerId = dbcontext.aspnet_Users.Join(dbcontext.vw_aspnet_UsersInRoles, au => au.UserId, aur => aur.UserId, (au, aur) => new { au, aur }).Join(dbcontext.aspnet_Roles, users => users.aur.RoleId, roles => roles.RoleId, (users, roles) => new { users, roles }).Where(x => x.users.au.UserName == User.Identity.Name).FirstOrDefault();
                    var customerwiseCatalog = dbcontext.Customer_Role_Catalog.Where(x => x.CATALOG_ID == catalogId && x.RoleId == customerId.roles.Role_id && x.CustomerId == (dbcontext.Customer_User.Where(cu => cu.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault())).ToList();

                    return customerwiseCatalog;

                }

                return null;

            }
        }

        [System.Web.Http.HttpGet]
        public int getUserSuperAdmin()
        {
            try
            {

                var SuperAdminCheck = dbcontext.vw_aspnet_UsersInRoles
                    .Join(dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name).Select(x => x.tcp.RoleType).ToList();
                if (SuperAdminCheck.Any())
                {

                    return Convert.ToInt16(SuperAdminCheck[0]);
                }
                else
                {
                    return 3;
                }


            }
            catch (Exception objexception)
            {
                _logger.Error("Error at UserAdminApiController : getUserRoleRights", objexception);
                return 0;
            }

        }


        [HttpPost]
        public IHttpActionResult SaveModifiedUserData(object model)
        {
            //int companyID;
            int allowedUserCount = 0;
            //  int activeUserCount = 0;
            try
            {

                var userStatusList = ((JArray)model).Select(x => new UserStatusModel()
                {
                    TB_USER_ID = (string)x["TB_USER_ID"],
                    ROLE_ID = (string)x["ROLE_ID"],
                    ROLE_NAME = (string)x["ROLE_NAME"],
                    STATUS = (string)x["STATUS"]

                }).ToList();

                foreach (var item in userStatusList)
                {
                    if (item.STATUS.ToUpper() == "ACTIVE")
                    {

                        var allowedusers = from cu in dbcontext.Customer_User
                                           from cust in dbcontext.Customers
                                           from plan in dbcontext.TB_PLAN
                                           where
                                               (cu.User_Name == item.TB_USER_ID && cust.CustomerId == cu.CustomerId &&
                                                cust.PlanId == plan.PLAN_ID)
                                           select new CustomerUsers() { NO_OF_USERS = plan.NO_OF_USERS, CustomerId = cu.CustomerId };
                        allowedUserCount = allowedusers.FirstOrDefault().NO_OF_USERS;

                        var activeusers = (from company in dbcontext.Customer_User
                                           from user in dbcontext.aspnet_Users
                                           where
                                               (company.CustomerId ==
                                                ((IQueryable<CustomerUsers>)allowedusers).FirstOrDefault().CustomerId &&
                                                company.User_Name == user.UserName && !user.IsAnonymous)
                                           select user).Count();
                        // activeUserCount = activeusers;




                        if (allowedusers.Any())
                        {

                            if (activeusers < ((IQueryable<CustomerUsers>)allowedusers).FirstOrDefault().NO_OF_USERS)
                            {

                                var objuser = dbcontext.aspnet_Users.FirstOrDefault(x => x.UserName == item.TB_USER_ID);

                                if (objuser != null)
                                {

                                    objuser.IsAnonymous = item.STATUS.ToUpper() == "ACTIVE" ? false : true;

                                }
                                dbcontext.SaveChanges();





                            }
                            else
                            {
                                return Ok("userexceeded");
                            }
                        }
                    }

                    else
                    {

                        var objuser = dbcontext.aspnet_Users.FirstOrDefault(x => x.UserName == item.TB_USER_ID);

                        if (objuser != null)
                        {

                            objuser.IsAnonymous = item.STATUS.ToUpper() == "ACTIVE" ? false : true;

                        }

                        dbcontext.SaveChanges();

                        var objuser1 = dbcontext.TB_USER.FirstOrDefault(x => x.TB_USER_ID == item.TB_USER_ID);

                        if (objuser1 != null)
                        {

                            objuser1.STATUS = item.STATUS.ToUpper() == "ACTIVE" ? "ACTIVE" : "INACTIVE";

                        }

                        dbcontext.SaveChanges();


                    }



                }

                return Ok("Updated");
            }

            catch (Exception ex)
            {
                _logger.Error("Error at SaveModifiedUserData", ex);
                return null;
            }
            //return Ok();

        }


        [HttpGet]
        public IHttpActionResult GetAllCatalogs(int customerId)
        {
            try
            {
                var result = (from x in dbcontext.TB_CATALOG where x.CATALOG_ID != 1 select x).ToList().
                    Select(i => new
                    {
                        CATALOG_ID = i.CATALOG_ID,
                        CATALOG_NAME = i.CATALOG_NAME,
                        ACTIVE =
                            dbcontext.Customer_Catalog.Any(
                                x => x.CustomerId == customerId && x.CATALOG_ID == i.CATALOG_ID)
                    }).ToList();
                return Ok(result);

            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public DataSourceResult GetAllCatalogsByRoleId(int roleId, int customerId, DataSourceRequest filter)
        {
            if (filter.Sort == null || !filter.Sort.Any())
            {
                filter.Sort = new List<Sort> { new Sort { Field = "Catalog_Name", Dir = "asc" } };
            }
            try
            {

                var cataloglist = dbcontext.TB_CATALOG
                    .Join(dbcontext.Customer_Catalog, cc => cc.CATALOG_ID, tc => tc.CATALOG_ID, (cc, tc) => new { cc, tc })
                    .Join(dbcontext.Customer_Role_Catalog, tcptps => tcptps.tc.CustomerId, crc => crc.CustomerId, (tcptps, crc) => new { tcptps, crc })
                    //.Join(dbcontext.TB_USER_ROLE, tcptpscrc => tcptpscrc.crc.RoleId, ur => ur.ROLE_ID, (tcptpscrc, ur) => new { tcptpscrc, ur })
                    .Where(
                        x =>
                            x.crc.RoleId == roleId && x.crc.CustomerId == customerId &&
                            x.crc.CATALOG_ID == x.tcptps.cc.CATALOG_ID && x.crc.CustomerId == x.tcptps.tc.CustomerId)
                    .Select(x => new { x.tcptps.cc.CATALOG_ID, x.tcptps.cc.CATALOG_NAME, x.crc.IsActive, x.crc.IsRead }).Distinct();

                var result = cataloglist.Select(i => new
                {
                    i.CATALOG_ID,
                    i.CATALOG_NAME,
                    ACTIVE = i.IsActive,
                    READ = i.IsRead
                    //dbcontext.Customer_Role_Catalog.Any(x => x.RoleId == roleId && x.CustomerId == customerId && x.IsActive == true && x.CATALOG_ID == i.CATALOG_ID) ? true : false
                });
                return result.ToDataSourceResult(filter);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public IList GetCompanyList()
        {
            try
            {
                var customerList = dbcontext.Customers.ToList();
                return
                    customerList.Where(c => c.CustomerId != 1).Select(a => new { a.CompanyName, a.CustomerId }).ToList();
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at UserAdminApiController : GetCompanyList", objexception);
                return null;
            }

        }

        [System.Web.Http.HttpPost]
        public IHttpActionResult Updatecustomerrolecatalog(int roleId, int customerId, int catalogid, bool activeflag, string flag)
        {
            var objcustomerrolecatalog =
                dbcontext.Customer_Role_Catalog.FirstOrDefault(
                    x => x.RoleId == roleId && x.CustomerId == customerId && x.CATALOG_ID == catalogid); //item.ROLE_ID

            var existingStatus = dbcontext.Customer_Role_Catalog.Join(dbcontext.TB_CATALOG, crc => crc.CATALOG_ID, tc => tc.CATALOG_ID, (crc, tc) => new { crc, tc }).Count(x => x.crc.RoleId == roleId && x.crc.CustomerId == customerId && x.crc.IsActive == true);
            if (existingStatus == 1 && activeflag == false)
            {
                return Ok("No");
            }
            if (objcustomerrolecatalog != null)
            {
                if (flag == "active")
                {
                    objcustomerrolecatalog.IsActive = activeflag;
                }
                else if (flag == "read")
                {
                    objcustomerrolecatalog.IsRead = activeflag;
                }

            }
            dbcontext.SaveChanges();
            return Ok();
        }

        [HttpGet]
        public IList GetUserRoleRightsAll(int catalogId)
        {
            try
            {
                string customerFolder = "";
                customerFolder = dbcontext.Customers.Join(dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
                if (string.IsNullOrEmpty(customerFolder))
                { customerFolder = "/STUDIOSOFT"; }
                else
                { customerFolder = "/" + customerFolder.Replace("&", ""); }

                if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder)))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Images"));
                }
                if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Temp/ImageandAttFile")))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Content/ProductImages" + customerFolder + "/Temp/ImageandAttFile"));
                }
                var superAdminCheck = dbcontext.vw_aspnet_UsersInRoles
                    .Join(dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
                if (superAdminCheck.Any())
                {
                    var rolefunctionresults = dbcontext.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 0).ToList();
                    if (rolefunctionresults.Count == 0)
                    {
                        return null;
                    }
                    else
                    {

                        var userrole = rolefunctionresults.Select(a => new userRoleModel
                        {
                            ACTION_ADD = a.ACTION_ADD == 1,
                            ACTION_MODIFY = a.ACTION_MODIFY == 1,
                            ACTION_REMOVE = a.ACTION_REMOVE == 1,
                            ACTION_VIEW = a.ACTION_VIEW == 1,
                            // ACTION_DETACH=a.ACTION_DETACH,
                            FUNCTION_ID = a.FUNC_ID
                        }).ToList();
                        return userrole;
                    }
                }
                var chkSubscription =
                    dbcontext.STP_LS_CHECK_SUBSCRIPTION_BY_USER(User.Identity.Name.ToString(), "").ToList();
                if (chkSubscription.Any())
                {
                    var chkSubscription1 = dbcontext.TB_STORE_SESSION
                        .Join(dbcontext.Customer_User, tps => tps.ID, tpf => tpf.CustomerId,
                            (tps, tpf) => new { tps, tpf })
                        .Where(x => x.tpf.User_Name == User.Identity.Name.ToString()).ToList();
                    if (chkSubscription1.Any())
                    {
                        // Convert.ToInt32(LS.Data.Utilities.CommonUtil.Decrypt(chkSubscription1.FirstOrDefault().tps.COL1));
                        int Noofusers =
                            Convert.ToInt32(
                                LS.Data.Utilities.CommonUtil.Decrypt(chkSubscription1.FirstOrDefault().tps.COL2));
                        int NoofSubscriptions =
                            Convert.ToInt32(
                                LS.Data.Utilities.CommonUtil.Decrypt(chkSubscription1.FirstOrDefault().tps.COL3));
                        string activationdate =
                            LS.Data.Utilities.CommonUtil.Decrypt(chkSubscription1.FirstOrDefault().tps.COL4);
                        DateTime dtStore = Convert.ToDateTime(activationdate);
                        string actStoreDate = String.Format("{0:dd-MMM-yyyy}", dtStore);

                        DateTime dtPlan = Convert.ToDateTime(chkSubscription[0].ActivationDate);
                        string actPlanDate = String.Format("{0:dd-MMM-yyyy}", dtPlan);

                        if (chkSubscription[0].NO_OF_USERS == Noofusers &&
                            chkSubscription[0].SUBSCRIPTION_IN_MONTHS == NoofSubscriptions &&
                            actPlanDate == actStoreDate)
                        {
                            var rolefunctionresults =
                                dbcontext.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 0).ToList();
                            if (rolefunctionresults.Count == 0)
                            {
                                return null;
                            }
                            else
                            {
                                if (catalogId != 0)
                                {
                                    var customerId = dbcontext.aspnet_Users.Join(dbcontext.vw_aspnet_UsersInRoles, au => au.UserId, aur => aur.UserId, (au, aur) => new { au, aur }).Join(dbcontext.aspnet_Roles, users => users.aur.RoleId, roles => roles.RoleId, (users, roles) => new { users, roles }).Where(x => x.users.au.UserName == User.Identity.Name).FirstOrDefault();
                                    var customerwiseCatalog = dbcontext.Customer_Role_Catalog.Where(x => x.CATALOG_ID == catalogId && x.RoleId == customerId.roles.Role_id && x.CustomerId == (dbcontext.Customer_User.Where(cu => cu.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault())).FirstOrDefault();
                                    if (customerwiseCatalog != null && customerwiseCatalog.IsRead == true)
                                    {
                                        var customerwiseuserrole = rolefunctionresults.Select(a => new userRoleModel
                                        {
                                            ACTION_ADD = false,
                                            ACTION_MODIFY = false,
                                            ACTION_REMOVE = false,
                                            ACTION_VIEW = true,
                                            FUNCTION_ID = a.FUNC_ID
                                        }).ToList();
                                        return customerwiseuserrole;
                                    }
                                }
                                var userrole = rolefunctionresults.Select(a => new userRoleModel
                                {
                                    ACTION_ADD = a.ACTION_ADD == 1,
                                    ACTION_MODIFY = a.ACTION_MODIFY == 1,
                                    ACTION_REMOVE = a.ACTION_REMOVE == 1,
                                    ACTION_VIEW = a.ACTION_VIEW == 1,
                                    ACTION_DETACH = a.ACTION_DETACH == 1,
                                    FUNCTION_ID = a.FUNC_ID
                                }).ToList();
                                return userrole;
                            }
                        }
                        else
                        {
                            HttpContext.Current.Session.Abandon();
                            FormsAuthentication.SignOut();
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at UserAdminApiController : getUserRoleRights", objexception);
                return null;
            }

        }

        [System.Web.Http.HttpGet]
        public IList GetPlanFunctionGroup(int planId)
        {
            try
            {
                if (planId == 0)
                    planId = 45;
                return
                    dbcontext.TB_FUNCTIONGROUP.OrderBy(x => x.SORT_ORDER)
                        .Select(
                            x =>
                                new
                                {
                                    x.SORT_ORDER,
                                    x.FUNCTION_GROUP_ID,
                                    x.FUNCTION_GROUP_NAME,
                                    DEFAULT_ACTION_ALLOW = dbcontext.TB_PLAN_FUNCTIONS.Any(a => a.FUNCTION_GROUP_ID == x.FUNCTION_GROUP_ID && a.PLAN_ID == planId && a.ACTION_ADD == true)
                                }).ToList();
            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController :GetPlanFunctionGroup", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetRoleFunctionGroup(int roleId)
        {
            try
            {

                var functionGroup = dbcontext.TB_FUNCTIONGROUP.OrderBy(x => x.SORT_ORDER)
                     .Select(
                         x =>
                             new
                             {
                                 x.SORT_ORDER,
                                 x.FUNCTION_GROUP_ID,
                                 x.FUNCTION_GROUP_NAME,
                                 DEFAULT_ACTION_ALLOW = dbcontext.TB_ROLE_FUNCTIONS.Any(a => a.FUNCTION_GROUP_ID == x.FUNCTION_GROUP_ID && a.ROLE_ID == roleId && a.ACTION_ADD == true)
                             }).ToList();
                return functionGroup;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController :GetPlanFunctionGroup", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetMyProfileFunctionGroup(string username)
        {
            try
            {
                int roleId = 0;
                if (username == "undefined" || username == null)
                    username = User.Identity.Name;
                var firstOrDefault = dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == username.Trim());
                if (firstOrDefault != null)
                {
                    int intRoleId = 0;
                    var getroleid = dbcontext.vw_aspnet_UsersInRoles
                        .Join(dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                        .Join(dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                            (tcptps, tcp) => new { tcptps, tcp })
                        .Where(x => x.tcptps.tpf.UserName == username).ToList();
                    if (getroleid.Any())
                    {
                        var sd = getroleid.FirstOrDefault();
                        intRoleId = Convert.ToInt32(sd.tcp.Role_id);
                        roleId = intRoleId;
                    }

                }
                return
                    dbcontext.TB_FUNCTIONGROUP.OrderBy(x => x.SORT_ORDER)
                        .Select(
                            x =>
                                new
                                {
                                    x.SORT_ORDER,
                                    x.FUNCTION_GROUP_ID,
                                    x.FUNCTION_GROUP_NAME,
                                    DEFAULT_ACTION_ALLOW = dbcontext.TB_ROLE_FUNCTIONS.Any(a => a.FUNCTION_GROUP_ID == x.FUNCTION_GROUP_ID && a.ROLE_ID == roleId && a.ACTION_ADD == true)
                                }).ToList();
            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController :GetPlanFunctionGroup", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetPlanFunctionsByGroupId(int planId, int functionGrpId)
        {
            try
            {
                if (planId == 0)
                {

                    return dbcontext.TB_FUNCTION.Where(
                        x => x.FUNCTION_GROUP_ID == functionGrpId)
                        .OrderBy(x => x.FUNCTION_ID)
                        .Select(
                            x =>
                                new
                                {
                                    x.FUNCTION_GROUP_ID,
                                    FUNC_ID = x.FUNCTION_ID,
                                    x.FUNCTION_NAME,
                                    DEFAULT_ACTION_ALLOW = 1

                                }).ToList();
                }
                else
                {
                    return dbcontext.TB_PLAN_FUNCTIONS.Where(
                        x => x.PLAN_ID == planId && x.FUNCTION_GROUP_ID == functionGrpId)
                        .OrderBy(x => x.FUNCTION_ID)
                        .Select(
                            x =>
                                new
                                {
                                    x.FUNCTION_GROUP_ID,
                                    x.FUNC_ID,
                                    x.FUNCTION_NAME,
                                    DEFAULT_ACTION_ALLOW = x.ACTION_ADD
                                }).ToList();
                }
            }

            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }

        }

        [System.Web.Http.HttpGet]
        public IList GetRolePlanFunctionsByGroupId(int roleId, int functionGrpId)
        {
            try
            {
                if (roleId == 0)
                {
                    functionGrpId = 0;
                    return dbcontext.TB_FUNCTION.Where(
                        x => x.FUNCTION_GROUP_ID == functionGrpId)
                        .OrderBy(x => x.FUNCTION_ID)
                        .Select(
                            x =>
                                new
                                {
                                    x.FUNCTION_GROUP_ID,
                                    FUNC_ID = x.FUNCTION_ID,
                                    x.FUNCTION_NAME,
                                    ACTION_VIEW = 1,
                                    ACTION_MODIFY = 1,
                                    ACTION_ADD = 1,
                                    ACTION_REMOVE = 1,
                                    x.ALLOW_ADD,
                                    x.ALLOW_MODIFY,
                                    x.ALLOW_REMOVE,
                                    x.ALLOW_VIEW,
                                    x.ALLOW_DETACH
                                }).ToList();
                }
                else
                {
                    return dbcontext.TB_ROLE_FUNCTIONS.Where(
                        x => x.ROLE_ID == roleId && x.FUNCTION_GROUP_ID == functionGrpId && x.FUNCTION_ID != 5000022)
                        .OrderBy(x => x.FUNCTION_ID)
                        .Select(
                            x =>
                                new
                                {
                                    x.FUNCTION_GROUP_ID,
                                    FUNC_ID = x.FUNCTION_ID,
                                    dbcontext.TB_FUNCTION.Where(xx => xx.FUNCTION_ID != 5000022).FirstOrDefault(a => a.FUNCTION_ID == x.FUNCTION_ID)
                                        .FUNCTION_NAME,
                                    x.ACTION_VIEW,
                                    x.ACTION_MODIFY,
                                    x.ACTION_ADD,
                                    x.ACTION_REMOVE,
                                    x.ACTION_DETACH,
                                    dbcontext.TB_FUNCTION.FirstOrDefault(a => a.FUNCTION_ID == x.FUNCTION_ID)
                                        .ALLOW_ADD,
                                    dbcontext.TB_FUNCTION.FirstOrDefault(a => a.FUNCTION_ID == x.FUNCTION_ID)
                                        .ALLOW_MODIFY,
                                    dbcontext.TB_FUNCTION.FirstOrDefault(a => a.FUNCTION_ID == x.FUNCTION_ID)
                                        .ALLOW_REMOVE,
                                    dbcontext.TB_FUNCTION.FirstOrDefault(a => a.FUNCTION_ID == x.FUNCTION_ID)
                                        .ALLOW_VIEW,
                                    dbcontext.TB_FUNCTION.FirstOrDefault(a => a.FUNCTION_ID == x.FUNCTION_ID)
                                        .ALLOW_DETACH,

                                }).ToList();

                }
            }

            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }

        }

        [System.Web.Http.HttpGet]
        public IList GetMyProfilePlanFunctionsByGroupId(string username, int functionGrpId)
        {
            try
            {
                int roleId = 0;
                if (username == "undefined" || username == null)
                    username = User.Identity.Name;
                var firstOrDefault = dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == username.Trim());
                if (firstOrDefault != null)
                {
                    var planids = dbcontext.Customers.Find(firstOrDefault.CustomerId);
                    var planID = planids.PlanId;
                    var newGridPlanFuctiontable = new List<PlanFunctionModel>();
                    var functionList =
                        ((from x in dbcontext.TB_FUNCTION select x).ToList()).Select(FunctionModel.GetModelForEmptyPlan)
                            .ToList();

                    int intRoleId = 0;
                    var getroleid = dbcontext.vw_aspnet_UsersInRoles
                        .Join(dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                        .Join(dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                            (tcptps, tcp) => new { tcptps, tcp })
                        .Where(x => x.tcptps.tpf.UserName == username).ToList();
                    if (getroleid.Any())
                    {
                        var sd = getroleid.FirstOrDefault();
                        intRoleId = Convert.ToInt32(sd.tcp.Role_id);
                        roleId = intRoleId;
                    }


                    if (roleId == 0)
                    {
                        functionGrpId = 0;
                        return dbcontext.TB_FUNCTION.Where(
                            x => x.FUNCTION_GROUP_ID == functionGrpId)
                            .OrderBy(x => x.FUNCTION_ID)
                            .Select(
                                x =>
                                    new
                                    {
                                        x.FUNCTION_GROUP_ID,
                                        FUNC_ID = x.FUNCTION_ID,
                                        x.FUNCTION_NAME,
                                        ACTION_VIEW = 1,
                                        ACTION_MODIFY = 1,
                                        ACTION_ADD = 1,
                                        ACTION_REMOVE = 1,
                                        ACTION_DETACH = 1
                                    }).ToList();
                    }
                    else
                    {
                        return dbcontext.TB_ROLE_FUNCTIONS.Where(
                            x => x.ROLE_ID == roleId && x.FUNCTION_GROUP_ID == functionGrpId && x.FUNCTION_ID != 5000022)
                            .OrderBy(x => x.FUNCTION_ID)
                            .Select(
                                x =>
                                    new
                                    {
                                        x.FUNCTION_GROUP_ID,
                                        FUNC_ID = x.FUNCTION_ID,
                                        dbcontext.TB_FUNCTION.FirstOrDefault(a => a.FUNCTION_ID == x.FUNCTION_ID)
                                            .FUNCTION_NAME,
                                        x.ACTION_VIEW,
                                        x.ACTION_MODIFY,
                                        x.ACTION_ADD,
                                        x.ACTION_REMOVE,
                                        x.ACTION_DETACH
                                    }).ToList();
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }

        }

        [System.Web.Http.HttpPost]
        //public HttpResponseMessage SendInviteToUser(int userinviteid, string useremail, int customerid)
        public HttpResponseMessage SendInviteToUser(int userinviteid, int customerid, Object emailmodel)
        {
            string useremail = emailmodel.ToString();
            var customername = User.Identity.Name;
            //var companyname = dbcontext.Customer_User.Join(dbcontext.Customers, tps => tps.CustomerId, tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
            //    .Where(a => a.tps.User_Name == customername).Select(x => x.tpf.CompanyName).FirstOrDefault();
            var companyname = dbcontext.Customers.Where(a => a.CustomerName == customername).Select(x => x.CompanyName).FirstOrDefault();
            var model = dbcontext.TB_USERINVITE.Where(x => x.ID == userinviteid).FirstOrDefault();
            {

                model.USER_STATUS = "Invited";
                model.DATECREATED = DateTime.Now;
                model.DATEUPDATED = DateTime.Now;
            };

            dbcontext.SaveChanges();
            if (useremail != null)
            {
                const string subject = "Invitation for CatalogStudio Account";
                var body = "Click on the following link to Create an Account for your CatalogStudio ,";

                //body += "<BR/><br/><a HREF=\"" + ConfigurationManager.AppSettings.Get("NewUserRegisterLinkForRegister") + "?CustomerId=" + CommonUtil.Encrypt(Convert.ToString(customerid)) + "&Email=" + CommonUtil.Encrypt(useremail) + "\"> Create a new Register </A>";
                body += "<BR/><br/><a HREF=\"" + ConfigurationManager.AppSettings.Get("NewUserRegisterLinkForRegister") + "?CustomerId=" + CommonUtil.Encrypt(Convert.ToString(customerid)) + "&ID=" + CommonUtil.Encrypt(Convert.ToString(userinviteid)) + "\"> Create a new Register </A>";
                body += "<br/>" + "<br/>" + "If the above link doesn't work, try the following: <b>.";
                body += "<br/>Mozilla Users- Right click on the 'Create a new password' link. Select 'Copy link location' & paste the URL in the address bar.";
                body += "<br/>Internet Explorer Users- Right click on the 'Create a new password' link. Select 'Open in New Window/Tab' option.";
                body += "<br/><br/> Note: This link will be functional for a one time use ";
                body += "<br/><br/><b>Regards,</b>";
                body += "<br/><br/> " + companyname + " Team";
                CommonUtil.SendEmailForCompanyandUserRegister(subject, useremail, body, "New Invitation");
            }
            else
            {
                ModelState.AddModelError("Error", Resource.AppController_ForgotPassword_User_Name_is_InCorrect__Please_Check);
                return Request.CreateResponse(HttpStatusCode.OK, "Invitation not sent, please resend");
            }
            return Request.CreateResponse(HttpStatusCode.OK, "Invitation sent successfully");
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveUserInvite(int userInvitecustomerID, object userinvites)
        {
            //Save UserInvite
            try
            {
                string userinvite = Convert.ToString(userinvites);
                if (!string.IsNullOrEmpty(userinvite))
                {
                    var checkEmailExisit = dbcontext.TB_USERINVITE.Where(a => a.EMAIL == userinvite).ToList();
                    if (checkEmailExisit.Any())
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Email Id already exists");
                    }
                    //var customername = User.Identity.Name;
                    //var customerid = dbcontext.Customer_User.Where(a => a.User_Name == customername).Select(x => x.CustomerId).FirstOrDefault();
                    TB_USERINVITE model = new TB_USERINVITE();

                    model.EMAIL = userinvite;
                    model.CUSTOMERID = userInvitecustomerID;
                    model.USER_STATUS = "New";
                    model.DATECREATED = DateTime.Now;
                    model.DATEUPDATED = DateTime.Now;


                    dbcontext.TB_USERINVITE.Add(model);
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "User added successfully");
                }
                return Request.CreateResponse(HttpStatusCode.OK, "User not added");
            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController :SaveUserInvite", objException);
                return null;

            }



        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage RemoveUserInvite(int userinvite)
        {

            try
            {
                TB_USERINVITE invite = dbcontext.TB_USERINVITE.Find(userinvite);
                if (invite != null)
                {
                    dbcontext.TB_USERINVITE.Remove(invite);
                    dbcontext.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK, "Remove successful");
            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController :RemoveUserInvite", objException);
                return Request.CreateResponse(HttpStatusCode.OK, "Invited User not removed");
            }


        }

        [System.Web.Http.HttpGet]
        public IList GetUserInvite(int userInvitecustomerID)
        {

            try
            {
                //var customername = User.Identity.Name;
                //var customerID = dbcontext.Customer_User.Where(a => a.User_Name == customername).Select(a => a.CustomerId).FirstOrDefault();
                var userinviteDetails = dbcontext.TB_USERINVITE.Where(x => x.CUSTOMERID == userInvitecustomerID).Select(x => new
                {
                    x.ID,
                    x.EMAIL,
                    x.CUSTOMERID,
                    x.USER_STATUS,
                    x.DATEUPDATED
                }).OrderByDescending(x => x.DATEUPDATED);
                return userinviteDetails.ToList();
            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController :GetUserInvite", objException);
                return null;
            }
        }

        //[System.Web.Http.HttpGet]
        //public IList GetUserInvite(string userinvite)
        //{

        //    try
        //    {
        //        var customername = User.Identity.Name;
        //        //var customerid = dbcontext.Customers.Where(a => a.CustomerName == customername).Select(x => x.CustomerId).FirstOrDefault();

        //        if (userinvite == "" || userinvite == null)
        //        {
        //            //var userinviteDetails=dbcontext.TB_USERINVITE.Where(x => x.CUSTOMERID == dbcontext.Customers.Where(a => a.CustomerName == customername).Select(a => a.CustomerId).FirstOrDefault())
        //            //                .OrderByDescending(x => x.DATEUPDATED).ToList();
        //            var userinviteDetails = dbcontext.TB_USERINVITE.OrderByDescending(x => x.DATEUPDATED).ToList();
        //            return userinviteDetails;

        //        }
        //        else
        //        {
        //            //Save UserInvite
        //            var model = new TB_USERINVITE
        //            {
        //                EMAIL = userinvite,
        //                USER_STATUS = "Invite",
        //                DATECREATED = DateTime.Now,
        //                DATEUPDATED = DateTime.Now,
        //                CUSTOMERID = dbcontext.Customers.Where(a => a.CustomerName == customername).Select(x => x.CustomerId).FirstOrDefault()



        //            };
        //            dbcontext.TB_USERINVITE.Add(model);
        //            dbcontext.SaveChanges();
        //            //var userinviteDetails = dbcontext.TB_USERINVITE.Where(x => x.CUSTOMERID == dbcontext.Customers.Where(a => a.CustomerName == customername).Select(a => a.CustomerId).FirstOrDefault())
        //            //                 .OrderByDescending(x => x.DATEUPDATED).ToList();
        //            var userinviteDetails = dbcontext.TB_USERINVITE.OrderByDescending(x => x.DATEUPDATED).ToList();
        //            return userinviteDetails;

        //        }

        //    }
        //    catch (Exception objException)
        //    {
        //        _logger.Error("Error at UserAdminApiController :GetUserInvite", objException);
        //        return null;
        //    }
        //}


        // [System.Web.Http.HttpPost]
        //public HttpResponseMessage SaveModifiedUserInviteData(Object userinvites)
        //{


        //    return Request.CreateResponse(HttpStatusCode.OK, "Updated.");


        //    //SaveModifiedUserInviteData
        //    //try
        //    //{
        //    //   // var userinvites ="Test";
        //    //    string userinvite = Convert.ToString(userinvites);
        //    //    if (!string.IsNullOrEmpty(userinvite))
        //    //    {
        //    //        var checkEmailExisit = dbcontext.TB_USERINVITE.Where(a => a.EMAIL == userinvite).ToList();
        //    //        if (checkEmailExisit.Any())
        //    //        {
        //    //            return Request.CreateResponse(HttpStatusCode.OK, "Email Id is Already Exsist.");
        //    //        }
        //    //        var customername = User.Identity.Name;
        //    //        var customerid = dbcontext.Customer_User.Where(a => a.User_Name == customername).Select(x => x.CustomerId).FirstOrDefault();
        //    //        TB_USERINVITE model = new TB_USERINVITE();

        //    //        model.EMAIL = userinvite;
        //    //        model.CUSTOMERID = customerid;
        //    //        model.USER_STATUS = "New";
        //    //        model.DATECREATED = DateTime.Now;
        //    //        model.DATEUPDATED = DateTime.Now;


        //    //        dbcontext.TB_USERINVITE.Add(model);
        //    //        dbcontext.SaveChanges();
        //    //        return Request.CreateResponse(HttpStatusCode.OK, "Invite Added Successfully.");
        //    //    }
        //    //    return Request.CreateResponse(HttpStatusCode.OK, "Invite Not Added.Please Check.");
        //    //}
        //    //catch (Exception objException)
        //    //{
        //    //    _logger.Error("Error at UserAdminApiController :GetUserInvite", objException);
        //    //    return null;

        //    //}



        //}
        [HttpPost]
        public IHttpActionResult SaveModifiedUserInviteData(object model)
        {

            try
            {

                var userInviteList = ((JArray)model).Select(x => new TB_USERINVITE()
                {
                    ID = (int)x["ID"],
                    CUSTOMERID = (int)x["CUSTOMERID"],
                    EMAIL = (string)x["EMAIL"],
                    USER_STATUS = (string)x["USER_STATUS"]

                }).ToList();

                foreach (var item in userInviteList)
                {
                    if (item.USER_STATUS == "New")
                    {
                        var userinvitemodel = dbcontext.TB_USERINVITE.Where(x => x.ID == item.ID).FirstOrDefault();
                        {
                            userinvitemodel.EMAIL = item.EMAIL;
                            userinvitemodel.USER_STATUS = item.USER_STATUS;
                            userinvitemodel.DATEUPDATED = DateTime.Now;
                        };
                        dbcontext.SaveChanges();
                    }
                }

                return Ok("Updated");
            }

            catch (Exception ex)
            {
                _logger.Error("Error at SaveModifiedUserInviteData:UserAdminApiController", ex);
                return null;
            }
            //return Ok();

        }

        #region Custom Menu
        /// <summary>
        /// Custom menu in settings->admin->custom
        /// </summary>
        /// <param name="Header">Header</param>
        /// <param name="URL">URL</param>
        /// <returns>Saved if success or Error if failed</returns>
        /// </summary>
        [System.Web.Http.HttpPost]
        public HttpResponseMessage saveCustom(string Header, string URL)
        {
            try
            {
                var customerid = dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                var objCustomermenu = new Custom_Menu();
                var customerId = Convert.ToInt32(customerid.CustomerId);
                var checkCount = dbcontext.Custom_Menu.Where(x => x.Customer_ID == customerId && x.Header == Header).ToList();
                if (checkCount.Count == 0)
                {
                    objCustomermenu.Customer_ID = Convert.ToInt32(customerid.CustomerId);
                    objCustomermenu.Header = Header;
                    if (URL.StartsWith("www."))
                    {
                        objCustomermenu.URL = URL;
                    }
                    else
                    {
                        Uri uri = new Uri(URL);
                        string URLWithoutport = uri.Host + uri.PathAndQuery;
                        objCustomermenu.URL = URLWithoutport;
                    }
                    dbcontext.Custom_Menu.Add(objCustomermenu);
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Saved.");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Duplicate");
                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController : Custommenu", objException);
                return Request.CreateResponse(HttpStatusCode.OK, "Error.");
            }

        }

        /// <summary>
        /// Custom app in dropdown menu 
        /// </summary>
        /// <returns></returns>
        /// </summary>
        [System.Web.Http.HttpGet]
        public IList getHeader()
        {
            try
            {
                var UserId = dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var result = dbcontext.Custom_Menu.Where(x => x.Customer_ID == UserId).Select(y => new { y.ID, y.Header, y.URL }).ToList();
                return result;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Custom ", ex);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public IList getUserRoleId()
        {
            try
            {
                var UserId = dbcontext.aspnet_Membership.Where(x => x.Email == User.Identity.Name).Select(y => y.UserId).FirstOrDefault();
                var result = dbcontext.vw_aspnet_UsersInRoles.Where(x => x.UserId == UserId).Select(y => new { y.UserId, y.RoleId }).ToList();

                string Encrypt_UserId = result[0].UserId.ToString();
                string Encrypt_RoleId = result[0].RoleId.ToString();

                Encrypt_UserId = HttpUtility.HtmlEncode(Security.Encrypt(Encrypt_UserId));
                Encrypt_RoleId = HttpUtility.HtmlEncode(Security.Encrypt(Encrypt_RoleId));

                List<string> result_User = new List<string>();
                result_User.Add(Encrypt_UserId);
                result_User.Add(Encrypt_RoleId);



                return result_User.ToList();
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Custom ", ex);
                return null;
            }
        }
        /// <summary>
        /// get header in setting->admin-custom page
        /// </summary>
        /// <param name="request">selected row as passed as request</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public IList getHeaderForMenu()
        {
            try
            {
                var UserId = dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var result = dbcontext.Custom_Menu.Where(x => x.Customer_ID == UserId).Select(y => new { y.ID, y.Header, y.URL }).ToList();
                return result;
                //var result = dbcontext.Custom_Menu.Where(x => x.Customer_ID == UserId).Select(y => new { y.ID, y.Header, y.URL }).ToList().AsQueryable();

                // return result.ToDataSourceResult(request);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Custom ", ex);
                return null;
            }
        }

        /// <summary>
        /// Custom menu Grid update
        /// </summary>
        /// <param name="model">get all values from grid</param>
        /// <returns>treu if updated</returns>
        /// </summary>
        [System.Web.Http.HttpPost]
        public bool updateCustomValues(JArray model)
        {
            try
            {
                var CustomeMenuUpdate = model.ToList();
                foreach (var item in CustomeMenuUpdate)
                {
                    int id = Convert.ToInt32(item["ID"]);
                    string Header = Convert.ToString(item["Header"]);
                    string URL = Convert.ToString(item["URL"]);
                    if (!URL.StartsWith("www."))
                    {
                        Uri uri1 = new Uri(URL, UriKind.RelativeOrAbsolute);
                        // string URLWithoutport = uri1.Host + uri1.PathAndQuery;
                        //Uri uri = new Uri(URL);
                        //string URLWithoutport = uri.Host + uri.PathAndQuery;
                        URL = Convert.ToString(uri1);
                    }
                    var CustomModel = dbcontext.Custom_Menu.Where(x => x.ID == id).FirstOrDefault();
                    {
                        CustomModel.Header = Header;
                        CustomModel.URL = URL;
                    };
                    dbcontext.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at UserAdminApiController ", ex);
                return false;
            }
        }

        /// <summary>
        ///  Custom menu delete
        /// </summary>
        /// <param name="ID">ID</param>
        /// <returns>true if deleted or false if not deleted </returns>

        [HttpPost]
        public bool deleteCustomemenu(int ID)
        {
            try
            {
                var CustomModel = dbcontext.Custom_Menu.Where(x => x.ID == ID).FirstOrDefault();

                dbcontext.Custom_Menu.Remove(CustomModel);
                dbcontext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Custom ", ex);
                return false;
            }
        }
        #endregion

        #region User Export

        /// <summary>
        /// Export the user Details in .xls  format
        /// </summary>
        /// <author>Kowshikkumar</author>
        /// <param name="ID">Nothing pass the parameter</param>
        /// <returns>if the process sucessfully completed it returns true otherwise returns false</returns>
        [System.Web.Http.HttpPost]

        public string GetUserExportDetails(int exportTypeValues)
        {
            try
            {
                string assignExportType = string.Empty;
                int count = 0;
                int customerid = dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                var userExportTable = new DataTable();
                if (exportTypeValues == 2)
                {
                    assignExportType = "ACTIVE";
                }
                else if (exportTypeValues == 3)
                {
                    assignExportType = "INACTIVE";
                }
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_USER_EXPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Parameters.Add("@USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.Parameters.Add("@CUSTOMER_ID", SqlDbType.Int).Value = customerid;
                    objSqlConnection.Open();
                    var userExport = new SqlDataAdapter(objSqlCommand);
                    userExport.Fill(userExportTable);
                    DataColumn Col = userExportTable.Columns.Add("Action");
                    Col.SetOrdinal(0);
                    userExportTable.AcceptChanges();



                    if (assignExportType.Equals("ACTIVE") || assignExportType.Equals("INACTIVE"))
                    {
                        foreach (DataRow dr in userExportTable.Rows)
                        {
                            string chkstatus = dr["STATUS"].ToString();
                            chkstatus = chkstatus.ToUpper();
                            if (!chkstatus.Equals(assignExportType))
                            {
                                dr.Delete();
                            }
                            else
                            {
                                count = count + 1;
                            }

                        }
                    }
                    if (count == 0 && exportTypeValues != 1)
                    {
                        return "No Value";
                    }
                    userExportTable.AcceptChanges();
                    System.Web.HttpContext.Current.Session["userExportTable"] = userExportTable;
                    try
                    {

                        string filename = "UserExport.xls";
                        var workbook = new Workbook();

                        if (userExportTable.Rows.Count > 0)
                        {

                            string tableName = "UserExport";
                            int rowcntTemp = userExportTable.Rows.Count;
                            int j = 0;
                            int runningcnt = 0;
                            do
                            {
                                int rowcnt;
                                if (rowcntTemp <= 65000)
                                {
                                    rowcnt = rowcntTemp;
                                    rowcntTemp = rowcnt - 65000;
                                }
                                else
                                {
                                    rowcnt = 65000;
                                    rowcntTemp = userExportTable.Rows.Count - (runningcnt + 65000);
                                }
                                j++;
                                if (j != 1)
                                {
                                    tableName = tableName + (j - 1);
                                }
                                var worksheet = workbook.Worksheets.Add(tableName);
                                workbook.WindowOptions.SelectedWorksheet = worksheet;
                                for (int columnIndex = 0; columnIndex < userExportTable.Columns.Count; columnIndex++)
                                {

                                    if (HttpUtility.HtmlDecode(userExportTable.Columns[columnIndex].ColumnName) != "ID" && HttpUtility.HtmlDecode(userExportTable.Columns[columnIndex].ColumnName) != "ROLE_ID" && HttpUtility.HtmlDecode(userExportTable.Columns[columnIndex].ColumnName) != "CATALOGID")
                                    {


                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(userExportTable.Columns[columnIndex].ColumnName);

                                    }
                                }
                                int rowIndex = 2;
                                int temprunningcnt = runningcnt;
                                for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                                {
                                    var row = worksheet.Rows[rowIndex++];
                                    runningcnt++;
                                    for (int columnIndex = 0; columnIndex < userExportTable.Rows[k].ItemArray.Length; columnIndex++)
                                    {
                                        row.Cells[columnIndex].Value = userExportTable.Rows[k].ItemArray[columnIndex];
                                    }
                                }

                            }
                            while (rowcntTemp > 0);
                        }
                        else
                        {
                            return "No Values";
                        }
                        string filepath = HttpContext.Current.Server.MapPath("~/Content/" + filename);
                        if (File.Exists(filepath))
                        {
                            File.Delete(filepath);
                        }
                        workbook.Save(filepath);

                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error at getUserExportDetails ", ex);
                        return "false";
                    }
                }
                return "true";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at getUserExportDetails ", ex);
                return "false";
            }
        }
        #endregion

        #region Role Export

        /// <summary>
        /// Export the role values of all users  
        /// passing the parameter user name and customer id  in stp
        /// get the values in datataset 
        /// </summary>
        /// <returns>If the process is succesfully completed returns true otherwise return false</returns>
        [System.Web.Http.HttpPost]
        public string GetRoleExport()
        {
            try
            {
                int customerid = dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                DataSet roleExport = new DataSet();
                DataTable roleExportTable = new DataTable();
                string filename = "RoleExport.xls";
                string[] tableName = { "RoleCatalog", "RoleGroup_name", "RoleAttribute" };
                var workbook = new Workbook();
                int incTableName = 0;
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_ROLE_EXPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Parameters.Add("@USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = customerid;
                    objSqlConnection.Open();
                    var Export = new SqlDataAdapter(objSqlCommand);
                    Export.Fill(roleExport);
                }
                roleExport.Tables[2].Columns.Add("Allow Edit");
                roleExport.Tables[0].Columns.Add("ACTION").SetOrdinal(0);
                roleExport.Tables[1].Columns.Add("ACTION").SetOrdinal(0);
                roleExport.Tables[2].Columns.Add("ACTION").SetOrdinal(0);
                roleExport.Tables[1].Columns.Add("ACTION_DETACH").SetOrdinal(10);
                roleExport.Tables[2].AcceptChanges();
                roleExport.AcceptChanges();
                foreach (DataRow dr in roleExport.Tables[2].Rows)
                {
                    dr["Allow Edit"] = "Yes";
                    string chkAttrID = dr["ATTRIBUTE_ID"].ToString();
                    string chkRoleID = dr["ROLE_ID"].ToString();
                    foreach (DataRow chk in roleExport.Tables[3].Rows)
                    {
                        string attrId = chk["ATTRIBUTE_ID"].ToString();
                        string roleID = chk["ROLE_ID"].ToString();
                        if (chkAttrID.Equals(attrId) && chkRoleID.Equals(roleID))
                        {
                            dr["Allow Edit"] = "No";
                        }

                    }
                }
                roleExport.Tables[2].AcceptChanges();
                roleExport.Tables.RemoveAt(3);

                foreach (DataRow dr in roleExport.Tables[1].Rows)
                {
                    string chkFunctionId = dr["FUNCTION_ID"].ToString();
                    int intFuctionId = int.Parse(chkFunctionId);
                    string chkFunctiongrpId = dr["FUNCTION_GROUP_ID"].ToString();
                    int intFunctiongrpId = int.Parse(chkFunctiongrpId);
                    string chkRoleId = dr["ROLE_ID"].ToString();
                    int intRoleId = int.Parse(chkRoleId);
                    var actionDetach = dbcontext.TB_ROLE_FUNCTIONS.Where(x => x.FUNCTION_GROUP_ID == intFunctiongrpId && x.FUNCTION_ID == intFuctionId && x.ROLE_ID == intRoleId).Select(x => x.ACTION_DETACH).FirstOrDefault();
                    dr["ACTION_DETACH"] = actionDetach.ToString().ToUpper();
                }
                if (roleExport.Tables[1].Columns.Contains("FUNCTION_ID"))
                {
                    roleExport.Tables[1].Columns.Remove("FUNCTION_ID");
                }
                if (roleExport.Tables[1].Columns.Contains("ROLE_ID"))
                {
                    roleExport.Tables[1].Columns.Remove("ROLE_ID");
                }
                if (roleExport.Tables[1].Columns.Contains("FUNCTION_GROUP_ID"))
                {
                    roleExport.Tables[1].Columns.Remove("FUNCTION_GROUP_ID");
                }
                if (roleExport.Tables[2].Columns.Contains("ATTRIBUTE_ID"))
                {
                    roleExport.Tables[2].Columns.Remove("ATTRIBUTE_ID");
                }
                if (roleExport.Tables[2].Columns.Contains("ROLE_ID"))
                {
                    roleExport.Tables[2].Columns.Remove("ROLE_ID");
                }
                roleExport.AcceptChanges();

                System.Web.HttpContext.Current.Session["roleExport"] = roleExport;
                foreach (DataTable dt in roleExport.Tables)
                {

                    roleExportTable = dt;
                    if (roleExportTable.Rows.Count > 0)
                    {

                        string table = tableName[incTableName];
                        incTableName = incTableName + 1;
                        int rowcntTemp = roleExportTable.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        do
                        {
                            int rowcnt;
                            if (rowcntTemp <= 65000)
                            {
                                rowcnt = rowcntTemp;
                                rowcntTemp = rowcnt - 65000;
                            }
                            else
                            {
                                rowcnt = 65000;
                                rowcntTemp = roleExportTable.Rows.Count - (runningcnt + 65000);
                            }
                            j++;
                            if (j != 1)
                            {
                                table = table + (j - 1);
                            }
                            var worksheet = workbook.Worksheets.Add(table);
                            workbook.WindowOptions.SelectedWorksheet = worksheet;
                            for (int columnIndex = 0; columnIndex < roleExportTable.Columns.Count; columnIndex++)
                            {

                                if (HttpUtility.HtmlDecode(roleExportTable.Columns[columnIndex].ColumnName) != "ID" && HttpUtility.HtmlDecode(roleExportTable.Columns[columnIndex].ColumnName) != "ROLE_ID" && HttpUtility.HtmlDecode(roleExportTable.Columns[columnIndex].ColumnName) != "CATALOGID")
                                {


                                    worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(roleExportTable.Columns[columnIndex].ColumnName);

                                }
                            }
                            int rowIndex = 2;
                            int temprunningcnt = runningcnt;
                            for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                            {
                                var row = worksheet.Rows[rowIndex++];
                                runningcnt++;
                                for (int columnIndex = 0; columnIndex < roleExportTable.Rows[k].ItemArray.Length; columnIndex++)
                                {
                                    row.Cells[columnIndex].Value = roleExportTable.Rows[k].ItemArray[columnIndex];
                                }
                            }

                        }
                        while (rowcntTemp > 0);
                    }
                    else
                    {
                        return "No Values";
                    }


                }
                string filepath = HttpContext.Current.Server.MapPath("~/Content/" + filename);
                if (File.Exists(filepath))
                {
                    File.Delete(filepath);
                }
                workbook.Save(filepath);


                return "true";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at UserAdmin : GetRoleExport", ex);
                return null;
            }
        }
        #endregion
        #region Announcement
        /// <summary>
        /// Announcement 
        /// </summary>
        /// <param name="Header">Header</param>
        /// <param name="URL">URL</param>
        /// <returns>Saved if success or Error if failed</returns>
        /// </summary>
        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveAnnouncement(DateTime AnnouncementDate, string AnnouncementSubject, string AnnouncementDescription)
        {
            try
            {
                var customerid = dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                var objAnnouncement = new TB_ANNOUNCEMENT();
                var customerId = Convert.ToInt32(customerid.CustomerId);
                var checkCount = dbcontext.TB_ANNOUNCEMENT.Where(x => x.Customer_ID == customerId && x.Announcement_Header == AnnouncementSubject).ToList();
                if (checkCount.Count == 0)
                {
                    objAnnouncement.Customer_ID = Convert.ToInt32(customerid.CustomerId);
                    objAnnouncement.Announcement_Header = AnnouncementSubject;
                    objAnnouncement.Announcement_Description = AnnouncementDescription;
                    objAnnouncement.Announcement_Date = AnnouncementDate;
                    dbcontext.TB_ANNOUNCEMENT.Add(objAnnouncement);
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Saved.");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Duplicate");
                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController : Custommenu", objException);
                return Request.CreateResponse(HttpStatusCode.OK, "Error.");
            }

        }

        /// <summary>
        /// get header in setting->admin-custom page
        /// </summary>
        /// <param name="request">selected row as passed as request</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public IList getAnnouncement()
        {
            try
            {
                var UserId = dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var result = dbcontext.TB_ANNOUNCEMENT.Where(x => x.Customer_ID == UserId).AsEnumerable().Select(y => new { y.Id, Announcement_Date = String.Format("{0:yyyy-MM-dd}", y.Announcement_Date), y.Announcement_Header, y.Announcement_Description }).ToList();
                return result;
                //var result = dbcontext.Custom_Menu.Where(x => x.Customer_ID == UserId).Select(y => new { y.ID, y.Header, y.URL }).ToList().AsQueryable();

                // return result.ToDataSourceResult(request);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Custom ", ex);
                return null;
            }
        }
        /// <summary>
        /// get header in setting->admin-custom page
        /// </summary>
        /// <param name="request">selected row as passed as request</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public IList getAnnouncementLastDateRecords()
        {
            try
            {
                var UserId = dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var annonucementCount = dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == UserId).Select(y => y.Announcement_Count).FirstOrDefault();
                var result = dbcontext.TB_ANNOUNCEMENT.Where(x => x.Customer_ID == UserId).OrderByDescending(x => x.Announcement_Date).Take(annonucementCount).ToArray().Select(y => new { y.Id, Announcement_Date = (string.Format("{0:MM/dd/yyyy}", y.Announcement_Date)), y.Announcement_Header, y.Announcement_Description }).ToList();
                return result;
                //var result = dbcontext.Custom_Menu.Where(x => x.Customer_ID == UserId).Select(y => new { y.ID, y.Header, y.URL }).ToList().AsQueryable();

                // return result.ToDataSourceResult(request);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Custom ", ex);
                return null;
            }
        }

        /// <summary>
        /// Custom menu Grid update
        /// </summary>
        /// <param name="model">get all values from grid</param>
        /// <returns>treu if updated</returns>
        /// </summary>
        [System.Web.Http.HttpPost]
        public bool updateAnnouncementValues(int id, string description, string header, DateTime date)
        {
            try
            {
                var announcementModel = dbcontext.TB_ANNOUNCEMENT.Where(x => x.Id == id).FirstOrDefault();
                {
                    announcementModel.Announcement_Date = date;
                    announcementModel.Announcement_Header = header;
                    announcementModel.Announcement_Description = description;
                };
                dbcontext.SaveChanges();
                //}

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at UserAdminApiController ", ex);
                return false;
            }
        }


        [HttpPost]
        public bool deleteAnnouncement(int ID)
        {
            try
            {
                var announcement = dbcontext.TB_ANNOUNCEMENT.Where(x => x.Id == ID).FirstOrDefault();

                dbcontext.TB_ANNOUNCEMENT.Remove(announcement);
                dbcontext.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Custom ", ex);
                return false;
            }
        }


        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveApplicationSettings(int CustomerId, int announcementCount, string restoreSourcePath, string restoreDestPath, string serverPath, string dataSource, string serverUserID, string Password, string backUpAttachement, string backUpMdfPath, string backUpDataBasePath, int importSheetProductCount)
        {
            try
            {
                var count = dbcontext.TB_APPLICATION_SETTINGS.Count(x => x.CustomerId == CustomerId);
                if (count == 0)
                {
                    var objApplicationSettings = new TB_APPLICATION_SETTINGS();
                    objApplicationSettings.CustomerId = CustomerId;
                    objApplicationSettings.Announcement_Count = announcementCount;
                    objApplicationSettings.RestoreSourcePath = restoreSourcePath;
                    objApplicationSettings.RestoreDestPath = restoreDestPath;
                    objApplicationSettings.ServerPath = serverPath;
                    objApplicationSettings.DataSource = dataSource;
                    objApplicationSettings.UserID = serverUserID;
                    objApplicationSettings.Password = Password;
                    objApplicationSettings.BackUpAttachement = backUpAttachement;
                    objApplicationSettings.BackUpDataBasePath = backUpDataBasePath;
                    objApplicationSettings.BackUpMdfPath = backUpMdfPath;
                    objApplicationSettings.ImportSheetProductCount = importSheetProductCount;
                    dbcontext.TB_APPLICATION_SETTINGS.Add(objApplicationSettings);
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Saved.");

                }
                else
                {
                    var objApplicationSettings = dbcontext.TB_APPLICATION_SETTINGS.Single(s => s.CustomerId == CustomerId);
                    objApplicationSettings.CustomerId = CustomerId;
                    objApplicationSettings.Announcement_Count = announcementCount;
                    objApplicationSettings.RestoreSourcePath = restoreSourcePath;
                    objApplicationSettings.RestoreDestPath = restoreDestPath;
                    objApplicationSettings.ServerPath = serverPath;
                    objApplicationSettings.DataSource = dataSource;
                    objApplicationSettings.UserID = serverUserID;
                    objApplicationSettings.Password = Password;
                    objApplicationSettings.BackUpAttachement = backUpAttachement;
                    objApplicationSettings.BackUpDataBasePath = backUpDataBasePath;
                    objApplicationSettings.BackUpMdfPath = backUpMdfPath;
                    objApplicationSettings.ImportSheetProductCount = importSheetProductCount;
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Update.");
                }

            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController : SaveApplicationSettings", objException);
                return Request.CreateResponse(HttpStatusCode.OK, "Error.");
            }

        }

        [System.Web.Http.HttpGet]
        public IList GetApplicationSettingDetails(int customerId)
        {
            try
            {


                var objPrjName = dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customerId).Select(x => new
                //if (objPrjName != null)
                {
                    x.Announcement_Count,
                    x.ImportSheetProductCount,
                    x.RestoreSourcePath,
                    x.RestoreDestPath,
                    x.ServerPath,
                    x.DataSource,
                    x.UserID,
                    x.Password,
                    x.BackUpAttachement,
                    x.BackUpMdfPath,
                    x.BackUpDataBasePath
                }).ToList();

                return objPrjName;
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public string getSAASCustomer()
        {
            try
            {
                var checkSaasCustomer = System.Web.Configuration.WebConfigurationManager.AppSettings["SAAS"].ToString();
                return checkSaasCustomer;
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
                return null;
            }
        }
        [HttpPost]
        public HttpResponseMessage saveworkflow(string workflowstatus, string workflowdescription)
        {
            try
            {
                var customerid = dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                var objCustomermenu = new Custom_Menu();
                var customerId = Convert.ToInt32(customerid.CustomerId);
                var objattribute = new TB_WORKFLOW_STATUS
                {
                    STATUS_CODE = dbcontext.TB_WORKFLOW_STATUS.Max(y => y.STATUS_CODE) + 1,
                    STATUS_NAME = workflowstatus,
                    STATUS_DESCRIPTION = workflowdescription,
                    MODIFIED_DATE = DateTime.Now,
                    MODIFIED_USER = User.Identity.Name,
                };
                dbcontext.TB_WORKFLOW_STATUS.Add(objattribute);
                dbcontext.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, "Saved.");
            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController : Custommenu", objException);
                return Request.CreateResponse(HttpStatusCode.OK, "Error.");
            }

        }

        [HttpPost]
        public HttpResponseMessage Saveworkflowsettings(string Selectedrole, string Selectedfromstatus, string SelectedTostatus)
        {
            try
            {
                var customerid = dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                var objCustomermenu = new Custom_Menu();
                var customerId = Convert.ToInt32(customerid.CustomerId);
                int fromStatuscode = Convert.ToInt32(Selectedfromstatus);
                int toStatuscode = Convert.ToInt32(SelectedTostatus);
                int RoleId = Convert.ToInt32(Selectedrole);
                var RoleName = dbcontext.aspnet_Roles.Where(x => x.Role_id == RoleId).Select(y => y.RoleName).FirstOrDefault();
                var fromStatus = dbcontext.TB_WORKFLOW_STATUS.Where(x => x.STATUS_CODE == fromStatuscode).Select(y => y.STATUS_NAME).FirstOrDefault();
                var toStatus = dbcontext.TB_WORKFLOW_STATUS.Where(x => x.STATUS_CODE == toStatuscode).Select(y => y.STATUS_NAME).FirstOrDefault();
                var dupcount = dbcontext.TB_WORKFLOW_SETTINGS.Where(x => x.USER_ROLE == Selectedrole && x.TO_STATUS == toStatus && x.FROM_STATUS == fromStatus && x.ROLE_NAME == RoleName).FirstOrDefault();
                if (dupcount == null)
                {
                    var objattribute = new TB_WORKFLOW_SETTINGS
                    {

                        USER_ROLE = Selectedrole,
                        ROLE_NAME = RoleName,
                        FROM_STATUS = fromStatus,
                        TO_STATUS = toStatus,


                    };
                    dbcontext.TB_WORKFLOW_SETTINGS.Add(objattribute);
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Saved.");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "not saved.");
                }


            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController : Custommenu", objException);
                return Request.CreateResponse(HttpStatusCode.OK, "Error.");
            }

        }


        [HttpPost]
        public DataSourceResult GetAllWorkFlowsettingsDataSourceValue(DataSourceRequest request)
        {
            if (request.Sort == null || !request.Sort.Any())
            {
                request.Sort = new List<Sort> { new Sort { Field = "USER_ROLE", Dir = "asc" } };
            }
            try
            {
                var workflowTable = dbcontext.TB_WORKFLOW_SETTINGS.Select(z => new
                {
                    z.USER_ROLE,
                    z.ROLE_NAME,
                    z.FROM_STATUS,
                    z.TO_STATUS
                });
                return workflowTable.ToDataSourceResult(request);
            }
            catch (Exception objException)
            {
                _logger.Error("Error at HomeApiController : GetAllWorkFlowDataSourceValue", objException);
                return null;
            }
        }

        #endregion
        
        [System.Web.Http.HttpGet]
        public IList getUserWebList()
        {
            // var getuserdetails1 = dbcontext.QSWS_WEB_CONTACTS.OrderByDescending(x=>x.CREATED_DATE).ToList();
            var getuserdetails = dbcontext.QSWS_WEB_CONTACTS.Select(a => new UserWebModel
            {

                FirstName = a.FIRST_NAME,
                LastName = a.LAST_NAME,
                Email = a.CONTACT_EMAIL,
                CompanyName = a.COMPANY_NAME,
                ActiveUser = dbcontext.QSWS_USER.Any(x => x.USER_EMAIL == a.CONTACT_EMAIL) ? dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == a.CONTACT_EMAIL).Select(y => y.USER_STATUS).FirstOrDefault() : 0,

            }).ToList();

            return getuserdetails;
        }
        //[System.Web.Http.HttpGet]
        //public IList selectrowevent(string Email_id)
        //{
        //    var selectrow = dbcontext.QSWS_WEB_CONTACTS.Where(x => x.CONTACT_EMAIL == Email_id).ToList();
        //    var activestatus = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == Email_id).Select(x => x.USER_STATUS).FirstOrDefault().ToString();
        //    int Status = Convert.ToInt16(activestatus);
        //    bool UserStatus = Convert.ToBoolean(Status);
        //    var checkFarm = dbcontext.QSWS_FARM_PROFILE_CONTACTS.Where(x => x.EMAIL_ADDRESS == Email_id).ToList();
        //    var checkmember = dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == Email_id).ToList();
        //    var checkTWI = dbcontext.QSWS_TWI_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == Email_id).ToList();




        //    if (checkFarm.Count != 0)
        //    {
        //        var Framdetails = dbcontext.QSWS_WEB_CONTACTS.Where(x => x.CONTACT_EMAIL == Email_id).Select(a => new RowData
        //        {
        //            FIRST_NAME = a.FIRST_NAME,
        //            LAST_NAME = a.LAST_NAME,
        //            CONTACT_EMAIL = a.CONTACT_EMAIL,
        //            USER_ID = 0,
        //            //PHONE = a.PHONE,
        //            //MOBILE = a.MOBILE,
        //            COMPANY_NAME = a.COMPANY_NAME,
        //            NOTES = a.NOTES,
        //            STATUS = UserStatus,
        //            role = dbcontext.QSWS_USER.Any(x => x.USER_EMAIL == a.CONTACT_EMAIL) ? dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == a.CONTACT_EMAIL).Select(x => x.USER_ROLE).FirstOrDefault() : 0,
        //            ORGID = dbcontext.QSWS_USER.Any(x => x.USER_EMAIL == a.CONTACT_EMAIL) ? dbcontext.QSWS_FARM_PROFILE_CONTACTS.Where(x => x.EMAIL_ADDRESS == a.CONTACT_EMAIL).Select(x => x.FARM_ID).FirstOrDefault() : 0,
        //            TYPE = 2,

        //        }).ToList();
        //        return Framdetails;
        //    }
        //    else if (checkmember.Count != 0)
        //    {
        //        var Memberdetails = dbcontext.QSWS_WEB_CONTACTS.Where(x => x.CONTACT_EMAIL == Email_id).Select(a => new RowData
        //        {
        //            FIRST_NAME = a.FIRST_NAME,
        //            LAST_NAME = a.LAST_NAME,
        //            CONTACT_EMAIL = a.CONTACT_EMAIL,
        //            USER_ID = 0,
        //            //PHONE = a.PHONE_NUMBER.ToString(),
        //            //MOBILE = a.MOBILE_NUMBER,
        //            COMPANY_NAME = a.COMPANY_NAME,
        //            NOTES = a.NOTES,
        //            STATUS = UserStatus,
        //            role = dbcontext.QSWS_USER.Any(x => x.USER_EMAIL == a.CONTACT_EMAIL) ? dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == a.CONTACT_EMAIL).Select(x => x.USER_ROLE).FirstOrDefault() : 0,
        //            ORGID = dbcontext.QSWS_USER.Any(x => x.USER_EMAIL == a.CONTACT_EMAIL) ? dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == a.CONTACT_EMAIL).Select(x => x.WHOLESALER_ID).FirstOrDefault() : 0,
        //            TYPE = 3,
        //        }).ToList();
        //        return Memberdetails;
        //    }
        //    else if (checkTWI.Count != 0)
        //    {
        //        var selectrowetails = dbcontext.QSWS_WEB_CONTACTS.Where(x => x.CONTACT_EMAIL == Email_id).Select(a => new RowData
        //        {
        //            FIRST_NAME = a.FIRST_NAME,
        //            LAST_NAME = a.LAST_NAME,
        //            CONTACT_EMAIL = a.CONTACT_EMAIL,
        //            USER_ID = 0,
        //            //PHONE = a.PHONE.ToString(),
        //            //MOBILE = a.MOBILE,
        //            COMPANY_NAME = a.COMPANY_NAME,
        //            NOTES = a.NOTES,
        //            STATUS = UserStatus,
        //            role = dbcontext.QSWS_USER.Any(x => x.USER_EMAIL == a.CONTACT_EMAIL) ? dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == a.CONTACT_EMAIL).Select(x => x.USER_ROLE).FirstOrDefault() : 0,
        //            ORGID = dbcontext.QSWS_USER.Any(x => x.USER_EMAIL == a.CONTACT_EMAIL) ? dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == a.CONTACT_EMAIL).Select(x => x.WHOLESALER_ID).FirstOrDefault() : 0,
        //            TYPE = 4,

        //        }).ToList();
        //        return selectrowetails;
        //    }
        //    else
        //    {
        //        var setFramdetails = dbcontext.QSWS_WEB_CONTACTS.Where(x => x.CONTACT_EMAIL == Email_id).Select(a => new RowData
        //        {
        //            FIRST_NAME = a.FIRST_NAME,
        //            LAST_NAME = a.LAST_NAME,
        //            CONTACT_EMAIL = a.CONTACT_EMAIL,
        //            USER_ID = 0,
        //            //PHONE = a.PHONE.ToString(),
        //            //MOBILE = a.MOBILE,
        //            COMPANY_NAME = a.COMPANY_NAME,
        //            NOTES = a.NOTES,
        //            STATUS = UserStatus,
        //            role = dbcontext.QSWS_USER.Any(x => x.USER_EMAIL == a.CONTACT_EMAIL) ? dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == a.CONTACT_EMAIL).Select(x => x.USER_ROLE).FirstOrDefault() : 0,
        //            ORGID = dbcontext.QSWS_USER.Any(x => x.USER_EMAIL == a.CONTACT_EMAIL) ? dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == a.CONTACT_EMAIL).Select(x => x.WHOLESALER_ID).FirstOrDefault() : 0,
        //            TYPE = 2,

        //        }).ToList();
        //        return setFramdetails;
        //    }




        //    //return selectrow;
        //}
        [System.Web.Http.HttpGet]
        public IList dropdownchange(string value)
        {
            if ((value == "6") || (value == "7"))
            {
                var loadFarmName = dbcontext.QSWS_FARM_PROFILE_DETAILS.Select(x => new Organisationlist
                {
                    code = x.FARM_ID.ToString(),
                    organisationName = x.FARM_NAME
                }).OrderBy(x => x.organisationName).ToList();

                return loadFarmName;
            }
            else if ((value == "4") || (value == "5"))
            {
                var loadWholesalerName = dbcontext.QSWS_WHOLESALER_PROFILE_DETAILS.Select(x => new Organisationlist
                {
                    code = x.WHOLESALER_ID.ToString(),
                    organisationName = x.WHOLESALER_NAME
                }).OrderBy(x => x.organisationName).ToList();

                return loadWholesalerName;
            }

            else if ((value == "2") || (value == "3"))
            {
                var loadWholesalerName = new List<Organisationlist>() {
                new Organisationlist(){ code = "", organisationName="Tradewinds"},

            };

                return loadWholesalerName;
            }
            return null;
        }

        [System.Web.Http.HttpGet]
        public IList dropdownRoleList(string value, string emailId)
        {
            //if (emailId != null)
            //{

            //    if (value == "2")
            //    {
            //        var loadRole = dbcontext.QSWS_USER_ROLE.Where(y => y.USER_ROLE_ID == 6 || y.USER_ROLE_ID == 7).Select(a => new { a.USER_ROLE_ID, a.ROLE_NAME }).ToList();
            //        return loadRole;
            //    }
            //    else if (value == "3")
            //    {
            //        var loadRole = dbcontext.QSWS_USER_ROLE.Where(y => y.USER_ROLE_ID == 4 || y.USER_ROLE_ID == 5).Select(a => new { a.USER_ROLE_ID, a.ROLE_NAME }).ToList();
            //        return loadRole;
            //    }
            //    else
            //    {
            //        var loadRole = dbcontext.QSWS_USER_ROLE.Where(y => y.USER_ROLE_ID == 2 || y.USER_ROLE_ID == 3).Select(a => new { a.USER_ROLE_ID, a.ROLE_NAME }).ToList();
            //        return loadRole;
            //    }

            //}
            //else
            //{

            var loadRoleList = dbcontext.QSWS_USER_ROLE.Where(a => a.USER_ROLE_ID != 1).Select(a => new { a.USER_ROLE_ID, a.ROLE_NAME }).ToList();
            return loadRoleList;

            //}
        }
        [System.Web.Http.HttpPost]
        public HttpResponseMessage saveUserWebdetails(int value, SaveuserWebDetails formdatas)
        {
            try
            {
                if (formdatas.role != "")
                {
                    var password = GetRandomPassword();
                    if ((value == 6) || (value == 7))
                    {
                        var WHOLESALER_PROFILE = dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == formdatas.Email).FirstOrDefault();
                        if (WHOLESALER_PROFILE != null)
                        {
                            dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Remove(WHOLESALER_PROFILE);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }
                        var TWI_PROFILE = dbcontext.QSWS_TWI_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == formdatas.Email).FirstOrDefault();
                        if (TWI_PROFILE != null)
                        {
                            dbcontext.QSWS_TWI_PROFILE_CONTACTS.Remove(TWI_PROFILE);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }

                        UpdateWebUserDetails(formdatas, password);
                        UpdateFarmProfileContacts(formdatas, value);
                        return Request.CreateResponse(HttpStatusCode.OK, "Success.");

                    }
                    else if ((value == 2) || (value == 3))
                    {
                        var WHOLESALER_PROFILE = dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == formdatas.Email).FirstOrDefault();
                        if (WHOLESALER_PROFILE != null)
                        {
                            dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Remove(WHOLESALER_PROFILE);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }
                        var QSWS_FARM_PROFILE_CONTACTS = dbcontext.QSWS_FARM_PROFILE_CONTACTS.Where(x => x.EMAIL_ADDRESS == formdatas.Email).FirstOrDefault();
                        if (QSWS_FARM_PROFILE_CONTACTS != null)
                        {
                            dbcontext.QSWS_FARM_PROFILE_CONTACTS.Remove(QSWS_FARM_PROFILE_CONTACTS);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }
                        UpdateWebUserDetails(formdatas, password);
                        UpdateTWIProfileContacts(formdatas, value);
                        return Request.CreateResponse(HttpStatusCode.OK, "Success.");

                    }
                    else
                    {
                        var TWI_PROFILE = dbcontext.QSWS_TWI_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == formdatas.Email).FirstOrDefault();
                        if (TWI_PROFILE != null)
                        {
                            dbcontext.QSWS_TWI_PROFILE_CONTACTS.Remove(TWI_PROFILE);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }
                        var QSWS_FARM_PROFILE_CONTACTS = dbcontext.QSWS_FARM_PROFILE_CONTACTS.Where(x => x.EMAIL_ADDRESS == formdatas.Email).FirstOrDefault();
                        if (QSWS_FARM_PROFILE_CONTACTS != null)
                        {
                            dbcontext.QSWS_FARM_PROFILE_CONTACTS.Remove(QSWS_FARM_PROFILE_CONTACTS);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }
                        UpdateWebUserDetails(formdatas, password);
                        UpdateWholesalerDetails(formdatas);
                        return Request.CreateResponse(HttpStatusCode.OK, "Success.");
                    }

                }
                else
                {
                    if ((value == 6) || (value == 7))
                    {
                        var WHOLESALER_PROFILE = dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == formdatas.Email).FirstOrDefault();
                        if (WHOLESALER_PROFILE != null)
                        {
                            dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Remove(WHOLESALER_PROFILE);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }
                        var TWI_PROFILE = dbcontext.QSWS_TWI_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == formdatas.Email).FirstOrDefault();
                        if (TWI_PROFILE != null)
                        {
                            dbcontext.QSWS_TWI_PROFILE_CONTACTS.Remove(TWI_PROFILE);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }

                        return UpdateFarmProfileContacts(formdatas, value);
                    }
                    else if ((value == 2) || (value == 3))
                    {
                        var WHOLESALER_PROFILE = dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == formdatas.Email).FirstOrDefault();
                        if (WHOLESALER_PROFILE != null)
                        {
                            dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Remove(WHOLESALER_PROFILE);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }
                        var QSWS_FARM_PROFILE_CONTACTS = dbcontext.QSWS_FARM_PROFILE_CONTACTS.Where(x => x.EMAIL_ADDRESS == formdatas.Email).FirstOrDefault();
                        if (QSWS_FARM_PROFILE_CONTACTS != null)
                        {
                            dbcontext.QSWS_FARM_PROFILE_CONTACTS.Remove(QSWS_FARM_PROFILE_CONTACTS);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }

                        return UpdateTWIProfileContacts(formdatas, value);
                    }
                    else
                    {
                        var TWI_PROFILE = dbcontext.QSWS_TWI_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == formdatas.Email).FirstOrDefault();
                        if (TWI_PROFILE != null)
                        {
                            dbcontext.QSWS_TWI_PROFILE_CONTACTS.Remove(TWI_PROFILE);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }
                        var QSWS_FARM_PROFILE_CONTACTS = dbcontext.QSWS_FARM_PROFILE_CONTACTS.Where(x => x.EMAIL_ADDRESS == formdatas.Email).FirstOrDefault();
                        if (QSWS_FARM_PROFILE_CONTACTS != null)
                        {
                            dbcontext.QSWS_FARM_PROFILE_CONTACTS.Remove(QSWS_FARM_PROFILE_CONTACTS);
                            var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                            if (QSWS_USER != null)
                            {
                                dbcontext.QSWS_USER.Remove(QSWS_USER);
                            }
                        }

                        return UpdateWholesalerDetails(formdatas);
                    }
                }

            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController : saveUserWebdetails", objException);
                return Request.CreateResponse(HttpStatusCode.OK, "Error.");
            }
        }

        private void UpdateWebUserDetails(SaveuserWebDetails formdatas, string password)
        {
            try
            {
                string Email_Subject = ConfigurationManager.AppSettings["Contact_Email_Subject"].ToString();
                string Email_Content = ConfigurationManager.AppSettings["Contact_Email_Content"].ToString();
                string Email_From = ConfigurationManager.AppSettings["SenderMail"].ToString();
                //string adminEmail = ConfigurationManager.AppSettings["Admn_Email"].ToString();
                string registryUrl = Convert.ToString(ConfigurationManager.AppSettings["Registry_URL"]);
                string EmailAttachment = ConfigurationManager.AppSettings["EmailAttachment"].ToString();
                string message_body = string.Format(Email_Content, formdatas.Email, password);
                string encrytRandomPassword = password;
                int active = 0;
                encrytRandomPassword = HttpUtility.UrlEncode(CryptoEngine.Encrypt(encrytRandomPassword, "sblw-3hn8-sqoy19"));
                var QSWS_WEB_CONTACTS = dbcontext.QSWS_WEB_CONTACTS.Where(x => x.CONTACT_EMAIL == formdatas.Email).FirstOrDefault();
                if (QSWS_WEB_CONTACTS != null)
                {
                    var objUserWEBProfileContact = dbcontext.QSWS_WEB_CONTACTS.Single(s => s.CONTACT_EMAIL == formdatas.Email);
                    objUserWEBProfileContact.CONTACT_EMAIL = formdatas.Email;
                    objUserWEBProfileContact.FIRST_NAME = formdatas.FirstName;
                    objUserWEBProfileContact.LAST_NAME = formdatas.LastName;
                    objUserWEBProfileContact.COMPANY_NAME = formdatas.Company;
                    objUserWEBProfileContact.NOTES = formdatas.Notes;
                    dbcontext.SaveChanges();

                }

                var dupcount = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email).FirstOrDefault();
                if (dupcount == null)
                {
                    if (formdatas.active == true)
                    {
                        active = 1;
                    }
                    else
                    {
                        active = 0;
                    }
                    var objattribute = new QSWS_USER
                    {

                        ENTITY_ID = 0,
                        USER_EMAIL = formdatas.Email,
                        USER_PASSWORD = encrytRandomPassword,
                        USER_FIRST_NAME = formdatas.FirstName,
                        USER_LAST_NAME = formdatas.LastName,
                        COMPANY = formdatas.Company,
                        USER_STATUS = active,
                        USER_ROLE = Convert.ToInt32(formdatas.role),
                        LOGGED_IN_FLAG = true,
                        CREATED_USER = User.Identity.Name,
                        CREATED_DATE = DateTime.Now,
                        MODIFIED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now,
                        ANNOUNCEMENT_FLAG = true,

                    };
                    dbcontext.QSWS_USER.Add(objattribute);
                    dbcontext.SaveChanges();
                    string Email_To = formdatas.Email;
                    if (formdatas.active == true)
                    {
                        NotifyThroughEmail(Email_From, Email_Subject, Email_To, message_body, EmailAttachment);
                    }

                }
                else
                {
                    if (formdatas.active == true)
                    {

                        var QSWS_USER = dbcontext.QSWS_USER.Where(x => x.USER_EMAIL == formdatas.Email && x.USER_STATUS == 1).FirstOrDefault();
                        if (QSWS_USER == null)
                        {
                            var objUserProfileContact = dbcontext.QSWS_USER.Single(s => s.USER_EMAIL == formdatas.Email);

                            objUserProfileContact.ENTITY_ID = 0;
                            objUserProfileContact.USER_EMAIL = formdatas.Email;
                            objUserProfileContact.USER_PASSWORD = encrytRandomPassword;
                            objUserProfileContact.USER_FIRST_NAME = formdatas.FirstName;
                            objUserProfileContact.USER_LAST_NAME = formdatas.LastName;
                            objUserProfileContact.COMPANY = formdatas.Company;
                            objUserProfileContact.USER_STATUS = 1;
                            objUserProfileContact.USER_ROLE = Convert.ToInt32(formdatas.role);
                            objUserProfileContact.LOGGED_IN_FLAG = true;
                            objUserProfileContact.CREATED_USER = User.Identity.Name;
                            objUserProfileContact.CREATED_DATE = DateTime.Now;
                            objUserProfileContact.MODIFIED_USER = User.Identity.Name;
                            objUserProfileContact.MODIFIED_DATE = DateTime.Now;
                            objUserProfileContact.ANNOUNCEMENT_FLAG = true;
                            dbcontext.SaveChanges();
                            NotifyThroughEmail(Email_From, Email_Subject, formdatas.Email, message_body, EmailAttachment);

                        }
                        else
                        {
                            var objUserProfileContact = dbcontext.QSWS_USER.Single(s => s.USER_EMAIL == formdatas.Email);
                            objUserProfileContact.ENTITY_ID = 0;
                            objUserProfileContact.USER_EMAIL = formdatas.Email;
                            //objUserProfileContact.USER_PASSWORD = encrytRandomPassword;
                            objUserProfileContact.USER_FIRST_NAME = formdatas.FirstName;
                            objUserProfileContact.USER_LAST_NAME = formdatas.LastName;
                            objUserProfileContact.COMPANY = formdatas.Company;
                            objUserProfileContact.USER_STATUS = 1;
                            objUserProfileContact.USER_ROLE = Convert.ToInt32(formdatas.role);
                            objUserProfileContact.LOGGED_IN_FLAG = true;
                            objUserProfileContact.CREATED_USER = User.Identity.Name;
                            objUserProfileContact.CREATED_DATE = DateTime.Now;
                            objUserProfileContact.MODIFIED_USER = User.Identity.Name;
                            objUserProfileContact.MODIFIED_DATE = DateTime.Now;
                            objUserProfileContact.ANNOUNCEMENT_FLAG = true;
                            dbcontext.SaveChanges();

                        }


                    }
                    else
                    {
                        var objUserProfileContact = dbcontext.QSWS_USER.Single(s => s.USER_EMAIL == formdatas.Email);
                        objUserProfileContact.ENTITY_ID = 0;
                        objUserProfileContact.USER_EMAIL = formdatas.Email;
                        //objUserProfileContact.USER_PASSWORD = encrytRandomPassword;
                        objUserProfileContact.USER_FIRST_NAME = formdatas.FirstName;
                        objUserProfileContact.USER_LAST_NAME = formdatas.LastName;
                        objUserProfileContact.COMPANY = formdatas.Company;
                        objUserProfileContact.USER_STATUS = 1;
                        objUserProfileContact.USER_ROLE = Convert.ToInt32(formdatas.role);
                        objUserProfileContact.LOGGED_IN_FLAG = true;
                        objUserProfileContact.CREATED_USER = User.Identity.Name;
                        objUserProfileContact.CREATED_DATE = DateTime.Now;
                        objUserProfileContact.MODIFIED_USER = User.Identity.Name;
                        objUserProfileContact.MODIFIED_DATE = DateTime.Now;
                        objUserProfileContact.ANNOUNCEMENT_FLAG = true;
                        dbcontext.SaveChanges();

                    }


                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController : UpdateWholesalerDetails", objException);

            }

        }

        public HttpResponseMessage UpdateWholesalerDetails(SaveuserWebDetails formdatas)
        {
            try
            {
                int userID = 0;
                //var farmId = dbcontext.QSWS_TWI_PROFILE_DETAILS.Where(x => x.FARM_CODE == formdatas.organisation).Select(x => x.FARM_ID).FirstOrDefault();
                var userIDChk = dbcontext.QSWS_USER.Where(a => a.USER_EMAIL == formdatas.Email).Select(a => a.USER_ID).FirstOrDefault();
                if (userIDChk != 0)
                {

                    userID = userIDChk;
                }
                else
                {
                    userID = 0;
                }
                int wholesalerID = Convert.ToInt32(formdatas.organisation);
                var wholesalerId = dbcontext.QSWS_WHOLESALER_PROFILE_DETAILS.Where(x => x.WHOLESALER_ID == wholesalerID).Select(x => x.WHOLESALER_ID).FirstOrDefault();
                var dupcount = dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == formdatas.Email).FirstOrDefault();
                if (dupcount == null)
                {

                    var objattribute = new QSWS_WHOLESALER_PROFILE_CONTACTS
                    {

                        WHOLESALER_ID = Convert.ToInt32(wholesalerId),
                        FIRST_NAME = formdatas.FirstName,
                        LAST_NAME = formdatas.LastName,
                        EMAIL_ID = formdatas.Email,
                        LOGIN_USER_ID = userID,
                        USER_ROLE = formdatas.role,
                        CREATED_USER = User.Identity.Name,
                        CREATED_DATE = DateTime.Now,
                        MODIFIED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now,
                        RECYCLE_FLAG = false

                    };
                    dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Add(objattribute);
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Saved.");
                }
                else
                {
                    var objWholesalerProfileContact = dbcontext.QSWS_WHOLESALER_PROFILE_CONTACTS.Single(s => s.EMAIL_ID == formdatas.Email);
                    objWholesalerProfileContact.WHOLESALER_ID = Convert.ToInt32(wholesalerId);
                    objWholesalerProfileContact.FIRST_NAME = formdatas.FirstName;
                    objWholesalerProfileContact.LAST_NAME = formdatas.LastName;
                    objWholesalerProfileContact.EMAIL_ID = formdatas.Email;
                    objWholesalerProfileContact.LOGIN_USER_ID = userID;
                    objWholesalerProfileContact.USER_ROLE = formdatas.role;
                    objWholesalerProfileContact.CREATED_USER = User.Identity.Name;
                    objWholesalerProfileContact.CREATED_DATE = DateTime.Now;
                    objWholesalerProfileContact.MODIFIED_USER = User.Identity.Name;
                    objWholesalerProfileContact.MODIFIED_DATE = DateTime.Now;
                    objWholesalerProfileContact.RECYCLE_FLAG = false;
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Update.");

                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController : UpdateWholesalerDetails", objException);
                return Request.CreateResponse(HttpStatusCode.OK, "Error.");
            }

        }

        public HttpResponseMessage UpdateFarmProfileContacts(SaveuserWebDetails formdatas, int value)
        {
            try
            {
                int userID = 0;
                int FarmData = 0;
                if (formdatas.organisation != null)
                {
                    FarmData = Convert.ToInt32(formdatas.organisation);
                }
                var userIDChk = dbcontext.QSWS_USER.Where(a => a.USER_EMAIL == formdatas.Email).Select(a => a.USER_ID).FirstOrDefault();
                if (userIDChk != 0)
                {

                    userID = userIDChk;
                }
                else
                {
                    userID = 0;
                }
                var farmId = dbcontext.QSWS_FARM_PROFILE_DETAILS.Where(x => x.FARM_ID == FarmData).Select(x => x.FARM_ID).FirstOrDefault();
                var dupcount = dbcontext.QSWS_FARM_PROFILE_CONTACTS.Where(x => x.EMAIL_ADDRESS == formdatas.Email).FirstOrDefault();
                if (dupcount == null)
                {

                    var objattribute = new QSWS_FARM_PROFILE_CONTACTS
                    {

                        FARM_ID = Convert.ToInt32(farmId),
                        FARM_FIRST_NAME = formdatas.FirstName,
                        FARM_LAST_NAME = formdatas.LastName,
                        EMAIL_ADDRESS = formdatas.Email,
                        USER_ID = userID,
                        USER_ROLE = Convert.ToInt32(formdatas.role),
                        CREATED_USER = User.Identity.Name,
                        CREATED_DATE = DateTime.Now,
                        MODIFIED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now,
                        FLAG_RECYCLE = false
                    };
                    dbcontext.QSWS_FARM_PROFILE_CONTACTS.Add(objattribute);
                    dbcontext.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK, "Saved.");
                }
                else
                {
                    var objfarmprofilecontact = dbcontext.QSWS_FARM_PROFILE_CONTACTS.Single(s => s.EMAIL_ADDRESS == formdatas.Email);
                    objfarmprofilecontact.FARM_ID = Convert.ToInt32(farmId);
                    objfarmprofilecontact.FARM_FIRST_NAME = formdatas.FirstName;
                    objfarmprofilecontact.FARM_LAST_NAME = formdatas.LastName;
                    objfarmprofilecontact.EMAIL_ADDRESS = formdatas.Email;
                    objfarmprofilecontact.USER_ID = userID;
                    objfarmprofilecontact.USER_ROLE = Convert.ToInt32(formdatas.role);
                    objfarmprofilecontact.CREATED_USER = User.Identity.Name;
                    objfarmprofilecontact.CREATED_DATE = DateTime.Now;
                    objfarmprofilecontact.MODIFIED_USER = User.Identity.Name;
                    objfarmprofilecontact.MODIFIED_DATE = DateTime.Now;
                    objfarmprofilecontact.FLAG_RECYCLE = false;
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Update.");

                }
            }

            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController : UpdateFarmProfileContacts", objException);
                return Request.CreateResponse(HttpStatusCode.OK, "Error.");
            }

        }

        public HttpResponseMessage UpdateTWIProfileContacts(SaveuserWebDetails formdatas, int value)
        {
            try
            {
                int userID = 0;
                //var farmId = dbcontext.QSWS_TWI_PROFILE_DETAILS.Where(x => x.FARM_CODE == formdatas.organisation).Select(x => x.FARM_ID).FirstOrDefault();
                var userIDChk = dbcontext.QSWS_USER.Where(a => a.USER_EMAIL == formdatas.Email).Select(a => a.USER_ID).FirstOrDefault();
                if (userIDChk != 0)
                {

                    userID = userIDChk;
                }
                else
                {
                    userID = 0;
                }
                var dupcount = dbcontext.QSWS_TWI_PROFILE_CONTACTS.Where(x => x.EMAIL_ID == formdatas.Email).FirstOrDefault();
                if (dupcount == null)
                {

                    var objattribute = new QSWS_TWI_PROFILE_CONTACTS
                    {

                        TWI_ID = 5,
                        FIRST_NAME = formdatas.FirstName,
                        LAST_NAME = formdatas.LastName,
                        EMAIL_ID = formdatas.Email,
                        USER_ID = userID,
                        USER_ROLE = formdatas.role,
                        //CREATED_USER = formdatas.FirstName,
                        CREATED_DATE = DateTime.Now,
                        // MODIFIED_USER = formdatas.FirstName,
                        MODIFIED_DATE = DateTime.Now,
                        RECYCLE_FLAG = false

                    };
                    dbcontext.QSWS_TWI_PROFILE_CONTACTS.Add(objattribute);
                    dbcontext.SaveChanges();

                    return Request.CreateResponse(HttpStatusCode.OK, "Saved.");
                }
                else
                {
                    var objfarmprofilecontact = dbcontext.QSWS_TWI_PROFILE_CONTACTS.Single(s => s.EMAIL_ID == formdatas.Email);
                    objfarmprofilecontact.TWI_ID = 5;
                    objfarmprofilecontact.FIRST_NAME = formdatas.FirstName;
                    objfarmprofilecontact.LAST_NAME = formdatas.LastName;
                    objfarmprofilecontact.EMAIL_ID = formdatas.Email;
                    objfarmprofilecontact.USER_ID = userID;
                    objfarmprofilecontact.USER_ROLE = formdatas.role;
                    //objfarmprofilecontact.CREATED_USER = formdatas.FirstName;
                    objfarmprofilecontact.CREATED_DATE = DateTime.Now;
                    //objfarmprofilecontact.MODIFIED_USER = formdatas.FirstName;
                    objfarmprofilecontact.MODIFIED_DATE = DateTime.Now;
                    objfarmprofilecontact.RECYCLE_FLAG = false;
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Update.");

                }
            }

            catch (Exception objException)
            {
                _logger.Error("Error at UserAdminApiController : UpdateFarmProfileContacts", objException);
                return Request.CreateResponse(HttpStatusCode.OK, "Error.");
            }

        }



        public string GetRandomPassword()
        {
            try
            {
                int randomPasswordLength = Int32.Parse(ConfigurationManager.AppSettings["Random_Password_Length"].ToString());
                string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
                Random randNum = new Random();
                char[] chars = new char[randomPasswordLength];
                int allowedCharCount = _allowedChars.Length;
                for (int i = 0; i < randomPasswordLength; i++)
                {
                    chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
                }
                string randomPassword = new string(chars);


                return randomPassword;
            }

            catch (Exception ex)
            {
                _logger.Error("Error at UserAdminApiController : RandomPasswordGeneration", ex);
                return null;
            }
        }

        public string NotifyThroughEmail(string Email_From, string Email_Subject, string Email_To, string Message_Content, string Email_Attachments)
        {
            try
            {

                string emailAttachment = ConfigurationManager.AppSettings["Contact_Email_Attachment"].ToString();
                string SMTP_Server = ConfigurationManager.AppSettings["SmtpServer"].ToString();
                string SMTP_MailId = ConfigurationManager.AppSettings["SenderMail"].ToString();
                string SMTP_Password = ConfigurationManager.AppSettings["Password"].ToString();
                string BCC_Emails = ConfigurationManager.AppSettings["BCC_Emails"].ToString();

                int SMTP_Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTP_Port"]);
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(SMTP_Server);
                SmtpServer.UseDefaultCredentials = false;
                mail.From = new MailAddress(Email_From);
                mail.To.Add(Email_To);
                foreach (var address in BCC_Emails.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.Bcc.Add(address);
                }
                mail.Subject = Email_Subject;


                //
                mail.Body = string.Format("<div style='font-family:Century Gothic;'>{0} </br></div> <div><img width='30%' src='{1}'></div>", Message_Content, emailAttachment);

                SmtpServer.Credentials = new System.Net.NetworkCredential(SMTP_MailId, SMTP_Password);
                SmtpServer.Port = SMTP_Port;

                SmtpServer.EnableSsl = true;

                mail.IsBodyHtml = true;
                SmtpServer.Send(mail);

                return "success";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at UserAdminApiController : EmailNotification", ex);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public IList getUserNameBasedonGroupName(int roleId, int CustomerId)
        {
            try
            {


                var getUserName = dbcontext.GET_USER_NAME_BASED_ON_GROUP(roleId, CustomerId).ToList();
                return getUserName.ToList();

            }
            catch (Exception ex)
            {
                _logger.Error("Error at UserAdminApiController : getUserNameBasedonGroupName", ex);
                return null;
            }
        }
        public partial class getUser
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public bool flag { get; set; }
        }

        [System.Web.Http.HttpGet]
        public IList getUsernameListExceptSelected(int roleId, int customerid)
        {
            try
            {
                string userName = User.Identity.Name;
                var getUserNameforAdd = dbcontext.STP_GET_USER_LIST_FOR_ADDING_USER_TO_GROUP(roleId, customerid, userName).ToList();
                return getUserNameforAdd.ToList();


            }
            catch (Exception ex)
            {
                _logger.Error("Error at UserAdminApiController : getGroupnameList", ex);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="CustomerId"></param>
        /// <param name="updateDatas"></param>
        /// <returns></returns>
        /// 
        ///
        public class GroupListItem
        {
            public Guid RoleId { get; set; }
        }
        public HttpResponseMessage updateGroupName(int RoleId, int CustomerId, JArray updateDatas)
        {

            var getDatas = ((JArray)updateDatas).Select(x => new UpdateGroupNameForUsers()
            {
                FirstName = (string)x["FirstName"],
                LastName = (string)x["LastName"],
                Email = (string)x["Email"],
            }).ToList();
            foreach (var a in getDatas)
            {
                var GuiRoleId = dbcontext.aspnet_Roles.Where(k => k.Role_id == RoleId && k.CustomerId == CustomerId).Select(b => b.RoleId).FirstOrDefault();
                var userId = dbcontext.aspnet_Membership.Where(s => s.Email == a.Email).Select(x => x.UserId).SingleOrDefault();
                var adminUser = User.Identity.Name;
                var adminuserId = dbcontext.aspnet_Membership.Where(f => f.Email == adminUser).Select(g => g.UserId).FirstOrDefault();
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    if (userId != adminuserId)
                    {
                        string insertQuery = @"INSERT INTO aspnet_UsersInRoles ([UserId],[RoleId]) values (@userId,@GuiRoleId)";
                        var getResult = db.Execute(insertQuery, new
                        {
                            userId,
                            GuiRoleId

                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Admin Group Users cannot modified.");
                    }
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, "Update.");
        }
        [System.Web.Http.HttpGet]
        public HttpResponseMessage deleteUser(string EmailID)
        {
            //var userId = dbcontext.aspnet_Membership.Where(s => s.Email == EmailID).Select(x => x.UserId).SingleOrDefault();
            var adminUser = User.Identity.Name;
            var UserIdForOrg = dbcontext.aspnet_Membership.Where(a => a.Email == EmailID).Select(b => b.UserId).SingleOrDefault();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                if (EmailID != adminUser)
                {
                    string deleteQuery = @"DELETE from [dbo].[aspnet_Membership]  WHERE Email=@EmailID";
                    var result = db.Execute(deleteQuery, new
                    {
                        EmailID

                    });
                    string deleteQuery2 = @"DELETE from [dbo].[Customer_User]  WHERE Email=@EmailID";
                    var result2 = db.Execute(deleteQuery2, new
                    {
                        EmailID

                    });
                    var orguserId = dbcontext.aspnet_Membership.Where(a => a.Email == EmailID).Select(b => b.UserId).SingleOrDefault();
                    string deleteQuery3 = @"DELETE from TB_ORGANIZATION_USER  WHERE userId=@orguserId";
                    var result3 = db.Execute(deleteQuery3, new
                    {
                        orguserId

                    });


                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Admin User Cannot Be Deleted");
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, "Update.");

        }
        [System.Web.Http.HttpGet]
        public HttpResponseMessage changeStatus(string EmailID)
        {
            var adminUser = User.Identity.Name;
            if (adminUser != EmailID)
            {
                var status = dbcontext.aspnet_Users.Where(a => a.UserName == EmailID).Select(b => b.IsAnonymous).FirstOrDefault();
                if (status == false)
                {
                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        string setToInActive = @"Update  [dbo].[aspnet_Users]set IsAnonymous=1 where UserName=@EmailID";
                        var result = db.Execute(setToInActive, new
                        {
                            EmailID

                        });
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, "InActive");

                }
                else
                {
                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        string setToActive = @"Update  [dbo].[aspnet_Users]set IsAnonymous=0 where UserName=@EmailID";
                        var result = db.Execute(setToActive, new
                        {
                            EmailID

                        });
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, "Active.");
            }
            return Request.CreateResponse(HttpStatusCode.OK, "Admin status cannot be changed");
        }
        [System.Web.Http.HttpGet]
        public HttpResponseMessage saveOrganisation(string organisationName)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                string checkDuplicate = @"SELECT  * FROM TB_ORGANIZATION WHERE ORGANIZATION_NAME=@organisationName";
                var result = db.Query(checkDuplicate, new
                {
                    organisationName

                }).ToList();
                if (result.Count == 0)
                {
                    string addNewOrganisation = @"INSERT INTO TB_ORGANIZATION([ORGANIZATION_NAME]) VALUES(@organisationName)";
                    var getResult = db.Execute(addNewOrganisation, new
                    {
                        organisationName

                    });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Duplicate");
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, "Inserted");
        }

        [System.Web.Http.HttpGet]
        public IList loadOrganizationName()
        {
            var loadOrganizationName = dbcontext.TB_ORGANIZATION.Select(x => new OrganizationName
            {
                code = x.ORGANIZATION_ID.ToString(),
                organisationName = x.ORGANIZATION_NAME
            }).OrderBy(x => x.organisationName).ToList();

            return loadOrganizationName;
        }
        [System.Web.Http.HttpGet]
        public String getUserNameForCheck()
        {
            string userName = User.Identity.Name;
            return userName.ToString();
        }
        [System.Web.Http.HttpGet]
        public String getOrganisationName(int organizationId)
        {
            var getOrganizationName = dbcontext.TB_ORGANIZATION.Where(s => s.ORGANIZATION_ID == organizationId).Select(f => f.ORGANIZATION_NAME).SingleOrDefault();
            return getOrganizationName;
        }
        [System.Web.Http.HttpGet]
        public HttpResponseMessage updateOrganisationName(String organizationName, String organizationNameEdit)
        {
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                string updateOrganisationName = @"update TB_ORGANIZATION SET ORGANIZATION_NAME=@organizationName WHERE ORGANIZATION_NAME=@organizationNameEdit ";
                var getResult = db.Execute(updateOrganisationName, new
                {
                    organizationName,
                    organizationNameEdit

                });

            }
            return Request.CreateResponse(HttpStatusCode.OK, "Updated");
        }

        [System.Web.Http.HttpGet]
        public string getGroupName(int GroupID)
        {
            try
            {
                var customerName = User.Identity.Name;
                var customerId = dbcontext.Customer_User.Where(d => d.User_Name == customerName).Select(s => s.CustomerId).FirstOrDefault();
                var getRoleName = dbcontext.aspnet_Roles.Where(s => s.Role_id == GroupID && s.CustomerId == customerId).Select(f => f.RoleName).FirstOrDefault();
                string[] role_Names = Convert.ToString(getRoleName).Split('_');
                string getroleNames = role_Names[1].ToString();
                return getroleNames;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at UserAdminApiController : getGroupName", ex);
                return null;
            }


        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage checkIforganizationIsassociated(int OrganizationId)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    string checkDuplicate = @"SELECT  * FROM TB_ORGANIZATION_USER WHERE ORGANIZATION_ID=@OrganizationId";
                    var result = db.Query(checkDuplicate, new
                    {
                        OrganizationId

                    }).ToList();
                    if (result.Count == 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Not Associated");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Associated");
                    }

                }

            }

            catch (Exception ex)
            {
                _logger.Error("Error at UserAdminApiController : checkIforganizationIsassociated", ex);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage deleteOrganization(int OrganizationId, bool checkUserAssociated)
        {
            try
            {
                if (checkUserAssociated == false)
                {
                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        string deleteOrganizationWithNoUser = @"DELETE FROM TB_ORGANIZATION WHERE ORGANIZATION_ID=@OrganizationId";
                        var result = db.Execute(deleteOrganizationWithNoUser, new
                        {
                            OrganizationId

                        });

                    }
                    return Request.CreateResponse(HttpStatusCode.OK, "Deleted");
                }
                else
                {
                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        string deleteFromOrganization = @"DELETE FROM TB_ORGANIZATION WHERE ORGANIZATION_ID=@OrganizationId";
                        var result = db.Execute(deleteFromOrganization, new
                        {
                            OrganizationId

                        });

                        string updateOrganizationUser = @"UPDATE TB_ORGANIZATION_USER SET ORGANIZATION_ID=(select  top 1 Organization_Id from TB_ORGANIZATION_DEFAULT) WHERE ORGANIZATION_ID=@OrganizationId";
                        var result1 = db.Execute(updateOrganizationUser, new
                        {
                            OrganizationId

                        });
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, "Group Deleted Users Moved to Default Organization");
                }

            }
            catch (Exception ex)
            {

                _logger.Error("Error at UserAdminApiController : deleteOrganization", ex);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public int getOrganizationID(string OrganizationName)
        {
            try
            {
                var OrgId = dbcontext.TB_ORGANIZATION.Where(d => d.ORGANIZATION_NAME == OrganizationName).Select(s => s.ORGANIZATION_ID).FirstOrDefault();
                return OrgId;
            }
            catch (Exception ex)
            {

                _logger.Error("Error at UserAdminApiController : getOrganizationID", ex);
                return 0;
            }
        }
        [System.Web.Http.HttpGet]
        public int checkifUserExistInTheSelectedGroup(string userRoleId)
        {
            try
            {
                string AdminUser = User.Identity.Name;
                int Role_Id = int.Parse(userRoleId);
                var guidRoleId = dbcontext.aspnet_Roles.Where(d => d.Role_id == Role_Id).Select(s => s.RoleId).FirstOrDefault();
                var checkifexist = dbcontext.vw_aspnet_UsersInRoles.Where(w => w.RoleId == guidRoleId).Select(x => x.UserId).Count();
                return checkifexist;
            }
            catch (Exception ex)
            {

                _logger.Error("Error at UserAdminApiController : checkifUserExistInTheSelectedGroup", ex);
                return 0;
            }
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage deleteGroups(string userRoleId, bool userDelete)
        {
            int Role_Id = int.Parse(userRoleId);
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    string deleteGroup = @"Delete from aspnet_Roles where Role_id=@Role_Id";
                    var deleteResult = db.Execute(deleteGroup, new
                    {
                        Role_Id

                    });
                }
                return Request.CreateResponse(HttpStatusCode.OK, "Group Deleted!");
            }
            catch (Exception ex)
            {

                _logger.Error("Error at UserAdminApiController : deleteGroups", ex);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public HttpResponseMessage removeGroupsfromUser(string userRoleId)
        {
            int Role_Id = int.Parse(userRoleId);
            try
            {
                var guidRoleId = dbcontext.aspnet_Roles.Where(h => h.Role_id == Role_Id).Select(w => w.RoleId).FirstOrDefault();
                var user_id = dbcontext.vw_aspnet_UsersInRoles.Where(j => j.RoleId == guidRoleId).Select(u => u.UserId).FirstOrDefault();
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    string removeGroup = @"delete from aspnet_UsersInRoles where UserId=@user_id and RoleId=@guidRoleId";
                    var removeUserFromGroups = db.Execute(removeGroup, new
                    {
                        user_id,
                        guidRoleId
                    });
                    return Request.CreateResponse(HttpStatusCode.OK, "Group Removed!");
                }


            }
            catch (Exception ex)
            {

                _logger.Error("Error at UserAdminApiController : removeGroupsfromUser", ex);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public HttpResponseMessage uncheckUserfromGroup(string Roleid, string removeData)
        {

            int Role_Id = int.Parse(Roleid);
            var GuiRoleId = dbcontext.aspnet_Roles.Where(w => w.Role_id == Role_Id).Select(o => o.RoleId).FirstOrDefault();

            var userId = dbcontext.aspnet_Membership.Where(s => s.Email == removeData).Select(x => x.UserId).SingleOrDefault();
            var adminUser = User.Identity.Name;
            var adminuserId = dbcontext.aspnet_Membership.Where(f => f.Email == adminUser).Select(g => g.UserId).FirstOrDefault();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                if (userId != adminuserId)
                {
                    string removeQuery = @"delete from aspnet_UsersInRoles where UserId=@userId and RoleId=@GuiRoleId";
                    var getResult = db.Execute(removeQuery, new
                    {
                        userId,
                        GuiRoleId

                    });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Admin Group Users cannot modified.");
                }
            }


            return Request.CreateResponse(HttpStatusCode.OK, "Deleted.");

        }


        [System.Web.Http.HttpGet]
        public IList getGroupNames(int CustomerId)
        {
            try
            {

                var loadGroupName = dbcontext.aspnet_Roles.Where(q => q.CustomerId == CustomerId).Select(x => new GroupNameload
                {
                    code = x.Role_id.ToString(),
                    roleName = x.RoleName
                }).OrderBy(x => x.roleName).ToList();

                var arrayTemp = loadGroupName.ToArray();
                for (int i = 0; i < arrayTemp.Length; i++)
                {
                    string[] array = arrayTemp[i].roleName.Split('_');
                    if (array.Length > 1)
                        arrayTemp[i].roleName = array[1];
                }

                loadGroupName = arrayTemp.ToList();



                return loadGroupName;
            }
            catch (Exception ex)
            {

                _logger.Error("Error at UserAdminApiController : getGroupNames", ex);
                return null;
            }

        }

        [System.Web.Http.HttpGet]
        public IList getRoleDetails(int CustomerId, string GroupNames)
        {
            string Full_roleName;
            try
            {

                var datas = new HashSet<GroupNameload>();
                string[] role_Names = Convert.ToString(GroupNames).Split(',');
                var customer_name = dbcontext.Customers.Where(w => w.CustomerId == CustomerId).Select(d => d.CompanyName).FirstOrDefault();
                foreach (var a in role_Names)
                {
                    Full_roleName = customer_name + "_" + a.Trim();
                    var loadSelectedGroupName = dbcontext.aspnet_Roles.Where(q => q.RoleName == Full_roleName).Select(x => new GroupNameload
                    {
                        code = x.Role_id.ToString(),
                        roleName = x.RoleName
                    }).OrderBy(x => x.roleName).ToList();
                    foreach (var c in loadSelectedGroupName)
                    {
                        datas.Add(new GroupNameload() { code = c.code, roleName = c.roleName });
                    }

                }
                var arrayTemp = datas.ToArray();
                for (int i = 0; i < arrayTemp.Length; i++)
                {
                    string[] array = arrayTemp[i].roleName.Split('_');
                    if (array.Length > 1)
                        arrayTemp[i].roleName = array[1];
                }

                //loadGroupName = arrayTemp.ToList();
                return arrayTemp.ToList();


            }
            catch (Exception ex)
            {
                _logger.Error("Error at UserAdminApiController : getRoleDetails", ex);
                return null;
            }

        }
        [System.Web.Http.HttpGet]
        public string editGroupName(string roleid)
        {
            try
            {
                int role_id = int.Parse(roleid);
                var getGroupName = dbcontext.aspnet_Roles.Where(e => e.Role_id == role_id).Select(w => w.RoleName).FirstOrDefault();
                string[] stringParts = getGroupName.Split('_');
                string removeCompanyName = "";
                if (stringParts.Length > 1)
                    removeCompanyName = stringParts[1];

                return removeCompanyName;
            }
            catch (Exception ex)
            {

                _logger.Error("Error at UserAdminApiController : editGroupName", ex);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public HttpResponseMessage saveEditName(string EditedGroupName, string ExisitingGroupName, int CustomerId)
        {
            try
            {
                var customer_name = dbcontext.Customers.Where(w => w.CustomerId == CustomerId).Select(d => d.CompanyName).FirstOrDefault();
                string changedGroupName = customer_name + "_" + EditedGroupName.Trim();
                string oldGroupName = customer_name + "_" + ExisitingGroupName.Trim();
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {

                    string updateGroupName = @"UPDATE aspnet_Roles SET RoleName=@changedGroupName WHERE RoleName=@oldGroupName";
                    var result1 = db.Execute(updateGroupName, new
                    {
                        changedGroupName,
                        oldGroupName

                    });
                }
                return Request.CreateResponse(HttpStatusCode.OK, "Updated.");

            }
            catch (Exception ex)
            {

                _logger.Error("Error at UserAdminApiController : saveEditName", ex);
                return null;
            }
        }




    }
}
