﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using log4net;
using System.Web.Http;
using System.Configuration;
using System.Data;
using Newtonsoft.Json;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Data.OleDb;
using LS.Web.Models;
using System.Web;
using LS.Data.Model.CatalogSectionModels;
using System.Web.Mvc;
using System.Diagnostics;
using LS.Data;

namespace LS.Web.Controllers
{
    public class PickListImportApiController : ApiController
    {
        private static ILog _logger = LogManager.GetLogger(typeof(PickListImportApiController));
        string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        private readonly CSEntities _dbcontext = new CSEntities();


        [System.Web.Http.HttpPost]
        public string validatePickListImport(string importExcelSheetDDSelectionValue, string excelPath)
        {
            try
            {

                string validateResults = string.Empty;
                QueryValues queryValues = new QueryValues();
                string importTemp, SQLString = string.Empty;
                importTemp = Guid.NewGuid().ToString();
                string validatePickListValuesMissing = "";
                string validateMissingColumns = "";
                string validateMissMatchedValues = "";
                string validateReferencesExists = "";
                string validateDataType = "";


                int rowcount = 0;
                int missingDataRowCount = 0;
                int missingData = 1;
                DataTable excelDatas = ReadExcelFile(importExcelSheetDDSelectionValue, excelPath);

                DataTable excelData = excelDatas.Clone();
                for (int j = 0; j < excelDatas.Columns.Count; j++)
                {
                    if (excelData.Columns[j].DataType != typeof(string))
                        excelData.Columns[j].DataType = typeof(string);
                }

                foreach (DataRow dr in excelDatas.Rows)
                {
                    excelData.ImportRow(dr);
                }

                if (!excelData.Columns.Contains("PICKLIST_ID"))
                {
                    excelData.Columns.Add("PICKLIST_ID", typeof(string));
                }
                if (!excelData.Columns.Contains("PICKLIST_ITEM_ID"))
                {
                    excelData.Columns.Add("PICKLIST_ITEM_ID", typeof(string));
                }


                DataTable importDataTable = excelData.Clone();
                DataTable filterMandatoryColumns = excelData.Copy();

                importDataTable.Columns.Add("VALIDATION TYPE", typeof(System.String));
                importDataTable.Columns.Add("MESSAGE", typeof(System.String));
                DataRow dataRow;
                int missingDataCount = 0;
                //  dataRow = importDataTable.NewRow();
                int count = 0;
                string[] mandatoryColumnNames = new string[] { "PICKLIST_NAME", "PICKLIST_DATA_TYPE", "PICKLIST_ITEM_VALUE" };
                foreach (var item in mandatoryColumnNames)
                {
                    DataColumnCollection columns = excelData.Columns;
                    if (!columns.Contains(item))
                    {
                        if (importDataTable.Rows.Count == 0)
                        {
                            var rows = excelData.AsEnumerable().Take(1).ToList();
                            importDataTable.Rows.Add(rows[0].ItemArray);
                        }


                        foreach (DataRow datarows in importDataTable.Rows)
                        {

                            string columnNamemissing = item;
                            validateMissingColumns = "1";



                            if (count == 0)
                            {
                                importDataTable.Rows[missingDataCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                                importDataTable.Rows[missingDataCount]["MESSAGE"] = columnNamemissing + " Column name Missing/Invalid.";

                            }
                            else
                            {
                                importDataTable.Rows[missingDataCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                                importDataTable.Rows[missingDataCount]["MESSAGE"] = importDataTable.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + columnNamemissing + "  Column name Missing/Invalid.";
                                // missingDataCount++;
                            }
                            missingDataCount++;
                            count++;
                        }

                    }
                }
                if (validateMissingColumns != "1")
                {
                    if (validatePickListValuesMissing == string.Empty)
                    {

                        foreach (DataRow rows in excelData.Rows)
                        {


                            string pickListName = rows["PICKLIST_NAME"].ToString();
                            // string pickListId = rows["PICKLIST_ID"].ToString();
                            string pickListItemValue = rows["PICKLIST_ITEM_VALUE"].ToString();

                            if (pickListName == string.Empty)
                            {
                                if (importDataTable.Rows.Count == 0)
                                {
                                    missingData = 0;
                                }
                                validatePickListValuesMissing = "2";
                                if (missingDataCount == 0)
                                {
                                    importDataTable.Rows.Add(rows.ItemArray);
                                    //importDataTable = importDataTable.AsEnumerable().Where(a => a.Field<string>("PICKLIST_NAME") == null).CopyToDataTable();
                                    //importDataTable.Rows.Add(result);
                                    //importDataTable.Rows.Add([1].ItemArray);
                                    //importDataTable.Rows.Add(rows.result);
                                    //  dataRow.Table.Rows.Add(importDataTable);
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = "Invalid PickList Name.";

                                    importDataTable.Rows[missingData]["MESSAGE"] = "PickList name should not be blank.";
                                    // importDataTable.Rows.Add(dataRow);


                                }
                                else
                                {
                                    importDataTable.Rows.Add(rows.ItemArray);
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = "Invalid PickList Name.";
                                    importDataTable.Rows[missingData]["MESSAGE"] = "PickList name should not be blank.";

                                }
                                //missingDataCount++;
                                // missingData++;
                                rowcount++;

                            }

                            if (pickListItemValue == string.Empty)
                            {
                                if (importDataTable.Rows.Count == 0)
                                {
                                    missingData = 0;
                                }
                                //if (missingData == 1)
                                //{

                                //}
                                validatePickListValuesMissing = "2";
                                if (missingDataCount == 0 && rowcount == 0)
                                {
                                    importDataTable.Rows.Add(rows.ItemArray);
                                    //importDataTable = importDataTable.AsEnumerable().Where(a => a.Field<string>("PICKLIST_NAME") == null).CopyToDataTable();
                                    //importDataTable.Rows.Add(result);
                                    //importDataTable.Rows.Add([1].ItemArray);
                                    //importDataTable.Rows.Add(rows.result);
                                    //  dataRow.Table.Rows.Add(importDataTable);
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = "Invalid PickList Item Value.";
                                    importDataTable.Rows[missingData]["MESSAGE"] = "PickList Item Value should not be blank.";
                                    // importDataTable.Rows.Add(dataRow);


                                }
                                else
                                {
                                    if (missingDataCount != 0)
                                    {
                                        importDataTable.Rows.Add(rows.ItemArray);
                                    }
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = importDataTable.Rows[missingData]["VALIDATION TYPE"].ToString() + "\n" + "Invalid PickList Item Value.";
                                    importDataTable.Rows[missingData]["MESSAGE"] = importDataTable.Rows[missingData]["MESSAGE"].ToString() + "\n" + "PickList Item Value should not be blank.";
                                }
                                rowcount++;


                            }
                            //missingDataCount++;
                            missingData++;
                        }
                    }


                    /*-------------------------------------PICKLIST ID VALIDATION--------------------------------------*/
                    //      var rowsPickListId = _dbcontext.TB_PICKLIST.Select(s => new { s.ID }).OrderBy(x => x.ID).ToList();
                    //           DataTable dt_rowsPickListId = new DataTable();

                    //           dt_rowsPickListId.Columns.Add("ID", typeof(Int32));
                    //           foreach (var misMatchValues in rowsPickListId)
                    //           {
                    //               dt_rowsPickListId.Rows.Add(misMatchValues.ID);

                    //           }
                    //           DataTable rowsPickListIdValues = null;

                    //   var rowsPickListIds = filterMandatoryColumns.Rows.OfType<DataRow>().Where(a => filterMandatoryColumns.Rows.OfType<DataRow>().
                    //Select(k => Convert.ToInt32(k["PICKLIST_ID"])).
                    //Except(dt_rowsPickListId.Rows.OfType<DataRow>().
                    //Select(k => Convert.ToInt32(k["ID"]))).Contains(Convert.ToInt32(a["PICKLIST_ID"])));



                    //   if (rowsPickListIds.Any())
                    //           {
                    //               rowsPickListIdValues = rowsPickListIds.CopyToDataTable();
                    //           }


                   // DataTable dt_PickListImportTable1 = new DataTable();
                    string importTemp1;
                    importTemp1 = Guid.NewGuid().ToString();
                    string sqlString = string.Empty;
                  //  dt_PickListImportTable1 = ReadExcelFile(importExcelSheetDDSelectionValue, excelPath);

                    using (var objSqlConnection = new SqlConnection(connectionString))
                    {


                        objSqlConnection.Open();


                        sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp1 + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp1 + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                        _DBCommand.ExecuteNonQuery();
                        sqlString = CreateTable("[##IMPORTTEMP" + importTemp1 + "]", filterMandatoryColumns);

                        SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                        dbCommandnew.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(objSqlConnection)
                        {
                            DestinationTableName = "[##IMPORTTEMP" + importTemp1 + "]"
                        };
                        bulkCopy.WriteToServer(filterMandatoryColumns);


                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_LS_IMPORT_PICKLIST";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.CommandTimeout = 0;
                        //objSqlCommand.Parameters.Add("@PICKLIST_NAME", SqlDbType.VarChar, 100).Value = pickList;
                        //objSqlCommand.Parameters.Add("@PICKLIST_ITEM_VALUE", SqlDbType.VarChar, 100).Value = itemValues;
                        //objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "import";

                        //objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;

                        //objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar).Value = User.Identity.Name;

                        objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = importTemp1;
                        objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "Validation For PickList Id";
                        SqlDataAdapter sde = new SqlDataAdapter(objSqlCommand);

                        DataTable rowsPickListIdValues = new DataTable();
                        sde.Fill(rowsPickListIdValues);

                        if (rowsPickListIdValues != null)
                        {
                            foreach (DataRow _misMatchedIdValues in rowsPickListIdValues.Rows)
                            {
                                if (importDataTable.Rows.Count == 0)
                                {
                                    missingData = 0;
                                }
                                validateMissMatchedValues = "3";
                                if (missingDataCount == 0)
                                {
                                    importDataTable.Rows.Add(_misMatchedIdValues.ItemArray);
                                    //importDataTable = importDataTable.AsEnumerable().Where(a => a.Field<string>("PICKLIST_NAME") == null).CopyToDataTable();
                                    //importDataTable.Rows.Add(result);
                                    //importDataTable.Rows.Add([1].ItemArray);
                                    //importDataTable.Rows.Add(rows.result);
                                    //  dataRow.Table.Rows.Add(importDataTable);
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = "Missmatched Picklist Id.";
                                    importDataTable.Rows[missingData]["MESSAGE"] = "PickList Id should be matched with tables Value.";
                                    // importDataTable.Rows.Add(dataRow);
                                }
                                else
                                {
                                    importDataTable.Rows.Add(_misMatchedIdValues.ItemArray);
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = "\n" + "Missmatched Picklist Id.";
                                    importDataTable.Rows[missingData]["MESSAGE"] = importDataTable.Rows[missingData]["MESSAGE"].ToString() + "\n" + "PickList Id should be matched with tables Value.";
                                }
                                //   missingDataCount++;
                                missingData++;
                            }
                        }
                        else if (validatePickListValuesMissing == "2")
                        {
                            validateMissMatchedValues = "3";
                        }

                        SqlCommand _objSqlCommand = objSqlConnection.CreateCommand();
                        _objSqlCommand.CommandText = "STP_LS_IMPORT_PICKLIST";
                        _objSqlCommand.CommandType = CommandType.StoredProcedure;
                        _objSqlCommand.Connection = objSqlConnection;
                        _objSqlCommand.CommandTimeout = 0;
                        //objSqlCommand.Parameters.Add("@PICKLIST_NAME", SqlDbType.VarChar, 100).Value = pickList;
                        //objSqlCommand.Parameters.Add("@PICKLIST_ITEM_VALUE", SqlDbType.VarChar, 100).Value = itemValues;
                        //objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "import";

                        //objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;

                        //objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar).Value = User.Identity.Name;

                        _objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = importTemp1;
                        _objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "Validation For Item ID";
                        SqlDataAdapter sde1 = new SqlDataAdapter(_objSqlCommand);

                        DataTable rowsPicklistItemValue = new DataTable();
                        sde1.Fill(rowsPicklistItemValue);

                        if (rowsPicklistItemValue != null)
                        {
                            foreach (DataRow _misMatchedIdValues in rowsPicklistItemValue.Rows)
                            {
                                if (importDataTable.Rows.Count == 0)
                                {
                                    missingData = 0;
                                }
                                validateMissMatchedValues = "3";
                                if (missingDataCount == 0)
                                {
                                    importDataTable.Rows.Add(_misMatchedIdValues.ItemArray);
                                    //importDataTable = importDataTable.AsEnumerable().Where(a => a.Field<string>("PICKLIST_NAME") == null).CopyToDataTable();
                                    //importDataTable.Rows.Add(result);
                                    //importDataTable.Rows.Add([1].ItemArray);
                                    //importDataTable.Rows.Add(rows.result);
                                    //  dataRow.Table.Rows.Add(importDataTable);
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = "Missmatched Picklist Item ID.";
                                    importDataTable.Rows[missingData]["MESSAGE"] = "Picklist Item ID should be matched with table value.";
                                    // importDataTable.Rows.Add(dataRow);
                                }
                                else
                                {
                                    importDataTable.Rows.Add(_misMatchedIdValues.ItemArray);
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = "\n" + "Missmatched Picklist Item ID.";
                                    importDataTable.Rows[missingData]["MESSAGE"] = importDataTable.Rows[missingData]["MESSAGE"].ToString() + "\n" + "Picklist Item ID should be matched with tables Value.";
                                }
                                //   missingDataCount++;
                                missingData++;
                            }
                        }
                        else if (validatePickListValuesMissing == "2")
                        {
                            validateMissMatchedValues = "3";
                        }

                        /***************DATA TYPE - START************/

                        SqlCommand dataType_objSqlCommand = objSqlConnection.CreateCommand();
                        dataType_objSqlCommand.CommandText = "STP_LS_IMPORT_PICKLIST";
                        dataType_objSqlCommand.CommandType = CommandType.StoredProcedure;
                        dataType_objSqlCommand.Connection = objSqlConnection;
                        dataType_objSqlCommand.CommandTimeout = 0;
                        dataType_objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = importTemp1;
                        dataType_objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "Validation For DataType";
                        SqlDataAdapter sde_DataType = new SqlDataAdapter(dataType_objSqlCommand);

                        DataTable dataType_DT = new DataTable();
                        sde_DataType.Fill(dataType_DT);

                     
                            foreach (DataRow _dataTypeVal in dataType_DT.Rows)
                            {
                                if (importDataTable.Rows.Count == 0)
                                {
                                    missingData = 0;
                                }
                              validateDataType = "5";
                                if (missingDataCount == 0)
                                {
                                    importDataTable.Rows.Add(_dataTypeVal.ItemArray);
                                    //importDataTable = importDataTable.AsEnumerable().Where(a => a.Field<string>("PICKLIST_NAME") == null).CopyToDataTable();
                                    //importDataTable.Rows.Add(result);
                                    //importDataTable.Rows.Add([1].ItemArray);
                                    //importDataTable.Rows.Add(rows.result);
                                    //  dataRow.Table.Rows.Add(importDataTable);
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = "Picklist Data Type.";
                                    importDataTable.Rows[missingData]["MESSAGE"] = "DataType and Values are mismatched.";
                                    // importDataTable.Rows.Add(dataRow);
                                }
                                else
                                {
                                    importDataTable.Rows.Add(_dataTypeVal.ItemArray);
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = "\n" + "Picklist Data Type.";
                                    importDataTable.Rows[missingData]["MESSAGE"] = importDataTable.Rows[missingData]["MESSAGE"].ToString() + "\n" + "DataType and Values are mismatched.";
                                }
                                //   missingDataCount++;
                                missingData++;
                            }

                     

                      



                    


                        /***************DATA TYPE - END************/

                        /***************DUPLICATE RECORDE-MORE THAN ONCE - START************/

                        DataTable dup_Picklist_Values = new DataTable();

                        //var dups_PicklistValues = from row in filterMandatoryColumns.Copy().AsEnumerable()
                        //                          group row by new { PICKLIST_NAME = row.Field<string>("PICKLIST_NAME"), PICKLIST_ITEM_VALUE = row.Field<string>("PICKLIST_ITEM_VALUE") }
                        //                         into grp
                        //                         where grp.Count() > 1
                        //                         select grp.Key;

                        var dups_PicklistValues = filterMandatoryColumns.AsEnumerable().GroupBy(i => new { PICKLIST_NAME = i.Field<string>("PICKLIST_NAME"), PICKLIST_ITEM_VALUE = i.Field<string>("PICKLIST_ITEM_VALUE") }).Where(g => g.Count() > 1).Select(g => new { g.Key.PICKLIST_NAME, g.Key.PICKLIST_ITEM_VALUE }).ToList();


                        foreach (var duplicate_Values in dups_PicklistValues)
                        {
                            string dupPicklistName = duplicate_Values.PICKLIST_NAME.ToString();
                            string dupPicklistValue = duplicate_Values.PICKLIST_ITEM_VALUE.ToString();
                            foreach (DataRow datarow_duplicate_Valus in filterMandatoryColumns.Rows)
                            {
                                string checkDuplicate_Name = datarow_duplicate_Valus["PICKLIST_NAME"].ToString();
                                string checkDuplicate_Value = datarow_duplicate_Valus["PICKLIST_ITEM_VALUE"].ToString();
                                if (checkDuplicate_Name.Equals(dupPicklistName) && checkDuplicate_Value.Equals(dupPicklistValue))
                                {
                                    if (importDataTable.Rows.Count == 0)
                                    {
                                        missingData = 0;
                                    }
                                    validateMissMatchedValues = "3";
                                    if (missingDataCount == 0)
                                    {
                                        importDataTable.Rows.Add(datarow_duplicate_Valus.ItemArray);
                                        //importDataTable = importDataTable.AsEnumerable().Where(a => a.Field<string>("PICKLIST_NAME") == null).CopyToDataTable();
                                        //importDataTable.Rows.Add(result);
                                        //importDataTable.Rows.Add([1].ItemArray);
                                        //importDataTable.Rows.Add(rows.result);
                                        //  dataRow.Table.Rows.Add(importDataTable);
                                        importDataTable.Rows[missingData]["VALIDATION TYPE"] = "Duplicate value.";
                                        importDataTable.Rows[missingData]["MESSAGE"] = "Values occurred more than once.";
                                        // importDataTable.Rows.Add(dataRow);


                                    }
                                    else
                                    {
                                        importDataTable.Rows.Add(datarow_duplicate_Valus.ItemArray);
                                        importDataTable.Rows[missingData]["VALIDATION TYPE"] = "\n" + "Duplicate value.";
                                        importDataTable.Rows[missingData]["MESSAGE"] = importDataTable.Rows[missingData]["MESSAGE"].ToString() + "\n" + "Values occurred more than once.";
                                    }
                                    //   missingDataCount++;
                                    missingData++;
                                }
                            }


                        }


                        /***************DUPLICATE RECORDE-MORE THAN ONCE - END************/


                        DataTable pickListNameId = null;
                        var rowsPickList = filterMandatoryColumns.AsEnumerable()
                                    .Where(r => r.Field<string>("PICKLIST_NAME") != null && r.Field<string>("PICKLIST_ID") != null);


                        if (rowsPickList.Any())
                        {
                            pickListNameId = rowsPickList.CopyToDataTable();
                        }


                        if (pickListNameId != null)
                        {
                            var misMatchValue = _dbcontext.TB_PICKLIST.Select(s => new { s.PICKLIST_NAME, s.ID }).OrderBy(x => x.ID).ToList();
                            DataTable dt_MisMatchValues = new DataTable();
                            dt_MisMatchValues.Columns.Add("PICKLIST_NAME", typeof(string));
                            dt_MisMatchValues.Columns.Add("ID", typeof(Int32));
                            foreach (var misMatchValues in misMatchValue)
                            {
                                dt_MisMatchValues.Rows.Add(misMatchValues.PICKLIST_NAME, misMatchValues.ID);

                            }

                            //if(pickListIdName== string.Empty)
                            //DataTable misMatchedValues = (from table1 in dt_MisMatchValues.AsEnumerable() join table2 in filterMandatoryColumns.AsEnumerable() on table1["ID"].ToString() equals table2["PICKLIST_ID"].ToString() where table1.Field<string>("PICKLIST_NAME") != table2.Field<string>("PICKLIST_NAME") && table2.Field<string>("PICKLIST_NAME") != null select table2).CopyToDataTable();
                            DataTable misMatchedValues = null;
                            var rowsPickListMisMatchedValues = from table1 in dt_MisMatchValues.AsEnumerable() join table2 in filterMandatoryColumns.AsEnumerable() on table1["ID"].ToString() equals table2["PICKLIST_ID"].ToString() where table1.Field<string>("PICKLIST_NAME") != table2.Field<string>("PICKLIST_NAME") && table2.Field<string>("PICKLIST_NAME") != null select table2;


                            if (rowsPickListMisMatchedValues.Any())
                            {
                                misMatchedValues = rowsPickListMisMatchedValues.CopyToDataTable();
                            }

                            if (misMatchedValues != null)
                            {
                                foreach (DataRow _misMatchedValues in misMatchedValues.Rows)
                                {
                                    if (importDataTable.Rows.Count == 0)
                                    {
                                        missingData = 0;
                                    }
                                    validateMissMatchedValues = "3";
                                    if (missingDataCount == 0)
                                    {
                                        importDataTable.Rows.Add(_misMatchedValues.ItemArray);
                                        //importDataTable = importDataTable.AsEnumerable().Where(a => a.Field<string>("PICKLIST_NAME") == null).CopyToDataTable();
                                        //importDataTable.Rows.Add(result);
                                        //importDataTable.Rows.Add([1].ItemArray);
                                        //importDataTable.Rows.Add(rows.result);
                                        //  dataRow.Table.Rows.Add(importDataTable);
                                        importDataTable.Rows[missingData]["VALIDATION TYPE"] = "Missmatched column value.";
                                        importDataTable.Rows[missingData]["MESSAGE"] = "PickList Name should be matched with tables Value.";
                                        // importDataTable.Rows.Add(dataRow);


                                    }
                                    else
                                    {
                                        importDataTable.Rows.Add(_misMatchedValues.ItemArray);
                                        importDataTable.Rows[missingData]["VALIDATION TYPE"] = "\n" + "Missmatched column value.";
                                        importDataTable.Rows[missingData]["MESSAGE"] = importDataTable.Rows[missingData]["MESSAGE"].ToString() + "\n" + "PickList Name should be matched with tables Value.";
                                    }
                                    //   missingDataCount++;
                                    missingData++;
                                }
                            }

                        }
                        else if (validatePickListValuesMissing == "2")
                        {
                            validateMissMatchedValues = "3";
                        }


                        //var referenceValue = _dbcontext.TB_ATTRIBUTE.GroupBy(x => x.PICKLIST_NAME).Select(x => x.FirstOrDefault()).ToList();

                        SqlCommand _objectSqlCommand = objSqlConnection.CreateCommand();
                        _objectSqlCommand.CommandText = "STP_LS_IMPORT_PICKLIST";
                        _objectSqlCommand.CommandType = CommandType.StoredProcedure;
                        _objectSqlCommand.Connection = objSqlConnection;
                        _objectSqlCommand.CommandTimeout = 0;
                        //objSqlCommand.Parameters.Add("@PICKLIST_NAME", SqlDbType.VarChar, 100).Value = pickList;
                        //objSqlCommand.Parameters.Add("@PICKLIST_ITEM_VALUE", SqlDbType.VarChar, 100).Value = itemValues;
                        //objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "import";

                        //objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;

                        //objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar).Value = User.Identity.Name;

                        _objectSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = importTemp1;
                        _objectSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "Validation For Delete";
                        SqlDataAdapter sde2 = new SqlDataAdapter(_objectSqlCommand);

                        DataTable referenceExists = new DataTable();
                        sde2.Fill(referenceExists);

                        if (referenceExists != null)
                        {

                            foreach (DataRow _referenceExists in referenceExists.Rows)
                            {
                                validateReferencesExists = "4";
                                if (importDataTable.Rows.Count == 0)
                                {
                                    missingData = 0;
                                }
                                if (missingDataCount == 0)
                                {
                                    importDataTable.Rows.Add(_referenceExists.ItemArray);
                                    //importDataTable = importDataTable.AsEnumerable().Where(a => a.Field<string>("PICKLIST_NAME") == null).CopyToDataTable();
                                    //importDataTable.Rows.Add(result);
                                    //importDataTable.Rows.Add([1].ItemArray);
                                    //importDataTable.Rows.Add(rows.result);
                                    //  dataRow.Table.Rows.Add(importDataTable);
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = "Invalid Delete Row.";
                                    importDataTable.Rows[missingData]["MESSAGE"] = "Delete Row is associated with other table.";
                                    // importDataTable.Rows.Add(dataRow);
                                }
                                else
                                {
                                    importDataTable.Rows.Add(_referenceExists.ItemArray);
                                    importDataTable.Rows[missingData]["VALIDATION TYPE"] = "\n" + "Invalid Delete Row..";
                                    importDataTable.Rows[missingData]["MESSAGE"] = importDataTable.Rows[missingData]["MESSAGE"].ToString() + "\n" + "Delete Row is associated with other table.";
                                }
                                //   missingDataCount++;
                                missingData++;

                            }

                        }

                    }
                }
                else
                {
                    validatePickListValuesMissing = "2";
                    validateMissMatchedValues = "3";
                    validateReferencesExists = "4";
                }


                //var duplicates = dt.AsEnumerable()
                //    .Select(dr => dr.Field<string>("PICKLIST_NAME"))
                //    .GroupBy(x => x)
                //    .Where(g => g.Count() > 1)
                //    .Select(g => g.Key)
                //    .ToList();
                var errorLogDataTableRowRemove = importDataTable.AsEnumerable().Where(row => row.Field<string>("VALIDATION TYPE") == null && row.Field<string>("MESSAGE") == null);
                foreach (var row in errorLogDataTableRowRemove.ToList())
                {
                    row.Delete();
                }
                HttpContext.Current.Session["validatePickListImport"] = importDataTable;
                System.Web.HttpContext.Current.Session["downloadLogPage"] = importDataTable;
                if (validateMissingColumns == string.Empty && validatePickListValuesMissing == string.Empty && validateMissMatchedValues == string.Empty && validateReferencesExists == string.Empty && validateDataType == string.Empty)
                {
                    validateResults = "0";
                }
                else
                {
                    validateResults = "1";
                }

                if (validateResults.Contains("0"))
                {
                    return "true";
                }
                else
                {
                    return "false" + "~" + validateMissingColumns + "~" + validatePickListValuesMissing + "~" + validateMissMatchedValues + "~" + validateReferencesExists + "~" + validateDataType;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at PickListImportApiController : validatePickListImport", ex);
                return "False";
            }

        }


        public DataTable ReadExcelFile(string importExcelSheetDDSelectionValue, string excelPath)
        {
            string sheet_Name = string.Empty;
            sheet_Name = importExcelSheetDDSelectionValue + "$";
            string sqlquery = "SELECT * FROM [" + sheet_Name + "]";
            DataSet importDataTables = new DataSet();
            string constring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 12.0;HDR=YES;\"";
            OleDbConnection oledbConnection = new OleDbConnection(constring + "");
            OleDbDataAdapter oledbDA = new OleDbDataAdapter(sqlquery, oledbConnection);
            oledbDA.Fill(importDataTables);
            DataTable importDataTable = importDataTables.Tables[0];
            return importDataTable;
        }
        [System.Web.Http.HttpPost]
        public JsonResult PickListViewLog()
        {
            DashBoardModel model = new DashBoardModel();
            var pickListImportTableLog = (DataTable)System.Web.HttpContext.Current.Session["validatePickListImport"];
            foreach (DataColumn columns in pickListImportTableLog.Columns)
            {
                var objPTColumns = new PTColumns();
                objPTColumns.Caption = columns.Caption;
                objPTColumns.ColumnName = columns.ColumnName;
                model.Columns.Add(objPTColumns);
            }
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(pickListImportTableLog);
            model.Data = JSONString;
            return new JsonResult() { Data = model };
        }

        //-------------------------------viewlogdownload--------------------------------------//
        [System.Web.Http.HttpPost]
        public HttpResponseMessage importlogs(string Errorlogoutputformat, string fileName)
        {
            try
            {
                if (Errorlogoutputformat.ToLower() == "csv")
                {

                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".csv" : fileName + ".csv");
                }
                else if (Errorlogoutputformat.ToLower() == "text")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".txt" : fileName + ".txt");
                }
                else if (Errorlogoutputformat.ToLower() == "xml")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".xml" : fileName + ".xml");
                }
                else if (Errorlogoutputformat.ToLower() == "xlsx")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".xlsx" : fileName + ".xlsx");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".xls" : fileName + ".xls");
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at PickListImportApiController : importlogs", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        [System.Web.Http.HttpPost]
        public string pickListImport(string importExcelSheetDDSelectionValue, string excelPath)
        {
            try
            {

                //int customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                int insertRecords = 0; int updateRecords = 0; int skippedRecords = 0; int deleteRecords = 0;

                DataTable dt_PickListImportTable = new DataTable();
                //  DataTable dt_PickListValResult = new DataTable();
                bool isAnonymous;
                string importTemp;
                importTemp = Guid.NewGuid().ToString();

                string sqlString = string.Empty;
                dt_PickListImportTable = ReadExcelFile(importExcelSheetDDSelectionValue, excelPath);
                


                //DataTable importExcelData = ImportTable.Clone();
                // DataTable pickListImportDataTable = dt_PickListImportTable.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                DataTable dt_PickListValResult = null;
                DataTable dt_PickList_Import_Res = null;
                //  var row = dt_PickListImportTable.AsEnumerable().Where(x => x.Field<string>("ACTION") != "") ;

                var pickListValueResult = dt_PickListImportTable.AsEnumerable().Where(x => x.Field<string>("ACTION") != "" && x.Field<string>("ACTION") != null);
                if (pickListValueResult.Any())
                {
                    dt_PickListValResult = pickListValueResult.CopyToDataTable();


                    foreach (DataRow dr in dt_PickListValResult.Rows)
                    {
                        string action = dr["ACTION"].ToString();
                        action = action.ToUpper();
                        if (action.Equals("SKIP"))
                        {
                            skippedRecords = skippedRecords + 1;
                            dr.Delete();
                        }

                        if (action.Equals("DELETE"))
                        {

                            string pickListname = dr["PICKLIST_NAME"].ToString();
                            string itemValue = dr["PICKLIST_ITEM_VALUE"].ToString();

                            using (var objSqlConnection = new SqlConnection(connectionString))
                            {
                                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                objSqlCommand.CommandText = "STP_LS_IMPORT_PICKLIST";
                                objSqlCommand.CommandType = CommandType.StoredProcedure;
                                objSqlCommand.Connection = objSqlConnection;
                                objSqlCommand.CommandTimeout = 0;
                                objSqlCommand.Parameters.Add("@PICKLIST_NAME", SqlDbType.VarChar, 100).Value = pickListname;
                                objSqlCommand.Parameters.Add("@PICKLIST_ITEM_VALUE", SqlDbType.VarChar, 100).Value = itemValue;
                                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "DELETE";
                                objSqlConnection.Open();
                                objSqlCommand.ExecuteNonQuery();
                                objSqlConnection.Close();
                            }
                            deleteRecords = deleteRecords + 1;
                            dr.Delete();
                        }

                    }
                }
                
                var PickListImports = dt_PickListImportTable.AsEnumerable().Where(x => x.Field<string>("ACTION") == "" || x.Field<string>("ACTION") == null);


                if (PickListImports.Any())
                {
                    dt_PickList_Import_Res = PickListImports.CopyToDataTable();


                    if (!dt_PickList_Import_Res.Columns.Contains("PICKLIST_ID"))
                    {
                        dt_PickList_Import_Res.Columns.Add("PICKLIST_ID", typeof(string));
                    }
                    if (!dt_PickList_Import_Res.Columns.Contains("PICKLIST_ITEM_ID"))
                    {
                        dt_PickList_Import_Res.Columns.Add("PICKLIST_ITEM_ID", typeof(string));
                    }


                    string pickList = Convert.ToString(dt_PickList_Import_Res.Rows[0]["PICKLIST_NAME"]);

                    string itemValues = Convert.ToString(dt_PickList_Import_Res.Rows[0]["PICKLIST_ITEM_VALUE"]);

                    using (var objSqlConnection = new SqlConnection(connectionString))
                    {


                        objSqlConnection.Open();


                        sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                        _DBCommand.ExecuteNonQuery();
                        sqlString = CreateTable("[##IMPORTTEMP" + importTemp + "]", dt_PickList_Import_Res);

                        SqlCommand dbCommandnew = new SqlCommand(sqlString, objSqlConnection);
                        dbCommandnew.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(objSqlConnection)
                        {
                            DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(dt_PickList_Import_Res);


                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_LS_IMPORT_PICKLIST";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.CommandTimeout = 0;
                        objSqlCommand.Parameters.Add("@PICKLIST_NAME", SqlDbType.VarChar, 100).Value = pickList;
                        objSqlCommand.Parameters.Add("@PICKLIST_ITEM_VALUE", SqlDbType.VarChar, 100).Value = itemValues;
                        //objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "import";

                        objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;

                        objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar).Value = User.Identity.Name;

                        objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = importTemp;
                        objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "IMPORT";
                        SqlDataAdapter sde = new SqlDataAdapter(objSqlCommand);

                        DataSet countValues = new DataSet();
                        sde.Fill(countValues);
                        if (countValues.Tables.Count > 2)
                        {
                            updateRecords = Convert.ToInt32(countValues.Tables[0].Rows[0]["updated"]) + Convert.ToInt32(countValues.Tables[2].Rows[0]["updated"]);
                            insertRecords = Convert.ToInt32(countValues.Tables[1].Rows[0]["inserted"]) + Convert.ToInt32(countValues.Tables[3].Rows[0]["inserted"]);
                        }
                        else
                        {
                            updateRecords = Convert.ToInt32(countValues.Tables[0].Rows[0]["updated"]);
                            insertRecords = Convert.ToInt32(countValues.Tables[1].Rows[0]["inserted"]);
                        }
                    }
                }
                // objSqlCommand.ExecuteNonQuery();



                stopWatch.Stop();
                string calcualteImportTiminings = string.Format("{0}:{1}:{2}", stopWatch.Elapsed.Hours, stopWatch.Elapsed.Minutes, stopWatch.Elapsed.Seconds);
                return "true" + "~" + insertRecords + "~" + updateRecords + "~" + calcualteImportTiminings + "~" + skippedRecords + "~" + deleteRecords + "~" + User.Identity.Name;

                // return "true";

            }
            catch (Exception ex)
            {
                _logger.Error("Error at pickListImport", ex);
                return "false";
            }
        }
        public string CreateTable(string tableName, DataTable objtable)
        {
            try
            {
                string sqlsc = "CREATE TABLE " + tableName + "(";
                for (int i = 0; i < objtable.Columns.Count; i++)
                {
                    if (!objtable.Columns[i].ColumnName.Contains('['))
                    {
                        sqlsc += "\n [" + objtable.Columns[i].ColumnName + "] ";
                    }
                    else
                    {
                        sqlsc += "\n" + objtable.Columns[i].ColumnName + "";
                    }
                    if (objtable.Columns[i].ColumnName.Contains("CATALOG_ID"))
                        sqlsc += "nvarchar(50) ";
                    else if (objtable.Columns[i].DataType.ToString().Contains("System.String") ||
                             objtable.Columns[i].ColumnName.Contains("SUBCATID") ||
                             objtable.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                        sqlsc += "nvarchar(max) ";
                    else
                        sqlsc += "nvarchar(max) ";
                    sqlsc += ",";
                }
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    var sqlString = "EXEC('delete from QS_IMPORTMAPPING')";
                    SqlCommand sqlCommand = new SqlCommand(sqlString, con);
                    sqlCommand.ExecuteNonQuery();
                    con.Dispose();
                }

                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CreateTable : Program - ", ex);
                throw;
            }
        }

    }
    //[System.Web.Http.HttpPost]
    //public string FinishImport(

}


