﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using LS.Data;
using LS.Data.Model.ProductView;
using System.Globalization;
using System.Collections;
using log4net;
using LS.Data.Utilities;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Http;
using Newtonsoft.Json.Linq;

namespace LS.Web.Controllers
{
    public class WizardController : Controller
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(WizardController));
        readonly CSEntities _dbcontext = new CSEntities();
        // GET: Wizard
        public ActionResult GlobalAttributemanager()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _dbcontext.vw_aspnet_UsersInRoles
                    .Join(_dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new {tcptps, tcp})
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/Wizard/GlobalAttributemanager.cshtml");
            }
            var rolefunctionresults = _dbcontext.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000061).ToList();
            var rolefunctionresults1 = _dbcontext.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000062).ToList();
            var rolefunctionresults2 = _dbcontext.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000063).ToList();
            var rolefunctionresults3 = _dbcontext.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000064).ToList();
            if (rolefunctionresults.Count > 0 || rolefunctionresults1.Count > 0 || rolefunctionresults2.Count > 0 || rolefunctionresults3.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/Wizard/GlobalAttributemanager.cshtml");
            }
            else
            {
                return RedirectToAction("LogOn", "Accounts");
            }
           
           // return View("~/Views/Wizard/GlobalAttributemanagersubproduct.cshtml");
        }
        
        public ActionResult ProductTableManager()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _dbcontext.vw_aspnet_UsersInRoles
                    .Join(_dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new {tcptps, tcp})
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/Wizard/ProductTableManager.cshtml");
            }
            var rolefunctionresults = _dbcontext.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000033).ToList();
            
            if (rolefunctionresults.Count > 0  )
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/Wizard/ProductTableManager.cshtml");
            }
            else
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            
        }

        public JsonResult GetAllattributes(int attributeid,int catalogId)
        {
            try
            {
                //var attributes = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.TB_CATALOG_ATTRIBUTES, tcp => tcp.ATTRIBUTE_ID,
                //         tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf }).Where(a => a.tpf.CATALOG_ID == catalogId && a.tcp.ATTRIBUTE_TYPE == attributeid).Select(s => new LS_TB_ATTRIBUTE
                //         {
                //             ATTRIBUTE_TYPE = s.tcp.ATTRIBUTE_TYPE,
                //             ATTRIBUTE_ID = s.tcp.ATTRIBUTE_ID,
                //             ATTRIBUTE_NAME = s.tcp.ATTRIBUTE_NAME
                //         }).ToList();
                 var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                if (customerid != null)
                {

                    var attributes =
                        _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                            .Where(a => a.tcp.ATTRIBUTE_TYPE == attributeid && a.tpf.CUSTOMER_ID == customerid.CustomerId)
                            .Select(s => new LS_TB_ATTRIBUTE
                            {
                                ATTRIBUTE_TYPE = s.tcp.ATTRIBUTE_TYPE,
                                ATTRIBUTE_ID = s.tcp.ATTRIBUTE_ID,
                                ATTRIBUTE_NAME = s.tcp.ATTRIBUTE_NAME
                            }).ToList();
                    var catalogAttributes = attributes.Join(_dbcontext.TB_CATALOG_ATTRIBUTES, attr => attr.ATTRIBUTE_ID, tca => tca.ATTRIBUTE_ID, (attr, tca) => new { attr, tca }).Where(y => y.tca.CATALOG_ID == catalogId).Select(x => x.attr).ToList();
                    return Json(catalogAttributes, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception objException)
            {
                return null;
            }
        }

        [HttpGet]
        public IList GetAttributeDetails(int Attributetype)
        {  var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
            if (customerid != null)
            {
                var attributes = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                    .Where(x => x.tcp.ATTRIBUTE_TYPE == Attributetype && x.tpf.CUSTOMER_ID == customerid.CustomerId)
                    .Select(x => new
                    {
                        x.tcp.ATTRIBUTE_NAME,
                        x.tcp.ATTRIBUTE_ID
                    }).ToList();
                return attributes;
            }
            else
            {
                return null;
            }
            //var aa = dbcontext.TB_ATTRIBUTE.Where(a => a.ATTRIBUTE_TYPE == Attributetype).ToList();
            //return aa;
        }
        public IList GetdropdownCatalog(int attrid)
        {
            try
            {
                // var list = new ArrayList();
                var catalogs = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_ID != 0 && !x.CATALOG_NAME.ToLower().Contains("master")).OrderBy(x => x.CATALOG_NAME)
                    .Select(x => new
                    {
                        x.CATALOG_ID,
                        x.CATALOG_NAME

                    }).ToList();
                return catalogs;
                //foreach (var row in catalogs)
                //{

                //    var catalog = new TB_CATALOG();

                //    catalog.CATALOG_ID = row.CATALOG_ID;
                //    catalog.CATALOG_NAME = row.CATALOG_NAME;
                //    list.Add(catalog);
                //}
                //return list;

            }
            catch (Exception objException)
            {
                return null;
            }
        }
        //[HttpPost]
        //public HttpResponseMessage globalattribute(string Attrid, int Catalogid, int Attributetype, string option, int sort, object model)
        //{
        //    try
        //    {
        //        string familyid = "23";
        //        string[] selectedoption = option.Split(',');
        //        foreach (string opt in selectedoption)
        //        {
        //            if (opt.Equals("REMOVE"))
        //            {
        //                Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
        //            }
        //            if (opt.Equals("UNPUBLISH"))
        //            {
        //                Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
        //            }
        //            var stp = _dbcontext.STP_CATALOGSTUDIO5_Global_attribute_manager(Catalogid, familyid, Attributetype, Attrid, opt, sort);
        //        }
        //      //  return ResponseMsg.Inserted;
        //        return null;


        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("Error at WizardController:  globalattribute", ex);
        //        //return ResponseMsg.Error;
        //        return null;
        //    }
        //}
        //public string globalCalculatedAttribute(string familyid, int Catalogid, string option)
        //{
        //    try
        //    {
        //        var stp = _dbcontext.STP_CATALOGSTUDIO5_Global_attribute_manager(Catalogid, familyid, 0, "0", option, 0);
        //        return ResponseMsg.Inserted;

        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("Error at WizardController:  globalCalculatedAttribute", ex);
        //        return ResponseMsg.Error;
        //    }
        //}

        public string UpdatePtHeader(int catalogId, int[] familyIds, bool header, string items)
        {
            try
            {
                if (!string.IsNullOrEmpty(items))
                {
                    var name = items.Split(',');
                    for (int fid = 0; fid < name.Length; fid++)
                    {
                        if (Convert.ToString(name[fid]).Contains('~'))
                        {
                            UpdateTableHeader(catalogId, Convert.ToString(name[fid]).Trim('~'), header);
                        }
                        else
                        {
                            string category_id = Convert.ToString(name[fid]);
                            var category_ids = _dbcontext.Category_Function(catalogId, category_id);
                            if (category_ids.Any())
                            {
                                foreach (var item in category_ids)
                                {
                                    category_id = item.CATEGORY_ID;
                                    var family_ids = _dbcontext.TB_CATALOG_FAMILY.Where(a => a.CATALOG_ID == catalogId && a.CATEGORY_ID == category_id);
                                    if (family_ids.Any())
                                    {
                                        foreach (var id in family_ids)
                                        {
                                            UpdateTableHeader(catalogId, id.FAMILY_ID.ToString(), header);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    _dbcontext.SaveChanges();
                }
                return "Update completed";
            }
            catch (Exception ex)
            {
                Logger.Error("Error at WizardController:  UpdatePTHeader", ex);
                return ResponseMsg.Error;
            }
        }

        public void UpdateTableHeader(int catalogId, string fid, bool header)
        {
            string structure = @"<?xml version=""1.0"" encoding=""utf-8""?><TradingBell TableType=""SuperTable""><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>";
            string familyId = fid;
            int famId = Convert.ToInt32(familyId);
            var tbFamilyTableStructure = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.FirstOrDefault(x => x.CATALOG_ID == catalogId && x.FAMILY_ID == famId && x.IS_DEFAULT);
            if (tbFamilyTableStructure == null)
            {
                tbFamilyTableStructure = new TB_FAMILY_TABLE_STRUCTURE();
                tbFamilyTableStructure.CATALOG_ID = catalogId;
                tbFamilyTableStructure.FAMILY_ID = famId;
                tbFamilyTableStructure.STRUCTURE_NAME = "Default Layout";
                tbFamilyTableStructure.FAMILY_TABLE_STRUCTURE = structure;
                tbFamilyTableStructure.IS_DEFAULT = true;
                tbFamilyTableStructure.CREATED_USER = User.Identity.Name;
                tbFamilyTableStructure.CREATED_DATE = DateTime.Now;
                tbFamilyTableStructure.MODIFIED_USER = User.Identity.Name;
                tbFamilyTableStructure.MODIFIED_DATE = DateTime.Now;
                _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Add(tbFamilyTableStructure);
                tbFamilyTableStructure = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.FirstOrDefault(x => x.CATALOG_ID == catalogId && x.FAMILY_ID == famId && x.IS_DEFAULT);

            }
            if (tbFamilyTableStructure != null)
            {
                if (tbFamilyTableStructure.FAMILY_TABLE_STRUCTURE != null)
                {
                    var ds = tbFamilyTableStructure.FAMILY_TABLE_STRUCTURE;
                    string productTableStructure = ds;
                    int rowIndex = productTableStructure.IndexOf("<DisplayRowHeader>", StringComparison.Ordinal);
                    int summeryIndex = productTableStructure.IndexOf("</DisplaySummaryHeader>", StringComparison.Ordinal);
                    string headerstructure = productTableStructure.Substring(rowIndex, (summeryIndex - rowIndex) + 23);
                    string newHeaderStructure;
                    if (header == true)
                    {
                        newHeaderStructure = "<DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>True</DisplayColumnHeader><DisplaySummaryHeader>True</DisplaySummaryHeader>";
                    }
                    else
                    {
                        newHeaderStructure = "<DisplayRowHeader>False</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader>";
                    }
                    productTableStructure = productTableStructure.Replace(headerstructure, newHeaderStructure);
                    tbFamilyTableStructure.FAMILY_TABLE_STRUCTURE = productTableStructure;


                }
            }
        }
        public string UpdatePtstructure(int[] familyid, string ttype, int refFid, string items, int catalogId)
        {

            try
            {
                if (!string.IsNullOrEmpty(items))
                {

                    string[] name = items.Split(',');


                    for (int fid = 0; fid < name.Length; fid++)
                    {
                        if (Convert.ToString(name[fid]).Contains('~'))
                        {
                            UpdateStructure(Convert.ToString(name[fid].Trim('~')), catalogId, ttype);
                        }
                        else
                        {
                            string category_id = Convert.ToString(name[fid]);
                            var category_ids = _dbcontext.Category_Function(catalogId, category_id);
                            if (category_ids.Any())
                            {
                                foreach (var item in category_ids)
                                {
                                    category_id = item.CATEGORY_ID;
                                    var family_ids = _dbcontext.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == category_id && a.CATALOG_ID == catalogId);
                                    if (family_ids.Any())
                                    {
                                        foreach (var id in family_ids)
                                        {
                                            UpdateStructure(Convert.ToString(id.FAMILY_ID), catalogId, ttype);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }
                return "Saved successfully";
            }
            catch (Exception ex)
            {
                Logger.Error("Error at WizardController:  UpdatePTHeader", ex);
                return ResponseMsg.Error;
            }
        }

        public void UpdateStructure(string family_id, int catalog_id, string ttype)
        {
            SqlConnection objSqlConnection =
                       new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            SqlCommand objcommand = new SqlCommand();
            int familyId = 0;
            familyId = Convert.ToInt32(family_id);
            var FamilyStructureDS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.FirstOrDefault(x => x.CATALOG_ID == catalog_id && x.FAMILY_ID == familyId && x.IS_DEFAULT);
            string structure = @"<?xml version=""1.0"" encoding=""utf-8""?><TradingBell TableType=""SuperTable""><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>";
            if (FamilyStructureDS == null)
            {
                CreateDefaultLayout(catalog_id, familyId, structure);
                FamilyStructureDS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.FirstOrDefault(x => x.CATALOG_ID == catalog_id && x.FAMILY_ID == familyId && x.IS_DEFAULT);
            }
            if (FamilyStructureDS != null)
            {
                var productTableStructure = FamilyStructureDS;
                if (ttype.StartsWith("Hor"))
                {
                    string SQLString = "EXEC STP_CATALOGSTUDIO5_InsertFamilyTableStructure " + familyId + "," + catalog_id + ",'','','Horizontal',1";
                    objcommand.Connection = objSqlConnection;
                    objcommand.CommandText = SQLString;
                    objcommand.CommandType = CommandType.Text;
                    objcommand.CommandTimeout = 0;
                    objSqlConnection.Open();
                    objcommand.ExecuteNonQuery();
                    objSqlConnection.Close();
                }
                else if (ttype.StartsWith("Ver"))
                {
                    string SQLString = "EXEC STP_CATALOGSTUDIO5_InsertFamilyTableStructure " + familyId + "," + catalog_id + ",'','','Vertical',1";
                    objcommand.Connection = objSqlConnection;
                    objcommand.CommandText = SQLString;
                    objcommand.CommandType = CommandType.Text;
                    objcommand.CommandTimeout = 0;
                    objSqlConnection.Open();
                    objcommand.ExecuteNonQuery();
                    objSqlConnection.Close();

                }
                else if (ttype.StartsWith("Piv"))
                {
                    string sqlQuery = string.Empty;
                    //if (productTableStructure.Length > 0)
                    {
                        //sqlQuery = "UPDATE TB_FAMILY_TABLE_STRUCTURE SET FAMILY_TABLE_STRUCTURE=N'" + productTableStructure + "' WHERE CATALOG_ID=" + _Catalog_ID + " AND FAMILY_ID=" + familyID[fid] + " AND IS_DEFAULT=1";
                    }
                    //else
                    {
                        //productTableStructure = "<?xml version=\"1.0\" encoding=\"utf-8\"?><TradingBell TableType=\"SuperTable\"><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>";
                        //sqlQuery = "UPDATE TB_FAMILY_TABLE_STRUCTURE SET FAMILY_TABLE_STRUCTURE=N'" + productTableStructure + "' WHERE CATALOG_ID=" + _Catalog_ID + " AND FAMILY_ID=" + familyID[fid] + " AND IS_DEFAULT=1";
                    }
                    //int resultQuery = ExecuteSQLQuery(sqlQuery);
                }
            }
        }
        public string UpdatePtstructurepivot(int[] familyid, string ttype, int refFid, string items, int catalogId)
        {
            int selectedFamilyid = familyid[0];
            int familyId = 0;

            try
            {
                if (!string.IsNullOrEmpty(items))
                {
                    if (selectedFamilyid != 0)
                    {
                        var FamilyStructureDS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.FirstOrDefault(x => x.CATALOG_ID == catalogId && x.FAMILY_ID == selectedFamilyid && x.IS_DEFAULT);
                        if (FamilyStructureDS != null)
                        {
                            string structure = string.Empty;
                            structure = FamilyStructureDS.FAMILY_TABLE_STRUCTURE;
                            string[] name = items.Split(',');
                            for (int fid = 0; fid < name.Length; fid++)
                            {
                                if (Convert.ToString(name[fid]).Contains('~'))
                                    InsertFamilyTableStruct(Convert.ToString(name[fid]).Trim('~'), catalogId, structure);
                                else
                                {
                                    string category_id = Convert.ToString(name[fid]);
                                    var category_ids = _dbcontext.Category_Function(catalogId, category_id);
                                    if (category_ids.Any())
                                    {
                                        foreach (var item in category_ids)
                                        {
                                            category_id = item.CATEGORY_ID;
                                            var family_ids = _dbcontext.TB_CATALOG_FAMILY.Where(a => a.CATALOG_ID == catalogId && a.CATEGORY_ID == category_id);
                                            if (family_ids.Any())
                                            {
                                                foreach (var id in family_ids)
                                                {
                                                    InsertFamilyTableStruct(Convert.ToString(id.FAMILY_ID), catalogId, structure);
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                            _dbcontext.SaveChanges();

                        }
                    }
                    else
                    {

                        string[] name = items.Split(',');
                        for (int fid = 0; fid < name.Length; fid++)
                        {
                            //familyId = Convert.ToInt32(name[fid]);
                            //var FamilyStructureDS = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.FirstOrDefault(x => x.CATALOG_ID == catalogId && x.FAMILY_ID == familyId && x.IS_DEFAULT);
                            //string structure = string.Empty;
                            //if (FamilyStructureDS == null)
                            //{
                            //    string SQLString = "EXEC STP_CATALOGSTUDIO5_InsertFamilyTableStructure " + familyId + "," + catalogId + ",'','','SuperTable',1";
                            //    objcommand.Connection = objSqlConnection;
                            //    objcommand.CommandText = SQLString;
                            //    objcommand.CommandType = CommandType.Text;
                            //    objcommand.CommandTimeout = 0;
                            //    objSqlConnection.Open();
                            //    objcommand.ExecuteNonQuery();
                            //    objSqlConnection.Close();

                            //}
                            if (Convert.ToString(name[fid]).Contains('~'))
                                InsertFamilyTableStruct(Convert.ToString(name[fid]).Trim('~'), catalogId, "");
                            else
                            {
                                string category_id = Convert.ToString(name[fid]);
                                var family_ids = _dbcontext.TB_CATALOG_FAMILY.Where(a => a.CATALOG_ID == catalogId && a.CATEGORY_ID == category_id);
                                if (family_ids.Any())
                                {
                                    foreach (var id in family_ids)
                                    {
                                        InsertFamilyTableStruct(Convert.ToString(id.FAMILY_ID), catalogId, "");
                                    }
                                    _dbcontext.SaveChanges();
                                }
                            }
                        }
                    }
                }
                return "Saved successfully";
            }
            catch (Exception ex)
            {
                Logger.Error("Error at WizardController:  UpdatePTHeader", ex);
                return ResponseMsg.Error;
            }
        }

        public void InsertFamilyTableStruct(string family_id, int catalogId, string structure)
        {
            SqlConnection objSqlConnection =
                      new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            SqlCommand objcommand = new SqlCommand();
            int familyId = Convert.ToInt32(family_id);
            var updatefamily = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.FirstOrDefault(x => x.CATALOG_ID == catalogId && x.FAMILY_ID == familyId && x.IS_DEFAULT);
            if (updatefamily != null && !string.IsNullOrEmpty(structure))
            {
                updatefamily.FAMILY_TABLE_STRUCTURE = structure;

            }
            else
            {
                string SQLString = "EXEC STP_CATALOGSTUDIO5_InsertFamilyTableStructure " + familyId + "," + catalogId + ",'','','SuperTable',1";
                objcommand.Connection = objSqlConnection;
                objcommand.CommandText = SQLString;
                objcommand.CommandType = CommandType.Text;
                objcommand.CommandTimeout = 0;
                objSqlConnection.Open();
                objcommand.ExecuteNonQuery();
                objSqlConnection.Close();
            }
        }
        public void CreateDefaultLayout(int catid, int famid, string structure)
        {
            try
            {
                int catalog_id = catid;
                int family_id = famid;
                string struc = structure;
                var tbFamilyTableStructure = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.FirstOrDefault(x => x.CATALOG_ID == catalog_id && x.FAMILY_ID == family_id && x.IS_DEFAULT);
                if (tbFamilyTableStructure == null)
                {
                    tbFamilyTableStructure = new TB_FAMILY_TABLE_STRUCTURE();
                    tbFamilyTableStructure.CATALOG_ID = catalog_id;
                    tbFamilyTableStructure.FAMILY_ID = family_id;
                    tbFamilyTableStructure.FAMILY_TABLE_STRUCTURE = struc;
                    tbFamilyTableStructure.IS_DEFAULT = true;
                    tbFamilyTableStructure.CREATED_USER = User.Identity.Name;
                    tbFamilyTableStructure.CREATED_DATE = DateTime.Now;
                    tbFamilyTableStructure.MODIFIED_USER = User.Identity.Name;
                    tbFamilyTableStructure.MODIFIED_DATE = DateTime.Now;
                    _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Add(tbFamilyTableStructure);
                    _dbcontext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at WizardController:  UpdatePTHeader", ex);
                //return ResponseMsg.Error;
            }

        }


        public JsonResult GetallAttributesByGroupId(int groupId, int catalogId, string packageType)
        {
            try
            {
                var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                if (customerid != null)
                {

                    if (packageType == "1")
                        packageType = "Family";
                    else if(packageType=="2")
                        packageType = "Product";

                    //var attributes =
                    //    _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                    //        .Where(a => a.tcp.ATTRIBUTE_TYPE == attributeid && a.tpf.CUSTOMER_ID == customerid.CustomerId)
                    //        .Select(s => new LS_TB_ATTRIBUTE
                    //        {
                    //            ATTRIBUTE_TYPE = s.tcp.ATTRIBUTE_TYPE,
                    //            ATTRIBUTE_ID = s.tcp.ATTRIBUTE_ID,
                    //            ATTRIBUTE_NAME = s.tcp.ATTRIBUTE_NAME
                    //        }).ToList();
                    //var catalogAttributes = attributes.Join(_dbcontext.TB_CATALOG_ATTRIBUTES, attr => attr.ATTRIBUTE_ID, tca => tca.ATTRIBUTE_ID, (attr, tca) => new { attr, tca }).Where(y => y.tca.CATALOG_ID == catalogId).Select(x => x.attr).ToList();

                    var attributeNames = (from packageDetails in _dbcontext.TB_PACKAGE_DETAILS
                                          join master in _dbcontext.TB_PACKAGE_MASTER on packageDetails.GROUP_ID equals master.GROUP_ID
                                          join catalogAttributes in _dbcontext.TB_CATALOG_ATTRIBUTES on packageDetails.ATTRIBUTE_ID equals catalogAttributes.ATTRIBUTE_ID
                                          join attributes in _dbcontext.TB_ATTRIBUTE on catalogAttributes.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                          where master.CATALOG_ID == catalogId && attributes.FLAG_RECYCLE == "A" && packageDetails.GROUP_ID == groupId && master.IS_FAMILY == packageType
                                          select new
                                          {
                                              attributes.ATTRIBUTE_ID,
                                              attributes.ATTRIBUTE_NAME,
                                              attributes.ATTRIBUTE_TYPE,
                                          }).Distinct().ToList();
                    
                  //  attributeNames = attributeNames.OrderBy(y => y.SORT_ORDER).ToList();

                    return Json(attributeNames, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("Error at WizardController:  GetallAttributesByGroupId", ex);
                return null;
            }
        }


    }
}