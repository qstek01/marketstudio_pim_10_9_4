﻿using System.Data.Common;
using System.Web;
using System.Web.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using log4net;
using LS.Data;
using LS.Data.Model;
using LS.Data.Model.Accounts;
using LS.Data.Utilities;
using System.Web.Security;
using Kendo.DynamicLinq;
using LS.Web.Common;
using Newtonsoft.Json.Linq;
using Stimulsoft.Controls;
using System.Web.Mvc.Html;
using System.IO;
using System.Configuration;
namespace LS.Web.Controllers
{
    public class AccountApiController : ApiController
    {
        private readonly CSEntities _dbcontext = new CSEntities();
        static ILog _logger = null;
        UserAccountsRepository userAccRepo = null;

        public AccountApiController()
        {
            _logger = LogManager.GetLogger(typeof(AccountApiController));
            userAccRepo = new UserAccountsRepository();
        }

        [HttpGet]
        public HttpResponseMessage GetCurretUserInfo()
        {
            try
            {
                string[] roles = Roles.GetRolesForUser(User.Identity.Name);
                bool isAdmin = false;
                foreach (var item in roles)
                {
                    if (item.Equals("Admin"))
                    {
                        isAdmin = true;
                    }
                }
                var currentUserInfo = new
                {
                    Username = User.Identity.Name,
                    IsAdmin = isAdmin,
                    Roles = string.Join(",", roles),
                    CustomerDetails = GetCustomerById()
                };






                return Request.CreateResponse(HttpStatusCode.OK, currentUserInfo);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        public CustomerModel GetCustomerById()
        {
            try
            {
                using (var CSEntities = new CSEntities())
                {

                    MembershipUser user = Membership.GetUser(User.Identity.Name);
                    Customer_User customerUser = CSEntities.Customer_User.FirstOrDefault(x => x.User_Name.ToLower() == user.UserName.ToLower());
                    // CustomerUserModel customerUserModel = CustomerUserModel.GetModel(customerUser);
                    Customer customer =
                        CSEntities.Customers.FirstOrDefault(x => x.CustomerId == customerUser.CustomerId);
                    // return CustomerModel.GetModel(customer);
                    if (customer != null)
                    {
                        var customerSettings = customer.Customer_Settings.FirstOrDefault();
                        var model = new CustomerModel
                        {
                            CustomerId = customer.CustomerId,
                            CustomerName = customer.CustomerName,
                            PlanId = (customer.PlanId != null) ? (int)customer.PlanId : 0,
                            CompanyName = customer.CompanyName.Replace(" ", "").Replace("_", ""),
                            ContactName = customer.CompanyName,
                            ContactTitle = customer.ContactTitle,
                            Address1 = customer.Address1,
                            Address2 = customer.Address2,
                            City = customer.City,
                            StateProvince = customer.StateProvince,
                            Zip = customer.Zip,
                            Country = customer.Country,
                            Phone = customer.Phone,
                            Fax = customer.Fax,
                            EMail = customer.EMail,
                            Comments = customer.Comments,
                            WebSite = customer.WebSite,
                            ActivationDate = customer.ActivationDate,
                            IsActive = customer.IsActive,
                            IsApproved = customer.IsApproved,
                            DateUpdated = customer.DateCreated,
                            DateCreated = customer.DateUpdated,
                            AllowDublicatePartNo =
                                customerSettings != null && customerSettings.AllowDuplicateItem_PartNum,
                            EnableSubProduct = (bool)customerSettings.EnableSubProduct
                            
                            
                        };
                        if (model.PlanId <= 0) return model;
                        using (var dbcontext = new CSEntities())
                        {
                            model.Plan =
                                (from x in dbcontext.TB_PLAN where x.PLAN_ID == model.PlanId select x).ToList().
                                    Select(i => new PlanModel { PLAN_ID = i.PLAN_ID, PLAN_NAME = i.PLAN_NAME })
                                    .FirstOrDefault();
                        }
                        return model;
                    }
                    else
                    {
                        return null;
                    }
                    //return CustomerModel.GetModel(CSEntities.Customers.FirstOrDefault(g => g.CustomerId == (CustomerUserModel.GetModel(CSEntities.Customer_User.FirstOrDefault(x => x.User_Name.ToLower() == (Membership.GetUser(User.Identity.Name)).UserName.ToLower()))).CustomerId));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return null;
            }
        }

        [HttpGet]
        public HttpResponseMessage GetCustomerTableDetailsByCurrentUser()
        {
            try
            {
                MembershipUser user = Membership.GetUser(User.Identity.Name);
                ProfileTable_1 entity = UserAccountsRepository.DC.ProfileTable_1.FirstOrDefault(x => x.UserId == (Guid)user.ProviderUserKey);

                return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        public DataSourceResult GetAppUsers(DataSourceRequest request)
        {
            try
            {
                DataSourceResult response = userAccRepo.GetAppUsers(request);
                return response;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at GetAppUsers", ex);
                return null;
            }
        }


        //[System.Web.Http.Authorize(Roles = "Admin")]
        //[AuthorizeEnumAttribute("Admin", "AdminNormalUser")]
        [HttpGet]
        public HttpResponseMessage GetAllRoles()
        {
            try
            {
                string[] roles = Roles.GetRolesForUser(User.Identity.Name);
                bool isAdmin = false;
                foreach (var item in roles.Where(item => item.Equals("Admin")))
                {
                    isAdmin = true;
                }
                var response = new List<RoleModel>();
                foreach (var item in Roles.GetAllRoles())
                {
                    if (isAdmin)
                    {
                        response.Add(new RoleModel { RoleName = item });
                    }
                    else
                    {
                        if (item.Contains("Customer"))
                        {
                            response.Add(new RoleModel { RoleName = item });
                        }
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at GetAllRoles", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        // [Authorize(Roles = "Admin")]
        [HttpPost]
        public HttpResponseMessage AddAppUser(UserModel model)
        {
            model.Email = model.UserName;
            try
            {
                if (ModelState.IsValid)
                {
                    // Attempt to register the user
                    MembershipCreateStatus createStatus;
                    Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, model.Active, null, out createStatus);

                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        foreach (string role in Roles.GetAllRoles())
                        {
                            if (!model.UserRoles.Contains(role) && Roles.IsUserInRole(model.UserName, role))
                            {
                                Roles.RemoveUserFromRole(model.UserName, role);
                            }
                        }

                        foreach (string role in model.UserRoles)
                        {
                            if (!Roles.IsUserInRole(model.UserName, role))
                            {
                                Roles.AddUsersToRole(new[] { model.UserName }, role);
                            }
                        }

                        //add profile information
                        var profile = ProfileModel.GetProfile(model.UserName);
                        profile.CustomerId = model.CustomerId;
                        profile.FirstName = model.FirstName;
                        profile.LastName = model.LastName;
                        profile.Title = model.Title;
                        profile.Address1 = model.Address1;
                        profile.Address2 = model.Address2;
                        profile.City = model.City;
                        profile.State = model.State;
                        profile.Zip = model.Zip;
                        profile.Country = model.Country;
                        profile.Phone = model.Phone;
                        profile.Fax = model.Fax;
                        profile.EMail = model.Email;
                        profile.Comments = model.Comments;
                        profile.DateUpdated = DateTime.Now;
                        profile.DateCreated = DateTime.Now;
                        profile.Save();
                        return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);
                    }
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }
                string messages = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                return Request.CreateResponse(HttpStatusCode.BadRequest, messages);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at SaveOrUpdateAppUser", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        // [Authorize(Roles = "Admin")]
        [HttpPost]
        public HttpResponseMessage UpdateAppUser(UserModel model)
        {
            model.Email = model.UserName;

            try
            {
                if (ModelState.IsValid)
                {
                    MembershipUser currentUser = Membership.GetUser(model.UserName);
                    if (currentUser != null)
                    {
                        currentUser.Email = model.Email;
                        currentUser.IsApproved = model.Active;
                        Membership.UpdateUser(currentUser);

                        foreach (string role in Roles.GetAllRoles())
                        {
                            if (!model.UserRoles.Contains(role) && Roles.IsUserInRole(currentUser.UserName, role))
                            {
                                Roles.RemoveUserFromRole(currentUser.UserName, role);
                            }
                        }

                        foreach (string role in model.UserRoles)
                        {
                            if (!Roles.IsUserInRole(currentUser.UserName, role))
                            {
                                Roles.AddUsersToRole(new[] { currentUser.UserName }, role);
                            }
                        }
                    }

                    //add profile information
                    var profile = ProfileModel.GetProfile(model.UserName);
                    profile.CustomerId = model.CustomerId;
                    profile.FirstName = model.FirstName;
                    profile.LastName = model.LastName;
                    profile.Title = model.Title;
                    profile.Address1 = model.Address1;
                    profile.Address2 = model.Address2;
                    profile.City = model.City;
                    profile.State = model.State;
                    profile.Zip = model.Zip;
                    profile.Country = model.Country;
                    profile.Phone = model.Phone;
                    profile.Fax = model.Fax;
                    profile.EMail = model.Email;
                    profile.Comments = model.Comments;
                    profile.DateUpdated = DateTime.Now;
                    profile.DateCreated = DateTime.Now;
                    profile.Save();

                    return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
                }
                string messages = string.Join("; ", ModelState.Values
                                         .SelectMany(x => x.Errors)
                                         .Select(x => x.ErrorMessage));
                return Request.CreateResponse(HttpStatusCode.BadRequest, messages);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at SaveOrUpdateAppUser", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        //  [Authorize(Roles = "Admin")]
        public string Userroles(string userName)
        {
            try
            {
                string rolesString = "";
                string[] currentRoles = Roles.GetRolesForUser(userName);
                if (currentRoles.Length > 0)
                {
                    rolesString = currentRoles.Aggregate(rolesString, (current, role) => current + ("," + role));
                    return rolesString.Substring(1);
                }
                return "";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Userroles", ex);
            }
            return null;
        }

        public static bool IsValidCustomerLogin(string userName, int customerId)
        {
            try
            {
                var profile = ProfileModel.GetProfile(userName);
                return profile != null && profile.CustomerId == customerId;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at IsValidCustomerLogin", ex);
            }
            return false;
        }

        public static int? GetCustomerId(string userName)
        {
            try
            {
                var profile = ProfileModel.GetProfile(userName);
                return profile != null && profile.CustomerId.HasValue ? profile.CustomerId : null;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at GetCustomerId", ex);
            }
            return null;
        }

        //[System.Web.Http.HttpGet]
        //public HttpResponseMessage GetCustomerById(int customerId)
        //{
        //    try
        //    {               
        //        return Request.CreateResponse(HttpStatusCode.OK, userAccRepo.GetCustomerById(customerId));
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at GetCustomerById", ex);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError);
        //    }
        //}

        //[System.Web.Http.HttpGet]
        //public HttpResponseMessage GetAllCustomers()
        //{
        //    try
        //    {
        //        List<Customer> customers = LS.Data.UserAccountsRepository.DC.Customers.ToList();
        //        List<CustomerModel> response = customers.Select(x => CustomerModel.GetModel(x)).ToList();
        //       return Request.CreateResponse(HttpStatusCode.OK, response);                
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at GetAllCustomers", ex);
        //       return Request.CreateResponse(HttpStatusCode.InternalServerError);    
        //    }
        //}

        //[System.Web.Http.HttpPost]
        //public DataSourceResult GetCustomerList(DataSourceRequest request)
        //{
        //    try
        //    {
        //        if (request.Sort == null || request.Sort.Count() == 0)
        //        {
        //            request.Sort = new List<Sort>() { new Sort() { Field = "CustomerId", Dir = "asc" } };
        //        }

        //        DataSourceResult result = (from x in LS.Data.UserAccountsRepository.DC.Customers
        //                                   select x).ToDataSourceResult<Customer>(request);
        //        result.Data = ((List<Customer>)result.Data).Select(x => CustomerModel.GetModel(x));
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at GetCustomerList", ex);
        //    }
        //    return null;
        //}

        public HttpResponseMessage ChangePassword(ChangePasswordModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool changePasswordSucceeded = false;
                    try
                    {
                        MembershipUser currentUser = Membership.GetUser(User.Identity.Name);
                        if (currentUser != null)
                        {
                            //string oldPassword = currentUser.ResetPassword();
                            changePasswordSucceeded = currentUser.ChangePassword(model.OldPassword, model.NewPassword);
                            currentUser.UnlockUser();
                        }

                        if (changePasswordSucceeded)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
                        }
                        //return Request.CreateResponse(HttpStatusCode.BadRequest);
                        ModelState.AddModelError("", "Old password is incorrect");
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }
                }
                string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
                return Request.CreateResponse(HttpStatusCode.BadRequest, messages);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ChangePassword", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }



        //// Customer Detail - Locations - Starts
        //[System.Web.Http.HttpPost]
        //public DataSourceResult GetLocationsAddressList(DataSourceRequest request)
        //{
        //    var result = new DataSourceResult();
        //    try
        //    {
        //        if (request.Sort == null || request.Sort.Count() == 0)
        //        {
        //            request.Sort = new List<Sort>() { new Sort() { Field = "Id", Dir = "asc" } };
        //        }

        //        result = (from x in UserAccountsRepository.DC.Addresses
        //                  select x).ToDataSourceResult<Address>(request);
        //        result.Data = ((List<Address>)result.Data).Select(x => AddressModel.GetModel(x));
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at GetLocationsAddressList", ex);
        //    }
        //    return result;
        //}

        //[System.Web.Http.HttpPost]
        //public HttpResponseMessage AddOrUpdateLocationAddress(AddressModel address)
        //{
        //    try
        //    {
        //        if (address.Id > 0)
        //        {
        //            Address existingDefaultAddreess = null;
        //            if (address.IsDefault == true)
        //            {
        //                existingDefaultAddreess = UserAccountsRepository.DC.Addresses.AsNoTracking().Where(x => x.CustomerId == address.CustomerId && x.IsDefault == true).FirstOrDefault();
        //                if (existingDefaultAddreess != null && existingDefaultAddreess.Id != address.Id)
        //                {
        //                    existingDefaultAddreess.IsDefault = false;
        //                    UserAccountsRepository.DC.Entry(existingDefaultAddreess).State = System.Data.Entity.EntityState.Modified;                            
        //                    UserAccountsRepository.DC.Entry(AddressModel.GetEntity(address)).State = System.Data.Entity.EntityState.Modified;
        //                }
        //                else
        //                {
        //                    existingDefaultAddreess = AddressModel.GetEntity(address);
        //                    UserAccountsRepository.DC.Entry(existingDefaultAddreess).State = System.Data.Entity.EntityState.Modified;
        //                }
        //            }
        //            else
        //            {
        //                UserAccountsRepository.DC.Entry(AddressModel.GetEntity(address)).State = System.Data.Entity.EntityState.Modified;
        //            }
        //            UserAccountsRepository.DC.SaveChanges();
        //            return Request.CreateResponse(HttpStatusCode.OK, "Updated Succuessfully");
        //        }
        //        else
        //        {
        //            address.DateCreated = DateTime.Now;
        //            address.DateUpdated = DateTime.Now;
        //            UserAccountsRepository.DC.Addresses.Add(AddressModel.GetEntity(address));
        //            UserAccountsRepository.DC.SaveChanges();
        //            return Request.CreateResponse(HttpStatusCode.OK, "Added Succuessfully");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at AddOrUpdateLocationAddress", ex);
        //        return Request.CreateResponse(HttpStatusCode.BadRequest);
        //    }
        //}

        [HttpPost]
        public bool CheckCustomerHasAddressType(int customerId, string addressType)
        {
            try
            {
                if (customerId > 0 && !string.IsNullOrEmpty(addressType))
                {
                    //return UserAccountsRepository.DC.Addresses.Any(x => x.AddressType == addressType && x.CustomerId == customerId);
                    return false;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AddOrUpdateLocationAddress", ex);
                return false;
            }
            return false;
        }

        //[System.Web.Http.HttpDelete]
        //public HttpResponseMessage DeleteLocationAddress(int id)
        //{
        //    try
        //    {
        //        Address delete = UserAccountsRepository.DC.Addresses.FirstOrDefault(x => x.Id == id);
        //        if (delete != null)
        //        {
        //            UserAccountsRepository.DC.Addresses.Remove(delete);
        //            UserAccountsRepository.DC.SaveChanges();
        //            return Request.CreateResponse(HttpStatusCode.OK, "Deleted Succuessfully");
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK, "Not Found");
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at DeleteLocationAddress", ex);
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
        //    }
        //}


        /// <summary>
        /// Gets List of Customers
        /// </summary>
        /// <param name="request">request object from Kendo Grid</param>
        /// <returns>DataSourceResult to Kendo Grid</returns>
        [HttpPost]
        public DataSourceResult GetCustomerList(DataSourceRequest request)
        {
            try
            {
                _logger.Info("Inside  at  AccountApiController: GetCustomerList");
                if (request.Sort == null || !request.Sort.Any())
                {
                    request.Sort = new List<Sort> { new Sort { Field = "CustomerId", Dir = "asc" } };
                }
                using (var dbcontext = new CSEntities())
                {
                    //var customersettings = dbcontext.Customers.Join(dbcontext.Customer_Settings, tps => tps.CustomerId,
                    //    tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf }).Select(x => new CustomerModel
                    //                                                           {
                    //                                                               CustomerId = x.tps.CustomerId,
                    //                                                               CustomerName = x.tps.CustomerName,
                    //                                                               PlanId = (x.tps.PlanId != null) ? (int)x.tps.PlanId : 0,
                    //                                                               CompanyName = x.tps.CompanyName,
                    //                                                               ContactName = x.tps.ContactName,
                    //                                                               ContactTitle = x.tps.ContactTitle,
                    //                                                               Address1 = x.tps.Address1,
                    //                                                               Address2 = x.tps.Address2,
                    //                                                               City = x.tps.City,
                    //                                                               StateProvince = x.tps.StateProvince,
                    //                                                               Zip = x.tps.Zip,
                    //                                                               Country = x.tps.Country,
                    //                                                               Phone = x.tps.Phone,
                    //                                                               Fax = x.tps.Fax,
                    //                                                               EMail = x.tps.EMail,
                    //                                                               Comments = x.tps.Comments,
                    //                                                               WebSite = x.tps.WebSite,
                    //                                                               ActivationDate = x.tps.ActivationDate,
                    //                                                               IsActive = x.tps.IsActive,
                    //                                                               IsApproved = x.tps.IsApproved,
                    //                                                               DateUpdated = x.tps.DateCreated,
                    //                                                               DateCreated = x.tps.DateUpdated,
                    //                                                               AllowDublicatePartNo = x.tpf.AllowDuplicateItem_PartNum,
                    //                                                               additionalUsers = 0,
                    //                                                               additionalSKUs = 0,
                    //                                                               additionalStorage = 0,

                    //                                                               Plan = (from y in dbcontext.TB_PLAN where y.PLAN_ID == x.tps.PlanId select y).ToList()
                    //                                                               .Select(i => new PlanModel { PLAN_ID = i.PLAN_ID, PLAN_NAME = i.PLAN_NAME }).FirstOrDefault()
                    //                                                           }).ToDataSourceResult(request);


                    var customersettings = dbcontext.Customers.Join(dbcontext.TB_COUNTRY, cus => cus.Country, cty => cty.COUNTRY_CODE, (cus, cty) => new { customer = cus, country = cty })
                                                              .Join(dbcontext.TB_STATE, tpc => tpc.customer.StateProvince, tpsp => tpsp.STATE_CODE, (tpc, tpsp) => new {tpc.customer,tpc.country,state=tpsp })
                                                              .Join(dbcontext.Customer_Settings, tps => tps.customer.CustomerId, tpf => tpf.CustomerId, (tps, tpf) => new { tps.customer, tps.country,tps.state, customersetting = tpf })
                       .Select(x => new CustomerModel
                       {
                           CustomerId = x.customer.CustomerId,
                           CustomerName = x.customer.CustomerName,
                           PlanId = (x.customer.PlanId != null) ? (int)x.customer.PlanId : 0,
                           CompanyName = x.customer.CompanyName,
                           ContactName = x.customer.ContactName,
                           ContactTitle = x.customer.ContactTitle,
                           Address1 = x.customer.Address1,
                           Address2 = x.customer.Address2,
                           City = x.customer.City,
                           STATE_CODE=x.customer.StateProvince,
                           StateProvince =x.state.STATE ,
                           Zip = x.customer.Zip,
                           COUNTRY_CODE = x.customer.Country,
                           Country = x.country.COUNTRY,
                           Phone = x.customer.Phone,
                           Fax = x.customer.Fax,
                           EMail = x.customer.EMail,
                           Comments = x.customer.Comments,
                           WebSite = x.customer.WebSite,
                           ActivationDate = x.customer.ActivationDate,
                           IsActive = x.customer.IsActive,
                           IsApproved = x.customer.IsApproved,
                           DateUpdated = x.customer.DateCreated,
                           DateCreated = x.customer.DateUpdated,
                           AllowDublicatePartNo = x.customersetting.AllowDuplicateItem_PartNum,
                           additionalUsers = 0,
                           additionalSKUs = 0,
                           additionalStorage = 0,
                           EnableSubProduct = (bool)x.customersetting.EnableSubProduct,

                           Plan = (from y in dbcontext.TB_PLAN where y.PLAN_ID == x.customer.PlanId select y).ToList()
                           .Select(i => new PlanModel { PLAN_ID = i.PLAN_ID, PLAN_NAME = i.PLAN_NAME }).FirstOrDefault()
                       });
                    return customersettings.ToDataSourceResult(request);

                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error at AccountApiController: GetCustomerList", ex);
            }
            return null;
        }



        [HttpPost]
        public HttpResponseMessage UpdateCatalog(int customerId, JArray model)
        {
            try
            {
                _logger.Info("Inside AccountApiController : UpdateOrSaveCustomer");
                using (var dbcontext = new CSEntities())
                {

                    if (customerId > 0)
                    {
                        //var objCustmer = dbcontext.Customer_Catalog.FirstOrDefault(x => x.CustomerId == customerId);
                        var functionAlloweditems = ((JArray)model).Select(x => new
                        {
                            CATALOG_ID = (int)x["CATALOG_ID"],
                            Active = (bool)x["ACTIVE"]
                        }).ToList();
                        foreach (var cucatalog in functionAlloweditems)
                        {
                            var availcatalog =
                                dbcontext.Customer_Catalog.Where(
                                    x => x.CATALOG_ID == cucatalog.CATALOG_ID && x.CustomerId == customerId);
                            if (availcatalog.Any())
                            {
                                var customercatalog = availcatalog.FirstOrDefault();
                                if (!cucatalog.Active)
                                {
                                    dbcontext.Customer_Catalog.Remove(customercatalog);

                                    var rolecatalog = dbcontext.Customer_Role_Catalog.Where(x => x.CATALOG_ID == cucatalog.CATALOG_ID && x.CustomerId == customerId);
                                    if (rolecatalog.Any())
                                    {
                                        var rolescatalog = rolecatalog.FirstOrDefault();
                                        if (!cucatalog.Active)
                                        {
                                            dbcontext.Customer_Role_Catalog.Remove(rolescatalog);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (cucatalog.Active)
                                {
                                    var customercatalog = new Customer_Catalog
                                    {
                                        CustomerId = customerId,
                                        CATALOG_ID = cucatalog.CATALOG_ID,
                                        DateCreated = DateTime.Now,
                                        DateUpdated = DateTime.Now
                                    };
                                    dbcontext.Customer_Catalog.Add(customercatalog);

                                    var rolecatalog = dbcontext.Customer_Role_Catalog.Where(x => x.CATALOG_ID == cucatalog.CATALOG_ID && x.CustomerId == customerId);
                                    if (!rolecatalog.Any())
                                    {
                                        var roleid = dbcontext.aspnet_Roles.Where(x => x.CustomerId == customerId).FirstOrDefault();
                                        if (roleid != null)
                                        {
                                            var rolescatalog = new Customer_Role_Catalog
                                            {
                                                CustomerId = customerId,
                                                CATALOG_ID = cucatalog.CATALOG_ID,
                                                DateCreated = DateTime.Now,
                                                DateUpdated = DateTime.Now,
                                                IsActive = true,
                                                RoleId = roleid.Role_id
                                            };
                                            dbcontext.Customer_Role_Catalog.Add(rolescatalog);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "Action completed ");
                }
            }
            catch (Exception exp)
            {
                _logger.Error("Error at AccountApiController: updateCatalog", exp);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Please reload the page and continue.");
            }

        }

        [HttpGet]
        public HttpResponseMessage customerValidation(string Comments, int CustomerId)
        {
            var dbcontext = new CSEntities();
            var objExistImageFolderName =0 ;
            if (CustomerId == 0 &&string.IsNullOrEmpty(Comments))
            {
                objExistImageFolderName = dbcontext.Customers.Count(x => x.Comments == Comments);
               

            }

            else if (CustomerId == 0)
            {
                objExistImageFolderName = dbcontext.Customers.Count(x => x.Comments == Comments);
            }

            else if (CustomerId > 0 && string.IsNullOrEmpty(Comments))
            {
                objExistImageFolderName = 0;
            }

            else if (CustomerId > 0)
            {
                var objExistingImageFolderNameList= dbcontext.Customers.Where(x => x.CustomerId == CustomerId && x.Comments == Comments).ToList();
                if (objExistingImageFolderNameList.Any())
                {
                    objExistImageFolderName = 1;
                }
                else if (!objExistingImageFolderNameList.Any())
                {
                    var objExistImageFolder = dbcontext.Customers.Count(x => x.Comments == Comments);
                    if (objExistImageFolder > 0)
                    {
                        objExistImageFolderName = 2;
                    }
                    else
                    {
                        objExistImageFolderName = 0;
                    }
                    
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, objExistImageFolderName);
        }


        [HttpPost]
        public HttpResponseMessage UpdateOrSaveCustomer(CustomerModel customer)
        {
            try
            {
                int roleIdreturn = 0;
                string customer_name = "";
                int customer_id = 0;
                string roleId = "";
                bool Activecheck = false;
                _logger.Info("Inside AccountApiController : UpdateOrSaveCustomer");
                using (var dbcontext = new CSEntities())
                {
                    Customer objCustmer;
                    TB_STORE_SESSION objStore;
                    TB_PLAN objPlan;
                    if (customer.CustomerId > 0)
                    {
                        customer_id = customer.CustomerId;
                        objCustmer = dbcontext.Customers.FirstOrDefault(x => x.CustomerId == customer.CustomerId);
                        if (objCustmer != null)
                        {
                            Activecheck = objCustmer.IsActive;
                          
                            if (!string.IsNullOrEmpty(objCustmer.Comments))
                            {
                                if (
                                    Directory.Exists(
                                        HttpContext.Current.Server.MapPath("~/Content/ProductImages/" +
                                                                           objCustmer.Comments)))
                                {
                                    string sourceDirectory =
                                        HttpContext.Current.Server.MapPath("~/Content/ProductImages/" +
                                                                           objCustmer.Comments);
                                    string destinationDirectory =
                                        HttpContext.Current.Server.MapPath("~/Content/ProductImages/" +
                                                                           customer.Comments);

                                    try
                                    {
                                        Directory.Move(sourceDirectory, destinationDirectory);
                                    }
                                    catch (Exception e)
                                    {
                                        // Console.WriteLine(e.Message);
                                    }
                                }
                            }

                            //DateTime dt = new DateTime(2017, 11, 07);
                            //string format= String.Format("{0:dd-MMM-yyyy}", customer.ActivationDate.Value);
                            //DateTime dt1 = DateTime.Parse(format, System.Globalization.CultureInfo.InvariantCulture);
                            //DateTime date= Convert.ToDateTime(DateTime.Parse(format).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                            objCustmer.DateUpdated = DateTime.Now;
                            objCustmer.DateCreated = customer.DateCreated;
                            objCustmer.IsApproved = customer.IsApproved;
                            objCustmer.IsActive = customer.IsActive;
                            objCustmer.ActivationDate = customer.IsActive ? customer.ActivationDate : null;
                            objCustmer.CustomerName = customer.CustomerName;
                            //  objCustmer.CompanyName = customer.CompanyName;
                            objCustmer.Address1 = customer.Address1;
                            objCustmer.Address2 = customer.Address2;
                            objCustmer.City = customer.City;
                            objCustmer.Comments = customer.Comments;
                            objCustmer.ContactName = customer.ContactName;
                            objCustmer.ContactTitle = customer.ContactTitle;
                            //objCustmer.Country = customer.Country;
                            objCustmer.Country = customer.COUNTRY_CODE;
                            objCustmer.EMail = customer.EMail;
                            objCustmer.Fax = customer.Fax;
                            objCustmer.Phone = customer.Phone;
                            objCustmer.StateProvince = customer.STATE_CODE;
                            objCustmer.WebSite = customer.WebSite;
                            objCustmer.PlanId = customer.PlanId;
                            objCustmer.Zip = customer.Zip;
                        }

                        objStore = dbcontext.TB_STORE_SESSION.FirstOrDefault(x => x.ID == customer.CustomerId);
                        objPlan = dbcontext.TB_PLAN.FirstOrDefault(x => x.PLAN_ID == customer.PlanId);
                    
                        string format = String.Format("{0:dd-MMM-yyyy}", customer.ActivationDate.Value);
                        if (objStore != null && objPlan != null)
                        {
                            objStore.COL4 = LS.Data.Utilities.CommonUtil.Encrypt(format);
                            objStore.PID = customer.PlanId;
                            objStore.COL1 = LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(objPlan.SKU_COUNT));
                            objStore.COL2 = LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(objPlan.NO_OF_USERS));
                            objStore.COL3 = LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(objPlan.SUBSCRIPTION_IN_MONTHS));
                        }
                        var customerAttributeOne =
                            dbcontext.CUSTOMERATTRIBUTE.Where(x => x.ATTRIBUTE_ID == 1 && x.CUSTOMER_ID == customer_id)
                                .ToList();
                        if (!customerAttributeOne.Any())
                        {
                            var customerAttribute = new CUSTOMERATTRIBUTE
                            {
                                ATTRIBUTE_ID = 1,
                                CUSTOMER_ID = customer_id,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now
                            };
                            dbcontext.CUSTOMERATTRIBUTE.Add(customerAttribute);
                            dbcontext.SaveChanges();
                        }
                        var customerAttributeThree =
                           dbcontext.CUSTOMERATTRIBUTE.Where(x => x.ATTRIBUTE_ID == 3 && x.CUSTOMER_ID == customer_id)
                               .ToList();
                        if (!customerAttributeThree.Any())
                        {
                            var customerAttribute = new CUSTOMERATTRIBUTE
                            {
                                ATTRIBUTE_ID = 3,
                                CUSTOMER_ID = customer_id,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now
                            };
                            dbcontext.CUSTOMERATTRIBUTE.Add(customerAttribute);
                            dbcontext.SaveChanges();
                        }
                        if (Activecheck != customer.IsActive)
                        {
                            if(customer.IsActive)
                            {
                                if(!string.IsNullOrEmpty(customer.EMail))
                                {
                                const string subject = "Account activation.";
                                var body = " Thanks for registering CatalogStudio Account,";
                                body += "You account was activated successfully";
                                body += "<br/><br/><b>Regards,</b>";
                                body += "<br/><br/> CatalogStudio Team";
                                CommonUtil.SendEmailForCompanyandUserRegister(subject, customer.EMail, body, "Account activation");
                                }
                            }
                        }
                    }
                    else
                    {
                        objCustmer = new Customer { DateCreated = DateTime.Now, DateUpdated = DateTime.Now };
                        dbcontext.Customers.Add(objCustmer);
                            objCustmer.CustomerName = customer.CustomerName;
                            objCustmer.CompanyName = customer.CompanyName;
                            objCustmer.DateUpdated = DateTime.Now;
                            objCustmer.IsActive = customer.IsActive;
                            objCustmer.ActivationDate = customer.IsActive ? customer.ActivationDate : null;
                            objCustmer.Address1 = customer.Address1;
                            objCustmer.Address2 = customer.Address2;
                            objCustmer.City = customer.City;
                            objCustmer.Comments = customer.Comments;
                            objCustmer.ContactName = customer.ContactName;
                            objCustmer.ContactTitle = customer.ContactTitle;
                            //objCustmer.Country = customer.Country;
                            objCustmer.Country = customer.COUNTRY_CODE;
                            objCustmer.EMail = customer.EMail;
                            objCustmer.CustomerId = customer.CustomerId;
                            objCustmer.Fax = customer.Fax;
                            objCustmer.Phone = customer.Phone;
                            objCustmer.StateProvince = customer.StateProvince;
                            objCustmer.WebSite = customer.WebSite;
                            objCustmer.PlanId = customer.PlanId;
                            objCustmer.Zip = customer.Zip;
                            objCustmer.IsApproved = customer.IsApproved;
                            dbcontext.SaveChanges();


                        int custid = dbcontext.Customers.Max(x => x.CustomerId);
                        //custid = custid + 1;
                        //  TB_PLAN objPlan;
                        customer.CustomerId = custid;
                        objStore = new TB_STORE_SESSION { ID = custid };
                        dbcontext.TB_STORE_SESSION.Add(objStore);
                        if (objStore != null)
                        {

                            objStore.COL4 = CommonUtil.Encrypt(Convert.ToString(customer.ActivationDate));
                            objStore.PID = customer.PlanId;
                            objPlan = dbcontext.TB_PLAN.FirstOrDefault(x => x.PLAN_ID == customer.PlanId);
                            if (objPlan != null)
                            {
                                objStore.COL1 = CommonUtil.Encrypt(Convert.ToString(objPlan.SKU_COUNT));
                                objStore.COL2 = CommonUtil.Encrypt(Convert.ToString(objPlan.NO_OF_USERS));
                                objStore.COL3 = CommonUtil.Encrypt(Convert.ToString(objPlan.SUBSCRIPTION_IN_MONTHS));
                            }
                        }

                        var objCustomerSettings = new Customer_Settings
                        {
                            CustomerId = custid,
                            DefaultAppStyle = "Black",
                            DisplayIDColumns = false,
                            ErrorLogFilePath = "C:",
                            Temporary_File_Path = "C:",
                            AllowDuplicateItem_PartNum = false,
                            TechSupportURL = "C:",
                            SpellCheckerMode = "None",
                            WebcatSyncURL = "C:",
                            WhiteboardWebserviceURL = "C:",
                            ShowCustomAPPS = true,
                            EnableWorkFlow = false,
                            ProductLevelMultitablePreview = true,
                            FamilyLevelMultitablePreview = true,
                            SKUAlertPercentage = 90,
                            AssetLimitPercentage = 90,
                            DisplaySkuProductCountInAlert = true,
                            Export_ImportPath = "C:",
                            ImagePath = "C:",
                            ImageConversionUtilityPath = @"C:\Program Files (x86)\ImageMagick-6.8.8-Q8",
                            WebCatImagePath = "C:",
                            ReferenceTableImagePath = "C:",
                            ImageConversionSize1Width = 0,
                            ImageConversionSize1Height = 0,
                            ImageConversionSize1Folder = "",
                            ImageConversionSize2Width = 0,
                            ImageConversionSize2Height = 0,
                            ImageConversionSize2Folder = "",
                            ImageConversionSize3Width = 0,
                            ImageConversionSize3Height = 0,
                            ImageConversionSize3Folder = "",
                            ImageConversionSize4Width = 0,
                            ImageConversionSize4Height = 0,
                            ImageConversionSize4Folder = "",
                            ImageConversionSize5Width = 0,
                            ImageConversionSize5Height = 0,
                            ImageConversionSize5Folder = "",
                            ImageConversionSize6Width = 0,
                            ImageConversionSize6Height = 0,
                            ImageConversionSize6Folder = "",
                            DisplayCategoryIdinNavigator = false,
                            DisplayFamilyandRelatedFamily = true,
                            UploadImagesPDFForWebSync = false,
                            CreateXMLForSelectedAttributesToAllFamily = false,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now,
                            bannerpath = "top_bg.jpg",
                            logopath = null,
                            Theme = "blueopal"
                        };
                        dbcontext.Customer_Settings.Add(objCustomerSettings);
                        dbcontext.SaveChanges();


                        customer_name = customer.CompanyName.Replace(" ", "").Replace("_", "");
                        customer_id = custid;
                        var customerAttributeOne =
                           dbcontext.CUSTOMERATTRIBUTE.Where(x => x.ATTRIBUTE_ID == 1 && x.CUSTOMER_ID == customer_id)
                               .ToList();
                        if (!customerAttributeOne.Any())
                        {
                            var customerAttribute = new CUSTOMERATTRIBUTE
                            {
                                ATTRIBUTE_ID = 1,
                                CUSTOMER_ID = customer_id,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now
                            };
                            dbcontext.CUSTOMERATTRIBUTE.Add(customerAttribute);
                            dbcontext.SaveChanges();
                        }
                        var customerAttributeThree =
                           dbcontext.CUSTOMERATTRIBUTE.Where(x => x.ATTRIBUTE_ID == 3 && x.CUSTOMER_ID == customer_id)
                               .ToList();
                        if (!customerAttributeThree.Any())
                        {
                            var customerAttribute = new CUSTOMERATTRIBUTE
                            {
                                ATTRIBUTE_ID = 3,
                                CUSTOMER_ID = customer_id,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now
                            };
                            dbcontext.CUSTOMERATTRIBUTE.Add(customerAttribute);
                            dbcontext.SaveChanges();
                        }
                        roleId = customer_name + "_" + "Admin";
                        var tbroleCheck = dbcontext.TB_ROLE.Where(a => a.ROLE_NAME == roleId);
                        if (!tbroleCheck.Any())
                        {
                            var result = dbcontext.STP_CATALOGSTUDIO5_AddNewRole(roleId);
                            dbcontext.SaveChanges();
                            if (result != null)
                            {
                                int rid = Convert.ToInt32(result);
                                var roleResult = dbcontext.TB_ROLE.FirstOrDefault(a => a.ROLE_NAME == roleId);
                                if (roleResult != null)
                                {
                                    UserRolesList role = new UserRolesList();
                                    var aspnetrolescheck = dbcontext.aspnet_Roles.Where(x => x.Role_id == roleResult.ROLE_ID);
                                    if (!aspnetrolescheck.Any())
                                    {
                                        //var objAspnetRoles = new aspnet_Roles
                                        //{
                                        //    RoleName = roleResult.ROLE_NAME,
                                        //    Role_id = roleResult.ROLE_ID
                                        //};
                                        dbcontext.aspnet_Roles_CreateRole("/", roleId);
                                        var aspnetrolescheck1 = dbcontext.aspnet_Roles.FirstOrDefault(x => x.RoleName == roleId);
                                        if (aspnetrolescheck1 != null)
                                        {
                                            roleIdreturn = roleResult.ROLE_ID;
                                            aspnetrolescheck1.Role_id = roleResult.ROLE_ID;
                                            aspnetrolescheck1.RoleType = 2;
                                            aspnetrolescheck1.CustomerId = customer_id;

                                            var objCustomercatalogs = dbcontext.Customer_Catalog.Where(x => x.CustomerId == customer_id);
                                            if (objCustomercatalogs.Any())
                                            {
                                                foreach (var objCustomercatalog in objCustomercatalogs)
                                                {
                                                    var rolecatalog = dbcontext.Customer_Role_Catalog.Where(x => x.CATALOG_ID == objCustomercatalog.CATALOG_ID && x.RoleId == roleIdreturn && x.CustomerId == customer_id);
                                                    if (!rolecatalog.Any())
                                                    {
                                                        var objcustomerrolecatalog = new Customer_Role_Catalog
                                                        {
                                                            RoleId = roleIdreturn,
                                                            CustomerId = customer_id,
                                                            CATALOG_ID = objCustomercatalog.CATALOG_ID,
                                                            DateCreated = DateTime.Now,
                                                            DateUpdated = DateTime.Now
                                                        };
                                                        dbcontext.Customer_Role_Catalog.Add(objcustomerrolecatalog);
                                                    }
                                                }
                                            }

                                            dbcontext.SaveChanges();
                                        }
                                    }
                                }
                            }
                            dbcontext.SaveChanges();
                        }
                    }
                    dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, customer);
                }
            }
            catch (Exception exp)
            {
                _logger.Error("Error at AccountApiController: UpdateOrSaveCustomer", exp);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Please reload the page and continue.");
            }

        }

        [HttpPost]
        public HttpResponseMessage UpdateOrSavePlan(PlanModel plan)
        {
            try
            {
                _logger.Info("Inside AccountApiController : UpdateOrSavePlan");
                int planid = 0;
                using (var dbcontext = new CSEntities())
                {
                    TB_PLAN objPlan;
                    if (plan.PLAN_ID > 0)
                    {
                        planid = plan.PLAN_ID;
                        objPlan = dbcontext.TB_PLAN.FirstOrDefault(x => x.PLAN_ID == planid);
                        if (objPlan != null)
                        {
                            objPlan.MODIFIED_DATE = DateTime.Now;
                            objPlan.MODIFIED_USER = User.Identity.Name;
                            objPlan.PLAN_NAME = plan.PLAN_NAME;
                            objPlan.IS_ACTIVE = plan.IS_ACTIVE;
                            objPlan.NO_OF_USERS = plan.NO_OF_USERS;
                            objPlan.Price_In_Dollars = plan.Price_In_Dollars;
                            objPlan.SKU_COUNT = plan.SKU_COUNT;
                            objPlan.SUBSCRIPTION_IN_MONTHS = plan.SUBSCRIPTION_IN_MONTHS;
                            objPlan.StorageGB = plan.StorageGB;
                            objPlan.CustomPDFCatalogTemplates = plan.CustomPDFCatalogTemplates;
                            //dbcontext.TB_PLAN.Add(objPlan1);
                            dbcontext.SaveChanges();

                        }
                        foreach (
                            var planStoreList in dbcontext.TB_STORE_SESSION.Where(x => x.PID == planid).ToList())
                        {
                            planStoreList.COL1 = LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(plan.SKU_COUNT));
                            planStoreList.COL2 = LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(plan.NO_OF_USERS));
                            planStoreList.COL3 =
                                LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(plan.SUBSCRIPTION_IN_MONTHS));
                        }
                        dbcontext.SaveChanges();
                    }
                    else
                    {
                        var planavail = dbcontext.TB_PLAN.Where(x => x.PLAN_NAME == plan.PLAN_NAME);
                        if (!planavail.Any())
                        {
                            objPlan = new TB_PLAN
                            {
                                PLAN_NAME = plan.PLAN_NAME,
                                SKU_COUNT = plan.SKU_COUNT,
                                NO_OF_USERS = plan.NO_OF_USERS,
                                SUBSCRIPTION_IN_MONTHS = plan.SUBSCRIPTION_IN_MONTHS,
                                IS_ACTIVE = plan.IS_ACTIVE,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                Price_In_Dollars = plan.Price_In_Dollars,
                                StorageGB = plan.StorageGB,
                                CustomPDFCatalogTemplates = plan.CustomPDFCatalogTemplates,
                            };
                            dbcontext.TB_PLAN.Add(objPlan);
                            dbcontext.SaveChanges();




                            var firstOrDefault = dbcontext.TB_PLAN.FirstOrDefault(x => x.PLAN_NAME == plan.PLAN_NAME);
                            if (firstOrDefault != null)
                            {
                                planid = firstOrDefault.PLAN_ID;

                                if (planid > 0)
                                {

                                    foreach (
                                        var functionGroupModels in
                                            plan.FunctionGroupModel)
                                    {
                                        var objPlanFunctions = dbcontext.TB_FUNCTION.Where(x => x.FUNCTION_GROUP_ID == functionGroupModels.FUNCTION_GROUP_ID).ToList();
                                        foreach (
                                            var functionModel in
                                               objPlanFunctions)
                                        {
                                            var planFunctionsList = new TB_PLAN_FUNCTIONS();
                                            planFunctionsList.FUNCTION_ID = functionModel.FUNCTION_ID;
                                            planFunctionsList.PLAN_ID = planid;
                                            planFunctionsList.ACTION_VIEW = functionModel.DEFAULT_ACTION_VIEW;
                                            planFunctionsList.ACTION_MODIFY = functionModel.DEFAULT_ACTION_MODIFY;
                                            planFunctionsList.ACTION_ADD = functionModel.DEFAULT_ACTION_ADD;
                                            planFunctionsList.ACTION_REMOVE = functionModel.DEFAULT_ACTION_REMOVE;
                                            planFunctionsList.FUNCTION_NAME = functionModel.FUNCTION_NAME;
                                            planFunctionsList.CREATED_USER = User.Identity.Name;
                                            planFunctionsList.CREATED_DATE = DateTime.Now;
                                            planFunctionsList.MODIFIED_USER = User.Identity.Name;
                                            planFunctionsList.MODIFIED_DATE = DateTime.Now;
                                            planFunctionsList.FUNC_ID = functionModel.FUNCTION_ID;
                                            planFunctionsList.FUNCTION_GROUP_ID = functionModel.FUNCTION_GROUP_ID;
                                            dbcontext.TB_PLAN_FUNCTIONS.Add(planFunctionsList);
                                            dbcontext.SaveChanges();
                                        }
                                    }

                                    var objPlanFunctions1 = dbcontext.TB_FUNCTION.Where(x => x.FUNCTION_GROUP_ID == 11).ToList();
                                    foreach (
                                        var functionModel in
                                           objPlanFunctions1)
                                    {
                                        var planFunctionsList = new TB_PLAN_FUNCTIONS();
                                        planFunctionsList.FUNCTION_ID = functionModel.FUNCTION_ID;
                                        planFunctionsList.PLAN_ID = planid;
                                        planFunctionsList.ACTION_VIEW = functionModel.DEFAULT_ACTION_VIEW;
                                        planFunctionsList.ACTION_MODIFY = functionModel.DEFAULT_ACTION_MODIFY;
                                        planFunctionsList.ACTION_ADD = functionModel.DEFAULT_ACTION_ADD;
                                        planFunctionsList.ACTION_REMOVE = functionModel.DEFAULT_ACTION_REMOVE;
                                        planFunctionsList.FUNCTION_NAME = functionModel.FUNCTION_NAME;
                                        planFunctionsList.CREATED_USER = User.Identity.Name;
                                        planFunctionsList.CREATED_DATE = DateTime.Now;
                                        planFunctionsList.MODIFIED_USER = User.Identity.Name;
                                        planFunctionsList.MODIFIED_DATE = DateTime.Now;
                                        planFunctionsList.FUNC_ID = functionModel.FUNCTION_ID;
                                        planFunctionsList.FUNCTION_GROUP_ID = functionModel.FUNCTION_GROUP_ID;
                                        dbcontext.TB_PLAN_FUNCTIONS.Add(planFunctionsList);
                                        dbcontext.SaveChanges();
                                    }

                                }
                            }
                            //  return Request.CreateResponse(HttpStatusCode.OK, "Saved Successfully.");
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Plan already exists, please create a new Plan");
                        }
                    }

                    foreach (var objFunModel in plan.FunctionModels)
                    {
                        List<TB_PLAN_FUNCTIONS> objPlanFunctions;
                        if (objFunModel.FUNCTION_ID != 0)
                        {
                            objPlanFunctions = dbcontext.TB_PLAN_FUNCTIONS.Where(
                              x => x.PLAN_ID == plan.PLAN_ID &&
                                   x.FUNCTION_GROUP_ID == objFunModel.FUNCTION_GROUP_ID && x.FUNC_ID == objFunModel.FUNCTION_ID).ToList();
                        }
                        else
                        {
                            objPlanFunctions = dbcontext.TB_PLAN_FUNCTIONS.Where(
                              x => x.PLAN_ID == planid &&
                                   x.FUNCTION_GROUP_ID == objFunModel.FUNCTION_GROUP_ID).ToList();

                        }
                        if (objPlanFunctions.Count > 0)
                        {
                            objPlanFunctions.ForEach(a =>
                            {
                                a.ACTION_ADD = objFunModel.DEFAULT_ACTION_ALLOW;
                                a.ACTION_MODIFY = objFunModel.DEFAULT_ACTION_ALLOW;
                                a.ACTION_REMOVE = objFunModel.DEFAULT_ACTION_ALLOW;
                                a.ACTION_VIEW = objFunModel.DEFAULT_ACTION_ALLOW;
                                a.MODIFIED_DATE = DateTime.Now;
                                a.MODIFIED_USER = User.Identity.Name;
                            });

                            dbcontext.SaveChanges();
                        }


                    }
                    return Request.CreateResponse(HttpStatusCode.OK, "Saved successfully");

                }
            }

            catch (Exception exp)
            {
                _logger.Error("Error at AccountApiController: UpdateOrSavePlan", exp);
            }
            return Request.CreateResponse(HttpStatusCode.InternalServerError, "Please reload the page and continue.");
        }

        [HttpPost]
        public HttpResponseMessage DeletePlan(int planId)
        {
            try
            {
                using (var dbcontext = new CSEntities())
                {
                    var deleteplanavail = dbcontext.Customers.Where(x => x.PlanId == planId);
                    if (!deleteplanavail.Any())
                    {
                        var deleteplanfunctionsavail = dbcontext.TB_PLAN_FUNCTIONS.Where(x => x.PLAN_ID == planId);
                        foreach (var tbPlanFunctions in deleteplanfunctionsavail)
                        {
                            dbcontext.TB_PLAN_FUNCTIONS.Remove(tbPlanFunctions);
                        }
                        dbcontext.SaveChanges();
                        var deleplanavail = dbcontext.TB_PLAN.Where(x => x.PLAN_ID == planId);
                        foreach (var tbPlanFunctions in deleplanavail)
                        {
                            dbcontext.TB_PLAN.Remove(tbPlanFunctions);
                        }
                        dbcontext.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted successfully");
                    }
                    else
                    {
                        var delecustomeravail = dbcontext.TB_PLAN.Where(x => x.PLAN_ID == planId);
                        var deletecust = delecustomeravail.FirstOrDefault();
                        if (deletecust != null)
                        {
                            deletecust.IS_ACTIVE = false;
                            dbcontext.SaveChanges();
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, "The selected Active Plan can only be disabled");
                    }
                }

            }

            catch (Exception exp)
            {
                _logger.Error("Error at AccountApiController: UpdateOrSavePlan", exp);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Please reload the page and continue.");
            }

        }
        [HttpPost]
        public HttpResponseMessage Deletecustomer(int customerId)
        {
            try
            {
                using (var dbcontext = new CSEntities())
                {
                    var deletecustavail = dbcontext.Customer_User.Where(x => x.CustomerId == customerId);
                    if (!deletecustavail.Any())
                    {
                        var deleteplanfunctionsavail = dbcontext.Customer_Catalog.Where(x => x.CustomerId == customerId);
                        foreach (var tbPlanFunctions in deleteplanfunctionsavail)
                        {
                            dbcontext.Customer_Catalog.Remove(tbPlanFunctions);
                        }
                        dbcontext.SaveChanges();
                        var deleplanavail = dbcontext.Customer_Settings.Where(x => x.CustomerId == customerId);
                        foreach (var tbPlanFunctions in deleplanavail)
                        {
                            dbcontext.Customer_Settings.Remove(tbPlanFunctions);
                        }
                        dbcontext.SaveChanges();
                        var delecustomeravail = dbcontext.Customers.Where(x => x.CustomerId == customerId);
                        foreach (var tbPlanFunctions in delecustomeravail)
                        {
                            dbcontext.Customers.Remove(tbPlanFunctions);
                        }
                        dbcontext.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, "Deleted successfully");
                    }
                    else
                    {
                        var delecustomeravail = dbcontext.Customers.Where(x => x.CustomerId == customerId);
                        var deletecust = delecustomeravail.FirstOrDefault();
                        if (deletecust != null)
                        {
                            deletecust.IsActive = false;
                            dbcontext.SaveChanges();
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, "Company cannot be deleted when there are active Users, please disable all Users and try again");
                    }
                }

            }

            catch (Exception exp)
            {
                _logger.Error("Error at AccountApiController: UpdateOrSavePlan", exp);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, "Please reload the page and continue.");
            }

        }

        //Product_Title
        [HttpGet]
        public string getProductTitleValue()
        {
            try
            {
                string ProductTitle = ConfigurationManager.AppSettings["productTitle"];
                return ProductTitle;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at GetAllRoles", ex);
                return "";
            }
        }
        [HttpGet]
        public HttpResponseMessage GetAllRolesForIndesign()
        {
            try
            {

                string[] roles = Roles.GetRolesForUser(User.Identity.Name);

                bool IsAdmin = false;
                int customer_Id;
                //var customerid = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).
                //customer_Id = Convert.ToInt32(customerid);
                var Rolename = roles[0].ToString();
                var customerId = 0;
                var Role_id = 0;
                int.TryParse(
                    _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name)
                        .Select(y => y.CustomerId)
                        .FirstOrDefault()
                        .ToString(), out customerId);

                int.TryParse(
                    _dbcontext.aspnet_Roles.Where(x => x.CustomerId == customerId && x.RoleName == Rolename)
                        .Select(y => y.Role_id)
                        .FirstOrDefault()
                        .ToString(), out Role_id);

                //var Role_id = _dbcontext.aspnet_Roles.Where(x => x.CustomerId == customerId).Select(y=> y.Role_id).FirstOrDefault().ToString();
                var Role_type = _dbcontext.aspnet_Roles.Where(x => x.Role_id == Role_id && x.CustomerId == customerId).Select(y => y.RoleType).FirstOrDefault().ToString();
                //if (Role_id == "2")
                if (Role_type == "2")
                {
                    IsAdmin = true;
                }
                else
                {
                    IsAdmin = false;
                }
                return Request.CreateResponse(HttpStatusCode.OK, IsAdmin);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at GetAllRoles", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
    }
}
