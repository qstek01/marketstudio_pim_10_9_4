﻿using System;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Windows.Forms;
using LS.Data;
using LS.Data.Model;
using Newtonsoft.Json;
using Stimulsoft.Base.Drawing;
using Stimulsoft.Report;
using Stimulsoft.Report.Components;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;
using System.Web.Routing;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Data;
using log4net;
using System.Text;
using System.Xml;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Collections;
using System.Web.Services.Description;
using Newtonsoft.Json.Linq;
using Stimulsoft.Report.Mvc;
using LS.Data.Model.XpressCatalog;
using System.IO;
using LS.Web.Helpers;


namespace LS.Web.Controllers
{
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class XpressCatalogController : Controller
    {
        #region init
        string Message;
        private ILog _logger = LogManager.GetLogger(typeof(AccountsController));
        private readonly PrinterSettings _printerSetting = new PrinterSettings();
        private string _uPath = string.Empty;
        private string _userImagepath = string.Empty;
        public bool Opensimple;
        public bool Formopen;
        private bool _runedTemplate;
        private StiReport _openedReport = new StiReport();
        public StiReport report = new StiReport();
        public StiReport simpleCatalogReport = new StiReport();
        public StiReport simplePDFReport = new StiReport();
        private CSEntities _dbcontext = new CSEntities();
        private static int _workingCatalogID;
        private StiDataBand _dataBand;
        private StiCrossDataBand _crossDataBand;
        private double _leftValue;
        private bool _fBand;
        private bool _cBand;
        private bool _familyAttachmentBool;
        private readonly double[] _widthValue = new double[10];
        private bool _pTablebool;
        private int _familyDescription;
        string multipleTables = "", referenceTables = "";
        private string _connectionString = string.Empty;
        private string strval = string.Empty;
        private string datatype = string.Empty;
        private int noofdecimalplace = 0;
        public string _SQLString = "";
        string sysAttr;
        private string cusAttr;
        public static int _workingCatalogID1;
        private string SQLString1;
        StiReport frmStiReport = new StiReport();
        public string showXml;
        CSEntities objLS = new CSEntities();

        private static string selectedCategoryId = "";
        private static string fullCategoryIds = "";

        /// <summary>
        /// Varibles used in API controller
        /// </summary>
        public static JArray selectedTreeNodes;
        private static string ColumnNumericEditor = "";
        private static string ColumnWidthNumericEditor = "";
        private static string ColumnGapNumericEditor = "";
        private static string LeftNumericEditor = "";
        private static string RightNumericEditor = "";
        private static string TopNumericEditor = "";
        private static string BottomNumericEditor = "";
        private static string PaperSizeCombo = "";
        private static string WidthNumericEditor = "";
        private static string HeightNumericEditor = "";
        private static string sWidthNumericEditor = "";
        private static string sHeightNumericEditor = "";
        private static int pageformatOptionSet = 0;

        private static int typeOptionSet_Tab1 = 0;
        private static int CoverPageOptionSet = 0;
        private static int TOCOptionSet = 0;
        private static int TOC_Cat_Fam_OptionSet = 0;
        private static int IndexPageOptionSet = 0;
        private static int Index_Cat_No_OptionSet = 0;
        private static string IndexColumns_NumericEditor = "";
        private static bool _flagOpenReportPreview = false;

        #endregion


        #region  PdfXpress Separate path
        // To pass dynamic path location from web config   - Start
        string IsServerUpload = WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
        string pathDesignation = WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
        string pathUrl = WebConfigurationManager.AppSettings["ExternalAssetDriveURL"];
        #endregion


        public xpressSimpleCatalogController objXpressSimpleCatalog = new xpressSimpleCatalogController();
        public string XpresspathCheckFlag = System.Web.Configuration.WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
        public string XpresspathServerValue = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDriveURL"].ToString();
        public string XpressserverPathShareVal = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();

        [System.Web.Http.HttpGet]
        public string getCategoryId(string categoryId)
        {
            selectedCategoryId = categoryId;
            fullCategoryIds = "";
            var categoryIds = selectedCategoryId.Split(',');
            TB_CATEGORY tb_category = new TB_CATEGORY();
            foreach (string category_Id in categoryIds)
            {
                var tb_categoryList = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category_Id).ToList();
                fullCategoryIds = fullCategoryIds + "," + category_Id;
                foreach (var category in tb_categoryList)
                {
                    fullCategoryIds = fullCategoryIds + "," + category.CATEGORY_ID;
                    //categoryIds.Add(category.CATEGORY_ID);
                    var tb_categoryLists = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category.CATEGORY_ID).ToList();
                    foreach (var categorys in tb_categoryLists)
                    {
                        fullCategoryIds = fullCategoryIds + "," + categorys.CATEGORY_ID;
                        //categoryIds.Add(categorys.CATEGORY_ID);
                        string category_ID = categorys.CATEGORY_ID;
                        do
                        {
                            tb_category = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category_ID).FirstOrDefault();
                            if (tb_category != null)
                            {
                                category_ID = tb_category.CATEGORY_ID;
                                fullCategoryIds = fullCategoryIds + "," + category_ID;
                                //categoryIds.Add(tb_category.CATEGORY_ID);
                            }
                        } while (tb_category != null);

                    }
                }
            }
            fullCategoryIds = fullCategoryIds.Substring(1);
            System.Web.HttpContext.Current.Session["SelectedCategories"] = fullCategoryIds;
            return fullCategoryIds;
        }

        public int getCatalogID(int catalogId)
        {
            _workingCatalogID1 = catalogId;
            _workingCatalogID = catalogId;
            return catalogId;
        }

        public string GetProjectHeaderFooter(string headertext, string footertext, string projectId, bool productTablechecked)
        {
            string[] headerfooter = { headertext, footertext, projectId, productTablechecked.ToString() };
            Session["pdfheader"] = headerfooter;
            return "OK";
        }

        #region Jsonmethods
        public bool SaveProject(int catalogID, string projectName, byte projectType, string comments)
        {
            try
            {
                var objPrjName = objLS.TB_PROJECT.FirstOrDefault(s => s.CATALOG_ID == catalogID && s.PROJECT_NAME == projectName && s.PROJECT_TYPE == projectType && s.COMMENTS == comments);
                if (objPrjName == null)
                {
                    var objproj = new TB_PROJECT
                    {
                        PROJECT_NAME = projectName,
                        COMMENTS = comments,
                        CATALOG_ID = catalogID,
                        PROJECT_TYPE = projectType,
                        CREATED_USER = "tbadmin",
                        CREATED_DATE = DateTime.Now,
                        MODIFIED_USER = "tbadmin",
                        MODIFIED_DATE = DateTime.Now
                    };
                    objLS.TB_PROJECT.Add(objproj);
                    objLS.SaveChanges();
                }
                return true;
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-SaveProject", exp);
                Console.WriteLine(exp.Message);
                return false;
            }

        }
        public JsonResult getProjectID(int catalogID, string projectName, byte projectType, string comments)
        {
            try
            {
                var objPrjName = objLS.TB_PROJECT.Where(s => s.CATALOG_ID == catalogID && s.PROJECT_NAME == projectName && s.PROJECT_TYPE == projectType && s.COMMENTS == comments).Select(x => new
                {
                    x.PROJECT_ID,
                    x.PROJECT_NAME,
                    x.PROJECT_TYPE,
                    x.COMMENTS,
                    x.TB_CATALOG.CATALOG_NAME

                }).ToList();
                return Json(objPrjName.ElementAt(0), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-getProjectID", exp);
                Console.WriteLine(exp.Message);
                return null;
            }
        }







        /// <summary>
        /// To Save the corresponding records into TB_PDFXPRESS_HIERARCHY 
        /// In this method have a non return type
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="ID"></param>
        /// Created By Mariyavijayan

        public void SaveTypeOfPdfxpressHierarchy(string Type, string ID, string fileName, string CatalogId)
        {

            int customer_id = 0;
            string customer_Name = string.Empty;

            if (Type != "Catalog")
            {
                string userName = User.Identity.Name;
                customer_Name = User.Identity.Name.ToString();
                customer_id = _dbcontext.Customers.Where(x => x.CustomerName == customer_Name).Select(x => x.CustomerId).SingleOrDefault();

                var userId = objLS.Customer_User.Join(objLS.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();


                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template"));
                }
            }



            string CategoryId = string.Empty;
            string query = string.Empty;
            string FamilyId;
            string ProductId;


            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            // SqlCommand cmd = new SqlCommand();

            //  string FilePath = Path.Combine(Server.MapPath("~/Customer Templates/" + userName + "/Opened Template"), fileName);

            // To save new file Path.


            int Catalog_Id = Convert.ToInt32(CatalogId);




            // var FilePath = System.Web.HttpContext.Current.Server.MapPath("~/PDF_Template/" + userId + "");
            // string FilePath = Path.Combine(Server.MapPath("~/PDF_Template/" + userId + ""), fileName);

            string customerPath = "PDF_Template";

            string FilePath = "" + customerPath + @"\" + fileName + "";


            // In linq
            var objPdfXpress = new TB_PDFXPRESS_HIERARCHY();

            switch (Type)
            {
                case "ALL":
                    break;

                case "Catalog":

                    FilePath = "" + customerPath + @"\" + CatalogId + "_" + fileName + "";

                    fileName = CatalogId + "_" + fileName + "";

                    objPdfXpress.PROJECT_ID = 0;
                    objPdfXpress.TEMPLATE_NAME = fileName;
                    objPdfXpress.TEMPLATE_PATH = FilePath;
                    objPdfXpress.TYPE = Type;
                    objPdfXpress.ASSIGN_TO = CatalogId;
                    objPdfXpress.CUSTOMER_ID = customer_id;
                    objPdfXpress.CATALOG_ID = Catalog_Id;
                    objPdfXpress.CREATED_USER = customer_Name;
                    objPdfXpress.CREATED_DATE = DateTime.Now;
                    objPdfXpress.MODIFIED_USER = customer_Name;
                    objPdfXpress.MODIFIED_DATE = DateTime.Now;



                    break;

                case "Category":

                    //string categoryId = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == ID && x.FLAG_RECYCLE == "A").Select(x => x.CATEGORY_ID).SingleOrDefault();

                    string categoryId = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tc => tc.CATEGORY_ID, tcs => tcs.CATEGORY_ID, (tc, tcs) => new { tc, tcs }).Where(x => x.tc.CATEGORY_SHORT == ID && x.tc.FLAG_RECYCLE == "A" && x.tcs.CATALOG_ID == Catalog_Id).Select(x => x.tc.CATEGORY_ID).SingleOrDefault();
                    objPdfXpress.PROJECT_ID = 0;
                    objPdfXpress.TEMPLATE_NAME = fileName;
                    objPdfXpress.TEMPLATE_PATH = FilePath;
                    objPdfXpress.TYPE = Type;
                    objPdfXpress.ASSIGN_TO = categoryId;
                    objPdfXpress.CUSTOMER_ID = customer_id;
                    objPdfXpress.CATALOG_ID = Catalog_Id;
                    objPdfXpress.CREATED_USER = customer_Name;
                    objPdfXpress.CREATED_DATE = DateTime.Now;
                    objPdfXpress.MODIFIED_USER = customer_Name;
                    objPdfXpress.MODIFIED_DATE = DateTime.Now;



                    break;

                case "Family":

                    FamilyId = ID;


                    objPdfXpress.PROJECT_ID = 0;
                    objPdfXpress.TEMPLATE_NAME = fileName;
                    objPdfXpress.TEMPLATE_PATH = FilePath;
                    objPdfXpress.TYPE = Type;
                    objPdfXpress.ASSIGN_TO = FamilyId;
                    objPdfXpress.CUSTOMER_ID = customer_id;
                    objPdfXpress.CATALOG_ID = Catalog_Id;
                    objPdfXpress.CREATED_USER = customer_Name;
                    objPdfXpress.CREATED_DATE = DateTime.Now;
                    objPdfXpress.MODIFIED_USER = customer_Name;
                    objPdfXpress.MODIFIED_DATE = DateTime.Now;



                    break;

                case "Product":

                    ProductId = ID;

                    objPdfXpress.PROJECT_ID = 0;
                    objPdfXpress.TEMPLATE_NAME = fileName;
                    objPdfXpress.TEMPLATE_PATH = FilePath;
                    objPdfXpress.TYPE = Type;
                    objPdfXpress.ASSIGN_TO = ProductId;
                    objPdfXpress.CUSTOMER_ID = customer_id;
                    objPdfXpress.CATALOG_ID = Catalog_Id;
                    objPdfXpress.CREATED_USER = customer_Name;
                    objPdfXpress.CREATED_DATE = DateTime.Now;
                    objPdfXpress.MODIFIED_USER = customer_Name;
                    objPdfXpress.MODIFIED_DATE = DateTime.Now;



                    break;
            }


            _dbcontext.TB_PDFXPRESS_HIERARCHY.Add(objPdfXpress);
            _dbcontext.SaveChanges();


            // In Ado .net


            //switch (Type)
            //{
            //    case "ALL":
            //        break;

            //    case "Catalog":
            //        query = "INSERT INTO TB_PDFXPRESS_HIERARCHY(PROJECT_ID,TEMPLATE_NAME,TEMPLATE_PATH,TYPE,ASSIGN_TO,CUSTOMER_ID,CATALOG_ID,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) VALUES(0,'" + fileName + "','" + FilePath + "','" + Type + "', '" + Catalog_Id + "' , '" + customer_id + "',  '" + Catalog_Id + "','" + customer_Name + "',getdate(),'" + customer_Name + "',getdate())";
            //        break;

            //    case "Category":

            //        string categoryId = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == ID).Select(x => x.CATEGORY_ID).SingleOrDefault();
            //        query =  "INSERT INTO TB_PDFXPRESS_HIERARCHY(PROJECT_ID,TEMPLATE_NAME,TEMPLATE_PATH,TYPE,ASSIGN_TO,CUSTOMER_ID,CATALOG_ID,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) VALUES(0,'" + fileName + "','" + FilePath + "','" + Type + "', '" + categoryId + "' , '" + customer_id + "',  '" + Catalog_Id + "','" + customer_Name + "',getdate(),'" + customer_Name  + "',getdate())";             
            //        break;

            //    case "Family":

            //        FamilyId = ID;
            //        query = "INSERT INTO TB_PDFXPRESS_HIERARCHY(PROJECT_ID,TEMPLATE_NAME,TEMPLATE_PATH,TYPE,ASSIGN_TO,CUSTOMER_ID,CATALOG_ID,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) VALUES(0,'" + fileName + "','" + FilePath + "','" + Type + "', '" + FamilyId + "' , '" + customer_id + "',  '" + Catalog_Id + "','" + customer_Name + "',getdate(),'" + customer_Name + "',getdate())";
            //        break;

            //    case "Product":

            //        ProductId = ID;
            //        query = "INSERT INTO TB_PDFXPRESS_HIERARCHY(PROJECT_ID,TEMPLATE_NAME,TEMPLATE_PATH,TYPE,ASSIGN_TO,CUSTOMER_ID,CATALOG_ID,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) VALUES(0,'" + fileName + "','" + FilePath + "','" + Type + "', '" + ProductId + "' , '" + customer_id + "',  '" + Catalog_Id + "','" + customer_Name + "',getdate(),'" + customer_Name + "',getdate())";
            //        break;
            //}


            //SqlCommand cmd = new SqlCommand(query, connection);
            //connection.Open();
            //cmd.ExecuteNonQuery();
            //connection.Close();


        }
        /// <summary>
        /// In this method to return a Full path of pdf xpress
        /// </summary>
        /// <param name="Exists"></param>
        /// <returns>
        /// Author : Mariya vijayan.
        /// </returns>
        public string ReturnsPath(string Exists)
        {
            try
            {
                var userId = objLS.Customer_User.Join(objLS.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();

                if (!string.IsNullOrEmpty(Exists))
                {

                    string path = string.Empty;
                    if (XpresspathCheckFlag == "true")
                    {
                        path = new Uri(XpressserverPathShareVal + "/Content/ProductImages/" + userId).LocalPath;
                    }
                    else
                    {
                        path = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "");
                    }

                    path = path + @"\" + Exists;
                    return path;
                }
                return null;
            }
            catch (Exception ex)
            {

                return null;
            }


        }


        /// <summary>
        /// In this method to return a file path 
        /// </summary>
        /// <param name="TYPE"></param>
        /// <param name="Id"></param>
        /// <param name="CatalogID"></param>
        /// <returns>
        /// MARIYAVIJAYAN
        /// </returns>

        [System.Web.Http.HttpPost]
        public string GetPdfXpressdefaultType(string TYPE, string Id, string CatalogID)
        {
            // Its Returns only Path:

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            HomeApiController objHome = new HomeApiController();

            string Exists = string.Empty;
            string Cat_Id = CatalogID;
            int catalog_id = Convert.ToInt32(Cat_Id);

            if (TYPE.ToUpper() == "CATEGORY")
            {
                if (!Id.Contains("CAT"))
                {
                    Id = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == Id).Select(x => x.CATEGORY_ID).FirstOrDefault();
                }
                string rootLevelCategory = objHome.FindRootLevelCategoryId(Id);

                if (!string.IsNullOrEmpty(rootLevelCategory))
                {
                    Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == catalog_id && a.ASSIGN_TO == rootLevelCategory && a.TYPE == TYPE).Select(x => x.TEMPLATE_PATH).FirstOrDefault();
                    //SqlCommand cmd = new SqlCommand("select TEMPLATE_PATH from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatalogID + "' and ASSIGN_TO = '" + rootLevelCategory + "' and [TYPE] = '" + TYPE + "' ", con);
                    //con.Open();
                    //Exists = (string)cmd.ExecuteScalar();
                    //con.Close();
                }
                else
                {
                    Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == catalog_id && a.ASSIGN_TO == Id && a.TYPE == TYPE).Select(x => x.TEMPLATE_PATH).FirstOrDefault();

                    //SqlCommand cmd = new SqlCommand("select TEMPLATE_PATH from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatalogID + "' and ASSIGN_TO = '" + Id + "' and [TYPE] = '" + TYPE + "' ", con);
                    //con.Open();
                    //Exists = (string)cmd.ExecuteScalar();
                    //con.Close();
                    if (string.IsNullOrEmpty(Exists))
                    {
                        Exists = null;
                    }
                }
                Exists = ReturnsPath(Exists);
                return Exists;
            }
            else if (TYPE.ToUpper() == "FAMILY")
            {

                int FamId = Convert.ToInt32(Id);
                // _dbcontext.TB_P
                //SqlCommand cmd = new SqlCommand("select TEMPLATE_PATH from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatalogID + "' and ASSIGN_TO = '" + Id + "' and [TYPE] = '" + TYPE + "' ", con);
                //con.Open();
                //Exists = (string)cmd.ExecuteScalar();
                //con.Close();
                Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == catalog_id && a.ASSIGN_TO == Id && a.TYPE == TYPE).Select(x => x.TEMPLATE_PATH).FirstOrDefault();
              

                if (!string.IsNullOrEmpty(Exists))
                {
                    Exists = ReturnsPath(Exists);
                    return Exists;
                }
                else
                {
                   // Exists1 = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == catalog_id && a.ASSIGN_TO == Id && a.TYPE == TYPE).Select(x => x.TEMPLATE_PATH).FirstOrDefault();
                    Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == catalog_id && a.ASSIGN_TO == Id).Select(x => x.TEMPLATE_PATH).First();
                    if (!string.IsNullOrEmpty(Exists))
                    {
                        Exists = ReturnsPath(Exists);
                        return Exists;
                    }
                    else
                    {
                        return null;
                    }
                    //cmd = new SqlCommand("select top 1 CATEGORY_ID from TB_CATALOG_FAMILY  where  FAMILY_ID = '" + Id + "' and CATALOG_ID = '" + catalog_id + "' ", con);
                    //con.Open();
                    //Exists = (string)cmd.ExecuteScalar();
                    //con.Close();
                }

                // get Category_id from tb_catalog_family


                string rootLevelCategory = objHome.FindRootLevelCategoryId(Exists);


                if (!string.IsNullOrEmpty(rootLevelCategory))
                {
                    Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == catalog_id && a.ASSIGN_TO == rootLevelCategory && a.TYPE == "Category").Select(x => x.TEMPLATE_PATH).FirstOrDefault();
                    //cmd = new SqlCommand("select TEMPLATE_PATH from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatalogID + "' and ASSIGN_TO = '" + rootLevelCategory + "' and [TYPE] = 'Category' ", con);
                    //con.Open();
                    //Exists = (string)cmd.ExecuteScalar();
                    //con.Close();
                    Exists = ReturnsPath(Exists);
                    return Exists;
                }
                else
                {
                    Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == catalog_id && a.ASSIGN_TO == Id && a.TYPE == "Category").Select(x => x.TEMPLATE_PATH).FirstOrDefault();
                    //cmd = new SqlCommand("select TEMPLATE_PATH from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatalogID + "' and ASSIGN_TO = '" + Id + "' and [TYPE] = 'Category' ", con);
                    //con.Open();
                    //Exists = (string)cmd.ExecuteScalar();
                    //con.Close();
                    if (string.IsNullOrEmpty(Exists))
                    {
                        Exists = null;
                    }
                }
                // var aa = ReturnsPath(Exists);
                return Exists;

            }
            else if (TYPE.ToUpper() == "PRODUCT")
            {

                // To check the Product _Id is in the table
                int FamId = Convert.ToInt32(Id);
                // To check the Product _Id is in the table

                Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.ASSIGN_TO == Id && a.TYPE == "Product").Select(x => x.TEMPLATE_PATH).FirstOrDefault();

                //SqlCommand cmd = new SqlCommand("select TEMPLATE_PATH from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatalogID + "' and ASSIGN_TO = '" + Id + "' and [TYPE] = '" + TYPE + "' ", con);
                //con.Open();
                //Exists = (string)cmd.ExecuteScalar();
                //con.Close();

                if (!string.IsNullOrEmpty(Exists))
                {
                    Exists = ReturnsPath(Exists);
                    return Exists;
                }
                else
                {
                    // to check Family Based on datails   - Start

                    // cmd = new SqlCommand("select TEMPLATE_PATH from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatalogID + "' and ASSIGN_TO = '" + Id + "' and [TYPE] = 'Family' ", con);
                    //con.Open();
                    //Exists = (string)cmd.ExecuteScalar();
                    //con.Close();
                    Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == catalog_id && a.ASSIGN_TO == Id && a.TYPE == TYPE).Select(x => x.TEMPLATE_PATH).FirstOrDefault();
                    if (!string.IsNullOrEmpty(Exists))
                    {
                        Exists = ReturnsPath(Exists);
                        return Exists;
                    }
                    else
                    {
                        Exists = _dbcontext.TB_CATALOG_FAMILY.Where(a => a.CATALOG_ID == catalog_id && a.FAMILY_ID == FamId).Select(x => x.CATEGORY_ID).First();

                        //cmd = new SqlCommand("select top 1 CATEGORY_ID from TB_CATALOG_FAMILY  where  FAMILY_ID = '" + Id + "' and CATALOG_ID = '" + catalog_id + "' ", con);
                        //con.Open();
                        //Exists = (string)cmd.ExecuteScalar();
                        //con.Close();
                    }

                    // get Category_id from tb_catalog_family


                    string rootLevelCategory = objHome.FindRootLevelCategoryId(Exists);


                    if (!string.IsNullOrEmpty(rootLevelCategory))
                    {
                        Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == catalog_id && a.ASSIGN_TO == rootLevelCategory && a.TYPE == "Category").Select(x => x.TEMPLATE_PATH).FirstOrDefault();
                        //cmd = new SqlCommand("select TEMPLATE_PATH from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatalogID + "' and ASSIGN_TO = '" + rootLevelCategory + "' and [TYPE] = 'Category' ", con);
                        //con.Open();
                        //Exists = (string)cmd.ExecuteScalar();
                        //con.Close();
                        Exists = ReturnsPath(Exists);
                        return Exists;
                    }
                    else
                    {
                        Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == catalog_id && a.ASSIGN_TO == Id && a.TYPE == "Category").Select(x => x.TEMPLATE_PATH).FirstOrDefault();
                        //cmd = new SqlCommand("select TEMPLATE_PATH from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatalogID + "' and ASSIGN_TO = '" + Id + "' and [TYPE] = 'Category' ", con);
                        //con.Open();
                        //Exists = (string)cmd.ExecuteScalar();
                        //con.Close();
                        if (string.IsNullOrEmpty(Exists))
                        {
                            Exists = null;
                        }
                    }
                    //End
                }

            }
            return null;
        }


        public JsonResult SaveFiles()
        {

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);

            string fileName, actualFileName;
            string FilePath = string.Empty;
            Message = fileName = actualFileName = string.Empty;
            bool flag = false;
            string Id = string.Empty;
            string fileExist = string.Empty;
            bool fileContains = true;
            string ids = "";
            if (Request.Form["ID"] != null) { 
            ids = Request.Form["ID"].ToString();
            }
            //  var file = Request.Files[0];
            // fileName = file.FileName;
            System.Web.HttpContext.Current.Session["CURRENTCATALOGID"] = Request.Form["CatalogId"].ToString();
            string Catlog_ID = Request.Form["CatalogId"].ToString();
            string Type = Request.Form["TYPE"].ToString();
            bool savePdfXpressValue = Convert.ToBoolean(Request.Form["SaveTemplateData"]);
            var userId = objLS.Customer_User.Join(objLS.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();

            HomeApiController objHome = new HomeApiController();

            //End
            // Convert Category_id root


            if (Type == "OPEN")
            {
                if (Request.Files.Count > 0)
                {
                    var file = Request.Files[0];

                    fileName = file.FileName;
                    string CatId = Request.Form["CatalogId"].ToString();
                    int CataLog_ID = Convert.ToInt32(CatId);
                    string customer_Name = User.Identity.Name.ToString();


                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/Customer Templates/Opened Template")))
                    {
                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/Customer Templates/Opened Template"));
                    }
                    file.SaveAs(Path.Combine(Server.MapPath("~/Content/ProductImages/" + userId + "/Customer Templates/Opened Template"), fileName));
                    FilePath = Path.Combine(Server.MapPath("~/Content/ProductImages/" + userId + "/Customer Templates/Opened Template"), fileName);



                    //    string folderPath = Server.MapPath("~/Content/ProductImages/" + userId + "/Customer Templates/Opened Template/");


                    Session["TemplatePath"] = FilePath;
                }

            }
            else
            {


                if (Type == "Category")
                {
                    if (!ids.Contains("CAT"))
                    {
                        string CatId = Request.Form["CatalogId"].ToString();
                        int CataLog_ID = Convert.ToInt32(CatId);
                        //ids = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == ids && x.FLAG_RECYCLE == "A").Select(x => x.CATEGORY_ID).SingleOrDefault();
                        ids = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tc => tc.CATEGORY_ID, tcs => tcs.CATEGORY_ID, (tc, tcs) => new { tc, tcs }).Where(a => a.tc.CATEGORY_SHORT == ids && a.tc.FLAG_RECYCLE == "A" && a.tcs.CATALOG_ID == CataLog_ID).Select(x => x.tc.CATEGORY_ID).SingleOrDefault();
                        string temp_Id = objHome.FindRootLevelCategoryId(ids);

                        if (!string.IsNullOrEmpty(temp_Id))
                        {
                            ids = temp_Id;
                        }
                    }
                }

                // End
                if (Request.Files != null)
                {
                    try
                    {

                        // To Save The specific values its save or update 
                        if (Type != "ALL")
                        {
                            try
                            {
                                var file = Request.Files[0];

                                fileName = file.FileName;
                                string CatId = Request.Form["CatalogId"].ToString();
                                int Catalog_ID = Convert.ToInt32(CatId);
                                string customer_Name = User.Identity.Name.ToString();
                                int customer_id = _dbcontext.Customers.Where(x => x.CustomerName == customer_Name).Select(x => x.CustomerId).SingleOrDefault();

                                // Check already the values is exist or not 
                                if (Type == "Category")
                                {
                                    if (!ids.Contains("CAT"))
                                    {
                                        ids = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tc => tc.CATEGORY_ID, tcs => tcs.CATEGORY_ID, (tc, tcs) => new { tc, tcs }).Where(a => a.tc.CATEGORY_SHORT == ids && a.tc.FLAG_RECYCLE == "A" && a.tcs.CATALOG_ID == Catalog_ID).Select(x => x.tc.CATEGORY_ID).SingleOrDefault();
                                      //  _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == ids).Select(x => x.CATEGORY_ID).SingleOrDefault();
                                    }

                                }

                                var Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == Catalog_ID && a.ASSIGN_TO == ids && a.CUSTOMER_ID == customer_id && a.TYPE == Type).Select(x => x.TEMPLATE_NAME).FirstOrDefault();



                                //SqlCommand checkTheValuePdfxpressHierarchy = new SqlCommand("select count(*) from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatId + "' and ASSIGN_TO = '" + ids + "' and CUSTOMER_ID = '" + customer_id + "' and [TYPE] = '" + Type + "' ", con);
                                //con.Open();
                                //int UserExist = (int)checkTheValuePdfxpressHierarchy.ExecuteScalar();
                                //con.Close();


                                if (savePdfXpressValue)   // Tosave the data when click the save button other wise its not saved.
                                {
                                    if (!string.IsNullOrEmpty(Exists))
                                    {
                                        //Username exist   [Delate]

                                        //   ADO.NET Code.

                                        //SqlCommand DeleteTheValuePdfxpressHierarchy = new SqlCommand("Delete from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatId + "' and ASSIGN_TO = '" + ids + "' and CUSTOMER_ID = '" + customer_id + "' and [TYPE] = '" + Type + "' ", con);
                                        //con.Open();
                                        //DeleteTheValuePdfxpressHierarchy.ExecuteNonQuery();
                                        //con.Close();


                                        //Linq Code.

                                        var pdfXpressdet = _dbcontext.TB_PDFXPRESS_HIERARCHY.FirstOrDefault(a => a.CATALOG_ID == Catalog_ID && a.ASSIGN_TO == ids && a.CUSTOMER_ID == customer_id && a.TYPE == Type);
                                        _dbcontext.TB_PDFXPRESS_HIERARCHY.Remove(pdfXpressdet);
                                        _dbcontext.SaveChanges();


                                        SaveTypeOfPdfxpressHierarchy(Type, Request.Form["ID"].ToString(), fileName, Request.Form["CatalogId"].ToString());
                                    }
                                    else
                                    {
                                        //Username doesn't exist.  [Insert]
                                        SaveTypeOfPdfxpressHierarchy(Type, Request.Form["ID"].ToString(), fileName, Request.Form["CatalogId"].ToString());
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                fileContains = false;

                                string CatalogID = Request.Form["CatalogId"].ToString();


                                string templateName = GetPdfXpressdefaultType(Type, ids, CatalogID);


                                //  "~/Content/ProductImages/" + userId + "/PDF_Template"


                                if (!string.IsNullOrEmpty(templateName))
                                {

                                    Session["TemplatePath"] = templateName;
                                    FilePath = templateName;

                                }
                                else
                                {
                                    // Get Default Template Name:
                                    string DefaultName = objHome.getPdfXpressdefaultTemplate(CatalogID);


                                    string folderPath = Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template/");
                                    Session["TemplatePath"] = folderPath + DefaultName;
                                    FilePath = folderPath + DefaultName;
                                    if(!Directory.Exists(FilePath))
                                    {
                                        Message = "File upload failed, please try again";
                                    }
                                }

                            }
                        }

                    }
                    catch  // In this have a default file selection.
                    {

                    }
                    string userName = User.Identity.Name;

                    if (fileContains)
                    {
                        try
                        {

                            var file = Request.Files[0];
                            fileName = file.FileName;

                            if (XpresspathCheckFlag == "true")
                            {
                                string serverPath = new Uri(XpressserverPathShareVal + "/Content/ProductImages/" + userId + "/PDF_Template").LocalPath;
                                if (!Directory.Exists(serverPath))
                                {
                                    Directory.CreateDirectory(serverPath);
                                }
                                string getFullPatheName = string.Format("{0}/{1}", serverPath, fileName);
                                if (getFullPatheName != null)
                                {
                                    file.SaveAs(Path.Combine(getFullPatheName));
                                    FilePath = Path.Combine(getFullPatheName);

                                    //  FilePath = "" + userId + @"\" + fileName + "";
                                    Session["TemplatePath"] = FilePath;
                                }
                            }
                            else
                            {


                                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template")))
                                {
                                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template"));
                                }

                                file.SaveAs(Path.Combine(Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template"), fileName));
                                FilePath = Path.Combine(Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template"), fileName);

                                //  FilePath = "" + userId + @"\" + fileName + "";
                                Session["TemplatePath"] = FilePath;
                            }

                        }
                        catch (Exception)
                        {

                            Message = "File upload failed, please try again";
                        }
                    }
                }

            }
            return Json(FilePath, JsonRequestBehavior.AllowGet);
        }

        

        public JsonResult SaveRunFiles()
        {
            string fileName, actualFileName;
            string FilePath = string.Empty;
            Message = fileName = actualFileName = string.Empty;
            bool flag = false;
            if (Request.Files != null)
            {
                var file = Request.Files[0];
                actualFileName = file.FileName;
                fileName = file.FileName;
                int size = file.ContentLength;
                string userName = User.Identity.Name;
                try
                {


                    string path = string.Empty;
                    string destinationPath;
                    string filePath = string.Empty;

                    var user_Name = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == userName).Select(y => y.c.Comments).FirstOrDefault().ToString();


                    if (IsServerUpload.ToUpper() == "TRUE")
                    {
                        destinationPath = pathDesignation + "\\Content\\ProductImages\\" + user_Name + "\\Customer Templates\\" + "RunTemplates";

                        destinationPath = destinationPath.Replace("////", "\\\\");

                        destinationPath = destinationPath.Replace("/", "\\");
                    }
                    else
                    {
                        destinationPath = Server.MapPath("~/Content/ProductImages/" + user_Name + "/Customer Templates" + "/RunTemplates");
                    }
                    if (!Directory.Exists(destinationPath))
                    {
                        Directory.CreateDirectory(destinationPath);
                    }

                    file.SaveAs(Path.Combine(destinationPath, fileName));
                    FilePath = Path.Combine(destinationPath, fileName);
                    Session["RunTemplatePath"] = FilePath;
                }
                catch (Exception)
                {
                    Message = "File upload failed, please try again";
                }
            }
            return Json(FilePath, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public JsonResult SaveCatalogFiles()
        {
            string fileName, actualFileName;
            string FilePath = string.Empty;
            Message = fileName = actualFileName = string.Empty;
            bool flag = false;
            if (Request.Files != null)
            {
                var file = Request.Files[0];
                actualFileName = file.FileName;
                fileName = file.FileName;
                int size = file.ContentLength;
                string userName = User.Identity.Name;
                try
                {




                    string path = string.Empty;
                    string destinationPath;
                    string filePath = string.Empty;

                    var user_Name = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == userName).Select(y => y.c.Comments).FirstOrDefault().ToString();


                    if (IsServerUpload.ToUpper() == "TRUE")
                    {
                        destinationPath = pathDesignation + "\\Content\\ProductImages\\" + user_Name + "\\Customer Templates\\" + "OpenCatalog";

                        destinationPath = destinationPath.Replace("////", "\\\\");

                        destinationPath = destinationPath.Replace("/", "\\");
                    }
                    else
                    {
                        destinationPath = Server.MapPath("~/Content/ProductImages/" + user_Name + "/Customer Templates" + "/OpenCatalog");
                    }









                    if (!Directory.Exists(Server.MapPath(destinationPath)))
                        Directory.CreateDirectory(destinationPath);
                    file.SaveAs(Path.Combine(destinationPath, fileName));
                    FilePath = Path.Combine(destinationPath, fileName);
                    Session["OpenCatalog"] = FilePath;

                }
                catch (Exception)
                {
                    Message = "File upload failed, please try again";
                }
            }
            return Json(FilePath, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RunCatalogPreview()
        {
            try
            {
                StiReport report = new StiReport();
                report.LoadDocument(Session["OpenCatalog"].ToString());
                //return StiMvcViewerFx.GetReportSnapshotResult(report);
                return null;
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-RunCatalogPreview", exp);
                Console.WriteLine(exp.Message);
                return null;
            }
        }
        /// <summary>
        /// Open template Method
        /// </summary>
        #region Open template Methods
        public ActionResult OpenUploadedTemplate()
        {
            try
            {
                _flagOpenReportPreview = false;
                showXml = "true";
                if (Session["TemplatePath"] != null)
                {
                    //Stimulsoft.Base.StiFontCollection.AddFontFile(@"d:\Work\Resources\MinionPro-Regular_0.otf");
                    var connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                    frmStiReport.Load(Session["TemplatePath"].ToString());
                    Boolean dsVariable = false;
                    for (int dsCount = 0; dsCount < frmStiReport.Dictionary.DataSources.Count; dsCount++)
                    {
                        if (frmStiReport.Dictionary.DataSources[dsCount].Name.ToString() == "Category")
                        {
                            dsVariable = true;
                            break;
                        }
                    }
                    if (dsVariable == true)
                    {
                        bool pVariable =
                            frmStiReport.Dictionary.DataSources["Category"].Parameters.Contains("CATALOG_ID");
                        string categoryIds = null;
                        string categoryIdsVal = null;
                        if (frmStiReport.Dictionary.Variables.Contains("CATEGORY_ID_VAL"))
                        {
                            categoryIds = "Format(\"{0}\", CATEGORY_ID_VAL)";
                            categoryIdsVal = frmStiReport.Dictionary.Variables["CATEGORY_ID_VAL"].Value;
                        }
                        if (pVariable == true)
                        {
                            frmStiReport.Dictionary.Databases.RemoveAt(0);
                            frmStiReport.Dictionary.Databases.Add(
                                new Stimulsoft.Report.Dictionary.StiSqlDatabase("CatalogStudio", connectionString));

                            frmStiReport.Dictionary.Variables["CATEGORY_ID_VAL"].Value = categoryIdsVal;

                            frmStiReport.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value =
                                _workingCatalogID1.ToString();
                            //frmStiReport.Dictionary.DataSources["Category"].Parameters["CATEGORY_ID"].Value = categoryIds;

                            frmStiReport.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value =
                                _workingCatalogID1.ToString();
                            //frmStiReport.Dictionary.DataSources["Family"].Parameters["CATEGORY_ID"].Value =
                            //    categoryIds;

                            frmStiReport.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value =
                                _workingCatalogID1.ToString();
                            //frmStiReport.Dictionary.DataSources["Family Description"].Parameters["CATEGORY_ID"].Value =
                            //    categoryIds;

                            frmStiReport.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value =
                                _workingCatalogID1.ToString();
                            //frmStiReport.Dictionary.DataSources["Family Attachment"].Parameters["CATEGORY_ID"].Value =
                            //    categoryIds;

                            frmStiReport.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value =
                                _workingCatalogID1.ToString();
                            //frmStiReport.Dictionary.DataSources["Product Family"].Parameters["CATEGORY_ID"].Value =
                            //    categoryIds;

                            frmStiReport.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value
                                = _workingCatalogID1.ToString();
                            //frmStiReport.Dictionary.DataSources["Product Specification"].Parameters["CATEGORY_ID"].Value =
                            //    categoryIds;
                            try
                            {
                                frmStiReport.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value =
                                    _workingCatalogID1.ToString();
                            }
                            catch (Exception exp)
                            {
                                _logger.Error("Error at OpenUploadedTemplate", exp);
                                Console.WriteLine(exp.Message);
                                //return null;
                            }
                        }
                    }
                    else
                    {
                        ShowWizard(ref (frmStiReport));
                    }
                    ImagePathXpc();
                    if (frmStiReport.Dictionary.Variables.Contains("AttachmentPath") == true)
                    {
                        var attachmentPath = ConfigurationManager.AppSettings["AttachmentPath"];
                        if (IsServerUpload.ToUpper() == "TRUE")
                        {
                            string destinationPath = pathDesignation + "\\Content\\ProductImages\\";

                            destinationPath = destinationPath.Replace("////", "\\\\");

                            destinationPath = destinationPath.Replace("//", "\\");
                            frmStiReport.Dictionary.Variables["AttachmentPath"].Value = destinationPath;
                        }
                        else
                        {
                            frmStiReport.Dictionary.Variables["AttachmentPath"].Value = Server.MapPath(attachmentPath);
                        }


                    }
                    FamilyFilter(_workingCatalogID1, frmStiReport);
                    ProductFilter(_workingCatalogID1, frmStiReport);
                }
                else
                {
                    Message = "ProjectID Not Found";
                }
                frmStiReport.Dictionary.Synchronize();
                frmStiReport.Dictionary.DataSources.Connect(true);
                return StiMvcDesigner.GetReportResult(frmStiReport);
                //new return StiMvcDesigner.GetReportTemplateResult(frmStiReport);
               
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-OpenUploadedTemplate", exp);
                Console.WriteLine(exp.Message);
                return null;
            }
        }
        public void ShowWizard(ref StiReport simpleReport)
        {
            StiDataSource dSource = simpleReport.DataSources[0];
            // simpleReport.DataSources.Remove(dSource);
            _openedReport = simpleReport;
            // objXpressSimpleCatalog.CustomFields(ref(_openedReport));
            SimplecatalogOpenReport(ref (_openedReport), "");

        }
        public ActionResult SaveOpenReportTemplateAs()
        {
            try
            {
                var report = StiMvcDesigner.GetReportObject();
                if (report.Pages["TOC"] != null)
                {
                    foreach (StiComponent databands in report.Pages["TOC"].Components)
                    {
                        var i = databands;
                        if (i.ComponentId.ToString() == "StiHierarchicalBand")
                        {
                            var databandss = (StiHierarchicalBand)databands;

                            if (databandss.Name == "TOC_Family_Band")
                            {
                                databandss.KeepGroupTogether = true;
                            }


                        }
                    }
                }
                if (report.Pages["Catalog"] != null)
                {
                    foreach (StiComponent databands in report.Pages["Catalog"].Components)
                    {
                        var i = databands;
                        if (i.ComponentId.ToString() == "StiHierarchicalBand")
                        {
                            var databandss = (StiHierarchicalBand)databands;

                            if (databandss.Name == "FamilyBand")
                            {
                                databandss.KeepChildTogether = true;
                                databandss.KeepDetails = StiKeepDetails.KeepDetailsTogether;
                                databandss.KeepGroupTogether = true;
                            }


                        }
                    }
                }
                if (report.Pages["Index"] != null)
                {
                    foreach (StiComponent databands in report.Pages["Index"].Components)
                    {
                        var i = databands;
                        if (i.ComponentId.ToString() == "StiHierarchicalBand")
                        {
                            var indexFamilyBand = (StiHierarchicalBand)databands;

                            if (indexFamilyBand.Name == "Index_Family_Band")
                            {
                                indexFamilyBand.KeepGroupTogether = true;
                            }
                            else if (indexFamilyBand.Name == "Index_Product_Family_Band")
                            {
                                indexFamilyBand.KeepGroupTogether = true;

                            }


                        }
                    }
                }
                if (Session["TemplatePath"] != null)
                {
                    report.Save(Session["TemplatePath"].ToString());
                }
                return StiMvcDesigner.SaveReportResult();
              
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-SaveReportTemplate", exp);
                Console.WriteLine(exp.Message);
                return null;
            }
        }
        public ActionResult openTemplatePreview()
        {
            showXml = "true";
            
           
            _flagOpenReportPreview = true;
            var connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
            if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct")))
            {
                if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Report_Config.Config")))
                {
                    string categoryIds = null;
                    string categoryIdsVal = null;
                    string CatalogId = null;
                    Session["Template"] = Session["TemplatePath"].ToString();

                    report.Load(Session["TemplatePath"].ToString());

                    if (report.Dictionary.Variables.Contains("CATEGORY_ID_VAL"))
                    {
                        categoryIds = "Format(\"{0}\", CATEGORY_ID_VAL)";
                        categoryIdsVal = report.Dictionary.Variables["CATEGORY_ID_VAL"].Value;
                        if (categoryIdsVal == "")
                            categoryIdsVal = null;
                    }
                    for (int dsCount = 0; dsCount < report.Dictionary.DataSources.Count; dsCount++)
                    {
                        if (report.Dictionary.DataSources[dsCount].Name == "Category")
                        {
                            dsVariable = true;
                            CatalogId = report.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value;
                            break;
                        }
                    }
                    if (dsVariable)
                    {
                        report.Dictionary.Databases.Clear();
                        report.Dictionary.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct"));
                        report.Dictionary.Synchronize();
                        report.Dictionary.Databases.Clear();
                        report.Dictionary.Databases.Add(new Stimulsoft.Report.Dictionary.StiSqlDatabase("CatalogStudio", connectionString));
                        FamilyFilter(_workingCatalogID1, report);
                        ProductFilter(_workingCatalogID1, report);

                        report.Dictionary.Variables["CATEGORY_ID_VAL"].Value = categoryIdsVal;

                        report.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value = CatalogId;// _workingCatalogID1.ToString();
                        report.Dictionary.DataSources["Category"].Parameters["CATEGORY_ID"].Value = categoryIds;                     //For hierarchical template category parameter

                        report.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value = CatalogId;
                        report.Dictionary.DataSources["Family"].Parameters["CATEGORY_ID"].Value = categoryIds;                       //For hierarchical template category parameter

                        report.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value = CatalogId;
                        report.Dictionary.DataSources["Family Description"].Parameters["CATEGORY_ID"].Value = categoryIds;           //For hierarchical template category parameter    

                        report.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value = CatalogId;
                        report.Dictionary.DataSources["Family Attachment"].Parameters["CATEGORY_ID"].Value = categoryIds;             //For hierarchical template category parameter

                        report.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value = CatalogId;
                        report.Dictionary.DataSources["Product Family"].Parameters["CATEGORY_ID"].Value = categoryIds;                //For hierarchical template category parameter

                        report.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value = CatalogId;
                        report.Dictionary.DataSources["Product Specification"].Parameters["CATEGORY_ID"].Value = categoryIds;         //For hierarchical template category parameter 
                        try
                        {
                            report.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value = CatalogId;
                        }
                        catch (Exception exception)
                        {
                        }
                        var folderName = _dbcontext.Customers.Join(_dbcontext.Customer_User, cs => cs.CustomerId, cu => cu.CustomerId, (cs, cu) => new { cs, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.cs.Comments).FirstOrDefault().ToString();
                        if (string.IsNullOrEmpty(folderName))
                        {
                            folderName = "Questudio";
                        }
                        var attachmentPath = ConfigurationManager.AppSettings["PreviewAttachmentPath"] + "/" + folderName + "/";
                        if (!Directory.Exists(attachmentPath))
                            Directory.CreateDirectory(attachmentPath);
                        if (report.Dictionary.Variables.Contains("PreviewAttachmentPath") == true)
                            report.Dictionary.Variables["PreviewAttachmentPath"].Value = Server.MapPath(attachmentPath);
                    }
                    else
                    {
                        RunTemplate(ref (report), "");
                    }
                }
                else
                {
                    Message = "ProjectID Not Found";
                }
            }
            if (report.Dictionary.Relations.Count < 3)
            {
                var relation = new StiDataRelation("FamilyRelation", "FamilySource", "FamilyRelation",
                    report.Dictionary.DataSources["Family Details"], report.Dictionary.DataSources["Catalog Objects"],
                    new System.String[] { "FAMILY_ID" }, new System.String[] { "FAMILY_ID" });
                if (!report.Dictionary.Relations.Contains(relation))
                    report.Dictionary.Relations.Add(relation);
                relation = new StiDataRelation("MultipleTableRelation", "MultipleTableSource", "MultipleTableRelation",
                    report.Dictionary.DataSources["Family Details"], report.Dictionary.DataSources["Multiple Tables"],
                    new System.String[] { "FAMILY_ID" }, new System.String[] { "FAMILY_ID" });
                if (!report.Dictionary.Relations.Contains(relation))
                    report.Dictionary.Relations.Add(relation);
                relation = new StiDataRelation("ReferenceTableRelation", "ReferenceTableSource",
                    "ReferenceTableRelation", report.Dictionary.DataSources["Family Details"],
                    report.Dictionary.DataSources["Reference Tables"], new System.String[] { "FAMILY_ID" },
                    new System.String[] { "FAMILY_ID" });
                if (!report.Dictionary.Relations.Contains(relation))
                    report.Dictionary.Relations.Add(relation);
            }
            report.CacheTotals = false;
            report.CacheAllData = false;


            report.ReportCacheMode = StiReportCacheMode.Off;
            report.Pages.CacheMode = false;
            report.PreviewSettings = 0;
            StiOptions.Engine.ImageCache.Enabled = false;
            report.Dictionary.Synchronize();
            //report.Dictionary.DataSources.Connect(true);
            // Session["Template"] = report;
            //return StiMvcViewerFx.GetReportSnapshotResult(report);
            //return StiMvcDesigner.PreviewReportResult(report);
            return StiMvcViewer.GetReportResult(report);
            //return StiMvcViewer.GetReportResult(report); 

        }

        public ActionResult ViewerEvent()

        {

            return StiMvcViewer.ViewerEventResult();

        }
        #endregion
        /// <summary>
        /// Design template
        /// </summary>
        #region Design template

        public void FamilyFilter(int catalogID, StiReport stiReport)
        {
            Boolean dsFamily = false;

            for (int dsCount = 0; dsCount < stiReport.Dictionary.DataSources.Count; dsCount++)
            {
                if (stiReport.Dictionary.DataSources[dsCount].Name == "Family")
                {
                    dsFamily = true;
                    break;
                }
            }

            if (dsFamily)//Advanced table  
            {
                var sqLstring = new StringBuilder();
                //TradingBell.CatalogStudio.AppLoader.DBConnection Ocon = new DBConnection();
                _SQLString = " SELECT FAMILY_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + catalogID + " ";
                DataSet oDsFamilyFilter = CreateDataSet();
                if (oDsFamilyFilter.Tables[0].Rows.Count > 0 && oDsFamilyFilter.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                {
                    string sFamilyFilter = oDsFamilyFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(sFamilyFilter);
                    XmlNode rNode = xmlDOc.DocumentElement;

                    if (rNode != null && rNode.ChildNodes.Count > 0)
                    {
                        for (int i = 0; i < rNode.ChildNodes.Count; i++)
                        {
                            XmlNode tableDataSetNode = rNode.ChildNodes[i];

                            if (tableDataSetNode.HasChildNodes)
                            {
                                if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                                {
                                    tableDataSetNode.ChildNodes[2].InnerText = "=";
                                }
                                if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                                {
                                    tableDataSetNode.ChildNodes[0].InnerText = "0";
                                }
                                string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                                if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                {
                                    sqLstring.Append("SELECT DISTINCT FAMILY_ID FROM [FAMILY DESCRIPTIONPDF](" + catalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                }
                                else
                                {
                                    sqLstring.Append("SELECT DISTINCT FAMILY_ID FROM [FAMILY DESCRIPTIONPDF](" + catalogID + ") WHERE (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                }


                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                            {

                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                            {
                                sqLstring.Append(" INTERSECT \n");
                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                            {
                                sqLstring.Append(" UNION \n");
                            }


                        }

                    }

                }
                string familyFiltersql = sqLstring.ToString();
                // Boolean variableFilter = false;
                if (familyFiltersql.Length > 0)
                {
                    string s = "\nWHERE CATALOG_ID=" + catalogID + " AND FAMILY_ID IN\n" +
                          "(\n";// +
                    //"SELECT DISTINCT FAMILY_ID\n" +
                    //"FROM [FAMILY DESCRIPTION]("+ catalogId +")\n" +
                    //"WHERE\n";
                    familyFiltersql = s + familyFiltersql + "\n)";
                    //for (int vCount = 0; vCount < stiReport.Dictionary.Variables.Count; vCount++)
                    //{
                    //    if (stiReport.Dictionary.Variables[vCount].Name.ToString() == "FamilyFilter")
                    //    {
                    //        variableFilter = true;
                    //        break; 
                    //    }
                    //}
                    //if (variableFilter == false)
                    //    stiReport.Dictionary.Variables.Add(new Stimulsoft.Report.Dictionary.StiVariable("", "FamilyFilter", "FamilyFilter", typeof(string), "", false, false));
                    //stiReport.Dictionary.Variables["FamilyFilter"].Value = familyFiltersql;
                    var tableSource = stiReport.DataSources["Family"] as Stimulsoft.Report.Dictionary.StiSqlSource;
                    if (tableSource != null)
                    {

                        if (tableSource.SqlCommand.Contains("@") || tableSource.SqlCommand.Contains("=") || tableSource.SqlCommand.Contains("where"))
                        {

                            tableSource.SqlCommand = tableSource.SqlCommand + " where family_id in( Select family_id from family(" + catalogID + ") " + familyFiltersql + ")";

                        }
                        else
                        {
                            tableSource.SqlCommand = "Select * from family( " + catalogID + ") " + familyFiltersql;
                        }


                    }
                }
            }
        }
        public void ProductFilter(int catalogID, StiReport stiReport)
        {
            Boolean dsPFamily = false;

            for (int dsCount = 0; dsCount < stiReport.Dictionary.DataSources.Count; dsCount++)
            {
                if (stiReport.Dictionary.DataSources[dsCount].Name == "Product Family")
                {
                    dsPFamily = true;
                    break;
                }
            }

            if (dsPFamily)//Advanced table  
            {
                var sqLstring = new StringBuilder();
                // TradingBell.CatalogStudio.AppLoader.DBConnection Ocon = new DBConnection();
                _SQLString = " SELECT PRODUCT_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + catalogID + " ";
                DataSet oDsProductFilter = CreateDataSet();
                if (oDsProductFilter.Tables[0].Rows.Count > 0 && oDsProductFilter.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                {
                    string sProductFilter = oDsProductFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(sProductFilter);
                    XmlNode rNode = xmlDOc.DocumentElement;

                    if (rNode != null && rNode.ChildNodes.Count > 0)
                    {
                        for (int i = 0; i < rNode.ChildNodes.Count; i++)
                        {
                            XmlNode tableDataSetNode = rNode.ChildNodes[i];

                            if (tableDataSetNode.HasChildNodes)
                            {
                                if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                                {
                                    tableDataSetNode.ChildNodes[2].InnerText = "=";
                                }
                                if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                                {
                                    tableDataSetNode.ChildNodes[0].InnerText = "0";
                                }
                                string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                                _SQLString = " SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE  ATTRIBUTE_ID = " + Convert.ToInt32(tableDataSetNode.ChildNodes[0].InnerText) + " ";
                                DataSet attribuetypeDs = CreateDataSet();
                                if (attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("TEX") || attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("DATE"))
                                {

                                    if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                    {
                                        sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATIONPDF](" + catalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                    }
                                    else
                                    {
                                        sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATIONPDF](" + catalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                    }
                                }
                                else if (attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("DECI") || attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("NUM"))
                                {
                                    if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                    {
                                        sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogID + ") WHERE  (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                    }
                                    else
                                    {
                                        sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + catalogID + ") WHERE  (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                    }
                                }


                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                            {

                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                            {
                                sqLstring.Append(" INTERSECT \n");
                            }
                            if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                            {
                                sqLstring.Append(" UNION \n");
                            }


                        }

                    }

                }
                string productFiltersql = sqLstring.ToString();
                // Boolean variableFilter = false;
                if (productFiltersql.Length > 0)
                {
                    string s = "\nWHERE CATALOG_ID=" + catalogID + " AND PRODUCT_ID IN\n" +
                          "(\n";// +
                    //"SELECT DISTINCT PRODUCT_ID\n" +
                    //"FROM [PRODUCT SPECIFICATION](" + catalogId +")\n"+
                    //"WHERE\n";
                    productFiltersql = s + productFiltersql + "\n)";
                    //for (int vCount = 0; vCount < stiReport.Dictionary.Variables.Count; vCount++)
                    //{
                    //    if (stiReport.Dictionary.Variables[vCount].Name.ToString() == "FamilyFilter")
                    //    {
                    //        variableFilter = true;
                    //        break; 
                    //    }
                    //}
                    //if (variableFilter == false)
                    //    stiReport.Dictionary.Variables.Add(new Stimulsoft.Report.Dictionary.StiVariable("", "FamilyFilter", "FamilyFilter", typeof(string), "", false, false));
                    //stiReport.Dictionary.Variables["FamilyFilter"].Value = familyFiltersql;
                    var tableSource = stiReport.DataSources["Product Family"] as Stimulsoft.Report.Dictionary.StiSqlSource;
                    if (tableSource != null)
                    {

                        if ((tableSource.SqlCommand.Contains("order by") != true) && (tableSource.SqlCommand.Contains("@") || tableSource.SqlCommand.Contains("=") || tableSource.SqlCommand.Contains("where")))
                        {

                            tableSource.SqlCommand = tableSource.SqlCommand + "where product_id in( Select product_id from [Product Family](" + catalogID + ") " + productFiltersql + ")";

                        }
                        else
                        {
                            tableSource.SqlCommand = "Select * from [Product Family](" + catalogID + ") " + productFiltersql;
                        }


                    }
                }
            }
        }
        public ActionResult GetReport(string id = "SimpleList")
        {
            // Create the report object and load data from xml file
            var report = new StiReport();
            // report.Load(Server.MapPath($"~/Content/ReportTemplates/{id}.mrt"));
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                StiReport frmStidesignReport = new StiReport();
                if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct")) == true)
                {
                    if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Report_Config.Config")) == true)
                    {
                        ImagePathXpc();

                        StiConfig.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Report_Config.Config"));
                        frmStiReport.ReportCacheMode = StiReportCacheMode.Auto;
                        // StiOptions.Engine.ReportCache.CachePath = _settingMembers.GetValue(SystemSettingsCollection.SettingsList.TEMPORARYPATH.ToString());
                        StiOptions.Engine.ImageCache.Enabled = false;
                        // StiOptions.Engine.ImageCache.CachePath = _settingMembers.GetValue(SystemSettingsCollection.SettingsList.TEMPORARYPATH.ToString());

                        frmStidesignReport.ReportUnit = Stimulsoft.Report.StiReportUnitType.Inches;
                        frmStidesignReport.Dictionary.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct"));
                        frmStidesignReport.Dictionary.Synchronize();
                        frmStidesignReport.Dictionary.Databases.Clear();
                        frmStidesignReport.Dictionary.Databases.Add(new Stimulsoft.Report.Dictionary.StiSqlDatabase("CatalogStudio", connectionString));
                        //_workingCatalogID1 = 3;
                        FamilyFilter(_workingCatalogID1, frmStidesignReport);
                        ProductFilter(_workingCatalogID1, frmStidesignReport);
                        frmStidesignReport.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        frmStidesignReport.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        frmStidesignReport.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        frmStidesignReport.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        frmStidesignReport.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        frmStidesignReport.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        try
                        {
                            frmStidesignReport.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        }
                        catch (Exception exception)
                        {
                            //_exception.Exceptions(exception); 
                        }
                        var attachmentPath = ConfigurationManager.AppSettings["AttachmentPath"];
                        frmStidesignReport.Dictionary.Variables["AttachmentPath"].Value = Server.MapPath(attachmentPath);
                    }
                    else
                    {
                        //Report_config not found
                        //_exception.ShowCustomMessage("E092", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    //MessageBox.Show("Dictionary File not Found");
                    //  Message("Error");
                }
                return StiMvcDesigner.GetReportResult(frmStidesignReport);
           
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-GetReportTemplate", exp);
                Console.WriteLine(exp.Message);
                return null;
            }

           
        }

        public ActionResult SaveReport()
        {

            try
            {
                string userName = User.Identity.Name;
                report = StiMvcDesigner.GetReportObject();
                var requestParams = StiMvcDesigner.GetRequestParams();
                //string packedReport = report.SavePackedReportToString();
                var savingReportName = requestParams.Designer.FileName;


                string path = string.Empty;
                string destinationPath;
                string filePath = string.Empty;

                var user_Name = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == userName).Select(y => y.c.Comments).FirstOrDefault().ToString();


                if (IsServerUpload.ToUpper() == "TRUE")
                {
                    destinationPath = pathDesignation + "\\Content\\ProductImages\\" + user_Name + "\\Customer Templates\\" + "Newly Design Template\\";

                    destinationPath = destinationPath.Replace("////", "\\\\");

                    destinationPath = destinationPath.Replace("/", "\\");
                }
                else
                {
                    destinationPath = Server.MapPath("~/Content/ProductImages/" + user_Name + "/Customer Templates" + "/Newly Design Template/");
                }




                if (!Directory.Exists(destinationPath))
                    Directory.CreateDirectory(destinationPath);
                report.Save(destinationPath + savingReportName);
                Session["previewDesignTemplate"] =
                    destinationPath + savingReportName;




                return StiMvcDesigner.SaveReportResult();
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-SaveDesignReportTemplate", exp);
                Console.WriteLine(exp.Message);
                return null;
            }
        }

        public ActionResult PreviewReport()
        {

            SaveDesignReportTemplate();
            var connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
            if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct")) == true)
            {
                if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Report_Config.Config")) == true)
                {
                    report.Load(Session["previewDesignTemplate"].ToString());
                    report.Dictionary.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct"));
                    report.Dictionary.Synchronize();
                    report.Dictionary.Databases.Clear();
                    report.Dictionary.Databases.Add(new Stimulsoft.Report.Dictionary.StiSqlDatabase("CatalogStudio", connectionString));
                    FamilyFilter(_workingCatalogID1, report);
                    ProductFilter(_workingCatalogID1, report);
                    report.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                    report.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                    report.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                    report.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                    report.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                    report.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                    try
                    {
                        report.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                    }
                    catch (Exception exception)
                    {
                    }
                    var attachmentPath = ConfigurationManager.AppSettings["AttachmentPath"];
                    report.Dictionary.Variables["AttachmentPath"].Value = Server.MapPath(attachmentPath);
                }

            }
           // return StiMvcDesigner.GetReportResult(report);
            return StiMvcDesigner.PreviewReportResult(report);

                //return StiMvcDesigner.PreviewReportResult(report);
        }

        public ActionResult DesignerEvent()
        {
            return StiMvcDesigner.DesignerEventResult();
        }
       
        public ActionResult DesignReportTemplate()
        {
            try
            {
                var connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                StiReport frmStidesignReport = new StiReport();
                if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct")) == true)
                {
                    if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Report_Config.Config")) == true)
                    {
                        ImagePathXpc();

                        StiConfig.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Report_Config.Config"));
                        frmStiReport.ReportCacheMode = StiReportCacheMode.Auto;
                        // StiOptions.Engine.ReportCache.CachePath = _settingMembers.GetValue(SystemSettingsCollection.SettingsList.TEMPORARYPATH.ToString());
                        StiOptions.Engine.ImageCache.Enabled = false;
                        // StiOptions.Engine.ImageCache.CachePath = _settingMembers.GetValue(SystemSettingsCollection.SettingsList.TEMPORARYPATH.ToString());

                        frmStidesignReport.ReportUnit = Stimulsoft.Report.StiReportUnitType.Inches;
                        frmStidesignReport.Dictionary.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct"));
                        frmStidesignReport.Dictionary.Synchronize();
                        frmStidesignReport.Dictionary.Databases.Clear();
                        frmStidesignReport.Dictionary.Databases.Add(new Stimulsoft.Report.Dictionary.StiSqlDatabase("CatalogStudio", connectionString));
                        //_workingCatalogID1 = 3;
                        FamilyFilter(_workingCatalogID1, frmStidesignReport);
                        ProductFilter(_workingCatalogID1, frmStidesignReport);
                        frmStidesignReport.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        frmStidesignReport.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        frmStidesignReport.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        frmStidesignReport.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        frmStidesignReport.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        frmStidesignReport.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        try
                        {
                            frmStidesignReport.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                        }
                        catch (Exception exception)
                        {
                            //_exception.Exceptions(exception); 
                        }
                        var attachmentPath = ConfigurationManager.AppSettings["AttachmentPath"];
                        frmStidesignReport.Dictionary.Variables["AttachmentPath"].Value = Server.MapPath(attachmentPath);
                    }
                    else
                    {
                        //Report_config not found
                        //_exception.ShowCustomMessage("E092", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    //MessageBox.Show("Dictionary File not Found");
                    //  Message("Error");
                }
                return StiMvcDesigner.GetReportTemplateResult(this.HttpContext,frmStidesignReport);
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-GetReportTemplate", exp);
                Console.WriteLine(exp.Message);
                return null;
            }

        }
      
        public ActionResult SaveDesignReportTemplate()
        {
            try
            {
                string userName = User.Identity.Name;
                report = StiMvcDesigner.GetReportObject();
                var requestParams = StiMvcDesigner.GetRequestParams();
                //string packedReport = report.SavePackedReportToString();
                var savingReportName = requestParams.Designer.FileName;


                string path = string.Empty;
                string destinationPath;
                string filePath = string.Empty;

                var user_Name = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == userName).Select(y => y.c.Comments).FirstOrDefault().ToString();


                if (IsServerUpload.ToUpper() == "TRUE")
                {
                    destinationPath = pathDesignation + "\\Content\\ProductImages\\" + user_Name + "\\Customer Templates\\" + "Newly Design Template\\";

                    destinationPath = destinationPath.Replace("////", "\\\\");

                    destinationPath = destinationPath.Replace("/", "\\");
                }
                else
                {
                    destinationPath = Server.MapPath("~/Content/ProductImages/" + user_Name + "/Customer Templates" + "/Newly Design Template/");
                }




                if (!Directory.Exists(destinationPath))
                    Directory.CreateDirectory(destinationPath);
                report.Save(destinationPath+ savingReportName);
                Session["previewDesignTemplate"] =
                    destinationPath + savingReportName;




                return StiMvcDesigner.SaveReportResult();
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-SaveDesignReportTemplate", exp);
                Console.WriteLine(exp.Message);
                return null;
            }
        }
        //public ActionResult GetDesignReportSnapshotPreview()
        //{
        //    SaveDesignReportTemplate();
        //    var connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        //    if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct")) == true)
        //    {
        //        if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Report_Config.Config")) == true)
        //        {
        //            report.Load(Session["previewDesignTemplate"].ToString());
        //            report.Dictionary.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct"));
        //            report.Dictionary.Synchronize();
        //            report.Dictionary.Databases.Clear();
        //            report.Dictionary.Databases.Add(new Stimulsoft.Report.Dictionary.StiSqlDatabase("CatalogStudio", connectionString));
        //            FamilyFilter(_workingCatalogID1, report);
        //            ProductFilter(_workingCatalogID1, report);
        //            report.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
        //            report.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
        //            report.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
        //            report.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
        //            report.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
        //            report.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
        //            try
        //            {
        //                report.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
        //            }
        //            catch (Exception exception)
        //            {
        //            }
        //            var attachmentPath = ConfigurationManager.AppSettings["AttachmentPath"];
        //            report.Dictionary.Variables["AttachmentPath"].Value = Server.MapPath(attachmentPath);
        //        }
        //        else
        //        {
        //        }
        //    }
        //    // return StiMvcViewerFx.GetReportSnapshotResult(report);
        //    return null;
        //}
        #endregion

        /// <summary>
        /// Run Template 
        /// </summary>
        #region RunTemplate
        private bool dsVariable;
        public ActionResult RunTemplatePreview()
        {
            try
            {
                showXml = "true";
                var connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct")))
                {
                    if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Report_Config.Config")))
                    {

                        report.Load(Session["RunTemplatePath"].ToString());
                        Session["Template"] = Session["RunTemplatePath"].ToString();
                        for (int dsCount = 0; dsCount < report.Dictionary.DataSources.Count; dsCount++)
                        {
                            if (report.Dictionary.DataSources[dsCount].Name == "Category")
                            {
                                dsVariable = true;
                                break;
                            }
                        }
                        if (dsVariable)
                        {
                            report.Dictionary.Databases.Clear();
                            report.Dictionary.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct"));
                            report.Dictionary.Synchronize();
                            report.Dictionary.Databases.Clear();
                            report.Dictionary.Databases.Add(new StiSqlDatabase("CatalogStudio", connectionString));
                            FamilyFilter(_workingCatalogID1, report);
                            ProductFilter(_workingCatalogID1, report);
                            report.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            report.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            report.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            report.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            report.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            report.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            try
                            {
                                report.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            }
                            catch (Exception exception)
                            {
                                //_exception.Exceptions(exception); 
                            }
                            var attachmentPath = ConfigurationManager.AppSettings["AttachmentPath"];
                            report.Dictionary.Variables["AttachmentPath"].Value = Server.MapPath(attachmentPath);

                        }
                        else
                        {
                            //Report_config not found
                            //_exception.ShowCustomMessage("E092", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            RunTemplate(ref (report), "");

                        }
                    }


                }

                //return StiMvcViewerFx.GetReportSnapshotResult(report);
                return null;
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-RunTemplatePreview", exp);
                Console.WriteLine(exp.Message);
                return null;
            }
        }
        public StiReport RenderReport()
        {
            try
            {
                showXml = "true";
                var connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct")))
                {
                    if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Report_Config.Config")))
                    {

                        report.Load(Session["TemplatePath"].ToString());
                        for (int dsCount = 0; dsCount < report.Dictionary.DataSources.Count; dsCount++)
                        {
                            if (report.Dictionary.DataSources[dsCount].Name == "Category")
                            {
                                dsVariable = true;
                                break;
                            }
                        }
                        if (dsVariable)
                        {
                            report.Dictionary.Databases.Clear();
                            report.Dictionary.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct"));
                            report.Dictionary.Synchronize();
                            report.Dictionary.Databases.Clear();
                            report.Dictionary.Databases.Add(new StiSqlDatabase("CatalogStudio", connectionString));
                            FamilyFilter(_workingCatalogID1, report);
                            ProductFilter(_workingCatalogID1, report);
                            report.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            report.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            report.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            report.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            report.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            report.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            try
                            {
                                report.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value = _workingCatalogID1.ToString();
                            }
                            catch (Exception exception)
                            {
                                //_exception.Exceptions(exception); 
                            }
                            var attachmentPath = ConfigurationManager.AppSettings["AttachmentPath"];
                            report.Dictionary.Variables["AttachmentPath"].Value = Server.MapPath(attachmentPath);

                        }
                        else
                        {
                            //Report_config not found
                            //_exception.ShowCustomMessage("E092", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            RunTemplate(ref (report), "Report");

                        }
                    }


                }
                //   Session["Template"] = report;
                return report;
            }
            catch (Exception exp)
            {
                _logger.Error("Error at XpressCatalog-RunTemplatePreview", exp);
                Console.WriteLine(exp.Message);
                return null;
            }
        }
        public void RunTemplate(ref StiReport simpleReport, string report)
        {
            Opensimple = true;
            _runedTemplate = true;
            // openedReport = simpleReport;
            //objXpressSimpleCatalog.CustomFields(ref(simpleReport));
            SimplecatalogOpenReport(ref (simpleReport), report);
        }
        //public FileResult ExportResult()
        //{
        //    // Return the exported report file
        //    return StiMvcViewerFx.ExportReportResult(this.Request);
        //}

        public void DeleteExistingFile(string fileName)
        {
            if (System.IO.File.Exists(fileName))
            {
                // Use a try block to catch IOExceptions, to
                // handle the case of the file already being
                // opened by another process.
                try
                {
                    System.IO.File.Delete(fileName);
                }
                catch (System.IO.IOException e)
                {
                    _logger.Error("Error at XpressCatalog-DeleteExistingFile", e);
                    Console.WriteLine(e.Message);
                    return;
                }
            }
        }




        public JsonResult ExportResult(string format)
        {
            try
            {
                // Return the exported report file

                // report = Session["Template"] as StiReport;
                report = RenderReport();
                var CustomerFolder =
                   _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId,
                       (a, b) => new { a, b })
                       .Where(z => z.b.User_Name == User.Identity.Name)
                       .Select(x => x.a.Comments)
                       .FirstOrDefault();
                report.CacheTotals = false;
                report.CacheAllData = false;
                report.ReportCacheMode = StiReportCacheMode.Off;
                report.Pages.CacheMode = false;
                StiOptions.Engine.ImageCache.Enabled = false;
                report.Dictionary.Synchronize();
                var pdfSettings = new Stimulsoft.Report.Export.StiPdfExportSettings
                {
                    EmbeddedFonts = true,
                    ImageQuality = 100,
                    ImageResolution = 600,
                    Compressed = true,
                    UseUnicode = true,
                    ImageCompressionMethod = Stimulsoft.Report.Export.StiPdfImageCompressionMethod.Jpeg
                };
                //report.CacheTotals = false;
                //report.CacheAllData = false;
                //report.ReportCacheMode = StiReportCacheMode.Off;
                //report.Pages.CacheMode = false;
                var attachmentPath = ConfigurationManager.AppSettings["AttachmentPath"];

                report.Dictionary.Variables["AttachmentPath"].Value = Server.MapPath(attachmentPath + "\\" + CustomerFolder);
                StiOptions.Engine.ReportCache.AmountOfProcessedPagesForStartGCCollect = 2; // default - 20
                StiOptions.Engine.ReportCache.AmountOfQuickAccessPages = 5; // default - 50
                report.Render();
                //Remove_Pdf();
                //if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\"))
                //{
                //    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\");
                //}
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\"))
                {
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\");
                }
                Remove_Pdf();
                string fileName = "";
                Stream ostream = null;

                // To remove previous file




                switch (format)
                {
                    case "mdc":
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".mdc");



                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.Document, ostream);
                        ostream.Dispose();
                        ostream.Close();
                        break;
                    case "pdf":
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".pdf");
                        DeleteExistingFile(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName);
                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.Pdf, ostream, pdfSettings);
                        ostream.Dispose();
                        ostream.Close();
                        break;
                    case "xps":
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".xps");
                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.Xps, ostream);
                        ostream.Dispose();
                        ostream.Close();
                        break;
                    case "ppt":
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".ppt");
                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.Ppt2007, ostream);
                        ostream.Dispose();
                        ostream.Close();
                        break;
                    case "html":
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".html");
                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.Html, ostream);
                        ostream.Dispose();
                        ostream.Close();
                        break;
                    case "txt":
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".txt");
                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.Text, ostream);
                        ostream.Dispose();
                        ostream.Close();
                        break;
                    case "rtf":
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".rtf");
                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.Rtf, ostream);
                        ostream.Dispose();
                        ostream.Close();
                        break;
                    case "doc":
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".doc");
                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.Word2007, ostream);
                        ostream.Dispose();
                        ostream.Close();
                        break;
                    case "xls":
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".xls");
                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.Excel, ostream);
                        ostream.Dispose();
                        ostream.Close();
                        break;
                    case "csv":
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".csv");
                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.Csv, ostream);
                        ostream.Dispose();
                        ostream.Close();
                        break;
                    case "jpg":
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".zip");
                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.ImageJpeg, ostream);
                        ostream.Dispose();
                        ostream.Close();
                        break;
                    default:
                        fileName = report.ReportFile.Split('\\')[report.ReportFile.Split('\\').Length - 1].Replace(".mrt", ".pdf");
                        ostream = new FileStream(AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName, FileMode.OpenOrCreate, FileAccess.Write);
                        report.ExportDocument(StiExportFormat.Pdf, ostream, pdfSettings);
                        ostream.Dispose();
                        ostream.Close();
                        break;

                }
                var filePath = AppDomain.CurrentDomain.BaseDirectory + @"ExportPDF\" + fileName;
                string url = "DownloadPDFTemplate.ashx?FileName=" + fileName + "&Path=PDF&UserName=" + User.Identity.Name;

                return Json(url, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                _logger.Error("Error at XpressCatalog-ExportResult", ex);
                try
                {
                    return null;//RedirectToAction("XpressCatalog", "App");
                }
                catch (Exception ex1)
                {
                    return null;
                }
            }

        }
        public void Remove_Pdf()
        {
            try
            {
                string path = "";
                path = Server.MapPath("Export//");
                string[] gPdf = Directory.GetFiles(System.Web.Hosting.HostingEnvironment.MapPath("~/ExportPDF/"), "*");
                for (int i = 0; i < gPdf.Count(); i++)
                {
                    var fileinfo = new FileInfo(gPdf[i]);
                    if (fileinfo.Exists)
                    {
                        int noOfDays;
                        int.TryParse(ConfigurationManager.AppSettings["PDFRemoveInDays"].ToString(), out noOfDays);
                        if ((DateTime.UtcNow - fileinfo.LastAccessTimeUtc.Date).Days > noOfDays)
                        {
                            System.IO.File.Delete(gPdf[i]);
                        }
                        // 
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error at XpressCatalog-Remove_Pdf", ex);
            }
        }
        #endregion
        /// <summary>
        ///Common Methods For Simple Templates
        /// </summary>
        #region Methods
        private void SimplecatalogOpenReport(ref StiReport simpleReport, string report)
        {
            int reportProjectID = 0;
            try
            {
                reportProjectID = Convert.ToInt32(simpleReport.Dictionary.Variables["ProjectID"].Value);
                ProjectID = reportProjectID;
            }
            catch (NullReferenceException)
            {
                Message = "ProjectID Not Found";
            }
            var ocnl = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            {
                _SQLString =
                    "SELECT XPRESSCATALOG_CONFIG FROM TB_PROJECT WHERE PROJECT_TYPE = 3 AND PROJECT_ID =" +
                    reportProjectID;
            };
            DataSet valDs = CreateDataSet();

            //if (!Directory.Exists(Server.MapPath("/Content/XPressConfig.xml")))
            //{
            //    Directory.CreateDirectory(Server.MapPath("Content/XPressConfig.xml"));
            //}
            if (showXml == "true")
            {
                string dataString = string.Empty;
                using (var fileStream = new FileStream(Server.MapPath("/Views/App/XpressCatalog/Content/XPressConfig.xml"), FileMode.Create))
                {
                    var streamWriter = new StreamWriter(fileStream);
                    if (valDs.Tables[0].Rows.Count == 0)
                    {
                        Message = "Project Not Found!";
                        return;
                    }
                    dataString = valDs.Tables[0].Rows[0].ItemArray[0].ToString();
                    dataString = dataString.Replace('\n', ' ');
                    dataString = dataString.Replace('\n', ' ');
                    streamWriter.Write(dataString);
                    streamWriter.Close();

                }

                var dataRuleDs = new DataSet();
                if (dataString != "")
                {
                    dataRuleDs.ReadXml(Server.MapPath("~/Views/App/XpressCatalog/Content/XPressConfig.xml"));
                    sysAttr = dataRuleDs.Tables[0].Rows[0].ItemArray[25].ToString();
                    cusAttr = dataRuleDs.Tables[0].Rows[0].ItemArray[26].ToString();
                    string multipleTables = "", referenceTables = "";
                    if (dataRuleDs.Tables[0].Columns.Count == 29)
                    {
                        multipleTables = dataRuleDs.Tables[0].Rows[0].ItemArray[27].ToString();
                        referenceTables = dataRuleDs.Tables[0].Rows[0].ItemArray[28].ToString();
                    }
                    Opensimple = true;
                    //if (Opensimple && _runedTemplate == false)
                    //{ }
                    //else
                    //{
                }
                CustomFields(ref (simpleReport), report);
                //}
            }
        }
        private void SelectAttributes(string sysAttr, string cusAttr, string multipleTables, string referecneTables)
        {
            if (multipleTables.Length > 0)
            {
                string[] multipleTablesAttr = multipleTables.Split(',');
            }
            if (referecneTables.Length > 0)
            {
                string[] referecneTablesAttr = referecneTables.Split(',');
            }

            if (cusAttr.Length > 0)
            {
                string[] customAttr = cusAttr.Split(',');

            }
            if (sysAttr.Length > 0)
            {
                string[] systemAttr = sysAttr.Split(',');
            }
        }
        private void CustomFields(ref StiReport simpleReport, string report)
        {
            var CustomerFolder =
                      _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId,
                          (a, b) => new { a, b })
                          .Where(z => z.b.User_Name == User.Identity.Name)
                          .Select(x => x.a.Comments)
                          .FirstOrDefault();
            string custAttrName = "";
            SelectAttributes(sysAttr, cusAttr, multipleTables, referenceTables);
            custAttrName = cusAttr;
            var selAttrName = new string[0];
            if (custAttrName != "")
            {
                if (custAttrName.Contains(",Product ID"))
                {
                    custAttrName = custAttrName.Replace(",Product ID", "");
                }
                if (custAttrName.Contains("Product ID,"))
                {
                    custAttrName = custAttrName.Replace("Product ID,", "");
                }
                if (custAttrName.ToLower().Contains(",product_id"))
                {
                    custAttrName = custAttrName.ToLower().Replace(",product_id", "");
                }
                if (custAttrName.ToLower().Contains("product_id,"))
                {
                    custAttrName = custAttrName.ToLower().Replace("product_id,", "");
                }
                selAttrName = custAttrName.Split(',');
            }
            DataSet flatDataset = FunctionAttr(selAttrName);
            DataSet flatFamily = FamilyFilterFlatTable(flatDataset);
            DataSet finalDs = ProductFilterFlatTable(flatFamily);
            finalDs.DataSetName = "CatalogStudio";
            if (finalDs.Tables.Count > 0)
            {
                DataTable dtFamilyDetails = finalDs.Tables[0].DefaultView.ToTable(true, "FAMILY_ID", "FAMILY_NAME");
                for (int i = 0; i < finalDs.Tables.Count; i++)
                {
                    if (i == 0)
                        finalDs.Tables[0].TableName = "Catalog Objects";
                    else if (i == 1)
                        finalDs.Tables[1].TableName = "Multiple Tables";
                    else if (i >= 2)
                        if (finalDs.Tables[i].Rows.Count > 0)
                            finalDs.Tables[i].TableName = "Reference Tables" + finalDs.Tables[i].Rows[0][1].ToString();
                }
                finalDs.Tables.Add(dtFamilyDetails);
                finalDs.Tables[finalDs.Tables.Count - 1].TableName = "Family Details";
                for (int mainCount = 0; mainCount < finalDs.Tables.Count; mainCount++)
                {
                    for (int cCount = 0; cCount < finalDs.Tables[mainCount].Columns.Count; cCount++)
                    {
                        try
                        {
                            finalDs.Tables[0].Columns[cCount].Caption = finalDs.Tables[0].Columns[cCount].ColumnName;
                            finalDs.Tables[0].Columns[cCount].ColumnName =
                                ReplaceCharacters(finalDs.Tables[0].Columns[cCount].ColumnName);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
            DataSet finalDataSource = PrefixSufixFlatTable(finalDs, selAttrName);
            simpleReport.RegData(finalDataSource);
            ImagePathXpc();
            simpleReport.Dictionary.Variables.Add(new StiVariable("", "AttachmentPath", "AttachmentPath", "", typeof(string), "", true, false));
            string attachmentPath;
            var folderName = _dbcontext.Customers.Join(_dbcontext.Customer_User, cs => cs.CustomerId, cu => cu.CustomerId, (cs, cu) => new { cs, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.cs.Comments).FirstOrDefault().ToString();
            if (string.IsNullOrEmpty(folderName))
            {
                folderName = "Questudio";
            }
            if (report == "Report")
            {
                attachmentPath = ConfigurationManager.AppSettings["AttachmentPath"];
            }
            else { attachmentPath = ConfigurationManager.AppSettings["PreviewAttachmentPath"] + "/" + folderName + "/"; }
            if (!Directory.Exists(attachmentPath))
                Directory.CreateDirectory(attachmentPath);
            simpleReport.Dictionary.Variables["AttachmentPath"].Value = Server.MapPath(attachmentPath);
            simpleReport.Dictionary.Variables.Add(new StiVariable("", "ProjectID", "ProjectID", "", typeof(string), "", true, false));
            simpleReport.Dictionary.Variables["ProjectID"].Value = ProjectID.ToString(CultureInfo.InvariantCulture);
        }
        private DataSet FunctionAttr(string[] attrNames)
        {
            if (_flagOpenReportPreview)
            {
                CatalogID = _workingCatalogID;
            }
            else
            {
                CatalogID = 0;
            }
            //var attrList = new int[attrNames.Length];
            var attrList = new string[attrNames.Length];
            for (int incVal = 0; incVal <= attrNames.Length - 1; incVal++)
                //attrList[incVal] = Convert.ToInt32(attrNames[incVal]);
                attrList[incVal] = attrNames[incVal];
            if (string.IsNullOrEmpty(multipleTables))
            {
                multipleTables = ",";
            }
            if (string.IsNullOrEmpty(referenceTables))
            {
                referenceTables = ",";
            }
            DataSet attrDtds1 = CatalogPdfx(CatalogID, 0, attrList, sysAttr, ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString, "", multipleTables.Substring(1), referenceTables.Substring(1));
            var attrDtds2 = new DataSet();
            for (int i = 0; i < attrDtds1.Tables.Count; i++)
            {
                attrDtds2.Tables.Add(attrDtds1.Tables[i].Copy());
            }
            DataSet attrDtds = ApplyStyleFormat(attrDtds2);
            return attrDtds;
        }
        private DataSet FamilyFilterFlatTable(DataSet flatDataset)
        {
            CatalogID = _workingCatalogID;
            var sqLstring = new StringBuilder();
            _SQLString = " SELECT FAMILY_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + CatalogID + " ";
            DataSet oDsFamilyFilter = CreateDataSet();
            if (oDsFamilyFilter.Tables[0].Rows.Count > 0 && oDsFamilyFilter.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
            {
                string sFamilyFilter = oDsFamilyFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                var xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(sFamilyFilter);
                XmlNode rNode = xmlDOc.DocumentElement;
                if (rNode != null && rNode.ChildNodes.Count > 0)
                {
                    for (int i = 0; i < rNode.ChildNodes.Count; i++)
                    {
                        XmlNode tableDataSetNode = rNode.ChildNodes[i];
                        if (tableDataSetNode.HasChildNodes)
                        {
                            if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[2].InnerText = "=";
                            }
                            if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[0].InnerText = "0";
                            }
                            string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                            if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                            {
                                sqLstring.Append("SELECT DISTINCT FAMILY_ID FROM [FAMILY DESCRIPTION](" + CatalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                            }
                            else
                            {
                                sqLstring.Append("SELECT DISTINCT FAMILY_ID FROM [FAMILY DESCRIPTION](" + CatalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                            }
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                        {

                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                        {
                            sqLstring.Append(" INTERSECT \n");
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                        {
                            sqLstring.Append(" UNION \n");
                        }
                    }
                }
            }
            string familyFiltersql = sqLstring.ToString();
            if (familyFiltersql.Length > 0)
            {
                string s = "SELECT FAMILY_ID FROM FAMILY(" + CatalogID + ") WHERE FAMILY_ID IN\n" +
                      "(\n";
                familyFiltersql = s + familyFiltersql + "\n)";
                _SQLString = familyFiltersql;
                oDsFamilyFilter = CreateDataSet();
                for (int j = 0; j < flatDataset.Tables[0].Rows.Count; j++)
                {
                    DataRow odr = flatDataset.Tables[0].Rows[j];
                    bool available = oDsFamilyFilter.Tables[0].Rows.Cast<DataRow>().Count(dr => dr["FAMILY_ID"].ToString() == odr["Family_ID"].ToString() || dr["FAMILY_ID"].ToString() == odr["SubFamily_ID"].ToString()) > 0;
                    if (available == false)
                    {
                        odr.Delete();
                        flatDataset.AcceptChanges();
                        j = -1;
                    }
                }
            }
            return flatDataset;
        }
        private DataSet ProductFilterFlatTable(DataSet flatDataset)
        {
            var sqLstring = new StringBuilder();
            _SQLString = " SELECT PRODUCT_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + CatalogID + " ";

            DataSet oDsProductFilter = CreateDataSet();
            if (oDsProductFilter.Tables[0].Rows.Count > 0 && oDsProductFilter.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
            {
                string sProductFilter = oDsProductFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                var xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(sProductFilter);
                XmlNode rNode = xmlDOc.DocumentElement;
                if (rNode != null && rNode.ChildNodes.Count > 0)
                {
                    for (int i = 0; i < rNode.ChildNodes.Count; i++)
                    {
                        XmlNode tableDataSetNode = rNode.ChildNodes[i];
                        if (tableDataSetNode.HasChildNodes)
                        {
                            if (tableDataSetNode.ChildNodes[2].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[2].InnerText = "=";
                            }
                            if (tableDataSetNode.ChildNodes[0].InnerText == " ")
                            {
                                tableDataSetNode.ChildNodes[0].InnerText = "0";
                            }
                            string stringval = tableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                            _SQLString = " SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE  ATTRIBUTE_ID = " + Convert.ToInt32(tableDataSetNode.ChildNodes[0].InnerText) + " ";
                            DataSet attribuetypeDs = CreateDataSet();
                            if (attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("TEX") || attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("DATE"))
                            {
                                if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + CatalogID + ") WHERE  (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                }
                                else
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + CatalogID + ") WHERE (STRING_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                }
                            }
                            else if (attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("DECI") || attribuetypeDs.Tables[0].Rows[0].ItemArray[0].ToString().ToUpper().Contains("NUM"))
                            {
                                if (tableDataSetNode.ChildNodes[4].InnerText != "NONE")
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + CatalogID + ") WHERE (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                }
                                else
                                {
                                    sqLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + CatalogID + ") WHERE  (NUMERIC_VALUE " + tableDataSetNode.ChildNodes[2].InnerText + " '" + stringval + "' AND ATTRIBUTE_ID = " + tableDataSetNode.ChildNodes[0].InnerText + ")" + "\n");
                                }
                            }
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "NONE")
                        {

                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "AND")
                        {
                            sqLstring.Append(" INTERSECT \n");
                        }
                        if (tableDataSetNode.ChildNodes[4].InnerText == "OR")
                        {
                            sqLstring.Append(" UNION \n");
                        }
                    }
                }
            }
            string productFiltersql = sqLstring.ToString();
            if (productFiltersql.Length > 0)
            {
                string s = "SELECT PRODUCT_ID FROM [PRODUCT FAMILY](" + CatalogID + ") WHERE PRODUCT_ID IN\n" +
                      "(\n";
                productFiltersql = s + productFiltersql + "\n)";

                _SQLString = productFiltersql;
                oDsProductFilter = CreateDataSet();
                var dataDs = new DataSet();
                DataTable table = flatDataset.Tables[0].Clone();
                dataDs.Tables.Add(table);
                foreach (DataRow odr in from DataRow odr in flatDataset.Tables[0].Rows from DataRow dr in oDsProductFilter.Tables[0].Rows where dr["PRODUCT_ID"].ToString() == odr["Product_ID"].ToString() select odr)
                {
                    table.ImportRow(odr);
                }
                flatDataset.Tables.Clear();
                flatDataset = dataDs.Copy();
            }
            return flatDataset;

        }
        private DataSet PrefixSufixFlatTable(DataSet finalDs, IEnumerable<string> selAttrName)
        {
            var oDsAttributeDatarule = new DataSet();

            foreach (string t in selAttrName)
            {
                oDsAttributeDatarule.Clear();
                _SQLString = string.Empty;
                _SQLString = "SELECT ATTRIBUTE_DATARULE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = '" + t + "' ";
                oDsAttributeDatarule = CreateDataSet();
                if (oDsAttributeDatarule != null)
                {
                    if (oDsAttributeDatarule.Tables[0].Rows.Count > 0 && oDsAttributeDatarule.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        string sAttributeDataRule = oDsAttributeDatarule.Tables[0].Rows[0].ItemArray[0].ToString();
                        var xmlDOc = new XmlDocument();
                        xmlDOc.LoadXml(sAttributeDataRule);
                        XmlNode rNode = xmlDOc.DocumentElement;
                        if (rNode != null && rNode.ChildNodes.Count > 0)
                        {
                            for (int i = 0; i < rNode.ChildNodes.Count; i++)
                            {
                                XmlNode tableDataSetNode = rNode.ChildNodes[i];
                                if (tableDataSetNode.HasChildNodes)
                                {
                                    string prefix = tableDataSetNode.ChildNodes[0].InnerText;
                                    string sufix = tableDataSetNode.ChildNodes[1].InnerText;
                                    string condition = tableDataSetNode.ChildNodes[2].InnerText;
                                    string customValue = tableDataSetNode.ChildNodes[3].InnerText;
                                    _SQLString = string.Empty;
                                    _SQLString = "SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = '" + t + "' ";
                                    DataSet attrName = CreateDataSet();
                                    string columnName = ReplaceCharacters(attrName.Tables[0].Rows[0].ItemArray[0].ToString());
                                    if (condition.Length != 0)
                                    {
                                        foreach (DataRow dr in finalDs.Tables[0].Rows)
                                        {
                                            if (condition.Length == 0)
                                            {
                                                if (dr[columnName].ToString().Length != 0)
                                                {
                                                    dr[columnName] = prefix + dr[columnName] + sufix;
                                                }
                                            }
                                            if (condition.Length != 0)
                                            {
                                                if (dr[columnName].ToString() == condition)
                                                {
                                                    if (dr[columnName].ToString().Length == 0)
                                                        if (condition == "Empty" || condition == "Null")
                                                        {
                                                            dr[columnName] = customValue;
                                                        }
                                                    if (dr[columnName].ToString().Length > 0)
                                                    {
                                                        dr[columnName] = prefix + customValue + sufix;
                                                    }
                                                }
                                                else
                                                {
                                                    if (dr[columnName].ToString().Length != 0)
                                                    {
                                                        dr[columnName] = prefix + dr[columnName] + sufix;
                                                    }
                                                    else
                                                    {
                                                        dr[columnName] = customValue;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (DataRow dr in finalDs.Tables[0].Rows)
                                        {
                                            try
                                            {
                                                if (dr[columnName].ToString().Length > 0)
                                                    dr[columnName] = prefix + dr[columnName] + sufix;
                                            }
                                            catch (Exception)
                                            { //_exception.Exceptions(exception); 
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            var selectedMultipleTables = multipleTables.Substring(1).Split(',');
            foreach (string t in selectedMultipleTables)
            {
                if (oDsAttributeDatarule != null)
                {
                    oDsAttributeDatarule.Clear();
                }
                _SQLString = string.Empty;
                _SQLString = "SELECT ATTRIBUTE_DATARULE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = '" + t + "' ";
                oDsAttributeDatarule = CreateDataSet();
                if (oDsAttributeDatarule != null)
                {
                    if (oDsAttributeDatarule.Tables[0].Rows.Count > 0 && oDsAttributeDatarule.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        string sAttributeDataRule = oDsAttributeDatarule.Tables[0].Rows[0].ItemArray[0].ToString();
                        var xmlDOc = new XmlDocument();
                        xmlDOc.LoadXml(sAttributeDataRule);
                        XmlNode rNode = xmlDOc.DocumentElement;
                        if (rNode != null && rNode.ChildNodes.Count > 0)
                        {
                            for (int i = 0; i < rNode.ChildNodes.Count; i++)
                            {
                                XmlNode tableDataSetNode = rNode.ChildNodes[i];
                                if (tableDataSetNode.HasChildNodes)
                                {
                                    string prefix = tableDataSetNode.ChildNodes[0].InnerText;
                                    string sufix = tableDataSetNode.ChildNodes[1].InnerText;
                                    string condition = tableDataSetNode.ChildNodes[2].InnerText;
                                    string customValue = tableDataSetNode.ChildNodes[3].InnerText;
                                    _SQLString = string.Empty;
                                    _SQLString = "SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = '" + t + "' ";
                                    DataSet attrName = CreateDataSet();
                                    string columnName = ReplaceCharacters(attrName.Tables[0].Rows[0].ItemArray[0].ToString());
                                    if (condition.Length != 0)
                                    {
                                        foreach (DataRow dr in finalDs.Tables[1].Rows)
                                        {
                                            if (condition.Length == 0)
                                            {
                                                if (dr[columnName].ToString().Length != 0)
                                                {
                                                    dr[columnName] = prefix + dr[columnName] + sufix;
                                                }
                                            }
                                            if (condition.Length != 0)
                                            {
                                                if (dr[columnName].ToString() == condition)
                                                {
                                                    if (dr[columnName].ToString().Length == 0)
                                                        if (condition == "Empty" || condition == "Null")
                                                        {
                                                            dr[columnName] = customValue;
                                                        }
                                                    if (dr[columnName].ToString().Length > 0)
                                                    {
                                                        dr[columnName] = prefix + customValue + sufix;
                                                    }
                                                }
                                                else
                                                {
                                                    if (dr[columnName].ToString().Length != 0)
                                                    {
                                                        dr[columnName] = prefix + dr[columnName] + sufix;
                                                    }
                                                    else
                                                    {
                                                        dr[columnName] = customValue;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (DataRow dr in finalDs.Tables[1].Rows)
                                        {
                                            try
                                            {
                                                if (dr[columnName].ToString().Length > 0)
                                                    dr[columnName] = prefix + dr[columnName] + sufix;
                                            }
                                            catch (Exception)
                                            { //_exception.Exceptions(exception); 
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return finalDs;

        }
        public DataSet CatalogPdfx(int catalogID, int familyID, string[] attributeList, string SysAttrIds, string connvalue, string catId, string multipleTables, string referenceTables)
        {
            if (catalogID == 0)
            {
                catalogID = Convert.ToInt32(System.Web.HttpContext.Current.Session["CURRENTCATALOGID"]);
            }


            string categor_id = string.Empty;
            string family_id = string.Empty;
            string product_id = string.Empty;

            string type = System.Web.HttpContext.Current.Session["PDFTYPE"].ToString();


            if (type == "ALL")
            {
                XpressCatalog objXpressCatalog = new XpressCatalog();

                var projectId = System.Web.HttpContext.Current.Session["CURRENTPROJECTID"];
                TB_PROJECT projectdetails = _dbcontext.TB_PROJECT.Find(projectId);
                var pdfDetails = projectdetails.XPRESSCATALOG_CONFIG;
                List<PDF_PROJECT_SETTINGS> objprojectSettings = objXpressCatalog.ProjectXmlDeserializefunction(pdfDetails);

                categor_id = objprojectSettings[0].CUSTOM_CATEGORY_ID;
                family_id = objprojectSettings[0].CUSTOM_FAMILY_ID;

                if (categor_id == "ALL")
                    categor_id = "";
                if (family_id == "ALL")
                    family_id = "";
            }
            else if (type == "CATEGORY")
            {


                if (System.Web.HttpContext.Current.Session["PDFCATRGORYID"] == "EMPTY" || System.Web.HttpContext.Current.Session["PDFCATRGORYID"] == null)
                {
                    categor_id = "";
                }
                else
                {
                    categor_id = System.Web.HttpContext.Current.Session["PDFCATRGORYID"].ToString();
                }



                if (System.Web.HttpContext.Current.Session["PDFFAMILYID"] == "EMPTY")
                {
                    family_id = "";
                }
                else
                {
                    family_id = System.Web.HttpContext.Current.Session["PDFFAMILYID"].ToString();
                }
            }
            else if (type == "FAMILY")
            {


                if (System.Web.HttpContext.Current.Session["PDFCATRGORYID"] == "EMPTY")
                {
                    categor_id = "";
                }
                else
                {
                    categor_id = System.Web.HttpContext.Current.Session["PDFCATRGORYID"].ToString();
                }



                if (System.Web.HttpContext.Current.Session["PDFFAMILYID"] == "EMPTY")
                {
                    family_id = "";
                }
                else
                {
                    family_id = System.Web.HttpContext.Current.Session["PDFFAMILYID"].ToString();
                }
            }
            else if (type == "PRODUCT")
            {


                if (System.Web.HttpContext.Current.Session["PDFCATRGORYID"] == "EMPTY")
                {
                    categor_id = "";
                }
                else
                {
                    categor_id = System.Web.HttpContext.Current.Session["PDFCATRGORYID"].ToString();
                }



                if (System.Web.HttpContext.Current.Session["PDFFAMILYID"] == "EMPTY")
                {
                    family_id = "";
                }
                else
                {
                    family_id = System.Web.HttpContext.Current.Session["PDFFAMILYID"].ToString();
                }
            }


            if (System.Web.HttpContext.Current.Session["PDFPRODUCTID"] == "EMPTY")
            {
                product_id = "";
            }
            else
            {
                product_id = System.Web.HttpContext.Current.Session["PDFPRODUCTID"].ToString();
            }




            if (string.IsNullOrEmpty(type))
            {
                type = "ALL";
            }
            if (string.IsNullOrEmpty(categor_id) || categor_id == "undefined")
            {
                categor_id = "";
            }
            if (string.IsNullOrEmpty(family_id) || family_id == "undefined")
            {
                family_id = "";
            }
            if (string.IsNullOrEmpty(product_id) || product_id == "undefined")
            {
                product_id = "";
            }

            //catalogID = 3;
            var joinedDs = new DataSet();
            string custAttrName = string.Empty;
            if (attributeList.Length > 0)
            {
                custAttrName = attributeList.Select(a => a.ToString()).Aggregate((a, b) => a + "," + b);
            }
            else
            {
                custAttrName = "0";
            }
            using (var sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                sqlConn.Open();
                try
                {
                    int seesionId = 1;
                    seesionId += 1;
                    var cmd = new SqlCommand("STP_CATALOGSTUDIO5_StimulSoftFlatDatas", sqlConn)
                    {
                        CommandTimeout = 0,
                        CommandType = CommandType.StoredProcedure
                    };
                    cmd.Parameters.AddWithValue("@SESSID", "CS1");
                    cmd.Parameters.AddWithValue("@CATALOG_ID", catalogID);
                    cmd.Parameters.AddWithValue("@ATTRIBUTE_IDS", custAttrName);
                    cmd.Parameters.AddWithValue("@SYSATTR_IDS", SysAttrIds);
                    cmd.Parameters.AddWithValue("@CATEGORY_ID", categor_id);
                    cmd.Parameters.AddWithValue("@PATH", "d:\\");

                    cmd.Parameters.AddWithValue("@PRODUCT_IDS", product_id);
                    cmd.Parameters.AddWithValue("@TYPE", type);
                    cmd.Parameters.AddWithValue("@FAMILY_IDS", family_id);



                    var da = new SqlDataAdapter(cmd);
                    da.Fill(joinedDs);
                    joinedDs = HtmlSanitizer.Sanitize(joinedDs);
                }
                catch (Exception ex)
                {
                    string msg = ex.Message;
                }
                //int ss = 2000;
                //for (int i = 3274; i > ss; i--)
                //{
                //    joinedDs.Tables[0].Rows.RemoveAt(i);
                //}


                #region "Decimal Place construction"
                if (joinedDs.Tables.Count > 0)
                {
                    foreach (DataRow dr in joinedDs.Tables[0].Rows)
                    {
                        {
                            if (dr.Table.Columns.Contains("PRODUCT_ID"))
                            {
                                if (dr["PRODUCT_ID"].ToString() != null && dr["PRODUCT_ID"].ToString() != "")
                                {
                                    var cmd = new SqlCommand("select ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE  from TB_PROD_SPECS aps join TB_ATTRIBUTE b on aps.ATTRIBUTE_ID=b.ATTRIBUTE_ID where PRODUCT_ID=" + dr["PRODUCT_ID"].ToString() + " and ATTRIBUTE_DATATYPE like 'Num%' union select ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE  from TB_FAMILY_SPECS aps join TB_ATTRIBUTE b on aps.ATTRIBUTE_ID=b.ATTRIBUTE_ID where FAMILY_ID=" + dr["FAMILY_ID"].ToString() + " and ATTRIBUTE_DATATYPE like 'Num%'", sqlConn);
                                    DataSet temp = new DataSet();
                                    var da = new SqlDataAdapter(cmd);
                                    da.Fill(temp);
                                    if (temp.Tables[0].Rows.Count > 0)
                                    {
                                        for (int i = 0; i < joinedDs.Tables[0].Columns.Count; i++)
                                        {
                                            for (int j = 0; j < temp.Tables[0].Rows.Count; j++)
                                            {
                                                if (temp.Tables[0].Rows[j].ItemArray[0].ToString() == joinedDs.Tables[0].Columns[i].ToString())
                                                {
                                                    if (dr[joinedDs.Tables[0].Columns[i]].ToString() != null && dr[joinedDs.Tables[0].Columns[i]].ToString() != "")
                                                    {
                                                        strval = dr[joinedDs.Tables[0].Columns[i].ToString()].ToString();
                                                        datatype = temp.Tables[0].Rows[j].ItemArray[1].ToString();
                                                        datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                                        noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                                        if (noofdecimalplace != 6)
                                                            //strval = strval.Remove(strval.IndexOf('.') + 1 + noofdecimalplace);
                                                            strval = decimal.Round(Convert.ToDecimal(strval), 2).ToString();
                                                        if (noofdecimalplace == 0)
                                                        {
                                                            strval = strval.TrimEnd('.');
                                                        }
                                                        dr[joinedDs.Tables[0].Columns[i]] = strval;
                                                        #region "Decimal Place Finding"
                                                        //            string afterDecimal = string.Empty;
                                                        //            string decimalPrecision = dr["A"].ToString();
                                                        //            int _afterDecimalVal = 0;
                                                        //            for (int i = 0; i < decimalPrecision.Length; i++)
                                                        //            {
                                                        //                if (decimalPrecision[i] == ',')
                                                        //                {
                                                        //                    int j = i + 1;
                                                        //                    while (decimalPrecision[j] != ')')
                                                        //                    {
                                                        //                        afterDecimal = afterDecimal + decimalPrecision[j].ToString();
                                                        //                        j++;
                                                        //                    }
                                                        //                }
                                                        //            }

                                                        #endregion
                                                        #region "Trimming as per decimal place"
                                                        //if(
                                                        //string attr = "A";

                                                        //string strvalue = dr["A"].ToString();
                                                        //int noofdecimal = strvalue.IndexOf(".", StringComparison.Ordinal);
                                                        //if (strvalue.IndexOf(".", StringComparison.Ordinal) > 0)
                                                        //{
                                                        //    if ((strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1) > 0)
                                                        //    {

                                                        //        if (Convert.ToDecimal(strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1)) == 0)
                                                        //        {
                                                        //            strvalue = strvalue.Substring(0, strvalue.IndexOf(".", StringComparison.Ordinal));
                                                        //            string retVal = "";
                                                        //            for (int preadd = 0; preadd < noofdecimal; preadd++)
                                                        //            { retVal = retVal + "0"; }
                                                        //            if (retVal != "")
                                                        //            {
                                                        //                dr["A"] = strvalue + "." + retVal;
                                                        //            }
                                                        //        }
                                                        //        else if (Convert.ToDecimal(strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1)) > 0)
                                                        //        {
                                                        //            if (strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1).Length >= noofdecimal)
                                                        //                dr["A"] = strvalue.Substring(0, strvalue.IndexOf(".", StringComparison.Ordinal)) + "." + (strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, noofdecimal));
                                                        //        }
                                                        //    }
                                                        //}
                                                        #endregion

                                                    }
                                                }
                                            }
                                        }

                                    }

                                }
                            }
                        }
                    }
                }
                #endregion
                #region "Date and Time Formatting"

                if (joinedDs.Tables.Count > 0)
                    if (joinedDs.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < attributeList.Count(); i++)
                        {
                            var cmd = new SqlCommand("SELECT ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID =" + attributeList[i].ToString() + "", sqlConn);
                            DataSet AttrName = new DataSet();
                            var da = new SqlDataAdapter(cmd);
                            da.Fill(AttrName);

                            if (AttrName.Tables.Count > 0)
                                if (AttrName.Tables[0].Rows.Count > 0)
                                    if (AttrName.Tables[0].Rows[0][1].ToString().StartsWith("Date"))
                                    {
                                        foreach (DataColumn dc in joinedDs.Tables[0].Columns)
                                        {
                                            if (dc.ColumnName == AttrName.Tables[0].Rows[0][0].ToString())
                                            {
                                                foreach (DataRow dr in joinedDs.Tables[0].Rows)
                                                {
                                                    if (AttrName.Tables[0].Rows[0][2].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
                                                    {
                                                        string tempdate = dr[dc.ColumnName].ToString();
                                                        if (tempdate != "" && tempdate != null)
                                                        {
                                                            dr[dc.ColumnName] = tempdate.Substring(3, 2) + "/" + tempdate.Substring(0, 2) + "/" + tempdate.Substring(6, 4);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                        }
                    }

                #endregion
                sqlConn.Close();
            }
            try
            {
                joinedDs.Tables[0].Columns.Remove("FAMILY_ID_1");
            }
            catch (Exception) { }
            bool supplierInfo = false;
            for (int i = 0; i < attributeList.Length; i++)
            {
                if (attributeList[i].ToString(CultureInfo.InvariantCulture) == "3")
                {
                    supplierInfo = true;
                    break;
                }
            }
            if (supplierInfo)
            {
                var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                string sql = "SELECT	distinct TB_CATALOG_FAMILY.CATALOG_ID,TB_PROD_FAMILY.FAMILY_ID,TB_PROD_FAMILY.PRODUCT_ID," +
                            " SUPPLIER_COMPANY_NAME,ADDRESS_LINE_1 as SUPPLIER_ADDRESS_LINE_1,ADDRESS_LINE_2 as SUPPLIER_ADDRESS_LINE_2," +
                            " ADDRESS_LINE_3 as SUPPLIER_ADDRESS_LINE_3,CITY as SUPPLIER_CITY,STATE AS SUPPLIER_STATE,ZIP AS SUPPLIER_ZIP,COUNTRY AS SUPPLIER_COUNTRY,PHONE_1 AS SUPPLIER_PHONE_1," +
                            " FAX AS SUPPLIER_FAX,EMAIL AS SUPPLIER_EMAIL,URL AS SUPPLIER_URL,LOGO_IMAGE_FILE AS SUPPLIER_LOGO_IMAGE_FILE,LOGO_IMAGE_TYPE AS SUPPLIER_LOGO_IMAGE_TYPE" +
                            " FROM	TB_CATALOG_FAMILY, TB_PROD_FAMILY, TB_CATALOG_PRODUCT, TB_PROD_SPECS,TB_SUPPLIER" +
                            " WHERE	TB_CATALOG_FAMILY.FAMILY_ID = TB_PROD_FAMILY.FAMILY_ID AND" +
                            " TB_PROD_FAMILY.PRODUCT_ID =  TB_CATALOG_PRODUCT.PRODUCT_ID AND" +
                            " TB_CATALOG_FAMILY.CATALOG_ID=" + catalogID + " AND" +
                            " TB_PROD_SPECS.ATTRIBUTE_ID = 3 AND TB_PROD_SPECS.PRODUCT_ID = TB_CATALOG_PRODUCT.PRODUCT_ID  AND" +
                            " TB_CATALOG_FAMILY.CATALOG_ID=TB_CATALOG_PRODUCT.CATALOG_ID AND TB_PROD_FAMILY.PUBLISH=1 AND" +
                            " TB_PROD_SPECS.STRING_VALUE=TB_SUPPLIER.SUPPLIER_NAME";
                DataSet supplierDs = new DataSet();
                SqlDataAdapter _DBAdapter = new SqlDataAdapter(sql, objSqlConnection);
                _DBAdapter.SelectCommand.CommandTimeout = 0;
                _DBAdapter.Fill(supplierDs);
                _DBAdapter.Dispose();


                if (supplierDs != null && supplierDs.Tables[0].Rows.Count > 0)
                {
                    DataTable newTable = JoinSupplierDS(joinedDs.Tables[0], supplierDs.Tables[0]);
                    var joinedDs1 = new DataSet();
                    joinedDs1.Tables.Add(newTable);
                    return joinedDs1;
                }
            }
            return joinedDs;
        }
        private DataTable JoinSupplierDS(DataTable first, DataTable second)
        {

            var table = new DataTable("JoinedTable");

            foreach (DataColumn dc in first.Columns)
            {
                table.Columns.Add(dc.ColumnName, dc.DataType);
            }
            foreach (DataColumn dc in second.Columns.Cast<DataColumn>().Where(dc => dc.ColumnName != "CATALOG_ID" && dc.ColumnName != "FAMILY_ID" && dc.ColumnName != "PRODUCT_ID"))
            {
                table.Columns.Add(dc.ColumnName, dc.DataType);
            }
            foreach (DataRow dr in first.Rows)
            {
                DataRow[] supplierRow = null;
                if (dr["PRODUCT_ID"].ToString().Length > 0)
                {
                    supplierRow = second.Select("CATALOG_ID=" + dr["CATALOG_ID"] + " AND FAMILY_ID=" + dr["FAMILY_ID"] + " AND PRODUCT_ID=" + dr["PRODUCT_ID"]);
                }
                DataRow newRow = table.NewRow();

                for (int i = 0; i < first.Columns.Count; i++)
                {
                    newRow[i] = dr[i];
                }
                if (supplierRow != null && supplierRow.Length > 0)
                {
                    for (int i = 0; i < second.Columns.Count; i++)
                    {
                        if (second.Columns[i].ToString() != "CATALOG_ID" && second.Columns[i].ToString() != "FAMILY_ID" && second.Columns[i].ToString() != "PRODUCT_ID")
                        {
                            newRow[second.Columns[i].ColumnName] = supplierRow[0][second.Columns[i].ColumnName];
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < second.Columns.Count; i++)
                    {
                        if (second.Columns[i].ToString() != "CATALOG_ID" && second.Columns[i].ToString() != "FAMILY_ID" && second.Columns[i].ToString() != "PRODUCT_ID")
                        {
                            newRow[second.Columns[i].ToString()] = DBNull.Value;
                        }
                    }
                }
                table.Rows.Add(newRow);

            }
            return table;

        }
        private DataSet ApplyStyleFormat(DataSet finalDs)
        {
            for (int mainCount = 0; mainCount < finalDs.Tables.Count; mainCount++)
            {
                for (int cName = 0; cName < finalDs.Tables[mainCount].Columns.Count; cName++)
                {
                    if (finalDs.Tables[mainCount].Columns[cName].ColumnName != "CATALOG_ID")
                    {
                        DataSet styleDs = new DataSet();
                        var oConn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                        {
                            string SQLString =
                                "SELECT STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_NAME = '" +
                                finalDs.Tables[mainCount].Columns[cName].ColumnName.Trim().Replace("'", "''") + "'";


                            SqlDataAdapter _DBAdapter = new SqlDataAdapter(SQLString, oConn);
                            _DBAdapter.SelectCommand.CommandTimeout = 0;
                            _DBAdapter.Fill(styleDs);

                            if (styleDs.Tables[0].Rows.Count > 0)
                            {
                                string style = styleDs.Tables[0].Rows[0].ItemArray[0].ToString();
                                if (style.Length > 0)
                                {
                                    int index = style.IndexOf("[", StringComparison.Ordinal);
                                    if (index != -1)
                                        style = style.Substring(0, index - 1);
                                    foreach (DataRow dr in finalDs.Tables[mainCount].Rows)
                                    {
                                        try
                                        {
                                            double dt = Convert.ToDouble(dr[finalDs.Tables[mainCount].Columns[cName].ColumnName]);
                                            dr[finalDs.Tables[mainCount].Columns[cName].ColumnName] = dt.ToString(style.Trim());
                                        }
                                        catch (Exception)
                                        {

                                        }
                                    }
                                }
                            }
                        };
                    }
                }
            }
            return finalDs;
        }
        public DataSet CreateDataSet()
        {
            try
            {
                DataSet dsReturn = new DataSet();
                using (
                    var objSqlConnection =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    DataSet ds = new DataSet();

                    SqlDataAdapter _DBAdapter = new SqlDataAdapter(_SQLString, objSqlConnection);
                    _DBAdapter.SelectCommand.CommandTimeout = 0;
                    _DBAdapter.Fill(dsReturn);
                    _DBAdapter.Dispose();
                    return dsReturn;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private void ImagePathXpc()
        {
            _uPath = string.Empty;
            if (_userImagepath.Length == 2)
            {
                _userImagepath = _userImagepath + "\\";
            }

            foreach (char t in _userImagepath)
            {
                if (t == '\\')
                {
                    _uPath = _uPath + "\\\\";
                }
                else
                {
                    _uPath = _uPath + t;
                }
            }
        }
        private string ReplaceCharacters(string input)
        {
            byte[] ascii = Encoding.ASCII.GetBytes(input);
            string ret = string.Empty;
            for (int i = 0; i < ascii.Length; i++)
            {
                int a = ascii[i];
                if ((a >= 48 && a <= 57) || (a >= 65 && a <= 90) || (a >= 97 && a <= 122))
                {
                    ret += input[i];
                }
                else
                {
                    ret += "_";
                    ascii[i] = 95;
                }
            }
            return ret;
        }
        public int ProjectID { private get; set; }
        public int CatalogID { private get; set; }
        #endregion
        #region Simple Catalog Method
        public ActionResult simpleCatalog()
        {
            // return StiMvcDesigner.GetReportTemplateResult(simplePDFReport);
            return null;
        }
        public void simpleCatalogCall(JArray model)
        {
            selectedTreeNodes = model;
        }
        #endregion

        // Pdf Express - Start

        //public JsonResult SaveFiles(string FileName)
        //{
        //    string fileName, actualFileName;
        //    string FilePath = string.Empty;
        //    Message = fileName = actualFileName = string.Empty;
        //    bool flag = false;

        //        var file = Request.Files[0];
        //        actualFileName = file.FileName;
        //        fileName = FileName;
        //        int size = file.ContentLength;
        //        string userName = User.Identity.Name;
        //        try
        //        {
        //            if (!Directory.Exists(Server.MapPath("~/Customer Templates/" + userName + "/Opened Template")))
        //                Directory.CreateDirectory(Server.MapPath("~/Customer Templates/" + userName + "/Opened Template"));
        //            file.SaveAs(Path.Combine(Server.MapPath("~/Customer Templates/" + userName + "/Opened Template"), fileName));
        //            FilePath = Path.Combine(Server.MapPath("~/Customer Templates/" + userName + "/Opened Template"), fileName);
        //            Session["TemplatePath"] = FilePath;

        //        }
        //        catch (Exception)
        //        {
        //            Message = "File upload failed, please try again";
        //        }

        //    return Json(FilePath, JsonRequestBehavior.AllowGet);
        //}


        // Pdf Express - End

        public ActionResult DirectlyopenTemplatePreview()
        {
            showXml = "true";


            _flagOpenReportPreview = true;
            var connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
            if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct")))
            {
                if (System.IO.File.Exists(Server.MapPath("~/Views/App/XpressCatalog/Content/Report_Config.Config")))
                {
                    string categoryIds = null;
                    string categoryIdsVal = null;
                    string CatalogId = null;
                    Session["Template"] = Session["TemplatePath"].ToString();

                    report.Load(Session["TemplatePath"].ToString());

                    if (report.Dictionary.Variables.Contains("CATEGORY_ID_VAL"))
                    {
                        categoryIds = "Format(\"{0}\", CATEGORY_ID_VAL)";
                        categoryIdsVal = report.Dictionary.Variables["CATEGORY_ID_VAL"].Value;
                        if (categoryIdsVal == "")
                            categoryIdsVal = null;
                    }
                    for (int dsCount = 0; dsCount < report.Dictionary.DataSources.Count; dsCount++)
                    {
                        if (report.Dictionary.DataSources[dsCount].Name == "Category")
                        {
                            dsVariable = true;
                            CatalogId = report.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value;
                            break;
                        }
                    }
                    if (dsVariable)
                    {
                        report.Dictionary.Databases.Clear();
                        report.Dictionary.Load(Server.MapPath("~/Views/App/XpressCatalog/Content/Dictionary/XpressCatalogMasterDictionary.dct"));
                        report.Dictionary.Synchronize();
                        report.Dictionary.Databases.Clear();
                        report.Dictionary.Databases.Add(new Stimulsoft.Report.Dictionary.StiSqlDatabase("CatalogStudio", connectionString));
                        FamilyFilter(_workingCatalogID1, report);
                        ProductFilter(_workingCatalogID1, report);

                        report.Dictionary.Variables["CATEGORY_ID_VAL"].Value = categoryIdsVal;

                        report.Dictionary.DataSources["Category"].Parameters["CATALOG_ID"].Value = CatalogId;// _workingCatalogID1.ToString();
                        report.Dictionary.DataSources["Category"].Parameters["CATEGORY_ID"].Value = categoryIds;                     //For hierarchical template category parameter

                        report.Dictionary.DataSources["Family"].Parameters["CATALOG_ID"].Value = CatalogId;
                        report.Dictionary.DataSources["Family"].Parameters["CATEGORY_ID"].Value = categoryIds;                       //For hierarchical template category parameter

                        report.Dictionary.DataSources["Family Description"].Parameters["CATALOG_ID"].Value = CatalogId;
                        report.Dictionary.DataSources["Family Description"].Parameters["CATEGORY_ID"].Value = categoryIds;           //For hierarchical template category parameter    

                        report.Dictionary.DataSources["Family Attachment"].Parameters["CATALOG_ID"].Value = CatalogId;
                        report.Dictionary.DataSources["Family Attachment"].Parameters["CATEGORY_ID"].Value = categoryIds;             //For hierarchical template category parameter

                        report.Dictionary.DataSources["Product Family"].Parameters["CATALOG_ID"].Value = CatalogId;
                        report.Dictionary.DataSources["Product Family"].Parameters["CATEGORY_ID"].Value = categoryIds;                //For hierarchical template category parameter

                        report.Dictionary.DataSources["Product Specification"].Parameters["CATALOG_ID"].Value = CatalogId;
                        report.Dictionary.DataSources["Product Specification"].Parameters["CATEGORY_ID"].Value = categoryIds;         //For hierarchical template category parameter 
                        try
                        {
                            report.Dictionary.DataSources["Supplier Details"].Parameters["CATALOG_ID"].Value = CatalogId;
                        }
                        catch (Exception exception)
                        {
                        }
                        var folderName = _dbcontext.Customers.Join(_dbcontext.Customer_User, cs => cs.CustomerId, cu => cu.CustomerId, (cs, cu) => new { cs, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.cs.Comments).FirstOrDefault().ToString();
                        if (string.IsNullOrEmpty(folderName))
                        {
                            folderName = "Questudio";
                        }
                        var attachmentPath = ConfigurationManager.AppSettings["PreviewAttachmentPath"] + "/" + folderName + "/";
                        if (!Directory.Exists(attachmentPath))
                            Directory.CreateDirectory(attachmentPath);
                        if (report.Dictionary.Variables.Contains("PreviewAttachmentPath") == true)
                            report.Dictionary.Variables["PreviewAttachmentPath"].Value = Server.MapPath(attachmentPath);
                    }
                    else
                    {
                        RunTemplate(ref (report), "");
                    }
                }
                else
                {
                    Message = "ProjectID Not Found";
                }
            }
            if (report.Dictionary.Relations.Count < 3)
            {
                var relation = new StiDataRelation("FamilyRelation", "FamilySource", "FamilyRelation",
                    report.Dictionary.DataSources["Family Details"], report.Dictionary.DataSources["Catalog Objects"],
                    new System.String[] { "FAMILY_ID" }, new System.String[] { "FAMILY_ID" });
                if (!report.Dictionary.Relations.Contains(relation))
                    report.Dictionary.Relations.Add(relation);
                relation = new StiDataRelation("MultipleTableRelation", "MultipleTableSource", "MultipleTableRelation",
                    report.Dictionary.DataSources["Family Details"], report.Dictionary.DataSources["Multiple Tables"],
                    new System.String[] { "FAMILY_ID" }, new System.String[] { "FAMILY_ID" });
                if (!report.Dictionary.Relations.Contains(relation))
                    report.Dictionary.Relations.Add(relation);
                relation = new StiDataRelation("ReferenceTableRelation", "ReferenceTableSource",
                    "ReferenceTableRelation", report.Dictionary.DataSources["Family Details"],
                    report.Dictionary.DataSources["Reference Tables"], new System.String[] { "FAMILY_ID" },
                    new System.String[] { "FAMILY_ID" });
                if (!report.Dictionary.Relations.Contains(relation))
                    report.Dictionary.Relations.Add(relation);
            }
            report.CacheTotals = false;
            report.CacheAllData = false;


            report.ReportCacheMode = StiReportCacheMode.Off;
            report.Pages.CacheMode = false;
            report.PreviewSettings = 0;
            StiOptions.Engine.ImageCache.Enabled = false;
            report.Dictionary.Synchronize();
            //report.Dictionary.DataSources.Connect(true);
            // Session["Template"] = report;
            //return StiMvcViewerFx.GetReportSnapshotResult(report);
            //return StiMvcDesigner.PreviewReportResult(report);
            return StiMvcDesigner.PreviewReportResult(report);
            //return StiMvcViewer.GetReportResult(report); 

        }
        public ActionResult DesignerTemplate()
        {
            return View();
        }

    }
}