﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Xml;
using log4net;
using LS.Data;
using LS.Data.Model;
using System.IO;
using System.Xml.Serialization;
using System.Data;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Infragistics.Documents.Excel;
using System.Text;
using System.Data.OleDb;
using System.IO.Compression;
using LS.Data.Model.CatalogSectionModels;

namespace LS.Web.Controllers
{
    public class CatalogController : Controller
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        static readonly ILog Logger = LogManager.GetLogger(typeof(CatalogController));
        readonly CSEntities _dbcontext = new CSEntities();
        private ImportController IC = new ImportController();
        HomeApiController homeController = new HomeApiController();
        XpressCatalogController obj_XpressCatalogController = new XpressCatalogController();
        //string dataSource = ConfigurationManager.AppSettings["DataSource"];
        //string userID = ConfigurationManager.AppSettings["UserID"];
       // string password = ConfigurationManager.AppSettings["Password"];
        private string sqlString = "";
        // GET: Catalog
        


        //------------------------------------------Seperated path values initializing------------------------------------------------------//
        public string pathCheckFlag = System.Web.Configuration.WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
        public string pathServerValue = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDriveURL"].ToString();
        public string serverPathShareVal = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
        //------------------------------------------Seperated path values initializing------------------------------------------------------//

        public JsonResult GetCatalog(int catalogId)
        {
            try
            {
                var categoryDetails = _dbcontext.TB_CATALOG.Where(s => s.CATALOG_ID == catalogId).ToList().Select(x => new CatalogFamilyOption
                {
                    CATALOG_ID = x.CATALOG_ID,
                    CATALOG_NAME = x.CATALOG_NAME,
                    DESCRIPTION = x.DESCRIPTION,
                    VERSION = x.VERSION,
                    FAMILY_FILTERS = x.FAMILY_FILTERS,
                    familyfilter = XmlDeserializeFamilyfunction(x.FAMILY_FILTERS),
                    PRODUCT_FILTERS = x.PRODUCT_FILTERS,
                    productfilter = XmlDeserializeProductfunction(x.PRODUCT_FILTERS),
                    DEFAULT_FAMILY = Convert.ToInt32(x.DEFAULT_FAMILY)
                }).ToList();
                return Json(categoryDetails, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objException)
            {
                Logger.Error("Error at CatalogController : GetCatalog", objException);
                return null;
            }
        }
        private List<FamilyFilters> XmlDeserializeFamilyfunction(string familyfilter)
        {
            try
            {


                // Call the Deserialize method to restore the object's state.
                //var deserializer = new XmlSerializer(typeof(CatalogFamilyOption));
                // var data = (CatalogFamilyOption)deserializer.Deserialize(xmlTextReader);



                // load data ...

                //}
                if (!string.IsNullOrEmpty(familyfilter))
                {
                    var deserializer = new XmlSerializer(typeof(CatalogFamilyOption));
                    using (var xmlTextReader = new XmlTextReader(new StringReader(familyfilter)))
                    {

                        xmlTextReader.WhitespaceHandling = WhitespaceHandling.Significant;
                        object obj = deserializer.Deserialize(xmlTextReader);
                        var xmlData = (CatalogFamilyOption)obj;
                        foreach (var familyfilters in xmlData.familyfilter)
                        {
                            int attrid = Convert.ToInt32(familyfilters.Attribute_id);
                            var sdf = _dbcontext.FAMILYFILTERS(familyfilters.Value, attrid).ToList();
                            var drpFamilyFilter = sdf.FirstOrDefault();
                            if (drpFamilyFilter != null)
                                familyfilters.Value = drpFamilyFilter.FAMILY_IDS;

                        }
                        return xmlData.familyfilter;
                    }
                    //return null;
                }
                return null;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at XmlDeserializefunction", objException);
                return null;
            }


        }
        private List<ProductFilter> XmlDeserializeProductfunction(string productfilter)
        {
            try
            {
                if (!string.IsNullOrEmpty(productfilter))
                {
                    var deserializers = new XmlSerializer(typeof(CatalogFamilyOption));
                    if (productfilter.Contains("<CatalogProductOption>"))
                    {
                        productfilter = productfilter.Replace("<CatalogProductOption>", "<CatalogFamilyOption>");
                        productfilter = productfilter.Replace("</CatalogProductOption>", "</CatalogFamilyOption>");
                    }
                    using (var xmlTextReader = new XmlTextReader(new StringReader(productfilter)))
                    {
                        xmlTextReader.WhitespaceHandling = WhitespaceHandling.Significant;
                        object objt = deserializers.Deserialize(xmlTextReader);
                        var xmlDatas = (CatalogFamilyOption)objt;
                        return xmlDatas.productfilter;
                    }
                }
                return null;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at XmlDeserializeProductfunction", objException);

                return null;
            }
        }

        public JsonResult GetAllattributes(int catalogId)
        {
            try
            {
                var catalogs = _dbcontext.QS_CATALOGALLATTRIBUTES(catalogId).ToList();

                return Json(catalogs, JsonRequestBehavior.AllowGet);

            }
            catch (Exception objException)
            {
                Logger.Error("Error at CatalogController : GetAllattributes", objException);
                return null;
            }
        }
        private string[] CalcAttrNames(string calcName, string dType)
        {
            string[] calcA;
            if (dType.Substring(0, 3) == "Tex")
            {
                calcA = calcName.Split('+');
            }
            else
            {
                calcA = new string[calcName.Length];
                int itcr = 0;
                for (int i = 0; i < calcName.Length - 1; i++)
                {
                    if (calcName.Substring(i, 2) == " +" ||
                        calcName.Substring(i, 2) == " -" ||
                        calcName.Substring(i, 2) == " *" ||
                        calcName.Substring(i, 2) == " /")
                    {
                        itcr++;
                        calcA[itcr] = calcName.Substring(i + 1, 1);
                        itcr++;
                        i = i + 1;
                        continue;
                    }
                    if (calcName.Substring(i, 1) != "+" &&
                        calcName.Substring(i, 1) != "-" &&
                        calcName.Substring(i, 1) != "*" &&
                        calcName.Substring(i, 1) != "/")
                    {
                        calcA[itcr] = calcA[itcr] + calcName.Substring(i, 1);
                    }
                    else if ((i - 2) >= 0)
                    {
                        if (calcName.Substring((i - 2), 1) != "]" && (i - 2) >= 0)
                        {
                            calcA[itcr] = calcA[itcr] + calcName.Substring(i, 1);
                        }

                    }
                    //else if (calcName.Substring((i - 2), 1) != "]")
                    //{
                    //    calcA[itcr] = calcA[itcr] + calcName.Substring(i, 1);
                    //}


                }
                calcA[itcr] = calcA[itcr] + calcName.Substring(calcName.Length - 1, 1);
                var str1 = new string[itcr + 1];
                for (int i = 0; i <= itcr; i++)
                {
                    str1[i] = calcA[i].Trim();
                }
                calcA = str1;
            }
            return (calcA);
        }
        private void EnableFormulaforColumns(int vall, int familyId, string categoryId, int catalogId, DataTable objDataTable)
        {
            var objProductLayout = new Data.Model.ProductPreview.ProductLayout();
            var sqlString = " SELECT DISTINCT TA.ATTRIBUTE_CALC_FORMULA , TA.ATTRIBUTE_ID  FROM TB_ATTRIBUTE TA, TB_CATALOG_ATTRIBUTES TCA WHERE TA.IS_CALCULATED =1  AND TA.ATTRIBUTE_CALC_FORMULA <> '' " +
                              "  AND TA.ATTRIBUTE_ID IN (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID IN " +
                              "  (SELECT PRODUCT_ID FROM TB_PROD_FAMILY WHERE FAMILY_ID = " + familyId + "))" +
                              "  AND TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID = " + catalogId + " " +
                              "  UNION " +
                              "  SELECT DISTINCT TA.ATTRIBUTE_CALC_FORMULA , TA.ATTRIBUTE_ID  FROM TB_ATTRIBUTE TA, TB_CATALOG_ATTRIBUTES TCA WHERE TA.IS_CALCULATED =1  AND " +
                              "  TA.ATTRIBUTE_CALC_FORMULA <> ''   AND TA.ATTRIBUTE_ID IN (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PARTS_KEY WHERE  FAMILY_ID =  " + familyId + " AND CATALOG_ID = " + catalogId + ")  " +
                              "  AND TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID = " + catalogId;
            var odsCalc = objProductLayout.Checkproductfilter(sqlString);
            // OdsCalc = _oCon.CreateDataSet(); _oCon._DBCon.Close();

            if (vall == 0)
            {
                if ((objDataTable.Rows.Count != 0))
                    foreach (DataRow dr in odsCalc.Rows)
                    {
                        var attrdetails = _dbcontext.TB_ATTRIBUTE.Find(Convert.ToInt32(dr[1].ToString()));
                        bool iscalculated = attrdetails.IS_CALCULATED;
                        //checkIScalculted(Convert.ToInt32(Dr[1].ToString()));
                        string attrCalcFormaula = dr[0].ToString();
                        string[] dataVal1 = null;
                        if (iscalculated)
                        {
                            if (attrdetails.ATTRIBUTE_DATATYPE != "Text")
                            {
                                string dr12 = attrdetails.ATTRIBUTE_DATATYPE.Substring(6, (attrdetails.ATTRIBUTE_DATATYPE.Length - 6));
                                dr12 = dr12.Replace("(", "").Replace(")", "");
                                dataVal1 = dr12.Split(',');

                            }

                            string[] strVal = CalcAttrNames(attrCalcFormaula, attrdetails.ATTRIBUTE_DATATYPE);
                            var sd = objDataTable.Columns.Cast<DataColumn>().FirstOrDefault(c => c.ColumnName.StartsWith(attrdetails.ATTRIBUTE_NAME)); // + "__"
                            if (sd != null)
                            {
                                if (attrdetails.ATTRIBUTE_DATATYPE.Substring(0, 3) == "Tex")
                                {
                                    for (var i = 0; i < objDataTable.Rows.Count; i++)
                                    {
                                        try
                                        {

                                            if (objDataTable.Columns.Contains(sd.ColumnName))
                                            {
                                                objDataTable.Rows[i][sd.ColumnName] = "";
                                                foreach (string t in strVal)
                                                {
                                                    try
                                                    {
                                                        if (t.StartsWith("[") || t.StartsWith(" ["))
                                                        {
                                                            var sdcal = objDataTable.Columns.Cast<DataColumn>().FirstOrDefault(c => c.ColumnName.StartsWith(t.Substring(t.IndexOf('[') + 1, t.LastIndexOf(']') - t.IndexOf('[') - 1))); //+ "__"
                                                            if (sdcal != null)
                                                            {
                                                                if (objDataTable.Columns.Contains(sdcal.ColumnName))
                                                                {
                                                                    objDataTable.Rows[i][sd.ColumnName] =
                                                                        Convert.ToString(
                                                                            objDataTable.Rows[i][sd.ColumnName]) +
                                                                        Convert.ToString(
                                                                            objDataTable.Rows[i][sdcal.ColumnName]);
                                                                }
                                                            }
                                                        }
                                                        else
                                                            objDataTable.Rows[i][sd.ColumnName] = Convert.ToString(objDataTable.Rows[i][sd.ColumnName]) + t;

                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        // Logger.Error("Error at HomeApiController:EnableFormulaforColumns", ex);
                                                    }
                                                }

                                                byte attrType = attrdetails.ATTRIBUTE_TYPE;
                                                int attributeID = attrdetails.ATTRIBUTE_ID;
                                                int productID = Convert.ToInt32(objDataTable.Rows[i]["PRODUCT_ID"].ToString());
                                                if (attrType != 6)
                                                {

                                                    var objcustomercatalogcount = _dbcontext.TB_PROD_SPECS.Where(x => x.PRODUCT_ID == productID && x.ATTRIBUTE_ID == attributeID);
                                                    if (objcustomercatalogcount.Any())
                                                    {
                                                        var stringvalue = Convert.ToString(objDataTable.Rows[i][sd.ColumnName]);
                                                        if (objcustomercatalogcount.FirstOrDefault().STRING_VALUE != stringvalue)
                                                        {
                                                            string updateprodspecs =
                                                                " UPDATE TB_PROD_SPECS SET STRING_VALUE =N'" +
                                                                stringvalue.Replace("'", "''") +
                                                                "' " +
                                                                " WHERE PRODUCT_ID = " + productID +
                                                                " AND ATTRIBUTE_ID = " +
                                                                attributeID + " AND COALESCE(STRING_VALUE,'') !='" +
                                                                stringvalue.Replace("'", "''") +
                                                                "'";
                                                            //int itcr = _oCon.ExecuteSQLQuery(); _oCon._DBCon.Close();
                                                            objProductLayout.Insertquery(updateprodspecs);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            string insertprodspecs = "INSERT INTO TB_PROD_SPECS(PRODUCT_ID,ATTRIBUTE_ID,STRING_VALUE) VALUES (" + productID + "," + attributeID + ",N'" + Convert.ToString(objDataTable.Rows[i][sd.ColumnName]) + "' )";
                                                            objProductLayout.Insertquery(insertprodspecs);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            // Logger.Error("Error at HomeApiController:EnableFormulaforColumnsFamily", ex);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    var objcustomercatalogcount = _dbcontext.TB_PARTS_KEY.Where(x => x.PRODUCT_ID == productID && x.ATTRIBUTE_ID == attributeID && x.CATALOG_ID == catalogId);
                                                    if (objcustomercatalogcount.Any())
                                                    {
                                                        var stringvalue = Convert.ToString(objDataTable.Rows[i][sd.ColumnName]);
                                                        if (objcustomercatalogcount.FirstOrDefault().ATTRIBUTE_VALUE != stringvalue)
                                                        {
                                                            string updateprodspecs =
                                                                " UPDATE TB_PARTS_KEY SET ATTRIBUTE_VALUE =N'" +
                                                                stringvalue.Replace("'", "''") +
                                                                "' " +
                                                                " WHERE PRODUCT_ID = " + productID +
                                                                "  AND CATALOG_ID = " + catalogId + " AND ATTRIBUTE_ID = " + attributeID + " AND CATEGORY_ID = '" + categoryId + "' AND COALESCE(ATTRIBUTE_VALUE,'') !='" +
                                                                stringvalue.Replace("'", "''") +
                                                                "'";
                                                            objProductLayout.Insertquery(updateprodspecs);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        try
                                                        {
                                                            string insertprodspecs =
                                                                "INSERT INTO TB_PARTS_KEY(CATALOG_ID,FAMILY_ID,CATEGORY_ID,PRODUCT_ID,ATTRIBUTE_ID,ATTRIBUTE_VALUE) VALUES (" +
                                                                catalogId + "," + familyId + ",'" + categoryId + "'," + productID + "," +
                                                                attributeID + ",N'" +
                                                                Convert.ToString(
                                                                    objDataTable.Rows[i][sd.ColumnName]).Replace("'", "''") +
                                                                "' )";
                                                            objProductLayout.Insertquery(insertprodspecs);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            //Logger.Error("Error at HomeApiController:EnableFormulaforColumns", ex);
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                        catch (ArgumentException) { }
                                    }
                                }
                                else
                                {
                                    for (int i = 0; i < objDataTable.Rows.Count; i++)
                                    {
                                        string formula = attrCalcFormaula;
                                        string newformula = attrCalcFormaula;
                                        while (formula.IndexOf("[", StringComparison.Ordinal) > 0)
                                        {
                                            int index = formula.IndexOf("[", StringComparison.Ordinal);
                                            formula = formula.Substring(index + 1);
                                            int endIndex = formula.IndexOf("]", StringComparison.Ordinal);
                                            string attrName = formula.Substring(0, endIndex);
                                            formula = formula.Substring(endIndex + 1);
                                            var sdcal = objDataTable.Columns.Cast<DataColumn>().FirstOrDefault(c => c.ColumnName.StartsWith(attrName)); // + "__"
                                            if (sdcal != null)
                                            {
                                                if (objDataTable.Columns.Contains(sdcal.ColumnName))
                                                {
                                                    if (Convert.ToString(objDataTable.Rows[i][sdcal.ColumnName]).Replace("_", "").Trim().Length > 0)
                                                    {
                                                        newformula = newformula.Replace(attrName, Convert.ToString(objDataTable.Rows[i][sdcal.ColumnName])
                                                                .Replace("_", ""));
                                                    }
                                                    else
                                                    {
                                                        newformula = newformula.Replace(attrName, "0");
                                                    }
                                                }
                                                else
                                                {
                                                    newformula = "0";
                                                    //  break;
                                                }

                                            }
                                        }
                                        // new added
                                        string strCmd = "Select (";
                                        for (int j = 0; j < strVal.Length; j++)
                                        {
                                            //  var columnname = sd.FirstOrDefault().ColumnName;

                                            strVal[j] = strVal[j].Trim();

                                            var sdcal = objDataTable.Columns.Cast<DataColumn>().FirstOrDefault(c => c.ColumnName.StartsWith(strVal[j].Substring(1, strVal[j].Length - 2))); //+ "__"
                                            if (sdcal != null)
                                            {

                                                if (strVal[j].StartsWith("[") &&
                                                    objDataTable.Columns.Contains(sdcal.ColumnName))
                                                {
                                                    if (
                                                        Convert.ToString(objDataTable.Rows[i][sdcal.ColumnName]).Length ==
                                                        0)
                                                        strCmd = strCmd + "0";
                                                    else
                                                    {
                                                        if (
                                                            Convert.ToString(objDataTable.Rows[i][sdcal.ColumnName])
                                                                .Replace("_", "") == ".")
                                                            strCmd = strCmd + "0";
                                                        else
                                                            strCmd = strCmd +
                                                                     Convert.ToString(
                                                                         objDataTable.Rows[i][sdcal.ColumnName])
                                                                         .Replace("_", "");
                                                    }
                                                }
                                                else if (strVal[j].StartsWith("[") == false)
                                                {
                                                    strCmd = strCmd + strVal[j];
                                                }
                                                else
                                                {
                                                    strCmd = strCmd + "0";
                                                }
                                                j++;
                                                if (j < strVal.Length - 1)
                                                {
                                                    strCmd = strCmd + strVal[j];
                                                }
                                            }
                                            else
                                            {
                                                j++;
                                                if (j < strVal.Length - 1)
                                                {
                                                    strCmd = strCmd + "0" + strVal[j];
                                                }
                                            }
                                        }
                                        strCmd = strCmd + ")";
                                        // new added
                                        if (newformula.IndexOf("[", StringComparison.Ordinal) > 0)
                                        {
                                            sqlString = "select " + newformula.Replace("[", "").Replace("]", "");
                                        }
                                        else
                                        {
                                            sqlString = strCmd;
                                        }
                                        try
                                        {
                                            // DataSet Dss = _oCon.CreateDataSet(); _oCon._DBCon.Close();
                                            var dss = objProductLayout.Checkproductfilter(sqlString);
                                            objDataTable.Rows[i][sd.ColumnName] = dss.Rows[0].ItemArray[0];
                                            int attrType = Convert.ToInt32(attrdetails.ATTRIBUTE_TYPE);
                                            int attributeID = Convert.ToInt32(attrdetails.ATTRIBUTE_ID);
                                            int productID =
                                                Convert.ToInt32(objDataTable.Rows[i]["PRODUCT_ID"].ToString());

                                            string tempValue1 = Convert.ToString(dss.Rows[0].ItemArray[0]);
                                            string[] tempVal1 = tempValue1.Split('.');
                                            if (tempVal1.Length > 1)
                                            {
                                                var claculatedvalue = double.Parse(tempValue1, System.Globalization.CultureInfo.InvariantCulture);
                                                int decimalvalue;
                                                int.TryParse(dataVal1[1], out decimalvalue);
                                                tempValue1 = Math.Round(claculatedvalue, decimalvalue).ToString(System.Globalization.CultureInfo.InvariantCulture);
                                                //if ((tempVal1[0].Length > Convert.ToInt32(dataVal1[0])) && (tempVal1[1].Length > Convert.ToInt32(dataVal1[1])))
                                                //{
                                                //    if (Convert.ToInt32(dataVal1[1]) == 0)
                                                //    {
                                                //        tempValue1 = tempVal1[0].Substring(0, Convert.ToInt32(dataVal1[0]));
                                                //    }
                                                //    else
                                                //    {
                                                //        tempValue1 =
                                                //            tempVal1[0].Substring(0, Convert.ToInt32(dataVal1[0])) + "." +
                                                //            tempVal1[1].Substring(0, Convert.ToInt32(dataVal1[1]));
                                                //    }
                                                //}
                                                //else if (tempVal1[0].Length > Convert.ToInt32(dataVal1[0]))
                                                //{
                                                //    tempValue1 = tempVal1[0].Substring(0, Convert.ToInt32(dataVal1[0])) + "." + tempVal1[1];
                                                //}
                                                //else if (tempVal1[1].Length > Convert.ToInt32(dataVal1[1]))
                                                //{
                                                //    if (Convert.ToInt32(dataVal1[1]) == 0)
                                                //    {
                                                //        tempValue1 = tempVal1[0];
                                                //    }
                                                //    else
                                                //    {
                                                //        tempValue1 = tempVal1[0] + "." +
                                                //                     tempVal1[1].Substring(0,
                                                //                         Convert.ToInt32(dataVal1[1]));
                                                //    }
                                                //}
                                            }
                                            else
                                            {

                                                if (tempVal1[0].Length > Convert.ToInt32(dataVal1[0]))
                                                {
                                                    tempValue1 = tempVal1[0].Substring(0, Convert.ToInt32(dataVal1[0]));
                                                }
                                            }
                                            objDataTable.Rows[i][sd.ColumnName] = tempValue1;
                                            if (attrType != 6)
                                            {
                                                var objcustomercatalogcount = _dbcontext.TB_PROD_SPECS.Where(x => x.PRODUCT_ID == productID && x.ATTRIBUTE_ID == attributeID);
                                                if (objcustomercatalogcount.Any())
                                                {
                                                    var stringvalue = Convert.ToString(objDataTable.Rows[i][sd.ColumnName]);
                                                    string updateprodspecs =
                                                            " UPDATE TB_PROD_SPECS SET NUMERIC_VALUE =" +
                                                            stringvalue.Replace("'", "''") +
                                                            " WHERE PRODUCT_ID = " + productID +
                                                            " AND ATTRIBUTE_ID = " + attributeID +
                                                            " AND COALESCE(NUMERIC_VALUE,0) !=" +
                                                            stringvalue.Replace("'", "''") + "";
                                                    objProductLayout.Insertquery(updateprodspecs);
                                                }

                                                else
                                                {
                                                    try
                                                    {
                                                        string insertprodspecs =
                                                            "INSERT INTO TB_PROD_SPECS(PRODUCT_ID,ATTRIBUTE_ID,NUMERIC_VALUE) VALUES (" +
                                                            productID + "," + attributeID + "," +
                                                            Convert.ToString(
                                                                objDataTable.Rows[i][sd.ColumnName]).Replace("'", "''") + " )";
                                                        objProductLayout.Insertquery(insertprodspecs);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        //  Logger.Error("Error at HomeApiController:EnableFormulaforColumns", ex);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var objcustomercatalogcount = _dbcontext.TB_PARTS_KEY.Where(x => x.PRODUCT_ID == productID && x.ATTRIBUTE_ID == attributeID && x.CATALOG_ID == catalogId);
                                                if (objcustomercatalogcount.Any())
                                                {
                                                    var stringvalue = Convert.ToString(objDataTable.Rows[i][sd.ColumnName]);
                                                    if (objcustomercatalogcount.FirstOrDefault().ATTRIBUTE_VALUE != stringvalue)
                                                    {
                                                        string updateprodspecs =
                                                            " UPDATE TB_PARTS_KEY SET ATTRIBUTE_VALUE =N'" +
                                                           stringvalue.Replace("'", "''") + "' " +
                                                            " WHERE PRODUCT_ID = " + productID + "  AND CATALOG_ID = " +
                                                            catalogId + " AND ATTRIBUTE_ID = " + attributeID +
                                                            " AND CATEGORY_ID = '" + categoryId + "' AND COALESCE(ATTRIBUTE_VALUE,'') !='" +
                                                            stringvalue.Replace("'", "''") + "'";
                                                        objProductLayout.Insertquery(updateprodspecs);
                                                    }
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        string insertprodspecs =
                                                            "INSERT INTO TB_PARTS_KEY(CATALOG_ID,FAMILY_ID,CATEGORY_ID,PRODUCT_ID,ATTRIBUTE_ID,ATTRIBUTE_VALUE) VALUES (" +
                                                            catalogId + "," + familyId + ",'" + categoryId + "'," + productID + "," +
                                                            attributeID + ",N'" + Convert.ToString(objDataTable.Rows[i][sd.ColumnName]).Replace("'", "''") + "' )";
                                                        objProductLayout.Insertquery(insertprodspecs);
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        // Logger.Error("Error at HomeApiController:EnableFormulaforColumns", ex);
                                                    }
                                                }
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            //  Logger.Error("Error at HomeApiController:EnableFormulaforColumns", ex);
                                        }
                                    }
                                }
                            }



                        }
                    }
            }

            //  oDsIscalc.Dispose();
            odsCalc.Dispose();

        }

        //public string GetProdSpecsExport(int catalogId, int familyId, bool displayIdColumns, string categoryId, bool EnableSubProduct)
        //{
        //    try
        //    {
        //        var objProdspecsdDataSet = new DataSet();
        //        // var objExport = new Export();
        //        Guid objGuid = Guid.NewGuid();
        //        string sessionid = Regex.Replace(objGuid.ToString(), "[^a-zA-Z0-9_]+", "");
        //        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
        //        {
        //            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
        //            objSqlCommand.CommandTimeout = 0;
        //            objSqlCommand.CommandText = "STP_LS_ProductPivotTable_Export";
        //            objSqlCommand.CommandType = CommandType.StoredProcedure;
        //            objSqlCommand.Connection = objSqlConnection;
        //            objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 100).Value = catalogId;
        //            objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 100).Value = familyId;
        //            objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar, 500).Value = categoryId;
        //            objSqlCommand.Parameters.Add("@SESSID", SqlDbType.NVarChar, 100).Value = sessionid;
        //            objSqlConnection.Open();
        //            var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
        //            objSqlDataAdapter.Fill(objProdspecsdDataSet);
        //            EnableFormulaforColumns(0, familyId, categoryId, catalogId, objProdspecsdDataSet.Tables[0]);
        //            if (objProdspecsdDataSet.Tables.Count > 1)
        //            { EnableFormulaforColumns(0, familyId, categoryId, catalogId, objProdspecsdDataSet.Tables[1]); }
        //            if (objProdspecsdDataSet.Tables.Count > 0)
        //            {
        //                if (objProdspecsdDataSet.Tables[0].Columns.Contains("Sort"))
        //                {
        //                    //objProdspecsdDataSet.Tables[0].Columns.Remove("Sort");
        //                }
        //            }
        //            //string flname = "Excel\\" + familyId + ".xls";
        //            //string path = Server.MapPath("~/Content/");
        //            //string fileName, fileNames;
        //            //string destFile = Path.Combine(path, flname);
        //            //if (System.IO.File.Exists(destFile))
        //            //{
        //            //    int count = 1;
        //            //    fileNames = "Excel\\" + familyId + "(" + count.ToString(CultureInfo.InvariantCulture) + ")" + ".xls";
        //            //    fileName = path + fileNames;
        //            //    while (System.IO.File.Exists(fileName))
        //            //    {
        //            //        count++;
        //            //        fileNames = "Excel\\" + familyId + "(" + count.ToString(CultureInfo.InvariantCulture) + ")" + ".xls";
        //            //        fileName = path + fileNames;
        //            //    }

        //            //}
        //            //else
        //            //{
        //            //    fileNames = "Excel\\" + familyId + ".xls";
        //            //    fileName = path + fileNames;
        //            //}
        //            var familyname = _dbcontext.TB_FAMILY.Find(familyId);

        //            var familynamewithoutspl = Regex.Replace(familyname.FAMILY_NAME, @"[^0-9a-zA-Z ]+", "_");
        //            ExportDataSetToExcel(objProdspecsdDataSet, familynamewithoutspl + ".xls", displayIdColumns);
        //            return familynamewithoutspl + ".xls";
        //        }
        //    }
        //    catch (Exception objexception)
        //    {
        //        Logger.Error("Error at CatalogController : GetProdSpecsExport", objexception);
        //        return "-1";
        //    }
        //}


        //Srianjeev MS_10_9_3 Family Export//
        public string GetProdSpecsExport(int catalogId, int familyId, bool displayIdColumns, string categoryId, bool EnableSubProduct)
        {
            try
            {
                var objProdspecsdDataSet = new DataSet();
                // var objExport = new Export();
                string attrbutteids = "";
                string attr = string.Empty;
                string SelectedCatIds = string.Empty;
                string catids = "'" + categoryId + "'";

                // SelectedCatIds = selcategory_id.Replace(",", "','");
                DataTable ds = new DataTable();
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO_ROOTCATEGORYFILTER";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = catids;
                    objSqlConnection.Open();
                    var objDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objDataAdapter.Fill(ds);
                    objSqlConnection.Close();
                }
                int index = ds.Rows.Count - 1;
                categoryId = ds.Rows[index].ItemArray[0].ToString();

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand objSqlCommandExoprt = objSqlConnection.CreateCommand();
                    SqlCommand tableDesignerExportSqlCommand = objSqlConnection.CreateCommand();
                    tableDesignerExportSqlCommand.CommandText = "STP_LS_Export_Attributes";
                    tableDesignerExportSqlCommand.CommandType = CommandType.StoredProcedure;
                    tableDesignerExportSqlCommand.CommandTimeout = 0;
                    tableDesignerExportSqlCommand.Connection = objSqlConnection;
                    tableDesignerExportSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 3000).Value = familyId;
                    tableDesignerExportSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar, 100).Value = categoryId;
                    tableDesignerExportSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                    attr = Convert.ToString(tableDesignerExportSqlCommand.ExecuteScalar());
                    objSqlConnection.Close();
                }

                attr = attr.Replace(",,", ",");
                Guid objGuid = Guid.NewGuid();
                string sessionid = Regex.Replace(objGuid.ToString(), "[^a-zA-Z0-9_]+", "");
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.CommandText = "STP_LS_FAMILY_EXPORT_LATEST";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                    objSqlCommand.Parameters.Add("@ATTRIBUTE_IDS", SqlDbType.NVarChar, -1).Value = attr;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar, 500).Value = categoryId;
                    objSqlCommand.Parameters.Add("@SESSID", SqlDbType.NVarChar, 100).Value = sessionid;
                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 100).Value = familyId;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objProdspecsdDataSet);
                    EnableFormulaforColumns(0, familyId, categoryId, catalogId, objProdspecsdDataSet.Tables[0]);
                    if (objProdspecsdDataSet.Tables.Count > 1)
                    { EnableFormulaforColumns(0, familyId, categoryId, catalogId, objProdspecsdDataSet.Tables[1]); }
                    if (objProdspecsdDataSet.Tables.Count > 0)
                    {
                        if (objProdspecsdDataSet.Tables[0].Columns.Contains("Sort"))
                        {
                            //objProdspecsdDataSet.Tables[0].Columns.Remove("Sort");
                        }
                    }

                    var checksubfam = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID == familyId).Select(x => x.ROOT_FAMILY).FirstOrDefault();
                    if (checksubfam.ToString() == "1")
                    {

                        var rows1 = objProdspecsdDataSet.Tables[2].Select("Family_id <> " + familyId + "");
                        foreach (var row in rows1)
                            row.Delete();
                        var rowssub = objProdspecsdDataSet.Tables[2].Select("Subfamily_id > 0");
                        foreach (var row in rowssub)
                            row.Delete();
                    }
                    else
                    {
                        var rows1 = objProdspecsdDataSet.Tables[2].Select("subfamily_id <> " + familyId + "");
                        foreach (var row in rows1)
                            row.Delete();

                        for (int i = objProdspecsdDataSet.Tables[2].Rows.Count - 1; i >= 0; i--)
                        {
                            DataRow dr = objProdspecsdDataSet.Tables[2].Rows[i];
                            if (!(dr["SUBFAMILY_ID"].ToString() == familyId.ToString()))
                            {
                                dr.Delete();
                            }

                        }
                        objProdspecsdDataSet.Tables[2].AcceptChanges();
                    }



                    var familyname = _dbcontext.TB_FAMILY.Find(familyId);

                    var familynamewithoutspl = Regex.Replace(familyname.FAMILY_NAME, @"[^0-9a-zA-Z ]+", "_");
                    ExportDataSetToExcelfamily(objProdspecsdDataSet, familynamewithoutspl + ".xls", displayIdColumns);
                    return familynamewithoutspl + ".xls";
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at CatalogController : GetProdSpecsExport", objexception);
                return "-1";
            }
        }
        //Srianjeev MS_10_9_3 Family Export//
        public void ExportDataSetToExcelfamily(DataSet finalDs, string fileName, bool displayIdColumns)
        {
            try
            {


                // To Allocate the coloums properly  -- Start
                DataTable FinalCategoryResult = finalDs.Tables[3];
                Session["ExportTableCAT"] = null;
                if (FinalCategoryResult.Columns.Contains("C S 1"))
                {
                    FinalCategoryResult.Columns.Remove("C S 1");
                }
                if (FinalCategoryResult.Columns.Contains("CS2"))
                {
                    FinalCategoryResult.Columns.Remove("CS2");
                }
                if (FinalCategoryResult.Columns.Contains("CS3"))
                {
                    FinalCategoryResult.Columns.Remove("CS3");
                }
                if (FinalCategoryResult.Columns.Contains("CS4"))
                {
                    FinalCategoryResult.Columns.Remove("CS4");
                }
                if (FinalCategoryResult.Columns.Contains("CS5"))
                {
                    FinalCategoryResult.Columns.Remove("CS5");
                }
                if (FinalCategoryResult.Columns.Contains("CS6"))
                {
                    FinalCategoryResult.Columns.Remove("CS6");
                }
                if (FinalCategoryResult.Columns.Contains("CS7"))
                {
                    FinalCategoryResult.Columns.Remove("CS7");
                }
                if (FinalCategoryResult.Columns.Contains("CS8"))
                {
                    FinalCategoryResult.Columns.Remove("CS8");
                }
                if (FinalCategoryResult.Columns.Contains("CS9"))
                {
                    FinalCategoryResult.Columns.Remove("CS9");
                }
                if (FinalCategoryResult.Columns.Contains("CS10"))
                {
                    FinalCategoryResult.Columns.Remove("CS10");
                }
                if (FinalCategoryResult.Columns.Contains("CS11"))
                {
                    FinalCategoryResult.Columns.Remove("CS11");
                }
                if (FinalCategoryResult.Columns.Contains("CS12"))
                {
                    FinalCategoryResult.Columns.Remove("CS12");
                }
                if (!displayIdColumns)
                {
                    if (FinalCategoryResult.Columns.Contains("CATALOG_ID"))
                    {
                        FinalCategoryResult.Columns.Remove("CATALOG_ID");
                    }
                    if (FinalCategoryResult.Columns.Contains("CATEGORY_ID"))
                    {
                        FinalCategoryResult.Columns.Remove("CATEGORY_ID");
                    }

                }

                // System.Web.HttpContext.Current.Session["ExportTableCAT"] = FinalCategoryResult;
                System.Web.HttpContext.Current.Session["ExportTableCAT"] = null;
                //Session["ExportTableCAT"] = FinalCategoryResult;
                //-- End

                DataTable FinalFamilyResult = finalDs.Tables[2];
                Session["ExportTableFAM"] = null;

                if (FinalFamilyResult.Columns.Contains("SUBFAMILY_ID"))
                {
                    FinalFamilyResult.Columns.Remove("SUBFAMILY_ID");
                }
                if (FinalFamilyResult.Columns.Contains("FAMILY_FOOT_NOTES"))
                {
                    FinalFamilyResult.Columns.Remove("FAMILY_FOOT_NOTES");
                }

                if (FinalFamilyResult.Columns.Contains("FAMILY_STATUS"))
                {
                    FinalFamilyResult.Columns.Remove("FAMILY_STATUS");
                }
                if (FinalFamilyResult.Columns.Contains("CATEGORY_PUBLISH2WEB"))
                {
                    FinalFamilyResult.Columns.Remove("CATEGORY_PUBLISH2WEB");
                }
                if (FinalFamilyResult.Columns.Contains("CATEGORY_PUBLISH2PRINT"))
                {
                    FinalFamilyResult.Columns.Remove("CATEGORY_PUBLISH2PRINT");
                }
                if (FinalFamilyResult.Columns.Contains("CATEGORY_PUBLISH2PDF"))
                {
                    FinalFamilyResult.Columns.Remove("CATEGORY_PUBLISH2PDF");
                }
                if (FinalFamilyResult.Columns.Contains("CATEGORY_PUBLISH2EXPORT"))
                {
                    FinalFamilyResult.Columns.Remove("CATEGORY_PUBLISH2EXPORT");
                }
                if (FinalFamilyResult.Columns.Contains("CATEGORY_PUBLISH2PORTAL"))
                {
                    FinalFamilyResult.Columns.Remove("CATEGORY_PUBLISH2PORTAL");
                }
                if (FinalFamilyResult.Columns.Contains("Family_PdfTemplate"))
                {
                    FinalFamilyResult.Columns.Remove("Family_PdfTemplate");
                }
                if (FinalFamilyResult.Columns.Contains("Product_PdfTemplate"))
                {
                    FinalFamilyResult.Columns.Remove("Product_PdfTemplate");
                }
                if (FinalFamilyResult.Columns.Contains("FAMILY_PUBLISH2PORTAL"))
                {
                    FinalFamilyResult.Columns.Remove("FAMILY_PUBLISH2PORTAL");
                }
                if (FinalFamilyResult.Columns.Contains("FAMILY_PUBLISH2EXPORT"))
                {
                    FinalFamilyResult.Columns.Remove("FAMILY_PUBLISH2EXPORT");
                }
                //Family_ID_1 Column Removal
                if (FinalFamilyResult.Columns.Contains("FAMILY_ID_1"))
                {
                    FinalFamilyResult.Columns.Remove("FAMILY_ID_1");
                }
                if (!displayIdColumns)
                {
                    if (FinalFamilyResult.Columns.Contains("FAMILY_ID"))
                    {
                        FinalFamilyResult.Columns.Remove("FAMILY_ID");
                    }
                    if (FinalFamilyResult.Columns.Contains("CATALOG_ID"))
                    {
                        FinalFamilyResult.Columns.Remove("CATALOG_ID");
                    }
                    if (FinalFamilyResult.Columns.Contains("CATEGORY_ID"))
                    {
                        FinalFamilyResult.Columns.Remove("CATEGORY_ID");
                    }
                    if (FinalFamilyResult.Columns.Contains("SUBCATID_L1"))
                    {
                        FinalFamilyResult.Columns.Remove("SUBCATID_L1");
                    }
                    if (FinalFamilyResult.Columns.Contains("SUBCATID_L2"))
                    {
                        FinalFamilyResult.Columns.Remove("SUBCATID_L2");
                    }
                    if (FinalFamilyResult.Columns.Contains("SUBCATID_L3"))
                    {
                        FinalFamilyResult.Columns.Remove("SUBCATID_L3");
                    }
                    if (FinalFamilyResult.Columns.Contains("SUBCATID_L4"))
                    {
                        FinalFamilyResult.Columns.Remove("SUBCATID_L4");
                    }
                    if (FinalFamilyResult.Columns.Contains("SUBCATID_L5"))
                    {
                        FinalFamilyResult.Columns.Remove("SUBCATID_L5");
                    }
                    if (FinalFamilyResult.Columns.Contains("SUBCATID_L6"))
                    {
                        FinalFamilyResult.Columns.Remove("SUBCATID_L6");
                    }
                    if (FinalFamilyResult.Columns.Contains("SUBCATID_L7"))
                    {
                        FinalFamilyResult.Columns.Remove("SUBCATID_L7");
                    }
                    if (FinalFamilyResult.Columns.Contains("PRODUCT_ID"))
                    {
                        FinalFamilyResult.Columns.Remove("PRODUCT_ID");
                    }
                    if (FinalFamilyResult.Columns.Contains("SUBFAMILY_ID"))
                    {
                        FinalFamilyResult.Columns.Remove("SUBFAMILY_ID");
                    }

                }
                System.Web.HttpContext.Current.Session["ExportTableFAM"] = FinalFamilyResult;

                DataTable FinalProductResult = finalDs.Tables[0];
                Session["ExportTable"] = null;
                if (FinalProductResult.Columns.Contains("SUBFAMILY_NAME"))
                {
                    FinalProductResult.Columns.Remove("SUBFAMILY_NAME");
                }
                if (FinalProductResult.Columns.Contains("SUBFAMILY_ID"))
                {
                    FinalProductResult.Columns.Remove("SUBFAMILY_ID");
                }

                if (FinalProductResult.Columns.Contains("FAMILY_FOOT_NOTES"))
                {
                    FinalProductResult.Columns.Remove("FAMILY_FOOT_NOTES");
                }
                if (FinalProductResult.Columns.Contains("FAMILY_STATUS"))
                {
                    FinalProductResult.Columns.Remove("FAMILY_STATUS");
                }
                if (FinalProductResult.Columns.Contains("FAMILY_ID_1"))
                {
                    FinalProductResult.Columns.Remove("FAMILY_ID_1");
                }
                // removal of export and portal
                if (FinalProductResult.Columns.Contains("PUBLISH2EXPORT"))
                {
                    FinalProductResult.Columns.Remove("PUBLISH2EXPORT");
                }
                if (FinalProductResult.Columns.Contains("PUBLISH2PORTAL"))
                {
                    FinalProductResult.Columns.Remove("PUBLISH2PORTAL");
                }
                if (!displayIdColumns)
                {
                    if (FinalProductResult.Columns.Contains("SUBFAMILY_ID"))
                    {
                        FinalProductResult.Columns.Remove("SUBFAMILY_ID");
                    }
                    if (FinalProductResult.Columns.Contains("FAMILY_ID"))
                    {
                        FinalProductResult.Columns.Remove("FAMILY_ID");
                    }
                    if (FinalProductResult.Columns.Contains("CATALOG_ID"))
                    {
                        FinalProductResult.Columns.Remove("CATALOG_ID");
                    }
                    if (FinalProductResult.Columns.Contains("CATEGORY_ID"))
                    {
                        FinalProductResult.Columns.Remove("CATEGORY_ID");
                    }
                    if (FinalProductResult.Columns.Contains("SUBCATID_L1"))
                    {
                        FinalProductResult.Columns.Remove("SUBCATID_L1");
                    }
                    if (FinalProductResult.Columns.Contains("SUBCATID_L2"))
                    {
                        FinalProductResult.Columns.Remove("SUBCATID_L2");
                    }
                    if (FinalProductResult.Columns.Contains("SUBCATID_L3"))
                    {
                        FinalProductResult.Columns.Remove("SUBCATID_L3");
                    }
                    if (FinalProductResult.Columns.Contains("SUBCATID_L4"))
                    {
                        FinalProductResult.Columns.Remove("SUBCATID_L4");
                    }
                    if (FinalProductResult.Columns.Contains("SUBCATID_L5"))
                    {
                        FinalProductResult.Columns.Remove("SUBCATID_L5");
                    }
                    if (FinalProductResult.Columns.Contains("SUBCATID_L6"))
                    {
                        FinalProductResult.Columns.Remove("SUBCATID_L6");
                    }
                    if (FinalProductResult.Columns.Contains("SUBCATID_L7"))
                    {
                        FinalProductResult.Columns.Remove("SUBCATID_L7");
                    }

                    if (FinalProductResult.Columns.Contains("PRODUCT_ID"))
                    {
                        FinalProductResult.Columns.Remove("PRODUCT_ID");
                    }
                    if (FinalProductResult.Columns.Contains("Prod Key"))
                    {
                        FinalProductResult.Columns.Remove("Prod Key");
                    }
                    if (FinalProductResult.Columns.Contains("Prod Spec"))
                    {
                        FinalProductResult.Columns.Remove("Prod Spec");
                    }
                    if (FinalProductResult.Columns.Contains("Prod Image"))
                    {
                        FinalProductResult.Columns.Remove("Prod Image");
                    }
                    if (FinalProductResult.Columns.Contains("Prod Price"))
                    {
                        FinalProductResult.Columns.Remove("Prod Price");
                    }
                }
                System.Web.HttpContext.Current.Session["ExportTable"] = FinalProductResult;

            }

            catch (Exception objexception)
            {
                Logger.Error("Error at Export : ExportXls", objexception);
            }
        }
        //Srianjeev MS_10_9_3 Family Export//




        /// <summary>
        /// To return a pdf xpress data 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns>
        /// Author : Mariya vijayan:
        /// </returns>
        public DataTable ExportDataForPdfXpress(DataTable dt)
        {

            try
            {

                string FamilyFilePath;
                string ProductFilePath;

                // Get the data into pdfxpress table ===============================================================================

                //DataTable dt_PdfXpress = new DataTable();
                var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                //var da = new SqlDataAdapter("select * from TB_PDFXPRESS_HIERARCHY", conn);
                //da.Fill(dt_PdfXpress);


                // To Add the manditory coloums for pdf xpress  and set odinal [coloum order]

                if (!dt.Columns.Contains("Catalog_PdfTemplate"))
                {
                    dt.Columns.Add("Catalog_PdfTemplate");
                    dt.Columns.Add("Category_PdfTemplate");
                    dt.Columns.Add("Family_PdfTemplate");
                    dt.Columns.Add("Product_PdfTemplate");

                }


                // Update the Values for pdfxpress coloums



                string CatalogName = dt.AsEnumerable().Select(x => x.Field<string>("CATALOG_NAME")).FirstOrDefault();
                int catalog_Id = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_NAME == CatalogName.ToString()).Select(x => x.CATALOG_ID).FirstOrDefault();
                string Category_Id = dt.AsEnumerable().Select(x => x.Field<string>("CATEGORY_ID")).FirstOrDefault();
                int family_Id = dt.AsEnumerable().Select(x => x.Field<int>("FAMILY_ID")).FirstOrDefault();


                string Cat_Id = Convert.ToString(catalog_Id);



                #region Catalog _details Update===============================================================================

                string Catalog_FileName = homeController.getPdfXpressdefaultTemplate(Cat_Id);

                if (!string.IsNullOrEmpty(Catalog_FileName))
                {

                    string FilePath = "/PDF_Template/" + Catalog_FileName;

                    string fullFileName = Cat_Id + "_";

                    FilePath = FilePath.Replace(fullFileName, "").ToString();

                    // Update table values by linq

                    dt.Select().ToList<DataRow>().ForEach(x => x["Catalog_PdfTemplate"] = FilePath);

                    //foreach (DataRow dr in dt.Rows)
                    //{
                    //    dr["Catalog_PdfTemplate"] = FilePath.ToString();
                    //}
                }


                #endregion====================================================================================================


                if (!string.IsNullOrEmpty(Catalog_FileName))
                {


                    #region Category _details Update===============================================================================


                    string CategoryFilePath = homeController.GetPdfXpressdefaultType("CATEGORY", Category_Id, catalog_Id.ToString());

                    if (CategoryFilePath != null)
                    {
                        string FilePathCategory = "/PDF_Template/" + CategoryFilePath;

                        // Update values by linq

                        dt.Select().ToList<DataRow>().ForEach(x => x["Category_PdfTemplate"] = FilePathCategory);


                        //foreach (DataRow dr in dt.Rows)
                        //{
                        //    dr["Category_PdfTemplate"] = FilePathCategory;
                        //}

                    }



                    #endregion===============================================================================


                    #region Family _details Update===============================================================================

                    string CATID = catalog_Id.ToString();
                    string FAMID = family_Id.ToString();




                    //SqlCommand cmd = new SqlCommand("select TEMPLATE_NAME from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CATID + "' and ASSIGN_TO = '" + FAMID + "' and [TYPE] = 'family' ", conn);
                    //conn.Open();
                    //FamilyFilePath = (string)cmd.ExecuteScalar();
                    //conn.Close();
                    FamilyFilePath = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(x => x.CATALOG_ID == catalog_Id && x.ASSIGN_TO == FAMID && x.TYPE == "family").Select(a => a.TEMPLATE_NAME).FirstOrDefault();

                    if (FamilyFilePath != null)
                    {
                        string FilePathFamily = "/PDF_Template/" + FamilyFilePath;


                        // Update Family_PdfTemplate values in linq.

                        dt.Select().ToList<DataRow>().ForEach(x => x["Family_PdfTemplate"] = FilePathFamily);

                        //foreach (DataRow dr in dt.Rows)
                        //{
                        //    dr["Family_PdfTemplate"] = FilePathFamily;
                        //}


                    }

                    #endregion===============================================================================


                    #region Product _details Update===============================================================================


                    //cmd = new SqlCommand("select TEMPLATE_NAME from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CATID + "' and ASSIGN_TO = '" + FAMID + "' and [TYPE] = 'product' ", conn);
                    //conn.Open();
                    //ProductFilePath = (string)cmd.ExecuteScalar();
                    //conn.Close();

                    ProductFilePath = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(x => x.CATALOG_ID == catalog_Id && x.ASSIGN_TO == FAMID && x.TYPE == "product").Select(a => a.TEMPLATE_NAME).FirstOrDefault();


                    //  ProductFilePath = homeController.GetPdfXpressdefaultType("PRODUCT", family_Id.ToString(), catalog_Id.ToString());


                    if (ProductFilePath != null)
                    {
                        string FilePathProduct = "/PDF_Template/" + ProductFilePath;

                        // update Product_PdfTemplate values by linq

                        dt.Select().ToList<DataRow>().ForEach(x => x["Product_PdfTemplate"] = FilePathProduct);

                        //foreach (DataRow dr in dt.Rows)
                        //{
                        //    dr["Product_PdfTemplate"] = FilePathProduct;
                        //}

                    }

                    #endregion===============================================================================


                }

                return dt;
            }
            catch (Exception ex)
            {

                return null;
            }

        }

        /// <summary>
        /// To allocate the Coloums properly
        /// </summary>
        /// <param name="dt"></param>
        public void AllocateTheColoums(DataTable dt)
        {
            var count = dt.Columns.Count;
            var withoutTemplateCount = (count - 4);

            if (dt.Columns.Contains("Action"))
            {
                dt.Columns["Action"].SetOrdinal(0);
            }

            dt.Columns["PUBLISH2WEB"].SetOrdinal(dt.Columns.Count - 1);

            dt.Columns["PUBLISH2PRINT"].SetOrdinal(dt.Columns.IndexOf("PUBLISH2WEB"));

            dt.Columns["PUBLISH2PDF"].SetOrdinal(dt.Columns.IndexOf("PUBLISH2PRINT"));

            dt.Columns["PUBLISH2EXPORT"].SetOrdinal(dt.Columns.IndexOf("PUBLISH2PDF"));

            dt.Columns["PUBLISH2PORTAL"].SetOrdinal(dt.Columns.IndexOf("PUBLISH2EXPORT"));

        }


        public void ExportDataSetToExcel(DataSet finalDs, string fileName, bool displayIdColumns)
        {
            try
            {


                // To Allocate the coloums properly  -- Start




                //-- End

              //  DataTable FinalExportResult = finalDs.Tables[0];
                 DataTable FinalExportResult = ExportDataForPdfXpress(finalDs.Tables[0]);
                AllocateTheColoums(FinalExportResult);
                Session["ExportTable"] = null;
                Session["ExportTableSubProduct"] = null;
                if (FinalExportResult.Columns.Contains("FAMILY_ID_1"))
                {
                    FinalExportResult.Columns.Remove("FAMILY_ID_1");
                }
                if (FinalExportResult.Columns.Contains("FAMILY_FOOT_NOTES"))
                {
                    FinalExportResult.Columns.Remove("FAMILY_FOOT_NOTES");
                }
                if (FinalExportResult.Columns.Contains("FAMILY_STATUS"))
                {
                    FinalExportResult.Columns.Remove("FAMILY_STATUS");
                }
                if (!displayIdColumns)
                {
                    if (FinalExportResult.Columns.Contains("FAMILY_ID"))
                    {
                        FinalExportResult.Columns.Remove("FAMILY_ID");
                    }
                    if (FinalExportResult.Columns.Contains("CATALOG_ID"))
                    {
                        FinalExportResult.Columns.Remove("CATALOG_ID");
                    }
                    if (FinalExportResult.Columns.Contains("CATEGORY_ID"))
                    {
                        FinalExportResult.Columns.Remove("CATEGORY_ID");
                    }
                    if (FinalExportResult.Columns.Contains("PRODUCT_ID"))
                    {
                        FinalExportResult.Columns.Remove("PRODUCT_ID");
                    }

                }
                //if (finalDs.Tables[0].Columns.Contains("PUBLISH2PRINT"))
                //{
                //    finalDs.Tables[0].Columns.Remove("PUBLISH2PRINT");
                //}
                if (FinalExportResult.Columns.Contains("PUBLISH"))
                {
                    FinalExportResult.Columns.Remove("PUBLISH");
                }
                if (fileName.Trim() == "") return;
                if (finalDs.Tables.Count >= 1)
                {
                    // To Add the pdf Xpress Values into export default values.



                    Session["ExportTable"] = FinalExportResult;
                }
                if (finalDs.Tables.Count > 1 && finalDs.Tables[1].Rows.Count > 0)
                {
                    if (!displayIdColumns)
                    {
                        if (finalDs.Tables[1].Columns.Contains("FAMILY_ID"))
                        {
                            finalDs.Tables[1].Columns.Remove("FAMILY_ID");
                        }
                        if (finalDs.Tables[1].Columns.Contains("CATALOG_ID"))
                        {
                            finalDs.Tables[1].Columns.Remove("CATALOG_ID");
                        }
                        if (finalDs.Tables[1].Columns.Contains("CATEGORY_ID"))
                        {
                            finalDs.Tables[0].Columns.Remove("CATEGORY_ID");
                        }
                        if (finalDs.Tables[1].Columns.Contains("PRODUCT_ID"))
                        {
                            finalDs.Tables[1].Columns.Remove("PRODUCT_ID");
                        }
                        if (finalDs.Tables[1].Columns.Contains("SUBPRODUCT_ID"))
                        {
                            finalDs.Tables[1].Columns.Remove("SUBPRODUCT_ID");
                        }
                    }
                    if (finalDs.Tables[1].Columns.Contains("item #"))
                    {
                        finalDs.Tables[1].Columns.Remove("Item #");
                    }
                    if (finalDs.Tables[1].Columns.Contains("Item#"))
                    {
                        finalDs.Tables[1].Columns.Remove("Item#");
                    }
                    if (finalDs.Tables[1].Columns.Contains("ITEM#"))
                    {
                        finalDs.Tables[1].Columns.Remove("ITEM#");
                    }
                    if (finalDs.Tables[1].Columns.Contains("SUBCATALOG_ITEM_NO"))
                    {
                        finalDs.Tables[1].Columns["SUBCATALOG_ITEM_NO"].ColumnName = "SUBITEM#";
                    }
                    if (finalDs.Tables[1].Columns.Contains("CATALOG_ITEM_NO"))
                    {
                        finalDs.Tables[1].Columns["CATALOG_ITEM_NO"].ColumnName = "ITEM#";
                    }
                    Session["ExportTableSubProduct"] = finalDs.Tables[1];

                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at Export : ExportXls", objexception);
            }
        }

        #region reference table import/export
        private string Message;
        List<string> resultsReference = new List<string>();

        public FileResult DownloadSample()
        {
            string fileName = "ReferenceExport.xls";
            string path = Path.Combine(Server.MapPath("~/Content/Excel/"), fileName);
            return File(path, "application/vnd.ms-excel", "Sample.xls");
        }

        public JsonResult SaveFilesReferenceTable()
        {
            try
            {
                long returnTableId = 0;
                string fileName, actualFileName;
                string FilePath = string.Empty;
                Message = fileName = actualFileName = string.Empty;
                bool flag = false;
                bool isNew = false;
                if (Request.Files != null)
                {

                    var file = Request.Files[0];
                    actualFileName = file.FileName;
                    fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    int size = file.ContentLength;
                    try
                    {
                        string physicalPath = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName);
                        file.SaveAs(physicalPath);

                        //CheckImportSheet(FilePath);                        
                        AdvanceImportApiController adImport = new AdvanceImportApiController();
                        DataTable objDatatable = GetDataFromExcel(actualFileName, physicalPath, " SELECT * from [" + actualFileName + "$]", "");
                        bool isvalid = CheckFileIsValid(objDatatable);

                        if (isvalid)
                        {
                            string tableName = string.Empty;
                            if (objDatatable != null)
                            {
                                Object tableNameObj = objDatatable.Columns[0].ColumnName;
                                string tableColumnName = objDatatable.Columns[0].ColumnName;
                                string rowValue = objDatatable.Rows[0][tableColumnName].ToString();
                                tableName = rowValue;
                              
                            }

                            long tableId = 0;
                            TB_REFERENCE_TABLE refTable = new TB_REFERENCE_TABLE();
                            if (tableName != null)
                                refTable = _dbcontext.TB_REFERENCE_TABLE.Where(s => s.TABLE_NAME == tableName).FirstOrDefault();
                            if (refTable != null && refTable.TABLE_ID > 0)
                            {
                                tableId = refTable.TABLE_ID;
                                returnTableId = tableId;
                            }
                            if (tableId != 0)
                            {


                                var fs = new DataSet();
                                using (
                                    var objSqlConnection =
                                        new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ReferenceTable";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@TABLE_ID", SqlDbType.BigInt).Value = tableId;
                                    objSqlConnection.Open();
                                    var da = new SqlDataAdapter(objSqlCommand);
                                    da.Fill(fs);
                                }
                                if (objDatatable.Rows.Count > 0)
                                {
                                    objDatatable.Rows[0].Delete();
                                    objDatatable.AcceptChanges();
                                }

                                if (fs.Tables.Count > 0)
                                {
                                    if (objDatatable.Columns.Count > fs.Tables[0].Columns.Count)
                                    {
                                        int countBeforeSave = objDatatable.Columns.Count - fs.Tables[0].Columns.Count;

                                        for (int j = 1; j <= countBeforeSave; j++)
                                        {
                                            var insertcolumn = _dbcontext.STP_CATALOGSTUDIO5_ReferenceTableRowsColumns(tableId, null, "Text", 0, 0, 0, 0, "Column", "Insert");
                                        }
                                    }

                                    if ((objDatatable.Columns.Count) < fs.Tables[0].Columns.Count)
                                    {
                                        int columnCount = fs.Tables[0].Columns.Count - objDatatable.Columns.Count;
                                        // int i = 0;
                                        for (int j = 0; j < columnCount; j++)
                                        {
                                            var insertcolumn = _dbcontext.STP_CATALOGSTUDIO5_ReferenceTableRowsColumns(tableId, null, "Text", 0, j, j, 0, "Column", "Delete");
                                            // i = i + 1;
                                        }
                                    }

                                    if ((objDatatable.Rows.Count) < fs.Tables[0].Rows.Count)
                                    {
                                        int rowCount = fs.Tables[0].Rows.Count - objDatatable.Rows.Count;
                                        for (int j = 0; j < rowCount; j++)
                                        {
                                            var deleteRow = _dbcontext.STP_CATALOGSTUDIO5_ReferenceTableRowsColumns(tableId, null, "Text", 0, j, j, 0, "Row", "Delete");
                                        }
                                    }

                                }

                                if (fs.Tables.Count == 0)
                                {
                                    isNew = true;
                                    for (int i = 0; i < objDatatable.Rows.Count; i++)
                                    {
                                        var insertrow = _dbcontext.STP_CATALOGSTUDIO5_ReferenceTableRowsColumns(tableId, "", "Text", 0, 0, 0, 0, "Row", "Insert");
                                    }

                                }

                                if (isNew)
                                {
                                    var addColumn = new DataSet();
                                    using (
                                        var objSqlConnection =
                                            new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                    {
                                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                        objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ReferenceTable";
                                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                                        objSqlCommand.Connection = objSqlConnection;
                                        objSqlCommand.Parameters.Add("@TABLE_ID", SqlDbType.BigInt).Value = tableId;
                                        objSqlConnection.Open();
                                        var daaddColumn = new SqlDataAdapter(objSqlCommand);
                                        daaddColumn.Fill(addColumn);
                                    }

                                    if (addColumn.Tables.Count > 0)
                                    {
                                        int columnCount = objDatatable.Columns.Count - 2;

                                        for (int k = 0; k < columnCount; k++)
                                        {
                                            var insertcolumn = _dbcontext.STP_CATALOGSTUDIO5_ReferenceTableRowsColumns(tableId, null, "Text", 0, 0, 0, 0, "Column", "Insert");
                                        }

                                    }

                                }
                                //var fs1 = new DataSet();
                                //using (
                                //    var objSqlConnection =
                                //        new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                //{
                                //    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                //    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ReferenceTable";
                                //    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                //    objSqlCommand.Connection = objSqlConnection;
                                //    objSqlCommand.Parameters.Add("@TABLE_ID", SqlDbType.BigInt).Value = tableId;
                                //    objSqlConnection.Open();
                                //    var da1 = new SqlDataAdapter(objSqlCommand);
                                //    da1.Fill(fs1);
                                //}

                                //DataTable dtAll = new DataTable();
                                //dtAll = fs1.Tables[0].Copy();
                                //dtAll.Merge(objDatatable);

                                var jsonString = JsonConvert.SerializeObject(objDatatable);
                                JArray model = JArray.Parse(jsonString);

                                Message = Savereferencetablevalues(Convert.ToInt32(tableId), model);
                            }
                            else
                            {
                                Message = "Please upload valid file";
                            }

                        }
                        else
                        {
                            Message = "Please upload valid file";
                        }
                    }
                    catch (Exception ex)
                    {
                        Message = "File upload failed, please try again";
                    }
                    ViewBag.TableId = returnTableId;
                    resultsReference.Add(Message);
                    if (returnTableId == 0)
                    {
                        returnTableId = _dbcontext.TB_REFERENCE_TABLE.FirstOrDefault().TABLE_ID;
                    }
                    resultsReference.Add(returnTableId.ToString());
                }
                return Json(resultsReference, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at Import : ImportXls", objexception);
                return Json("Please upload valid file", JsonRequestBehavior.AllowGet);
            }
        }

        public DataTable GetDataFromExcel(string SheetName, string excelPath, string query, string deleteflag)
        {
            DataTable dtexcel = new DataTable();
            DataTable dtexcelNew = new DataTable();
            try
            {
                var fileName = Path.GetFileName(excelPath).Split('.')[0];
                var path = Path.GetFullPath(excelPath);
                var fileExtention = Path.GetExtension(excelPath);
                string excelPathCopy = path.Replace(Path.GetFileName(excelPath), "") + "//" + fileName + "_Copy" + "." + fileExtention;
                using (System.Data.OleDb.OleDbConnection conn = excelConnection(excelPath))
                {
                    DataTable schemaTable = conn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    string ExcelSheetName = schemaTable.Rows[0]["Table_Name"].ToString();
                    DataTable dtColumns = new DataTable();
                    SheetName = ExcelSheetName;
                    SheetName = SheetName.Replace("$", "");
                    try
                    {

                        DataRow[] schemaRow = schemaTable.Select("TABLE_NAME like '" + SheetName + "%'");
                        string sheet = schemaRow[0]["TABLE_NAME"].ToString();
                        int selectedIndex = 0;
                        foreach (DataRow dRow in schemaTable.Rows)
                        {

                            if (sheet.ToUpper() == dRow["TABLE_NAME"].ToString().ToUpper())
                                break;
                            selectedIndex++;
                        }
                        query = " SELECT * from [" + SheetName + "$]";

                        if (!sheet.EndsWith("_"))
                        {

                            System.Data.OleDb.OleDbDataAdapter daexcel = new System.Data.OleDb.OleDbDataAdapter(query, conn);
                            dtexcel.Locale = System.Globalization.CultureInfo.CurrentCulture;


                            daexcel.Fill(dtColumns);

                        }
                        conn.Close();
                        conn.Dispose();
                        var workbook1 = new Workbook();
                        workbook1 = Workbook.Load(excelPath);
                        //foreach (WorksheetColumn colSheet in workbook1.Worksheets[1].Columns)
                        //    dt11.Columns.Add(colSheet.ToString());
                        dtexcel = dtColumns.Clone();
                        bool skipFirstRow = false;
                        if (!deleteflag.ToUpper().Contains("DELETE"))
                        {
                            foreach (WorksheetRow rowSheet in workbook1.Worksheets[selectedIndex].Rows)
                            {
                                if (dtColumns.Columns.Contains("ACTION") && !skipFirstRow)
                                {
                                    skipFirstRow = true;
                                    continue;
                                }
                                DataRow dRow = dtexcel.NewRow();
                                for (int col = 0; col < dtexcel.Columns.Count; col++)
                                    dRow[col] = rowSheet.Cells[col].Value == null ? null : rowSheet.Cells[col].Value.ToString();
                                dtexcel.Rows.Add(dRow);
                            }
                        }
                        else
                        {
                            dtexcel = new DataTable();
                            dtexcel = dtColumns;
                            // objLS = new CSEntities();
                        }


                        if (dtexcel.Rows[0][0] == "") // For indesging import change made here only
                        {
                            dtexcel.Rows.RemoveAt(0);
                        }


                        workbook1.Worksheets.Clear();
                    }
                    catch (Exception ex)
                    {
                        dtexcel = new DataTable();
                        dtexcel = dtColumns;
                    }
                }
                return dtexcel;
            }

            catch (Exception ex)
            {
                dtexcel = new DataTable();
                return dtexcel;
            }
        }

        public System.Data.OleDb.OleDbConnection excelConnection(string excelPath)
        {
            bool hasHeaders = true;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                          ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            else
                //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                         ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection(strConn);
            conn.Open();
            var workbook1 = new Workbook();
            string sheetName;
            foreach (Microsoft.Office.Interop.Excel.Worksheet wSheet in workbook1.Worksheets)
            {
                sheetName = wSheet.Name.ToString();
            }
            return conn;
        }

        public string Savereferencetablevalues(int tableId, JArray model)
        {
            try
            {
                string alert = "";
                var SpaceProvided = _dbcontext.TB_PLAN
                          .Join(_dbcontext.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                          .Join(_dbcontext.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                          .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => x.tcptps.tps.StorageGB).ToList();
                double alotment = (double)SpaceProvided[0];
                var userId = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();
                double folderSize = 0;
                if (string.IsNullOrEmpty(userId))
                {
                    userId = "Questudio";
                }

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + ""));
                }
                var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "");
                DirectoryInfo dInfo = new DirectoryInfo(path);
                long sizeOfDir = DirectorySize(dInfo, true);
                folderSize = (((double)sizeOfDir) / (double)(1024 * 1024 * 1024));

                double availableSpace = alotment - folderSize;

                if (availableSpace > 0)
                {
                    var usedPercentage = ((folderSize / alotment) * 100);

                    var fs1 = new DataSet();
                    using (
                        var objSqlConnection =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ReferenceTable";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.Parameters.Add("@TABLE_ID", SqlDbType.BigInt).Value = tableId;
                        objSqlConnection.Open();
                        var da = new SqlDataAdapter(objSqlCommand);
                        da.Fill(fs1);
                    }

                    var functionAlloweditem = model.Select(x => x).ToList();
                    if (fs1.Tables.Count > 0)
                    {
                        for (int i = fs1.Tables[0].Rows.Count; i < functionAlloweditem.Count; i++)
                        {
                            var insertrow = _dbcontext.STP_CATALOGSTUDIO5_ReferenceTableRowsColumns(tableId, "", "Text", 0, 0, 0, 0, "Row", "Insert");
                        }
                    }
                    var fs = new DataSet();
                    using (
                        var objSqlConnection =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ReferenceTable";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.Parameters.Add("@TABLE_ID", SqlDbType.BigInt).Value = tableId;
                        objSqlConnection.Open();
                        var da = new SqlDataAdapter(objSqlCommand);
                        da.Fill(fs);
                    }

                    string tableName = string.Empty;
                    tableName = _dbcontext.TB_REFERENCE_TABLE.Where(s => s.TABLE_ID == tableId).FirstOrDefault().TABLE_NAME;

                    var functionAlloweditems = model.Select(x => x).ToList();

                    for (int i = 0; i < functionAlloweditems.Count; i++)
                    {
                        if (fs.Tables.Count > 0 && fs.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataColumn rowidcolumn in fs.Tables[1].Columns)
                            {
                                if (rowidcolumn.ColumnName != tableName)
                                {
                                    int columnIndex = rowidcolumn.Ordinal;
                                    var rowValue =
                                        Convert.ToString(functionAlloweditems[i][rowidcolumn.ColumnName])
                                            .Replace("\r\n", "\n")
                                            .Replace("\n", "\r\n");


                                    var rowId = Convert.ToInt32(fs.Tables[1].Rows[i][rowidcolumn.ColumnName]);
                                    if (rowValue.ToLower().EndsWith(".jpg") || rowValue.ToLower().EndsWith(".gif") ||
                                        rowValue.ToLower().EndsWith(".eps") || rowValue.ToLower().EndsWith(".svg") ||
                                        rowValue.ToLower().EndsWith(".tif") ||
                                        rowValue.ToLower().EndsWith(".tiff") ||
                                        rowValue.ToLower().EndsWith(".psd") ||
                                        rowValue.ToLower().EndsWith(".tga") ||
                                        rowValue.ToLower().EndsWith(".pcx") ||
                                        rowValue.ToLower().EndsWith(".bmp") ||
                                        rowValue.ToLower().EndsWith(".png"))
                                    {
                                        _dbcontext.STP_CATALOGSTUDIO5_ReferenceTableRowsColumns(tableId, rowValue, "Image",
                                            rowId, columnIndex,
                                            i, 0, "Row", "Update");
                                    }
                                    else
                                    {
                                        _dbcontext.STP_CATALOGSTUDIO5_ReferenceTableRowsColumns(tableId, rowValue, "Text",
                                            rowId, columnIndex,
                                            i, 0, "Row", "Update");
                                    }
                                }
                            }
                        }
                    }
                    var AssetLimitPercentage = _dbcontext.Customer_Settings.Where(x => x.CustomerId == 1);
                    if (AssetLimitPercentage.Any())
                    {
                        var assetLimitPercentageno = AssetLimitPercentage.FirstOrDefault();
                        double asset = Convert.ToDouble(assetLimitPercentageno.AssetLimitPercentage);
                        if (usedPercentage >= asset)
                        {
                            alert = "Update completed. You are about to reach the maximum memory";
                        }
                        else
                        {
                            alert = "Update completed";
                        }
                    }
                }
                else
                {
                    alert = "You have reached the maximum allotted memory, please contact Administrator";
                }
                return alert;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController : Savereferencetablevalues", objException);

                return "File upload failed, please try again";
            }

        }

        static long DirectorySize(DirectoryInfo dInfo, bool includeSubDir)
        {
            long totalSize = dInfo.EnumerateFiles()
                         .Sum(file => file.Length);
            if (includeSubDir)
            {
                totalSize += dInfo.EnumerateDirectories()
                         .Sum(dir => DirectorySize(dir, true));
            }
            return totalSize;
        }

        public bool CheckFileIsValid(DataTable dt)
        {
            if (dt != null && (dt.Columns.Count > 0 && dt.Rows.Count > 0))
            {
                bool isValid = true;

                for (int i = 1; i <= dt.Columns.Count - 1; i++)
                {
                    if (dt.Columns[i].ColumnName == "Col_" + i)
                    {
                        isValid = true;
                    }
                    else
                    {
                        isValid = false;
                    }
                }

                for (int i = 1; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][0].Equals("Row_" + (i)))
                    {
                        isValid = true;
                    }
                    else
                    {
                        isValid = false;
                    }
                }
                return isValid;

            }
            else
                return false;
        }
        #endregion


        #region Attribute Grouping

        public JsonResult FamilyPackages(int catalogid)
        {
            string groupType = "Family";
            var data = _dbcontext.TB_PACKAGE_MASTER.Where(s => s.CATALOG_ID == catalogid && s.IS_FAMILY == groupType && s.FLAG_RECYCLE == "A").Select(s => new { s.GROUP_ID, s.GROUP_NAME }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ProductPackages(int catalogid)
        {
            string groupType = "Product";
            var data = _dbcontext.TB_PACKAGE_MASTER.Where(s => s.CATALOG_ID == catalogid && s.IS_FAMILY == groupType && s.FLAG_RECYCLE=="A").Select(s => new { s.GROUP_ID, s.GROUP_NAME }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region InDesignImport

        public JsonResult SaveInDesignImportFile(int catalogId)
        {
            try
            {
                string fileName, actualFileName;
                string FilePath = string.Empty;
                Message = fileName = actualFileName = string.Empty;
                if (Request.Files != null)
                {
                    var file = Request.Files[0];
                    actualFileName = file.FileName;
                    fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    int size = file.ContentLength;
                    try
                    {
                        string physicalPath = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName);
                        file.SaveAs(physicalPath);

                        AdvanceImportApiController adImport = new AdvanceImportApiController();
                        DataTable objDatatable = GetDataFromExcel(actualFileName, physicalPath, " SELECT * from [" + actualFileName + "$]", "");

                        bool isvalid = CheckFileIsValid(objDatatable);

                        var objDataSet = new DataSet();

                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESIGNEXPORT_IMPORT";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = 0;
                            objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETPARENT";
                            objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.Int).Value = 0;
                            objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.VarChar).Value = "";
                            objSqlConnection.Open();
                            var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                            objSqlDataAdapter.Fill(objDataSet);
                        }


                    }
                    catch (Exception ex)
                    {
                        Message = "File upload failed, please try again";
                    }

                }
                return Json(resultsReference, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at Import : ImportXls", objexception);
                return Json("Please upload valid file", JsonRequestBehavior.AllowGet);
            }
        }



        #endregion
        /// <summary>
        /// Default Pdf Template by Catalog Added By Mariya Vijayan
        /// </summary>
        /// 
        /// <returns>
        /// Returns File is saved or not  
        /// </returns>

        public JsonResult SaveFiles()
        {
            string fileName = string.Empty;
            string filePath = string.Empty;
            string actualFileName = string.Empty;

            bool flag = false;

            if (Request.Files != null)
            {

                var file = Request.Files[0];
                var catalogId = Request.Form["CatalogId"];
                actualFileName = file.FileName;
                fileName = file.FileName;
                int size = file.ContentLength;
                string userName = User.Identity.Name;
                string newFilePath = string.Empty;
                string fileExist = string.Empty;
                var userId = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();
              
                // To save the template in PDF_Template folder.

                try
                {
                    string folderPath = "";
                    if (pathCheckFlag.ToLower() == "true")
                    {
                        string localPath = new Uri(serverPathShareVal + "/Content/ProductImages/" + userId + "/PDF_Template").LocalPath;
                        if (!Directory.Exists(localPath))
                        {
                            Directory.CreateDirectory(localPath);
                        }

                        newFilePath = localPath + catalogId + "_" + fileName;


                        // Check the file already exist and delete it


                        folderPath = localPath;
                        fileExist = catalogId + "*";
                        DirectoryInfo dir = new DirectoryInfo(folderPath);
                        FileInfo[] files = dir.GetFiles(fileExist, SearchOption.TopDirectoryOnly);
                        foreach (var item in files)
                        {
                            System.IO.File.Delete(folderPath + "\\" + item.Name);
                        }


                        // End

                        if (!System.IO.File.Exists(newFilePath))
                        {
                            file.SaveAs(Path.Combine(newFilePath));
                            filePath = Path.Combine(newFilePath);
                        }
                    }
                    else
                    {
                        if (!Directory.Exists(Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template")))
                        {
                            Directory.CreateDirectory(Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template"));
                        }

                        newFilePath = "~/Content/ProductImages/" + userId + "/PDF_Template/" + catalogId + "_" + fileName;


                        // Check the file already exist and delete it


                        folderPath = Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template");
                        fileExist = catalogId + "*";
                        DirectoryInfo dir = new DirectoryInfo(folderPath);
                        FileInfo[] files = dir.GetFiles(fileExist, SearchOption.TopDirectoryOnly);
                        foreach (var item in files)
                        {
                            System.IO.File.Delete(folderPath + "\\" + item.Name);
                        }


                        // End

                        if (!System.IO.File.Exists(Server.MapPath(newFilePath)))
                        {
                            file.SaveAs(Path.Combine(Server.MapPath(newFilePath)));
                            filePath = Path.Combine(Server.MapPath(newFilePath));
                        }
                    }

                    // End

                    //if (!System.IO.File.Exists(Server.MapPath(newFilePath)))
                    //{
                    //    file.SaveAs(Path.Combine(Server.MapPath(newFilePath)));
                    //    filePath = Path.Combine(Server.MapPath(newFilePath));
                    //}



                    // Delete the existing one.
                    int cat_Id = Convert.ToInt32(catalogId);

                    var catalogExists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(x => x.CATALOG_ID == cat_Id).Select(x => x).FirstOrDefault();

                    if (catalogExists != null)
                    {
                        _dbcontext.TB_PDFXPRESS_HIERARCHY.Remove(catalogExists);
                        _dbcontext.SaveChanges();
                    }

                    // To Save The PdfXpress Catalog Value on dataBase:



                    obj_XpressCatalogController.SaveTypeOfPdfxpressHierarchy("Catalog", catalogId, fileName, catalogId);




                }
                catch
                {
                    Message = "Template Upload failed, Please try again";
                }
            }
            return Json(filePath, JsonRequestBehavior.AllowGet);
        }

        //----------------------Indesign Import starts-----------------
        public JsonResult SaveFilesIndesgin()
        {
            List<string> resultsReference = new List<string>();
            List<string> Message1 = new List<string>();
            try
            {
                List<string> validationResult = new List<string>();
                long returnTableId = 0;
                string fileName, actualFileName;
                string FilePath = string.Empty;
                Message = fileName = actualFileName = string.Empty;
                if (Request.Files != null)
                {

                    var file = Request.Files[0];
                    actualFileName = file.FileName;
                    fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    int size = file.ContentLength;
                    //  try
                    //  {
                    string physicalPath = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName);
                    file.SaveAs(physicalPath);

                    //CheckImportSheet(FilePath);                        
                    AdvanceImportApiController adImport = new AdvanceImportApiController();
                    DataTable objDatatable = GetDataFromExcel(actualFileName, physicalPath, " SELECT * from [" + actualFileName + "$]", "");

                    validationResult = IndesignImportvalidation(objDatatable);
                    //if(validationResult.Count == 0)
                    //{
                    //   string result= ImportIndesgin(objDatatable);
                    //}
                    //else
                    //{
                    //    Message1 = validationResult.ToList();
                    //}
                    // if (validationResult=="")
                    //{


                    // }
                    //else
                    //{
                    //    Message = "Please upload valid file";
                    //}
                    //  }
                    //  catch (Exception ex)
                    //  {
                    //      Message = "File upload failed, please try again";
                    //   }
                    // ViewBag.TableId = returnTableId;
                    // resultsReference.Add(Message1);
                    // if (returnTableId == 0)
                    // {
                    //     returnTableId = _dbcontext.TB_REFERENCE_TABLE.FirstOrDefault().TABLE_ID;
                    // }
                    List<string> nameList = Message1.Select(p => p.ToString()).ToList();
                    resultsReference.Add(nameList.ToString());
                }
                return Json(validationResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at Import : ImportXls", objexception);
                return Json("Please upload valid file", JsonRequestBehavior.AllowGet);
            }
        }
        #region IndesignImportvalidation
        public List<string> IndesignImportvalidation(DataTable dt)
        {
            List<string> validationResult = new List<string>();
            try
            {
                DataTable errorTable = new DataTable();
                int index; string indexOfSubCat = string.Empty;
                List<string> errorlistMissing = new List<string>();
                List<string> errorlistNewItem = new List<string>();
                List<string> errorlistDuplicateItem = new List<string>();
                if (!dt.Columns.Contains("CATALOG_ID"))
                {
                    index = dt.Columns["CATALOG_NAME"].Ordinal;
                    dt.Columns.Add("CATALOG_ID").SetOrdinal(index);
                }
                if (!dt.Columns.Contains("CATEGORY_ID"))
                {
                    index = dt.Columns["CATEGORY_NAME"].Ordinal;
                    dt.Columns.Add("CATEGORY_ID").SetOrdinal(index);
                }
                if (!dt.Columns.Contains("FAMILY_ID"))
                {
                    index = dt.Columns["FAMILY_NAME"].Ordinal;
                    dt.Columns.Add("FAMILY_ID").SetOrdinal(index);
                }
                if (!dt.Columns.Contains("PROJECT_ID"))
                {
                    index = dt.Columns["PROJECT_NAME"].Ordinal;
                    dt.Columns.Add("PROJECT_ID").SetOrdinal(index);
                }
                if (!dt.Columns.Contains("DATA_FILE_ID"))
                {
                    index = dt.Columns["DATA_FILE_NAME"].Ordinal;
                    dt.Columns.Add("DATA_FILE_ID").SetOrdinal(index);
                }

                errorlistMissing = ValidateIndesignMissingColumn(dt);
                errorlistNewItem = ValidateIndesignNewItems(dt);
                errorlistDuplicateItem = ValidateDuplicateItem(dt);
                if (!(errorlistMissing.Count == 0))
                {
                    validationResult.Add("Missing Column");
                    validationResult.AddRange(errorlistMissing);
                }
                if (!(errorlistNewItem.Count == 0))
                {
                    validationResult.Add("New Item");
                    validationResult.AddRange(errorlistNewItem);
                }
                if (!(errorlistDuplicateItem.Count == 0))
                {
                    validationResult.Add("DuplicateItem");
                    validationResult.AddRange(errorlistDuplicateItem);
                }
                // if (errorlistMissing.Count == 0 && errorlistNewItem.Count() == 0 && errorlistDuplicateItem.Count() == 0)
                return validationResult;
                // else
                //return validationResult;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at CatalogController :IndesignImportvalidation", objException);
                return validationResult;
            }

        }
        #endregion
        #region missing column validation in Indesign import
        public List<string> ValidateIndesignMissingColumn(DataTable dt)
        {
            DataTable errordt = new DataTable();
            var errorlist = new List<string>();
            var distinctlist = new List<string>();
            /// List<string> errorlist = new List<string>();
            try
            {
                if (!dt.Columns.Contains("ACTION"))
                {
                    dt.Columns["ACTIONS"].SetOrdinal(0);
                }
                errordt.Columns.Add("MISSING_COLUMNS");
                int totalRows = dt.Rows.Count; int projectId; int catalogId; int recordId;
                for (int i = 0; i < totalRows; i++)
                {
                    string action = dt.Rows[i]["ACTION"].ToString().ToLower();
                    if (action != "delete")
                    {
                        if ((dt.Columns.Contains("CATALOG_ID") || dt.Columns.Contains("CATALOG_NAME")) && (dt.Columns.Contains("PROJECT_ID") || dt.Columns.Contains("PROJECT_NAME")) && (dt.Columns.Contains("FAMILY_ID") || dt.Columns.Contains("FAMILY_NAME")) && (dt.Columns.Contains("CATEGORY_ID") || dt.Columns.Contains("CATEGORY_NAME")) && (dt.Columns.Contains("DATA_FILE_ID") || dt.Columns.Contains("DATA_FILE_NAME")))
                        {
                            foreach (DataColumn column in dt.Columns)
                            {

                                if (dt.Columns.Contains("CATALOG_NAME") || dt.Columns.Contains("PROJRCT_NAME") || dt.Columns.Contains("CATEGORY_NAME") || (dt.Columns.Contains("FAMILY_NAME")))
                                {
                                    if (dt.Rows.OfType<DataRow>().Any(r => r.IsNull(column)))
                                    {


                                        DataRow workRow;
                                        workRow = errordt.NewRow();
                                        workRow[0] = column;
                                        if (((column.Caption.Equals("CATALOG_NAME")) || (column.Caption.Equals("CATEGORY_NAME")) || (column.Caption.Equals("FAMILY_NAME")) || (column.Caption.Equals("PROJECT_NAME"))))
                                        {
                                            distinctlist.Add("Missing-" + column.ToString());

                                            errorlist = distinctlist.Distinct().ToList();

                                        }
                                    }
                                }
                            }
                            if (dt.Rows[i]["CATALOG_ID"] is DBNull || dt.Rows[i]["CATALOG_NAME"] is DBNull)
                            {
                                string catalogName = dt.Rows[i]["CATALOG_NAME"].ToString();
                                catalogId = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_NAME == catalogName && x.FLAG_RECYCLE == "A").Select(y => y.CATALOG_ID).FirstOrDefault();
                                if (catalogId != null)
                                {
                                    dt.Rows[i]["CATALOG_ID"] = catalogId;
                                }
                            }
                            if (dt.Rows[i]["PROJECT_ID"] is DBNull || dt.Rows[i]["DATA_FILE_ID"] is DBNull)
                            {
                                string projectName = dt.Rows[i]["PROJECT_NAME"].ToString();
                                catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                                projectId = _dbcontext.TB_PROJECT.Where(x => x.PROJECT_NAME == projectName && x.CATALOG_ID == catalogId).Select(y => y.PROJECT_ID).FirstOrDefault();
                                if (projectId == null)
                                {
                                    errorlist.Remove("Missing-PROJECT_NAME");
                                    // dt.Rows[i]["PROJECT_ID"] = projectId;
                                }
                                string xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                                xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                                var xml = xmlFileName.Split('.');
                                string extenstion = xml[1];
                                if (extenstion == "xml")
                                {
                                    xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                                }
                                else
                                {
                                    xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString() + ".xml";
                                }
                                recordId = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.XML_FILE_NAME == xmlFileName && x.PROJECT_ID == projectId).Select(y => y.RECORD_ID).FirstOrDefault();
                                if (recordId == 0)
                                {
                                    errorlist.Remove("Missing-DATA_FILE_NAME");
                                    errorlist.Remove("Missing-CATEGORY_NAME");
                                    errorlist.Remove("Missing-FAMILY_NAME");
                                }
                            }
                            // errordt.Rows.RemoveAt(0);
                        }

                    }

                }

                //foreach(DataRow dr in errordt.Rows)
                //{
                //    errorlist.Add(dr.ToString());
                //}
                return errorlist;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at CatalogController :ValidateIndesignMissingColumn", objException);
                return errorlist;
            }
        }
        #endregion

        #region NewItem Item validaion in Indesgin import
        public List<string> ValidateIndesignNewItems(DataTable dt)
        {
            List<string> errorlist = new List<string>();
            try
            {
                DataTable objdatatable = dt.Clone();
                int index = 0;
                string indexOfSubCat = string.Empty;
                string subcategorySplit = string.Empty;
                int subCatCount = 0;
                int count = 0;
                bool flag = false; string cat_Name = string.Empty;
                if (dt.Rows.Count > 0)
                {
                    if (dt.Columns.Contains("FAMILY_ID"))
                    {
                        index = dt.Columns["FAMILY_ID"].Ordinal;
                        indexOfSubCat = dt.Columns[index - 1].ToString();
                    }
                    else if (dt.Columns.Contains("FAMILY_NAME"))
                    {
                        index = dt.Columns["FAMILY_NAME"].Ordinal;
                        indexOfSubCat = dt.Columns[index - 1].ToString();
                    }
                    if (indexOfSubCat.Contains("SUBCAT"))
                    {
                        subcategorySplit = indexOfSubCat.Split('_')[1];
                        subCatCount = Convert.ToInt32(subcategorySplit[1].ToString());
                    }
                    if (subCatCount > 0)
                    {
                        for (int i = 1; i <= subCatCount; i++)
                        {
                            if (!dt.Columns.Contains("SUBCATID_L" + i))
                            {
                                index = dt.Columns["SUBCATNAME_L" + i].Ordinal;
                                dt.Columns.Add("SUBCATID_L" + i).SetOrdinal(index);
                            }

                            //if (dt.AsEnumerable().All(dr => dr.IsNull("SUBCATID_L" + i)))
                            //    dt.Columns.Remove("SUBCATID_L" + i);
                            //if (dt.AsEnumerable().All(dr => dr.IsNull("SUBCATNAME_L" + i)))
                            //    dt.Columns.Remove("SUBCATNAME_L" + i);
                        }
                    }
                }

                if ((dt.Columns.Contains("CATALOG_ID") || dt.Columns.Contains("CATALOG_NAME")) && (dt.Columns.Contains("FAMILY_ID") || dt.Columns.Contains("FAMILY_NAME")) && (dt.Columns.Contains("CATEGORY_ID") || dt.Columns.Contains("CATEGORY_NAME")))
                {
                    int totalRows = dt.Rows.Count;
                    for (int i = 0; i < totalRows; i++)
                    {
                        count = subCatCount;
                        int catalogId = 0; int familyId = 0; string categoryId = ""; int projectId = 0; int recordId = 0; string categoryIdProject = ""; string projectName = string.Empty; string categoryName = string.Empty;
                        string categoryShort = string.Empty;
                        //---------------- check for catalogid is exist in datatavle if not fetch from db and add to datatable-------
                        if (dt.Rows[i]["CATALOG_ID"] is DBNull)
                        {
                            string catalogName = dt.Rows[i]["CATALOG_NAME"].ToString();
                            catalogId = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_NAME == catalogName && x.FLAG_RECYCLE == "A").Select(y => y.CATALOG_ID).FirstOrDefault();
                            if (catalogId != null && catalogId != 0)
                            {
                                dt.Rows[i]["CATALOG_ID"] = catalogId;
                            }
                            else
                            {
                                errorlist.Add("New-Does not have catalog like" + catalogName + ".Cannot add catalog here");
                                //return errorlist;
                            }
                        }
                        else
                        {
                            catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                            TB_CATALOG checkcatalogexist = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogId && x.FLAG_RECYCLE == "A").FirstOrDefault();
                            if (checkcatalogexist == null)
                            {
                                errorlist.Add("New-Does not have catalog like" + catalogId + ".Cannot add catalog here");
                                // return errorlist;
                            }

                        }

                        //--------------------------------End---------------
                        if (dt.Rows[i]["CATEGORY_ID"] is DBNull)
                        {
                            categoryName = dt.Rows[i]["CATEGORY_NAME"].ToString();
                            catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                            categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_NAME == categoryName).Select(y => y.CATEGORY_ID).FirstOrDefault();
                            dt.Rows[i]["CATEGORY_ID"] = categoryShort;
                        }
                        else
                        {
                            categoryId = dt.Rows[i]["CATEGORY_ID"].ToString();
                            categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == categoryId).Select(y => y.CATEGORY_ID).FirstOrDefault();
                            if (categoryShort == null)
                            {
                                errorlist.Add("New-" + categoryShort + "category doesnot belong to catalog.Cannot add category here");
                            }
                        }

                        if (dt.Rows[i]["PROJECT_ID"] is DBNull)
                        {
                            projectName = dt.Rows[i]["PROJECT_NAME"].ToString();
                            catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                            projectId = _dbcontext.TB_PROJECT.Join(_dbcontext.TB_CATALOG, tc => tc.CATALOG_ID, tp => tp.CATALOG_ID, (tc, tp) => new { tc, tp }).Where(x => x.tc.PROJECT_NAME == projectName && x.tp.CATALOG_ID == catalogId).Select(y => y.tc.PROJECT_ID).FirstOrDefault();
                            if (projectId != null)
                            {
                                dt.Rows[i]["PROJECT_ID"] = projectId;
                            }
                            else
                            {
                                errorlist.Add("New- Does not have projectName like" + projectName);
                            }
                        }

                        //---------------- check for categoryid is exist in datatable if not fetch from db and add to datatable-------
                        string DATA_FILE_NAME = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                        int filecount = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.XML_FILE_NAME == DATA_FILE_NAME).Count();
                        //if (filecount > 0)
                        //{
                        //    int count1=1;
                        //    while(count1<=count)
                        //    { 
                        //    if (dt.Columns.Contains("SUBCATID_L" + count) || dt.Columns.Contains("SUBCATNAME_L" + count))
                        //    {
                        //        if (!DBNull.Value.Equals(dt.Rows[i]["SUBCATID_L" + count]))
                        //        //if (!(dt.Rows[i]["SUBCATID_L"+count] is DBNull) || (dt.Rows[i]["SUBCATNAME_L"+count] is DBNull))
                        //        {
                        //            if(dt.Rows[i]["SUBCATID_L"+count] is DBNull)
                        //            { 
                        //            string categoryName = dt.Rows[i]["SUBCATNAME_L"+count].ToString();
                        //            categoryId = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tcs => tcs.CATEGORY_ID, tc => tc.CATEGORY_ID, (tcs, tc) => new { tcs, tc }).Where(x => x.tcs.CATEGORY_NAME == categoryName && x.tc.CATEGORY_ID == x.tcs.CATEGORY_ID && x.tc.CATALOG_ID == catalogId && x.tcs.FLAG_RECYCLE == "A").Select(y => y.tc.CATEGORY_ID).FirstOrDefault();
                        //            // string categorycount = _dbcontext.TB_PROJECT_SECTION_DETAILS.Where(x => x.CATEGORY_ID == categoryId).Select(y => y.CATEGORY_ID).FirstOrDefault();
                        //            if (categoryId != null)
                        //            {
                        //                dt.Rows[i]["SUBCATNAME_L" + count] = categoryId;
                        //            }
                        //            else
                        //            {
                        //                errorlist.Add(categoryName + "category doesnot belong to catalog.Cannot add category here");
                        //                return errorlist;
                        //            }
                        //            }
                        //            else
                        //            {
                        //                categoryId = dt.Rows[i]["SUBCATID_L" + count].ToString();
                        //                TB_CATALOG_SECTIONS checkcategoryexist = _dbcontext.TB_CATALOG_SECTIONS.Where(x => x.CATEGORY_ID == categoryId && x.CATALOG_ID == catalogId && x.FLAG_RECYCLE == "A").FirstOrDefault();
                        //                if (checkcategoryexist == null)
                        //                {
                        //                    errorlist.Add(categoryId + "category doesnot belong to catalog.Cannot add category here");
                        //                    return errorlist;
                        //                }
                        //            }
                        //        }

                        //        else
                        //        {
                        //            if (dt.Rows[i]["CATEGORY_ID"] is DBNull)
                        //            {


                        //                string categoryName = dt.Rows[i]["CATEGORY_NAME"].ToString();


                        //                categoryId = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tcs => tcs.CATEGORY_ID, tc => tc.CATEGORY_ID, (tcs, tc) => new { tcs, tc }).Where(x => x.tcs.CATEGORY_NAME == categoryName && x.tc.CATEGORY_ID == x.tcs.CATEGORY_ID && x.tc.CATALOG_ID == catalogId && x.tcs.FLAG_RECYCLE == "A").Select(y => y.tc.CATEGORY_ID).FirstOrDefault();
                        //                // string categorycount = _dbcontext.TB_PROJECT_SECTION_DETAILS.Where(x => x.CATEGORY_ID == categoryId).Select(y => y.CATEGORY_ID).FirstOrDefault();
                        //                if (categoryId != null)
                        //                {
                        //                    dt.Rows[i]["CATEGORY_ID"] = categoryId;
                        //                }
                        //                else
                        //                {
                        //                    errorlist.Add(categoryName + "category doesnot belong to catalog.Cannot add category here");
                        //                    return errorlist;
                        //                }
                        //            }

                        //            else
                        //            {
                        //                categoryId = dt.Rows[i]["CATEGORY_ID"].ToString();
                        //                TB_CATALOG_SECTIONS checkcategoryexist = _dbcontext.TB_CATALOG_SECTIONS.Where(x => x.CATEGORY_ID == categoryId && x.CATALOG_ID == catalogId && x.FLAG_RECYCLE == "A").FirstOrDefault();
                        //                if (checkcategoryexist == null)
                        //                {
                        //                    errorlist.Add(categoryId + "category doesnot belong to catalog.Cannot add category here");
                        //                    return errorlist;
                        //                }
                        //            }
                        //        }
                        //    }
                        //    count = count - 1;
                        //    }
                        //}

                        //--------------------------------End---------------

                        //---------------- check for Familyid is exist in datatable if not fetch from db and add to datatable-------
                        DATA_FILE_NAME = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                        // filecount = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.XML_FILE_NAME == DATA_FILE_NAME).Count();
                        // if (filecount > 0)
                        // {
                        if (dt.Columns.Contains("SUBCATID_L" + count) || dt.Columns.Contains("SUBCATNAME_L" + count))
                        {
                            int subCatFamilyCheck = 1; string cat_Id = string.Empty;
                            while (subCatFamilyCheck <= count)
                            {
                                if (dt.Rows[i]["SUBCATID_L" + count] is DBNull)
                                {
                                    if (!(dt.Rows[i]["SUBCATNAME_L" + count] is DBNull))
                                    {
                                        //if (count == subCatFamilyCheck)
                                        //{
                                        //    cat_Id = dt.Rows[i]["CATEGORY_ID"].ToString();
                                        //} 
                                        //else 
                                        //{
                                        //    count = count - 1;
                                        //    cat_Name = dt.Rows[i]["SUBCATNAME_L" + count].ToString();
                                        //    cat_Id = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tc => tc.CATEGORY_ID, tcs => tcs.CATEGORY_ID, (tc, tcs) => new { tc, tcs }).Where(x => x.tc.CATEGORY_NAME == cat_Name && x.tcs.CATALOG_ID == catalogId).Select(y => y.tc.CATEGORY_ID).FirstOrDefault();
                                        //}
                                        categoryName = dt.Rows[i]["SUBCATNAME_L" + count].ToString();
                                        categoryShort = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tc => tc.CATEGORY_ID, tcs => tcs.CATEGORY_ID, (tc, tcs) => new { tc, tcs }).Where(x => x.tc.CATEGORY_NAME == categoryName && x.tcs.CATALOG_ID == catalogId).Select(y => y.tc.CATEGORY_ID).FirstOrDefault();
                                        if (categoryShort != null)
                                        {
                                            dt.Rows[i]["SUBCATID_L" + count] = categoryShort;
                                        }
                                        if (dt.Rows[i]["FAMILY_ID"] is DBNull)
                                        {
                                            string familyName = dt.Rows[i]["FAMILY_NAME"].ToString();
                                            familyId = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tf => tf.FAMILY_ID, tcf => tcf.FAMILY_ID, (tf, tcf) => new { tf, tcf }).Where(x => x.tcf.FAMILY_NAME == familyName && x.tf.CATALOG_ID == catalogId && x.tf.CATEGORY_ID == categoryShort && x.tcf.FLAG_RECYCLE == "A").Select(y => y.tcf.FAMILY_ID).FirstOrDefault();
                                            if (familyId != null && familyId != 0)
                                            {
                                                dt.Rows[i]["FAMILY_ID"] = familyId;
                                                flag = true;
                                                break;
                                            }
                                            else
                                            {
                                                errorlist.Add("New-" + familyName + "family doesnot belong to catalog.Cannot add family here");
                                                //return errorlist;
                                            }
                                        }
                                        else
                                        {
                                            familyId = Convert.ToInt32(dt.Rows[i]["FAMILY_ID"]);
                                            TB_CATALOG_FAMILY checkfamilyexist = _dbcontext.TB_CATALOG_FAMILY.Where(x => x.CATEGORY_ID == categoryShort && x.CATALOG_ID == catalogId && x.FAMILY_ID == familyId && x.FLAG_RECYCLE == "A").FirstOrDefault();
                                            if (checkfamilyexist == null)
                                            {
                                                errorlist.Add("New-" + familyId + "family doesnot belong to catalog.Cannot add family here");
                                                //return errorlist;
                                            }
                                            else
                                            {
                                                flag = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                                count = count - 1;
                            }
                        }
                        //else
                        // {
                        if (!flag)
                        {
                            if (dt.Rows[i]["CATEGORY_ID"] is DBNull)
                            {
                                categoryName = dt.Rows[i]["CATEGORY_NAME"].ToString();
                                catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                                categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_NAME == categoryName).Select(y => y.CATEGORY_ID).FirstOrDefault();
                            }
                            else
                            {
                                categoryId = dt.Rows[i]["CATEGORY_ID"].ToString();
                                categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == categoryId).Select(y => y.CATEGORY_ID).FirstOrDefault();
                                if (categoryShort == null)
                                {
                                    errorlist.Add("New-" + categoryShort + "category doesnot belong to catalog.Cannot add category here");
                                }
                            }
                            if (dt.Columns.Contains("SUBCATID_L" + count) || dt.Columns.Contains("SUBCATNAME_L" + count))
                            {
                                int subCatFamilyCheck = 1; string cat_Id = string.Empty;
                                while (subCatFamilyCheck <= count)
                                {
                                    if (dt.Rows[i]["SUBCATID_L" + count] is DBNull)
                                    {
                                        cat_Id = dt.Rows[i]["CATEGORY_ID"].ToString();
                                        categoryName = dt.Rows[i]["SUBCATNAME_L" + count].ToString();
                                        categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_NAME == categoryName && x.PARENT_CATEGORY == cat_Id).Select(y => y.CATEGORY_ID).FirstOrDefault();
                                        if (categoryShort == null)
                                        {
                                            dt.Rows[i]["SUBCATNAME_L" + count] = categoryShort;
                                        }
                                        if (dt.Rows[i]["FAMILY_ID"] is DBNull)
                                        {
                                            string familyName = dt.Rows[i]["FAMILY_NAME"].ToString();
                                            familyId = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tf => tf.FAMILY_ID, tcf => tcf.FAMILY_ID, (tf, tcf) => new { tf, tcf }).Where(x => x.tcf.FAMILY_NAME == familyName && x.tf.CATALOG_ID == catalogId && x.tf.CATEGORY_ID == categoryShort && x.tcf.FLAG_RECYCLE == "A").Select(y => y.tcf.FAMILY_ID).FirstOrDefault();
                                            if (familyId != null && familyId != 0)
                                            {
                                                dt.Rows[i]["FAMILY_ID"] = familyId;
                                                flag = true;
                                                break;
                                            }
                                            else
                                            {
                                                errorlist.Add("New-" + familyName + "family doesnot belong to catalog.Cannot add family here");
                                                //return errorlist;
                                            }
                                        }
                                        else
                                        {
                                            familyId = Convert.ToInt32(dt.Rows[i]["FAMILY_ID"]);
                                            TB_CATALOG_FAMILY checkfamilyexist = _dbcontext.TB_CATALOG_FAMILY.Where(x => x.CATEGORY_ID == categoryShort && x.CATALOG_ID == catalogId && x.FAMILY_ID == familyId && x.FLAG_RECYCLE == "A").FirstOrDefault();
                                            if (checkfamilyexist == null)
                                            {
                                                errorlist.Add("New-" + familyId + "family doesnot belong to catalog.Cannot add family here");
                                                //return errorlist;
                                            }
                                            else
                                            {
                                                flag = true;
                                                break;
                                            }
                                        }

                                    }
                                    count = count - 1;
                                }
                            }
                            //if (dt.Rows[i]["FAMILY_ID"] is DBNull)
                            //{
                            //    string familyName = dt.Rows[i]["FAMILY_NAME"].ToString();
                            //    familyId = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tf => tf.FAMILY_ID, tcf => tcf.FAMILY_ID, (tf, tcf) => new { tf, tcf }).Where(x => x.tcf.FAMILY_NAME == familyName && x.tf.CATALOG_ID == catalogId && x.tf.CATEGORY_ID == categoryShort && x.tcf.FLAG_RECYCLE == "A").Select(y => y.tcf.FAMILY_ID).FirstOrDefault();
                            //    if (familyId != null && familyId != 0)
                            //    {
                            //        dt.Rows[i]["FAMILY_ID"] = familyId;
                            //    }
                            //    else
                            //    {
                            //        errorlist.Add("New-" + familyName + "family doesnot belong to catalog.Cannot add family here");
                            //        // return errorlist;
                            //    }
                            //}
                            //else
                            //{
                            //    familyId = Convert.ToInt32(dt.Rows[i]["FAMILY_ID"]);
                            //    categoryId = dt.Rows[i]["CATEGORY_ID"].ToString();
                            //    categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == categoryId).Select(y => y.CATEGORY_ID).FirstOrDefault();
                            //    TB_CATALOG_FAMILY checkfamilyexist = _dbcontext.TB_CATALOG_FAMILY.Where(x => x.CATEGORY_ID == categoryShort && x.CATALOG_ID == catalogId && x.FAMILY_ID == familyId && x.FLAG_RECYCLE == "A").FirstOrDefault();
                            //    if (checkfamilyexist == null)
                            //    {
                            //        errorlist.Add("New-" + familyId + "family doesnot belong to catalog.Cannot add family here");
                            //        //return errorlist;
                            //    }
                            //}
                        }
                        // }
                        // }

                        //--------------------------------End---------------



                        // ---------------- check for Projectid is exist in datatable if not fetch from db and add to datatable-------
                        string action = dt.Rows[i]["ACTION"].ToString().ToLower();
                        if (action == "delete")
                        {
                            if (dt.Columns.Contains("PROJECT_ID") || dt.Columns.Contains("PROJECT_NAME"))
                            {
                                if (dt.Rows[i]["PROJECT_ID"] is DBNull)
                                {
                                    projectName = dt.Rows[i]["PROJECT_NAME"].ToString();
                                    projectId = _dbcontext.TB_PROJECT.Where(x => x.PROJECT_NAME == projectName && x.CATALOG_ID == catalogId).Select(y => y.PROJECT_ID).FirstOrDefault();
                                    if (projectId != null)
                                    {
                                        dt.Rows[i]["PROJECT_ID"] = projectId;
                                    }
                                    else
                                    {
                                        errorlist.Add("New-" + projectName + "project doesnot belong to catalog");
                                        // return errorlist;
                                    }
                                }
                                else
                                {
                                    projectId = Convert.ToInt32(dt.Rows[i]["PROJECT_ID"]);
                                    TB_PROJECT checkProjectexist = _dbcontext.TB_PROJECT.Where(x => x.PROJECT_ID == projectId).FirstOrDefault();
                                    if (checkProjectexist == null)
                                    {
                                        errorlist.Add("New-" + projectId + "project doesnot belong to catalog");
                                        //return errorlist;
                                    }
                                }
                                if (dt.Columns.Contains("DATA_FILE_ID") || dt.Columns.Contains("DATA_FILE_NAME"))
                                {
                                    if (dt.Rows[i]["DATA_FILE_ID"] is DBNull)
                                    {
                                        string xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                                        xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                                        var xml = xmlFileName.Split('.');
                                        string extenstion = xml[0];
                                        if (extenstion == "xml")
                                        {
                                            xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                                        }
                                        else
                                        {
                                            xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString() + ".xml";
                                        }
                                        recordId = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.XML_FILE_NAME == xmlFileName && x.PROJECT_ID == projectId).Select(y => y.RECORD_ID).FirstOrDefault();
                                        if (recordId != null)
                                        {
                                            dt.Rows[i]["DATA_FILE_ID"] = recordId;
                                        }
                                        else
                                        {
                                            errorlist.Add("New-" + xmlFileName + "file doesnot belong to project");
                                            // return errorlist;
                                        }
                                    }
                                    else
                                    {
                                        recordId = Convert.ToInt32(dt.Rows[i]["DATA_FILE_ID"]);
                                        TB_PROJECT_SECTIONS checkRecordtexist = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.RECORD_ID == recordId).FirstOrDefault();
                                        if (checkRecordtexist == null)
                                        {
                                            errorlist.Add("New-" + recordId + "file doesnot belong to project");
                                            //return errorlist;
                                        }
                                    }
                                    if (dt.Columns.Contains("FAMILY_ID") || dt.Columns.Contains("FAMILY_NAME"))
                                    {
                                        // if(dt.Rows[i]["FAMILY_ID"] is DBNull)
                                        //{
                                        familyId = Convert.ToInt32(dt.Rows[i]["FAMILY_ID"]);
                                        TB_PROJECT_SECTION_DETAILS checkFamilyExist = _dbcontext.TB_PROJECT_SECTION_DETAILS.Where(x => x.FAMILY_ID == familyId && x.RECORD_ID == recordId).FirstOrDefault();
                                        if (checkFamilyExist == null)
                                        {
                                            errorlist.Add("New-" + familyId + "family doesnot belong to project");
                                            //return errorlist;
                                        }
                                        // }
                                        //   else
                                        // {

                                        // }
                                    }
                                }
                            }
                        }
                        dt.AcceptChanges();
                    }
                }
                return errorlist;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at CatalogController :ValidateIndesignNewItems", objException);
                return errorlist;
            }
        }
        #endregion

        #region Duplicate Project Id/name in sheet
        public List<string> ValidateDuplicateItem(DataTable dt)
        {
            List<string> errorlist = new List<string>();
            DataTable resultTable = new DataTable();
            try
            {

                //if ((dt.Columns.Contains("FAMILY_ID") || dt.Columns.Contains("FAMILY_NAME")) && (dt.Columns.Contains("DATA_FILE_ID") || dt.Columns.Contains("DATA_FILE_NAME")) && (dt.Columns.Contains("PROJECT_ID") || dt.Columns.Contains("PROJECT_NAME")))
                //{
                //    int totalRows = dt.Rows.Count;
                //    for (int i = 0; i < totalRows; i++)
                //    {
                //        int catalogId = 0; int familyId = 0; string categoryId = ""; int projectId = 0; int recordId = 0;int sortorder=0;
                //        if (dt.Rows[i]["CATALOG_ID"] is DBNull)
                //        {
                //            string catalogName = dt.Rows[i]["CATALOG_NAME"].ToString();
                //            catalogId = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_NAME == catalogName && x.FLAG_RECYCLE == "A").Select(y => y.CATALOG_ID).FirstOrDefault();
                //            if (catalogId != null)
                //            {
                //                dt.Rows[i]["CATALOG_ID"] = catalogId;
                //            }
                //        }
                //        if (dt.Rows[i]["CATEGORY_ID"] is DBNull)
                //        {
                //            string categoryName = dt.Rows[i]["CATEGORY_NAME"].ToString();
                //            categoryId = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tcs => tcs.CATEGORY_ID, tc => tc.CATEGORY_ID, (tcs, tc) => new { tcs, tc }).Where(x => x.tcs.CATEGORY_NAME == categoryName && x.tc.CATEGORY_ID == x.tcs.CATEGORY_ID && x.tc.CATALOG_ID == catalogId && x.tcs.FLAG_RECYCLE == "A").Select(y => y.tc.CATEGORY_ID).FirstOrDefault();
                //            if (categoryId != null)
                //            {
                //                dt.Rows[i]["CATEGORY_ID"] = categoryId;
                //            }
                //        }
                //        if (dt.Rows[i]["PROJECT_ID"] is DBNull)
                //        {
                //            string projectName = dt.Rows[i]["PROJECT_NAME"].ToString();
                //            projectId = _dbcontext.TB_PROJECT.Where(x => x.PROJECT_NAME == projectName && x.CATALOG_ID == catalogId).Select(y => y.PROJECT_ID).FirstOrDefault();
                //            if (projectId != null)
                //            {
                //                dt.Rows[i]["PROJECT_ID"] = projectId;
                //            }
                //        }
                //        if (dt.Rows[i]["FAMILY_ID"] is DBNull)
                //        {
                //            string familyName = dt.Rows[i]["FAMILY_NAME"].ToString();
                //            familyId = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tf => tf.FAMILY_ID, tcf => tcf.FAMILY_ID, (tf, tcf) => new { tf, tcf }).Where(x => x.tcf.FAMILY_NAME == familyName && x.tf.CATALOG_ID == catalogId && x.tf.CATEGORY_ID == categoryId && x.tcf.FLAG_RECYCLE == "A").Select(y => y.tcf.FAMILY_ID).FirstOrDefault();
                //            if (familyId != null)
                //            {
                //                dt.Rows[i]["FAMILY_ID"] = familyId;
                //            }
                //        }
                //        if (dt.Rows[i]["DATA_FILE_ID"] is DBNull)
                //        {
                //            string xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                //            xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                //            var xml = xmlFileName.Split('.');
                //            string extenstion = xml[0];
                //            if (extenstion == "xml")
                //            {
                //                xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                //            }
                //            else
                //            {
                //                xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString() + ".xml";
                //            }
                //            recordId = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.XML_FILE_NAME == xmlFileName && x.PROJECT_ID == projectId).Select(y => y.RECORD_ID).FirstOrDefault();
                //            if (recordId != null)
                //            {
                //                dt.Rows[i]["DATA_FILE_ID"] = recordId;
                //            }
                //        }
                //    }
                //}
                DataView dv = dt.DefaultView;
                DataTable DT1 = new DataTable();
                int newcount = 0;
                int oldcount = 0;
                oldcount = dt.Rows.Count;
                //List the column names whose values are to be distinct

                string[] Columns = { "DATA_FILE_ID", "FAMILY_ID", "PROJECT_ID", "CATALOG_ID", "CATEGORY_ID", "SORT_ORDER" };

                DT1 = dv.ToTable(true, Columns);
                newcount = DT1.Rows.Count;
                if (oldcount != newcount)
                {
                    errorlist.Add("Dup-Duplicate records found in sheet");
                }
                //var result = from c in dt.AsEnumerable()
                //             group c by new
                //             {
                //                 //column names for checking duplicate values.
                //                 recordId = c["DATA_FILE_ID"],
                //                 familyId = c["FAMILY_ID"],
                //                 projectId = c["PROJECT_ID"],
                //                 cataglogId = c["CATALOG_ID"],
                //                 categoryId = c["CATEGORY_ID"],
                //                 sortorder=c["SORT_ORDER"]


                //             } into g
                //             where g.Count() > 1
                //             select new
                //             {
                //                 g.Key.familyId,
                //                 g.Key.recordId,
                //                 g.Key.projectId,
                //                 g.Key.cataglogId,
                //                 g.Key.categoryId,
                //                 g.Key.sortorder

                //             };
                //if (result.ToList().Count > 0)
                //{
                //    errorlist.Add("Duplicate records found");
                //}
                //for (int j = 0; j < result.ToList().Count; j++)
                //{
                //    var item = result.ToList()[j];
                //    errorlist.Add(item);
                //}
                //}//Select(s => new { s.CATEGORY_ID, s.FAMILY_ID,s.RECORD_ID,s.SORT_ORDER })
                //var duplicates = dt.AsEnumerable()
                //   .Select(dr => dr["FAMILY_NAME"])
                //   .GroupBy(x => x)
                //   .Where(g => g.Count() > 1)
                //   .Select(g => g.Key)
                //   .ToList();

                //var values = dt.Rows.Cast<DataRow>().Select(r => r["SORT_ORDER"]).Distinct().ToList();
                //var unique = values.Count == dt.Rows.Count;
                //if (!unique)
                //{
                //    errorlist.Add("Duplicate records found in sort order");
                //}
                var grouped = dt.AsEnumerable().GroupBy(d => new
                {
                    DATA_FILE_ID = d.Field<int>("DATA_FILE_ID"),
                    SORT_ORDER = d.Field<int>("SORT_ORDER")
                })
              .Select(x => new
              {
                  SORT_ORDER = x.Key.SORT_ORDER
              });

                return errorlist;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at CatalogController :ValidateDuplicateItem", objException);
                return errorlist;
            }
        }
        #endregion

        #region importIndesgin process
        [HttpPost]
        public string ImportIndesgin()
        {
            try
            {
                DataTable dt = new DataTable();
                string fileName, actualFileName;
                string FilePath = string.Empty;
                Message = fileName = actualFileName = string.Empty;
                string physicalPath = string.Empty;
                string strConn;
                string SheetName = string.Empty;

                if (Request.Files != null)
                {

                    var file = Request.Files[0];
                    actualFileName = file.FileName;
                    fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    int size = file.ContentLength;
                    physicalPath = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName);
                    file.SaveAs(physicalPath);
                    AdvanceImportApiController adImport = new AdvanceImportApiController();
                    dt = GetDataFromExcel(actualFileName, physicalPath, " SELECT * from [" + actualFileName + "$]", "");
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    if (physicalPath.Substring(physicalPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + physicalPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    else
                        //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + physicalPath +
                        //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + physicalPath +
                                ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,
                        new object[] { null, null, null, "TABLE" });
                    DataRow schemaRow = schemaTable.Rows[0];
                    SheetName = schemaRow["TABLE_NAME"].ToString();
                }

                //DataTable importSheeTable = IC.BasicImportExcelSheetSelection(physicalPath, SheetName) as DataTable;
                string importTemp, SQLString = string.Empty;
                importTemp = Guid.NewGuid().ToString();
                _dbcontext.Database.CommandTimeout = 0;
                string action = string.Empty;
                int totalRows = dt.Rows.Count; int index; string indexOfSubCat = string.Empty; string subcategorySplit = string.Empty;
                int subCatCount = 0;
                if (!dt.Columns.Contains("CATALOG_ID"))
                {
                    dt.Columns.Add("CATALOG_ID");
                    dt.Columns["CATALOG_ID"].SetOrdinal(dt.Columns.IndexOf("CATALOG_NAME"));
                }
                if (!dt.Columns.Contains("CATEGORY_ID"))
                {
                    dt.Columns.Add("CATEGORY_ID");
                    dt.Columns["CATEGORY_ID"].SetOrdinal(dt.Columns.IndexOf("CATEGORY_NAME"));
                }
                if (!dt.Columns.Contains("FAMILY_ID"))
                {
                    dt.Columns.Add("FAMILY_ID");
                    dt.Columns["FAMILY_ID"].SetOrdinal(dt.Columns.IndexOf("FAMILY_NAME"));
                }
                if (dt.Rows.Count > 0)
                {
                    if (dt.Columns.Contains("FAMILY_ID"))
                    {
                        index = dt.Columns["FAMILY_ID"].Ordinal;
                        indexOfSubCat = dt.Columns[index - 1].ToString();
                    }
                    else if (dt.Columns.Contains("FAMILY_NAME"))
                    {
                        index = dt.Columns["FAMILY_NAME"].Ordinal;
                        indexOfSubCat = dt.Columns[index - 1].ToString();
                    }
                    if (indexOfSubCat.Contains("SUBCAT"))
                    {
                        subcategorySplit = indexOfSubCat.Split('_')[1];
                        subCatCount = Convert.ToInt32(subcategorySplit[1].ToString());
                    }
                    if (subCatCount > 0)
                    {
                        for (int i = 1; i <= subCatCount; i++)
                        {
                            if (!dt.Columns.Contains("SUBCATID_L" + i))
                            {
                                index = dt.Columns["SUBCATNAME_L" + i].Ordinal;
                                dt.Columns.Add("SUBCATID_L" + i).SetOrdinal(index);
                            }
                        }
                    }
                }
                if (!dt.Columns.Contains("PROJECT_ID"))
                {
                    dt.Columns.Add("PROJECT_ID");
                    dt.Columns["PROJECT_ID"].SetOrdinal(dt.Columns.IndexOf("PROJECT_NAME"));
                }
                if (!dt.Columns.Contains("DATA_FILE_ID"))
                {
                    dt.Columns.Add("DATA_FILE_ID");
                    dt.Columns["DATA_FILE_ID"].SetOrdinal(dt.Columns.IndexOf("DATA_FILE_NAME"));
                }

                for (int i = 0; i < totalRows; i++)
                {
                    // action = dt.Rows[i]["ACTION"].ToString().ToUpper();
                    // if (action == "" || action == "update" || action == "new") 
                    // { 
                    int catalogId = 0; int familyId = 0; string categoryId = ""; int projectId = 0; int recordId = 0;
                    if (dt.Rows[i]["CATALOG_ID"] is DBNull)
                    {
                        string catalogName = dt.Rows[i]["CATALOG_NAME"].ToString();
                        catalogId = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_NAME == catalogName).Select(y => y.CATALOG_ID).FirstOrDefault();
                        if (catalogId != null)
                        {
                            dt.Rows[i]["CATALOG_ID"] = catalogId;
                        }
                    }
                    if (dt.Rows[i]["CATEGORY_ID"] is DBNull)
                    {
                         string categoryName = dt.Rows[i]["CATEGORY_NAME"].ToString();
                        catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                        categoryId = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tcs => tcs.CATEGORY_ID, tc => tc.CATEGORY_ID, (tcs, tc) => new { tcs, tc }).Where(x => x.tcs.CATEGORY_NAME == categoryName && x.tc.CATEGORY_ID == x.tcs.CATEGORY_ID && x.tc.CATALOG_ID == catalogId).Select(y => y.tcs.CATEGORY_SHORT).FirstOrDefault();
                        if (categoryId != null)
                        {
                            dt.Rows[i]["CATEGORY_ID"] = categoryId;
                        }
                    }
                    int count = subCatCount;
                    if (dt.Columns.Contains("SUBCATID_L" + count) || dt.Columns.Contains("SUBCATNAME_L" + count))
                    {
                        int subCatFamilyCheck = 1; string cat_Id = string.Empty;
                        string categoryName = string.Empty; string categoryShort = string.Empty;
                        while (subCatFamilyCheck <= count)
                        {
                            if (dt.Rows[i]["SUBCATID_L" + subCatFamilyCheck] is DBNull)
                            {
                                if (!(dt.Rows[i]["SUBCATNAME_L" + subCatFamilyCheck] is DBNull))
                                {
                                   string categoryName1 = Convert.ToString(dt.Rows[i]["SUBCATNAME_L" + subCatFamilyCheck]);
                                    cat_Id = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tc => tc.CATEGORY_ID, tcs => tcs.CATEGORY_ID, (tc, tcs) => new { tc, tcs }).Where(x => x.tc.CATEGORY_NAME == categoryName1 && x.tcs.CATALOG_ID == catalogId).Select(y => y.tc.CATEGORY_ID).FirstOrDefault();
                                    // cat_Id = dt.Rows[i]["CATEGORY_ID"].ToString();
                                    // categoryName = dt.Rows[i]["SUBCATNAME_L" + count].ToString();

                                    categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_NAME == categoryName1 && x.CATEGORY_ID == cat_Id).Select(y => y.CATEGORY_SHORT).FirstOrDefault();
                                    if (categoryShort != null)
                                    {
                                        dt.Rows[i]["SUBCATID_L" + subCatFamilyCheck] = categoryShort;
                                        categoryId = dt.Rows[i]["SUBCATID_L" + subCatFamilyCheck].ToString();
                                    }
                                }
                            }
                            subCatFamilyCheck = subCatFamilyCheck + 1;
                        }
                    }
                    if (dt.Rows[i]["FAMILY_ID"] is DBNull)
                    {
                        string familyName = dt.Rows[i]["FAMILY_NAME"].ToString();
                        catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                        string categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == categoryId).Select(y => y.CATEGORY_ID).FirstOrDefault();
                        familyId = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tf => tf.FAMILY_ID, tcf => tcf.FAMILY_ID, (tf, tcf) => new { tf, tcf }).Where(x => x.tcf.FAMILY_NAME == familyName && x.tf.CATALOG_ID == catalogId && x.tf.CATEGORY_ID == categoryShort).Select(y => y.tcf.FAMILY_ID).FirstOrDefault();
                        if (familyId != null)
                        {
                            dt.Rows[i]["FAMILY_ID"] = familyId;
                        }
                    }
                    if (dt.Columns.Contains("PROJECT_ID") || dt.Columns.Contains("PROJECT_NAME"))
                    {
                        if (dt.Rows[i]["PROJECT_ID"] is DBNull)
                        {
                            string projectName = dt.Rows[i]["PROJECT_NAME"].ToString();
                            catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                            projectId = _dbcontext.TB_PROJECT.Where(x => x.PROJECT_NAME == projectName && x.CATALOG_ID == catalogId).Select(y => y.PROJECT_ID).FirstOrDefault();
                            if (projectId != null)
                            {
                                dt.Rows[i]["PROJECT_ID"] = projectId;
                            }
                            else
                            {
                                dt.Rows[i]["PROJECT_ID"] = 0;
                            }

                        }

                        if (dt.Columns.Contains("DATA_FILE_ID") || dt.Columns.Contains("DATA_FILE_NAME"))
                        {
                            if (dt.Rows[i]["DATA_FILE_ID"] is DBNull)
                            {
                                string xmlFileName = string.Empty;
                                xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                                var xml = xmlFileName.Split('.');
                                string extenstion = string.Empty;
                                bool file_Name = false;
                                if (xml != null && xml.Count() >= 2)
                                {
                                    extenstion = xml[1];
                                }
                                if (extenstion == "xml")
                                {
                                    xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                                    file_Name = true;
                                }
                                else
                                {
                                    xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString(); // + ".xml"
                                }
                                projectId = Convert.ToInt32(dt.Rows[i]["PROJECT_ID"]);
                                recordId = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.XML_FILE_NAME == xmlFileName && x.PROJECT_ID == projectId).Select(y => y.RECORD_ID).FirstOrDefault();
                                //if (!file_Name)
                                //{
                                //    dt.Rows[i]["DATA_FILE_NAME"] = dt.Rows[i]["DATA_FILE_NAME"].ToString() + ".xml";
                                //}
                                if (recordId != null)
                                {
                                    dt.Rows[i]["DATA_FILE_ID"] = recordId;
                                }
                                else
                                {
                                    dt.Rows[i]["DATA_FILE_ID"] = 0;
                                }
                            }
                        }
                    }

                }
                DataSet resultSet1 = new DataSet();
                using (
                      var objSqlConnection =
                          new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SQLString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    SqlCommand _DBCommandnew = new SqlCommand(SQLString, objSqlConnection);
                    _DBCommandnew.ExecuteNonQuery();
                    SQLString = IC.CreateTable("[##IMPORTTEMP" + importTemp + "]", physicalPath, SheetName, dt);
                    _DBCommandnew = new SqlCommand(SQLString, objSqlConnection);
                    _DBCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(dt);
                    // }

                    //using (
                    //      var objSqlConnection =
                    //          new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    // {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESGINIMPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.AddWithValue("@SID", importTemp);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    // objSqlConnection.Open();
                    sqlDataAdapter.Fill(resultSet1);
                    objSqlConnection.Close();
                    if (resultSet1.Tables[0].Rows.Count > 0 && resultSet1.Tables[0].Rows[0].ToString()!="ALREADY RECORD IS HERE")
                    {
                        return "Import Failed";
                    }
                    else
                    {
                        return "Import success";
                    }
                    //objSqlConnection.Open();
                    //objSqlCommand.ExecuteNonQuery();
                    //objSqlConnection.Close();

                }

                //for (int i = 0; i < totalRows; i++)
                //{
                //    int projectId = 0; int fileno = 0; int recordId = 0;
                //    int sortOrder = 0; string Category_id = string.Empty; int familyId = 0;
                //    action = dt.Rows[i]["ACTION"].ToString().ToLower();
                //    if (action == "" || action == "update" || action == "new")
                //    {

                //        int catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                //        string projectName = dt.Rows[i]["PROJECT_NAME"].ToString();

                //        if (Convert.ToInt32(dt.Rows[i]["PROJECT_ID"]) == 0)
                //        {
                //            TB_PROJECT projectTable = new TB_PROJECT();
                //            projectTable.CATALOG_ID = catalogId;
                //            projectTable.PROJECT_TYPE = 1;
                //            projectTable.PROJECT_NAME = projectName;
                //            projectTable.CREATED_USER = User.Identity.Name;
                //            projectTable.MODIFIED_USER = User.Identity.Name;
                //            projectTable.CREATED_DATE = DateTime.Now;
                //            projectTable.MODIFIED_DATE = DateTime.Now;
                //            _dbcontext.TB_PROJECT.Add(projectTable);
                //            _dbcontext.SaveChanges();
                //            projectId = Convert.ToInt32(_dbcontext.TB_PROJECT.Where(x => x.PROJECT_NAME == projectName).Select(y => y.PROJECT_ID));
                //            dt.Rows[i]["PROJECT_ID"] = projectId;
                //        }
                //        if (Convert.ToInt32(dt.Rows[i]["DATA_FILE_ID"]) == 0)
                //        {

                //            projectId = Convert.ToInt32(dt.Rows[i]["PROJECT_ID"]);
                //            // dt.Rows[i]["PROJECT_ID"] = projectId;
                //            var filenoexisit = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.PROJECT_ID == projectId);
                //            if (filenoexisit.Any())
                //            {
                //                fileno = filenoexisit.Max(y => y.FILE_NO);
                //                fileno = fileno + 1;
                //            }
                //            else
                //            {
                //                fileno = 1;
                //            }

                //            string XMLfilename = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                //            var xml = XMLfilename.Split('.');
                //            string extenstion = xml[1];
                //            if (extenstion == "xml")
                //            {
                //                XMLfilename = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                //            }
                //            else
                //            {
                //                XMLfilename = dt.Rows[i]["DATA_FILE_NAME"].ToString() + ".xml";
                //            }
                //            TB_PROJECT_SECTIONS projectSectionTable = new TB_PROJECT_SECTIONS();
                //            projectSectionTable.PROJECT_ID = projectId;
                //            projectSectionTable.XML_FILE_NAME = XMLfilename;
                //            projectSectionTable.FILE_NO = fileno;
                //            projectSectionTable.CREATED_USER = User.Identity.Name;
                //            projectSectionTable.MODIFIED_USER = User.Identity.Name;
                //            projectSectionTable.CREATED_DATE = DateTime.Now;
                //            projectSectionTable.MODIFIED_DATE = DateTime.Now;
                //            _dbcontext.TB_PROJECT_SECTIONS.Add(projectSectionTable);
                //            _dbcontext.SaveChanges();
                //        }
                //        else
                //        {

                //            recordId = Convert.ToInt32(dt.Rows[i]["DATA_FILE_ID"]);
                //            string XMLfilename = dt.Rows[i]["DATA_FILE_NAME"].ToString() ;
                //            var xml = XMLfilename.Split('.');
                //            string extenstion = xml[0];
                //            if (extenstion == "xml")
                //            {
                //                XMLfilename = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                //            }
                //            else
                //            {
                //                XMLfilename = dt.Rows[i]["DATA_FILE_NAME"].ToString() + ".xml";
                //            }
                //            TB_PROJECT_SECTIONS projectSectionTable = new TB_PROJECT_SECTIONS();
                //            projectSectionTable = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.RECORD_ID == recordId).FirstOrDefault();
                //            projectSectionTable.XML_FILE_NAME = XMLfilename;
                //           // _dbcontext.TB_PROJECT_SECTIONS.Add(projectSectionTable);
                //            _dbcontext.SaveChanges();
                //        }
                //           if(dt.Columns.Contains("FAMILY_ID") || dt.Columns.Contains("FAMILY_NAME"))
                //           {

                //               Category_id = dt.Rows[i]["CATEGORY_ID"].ToString();
                //               familyId = Convert.ToInt32(dt.Rows[i]["FAMILY_ID"]);
                //               sortOrder = Convert.ToInt32( dt.Rows[i]["SORT_ORDER"]);
                //               var sortExist = _dbcontext.TB_PROJECT_SECTION_DETAILS.Where(x => x.RECORD_ID == recordId && x.FAMILY_ID == familyId && x.CATEGORY_ID == Category_id);
                //               if (sortExist.Any())
                //               {
                //                   SortOrderUpdate(recordId, familyId, Category_id, sortOrder);
                //               }
                //               else
                //               {
                //                   recordId = Convert.ToInt32(dt.Rows[i]["DATA_FILE_ID"]);
                //                   var sort = _dbcontext.TB_PROJECT_SECTION_DETAILS.Where(x => x.RECORD_ID == recordId);
                //                  if(!sort.Any())
                //                  { 
                //                   sortOrder = 1;
                //                  }
                //                  else
                //                  {
                //                      sortOrder = Convert.ToInt32(sort.Max(y => y.SORT_ORDER));
                //                      sortOrder = sortOrder + 1;
                //                  }
                //                   TB_PROJECT_SECTION_DETAILS projectSectionDetailsTable = new TB_PROJECT_SECTION_DETAILS();
                //                   projectSectionDetailsTable.SORT_ORDER = sortOrder;
                //                   projectSectionDetailsTable.RECORD_ID = recordId;
                //                   projectSectionDetailsTable.CATEGORY_ID = Category_id;
                //                   projectSectionDetailsTable.FAMILY_ID = familyId;
                //                   projectSectionDetailsTable.CREATED_USER = User.Identity.Name;
                //                   projectSectionDetailsTable.MODIFIED_USER = User.Identity.Name;
                //                   projectSectionDetailsTable.CREATED_DATE = DateTime.Now;
                //                   projectSectionDetailsTable.MODIFIED_DATE = DateTime.Now;
                //                   _dbcontext.TB_PROJECT_SECTION_DETAILS.Add(projectSectionDetailsTable);
                //                   _dbcontext.SaveChanges();                              
                //               }                                
                //           }   
                //    }
                //    else if(action == "delete")
                //    {
                //        DataTable deletetable = new DataTable();
                //        deletetable = dt;
                //        recordId = Convert.ToInt32(dt.Rows[i]["DATA_FILE_ID"]);
                //        familyId = Convert.ToInt32(dt.Rows[i]["FAMILY_ID"]);
                //        Category_id = dt.Rows[i]["CATEGORY_ID"].ToString();
                //        string XMLfilename = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                //        var xml = XMLfilename.Split('.');
                //        string extenstion = xml[0];
                //        if (extenstion == "xml")
                //        {
                //            XMLfilename = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                //        }
                //        else
                //        {
                //            XMLfilename = dt.Rows[i]["DATA_FILE_NAME"].ToString() + ".xml";
                //        }
                //         projectId = Convert.ToInt32(dt.Rows[i]["PROJECT_ID"]);
                //         var checkRecord = _dbcontext.TB_PROJECT_SECTION_DETAILS.Where(x => x.RECORD_ID == recordId && x.FAMILY_ID == familyId).Count();
                //         var checkFileName = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.RECORD_ID == recordId && x.XML_FILE_NAME == XMLfilename).Count();
                //         var checkProject = _dbcontext.TB_PROJECT.Where(x => x.PROJECT_ID == projectId).Count();
                //        if(checkRecord >0)
                //        { 
                //            string qry_String = "Delete from TB_PROJECT_SECTION_DETAILS where RECORD_ID=" + recordId + " and FAMILY_ID=" + familyId;
                //            SqlConnection con = new SqlConnection(connectionString);
                //            SqlCommand cmdObj = new SqlCommand(qry_String, con);
                //            con.Open();
                //            cmdObj.ExecuteNonQuery();
                //            con.Close();

                //        }
                //        else if(checkFileName > 0)
                //        { 
                //            string qry_String = "Delete from TB_PROJECT_SECTIONS where RECORD_ID=" + recordId + " and XML_FILE_NAME=" + XMLfilename;
                //            SqlConnection con = new SqlConnection(connectionString);
                //            SqlCommand cmdObj = new SqlCommand(qry_String, con);
                //            con.Open();
                //            cmdObj.ExecuteNonQuery();
                //            con.Close();
                //        }
                //        else if(checkProject > 0)
                //        { 
                //            string qry_String = "Delete from TB_PROJECT where PROJECT_ID=" + projectId ;
                //            SqlConnection con = new SqlConnection(connectionString);
                //            SqlCommand cmdObj = new SqlCommand(qry_String, con);
                //            con.Open();
                //            cmdObj.ExecuteNonQuery();
                //            con.Close();
                //       }
                //    }
                //}
                //return "success";
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);
                        // raise a new exception nesting
                        // the current instance as InnerException
                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
                //Logger.Error("Error at CatalogController :ImportIndesgin", objException);
                return "Import Failed";
            }
        }
        #endregion
        #region sortOrder update

        public void SortOrderUpdate(int recordId, int family_Id, string category_Id, int sortOrder)
        {
            try
            {
                // int oldsort=0;
                //  int newsort=0;
                var selectedrecordId = recordId;
                var selectedfamilyId = family_Id;
                var selectedcategoryId = category_Id;
                var newsort = sortOrder;

                //CATEGORY_ID = (string)x["CATEGORY_ID"],
                //FAMILY_ID = (int)x["FAMILY_ID"],
                //RECORD_ID = (int)x["RECORD_ID"],

                var functionAlloweditems = _dbcontext.TB_PROJECT_SECTION_DETAILS.Where(x => x.RECORD_ID == recordId).Select(s => new { s.CATEGORY_ID, s.FAMILY_ID, s.RECORD_ID, s.SORT_ORDER }).ToList();
                // int newsort = selecteditem[0].SORT_ORDER;
                var projectSectionDetails = _dbcontext.TB_PROJECT_SECTION_DETAILS.FirstOrDefault(x => x.RECORD_ID == selectedrecordId && x.FAMILY_ID == selectedfamilyId &&
                                                                                                      x.CATEGORY_ID == selectedcategoryId);
                if (projectSectionDetails != null)
                {
                    if (projectSectionDetails.SORT_ORDER != null)
                    {
                        var oldsort = (int)projectSectionDetails.SORT_ORDER;

                        var objProjectSections = new List<TB_PROJECT_SECTION_DETAILS>();
                        if (oldsort < newsort)
                        {
                            for (int i = oldsort - 1; i < newsort; i++)
                            {
                                var categoryId = functionAlloweditems[i].CATEGORY_ID;
                                var familyId = functionAlloweditems[i].FAMILY_ID;
                                // var sortOrder = functionAlloweditems[i].SORT_ORDER;
                                var projectsections =
                                    _dbcontext.TB_PROJECT_SECTION_DETAILS.FirstOrDefault(
                                        x =>
                                            x.CATEGORY_ID == categoryId &&
                                            x.FAMILY_ID == familyId &&
                                            x.RECORD_ID == recordId);
                                if (i == oldsort - 1)
                                {
                                    if (projectsections != null) projectsections.SORT_ORDER = newsort;
                                    _dbcontext.SaveChanges();
                                }
                                else
                                {
                                    if (projectsections != null)
                                        projectsections.SORT_ORDER = functionAlloweditems[i].SORT_ORDER - 1;
                                    _dbcontext.SaveChanges();
                                }
                            }
                        }
                        if (oldsort > newsort)
                        {

                            for (int i = oldsort - 1; i >= newsort - 1; i--)
                            {
                                var categoryId = functionAlloweditems[i].CATEGORY_ID;
                                var familyId = functionAlloweditems[i].FAMILY_ID;
                                var recordIds = functionAlloweditems[1].RECORD_ID;

                                //var categoryId = _dbcontext.TB_PROJECT_SECTION_DETAILS.Where(x => x.RECORD_ID == recordIds && x.SORT_ORDER == oldsort).Select(x => x.CATEGORY_ID).FirstOrDefault();
                                //var newfam_id = _dbcontext.TB_PROJECT_SECTION_DETAILS.Where(x => x.RECORD_ID == recordIds && x.SORT_ORDER == oldsort).Select(x => x.FAMILY_ID).FirstOrDefault();
                                //var familyId = Convert.ToInt32(newfam_id);
                                //var sortOrder = functionAlloweditems[i].SORT_ORDER;
                                //var projectsections =
                                //    _dbcontext.TB_PROJECT_SECTION_DETAILS.FirstOrDefault(x => x.CATEGORY_ID == categoryId && x.FAMILY_ID == familyId);
                                var objSections = new TB_PROJECT_SECTION_DETAILS(); ;
                                objSections.CATEGORY_ID = categoryId;
                                objSections.FAMILY_ID = familyId;
                                objSections.RECORD_ID = recordIds;
                                if (i == oldsort - 1)
                                {
                                    // if (projectsections != null) projectsections.SORT_ORDER = newsort;
                                    // _dbcontext.SaveChanges();
                                    objSections.SORT_ORDER = newsort;
                                }
                                else
                                {
                                    //if (projectsections != null)
                                    //    projectsections.SORT_ORDER = functionAlloweditems[i].SORT_ORDER + 1;
                                    // _dbcontext.SaveChanges();
                                    objSections.SORT_ORDER = functionAlloweditems[i].SORT_ORDER + 1;
                                }
                                objProjectSections.Add(objSections);
                            }
                        }
                        //if (checkfileno)
                        //  return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);

                        foreach (var tbProjectSectionDetails in objProjectSections)
                        {
                            int recordIda = tbProjectSectionDetails.RECORD_ID;
                            int familyId = tbProjectSectionDetails.FAMILY_ID;
                            var proectdetails = _dbcontext.TB_PROJECT_SECTION_DETAILS.FirstOrDefault(x => x.RECORD_ID == recordIda && x.FAMILY_ID == familyId);
                            if (proectdetails != null)
                            {
                                proectdetails.SORT_ORDER = tbProjectSectionDetails.SORT_ORDER;
                            }
                            _dbcontext.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController :  CategorysortChange", objException);
                // return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region Restore Catalog


        DataTable dTable = new DataTable();
        DataTable dTable_File = new DataTable();

        /// <summary>
        /// RestoreCatalog
        /// </summary>
        /// <param name="catalogId"></param>
        /// <returns></returns>
        public JsonResult RestoreCatalog(int catalogId, string filePath)
        {
            try
            {
                
                List<string> resultsRestore = new List<string>();
                string allowDuplicate = "0";
                string XMLName = "";
                string fileName, actualFileName, message, customerFolder;
                string FilePath = string.Empty;
                customerFolder = message = fileName = actualFileName = string.Empty;
                int customer_Id = 0;
                customer_Id = homeController.GetCustomerId();
                customerFolder = _dbcontext.Customers.Where(s => s.CustomerId == customer_Id).FirstOrDefault().Comments;
                if (Request.Files != null)
                {
                    var customers = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    string dataSource = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.DataSource).FirstOrDefault();
                    string userID = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.UserID).FirstOrDefault();
                    string password = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.Password).FirstOrDefault();
                    //var file = Request.Files[0];
                    //actualFileName = file.FileName;
                    //int size = file.ContentLength;
                    try
                    {
                        // Extract zip file start
                        string physicalPath = "";
                        string path = "";
                        string restoreSourcePath = "";

                        if (pathCheckFlag.ToLower() == "false")
                        {
                            physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages/" + customerFolder)+ filePath);
                            path = Path.Combine(Server.MapPath("~/Content/ProductImages/" + customerFolder + "/BacupFiles"));
                            fileName = Path.GetFileNameWithoutExtension(physicalPath);
                            restoreSourcePath = Path.Combine(Server.MapPath("~/Content/ProductImages/" + customerFolder)+ filePath);
                        }
                        else if (pathCheckFlag.ToLower() == "true")
                        {
                            physicalPath = Path.Combine(serverPathShareVal+"/Content/ProductImages/" + customerFolder+filePath);
                            path = Path.Combine(serverPathShareVal+"/Content/ProductImages/" + customerFolder+ "/BacupFiles");
                            fileName = Path.GetFileNameWithoutExtension(physicalPath);
                            restoreSourcePath = Path.Combine(serverPathShareVal + "/Content/ProductImages/" + customerFolder+ filePath);
                        }

                        
                        string restoreDestPath = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.RestoreDestPath).FirstOrDefault();
                        //string restoreDestPath = ConfigurationManager.AppSettings["RestoreDestPath"];
                        restoreDestPath = restoreDestPath + fileName + ".bak";
                        // CopyToServer(restoreSourcePath, restoreDestPath);

                        string ServerPath = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.ServerPath).FirstOrDefault();

                        //string ServerPath = ConfigurationManager.AppSettings["ServerPath"];
                        ServerPath = ServerPath + fileName + ".bak";
                        BackUpRestoreController obj = new BackUpRestoreController();

                        string backUpMdFPath = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.RestoreDestPath).FirstOrDefault();

                        //string backUpMdFPath = ConfigurationManager.AppSettings["RestoreDestPath"];
                        System.Web.HttpContext.Current.Session["CreateDatabasePath"] = null;
                        System.Web.HttpContext.Current.Session["CreateDatabasePath"] = backUpMdFPath;
                        // Create database
                        string create = obj.CreateDatabse(fileName, backUpMdFPath);
                        // Restore database
                        CopyToServer(restoreSourcePath, ServerPath);
                        string restore = RestoreDatabse(fileName, restoreDestPath);
                        System.Web.HttpContext.Current.Session["DatabaseName"] = null;
                        System.Web.HttpContext.Current.Session["DatabaseName"] = fileName;
                        string strConString = @"Data Source= " + dataSource + " ;Initial Catalog=" + fileName + ";User ID=" + userID + ";Password=" + password + ";MultipleActiveResultSets=True;Connection Timeout=0";
                        string[] tables = { "PICKLIST", "AttributeLIst", "Category", "FAMILY", "PRODUCT", "UserDetails", "IndesignDetails", "RoleCatalog", "RoleGroup_name", "RoleAttribute", "TableDesigner" };
                        List<string> allTables = new List<string>();
                        List<string> selectedTables = new List<string>();
                        allTables = tables.ToList();
                        string[] tables_Selected = new string[] { };

                        // Restore "File" (Import as)
                        if (XMLName != null && XMLName != "")
                        {
                            tables_Selected = XMLName.Split(',');
                            selectedTables = tables_Selected.ToList();
                            allTables = selectedTables;
                        }
                        string catalog_Name = _dbcontext.TB_CATALOG.Where(s => s.CATALOG_ID == catalogId).FirstOrDefault().CATALOG_NAME;
                        string sessionid = Guid.NewGuid().ToString();
                        sessionid = Regex.Replace(sessionid, "[^a-zA-Z0-9_]+", "");
                        int New_catalogId = 0;
                        var roleCatalog = new DataSet();
                        DataTable newRole = new DataTable();
                        DataTable newAttributes = new DataTable();
                        int ItemVal = Convert.ToInt32(allowDuplicate);
                        DataTable objDt2 = new DataTable("RoleGroup_name");
                        DataTable roleattribute = new DataTable("RoleAttribute");
                        AdvanceImportApiController apiController = new AdvanceImportApiController();
                        ImportApiController importApi = new ImportApiController();
                        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            conn.Open();
                            foreach (string table in allTables)
                            {
                                var Passtable_Temp = new DataTable();
                                var val = "";
                                // Check table is available and move to data table to create temp table 
                                using (var connection = new SqlConnection(strConString))
                                {
                                    connection.Open();
                                    var cmd = new SqlCommand("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'" + table + " '", connection);
                                    cmd.CommandTimeout = 0;
                                    val = Convert.ToString(cmd.ExecuteScalar());
                                    if (val != null && val != "")
                                    {
                                        string query = "SELECT * FROM " + table;
                                        SqlCommand objSqlCommand = connection.CreateCommand();
                                        objSqlCommand.CommandText = query;
                                        objSqlCommand.CommandType = CommandType.Text;
                                        objSqlCommand.Connection = connection;
                                        var da = new SqlDataAdapter(objSqlCommand);
                                        da.Fill(Passtable_Temp);
                                    }
                                }

                                if (val != null && val != "" && Passtable_Temp != null && Passtable_Temp.Rows.Count > 0)
                                {
                                    DataTable Passtable = new DataTable();
                                    Passtable = RemoveIdColums(Passtable_Temp);

                                    if (conn != null && conn.State != ConnectionState.Open)
                                    {
                                        conn.Open();
                                    }

                                    if (table.ToLower() == "product")
                                    {
                                        if (Passtable.Columns.Contains("ITEM#"))
                                            Passtable.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                                    }

                                    // Create new catalog
                                    if (table.ToLower() == "category")
                                    {
                                        System.Web.HttpContext.Current.Session["new_catalogId"] = 0;
                                        New_catalogId = CreateCatalog(fileName);
                                        System.Web.HttpContext.Current.Session["new_catalogId"] = New_catalogId;
                                    }
                                    if (table.ToLower() == "rolegroup_name")
                                    {
                                        objDt2 = FillIdColumnValues(Passtable);
                                    }
                                    if (table.ToLower() == "roleattribute")
                                    {
                                        roleattribute = FillIdColumnValuesforRoleAttr(Passtable);
                                    }

                                    if (conn != null && conn.State != ConnectionState.Open)
                                    {
                                        conn.Open();
                                    }
                                    // Create Temp table
                                    sqlString = "IF OBJECT_ID('TEMPDB..##IMPORTTEMP" + sessionid + "') IS NOT NULL   DROP TABLE  [##IMPORTTEMP" + sessionid + "]";
                                    SqlCommand CmdObj = new SqlCommand(sqlString, conn);
                                    CmdObj.ExecuteNonQuery();
                                    if (table.ToLower() == "rolegroup_name")
                                        sqlString = CreateTable("[##IMPORTTEMP" + sessionid + "]", objDt2);
                                    else if (table.ToLower() == "roleattribute")
                                        sqlString = CreateTable("[##IMPORTTEMP" + sessionid + "]", roleattribute);
                                    else
                                        sqlString = CreateTable("[##IMPORTTEMP" + sessionid + "]", Passtable);
                                    CmdObj = new SqlCommand(sqlString, conn);
                                    CmdObj.ExecuteNonQuery();
                                    var bulkCopy_cp = new SqlBulkCopy(conn)
                                    {
                                        DestinationTableName = "[##IMPORTTEMP" + sessionid + "]"
                                    };
                                    if (table.ToLower() == "rolegroup_name")
                                        bulkCopy_cp.WriteToServer(objDt2);
                                    else if (table.ToLower() == "roleattribute")
                                        bulkCopy_cp.WriteToServer(roleattribute);
                                    else
                                        bulkCopy_cp.WriteToServer(Passtable);

                                    if (table.ToLower() == "category" || table.ToLower() == "family" || table.ToLower() == "product" || table.ToLower() == "indesigndetails" || table.ToLower() == "tabledesigner" || table.ToLower() == "rolecatalog")
                                    {
                                        sqlString = "UPDATE [##IMPORTTEMP" + sessionid + "] SET CATALOG_NAME = '" + fileName + "'";
                                        CmdObj = new SqlCommand(sqlString, conn);
                                        CmdObj.ExecuteNonQuery();

                                        Passtable = new DataTable();
                                        string query = "";
                                        if (table.ToLower() == "rolecatalog")
                                            query = "SELECT * FROM [##IMPORTTEMP" + sessionid + "] Where CATALOG_NAME ='" + catalog_Name + "'";
                                        else
                                            query = "SELECT * FROM [##IMPORTTEMP" + sessionid + "]";
                                        CmdObj = new SqlCommand(query, conn);
                                        var da = new SqlDataAdapter(CmdObj);
                                        da.Fill(Passtable);
                                    }

                                    DataTable importData = new DataTable();
                                    if (table.ToLower() == "rolecatalog")
                                    {
                                        DataTable objDt1 = new DataTable("RoleCatalog");
                                        objDt1 = Passtable;
                                        roleCatalog.Tables.Add(objDt1);
                                        newRole = new DataTable();
                                        sqlString = "SELECT * FROM [##IMPORTTEMP" + sessionid + "] Where ROLE_NAME not in (select ROLE_NAME from TB_ROLE where ROLE_ID in (Select RoleId from Customer_Role_Catalog where CATALOG_ID=" + New_catalogId + "))";
                                        CmdObj = new SqlCommand(sqlString, conn);
                                        var da = new SqlDataAdapter(CmdObj);
                                        da.Fill(newRole);
                                        string roleCatalogResult = "";
                                        if (newRole != null && newRole.Rows.Count > 0)
                                        {
                                            foreach (DataRow dr in newRole.Rows)
                                            {
                                                int roleId = SaveRole(Convert.ToString(dr["ROLE_NAME"]), New_catalogId);
                                                if (roleId > 0)
                                                {
                                                    roleCatalogResult = "Saved";
                                                }
                                            }
                                            resultsRestore.Add("rolecatalog" + "," + roleCatalogResult);
                                        }
                                        else
                                        {
                                            resultsRestore.Add("rolecatalog" + "," + "Saved");
                                        }
                                    }
                                    else if (table.ToLower() == "rolegroup_name")
                                    {
                                        CmdObj = new SqlCommand("RestoreDatabase", conn) { CommandTimeout = 0 };
                                        CmdObj.CommandType = CommandType.StoredProcedure;
                                        CmdObj.Parameters.Add("@SESSION_ID", SqlDbType.VarChar).Value = sessionid;
                                        CmdObj.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "ROLEGROUP";
                                        int roleGroup = CmdObj.ExecuteNonQuery();
                                        if (roleGroup > 0)
                                        {
                                            resultsRestore.Add("rolegroup_name" + "," + "Saved");
                                        }
                                        else
                                            resultsRestore.Add("rolegroup_name" + "," + "Failed");
                                    }
                                    else if (table.ToLower() == "roleattribute")
                                    {
                                        DataTable objDt3 = new DataTable("RoleAttribute");
                                        objDt3 = Passtable;
                                        roleCatalog.Tables.Add(objDt3);

                                        sqlString = "SELECT * FROM [##IMPORTTEMP" + sessionid + "] Where ATTRIBUTE_ID not in (SELECT DISTINCT(ATTRIBUTE_ID) FROM CUSTOMERATTRIBUTE WHERE CUSTOMER_ID = " + customer_Id + ")";
                                        CmdObj = new SqlCommand(sqlString, conn);
                                        var da = new SqlDataAdapter(CmdObj);
                                        da.Fill(newAttributes);
                                        string roleResult = RoleAttribute(newAttributes, customer_Id);
                                        if (roleResult.Contains("Saved"))
                                            resultsRestore.Add("roleattribute" + "," + roleResult);
                                        else
                                            resultsRestore.Add("roleattribute" + "," + "Failed");
                                    }

                                    if (table.ToLower() == "attributelist")
                                    {
                                        CmdObj = new SqlCommand("RestoreDatabase", conn) { CommandTimeout = 0 };
                                        CmdObj.CommandType = CommandType.StoredProcedure;
                                        CmdObj.Parameters.Add("@SESSION_ID", SqlDbType.VarChar).Value = sessionid;
                                        CmdObj.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "ATTRIBUTELIST";
                                        int attributeList = CmdObj.ExecuteNonQuery();
                                        if (attributeList > 0)
                                        {
                                            resultsRestore.Add("attributelist" + "," + "Saved");
                                        }
                                        else
                                            resultsRestore.Add("attributelist" + "," + "Failed");
                                    }
                                    else if (table.ToLower() == "picklist")
                                    {
                                        CmdObj = new SqlCommand("STP_LS_IMPORT_PICKLIST", conn) { CommandTimeout = 0 };
                                        CmdObj.CommandType = CommandType.StoredProcedure;
                                        CmdObj.Parameters.Add("@PICKLIST_NAME", SqlDbType.VarChar, 100).Value = "";
                                        CmdObj.Parameters.Add("@PICKLIST_ITEM_VALUE", SqlDbType.VarChar, 100).Value = "";
                                        CmdObj.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                                        CmdObj.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar).Value = User.Identity.Name;
                                        CmdObj.Parameters.Add("@SESSION_ID", SqlDbType.VarChar).Value = sessionid;
                                        CmdObj.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "IMPORT";
                                        int pick_List = CmdObj.ExecuteNonQuery();
                                        if (pick_List > 0)
                                        {
                                            resultsRestore.Add("picklist" + "," + "Saved");
                                        }
                                        else
                                        {
                                            resultsRestore.Add("picklist" + "," + "Failed");
                                        }
                                    }
                                    else if (table.ToLower() == "category")
                                    {
                                        DataTable objDatatable = apiController.GetImportSpecsForRestore(Passtable, "CATEGORIES");
                                        var jsonString = JsonConvert.SerializeObject(objDatatable);
                                        JArray model = JArray.Parse(jsonString);
                                        string result = apiController.FinishRestore(allowDuplicate, New_catalogId, "Categories", Passtable, model);
                                        if (result.Contains("Success"))
                                        {
                                            resultsRestore.Add("category" + "," + "Saved");
                                        }
                                        else
                                        {
                                            resultsRestore.Add("category" + "," + "Failed");
                                        }
                                    }
                                    else if (table.ToLower() == "family")
                                    {
                                        DataTable objDatatable = apiController.GetImportSpecsForRestore(Passtable, "FAMILIES");
                                        var jsonString = JsonConvert.SerializeObject(objDatatable);
                                        JArray model = JArray.Parse(jsonString);
                                        string result = apiController.FinishRestore(allowDuplicate, New_catalogId, "Families", Passtable, model);
                                        if (result.Contains("Success"))
                                        {
                                            resultsRestore.Add("family" + "," + "Saved");
                                        }
                                        else
                                            resultsRestore.Add("family" + "," + "Failed");
                                    }
                                    else if (table.ToLower() == "product")
                                    {
                                        DataTable objDatatable = apiController.GetImportSpecsForRestore(Passtable, "PRODUCTS");
                                        var jsonString = JsonConvert.SerializeObject(objDatatable);
                                        JArray model = JArray.Parse(jsonString);
                                        string result = apiController.FinishRestore(allowDuplicate, New_catalogId, "Products", Passtable, model);
                                        if (result.Contains("Success"))
                                        {
                                            resultsRestore.Add("product" + "," + "Saved");
                                        }
                                        else
                                            resultsRestore.Add("product" + "," + "Failed");
                                    }
                                    else if (table.ToLower() == "tabledesigner")
                                    {
                                        string action = "INSERT";
                                        CmdObj = new SqlCommand("STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION", conn) { CommandTimeout = 0 };
                                        CmdObj.CommandType = CommandType.StoredProcedure;
                                        CmdObj.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = New_catalogId;
                                        CmdObj.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                                        CmdObj.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                        CmdObj.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = "";
                                        CmdObj.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                        CmdObj.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = "";
                                        CmdObj.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 4000).Value = "";
                                        CmdObj.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar, 500).Value = "";
                                        CmdObj.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar, -1).Value = "";
                                        CmdObj.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action ?? "";
                                        CmdObj.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                                        CmdObj.Parameters["@Result"].Direction = ParameterDirection.Output;
                                        int k = CmdObj.ExecuteNonQuery();

                                        sqlString = "DELETE FROM TB_FAMILY_TABLE_STRUCTURE WHERE FAMILY_ID=0 AND CATALOG_ID = " + New_catalogId;
                                        CmdObj = new SqlCommand(sqlString, conn);
                                        CmdObj.ExecuteNonQuery();
                                        if (k > 0)
                                        {
                                            resultsRestore.Add("tabledesigner" + "," + "Saved");
                                        }
                                        else
                                            resultsRestore.Add("tabledesigner" + "," + "Failed");
                                    }
                                    else if (table.ToLower() == "indesigndetails")
                                    {
                                        string indesignResult = RestoreInDesignDetails(sessionid, Passtable);
                                        if (indesignResult.ToLower() == "subquery")
                                            indesignResult = RestoreInDesignDetails(sessionid, Passtable);
                                        if (indesignResult.Contains("success"))
                                        {
                                            resultsRestore.Add("indesigndetails" + "," + "Saved");
                                        }
                                        else
                                            resultsRestore.Add("indesigndetails" + "," + "Failed");
                                    }
                                    else if (table.ToLower() == "userdetails")
                                    {
                                        string userResult = importApi.RestoreUserDetails(customer_Id, Passtable);
                                        if (userResult.Contains("Saved"))
                                        {
                                            resultsRestore.Add("userdetails" + "," + "Saved");
                                        }
                                        else
                                        {
                                            resultsRestore.Add("userdetails" + "," + "Failed");
                                        }
                                    }
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message + " First ";
                    }
                    dTable = GetResult(resultsRestore, null, "Data");

                    DashBoardModel model1 = new DashBoardModel();
                    foreach (DataColumn columns in dTable.Columns)
                    {
                        var objPTColumns = new PTColumns();
                        objPTColumns.Caption = columns.Caption;
                        objPTColumns.ColumnName = columns.ColumnName;
                        model1.Columns.Add(objPTColumns);
                    }
                    string JSONString = string.Empty;

                    for (int i = 0; i <= dTable.Columns.Count - 1; i++)
                    {
                        JSONString = JSONString + "~" + dTable.Rows[0][i];
                    }

                    // JSONString = JsonConvert.SerializeObject(dTable);
                    //JArray modelw = JArray.Parse(JSONString);
                    model1.Data = JSONString;
                    return new JsonResult() { Data = model1 };

                    // return Json(dTable, JsonRequestBehavior.AllowGet);
                }
                return Json(dTable, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController : RestoreCatalog", ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public string Restore(int catalogId, string restoreAs)
        {
            try
            {
                string returnMessage = "";
                int new_CatalogId = 0;
                if (System.Web.HttpContext.Current.Session["new_catalogId"] != null)
                {
                    new_CatalogId = (int)System.Web.HttpContext.Current.Session["new_catalogId"];
                }
                var catalogDetails = _dbcontext.TB_CATALOG.Where(s => s.CATALOG_ID == catalogId && s.CATALOG_NAME.ToLower() == restoreAs.ToLower()).FirstOrDefault();
                if (catalogDetails != null && catalogDetails.CATALOG_ID > 0)
                {
                    using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        connection.Open();
                        // Update New Catalog

                        string catalog_Name = _dbcontext.TB_CATALOG.Where(s => s.CATALOG_ID == catalogId).FirstOrDefault().CATALOG_NAME;
                        sqlString = "UPDATE TB_CATALOG SET CATALOG_NAME = '" + catalog_Name + "' WHERE CATALOG_ID = " + new_CatalogId;
                        SqlCommand sqlCommand = new SqlCommand(sqlString, connection);
                        sqlCommand.ExecuteNonQuery();

                        // Delete Previous catalog
                        // sqlCommand = new SqlCommand("STP_QSWS_CATALOG_DETAILS_DELETE", connection);
                        // sqlCommand.CommandType = CommandType.StoredProcedure;
                        //sqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                        //sqlCommand.ExecuteNonQuery();

                        string catalog = DeletePreviousCatalog(catalogId);
                        if (catalog.Contains("Deleted"))
                        {
                            returnMessage = "Catalog Restored Successfully";
                        }

                    }
                    return returnMessage;
                }
                else
                {
                    using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        connection.Open();
                        // Update New Catalog
                        sqlString = "UPDATE TB_CATALOG SET CATALOG_NAME = '" + restoreAs + "' WHERE CATALOG_ID = " + new_CatalogId;
                        SqlCommand sqlCommand = new SqlCommand(sqlString, connection);
                        sqlCommand.ExecuteNonQuery();

                        DeletedDatabase();
                    }
                    return "Catalog Restored Successfully";
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController : Restore", ex);
                return "Catalog Restored Failed";
            }
        }

        public string DeletePreviousCatalog(int catalogId)
        {
            try
            {
                var categorycount = _dbcontext.TB_CATALOG_SECTIONS.Count(x => x.CATALOG_ID == catalogId && x.FLAG_RECYCLE == "A");
                var objCatalogAttributes =
                    _dbcontext.TB_CATALOG_ATTRIBUTES.Where(x => x.CATALOG_ID == catalogId);
                if (objCatalogAttributes.Any())
                {
                    foreach (var objCatalogAttribute in objCatalogAttributes)
                    {
                        _dbcontext.TB_CATALOG_ATTRIBUTES.Remove(objCatalogAttribute);
                    }
                    _dbcontext.SaveChanges();
                }

                var objCustomerCatalogs =
                  _dbcontext.Customer_Catalog.Where(x => x.CATALOG_ID == catalogId);
                if (objCustomerCatalogs.Any())
                {
                    foreach (var objCustomerCatalog in objCustomerCatalogs)
                    {
                        _dbcontext.Customer_Catalog.Remove(objCustomerCatalog);
                    }
                    _dbcontext.SaveChanges();
                }
                var objproject = _dbcontext.TB_PROJECT.Where(y => y.CATALOG_ID == catalogId);
                if (objproject.Any())
                {
                    foreach (var tbProject in objproject)
                    {
                        _dbcontext.TB_PROJECT.Remove(tbProject);
                    }
                    _dbcontext.SaveChanges();
                }

                TB_CATALOG objcatalog = _dbcontext.TB_CATALOG.Find(catalogId);
                if (objcatalog != null)
                {
                    objcatalog.FLAG_RECYCLE = "R";
                }
                _dbcontext.SaveChanges();
                DeletedDatabase();
                return "Deleted";
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController : DeletePreviousCatalog", ex);
                return "Action not completed";
            }
        }

        public DataTable GetResult(List<string> restoreResult, string selectedItems, string RestoreType)
        {
            try
            {
                var result = new DataTable();

                string ATTRIBUTE_LIST = "";
                string PICKLIST = "";
                string USER_AND_ROLE = "";
                string INDESIGN_PROJECTS = "";
                string TABLE_DESIGNER = "";
                string PRODUCTS_WITH_HIERARCHY = "";

                string[] tables = { "PRODUCTS WITH HIERARCHY", "ATTRIBUTE LIST", "PICKLIST", "USER AND ROLE", "INDESIGN PROJECTS", "TABLE DESIGNER" };
                List<string> allTables = new List<string>();
                if (RestoreType.ToLower() != "file")
                {
                    allTables = tables.ToList();
                }
                else
                {
                    if (selectedItems != null)
                    {
                        if (selectedItems.Contains("CATEGORY"))
                            allTables.Add("PRODUCTS WITH HIERARCHY");
                        if (selectedItems.Contains("ATTRIBUTE"))
                            allTables.Add("ATTRIBUTE LIST");
                        if (selectedItems.Contains("USER"))
                            allTables.Add("USER AND ROLE");
                        if (selectedItems.Contains("INDESIGN"))
                            allTables.Add("INDESIGN PROJECTS");
                        if (selectedItems.Contains("TABLE"))
                            allTables.Add("TABLE DESIGNER");
                    }
                }


                foreach (string selectedTable in allTables)
                {
                    DataColumn dc = new DataColumn(selectedTable);
                    dc.DataType = typeof(string);
                    dc.DefaultValue = "";
                    result.Columns.Add(dc);
                }
                if (restoreResult != null)
                {
                    foreach (string item in restoreResult)
                    {
                        string[] splitItem = item.Split(',');
                        if (splitItem[0].Contains("category") || splitItem[0].Contains("family") || splitItem[0].Contains("product"))
                        {
                            PRODUCTS_WITH_HIERARCHY = PRODUCTS_WITH_HIERARCHY + "," + splitItem[0] + " " + splitItem[1];
                        }
                        if (splitItem[0].Contains("user") || splitItem[0].Contains("role"))
                        {
                            USER_AND_ROLE = USER_AND_ROLE + "," + splitItem[0] + " " + splitItem[1];
                        }
                        if (splitItem[0].Contains("picklist"))
                        {
                            PICKLIST = PICKLIST + "," + splitItem[0] + "" + splitItem[1];
                        }
                        if (splitItem[0].Contains("attributel"))
                        {
                            ATTRIBUTE_LIST = splitItem[0] + " " + splitItem[1];
                        }
                        if (splitItem[0].Contains("indesign"))
                        {
                            INDESIGN_PROJECTS = splitItem[0] + " " + splitItem[1];
                        }
                        if (splitItem[0].Contains("table"))
                        {
                            TABLE_DESIGNER = splitItem[0] + " " + splitItem[1];
                        }
                    }

                    DataRow dRow = result.NewRow();
                    result.Rows.InsertAt(dRow, 0);
                    if (result.Columns.Contains("PRODUCTS WITH HIERARCHY"))
                    {
                        result.Rows[0]["PRODUCTS WITH HIERARCHY"] = PRODUCTS_WITH_HIERARCHY;
                    }
                    if (result.Columns.Contains("ATTRIBUTE LIST"))
                    {
                        result.Rows[0]["ATTRIBUTE LIST"] = ATTRIBUTE_LIST;
                    }
                    if (result.Columns.Contains("PICKLIST"))
                    {
                        result.Rows[0]["PICKLIST"] = PICKLIST;
                    }
                    if (result.Columns.Contains("USER AND ROLE"))
                    {
                        result.Rows[0]["USER AND ROLE"] = USER_AND_ROLE;
                    }
                    if (result.Columns.Contains("INDESIGN PROJECTS"))
                    {
                        result.Rows[0]["INDESIGN PROJECTS"] = INDESIGN_PROJECTS;
                    }
                    if (result.Columns.Contains("TABLE DESIGNER"))
                    {
                        result.Rows[0]["TABLE DESIGNER"] = TABLE_DESIGNER;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController : GetResult", ex);
                return null;
            }
        }


        /// <summary>
        /// RestoreCatalogfromFiles
        /// </summary>
        /// <param name="catalogId"></param>
        /// <param name="selectedItems"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public JsonResult RestoreCatalogfromFiles(int catalogId, string selectedItems, string filePath)
        {
            try
            {
               
                List<string> resultsRestore_File = new List<string>();
                string allowDuplicate = "0";
                string fileName, actualFileName, message, customerFolder;
                string FilePath = string.Empty;
                customerFolder = message = fileName = actualFileName = string.Empty;
                int customer_Id = 0;
                customer_Id = homeController.GetCustomerId();
                customerFolder = _dbcontext.Customers.Where(s => s.CustomerId == customer_Id).FirstOrDefault().Comments;
                List<string> selectedTables = new List<string>();
                List<string> allTables = new List<string>();
                string[] tables_Selected = new string[] { };

                // Restore "File" (Import as)
                if (selectedItems != null && selectedItems != "")
                {
                    tables_Selected = selectedItems.Split(',');
                    selectedTables = tables_Selected.ToList();
                    allTables = selectedTables;
                }
                string catalog_Name = _dbcontext.TB_CATALOG.Where(s => s.CATALOG_ID == catalogId).FirstOrDefault().CATALOG_NAME;
                string sessionid = Guid.NewGuid().ToString();
                sessionid = Regex.Replace(sessionid, "[^a-zA-Z0-9_]+", "");
                var roleCatalog = new DataSet();
                DataTable newRole = new DataTable();
                DataTable newAttributes = new DataTable();
                int ItemVal = Convert.ToInt32(allowDuplicate);
                DataTable objDt2 = new DataTable("RoleGroup_name");
                DataTable roleattribute = new DataTable("RoleAttribute");
                AdvanceImportApiController apiController = new AdvanceImportApiController();
                ImportApiController importApi = new ImportApiController();
                if (Request.Files != null)
                {
                    var file = "";
                    if (pathCheckFlag.ToLower() == "false")
                    {
                        file = Path.Combine(Server.MapPath("~/Content/ProductImages/" + customerFolder) + filePath);                       
                    }
                    else if (pathCheckFlag.ToLower() == "true")
                    {
                        file = Path.Combine(serverPathShareVal + "/Content/ProductImages/" + customerFolder+ filePath);                        
                    }
                    FileInfo oFileInfo = new FileInfo(file);
                    actualFileName = oFileInfo.Name;
                    fileName = Guid.NewGuid() + Path.GetExtension(oFileInfo.Name);
                    long size = oFileInfo.Length;
                    try
                    {
                        string physicalPath = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName);
                        oFileInfo.CopyTo(physicalPath);
                        fileName = Path.GetFileNameWithoutExtension(actualFileName);
                        var ds = new DataSet();
                        var indesignDetails = new DataTable();
                        ds = GetDataFromExcelForRestore(actualFileName, physicalPath, " SELECT * from [" + actualFileName + "$]", "");
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables.Contains("PRODUCT") && ds.Tables.Contains("IndesignDetails"))
                        {
                            indesignDetails = ds.Tables["IndesignDetails"];
                            ds.Tables["IndesignDetails"].TableName = "Remove";
                            indesignDetails.TableName = "IndesignDetails";
                            ds.Tables.Remove(indesignDetails);
                            ds.Tables.Add(indesignDetails);
                        }
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                            {
                                conn.Open();
                                foreach (DataTable dt in ds.Tables)
                                {
                                    if (dt != null && dt.Rows.Count > 0)
                                    {
                                        string table = string.Empty;
                                        table = dt.TableName;
                                        if (allTables.Contains(table.ToUpper()))
                                        {
                                            DataTable Passtable = new DataTable();
                                            Passtable = RemoveIdColums(dt);

                                            if (table.ToLower() == "product")
                                            {
                                                if (Passtable.Columns.Contains("ITEM#"))
                                                    Passtable.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                                            }

                                            if (table.ToLower() == "rolegroup_name")
                                            {
                                                objDt2 = FillIdColumnValues(Passtable);
                                            }
                                            if (table.ToLower() == "roleattribute")
                                            {
                                                roleattribute = FillIdColumnValuesforRoleAttr(Passtable);
                                            }

                                            // Create Temp table
                                            sqlString = "IF OBJECT_ID('TEMPDB..##IMPORTTEMP" + sessionid + "') IS NOT NULL   DROP TABLE  [##IMPORTTEMP" + sessionid + "]";
                                            SqlCommand CmdObj = new SqlCommand(sqlString, conn);
                                            CmdObj.ExecuteNonQuery();
                                            if (table.ToLower() == "rolegroup_name")
                                                sqlString = CreateTable("[##IMPORTTEMP" + sessionid + "]", objDt2);
                                            else if (table.ToLower() == "roleattribute")
                                                sqlString = CreateTable("[##IMPORTTEMP" + sessionid + "]", roleattribute);
                                            else
                                                sqlString = CreateTable("[##IMPORTTEMP" + sessionid + "]", Passtable);
                                            CmdObj = new SqlCommand(sqlString, conn);
                                            CmdObj.ExecuteNonQuery();
                                            var bulkCopy_cp = new SqlBulkCopy(conn)
                                            {
                                                DestinationTableName = "[##IMPORTTEMP" + sessionid + "]"
                                            };
                                            if (table.ToLower() == "rolegroup_name")
                                                bulkCopy_cp.WriteToServer(objDt2);
                                            else if (table.ToLower() == "roleattribute")
                                                bulkCopy_cp.WriteToServer(roleattribute);
                                            else
                                                bulkCopy_cp.WriteToServer(Passtable);

                                            if (table.ToLower() == "rolecatalog")
                                            {
                                                DataTable objDt1 = new DataTable("RoleCatalog");
                                                objDt1 = Passtable;
                                                // roleCatalog.Tables.Add(objDt1);
                                                newRole = new DataTable();
                                                sqlString = "SELECT * FROM [##IMPORTTEMP" + sessionid + "] Where ROLE_NAME not in (select ROLE_NAME from TB_ROLE where ROLE_ID in (Select RoleId from Customer_Role_Catalog where CATALOG_ID=" + catalogId + "))";
                                                CmdObj = new SqlCommand(sqlString, conn);
                                                var da = new SqlDataAdapter(CmdObj);
                                                da.Fill(newRole);
                                                string roleCatalogResult = string.Empty;
                                                if (newRole != null && newRole.Rows.Count > 0)
                                                {
                                                    foreach (DataRow dr in newRole.Rows)
                                                    {
                                                        int roleId = SaveRole(Convert.ToString(dr["ROLE_NAME"]), catalogId);
                                                        if (roleId > 0)
                                                        {
                                                            roleCatalogResult = "Saved";
                                                        }
                                                    }
                                                    resultsRestore_File.Add("rolecatalog" + "," + roleCatalogResult);
                                                }
                                                resultsRestore_File.Add("rolecatalog" + "," + roleCatalogResult);
                                            }
                                            else if (table.ToLower() == "rolegroup_name")
                                            {
                                                CmdObj = new SqlCommand("RestoreDatabase", conn) { CommandTimeout = 0 };
                                                CmdObj.CommandType = CommandType.StoredProcedure;
                                                CmdObj.Parameters.Add("@SESSION_ID", SqlDbType.VarChar).Value = sessionid;
                                                CmdObj.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "ROLEGROUP";
                                                int roleGroup = CmdObj.ExecuteNonQuery();
                                                if (roleGroup > 0)
                                                {
                                                    resultsRestore_File.Add("rolegroup_name" + "," + "Saved");
                                                }
                                                else
                                                    resultsRestore_File.Add("rolegroup_name" + "," + "Failed");
                                            }
                                            else if (table.ToLower() == "roleattribute")
                                            {
                                                DataTable objDt3 = new DataTable("RoleAttribute");
                                                objDt3 = Passtable;
                                                // roleCatalog.Tables.Add(objDt3);

                                                sqlString = "SELECT * FROM [##IMPORTTEMP" + sessionid + "] Where ATTRIBUTE_ID not in (SELECT DISTINCT(ATTRIBUTE_ID) FROM CUSTOMERATTRIBUTE WHERE CUSTOMER_ID = " + customer_Id + ")";
                                                CmdObj = new SqlCommand(sqlString, conn);
                                                var da = new SqlDataAdapter(CmdObj);
                                                da.Fill(newAttributes);
                                                string roleResult = RoleAttribute(newAttributes, customer_Id);
                                                if (roleResult.Contains("Saved"))
                                                    resultsRestore_File.Add("roleattribute" + "," + roleResult);
                                                else
                                                    resultsRestore_File.Add("roleattribute" + "," + "Failed");
                                            }

                                            if (table.ToLower() == "attributelist")
                                            {
                                                CmdObj = new SqlCommand("RestoreDatabase", conn) { CommandTimeout = 0 };
                                                CmdObj.CommandType = CommandType.StoredProcedure;
                                                CmdObj.Parameters.Add("@SESSION_ID", SqlDbType.VarChar).Value = sessionid;
                                                CmdObj.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "ATTRIBUTELIST";
                                                int attributeList = CmdObj.ExecuteNonQuery();
                                                if (attributeList > 0)
                                                {
                                                    resultsRestore_File.Add("attributelist" + "," + "Saved");
                                                }
                                                else
                                                    resultsRestore_File.Add("attributelist" + "," + "Failed");
                                            }
                                            else if (table.ToLower() == "picklist")
                                            {
                                                CmdObj = new SqlCommand("STP_LS_IMPORT_PICKLIST", conn) { CommandTimeout = 0 };
                                                CmdObj.CommandType = CommandType.StoredProcedure;
                                                CmdObj.Parameters.Add("@PICKLIST_NAME", SqlDbType.VarChar, 100).Value = "";
                                                CmdObj.Parameters.Add("@PICKLIST_ITEM_VALUE", SqlDbType.VarChar, 100).Value = "";
                                                CmdObj.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                                                CmdObj.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar).Value = User.Identity.Name;
                                                CmdObj.Parameters.Add("@SESSION_ID", SqlDbType.VarChar).Value = sessionid;
                                                CmdObj.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "IMPORT";
                                                int pick_List = CmdObj.ExecuteNonQuery();
                                                if (pick_List > 0)
                                                {
                                                    resultsRestore_File.Add("picklist" + "," + "Saved");
                                                }
                                                else
                                                {
                                                    resultsRestore_File.Add("picklist" + "," + "Failed");
                                                }
                                            }
                                            else if (table.ToLower() == "category")
                                            {
                                                DataTable objDatatable = apiController.GetImportSpecsForRestore(Passtable, "CATEGORIES");
                                                var jsonString = JsonConvert.SerializeObject(objDatatable);
                                                JArray model = JArray.Parse(jsonString);
                                                string result = apiController.FinishRestore(allowDuplicate, catalogId, "Categories", Passtable, model);
                                                if (result.Contains("Success"))
                                                {
                                                    resultsRestore_File.Add("category" + "," + "Saved");
                                                }
                                                else
                                                {
                                                    resultsRestore_File.Add("category" + "," + "Failed");
                                                }
                                            }
                                            else if (table.ToLower() == "family")
                                            {
                                                DataTable objDatatable = apiController.GetImportSpecsForRestore(Passtable, "FAMILIES");
                                                var jsonString = JsonConvert.SerializeObject(objDatatable);
                                                JArray model = JArray.Parse(jsonString);
                                                string result = apiController.FinishRestore(allowDuplicate, catalogId, "Families", Passtable, model);
                                                if (result.Contains("Success"))
                                                {
                                                    resultsRestore_File.Add("family" + "," + "Saved");
                                                }
                                                else
                                                {
                                                    resultsRestore_File.Add("family" + "," + "Failed");
                                                }
                                            }
                                            else if (table.ToLower() == "product")
                                            {
                                                DataTable objDatatable = apiController.GetImportSpecsForRestore(Passtable, "PRODUCTS");
                                                var jsonString = JsonConvert.SerializeObject(objDatatable);
                                                JArray model = JArray.Parse(jsonString);
                                                string result = apiController.FinishRestore(allowDuplicate, catalogId, "Products", Passtable, model);
                                                if (result.Contains("Success"))
                                                {
                                                    resultsRestore_File.Add("product" + "," + "Saved");
                                                }
                                                else
                                                {
                                                    resultsRestore_File.Add("product" + "," + "Failed");
                                                }
                                            }
                                            else if (table.ToLower() == "tabledesigner")
                                            {
                                                string action = "INSERT";
                                                CmdObj = new SqlCommand("STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION", conn) { CommandTimeout = 0 };
                                                CmdObj.CommandType = CommandType.StoredProcedure;
                                                CmdObj.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = catalogId;
                                                CmdObj.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                                                CmdObj.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                                CmdObj.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = "";
                                                CmdObj.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                                CmdObj.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = "";
                                                CmdObj.Parameters.Add("@STRUCTURE_NAME", SqlDbType.VarChar, 4000).Value = "";
                                                CmdObj.Parameters.Add("@IS_DEFAULT", SqlDbType.VarChar, 500).Value = "";
                                                CmdObj.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.VarChar, -1).Value = "";
                                                CmdObj.Parameters.Add("@Action", SqlDbType.VarChar, 50).Value = action ?? "";
                                                CmdObj.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                                                CmdObj.Parameters["@Result"].Direction = ParameterDirection.Output;
                                                int k = CmdObj.ExecuteNonQuery();

                                                sqlString = "DELETE FROM TB_FAMILY_TABLE_STRUCTURE WHERE FAMILY_ID=0 AND CATALOG_ID = " + catalogId;
                                                CmdObj = new SqlCommand(sqlString, conn);
                                                CmdObj.ExecuteNonQuery();
                                                if (k > 0)
                                                {
                                                    resultsRestore_File.Add("tabledesigner" + "," + "Saved");
                                                }
                                                else
                                                    resultsRestore_File.Add("tabledesigner" + "," + "Failed");

                                            }
                                            else if (table.ToLower() == "indesigndetails")
                                            {
                                                string indesignResult = RestoreInDesignDetails(sessionid, Passtable);
                                                if (indesignResult.ToLower() == "subquery")
                                                    indesignResult = RestoreInDesignDetails(sessionid, Passtable);
                                                if (indesignResult.Contains("success"))
                                                {
                                                    resultsRestore_File.Add("indesigndetails" + "," + "Saved");
                                                }
                                                else
                                                    resultsRestore_File.Add("indesigndetails" + "," + "Failed");
                                            }
                                            else if (table.ToLower() == "userdetails")
                                            {
                                                string userResult = importApi.RestoreUserDetails(customer_Id, Passtable);
                                                if (userResult.Contains("Saved"))
                                                {
                                                    resultsRestore_File.Add("userdetails" + "," + "Saved");
                                                }
                                                else
                                                {
                                                    resultsRestore_File.Add("userdetails" + "," + "Failed");
                                                }
                                            }

                                            message = "Restored";
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        message = ex.Message + " First ";
                    }
                    //  dTable_File = GetResult(resultsRestore_File, selectedItems, "File");
                    dTable_File = GetResult(resultsRestore_File, null, "Data");

                    DashBoardModel model1 = new DashBoardModel();
                    foreach (DataColumn columns in dTable_File.Columns)
                    {
                        var objPTColumns = new PTColumns();
                        objPTColumns.Caption = columns.Caption;
                        objPTColumns.ColumnName = columns.ColumnName;
                        model1.Columns.Add(objPTColumns);
                    }
                    string JSONString = string.Empty;

                    for (int i = 0; i <= dTable_File.Columns.Count - 1; i++)
                    {
                        JSONString = JSONString + "~" + dTable_File.Rows[0][i];
                    }

                    model1.Data = JSONString;
                    return new JsonResult() { Data = model1 };
                    // return Json(dTable_File, JsonRequestBehavior.AllowGet);
                }
                return Json(dTable_File, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController : RestoreCatalogfromFiles", ex);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// RemoveIdColums
        /// </summary>
        /// <param name="finalDt"></param>
        /// <returns></returns>
        public DataTable RemoveIdColums(DataTable finalDt)
        {
            try
            {
                string[] exportidColumns = { "CATALOG_ID", "CATEGORY_ID", "SUBCATID_L1", "SUBCATID_L2", "SUBCATID_L3", "SUBCATID_L4", "SUBCATID_L5", "SUBCATID_L6", "SUBCATID_L7", "SUBCATID_L8", "FAMILY_ID", "SUBFAMILY_ID", "PRODUCT_ID", "SUBPRODUCT_ID" };
                foreach (var items in exportidColumns)
                {
                    if (finalDt.Columns.Contains(items))
                        finalDt.Columns.Remove(items);
                }
                return finalDt;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController : RemoveIdColums", ex);
                return null;
            }
        }

        /// <summary>
        /// CreateCatalog
        /// </summary>
        /// <param name="catalogName"></param>
        /// <returns></returns>
        public int CreateCatalog(string catalogName)
        {
            try
            {
                int result = 0;
                int customer_Id = Convert.ToInt32(_dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(a => a.CustomerId).FirstOrDefault());
                var catalognamecheck =
                       _dbcontext.TB_CATALOG.Join(_dbcontext.Customer_Catalog, cc => cc.CATALOG_ID, tc => tc.CATALOG_ID,
                           (cc, tc) => new { cc, tc })
                           .Any(x => x.cc.CATALOG_NAME == catalogName && x.tc.CustomerId == customer_Id);
                if (!catalognamecheck)
                {

                    var objcatalog = new TB_CATALOG
                    {
                        CATALOG_NAME = catalogName,
                        VERSION = "",
                        DESCRIPTION = "",
                        MODIFIED_USER = User.Identity.Name,
                        CREATED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now,
                        CREATED_DATE = DateTime.Now,
                        DEFAULT_FAMILY = 0
                    };
                    _dbcontext.TB_CATALOG.Add(objcatalog);
                    _dbcontext.SaveChanges();
                    var custmorids = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name);
                    var catalogids = _dbcontext.TB_CATALOG.Max(x => x.CATALOG_ID);
                    result = catalogids;
                    if (custmorids.Any())
                    {
                        var firstOrDefault = custmorids.FirstOrDefault();
                        if (firstOrDefault != null)
                        {
                            int customerid = firstOrDefault.CustomerId;
                            var objcustomercatalogcount =
                                _dbcontext.Customer_Catalog.Where(
                                    x => x.CATALOG_ID == catalogids && x.CustomerId == customerid);
                            if (!objcustomercatalogcount.Any())
                            {
                                var objcustomercatalog = new Customer_Catalog
                                {
                                    CustomerId = customerid,
                                    CATALOG_ID = catalogids,
                                    DateCreated = DateTime.Now,
                                    DateUpdated = DateTime.Now
                                };
                                _dbcontext.Customer_Catalog.Add(objcustomercatalog);


                                var objCustomerRoles =
                                    _dbcontext.aspnet_Roles.Where(x => x.CustomerId == customerid);
                                if (objCustomerRoles.Any())
                                {
                                    foreach (var objobjCustomerRole in objCustomerRoles)
                                    {
                                        var rolecatalog = _dbcontext.Customer_Role_Catalog.Where(x => x.CATALOG_ID == catalogids && x.RoleId == objobjCustomerRole.Role_id && x.CustomerId == customerid);
                                        if (!rolecatalog.Any())
                                        {
                                            var objcustomerrolecatalog = new Customer_Role_Catalog
                                            {
                                                RoleId = objobjCustomerRole.Role_id,
                                                CustomerId = customerid,
                                                CATALOG_ID = catalogids,
                                                DateCreated = DateTime.Now,
                                                DateUpdated = DateTime.Now,
                                                IsActive = true

                                            };
                                            _dbcontext.Customer_Role_Catalog.Add(objcustomerrolecatalog);
                                        }
                                    }
                                }
                                _dbcontext.SaveChanges();
                            }
                        }
                    }
                    var currCatalogId =
                        _dbcontext.TB_CATALOG_ATTRIBUTES.Where(
                            x => x.CATALOG_ID == catalogids && x.ATTRIBUTE_ID == 1);
                    if (!currCatalogId.Any())
                    {
                        var objcatalogAttributes = new TB_CATALOG_ATTRIBUTES
                        {
                            CATALOG_ID = catalogids,
                            ATTRIBUTE_ID = 1,
                            CREATED_USER = User.Identity.Name,
                            CREATED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now
                        };

                        _dbcontext.TB_CATALOG_ATTRIBUTES.Add(objcatalogAttributes);
                        _dbcontext.SaveChanges();
                    }
                }
                else
                {
                    var catalogname =
                       _dbcontext.TB_CATALOG.Join(_dbcontext.Customer_Catalog, cc => cc.CATALOG_ID, tc => tc.CATALOG_ID,
                           (cc, tc) => new { cc, tc })
                           .Where(x => x.cc.CATALOG_NAME == catalogName && x.tc.CustomerId == customer_Id).FirstOrDefault();
                    if (catalogname != null && catalogname.cc.CATALOG_ID > 0)
                    {
                        result = catalogname.cc.CATALOG_ID;
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController : CreateCatalog", ex);
                return 0;
            }
        }

        /// <summary>
        /// CopyToServer
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <param name="destPath"></param>
        public void CopyToServer(string sourcePath, string destPath)
        {
            System.IO.File.Copy(sourcePath, destPath, true);
        }

        /// <summary>
        /// RestoreDatabse
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="backUpPath"></param>
        /// <returns></returns>
        public string RestoreDatabse(string databaseName, string backUpPath)
        {
            try
            {
                SqlConnection con = new SqlConnection(connectionString);
                string qry_String = "RESTORE DATABASE [" + databaseName + "] FROM DISK = '" + backUpPath + "' WITH REPLACE";
                SqlCommand cmdObj = new SqlCommand(qry_String, con);
                cmdObj.CommandTimeout = 0;
                con.Open();
                cmdObj.ExecuteNonQuery();
                con.Close();
                return "Restored";
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at CatalogController : RestoreDatabse", objexception);
                return "Restore action not completed";
            }
        }

        /// <summary>
        /// ExecuteSqlQuery
        /// </summary>
        public void ExecuteSqlQuery()
        {
            try
            {
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = sqlString;
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController:  ExecuteSqlQuery", ex);
            }
        }

        /// <summary>
        /// CreateTable
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public static string CreateTable(string tableName, DataTable table)
        {
            string sqlsc = "CREATE TABLE " + tableName + "(";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                if (!table.Columns[i].ColumnName.Contains("["))
                {
                    sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                }
                else
                {
                    sqlsc += "\n" + table.Columns[i].ColumnName + "";
                }
                if (table.Columns[i].DataType.ToString().Contains("System.String"))
                    sqlsc += "nvarchar(max) ";
                else
                    sqlsc += "int";
                sqlsc += ",";
            }
            return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
        }

        /// <summary>
        /// SaveRole
        /// </summary>
        /// <param name="roleId"></param>
        /// <param name="catalog_Id"></param>
        /// <returns></returns>
        public int SaveRole(string roleId, int catalog_Id)
        {
            try
            {
                int roleIdreturn = 0;
                string customer_name = "";
                int customer_id = 0;
                int roletype = 3;

                var SuperAdminCheck = _dbcontext.vw_aspnet_UsersInRoles
                   .Join(_dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                   .Join(_dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcptps.tpf.UserName == User.Identity.Name).ToList();
                if (SuperAdminCheck.Any())
                {
                    var sd = SuperAdminCheck.FirstOrDefault();
                    if (sd.tcp.RoleType == 1)
                    {
                        roletype = 2;
                    }
                    else if (sd.tcp.RoleType == 2)
                    {
                        roletype = 3;

                    }
                }

                var customerList = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name);
                var custlist = customerList.Select(x => new { x.a.CompanyName, x.a.CustomerId }).ToList();
                if (custlist.Any())
                {
                    customer_name = custlist.Select(a => a.CompanyName).FirstOrDefault().Replace(" ", "").Replace("_", "");
                    customer_id = custlist.Select(a => a.CustomerId).FirstOrDefault();
                }
                roleId = customer_name + "_" + roleId;
                var tbroleCheck = _dbcontext.TB_ROLE.Where(a => a.ROLE_NAME == roleId);
                if (!tbroleCheck.Any())
                {
                    var result = _dbcontext.STP_CATALOGSTUDIO5_AddNewRole(roleId);
                    _dbcontext.SaveChanges();
                    if (result != null)
                    {
                        int rid = Convert.ToInt32(result);
                        var roleResult = _dbcontext.TB_ROLE.FirstOrDefault(a => a.ROLE_NAME == roleId);
                        if (roleResult != null)
                        {
                            UserRolesList role = new UserRolesList();
                            var aspnetrolescheck = _dbcontext.aspnet_Roles.Where(x => x.Role_id == roleResult.ROLE_ID);
                            if (!aspnetrolescheck.Any())
                            {
                                _dbcontext.aspnet_Roles_CreateRole("/", roleId);
                                var aspnetrolescheck1 = _dbcontext.aspnet_Roles.FirstOrDefault(x => x.RoleName == roleId);
                                if (aspnetrolescheck1 != null)
                                {
                                    roleIdreturn = roleResult.ROLE_ID;
                                    aspnetrolescheck1.Role_id = roleResult.ROLE_ID;
                                    aspnetrolescheck1.RoleType = roletype;
                                    aspnetrolescheck1.CustomerId = customer_id;
                                    aspnetrolescheck1.Prefix_CompanyName = customer_name;

                                    var objCustomercatalogs = _dbcontext.Customer_Catalog.Where(x => x.CustomerId == customer_id && x.CATALOG_ID == catalog_Id);
                                    if (objCustomercatalogs.Any())
                                    {
                                        foreach (var objCustomercatalog in objCustomercatalogs)
                                        {
                                            var rolecatalog = _dbcontext.Customer_Role_Catalog.Where(x => x.CATALOG_ID == objCustomercatalog.CATALOG_ID && x.RoleId == roleIdreturn && x.CustomerId == customer_id);
                                            if (!rolecatalog.Any())
                                            {
                                                var objcustomerrolecatalog = new Customer_Role_Catalog
                                                {
                                                    RoleId = roleIdreturn,
                                                    CustomerId = customer_id,
                                                    CATALOG_ID = objCustomercatalog.CATALOG_ID,
                                                    DateCreated = DateTime.Now,
                                                    DateUpdated = DateTime.Now,
                                                    IsActive = true
                                                };
                                                _dbcontext.Customer_Role_Catalog.Add(objcustomerrolecatalog);
                                            }
                                        }
                                    }
                                    _dbcontext.SaveChanges();
                                }
                            }
                        }
                    }
                    _dbcontext.SaveChanges();
                }
                else
                {
                    return roleIdreturn;
                }

                return roleIdreturn;

            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController: SaveRole", ex);
                return 0;
            }
        }

        /// <summary>
        /// FillIdColumnValues
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public DataTable FillIdColumnValues(DataTable dt)
        {
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    dt.Columns.Add("FUNCTION_ID", typeof(int));
                    dt.Columns.Add("ROLE_ID", typeof(int));
                    dt.Columns.Add("FUNCTION_GROUP_ID", typeof(int));

                    foreach (DataRow dr in dt.Rows)
                    {
                        string functionName = Convert.ToString(dr["FUNCTION_NAME"]);
                        string roleName = Convert.ToString(dr["ROLE_NAME"]);
                        string functionGroupName = Convert.ToString(dr["FUNCTION_GROUP_NAME"]);
                        int fId = 0;
                        int fGroupId = 0;
                        int rId = 0;
                        var fGroup = _dbcontext.TB_FUNCTIONGROUP.Where(s => s.FUNCTION_GROUP_NAME.ToLower() == functionGroupName.ToLower()).FirstOrDefault();
                        var role = _dbcontext.TB_ROLE.Where(s => s.ROLE_NAME.ToLower() == roleName.ToLower()).FirstOrDefault();
                        var function = _dbcontext.TB_FUNCTION.Where(s => s.FUNCTION_NAME.ToLower() == functionName.ToLower()).FirstOrDefault();
                        if (function != null && function.FUNCTION_ID > 0)
                            fId = function.FUNCTION_ID;
                        if (role != null && role.ROLE_ID > 0)
                            rId = role.ROLE_ID;
                        if (fGroup != null && fGroup.FUNCTION_GROUP_ID > 0)
                            fGroupId = fGroup.FUNCTION_GROUP_ID;
                        dr["FUNCTION_ID"] = fId;
                        dr["ROLE_ID"] = rId;
                        dr["FUNCTION_GROUP_ID"] = fGroupId;
                    }

                }
                return dt;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController : RestoreDatabse", ex);
                return null;
            }
        }

        /// <summary>
        /// FillIdColumnValuesforRoleAttr
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public DataTable FillIdColumnValuesforRoleAttr(DataTable dt)
        {
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    dt.Columns.Add("ATTRIBUTE_ID", typeof(int));
                    dt.Columns.Add("ROLE_ID", typeof(int));
                    foreach (DataRow dr in dt.Rows)
                    {
                        string attributeName = Convert.ToString(dr["ATTRIBUTE_NAME"]);
                        string roleName = Convert.ToString(dr["ROLE_NAME"]);
                        int aId = 0;
                        int rId = 0;
                        var attribute = _dbcontext.TB_ATTRIBUTE.Where(s => s.ATTRIBUTE_NAME.ToLower() == attributeName.ToLower()).FirstOrDefault();
                        var role = _dbcontext.TB_ROLE.Where(s => s.ROLE_NAME.ToLower() == roleName.ToLower()).FirstOrDefault();
                        if (role != null && role.ROLE_ID > 0)
                            rId = role.ROLE_ID;
                        if (attribute != null && attribute.ATTRIBUTE_ID > 0)
                            aId = attribute.ATTRIBUTE_ID;
                        dr["ATTRIBUTE_ID"] = aId;
                        dr["ROLE_ID"] = rId;
                    }

                }
                return dt;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController : RestoreDatabse", ex);
                return null;
            }
        }

        /// <summary>
        /// RoleAttribute
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public string RoleAttribute(DataTable dt, int customerId)
        {
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        int rId = 0;
                        rId = Convert.ToInt32(dr["ROLE_ID"]);
                        int attributeId = Convert.ToInt32(dr["ATTRIBUTE_ID"]);
                        string allowedit = Convert.ToString(dr["Allow Edit"]);
                        var customerAttribute = _dbcontext.CUSTOMERATTRIBUTE.Where(s => s.ATTRIBUTE_ID == attributeId && s.CUSTOMER_ID == customerId).FirstOrDefault();
                        var readOnly = _dbcontext.TB_READONLY_ATTRIBUTES.Where(s => s.ATTRIBUTE_ID == attributeId && s.ROLE_ID == rId).FirstOrDefault();
                        if (allowedit == "Yes")
                        {
                            if (customerAttribute == null)
                            {
                                var objcustomerattribute = new CUSTOMERATTRIBUTE
                                {
                                    CUSTOMER_ID = customerId,
                                    ATTRIBUTE_ID = attributeId,
                                    MODIFIED_USER = User.Identity.Name,
                                    CREATED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now,
                                    CREATED_DATE = DateTime.Now
                                };
                                _dbcontext.CUSTOMERATTRIBUTE.Add(objcustomerattribute);
                                _dbcontext.SaveChanges();
                            }
                        }
                        else if (allowedit == "false")
                        {
                            if (readOnly == null)
                            {
                                var readonlyattributes = new TB_READONLY_ATTRIBUTES
                                {
                                    ATTRIBUTE_ID = attributeId,
                                    ROLE_ID = rId,
                                    CREATED_USER = User.Identity.Name,
                                    CREATED_DATE = DateTime.Now,
                                    MODIFIED_DATE = DateTime.Now,
                                    MODIFIED_USER = User.Identity.Name
                                };
                                _dbcontext.TB_READONLY_ATTRIBUTES.Add(readonlyattributes);
                                _dbcontext.SaveChanges();
                            }
                        }
                    }
                }
                return "Saved";
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CatalogController : RestoreDatabse", ex);
                return "Action not successful";
            }
        }

        /// <summary>
        /// RestoreInDesignDetails
        /// </summary>
        /// <param name="importTemp"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string RestoreInDesignDetails(string importTemp, DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("PROJECT_ID"))
                {
                  dt.Columns.Remove("PROJECT_ID");
                }
                if (dt.Columns.Contains("DATA_FILE_ID"))
                {
                    dt.Columns.Remove("DATA_FILE_ID");
                }
                string action = string.Empty;
                int totalRows = dt.Rows.Count; int index; string indexOfSubCat = string.Empty; string subcategorySplit = string.Empty;
                int subCatCount = 0;
                if (!dt.Columns.Contains("CATALOG_ID"))
                {
                    dt.Columns.Add("CATALOG_ID");
                    dt.Columns["CATALOG_ID"].SetOrdinal(dt.Columns.IndexOf("CATALOG_NAME"));
                }
                if (!dt.Columns.Contains("CATEGORY_ID"))
                {
                    dt.Columns.Add("CATEGORY_ID");
                    dt.Columns["CATEGORY_ID"].SetOrdinal(dt.Columns.IndexOf("CATEGORY_NAME"));
                }
                if (!dt.Columns.Contains("FAMILY_ID"))
                {
                    dt.Columns.Add("FAMILY_ID");
                    dt.Columns["FAMILY_ID"].SetOrdinal(dt.Columns.IndexOf("FAMILY_NAME"));
                }
                if (dt.Rows.Count > 0)
                {
                    if (dt.Columns.Contains("FAMILY_ID"))
                    {
                        index = dt.Columns["FAMILY_ID"].Ordinal;
                        indexOfSubCat = dt.Columns[index - 1].ToString();
                    }
                    else if (dt.Columns.Contains("FAMILY_NAME"))
                    {
                        index = dt.Columns["FAMILY_NAME"].Ordinal;
                        indexOfSubCat = dt.Columns[index - 1].ToString();
                    }
                    if (indexOfSubCat.Contains("SUBCAT"))
                    {
                        subcategorySplit = indexOfSubCat.Split('_')[1];
                        subCatCount = Convert.ToInt32(subcategorySplit[1].ToString());
                    }
                    if (subCatCount > 0)
                    {
                        for (int i = 1; i <= subCatCount; i++)
                        {
                            if (!dt.Columns.Contains("SUBCATID_L" + i))
                            {
                                index = dt.Columns["SUBCATNAME_L" + i].Ordinal;
                                dt.Columns.Add("SUBCATID_L" + i).SetOrdinal(index);
                            }
                        }
                    }
                }
                if (!dt.Columns.Contains("PROJECT_ID"))
                {
                    dt.Columns.Add("PROJECT_ID");
                    dt.Columns["PROJECT_ID"].SetOrdinal(dt.Columns.IndexOf("PROJECT_NAME"));
                }
                if (!dt.Columns.Contains("DATA_FILE_ID"))
                {
                    dt.Columns.Add("DATA_FILE_ID");
                    dt.Columns["DATA_FILE_ID"].SetOrdinal(dt.Columns.IndexOf("DATA_FILE_NAME"));
                }

                for (int i = 0; i < totalRows; i++)
                {
                    int catalogId = 0; int familyId = 0; string categoryId = ""; int projectId = 0; int recordId = 0;
                    if (dt.Rows[i]["CATALOG_ID"] is DBNull)
                    {
                        string catalogName = dt.Rows[i]["CATALOG_NAME"].ToString();
                        catalogId = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_NAME == catalogName).Select(y => y.CATALOG_ID).FirstOrDefault();
                        if (catalogId != null)
                        {
                            dt.Rows[i]["CATALOG_ID"] = catalogId;
                        }
                    }
                    if (dt.Rows[i]["CATEGORY_ID"] is DBNull)
                    {
                        string categoryName = dt.Rows[i]["CATEGORY_NAME"].ToString();
                        catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                        categoryId = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tcs => tcs.CATEGORY_ID, tc => tc.CATEGORY_ID, (tcs, tc) => new { tcs, tc }).Where(x => x.tcs.CATEGORY_NAME == categoryName && x.tc.CATEGORY_ID == x.tcs.CATEGORY_ID && x.tc.CATALOG_ID == catalogId).Select(y => y.tcs.CATEGORY_SHORT).FirstOrDefault();
                        if (categoryId != null)
                        {
                            dt.Rows[i]["CATEGORY_ID"] = categoryId;
                        }
                    }
                    int count = subCatCount;
                    if (dt.Columns.Contains("SUBCATID_L" + count) || dt.Columns.Contains("SUBCATNAME_L" + count))
                    {
                        int subCatFamilyCheck = 1; string cat_Id = string.Empty;
                        string categoryName = string.Empty; string categoryShort = string.Empty;
                        while (subCatFamilyCheck <= count)
                        {
                            if (dt.Rows[i]["SUBCATID_L" + count] is DBNull)
                            {
                                if (!(dt.Rows[i]["SUBCATNAME_L" + count] is DBNull))
                                {
                                    categoryName = dt.Rows[i]["SUBCATNAME_L" + count].ToString();
                                    cat_Id = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tc => tc.CATEGORY_ID, tcs => tcs.CATEGORY_ID, (tc, tcs) => new { tc, tcs }).Where(x => x.tc.CATEGORY_NAME == categoryName && x.tcs.CATALOG_ID == catalogId).Select(y => y.tc.CATEGORY_ID).FirstOrDefault();

                                    categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_NAME == categoryName && x.CATEGORY_ID == cat_Id).Select(y => y.CATEGORY_SHORT).FirstOrDefault();
                                    if (categoryShort != null)
                                    {
                                        dt.Rows[i]["SUBCATID_L" + count] = categoryShort;
                                        categoryId = dt.Rows[i]["SUBCATID_L" + count].ToString();
                                    }
                                }
                            }
                            count = count - 1;
                        }
                    }
                    if (dt.Rows[i]["FAMILY_ID"] is DBNull)
                    {
                        string familyName = dt.Rows[i]["FAMILY_NAME"].ToString();
                        catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                        string categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == categoryId).Select(y => y.CATEGORY_ID).FirstOrDefault();
                        familyId = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tf => tf.FAMILY_ID, tcf => tcf.FAMILY_ID, (tf, tcf) => new { tf, tcf }).Where(x => x.tcf.FAMILY_NAME == familyName && x.tf.CATALOG_ID == catalogId && x.tf.CATEGORY_ID == categoryShort).Select(y => y.tcf.FAMILY_ID).FirstOrDefault();
                        if (familyId != null)
                        {
                            dt.Rows[i]["FAMILY_ID"] = familyId;
                        }
                    }
                    if (dt.Columns.Contains("PROJECT_ID") || dt.Columns.Contains("PROJECT_NAME"))
                    {
                        if (dt.Rows[i]["PROJECT_ID"] is DBNull)
                        {
                            string projectName = dt.Rows[i]["PROJECT_NAME"].ToString();
                            catalogId = Convert.ToInt32(dt.Rows[i]["CATALOG_ID"]);
                            projectId = _dbcontext.TB_PROJECT.Where(x => x.PROJECT_NAME == projectName && x.CATALOG_ID == catalogId).Select(y => y.PROJECT_ID).FirstOrDefault();
                            if (projectId != null)
                            {
                                dt.Rows[i]["PROJECT_ID"] = projectId;
                            }
                            else
                            {
                                dt.Rows[i]["PROJECT_ID"] = 0;
                            }

                        }

                        if (dt.Columns.Contains("DATA_FILE_ID") || dt.Columns.Contains("DATA_FILE_NAME"))
                        {
                            if (dt.Rows[i]["DATA_FILE_ID"] is DBNull)
                            {
                                string xmlFileName = string.Empty;
                                xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                                var xml = xmlFileName.Split('.');
                                string extenstion = string.Empty;
                                bool file_Name = false;
                                if (xml != null && xml.Count() >= 2)
                                {
                                    extenstion = xml[1];
                                }
                                if (extenstion == "xml")
                                {
                                    xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                                    file_Name = true;
                                }
                                else
                                {
                                    xmlFileName = dt.Rows[i]["DATA_FILE_NAME"].ToString();
                                }
                                projectId = Convert.ToInt32(dt.Rows[i]["PROJECT_ID"]);
                                recordId = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.XML_FILE_NAME == xmlFileName && x.PROJECT_ID == projectId).Select(y => y.RECORD_ID).FirstOrDefault();
                                //if (!file_Name)
                                //{
                                //    dt.Rows[i]["DATA_FILE_NAME"] = dt.Rows[i]["DATA_FILE_NAME"].ToString() + ".xml";
                                //}
                                if (recordId > 0)
                                {
                                    dt.Rows[i]["DATA_FILE_ID"] = recordId;
                                }
                                else
                                {
                                    dt.Rows[i]["DATA_FILE_ID"] = 0;
                                }
                            }
                        }
                    }
                }

                string query = string.Empty;
                DataSet resultSet1 = new DataSet();
                using (
                      var objSqlConnection =
                          new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    query =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    query = "IF OBJECT_ID('TEMPDB..##IMPORTTEMP" + importTemp + "') IS NOT NULL   DROP TABLE  [##IMPORTTEMP" + importTemp + "]";
                    SqlCommand _DBCommandnew = new SqlCommand(query, objSqlConnection);
                    _DBCommandnew.ExecuteNonQuery();
                    query = CreateTable("[##IMPORTTEMP" + importTemp + "]", dt);
                    _DBCommandnew = new SqlCommand(query, objSqlConnection);
                    _DBCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(dt);
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESGINIMPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.AddWithValue("@SID", importTemp);
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    sqlDataAdapter.Fill(resultSet1);
                    objSqlConnection.Close();
                    if (resultSet1.Tables[0].Rows.Count > 0)
                    {
                        return "Import Failed";
                    }
                    else
                    {
                        return "Import success";
                    }
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Subquery"))
                {
                    return "Subquery";
                }
                else
                {
                    Logger.Error("Error at CatalogController : RestoreDatabse", ex);
                    return "Action not Completed";
                }
            }

        }

        /// <summary>
        /// GetDataFromExcelForRestore
        /// </summary>
        /// <param name="SheetName"></param>
        /// <param name="excelPath"></param>
        /// <param name="query"></param>
        /// <param name="deleteflag"></param>
        /// <returns></returns>
        public DataSet GetDataFromExcelForRestore(string SheetName, string excelPath, string query, string deleteflag)
        {
            DataSet restoreDS = new DataSet();
            DataTable dtexcel = new DataTable();
            DataTable dtexcelNew = new DataTable();
            try
            {
                var fileName = Path.GetFileName(excelPath).Split('.')[0];
                var path = Path.GetFullPath(excelPath);
                var fileExtention = Path.GetExtension(excelPath);
                string excelPathCopy = path.Replace(Path.GetFileName(excelPath), "") + "//" + fileName + "_Copy" + "." + fileExtention;
                using (System.Data.OleDb.OleDbConnection conn = excelConnection(excelPath))
                {
                    DataTable schemaTable = conn.GetOleDbSchemaTable(System.Data.OleDb.OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    foreach (DataRow dtRow in schemaTable.Rows)
                    {
                        dtexcel = new DataTable();
                        string ExcelSheetName = dtRow["Table_Name"].ToString();
                        DataTable dtColumns = new DataTable();
                        SheetName = ExcelSheetName;
                        SheetName = SheetName.Replace("$", "");
                        try
                        {
                            DataRow[] schemaRow = schemaTable.Select("TABLE_NAME like '" + SheetName + "%'");
                            string sheet = schemaRow[0]["TABLE_NAME"].ToString();
                            int selectedIndex = 0;
                            foreach (DataRow dRow in schemaTable.Rows)
                            {
                                if (sheet.ToUpper() == dRow["TABLE_NAME"].ToString().ToUpper())
                                    break;
                                selectedIndex++;
                            }
                            query = " SELECT * from [" + SheetName + "$]";

                            if (!sheet.EndsWith("_"))
                            {
                                System.Data.OleDb.OleDbDataAdapter daexcel = new System.Data.OleDb.OleDbDataAdapter(query, conn);
                                dtexcel.Locale = System.Globalization.CultureInfo.CurrentCulture;
                                daexcel.Fill(dtColumns);
                            }

                            var workbook1 = new Workbook();
                            workbook1 = Workbook.Load(excelPath);
                            dtexcel = dtColumns.Clone();
                            dtexcel = new DataTable();
                            dtexcel = dtColumns;
                            workbook1.Worksheets.Clear();
                        }
                        catch (Exception ex)
                        {
                            dtexcel = new DataTable();
                            dtexcel = dtColumns;
                        }
                        dtexcel.TableName = SheetName;
                        restoreDS.Tables.Add(dtexcel);

                    }
                    conn.Close();
                    conn.Dispose();
                }
                return restoreDS;
            }
            catch (Exception ex)
            {
                restoreDS = new DataSet();
                return restoreDS;
            }
        }

        /// <summary>
        /// DeleteDatabase
        /// </summary>
        /// <param name="fullPathName"></param>
        /// <param name="ConnectionString"></param>
        public void DeletedDatabase()
        {
            try
            {
                var customers = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                string dataSource = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.DataSource).FirstOrDefault();
                string userID = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.UserID).FirstOrDefault();
                string password = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.Password).FirstOrDefault();
                string databaseName = string.Empty;
                if (System.Web.HttpContext.Current.Session["DatabaseName"] != null)
                    databaseName = (string)System.Web.HttpContext.Current.Session["DatabaseName"];
                if (databaseName != null && databaseName != "")
                {
                    string strConString = @"Data Source= " + dataSource + " ;Initial Catalog=" + databaseName + ";User ID=" + userID + ";Password=" + password + ";Integrated Security=True;MultipleActiveResultSets=True;Connection Timeout=0";
                   
                  
                    string databaseBackUpPath = _dbcontext.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.RestoreDestPath).FirstOrDefault();
                    //string databaseBackUpPath = ConfigurationManager.AppSettings["RestoreDestPath"];
                    string bak_StoreLocalPath = string.Format(@"{0}{1}.bak", databaseBackUpPath, databaseName);
                    string sDataBase = string.Format(@"{0}[{1}].mdf", databaseBackUpPath, databaseName);
                    string sDataBaseLog = string.Format(@"{0}[{1}].ldf", databaseBackUpPath, databaseName);
                    using (SqlConnection conn = new SqlConnection(strConString))
                    {
                        conn.Open();
                        var AlterStr = "ALTER DATABASE [" + databaseName + " ] SET OFFLINE WITH ROLLBACK IMMEDIATE";
                        var AlterCmd = new SqlCommand(AlterStr, conn);
                        var DropStr = "DROP DATABASE [" + databaseName + "] ";
                        var DropCmd = new SqlCommand(DropStr, conn);
                        AlterCmd.ExecuteNonQuery();
                        DropCmd.ExecuteNonQuery();
                    }

                    if ((System.IO.File.Exists(bak_StoreLocalPath)))
                    {
                        System.IO.File.Delete(bak_StoreLocalPath);
                    }
                    if ((System.IO.File.Exists(sDataBase)))
                    {
                        System.IO.File.Delete(sDataBase);
                    }
                    if ((System.IO.File.Exists(sDataBaseLog)))
                    {
                        System.IO.File.Delete(sDataBaseLog);
                    }
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at CatalogController : DeletedFiles", objexception);
            }
        }
        #endregion


        public string Getdefaultfamilyexists(int catalogId)
        {
            try
            {
                var DF_ID = _dbcontext.TB_CATALOG
                         .Join(_dbcontext.TB_CATALOG_FAMILY, tc => tc.CATALOG_ID, tcf => tcf.CATALOG_ID, (tc, tcf) => new { tc, tcf })
                         .Join(_dbcontext.TB_FAMILY, tftcf => tftcf.tcf.FAMILY_ID, tf => tf.FAMILY_ID, (tftcf, tf) => new { tftcf, tf })
                         .Where(x => x.tftcf.tcf.FLAG_RECYCLE == "A" && x.tftcf.tc.CATALOG_ID == catalogId).Select(x => x.tftcf.tc.DEFAULT_FAMILY).First();
                int Default_FamilyId = Convert.ToInt32(DF_ID);

                var Default_FamilyName = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID == Default_FamilyId && x.FLAG_RECYCLE == "A").Select(y => y.FAMILY_NAME).FirstOrDefault().ToString();

                return Default_FamilyName;
               
            }
            catch (Exception objException)
            {
                Logger.Error("Error at CatalogController : Getdefaultfamilyexists", objException);
                return null;
            }
        }

        public string getdefaultfamily(int catalogId)
        {
            try
            {
                var DF_ID = _dbcontext.TB_CATALOG.Where(s => s.CATALOG_ID == catalogId).Select(y => y.DEFAULT_FAMILY).FirstOrDefault().ToString();
                int Default_FamilyId = Convert.ToInt32(DF_ID);

                var Default_FamilyName = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID == Default_FamilyId && x.FLAG_RECYCLE == "A").Select(y => y.FAMILY_NAME).FirstOrDefault().ToString();

                return Default_FamilyName;

            }
            catch (Exception objException)
            {
                Logger.Error("Error at CatalogController : GetCatalog", objException);
                return null;
            }
        }
        public int GetProductExists(int productId,int catalogId)
        {
            try
            {
                var DF_ID = _dbcontext.TB_CATALOG.Where(s => s.CATALOG_ID == catalogId).Select(y => y.DEFAULT_FAMILY).FirstOrDefault().ToString();
                int Default_FamilyId = Convert.ToInt32(DF_ID);


                var Count = _dbcontext.TB_PROD_FAMILY.Where(x => x.FAMILY_ID != Default_FamilyId && x.PRODUCT_ID == productId && x.FLAG_RECYCLE == "A").Count();
                return Count;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at CatalogController : GetCatalog", objException);
                return 0;
            }
        }

    }
}