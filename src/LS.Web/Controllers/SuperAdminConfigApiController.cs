﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kendo.DynamicLinq;
using LS.Data;
using log4net;
using System.Web.Http;
using LS.Data.Model;
using LS.Data.Model.Accounts;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Web.Mvc.Html;
using System.Net.Http;
using System.Net;
namespace LS.Web.Controllers
{
    public class SuperAdminConfigApiController : ApiController
    {
        // GET: SuperAdminConfigApiController
        static ILog _logger = LogManager.GetLogger(typeof(SuperAdminConfigApiController));
        
        /// <summary>
        /// Gets List of Plans
        /// </summary>
        /// <param name="request">Request Object from Kendo Grid</param>
        /// <returns>DataSourceResult for Kendo Grid</returns>
        [System.Web.Http.HttpPost]
        public DataSourceResult GetPlanList(DataSourceRequest request)
        {
            try
            {
                _logger.Info("Inside  at SuperAdminConfigApiController : GetPlanList");
                if (request.Sort == null || !request.Sort.Any())
                {
                    request.Sort = new List<Sort>() { new Sort() { Field = "Plan_Id", Dir = "asc" } };
                }
                using (var dbcontext = new CSEntities())
                {
                    DataSourceResult result = (from x in dbcontext.TB_PLAN
                                               where x.PLAN_NAME != "" && x.PLAN_NAME != "One Month subscribe"
                                               select x).ToDataSourceResult<TB_PLAN>(request);

                    result.Data = ((List<TB_PLAN>)result.Data).Select(PlanModel.GetModel);
                    return result;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at SuperAdminConfigApiController : GetPlanList", ex);
            }
            return null;
        }

        /// <summary>
        /// Gets List of Plans
        /// </summary>
        /// <returns>DataSourceResult for Kendo Grid</returns>
        [System.Web.Http.HttpGet]
        public List<PlanModel> GetListOfPlans(int planMode)
        {
            try
            {
                _logger.Info("Inside  at SuperAdminConfigApiController : GetListOfPlans");
                using (var dbcontext = new CSEntities())
                {

                    if (planMode == 0)
                    {
                        var planDetails = dbcontext.TB_PLAN.Where(x => x.IS_ACTIVE == true)
                                                    .Select(x => new PlanModel
                                                    {
                                                        PLAN_ID = x.PLAN_ID,
                                                        PLAN_NAME = x.PLAN_NAME
                                                    }).ToList();
                        return planDetails;
                        
                    }
                    else
                    {
                        //return (from x in dbcontext.TB_PLAN select x).ToList().Select(i => new PlanModel { PLAN_ID = i.PLAN_ID, PLAN_NAME = i.PLAN_NAME }).Where(x => x.PLAN_NAME != "").ToList();
                        var planDetails=(from x in dbcontext.TB_PLAN select x).ToList().Select(i => new PlanModel { PLAN_ID = i.PLAN_ID, PLAN_NAME = i.PLAN_NAME }).Where(x => x.PLAN_NAME != "").ToList();
                        return planDetails;
                    } 
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at SuperAdminConfigApiController : GetListOfPlans", ex);
            }
            return null;
        }

         [System.Web.Http.HttpGet]
        public List<CountryVModel> GetListOfCountries()
        {
            try
            {
                // _logger.Info("Inside  at SuperAdminConfigApiController : GetListOfCountries");
                using (var dbcontext = new CSEntities())
                {
                    return (from x in dbcontext.TB_COUNTRY select x).ToList().Select(i => new CountryVModel { COUNTRY_CODE = i.COUNTRY_CODE, COUNTRY = i.COUNTRY }).OrderBy(x=>x.COUNTRY).ToList();
                }
            }
            catch (Exception ex)
            {
                
                _logger.Error("Error at SuperAdminConfigApiController :GetListOfCountries ", ex);
            }
            return null;
        }

         [System.Web.Http.HttpGet]
         public List<StateModel> GetListOfStates(string countryCode)
         {
             try
             {
                 //_logger.Info("Inside at SuperAdminConfigApiController : GetListOfStates");
                 using (var dbcontext = new CSEntities())
                 {
                     var country_code="";
                     string [] country = countryCode.Split('~');
                     foreach (var item in country)
                     {
                         if (item=="")
                         {
                             
                         }
                         else
                         {
                             country_code = item;
                         }
                     }
                     if (countryCode.Contains('~'))
                     {
                         var _countryCode = dbcontext.TB_COUNTRY.Where(v => v.COUNTRY == country_code).Select(b => b.COUNTRY_CODE).FirstOrDefault();
                         return (dbcontext.TB_STATE.Where(x => x.COUNTRY_CODE == _countryCode).Select(a => new StateModel { STATE = a.STATE, STATE_CODE = a.STATE_CODE }).OrderBy(x => x.STATE).ToList());
                     }
                     else
                     {

                         return (dbcontext.TB_STATE.Where(x => x.COUNTRY_CODE == countryCode).Select(a => new StateModel { STATE = a.STATE, STATE_CODE = a.STATE_CODE }).OrderBy(x => x.STATE).ToList());
                     }

                 }
             }
             catch (Exception ex)
             {

                 _logger.Error("Error at SuperAdminConfigApiController:GetListOfStates", ex);
             }
             return null;
         }

         [System.Web.Http.HttpGet]
         public List<StateModel> GetListOfStatesForSupplier(string countryCode)
         {
             try
             {
                 _logger.Info("Inside at SuperAdminConfigApiController : GetListOfStates");
                 using (var dbcontext = new CSEntities())
                 {
                     var country_code = "";
                     string[] country = countryCode.Split('~');
                     foreach (var item in country)
                     {
                         if (item == "")
                         {

                         }
                         else
                         {
                             country_code = item;
                         }
                     }
                     if (countryCode.Contains('~'))
                     {
                         var _countryCode = dbcontext.TB_COUNTRY.Where(v => v.COUNTRY == country_code).Select(b => b.COUNTRY_CODE).FirstOrDefault();
                         return (dbcontext.TB_STATE.Where(x => x.COUNTRY_CODE == _countryCode).Select(a => new StateModel { STATE = a.STATE, STATE_CODE = a.STATE_CODE }).OrderBy(x => x.STATE).ToList());
                     }
                     else
                     {
                         var _countryCode = dbcontext.TB_COUNTRY.Where(v => v.COUNTRY == country_code).Select(b => b.COUNTRY_CODE).FirstOrDefault();
                         return (dbcontext.TB_STATE.Where(x => x.COUNTRY_CODE == _countryCode).Select(a => new StateModel { STATE = a.STATE, STATE_CODE = a.STATE_CODE }).OrderBy(x => x.STATE).ToList());
                     }

                 }
             }
             catch (Exception ex)
             {

                 _logger.Error("Error at SuperAdminConfigApiController:GetListOfStates", ex);
             }
             return null;
         }
     

        [System.Web.Http.HttpPost]
        public IHttpActionResult SaveFunctionAllowedForPlan(object model, PlanModel planmodel)
        {
            try
            {
                using (var db = new CSEntities())
                {
                    var arr = (JArray)((JObject.Parse(model.ToString())).SelectToken("data")).SelectToken("models");
                    var functionAlloweditems = ((JArray)arr).Select(x => new TB_USER_FUNCTION_ALLOWED
                    {
                        FUNCTION_ID = (int)x["FUNCTION_ID"],
                        FUNCTION_NAME = (string)x["FUNCTION_NAME"],
                        ROLE_ID = (int)x["ROLE_ID"],
                        ACTION_VIEW = (bool)x["ACTION_VIEW"],
                        ACTION_MODIFY = (bool)x["ACTION_MODIFY"],
                        ACTION_ADD = (bool)x["ACTION_ADD"],
                        ACTION_REMOVE = (bool)x["ACTION_REMOVE"]
                    }).ToList();

                    foreach (var item in functionAlloweditems)
                    {
                        var objuserFunctionAllowed = db.TB_ROLE_FUNCTIONS.FirstOrDefault(x => x.FUNCTION_ID == item.FUNCTION_ID && x.ROLE_ID == item.ROLE_ID);
                        if (objuserFunctionAllowed != null)
                        {
                            objuserFunctionAllowed.ACTION_VIEW = Convert.ToBoolean(item.ACTION_VIEW);
                            objuserFunctionAllowed.ACTION_MODIFY = Convert.ToBoolean(item.ACTION_MODIFY);
                            objuserFunctionAllowed.ACTION_ADD = Convert.ToBoolean(item.ACTION_ADD);
                            objuserFunctionAllowed.ACTION_REMOVE = Convert.ToBoolean(item.ACTION_REMOVE);
                        }
                        db.SaveChanges();
                    }
                    return Ok();
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at SaveOrUpdateAppUser", ex);
            }
            return null;
        }

        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetCutomerPlan (int customerId, int planId)
        {
            try
            {
                using (var dbcontext = new CSEntities())
                {
                    var cusplan = dbcontext.Customers.Join(dbcontext.TB_PLAN,cus=>cus.PlanId,plan=>plan.PLAN_ID,(cus,plan)=>new{cus,plan})
                                                     .Where(x => x.cus.CustomerId == customerId)
                                                     .Select(i=>new
                                                     { 
                                                        PlanId= i.cus.PlanId,
                                                        IsActive= i.plan.IS_ACTIVE
                                                     
                                                     }).FirstOrDefault();
                    var cusplanStatus = dbcontext.TB_PLAN.Where(x => x.PLAN_ID == cusplan.PlanId).Select(i => i.IS_ACTIVE).FirstOrDefault();
                    var selectedplanStatus = dbcontext.TB_PLAN.Where(x => x.PLAN_ID ==planId).Select(i => new { i.IS_ACTIVE ,i.PLAN_ID}).FirstOrDefault();
                    //compare plan with cusotmers
                    if (selectedplanStatus.IS_ACTIVE == true && cusplan.PlanId == planId)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Update");
                    }
                    else if (selectedplanStatus.IS_ACTIVE == true && cusplan.PlanId != planId)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Update");
                    }
                    else if (selectedplanStatus.IS_ACTIVE != true && cusplan.PlanId == planId)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Update");
                    }
                    else if (selectedplanStatus.IS_ACTIVE != true && cusplan.PlanId != planId)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Disabled");
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, "Disabled");
            }
            catch (Exception ex)
            {

                _logger.Error("Error at SuperAdminConfigApiController:GetCutomerPlan", ex);
                return Request.CreateResponse(HttpStatusCode.OK, "The Plan you have selected is disabled, please select an active Plan");
            }
            
        }
    }
}
