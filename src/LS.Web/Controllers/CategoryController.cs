﻿using System;
using System.Linq;
using System.Web.Mvc;
using log4net;
using LS.Data;
using LS.Web.Utility;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;
using System.Web;
using System.IO;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Stimulsoft.Report.Export;
using LS.Models;
using System.Drawing;
using LS.Data.Model.ProductPreview;
namespace LS.Web.Controllers
{
    public class CategoryController : Controller
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        readonly CSEntities _objCSEntities = new CSEntities();
        static readonly ILog Logger = LogManager.GetLogger(typeof(HomeApiController));
        public string SQLString = string.Empty;
        public string CustomerFolder = string.Empty;   
        private static readonly char[] SpecialChars = "'#&".ToCharArray();
        public string convertionPath = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePath"].ToString();
        public string convertionPathREA = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePathREA"].ToString();
        public string converttype = string.Empty;
        public string Exefile = string.Empty;

        public JsonResult GetCategory(string categoryId)
        {
            var famCheck = 0;
            var categoryCheck = "";
            try
            {

                // To get Least level category For Preview

                string leastLevelCategoryId = categoryId;

                Check:
                categoryCheck = _objCSEntities.TB_CATEGORY.Where(x => x.PARENT_CATEGORY == leastLevelCategoryId && x.FLAG_RECYCLE == "A").Select(x => x.CATEGORY_ID).FirstOrDefault();
                if (categoryCheck != null)
                {
                    famCheck = _objCSEntities.TB_CATALOG_FAMILY.Where(x => x.CATEGORY_ID == categoryCheck).Select(x => x.FAMILY_ID).FirstOrDefault();

                    if (famCheck != 0)
                    {
                        leastLevelCategoryId = categoryCheck.ToString();
                        goto Check;
                    }
                    else if (famCheck == 0)
                    {
                        famCheck = _objCSEntities.TB_CATALOG_FAMILY.Where(x => x.CATEGORY_ID == categoryId).Select(x => x.FAMILY_ID).FirstOrDefault();
                        if (famCheck != 0)
                        {
                            leastLevelCategoryId = categoryCheck.ToString();
                            goto Check;
                        }
                        else if (famCheck == 0)
                        {
                            categoryCheck = _objCSEntities.TB_CATEGORY.Where(x => x.PARENT_CATEGORY == categoryCheck && x.FLAG_RECYCLE == "A").Select(x => x.CATEGORY_ID).FirstOrDefault();
                            famCheck = _objCSEntities.TB_CATALOG_FAMILY.Where(x => x.CATEGORY_ID == categoryCheck).Select(x => x.FAMILY_ID).FirstOrDefault();

                                if (famCheck != 0)
                                {
                                    leastLevelCategoryId = categoryCheck.ToString();
                                    goto Check;
                                }
                                else
                            {
                                categoryCheck = _objCSEntities.TB_CATEGORY.Where(x => x.PARENT_CATEGORY == categoryCheck && x.FLAG_RECYCLE == "A").Select(x => x.CATEGORY_ID).FirstOrDefault();
                                famCheck = _objCSEntities.TB_CATALOG_FAMILY.Where(x => x.CATEGORY_ID == categoryCheck).Select(x => x.FAMILY_ID).FirstOrDefault();
                                if (famCheck != 0)
                                {
                                    leastLevelCategoryId = categoryCheck.ToString();
                                    goto Check;
                                }
                            }
                            }
                        }
                    }
                

                // End


                CustomerFolder = _objCSEntities.Customers.Join(_objCSEntities.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }

                string _imageMagickPath = "";
                int userID = 0;
                var customer_det = _objCSEntities.Customer_User.FirstOrDefault(a => a.User_Name == User.Identity.Name);
                if (customer_det != null)
                {
                    userID = customer_det.CustomerId;
                }
                var imagemagickpath = _objCSEntities.Customer_Settings.FirstOrDefault(a => a.CustomerId == userID);
                //if (imagemagickpath != null)
                //{
                //    _imageMagickPath = imagemagickpath.ImageConversionUtilityPath;
                //    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");
                //}
                //else
                //{
                //    _imageMagickPath = "C:\\Program Files\\ImageMagick-6.8.5-Q8";
                //}
                string converttype = _objCSEntities.Customer_Settings.Where(a => a.CustomerId == userID).Select(a => a.ConverterType).SingleOrDefault();

                if (imagemagickpath != null && converttype == "Image Magic")
                {
                    _imageMagickPath = convertionPath;
                    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");
                }
                else
                {
                    _imageMagickPath = convertionPathREA;
                    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");

                }
                var ImageConversion = new FamilyPreviews();
                if (categoryId.Contains("~")) return null;

                string CloneCheck = string.Empty;
                if (categoryId.StartsWith("CLONE"))
                {
                    CloneCheck = "CLONE-" + categoryId.Replace("[", "" + categoryId + "");
                }
                else
                {
                    CloneCheck = categoryId;
                }

                var ImageDetails = _objCSEntities.TB_CATEGORY.FirstOrDefault(s => s.CATEGORY_ID == categoryId);
                string IMAGE_FILE = string.Empty;
                string IMAGE_FILE2 = string.Empty;
                if (ImageDetails != null)
                {
                    if (!string.IsNullOrEmpty(ImageDetails.IMAGE_FILE))
                        IMAGE_FILE = ImageConversion.ImageConversionForAll(ImageDetails.IMAGE_FILE, ImageDetails.CATEGORY_ID, 0, _imageMagickPath, CustomerFolder);

                    if (!string.IsNullOrEmpty(ImageDetails.IMAGE_FILE2))
                        IMAGE_FILE2 = ImageConversion.ImageConversionForAll(ImageDetails.IMAGE_FILE2, ImageDetails.CATEGORY_ID, 0, _imageMagickPath, CustomerFolder);
                }
                var categoryList = _objCSEntities.TB_CATEGORY.Where(x => x.CATEGORY_ID == categoryId).Select(y => y.CATEGORY_SHORT).FirstOrDefault();
                string categoryShort = categoryId;
                if (categoryList != null)
                    categoryShort = categoryList.ToString();
                int Family_ID = 0;
                var categoryDetails = _objCSEntities.TB_CATEGORY.Where(s => s.CATEGORY_ID == categoryId).Select(x => new
                {
                    x.CATEGORY_ID,
                    x.CATEGORY_NAME,
                    x.PARENT_CATEGORY,
                    x.PUBLISH2WEB,
                    x.PUBLISH2PRINT,
                    x.PUBLISH2PDF,
                    x.PUBLISH2EXPORT,
                    x.PUBLISH2PORTAL,
                    x.SHORT_DESC,
                    x.CUSTOM_NUM_FIELD1,
                    x.CUSTOM_NUM_FIELD2,
                    x.CUSTOM_NUM_FIELD3,
                    x.CUSTOM_TEXT_FIELD1,
                    x.CUSTOM_TEXT_FIELD2,
                    x.CUSTOM_TEXT_FIELD3,
                    IMAGE_FILE = IMAGE_FILE,
                    IMAGE_FILE2 = IMAGE_FILE2,
                    x.IMAGE_NAME,
                    x.IMAGE_NAME2,
                    x.IMAGE_TYPE,
                    x.IMAGE_TYPE2,
                    x.WORKFLOW_STATUS,
                    STATUS_NAME = _objCSEntities.TB_WORKFLOW_STATUS.FirstOrDefault(z => z.STATUS_CODE == x.WORKFLOW_STATUS).STATUS_NAME,
                    PARENT_CATEGORYNAME = x.PARENT_CATEGORY == "0" ? "Root" : _objCSEntities.TB_CATEGORY.FirstOrDefault(y => y.CATEGORY_ID == x.PARENT_CATEGORY).CATEGORY_NAME,
                    IS_CLONE = (_objCSEntities.TB_CATEGORY.Where(a => a.CATEGORY_SHORT.Contains("CLONE") && a.CATEGORY_SHORT.Contains("-" + categoryShort + "")).Count()) > 0 ? true : false,
                    x.CLONE_LOCK,
                    x.CATEGORY_SHORT,
                    x.CATEGORY_PARENT_SHORT,
                    x.CUSTOMER_ID,
                    Family_ID = _objCSEntities.TB_FAMILY.Where(Z => Z.CATEGORY_ID == leastLevelCategoryId && Z.FLAG_RECYCLE == "A").Select(D => D.FAMILY_ID).Count()
                }).ToList();
                if (categoryDetails.Count == 0)
                    return null;
                return Json(categoryDetails.ElementAt(0), JsonRequestBehavior.AllowGet);
            }
            catch (Exception objException)
            {
                Logger.Error("Error at CategoryController : GetCatalogAssociation", objException);
                return null;
            }

        }

        public JsonResult GetCatalogAssociation(string categoryId)
        {
            try
            {
                if (!string.IsNullOrEmpty(categoryId))
                {
                    var catalogAssociation = from cs in _objCSEntities.TB_CATALOG_SECTIONS
                                             join c in _objCSEntities.TB_CATALOG on new { a = cs.CATALOG_ID, b = cs.CATEGORY_ID } equals new { a = c.CATALOG_ID, b = categoryId }
                                             select new { cs.CATALOG_ID, c.CATALOG_NAME, cs.CATEGORY_ID };

                    return Json(catalogAssociation, JsonRequestBehavior.AllowGet);
                }
                return null;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at CategoryController : GetCatalogAssociation", objException);
                return null;
            }

        }

        static long DirectorySize(DirectoryInfo dInfo, bool includeSubDir)
        {
            long totalSize = dInfo.EnumerateFiles()
                         .Sum(file => file.Length);
            if (includeSubDir)
            {
                totalSize += dInfo.EnumerateDirectories()
                         .Sum(dir => DirectorySize(dir, true));
            }
            return totalSize;
        }

        public ActionResult SaveImage(IEnumerable<HttpPostedFileBase> files, IEnumerable<TextReader> id)
        {
            try
            {

                var SpaceProvided = _objCSEntities.TB_PLAN
                          .Join(_objCSEntities.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                          .Join(_objCSEntities.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                          .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => x.tcptps.tps.StorageGB).ToList();
                double alotment = (double)SpaceProvided[0];
                var userId = _objCSEntities.Customer_User.Join(_objCSEntities.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();
                double folderSize = 0;
                if (string.IsNullOrEmpty(userId))
                {
                    userId = "Questudio";
                }

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + ""));
                }
                var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "");
                DirectoryInfo dInfo = new DirectoryInfo(path);
                long sizeOfDir = DirectorySize(dInfo, true);
                folderSize = (((double)sizeOfDir) / (double)(1024 * 1024 * 1024));

                double availableSpace = alotment - folderSize;
                if (availableSpace > 0)
                {
                    CustomerFolder = _objCSEntities.Customers.Join(_objCSEntities.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
                    if (string.IsNullOrEmpty(CustomerFolder))
                    { CustomerFolder = "/STUDIOSOFT"; }
                    else
                    { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }

                    if (files == null) return Content("");
                    foreach (var file in files)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        if (fileName != null && fileName.IndexOfAny(SpecialChars) == -1)
                        {
                            string physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages" + CustomerFolder + "/Images"), fileName);
                            file.SaveAs(physicalPath);
                        }
                    }

                }
                return Content("");
            }
            catch (Exception objException)
            {
                Logger.Error("Error at CategoryController : SaveImage", objException);
                return null;
            }
        }


        public ActionResult RemoveImage(string[] fileNames)
        {
            CustomerFolder = _objCSEntities.Customers.Join(_objCSEntities.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
            if (string.IsNullOrEmpty(CustomerFolder))
            { CustomerFolder = "/STUDIOSOFT"; }
            else
            { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }

            if (fileNames != null)
            {
                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    if (fileName != null)
                    {
                        var physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages" + CustomerFolder + "/Images"), fileName);
                        if (System.IO.File.Exists(physicalPath))
                        {
                            System.IO.File.Delete(physicalPath);
                        }
                    }
                }
            }
            return Content("");
        }

        public bool CutPasteCategory(string cutCatId, string pasteCatId, int catalogId, int previousCatalogID, string multipleFamilyids, string multipleFamilyidsForAnotherCatalog)
        {
            try
            {
                if (multipleFamilyids != null && multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog != null && multipleFamilyidsForAnotherCatalog.Trim() != "")
                {
                    //same catalog
                    string[] multipleCategoryIds;
                    if (catalogId == previousCatalogID)
                    {
                        if (multipleFamilyids == null || multipleFamilyids.Trim() == "")
                        {
                            multipleCategoryIds = multipleFamilyidsForAnotherCatalog.Split(',');
                        }
                        else
                        {
                            multipleCategoryIds = multipleFamilyids.Split(',');
                        }
                        var rootCategory = new List<string>(multipleCategoryIds);
                        var subCategory = new List<string>();
                        var subcategoryCheck = new List<string>();


                        foreach (string categoryID in multipleCategoryIds)
                        {
                            if (!categoryID.Contains("~"))
                            {
                                string id = categoryID;
                                var categoryCheck = _objCSEntities.TB_CATEGORY.FirstOrDefault(a => a.CATEGORY_ID == id && a.PARENT_CATEGORY == "0");
                                if (categoryCheck == null)
                                {
                                    var subCategoryCheck = _objCSEntities.TB_CATEGORY.Where(a => a.PARENT_CATEGORY == categoryID);

                                    if (subCategoryCheck.Any())
                                    {
                                        subCategory.Add(categoryID);
                                        subcategoryCheck.AddRange(subCategoryCheck.Select(item => item.CATEGORY_ID));
                                    }
                                    else
                                    {
                                        subCategory.Add(categoryID);
                                    }

                                }
                                else
                                {
                                    rootCategory.Remove(categoryID);
                                }
                            }
                        }

                        if (subCategory != null)
                        {
                            foreach (var item in subcategoryCheck)
                            {
                                subCategory.Remove(item);
                            }
                            multipleCategoryIds = subCategory.ToArray();
                        }
                        else
                        {
                            multipleCategoryIds = rootCategory.ToArray();
                        }
                    }
                    else
                    {
                        //other catalog
                        multipleCategoryIds = multipleFamilyidsForAnotherCatalog.Split(',');
                        var rootCategory = new List<string>(multipleCategoryIds);
                        var subCategory = new List<string>();
                        var subcategoryCheck = new List<string>();
                        foreach (string categoryID in multipleCategoryIds)
                        {
                            if (categoryID.Contains("~")) continue;
                            string id = categoryID;
                            var categoryCheck = _objCSEntities.TB_CATEGORY.FirstOrDefault(a => a.CATEGORY_ID == id && a.PARENT_CATEGORY == "0");
                            if (categoryCheck == null)
                            {
                                var subCategoryCheck = _objCSEntities.TB_CATEGORY.Where(a => a.PARENT_CATEGORY == categoryID);

                                if (subCategoryCheck.Any())
                                {
                                    subCategory.Add(categoryID);
                                    subcategoryCheck.AddRange(subCategoryCheck.Select(item => item.CATEGORY_ID));
                                }
                                else
                                {
                                    subCategory.Add(categoryID);
                                }

                            }
                            else
                            {
                                rootCategory.Remove(categoryID);
                            }
                        }

                        if (subCategory != null)
                        {
                            foreach (var item in subcategoryCheck)
                            {
                                subCategory.Remove(item);
                            }
                            multipleCategoryIds = subCategory.ToArray();
                        }
                        else
                        {
                            multipleCategoryIds = rootCategory.ToArray();
                        }
                    }
                    foreach (string t in multipleCategoryIds)
                    {
                        if (t.Contains("~")) continue;
                        string categoryID = t;
                        int sortOrder = 1;
                        var td = _objCSEntities.TB_CATEGORY.Where(z => z.PARENT_CATEGORY == pasteCatId)
                            .Join(_objCSEntities.TB_CATALOG_SECTIONS, a => a.CATEGORY_ID, b => b.CATEGORY_ID, (a, b) => new { b }).Where(x => x.b.CATALOG_ID == catalogId);

                        if (td.Any())
                        {
                            var sortordercount = td.Max(x => x.b.SORT_ORDER);
                            sortOrder = sortordercount + 1;
                        }

                        var objCutCat = _objCSEntities.TB_CATEGORY.Where(s => s.CATEGORY_ID == categoryID);

                        if (objCutCat.Any())
                        {
                            var objcutcategoryid = objCutCat.FirstOrDefault();
                            if (objcutcategoryid != null)
                            {
                                objcutcategoryid.PARENT_CATEGORY = pasteCatId;
                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    objSqlConnection.Open();
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "UPDATE TB_CATEGORY SET PARENT_CATEGORY='" + pasteCatId + "' WHERE CATEGORY_ID='" + categoryID + "'";
                                    objSqlCommand.CommandType = CommandType.Text;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.ExecuteNonQuery();
                                }
                                //  _objCSEntities.SaveChanges();
                                if (previousCatalogID != catalogId)
                                {
                                    //var catalogsectionscheck = _objCSEntities.TB_CATALOG_SECTIONS.Where(s => s.CATEGORY_ID == category_id && s.CATALOG_ID == previousCatalogID);
                                    var catalogsectionscheck = _objCSEntities.Category_Function(previousCatalogID, categoryID);
                                    if (catalogsectionscheck.Any())
                                    {
                                        foreach (var item in catalogsectionscheck)
                                        {
                                            var deletecatalogsectionscheck = _objCSEntities.TB_CATALOG_SECTIONS.Where(s => s.CATEGORY_ID == item.CATEGORY_ID && s.CATALOG_ID == previousCatalogID);
                                            if (deletecatalogsectionscheck.Any())
                                            {
                                                foreach (var deleteitem in deletecatalogsectionscheck)
                                                {
                                                    _objCSEntities.TB_CATALOG_SECTIONS.Remove(deleteitem);
                                                }
                                            }

                                            var objcatalogsectionscheck = _objCSEntities.TB_CATALOG_SECTIONS.Where(s => s.CATEGORY_ID == item.CATEGORY_ID && s.CATALOG_ID == catalogId);
                                            if (catalogsectionscheck.Any())
                                            {
                                                int sortordercatalog = 1;
                                                var sortorder = _objCSEntities.TB_CATALOG_SECTIONS.Where(s => s.CATALOG_ID == catalogId);
                                                if (sortorder.Any())
                                                {
                                                    sortordercatalog = sortorder.Max(x => x.SORT_ORDER) + 1;
                                                }
                                                var objTbcatalogproduct = new TB_CATALOG_SECTIONS
                                                {
                                                    CATALOG_ID = catalogId,
                                                    CATEGORY_ID = item.CATEGORY_ID,
                                                    SORT_ORDER = sortordercatalog,
                                                    CREATED_USER = User.Identity.Name,
                                                    CREATED_DATE = DateTime.Now,
                                                    MODIFIED_USER = User.Identity.Name,
                                                    MODIFIED_DATE = DateTime.Now,
                                                    FLAG_RECYCLE = "A"
                                                };
                                                _objCSEntities.TB_CATALOG_SECTIONS.Add(objTbcatalogproduct);


                                            }
                                            //catalog_family_update
                                            var objCatalogFamily = _objCSEntities.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == item.CATEGORY_ID && a.CATALOG_ID == previousCatalogID);
                                            if (objCatalogFamily.Any())
                                            {
                                                foreach (var objCatalogFamilyItem in objCatalogFamily.Select(values => new TB_CATALOG_FAMILY
                                                {
                                                    CATALOG_ID = catalogId,
                                                    FAMILY_ID = values.FAMILY_ID,
                                                    SORT_ORDER = values.SORT_ORDER,
                                                    CATEGORY_ID = item.CATEGORY_ID,
                                                    CREATED_USER = User.Identity.Name,
                                                    CREATED_DATE = DateTime.Now,
                                                    MODIFIED_USER = User.Identity.Name,
                                                    MODIFIED_DATE = DateTime.Now,
                                                    ROOT_CATEGORY = values.ROOT_CATEGORY,
                                                    FLAG_RECYCLE = "A"

                                                }))
                                                {
                                                    _objCSEntities.TB_CATALOG_FAMILY.Add(objCatalogFamilyItem);
                                                }

                                                foreach (var values in objCatalogFamily)
                                                {
                                                    _objCSEntities.TB_CATALOG_FAMILY.Remove(values);
                                                }
                                            }
                                            //category_fanily_attr_list
                                            var objCategoryFamilyattlist = _objCSEntities.TB_CATEGORY_FAMILY_ATTR_LIST.Where(a => a.CATALOG_ID == previousCatalogID && a.CATEGORY_ID == item.CATEGORY_ID);
                                            if (!objCategoryFamilyattlist.Any()) continue;
                                            foreach (var objCategoryFamilyaatlist in objCategoryFamilyattlist.Select(values => new TB_CATEGORY_FAMILY_ATTR_LIST
                                            {
                                                ATTRIBUTE_ID = values.ATTRIBUTE_ID,
                                                FAMILY_ID = values.FAMILY_ID,
                                                CATEGORY_ID = item.CATEGORY_ID,
                                                SORT_ORDER = values.SORT_ORDER,
                                                CATALOG_ID = catalogId,
                                                CREATED_USER = User.Identity.Name,
                                                CREATED_DATE = DateTime.Now,
                                                MODIFIED_USER = User.Identity.Name,
                                                MODIFIED_DATE = DateTime.Now,
                                                FLAG_RECYCLE = "A"
                                            }))
                                            {
                                                _objCSEntities.TB_CATEGORY_FAMILY_ATTR_LIST.Add(objCategoryFamilyaatlist);
                                            }

                                            //foreach (var Delete_Id in objCategory_familyattlist)
                                            //{
                                            //    _objCSEntities.TB_CATEGORY_FAMILY_ATTR_LIST.Remove(Delete_Id);
                                            //}
                                            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                            {
                                                objSqlConnection.Open();
                                                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                                objSqlCommand.CommandText = "DELETE FROM TB_CATEGORY_FAMILY_ATTR_LIST WHERE CATALOG_ID = " + previousCatalogID + " AND CATEGORY_ID = '" + item.CATEGORY_ID + "'";
                                                objSqlCommand.CommandType = CommandType.Text;
                                                objSqlCommand.Connection = objSqlConnection;
                                                objSqlCommand.ExecuteNonQuery();
                                            }
                                        }
                                        _objCSEntities.SaveChanges();
                                    }

                                }

                            }
                        }
                    }
                }
                else
                {
                    string catid = string.Empty;
                    int catalog = 0;
                    int sortOrder = 1;
                    var td = _objCSEntities.TB_CATEGORY.Where(z => z.PARENT_CATEGORY == pasteCatId)
                        .Join(_objCSEntities.TB_CATALOG_SECTIONS, a => a.CATEGORY_ID, b => b.CATEGORY_ID, (a, b) => new { b }).Where(x => x.b.CATALOG_ID == catalogId);

                    if (td.Any())
                    {
                        var sortordercount = td.Max(x => x.b.SORT_ORDER);
                        sortOrder = sortordercount + 1;
                    }

                    var objCutCat = _objCSEntities.TB_CATEGORY.Where(s => s.CATEGORY_ID == cutCatId);

                    if (objCutCat.Any())
                    {
                        var objcutcategoryid = objCutCat.FirstOrDefault();
                        if (objcutcategoryid != null)
                        {
                            objcutcategoryid.PARENT_CATEGORY = pasteCatId;
                            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                            {
                                objSqlConnection.Open();
                                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                objSqlCommand.CommandText = "UPDATE TB_CATEGORY SET PARENT_CATEGORY='" + pasteCatId + "' WHERE CATEGORY_ID='" + cutCatId + "'";
                                objSqlCommand.CommandType = CommandType.Text;
                                objSqlCommand.Connection = objSqlConnection;
                                objSqlCommand.ExecuteNonQuery();
                            }

                            #region Attribute Pack

                            List<TB_ATTRIBUTE_HIERARCHY> list = new List<TB_ATTRIBUTE_HIERARCHY>();
                            List<TB_ATTRIBUTE_HIERARCHY> list1 = new List<TB_ATTRIBUTE_HIERARCHY>();
                            list = _objCSEntities.TB_ATTRIBUTE_HIERARCHY.Where(a => a.ASSIGN_TO == pasteCatId).ToList();
                            list1 = _objCSEntities.TB_ATTRIBUTE_HIERARCHY.Where(a => a.ASSIGN_TO == pasteCatId).ToList();
                            list.AddRange(list1);
                            int customer_Id = 0;
                            HomeApiController obj = new HomeApiController();
                            customer_Id = obj.GetCustomerId();
                            foreach (var item in list)
                            {
                                TB_ATTRIBUTE_HIERARCHY attributeHierarchyInsert1 = new TB_ATTRIBUTE_HIERARCHY();
                                int catalog_ID = 0;
                                if (int.TryParse(item.ASSIGN_TO, out catalog_ID))
                                {
                                    attributeHierarchyInsert1 = _objCSEntities.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == cutCatId && s.PACK_ID == item.PACK_ID && s.TYPE == "Category").FirstOrDefault();
                                    if (attributeHierarchyInsert1 == null)
                                    {
                                        string QryString = "insert into TB_ATTRIBUTE_HIERARCHY values(" + item.PACK_ID + ",'" + "Category" + "','" + cutCatId + "'," + customer_Id + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                        SqlConnection conn = new SqlConnection(connectionString);
                                        SqlCommand CmdObj = new SqlCommand(QryString, conn);
                                        conn.Open();
                                        CmdObj.ExecuteNonQuery();
                                        conn.Close();
                                    }
                                }
                                int groupId = Convert.ToInt32(item.PACK_ID);
                                obj.AssociateAttributePackaging(cutCatId, groupId, catalogId);
                            }

                            #endregion

                            //_objCSEntities.SaveChanges();
                            if (previousCatalogID != catalogId)
                            {
                                //var catalogsectionscheck = _objCSEntities.TB_CATALOG_SECTIONS.Where(s => s.CATEGORY_ID == cutCatId && s.CATALOG_ID == previousCatalogID);
                                var catalogsectionscheck = _objCSEntities.Category_Function(previousCatalogID, cutCatId);
                                if (catalogsectionscheck.Any())
                                {
                                    foreach (var item in catalogsectionscheck)
                                    {
                                        catid = item.CATEGORY_ID;
                                        catalog = Convert.ToInt32(item.CATALOG_ID);
                                        var deletecatalogsectionscheck = _objCSEntities.TB_CATALOG_SECTIONS.Where(s => s.CATEGORY_ID == item.CATEGORY_ID && s.CATALOG_ID == previousCatalogID);
                                        if (deletecatalogsectionscheck.Any())
                                        {
                                            foreach (var deleteitem in deletecatalogsectionscheck)
                                            {
                                                _objCSEntities.TB_CATALOG_SECTIONS.Remove(deleteitem);
                                            }
                                        }

                                        //_objCSEntities.SaveChanges();

                                        var objcatalogsectionscheck = _objCSEntities.TB_CATALOG_SECTIONS.Where(s => s.CATEGORY_ID == item.CATEGORY_ID && s.CATALOG_ID == catalogId);
                                        if (!objcatalogsectionscheck.Any())
                                        {
                                            int sortordercatalog = 1;
                                            var sortorder = _objCSEntities.TB_CATALOG_SECTIONS.Where(s => s.CATALOG_ID == catalogId);
                                            if (sortorder.Any())
                                            {
                                                sortordercatalog = sortorder.Max(x => x.SORT_ORDER) + 1;
                                            }
                                            var objTbcatalogproduct = new TB_CATALOG_SECTIONS
                                            {

                                                CATALOG_ID = catalogId,
                                                CATEGORY_ID = item.CATEGORY_ID,
                                                SORT_ORDER = sortordercatalog,
                                                CREATED_USER = User.Identity.Name,
                                                CREATED_DATE = DateTime.Now,
                                                MODIFIED_USER = User.Identity.Name,
                                                MODIFIED_DATE = DateTime.Now,
                                                FLAG_RECYCLE = "A"
                                            };
                                            _objCSEntities.TB_CATALOG_SECTIONS.Add(objTbcatalogproduct);

                                            //catalog_family_update
                                            var objCatalogFamily = _objCSEntities.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == item.CATEGORY_ID && a.CATALOG_ID == previousCatalogID);
                                            if (objCatalogFamily.Any())
                                            {
                                                foreach (var objCatalog_Family in objCatalogFamily)
                                                {
                                                    var objcatfamily = new TB_CATALOG_FAMILY
                                                    {
                                                        CATALOG_ID = catalogId,
                                                        FAMILY_ID = objCatalog_Family.FAMILY_ID,
                                                        SORT_ORDER = objCatalog_Family.SORT_ORDER,
                                                        CATEGORY_ID = item.CATEGORY_ID,
                                                        CREATED_USER = User.Identity.Name,
                                                        CREATED_DATE = DateTime.Now,
                                                        MODIFIED_USER = User.Identity.Name,
                                                        MODIFIED_DATE = DateTime.Now,
                                                        ROOT_CATEGORY = objCatalog_Family.ROOT_CATEGORY,
                                                        FLAG_RECYCLE = "A"
                                                    };
                                                    _objCSEntities.TB_CATALOG_FAMILY.Add(objcatfamily);
                                                }
                                                //_objCSEntities.SaveChanges();
                                                //foreach (var values in objCatalogFamily)
                                                //{
                                                //    _objCSEntities.TB_CATALOG_FAMILY.Remove(values);
                                                //}

                                                //_objCSEntities.SaveChanges();
                                            }
                                            //category_fanily_attr_list
                                            var objCategoryFamilyattlist = _objCSEntities.TB_CATEGORY_FAMILY_ATTR_LIST.Where(a => a.CATALOG_ID == previousCatalogID && a.CATEGORY_ID == item.CATEGORY_ID);
                                            if (objCategoryFamilyattlist.Any())
                                            {
                                                foreach (var objCategoryFamilyaatlist in objCategoryFamilyattlist)
                                                {
                                                    var objattrlist = new TB_CATEGORY_FAMILY_ATTR_LIST
                                                    {
                                                        ATTRIBUTE_ID = objCategoryFamilyaatlist.ATTRIBUTE_ID,
                                                        FAMILY_ID = objCategoryFamilyaatlist.FAMILY_ID,
                                                        CATEGORY_ID = item.CATEGORY_ID,
                                                        SORT_ORDER = objCategoryFamilyaatlist.SORT_ORDER,
                                                        CATALOG_ID = catalogId,
                                                        CREATED_USER = User.Identity.Name,
                                                        CREATED_DATE = DateTime.Now,
                                                        MODIFIED_USER = User.Identity.Name,
                                                        MODIFIED_DATE = DateTime.Now,
                                                        FLAG_RECYCLE = "A"

                                                    };
                                                    _objCSEntities.TB_CATEGORY_FAMILY_ATTR_LIST.Add(objattrlist);
                                                }
                                            }
                                            //_objCSEntities.SaveChanges();
                                            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                            {
                                                objSqlConnection.Open();
                                                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                                objSqlCommand.CommandText = "DELETE FROM TB_CATEGORY_FAMILY_ATTR_LIST WHERE CATALOG_ID = " + previousCatalogID + " AND CATEGORY_ID = '" + item.CATEGORY_ID + "'";
                                                objSqlCommand.CommandType = CommandType.Text;
                                                objSqlCommand.Connection = objSqlConnection;
                                                objSqlCommand.ExecuteNonQuery();
                                            }

                                            //subproducts entry//
                                            var family_ids = _objCSEntities.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == item.CATEGORY_ID && a.CATALOG_ID == item.CATALOG_ID).Select(x => x.FAMILY_ID.ToString()).ToList();
                                            if (family_ids.Any())
                                            {
                                                var subproducts = _objCSEntities.TB_PROD_FAMILY.Join(_objCSEntities.TB_SUBPRODUCT, pf => pf.PRODUCT_ID, sp => sp.PRODUCT_ID, (pf, sp) => new { pf, sp }).Where(a => a.sp.CATALOG_ID == previousCatalogID && family_ids.Contains(a.pf.FAMILY_ID.ToString())).Select(b => b.sp);
                                                if (subproducts.Any())
                                                {
                                                    foreach (var subprod in subproducts)
                                                    {
                                                        var objsubproduct = new TB_SUBPRODUCT
                                                        {
                                                            PRODUCT_ID = subprod.PRODUCT_ID,
                                                            SUBPRODUCT_ID = subprod.SUBPRODUCT_ID,
                                                            CATALOG_ID = catalogId,
                                                            SORT_ORDER = subprod.SORT_ORDER,
                                                            CREATED_USER = subprod.CREATED_USER,
                                                            CREATED_DATE = subprod.CREATED_DATE,
                                                            MODIFIED_DATE = subprod.MODIFIED_DATE,
                                                            MODIFIED_USER = subprod.MODIFIED_USER,
                                                            FLAG_RECYCLE = "A"
                                                        };

                                                        _objCSEntities.TB_SUBPRODUCT.Add(objsubproduct);
                                                    }

                                                    var subproductkey = _objCSEntities.TB_SUBPRODUCT_KEY.Join(subproducts, sk => sk.PRODUCT_ID, sp => sp.PRODUCT_ID, (sk, sp) => new { sk, sp }).Where(x => x.sk.SUBPRODUCT_ID == x.sp.SUBPRODUCT_ID && x.sk.CATALOG_ID == x.sp.CATALOG_ID && x.sp.CATALOG_ID == previousCatalogID && x.sk.CATEGORY_ID == cutCatId).Select(y => y.sk);
                                                    if (subproductkey.Any())
                                                    {
                                                        foreach (var val in subproductkey)
                                                        {
                                                            var objsubkey = new TB_SUBPRODUCT_KEY
                                                            {
                                                                ATTRIBUTE_ID = val.ATTRIBUTE_ID,
                                                                SUBPRODUCT_ID = val.SUBPRODUCT_ID,
                                                                PRODUCT_ID = val.PRODUCT_ID,
                                                                FAMILY_ID = val.FAMILY_ID,
                                                                ATTRIBUTE_VALUE = val.ATTRIBUTE_VALUE,
                                                                CATEGORY_ID = val.CATEGORY_ID,
                                                                CATALOG_ID = catalogId,
                                                                CREATED_USER = User.Identity.Name,
                                                                MODIFIED_USER = User.Identity.Name,
                                                                MODIFIED_DATE = DateTime.Now,
                                                                CREATED_DATE = DateTime.Now
                                                            };
                                                            _objCSEntities.TB_SUBPRODUCT_KEY.Add(objsubkey);
                                                        }

                                                    }
                                                    //_objCSEntities.TB_SUBPRODUCT.RemoveRange(subproducts);
                                                    #region
                                                    //var subproductids = _objCSEntities.TB_PROD_FAMILY.Join(_objCSEntities.TB_SUBPRODUCT, pf => pf.PRODUCT_ID, sp => sp.PRODUCT_ID, (pf, sp) => new { pf, sp }).Where(a => a.sp.CATALOG_ID == previousCatalogID && family_ids.Contains(a.pf.FAMILY_ID.ToString())).Select(b => new { b.sp.SUBPRODUCT_ID, b.sp.PRODUCT_ID });
                                                    //var subprodattrlist = _objCSEntities.TB_SUBPRODUCT_ATTR_LIST.Join(subproductids, sal => sal.PRODUCT_ID, sp => sp.PRODUCT_ID, (sal, sp) => new { sal, sp }).Select(x => x.sal);
                                                    //if (subprodattrlist.Any())
                                                    //{
                                                    //    _objCSEntities.TB_SUBPRODUCT_ATTR_LIST.AddRange(subprodattrlist);
                                                    //}

                                                    //using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                                    //{
                                                    //    string f = string.Empty;
                                                    //    foreach (var id in family_ids)
                                                    //    {
                                                    //        f = f + "," + id;
                                                    //    }
                                                    //    objSqlConnection.Open();
                                                    //    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                                    //    objSqlCommand.CommandText = "DELETE SAL FROM TB_SUBPRODUCT_ATTR_LIST SAL INNER JOIN TB_SUBPRODUCT SP ON SP.PRODUCT_ID = SAL.PRODUCT_ID AND SAL.SUBPRODUCT_ID = SP.SUBPRODUCT_ID INNER JOIN TB_PROD_FAMILY PF ON PF.PRODUCT_ID = SP.PRODUCT_ID AND PF.FAMILY_ID IN(" + f.Trim(',') + ")";
                                                    //    objSqlCommand.CommandType = CommandType.Text;
                                                    //    objSqlCommand.Connection = objSqlConnection;
                                                    //    objSqlCommand.ExecuteNonQuery();
                                                    //}
                                                    #endregion
                                                }


                                            }
                                        }
                                    }
                                    _objCSEntities.SaveChanges();
                                    var family = _objCSEntities.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == catid && a.CATALOG_ID == catalog).Select(x => x.FAMILY_ID.ToString()).ToList();
                                    if (family.Any())
                                    {
                                        var subproducts = _objCSEntities.TB_PROD_FAMILY.Join(_objCSEntities.TB_SUBPRODUCT, pf => pf.PRODUCT_ID, sp => sp.PRODUCT_ID, (pf, sp) => new { pf, sp }).Where(a => a.sp.CATALOG_ID == previousCatalogID && family.Contains(a.pf.FAMILY_ID.ToString())).Select(b => b.sp);
                                        if (subproducts.Any())
                                        {
                                            _objCSEntities.TB_SUBPRODUCT.RemoveRange(subproducts);
                                            _objCSEntities.SaveChanges();
                                        }

                                    }
                                    var objCatalogFamilydelete = _objCSEntities.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == catid && a.CATALOG_ID == previousCatalogID);
                                    if (objCatalogFamilydelete.Any())
                                    {
                                        _objCSEntities.TB_CATALOG_FAMILY.RemoveRange(objCatalogFamilydelete);
                                        _objCSEntities.SaveChanges();
                                    }

                                }
                            }

                        }
                    }
                }

                return true;


            }
            catch (Exception objException)
            {
                Logger.Error("Error at CategoryController : CutPasteCategory", objException);
                return false;
            }
        }


        public string CopyPasteCategory(string copyCatId, string pasteCatId, int catalogId, int previousCatalogID, string multipleFamilyids, string multipleFamilyidsForAnotherCatalog, int pasteFlag, bool SubProductCFM)
        {
            try
            {
                // if (SubProductCFM == true)
                //{
                _objCSEntities.Database.CommandTimeout = 0;
                bool result = false;
                string[] MultipleCategoryIdsChk = null;
                string cat_idChk = string.Empty;

                // ***************************************************
                //  Check Product Count Before insert 
                // ***************************************************
                int CategoryProductCount = 0;
                int categoryId = 0;
                int allowduplicate = 0;
                var alloduplicate_check = _objCSEntities.Customer_Settings.FirstOrDefault(a => a.CustomerId == _objCSEntities.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId);
                if (alloduplicate_check != null)
                {
                    if (alloduplicate_check.AllowDuplicateItem_PartNum == true)
                    { allowduplicate = 1; }
                    else
                    { allowduplicate = 0; }

                }
                int productcountExist = 0;
                var productcountall = _objCSEntities.STP_LS_GET_PRODUCTS_BY_USER("PRODUCT", User.Identity.Name, "").ToList();

                if (productcountall.Any())
                {
                    productcountExist = Convert.ToInt32(productcountall[0]);
                }

                var skucnt = _objCSEntities.TB_PLAN
                          .Join(_objCSEntities.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                          .Join(_objCSEntities.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                          .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => x.tcptps.tps.SKU_COUNT).ToList();
                int productSkuCount = skucnt[0];

                if (multipleFamilyids != null && multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog != null && multipleFamilyidsForAnotherCatalog.Trim() != "")
                {
                    if (multipleFamilyids == null || multipleFamilyids.Trim() == "")
                    {
                        MultipleCategoryIdsChk = multipleFamilyidsForAnotherCatalog.Split(',');
                    }
                    else
                    {
                        MultipleCategoryIdsChk = multipleFamilyids.Split(',');
                    }
                    var RootCategory = new List<string>(MultipleCategoryIdsChk);
                    var SubCategory = new List<string>();
                    var Subcategory_Check = new List<string>();
                    var RootCategory2 = new List<string>();


                    for (int i = 0; i < MultipleCategoryIdsChk.Length; i++)
                    {
                        string category_id = MultipleCategoryIdsChk[i];
                        if (!category_id.Contains("~"))
                        {
                            var category_check = _objCSEntities.TB_CATEGORY.FirstOrDefault(a => a.CATEGORY_ID == category_id && a.PARENT_CATEGORY == "0");
                            if (category_check == null)
                            {
                                var Sub_category_check = _objCSEntities.TB_CATEGORY.Where(a => a.PARENT_CATEGORY == category_id);

                                if (Sub_category_check.Any())
                                {
                                    SubCategory.Add(category_id);
                                    foreach (var item in Sub_category_check)
                                    {
                                        Subcategory_Check.Add(item.CATEGORY_ID);
                                    }

                                }
                                else
                                {
                                    SubCategory.Add(category_id);

                                }

                            }
                            else
                            {
                                RootCategory.Remove(category_id);
                                RootCategory2.Add(category_id);
                            }
                        }
                    }

                    if (SubCategory != null)
                    {
                        foreach (var item in Subcategory_Check)
                        {
                            SubCategory.Remove(item);
                        }
                        SubCategory.Add(cat_idChk);
                        if (RootCategory2 != null)
                        {
                            MultipleCategoryIdsChk = RootCategory2.ToArray();
                        }
                        else
                        {
                            MultipleCategoryIdsChk = SubCategory.ToArray();
                        }
                    }

                    else
                    {
                        MultipleCategoryIdsChk = RootCategory.ToArray();
                    }
                    for (int i = 0; i < MultipleCategoryIdsChk.Length; i++)
                    {
                        if (!MultipleCategoryIdsChk[i].Contains("~"))
                        {
                            string category_id = MultipleCategoryIdsChk[i];
                            {
                                //var Cat_Id = _objCSEntities.STP_CATALOGSTUDIO5_Copy_Category_Family(pasteCatId, category_id, 0, catalogId, previousCatalogID, 0, pasteFlag, 0).ToList();
                                //_objCSEntities.SaveChanges();
                                var productcount = _objCSEntities.STP_LS_GET_PRODUCTS_BY_USER("CATEGORY", User.Identity.Name, category_id).ToList();

                                if (productcount.Any())
                                {
                                    CategoryProductCount = CategoryProductCount + Convert.ToInt32(productcount[0]);
                                }
                            }
                        }

                    }

                }
                else
                {
                    var productcount = _objCSEntities.STP_LS_GET_PRODUCTS_BY_USER("CATEGORY", User.Identity.Name, copyCatId).ToList();

                    if (productcount.Any())
                    {
                        CategoryProductCount = CategoryProductCount + Convert.ToInt32(productcount[0]);
                    }

                }

                string[] MultipleCategoryIds = null;
                string cat_id = string.Empty;
                if ((productcountExist + CategoryProductCount) > productSkuCount && allowduplicate == 1)
                {
                    return "You have exceeded the Maximum No. of SKUs as per your Plan";
                }
                else
                {
                    // ***************************************************
                    //  Check Product Count Before insert end
                    // ***************************************************


                    if (multipleFamilyids != null && multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog != null && multipleFamilyidsForAnotherCatalog.Trim() != "")
                    {
                        if (multipleFamilyids == null || multipleFamilyids.Trim() == "")
                        {
                            MultipleCategoryIds = multipleFamilyidsForAnotherCatalog.Split(',');
                        }
                        else
                        {
                            MultipleCategoryIds = multipleFamilyids.Split(',');
                        }
                        var RootCategory = new List<string>(MultipleCategoryIds);
                        var SubCategory = new List<string>();
                        var Subcategory_Check = new List<string>();
                        var RootCategory2 = new List<string>();


                        for (int i = 0; i < MultipleCategoryIds.Length; i++)
                        {
                            string category_id = MultipleCategoryIds[i];
                            if (!category_id.Contains("~"))
                            {
                                var category_check = _objCSEntities.TB_CATEGORY.FirstOrDefault(a => a.CATEGORY_ID == category_id && a.PARENT_CATEGORY == "0");
                                if (category_check == null)
                                {
                                    var Sub_category_check = _objCSEntities.TB_CATEGORY.Where(a => a.PARENT_CATEGORY == category_id);

                                    if (Sub_category_check.Any())
                                    {
                                        SubCategory.Add(category_id);
                                        foreach (var item in Sub_category_check)
                                        {
                                            Subcategory_Check.Add(item.CATEGORY_ID);

                                        }

                                    }
                                    else
                                    {
                                        SubCategory.Add(category_id);

                                    }
                                }
                                else
                                {
                                    RootCategory.Remove(category_id);
                                    RootCategory2.Add(category_id);
                                }
                            }
                        }
                        if (SubCategory != null)
                        {
                            foreach (var item in Subcategory_Check)
                            {
                                SubCategory.Remove(item);
                            }
                            SubCategory.Add(cat_id);
                            if (RootCategory2 != null && RootCategory2.Count != 0)
                            {
                                MultipleCategoryIds = RootCategory2.ToArray();
                            }
                            else
                            {
                                MultipleCategoryIds = SubCategory.ToArray();
                            }
                        }
                        else
                        {
                            MultipleCategoryIds = RootCategory.ToArray();
                        }
                        for (int i = 0; i < MultipleCategoryIds.Length; i++)
                        {
                            if (!MultipleCategoryIds[i].Contains("~"))
                            {
                                string category_id = MultipleCategoryIds[i];
                                {
                                  //  _objCSEntities.Database.CommandTimeout = 0;
                                   // _objCSEntities.STP_CATALOGSTUDIO5_Copy_Category_Family(pasteCatId, category_id, 0, catalogId, previousCatalogID, 0, pasteFlag, 0, User.Identity.Name).ToList();
                                   // _objCSEntities.SaveChanges();

                                    using (var conn = new SqlConnection(connectionString))
                                    {
                                        conn.Open();
                                        var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_Copy_Category_Family ''" + pasteCatId + "'',''" + category_id + "'',''" + 0 + "'',''" + catalogId + "'',''" + previousCatalogID + "'',''" + 0 + "'',''" + pasteFlag + "'',''" + 0 + "'',''" + User.Identity.Name + "''')", conn);
                                        cmd.CommandTimeout = 0;
                                        cmd.ExecuteNonQuery();
                                        //var df = cmd.ExecuteScalar();
                                    }


                                    if (SubProductCFM)
                                    {
                                        bool val = SubproductPaste(copyCatId, catalogId, previousCatalogID, allowduplicate);
                                    }
                                }
                            }
                            //return "successfully Pasted.";
                        }
                        return "Paste successfully";
                    }
                    else
                    {
                        using (var conn = new SqlConnection(connectionString))
                        {
                            conn.Open();
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_Copy_Category_Family ''" + pasteCatId + "'',''" + copyCatId + "'',''" + 0 + "'',''" + catalogId + "'',''" + previousCatalogID + "'',''" + 0 + "'',''" + pasteFlag + "'',''" + allowduplicate + "'',''" + User.Identity.Name + "''')", conn);
                            cmd.CommandTimeout = 0;
                            cmd.ExecuteNonQuery();
                            //var df = cmd.ExecuteScalar();
                        }


                       // _objCSEntities.Database.CommandTimeout = 0;
                        //var list = _objCSEntities.STP_CATALOGSTUDIO5_Copy_Category_Family(pasteCatId, copyCatId, 0, catalogId, previousCatalogID, 0, pasteFlag, allowduplicate, User.Identity.Name).ToList();
                        //_objCSEntities.SaveChanges();
                        #region
                        //var catidsnew = _objCSEntities.TB_CATEGORY_CHANGE_LIST.Where(a => a.OLD_PARENT_CATEGORY == copyCatId && a.MODIFIED_USER == User.Identity.Name).OrderBy(a => a.CATEGORY_ID).ToArray();
                        //var catidsold = _objCSEntities.Category_Function(catalogId, copyCatId).OrderBy(a => a.CATEGORY_ID).ToArray();
                        //if (catidsold.Any())
                        //{
                        //    for (int i = 0; i < catidsold.Count(); i++)
                        //    {
                        //        string oldcat = catidsold[i].CATEGORY_ID;
                        //        string newcat = catidsnew[i].CATEGORY_ID;
                        //        var familyidsold = _objCSEntities.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == oldcat && a.CATALOG_ID == catalogId).OrderBy(a => a.FAMILY_ID).ToArray();
                        //        var familyidsnew = _objCSEntities.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == newcat && a.CATALOG_ID == catalogId).OrderBy(a => a.FAMILY_ID).ToArray();
                        //        if (familyidsold.Any() && familyidsnew.Any())
                        //        {
                        //            for (int j = 0; j < familyidsold.Count(); j++)
                        //            {
                        //                var stp = _objCSEntities.STP_LS_SUBPRODUCTS_COPY_PASTE_FAMILY(familyidsold[i].FAMILY_ID, familyidsnew[i].FAMILY_ID, "FAMILY", allowduplicate, User.Identity.Name, catalogId);
                        //                _objCSEntities.SaveChanges();

                        //                int oldfamily_id = familyidsold[i].FAMILY_ID;
                        //                int newfamily_id = familyidsnew[i].FAMILY_ID;
                        //                var subfamily_old = _objCSEntities.TB_FAMILY.Where(a => a.PARENT_FAMILY_ID == oldfamily_id).OrderBy(b => b.FAMILY_ID).ToArray();
                        //                var subfamily_checkforsubproducts = _objCSEntities.TB_FAMILY.Where(a => a.PARENT_FAMILY_ID == newfamily_id).OrderBy(a => a.FAMILY_ID).ToArray();
                        //                if (subfamily_old.Any())
                        //                {
                        //                    for (int k = 0; k < subfamily_old.Count(); k++)
                        //                    {
                        //                        var val0 = _objCSEntities.STP_LS_SUBPRODUCTS_COPY_PASTE_FAMILY(subfamily_old[i].FAMILY_ID, subfamily_checkforsubproducts[i].FAMILY_ID, "FAMILY", allowduplicate, User.Identity.Name, catalogId);
                        //                        _objCSEntities.SaveChanges();
                        //                    }

                        //                }
                        //            }

                        //        }

                        //    }

                        //    var catidsnewupdate = _objCSEntities.TB_CATEGORY_CHANGE_LIST.Where(a => a.OLD_PARENT_CATEGORY == copyCatId && a.MODIFIED_USER == User.Identity.Name).ToList();
                        //    catidsnewupdate.ForEach(a => a.OLD_PARENT_CATEGORY = "Updated");
                        //    _objCSEntities.SaveChanges();
                        //    result = true;
                        //}
                        #endregion
                        //sub products paste
                        if (SubProductCFM)
                        {
                            bool val = SubproductPaste(copyCatId, catalogId, previousCatalogID, allowduplicate);
                        }
                        return "Paste successfully";
                    }
                }
                //  }
                //else 
                // { 
                ////    return "Paste Failed"; 
                //   }

            }

            catch (Exception objException)
            {
                Logger.Error("Error at CategoryController : CopyPasteCategory", objException);
                return "Action not successful";
            }

        }
        public bool SubproductPaste(string copyCatId, int catalogId, int previousCatalogID, int allowduplicate)
        {
            try
            {
                var catids = _objCSEntities.Category_Function(previousCatalogID, copyCatId).Select(a => a.CATEGORY_ID).ToArray();
                // family_ids = _dbcontext.TB_CATALOG_FAMILY.Where(a => catids.Contains(a.CATEGORY_ID) && a.CATALOG_ID == catalog_id).Select(a => a.FAMILY_ID.ToString()).ToArray();
                var catidsold = _objCSEntities.Category_Function(previousCatalogID, copyCatId).ToArray();
                var catidsnew = _objCSEntities.TB_CATEGORY_CHANGE_LIST.Where(a => catids.Contains(a.OLD_PARENT_CATEGORY) && a.MODIFIED_USER == User.Identity.Name).ToArray();

                if (catidsold.Any())
                {
                    for (int i = 0; i < catidsold.Count(); i++)
                    {
                        string oldcat = catidsold[i].CATEGORY_ID;
                        string newcat = catidsnew[i].CATEGORY_ID;
                        var familyidsold = _objCSEntities.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == oldcat && a.CATALOG_ID == previousCatalogID).OrderBy(a => a.FAMILY_ID).
                                           Join(_objCSEntities.TB_FAMILY, tcf => tcf.FAMILY_ID, tb => tb.FAMILY_ID, (tcf, tb) => new { tcf, tb }).Select(a => new { a.tb.FAMILY_ID, a.tb.FAMILY_NAME }).ToArray();
                        var familyidsnew = _objCSEntities.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == newcat && a.CATALOG_ID == catalogId).OrderBy(a => a.FAMILY_ID).
                                           Join(_objCSEntities.TB_FAMILY, tcf => tcf.FAMILY_ID, tb => tb.FAMILY_ID, (tcf, tb) => new { tcf, tb }).Select(a => new { a.tb.FAMILY_ID, a.tb.FAMILY_NAME }).ToArray();
                        if (familyidsold.Any() && familyidsnew.Any())
                        {
                            for (int j = 0; j < familyidsold.Count(); j++)
                            {
                                var stp = _objCSEntities.STP_LS_SUBPRODUCTS_COPY_PASTE_FAMILY(familyidsold[j].FAMILY_ID, familyidsnew[j].FAMILY_ID, "FAMILY", allowduplicate, User.Identity.Name, previousCatalogID, catalogId, newcat, oldcat);
                                _objCSEntities.SaveChanges();

                                int oldfamily_id = familyidsold[j].FAMILY_ID;
                                int newfamily_id = familyidsnew[j].FAMILY_ID;
                                var subfamily_old = _objCSEntities.TB_FAMILY.Where(a => a.PARENT_FAMILY_ID == oldfamily_id).OrderBy(b => b.FAMILY_ID).ToArray();
                                var subfamily_checkforsubproducts = _objCSEntities.TB_FAMILY.Where(a => a.PARENT_FAMILY_ID == newfamily_id).OrderBy(a => a.FAMILY_ID).ToArray();
                                if (subfamily_old.Any())
                                {
                                    for (int k = 0; k < subfamily_old.Count(); k++)
                                    {
                                        var val0 = _objCSEntities.STP_LS_SUBPRODUCTS_COPY_PASTE_FAMILY(subfamily_old[k].FAMILY_ID, subfamily_checkforsubproducts[k].FAMILY_ID, "FAMILY", allowduplicate, User.Identity.Name, previousCatalogID, catalogId, newcat, oldcat);
                                        _objCSEntities.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    // var catidsnewupdate = _objCSEntities.TB_CATEGORY_CHANGE_LIST.Where(a => a.OLD_PARENT_CATEGORY == copyCatId && a.MODIFIED_USER == User.Identity.Name).ToList();
                    var catidsnewupdate = _objCSEntities.TB_CATEGORY_CHANGE_LIST.Where(a => catids.Contains(a.OLD_PARENT_CATEGORY) && a.MODIFIED_USER == User.Identity.Name).OrderBy(a => a.CATEGORY_ID).ToList();
                    catidsnewupdate.ForEach(a => a.OLD_PARENT_CATEGORY = "Updated");
                    _objCSEntities.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                Logger.Error("Error at CategoryController : SubproductPaste", e);
                return false;
            }
        }


        private static string _previewCategoryId = string.Empty;
        private static int _previewcatalogId = 0;
        private static string imagemagickPath = "";
        //[Authorize(Roles = "Admin, SuperAdmin")]
        //public ActionResult Preview(string catId, int catalogId)
        //{
        //    _previewcatalogId = catalogId;
        //    _previewCategoryId = "\"" + catId + "\"";
        //    return View();
        //}

        //   [Authorize(Roles = "Admin, SuperAdmin")]


        public ActionResult PdfPreview(string category_id)
        {
           
            return View();

        }
        public ActionResult PdfPreviewFamily(string category_id)
        {
           
            return View();

        }

        public ActionResult PdfPreviewProduct(string category_id)
        {

            return View();

        }

        public ActionResult Preview(string catId, string catalogId, int userId)
        {
            var catId1 = AesEncrytDecry.DecryptStringAes(catId);
            var catalogId1 = AesEncrytDecry.DecryptIntAes(catalogId);
            _previewcatalogId = catalogId1;
            _previewCategoryId = catId1;
            var imagemagickpath = _objCSEntities.Customer_Settings.FirstOrDefault(a => a.CustomerId == userId);
            string converttype = _objCSEntities.Customer_Settings.Where(a => a.CustomerId == userId).Select(a => a.ConverterType).SingleOrDefault();
            //if (imagemagickpath != null)
            //{
            //    imagemagickPath = imagemagickpath.ImageConversionUtilityPath;
            //    imagemagickPath = imagemagickPath.Replace("\\\\", "\\");
            //}
            //else
            //{
            //    imagemagickPath = "C:\\Program Files\\ImageMagick-6.8.5-Q8";
            //}
            if (imagemagickpath != null && converttype == "Image Magic")
            {
                imagemagickPath = convertionPath;
                imagemagickPath = imagemagickPath.Replace("\\\\", "\\");
            }
            else
            {
                imagemagickPath = convertionPathREA;
                imagemagickPath = imagemagickPath.Replace("\\\\", "\\");
            }
            //_previewCategoryId = "\"" + catId1 + "\"" + catalogId1;
            return View();

        }

        //[Authorize(Roles = "Admin, SuperAdmin")]
        public ActionResult CategoryPreview()
        {
            try
            {
                CustomerFolder = _objCSEntities.Customers.Join(_objCSEntities.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
                var user_Id = _objCSEntities.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.Id).ToList()[0];
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }
                //StiOptions.Viewer.Windows.ShowSaveDocumentFileButton = true;

                //StiOptions.Viewer.Windows.ShowSaveDocumentFileButton = true;
                //var report = new StiReport();
                //report.Load(Server.MapPath("../Reports/LSCategoryPreview.mrt"));
                //((StiSqlDatabase)report.Dictionary.Databases["Connection"]).ConnectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                //string previewCatalogId = Convert.ToString(_previewcatalogId);
                //report.Dictionary.DataSources["TBL1"].Parameters["CatalogId"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["TBL1"].Parameters["CategoryId"].Expression = _previewCategoryId;
                //report.Dictionary.DataSources["TBL2"].Parameters["CatalogId"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["TBL2"].Parameters["CategoryId"].Expression = _previewCategoryId;
                //report.Dictionary.DataSources["TBL3"].Parameters["CatalogId"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["TBL3"].Parameters["CategoryId"].Expression = _previewCategoryId;
                //report.Dictionary.DataSources["TBL4"].Parameters["@CatalogId"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["TBL4"].Parameters["@CategoryId"].Expression = _previewCategoryId;
                //report.Dictionary.DataSources["MultipleTableSoruce"].Parameters["CATALOG_ID"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["MultipleTables"].Parameters["CATALOG_ID"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["ReferenceTableSource"].Parameters["CATALOG_ID"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["ReferenceTables"].Parameters["CATALOG_ID"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["ReferenceTables"].Parameters["PATH"].Expression = "\"C:\"";
                //report.Dictionary.DataSources["FamilySpecs"].Parameters["CatalogId"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["FamilySpecs"].Parameters["CategoryId"].Expression = _previewCategoryId;
                //report.Dictionary.Variables["ImagePath"].Value = Server.MapPath("~/Content/ProductImages/");
                //report.Dictionary.Variables["UnSupportedImagePath"].Value = Server.MapPath("~/Content/ProductImages/") + "\\Images\\unsupportedImageformat.jpg";
                //report.Dictionary.Synchronize();
                //report.Render();
                //var pdfSettings = new StiPdfExportSettings
                //{
                //    EmbeddedFonts = true,
                //    ImageQuality = 1f,
                //    ImageResolution = 300,
                //    Compressed = true,
                //    UseUnicode = true
                //};
                ////var report = new Report(Server.MapPath("~/Content/ProductImages/"), Server.MapPath("~/Content/ProductImages/") + "\\Images\\unsupportedImageformat.jpg", _previewcatalogId, _previewCategoryId, ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString)
                ////{
                ////    EngineVersion = Stimulsoft.Report.Engine.StiEngineVersion.EngineV1
                ////};
                ////report.Dictionary.Synchronize();
                ////report.Render(true);
                //return StiMvcViewerFx.GetReportSnapshotResult(Request, report);
                // CustomerFolder = "/";
                string converttype = _objCSEntities.Customer_Settings.Where(a => a.CustomerId == user_Id).Select(a => a.ConverterType).SingleOrDefault();
                StiOptions.Viewer.Windows.ShowSaveDocumentFileButton = true;
                //string un = Server.MapPath("~/Content/ProductImages/") + "\\Images\\unsupportedImageformat.jpg";
                StiReport report = new Reports(converttype,Server.MapPath("~/Content/ProductImages/" + CustomerFolder + "/"), Server.MapPath("~/Content/ProductImages/" + CustomerFolder + "/"), "", imagemagickPath, _previewcatalogId, _previewCategoryId, ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString)
               // StiReport report = new Reports(Server.MapPath("~/Content/ProductImages/" + CustomerFolder + "/"), Server.MapPath("~/Content/ProductImages/" + CustomerFolder + "/"), "", imagemagickPath, _previewcatalogId, _previewCategoryId, ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString)
                {
                    EngineVersion = Stimulsoft.Report.Engine.StiEngineVersion.EngineV2
                };
                report.CacheTotals = false;
                report.CacheAllData = false;
                report.ReportCacheMode = StiReportCacheMode.Off;
                report.Pages.CacheMode = false;
                StiOptions.Engine.ImageCache.Enabled = false;

                report.Dictionary.Synchronize();
                report.Render(true);

                // report.Load((Server.MapPath("../Reports/LSCategoryPreview.mrt"));
                //new return StiMvcViewerFx.GetReportSnapshotResult(Request, report);
                return StiMvcDesigner.PreviewReportResult(report);
                
                //return null;


            }

            catch (Exception objException)
            {
                Logger.Error("Error at CategoryController : Preview", objException);
                return null;
            }
        }

        //public ActionResult ViewerEvent()
        //{
        //    //return StiMvcViewer.ViewerEventResult(HttpContext);
        //}
        //public ActionResult Interaction()
        //{
        //    //return StiMvcViewer.InteractionResult(HttpContext);
        //}
        //// This is used to export the report.
        //public FileResult ExportResult()
        //{
        //    // Return the exported report file
        ////    return StiMvcViewerFx.ExportReportResult(Request);
        //}


        public bool RenameFromContextMenu(string currenttext, string rightClickedCategoryId, int catalogID)
        {
            try
            {
                if (!rightClickedCategoryId.Contains("~"))
                {
                    int userrolecount = _objCSEntities.TB_CATEGORY.Count(x => x.CATEGORY_ID == rightClickedCategoryId);
                    if (userrolecount != 0)
                    {
                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            objSqlConnection.Open();
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "UPDATE TB_CATEGORY SET CATEGORY_NAME='" + currenttext + "' WHERE CATEGORY_ID='" + rightClickedCategoryId + "'";
                            objSqlCommand.CommandType = CommandType.Text;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.ExecuteNonQuery();
                        }
                    }
                    //int userrolecount = _objCSEntities.TB_CATEGORY.Count(x => x.CATEGORY_ID == RightClickedCategoryId);
                    //if (userrolecount != 0)
                    //{
                    //    TB_CATEGORY objTB_CATEGORY = new TB_CATEGORY();
                    //    objTB_CATEGORY.CATEGORY_NAME = currenttext;
                    //    objTB_CATEGORY.CATEGORY_ID = RightClickedCategoryId;
                    //    _objCSEntities.TB_CATEGORY.Add(objTB_CATEGORY);
                    //    _objCSEntities.SaveChanges();
                    //}
                }
                else
                {
                    int copiedfamilyIdId;
                    int.TryParse(rightClickedCategoryId.Trim('~'), out copiedfamilyIdId);
                    int userrolecount = _objCSEntities.TB_FAMILY.Count(x => x.FAMILY_ID == copiedfamilyIdId);
                    if (userrolecount != 0)
                    {
                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            objSqlConnection.Open();
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "UPDATE TB_FAMILY SET FAMILY_NAME='" + currenttext + "' WHERE FAMILY_ID='" + copiedfamilyIdId + "'";
                            objSqlCommand.CommandType = CommandType.Text;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.ExecuteNonQuery();
                        }
                    }

                    //int userrolecount = _objCSEntities.TB_FAMILY.Count(x => x.FAMILY_ID == CopiedfamilyIdId);
                    //if (userrolecount != 0)
                    //{
                    //    TB_FAMILY objTB_FAMILY = new TB_FAMILY();
                    //    objTB_FAMILY.FAMILY_NAME = currenttext;
                    //    objTB_FAMILY.FAMILY_ID = CopiedfamilyIdId;
                    //    _objCSEntities.TB_FAMILY.Add(objTB_FAMILY);
                    //    _objCSEntities.SaveChanges();
                    //}
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at Category/RenameFromContextMenu", ex);
            }
            return true;
        }

        public string CloneasRootCategory(string Catid, string RightClickedCategoryId, int catalogId, int previousCatalogID, string option, string multipleFamilyids, string multipleFamilyidsForAnotherCatalog)
        {
            try
            {
                if (multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog.Trim() != "")
                {
                    if (catalogId == previousCatalogID)
                    {
                        var multipleids = multipleFamilyids.Split(',');

                        var RootCategory = new List<string>(multipleids);
                        var SubCategory = new List<string>();
                        var Subcategory_Check = new List<string>();
                        var RootCategory2 = new List<string>();
                        string cat_id = string.Empty;

                        for (int i = 0; i < multipleids.Length; i++)
                        {
                            string category_id = multipleids[i];
                            if (!category_id.Contains("~"))
                            {
                                var category_check = _objCSEntities.TB_CATEGORY.FirstOrDefault(a => a.CATEGORY_ID == category_id && a.PARENT_CATEGORY == "0");
                                if (category_check == null)
                                {
                                    var Sub_category_check = _objCSEntities.TB_CATEGORY.Where(a => a.PARENT_CATEGORY == category_id);

                                    if (Sub_category_check.Any())
                                    {
                                        SubCategory.Add(category_id);
                                        foreach (var item in Sub_category_check)
                                        {
                                            Subcategory_Check.Add(item.CATEGORY_ID);

                                        }

                                    }
                                    else
                                    {
                                        SubCategory.Add(category_id);

                                    }

                                }
                                else
                                {
                                    RootCategory.Remove(category_id);
                                    RootCategory2.Add(category_id);
                                }
                            }
                        }

                        if (SubCategory != null)
                        {
                            foreach (var item in Subcategory_Check)
                            {
                                SubCategory.Remove(item);
                            }
                            SubCategory.Add(cat_id);
                            if (RootCategory2.Count != 0)
                            {
                                multipleids = RootCategory2.ToArray();
                            }
                            else
                            {
                                multipleids = SubCategory.ToArray();
                            }
                        }

                        else
                        {
                            multipleids = RootCategory.ToArray();
                        }


                        if (multipleids.Length > 0)
                        {
                            foreach (var item in multipleids)
                            {
                                if (!item.Trim().Contains("~") && item.Trim() != "")
                                {
                                    string category_id = item.Trim();
                                    if (option.Trim() == "root")
                                    {
                                        var result = _objCSEntities.STP_CATALOGSTUDIO5_CLONE_Category_Family("0", category_id, catalogId, previousCatalogID);
                                        _objCSEntities.SaveChanges();
                                    }
                                    else
                                    {
                                        var result = _objCSEntities.STP_CATALOGSTUDIO5_CLONE_Category_Family(RightClickedCategoryId, category_id, catalogId, previousCatalogID);
                                        _objCSEntities.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var multipleids = multipleFamilyidsForAnotherCatalog.Split(',');
                        var RootCategory = new List<string>(multipleids);
                        var SubCategory = new List<string>();
                        var Subcategory_Check = new List<string>();
                        var RootCategory2 = new List<string>();
                        string cat_id = string.Empty;

                        for (int i = 0; i < multipleids.Length; i++)
                        {
                            string category_id = multipleids[i];
                            if (!category_id.Contains("~"))
                            {
                                var category_check = _objCSEntities.TB_CATEGORY.FirstOrDefault(a => a.CATEGORY_ID == category_id && a.PARENT_CATEGORY == "0");
                                if (category_check == null)
                                {
                                    var Sub_category_check = _objCSEntities.TB_CATEGORY.Where(a => a.PARENT_CATEGORY == category_id);

                                    if (Sub_category_check.Any())
                                    {
                                        SubCategory.Add(category_id);
                                        foreach (var item in Sub_category_check)
                                        {
                                            Subcategory_Check.Add(item.CATEGORY_ID);

                                        }

                                    }
                                    else
                                    {
                                        SubCategory.Add(category_id);

                                    }

                                }
                                else
                                {
                                    RootCategory.Remove(category_id);
                                    RootCategory2.Add(category_id);
                                }
                            }
                        }

                        if (SubCategory != null)
                        {
                            foreach (var item in Subcategory_Check)
                            {
                                SubCategory.Remove(item);
                            }
                            SubCategory.Add(cat_id);
                            if (RootCategory2.Count != 0)
                            {
                                multipleids = RootCategory2.ToArray();
                            }
                            else
                            {
                                multipleids = SubCategory.ToArray();
                            }
                        }

                        else
                        {
                            multipleids = RootCategory.ToArray();
                        }
                        if (multipleids.Length > 0)
                        {
                            foreach (var item in multipleids)
                            {
                                if (!item.Trim().Contains("~") && item.Trim() != "")
                                {
                                    string category_id = item.Trim();
                                    if (option.Trim() == "root")
                                    {
                                        var result = _objCSEntities.STP_CATALOGSTUDIO5_CLONE_Category_Family("0", category_id, catalogId, previousCatalogID);
                                        _objCSEntities.SaveChanges();
                                    }
                                    else
                                    {
                                        var result = _objCSEntities.STP_CATALOGSTUDIO5_CLONE_Category_Family(RightClickedCategoryId, category_id, catalogId, previousCatalogID);
                                        _objCSEntities.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (Catid.Trim() != "")
                    {

                        if (option.Trim() == "root")
                        {
                            var result = _objCSEntities.STP_CATALOGSTUDIO5_CLONE_Category_Family("0", Catid, catalogId, previousCatalogID);
                            _objCSEntities.SaveChanges();
                        }
                        else
                        {
                            var result = _objCSEntities.STP_CATALOGSTUDIO5_CLONE_Category_Family(RightClickedCategoryId, Catid, catalogId, previousCatalogID);
                            _objCSEntities.SaveChanges();
                        }
                    }
                }
                return "True";


            }
            catch (Exception objException)
            {
                if (objException.InnerException.Message.Contains("Violation of PRIMARY KEY"))
                {
                    return "Name already exists, please enter a different Name for Family";
                }
                else
                {
                    Logger.Error("Error at CategoryController : CutPasteCategory", objException);
                    return "False";
                }
            }
        }

        public int GetCategoryCount(string multipleFamilyids, string multipleFamilyidsForAnotherCatalog, string Option)
        {
            try
            {
                if (multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog.Trim() != "")
                {
                    var multipleids = multipleFamilyids.Split(',');
                    if (Option == "Category")
                    {
                        var RootCategory = new List<string>(multipleids);
                        var SubCategory = new List<string>();
                        var Subcategory_Check = new List<string>();
                        var RootCategory2 = new List<string>();
                        string cat_id = string.Empty;

                        for (int i = 0; i < multipleids.Length; i++)
                        {
                            string category_id = multipleids[i];
                            if (!category_id.Contains("~"))
                            {
                                var category_check = _objCSEntities.TB_CATEGORY.FirstOrDefault(a => a.CATEGORY_ID == category_id && a.PARENT_CATEGORY == "0" && a.FLAG_RECYCLE == "A");
                                if (category_check == null)
                                {
                                    var Sub_category_check = _objCSEntities.TB_CATEGORY.Where(a => a.PARENT_CATEGORY == category_id && a.FLAG_RECYCLE == "A");

                                    if (Sub_category_check.Any())
                                    {
                                        SubCategory.Add(category_id);
                                        foreach (var item in Sub_category_check)
                                        {
                                            Subcategory_Check.Add(item.CATEGORY_ID);

                                        }

                                    }
                                    else
                                    {
                                        SubCategory.Add(category_id);

                                    }

                                }
                                else
                                {
                                    RootCategory.Remove(category_id);
                                    RootCategory2.Add(category_id);
                                }
                            }
                        }

                        if (SubCategory != null)
                        {
                            foreach (var item in Subcategory_Check)
                            {
                                SubCategory.Remove(item);
                            }
                            //SubCategory.Add(cat_id);
                            if (RootCategory2.Count != 0)
                            {
                                multipleids = RootCategory2.ToArray();
                            }
                            else
                            {
                                multipleids = SubCategory.ToArray();
                            }
                        }

                        else
                        {
                            multipleids = RootCategory.ToArray();
                        }


                        if (multipleids.Length > 0)
                        {
                            return multipleids.Length;
                        }

                    }
                    else
                    {
                        int count = 0;
                        foreach (var item in multipleids)
                        {
                            if (item.Contains('~'))
                            {
                                int famid = Convert.ToInt32(item.Trim('~'));
                                var fam_Det = _objCSEntities.TB_FAMILY.Where(a => a.FAMILY_ID == famid && a.PARENT_FAMILY_ID == 0);
                                if (fam_Det.Any())
                                {
                                    count++;
                                }
                            }
                        }
                        return count;
                    }
                }


                return 0;


            }
            catch (Exception objException)
            {

                Logger.Error("Error at CategoryController : CutPasteCategory", objException);
                return 0;

            }
        }

        public string GetClonedFamilyDet(int Catalog_Id, string Family_Id, string Category_Id)
        {
            try
            {
                int family_id = 0;
                if (!string.IsNullOrEmpty(Family_Id))
                    family_id = Convert.ToInt32(Family_Id.Trim('~'));
                var clone_check = _objCSEntities.TB_CATALOG_FAMILY.Where(a => a.CATALOG_ID != 1 && a.FAMILY_ID == family_id && a.ROOT_CATEGORY == Category_Id);
                var subfamily = _objCSEntities.TB_SUBFAMILY.FirstOrDefault(x => x.SUBFAMILY_ID == family_id);
                if (clone_check.Any())
                {
                    if (subfamily != null)
                    {
                        return "2"; // assign master not enable
                    }
                    else
                        return "1"; //assign master enable
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception objException)
            {

                Logger.Error("Error at CategoryController : GetClonedFamilyDet", objException);
                return "";

            }

        }

        public ActionResult GetReport()

        {
            try
            {
                CustomerFolder = _objCSEntities.Customers.Join(_objCSEntities.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
                var user_Id = _objCSEntities.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.Id).ToList()[0];
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }
                //StiOptions.Viewer.Windows.ShowSaveDocumentFileButton = true;

                //StiOptions.Viewer.Windows.ShowSaveDocumentFileButton = true;
                //var report = new StiReport();
                //report.Load(Server.MapPath("../Reports/LSCategoryPreview.mrt"));
                //((StiSqlDatabase)report.Dictionary.Databases["Connection"]).ConnectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                //string previewCatalogId = Convert.ToString(_previewcatalogId);
                //report.Dictionary.DataSources["TBL1"].Parameters["CatalogId"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["TBL1"].Parameters["CategoryId"].Expression = _previewCategoryId;
                //report.Dictionary.DataSources["TBL2"].Parameters["CatalogId"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["TBL2"].Parameters["CategoryId"].Expression = _previewCategoryId;
                //report.Dictionary.DataSources["TBL3"].Parameters["CatalogId"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["TBL3"].Parameters["CategoryId"].Expression = _previewCategoryId;
                //report.Dictionary.DataSources["TBL4"].Parameters["@CatalogId"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["TBL4"].Parameters["@CategoryId"].Expression = _previewCategoryId;
                //report.Dictionary.DataSources["MultipleTableSoruce"].Parameters["CATALOG_ID"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["MultipleTables"].Parameters["CATALOG_ID"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["ReferenceTableSource"].Parameters["CATALOG_ID"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["ReferenceTables"].Parameters["CATALOG_ID"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["ReferenceTables"].Parameters["PATH"].Expression = "\"C:\"";
                //report.Dictionary.DataSources["FamilySpecs"].Parameters["CatalogId"].Expression = previewCatalogId;
                //report.Dictionary.DataSources["FamilySpecs"].Parameters["CategoryId"].Expression = _previewCategoryId;
                //report.Dictionary.Variables["ImagePath"].Value = Server.MapPath("~/Content/ProductImages/");
                //report.Dictionary.Variables["UnSupportedImagePath"].Value = Server.MapPath("~/Content/ProductImages/") + "\\Images\\unsupportedImageformat.jpg";
                //report.Dictionary.Synchronize();
                //report.Render();
                //var pdfSettings = new StiPdfExportSettings
                //{
                //    EmbeddedFonts = true,
                //    ImageQuality = 1f,
                //    ImageResolution = 300,
                //    Compressed = true,
                //    UseUnicode = true
                //};
                ////var report = new Report(Server.MapPath("~/Content/ProductImages/"), Server.MapPath("~/Content/ProductImages/") + "\\Images\\unsupportedImageformat.jpg", _previewcatalogId, _previewCategoryId, ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString)
                ////{
                ////    EngineVersion = Stimulsoft.Report.Engine.StiEngineVersion.EngineV1
                ////};
                ////report.Dictionary.Synchronize();
                ////report.Render(true);
                //return StiMvcViewerFx.GetReportSnapshotResult(Request, report);
                // CustomerFolder = "/";
                string converttype = _objCSEntities.Customer_Settings.Where(a => a.CustomerId == user_Id).Select(a => a.ConverterType).SingleOrDefault();
                StiOptions.Viewer.Windows.ShowSaveDocumentFileButton = true;
                //string un = Server.MapPath("~/Content/ProductImages/") + "\\Images\\unsupportedImageformat.jpg";
                StiReport report = new Reports(converttype, Server.MapPath("~/Content/ProductImages/" + CustomerFolder + "/"), Server.MapPath("~/Content/ProductImages/" + CustomerFolder + "/"), "", imagemagickPath, _previewcatalogId, _previewCategoryId, ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString)
                // StiReport report = new Reports(Server.MapPath("~/Content/ProductImages/" + CustomerFolder + "/"), Server.MapPath("~/Content/ProductImages/" + CustomerFolder + "/"), "", imagemagickPath, _previewcatalogId, _previewCategoryId, ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString)
                {
                    EngineVersion = Stimulsoft.Report.Engine.StiEngineVersion.EngineV2
                };
                report.CacheTotals = false;
                report.CacheAllData = false;
                report.ReportCacheMode = StiReportCacheMode.Off;
                report.Pages.CacheMode = false;
                StiOptions.Engine.ImageCache.Enabled = false;

                report.Dictionary.Synchronize();
                report.Render(true);

                // report.Load((Server.MapPath("../Reports/LSCategoryPreview.mrt"));
                //new return StiMvcViewerFx.GetReportSnapshotResult(Request, report);
                //return StiMvcDesigner.PreviewReportResult(report);
                return StiMvcViewer.GetReportResult(report);
                //return null;


            }

            catch (Exception objException)
            {
                Logger.Error("Error at CategoryController : Preview", objException);
                return null;
            }




            //StiReport report = new StiReport();

            //report.Load(Server.MapPath("~/Content/SimpleList.mrt"));

            //report.Load(Server.MapPath("~/Content/Dashboard.mrt"));



          

        }

        public ActionResult ViewerEvent()

        {

            return StiMvcViewer.ViewerEventResult();

        }



    }
}