﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Linq;
using System.Web.Mvc;
using LS.Data.Model.Accounts;
using LS.Data.Utilities;
using System.Web.Security;
using log4net;
using System.Configuration;
using LS.Data;
using LS.Web.Resources;
using System.Web;
using System.Web.Routing;
using LS.Data.Model;
using System.Collections.Generic;
using Stimulsoft.Report.Export;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc.Html;
using System.Security.Cryptography;
using System.Net.Mail;


namespace LS.Web.Controllers
{
    public class AppController : Controller
    {
        readonly CSEntities _objLs = new CSEntities();
        private static ILog _logger;
        private string _sqlString = "";
        public AppController()
        {
            _logger = LogManager.GetLogger(typeof(AppController));

        }


        #region "Category Controller"

        //  [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult CreateCategory()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            return View("~/Views/App/Category/Category.cshtml");
        }

        #endregion
        // [Authorize(Roles = "AdminNormalUser,SuperAdmin")]
        public ActionResult Dashboard()
        {
            //To Get the ProductTitle from webconfig.
            Session["ProductTitle"] = ConfigurationManager.AppSettings["productTitle"];

            //  get the Cutomerwise Catalog Item Value.

            var CatalogItemNumber = _objLs.Customers.Join(_objLs.Customer_User, c => c.CustomerId, cu => cu.CustomerId, (c, cu) => new { c, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(x => new { x.c.CustomizeItemNo, x.c.CustomizeSubItemNo }).ToList();

            if (CatalogItemNumber.Count != 0)
            {
                if (string.IsNullOrEmpty(CatalogItemNumber[0].CustomizeItemNo))
                {
                    Session["CustomerItemNo"] = "ITEM#";
                }
                else
                {
                    Session["CustomerItemNo"] = CatalogItemNumber[0].CustomizeItemNo;
                }
                if (string.IsNullOrEmpty(CatalogItemNumber[0].CustomizeSubItemNo))
                {
                    Session["CustomerSubItemNo"] = "SUBITEM#";
                }
                else
                {
                    Session["CustomerSubItemNo"] = CatalogItemNumber[0].CustomizeSubItemNo;
                }

            }
            else
            {
                Session["CustomerItemNo"] = "ITEM#";
                Session["CustomerSubItemNo"] = "SUBITEM#";
            }




            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            else
            {
                var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                           .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                           .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                           .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
                if (SuperAdminCheck.Any())
                {
                    var superAdminName = _objLs.aspnet_Roles.Join(_objLs.Customers,
                    x => x.Prefix_CompanyName,
                    y => y.CompanyName, (x, y)
                        => new { x, y })
                    .Where(z => z.x.Role_id == 1)
                    .Select(y => new { y.y.CustomerName }).ToList().FirstOrDefault();
                    if (superAdminName != null)
                    {
                        TempData["WelcomeName"] = superAdminName.CustomerName;
                        TempData.Keep("WelcomeName");
                    }
                    return View("~/Views/App/Admin/AdminDashboard_.cshtml");
                }
                else
                {
                    var customerName = _objLs.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => new { x.CustomerId, x.FirstName, x.LastName })
                        .ToList().FirstOrDefault();
                    if (customerName != null)
                    {
                        TempData["WelcomeName"] = Convert.ToString(customerName.FirstName) + " " + Convert.ToString(customerName.LastName);
                        TempData.Keep("WelcomeName");
                    }
                    return View("~/Views/App/Admin/MainDashboard.cshtml");
                }
            }

        }

        //  [CustomAuthorizeAttribute("CustomerAdmin", "CustomerNormalUser")]
        public ActionResult CustomerDashboard()
        {
            Session["ProductTitle"] = ConfigurationManager.AppSettings["productTitle"];


            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            else
            {
                var UserList = _objLs.aspnet_Users.Where(
                                          z => z.UserName == User.Identity.Name && z.IsAnonymous == false)
                                          .Select(x => x.UserName)
                                          .ToList();
                if (UserList.Any())
                {
                    return View("~/Views/App/Admin/Dashboard.cshtml");
                }
                else
                {
                    Session.Abandon();
                    FormsAuthentication.SignOut();
                    return RedirectToAction("LogOn", "Accounts");
                }
            }

        }
        //    [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult AdminDashboard()
        {



            Session["ProductTitle"] = ConfigurationManager.AppSettings["productTitle"];

            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            else
            {
                var UserList = _objLs.aspnet_Users.Where(
                                          z => z.UserName == User.Identity.Name && z.IsAnonymous == false)
                                          .Select(x => x.UserName)
                                          .ToList();
                if (UserList.Any())
                {
                    TempData.Keep("WelcomeName");
                    return View("~/Views/App/Admin/Dashboard.cshtml");
                }
                else
                {
                    Session.Abandon();
                    FormsAuthentication.SignOut();
                    return RedirectToAction("LogOn", "Accounts");
                }
            }

        }

        public ActionResult Schedule(string value)
        {
            Session["ProductTitle"] = ConfigurationManager.AppSettings["productTitle"];
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            else
            {
                var UserList = _objLs.aspnet_Users.Where(
                                          z => z.UserName == User.Identity.Name && z.IsAnonymous == false)
                                          .Select(x => x.UserName)
                                          .ToList();
                if (UserList.Any())
                {
                    ViewBag.Date = value;
                    return View("~/Views/App/Admin/Schedule.cshtml");
                }
                else
                {
                    Session.Abandon();
                    FormsAuthentication.SignOut();
                    return RedirectToAction("LogOn", "Accounts");
                }
            }

        }

        [Authorize(Roles = "SuperAdmin ,Studiosoft_SuperAdmin")]
        public ActionResult AdminConfig()
        {
            Session["ProductTitle"] = ConfigurationManager.AppSettings["productTitle"];
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            TempData.Keep("WelcomeName");
            return View("~/Views/App/Admin/AdminConfig.cshtml");
        }

        // [CustomAuthorizeAttribute("AdminNormalUser", "SuperAdmin")]
        public ActionResult Catalog()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {

                return View("~/Views/App/Catalog/Catalog.cshtml");
            }

            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 40201).ToList();
            if (rolefunctionresults.Count > 0)
            {
                return View("~/Views/App/Catalog/Catalog.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }

        //[Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult Attribute()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                   .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                   .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                       (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Attribute/Attribute.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 40204).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Attribute/Attribute.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }
        //[Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult Reports()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                   .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                   .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                       (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Reports/ReportsNew.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000080).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Reports/ReportsNew.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }

        // [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult Summary()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Reports/Summary.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000082).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Reports/Summary.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }
        public ActionResult DataCompleteness()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Reports/DataCompleteness.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000082).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Reports/DataCompleteness.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }

        //[Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult ColumnSetup()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            return View("~/Views/App/ColumnSetup/ColumnSetup.cshtml");
        }
        //[Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult Search()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                //    return View("~/Views/App/Search/Search.cshtml");
                return View("~/Views/App/AdvancedSearch/Search.cshtml");
            }
            var rolefunctionresults =
                                 _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000083).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                //    return View("~/Views/App/Search/Search.cshtml");
                return View("~/Views/App/AdvancedSearch/Search.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }
        }
        public ActionResult ImageManagement()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                //    return View("~/Views/App/Search/Search.cshtml");
                return View("~/Views/ImageManagement/Manage.cshtml");
            }
            var rolefunctionresults =
                                 _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000084).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                //    return View("~/Views/App/Search/Search.cshtml");
                return View("~/Views/ImageManagement/Manage.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }
        }
        //[Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult Customers()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            return View("~/Views/App/Admin/Customers.cshtml");
        }

        // [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult PickList()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Picklist/PickList.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 40205).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Picklist/PickList.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }
        //[Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult Reference()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Reference/Referencetable.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 4020403).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Reference/Referencetable.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }

        // [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult WorkFlow()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            TempData.Keep("WelcomeName");
            return View("~/Views/App/WorkFlow/WorkflowFilter.cshtml");
        }


        // [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult Supplier()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                   .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                   .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                       (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Supplier/Supplier.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 40206).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Supplier/Supplier.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }

        // [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult Export()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }


            var projectdetails = _objLs.TB_PROJECT.ToList();
            bool isAvailable = projectdetails.Any(a => a.PROJECT_NAME == "Export");
            if (isAvailable)
            {
                TB_PROJECT tProject = _objLs.TB_PROJECT.Where(x => x.PROJECT_NAME == "Export").FirstOrDefault();
                _objLs.TB_PROJECT.Remove(tProject);
                _objLs.SaveChanges();
            }

            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Export/Export.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 40303).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Export/Export.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }
        // [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult Indesign()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Indesign/Indesign.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000070).ToList();
            var rolefunctionresults1 = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000071).ToList();
            var rolefunctionresults2 = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000072).ToList();
            if (rolefunctionresults.Count > 0 || rolefunctionresults1.Count > 0 || rolefunctionresults2.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Indesign/Indesign.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }
        // [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult XpressCatalog()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                   .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                   .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                       (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/XpressCatalog/XpressCatalog.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000010).ToList();
            var rolefunctionresults1 = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000012).ToList();
            var rolefunctionresults2 = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000013).ToList();
            var rolefunctionresults3 = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000014).ToList();
            if (rolefunctionresults.Count > 0 || rolefunctionresults1.Count > 0 || rolefunctionresults2.Count > 0 || rolefunctionresults3.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/XpressCatalog/XpressCatalog.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }
        // [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult ReportLog()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                     .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                     .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                         (tcptps, tcp) => new { tcptps, tcp })
                     .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/SqlReports/Reports.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000081).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/SqlReports/Reports.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }
        // [CustomAuthorizeAttribute("SuperAdmin")]
        public ActionResult Preference()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            return View("~/Views/App/Preference/PreferenceDetails.cshtml");
        }

        // [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult Inverted()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                   .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                   .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                       (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Admin/Dashboard.cshtml");
                //return View("~/Views/App/Inverted/InvertedProducts.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000030).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Admin/Dashboard.cshtml");
                //return View("~/Views/App/Inverted/InvertedProducts.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }

        public ActionResult InvertedSub()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {

                return View("~/Views/App/Inverted/Dashboard.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 5000031).ToList();
            if (rolefunctionresults.Count > 0)
            {
                return View("~/Views/App/Inverted/Dashboard.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }

        public ActionResult SubProductsImport()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {

                return View("~/Views/App/Import/SubProductImport.cshtml");
            }
            var rolefunctionresults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 40303).ToList();
            if (rolefunctionresults.Count > 0)
            {
                return View("~/Views/App/Import/SubProductImport.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("LogOn", "Accounts");
            }

        }

        public ActionResult BasicImport()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var superAdminCheck =
                _objLs.vw_aspnet_UsersInRoles.Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId,
                    (tps, tpf) => new { tps, tpf })
                    .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                        (tcptps, tcp) => new { tcptps, tcp })
                    .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1)
                    .ToList();
            if (superAdminCheck.Any())
            {
                return View("~/Views/App/Import/BasicImport.cshtml");
            }
            var roleFunctionResults = _objLs.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 40303).ToList();
            if (roleFunctionResults.Count > 0)
            {
                return View("~/Views/App/Import/BasicImport.cshtml");
            }
            else
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("logOn", "Account");
            }

        }
        public ActionResult EPayment()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            return View("~/Views/App/Epayment/Epayment.cshtml");
        }
        public ActionResult Success()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            return View("~/Views/App/Epayment/Epayment.cshtml");
        }
        public ActionResult Ps()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            return View("~/Views/App/Product Specs/ProductGrid.cshtml");
        }

        public ActionResult Product()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            return View("~/Views/App/Product Specs/CKEDITOR.cshtml");
        }
        [Authorize(Users = "*")]
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            try
            {
                var fm = new ForgorPasswordModel();
                return View("~/Views/Accounts/ForgotPassword.cshtml", fm);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
            return null;
        }

        [Authorize(Users = "*")]
        [AllowAnonymous]
        public ActionResult VersionHistory()
        {
            try
            {
                return View("~/Views/Version/index.cshtml");
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
            return null;
        }


        #region Register_Original
        //[Authorize(Users = "*")]
        //[AllowAnonymous]
        //public ActionResult Register()
        //{
        //    //ViewBag.Companies = _objLs.Customers.Where(x => (x.CompanyName != string.Empty && x.CompanyName != null && x.CompanyName != "Studiosoft")).Select(x => x).Distinct().ToList();
        //    var States = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == "Select").Select(x => new { x.STATE_CODE, x.STATE }).ToList();

        //    ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
        //    ViewBag.States = new SelectList(States, "STATE_CODE", "STATE");
        //    var model = new LS.Data.Model.Accounts.UserModel();
        //    if (Session["CompanyID"] != null)
        //    {
        //        if (!string.IsNullOrEmpty(Session["CompanyID"].ToString()))
        //        {
        //            model.CompanyName = Session["CompanyID"].ToString();
        //            Session["CompanyID"] = string.Empty;
        //        }
        //    }
        //    return View("~/Views/Accounts/Register.cshtml", model);
        //}

        //[HttpPost]
        //[Authorize(Users = "*")]
        //[AllowAnonymous]
        //public ActionResult Register(UserModel model)
        //{
        //    try
        //    {
        //        int company_id = 0;
        //        if (ModelState.IsValid)
        //         {
        //            MembershipCreateStatus createStatus;
        //            var emailchk = _objLs.aspnet_Membership.Where(z => z.Email == model.Email).Select(x => x.UserId).ToList();

        //            if (emailchk.Any())
        //            {
        //                ModelState.AddModelError("", "The email address you have entered is already registered.");
        //                //ViewBag.Companies = _objLs.Customers.Where(x => (x.CompanyName != string.Empty && x.CompanyName != null && x.CompanyName != "Studiosoft")).Select(x => x).Distinct().ToList();
        //                var objStates = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == model.Country).Select(x => new { x.STATE_CODE, x.STATE }).ToList();
        //                ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
        //                ViewBag.States = new SelectList(objStates, "STATE_CODE", "STATE", model.State);
        //                return View("~/Views/Accounts/Register.cshtml", model);
        //            }

        //            Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);

        //            if (createStatus == MembershipCreateStatus.Success)
        //            {
        //                var profile = ProfileModel.GetProfile(model.UserName);
        //                profile.CustomerId = model.CustomerId;
        //                profile.FirstName = model.FirstName;
        //                profile.LastName = model.LastName;
        //                profile.Title = model.Title;
        //                profile.Address1 = model.Address1;
        //                profile.Address2 = model.Address2;
        //                profile.City = model.City;
        //                profile.State = model.State;
        //                profile.Zip = model.Zip;
        //                profile.Country = model.Country;
        //                profile.Phone = model.Phone;
        //                profile.Fax = model.Fax;
        //                profile.EMail = model.Email;
        //                profile.Comments = model.Comments;
        //                profile.DateUpdated = DateTime.Now;
        //                profile.DateCreated = DateTime.Now;
        //                profile.Save();

        //                SaveCustomerData(model);
        //                _objLs.SaveChanges();

        //                var objuser = _objLs.aspnet_Users.FirstOrDefault(x => x.UserName == model.UserName);

        //                if (objuser != null)
        //                {

        //                    objuser.IsAnonymous = true;

        //                }
        //                _objLs.SaveChanges();


        //                company_id = Convert.ToInt16(model.CompanyName);
        //                var customer_name1 = _objLs.Customers.Where(z => z.CustomerId == company_id).Select(x => x.CompanyName).ToList();
        //                // string customer_name =  customerList.Select(x => x.CompanyName).FirstOrDefault();
        //                if (customer_name1.Any())
        //                {
        //                    string customer_name = Convert.ToString(customer_name1[0]);
        //                    customer_name = customer_name.Replace("_", "").Replace(" ", "") + "_Admin";
        //                    int roleid = Convert.ToInt16(_objLs.aspnet_Roles.Where(x => x.RoleName == customer_name).Select(y => y.Role_id).FirstOrDefault());

        //                    int result = NewSqlUser(model.UserName, model.Password, roleid);

        //                    if (result == 1)
        //                    {
        //                        var result1 = _objLs.STP_CATALOGSTUDIO5_AddNewUser(model.UserName, "InActive", roleid);
        //                        _objLs.SaveChanges();
        //                    }

        //                    ViewBag.result = "Registered Successfully. Your account will be active once approved by the admin";

        //                    //ViewBag.Companies = _objLs.Customers.Where(x => (x.CompanyName != string.Empty && x.CompanyName != null && x.CompanyName != "Studiosoft")).Select(x => x).Distinct().ToList();
        //                    var States = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == "Select").Select(x => new { x.STATE_CODE, x.STATE }).ToList();

        //                    ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
        //                    ViewBag.States = new SelectList(States, "STATE_CODE", "STATE");
        //                    if (Session["CompanyID"] != null)
        //                    {
        //                        if (!string.IsNullOrEmpty(Session["CompanyID"].ToString()))
        //                        {
        //                            model.CompanyName = Session["CompanyID"].ToString();
        //                            Session["CompanyID"] = string.Empty;

        //                        }
        //                    }
        //                    UserModel usermodel = new UserModel();
        //                    return View("~/Views/Accounts/Register.cshtml", usermodel);

        //                }
        //                else
        //                { ModelState.AddModelError("", ErrorCodeToString(createStatus)); }
        //                //ViewBag.Message = "Registered Successfully. Your account will be active once approved by the admin";
        //                //return View("~/Views/Accounts/Register.cshtml");
        //            }
        //            ModelState.AddModelError("", ErrorCodeToString(createStatus));
        //        }
        //        //ViewBag.Companies = _objLs.Customers.Where(x => (x.CompanyName != string.Empty && x.CompanyName != null && x.CompanyName != "Studiosoft")).Select(x => x).Distinct().ToList();
        //        var ObjStates = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == model.Country).Select(x => new { x.STATE_CODE, x.STATE }).ToList();
        //        ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
        //        ViewBag.States = new SelectList(ObjStates, "STATE_CODE", "STATE", model.State);
        //        if (Session["CompanyID"] != null)
        //        {
        //            if (!string.IsNullOrEmpty(Session["CompanyID"].ToString()))
        //            {
        //                model.CompanyName = Session["CompanyID"].ToString();
        //                Session["CompanyID"] = string.Empty;
        //            }
        //        }

        //        return View("~/Views/Accounts/Register.cshtml", model);
        //    }

        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //    }
        //    return View("~/Views/Accounts/Register.cshtml", model);
        //}
        #endregion


        [Authorize(Users = "*")]
        [AllowAnonymous]
        public ActionResult Register()
        {

            if (Request.QueryString.Count > 0 && Request.QueryString["CustomerId"] != null && Request.QueryString["ID"] != null)
            {



                UserModel model = new UserModel();
                int userInviteId = Convert.ToInt32(CommonUtil.Decrypt(Request.QueryString["ID"]));
                var email = _objLs.TB_USERINVITE.Where(x => x.ID == userInviteId).Select(x => x.EMAIL).FirstOrDefault();
                var emailchk = _objLs.aspnet_Membership.Where(z => z.Email == email).Select(x => x.UserId).ToList();
                if (emailchk.Any())
                {
                    //ModelState.AddModelError("", "User already Registered.");
                    ViewBag.result = "User already Registered";
                    ModelState.Clear();
                    var StatesExist = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == "Select").Select(x => new { x.STATE_CODE, x.STATE }).ToList();
                    ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                    ViewBag.States = new SelectList(StatesExist, "STATE_CODE", "STATE");
                    return View("~/Views/Accounts/Register.cshtml", model);
                }
                int customerid = Convert.ToInt32(CommonUtil.Decrypt(Request.QueryString["CustomerId"]));


                var companyname = _objLs.Customers.Where(x => x.CustomerId == customerid).Select(x => x.CompanyName).FirstOrDefault();
                model.CustomerId = customerid;
                model.CompanyName = companyname;
                model.Email = email;

                var States = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == "Select").Select(x => new { x.STATE_CODE, x.STATE }).ToList();
                ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                ViewBag.States = new SelectList(States, "STATE_CODE", "STATE");
                if (Session["CompanyID"] != null)
                {
                    if (!string.IsNullOrEmpty(Session["CompanyID"].ToString()))
                    {
                        model.CompanyName = Session["CompanyID"].ToString();
                        Session["CompanyID"] = string.Empty;
                    }
                }
                if (companyname != null)
                {
                    return View("~/Views/Accounts/Register.cshtml", model);
                }
            }


            var objStates = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == "Select").Select(x => new { x.STATE_CODE, x.STATE }).ToList();
            ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
            ViewBag.States = new SelectList(objStates, "STATE_CODE", "STATE");
            var usermodel = new LS.Data.Model.Accounts.UserModel();
            if (Session["CompanyID"] != null)
            {
                if (!string.IsNullOrEmpty(Session["CompanyID"].ToString()))
                {
                    usermodel.CompanyName = Session["CompanyID"].ToString();
                    Session["CompanyID"] = string.Empty;
                }
            }
            return View("~/Views/Accounts/Register.cshtml", usermodel);
        }




        [HttpPost]
        [Authorize(Users = "*")]
        [AllowAnonymous]
        public ActionResult Register(UserModel model)
        {
            try
            {

                int company_id = 0;
                if (ModelState.IsValid)
                {
                    model.UserName = model.Email;

                    if (model.CustomerId == 0 || model.CustomerId == null)
                    {
                        MembershipCreateStatus createStatus;
                        if (model.Email != null && model.Email.ToString() != string.Empty)
                        {


                            var emailchk = _objLs.aspnet_Membership.Where(z => z.Email == model.Email).Select(x => x.UserId).ToList();

                            if (emailchk.Any())
                            {
                                ModelState.AddModelError("", "A user name for that e-mail address already exists, please enter a different e-mail address");

                                //ViewBag.Companies = _objLs.Customers.Where(x => (x.CompanyName != string.Empty && x.CompanyName != null && x.CompanyName != "Studiosoft")).Select(x => x).Distinct().ToList();
                                var objStates = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == model.Country).Select(x => new { x.STATE_CODE, x.STATE }).ToList();
                                ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                                ViewBag.States = new SelectList(objStates, "STATE_CODE", "STATE", model.State);
                                return View("~/Views/Accounts/Register.cshtml", model);
                            }
                        }

                        //Save company 
                        TB_STORE_SESSION objStore;
                        int roleIdreturn = 0;
                        string customer_name = "";
                        int customer_id = 0;
                        string roleId = "";
                        // int custid = _objLs.Customers.Max(x => x.CustomerId);
                        // custid = custid + 1;
                        if (model.CompanyName != null && model.CompanyName.ToString() != string.Empty)
                        {
                            var customersavail = _objLs.Customers.Where(x => x.CompanyName == model.CompanyName);
                            var planid = _objLs.TB_PLAN.Where(x => x.PLAN_NAME == "One Month subscribe").Select(x => x.PLAN_ID).FirstOrDefault();
                            if (!customersavail.Any())
                            {
                                Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);
                                if (createStatus == MembershipCreateStatus.Success)
                                {
                                    Customer objcustomer = new Customer();
                                    //CustomerId = 17,
                                    objcustomer.PlanId = planid;
                                    objcustomer.CustomerName = (model.FirstName + " " + model.LastName);
                                    objcustomer.ContactTitle = "";
                                    objcustomer.ContactName = (model.FirstName + " " + model.LastName);
                                    objcustomer.CompanyName = model.CompanyName;
                                    objcustomer.ThemeName = "metro";
                                    objcustomer.BannerImageURL = "BlueLogo";
                                    objcustomer.Address1 = model.Address1;
                                    objcustomer.Address2 = model.Address2;
                                    objcustomer.City = model.City;
                                    objcustomer.StateProvince = model.State;
                                    objcustomer.Zip = model.Zip;
                                    objcustomer.Country = model.Country;
                                    objcustomer.Phone = model.Phone;
                                    objcustomer.Fax = model.Fax;
                                    objcustomer.EMail = model.Email;
                                    objcustomer.Comments = model.Comments;
                                    objcustomer.WebSite = model.WebSite;
                                    objcustomer.IsActive = true;
                                    objcustomer.IsApproved = true;
                                    objcustomer.CustomizeItemNo = "ITEM#";
                                    objcustomer.CustomizeSubItemNo = "SUBITEM#";
                                    objcustomer.DateCreated = DateTime.Now;
                                    objcustomer.DateUpdated = DateTime.Now;

                                    _objLs.Customers.Add(objcustomer);
                                    _objLs.SaveChanges();

                                    int custid = _objLs.Customers.Max(x => x.CustomerId);
                                    //custid = custid + 1;
                                    Session["CompanyID"] = custid.ToString(CultureInfo.InvariantCulture);

                                    var objCustomerSettings = new Customer_Settings
                                    {
                                        CustomerId = custid,
                                        DefaultAppStyle = "Black",
                                        DisplayIDColumns = false,
                                        ErrorLogFilePath = "C:",
                                        Temporary_File_Path = "C:",
                                        AllowDuplicateItem_PartNum = false,
                                        TechSupportURL = "C:",
                                        SpellCheckerMode = "None",
                                        WebcatSyncURL = "C:",
                                        WhiteboardWebserviceURL = "C:",
                                        ShowCustomAPPS = true,
                                        EnableWorkFlow = false,
                                        ProductLevelMultitablePreview = true,
                                        FamilyLevelMultitablePreview = true,
                                        SKUAlertPercentage = 90,
                                        AssetLimitPercentage = 90,
                                        DisplaySkuProductCountInAlert = true,
                                        Export_ImportPath = "C:",
                                        ImagePath = "C:",
                                        ImageConversionUtilityPath = @"C:\Program Files (x86)\ImageMagick-6.8.8-Q8",
                                        WebCatImagePath = "C:",
                                        ReferenceTableImagePath = "C:",
                                        ImageConversionSize1Width = 0,
                                        ImageConversionSize1Height = 0,
                                        ImageConversionSize1Folder = "",
                                        ImageConversionSize2Width = 0,
                                        ImageConversionSize2Height = 0,
                                        ImageConversionSize2Folder = "",
                                        ImageConversionSize3Width = 0,
                                        ImageConversionSize3Height = 0,
                                        ImageConversionSize3Folder = "",
                                        ImageConversionSize4Width = 0,
                                        ImageConversionSize4Height = 0,
                                        ImageConversionSize4Folder = "",
                                        ImageConversionSize5Width = 0,
                                        ImageConversionSize5Height = 0,
                                        ImageConversionSize5Folder = "",
                                        ImageConversionSize6Width = 0,
                                        ImageConversionSize6Height = 0,
                                        ImageConversionSize6Folder = "",
                                        DisplayCategoryIdinNavigator = false,
                                        DisplayFamilyandRelatedFamily = true,
                                        UploadImagesPDFForWebSync = false,
                                        CreateXMLForSelectedAttributesToAllFamily = false,
                                        DateCreated = DateTime.Now,
                                        DateUpdated = DateTime.Now,
                                        bannerpath = "top_bg.jpg",
                                        logopath = null,
                                        Theme = "blueopal",
                                        EnableSubProduct = true
                                    };
                                    _objLs.Customer_Settings.Add(objCustomerSettings);
                                    _objLs.SaveChanges();

                                    TB_PLAN objPlan;
                                    objStore = new TB_STORE_SESSION { ID = custid };
                                    _objLs.TB_STORE_SESSION.Add(objStore);
                                    if (objStore != null)
                                    {
                                        objStore.PID = model.PlanId;
                                        objPlan = _objLs.TB_PLAN.FirstOrDefault(x => x.PLAN_ID == model.PlanId);
                                        if (objPlan != null)
                                        {
                                            objStore.COL1 = LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(objPlan.SKU_COUNT));
                                            objStore.COL2 = LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(objPlan.NO_OF_USERS));
                                            objStore.COL3 = LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(objPlan.SUBSCRIPTION_IN_MONTHS));
                                        }
                                    }


                                    _objLs.SaveChanges();

                                    customer_name = model.CompanyName.Replace(" ", "").Replace("_", "");
                                    customer_id = custid;

                                    roleId = customer_name + "_" + "Admin";
                                    var tbroleCheck = _objLs.TB_ROLE.Where(a => a.ROLE_NAME == roleId);
                                    if (!tbroleCheck.Any())
                                    {
                                        var result = _objLs.STP_CATALOGSTUDIO5_AddNewRole(roleId);
                                        _objLs.SaveChanges();
                                        if (result != null)
                                        {
                                            int rid = Convert.ToInt32(result);
                                            var roleResult = _objLs.TB_ROLE.FirstOrDefault(a => a.ROLE_NAME == roleId);
                                            if (roleResult != null)
                                            {
                                                UserRolesList role = new UserRolesList();
                                                var aspnetrolescheck = _objLs.aspnet_Roles.Where(x => x.Role_id == roleResult.ROLE_ID);
                                                if (!aspnetrolescheck.Any())
                                                {
                                                    //var objAspnetRoles = new aspnet_Roles
                                                    //{
                                                    //    RoleName = roleResult.ROLE_NAME,
                                                    //    Role_id = roleResult.ROLE_ID
                                                    //};
                                                    _objLs.aspnet_Roles_CreateRole("/", roleId);
                                                    var aspnetrolescheck1 = _objLs.aspnet_Roles.FirstOrDefault(x => x.RoleName == roleId);
                                                    if (aspnetrolescheck1 != null)
                                                    {
                                                        roleIdreturn = roleResult.ROLE_ID;
                                                        aspnetrolescheck1.Role_id = roleResult.ROLE_ID;
                                                        aspnetrolescheck1.RoleType = 2;
                                                        aspnetrolescheck1.CustomerId = customer_id;

                                                        var objCustomercatalogs = _objLs.Customer_Catalog.Where(x => x.CustomerId == customer_id);
                                                        if (objCustomercatalogs.Any())
                                                        {
                                                            foreach (var objCustomercatalog in objCustomercatalogs)
                                                            {
                                                                var rolecatalog = _objLs.Customer_Role_Catalog.Where(x => x.CATALOG_ID == objCustomercatalog.CATALOG_ID && x.RoleId == roleIdreturn &&
                                                                            x.CustomerId == customer_id);
                                                                if (!rolecatalog.Any())
                                                                {
                                                                    var objcustomerrolecatalog = new Customer_Role_Catalog
                                                                    {
                                                                        RoleId = roleIdreturn,
                                                                        CustomerId = customer_id,
                                                                        CATALOG_ID = objCustomercatalog.CATALOG_ID,
                                                                        DateCreated = DateTime.Now,
                                                                        DateUpdated = DateTime.Now
                                                                    };
                                                                    _objLs.Customer_Role_Catalog.Add(objcustomerrolecatalog);
                                                                }
                                                            }
                                                        }

                                                        _objLs.SaveChanges();
                                                    }


                                                }
                                            }
                                        }
                                        _objLs.SaveChanges();
                                    }

                                    int Customer_Id = _objLs.Customers.Max(x => x.CustomerId);
                                    //Save Users
                                    var profile = ProfileModel.GetProfile(model.UserName);
                                    //profile.CustomerId = model.CustomerId;
                                    profile.CustomerId = Customer_Id;
                                    profile.FirstName = model.FirstName;
                                    profile.LastName = model.LastName;
                                    profile.Title = model.Title;
                                    profile.Address1 = model.Address1;
                                    profile.Address2 = model.Address2;
                                    profile.City = model.City;
                                    profile.State = model.State;
                                    profile.Zip = model.Zip;
                                    profile.Country = model.Country;
                                    profile.Phone = model.Phone;
                                    profile.Fax = model.Fax;
                                    profile.EMail = model.Email;
                                    profile.Comments = model.Comments;
                                    profile.DateUpdated = DateTime.Now;
                                    profile.DateCreated = DateTime.Now;
                                    profile.Save();

                                    SaveCustomerData(model);
                                    _objLs.SaveChanges();

                                    //Save Email to User Invite
                                    var userinvite = new TB_USERINVITE
                                    {
                                        CUSTOMERID = Customer_Id,
                                        EMAIL = model.Email,
                                        USER_STATUS = "Registered",
                                        DATECREATED = DateTime.Now,
                                        DATEUPDATED = DateTime.Now,
                                    };
                                    _objLs.TB_USERINVITE.Add(userinvite);
                                    _objLs.SaveChanges();
                                    //End Save Email to UserInvite
                                    var objuser = _objLs.aspnet_Users.FirstOrDefault(x => x.UserName == model.UserName);

                                    if (objuser != null)
                                    {

                                        objuser.IsAnonymous = false;

                                    }
                                    _objLs.SaveChanges();
                                    //company_id = Convert.ToInt16(model.CompanyName);
                                    company_id = _objLs.Customers.Max(x => x.CustomerId);

                                    var customer_name1 = _objLs.Customers.Where(z => z.CustomerId == company_id).Select(x => x.CompanyName).ToList();
                                    // string customer_name =  customerList.Select(x => x.CompanyName).FirstOrDefault();
                                    if (customer_name1.Any())
                                    {
                                        customer_name = Convert.ToString(customer_name1[0]);
                                        customer_name = customer_name.Replace("_", "").Replace(" ", "") + "_Admin";
                                        int roleid = Convert.ToInt16(_objLs.aspnet_Roles.Where(x => x.RoleName == customer_name).Select(y => y.Role_id).FirstOrDefault());

                                        int result = NewSqlUser(model.UserName, model.Password, roleid);
                                        if (result == 1)
                                        {
                                            var result1 = _objLs.STP_CATALOGSTUDIO5_AddNewUser(model.UserName, "Active", roleid);
                                            _objLs.SaveChanges();
                                        }

                                        MembershipUser currentUser = Membership.GetUser(model.UserName);
                                        if (currentUser != null)
                                        {

                                            const string subject = "Registered successfully with MarketStudio";
                                            var body = " Thanks for registering MarketStudio Account,";
                                            body += "Kindly Wait for admin Approval. Once Approved, you can access MarketStudio,";
                                            body += "<br/><br/><b>Regards,</b>";
                                            body += "<br/><br/> MarketStudio Team";
                                            CommonUtil.SendEmailForCompanyandUserRegister(subject, currentUser.Email, body, "New Registration");
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("Error", Resource.AppController_ForgotPassword_User_Name_is_InCorrect__Please_Check);
                                        }


                                        ModelState.Clear();
                                        ViewBag.result = "Thank you for subscription of MarketStudio. Your subscription will be activated soon.If you have any query please contact support@questudio.com.";

                                        //ViewBag.Companies = _objLs.Customers.Where(x => (x.CompanyName != string.Empty && x.CompanyName != null && x.CompanyName != "Studiosoft")).Select(x => x).Distinct().ToList();
                                        var States = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == "Select").Select(x => new { x.STATE_CODE, x.STATE }).ToList();

                                        ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                                        ViewBag.States = new SelectList(States, "STATE_CODE", "STATE");
                                        if (Session["CompanyID"] != null)
                                        {
                                            if (!string.IsNullOrEmpty(Session["CompanyID"].ToString()))
                                            {
                                                model.CompanyName = Session["CompanyID"].ToString();
                                                Session["CompanyID"] = string.Empty;

                                            }
                                        }
                                        UserModel usermodel = new UserModel();

                                        // return View("~/Views/UserAdmin/RegistrationSuccessPage.cshtml", usermodel);
                                        return View("~/Views/UserAdmin/ThankyouPage.cshtml", usermodel);
                                    }
                                    else
                                    { ModelState.AddModelError("", ErrorCodeToString(createStatus)); }
                                    //ViewBag.Message = "Registered Successfully. Your account will be active once approved by the admin";
                                    //return View("~/Views/Accounts/Register.cshtml");
                                }
                                ModelState.AddModelError("", ErrorCodeToString(createStatus));


                            }
                            else
                            {

                                ModelState.AddModelError("", "The Customer Name already exists");
                                var objStates = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == model.Country).Select(x => new { x.STATE_CODE, x.STATE }).ToList();
                                ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                                ViewBag.States = new SelectList(objStates, "STATE_CODE", "STATE", model.State);
                                return View("~/Views/Accounts/Register.cshtml", model);
                            }
                        }
                    }
                    else
                    {
                        //Adding the Customer User under the Company
                        MembershipCreateStatus createStatus;
                        var emailchk = _objLs.aspnet_Membership.Where(z => z.Email == model.Email).Select(x => x.UserId).ToList();

                        if (emailchk.Any())
                        {
                            ModelState.AddModelError("", "A user name for that e-mail address already exists, please enter a different e-mail address");
                            //ViewBag.Companies = _objLs.Customers.Where(x => (x.CompanyName != string.Empty && x.CompanyName != null && x.CompanyName != "Studiosoft")).Select(x => x).Distinct().ToList();
                            var objStates = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == model.Country).Select(x => new { x.STATE_CODE, x.STATE }).ToList();
                            ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                            ViewBag.States = new SelectList(objStates, "STATE_CODE", "STATE", model.State);
                            return View("~/Views/Accounts/Register.cshtml", model);
                        }

                        Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);

                        if (createStatus == MembershipCreateStatus.Success)
                        {
                            var profile = ProfileModel.GetProfile(model.UserName);
                            profile.CustomerId = model.CustomerId;
                            profile.FirstName = model.FirstName;
                            profile.LastName = model.LastName;
                            profile.Title = model.Title;
                            profile.Address1 = model.Address1;
                            profile.Address2 = model.Address2;
                            profile.City = model.City;
                            profile.State = model.State;
                            profile.Zip = model.Zip;
                            profile.Country = model.Country;
                            profile.Phone = model.Phone;
                            profile.Fax = model.Fax;
                            profile.EMail = model.Email;
                            profile.Comments = model.Comments;
                            profile.DateUpdated = DateTime.Now;
                            profile.DateCreated = DateTime.Now;
                            profile.Save();

                            SaveCustomerData(model);
                            _objLs.SaveChanges();

                            //Update the Email to UserInvite
                            var userinvite = _objLs.TB_USERINVITE.Where(x => x.EMAIL == model.Email).FirstOrDefault();
                            {
                                userinvite.USER_STATUS = "Registered";
                                userinvite.DATECREATED = DateTime.Now;
                                userinvite.DATEUPDATED = DateTime.Now;
                            };
                            _objLs.SaveChanges();
                            //End the Email to UserInvite
                            var objuser = _objLs.aspnet_Users.FirstOrDefault(x => x.UserName == model.UserName);

                            if (objuser != null)
                            {

                                objuser.IsAnonymous = true;

                            }
                            _objLs.SaveChanges();


                            company_id = _objLs.Customers.Max(x => x.CustomerId);
                            var customer_name1 = _objLs.Customers.Where(z => z.CustomerId == company_id).Select(x => x.CompanyName).ToList();

                            if (customer_name1.Any())
                            {
                                string customer_name = Convert.ToString(customer_name1[0]);
                                customer_name = customer_name.Replace("_", "").Replace(" ", "") + "_Admin";
                                int roleid = Convert.ToInt16(_objLs.aspnet_Roles.Where(x => x.RoleName == customer_name).Select(y => y.Role_id).FirstOrDefault());

                                int result = NewSqlUser(model.UserName, model.Password, roleid);

                                if (result == 1)
                                {
                                    var result1 = _objLs.STP_CATALOGSTUDIO5_AddNewUser(model.UserName, "InActive", roleid);
                                    _objLs.SaveChanges();
                                }

                                ModelState.Clear();
                                ViewBag.result = "Thank you for subscription of MarketStudio. Your subscription will be activated soon.If you have any query please contact support@questudio.com.";

                                ViewBag.Companies = _objLs.Customers.Where(x => (x.CompanyName != string.Empty && x.CompanyName != null && x.CompanyName != "Studiosoft")).Select(x => x).Distinct().ToList();
                                var States = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == "Select").Select(x => new { x.STATE_CODE, x.STATE }).ToList();

                                ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                                ViewBag.States = new SelectList(States, "STATE_CODE", "STATE");
                                if (Session["CompanyID"] != null)
                                {
                                    if (!string.IsNullOrEmpty(Session["CompanyID"].ToString()))
                                    {
                                        model.CompanyName = Session["CompanyID"].ToString();
                                        Session["CompanyID"] = string.Empty;

                                    }
                                }
                                UserModel usermodel = new UserModel();
                                return View("~/Views/UserAdmin/ThankyouPage.cshtml", usermodel);
                                //return View("~/Views/Accounts/Register.cshtml", usermodel);

                            }
                            else
                            {
                                ModelState.AddModelError("", ErrorCodeToString(createStatus));
                            }
                            //Send Mail to User
                            MembershipUser currentUser = Membership.GetUser(model.UserName);
                            if (currentUser != null)
                            {

                                const string subject = "Registered successfully with MarketStudio";
                                var body = " Thanks for Registering MarketStudio Account,";
                                body += "Kindly Wait for admin Approval. Once Approved, you can access MarketStudio,";
                                body += "<br/><br/><b>Regards,</b>";
                                body += "<br/><br/> MarketStudio Team";
                                CommonUtil.SendEmailForCompanyandUserRegister(subject, currentUser.Email, body, "New Registeration");
                            }
                            else
                            {
                                ModelState.AddModelError("Error", Resource.AppController_ForgotPassword_User_Name_is_InCorrect__Please_Check);
                            }
                            // super Admin mail
                            var SuperAdminMailCheck = _objLs.vw_aspnet_UsersInRoles
                       .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                       .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                       .Join(_objLs.aspnet_Membership, tcm => tcm.tcptps.tpf.UserId, tcms => tcms.UserId, (tcm, tcms) => new { tcm, tcms })
                       .Where(x => x.tcm.tcp.RoleType == 1).Select(x => x.tcms.Email).ToList();

                            const string adminsubject = "New company has been registered successfully with MarketStudio";
                            var adminbody = "Company" + " " + model.CompanyName.ToString() + " " + " has been registered successfully";

                            adminbody += "<br/><br/><b>Regards,</b>";
                            adminbody += "<br/><br/> MarketStudio Team";

                            foreach (var item in SuperAdminMailCheck)
                            {

                                CommonUtil.SendEmailForCompanyandUserRegister(adminsubject, item.ToString(), adminbody, "New Registeration");
                            }
                            //-------------------------------------------------------------
                            ModelState.Clear();
                            ViewBag.Message = "Registered successfully. Your account will be active once approved by the admin";
                            //  return View("~/Views/Accounts/Register.cshtml");
                            return View("~/Views/UserAdmin/ThankyouPage.cshtml");

                        }
                        ModelState.AddModelError("", ErrorCodeToString(createStatus));

                        //ViewBag.Companies = _objLs.Customers.Where(x => (x.CompanyName != string.Empty && x.CompanyName != null && x.CompanyName != "Studiosoft")).Select(x => x).Distinct().ToList();
                        var ObjStates = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == model.Country).Select(x => new { x.STATE_CODE, x.STATE }).ToList();
                        ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                        ViewBag.States = new SelectList(ObjStates, "STATE_CODE", "STATE", model.State);
                        if (Session["CompanyID"] != null)
                        {
                            if (!string.IsNullOrEmpty(Session["CompanyID"].ToString()))
                            {
                                model.CompanyName = Session["CompanyID"].ToString();
                                Session["CompanyID"] = string.Empty;
                            }
                        }



                    }

                }
                var ObjStatess = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == model.Country).Select(x => new { x.STATE_CODE, x.STATE }).ToList();
                ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                ViewBag.States = new SelectList(ObjStatess, "STATE_CODE", "STATE", model.State);
                if (Session["CompanyID"] != null)
                {
                    if (!string.IsNullOrEmpty(Session["CompanyID"].ToString()))
                    {
                        model.CompanyName = Session["CompanyID"].ToString();
                        Session["CompanyID"] = string.Empty;
                    }
                }

                return View("~/Views/Accounts/Register.cshtml", model);



            }

            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            var ObjStates1 = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == model.Country).Select(x => new { x.STATE_CODE, x.STATE }).ToList();
            ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
            ViewBag.States = new SelectList(ObjStates1, "STATE_CODE", "STATE", model.State);
            return View("~/Views/Accounts/Register.cshtml", model);
        }






        [Authorize(Users = "*")]
        [AllowAnonymous]
        public ActionResult RegisterWeb()
        {


            var UserWebModel = new LS.Data.Model.Accounts.UserWebModel();



            return View("~/Views/App/RegisterWeb.cshtml", UserWebModel);
        }




        [HttpPost]
        [Authorize(Users = "*")]
        [AllowAnonymous]
        public ActionResult RegisterWeb(UserWebModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (model.Email != null && model.Email.ToString() != string.Empty)
                    {

                        var userContact = _objLs.QSWS_USER.Where(y => y.USER_EMAIL == model.Email).Select(y => y.USER_ID).ToList();
                        var emailchk = _objLs.QSWS_WEB_CONTACTS.Where(z => z.CONTACT_EMAIL == model.Email).Select(x => x.CONTACT_ID).ToList();
                        var farmChk = _objLs.QSWS_FARM_PROFILE_CONTACTS.Where(x => x.EMAIL_ADDRESS == model.Email).Select(x => x.FARM_ID).ToList();
                        var wholesalerChk = _objLs.QSWS_WHOLESALER_PROFILE_CONTACTS.Where(a => a.EMAIL_ID == model.Email).Select(a => a.WHOLESALER_ID).ToList();
                        var TWIChk = _objLs.QSWS_TWI_PROFILE_CONTACTS.Where(b => b.EMAIL_ID == model.Email).Select(b => b.TWI_ID).ToList();

                        if (userContact.Any())
                        {
                            ModelState.AddModelError("", "A user name for that e-mail address already exists, please enter a different e-mail address");

                            return View("~/Views/App/RegisterWeb.cshtml", model);
                        }


                        if (farmChk.Any())
                        {
                            if (emailchk.Any())
                            {
                                ModelState.AddModelError("", "A user name for that e-mail address already exists, please enter a different e-mail address");

                                return View("~/Views/App/RegisterWeb.cshtml", model);
                            }

                        }
                        if (wholesalerChk.Any())
                        {
                            if (emailchk.Any())
                            {
                                ModelState.AddModelError("", "A user name for that e-mail address already exists, please enter a different e-mail address");

                                return View("~/Views/App/RegisterWeb.cshtml", model);
                            }
                        }
                        if (TWIChk.Any())
                        {
                            if (emailchk.Any())
                            {
                                ModelState.AddModelError("", "A user name for that e-mail address already exists, please enter a different e-mail address");

                                return View("~/Views/App/RegisterWeb.cshtml", model);
                            }
                        }

                        if (emailchk.Any())
                        {
                            ModelState.AddModelError("", "A user name for that e-mail address already exists, please enter a different e-mail address");

                            return View("~/Views/App/RegisterWeb.cshtml", model);
                        }
                        else
                        {
                            QSWS_WEB_CONTACTS WebContact = new QSWS_WEB_CONTACTS();
                            WebContact.FIRST_NAME = model.FirstName;
                            WebContact.LAST_NAME = model.LastName;
                            WebContact.CONTACT_EMAIL = model.Email;
                            WebContact.COMPANY_NAME = model.CompanyName;
                            WebContact.NOTES = model.Notes;
                            WebContact.CREATED_USER = model.Email;
                            WebContact.CREATED_DATE = DateTime.Now;
                            WebContact.MODIFIED_USER = model.Email;
                            WebContact.MODIFIED_DATE = DateTime.Now;
                            WebContact.FLAG_RECYCLE = true;
                            _objLs.QSWS_WEB_CONTACTS.Add(WebContact);
                            _objLs.SaveChanges();
                            string SMTP_Server = ConfigurationManager.AppSettings["SmtpServer"].ToString();
                            string SMTP_MailId = ConfigurationManager.AppSettings["SenderMail"].ToString();
                            string SMTP_Password = ConfigurationManager.AppSettings["Password"].ToString();
                            int SMTP_Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTP_Port"]);
                            string emailAttachment = ConfigurationManager.AppSettings["Contact_Email_Attachment"].ToString();
                            string webUserSubject = ConfigurationManager.AppSettings["Contact_Email_SubjectConfirmation"].ToString();
                            string webUserContent = ConfigurationManager.AppSettings["Contact_Email_UserConfirmation"].ToString();
                            string AdminEmail = ConfigurationManager.AppSettings["Admn_Email"].ToString();
                            string adminSubject = ConfigurationManager.AppSettings["Contact_AdminEmail_SubjectConfirmation"].ToString();
                            string webUserAdmin = ConfigurationManager.AppSettings["Contact_AdminEmail_UserConfirmation"].ToString();
                            WebMailUser(SMTP_MailId, model.Email, SMTP_Server, SMTP_MailId, SMTP_Password, emailAttachment, SMTP_Port, webUserContent, webUserSubject);
                            WebMailAdmin(SMTP_MailId, model.FirstName, model.LastName, model.CompanyName, AdminEmail, SMTP_Server, SMTP_MailId, SMTP_Password, emailAttachment, SMTP_Port, webUserAdmin, adminSubject);
                            ModelState.Clear();
                            HttpCookie aCookie;
                            string cookieName;
                            int limit = Request.Cookies.Count;
                            for (int i = 0; i < limit; i++)
                            {
                                cookieName = Request.Cookies[i].Name;
                                aCookie = new HttpCookie(cookieName);
                                aCookie.Expires = DateTime.Now.AddDays(-1);
                                Response.Cookies.Add(aCookie);
                            }

                            ViewBag.result = "Thank you for subscription of Tradewinds.Your subscription will be activated soon.If you have any query please contact Admin";

                        }
                    }



                }
                return View("~/Views/App/RegisterWeb.cshtml");



            }

            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            //var ObjStates1 = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == model.Country).Select(x => new { x.STATE_CODE, x.STATE }).ToList();
            //ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
            //ViewBag.States = new SelectList(ObjStates1, "STATE_CODE", "STATE", model.State);
            return View("~/Views/App/RegisterWeb.cshtml");
        }



        private static void WebMailUser(string Email_From, string Email_To, string SMTP_Server, string SMTP_MailId, string SMTP_Password, string emailAttachment, int SMTP_Port, string Message_Content, string Email_Subject)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(SMTP_Server);
                string BCC_Emails = ConfigurationManager.AppSettings["BCC_Emails"].ToString();
                SmtpServer.UseDefaultCredentials = false;
                mail.From = new MailAddress(Email_From);
                mail.To.Add(Email_To);
                foreach (var address in BCC_Emails.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.Bcc.Add(address);
                }
                mail.Subject = Email_Subject;
                mail.Body = string.Format("<div style='font-family:Century Gothic;'>{0} </br></div> <div><img width='30%' src='{1}'></div>", Message_Content, emailAttachment);
                SmtpServer.Credentials = new System.Net.NetworkCredential(SMTP_MailId, SMTP_Password);
                SmtpServer.Port = SMTP_Port;
                SmtpServer.EnableSsl = true;
                mail.IsBodyHtml = true;
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }


        private static void WebMailAdmin(string Email_From, string First_Name, string last_Name, string Company_name, string Email_To, string SMTP_Server, string SMTP_MailId, string SMTP_Password, string emailAttachment, int SMTP_Port, string Message_Content, string Email_Subject)
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(SMTP_Server);
                string BCC_Emails = ConfigurationManager.AppSettings["BCC_Emails"].ToString();
                SmtpServer.UseDefaultCredentials = false;
                mail.From = new MailAddress(Email_From);
                mail.To.Add(Email_To);
                foreach (var address in BCC_Emails.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                {
                    mail.Bcc.Add(address);
                }
                mail.Subject = Email_Subject;
                mail.Body = string.Format("<div style='font-family:Century Gothic;'>{0} </br></div> <div><img width='30%' src='{1}'></div>", First_Name + " " + last_Name + "," + " " + Company_name + Message_Content, emailAttachment);
                SmtpServer.Credentials = new System.Net.NetworkCredential(SMTP_MailId, SMTP_Password);
                SmtpServer.Port = SMTP_Port;
                SmtpServer.EnableSsl = true;
                mail.IsBodyHtml = true;
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }


        [HttpGet]
        public JsonResult GetStateList(string countryId)
        {
            var states = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == countryId).Select(a => new { a.STATE, a.STATE_CODE }).ToList();
            return Json(states, JsonRequestBehavior.AllowGet);
        }
        public int NewSqlUser(string newuser, string password, int roleid)
        {

            int Result = 0;
            string _SqlAddUser = "CREATE LOGIN [" + newuser + "] WITH PASSWORD=N'" + password + "',CHECK_EXPIRATION = OFF,CHECK_POLICY = OFF";
            string _SqlCreateUser = "CREATE USER [" + newuser + " ] FOR LOGIN [" + newuser + "]";
            //string _SqlSchma = "ALTER AUTHORIZATION ON SCHEMA::[db_owner] TO [" + newuser + "]";
            string _SqlSpUser = "EXEC sp_addrolemember N'db_owner', N'" + newuser + "'";
            string _sqlseladmin = "";
            try
            {
                //  _adduser = new SqlCommand(_SqlAddUser, oConn);
                _sqlString = _SqlAddUser;
                ExecuteSqlQuery();
                _sqlString = _SqlCreateUser;
                //_adduser = new SqlCommand(_SqlCreateUser, oConn);
                ExecuteSqlQuery();
                // _adduser = new SqlCommand(_SqlSchma, oConn);
                // _adduser.ExecuteNonQuery();
                _sqlString = _SqlSpUser;
                //_adduser = new SqlCommand(_SqlSpUser, oConn);
                ExecuteSqlQuery();
                if (roleid == 1)
                {
                    _sqlString = _sqlseladmin;
                    //  _adduser = new SqlCommand(_sqlseladmin, oConn);
                    ExecuteSqlQuery();
                }
                Result = 1;
            }
            catch (SqlException es)
            {


            }


            return Result;
        }




        [HttpPost]
        [Authorize(Users = "*")]
        [AllowAnonymous]
        public ActionResult RegisterCustomer(CustomerModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {


                    TB_STORE_SESSION objStore;
                    int roleIdreturn = 0;
                    string customer_name = "";
                    int customer_id = 0;
                    string roleId = "";
                    // int custid = _objLs.Customers.Max(x => x.CustomerId);
                    // custid = custid + 1;
                    var customersavail = _objLs.Customers.Where(x => x.CompanyName == model.CompanyName);
                    var planid = _objLs.TB_PLAN.Where(x => x.PLAN_NAME == "One Month subscribe").Select(x => x.PLAN_ID).FirstOrDefault();
                    if (!customersavail.Any())
                    {
                        var objcustomer = new Customer
                        {
                            // CustomerId = custid,

                            PlanId = planid,
                            CustomerName = model.CustomerName,
                            ContactTitle = model.ContactTitle,
                            ContactName = model.ContactName,
                            CompanyName = model.CompanyName,
                            ThemeName = "metro",
                            BannerImageURL = "BlueLogo",
                            Address1 = model.Address1,
                            Address2 = model.Address2,
                            City = model.City,
                            StateProvince = model.StateProvince,
                            Zip = model.Zip,
                            Country = model.Country,
                            Phone = model.Phone,
                            Fax = model.Fax,
                            EMail = model.EMail,
                            Comments = model.Comments,
                            WebSite = model.WebSite,
                            IsActive = true,
                            IsApproved = true,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now,
                        };
                        _objLs.Customers.Add(objcustomer);
                        _objLs.SaveChanges();

                        int custid = _objLs.Customers.Max(x => x.CustomerId);
                        //custid = custid + 1;
                        Session["CompanyID"] = custid.ToString(CultureInfo.InvariantCulture);

                        var objCustomerSettings = new Customer_Settings
                        {
                            CustomerId = custid,
                            DefaultAppStyle = "Black",
                            DisplayIDColumns = false,
                            ErrorLogFilePath = "C:",
                            Temporary_File_Path = "C:",
                            AllowDuplicateItem_PartNum = false,
                            TechSupportURL = "C:",
                            SpellCheckerMode = "None",
                            WebcatSyncURL = "C:",
                            WhiteboardWebserviceURL = "C:",
                            ShowCustomAPPS = true,
                            EnableWorkFlow = false,
                            ProductLevelMultitablePreview = true,
                            FamilyLevelMultitablePreview = true,
                            SKUAlertPercentage = 90,
                            AssetLimitPercentage = 90,
                            DisplaySkuProductCountInAlert = true,
                            Export_ImportPath = "C:",
                            ImagePath = "C:",
                            ImageConversionUtilityPath = @"C:\Program Files (x86)\ImageMagick-6.8.8-Q8",
                            WebCatImagePath = "C:",
                            ReferenceTableImagePath = "C:",
                            ImageConversionSize1Width = 0,
                            ImageConversionSize1Height = 0,
                            ImageConversionSize1Folder = "",
                            ImageConversionSize2Width = 0,
                            ImageConversionSize2Height = 0,
                            ImageConversionSize2Folder = "",
                            ImageConversionSize3Width = 0,
                            ImageConversionSize3Height = 0,
                            ImageConversionSize3Folder = "",
                            ImageConversionSize4Width = 0,
                            ImageConversionSize4Height = 0,
                            ImageConversionSize4Folder = "",
                            ImageConversionSize5Width = 0,
                            ImageConversionSize5Height = 0,
                            ImageConversionSize5Folder = "",
                            ImageConversionSize6Width = 0,
                            ImageConversionSize6Height = 0,
                            ImageConversionSize6Folder = "",
                            DisplayCategoryIdinNavigator = false,
                            DisplayFamilyandRelatedFamily = true,
                            UploadImagesPDFForWebSync = false,
                            CreateXMLForSelectedAttributesToAllFamily = false,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now,
                            bannerpath = "top_bg.jpg",
                            logopath = null,
                            Theme = "blueopal"
                        };
                        _objLs.Customer_Settings.Add(objCustomerSettings);
                        _objLs.SaveChanges();

                        TB_PLAN objPlan;
                        objStore = new TB_STORE_SESSION { ID = custid };
                        _objLs.TB_STORE_SESSION.Add(objStore);
                        if (objStore != null)
                        {
                            objStore.PID = model.PlanId;
                            objPlan = _objLs.TB_PLAN.FirstOrDefault(x => x.PLAN_ID == model.PlanId);
                            if (objPlan != null)
                            {
                                objStore.COL1 = LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(objPlan.SKU_COUNT));
                                objStore.COL2 = LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(objPlan.NO_OF_USERS));
                                objStore.COL3 = LS.Data.Utilities.CommonUtil.Encrypt(Convert.ToString(objPlan.SUBSCRIPTION_IN_MONTHS));
                            }
                        }


                        _objLs.SaveChanges();

                        customer_name = model.CompanyName.Replace(" ", "").Replace("_", "");
                        customer_id = custid;

                        roleId = customer_name + "_" + "Admin";
                        var tbroleCheck = _objLs.TB_ROLE.Where(a => a.ROLE_NAME == roleId);
                        if (!tbroleCheck.Any())
                        {
                            var result = _objLs.STP_CATALOGSTUDIO5_AddNewRole(roleId);
                            _objLs.SaveChanges();
                            if (result != null)
                            {
                                int rid = Convert.ToInt32(result);
                                var roleResult = _objLs.TB_ROLE.FirstOrDefault(a => a.ROLE_NAME == roleId);
                                if (roleResult != null)
                                {
                                    UserRolesList role = new UserRolesList();
                                    var aspnetrolescheck = _objLs.aspnet_Roles.Where(x => x.Role_id == roleResult.ROLE_ID);
                                    if (!aspnetrolescheck.Any())
                                    {
                                        //var objAspnetRoles = new aspnet_Roles
                                        //{
                                        //    RoleName = roleResult.ROLE_NAME,
                                        //    Role_id = roleResult.ROLE_ID
                                        //};
                                        _objLs.aspnet_Roles_CreateRole("/", roleId);
                                        var aspnetrolescheck1 = _objLs.aspnet_Roles.FirstOrDefault(x => x.RoleName == roleId);
                                        if (aspnetrolescheck1 != null)
                                        {
                                            roleIdreturn = roleResult.ROLE_ID;
                                            aspnetrolescheck1.Role_id = roleResult.ROLE_ID;
                                            aspnetrolescheck1.RoleType = 2;
                                            aspnetrolescheck1.CustomerId = customer_id;

                                            var objCustomercatalogs = _objLs.Customer_Catalog.Where(x => x.CustomerId == customer_id);
                                            if (objCustomercatalogs.Any())
                                            {
                                                foreach (var objCustomercatalog in objCustomercatalogs)
                                                {
                                                    var rolecatalog = _objLs.Customer_Role_Catalog.Where(x => x.CATALOG_ID == objCustomercatalog.CATALOG_ID && x.RoleId == roleIdreturn && x.CustomerId == customer_id);
                                                    if (!rolecatalog.Any())
                                                    {
                                                        var objcustomerrolecatalog = new Customer_Role_Catalog
                                                        {
                                                            RoleId = roleIdreturn,
                                                            CustomerId = customer_id,
                                                            CATALOG_ID = objCustomercatalog.CATALOG_ID,
                                                            DateCreated = DateTime.Now,
                                                            DateUpdated = DateTime.Now
                                                        };
                                                        _objLs.Customer_Role_Catalog.Add(objcustomerrolecatalog);
                                                    }
                                                }
                                            }

                                            _objLs.SaveChanges();
                                        }


                                    }
                                }
                            }
                            _objLs.SaveChanges();
                        }
                        ViewBag.Plans = _objLs.TB_PLAN.Select(x => x).ToList();
                        var States = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == "Select").Select(x => new { x.STATE_CODE, x.STATE }).ToList();
                        ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                        ViewBag.States = new SelectList(States, "STATE_CODE", "STATE");
                        return RedirectToAction("Register");

                    }
                    else
                    {

                        ModelState.AddModelError("", "The Customer Name already exists");
                        ViewBag.Plans = _objLs.TB_PLAN.Select(x => x).ToList();
                        var States = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == model.Country).Select(x => new { x.STATE_CODE, x.STATE }).ToList();
                        ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                        ViewBag.States = new SelectList(States, "STATE_CODE", "STATE", model.StateProvince);
                        return View("~/Views/Accounts/RegisterCustomer.cshtml", model);
                    }
                }
                else
                {

                    ModelState.AddModelError("", "The Customer Name already exists");
                    ViewBag.Plans = _objLs.TB_PLAN.Select(x => x).ToList();
                    var States = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == model.Country).Select(x => new { x.STATE_CODE, x.STATE }).ToList();
                    ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                    ViewBag.States = new SelectList(States, "STATE_CODE", "STATE", model.StateProvince);
                    return View("~/Views/Accounts/RegisterCustomer.cshtml", model);
                }
            }



            catch (Exception ex)
            {
                _logger.Error(ex);
                ViewBag.Plans = _objLs.TB_PLAN.Select(x => x).ToList();
                var States = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == "Select").Select(x => new { x.STATE_CODE, x.STATE }).ToList();
                ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
                ViewBag.States = new SelectList(States, "STATE_CODE", "STATE");
                return View("~/Views/Accounts/RegisterCustomer.cshtml", model);
            }

        }

        //public PartialViewResult RegisterUser()
        //{
        //    ViewBag.Companies = _objLs.Customers.Where(x => (x.CompanyName != string.Empty && x.CompanyName != null)).Select(x => x).Distinct().ToList();

        //    var model = new LS.Data.Model.Accounts.UserModel();
        //    if (Session["CompanyID"] != null)
        //    {
        //        if (!string.IsNullOrEmpty(Session["CompanyID"].ToString()))
        //        {
        //            model.CompanyName = Session["CompanyID"].ToString();
        //            Session["CompanyID"] = string.Empty;
        //        }
        //    }
        //    return PartialView("~/Views/Accounts/RegisterUser.cshtml", model);
        //}

        [Authorize(Users = "*")]
        [AllowAnonymous]
        public ActionResult RegisterCustomer()
        {
            ViewBag.Plans = _objLs.TB_PLAN.Select(x => x).ToList();
            var States = _objLs.TB_STATE.Where(x => x.COUNTRY_CODE == "Select").Select(x => new { x.STATE_CODE, x.STATE }).ToList();

            ViewBag.Countries = _objLs.TB_COUNTRY.Select(x => new { x.COUNTRY_CODE, x.COUNTRY }).OrderBy(x => x.COUNTRY).ToList();
            ViewBag.States = new SelectList(States, "STATE_CODE", "STATE");
            return View("~/Views/Accounts/RegisterCustomer.cshtml", new LS.Data.Model.CustomerModel());
        }
        public void ClearSession()
        {
            try
            {
                Session.Abandon();
                FormsAuthentication.SignOut();
                // return RedirectToAction("LogOn", "Accounts");
            }
            catch (Exception ex)
            {

            }
        }
        public JsonResult IsEmailAlreadyExists(string Email)
        {
            if (!EmailExists(Email))
                return Json(true, JsonRequestBehavior.AllowGet);
            else
                return Json(Email + " already exists", JsonRequestBehavior.AllowGet);
        }

        private bool EmailExists(string Email)
        {
            var email = _objLs.aspnet_Membership.FirstOrDefault(d => d.Email == Email);
            return (email != null);
        }

        private void SaveCustomerData(UserModel model)
        {
            try
            {

                int Customer_Id = _objLs.Customers.Max(x => x.CustomerId);
                var objuser = new Customer_User();
                //objuser.CustomerId = Convert.ToInt32(model.CompanyName);
                objuser.CustomerId = Customer_Id;
                objuser.User_Name = model.UserName;
                objuser.FirstName = model.FirstName;
                objuser.LastName = model.LastName;
                objuser.Title = model.Title;
                objuser.Address = model.Address1;
                objuser.City = model.City;
                objuser.State = model.State;
                objuser.Zip = model.Zip;
                objuser.Country = model.Country;
                objuser.Email = model.Email;
                objuser.Phone = model.Phone;
                objuser.DateCreated = DateTime.Now;
                objuser.DateUpdated = DateTime.Now;
                _objLs.Customer_User.Add(objuser);
                _objLs.SaveChanges();


            }

            catch (Exception objexception)
            {
                _logger.Error("Error at SaveCustomerData", objexception);

            }


        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists, please enter a different user name";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists, please enter a different e-mail address";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid, please enter a valid password value";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address entered is invalid, please check the value and try again";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid, please check the value and try again";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid, please check the value and try again";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid, please check the value and try again";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }

        public ActionResult RemoveProject()
        {
            TempData.Keep("WelcomeName");
            return View("~/Views/App/Remove/RemoveProject.cshtml");
        }

        public string RemoveProjectFromDb(int catlogId, int selectedprojectId, string selectedprojectName)
        {
            try
            {
                var db = _objLs.TB_PROJECT.FirstOrDefault(x => x.CATALOG_ID == catlogId && x.PROJECT_ID == selectedprojectId);
                if (db != null)
                    _objLs.TB_PROJECT.Remove(db);
                _objLs.SaveChanges();
                return "Successfully removed";
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at RemoveProject : GetProjectDetails", objexception);
                return "Action not successful";
            }
        }

        [HttpPost]
        public ActionResult ForgotPassword(ForgorPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // string username = Membership.GetUserNameByEmail(model.Email);
                    var user_name = _objLs.aspnet_Membership
                                             .Join(_objLs.aspnet_Users, cc => cc.UserId, tc => tc.UserId,
                                                 (cc, tc) => new { cc, tc })
                                             .Where(
                                                 x =>
                                                     x.cc.Email == model.Email).Select(y => new { y.tc.UserName }).FirstOrDefault();

                    if (user_name != null)
                    {
                        MembershipUser currentUser = Membership.GetUser(user_name.UserName.ToString().Trim());
                        if (currentUser != null)
                        {

                            const string subject = "Reset Password for MarketStudio Account";
                            var body = "Click on the following link to reset a new password for your MarketStudio account,";
                            if (currentUser.ProviderUserKey != null)
                                body += "<BR/><br/><a HREF=\"" + ConfigurationManager.AppSettings.Get("changePasswordLinkForForgotPassword") + "?VerificationId=" + CommonUtil.Encrypt(currentUser.ProviderUserKey.ToString().Replace("{", "").Replace("}", "")) + "&UId=" + CommonUtil.Encrypt(user_name.UserName.ToString().Trim()) + "\"> Create a new password </A>";
                            body += "<br/>" + "<br/>" + "If the above link doesn't work, try the following: <b>.";
                            body += "<br/>Mozilla Users- Right click on the 'Create a new password' link. Select 'Copy link location' & paste the URL in the address bar.";
                            body += "<br/>Internet Explorer Users- Right click on the 'Create a new password' link. Select 'Open in New Window/Tab' option.";
                            body += "<br/><br/> Note: This link will be functional for a one time use ";
                            body += "<br/><br/><b>Regards,</b>";
                            body += "<br/><br/> MarketStudio Team";
                            CommonUtil.SendEmail(subject, currentUser.Email, body, "Forgot Password");
                        }
                        else
                        {
                            ModelState.AddModelError("Error", Resource.AppController_ForgotPassword_User_Name_is_InCorrect__Please_Check);
                        }
                    }
                    else
                    {
                        ModelState.AddModelError("Error", Resource.AppController_ForgotPassword_User_Name_is_InCorrect__Please_Check);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("Error", Resource.AppController_ForgotPassword_Internal_Server_Error);
                    _logger.Error("Error at SaveOrUpdateAppUser", ex);
                }
            }
            string error = string.Join("; ", ModelState.Values
                                    .SelectMany(x => x.Errors)
                                    .Select(x => x.ErrorMessage));
            if (!string.IsNullOrEmpty(error))
            {
                model.ReturnMessage = error;
            }
            else
            {
                model.ReturnMessage = "Password sent to your registered EMail.";
            }
            return View("~/Views/Accounts/ForgotPassword.cshtml", model);
        }

        [Authorize(Users = "*")]
        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            try
            {
                var model = new ResetPasswordModel();
                if (Request.QueryString.Count > 0 && Request.QueryString["VerificationId"] != null && Request.QueryString["UId"] != null)
                {
                    var verificationId = CommonUtil.Decrypt(Request.QueryString["VerificationId"]);
                    model.UserName = CommonUtil.Decrypt(Request.QueryString["UId"]);

                    MembershipUser currentUser = Membership.GetUser(model.UserName);
                    if (currentUser != null)
                    {
                        if (currentUser.ProviderUserKey != null)
                        {
                            var providerKey = currentUser.ProviderUserKey.ToString().Replace("{", "").Replace("}", "");
                            if (providerKey == verificationId)
                            {
                                return View("~/Views/Accounts/ResetPassword.cshtml", model);
                            }
                        }
                    }
                }
                return View("~/Views/Accounts/ResetPassword.cshtml", model);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            return null;
        }

        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            if (!ModelState.IsValid) return View("~/Views/Accounts/ResetPassword.cshtml", model);
            bool changePasswordSucceeded = false;
            try
            {
                MembershipUser currentUser = Membership.GetUser(model.UserName);
                if (currentUser != null)
                {
                    string oldPassword = currentUser.ResetPassword();
                    changePasswordSucceeded = currentUser.ChangePassword(oldPassword, model.NewPassword);
                    currentUser.UnlockUser();
                }

                if (changePasswordSucceeded)
                {
                    model.ReturnMessage = Resource.AppController_ResetPassword_Password_changed_successfully;
                }
                else
                {
                    model.ReturnMessage = Resource.AppController_ResetPassword_Unable_to_change_the_password__Please_contact_administrator;
                }

                return View("~/Views/Accounts/ResetPassword.cshtml", model);
            }
            catch (Exception)
            {
                return View("~/Views/Accounts/ResetPassword.cshtml", model);
            }

            // If we got this far, something failed, redisplay form

        }

        // [Authorize(Roles = "AdminNormalUser, SuperAdmin")]
        public ActionResult AttributeGroupingPopUp()
        {
            return View("~/Views/App/Admin/AttributeGroupingPopUp.cshtml");
        }

        public JsonResult GetRightTreeAttributes(string objstrid)
        {
            try
            {
                var tokens = objstrid.Split(',');
                var attributeIds = Array.ConvertAll(tokens, int.Parse);
                var attributes = _objLs.TB_ATTRIBUTE.Where(a => attributeIds.Contains(a.ATTRIBUTE_ID)).Select(s => new LS_TB_ATTRIBUTE
                {
                    ATTRIBUTE_TYPE = s.ATTRIBUTE_TYPE,
                    ATTRIBUTE_ID = s.ATTRIBUTE_ID,
                    ATTRIBUTE_NAME = s.ATTRIBUTE_NAME
                }).ToList();

                return Json(attributes, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objException)
            {
                _logger.Error(objException);
                return null;
            }
        }
        public JsonResult GetAllattributes(int attributeId, string familyId, int catalogId)
        {
            try
            {
                int famId;
                int.TryParse(familyId.Trim('~'), out famId);
                //var Attributes = objLS.TB_ATTRIBUTE.Where(a => a.ATTRIBUTE_TYPE == Attributeid).Select(s => new LS_TB_ATTRIBUTE
                //var Attributes = objLS.TB_ATTRIBUTE.Join(objLS.TB_PROD_FAMILY_ATTR_LIST,a=>a.ATTRIBUTE_ID,b=>b.ATTRIBUTE_ID,(a,b)=>new {a.ATTRIBUTE_TYPE,a.ATTRIBUTE_ID,a.ATTRIBUTE_NAME})
                var attributes = _objLs.TB_ATTRIBUTE.Join(_objLs.TB_PROD_FAMILY_ATTR_LIST, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a.ATTRIBUTE_TYPE, a.ATTRIBUTE_ID, a.ATTRIBUTE_NAME, b.FAMILY_ID })
                    .Join(_objLs.TB_CATALOG_FAMILY, a => a.FAMILY_ID, b => b.FAMILY_ID, (a, b) => new { a.ATTRIBUTE_ID, a.ATTRIBUTE_NAME, a.ATTRIBUTE_TYPE, a.FAMILY_ID, b.CATALOG_ID, b.CATEGORY_ID })
                    .Join(_objLs.TB_ATTRIBUTE_GROUP_SPECS, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a.ATTRIBUTE_ID, a.ATTRIBUTE_NAME, a.ATTRIBUTE_TYPE, a.FAMILY_ID, b.GROUP_ID, a.CATALOG_ID })
                    .Where(x => x.ATTRIBUTE_TYPE == attributeId && x.FAMILY_ID == famId && x.CATALOG_ID == catalogId)
                .Select(s => new LS_TB_ATTRIBUTE
                {
                    ATTRIBUTE_TYPE = s.ATTRIBUTE_TYPE,
                    ATTRIBUTE_ID = s.ATTRIBUTE_ID,
                    ATTRIBUTE_NAME = s.ATTRIBUTE_NAME
                }).Distinct().ToList();

                return Json(attributes, JsonRequestBehavior.AllowGet);

            }
            catch (Exception objException)
            {
                _logger.Error(objException);
                return null;
            }
        }

        private class CustomAuthorizeAttribute : AuthorizeAttribute
        {
            private string[] allowedroles;
            public CustomAuthorizeAttribute(params string[] roles)
            {
                allowedroles = roles;
            }
            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                bool authorize = false;
                // foreach (bool validRole in allowedroles.Select(role => httpContext.User.IsInRole(role)).Where(validRole => validRole))
                // {
                authorize = true; /* return true if Entity has current user(active) with specific role */
                // }
                return authorize;
            }
            protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
            {
                filterContext.Result = new RedirectToRouteResult(
                   new RouteValueDictionary(
                       new
                       {
                           controller = "Accounts",
                           action = "Error"
                       })
                   );
            }
        }

        public void ExecuteSqlQuery()
        {
            try
            {
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = _sqlString;
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);

            }
        }
        ////sathya----------------Hyperlink for assets folder images

        public ActionResult CopyImageManagement(string url)
        {
            try
            {
                //if (string.IsNullOrEmpty(User.Identity.Name))
                //{
                //    return RedirectToAction("LogOn", "Accounts");
                //}
                var keybytes = System.Text.Encoding.UTF8.GetBytes("8080808080808080");
                var iv = System.Text.Encoding.UTF8.GetBytes("8080808080808080");
                var encrypted = Convert.FromBase64String(url.Replace(" ", "+"));
                var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
                //var uri = new Uri(Request.Url.Authority);
                var uri = ConfigurationManager.AppSettings["AssetmanagementHyperLinkEmailPath"].ToString();
                var PreviewPath = uri + "/Content/ProductImages///DynamicPreview";
                bool exists = System.IO.Directory.Exists(Server.MapPath("///DynamicPath"));

                if (!exists)
                    System.IO.Directory.CreateDirectory(Server.MapPath("///DynamicPath"));

                string DynamicImgPath = Server.MapPath(new Uri(PreviewPath).LocalPath);
                string ActualImgPath = Server.MapPath(new Uri(decriptedFromJavascript).LocalPath);
                string FinalImgPath = Path.Combine(DynamicImgPath, Path.GetFileName(ActualImgPath));

                if (!System.IO.File.Exists(FinalImgPath))
                {
                    System.IO.File.Copy(ActualImgPath, Path.Combine(DynamicImgPath, Path.GetFileName(ActualImgPath)));
                }

                //string ImgName = Path.GetFileName(ActualImgPath);

                string sessId = Guid.NewGuid().ToString();
                string fileLocation = string.Format("{0}\\{1}.jpg", Server.MapPath("upload"), sessId);

                System.IO.File.Copy(FinalImgPath, Path.Combine(DynamicImgPath, Path.GetFileName(fileLocation)));



                string GetImgName = Path.GetFileName(fileLocation);
                string NewImgPath = Path.Combine(DynamicImgPath, GetImgName);
                string imgLink = Path.GetFileName(NewImgPath);
                string PicturePath = PreviewPath + "//" + imgLink;
                return Redirect(PicturePath);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return null;
            }
        }

        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext = null;

            // Create an RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new System.Security.Cryptography.RijndaelManaged())
            {
                //Settings  
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.  
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream  
                                // and place them in a string.  
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }

        public ActionResult Sendgmail()
        {
            return View();
        }
        public System.Net.Mail.MailAddress From { get; set; }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="recievermailID"></param>
        /// <param name="encryptedfileUrl"></param> 
        /// <returns></returns>
        [HttpPost]
        public string Sendgmail(string recievermailID, string fileUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string encryptedfileUrl = fileUrl.Replace(" ", "+");
                    string SenderMail = ConfigurationManager.AppSettings["SenderMail"].ToString();
                    string senderPassword = ConfigurationManager.AppSettings["Password"].ToString();
                    string Host = ConfigurationManager.AppSettings["SmtpServer"].ToString();

                    //Creating the object of MailMessage
                    MailMessage mail = new MailMessage();
                    mail.From = new MailAddress(SenderMail);
                    mail.To.Add(recievermailID);
                    mail.Subject = "File URL";

                    //network and security related credential    
                    SmtpClient SmtpServer = new SmtpClient(Host);
                    SmtpServer.Credentials = new System.Net.NetworkCredential(SenderMail, senderPassword);
                    SmtpServer.Port = 587;
                    SmtpServer.EnableSsl = true;
                    // SmtpServer.UseDefaultCredentials = true;

                    mail.Body = "Hello," + Environment.NewLine + Environment.NewLine + "Use this link to preview the File : " + encryptedfileUrl + Environment.NewLine + Environment.NewLine + " This email has been sent on behalf of request confirmation, you can login to website to verify your details." + Environment.NewLine + Environment.NewLine +
                                   "Thanks," + Environment.NewLine + "MarketStudio ";

                    SmtpServer.Send(mail);

                    //spliting Multiple reciever email id
                    string[] MuliteReciever = recievermailID.Split(',');
                    foreach (string Tomail in MuliteReciever)
                    {
                        mail.To.Add(Tomail);
                    }
                }
                return "okey";
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
                return null;
            }
        }

        /// <summary>
        /// To get the OCP URL from web config
        /// </summary>
        /// <returns></returns>
        public string GetOCPURL()
        {
            try
            {
                var url = ConfigurationManager.AppSettings["WebURL"].ToString();
                if (url == "") {
                    return "Empty";
                }
                else {
                    return url;
                }
            }
            catch(Exception ex)
            {
                _logger.Error("Error at AppCOntroller : GetOCPURL", ex);
                return "Empty";
            }
        }

        /// <summary>
        /// To get the online catalog portal URL from the web config
        /// </summary>
        /// <returns></returns>
        public string GetPDFXpressURL()
        {
            try
            {
                var url = ConfigurationManager.AppSettings["PDFXpress_URL"].ToString();
                if (url == "") {
                    return "Empty";
                }
                else {
                    return url;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at AppCOntroller : GetPDFXpressURL", ex);
                return "Empty";
            }
        }

    }
}