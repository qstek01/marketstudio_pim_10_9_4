﻿using log4net;
using LS.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace LS.Web.Controllers
{
    public class AnalyticsApiController : ApiController
    {
        private static readonly ILog _logger = LogManager.GetLogger(typeof(AnalyticsApiController));
        readonly CSEntities _dbcontext = new CSEntities();
        // GET: api/AnalyticsApi
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }
    
        // GET: api/AnalyticsApi/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/AnalyticsApi
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/AnalyticsApi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/AnalyticsApi/5
        public void Delete(int id)
        { 
        }
        public void getUser(string USER_ID, string RoleId, string username,string session_id)
        {              
            var objExistsforLogout = _dbcontext.QSWS_USER_ANALYTICS.Where(x => x.EMAIL == username && x.LOGGED_OUT_DATETIME == null).OrderByDescending(x => x.ID).ToList();
            objExistsforLogout.ForEach(a => a.LOGGED_OUT_DATETIME = DateTime.Now);
            _dbcontext.SaveChanges();
            
            try
            {
                var Role_Id = _dbcontext.aspnet_Roles.Join(_dbcontext.vw_aspnet_UsersInRoles, c => c.RoleId, cu => cu.RoleId, (c, cu) => new { c, cu }).Where(y => y.cu.UserId.ToString() == USER_ID).Select(x => x.c.Role_id).FirstOrDefault();
                RoleId = Role_Id.ToString();
                string Session_ID = session_id;
                string loggedintime = DateTime.Now.ToString();
                string str_Result = string.Empty;
                
                var objCustomSettings = new QSWS_USER_ANALYTICS();
                objCustomSettings.EMAIL = username;
                objCustomSettings.USER_ID = USER_ID;
                objCustomSettings.ORGANIZATION_ROLE = RoleId;
                objCustomSettings.Session_Id = Session_ID;
                objCustomSettings.LOGGED_IN_DATETIME = Convert.ToDateTime(loggedintime);
                _dbcontext.QSWS_USER_ANALYTICS.Add(objCustomSettings);
                _dbcontext.SaveChanges();
                str_Result = "Updated Successfully";
            }

            catch (Exception ex)
            {
                _logger.Error("Error at AnalyticsController : getUser", ex);

            }


        }

        public void getLogoutDetails()
        {
            string session_id = HttpContext.Current.Session.SessionID.ToString();
            var objExists = _dbcontext.QSWS_USER_ANALYTICS.Where(x => x.Session_Id == session_id && x.LOGGED_OUT_DATETIME == null).FirstOrDefault();
            var getUseremail = User.Identity.Name;
            string EmailId = getUseremail;
            string loggedouttime = DateTime.Now.ToString();
            string str_Result = string.Empty;
            try
            {
                var objExistsforLogout = _dbcontext.QSWS_USER_ANALYTICS.Where(x => x.EMAIL == EmailId && x.LOGGED_OUT_DATETIME == null).OrderByDescending(x => x.ID).ToList();
                objExistsforLogout.ForEach(a => a.LOGGED_OUT_DATETIME = DateTime.Now);
                _dbcontext.SaveChanges();
                str_Result = "Updated Successfully";
            }

            catch (Exception ex)
            {
                _logger.Error("Error at AnalyticsController : getLogoutDetails", ex);

            }
        }
    }
}
