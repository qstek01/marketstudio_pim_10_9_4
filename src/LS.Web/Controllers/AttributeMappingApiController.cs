﻿using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Kendo.DynamicLinq;
using LS.Data;
using LS.Data.Model.CatalogSectionModels;
using log4net;
using LS.Data.Model;
using System.Web.Http;
using System.Collections;
using System.Net.Http;
using System.Net;
using LS.Data.Utilities;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using LS.Data.Model.ProductView;
using System.Data.OleDb;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data;
using System;
using System.Globalization;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Linq;
using System.Configuration;
using System.Web.Script.Serialization;
using Infragistics.Documents.Excel;
using System.Data.Entity.Core.Objects;
using LS.Web.Models;

namespace LS.Web.Controllers
{
    public class AttributeMappingApiController : ApiController
    {
        DataTable _dtable;
        DataTable _objdatatable;
        private string importStatus = string.Empty;
        private string Message = string.Empty;
        private static ILog _logger = LogManager.GetLogger(typeof(AdvanceImportApiController));
        private ImportController importController = new ImportController();
        private AdvanceImportApiController advanceaimportapi = new AdvanceImportApiController();
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        public static DataSet TempDS = new DataSet();
        public static DataSet Customtbl = new DataSet();
        static readonly ILog Logger = LogManager.GetLogger(typeof(SubProductsImportApiController));
        readonly CSEntities _dbcontext = new CSEntities();
        readonly DataSet _dsSheets = new DataSet();
        string _message;
        public static string customtableexcelpath = string.Empty;
        readonly string _connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        //static ILog _logger = LogManager.GetLogger(typeof(FamilyApiController));
        readonly ImportController _ic = new ImportController();
        public string SqlString = "";
        public string RefID = "";
        private DataSet _recycleDSPerma = new DataSet();
        XpressCatalogController obj_XpressCatalogController = new XpressCatalogController();
        HomeApiController objhome = new HomeApiController();
        public CSEntities objLS = new CSEntities();


        public enum CSF
        {
            [Description("CATALOG_ID")]
            CATALOG_ID = 1,
            [Description("CATALOG_NAME")]
            CATALOG_NAME = 2,
            [Description("CATALOG_VERSION")]
            CATALOG_VERSION = 3,
            [Description("CATALOG_DESCRIPTION")]
            CATALOG_DESCRIPTION = 4,


            //marketstudio
            [Description("CATEGORY_ID")]
            CATEGORY_ID = 101,
            [Description("CATEGORY_NAME")]
            CATEGORY_NAME = 102,
            [Description("SUBCATID_L1")]
            SUBCATID_L1 = 103,
            [Description("SUBCATNAME_L1")]
            SUBCATNAME_L1 = 104,
            [Description("SUBCATID_L2")]
            SUBCATID_L2 = 105,
            [Description("SUBCATNAME_L2")]
            SUBCATNAME_L2 = 106,
            [Description("SUBCATID_L3")]
            SUBCATID_L3 = 107,
            [Description("SUBCATNAME_L3")]
            SUBCATNAME_L3 = 108,
            [Description("SUBCATID_L4")]
            SUBCATID_L4 = 109,
            [Description("SUBCATNAME_L4")]
            SUBCATNAME_L4 = 110,
            [Description("SUBCATID_L5")]
            SUBCATID_L5 = 111,
            [Description("SUBCATNAME_L5")]
            SUBCATNAME_L5 = 112,
            [Description("SUBCATID_L6")]
            SUBCATID_L6 = 113,
            [Description("SUBCATNAME_L6")]
            SUBCATNAME_L6 = 114,
            [Description("SUBCATID_L7")]
            SUBCATID_L7 = 115,
            [Description("SUBCATNAME_L7")]
            SUBCATNAME_L7 = 116,
            [Description("SUBCATID_L8")]
            SUBCATID_L8 = 117,
            [Description("SUBCATNAME_L8")]
            SUBCATNAME_L8 = 118,
            [Description("SUBCATID_L9")]
            SUBCATID_L9 = 119,
            [Description("SUBCATNAME_L9")]
            SUBCATNAME_L9 = 120,
            [Description("CATEGORY_SHORT_DESC")]
            CATEGORY_SHORT_DESC = 151,
            [Description("CATEGORY_IMAGE_FILE")]
            CATEGORY_IMAGE_FILE = 152,
            [Description("CATEGORY_IMAGE_NAME")]
            CATEGORY_IMAGE_NAME = 153,
            [Description("CATEGORY_IMAGE_TYPE")]
            CATEGORY_IMAGE_TYPE = 154,
            [Description("CATEGORY_IMAGE_FILE2")]
            CATEGORY_IMAGE_FILE2 = 155,
            [Description("CATEGORY_IMAGE_NAME2")]
            CATEGORY_IMAGE_NAME2 = 156,
            [Description("CATEGORY_IMAGE_TYPE2")]
            CATEGORY_IMAGE_TYPE2 = 157,
            [Description("CATEGORY_CUSTOM_NUM_FIELD1")]
            CATEGORY_CUSTOM_NUM_FIELD1 = 158,
            [Description("CATEGORY_CUSTOM_NUM_FIELD2")]
            CATEGORY_CUSTOM_NUM_FIELD2 = 159,
            [Description("CATEGORY_CUSTOM_NUM_FIELD3")]
            CATEGORY_CUSTOM_NUM_FIELD3 = 160,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD1")]
            CATEGORY_CUSTOM_TEXT_FIELD1 = 161,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD2")]
            CATEGORY_CUSTOM_TEXT_FIELD2 = 162,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD3")]
            CATEGORY_CUSTOM_TEXT_FIELD3 = 163,



            [Description("FAMILY_ID")]
            FAMILY_ID = 201,
            [Description("FAMILY_NAME")]
            FAMILY_NAME = 202,
            [Description("SUBFAMILY_ID")]
            SUBFAMILY_ID = 203,
            [Description("SUBFAMILY_NAME")]
            SUBFAMILY_NAME = 204,
            [Description("CATEGORY_ID")]
            FAMCATEGORY_ID = 205,
            [Description("FOOT_NOTES")]
            FOOT_NOTES = 206,
            [Description("STATUS")]
            STATUS = 207,
            [Description("FAMILY_SORT")]
            FAMILY_SORT = 208,

            [Description("PRODUCT_ID")]
            PRODUCT_ID = 301,
            [Description("CATALOG_ITEM_NO")]
            //[Description("CAT#")]
            CATALOG_ITEM_NO = 302,
            [Description("SUBPRODUCT_ID")]
            SUBPRODUCT_ID = 303,
            [Description("SUBCATALOG_ITEM_NO")]
            SUBCATALOG_ITEM_NO = 304,


            [Description("PRODUCT_SORT")]
            PRODUCT_SORT = 305,
            [Description("REMOVE_PRODUCT")]
            REMOVE_PRODUCT = 306,
            [Description("ATTRIBUTE_ID")]
            ATTRIBUTE_ID = 307,

        };

        [System.Web.Http.HttpGet]
        public IList importExcelSheetSelection(string excelPath)
        {
            List<ImportSheetName> newList = new List<ImportSheetName>();
            //  excelPath = "C:\\Users\\Rakesh\\Downloads\\LS Export.xls";
            //var myList = new List<ImportSheetName>();
            try
            {
                if (!string.IsNullOrEmpty(excelPath))
                {
                    // bool hasHeaders = true;
                    //  string HDR = hasHeaders ? "Yes" : "No";
                    // string strConn;
                    //if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                    //    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                    //              ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    //else
                    //    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                    //              ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";

                    Infragistics.Documents.Excel.Workbook workbook =
                        Infragistics.Documents.Excel.Workbook.Load(excelPath);
                    // OleDbConnection conn = new OleDbConnection(strConn);
                    // conn.Open();
                    //  DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,null);
                    //int i = 0;
                    // string SheetName = string.Empty;
                    foreach (Worksheet rows in workbook.Worksheets)
                    {

                        //SheetName = Convert.ToString(rows[2]);
                        // if (!SheetName.EndsWith("_"))
                        // {
                        // if (SheetName.Contains("$"))
                        // {
                        //     SheetName = SheetName.Replace("$", "");
                        // }
                        // if (SheetName.Contains("'"))
                        //  {
                        //      SheetName = SheetName.Replace("'", "");
                        //  }

                        ImportSheetName objImportSheetName = new ImportSheetName();
                        //   if (!rows.Name.ToLower().Contains("subproducts1"))
                        {
                            objImportSheetName.TABLE_NAME = rows.Name;
                            objImportSheetName.SHEET_PATH = excelPath;
                            newList.Add(objImportSheetName); //sheetnames[i] = schemaTable.Rows[i];
                        }
                        //  i++;
                    }
                }
                //else
                //{
                //    var objImportSheetName = new ImportSheetName();
                //    objImportSheetName.TABLE_NAME = "";
                //    objImportSheetName.SHEET_PATH = "";
                //    newList.Add(objImportSheetName);
                //}
                // conn.Close();


            }

            catch (Exception objException)
            {
                Logger.Error("Error at loading importExcelSheetSelection in Import Controller", objException);
                return null;
            }
            return newList;
        }
        //public IList ImportExcelSheetSelection(string excelPath)
        //{
        //    var newList = new List<ImportSheetName>();
        //    //  excelPath = "C:\\Users\\Rakesh\\Downloads\\LS Export.xls";
        //    //var myList = new List<ImportSheetName>();
        //    customtableexcelpath = excelPath;
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(excelPath))
        //        {
        //            const bool hasHeaders = true;
        //            const string hdr = hasHeaders ? "Yes" : "No";
        //            string strConn;
        //            if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() != ".csv")
        //            {
        //                if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
        //                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 12.0;HDR=" + hdr + ";IMEX=1\"";
        //                else
        //                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 8.0;HDR=" + hdr + ";IMEX=1\"";
        //                var conn = new OleDbConnection(strConn);
        //                conn.Open();
        //                var schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
        //                int i = 0;
        //                if (schemaTable != null)
        //                    foreach (DataRow rows in schemaTable.Rows)
        //                    {

        //                        string sheetName = Convert.ToString(rows[2]);
        //                        if (!sheetName.EndsWith("_"))
        //                        {
        //                            if (sheetName.Contains("$"))
        //                            {
        //                                sheetName = sheetName.Replace("$", "");
        //                            }
        //                            if (sheetName.Contains("'"))
        //                            {
        //                                sheetName = sheetName.Replace("'", "");
        //                            }

        //                            var objImportSheetName = new ImportSheetName();
        //                            objImportSheetName.TABLE_NAME = sheetName;
        //                            objImportSheetName.SHEET_PATH = excelPath;
        //                            newList.Add(objImportSheetName); //sheetnames[i] = schemaTable.Rows[i];
        //                            i++;
        //                        }

        //                    }
        //                conn.Close();
        //            }
        //        }

        //    }

        //    catch (Exception objException)
        //    {
        //        Logger.Error("Error at loading importExcelSheetSelection in Import Controller", objException);
        //        return null;
        //    }
        //    return newList;
        //}

        [System.Web.Http.HttpGet]
        public DataTable ImportSelectionColumnGrid(string sheetName, string excelPath)
        {
            var dt = new DataTable();
            try
            {
                if (!string.IsNullOrEmpty(excelPath))
                {
                    Workbook workbook = Workbook.Load(excelPath);
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    string strConn;
                    if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    else
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    Customtbl = new DataSet();
                    //   OleDbCommand cmd = new OleDbCommand();
                    //   OleDbDataAdapter oleda = new OleDbDataAdapter();
                    //   cmd.Connection = conn;
                    //   cmd.CommandType = CommandType.Text;
                    //   cmd.CommandText = "SELECT * FROM [" + sheetName + "$]";
                    //   oleda = new OleDbDataAdapter(cmd);
                    //  // oleda.Fill(Customtbl);

                    //  // TempDS = Customtbl;
                    ////   conn.Close();

                    if (!sheetName.Contains("$"))
                    {
                        sheetName = sheetName + "$";
                    }
                    dt = new DataTable(sheetName);
                    var dc = new DataColumn("ExcelColumn", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("CatalogField", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("SelectedToImport", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("IsSystemField", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("FieldType", typeof(string));
                    dt.Columns.Add(dc);

                    string sheetNameworkbook = sheetName.Replace("$", "").TrimEnd();
                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells.Count();
                    for (int j = 0; j < iCols; j++)
                    {
                        // Name Changes Product(Family) in November 11 2022
                        if (sheetNameworkbook == "Products")
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null && Convert.ToString(workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value).ToLower() != "sort")
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace("PRODUCT_ID", "FAMILY_ID")
                                        .Replace("PRODUCT_NAME", "FAMILY_NAME")
                                        .Replace("SUBPRODUCT_ID", "SUBFAMILY_ID")
                                        .Replace("SUBPRODUCT_NAME", "SUBFAMILY_NAME")
                                        .Replace("Product_PdfTemplate", "Family_PdfTemplate")
                                        .Replace("Item_PdfTemplate", "Product_PdfTemplate")
                                        .Replace("PRODUCT_PUBLISH2PDF", "FAMILY_PUBLISH2PDF")
                                        .Replace("PRODUCT_PUBLISH2PRINT", "FAMILY_PUBLISH2PRINT")
                                        .Replace("PRODUCT_PUBLISH2WEB", "FAMILY_PUBLISH2WEB")
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();
                                dr[0] = sColName;
                                dt.Rows.Add(dr);

                            }
                        }
                        else if (sheetNameworkbook == "Items")
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null && Convert.ToString(workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value).ToLower() != "sort")
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace("PRODUCT_ID", "FAMILY_ID")
                                        .Replace("ITEM_ID", "PRODUCT_ID")
                                        .Replace("PRODUCT_NAME", "FAMILY_NAME")
                                        .Replace("SUBPRODUCT_ID", "SUBFAMILY_ID")
                                        .Replace("SUBPRODUCT_NAME", "SUBFAMILY_NAME")
                                        .Replace("ITEM_PUBLISH2WEB", "PRODUCT_PUBLISH2WEB")
                                        .Replace("ITEM_PUBLISH2PRINT", "PRODUCT_PUBLISH2PRINT")
                                        .Replace("ITEM_PUBLISH2PDF", "PRODUCT_PUBLISH2PDF")
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();
                                dr[0] = sColName;
                                dt.Rows.Add(dr);
                            }
                        }
                        else
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null && Convert.ToString(workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value).ToLower() != "sort")
                            {
                                String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                DataRow dr = dt.NewRow();
                                dr[0] = sColName;
                                dt.Rows.Add(dr);
                            }
                        }

                        //if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null && Convert.ToString(workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value).ToLower() != "sort")
                        //{
                        //    String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                        //    DataRow dr = dt.NewRow();
                        //    dr[0] = sColName;
                        //    dt.Rows.Add(dr);
                        //}
                    }

                    _dsSheets.Tables.Add(dt);
                    dt.Dispose();
                    _dtable = GetExcelColumn2CatalogMapping(sheetName);
                    conn.Close();



                }
                return _dtable;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at ImportApiController :importSelectionColumnGrid", objException);
                return null;
            }
        }


        public static Boolean IsCatalogStudioField(String enumFieldName)
        {
            return Enum.GetNames(typeof(CSF)).Any(val => enumFieldName == val);
        }
        public DataTable GetExcelColumn2CatalogMapping(String xlsSheetName)
        {
            try
            {
                DataTable dt = _dsSheets.Tables[xlsSheetName];
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    string sCheckAttributeName = dt.Rows[i][0].ToString().ToUpper();
                    //Check if this is a CatalogStudio system defined field name
                    Boolean vaild = IsCatalogStudioField(sCheckAttributeName);

                    if (sCheckAttributeName == "SUBITEM#" || sCheckAttributeName == "ITEM#")
                    {
                        vaild = true;
                    }
                    if (vaild) //CatalogStudio Field
                    {
                        dr["CatalogField"] = sCheckAttributeName;
                        dr["IsSystemField"] = true;
                        dr["FieldType"] = 0;
                        dr["SelectedToImport"] = true;
                    }
                    else //User defined attribute name
                    {
                        var drAttr = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == sCheckAttributeName).ToList();

                        //var reResult = (IEnumerable<DataRow>)result;
                        //_tblAttr.Select("ATTRIBUTE_NAME = '" + sCheckAttributeName.Replace("'", "''") + "'");
                        //DataTable drAttr = reResult.CopyToDataTable();

                        if (drAttr.Count > 0)
                        {
                            string sAttributeNameInCatalog = drAttr[0].ATTRIBUTE_NAME;
                            string sAttributeDatatype = drAttr[0].ATTRIBUTE_DATATYPE;
                            string sAttrType = drAttr[0].ATTRIBUTE_TYPE.ToString(CultureInfo.InvariantCulture);
                            if (sAttributeDatatype == "Date and Time")
                            {
                                if (sAttrType == "6" || sAttrType == "1" || sAttrType == "3")
                                {
                                    dr["FieldType"] = 10;
                                    dr["SelectedToImport"] = true;
                                    dr["IsSystemField"] = true;
                                }
                                else if (sAttrType == "7" || sAttrType == "11" || sAttrType == "13")
                                {
                                    dr["FieldType"] = 14;
                                    dr["SelectedToImport"] = true;
                                    dr["IsSystemField"] = true;
                                }
                            }
                            else
                            {
                                dr["FieldType"] = drAttr[0].ATTRIBUTE_TYPE.ToString(CultureInfo.InvariantCulture);
                                dr["SelectedToImport"] = true;
                                dr["IsSystemField"] = true;
                            }
                            dr["CatalogField"] = sAttributeNameInCatalog;
                            //dr["IsSystemField"] = false;

                        }
                        else
                        {
                            dr["FieldType"] = "";
                            dr["SelectedToImport"] = false;
                        }
                    }

                    i++;
                }
                int inc = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    string CheckActionColumn = dt.Rows[inc][0].ToString().ToUpper();
                    if (CheckActionColumn == "ACTION")
                    {
                        dr["FieldType"] = 0;
                        dr["SelectedToImport"] = false;
                        dr["IsSystemField"] = true;
                    }
                    inc++;

                }


                return dt;

            }
            catch (Exception objException)
            {
                Logger.Error("Error at loading GetExcelColumn2CatalogMapping in Import Controller", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public JsonResult GetImportSpecs(string sheetName, string excelPath)
        {
            // ExcelPath = "C:\\Users\\Rakesh\\Downloads\\LS Export.xls";
            DataTable dtable = ImportSelectionColumnGrid(sheetName, excelPath);
            try
            {
                var dynamicColumns = new Dictionary<string, string>();
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                JsonWriter jsonWriter = new JsonTextWriter(sw);
                var model = new DashBoardModel();
                using (DataTableReader reader = dtable.CreateDataReader())
                {
                    jsonWriter.WriteStartObject();
                    jsonWriter.WritePropertyName("Data");
                    jsonWriter.WriteStartArray();
                    while (reader.Read())
                    {
                        jsonWriter.WriteStartObject();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            jsonWriter.WritePropertyName(reader.GetName(i));
                            jsonWriter.WriteValue(reader.GetValue(i).ToString());
                            if (!dynamicColumns.ContainsKey(reader.GetName(i)) && !string.IsNullOrEmpty(reader.GetName(i)))
                            {
                                dynamicColumns.Add(reader.GetName(i), reader.GetValue(i).ToString());
                            }
                        }
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WritePropertyName("Columns");
                    jsonWriter.WriteStartArray();
                    foreach (string key in dynamicColumns.Keys)
                    {
                        jsonWriter.WriteStartObject();
                        jsonWriter.WritePropertyName("title");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WritePropertyName("field");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WriteEndObject();
                }
                // model.Data = sb.ToString();

                //Name Changes in display
                if (sheetName == "Products")
                {
                    model.Data = sb.ToString().Replace("FAMILY_ID", "PRODUCT_ID")
                                              .Replace("FAMILY_NAME", "PRODUCT_NAME")
                                              .Replace("SUBFAMILY_ID", "SUBPRODUCT_ID")
                                              .Replace("SUBFAMILY_NAME", "SUBPRODUCT_NAME")
                                              .Replace("FAMILY_PUBLISH2PDF", "PRODUCT_PUBLISH2WEB")
                                              .Replace("FAMILY_PUBLISH2PRINT", "PRODUCT_PUBLISH2PRINT")
                                              .Replace("FAMILY_PUBLISH2WEB", "PRODUCT_PUBLISH2PDF");
                }
                else if (sheetName == "Items")
                {
                    model.Data = sb.ToString().Replace("PRODUCT_ID", "ITEM_ID")
                                       .Replace("FAMILY_ID", "PRODUCT_ID")
                                       .Replace("FAMILY_NAME", "PRODUCT_NAME")
                                       .Replace("SUBFAMILY_ID", "SUBPRODUCT_ID")
                                       .Replace("SUBFAMILY_NAME", "SUBPRODUCT_NAME");

                }
                return new JsonResult { Data = model };

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : GetProdSpecs", objexception);
                return null;
            }
        }


        public DataTable SelectedColumnsToImport(DataTable objdatatable, JArray model)
        {
            try
            {
                var functionAlloweditems = ((JArray)model).Select(x => new SelectedAttribute()
                {
                    ExcelColumn = (string)x["ExcelColumn"],
                    FieldType = (string)x["FieldType"],
                    SelectedToImport = (bool)x["SelectedToImport"]
                }).ToList();

                DataTable dt = new DataTable();
                dt.Columns.Add("ATTRIBUTE_TYPE");
                dt.Columns.Add("ATTRIBUTE_NAME");

                foreach (var item in functionAlloweditems)
                {
                    if (item.SelectedToImport)
                    {
                        DataRow dr = dt.NewRow();
                        dr["ATTRIBUTE_TYPE"] = item.FieldType;
                        dr["ATTRIBUTE_NAME"] = item.ExcelColumn;
                        dt.Rows.Add(dr);
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                // _logger.Error("Error at SelectedColumnsToImport : exceldata", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public string FinishImport(string sheetName, string allowDuplicate, string excelPath, JArray model)
        {
            string importTemp;
            importTemp = Guid.NewGuid().ToString();
            int userProductCount = 0;
            int userSkuProductCount = 0;
            try
            {
                int itemVal = Convert.ToInt32(allowDuplicate);
                using (var conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    string sqlString = "EXEC('if((select COUNT(*) from sys.tables where name = ''Sheet'')<>0)BEGIN DROP TABLE [Sheet] END')";
                    // _ic.importExcelSheetSelection(excelPath, sheetName);
                    var dbCommand = new SqlCommand(sqlString, conn);
                    dbCommand.ExecuteNonQuery();
                    _objdatatable = _ic.exceldata(excelPath, sheetName);
                    sqlString = _ic.CreateTable("[Sheet]", excelPath, sheetName, _objdatatable);
                    dbCommand = new SqlCommand(sqlString, conn);
                    dbCommand.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(conn)
                    {
                        DestinationTableName = "[Sheet]"
                    };
                    bulkCopy.WriteToServer(_objdatatable);
                    DataTable oattType = _ic.SelectedColumnsToImport(_objdatatable, model);
                    var sqlstring1 = new SqlCommand("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##ATTRIBUTEMAPPING" + importTemp + "]'')BEGIN DROP TABLE [##ATTRIBUTEMAPPING" + importTemp + "] END')", conn);
                    sqlstring1.ExecuteNonQuery();
                    var cmd2 = new SqlCommand();
                    cmd2.Connection = conn;
                    sqlString = _ic.CreateTableToImport("[##ATTRIBUTEMAPPING" + importTemp + "]", oattType);
                    cmd2.CommandText = sqlString;
                    cmd2.CommandType = CommandType.Text;
                    cmd2.ExecuteNonQuery();
                    bulkCopy.DestinationTableName = "[##ATTRIBUTEMAPPING" + importTemp + "]";
                    bulkCopy.WriteToServer(oattType);
                    DataSet catalog = new DataSet();
                    SqlDataAdapter dd = new SqlDataAdapter("SELECT TOP(1) CATALOG_NAME FROM SHEET", conn);
                    dd.Fill(catalog);
                    if (catalog.Tables[0].Rows.Count > 0)
                    {
                        string column_value = catalog.Tables[0].Rows[0][0].ToString();
                        string attrmapp = "EXEC STP_LS_ATTRIBUTEMAPPING '" + column_value + "','" + importTemp + "'";
                        var dbCommandap = new SqlCommand(attrmapp, conn);
                        dbCommandap.ExecuteNonQuery();
                        return "Import Success~" + importTemp;
                    }
                    else
                    {
                        return "Import Failed~" + importTemp;

                    }


                }
            }

            catch (Exception ex)
            {
                Logger.Error("Error at ImportApiController : FinishImport", ex);
                return "Import Failed~" + importTemp;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="ID"></param>
        /// <param name="fileName"></param>
        /// <param name="CatalogId">
        /// By : Mariyavijayan.
        /// </param>
        public void CheckPdfData(string Type, string ID, string fileName, string CatalogId)
        {
            try
            {


                var con = new SqlConnection(_connectionString);

                int cat_ID = Convert.ToInt32(CatalogId);

                string customer_Name = User.Identity.Name.ToString();
                int customer_id = _dbcontext.Customers.Where(x => x.CustomerName == customer_Name).Select(x => x.CustomerId).SingleOrDefault();


                // Check already the values is exist or not 
                if (Type == "Category")
                {
                    if (!ID.Contains("CAT"))
                    {
                        ID = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == ID).Select(x => x.CATEGORY_ID).SingleOrDefault();
                    }

                }



                //SqlCommand checkTheValuePdfxpressHierarchy = new SqlCommand("select count(*) from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatalogId + "' and ASSIGN_TO = '" + ID + "' and CUSTOMER_ID = '" + customer_id + "' and [TYPE] = '" + Type + "' ", con);
                //con.Open();
                //int UserExist = (int)checkTheValuePdfxpressHierarchy.ExecuteScalar();
                //con.Close();

                var Exists = _dbcontext.TB_PDFXPRESS_HIERARCHY.Where(a => a.CATALOG_ID == cat_ID && a.ASSIGN_TO == ID && a.CUSTOMER_ID == customer_id && a.TYPE == Type).Select(x => x.TEMPLATE_NAME).FirstOrDefault();


                //if (savePdfXpressValue)   // Tosave the data when click the save button other wise its not saved.
                //{
                if (Exists != null)
                {


                    ////Username exist   [Delate]
                    //SqlCommand DeleteTheValuePdfxpressHierarchy = new SqlCommand("Delete from  TB_PDFXPRESS_HIERARCHY where CATALOG_ID = '" + CatalogId + "' and ASSIGN_TO = '" + ID + "' and CUSTOMER_ID = '" + customer_id + "' and [TYPE] = '" + Type + "' ", con);
                    //con.Open();
                    //DeleteTheValuePdfxpressHierarchy.ExecuteNonQuery();
                    //con.Close();



                    var pdfXpressdet = _dbcontext.TB_PDFXPRESS_HIERARCHY.FirstOrDefault(a => a.CATALOG_ID == cat_ID && a.ASSIGN_TO == ID && a.CUSTOMER_ID == customer_id && a.TYPE == Type);
                    _dbcontext.TB_PDFXPRESS_HIERARCHY.Remove(pdfXpressdet);
                    _dbcontext.SaveChanges();


                    SaveTypeOfPdfxpressHierarchy(Type, ID, fileName, CatalogId);
                }
                else
                {
                    //Username doesn't exist.  [Insert]
                    SaveTypeOfPdfxpressHierarchy(Type, ID, fileName, CatalogId);
                }
                //}




            }
            catch (Exception)
            {

                throw;
            }
        }


        /// <summary>
        /// To Save the corresponding records into TB_PDFXPRESS_HIERARCHY 
        /// In this method have a non return type
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="ID"></param>
        /// Created By Mariyavijayan

        public void SaveTypeOfPdfxpressHierarchy(string Type, string ID, string fileName, string CatalogId)
        {

            int customer_id = 0;
            string customer_Name = string.Empty;


            string userName = User.Identity.Name;
            customer_Name = User.Identity.Name.ToString();
            customer_id = _dbcontext.Customers.Where(x => x.CustomerName == customer_Name).Select(x => x.CustomerId).SingleOrDefault();

            var userId = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();


            if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template")))
            {
                Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template"));
            }




            string CategoryId = string.Empty;
            string query = string.Empty;
            string FamilyId;
            string ProductId;


            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            // SqlCommand cmd = new SqlCommand();

            //  string FilePath = Path.Combine(Server.MapPath("~/Customer Templates/" + userName + "/Opened Template"), fileName);

            // To save new file Path.


            int Catalog_Id = Convert.ToInt32(CatalogId);




            // var FilePath = System.Web.HttpContext.Current.Server.MapPath("~/PDF_Template/" + userId + "");
            // string FilePath = Path.Combine(Server.MapPath("~/PDF_Template/" + userId + ""), fileName);

            string customerPath = "PDF_Template";

            string FilePath = "" + customerPath + @"\" + fileName + "";




            // In linq
            var objPdfXpress = new TB_PDFXPRESS_HIERARCHY();

            switch (Type)
            {
                case "ALL":
                    break;

                case "Catalog":
                    FilePath = "" + customerPath + @"\" + CatalogId + "_" + fileName + "";

                    fileName = CatalogId + "_" + fileName + "";

                    objPdfXpress.PROJECT_ID = 0;
                    objPdfXpress.TEMPLATE_NAME = fileName;
                    objPdfXpress.TEMPLATE_PATH = FilePath;
                    objPdfXpress.TYPE = Type;
                    objPdfXpress.ASSIGN_TO = CatalogId;
                    objPdfXpress.CUSTOMER_ID = customer_id;
                    objPdfXpress.CATALOG_ID = Catalog_Id;
                    objPdfXpress.CREATED_USER = customer_Name;
                    objPdfXpress.CREATED_DATE = DateTime.Now;
                    objPdfXpress.MODIFIED_USER = customer_Name;
                    objPdfXpress.MODIFIED_DATE = DateTime.Now;



                    break;

                case "Category":

                    // Check already the values is exist or not 

                    if (!ID.Contains("CAT"))
                    {
                        ID = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == ID).Select(x => x.CATEGORY_ID).SingleOrDefault();
                    }

                    string categoryId = ID;




                    categoryId = objhome.FindRootLevelCategoryId(categoryId);


                    objPdfXpress.PROJECT_ID = 0;
                    objPdfXpress.TEMPLATE_NAME = fileName;
                    objPdfXpress.TEMPLATE_PATH = FilePath;
                    objPdfXpress.TYPE = Type;
                    objPdfXpress.ASSIGN_TO = categoryId;
                    objPdfXpress.CUSTOMER_ID = customer_id;
                    objPdfXpress.CATALOG_ID = Catalog_Id;
                    objPdfXpress.CREATED_USER = customer_Name;
                    objPdfXpress.CREATED_DATE = DateTime.Now;
                    objPdfXpress.MODIFIED_USER = customer_Name;
                    objPdfXpress.MODIFIED_DATE = DateTime.Now;



                    break;

                case "Family":

                    FamilyId = ID;


                    objPdfXpress.PROJECT_ID = 0;
                    objPdfXpress.TEMPLATE_NAME = fileName;
                    objPdfXpress.TEMPLATE_PATH = FilePath;
                    objPdfXpress.TYPE = Type;
                    objPdfXpress.ASSIGN_TO = FamilyId;
                    objPdfXpress.CUSTOMER_ID = customer_id;
                    objPdfXpress.CATALOG_ID = Catalog_Id;
                    objPdfXpress.CREATED_USER = customer_Name;
                    objPdfXpress.CREATED_DATE = DateTime.Now;
                    objPdfXpress.MODIFIED_USER = customer_Name;
                    objPdfXpress.MODIFIED_DATE = DateTime.Now;



                    break;

                case "Product":

                    ProductId = ID;

                    objPdfXpress.PROJECT_ID = 0;
                    objPdfXpress.TEMPLATE_NAME = fileName;
                    objPdfXpress.TEMPLATE_PATH = FilePath;
                    objPdfXpress.TYPE = Type;
                    objPdfXpress.ASSIGN_TO = ProductId;
                    objPdfXpress.CUSTOMER_ID = customer_id;
                    objPdfXpress.CATALOG_ID = Catalog_Id;
                    objPdfXpress.CREATED_USER = customer_Name;
                    objPdfXpress.CREATED_DATE = DateTime.Now;
                    objPdfXpress.MODIFIED_USER = customer_Name;
                    objPdfXpress.MODIFIED_DATE = DateTime.Now;



                    break;
            }


            _dbcontext.TB_PDFXPRESS_HIERARCHY.Add(objPdfXpress);
            _dbcontext.SaveChanges();


            //switch (Type)
            //{
            //    case "ALL":
            //        break;

            //    case "Catalog":
            //        query = "INSERT INTO TB_PDFXPRESS_HIERARCHY(PROJECT_ID,TEMPLATE_NAME,TEMPLATE_PATH,TYPE,ASSIGN_TO,CUSTOMER_ID,CATALOG_ID,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) VALUES(0,'" + fileName + "','" + FilePath + "','" + Type + "', '" + Catalog_Id + "' , '" + customer_id + "',  '" + Catalog_Id + "','" + customer_Name + "',getdate(),'" + customer_Name + "',getdate())";
            //        break;

            //    case "Category":

            //       // string categoryId = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == ID).Select(x => x.CATEGORY_ID).SingleOrDefault();
            //        query = "INSERT INTO TB_PDFXPRESS_HIERARCHY(PROJECT_ID,TEMPLATE_NAME,TEMPLATE_PATH,TYPE,ASSIGN_TO,CUSTOMER_ID,CATALOG_ID,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) VALUES(0,'" + fileName + "','" + FilePath + "','" + Type + "', '" + ID + "' , '" + customer_id + "',  '" + Catalog_Id + "','" + customer_Name + "',getdate(),'" + customer_Name + "',getdate())";
            //        break;

            //    case "Family":

            //        FamilyId = ID;
            //        query = "INSERT INTO TB_PDFXPRESS_HIERARCHY(PROJECT_ID,TEMPLATE_NAME,TEMPLATE_PATH,TYPE,ASSIGN_TO,CUSTOMER_ID,CATALOG_ID,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) VALUES(0,'" + fileName + "','" + FilePath + "','" + Type + "', '" + FamilyId + "' , '" + customer_id + "',  '" + Catalog_Id + "','" + customer_Name + "',getdate(),'" + customer_Name + "',getdate())";
            //        break;

            //    case "Product":

            //        ProductId = ID;
            //        query = "INSERT INTO TB_PDFXPRESS_HIERARCHY(PROJECT_ID,TEMPLATE_NAME,TEMPLATE_PATH,TYPE,ASSIGN_TO,CUSTOMER_ID,CATALOG_ID,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) VALUES(0,'" + fileName + "','" + FilePath + "','" + Type + "', '" + ProductId + "' , '" + customer_id + "',  '" + Catalog_Id + "','" + customer_Name + "',getdate(),'" + customer_Name + "',getdate())";
            //        break;
            //}


            //SqlCommand cmd = new SqlCommand(query, connection);
            //connection.Open();
            //cmd.ExecuteNonQuery();
            //connection.Close();


        }


        /// <summary>
        /// Inthis Method is only for to save the values into corresponding tables
        /// </summary>
        /// Author : Mariya Vijayan
        /// <param name="PdfXpressVaues"></param>
        public void SaveXpressPdfFamilyExport(DataTable PdfXpressVaues, string userId)
        {
            try
            {
                string fileName = string.Empty;
                string fileExist = string.Empty;


                string CatalogName = PdfXpressVaues.AsEnumerable().Select(x => x.Field<string>("CATALOG_NAME")).FirstOrDefault();
                int catalog_Id = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_NAME == CatalogName.ToString()).Select(x => x.CATALOG_ID).FirstOrDefault();
                string Category_Id = PdfXpressVaues.AsEnumerable().Select(x => x.Field<string>("CATEGORY_ID")).FirstOrDefault();
                double family_Id = PdfXpressVaues.AsEnumerable().Select(x => x.Field<double>("FAMILY_ID")).FirstOrDefault();


                string CATID = catalog_Id.ToString();
                string FAMID = family_Id.ToString();



                foreach (DataRow dr in PdfXpressVaues.Rows)
                {

                    if (!string.IsNullOrWhiteSpace(dr["Catalog_PdfTemplate"].ToString()))
                    {
                        fileName = dr["Catalog_PdfTemplate"].ToString().Replace("/PDF_Template/", "");

                        if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template")))
                        {
                            Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template"));
                        }

                        //  string newFilePath = "~/Content/ProductImages/" + userId + "/PDF_Template/" + CATID + "_" + fileName;
                        string newFilePath = "~/Content/ProductImages/" + userId + "/PDF_Template/" + fileName;

                        // Check the file already exist and delete it


                        string folderPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "/PDF_Template");
                        fileExist = CATID + "*";
                        DirectoryInfo dir = new DirectoryInfo(folderPath);
                        FileInfo[] files = dir.GetFiles(fileExist, SearchOption.TopDirectoryOnly);
                        foreach (var item in files)
                        {
                            System.IO.File.Delete(folderPath + "\\" + item.Name);
                        }

                        //  fileName = dr["Category_PdfTemplate"].ToString().Replace("/PDF_Template/", "");

                        CheckPdfData("Catalog", CATID, fileName, CATID);
                        // End


                    }

                    if (!string.IsNullOrWhiteSpace(dr["Category_PdfTemplate"].ToString()))
                    {
                        fileName = dr["Category_PdfTemplate"].ToString().Replace("/PDF_Template/", "");

                        CheckPdfData("Category", Category_Id, fileName, CATID);
                    }

                    if (!string.IsNullOrWhiteSpace(dr["Family_PdfTemplate"].ToString()))
                    {
                        fileName = dr["Family_PdfTemplate"].ToString().Replace("/PDF_Template/", "");
                        CheckPdfData("Family", FAMID, fileName, CATID);
                    }


                    if (!string.IsNullOrWhiteSpace(dr["Product_PdfTemplate"].ToString()))
                    {
                        fileName = dr["Product_PdfTemplate"].ToString().Replace("/PDF_Template/", "");
                        CheckPdfData("Product", FAMID, fileName, CATID);
                    }

                }
            }
            catch (Exception ex)
            {


            }
        }




        //--------------------------------------------------FAMILY IMPORT PROCESS--------------------------------------------------------------------------------

        //[System.Web.Http.HttpPost]
        //public string BtnFamilyfinishImport(string sheetName, string allowDuplicate, string excelPath, string Deleteproducts, JArray model)
        //{

        //    string importresult = "";
        //    string importTemp;
        //    importTemp = Guid.NewGuid().ToString();
        //    int userProductCount = 0;
        //    AdvanceImportApiController adImport = new AdvanceImportApiController();
        //    int userSkuProductCount = 0;
        //    //Deleteproducts = "true";
        //    DataTable PdfXpressFamilyExportTable = new DataTable();
        //    var userId = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();

        //    try
        //    {
        //        importresult = ImportandUpdate(sheetName, allowDuplicate, excelPath, model, importTemp, adImport, out PdfXpressFamilyExportTable, userId);
        //        DataTable deleteRecords = GetDataFromExcelForDelete(sheetName, excelPath, " SELECT * from [" + sheetName + "$]", "DELETE");
        //        if (deleteRecords != null && deleteRecords.Rows.Count > 0)
        //        {
        //            importresult = FamilyProductDelete(sheetName, excelPath, importTemp, deleteRecords);
        //        }
        //        DataTable detachRecords = GetDataFromExcelForDetach(sheetName, excelPath, " SELECT * from [" + sheetName + "$]", "DETACH");
        //        if (detachRecords !=  null && detachRecords.Rows.Count > 0)
        //        {
        //            string importType = "Products";
        //            int catalogId = 0;
        //            string detachCount = DetachRecords(sheetName, allowDuplicate, excelPath, importType, catalogId, model, importTemp);
        //        }

        //        return importresult;
        //    }

        //    catch (Exception ex)
        //    {
        //        Logger.Error("Error at ImportApiController : FinishImport", ex);
        //        return "Import Failed~" + importTemp;
        //    }

        //}

        //Siranjeevi Family Import//
        [System.Web.Http.HttpPost]
        public string BtnFamilyfinishImport(string sheetName, string allowDuplicate, string excelPath, string Deleteproducts, JArray model)
        {
            if (sheetName == "Products")
            {
                sheetName = "Families";
            }

            if (sheetName == "Items")
            {
                sheetName = "Products";
            }

            int catalogId = 0;
            string importresult = "";
            string importTemp;
            importTemp = Guid.NewGuid().ToString();
            int userProductCount = 0;
            AdvanceImportApiController adImport = new AdvanceImportApiController();
            int userSkuProductCount = 0;
            //Deleteproducts = "true";
            DataTable PdfXpressFamilyExportTable = new DataTable();
            var userId = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();

            try
            {
                if (sheetName == "Products")
                {
                    importresult = FinishProductImport(sheetName, allowDuplicate, excelPath, catalogId, "Products", 0, model);
                    DataTable deleteRecords = GetDataFromExcelForDelete(sheetName, excelPath, " SELECT * from [" + sheetName + "$]", "DELETE");
                    if (deleteRecords != null && deleteRecords.Rows.Count > 0)
                    {
                        importresult = FamilyProductDelete(sheetName, excelPath, importTemp, deleteRecords);
                    }
                    DataTable detachRecords = GetDataFromExcelForDetach(sheetName, excelPath, " SELECT * from [" + sheetName + "$]", "DETACH");
                    if (detachRecords != null && detachRecords.Rows.Count > 0)
                    {
                        string importType = "Products";
                        string detachCount = DetachRecords(sheetName, allowDuplicate, excelPath, importType, catalogId, model, importTemp);
                    }

                }

                else
                {
                    JArray a = new JArray();

                    importresult = FinishProductImport("Families", "0", excelPath, catalogId, "Families", 0, model);
                }

                return importresult;
            }

            catch (Exception ex)
            {
                Logger.Error("Error at ImportApiController : FinishImport", ex);
                return "Import Failed~" + importTemp;
            }

        }

        public string FamilyProductDelete(string sheetName, string excelPath, string importTemp, DataTable deleteRecords)
        {
            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    string subitemdelete = string.Empty;
                    string sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" + importTemp + "]'') BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    // _ic.importExcelSheetSelection(excelPath, sheetName);
                    var dbCommand = new SqlCommand(sqlString, conn);
                    dbCommand.ExecuteNonQuery();
                    // _objdatatable = _ic.exceldata(excelPath, sheetName);
                    _objdatatable = deleteRecords;
                    if (!_objdatatable.Columns.Contains("REMOVE_PRODUCT"))
                    {
                        _objdatatable.Columns.Add("REMOVE_PRODUCT");

                    }
                    if (!_objdatatable.Columns.Contains("CATALOG_ID"))
                    {
                        return "CATALOG_ID~" + importTemp;

                    }

                    if (_objdatatable.Columns.Contains("SUBITEM#"))
                    {
                        subitemdelete = "subproductdelete";
                        if (!_objdatatable.Columns.Contains("SUBPRODUCT_ID"))
                        {
                            _objdatatable.Columns.Add("SUBPRODUCT_ID");

                        }

                    }

                    string sqlstring1 = _ic.CreateTable("[##IMPORT" + importTemp + "]", excelPath, sheetName, _objdatatable);
                    dbCommand = new SqlCommand(sqlstring1, conn);
                    dbCommand.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(conn)
                    {
                        DestinationTableName = "[##IMPORT" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(_objdatatable);
                    if (subitemdelete != "subproductdelete")
                    {

                        string sqlstring11 = "select distinct CATALOG_ITEM_NO, REMOVE_PRODUCT,CATALOG_ID into [##IMPORTTEMP" + importTemp + "] from [##IMPORT" + importTemp + "] ";
                        dbCommand = new SqlCommand(sqlstring11, conn);
                        dbCommand.ExecuteNonQuery();
                        string sqlString4 = "EXEC('update [##IMPORTTEMP" + importTemp + "] set REMOVE_PRODUCT = ''YES'' ')";
                        var dbCommand4 = new SqlCommand(sqlString4, conn);
                        dbCommand4.ExecuteNonQuery();

                        sqlString4 = "EXEC('STP_LS_PROD_BASED_REMOVAL ''" + importTemp + "'',''" + User.Identity.Name + "'' ')";
                        dbCommand4 = new SqlCommand(sqlString4, conn);
                        dbCommand4.CommandTimeout = 0;
                        dbCommand4.ExecuteNonQuery();


                        string sqlString5 = "SELECT * into [deleteresult" + importTemp + "] FROM [##LOGTEMPTABLE" + importTemp + "]";
                        var dbCommand5 = new SqlCommand(sqlString5, conn);
                        dbCommand5.ExecuteNonQuery();
                    }
                    else
                    {
                        string sqlstring11 = "select distinct SUBITEM# , REMOVE_PRODUCT,CATALOG_ID,FAMILY_ID,SUBPRODUCT_ID,FAMILY_NAME ,CATEGORY_ID into [##IMPORTTEMP" + importTemp + "] from [##IMPORT" + importTemp + "] ";
                        dbCommand = new SqlCommand(sqlstring11, conn);
                        dbCommand.ExecuteNonQuery();
                        sqlstring11 = "update ad set SUBPRODUCT_ID = tps.PRODUCT_ID from TB_PROD_SPECS  tps join [##IMPORTTEMP" + importTemp + "] ad on ad.SUBITEM# = tps.STRING_VALUE and tps.attribute_id=1 where ad.SUBPRODUCT_ID is null ";
                        dbCommand = new SqlCommand(sqlstring11, conn);
                        dbCommand.ExecuteNonQuery();

                        sqlstring11 = "update TT set family_id=tcf.FAMILY_ID  from TB_CATALOG_FAMILY tcf join TB_FAMILY tf on tf.FAMILY_ID = tcf.FAMILY_ID	JOIN [##IMPORTTEMP" + importTemp + "] TT on TT.FAMILY_NAME = tf.FAMILY_NAME and tcf.CATALOG_ID = TT.catalog_id where TT.FAMILY_ID is null";
                        dbCommand = new SqlCommand(sqlstring11, conn);
                        dbCommand.ExecuteNonQuery();



                        DataSet subitems = new DataSet();
                        var cmd1f = new SqlCommand("SELECT * FROM [##IMPORTTEMP" + importTemp + "]", conn);
                        var daf = new SqlDataAdapter(cmd1f);
                        daf.Fill(subitems);
                        foreach (DataRow item in subitems.Tables[0].Rows)
                        {
                            string family_id = item["FAMILY_ID"].ToString();
                            int catalog_id = Convert.ToInt32(item["CATALOG_ID"].ToString());
                            int subproduct_id = Convert.ToInt32(item["SUBPRODUCT_ID"].ToString());
                            string category_idsub = item["CATEGORY_ID"].ToString();
                            string catid = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == category_idsub).Select(c => c.CATEGORY_ID).FirstOrDefault().ToString();
                            DeleteSubProductPermanentFromMaster(family_id, catalog_id, catid, subproduct_id);

                        }

                        string sqlString5 = "SELECT SUBITEM# as CATALOG_ITEM_NO,'DELETED' as STATUS into [deleteresult" + importTemp + "] FROM [##IMPORTTEMP" + importTemp + "]";
                        var dbCommand5 = new SqlCommand(sqlString5, conn);
                        dbCommand5.ExecuteNonQuery();


                    }
                    var cmbpk41 = new SqlCommand("  exec ('UPDATE [tempresult" + importTemp + "] SET  STATUS =''DELETED'' WHERE CATALOG_ITEM_NO IN (SELECT CATALOG_ITEM_NO   FROM [##LOGTEMPTABLE" + importTemp + "] ) ')", conn);
                    cmbpk41.ExecuteNonQuery();
                    return "import Success~" + importTemp;
                }
            }
            catch (Exception ex)
            {
                return "Import Failed~" + importTemp;
            }
        }
        //Siranjeevi Family Import//
        public string FinishProductImport(string SheetName, string allowDuplicate, string excelPath, int catalogId, string importType, int templateId, JArray model)
        {
            ProgressBar progressBar = new ProgressBar();
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            List<object> newList = new List<object>();
            DataSet dsGetProdCnt = new DataSet();
            int importCount = 0;
            importController.ImportStatus("Called");
            int importProductCount = 0;
            int userProductCount = 0;
            int userSKUProductCount = 0;
            QueryValues queryValues = new QueryValues();
            // CustomerItemNo
            if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
            {
                var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                int customerIds;
                int.TryParse(customerDetails.ToString(), out customerIds);
                string customerCatalog = queryValues.GetCatalogItemNo(customerIds);
                System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerCatalog;
            }
            // CustomerSubItemNo
            if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
            {
                var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                int customerId;
                int.TryParse(customerDetails.ToString(), out customerId);
                string customerCatalog = queryValues.GetSubCatalogItemNo(customerId);
                System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerCatalog;
            }
            try
            {
                if (SheetName == "Products")
                {
                    SheetName = "Items";
                }

                if (SheetName == "Families")
                {
                    SheetName = "Products";
                }
                var customerId = 0;
                using (objLS = new CSEntities())
                {
                    var skucnt = objLS.TB_PLAN
                       .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                       .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                       .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                    var SKUcount = skucnt.Select(a => a).ToList();
                    //Item# import Process Start
                    string customAttributeName = HttpContext.Current.Session["CustomerItemNo"].ToString();
                    DataTable attrName = importController.SelectedColumnsToImport(new DataTable(), model);
                    string[] fieldNames = attrName == null ? null : attrName.Rows.Count == 0 ? null : attrName.AsEnumerable().Select(x => x.Field<string>("ATTRIBUTE_NAME")).ToArray();
                    // DataTable itemImport = ValidateItemNumber(GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", ""));

                    // To delete the coloums for pdf xpress 

                    //Item# import process stop
                    ////Familyname import starts
                    DataTable attrNameFamily = importController.SelectedFamilyColumnsToImport(new DataTable(), model);
                    string[] fieldNamesFamily = attrNameFamily == null ? null : attrNameFamily.Rows.Count == 0 ? null : attrNameFamily.AsEnumerable().Select(x => x.Field<string>("ATTRIBUTE_NAME")).ToArray();
                    DataTable familyNameImport = advanceaimportapi.ValidateFamilyData(advanceaimportapi.GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", ""));

                    // To delete the coloums for pdf xpress 
                    if (familyNameImport != null)
                    {
                        if (familyNameImport.Columns.Contains("Catalog_PdfTemplate"))
                        {
                            familyNameImport.Columns.Remove("Catalog_PdfTemplate");
                            familyNameImport.Columns.Remove("Category_PdfTemplate");
                            familyNameImport.Columns.Remove("Family_PdfTemplate");
                            familyNameImport.Columns.Remove("Product_PdfTemplate");

                        }
                    }






                    if (familyNameImport != null && familyNameImport.Rows.Count > 0)
                    {
                        foreach (string fieldName in fieldNamesFamily)
                        {
                            foreach (DataColumn colName in familyNameImport.Columns)
                            {
                                if (colName.ColumnName.ToString().ToUpper() == fieldName.ToUpper().Replace(".", "#"))
                                {
                                    colName.ColumnName = fieldName;
                                }
                            }
                        }

                    }
                    ////Familyname import stop
                    if (importType != "Families" && importType != "Categories")
                    {
                        if (allowDuplicate == "1")
                        {
                            DataTable dtGetProdCnt = advanceaimportapi.GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "");
                            if (importType.ToUpper().Contains("PRODUCT") && dtGetProdCnt.Columns.Contains("ITEM#"))
                            {
                                dtGetProdCnt.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                            }
                            if (importType.ToUpper().Contains("PRODUCT"))
                            {
                                foreach (DataColumn dcReplaceColumn in dtGetProdCnt.Columns)
                                {
                                    if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                                    {
                                        dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                        break;
                                    }
                                }
                            }
                            dsGetProdCnt.Tables.Add(dtGetProdCnt.Select(" isnull(Catalog_item_no,'') <> ''").CopyToDataTable());
                        }
                        else
                        {
                            DataTable dtGetProdCnt = advanceaimportapi.GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "");
                            if (importType.ToUpper().Contains("PRODUCT"))
                            {
                                foreach (DataColumn dcReplaceColumn in dtGetProdCnt.Columns)
                                {
                                    if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                                    {
                                        dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                        break;
                                    }
                                }
                            }
                            if (!dtGetProdCnt.Columns.Contains("CATALOG_ID"))
                            {
                                dtGetProdCnt.Columns.Add("CATALOG_ID");
                                dtGetProdCnt.Columns["CATALOG_ID"].SetOrdinal(0);
                            }
                            if (!dtGetProdCnt.Columns.Contains("CATEGORY_ID"))
                            {
                                dtGetProdCnt.Columns.Add("CATEGORY_ID");
                                dtGetProdCnt.Columns["CATEGORY_ID"].SetOrdinal(2);
                            }
                            if (!dtGetProdCnt.Columns.Contains("FAMILY_ID"))
                            {
                                dtGetProdCnt.Columns.Add("FAMILY_ID");
                                dtGetProdCnt.Columns["FAMILY_ID"].SetOrdinal(4);

                            }
                            if (!dtGetProdCnt.Columns.Contains("SUBFAMILY_ID"))
                            {
                                dtGetProdCnt.Columns.Add("SUBFAMILY_ID");
                                dtGetProdCnt.Columns["SUBFAMILY_ID"].SetOrdinal(6);

                            }
                            if (!dtGetProdCnt.Columns.Contains("PRODUCT_ID"))
                            {
                                dtGetProdCnt.Columns.Add("PRODUCT_ID");
                                dtGetProdCnt.Columns["PRODUCT_ID"].SetOrdinal(8);
                            }

                            if (importType.ToUpper().Contains("PRODUCT"))
                            {
                                foreach (DataColumn dcRename in dtGetProdCnt.Columns)
                                {
                                    if (dcRename.ColumnName.ToUpper() == "ITEM#")
                                    {
                                        dtGetProdCnt.Columns[dcRename.ColumnName].ColumnName = "CATALOG_ITEM_NO";
                                        break;
                                    }
                                }
                            }
                            // DataRow[] drow = dtGetProdCnt.Select("PRODUCT_ID IS NULL  and isnull(Catalog_item_no,'') <> ''");
                            DataRow[] drow = null;


                        }
                    }
                    else
                    {
                        if (SKUcount.Any())
                        {
                            customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                        }
                    }
                    if (customerId == 0)
                    {
                        if (SKUcount.Any())
                        {
                            customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                        }
                    }
                    advanceaimportapi.importExcelSheetSelection(excelPath, SheetName);
                    DataTable excelData = advanceaimportapi.GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "");
                    //string catname = excelData.Rows[0][1].ToString();
                    //var catid = _dbcontext.TB_CATALOG.Where(r => r.CATALOG_NAME == catname).Select(a => a.CATALOG_ID).FirstOrDefault().ToString();
                    //catalogId = Int16.Parse(catid);
                    // To delete the coloums for pdf xpress 
                    if (excelData != null)
                    {
                        if (excelData.Columns.Contains("Catalog_PdfTemplate"))
                        {
                            excelData.Columns.Remove("Catalog_PdfTemplate");
                            excelData.Columns.Remove("Category_PdfTemplate");
                            excelData.Columns.Remove("Family_PdfTemplate");
                            excelData.Columns.Remove("Product_PdfTemplate");

                        }
                    }
                    if (importType == "Families")
                    {

                        if (excelData.Columns.Contains("CATALOG_ID"))
                        {
                            catalogId = Convert.ToInt32(excelData.Rows[0][1]);
                            foreach (DataRow dr in excelData.Rows)
                            {
                                dr["CATALOG_ID"] = catalogId;
                            }
                        }
                        else
                        {
                            if (!excelData.Columns.Contains("CATALOG_ID"))
                            {
                                excelData.Columns.Add("CATALOG_ID");
                                excelData.Columns["CATALOG_ID"].SetOrdinal(1);
                            }

                            foreach (DataRow dr in excelData.Rows)
                            {

                                string catname = dr["CATALOG_NAME"].ToString();
                                var catalog_id = _dbcontext.TB_CATALOG.Where(k => k.CATALOG_NAME.Equals(catname) && k.FLAG_RECYCLE == "A").Select(m => m.CATALOG_ID).FirstOrDefault().ToString();
                                catalogId = Int16.Parse(catalog_id);
                                dr["CATALOG_ID"] = catalogId;
                            }
                        }
                    }
                    else
                    {

                        if (excelData.Columns.Contains("CATALOG_ID"))
                        {
                            catalogId = Convert.ToInt32(excelData.Rows[0][0]);
                            foreach (DataRow dr in excelData.Rows)
                            {
                                dr["CATALOG_ID"] = catalogId;
                            }
                        }
                        else
                        {
                            if (!excelData.Columns.Contains("CATALOG_ID"))
                            {
                                excelData.Columns.Add("CATALOG_ID");
                                excelData.Columns["CATALOG_ID"].SetOrdinal(1);
                            }
                            foreach (DataRow dr in excelData.Rows)
                            {

                                string catname = dr["CATALOG_NAME"].ToString();
                                var catalog_id = _dbcontext.TB_CATALOG.Where(k => k.CATALOG_NAME.Equals(catname) && k.FLAG_RECYCLE == "A").Select(m => m.CATALOG_ID).FirstOrDefault().ToString();
                                catalogId = Int16.Parse(catalog_id);
                                dr["CATALOG_ID"] = catalogId;
                            }
                        }

                    }


                    if (importType.ToUpper().Contains("PRODUCT"))
                    {
                        foreach (DataColumn dcReplaceColumn in excelData.Columns)
                        {
                            if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                            {
                                dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                break;
                            }

                        }
                    }

                    if (!excelData.Columns.Contains("CATALOG_NAME"))
                    {
                        excelData.Columns.Add("CATALOG_NAME");
                        excelData.Columns["CATALOG_NAME"].SetOrdinal(1);
                    }
                    if (!excelData.Columns.Contains("CATEGORY_ID"))
                    {
                        excelData.Columns.Add("CATEGORY_ID");
                        excelData.Columns["CATEGORY_ID"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_NAME"));

                    }
                    int col = 1;
                    for (int colindex = 0; colindex < excelData.Columns.Count; colindex++)
                    {
                        if (excelData.Columns[colindex].ColumnName.ToUpper().Contains("SUBCATNAME"))
                        {
                            if (!excelData.Columns.Contains("SUBCATID_L" + col))
                            {
                                excelData.Columns.Add("SUBCATID_L" + col);
                                excelData.Columns["SUBCATID_L" + col].SetOrdinal(excelData.Columns.IndexOf("SUBCATNAME_L" + col));
                                colindex = colindex + 1;
                            }
                            if (excelData.Columns.Contains("SUBCATID_L" + col))
                            {
                                col = col + 1;
                            }
                        }
                    }
                    if (importType != "Categories")
                    {
                        if (!excelData.Columns.Contains("FAMILY_ID"))
                        {
                            excelData.Columns.Add("FAMILY_ID");
                            excelData.Columns["FAMILY_ID"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_NAME"));
                        }
                        if (!excelData.Columns.Contains("SUBFAMILY_ID"))
                        {
                            excelData.Columns.Add("SUBFAMILY_ID");
                            excelData.Columns["SUBFAMILY_ID"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_NAME") + 1);
                        }
                        if (!excelData.Columns.Contains("SUBFAMILY_NAME"))
                        {
                            excelData.Columns.Add("SUBFAMILY_NAME");
                            excelData.Columns["SUBFAMILY_NAME"].SetOrdinal(excelData.Columns.IndexOf("SUBFAMILY_ID") + 1);
                        }
                    }
                    if (!excelData.Columns.Contains("PRODUCT_ID"))
                    {
                        if (importType == "Products")
                        {
                            excelData.Columns.Add("PRODUCT_ID");
                            if (excelData.Columns.Contains("CATALOG_ITEM_NO") == true)
                            {
                                excelData.Columns["PRODUCT_ID"].SetOrdinal(excelData.Columns.IndexOf("CATALOG_ITEM_NO"));
                            }
                        }
                    }
                    //if (importType.ToUpper().Contains("FAM"))
                    //{
                    if (importType != "Categories")
                    {
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2WEB"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2WEB");
                            excelData.Columns["FAMILY_PUBLISH2WEB"].SetOrdinal(excelData.Columns.IndexOf("SUBFAMILY_ID") + 1);
                        }
                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2PDF"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2PDF");
                            excelData.Columns["FAMILY_PUBLISH2PDF"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_PUBLISH2WEB"));
                        }

                        if (!excelData.Columns.Contains("FAMILY_PUBLISH2PRINT"))
                        {
                            excelData.Columns.Add("FAMILY_PUBLISH2PRINT");
                            excelData.Columns["FAMILY_PUBLISH2PRINT"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_PUBLISH2PDF"));
                        }

                    }
                    if (importType != "Categories")
                    {
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2WEB"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2WEB");
                            excelData.Columns["CATEGORY_PUBLISH2WEB"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_ID"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PDF"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PDF");
                            excelData.Columns["CATEGORY_PUBLISH2PDF"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2WEB"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2EXPORT"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2EXPORT");
                            excelData.Columns["CATEGORY_PUBLISH2EXPORT"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PDF"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PRINT"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PRINT");
                            excelData.Columns["CATEGORY_PUBLISH2PRINT"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2EXPORT"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PORTAL"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PORTAL");
                            excelData.Columns["CATEGORY_PUBLISH2PORTAL"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PRINT"));
                        }
                    }
                    else
                    {
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2WEB"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2WEB");
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PDF"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PDF");
                            excelData.Columns["CATEGORY_PUBLISH2PDF"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2WEB"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2EXPORT"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2EXPORT");
                            excelData.Columns["CATEGORY_PUBLISH2EXPORT"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PDF"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PRINT"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PRINT");
                            excelData.Columns["CATEGORY_PUBLISH2PRINT"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2EXPORT"));
                        }
                        if (!excelData.Columns.Contains("CATEGORY_PUBLISH2PORTAL"))
                        {
                            excelData.Columns.Add("CATEGORY_PUBLISH2PORTAL");
                            excelData.Columns["CATEGORY_PUBLISH2PORTAL"].SetOrdinal(excelData.Columns.IndexOf("CATEGORY_PUBLISH2PRINT"));
                        }
                    }

                    if (importType.ToUpper().Contains("PRODUCT"))
                    {
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2WEB"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2WEB");
                        excelData.Columns["PRODUCT_PUBLISH2WEB"].SetOrdinal(excelData.Columns.IndexOf("FAMILY_ID"));
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2PDF"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2PDF");
                        excelData.Columns["PRODUCT_PUBLISH2PDF"].SetOrdinal(excelData.Columns.IndexOf("PRODUCT_PUBLISH2WEB"));
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2EXPORT"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2EXPORT");
                        excelData.Columns["PRODUCT_PUBLISH2EXPORT"].SetOrdinal(excelData.Columns.IndexOf("PRODUCT_PUBLISH2PDF"));
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2PRINT"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2PRINT");
                        excelData.Columns["PRODUCT_PUBLISH2PRINT"].SetOrdinal(excelData.Columns.IndexOf("PRODUCT_PUBLISH2EXPORT"));
                        if (!excelData.Columns.Contains("PRODUCT_PUBLISH2PORTAL"))
                            excelData.Columns.Add("PRODUCT_PUBLISH2PORTAL");
                        excelData.Columns["PRODUCT_PUBLISH2PORTAL"].SetOrdinal(excelData.Columns.IndexOf("PRODUCT_PUBLISH2PRINT"));
                    }
                    // }
                    ImportApiController importApiController = new ImportApiController();
                    if (importType.ToUpper().Contains("PRODUCT") && excelData.Columns.Contains("ITEM#"))
                    {
                        excelData.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                    }
                    excelData = importApiController.UpdateCatalogDetails(importType, catalogId, excelData);
                    DataTable oattType = new DataTable();
                    DataSet replace = new DataSet();
                    int ItemVal = Convert.ToInt32(allowDuplicate);
                    using (var conn = new SqlConnection(connectionString))
                    {
                        conn.Open();
                        SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                        _DBCommand.ExecuteNonQuery();


                        //Mapping attribute -Start-------------------------------------------------------------------

                        //var Mappingattributes = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                        //if (Mappingattributes.Count != 0)
                        //{
                        //    for (int i = 0; i < Mappingattributes.Count; i++)
                        //    {
                        //        for (int j = 0; j < excelData.Columns.Count; j++)
                        //        {
                        //            if (Mappingattributes[i].Excel_Column == excelData.Columns[j].ColumnName)
                        //            {
                        //                excelData.Columns[j].ColumnName = Mappingattributes[i].Mapping_Name;
                        //            }
                        //        }
                        //    }
                        //}


                        // var importTempMappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Select(x => x).Where(x => x.TEMPLATE_ID == templateId).ToList();

                        string str_GetImportType = null;

                        if (importType.ToUpper().Contains("CAT"))
                        {
                            str_GetImportType = "Category";
                        }
                        else if (importType.ToUpper().Contains("FAM"))
                        {
                            str_GetImportType = "Family";
                        }
                        else if (importType.ToUpper().Contains("PRO"))
                        {
                            str_GetImportType = "Product";
                        }


                        bool mappingFlag = false;

                        List<string> lt_MissingMappingAttributes = new List<string>();

                        DataTable dt_MappingAttributesErrorLog = new DataTable();

                        dt_MappingAttributesErrorLog.Columns.Add("SheetName");
                        dt_MappingAttributesErrorLog.Columns.Add("ImportType");
                        dt_MappingAttributesErrorLog.Columns.Add("Attribute(s)");

                        var importTempMappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                      Where(x => x.cs.TEMPLATE_ID == templateId && x.cu.IMPORT_TYPE == str_GetImportType).ToList();

                        if (importTempMappingattributesValue.Count != 0)
                        {
                            for (int j = 0; j < excelData.Columns.Count; j++)
                            {
                                string excelColumnname = Convert.ToString(excelData.Columns[j].ColumnName);
                                //string attributeType = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_TYPE"]);



                                //var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"])  && x.ATTRIBUTE_TYPE!=0).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                                int attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.CAPTION == excelColumnname && x.ATTRIBUTE_ID >= 0 && x.TEMPLATE_ID == templateId).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());
                                if (attributeId != 0)
                                {
                                    var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();

                                    if (attributeName != null)
                                    {
                                        excelData.Columns[j].ColumnName = attributeName;
                                    }

                                    else
                                    {
                                        // excelData.Columns[j].ColumnName = excelColumnname;
                                        _logger.Info("Missing Attributes/Caption in Mapping table: " + excelColumnname);
                                        // return "Import Failed" + "~MappingAttribute:" + "~" + excelColumnname;
                                        lt_MissingMappingAttributes.Add(excelColumnname);
                                        DataRow row = dt_MappingAttributesErrorLog.NewRow();
                                        row["SheetName"] = SheetName;
                                        row["ImportType"] = importType;
                                        row["Attribute(s)"] = excelColumnname;
                                        dt_MappingAttributesErrorLog.Rows.Add(row);
                                        mappingFlag = true;
                                    }
                                }


                            }

                        }

                        //Mapping Attribute - End ---------------------------------------------------------------------

                        SQLString = importController.CreateTable("[##IMPORTTEMP" + importTemp + "]", excelPath, SheetName, excelData);
                        SqlCommand dbCommandnew = new SqlCommand(SQLString, conn);
                        dbCommandnew.ExecuteNonQuery();
                        //bulkCopy.WriteToServer(excelData);

                        // importData = UnPivotTable(excelData, importType + "_bulk");
                        // dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTTEMPDATA", conn) { CommandTimeout = 0 };

                        DataTable importData = new DataTable();
                        SqlCommand dbCommandTemp = new SqlCommand();
                        if (importType.ToUpper() == "FAMILIES")
                        {
                            //importData = UnPivotTableFamily(excelData, importType);
                            dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTFAMILYTEMPDATA", conn) { CommandTimeout = 0 };
                        }
                        else if (importType.ToUpper() == "CATEGORIES")
                        {
                            //importData = UnPivotTableCategory(excelData, importType);
                            dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTCATEGORYTEMPDATA", conn) { CommandTimeout = 0 };
                        }
                        else
                        {
                            //importData = UnPivotTable(excelData, importType + "_bulk");
                            dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTTEMPDATA", conn) { CommandTimeout = 0 };
                        }

                        dbCommandTemp.CommandType = CommandType.StoredProcedure;
                        // dbCommandTemp.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = excelData;
                        dbCommandTemp.Parameters.Add("@TABLENAME", SqlDbType.NChar).Value = "[##IMPORTTEMP" + importTemp + "]";
                        dbCommandTemp.ExecuteNonQuery();

                        DataColumn newCol = new DataColumn("rowImportid", typeof(string));
                        excelData.Columns.Add(newCol);
                        int i = 0;
                        foreach (DataRow row in excelData.Rows)
                        {
                            i++;
                            row["rowImportid"] = i.ToString();
                        }

                        importStatus = "20";
                        using (SqlBulkCopy bulkCopyss = new SqlBulkCopy(conn))
                        {
                            bulkCopyss.DestinationTableName = "[##IMPORTTEMP" + importTemp + "]";


                            foreach (var excelColumn in excelData.Columns)
                            {

                                bulkCopyss.ColumnMappings.Add(excelColumn.ToString(), excelColumn.ToString());

                            }

                            bulkCopyss.WriteToServer(excelData);
                        }
                        using (var connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            SQLString = string.Format("CREATE CLUSTERED INDEX IX_TestTableone ON [##IMPORTTEMP{0}] (rowImportid ASC)", importTemp);
                            SqlCommand sqlCommandIndexing = new SqlCommand(SQLString, connection);
                            sqlCommandIndexing.ExecuteNonQuery();
                        }
                        if (importType.ToUpper() == "CATEGORIES")
                        {
                            SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCATEGORYCOLUMNS ''''" + importTemp + "'''''');END')";

                        }
                        else
                        {
                            SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";

                        }
                        _DBCommand = new SqlCommand(SQLString, conn);
                        _DBCommand.ExecuteNonQuery();

                        oattType = importController.SelectedColumnsToImport(excelData, model);


                        //Mapping attribute -Start-------------------------------------------------------------------

                        //var MappingattributesValue = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                        //if (MappingattributesValue.Count != 0)
                        //{
                        //    for (int i = 0; i < MappingattributesValue.Count; i++)
                        //    {
                        //        for (int j = 0; j < oattType.Rows.Count; j++)
                        //        {
                        //            if (MappingattributesValue[i].Excel_Column.ToString() == oattType.Rows[j]["ATTRIBUTE_NAME"].ToString())
                        //            {
                        //                oattType.Rows[j][1] = MappingattributesValue[i].Mapping_Name;
                        //            }
                        //        }
                        //    }
                        //}


                        //  // var MappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Select(x => x).Where(x=>x.TEMPLATE_ID== templateId).ToList();

                        //  var MappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                        //Where(x => x.cs.TEMPLATE_ID == templateId && x.cu.IMPORT_TYPE == str_GetImportType).ToList();


                        //  if (MappingattributesValue.Count != 0)
                        //  {
                        //      for (int j = 0; j < oattType.Rows.Count; j++)
                        //      {
                        //          string excelColumnname = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"]);
                        //          string attributeType = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_TYPE"]);

                        //          if (attributeType != "0")
                        //          {

                        //              //var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"])  && x.ATTRIBUTE_TYPE!=0).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                        //              int attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.CAPTION == excelColumnname && x.ATTRIBUTE_ID >= 0 && x.TEMPLATE_ID == templateId).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());

                        //              if (attributeId > 0)
                        //              {
                        //                  var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                        //                  oattType.Rows[j][1] = attributeName;
                        //              }
                        //              else
                        //              {
                        //                  //var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.CAPTION == excelColumnname).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                        //                  //if (attributeName != "")
                        //                  //{ oattType.Rows[j][1] = attributeName; }
                        //                  //else
                        //                  //{
                        //                  _logger.Info("Missing Attributes/Caption in Mapping table: " + excelColumnname);
                        //                  return "Import Failed";
                        //                  //}
                        //              }
                        //          }
                        //      }

                        //  }



                        var MappingattributesValue = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                                            Where(x => x.cs.TEMPLATE_ID == templateId && x.cu.IMPORT_TYPE == str_GetImportType).ToList();


                        if (MappingattributesValue.Count != 0)
                        {
                            for (int j = 0; j < oattType.Rows.Count; j++)
                            {
                                string excelColumnname = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"]);
                                string attributeType = Convert.ToString(oattType.Rows[j]["ATTRIBUTE_TYPE"]);

                                if (attributeType != "0")
                                {

                                    //var attributeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.CAPTION == Convert.ToString(oattType.Rows[j]["ATTRIBUTE_NAME"])  && x.ATTRIBUTE_TYPE!=0).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();

                                    var checkcolumns = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                         Where(x => x.cs.CAPTION == excelColumnname && x.cs.TEMPLATE_ID == templateId && x.cu.SHEET_NAME == SheetName).ToList();


                                    if (checkcolumns.Count > 0)
                                    {
                                        int attributeId = Convert.ToInt32(objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Where(x => x.CAPTION == excelColumnname && x.ATTRIBUTE_ID >= 0 && x.TEMPLATE_ID == templateId).Select(x => x.ATTRIBUTE_ID).FirstOrDefault());

                                        if (attributeId > 0)
                                        {
                                            var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeId && x.FLAG_RECYCLE == "A").Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                                            oattType.Rows[j][1] = attributeName;
                                        }
                                        else if (attributeId == 0)
                                        {
                                            oattType.Rows[j][1] = excelColumnname;
                                        }

                                    }

                                    else
                                    {
                                        //var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.CAPTION == excelColumnname).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                                        //if (attributeName != "")
                                        //{ oattType.Rows[j][1] = attributeName; }
                                        //else
                                        //{
                                        _logger.Info("Missing Attributes/Caption in Mapping table: " + excelColumnname);
                                        // return "Import Failed" + "~MappingAttribute:" + "~" + excelColumnname;
                                        lt_MissingMappingAttributes.Add(excelColumnname);
                                        DataRow row = dt_MappingAttributesErrorLog.NewRow();
                                        row["SheetName"] = SheetName;
                                        row["ImportType"] = importType;
                                        row["Attribute(s)"] = excelColumnname;
                                        dt_MappingAttributesErrorLog.Rows.Add(row);
                                        mappingFlag = true;
                                        //}
                                    }
                                }
                            }

                        }


                        //Mapping Attribute - End ---------------------------------------------------------------------


                        if (mappingFlag == true)
                        {
                            HttpContext.Current.Session["MissingMappingAttribute"] = lt_MissingMappingAttributes;
                            HttpContext.Current.Session["MappingAttributeErrorLog"] = dt_MappingAttributesErrorLog;
                            return "Import Failed" + "~MappingAttribute";

                        }

                        var cmd1 =
                            new SqlCommand(
                                "ALTER TABLE [##IMPORTTEMP" + importTemp +
                                "] ADD Family_Status nvarchar(10),FStatusUI nvarchar(10),StatusUI nvarchar(10),SFSort_Order int,Sort_Order int",
                                conn);
                        cmd1.ExecuteNonQuery();
                        var sqlstring1 =
                            new SqlCommand(
                                "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##AttributeTemp" +
                                importTemp + "]'')BEGIN DROP TABLE [##AttributeTemp" + importTemp + "] END')", conn);
                        sqlstring1.ExecuteNonQuery();
                        oattType = _ic.SelectedColumnsToImport(excelData, model);
                        var cmd2 = new SqlCommand();
                        cmd2.Connection = conn;
                        SQLString = importController.CreateTableToImport("[##AttributeTemp" + importTemp + "]", oattType);
                        cmd2.CommandText = SQLString;
                        cmd2.CommandType = CommandType.Text;
                        cmd2.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##AttributeTemp" + importTemp + "]"
                        };
                        bulkCopy.WriteToServer(oattType);
                        var cmbpk10 = new SqlCommand("update [##AttributeTemp" + importTemp + "] set ATTRIBUTE_TYPE = 1 where ATTRIBUTE_TYPE = 10", conn);
                        cmbpk10.ExecuteNonQuery();

                        var cmbpk1 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1'') IS NOT NULL  DROP TABLE  ##t1') ", conn);
                        cmbpk1.ExecuteNonQuery();
                        var cmbpk2 = new SqlCommand("select * into ##t1  from [##AttributeTemp" + importTemp + "] where attribute_type <>0", conn);
                        cmbpk2.ExecuteNonQuery();
                        var cmbpk3 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2'') IS NOT NULL  DROP TABLE  ##t2') ", conn);
                        cmbpk3.ExecuteNonQuery();
                        var cmbpk4 = new SqlCommand("select * into ##t2  from [##importtemp" + importTemp + "]", conn);
                        cmbpk4.ExecuteNonQuery();
                        //---------------------------------------FOR REPLACING EMPTY VALUES IN TEMP TABLE ---------------------FEB-2017 ------------//
                        var cmd1f12 = new SqlCommand("SELECT * FROM [##importtemp" + importTemp + "]", conn);
                        var daf12 = new SqlDataAdapter(cmd1f12);
                        daf12.Fill(replace);
                        progressBar.progressVale = "30";
                        if (replace.Tables[0].Rows.Count > 0)
                        {
                            foreach (var item1 in replace.Tables[0].Columns)
                            {
                                string column_name2 = item1.ToString();
                                if (column_name2.Contains("'"))
                                {
                                    column_name2 = column_name2.Replace("'", "''''");
                                }
                                var cmbpk41 = new SqlCommand("  exec ('UPDATE [##IMPORTTEMP" + importTemp + "] SET  [" + column_name2 + "] = NULL  WHERE cast( [" + column_name2 + "] as nvarchar) = '''' and  [" + column_name2 + "] is not null ')", conn);
                                cmbpk41.ExecuteNonQuery();
                            }
                        }
                        _logger.Error("Error at Import Start : FinishProductImport");
                        progressBar.progressVale = "50";
                        if (importType.ToUpper() == "FAMILIES")
                        {
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT_HIERARCHY ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                            cmd.ExecuteNonQuery();
                        }
                        else if (importType.ToUpper() == "CATEGORIES")
                        {
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_CATEGORYATTRIBUTEIMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            _logger.Error("Error at Get STP : FinishProductImport");
                            var customers = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                            int maximumSheetLength = objLS.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.ImportSheetProductCount).FirstOrDefault();
                            //int maximumSheetLength = Int32.Parse(ConfigurationManager.AppSettings["ImportSheetProductCount"].ToString());
                            int currentSheetLength = replace.Tables[0].Rows.Count;
                            int noOfIteration = (currentSheetLength / maximumSheetLength) + 1;

                            int importStartCount = 0;
                            int importEndCount = maximumSheetLength;

                            SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";
                            SqlCommand sqlCommand = new SqlCommand(SQLString, conn);
                            sqlCommand.ExecuteNonQuery();

                            SQLString = string.Format("EXEC('IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE=''U'' AND NAME=''##IMPORTALLDATA{0}'') BEGIN DROP TABLE [##IMPORTALLDATA{0}] END SELECT ROW_NUMBER() OVER (ORDER BY (select 100)) AS RowID,* INTO [##IMPORTALLDATA{0}] FROM [##IMPORTTEMP{0}]')", importTemp);
                            sqlCommand = new SqlCommand(SQLString, conn);
                            sqlCommand.ExecuteNonQuery();

                            while (noOfIteration >= 1)
                            {
                                SQLString = string.Format("EXEC('IF EXISTS(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE=''U'' AND NAME=''##IMPORTTEMP{0}'') BEGIN DROP TABLE [##IMPORTTEMP{0}] END')", importTemp);
                                sqlCommand = new SqlCommand(SQLString, conn);
                                sqlCommand.ExecuteNonQuery();

                                SQLString = string.Format("SELECT * INTO [##IMPORTTEMP{0}] FROM [##IMPORTALLDATA{0}] WHERE RowID  between {1} and {2}", importTemp, importStartCount, importEndCount);
                                sqlCommand = new SqlCommand(SQLString, conn);
                                sqlCommand.ExecuteNonQuery();

                                //SQLString = string.Format("CREATE CLUSTERED INDEX IX_TestTableone ON [##IMPORTTEMP{0}] (rowImportid ASC)", importTemp);
                                //sqlCommand = new SqlCommand(SQLString, conn);
                                //sqlCommand.ExecuteNonQuery();

                                SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" + importTemp + "'')BEGIN exec(''STP_CATALOGSTUDIO_UPDATEIDCOLUMNS ''''" + importTemp + "'''''');END')";
                                sqlCommand = new SqlCommand(SQLString, conn);
                                sqlCommand.ExecuteNonQuery();
                                _logger.Error("Error at STP start : FinishProductImport");
                                var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                                cmd.ExecuteNonQuery();
                                _logger.Error("Error at End start : FinishProductImport");
                                noOfIteration = noOfIteration - 1;
                                importStartCount = importEndCount + 1;
                                importEndCount = importEndCount + maximumSheetLength;

                            }
                        }
                        DataSet rowaffected = new DataSet();
                        SQLString = string.Empty;
                        if (importType.ToUpper() == "FAMILIES")
                        {
                            SQLString =
                         "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" +
                         importTemp + "'')BEGIN select count(*) as UpdateCount from [##IMPORTTEMP" + importTemp + "]  where Family_Status=''Update'' or Family_Status=''Associate'';select count(*) as InsertCount from [##IMPORTTEMP" + importTemp + "]  where Family_Status=''Insert'' ; END')";
                        }
                        else if (importType.ToUpper() == "CATEGORIES")
                        {
                            SQLString =
                          "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" +
                         importTemp + "'')BEGIN select Count (distinct CATEGORY_ID) as UpdateCount from [##CATEGORYCOUNTDETAILS" + importTemp + "] where FLAG=''U'' ;select Count (distinct CATEGORY_ID) as InsertCount from [##CATEGORYCOUNTDETAILS" + importTemp + "] where FLAG =''I''; END')";
                        }
                        else
                        {
                            _logger.Error("Error at NEW start : FinishProductImport");
                            SQLString =
                         "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" +
                         importTemp + "'')BEGIN select count(*) as UpdateCount from [##IMPORTALLDATA" + importTemp + "]  where StatusUI=''Update'' or StatusUI=''Associate'';select count(*) as InsertCount from [##IMPORTALLDATA" + importTemp + "]  where StatusUI=''Insert''; END')";
                        }
                        _DBCommand = new SqlCommand(SQLString, conn);
                        SqlDataAdapter dbAdapter = new SqlDataAdapter(_DBCommand);
                        dbAdapter.Fill(rowaffected);
                        progressBar.progressVale = "90";
                        _logger.Error("Error at NEWEND start : FinishProductImport");
                        #region Attribute Pack
                        DataSet attributePackFamily = new DataSet();
                        string QueryString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##TEMP_ATTRIBUTE_PACK_FAMILY" +
                        importTemp + "'')BEGIN select * from [##TEMP_ATTRIBUTE_PACK_FAMILY" + importTemp + "]; END')";
                        _DBCommand = new SqlCommand(QueryString, conn);
                        SqlDataAdapter dbAdapter_AP = new SqlDataAdapter(_DBCommand);
                        dbAdapter_AP.Fill(attributePackFamily);
                        _logger.Error("Error at NEWEND1 start : FinishProductImport");
                        DataSet attributePackCategory = new DataSet();
                        string QueryString_Category =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##TEMP_ATTRIBUTE_PACK_CATEGORY" +
                        importTemp + "'')BEGIN select * from [##TEMP_ATTRIBUTE_PACK_CATEGORY" + importTemp + "]; END')";
                        _DBCommand = new SqlCommand(QueryString_Category, conn);
                        SqlDataAdapter dbAdapter_APCategory = new SqlDataAdapter(_DBCommand);
                        dbAdapter_APCategory.Fill(attributePackCategory);
                        _logger.Error("Error at NEWEND2 start : FinishProductImport");
                        DataSet attributePackProduct = new DataSet();
                        string QueryString_Product =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##TEMP_ATTRIBUTE_PACK_PRODUCT" +
                        importTemp + "'')BEGIN select * from [##TEMP_ATTRIBUTE_PACK_PRODUCT" + importTemp + "]; END')";
                        _DBCommand = new SqlCommand(QueryString_Product, conn);
                        SqlDataAdapter dbAdapter_APProduct = new SqlDataAdapter(_DBCommand);
                        dbAdapter_APProduct.Fill(attributePackProduct);
                        _logger.Error("Error at NEWEND3 start : FinishProductImport");
                        // for category
                        if (attributePackCategory != null && attributePackCategory.Tables.Count > 0 && attributePackCategory.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dtRow in attributePackCategory.Tables[0].Rows)
                            {
                                string category_Short = Convert.ToString(dtRow["CATEGORY_ID"]);
                                string category_Id = objLS.TB_CATEGORY.Where(s => s.CATEGORY_SHORT == category_Short).FirstOrDefault().CATEGORY_ID;
                                List<string> categoryIds = new List<string>();
                                TB_CATEGORY tb_category = new TB_CATEGORY();
                                categoryIds.Add(category_Id);
                                var ds_AP = new DataSet();

                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESIGNEXPORT_IMPORT";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETPARENT";
                                    objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.VarChar).Value = category_Id;
                                    objSqlConnection.Open();
                                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                    objSqlDataAdapter.Fill(ds_AP);
                                }

                                if (ds_AP != null && ds_AP.Tables.Count > 0 && ds_AP.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow dtrow in ds_AP.Tables[0].Rows)
                                    {
                                        if (dtrow["CATEGORY_ID"].ToString() != null)
                                        {
                                            categoryIds.Add(dtrow["CATEGORY_ID"].ToString());
                                        }
                                    }
                                }

                                string catalogIdstring = Convert.ToString(catalogId);
                                categoryIds.Add(catalogIdstring);
                                HomeApiController objHomeApiController = new HomeApiController();
                                List<TB_ATTRIBUTE_HIERARCHY> attributeHierarchyList = objLS.TB_ATTRIBUTE_HIERARCHY.Where(s => categoryIds.Contains(s.ASSIGN_TO)).ToList();
                                foreach (var attributeHierarchy in attributeHierarchyList)
                                {
                                    int packId = Convert.ToInt32(attributeHierarchy.PACK_ID);
                                    objHomeApiController.AssociateAttributePackaging(category_Id, packId, catalogId);
                                }

                            }
                        }

                        // For Family
                        if (attributePackFamily != null && attributePackFamily.Tables.Count > 0 && attributePackFamily.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dtRow in attributePackFamily.Tables[0].Rows)
                            {
                                string category_Id = Convert.ToString(dtRow["CATEGORY_ID"]);
                                int family_Id = Convert.ToInt32(dtRow["FAMILY_ID"]);
                                string familyIdString = Convert.ToString(family_Id);
                                string family_ID = "~" + familyIdString;
                                List<string> categoryIds = new List<string>();
                                TB_CATEGORY tb_category = new TB_CATEGORY();
                                categoryIds.Add(category_Id);
                                var ds_AP = new DataSet();

                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESIGNEXPORT_IMPORT";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETPARENT";
                                    objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.VarChar).Value = category_Id;
                                    objSqlConnection.Open();
                                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                    objSqlDataAdapter.Fill(ds_AP);
                                }

                                if (ds_AP != null && ds_AP.Tables.Count > 0 && ds_AP.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow dtrow in ds_AP.Tables[0].Rows)
                                    {
                                        if (dtrow["CATEGORY_ID"].ToString() != null)
                                        {
                                            categoryIds.Add(dtrow["CATEGORY_ID"].ToString());
                                        }
                                    }
                                }

                                string catalogIdstring = Convert.ToString(catalogId);
                                categoryIds.Add(catalogIdstring);
                                HomeApiController objHomeApiController = new HomeApiController();
                                List<TB_ATTRIBUTE_HIERARCHY> attributeHierarchyList = objLS.TB_ATTRIBUTE_HIERARCHY.Where(s => categoryIds.Contains(s.ASSIGN_TO)).ToList();
                                foreach (var attributeHierarchy in attributeHierarchyList)
                                {
                                    int packId = Convert.ToInt32(attributeHierarchy.PACK_ID);
                                    objHomeApiController.AssociateAttributePackaging(family_ID, packId, catalogId);
                                }
                            }
                        }

                        // For Product
                        if (attributePackProduct != null && attributePackProduct.Tables.Count > 0 && attributePackProduct.Tables[0].Rows.Count > 0)
                        {
                            DataView view = new DataView(attributePackProduct.Tables[0]);
                            DataTable distinctValues = view.ToTable(true, "FAMILY_ID");

                            foreach (DataRow dtRow in distinctValues.Rows)
                            {
                                int family_Id = Convert.ToInt32(dtRow["FAMILY_ID"]);
                                string category_Id = objLS.TB_CATALOG_FAMILY.Where(s => s.CATALOG_ID == catalogId && s.FAMILY_ID == family_Id).FirstOrDefault().CATEGORY_ID;
                                string familyIdString = Convert.ToString(family_Id);
                                string family_ID = "~" + familyIdString;
                                List<string> categoryIds = new List<string>();
                                TB_CATEGORY tb_category = new TB_CATEGORY();
                                categoryIds.Add(category_Id);
                                var ds_AP = new DataSet();

                                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESIGNEXPORT_IMPORT";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETPARENT";
                                    objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.Int).Value = 0;
                                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.VarChar).Value = category_Id;
                                    objSqlConnection.Open();
                                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                                    objSqlDataAdapter.Fill(ds_AP);
                                }

                                if (ds_AP != null && ds_AP.Tables.Count > 0 && ds_AP.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow dtrow in ds_AP.Tables[0].Rows)
                                    {
                                        if (dtrow["CATEGORY_ID"].ToString() != null)
                                        {
                                            categoryIds.Add(dtrow["CATEGORY_ID"].ToString());
                                        }
                                    }
                                }

                                string catalogIdstring = Convert.ToString(catalogId);
                                categoryIds.Add(catalogIdstring);
                                categoryIds.Add(familyIdString);
                                HomeApiController objHomeApiController = new HomeApiController();
                                List<TB_ATTRIBUTE_HIERARCHY> attributeHierarchyList = objLS.TB_ATTRIBUTE_HIERARCHY.Where(s => categoryIds.Contains(s.ASSIGN_TO)).ToList();
                                foreach (var attributeHierarchy in attributeHierarchyList)
                                {
                                    int packId = Convert.ToInt32(attributeHierarchy.PACK_ID);
                                    objHomeApiController.AssociateAttributePackaging(family_ID, packId, catalogId);
                                }
                            }
                        }

                        #endregion

                        int insertRecords = 0;
                        int updateRecords = 0;
                        DataTable detachRecords = GetDataFromExcelForDetach(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DETACH");
                        int detachcount = detachRecords.Rows.Count;
                        if (rowaffected != null && rowaffected.Tables.Count > 1)
                        {
                            if (rowaffected.Tables[0].Rows.Count > 0)
                            {
                                updateRecords = int.Parse(rowaffected.Tables[0].Rows[0][0].ToString());
                            }
                            if (rowaffected.Tables[1].Rows.Count > 0)
                            {
                                insertRecords = int.Parse(rowaffected.Tables[1].Rows[0][0].ToString());
                            }
                        }
                        if (!(importCount == 0))
                        {
                            updateRecords = importCount - detachcount;
                        }

                        if (updateRecords == 0 && insertRecords == 0)
                        {
                            updateRecords = importCount;
                        }
                        importController._SQLString = @" if exists(select NAME from TEMPDB.sys.objects where type='u' and name='##LOGTEMPTABLE" + importTemp + "') BEGIN  select * from [##LOGTEMPTABLE" + importTemp + "] End else  Begin select 'Import Success' End ";
                        DataSet ds = importController.CreateDataSet();
                        if (ds != null && ds.Tables[0].Rows.Count > 0)
                        {
                            if (ds.Tables[0].Rows[0][0].ToString() != "Import Success")
                            {
                                importController._SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp + "]";
                                cmd1.CommandText = importController._SQLString;
                                cmd1.CommandType = CommandType.Text;
                                cmd1.ExecuteNonQuery();
                            }
                        }
                        if (ds.Tables[0].Columns[0].ColumnName == "ErrorMessage")
                        {
                            return "Import Failed~" + importTemp;
                        }
                        else
                        {
                            if (importType.ToUpper() == "PRODUCTS")
                            {
                                var picklistvaluecreation = objLS.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerId);//To check Picklist validation in enabled or not in customer setting
                            }



                            return "Import Success~" + importTemp + "~InsertRecords:" + insertRecords + "~" + "UpdateRecords:" + updateRecords + "~SkippedRecords:0";

                        }
                        if (conn.State.ToString().ToUpper() == "OPEN")
                            conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at AdvanceImportApiController : FinishProductImport", ex);
                return null;
            }
        }
        //Siranjeevi Family Import//
        //public string FamilyProductDelete(string sheetName, string excelPath, string importTemp, DataTable deleteRecords)
        //{
        //    try
        //    {
        //        using (var conn = new SqlConnection(_connectionString))
        //        {
        //            conn.Open();
        //            string subitemdelete = string.Empty;
        //            string sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" + importTemp + "]'') BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
        //            // _ic.importExcelSheetSelection(excelPath, sheetName);
        //            var dbCommand = new SqlCommand(sqlString, conn);
        //            dbCommand.ExecuteNonQuery();
        //            // _objdatatable = _ic.exceldata(excelPath, sheetName);
        //            _objdatatable = deleteRecords;
        //            if (!_objdatatable.Columns.Contains("REMOVE_PRODUCT"))
        //            {
        //                _objdatatable.Columns.Add("REMOVE_PRODUCT");

        //            }
        //            if (!_objdatatable.Columns.Contains("CATALOG_ID"))
        //            {
        //                return "CATALOG_ID~" + importTemp;

        //            }

        //            if (_objdatatable.Columns.Contains("SUBITEM#"))
        //            {
        //                subitemdelete = "subproductdelete";
        //                if (!_objdatatable.Columns.Contains("SUBPRODUCT_ID"))
        //                {
        //                    _objdatatable.Columns.Add("SUBPRODUCT_ID");

        //                }

        //            }

        //            string sqlstring1 = _ic.CreateTable("[##IMPORT" + importTemp + "]", excelPath, sheetName, _objdatatable);
        //            dbCommand = new SqlCommand(sqlstring1, conn);
        //            dbCommand.ExecuteNonQuery();
        //            var bulkCopy = new SqlBulkCopy(conn)
        //            {
        //                DestinationTableName = "[##IMPORT" + importTemp + "]"
        //            };
        //            bulkCopy.WriteToServer(_objdatatable);
        //            if (subitemdelete != "subproductdelete")
        //            {

        //                string sqlstring11 = "select distinct CATALOG_ITEM_NO, REMOVE_PRODUCT,CATALOG_ID into [##IMPORTTEMP" + importTemp + "] from [##IMPORT" + importTemp + "] ";
        //                dbCommand = new SqlCommand(sqlstring11, conn);
        //                dbCommand.ExecuteNonQuery();
        //                string sqlString4 = "EXEC('update [##IMPORTTEMP" + importTemp + "] set REMOVE_PRODUCT = ''YES'' ')";
        //                var dbCommand4 = new SqlCommand(sqlString4, conn);
        //                dbCommand4.ExecuteNonQuery();

        //                sqlString4 = "EXEC('STP_LS_PROD_BASED_REMOVAL ''" + importTemp + "'',''" + User.Identity.Name + "'' ')";
        //                dbCommand4 = new SqlCommand(sqlString4, conn);
        //                dbCommand4.CommandTimeout = 0;
        //                dbCommand4.ExecuteNonQuery();


        //                string sqlString5 = "SELECT * into [deleteresult" + importTemp + "] FROM [##LOGTEMPTABLE" + importTemp + "]";
        //                var dbCommand5 = new SqlCommand(sqlString5, conn);
        //                dbCommand5.ExecuteNonQuery();
        //            }
        //            else
        //            {
        //                string sqlstring11 = "select distinct SUBITEM# , REMOVE_PRODUCT,CATALOG_ID,FAMILY_ID,SUBPRODUCT_ID,FAMILY_NAME ,CATEGORY_ID into [##IMPORTTEMP" + importTemp + "] from [##IMPORT" + importTemp + "] ";
        //                dbCommand = new SqlCommand(sqlstring11, conn);
        //                dbCommand.ExecuteNonQuery();
        //                sqlstring11 = "update ad set SUBPRODUCT_ID = tps.PRODUCT_ID from TB_PROD_SPECS  tps join [##IMPORTTEMP" + importTemp + "] ad on ad.SUBITEM# = tps.STRING_VALUE and tps.attribute_id=1 where ad.SUBPRODUCT_ID is null ";
        //                dbCommand = new SqlCommand(sqlstring11, conn);
        //                dbCommand.ExecuteNonQuery();

        //                sqlstring11 = "update TT set family_id=tcf.FAMILY_ID  from TB_CATALOG_FAMILY tcf join TB_FAMILY tf on tf.FAMILY_ID = tcf.FAMILY_ID	JOIN [##IMPORTTEMP" + importTemp + "] TT on TT.FAMILY_NAME = tf.FAMILY_NAME and tcf.CATALOG_ID = TT.catalog_id where TT.FAMILY_ID is null";
        //                dbCommand = new SqlCommand(sqlstring11, conn);
        //                dbCommand.ExecuteNonQuery();



        //                DataSet subitems = new DataSet();
        //                var cmd1f = new SqlCommand("SELECT * FROM [##IMPORTTEMP" + importTemp + "]", conn);
        //                var daf = new SqlDataAdapter(cmd1f);
        //                daf.Fill(subitems);
        //                foreach (DataRow item in subitems.Tables[0].Rows)
        //                {
        //                    string family_id = item["FAMILY_ID"].ToString();
        //                    int catalog_id = Convert.ToInt32(item["CATALOG_ID"].ToString());
        //                    int subproduct_id = Convert.ToInt32(item["SUBPRODUCT_ID"].ToString());
        //                    string category_idsub = item["CATEGORY_ID"].ToString();
        //                    string catid = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == category_idsub).Select(c => c.CATEGORY_ID).FirstOrDefault().ToString();
        //                    DeleteSubProductPermanentFromMaster(family_id, catalog_id, catid, subproduct_id);

        //                }

        //                string sqlString5 = "SELECT SUBITEM# as CATALOG_ITEM_NO,'DELETED' as STATUS into [deleteresult" + importTemp + "] FROM [##IMPORTTEMP" + importTemp + "]";
        //                var dbCommand5 = new SqlCommand(sqlString5, conn);
        //                dbCommand5.ExecuteNonQuery();


        //            }
        //            var cmbpk41 = new SqlCommand("  exec ('UPDATE [tempresult" + importTemp + "] SET  STATUS =''DELETED'' WHERE CATALOG_ITEM_NO IN (SELECT CATALOG_ITEM_NO   FROM [##LOGTEMPTABLE" + importTemp + "] ) ')", conn);
        //            cmbpk41.ExecuteNonQuery();
        //            return "import Success~" + importTemp;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Import Failed~" + importTemp;
        //    }
        //}

        public string ImportandUpdate(string sheetName, string allowDuplicate, string excelPath, JArray model, string importTemp, AdvanceImportApiController adImport, out DataTable PdfXpressFamilyExportTable, string userId)
        {
            string subproductimport = string.Empty;
            int itemVal = Convert.ToInt32(allowDuplicate);
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                string sqlString = "EXEC('if((select COUNT(*) from sys.tables where name = ''Sheet'')<>0)BEGIN DROP TABLE [Sheet] END')";
                // _ic.importExcelSheetSelection(excelPath, sheetName);
                var dbCommand = new SqlCommand(sqlString, conn);
                dbCommand.ExecuteNonQuery();
                _objdatatable = _ic.exceldata(excelPath, sheetName);

                // Pdf Xpress To Implent the pdf xpress Concepts --- Start 
                PdfXpressFamilyExportTable = _objdatatable.Copy();


                if (_objdatatable.Columns.Contains("Catalog_PdfTemplate"))
                {
                    _objdatatable.Columns.Remove("Catalog_PdfTemplate");
                    _objdatatable.Columns.Remove("Category_PdfTemplate");
                    _objdatatable.Columns.Remove("Family_PdfTemplate");
                    _objdatatable.Columns.Remove("Product_PdfTemplate");

                }




                // Pdf Xpress To End

                if (_objdatatable.Columns.Contains("SUBITEM#"))
                {
                    subproductimport = "Skip main import and proceed to subproduct import";


                    //  return "Cannot import subproducts.Please use main import.";
                }
                if (_objdatatable.Columns.Contains("Sort"))
                {
                    _objdatatable.Columns.Remove("Sort");
                }

                int catalogId = 0;
                string categoryId = "";
                string category_short = "";
                int family_Id = 0;
                // foreach( DataColumn rr in  _objdatatable.Columns )
                {

                    if (_objdatatable.Columns.Contains("CATALOG_ID"))
                    {
                        catalogId = Convert.ToInt32(_objdatatable.Rows[0]["CATALOG_ID"]);
                    }
                    if (_objdatatable.Columns.Contains("CATEGORY_ID"))
                    {
                        category_short = Convert.ToString(_objdatatable.Rows[0]["CATEGORY_ID"]);
                        categoryId = _dbcontext.TB_CATEGORY.Where(s => s.CATEGORY_SHORT == category_short).FirstOrDefault().CATEGORY_ID;
                    }
                    if (_objdatatable.Columns.Contains("FAMILY_ID"))
                    {
                        family_Id = Convert.ToInt32(_objdatatable.Rows[0]["FAMILY_ID"]);
                    }

                    if (!_objdatatable.Columns.Contains("CATALOG_ID"))
                    {

                        _objdatatable.Columns.Add("CATALOG_ID");
                        _objdatatable.Columns["CATALOG_ID"].SetOrdinal(0);
                    }
                    if (!_objdatatable.Columns.Contains("CATEGORY_ID"))
                    {

                        _objdatatable.Columns.Add("CATEGORY_ID");
                        _objdatatable.Columns["CATEGORY_ID"].SetOrdinal(_objdatatable.Columns.IndexOf("CATEGORY_NAME"));
                    }
                    if (!_objdatatable.Columns.Contains("FAMILY_ID"))
                    {

                        _objdatatable.Columns.Add("FAMILY_ID");
                        _objdatatable.Columns["FAMILY_ID"].SetOrdinal(_objdatatable.Columns.IndexOf("FAMILY_NAME"));
                    }

                    if (!_objdatatable.Columns.Contains("PRODUCT_ID"))
                    {
                        _objdatatable.Columns.Add("PRODUCT_ID");
                        //_objdatatable.Columns["PRODUCT_ID"].SetOrdinal(_objdatatable.Columns.IndexOf("CATALOG_ITEM_NO"));
                    }
                }
                if (subproductimport != "Skip main import and proceed to subproduct import")
                {
                    if (!_objdatatable.Columns.Contains("PUBLISH2WEB"))
                    {
                        _objdatatable.Columns.Add("PUBLISH2WEB");
                        _objdatatable.Columns["PUBLISH2WEB"].SetOrdinal(_objdatatable.Columns.IndexOf("FAMILY_ID"));
                    }
                    if (!_objdatatable.Columns.Contains("PUBLISH2PRINT"))
                    {
                        _objdatatable.Columns.Add("PUBLISH2PRINT");
                        _objdatatable.Columns["PUBLISH2PRINT"].SetOrdinal(_objdatatable.Columns.IndexOf("PRODUCT_PUBLISH2WEB"));
                    }
                    if (!_objdatatable.Columns.Contains("PUBLISH2PDF"))
                    {
                        _objdatatable.Columns.Add("PUBLISH2PDF");
                        _objdatatable.Columns["PUBLISH2PDF"].SetOrdinal(_objdatatable.Columns.IndexOf("PRODUCT_PUBLISH2PDF"));
                    }

                    sqlString = _ic.CreateTable("[##IMPORTTEMP" + importTemp + "]", excelPath, sheetName, _objdatatable);
                    dbCommand = new SqlCommand(sqlString, conn);
                    dbCommand.ExecuteNonQuery();
                    //var bulkCopy = new SqlBulkCopy(conn)
                    //{
                    //    DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    //};
                    //bulkCopy.WriteToServer(_objdatatable);

                    DataTable importData = adImport.UnPivotTable(_objdatatable, "Product_BULK");
                    SqlCommand dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_FAMILYINSERTTEMPDATA", conn);
                    dbCommandTemp.CommandType = CommandType.StoredProcedure;

                    dbCommandTemp.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = importData;
                    dbCommandTemp.Parameters.Add("@TABLENAME", SqlDbType.NChar).Value = "[##IMPORTTEMP" + importTemp + "]";
                    dbCommand.CommandTimeout = 0;
                    dbCommandTemp.ExecuteNonQuery();
                }
                else
                {

                    //----------------------For subproduct import ---------------------------------------
                    //-------------For adding sub productid column for import without id Columns//////////////
                    if (!_objdatatable.Columns.Contains("SUBPRODUCT_PUBLISH2WEB"))
                    {
                        _objdatatable.Columns.Add("SUBPRODUCT_PUBLISH2WEB");
                        _objdatatable.Columns["SUBPRODUCT_PUBLISH2WEB"].SetOrdinal(_objdatatable.Columns.IndexOf("SUBPRODUCT_ID"));
                    }
                    if (!_objdatatable.Columns.Contains("SUBPRODUCT_PUBLISH2PDF"))
                    {
                        _objdatatable.Columns.Add("SUBPRODUCT_PUBLISH2PDF");
                        _objdatatable.Columns["SUBPRODUCT_PUBLISH2PDF"].SetOrdinal(_objdatatable.Columns.IndexOf("SUBPRODUCT_PUBLISH2WEB"));
                    }
                    if (!_objdatatable.Columns.Contains("SUBPRODUCT_PUBLISH2EXPORT"))
                    {
                        _objdatatable.Columns.Add("SUBPRODUCT_PUBLISH2EXPORT");
                        _objdatatable.Columns["SUBPRODUCT_PUBLISH2EXPORT"].SetOrdinal(_objdatatable.Columns.IndexOf("SUBPRODUCT_PUBLISH2PDF"));
                    }
                    if (!_objdatatable.Columns.Contains("SUBPRODUCT_PUBLISH2PRINT"))
                    {
                        _objdatatable.Columns.Add("SUBPRODUCT_PUBLISH2PRINT");
                        _objdatatable.Columns["SUBPRODUCT_PUBLISH2PRINT"].SetOrdinal(_objdatatable.Columns.IndexOf("SUBPRODUCT_PUBLISH2EXPORT"));
                    }
                    if (!_objdatatable.Columns.Contains("SUBPRODUCT_PUBLISH2PORTAL"))
                    {
                        _objdatatable.Columns.Add("SUBPRODUCT_PUBLISH2PORTAL");
                        _objdatatable.Columns["SUBPRODUCT_PUBLISH2PORTAL"].SetOrdinal(_objdatatable.Columns.IndexOf("SUBPRODUCT_PUBLISH2PRINT"));
                    }
                    if (!_objdatatable.Columns.Contains("SUBPRODUCT_ID"))
                    {

                        _objdatatable.Columns.Add("SUBPRODUCT_ID");
                        _objdatatable.Columns["SUBPRODUCT_ID"].SetOrdinal(8);
                    }
                    /////////////////////////////////////////--------------------------------------//////////////////////////////////----------------VinothKumar March 2017
                    sqlString = _ic.CreateTable("[##SUBPRODUCTIMPORTTEMP" + importTemp + "]", excelPath, sheetName, _objdatatable);
                    dbCommand = new SqlCommand(sqlString, conn);
                    dbCommand.ExecuteNonQuery();

                    DataTable importData = adImport.UnPivotTable(_objdatatable, "SubProduct");
                    SqlCommand dbCommandTemp = new SqlCommand("STP_CATALOGSTUDIO_INSERTSUBTEMPDATA", conn);
                    dbCommandTemp.CommandType = CommandType.StoredProcedure;
                    dbCommandTemp.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = importData;
                    dbCommandTemp.Parameters.Add("@TABLENAME", SqlDbType.NChar).Value = "[##SUBPRODUCTIMPORTTEMP" + importTemp + "]";
                    dbCommandTemp.ExecuteNonQuery();
                    //var bulkCopysp = new SqlBulkCopy(conn)
                    //{
                    //    DestinationTableName = "[##SUBPRODUCTIMPORTTEMP" + importTemp + "]"
                    //};
                    //bulkCopysp.WriteToServer(_objdatatable);

                    //------------------------
                }

                DataTable oattType = _ic.SelectedColumnsToImport(_objdatatable, model);
                if (subproductimport != "Skip main import and proceed to subproduct import")
                {
                    var sqlstring1 = new SqlCommand("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##ATTRIBUTETEMP" + importTemp + "]'') BEGIN DROP TABLE [##ATTRIBUTETEMP" + importTemp + "] END')", conn);
                    sqlstring1.ExecuteNonQuery();
                    var cmd2 = new SqlCommand();
                    cmd2.Connection = conn;
                    sqlString = _ic.CreateTableToImport("[##ATTRIBUTETEMP" + importTemp + "]", oattType);
                    cmd2.CommandText = sqlString;
                    cmd2.CommandType = CommandType.Text;
                    cmd2.ExecuteNonQuery();
                    var bulkCopysp = new SqlBulkCopy(conn)
                    {
                        DestinationTableName = "[##ATTRIBUTETEMP" + importTemp + "]"
                    };
                    bulkCopysp.WriteToServer(oattType);
                    var sqlstring12 = new SqlCommand("update [##ATTRIBUTETEMP" + importTemp + "] set ATTRIBUTE_TYPE = 1 where ATTRIBUTE_TYPE = 10", conn);
                    sqlstring12.ExecuteNonQuery();

                }
                else
                {

                    //------------for sub product attribute temp-------------------

                    var sqlstring1 = new SqlCommand("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]'') BEGIN DROP TABLE [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] END')", conn);
                    sqlstring1.ExecuteNonQuery();
                    var cmd2 = new SqlCommand();
                    cmd2.Connection = conn;
                    sqlString = _ic.CreateTableToImport("[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]", oattType);
                    cmd2.CommandText = sqlString;
                    cmd2.CommandType = CommandType.Text;
                    cmd2.ExecuteNonQuery();
                    var bulkCopysp = new SqlBulkCopy(conn)
                    {
                        DestinationTableName = "[##SUBPRODUCTATTRIBUTETEMP" + importTemp + "]"
                    };
                    bulkCopysp.WriteToServer(oattType);

                    var sqlstring122 = new SqlCommand("update [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] set ATTRIBUTE_TYPE = 1 where ATTRIBUTE_TYPE = 10", conn);
                    sqlstring122.ExecuteNonQuery();
                    //-----------------------------
                }
                DataSet catalog = new DataSet();
                // SqlDataAdapter dd = new SqlDataAdapter("SELECT TOP(1) CATALOG_NAME FROM SHEET", conn);
                // dd.Fill(catalog);
                // if (catalog.Tables[0].Rows.Count > 0)
                // {
                //  string column_value = catalog.Tables[0].Rows[0][0].ToString();
                if (subproductimport != "Skip main import and proceed to subproduct import")
                {
                    var cmbpk1 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1'') IS NOT NULL  DROP TABLE  ##t1') ", conn);
                    cmbpk1.ExecuteNonQuery();
                    var cmbpk2 = new SqlCommand("select * into ##t1  from [##AttributeTemp" + importTemp + "] where attribute_type <>0", conn);
                    cmbpk2.ExecuteNonQuery();
                    var cmbpk3 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2'') IS NOT NULL  DROP TABLE  ##t2') ", conn);
                    cmbpk3.ExecuteNonQuery();
                    var cmbpk4 = new SqlCommand("select * into ##t2  from [##importtemp" + importTemp + "]", conn);
                    cmbpk4.ExecuteNonQuery();
                }
                else
                {
                    var cmbpk1 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1'') IS NOT NULL  DROP TABLE  ##t1') ", conn);
                    cmbpk1.ExecuteNonQuery();
                    var cmbpk2 = new SqlCommand("select * into ##t1  from [##SUBPRODUCTATTRIBUTETEMP" + importTemp + "] where attribute_type <>0", conn);
                    cmbpk2.ExecuteNonQuery();
                    var cmbpk3 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2'') IS NOT NULL  DROP TABLE  ##t2') ", conn);
                    cmbpk3.ExecuteNonQuery();
                    var cmbpk4 = new SqlCommand("select * into ##t2  from [##SUBPRODUCTIMPORTTEMP" + importTemp + "]", conn);
                    cmbpk4.ExecuteNonQuery();
                }
                var customerId = 0;
                var skucnt = _dbcontext.TB_PLAN
                  .Join(_dbcontext.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                  .Join(_dbcontext.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                  .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                var SKUcount = skucnt.Select(a => a).ToList();

                if (SKUcount.Any())
                {
                    // userSKUProductCount = Convert.ToInt32(SKUcount[0].SKU_COUNT);
                    customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                }

                var Checkpicklistupdate = _dbcontext.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerId);
                if (Checkpicklistupdate != null && !Checkpicklistupdate.ShowCustomAPPS)
                {
                    DataSet pkname = new DataSet();
                    SqlDataAdapter daa = new SqlDataAdapter("select PICKLIST_NAME,ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select attribute_name  from ##t1 where attribute_type <>0) and USE_PICKLIST =1", conn);
                    daa.Fill(pkname);
                    if (pkname.Tables[0].Rows.Count > 0)
                    {
                        DataTable Errortable = new DataTable();
                        Errortable.Columns.Add("STATUS");
                        Errortable.Columns.Add("ITEM_NO");
                        Errortable.Columns.Add("ATTRIBUTE_NAME");
                        Errortable.Columns.Add("PICKLIST_VALUE");

                        foreach (DataRow vr in pkname.Tables[0].Rows)
                        {
                            DataTable _pickListValue = new DataTable();
                            DataSet newsheetval = new DataSet();
                            _pickListValue.Columns.Add("Value");
                            string picklistname = vr["PICKLIST_NAME"].ToString();
                            DataSet dpicklistdata = new DataSet();
                            if (!string.IsNullOrEmpty(picklistname))
                            {
                                var pickList = _dbcontext.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                                {
                                    PICKLIST_DATA = s.PICKLIST_DATA,
                                    PICKLIST_NAME = s.PICKLIST_NAME,
                                    PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                                }).FirstOrDefault(d => d.PICKLIST_NAME == picklistname);
                                var objPick = new List<PickList>();
                                if (pickList != null && !string.IsNullOrEmpty(pickList.PICKLIST_DATA))
                                {
                                    var pkxmlData = pickList.PICKLIST_DATA;

                                    if (pkxmlData.Contains("<?xml version"))
                                    {
                                        pkxmlData = pkxmlData.Replace("\\r\\n", "");
                                        pkxmlData = pkxmlData.Replace("\r\n", "");
                                        pkxmlData = pkxmlData.Replace("\\\"", "\"");
                                        pkxmlData = pkxmlData.Replace("<?xml version=\"1.0\" standalone=\"yes\"?>", "");
                                        XDocument objXml = XDocument.Parse(pkxmlData);

                                        objPick =
                                            objXml.Descendants("NewDataSet")
                                                .Descendants("Table1")
                                                .Select(d =>
                                                {
                                                    var xElement = d.Element("ListItem");
                                                    return xElement != null
                                                        ? new PickList
                                                        {
                                                            ListItem = xElement.Value
                                                        }
                                                        : null;
                                                }).ToList();
                                    }
                                }
                                SqlDataAdapter daa2 =
                                        new SqlDataAdapter("Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  ['+ @colName+'],CATALOG_ITEM_NO   from [##t2]') ",
                                            conn);
                                daa2.Fill(newsheetval);
                                SqlCommand cvb = new SqlCommand("DELETE TOP(1) From ##t1", conn);
                                cvb.ExecuteNonQuery();
                                DataSet picklistdataDS = new DataSet();
                                DataTable newTable = new DataTable();
                                // DataSet newsheetval1 = new DataSet();
                                for (int rowCount = 0; rowCount < newsheetval.Tables[0].Rows.Count; rowCount++)
                                {
                                    if (newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim() != "")
                                    {
                                        var picklistvalue = newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim();
                                        var dd = objPick.Where(x => x.ListItem == picklistvalue);
                                        if (!dd.Any())
                                        {

                                            // SqlDataAdapter daa3 =
                                            //  new SqlDataAdapter("Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  CATALOG_ITEM_NO   from [##t2] where  ['+ @colName+'] = ''" + picklistvalue + "''') ",
                                            //    conn);
                                            //       //  daa3.Fill(newsheetval1);
                                            string CAT = string.Empty;
                                            //if (newsheetval1.Tables[0].Rows.Count > 0)
                                            //{
                                            CAT = newsheetval.Tables[0].Rows[rowCount][1].ToString();
                                            // }

                                            Errortable.Rows.Add("Import failed due to invalid Pick List value.", CAT, vr["ATTRIBUTE_NAME"], picklistvalue);
                                            // return "Import failed due to invalid Pick List value\nAttribute Name : " + vr["ATTRIBUTE_NAME"] + "\nPicklist Value : " + picklistvalue + "\nCat # : " + CAT;
                                        }

                                    }
                                }


                            }
                        }
                        if (Errortable.Rows.Count > 0)
                        {
                            sqlString = _ic.CreateTableToImport("[Picklistlog" + importTemp + "]", Errortable);
                            var cmd2 = new SqlCommand();
                            cmd2.Connection = conn;
                            cmd2.CommandText = sqlString;
                            cmd2.CommandType = CommandType.Text;
                            cmd2.ExecuteNonQuery();
                            var bulkCopysp = new SqlBulkCopy(conn)
                            {
                                DestinationTableName = "[Picklistlog" + importTemp + "]"
                            };
                            bulkCopysp.WriteToServer(Errortable);
                            return "Import failed due to invalid Pick List value~" + importTemp;
                        }
                    }
                }
                try
                {
                    DataSet newsheetval4 = new DataSet();
                    DataSet newsheetval5 = new DataSet();
                    DataSet newsheetval6 = new DataSet();
                    if (subproductimport != "Skip main import and proceed to subproduct import")
                    {
                        SqlDataAdapter daa55 =
               new SqlDataAdapter("SELECT CATALOG_ITEM_NO FROM [##IMPORTTEMP" + importTemp + "] WHERE CATALOG_ITEM_NO IS   NULL ",
                   conn);
                        daa55.Fill(newsheetval4);

                        if (newsheetval4.Tables[0].Rows.Count == 0)
                        {

                            //Id is must for family import
                            SqlDataAdapter daa57 =
               new SqlDataAdapter("SELECT'Import failed due to errors.' AS STATUS, PRODUCT_ID ,CATALOG_ITEM_NO AS ITEM_NO FROM  [##IMPORTTEMP" + importTemp + "] WHERE PRODUCT_ID NOT IN (SELECT PRODUCT_ID FROM TB_PROD_FAMILY WHERE FAMILY_ID in  (select distinct FAMILY_ID from [##IMPORTTEMP" + importTemp + "])) ",
                   conn);
                            daa57.Fill(newsheetval6);

                            DataSet pkname = new DataSet();
                            SqlDataAdapter daa = new SqlDataAdapter("select PICKLIST_NAME,ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select attribute_name  from ##t1 where attribute_type <>0) and USE_PICKLIST =1", conn);
                            daa.Fill(pkname);
                            string pickdatatype = string.Empty;
                            if (pkname.Tables[0].Rows.Count > 0)
                            {
                                DataTable Errortable = new DataTable();
                                Errortable.Columns.Add("STATUS");
                                Errortable.Columns.Add("ITEM_NO");
                                Errortable.Columns.Add("ATTRIBUTE_NAME");
                                Errortable.Columns.Add("PICKLIST_VALUE");

                                foreach (DataRow vr in pkname.Tables[0].Rows)
                                {
                                    DataTable _pickListValue = new DataTable();
                                    DataSet newsheetval = new DataSet();
                                    _pickListValue.Columns.Add("Value");
                                    string picklistname = vr["PICKLIST_NAME"].ToString();
                                    DataSet dpicklistdata = new DataSet();
                                    if (!string.IsNullOrEmpty(picklistname))
                                    {
                                        var pickList = _dbcontext.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                                        {
                                            PICKLIST_DATA = s.PICKLIST_DATA,
                                            PICKLIST_NAME = s.PICKLIST_NAME,
                                            PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                                        }).FirstOrDefault(d => d.PICKLIST_NAME == picklistname);
                                        pickdatatype = pickList.PICKLIST_DATA_TYPE.ToString();

                                        if (pickdatatype != null && pickdatatype.ToString() == "Numeric")
                                        {


                                            var objPick = new List<PickList>();
                                            if (pickList != null && !string.IsNullOrEmpty(pickList.PICKLIST_DATA))
                                            {
                                                var pkxmlData = pickList.PICKLIST_DATA;

                                                if (pkxmlData.Contains("<?xml version"))
                                                {
                                                    pkxmlData = pkxmlData.Replace("\\r\\n", "");
                                                    pkxmlData = pkxmlData.Replace("\r\n", "");
                                                    pkxmlData = pkxmlData.Replace("\\\"", "\"");
                                                    pkxmlData = pkxmlData.Replace("<?xml version=\"1.0\" standalone=\"yes\"?>", "");
                                                    XDocument objXml = XDocument.Parse(pkxmlData);

                                                    objPick =
                                                        objXml.Descendants("NewDataSet")
                                                            .Descendants("Table1")
                                                            .Select(d =>
                                                            {
                                                                var xElement = d.Element("ListItem");
                                                                return xElement != null
                                                                    ? new PickList
                                                                    {
                                                                        ListItem = xElement.Value
                                                                    }
                                                                    : null;
                                                            }).ToList();
                                                }
                                            }
                                            SqlDataAdapter daa2 =
                                                    new SqlDataAdapter("Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  ['+ @colName+'],CATALOG_ITEM_NO   from [##t2]') ",
                                                        conn);
                                            daa2.Fill(newsheetval);
                                            SqlCommand cvb = new SqlCommand("DELETE TOP(1) From ##t1", conn);
                                            cvb.ExecuteNonQuery();
                                            DataSet picklistdataDS = new DataSet();
                                            DataTable newTable = new DataTable();

                                            // DataSet newsheetval1 = new DataSet();
                                            for (int rowCount = 0; rowCount < newsheetval.Tables[0].Rows.Count; rowCount++)
                                            {
                                                if (newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim() != "")
                                                {
                                                    int value = 0;
                                                    var picklistvalue = newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim();
                                                    var dd = objPick.Where(x => x.ListItem == picklistvalue);
                                                    if (int.TryParse(picklistvalue.ToString(), out value))
                                                    {
                                                        //  picklistvalue = Convert.ToString(value);
                                                    }
                                                    else
                                                    {
                                                        string CAT = string.Empty;

                                                        CAT = newsheetval.Tables[0].Rows[rowCount][1].ToString();
                                                        // }

                                                        Errortable.Rows.Add("Import failed due to invalid Pick List value.", CAT, vr["ATTRIBUTE_NAME"], picklistvalue);

                                                    }
                                                    //if (!dd.Any())
                                                    {

                                                        // SqlDataAdapter daa3 =
                                                        //  new SqlDataAdapter("Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  CATALOG_ITEM_NO   from [##t2] where  ['+ @colName+'] = ''" + picklistvalue + "''') ",
                                                        //    conn);
                                                        //       //  daa3.Fill(newsheetval1);

                                                        // return "Import failed due to invalid Pick List value\nAttribute Name : " + vr["ATTRIBUTE_NAME"] + "\nPicklist Value : " + picklistvalue + "\nCat # : " + CAT;
                                                    }

                                                }
                                            }
                                            if (Errortable.Rows.Count > 0)
                                            {
                                                sqlString = _ic.CreateTableToImport("[Picklistlog" + importTemp + "]", Errortable);
                                                var cmd2 = new SqlCommand();
                                                cmd2.Connection = conn;
                                                cmd2.CommandText = sqlString;
                                                cmd2.CommandType = CommandType.Text;
                                                cmd2.ExecuteNonQuery();
                                                var bulkCopysp = new SqlBulkCopy(conn)
                                                {
                                                    DestinationTableName = "[Picklistlog" + importTemp + "]"
                                                };
                                                bulkCopysp.WriteToServer(Errortable);
                                                return "Import failed due to invalid Pick List value~" + importTemp;
                                            }

                                        }
                                    }
                                }
                            }

                            if (newsheetval6.Tables[0].Rows.Count == 0)
                            {
                                string attrmapp = "EXEC STP_LS_FAMILY_IMPORT '" + importTemp + "','0','" + User.Identity.Name + "'," + customerId + "";
                                var dbCommandap = new SqlCommand(attrmapp, conn);
                                dbCommandap.CommandTimeout = 0;
                                dbCommandap.ExecuteNonQuery();
                            }
                            else
                            {
                                string Checkbeforeimport = string.Empty;
                                string Checkbeforefailed = string.Empty;
                                foreach (DataRow row in newsheetval6.Tables[0].Rows)
                                {
                                    object value = row["PRODUCT_ID"];
                                    if (value == DBNull.Value)
                                    {
                                        Checkbeforeimport = "PROCEED";
                                    }
                                    else
                                    {
                                        Checkbeforefailed = "STOP";
                                    }
                                }

                                if (Checkbeforeimport == "PROCEED")
                                {
                                    string attrmapp = "EXEC STP_LS_FAMILY_IMPORT '" + importTemp + "','0','" + User.Identity.Name + "'," + customerId + "";

                                    var dbCommandap = new SqlCommand(attrmapp, conn);
                                    dbCommandap.CommandTimeout = 0;
                                    dbCommandap.ExecuteNonQuery();
                                }
                                if (Checkbeforefailed == "STOP")
                                {
                                    sqlString = _ic.CreateTableToImport("[errorlog" + importTemp + "]", newsheetval6.Tables[0]);
                                    var cmd2 = new SqlCommand();
                                    cmd2.CommandText = sqlString;
                                    cmd2.CommandType = CommandType.Text;
                                    cmd2.ExecuteNonQuery();
                                    var bulkCopysp = new SqlBulkCopy(conn)
                                    {
                                        DestinationTableName = "[errorlog" + importTemp + "]"
                                    };
                                    bulkCopysp.WriteToServer(newsheetval6.Tables[0]);
                                    return "Import failed due to errors" + importTemp;
                                }

                            }
                        }
                        else
                        {
                            return "Import failed [Cat #] cannot be empty.";
                        }
                    }
                    else
                    {
                        SqlDataAdapter daa55 =
               new SqlDataAdapter("SELECT CATALOG_ITEM_NO FROM [##SUBPRODUCTIMPORTTEMP" + importTemp + "] WHERE CATALOG_ITEM_NO IS  NULL ",
                   conn);
                        daa55.Fill(newsheetval4);

                        if (newsheetval4.Tables[0].Rows.Count == 0)
                        {


                            SqlDataAdapter daa57 =
               new SqlDataAdapter("SELECT'Import failed due to errors.' AS STATUS, PRODUCT_ID ,CATALOG_ITEM_NO AS ITEM_NO FROM  [##SUBPRODUCTIMPORTTEMP" + importTemp + "] WHERE PRODUCT_ID NOT IN (SELECT PRODUCT_ID FROM TB_PROD_FAMILY WHERE FAMILY_ID in  (select distinct FAMILY_ID from [##SUBPRODUCTIMPORTTEMP" + importTemp + "])) ",
                   conn);
                            daa57.Fill(newsheetval6);
                            if (newsheetval6.Tables[0].Rows.Count == 0)
                            {
                                string subimp = "EXEC STP_CATALOGSTUDIO5_PROD_INSERT_SUBPRODUCTIMPORT_FAMILY '" + importTemp + "','0'," + customerId + ",'" + User.Identity.Name + "'";
                                var dbCommandsub = new SqlCommand(subimp, conn);
                                dbCommandsub.CommandTimeout = 0;
                                dbCommandsub.ExecuteNonQuery();
                            }
                            else
                            {
                                string Checkbeforeimport = string.Empty;
                                string Checkbeforefailed = string.Empty;
                                foreach (DataRow row in newsheetval6.Tables[0].Rows)
                                {
                                    object value = row["PRODUCT_ID"];
                                    if (value == DBNull.Value)
                                    {
                                        Checkbeforeimport = "PROCEED";
                                    }
                                    else
                                    {
                                        Checkbeforefailed = "STOP";
                                    }
                                }
                                if (Checkbeforeimport == "PROCEED")
                                {
                                    string attrmapp = "EXEC STP_CATALOGSTUDIO5_PROD_INSERT_SUBPRODUCTIMPORT_FAMILY '" + importTemp + "','0'," + customerId + ",'" + User.Identity.Name + "'";
                                    var dbCommandap = new SqlCommand(attrmapp, conn);
                                    dbCommandap.ExecuteNonQuery();
                                }
                                if (Checkbeforefailed == "STOP")
                                {
                                    sqlString = _ic.CreateTableToImport("[errorlog" + importTemp + "]", newsheetval6.Tables[0]);
                                    var cmd2 = new SqlCommand();
                                    cmd2.CommandText = sqlString;
                                    cmd2.CommandType = CommandType.Text;
                                    cmd2.ExecuteNonQuery();
                                    var bulkCopysp = new SqlBulkCopy(conn)
                                    {
                                        DestinationTableName = "[errorlog" + importTemp + "]"
                                    };
                                    bulkCopysp.WriteToServer(newsheetval6.Tables[0]);
                                    return "Import failed due to errors" + importTemp;
                                }

                            }
                        }
                        else
                        {
                            return "Import failed [Cat #] cannot be empty.";
                        }


                    }



                }
                catch (Exception ex)
                { }

                DataSet ds = new DataSet();

                if (subproductimport == "Skip main import and proceed to subproduct import")
                {

                    _ic._SQLString = @"select * from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                    ds = _ic.CreateDataSet();
                    _ic._SQLString = @"select * into [tempresultsub" + importTemp + "] from [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]";
                    var dbCommandap1 = new SqlCommand();
                    dbCommandap1.Connection = conn;
                    dbCommandap1.CommandText = _ic._SQLString;
                    dbCommandap1.CommandType = CommandType.Text;
                    dbCommandap1.ExecuteNonQuery();
                    //return "Import failed for subproducts~" + importTemp;

                }
                else
                {
                    _ic._SQLString = @"select * from [##LOGTEMPTABLE" + importTemp + "]";
                    ds = _ic.CreateDataSet();
                    _ic._SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp + "]";
                    var dbCommandap1 = new SqlCommand();
                    dbCommandap1.Connection = conn;
                    dbCommandap1.CommandText = _ic._SQLString;
                    dbCommandap1.CommandType = CommandType.Text;
                    dbCommandap1.ExecuteNonQuery();

                }
                var picklistvaluecreation = _dbcontext.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerId);
                //------------------For picklist data addition of new values -------------------------------

                if (picklistvaluecreation != null && picklistvaluecreation.ShowCustomAPPS)
                {
                    try
                    {
                        DataSet checkPICK = new DataSet();
                        if (subproductimport != "Skip main import and proceed to subproduct import")
                        {
                            var cmd1f = new SqlCommand("SELECT * FROM [##LOGTEMPTABLE" + importTemp + "]", conn);
                            var daf = new SqlDataAdapter(cmd1f);
                            daf.Fill(checkPICK);
                        }
                        else
                        {
                            var cmd1f = new SqlCommand("SELECT * FROM [##SUBPRODUCTLOGTEMPTABLE" + importTemp + "]", conn);
                            var daf = new SqlDataAdapter(cmd1f);
                            daf.Fill(checkPICK);
                        }

                        string check = string.Empty;
                        if (checkPICK.Tables[0].Rows.Count > 0)
                            foreach (DataColumn column1 in checkPICK.Tables[0].Columns)
                            {
                                if (column1.ToString() == "STATUS")
                                {
                                    check = "UPDATED";

                                }
                            }
                        if (check == "UPDATED")
                        {
                            // if (checkPICK.Tables[0].Rows[0][0].ToString() == "UPDATED")
                            {

                                DataSet pkname = new DataSet();
                                SqlDataAdapter daa =
                                    new SqlDataAdapter(
                                        "select PICKLIST_NAME,ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select attribute_name  from ##t1 where attribute_type <>0) and USE_PICKLIST =1",
                                        conn);
                                daa.Fill(pkname);
                                if (pkname.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow vr in pkname.Tables[0].Rows)
                                    {
                                        DataTable _pickListValue = new DataTable();
                                        DataSet newsheetval = new DataSet();
                                        _pickListValue.Columns.Add("Value");
                                        string picklistname = vr["PICKLIST_NAME"].ToString();
                                        string attrname = vr["ATTRIBUTE_NAME"].ToString();
                                        DataSet dpicklistdata = new DataSet();
                                        if (!string.IsNullOrEmpty(picklistname))
                                        {
                                            var pickList = _dbcontext.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                                            {
                                                PICKLIST_DATA = s.PICKLIST_DATA,
                                                PICKLIST_NAME = s.PICKLIST_NAME,
                                                PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                                            }).FirstOrDefault(d => d.PICKLIST_NAME == picklistname);
                                            var objPick = new List<PickList>();
                                            if (pickList != null && !string.IsNullOrEmpty(pickList.PICKLIST_DATA))
                                            {
                                                var pkxmlData = pickList.PICKLIST_DATA;
                                                if (pkxmlData.Contains("<?xml version"))
                                                {
                                                    // pkxmlData = pkxmlData.Remove(0, 1);
                                                    //pkxmlData = pkxmlData.Remove(pkxmlData.Length - 1);
                                                    pkxmlData = pkxmlData.Replace("\\r\\n", "");
                                                    pkxmlData = pkxmlData.Replace("\r\n", "");
                                                    pkxmlData = pkxmlData.Replace("\\\"", "\"");
                                                    pkxmlData =
                                                        pkxmlData.Replace(
                                                            "<?xml version=\"1.0\" standalone=\"yes\"?>",
                                                            "");
                                                    //PKXMLData = PKXMLData.Replace("ListItem", "Value");
                                                    XDocument objXml = XDocument.Parse(pkxmlData);
                                                    objPick =
                                                        objXml.Descendants("NewDataSet")
                                                            .Descendants("Table1")
                                                            .Select(d =>
                                                            {
                                                                var xElement = d.Element("ListItem");
                                                                return xElement != null
                                                                    ? new PickList
                                                                    {
                                                                        ListItem = xElement.Value
                                                                    }
                                                                    : null;
                                                            }).ToList();
                                                }
                                            }
                                            //  DataSet pkcheck = new DataSet();
                                            // SqlDataAdapter dd = new SqlDataAdapter("select ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select top(1)attribute_name  from ##t1 where attribute_type <>0) and USE_PICKLIST =1", conn);
                                            //  dd.Fill(pkcheck);
                                            // if (pkcheck.Tables[0].Rows.Count > 0)

                                            //SqlDataAdapter daa2 =
                                            //        new SqlDataAdapter(" Declare @colName VARCHAR(max) " +
                                            //                           "SET @colName = (select ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select top(1)attribute_name  from ##t1 where attribute_type <>0) and USE_PICKLIST =1) " +
                                            //                           " Exec('select distinct ['+ @colName+']   from [##t2]') ",
                                            var pickdatatype = _dbcontext.TB_PICKLIST.Where(p1 => p1.PICKLIST_NAME == picklistname).Select(p => p.PICKLIST_DATA_TYPE);
                                            SqlDataAdapter daa2 =
                                                   new SqlDataAdapter("select distinct [" + attrname + "] from [##t2] ", conn);
                                            daa2.Fill(newsheetval);
                                            SqlCommand cvb = new SqlCommand("DELETE TOP(1) From ##t1", conn);
                                            cvb.ExecuteNonQuery();
                                            DataSet picklistdataDS = new DataSet();
                                            DataTable newTable = new DataTable();
                                            // newTable.Columns.Add("value");
                                            for (int rowCount = 0;
                                                rowCount < newsheetval.Tables[0].Rows.Count;
                                                rowCount++)
                                            {
                                                //DataRow DRow = _pickListValue.NewRow();
                                                if (newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim() != "")
                                                {
                                                    var picklistvalue =
                                                        newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim();
                                                    int value;
                                                    if (pickList != null && pickList.PICKLIST_DATA_TYPE.ToString() == "Numeric")
                                                    {
                                                        if (int.TryParse(picklistvalue.ToString(), out value))
                                                        {
                                                            picklistvalue = Convert.ToString(value);
                                                        }
                                                        else
                                                        {


                                                            picklistvalue = string.Empty;

                                                        }
                                                    }
                                                    PickList objlist = new PickList();
                                                    if (!string.IsNullOrEmpty(picklistvalue))
                                                    {
                                                        objlist.ListItem = picklistvalue;
                                                    }
                                                    if (!objPick.Contains(objlist))
                                                    {
                                                        objPick.Add(objlist);
                                                    }

                                                }
                                            }
                                            var picklistdatas = objPick.DistinctBy(x => x.ListItem).ToList();
                                            for (int i = picklistdatas.Count - 1; i >= 0; i--)
                                            {

                                                if (string.IsNullOrEmpty(picklistdatas[i].ToString()))
                                                {
                                                    picklistdatas.RemoveAt(i);
                                                }
                                                else if (picklistdatas[i].ToString() == "")
                                                {
                                                    picklistdatas.RemoveAt(i);
                                                }
                                                else
                                                {
                                                    //  item.ListItem = item.ListItem.Trim();
                                                    picklistdatas[i] = picklistdatas[i];
                                                }

                                            }
                                            foreach (var item in picklistdatas)
                                            {

                                            }
                                            var serializer1 = new JavaScriptSerializer();
                                            var json = serializer1.Serialize(picklistdatas);
                                            XmlDocument doc = JsonConvert.DeserializeXmlNode(
                                                "{\"Table1\":" + json + "}", "NewDataSet");
                                            string picklistdataxml =
                                                "<?xml version=\"1.0\" standalone=\"yes\"?>" +
                                                doc.InnerXml;
                                            var objPicklist =
                                                _dbcontext.TB_PICKLIST.FirstOrDefault(
                                                    s => s.PICKLIST_NAME == picklistname);
                                            if (objPicklist != null)
                                                objPicklist.PICKLIST_DATA = picklistdataxml;
                                            _dbcontext.SaveChanges();

                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
                //----------------------end 

                HomeApiController objHomeApiController = new HomeApiController();
                objHomeApiController.AssociateProductAttributePack(catalogId, family_Id, categoryId);

                if (subproductimport == "Skip main import and proceed to sub product import")
                {
                    if (ds.Tables[0].Columns[0].ColumnName == "ErrorMessage")
                    {
                        if (ds.Tables[0].Rows[0][0].ToString() == "failed subproducts")
                        {
                            return "Import failed for sub products~" + importTemp;
                        }
                    }
                    return "Import Success Sub products~" + importTemp;

                }
                else if (ds.Tables[0].Columns[0].ColumnName == "ErrorMessage")
                {

                    return "Import Failed~" + importTemp;


                }
                else
                {

                    // When Importing Success then its Go to update a pdf xpress table values

                    DataTable PdfXpressVaues = PdfXpressFamilyExportTable;


                    // To get First Row data Values:
                    for (int i = 1; i < PdfXpressVaues.Rows.Count; i++)
                    {
                        PdfXpressVaues.Rows.RemoveAt(i);
                    }


                    SaveXpressPdfFamilyExport(PdfXpressVaues, userId);
                    // obj_XpressCatalogController.SaveTypeOfPdfxpressHierarchy();


                    return "Import Success~" + importTemp;
                }

            }
        }

        //--------------------------------------------------DELETE PROCESS----------------------------------------------

        public DataTable GetDataFromExcelForDelete(string SheetName, string excelPath, string query, string deleteflag)
        {
            DataTable dtexcel = new DataTable();
            DataTable dtexcelNew = new DataTable();
            try
            {
                var fileName = Path.GetFileName(excelPath).Split('.')[0];
                var path = Path.GetFullPath(excelPath);
                var fileExtention = Path.GetExtension(excelPath);
                string excelPathCopy = path.Replace(Path.GetFileName(excelPath), "") + "//" + fileName + "_Copy" + "." + fileExtention;
                using (OleDbConnection conn = excelConnection(excelPath))
                {

                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    DataTable dtColumns = new DataTable();
                    try
                    {
                        DataRow[] schemaRow = schemaTable.Select("TABLE_NAME like '" + SheetName + "%'");
                        string sheet = schemaRow[0]["TABLE_NAME"].ToString();
                        int selectedIndex = 0;
                        foreach (DataRow dRow in schemaTable.Rows)
                        {

                            if (sheet.ToUpper() == dRow["TABLE_NAME"].ToString().ToUpper())
                                break;
                            selectedIndex++;
                        }

                        DataTable dt_checkHeaderColumns = new DataTable();
                        if (!sheet.EndsWith("_"))
                        {
                            OleDbDataAdapter da_checkHeaderexcel = new OleDbDataAdapter(query, conn);
                            dt_checkHeaderColumns.Locale = CultureInfo.CurrentCulture;


                            da_checkHeaderexcel.Fill(dt_checkHeaderColumns);

                        }




                        if (dt_checkHeaderColumns.Rows.Count > 0)
                        {

                            var categoryIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                           .Select((c, i) => new { c, i })
                           .Where(x => x.c.ToString().Equals("CATEGORY_NAME"))
                           .Select(x => x.i)
                           .ToArray();


                            var familyIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                          .Select((c, i) => new { c, i })
                          .Where(x => x.c.ToString().Equals("FAMILY_NAME"))
                          .Select(x => x.i)
                          .ToArray();

                            // CSEntities objLS = new CSEntities();


                            int customerId = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => x.CustomerId).FirstOrDefault();

                            string displayItemnumber = _dbcontext.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();


                            var displayItemnumberIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                               .Select((c, i) => new { c, i })
                               .Where(x => x.c.ToString().Equals(displayItemnumber))
                               .Select(x => x.i)
                               .ToArray();

                            var itemNumberIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                             .Select((c, i) => new { c, i })
                             .Where(x => x.c.ToString().Equals("ITEM#"))
                             .Select(x => x.i)
                             .ToArray();


                            if (categoryIndex.Count() > 0 || familyIndex.Count() > 0 || displayItemnumberIndex.Count() > 0 || itemNumberIndex.Count() > 0)
                            {
                                int columnsCount = dt_checkHeaderColumns.Columns.Count;
                                string str_Columnname = AdvanceImportApiController.ExcelColumnFromNumber(columnsCount);
                                query = "select * from [" + SheetName + "$A2:" + str_Columnname + "65000]";
                            }

                        }



                        if (!sheet.EndsWith("_"))
                        {
                            OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                            dtexcel.Locale = CultureInfo.CurrentCulture;



                            daexcel.Fill(dtColumns);

                        }
                        conn.Close();
                        conn.Dispose();
                        var workbook1 = new Workbook();

                        workbook1 = Workbook.Load(excelPath);


                        for (int i = 0; i < workbook1.Worksheets.Count; i++)
                        {
                            if (workbook1.Worksheets[i].Name.ToUpper() == SheetName.ToUpper())
                            {
                                selectedIndex = i;

                            }
                        }


                        //foreach (WorksheetColumn colSheet in workbook1.Worksheets[1].Columns)
                        //    dt11.Columns.Add(colSheet.ToString());
                        dtexcel = dtColumns.Clone();
                        bool skipFirstRow = false;



                        if (!deleteflag.ToUpper().Contains("DELETE"))
                        {

                            foreach (WorksheetRow rowSheet in workbook1.Worksheets[selectedIndex].Rows)
                            {
                                if (dtColumns.Columns.Contains("ACTION") && !skipFirstRow)
                                {
                                    skipFirstRow = true;
                                    continue;
                                }
                                DataRow dRow = dtexcel.NewRow();

                                for (int col = 0; col < dtexcel.Columns.Count; col++)
                                {

                                    if (rowSheet.Cells[col].Value == null)
                                    {
                                        dRow[col] = DBNull.Value;
                                    }

                                    else
                                    {
                                        dRow[col] = rowSheet.Cells[col].Value.ToString();
                                    }


                                }
                                //new
                                if (dRow.ItemArray[0].ToString() != "Action")
                                {

                                    dtexcel.Rows.Add(dRow);
                                }
                            }
                        }
                        else
                        {
                            dtexcel = new DataTable();
                            dtexcel = dtColumns;
                            //CSEntities objLS = new CSEntities();
                            // objLS = new CSEntities();
                        }
                        workbook1.Worksheets.Clear();
                    }
                    catch (Exception ex)
                    {
                        dtexcel = new DataTable();
                        dtexcel = dtColumns;
                    }
                }

                if (dtexcel.Columns[0].ColumnName.ToString().ToUpper() == "ACTION" && deleteflag.ToUpper() == "VALID")
                {
                    dtexcelNew = dtexcel;
                    goto Skip;
                }
                if ((dtexcel == null || dtexcel.Rows.Count == 0 || dtexcel.Rows.Count == 1) && deleteflag.ToUpper() != "SKIP" && deleteflag.ToUpper() != "DELETE")
                {
                    return dtexcel;
                }

                dtexcelNew = SetDataTableForDelete(dtexcel, deleteflag);
                Skip:
                CSEntities objLS = new CSEntities();
                var tbattrcol = objLS.TB_ATTRIBUTE.Find(1);
                Workbook workbook;
                try
                {
                    workbook = Workbook.Load(excelPath);
                    string sheetNameworkbook = SheetName.Replace("$", "").TrimEnd();
                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells.Count();
                    if (deleteflag == "Valid")
                    {
                        for (int i = 0; i < dtexcelNew.Columns.Count; i++)
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value != null)
                            {
                                String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i].ColumnName != sColName && sColName.ToUpper() != "ACTION" && !dtexcelNew.Columns.Contains(sColName))
                                {
                                    dtexcelNew.Columns[i].ColumnName = sColName;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= dtexcelNew.Columns.Count; i++)
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value != null)
                            {
                                String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i - 1].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i - 1].ColumnName != sColName && !dtexcelNew.Columns.Contains(sColName))
                                {
                                    dtexcelNew.Columns[i - 1].ColumnName = sColName;
                                }
                            }
                        }
                    }
                }
                catch
                {
                    if (deleteflag == "Valid")
                    {
                        for (int i = 0; i < dtexcelNew.Columns.Count; i++)
                        {
                            if (dtexcel.Rows[0][i].ToString() != null)
                            {
                                String sColName = dtexcel.Rows[0][i].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i].ColumnName != sColName && sColName.ToUpper() != "ACTION")
                                {
                                    dtexcelNew.Columns[i].ColumnName = sColName;
                                }
                            }
                        }
                    }
                    else
                    {
                        var validCheck = SheetName.ToUpper().Contains("FAM") ? dtexcelNew.Columns.Contains("FAMILY_ID") : dtexcelNew.Columns.Contains("PRODUCT_ID");
                        if (!validCheck)
                        {
                            for (int i = 1; i <= dtexcelNew.Columns.Count; i++)
                            {
                                if (dtexcel.Rows[0][i].ToString() != null)
                                {
                                    String sColName = dtexcel.Rows[0][i].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                    if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                    {
                                        dtexcelNew.Columns[i - 1].ColumnName = "CATALOG_ITEM_NO";
                                    }
                                    else if (dtexcelNew.Columns[i - 1].ColumnName != sColName)
                                    {
                                        dtexcelNew.Columns[i - 1].ColumnName = sColName;
                                    }
                                }
                            }
                        }
                    }
                }
                if (dtexcelNew.Columns.Count > 0 && dtexcelNew.Columns[0].ColumnName.ToString().ToUpper() == "ACTION" && deleteflag.ToUpper() != "VALID")
                {
                    dtexcelNew.Columns.Remove(dtexcelNew.Columns[0].ColumnName);
                }
                dtexcelNew.AcceptChanges();
            }
            catch (Exception objException)
            {
                Logger.Error("Error at loading GetDataFromExcel in AdvanceImportApiController", objException);
                return null;
            }
            return dtexcelNew;
        }

        public DataTable SetDataTableForDelete(DataTable oldSheet, string deleteflag)
        {
            DataTable newSheet = new DataTable();
            try
            {

                if (oldSheet.Columns[0].ColumnName.ToString().ToUpper() == "ACTION")
                {
                    foreach (DataColumn dcol in oldSheet.Columns)
                    {
                        if (dcol.ColumnName.ToString().ToUpper() != "ACTION" && !newSheet.Columns.Contains(dcol.ColumnName))
                            newSheet.Columns.Add(dcol.ColumnName);
                    }
                    foreach (DataRow dtrow in oldSheet.Rows)
                    {
                        DataRow drow = newSheet.NewRow();
                        if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && deleteflag == "")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (deleteflag == "Valid")
                        {
                            for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count - 1 < exl)
                                {
                                    break;
                                }
                                drow[exl] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() == "DELETE" && deleteflag == "DELETE")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() == "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && dtrow[0].ToString().ToUpper() != "" && deleteflag == "SKIP")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                    }
                    return newSheet;
                }
                foreach (DataRow dtrow in oldSheet.Rows)
                {
                    if (dtrow.ItemArray[0].ToString().ToUpper() == "ACTION")
                    {
                        foreach (var colName in dtrow.ItemArray)
                        {
                            if (colName.ToString().ToUpper() != "ACTION" && !newSheet.Columns.Contains(colName.ToString()) && !string.IsNullOrEmpty(colName.ToString()))
                                newSheet.Columns.Add(colName.ToString());
                            else if (deleteflag == "Valid" && !string.IsNullOrEmpty(colName.ToString()))
                                newSheet.Columns.Add(colName.ToString());
                        }

                    }
                    else
                    {
                        DataRow drow = newSheet.NewRow();

                        if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && deleteflag == "")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (deleteflag == "Valid")
                        {
                            for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count - 1 < exl)
                                {
                                    break;
                                }
                                drow[exl] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() == "DELETE" && deleteflag == "DELETE")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() == "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && dtrow[0].ToString().ToUpper() != "" && deleteflag == "SKIP")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                    }
                }

            }
            catch (Exception objException)
            {
                Logger.Error("Error at loading SetDataTable in AdvanceImportApiController", objException);
                return null;
            }
            return newSheet;
        }

        //--------------------------------------------------DELETE PROCESS----------------------------------------------
        //--------------------------------------------------DETACH PROCESS----------------------------------------------

        public DataTable GetDataFromExcelForDetach(string SheetName, string excelPath, string query, string deleteflag)
        {
            DataTable dtexcel = new DataTable();
            DataTable dtexcelNew = new DataTable();
            try
            {
                var fileName = Path.GetFileName(excelPath).Split('.')[0];
                var path = Path.GetFullPath(excelPath);
                var fileExtention = Path.GetExtension(excelPath);
                string excelPathCopy = path.Replace(Path.GetFileName(excelPath), "") + "//" + fileName + "_Copy" + "." + fileExtention;
                using (OleDbConnection conn = excelConnection(excelPath))
                {

                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    DataTable dtColumns = new DataTable();
                    try
                    {
                        DataRow[] schemaRow = schemaTable.Select("TABLE_NAME like '" + SheetName + "%'");
                        string sheet = schemaRow[0]["TABLE_NAME"].ToString();
                        int selectedIndex = 0;
                        foreach (DataRow dRow in schemaTable.Rows)
                        {

                            if (sheet.ToUpper() == dRow["TABLE_NAME"].ToString().ToUpper())
                                break;
                            selectedIndex++;
                        }

                        DataTable dt_checkHeaderColumns = new DataTable();
                        if (!sheet.EndsWith("_"))
                        {
                            OleDbDataAdapter da_checkHeaderexcel = new OleDbDataAdapter(query, conn);
                            dt_checkHeaderColumns.Locale = CultureInfo.CurrentCulture;


                            da_checkHeaderexcel.Fill(dt_checkHeaderColumns);

                        }




                        if (dt_checkHeaderColumns.Rows.Count > 0)
                        {

                            var categoryIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                           .Select((c, i) => new { c, i })
                           .Where(x => x.c.ToString().Equals("CATEGORY_NAME"))
                           .Select(x => x.i)
                           .ToArray();


                            var familyIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                          .Select((c, i) => new { c, i })
                          .Where(x => x.c.ToString().Equals("FAMILY_NAME"))
                          .Select(x => x.i)
                          .ToArray();

                            // CSEntities objLS = new CSEntities();


                            int customerId = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => x.CustomerId).FirstOrDefault();

                            string displayItemnumber = _dbcontext.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();


                            var displayItemnumberIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                               .Select((c, i) => new { c, i })
                               .Where(x => x.c.ToString().Equals(displayItemnumber))
                               .Select(x => x.i)
                               .ToArray();

                            var itemNumberIndex = dt_checkHeaderColumns.Rows[0].ItemArray
                             .Select((c, i) => new { c, i })
                             .Where(x => x.c.ToString().Equals("ITEM#"))
                             .Select(x => x.i)
                             .ToArray();


                            if (categoryIndex.Count() > 0 || familyIndex.Count() > 0 || displayItemnumberIndex.Count() > 0 || itemNumberIndex.Count() > 0)
                            {
                                int columnsCount = dt_checkHeaderColumns.Columns.Count;
                                string str_Columnname = AdvanceImportApiController.ExcelColumnFromNumber(columnsCount);
                                query = "select * from [" + SheetName + "$A2:" + str_Columnname + "65000]";
                            }

                        }



                        if (!sheet.EndsWith("_"))
                        {
                            OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                            dtexcel.Locale = CultureInfo.CurrentCulture;



                            daexcel.Fill(dtColumns);

                        }
                        conn.Close();
                        conn.Dispose();
                        var workbook1 = new Workbook();

                        workbook1 = Workbook.Load(excelPath);


                        for (int i = 0; i < workbook1.Worksheets.Count; i++)
                        {
                            if (workbook1.Worksheets[i].Name.ToUpper() == SheetName.ToUpper())
                            {
                                selectedIndex = i;

                            }
                        }


                        //foreach (WorksheetColumn colSheet in workbook1.Worksheets[1].Columns)
                        //    dt11.Columns.Add(colSheet.ToString());
                        dtexcel = dtColumns.Clone();
                        bool skipFirstRow = false;



                        if (!deleteflag.ToUpper().Contains("DETACH"))
                        {

                            foreach (WorksheetRow rowSheet in workbook1.Worksheets[selectedIndex].Rows)
                            {
                                if (dtColumns.Columns.Contains("ACTION") && !skipFirstRow)
                                {
                                    skipFirstRow = true;
                                    continue;
                                }
                                DataRow dRow = dtexcel.NewRow();

                                for (int col = 0; col < dtexcel.Columns.Count; col++)
                                {

                                    if (rowSheet.Cells[col].Value == null)
                                    {
                                        dRow[col] = DBNull.Value;
                                    }

                                    else
                                    {
                                        dRow[col] = rowSheet.Cells[col].Value.ToString();
                                    }


                                }
                                //new
                                if (dRow.ItemArray[0].ToString() != "Action")
                                {

                                    dtexcel.Rows.Add(dRow);
                                }
                            }
                        }
                        else
                        {
                            dtexcel = new DataTable();
                            dtexcel = dtColumns;
                            //CSEntities objLS = new CSEntities();
                            // objLS = new CSEntities();
                        }
                        workbook1.Worksheets.Clear();
                    }
                    catch (Exception ex)
                    {
                        dtexcel = new DataTable();
                        dtexcel = dtColumns;
                    }
                }

                if (dtexcel.Columns[0].ColumnName.ToString().ToUpper() == "ACTION" && deleteflag.ToUpper() == "VALID")
                {
                    dtexcelNew = dtexcel;
                    goto Skip;
                }
                if ((dtexcel == null || dtexcel.Rows.Count == 0 || dtexcel.Rows.Count == 1) && deleteflag.ToUpper() != "SKIP" && deleteflag.ToUpper() != "DETACH")
                {
                    return dtexcel;
                }

                dtexcelNew = SetDataTableForDetach(dtexcel, deleteflag);
                Skip:
                CSEntities objLS = new CSEntities();
                var tbattrcol = objLS.TB_ATTRIBUTE.Find(1);
                Workbook workbook;
                try
                {
                    workbook = Workbook.Load(excelPath);
                    string sheetNameworkbook = SheetName.Replace("$", "").TrimEnd();
                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells.Count();
                    if (deleteflag == "Valid")
                    {
                        for (int i = 0; i < dtexcelNew.Columns.Count; i++)
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value != null)
                            {
                                String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i].ColumnName != sColName && sColName.ToUpper() != "ACTION" && !dtexcelNew.Columns.Contains(sColName))
                                {
                                    dtexcelNew.Columns[i].ColumnName = sColName;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= dtexcelNew.Columns.Count; i++)
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value != null)
                            {
                                String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i - 1].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i - 1].ColumnName != sColName && !dtexcelNew.Columns.Contains(sColName))
                                {
                                    dtexcelNew.Columns[i - 1].ColumnName = sColName;
                                }
                            }
                        }
                    }
                }
                catch
                {
                    if (deleteflag == "Valid")
                    {
                        for (int i = 0; i < dtexcelNew.Columns.Count; i++)
                        {
                            if (dtexcel.Rows[0][i].ToString() != null)
                            {
                                String sColName = dtexcel.Rows[0][i].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i].ColumnName != sColName && sColName.ToUpper() != "ACTION")
                                {
                                    dtexcelNew.Columns[i].ColumnName = sColName;
                                }
                            }
                        }
                    }
                    else
                    {
                        var validCheck = SheetName.ToUpper().Contains("FAM") ? dtexcelNew.Columns.Contains("FAMILY_ID") : dtexcelNew.Columns.Contains("PRODUCT_ID");
                        if (!validCheck)
                        {
                            for (int i = 1; i <= dtexcelNew.Columns.Count; i++)
                            {
                                if (dtexcel.Rows[0][i].ToString() != null)
                                {
                                    String sColName = dtexcel.Rows[0][i].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                    if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                    {
                                        dtexcelNew.Columns[i - 1].ColumnName = "CATALOG_ITEM_NO";
                                    }
                                    else if (dtexcelNew.Columns[i - 1].ColumnName != sColName)
                                    {
                                        dtexcelNew.Columns[i - 1].ColumnName = sColName;
                                    }
                                }
                            }
                        }
                    }
                }
                if (dtexcelNew.Columns.Count > 0 && dtexcelNew.Columns[0].ColumnName.ToString().ToUpper() == "ACTION" && deleteflag.ToUpper() != "VALID")
                {
                    dtexcelNew.Columns.Remove(dtexcelNew.Columns[0].ColumnName);
                }
                dtexcelNew.AcceptChanges();
            }
            catch (Exception objException)
            {
                Logger.Error("Error at loading GetDataFromExcel in AdvanceImportApiController", objException);
                return null;
            }
            return dtexcelNew;
        }

        public DataTable SetDataTableForDetach(DataTable oldSheet, string deleteflag)
        {
            DataTable newSheet = new DataTable();
            try
            {

                if (oldSheet.Columns[0].ColumnName.ToString().ToUpper() == "ACTION")
                {
                    foreach (DataColumn dcol in oldSheet.Columns)
                    {
                        if (dcol.ColumnName.ToString().ToUpper() != "ACTION" && !newSheet.Columns.Contains(dcol.ColumnName))
                            newSheet.Columns.Add(dcol.ColumnName);
                    }
                    foreach (DataRow dtrow in oldSheet.Rows)
                    {
                        DataRow drow = newSheet.NewRow();
                        if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() != "DETACH" && deleteflag == "")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (deleteflag == "Valid")
                        {
                            for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count - 1 < exl)
                                {
                                    break;
                                }
                                drow[exl] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() == "DETACH" && deleteflag == "DETACH")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() == "SKIP" && dtrow[0].ToString().ToUpper() != "DETACH" && dtrow[0].ToString().ToUpper() != "" && deleteflag == "SKIP")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                    }
                    return newSheet;
                }
                foreach (DataRow dtrow in oldSheet.Rows)
                {
                    if (dtrow.ItemArray[0].ToString().ToUpper() == "ACTION")
                    {
                        foreach (var colName in dtrow.ItemArray)
                        {
                            if (colName.ToString().ToUpper() != "ACTION" && !newSheet.Columns.Contains(colName.ToString()) && !string.IsNullOrEmpty(colName.ToString()))
                                newSheet.Columns.Add(colName.ToString());
                            else if (deleteflag == "Valid" && !string.IsNullOrEmpty(colName.ToString()))
                                newSheet.Columns.Add(colName.ToString());
                        }

                    }
                    else
                    {
                        DataRow drow = newSheet.NewRow();

                        if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() != "DETACH" && deleteflag == "")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (deleteflag == "Valid")
                        {
                            for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count - 1 < exl)
                                {
                                    break;
                                }
                                drow[exl] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() == "DETACH" && deleteflag == "DETACH")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() == "SKIP" && dtrow[0].ToString().ToUpper() != "DETACH" && dtrow[0].ToString().ToUpper() != "" && deleteflag == "SKIP")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                    }
                }

            }
            catch (Exception objException)
            {
                Logger.Error("Error at loading SetDataTable in AdvanceImportApiController", objException);
                return null;
            }
            return newSheet;
        }

        public OleDbConnection excelConnection(string excelPath)
        {

            bool hasHeaders = true;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                          ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            else
                //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                         ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            return conn;
        }

        public string DetachRecords(string SheetName, string allowDuplicate, string excelPath, string importType, int catalogId, JArray model, string importTemp)
        {
            string SQLString = string.Empty;
            //importTemp = Guid.NewGuid().ToString();
            List<object> newList = new List<object>();
            DataSet dsGetProdCnt = new DataSet();
            int importProductCount = 0;
            int userProductCount = 0;
            int userSKUProductCount = 0;
            try
            {
                var customerId = 0;
                var skucnt = objLS.TB_PLAN
                   .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                   .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                var SKUcount = skucnt.Select(a => a).ToList();
                customerId = SKUcount[0].CustomerId;

                DataTable dtGetProdCnt = GetDataFromExcelForDetach(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DETACH");
                if (dtGetProdCnt == null || dtGetProdCnt.Rows.Count == 0)
                {
                    return "0";
                }

                //importExcelSheetSelection(excelPath, SheetName);
                DataTable detachRecords = GetDataFromExcelForDetach(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "DETACH");
                DataTable excelData = new DataTable();
                ImportApiController importApiController = new ImportApiController();

                if (importType.ToUpper().Contains("SUBPRODUCT") && detachRecords.Columns.Contains("SUBITEM#"))
                {
                    detachRecords.Columns["SUBITEM#"].ColumnName = "SUBCATALOG_ITEM_NO";
                }
                if (importType.ToUpper().Contains("SUBPRODUCT"))
                {
                }
                else
                {


                    var customerDetails = objLS.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();

                    //With Hierarchy & ID
                    if (detachRecords.Columns.Contains("PRODUCT_ID") && detachRecords.Columns.Contains("FAMILY_ID") && detachRecords.Columns.Contains("CATALOG_ID"))
                    {

                        if (detachRecords.Columns.Contains(customerDetails))
                        {
                            detachRecords.Columns[customerDetails].ColumnName = "CATALOG_ITEM_NO";
                        }
                        excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "CATALOG_ID", "FAMILY_ID", "PRODUCT_ID");
                        excelData.Columns.Add("REMOVE_ASSOCIATE");
                        excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_ASSOCIATE"] = "YES");
                    }
                    //With Hi,without ID
                    else if (!(detachRecords.Columns.Contains("PRODUCT_ID") && detachRecords.Columns.Contains("FAMILY_ID") && detachRecords.Columns.Contains("CATALOG_ID")) && (detachRecords.Columns.Contains("CATALOG_NAME") && detachRecords.Columns.Contains("FAMILY_NAME")))
                    {

                        if (detachRecords.Columns.Contains(customerDetails))
                        {
                            detachRecords.Columns[customerDetails].ColumnName = "CATALOG_ITEM_NO";
                        }
                        if (detachRecords.Columns.Contains("SUBCATNAME_L6"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_NAME", "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1", "SUBCATNAME_L2", "SUBCATNAME_L3", "SUBCATNAME_L4", "SUBCATNAME_L5", "SUBCATNAME_L6");
                        }
                        else if (detachRecords.Columns.Contains("SUBCATNAME_L5"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_NAME", "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1", "SUBCATNAME_L2", "SUBCATNAME_L3", "SUBCATNAME_L4", "SUBCATNAME_L5");
                        }
                        else if (detachRecords.Columns.Contains("SUBCATNAME_L4"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_NAME", "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1", "SUBCATNAME_L2", "SUBCATNAME_L3", "SUBCATNAME_L4");
                        }
                        else if (detachRecords.Columns.Contains("SUBCATNAME_L3"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_NAME", "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1", "SUBCATNAME_L2", "SUBCATNAME_L3");
                        }
                        else if (detachRecords.Columns.Contains("SUBCATNAME_L2"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_NAME", "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1", "SUBCATNAME_L2");
                        }
                        else if (detachRecords.Columns.Contains("SUBCATNAME_L1"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_NAME", "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME", "SUBCATNAME_L1");
                        }
                        else if (detachRecords.Columns.Contains("CATEGORY_NAME"))
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_NAME", "CATALOG_ITEM_NO", "FAMILY_NAME", "CATEGORY_NAME");
                        }


                        excelData.Columns.Add("CATALOG_ID");
                        excelData.Columns.Add("PRODUCT_ID");
                        excelData.Columns.Add("FAMILY_ID");
                        excelData.Columns.Add("REMOVE_ASSOCIATE");
                        excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_ASSOCIATE"] = "YES");
                        foreach (DataRow dr in excelData.Rows)
                        {

                            string catname = dr["CATALOG_NAME"].ToString();
                            var catalog_id = _dbcontext.TB_CATALOG.Where(k => k.CATALOG_NAME.Equals(catname)).Select(m => m.CATALOG_ID).FirstOrDefault().ToString();
                            catalogId = Int16.Parse(catalog_id);
                            dr["CATALOG_ID"] = catalogId;
                            string ITEM = dr["CATALOG_ITEM_NO"].ToString();
                            string Famname = dr["FAMILY_NAME"].ToString();
                            string Categoryname = "";
                            if (excelData.Columns.Contains("SUBCATNAME_L6") && (!(dr["SUBCATNAME_L6"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L4"].ToString();
                            }
                            else if (excelData.Columns.Contains("SUBCATNAME_L5") && (!(dr["SUBCATNAME_L5"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L4"].ToString();
                            }
                            else if (excelData.Columns.Contains("SUBCATNAME_L4") && (!(dr["SUBCATNAME_L4"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L4"].ToString();
                            }
                            else if (excelData.Columns.Contains("SUBCATNAME_L3") && (!(dr["SUBCATNAME_L3"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L3"].ToString();
                            }
                            else if (excelData.Columns.Contains("SUBCATNAME_L2") && (!(dr["SUBCATNAME_L2"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L2"].ToString();
                            }
                            else if (excelData.Columns.Contains("SUBCATNAME_L1") && (!(dr["SUBCATNAME_L1"].ToString() == "")))
                            {
                                Categoryname = dr["SUBCATNAME_L1"].ToString();
                            }
                            else if (excelData.Columns.Contains("CATEGORY_NAME") && (!(dr["CATEGORY_NAME"].ToString() == "")))
                            {
                                Categoryname = dr["CATEGORY_NAME"].ToString();
                            }
                            DataTable DETACHIMPORT = new DataTable();
                            string sqlConn = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                            using (var objSqlConnection = new SqlConnection(sqlConn))
                            {
                                objSqlConnection.Open();
                                var objSqlDataAdapter = new SqlDataAdapter("SELECT TPF.FAMILY_ID,TF.FAMILY_NAME,TPF.PRODUCT_ID,TPS.STRING_VALUE,TC.CATEGORY_ID,TC.CATEGORY_NAME    FROM TB_PROD_SPECS TPS JOIN TB_PROD_FAMILY TPF ON TPF.PRODUCT_ID = TPS.PRODUCT_ID JOIN TB_CATALOG_FAMILY TCF ON TCF.FAMILY_ID = TPF.FAMILY_ID AND TCF.CATALOG_ID = " + catalogId + " JOIN TB_FAMILY TF ON TF.FAMILY_ID = TPF.FAMILY_ID AND TF.FAMILY_NAME = '" + Famname + "' JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TF.CATEGORY_ID AND TC.CATEGORY_NAME = '" + Categoryname + "' JOIN TB_CATALOG_SECTIONS TCS ON TCS.CATEGORY_ID = TC.CATEGORY_ID AND TCS.CATALOG_ID = " + catalogId + "  WHERE TPS.STRING_VALUE = '" + ITEM + "' AND TPS.ATTRIBUTE_ID = 1 ", objSqlConnection);
                                objSqlDataAdapter.Fill(DETACHIMPORT);
                                objSqlConnection.Close();
                            }



                            dr["PRODUCT_ID"] = DETACHIMPORT.Rows[0].ItemArray[2].ToString();
                            dr["FAMILY_ID"] = DETACHIMPORT.Rows[0].ItemArray[0].ToString();
                        }
                        excelData.Columns.Remove("CATEGORY_NAME");
                    }
                    else
                    {
                        if (detachRecords.Columns.Contains(customerDetails))
                        {
                            detachRecords.Columns[customerDetails].ColumnName = "CATALOG_ITEM_NO";

                        }
                        //Without Hi,with productID
                        if (detachRecords.Columns.Contains("PRODUCT_ID"))
                        {
                            detachRecords.Columns[customerDetails].ColumnName = "CATALOG_ITEM_NO";
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO", "PRODUCT_ID");
                            excelData.Columns.Add("CATALOG_ID");
                            excelData.Columns.Add("REMOVE_ASSOCIATE");
                            excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_ASSOCIATE"] = "YES");
                            foreach (DataRow dr in excelData.Rows)
                            {
                                dr["CATALOG_ID"] = catalogId;
                            }
                        }
                        //Without Hi,ID
                        else
                        {
                            excelData = detachRecords.DefaultView.ToTable(false, "CATALOG_ITEM_NO");
                            excelData.Columns.Add("PRODUCT_ID");
                            excelData.Columns.Add("CATALOG_ID");
                            excelData.Columns.Add("REMOVE_ASSOCIATE");
                            excelData.Select("CATALOG_ITEM_NO<>''").ToList<DataRow>().ForEach(x => x["REMOVE_ASSOCIATE"] = "YES");
                            foreach (DataRow dr in excelData.Rows)
                            {
                                dr["CATALOG_ID"] = catalogId;
                                string ITEM = dr["CATALOG_ITEM_NO"].ToString();
                                var prodid = _dbcontext.TB_PROD_SPECS.Join(_dbcontext.TB_CATALOG_PRODUCT, ta => ta.PRODUCT_ID, ca => ca.PRODUCT_ID, (ta, ca) => new { ta, ca }).Where(x => x.ta.STRING_VALUE.Equals(ITEM) && x.ta.ATTRIBUTE_ID == 1 && x.ca.CATALOG_ID == catalogId).Select(z => z.ta.PRODUCT_ID).FirstOrDefault().ToString();
                                dr["PRODUCT_ID"] = prodid;

                            }
                        }


                    }


                }





                DataTable oattType = new DataTable();
                DataSet replace = new DataSet();

                int ItemVal = Convert.ToInt32(allowDuplicate);
                if (detachRecords.Rows.Count == 0)
                {
                    return detachRecords.Rows.Count.ToString();
                }
                using (var conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    SQLString =
                  "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" +
                  importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                    _DBCommand.ExecuteNonQuery();
                    SQLString = _ic.CreateTable("[##IMPORTTEMP" + importTemp + "]", excelPath, SheetName, excelData);
                    SqlCommand _DBCommandnew = new SqlCommand(SQLString, conn);
                    _DBCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(conn)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(excelData);

                    oattType = _ic.SelectedColumnsToImport(detachRecords, model);
                    var cmd1 =
                        new SqlCommand(
                            "ALTER TABLE [##IMPORTTEMP" + importTemp +
                            "] ADD STATUS nvarchar(20)",
                            conn);
                    cmd1.ExecuteNonQuery();




                    var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                    cmd.ExecuteNonQuery();
                    _ic._SQLString = @"select * from [##LOGTEMPTABLE" + importTemp + "]";
                    DataSet ds = _ic.CreateDataSet();
                    _ic._SQLString = @"select * into [tempresultDetach" + importTemp + "] from [##LOGTEMPTABLE" + importTemp +
                                    "]";
                    cmd1.CommandText = _ic._SQLString;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.ExecuteNonQuery();
                    var altrtable = new SqlCommand("EXEC('ALTER TABLE  [tempresult" + importTemp + "] ALTER COLUMN [STATUS] VARCHAR( 10)')", conn);
                    altrtable.ExecuteNonQuery();
                    var cmbpk41 = new SqlCommand("  exec ('UPDATE [tempresult" + importTemp + "] SET  STATUS =''DETACHED''WHERE LEFT(product_details+''('', CHARINDEX(''('', product_details) - 1) IN (SELECT CATALOG_ITEM_NO    FROM [##LOGTEMPTABLE" + importTemp + "] ) ')", conn);
                    cmbpk41.ExecuteNonQuery();
                    return detachRecords.Rows.Count.ToString();
                }

            }
            catch (Exception ex)
            {

                Logger.Error("Error at AdvanceImportApiController : FinishProductImport", ex);
                return null;
            }
        }


        //--------------------------------------------------DETACH PROCESS----------------------------------------------
        //--------------------------------------------------FAMILY IMPORT PROCESS--------------------------------------------------------------------------------

        public void DeleteSubProductPermanentFromMaster(string familyId, int catalogId, string categoryId, int productId)
        {
            try
            {
                var parentproducts = _dbcontext.TB_SUBPRODUCT.Where(x => x.SUBPRODUCT_ID == productId).ToList();
                string catalogIds = Convert.ToString(catalogId);
                string productIds = Convert.ToString(productId);
                var returnValue = new ObjectParameter("REFINT", typeof(Int32));
                RefID = Convert.ToString(productId);
                _recycleDSPerma = xmlcreate(Convert.ToInt32(catalogId), categoryId, familyId, RefID.ToString(), "",
                    "Product");
                _dbcontext.STP_CATALOGSTUDIO5_Remove("0", categoryId, familyId, productIds, "0", "Product", "", 0, 1,
                    returnValue);
                foreach (var mainproductid in parentproducts)
                {
                    using (
                        var objSqlConnection =
                            new SqlConnection(
                                ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        var cmd = new SqlCommand();
                        objSqlConnection.Open();
                        cmd.CommandText = "STP_UPDATESUBPRODUCT_SORT";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Connection = objSqlConnection;
                        cmd.Parameters.Add("@PRODUCT_ID", SqlDbType.Int);
                        cmd.Parameters["@PRODUCT_ID"].Value = mainproductid.PRODUCT_ID;
                        cmd.ExecuteNonQuery();
                        objSqlConnection.Close();
                    }
                }
                // return Request.CreateResponse(HttpStatusCode.OK, "Success. Sub Product Deleted.");
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : DeleteSubProductPermanentFromMaster", objexception);
                //return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        public DataSet CreateDataSet()
        {
            DataSet dsReturn = new DataSet();
            using (
                var objSqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlDataAdapter _DBAdapter = new SqlDataAdapter(SqlString, objSqlConnection);
                _DBAdapter.SelectCommand.CommandTimeout = 0;
                _DBAdapter.Fill(dsReturn);
                _DBAdapter.Dispose();
                return dsReturn;
            }
        }
        #region xmlcreate

        ///<summary>
        ///created by Deepan
        ///deleted details will be collected as a single XML document and uploaded into table.
        /// </summary>        
        public DataSet xmlcreate(int _catalogID, string categoryID, string familyID, string productID,
            string attributeID, string RmvType)
        {
            string xml = string.Empty;
            string category_cat = string.Empty;
            string product_cat = string.Empty;
            string familyids = string.Empty;
            string catalog_xml = "<?xml version=\"1.0\" standalone=\"yes\"?><CATALOG_ASSOCIATES>";
            string prdfamassoc = "<?xml version=\"1.0\" standalone=\"yes\"?><PROD_FAMILY>";
            string projectsec = "<?xml version=\"1.0\" standalone=\"yes\"?><PROJECTS>";
            string partskey = "<?xml version=\"1.0\" standalone=\"yes\"?><PARTS_KEY>";
            string subpartskey = "<?xml version=\"1.0\" standalone=\"yes\"?><SUBPARTS_KEY>";
            string prodpubattr = "<?xml version=\"1.0\" standalone=\"yes\"?><PRODUCT_PUBLISHED_ATTR>";
            string fampubattr = "<?xml version=\"1.0\" standalone=\"yes\"?><FAMILY_PUBLISHED_ATTR>";
            int xml_id = 0;
            SqlConnection _SQLCon =
                new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            DataSet recycleDS1 = new DataSet();
            DataSet _dsRmvPmnt = new DataSet();
            DataSet _dsCatlevel = new DataSet();
            SqlCommand cmd = null;
            DataSet ds = new DataSet();
            SqlDataAdapter DataAdapter;
            _SQLCon.Open();
            Random random = new Random();
            xml_id = random.Next(1, 10000);
            if (RmvType == "Category")
            {
                //    string _categoryLevel = " exec STP_CATALOGSTUDIO5_CategoryLevel '" + categoryID + "'";
                //    SqlString = _categoryLevel;
                //    _dsCatlevel = CreateDataSet();
                //    string _sqlCatLevel = "SELECT CATEGORY_ID,PARENT_CATEGORY FROM CATEGORY_FUNCTION( " +
                //                          Convert.ToInt32(_catalogID) + " , N'" + categoryID + "')";
                //    SqlString = _sqlCatLevel;
                //    _dsRmvPmnt = CreateDataSet();
                //    if (_dsRmvPmnt.Tables[0].Rows.Count == 1)
                //    {
                //        category_cat = "'" + categoryID + "'";
                //        string familysql = "SELECT DISTINCT FAMILY_ID FROM TB_CATALOG_FAMILY WHERE CATEGORY_ID='" +
                //                           categoryID + "' AND ROOT_CATEGORY='0'";
                //        SqlCommand Command = new SqlCommand(familysql, _SQLCon);
                //        DataSet familydeleteDS = new DataSet();
                //        SqlDataAdapter familyDA = new SqlDataAdapter(Command);
                //        familyDA.Fill(familydeleteDS);
                //        if (familydeleteDS != null && familydeleteDS.Tables[0].Rows.Count == 0)
                //        {
                //            familysql = "SELECT DISTINCT FAMILY_ID FROM TB_CATALOG_FAMILY WHERE CATEGORY_ID='" + categoryID +
                //                        "' AND ROOT_CATEGORY!='0'";
                //            Command = new SqlCommand(familysql, _SQLCon);
                //            familydeleteDS = new DataSet();
                //            familyDA = new SqlDataAdapter(Command);
                //            familyDA.Fill(familydeleteDS);
                //        }
                //        if (familydeleteDS != null && familydeleteDS.Tables[0].Rows.Count > 0)
                //        {
                //            foreach (DataRow dr in familydeleteDS.Tables[0].Rows)
                //            {
                //                familyids = familyids + ',' + dr[0].ToString();
                //                string productsql =
                //                    "SELECT  CONVERT (NVARCHAR, PRODUCT_ID)  + ',' FROM TB_PROD_FAMILY WHERE FAMILY_ID='" +
                //                    dr[0].ToString() + "' for xml path('')";
                //                Command = new SqlCommand(productsql, _SQLCon);
                //                productID = Convert.ToString(Command.ExecuteScalar());
                //                //DataSet productdeleteDS = new DataSet();
                //                //SqlDataAdapter productDA = new SqlDataAdapter(Command);
                //                //productDA.Fill(productdeleteDS);
                //                //if (productdeleteDS != null && productdeleteDS.Tables[0].Rows.Count > 0)
                //                //{
                //                //    foreach (DataRow drow in productdeleteDS.Tables[0].Rows)
                //                //    {
                //                //        productID = productID + "," + drow[0].ToString();
                //                //    }
                //                //    if (productID.Substring(0, 1) == ",")
                //                //    {
                //                //        productID = productID.Substring(1);
                //                //    }
                //                //    product_cat = product_cat + "," + productID.Substring(0,productID.Length-1);
                //                //}
                //                if (productID.Length > 0)
                //                {
                //                    product_cat = product_cat + "," + productID.Substring(0, productID.Length - 1);
                //                    productID = productID.Substring(0, productID.Length - 1);
                //                }
                //                else
                //                {
                //                    product_cat = product_cat + ",0";
                //                    ;
                //                    productID = "0";
                //                }
                //                DataSet recycleDS = new DataSet();
                //                Command = new SqlCommand("[STP_CATALOGSTUDIO5_RECYCLE_TABLE_INSERT]", _SQLCon);
                //                Command.CommandType = CommandType.StoredProcedure;
                //                Command.Parameters.Add(new SqlParameter("@CATALOG_ID", _catalogID));
                //                Command.Parameters.Add(new SqlParameter("@CATEGORYID", categoryID));
                //                Command.Parameters.Add(new SqlParameter("@FAMILY_ID", dr[0].ToString()));
                //                Command.Parameters.Add(new SqlParameter("@ALLOW_DUPLICATES", "N"));
                //                Command.Parameters.Add(new SqlParameter("@PRODUCTIDS", productID));
                //                DataAdapter = new SqlDataAdapter(Command);
                //                DataAdapter.SelectCommand.CommandTimeout = 0;
                //                DataAdapter.Fill(recycleDS);
                //                xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" +
                //                      recycleDS.Tables[0].Rows[0][0].ToString();
                //                xml = xml.Replace("'", "''");
                //                {
                //                    string sqlstr1 = " DECLARE @CATEGORY_NAME VARCHAR(MAX) DECLARE @CATEGORY_SHORT VARCHAR(MAX) "

                //                                + " SET @CATEGORY_NAME = (SELECT CATEGORY_NAME FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //                                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //                                + " INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,CATEGORY_ID,CATEGORY_NAME,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES"
                //                                + "('" + xml_id + "'," + _catalogID + ",'" + categoryID + "',@CATEGORY_NAME,'" + xml + "', '" + User.Identity.Name + "', @CATEGORY_SHORT)";



                //                    //string sqlstr1 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,CATEGORY_ID,CATEGORY_NAME,XML_DOC) VALUES"
                //                    //                   + "('" + xml_id + "'," + _catalogID + ",'" + categoryID + "',(SELECT CATEGORY_NAME FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "'),'" + xml + "' ) ";
                //                    Command = new SqlCommand(sqlstr1, _SQLCon);
                //                    Command.ExecuteNonQuery();
                //                }

                //                foreach (DataRow drcat in _dsCatlevel.Tables[0].Rows)
                //                {
                //                    string sqlstr1 = "INSERT INTO TB_RECYCLE_CATEGORY_REFERENCE (XML_ID,CAT_ID,LEVEL,CREATED_USER) VALUES"
                //                                     + "('" + xml_id + "','" + drcat["CATEGORY_ID"].ToString() + "','" +
                //                                     drcat["LEVEL"].ToString() + "', '" + User.Identity.Name + "')";
                //                    Command = new SqlCommand(sqlstr1, _SQLCon);
                //                    Command.ExecuteNonQuery();
                //                }
                //                productID = string.Empty;
                //                xml = string.Empty;
                //                recycleDS.Dispose();

                //            }

                //        }
                //        else
                //        {
                //            familyids = familyID;
                //            DataSet recycleDS = new DataSet();
                //            Command = new SqlCommand("[STP_CATALOGSTUDIO5_RECYCLE_TABLE_INSERT]", _SQLCon);
                //            Command.CommandType = CommandType.StoredProcedure;
                //            Command.Parameters.Add(new SqlParameter("@CATALOG_ID", _catalogID));
                //            Command.Parameters.Add(new SqlParameter("@CATEGORYID", categoryID));
                //            Command.Parameters.Add(new SqlParameter("@FAMILY_ID", familyID));
                //            Command.Parameters.Add(new SqlParameter("@ALLOW_DUPLICATES", "N"));
                //            Command.Parameters.Add(new SqlParameter("@PRODUCTIDS", productID));
                //            DataAdapter = new SqlDataAdapter(Command);
                //            DataAdapter.SelectCommand.CommandTimeout = 0;
                //            DataAdapter.Fill(recycleDS);

                //            xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" + recycleDS.Tables[0].Rows[0][0].ToString();
                //            xml = xml.Replace("'", "''");
                //            {
                //                string sqlstr1 = " DECLARE @CATEGORY_NAME VARCHAR(MAX) DECLARE @CATEGORY_SHORT VARCHAR(100)  "
                //                                + " SET @CATEGORY_NAME = (SELECT CATEGORY_NAME FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //                                + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //                                + " INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,CATEGORY_ID,CATEGORY_NAME,XML_DOC,DELETED_USER, CATEGORY_SHORT) VALUES"
                //                                   + "('" + xml_id + "'," + _catalogID + ",'" + categoryID + "',@CATEGORY_NAME,'" + xml + "', '" + User.Identity.Name + "' , @CATEGORY_SHORT) ";
                //                Command = new SqlCommand(sqlstr1, _SQLCon);
                //                Command.ExecuteNonQuery();
                //            }
                //            foreach (DataRow drcat in _dsCatlevel.Tables[0].Rows)
                //            {
                //                string sqlstr1 = "INSERT INTO TB_RECYCLE_CATEGORY_REFERENCE (XML_ID,CAT_ID,LEVEL,CREATED_USER) VALUES"
                //                                 + "('" + xml_id + "','" + drcat["CATEGORY_ID"].ToString() + "','" +
                //                                 drcat["LEVEL"].ToString() + "', '" + User.Identity.Name + "')";
                //                Command = new SqlCommand(sqlstr1, _SQLCon);
                //                Command.ExecuteNonQuery();
                //            }
                //        }
                //    }
                //    else
                //    {
                //        foreach (DataRow _drCategory in _dsRmvPmnt.Tables[0].Rows)
                //        {
                //            category_cat = category_cat + "," + "'" + _drCategory["CATEGORY_ID"].ToString() + "'";
                //            string familysql = "SELECT DISTINCT FAMILY_ID FROM TB_CATALOG_FAMILY WHERE CATEGORY_ID='" +
                //                               _drCategory["CATEGORY_ID"].ToString() + "' AND ROOT_CATEGORY='0'";
                //            SqlCommand Command = new SqlCommand(familysql, _SQLCon);
                //            DataSet familydeleteDS = new DataSet();
                //            SqlDataAdapter familyDA = new SqlDataAdapter(Command);
                //            familyDA.Fill(familydeleteDS);
                //            if (familydeleteDS != null && familydeleteDS.Tables[0].Rows.Count == 0)
                //            {
                //                familysql = "SELECT DISTINCT FAMILY_ID FROM TB_CATALOG_FAMILY WHERE CATEGORY_ID='" +
                //                            _drCategory["CATEGORY_ID"].ToString() + "' AND ROOT_CATEGORY!='0'";
                //                Command = new SqlCommand(familysql, _SQLCon);
                //                familydeleteDS = new DataSet();
                //                familyDA = new SqlDataAdapter(Command);
                //                familyDA.Fill(familydeleteDS);
                //            }
                //            if (familydeleteDS != null && familydeleteDS.Tables[0].Rows.Count > 0)
                //            {
                //                foreach (DataRow dr in familydeleteDS.Tables[0].Rows)
                //                {
                //                    familyids = familyids + ',' + dr[0].ToString();
                //                    string productsql =
                //                        "SELECT  CONVERT (NVARCHAR, PRODUCT_ID)  + ',' FROM TB_PROD_FAMILY WHERE FAMILY_ID='" +
                //                        dr[0].ToString() + "' for xml path('')";
                //                    Command = new SqlCommand(productsql, _SQLCon);
                //                    productID = Convert.ToString(Command.ExecuteScalar());
                //                    //DataSet productdeleteDS = new DataSet();
                //                    //SqlDataAdapter productDA = new SqlDataAdapter(Command);
                //                    //productDA.Fill(productdeleteDS);
                //                    //if (productdeleteDS != null && productdeleteDS.Tables[0].Rows.Count > 0)
                //                    //{
                //                    //    foreach (DataRow drow in productdeleteDS.Tables[0].Rows)
                //                    //    {
                //                    //        productID = productID + "," + drow[0].ToString();
                //                    //    }
                //                    //    if (productID.Substring(0, 1) == ",")
                //                    //    {
                //                    //        productID = productID.Substring(1);
                //                    //    }
                //                    //    product_cat = product_cat + "," + productID;
                //                    //}
                //                    if (productID.Length > 0)
                //                    {
                //                        product_cat = product_cat + "," + productID.Substring(0, productID.Length - 1);
                //                        productID = productID.Substring(0, productID.Length - 1);
                //                    }
                //                    else
                //                    {
                //                        product_cat = product_cat + ",0";
                //                        ;
                //                        productID = "0";
                //                    }
                //                    DataSet recycleDS = new DataSet();
                //                    Command = new SqlCommand("[STP_CATALOGSTUDIO5_RECYCLE_TABLE_INSERT]", _SQLCon);
                //                    Command.CommandType = CommandType.StoredProcedure;
                //                    Command.Parameters.Add(new SqlParameter("@CATALOG_ID", _catalogID));
                //                    Command.Parameters.Add(new SqlParameter("@CATEGORYID",
                //                        _drCategory["CATEGORY_ID"].ToString()));
                //                    Command.Parameters.Add(new SqlParameter("@FAMILY_ID", dr[0].ToString()));
                //                    Command.Parameters.Add(new SqlParameter("@ALLOW_DUPLICATES", "N"));
                //                    Command.Parameters.Add(new SqlParameter("@PRODUCTIDS", productID));
                //                    DataAdapter = new SqlDataAdapter(Command);
                //                    DataAdapter.SelectCommand.CommandTimeout = 0;
                //                    DataAdapter.Fill(recycleDS); // 11980
                //                    xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" +
                //                          recycleDS.Tables[0].Rows[0][0].ToString();
                //                    xml = xml.Replace("'", "''");
                //                    {
                //                        string sqlstr1 = " DECLARE @CATEGORY_NAME VARCHAR(MAX) DECLARE @CATEGORY_SHORT VARCHAR(100)  "

                //                                + " SET @CATEGORY_NAME = (SELECT CATEGORY_NAME FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //                                  + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //                                + " INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,CATEGORY_ID,CATEGORY_NAME,XML_DOC,DELETED_USER, CATEGORY_SHORT) VALUES"
                //                                   + "('" + xml_id + "'," + _catalogID + ",'" + categoryID + "',@CATEGORY_NAME,'" + xml + "' , '" + User.Identity.Name + "'  , @CATEGORY_SHORT) ";
                //                        //string sqlstr1 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,CATEGORY_ID,CATEGORY_NAME,XML_DOC) VALUES"
                //                        //                   + "('" + xml_id + "'," + _catalogID + ",'" + categoryID + "',(SELECT CATEGORY_NAME FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "'),'" + xml + "' ) ";
                //                        Command = new SqlCommand(sqlstr1, _SQLCon);
                //                        Command.ExecuteNonQuery();
                //                    }
                //                    foreach (DataRow drcat in _dsCatlevel.Tables[0].Rows)
                //                    {
                //                        string sqlstr1 = "INSERT INTO TB_RECYCLE_CATEGORY_REFERENCE (XML_ID,CAT_ID,LEVEL,CREATED_USER) VALUES"
                //                                         + "('" + xml_id + "','" + drcat["CATEGORY_ID"].ToString() + "','" +
                //                                         drcat["LEVEL"].ToString() + "', '" + User.Identity.Name + "')";
                //                        Command = new SqlCommand(sqlstr1, _SQLCon);
                //                        Command.ExecuteNonQuery();
                //                    }
                //                    productID = string.Empty;
                //                    xml = string.Empty;
                //                    recycleDS.Dispose();
                //                }
                //            }
                //            else
                //            {
                //                familyids = familyID;
                //                DataSet recycleDS = new DataSet();
                //                Command = new SqlCommand("[STP_CATALOGSTUDIO5_RECYCLE_TABLE_INSERT]", _SQLCon);
                //                Command.CommandType = CommandType.StoredProcedure;
                //                Command.Parameters.Add(new SqlParameter("@CATALOG_ID", _catalogID));
                //                Command.Parameters.Add(new SqlParameter("@CATEGORYID", _drCategory["CATEGORY_ID"].ToString()));
                //                Command.Parameters.Add(new SqlParameter("@FAMILY_ID", familyID));
                //                Command.Parameters.Add(new SqlParameter("@ALLOW_DUPLICATES", "N"));
                //                Command.Parameters.Add(new SqlParameter("@PRODUCTIDS", productID));
                //                DataAdapter = new SqlDataAdapter(Command);
                //                DataAdapter.SelectCommand.CommandTimeout = 0;
                //                DataAdapter.Fill(recycleDS);

                //                xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" +
                //                      recycleDS.Tables[0].Rows[0][0].ToString();
                //                xml = xml.Replace("'", "''");
                //                string sqlstr1 = " DECLARE @CATEGORY_NAME VARCHAR(MAX) DECLARE @CATEGORY_SHORT VARCHAR(100)  "

                //                                + " SET @CATEGORY_NAME = (SELECT CATEGORY_NAME FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //                                    + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //                                + " INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,CATEGORY_ID,CATEGORY_NAME,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES"
                //                                   + "('" + xml_id + "'," + _catalogID + ",'" + categoryID + "',@CATEGORY_NAME,'" + xml + "' , '" + User.Identity.Name + "' , @CATEGORY_SHORT) ";
                //                //string sqlstr1 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,CATEGORY_ID,CATEGORY_NAME,XML_DOC) VALUES"
                //                //                           + "('" + xml_id + "'," + _catalogID + ",'" + categoryID + "',(SELECT CATEGORY_NAME FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "'),'" + xml + "' ) ";
                //                Command = new SqlCommand(sqlstr1, _SQLCon);
                //                Command.ExecuteNonQuery();
                //                //string sqlstr1 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,CATEGORY_ID,CATEGORY_NAME,PARENT_CATEGORY_ID,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,PRODUCT_NAME,XML_DOC) VALUES"
                //                //                            + "('" + xml_id + "'," + _catalogID + ",'" + _drCategory["CATEGORY_ID"].ToString() + "',(SELECT CATEGORY_NAME FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "'),'" + _drCategory["PARENT_CATEGORY"].ToString() + "',"
                //                //                            + " '','','','','" + xml + "' ) ";
                //                //Command = new SqlCommand(sqlstr1, _SQLCon);
                //                //Command.ExecuteNonQuery();
                //                foreach (DataRow drcat in _dsCatlevel.Tables[0].Rows)
                //                {
                //                    string sqlstr2 = "INSERT INTO TB_RECYCLE_CATEGORY_REFERENCE (XML_ID,CAT_ID,LEVEL,CREATED_USER) VALUES"
                //                                     + "('" + xml_id + "','" + drcat["CATEGORY_ID"].ToString() + "','" +
                //                                     drcat["LEVEL"].ToString() + "', '" + User.Identity.Name + "')";
                //                    Command = new SqlCommand(sqlstr2, _SQLCon);
                //                    Command.ExecuteNonQuery();
                //                }
                //            }
                //        }
                //    }




                //    if (category_cat != string.Empty)
                //    {
                //        if (category_cat[0] == ',')
                //        {
                //            category_cat = category_cat.Substring(1);
                //        }
                //        cmd =
                //            new SqlCommand(
                //                "SELECT TCS.CATALOG_ID,TCS.SORT_ORDER,TCS.CATEGORY_ID,TCS.CREATED_USER,TCS.CREATED_DATE, TC.CATEGORY_SHORT, tc.CATEGORY_PARENT_SHORT FROM TB_CATALOG_SECTIONS  TCS JOIN TB_CATEGORY TC ON TC.CATEGORY_ID = TCS.CATEGORY_ID  WHERE TCS.CATALOG_ID!=1 AND TCS.CATEGORY_ID in (" +
                //                category_cat + ") for xml path('CATALOG_SECTIONS'),type", _SQLCon);
                //        SqlDataAdapter da = new SqlDataAdapter(cmd);
                //        da.Fill(ds, "CATALOG_CATEGORY");
                //        if (ds.Tables["CATALOG_CATEGORY"].Rows.Count > 0 &&
                //            ds.Tables["CATALOG_CATEGORY"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["CATALOG_CATEGORY"].Rows[0][0] != null)
                //        {
                //            catalog_xml = catalog_xml + ds.Tables["CATALOG_CATEGORY"].Rows[0][0].ToString();
                //        }
                //        cmd =
                //            new SqlCommand(
                //                "SELECT CATALOG_ID,SORT_ORDER,FAMILY_ID,tcf.CATEGORY_ID,tcf.CREATED_USER,tcf.CREATED_DATE,tcf.ROOT_CATEGORY, tc.CATEGORY_SHORT, tc.CATEGORY_PARENT_SHORT FROM TB_CATALOG_FAMILY tcf JOIN TB_CATEGORY tc ON tc.CATEGORY_ID = tcf.CATEGORY_ID  WHERE FAMILY_ID IN (SELECT FAMILY_ID FROM  TB_CATALOG_FAMILY WHERE CATALOG_ID!=1 AND CATEGORY_ID in (" +
                //                category_cat + ")) AND CATALOG_ID!=1 for xml path('CATALOG_FAMILY'),type", _SQLCon);
                //        SqlDataAdapter dafam = new SqlDataAdapter(cmd);
                //        dafam.Fill(ds, "CATALOG_FAMILY");
                //        if (ds.Tables["CATALOG_FAMILY"].Rows.Count > 0 &&
                //            ds.Tables["CATALOG_FAMILY"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["CATALOG_FAMILY"].Rows[0][0] != null)
                //        {
                //            catalog_xml = catalog_xml + ds.Tables["CATALOG_FAMILY"].Rows[0][0].ToString();
                //        }

                //        cmd =
                //            new SqlCommand(
                //                "SELECT CATALOG_ID,ATTRIBUTE_ID,FAMILY_ID,ATTRIBUTE_VALUE,tcf.CATEGORY_ID, tc.CATEGORY_SHORT, tc.CATEGORY_PARENT_SHORT FROM TB_FAMILY_KEY tcf JOIN TB_CATEGORY tc ON tc.CATEGORY_ID = tcf.CATEGORY_ID WHERE FAMILY_ID in (SELECT FAMILY_ID FROM  TB_CATALOG_FAMILY WHERE CATALOG_ID!=1 AND CATEGORY_ID in (" +
                //                category_cat + ")) for xml path('FAMILY_KEY'),type", _SQLCon);
                //        SqlDataAdapter dafamkey = new SqlDataAdapter(cmd);
                //        dafamkey.Fill(ds, "FAMILY_KEY");
                //        if (ds.Tables["FAMILY_KEY"].Rows.Count > 0 && ds.Tables["FAMILY_KEY"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["FAMILY_KEY"].Rows[0][0] != null)
                //        {
                //            catalog_xml = catalog_xml + ds.Tables["FAMILY_KEY"].Rows[0][0].ToString();
                //        }
                //    }
                //    if (product_cat != string.Empty)
                //    {
                //        if (product_cat[0] == ',')
                //        {
                //            product_cat = product_cat.Substring(1);
                //        }
                //        cmd =
                //            new SqlCommand(
                //                "SELECT CATALOG_ID,cp.PRODUCT_ID,P.CATEGORY_ID,cp.CREATED_USER,cp.CREATED_DATE, tc.CATEGORY_SHORT, tc.CATEGORY_PARENT_SHORT FROM TB_CATALOG_PRODUCT cp join tb_product p on p.product_id = cp.product_id JOIN TB_CATEGORY tc ON tc.CATEGORY_ID = p.CATEGORY_ID  WHERE CATALOG_ID!=1 AND cp.PRODUCT_ID IN(" + product_cat + ") for xml path('CATALOG_PRODUCT'),type", _SQLCon);

                //        SqlDataAdapter daprod = new SqlDataAdapter(cmd);
                //        daprod.Fill(ds, "CATALOG_PRODUCT");
                //        if (ds.Tables["CATALOG_PRODUCT"].Rows.Count > 0 &&
                //            ds.Tables["CATALOG_PRODUCT"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["CATALOG_PRODUCT"].Rows[0][0] != null)
                //        {
                //            for (int i = 0; i < ds.Tables["CATALOG_PRODUCT"].Rows.Count; i++)
                //            {
                //                catalog_xml = catalog_xml + ds.Tables["CATALOG_PRODUCT"].Rows[i][0].ToString();
                //            }
                //        }
                //        cmd =
                //            new SqlCommand(
                //                "SELECT ATTRIBUTE_ID,SORT_ORDER,FAMILY_ID,PRODUCT_ID FROM TB_PROD_FAMILY_ATTR_LIST WHERE PRODUCT_ID IN(" +
                //                product_cat + ") for xml path('PRODUCT_PUBLISH'),type", _SQLCon);
                //        daprod = new SqlDataAdapter(cmd);
                //        daprod.Fill(ds, "PROD_PUB_ATTR");
                //        if (ds.Tables["PROD_PUB_ATTR"].Rows.Count > 0 &&
                //            ds.Tables["PROD_PUB_ATTR"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["CATALOG_PRODUCT"].Rows[0][0] != null)
                //        {
                //            for (int i = 0; i < ds.Tables["PROD_PUB_ATTR"].Rows.Count; i++)
                //            {
                //                prodpubattr = prodpubattr + ds.Tables["PROD_PUB_ATTR"].Rows[i][0].ToString();
                //            }
                //        }
                //        if (prodpubattr != "<?xml version=\"1.0\" standalone=\"yes\"?><PRODUCT_PUBLISHED_ATTR>")
                //        {
                //            prodpubattr = prodpubattr + "</PRODUCT_PUBLISHED_ATTR>";
                //            prodpubattr = prodpubattr.Replace("'", "''");


                //            string sqlstr = "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + "  INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryID + "','" + prodpubattr + "', '" + User.Identity.Name + "' , @CATEGORY_SHORT) ";
                //            cmd = new SqlCommand(sqlstr, _SQLCon);
                //            cmd.ExecuteNonQuery();
                //        }

                //        cmd =
                //            new SqlCommand(
                //                "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + "  SELECT *, @CATEGORY_SHORT as CATEGORY_SHORT FROM TB_PARTS_KEY WHERE PRODUCT_ID IN (" + product_cat +
                //                ") for xml path('PARTSKEY')", _SQLCon);
                //        SqlDataAdapter daparts = new SqlDataAdapter(cmd);
                //        daparts.Fill(ds, "PARTS_KEY");
                //        if (ds.Tables["PARTS_KEY"].Rows.Count > 0 && ds.Tables["PARTS_KEY"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["PARTS_KEY"].Rows[0][0] != null)
                //        {
                //            for (int i = 0; i < ds.Tables["PARTS_KEY"].Rows.Count; i++)
                //            {
                //                partskey = partskey + ds.Tables["PARTS_KEY"].Rows[i][0].ToString();
                //            }
                //        }
                //        if (partskey != string.Empty && partskey != null)
                //        {
                //            if (partskey != "<?xml version=\"1.0\" standalone=\"yes\"?><PARTS_KEY>")
                //            {
                //                partskey = partskey + "</PARTS_KEY>";
                //                partskey = partskey.Replace("'", "''");


                //                string sqlstr11 = "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //                 + "  INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryID + "','" + partskey + "' , '" + User.Identity.Name + "'  , @CATEGORY_SHORT) ";
                //                cmd = new SqlCommand(sqlstr11, _SQLCon);
                //                cmd.ExecuteNonQuery();
                //            }
                //        }

                //        /****************** Sub Product key added by Victor  ******************/

                //        cmd =
                //           new SqlCommand(
                //               "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //           + "  SELECT *, @CATEGORY_SHORT as CATEGORY_SHORT FROM TB_SUBPRODUCT_KEY WHERE PRODUCT_ID IN (" + product_cat +
                //               ") for xml path('SUBPARTSKEY')", _SQLCon);
                //        SqlDataAdapter dasubparts = new SqlDataAdapter(cmd);
                //        dasubparts.Fill(ds, "SUBPARTS_KEY");
                //        if (ds.Tables["SUBPARTS_KEY"].Rows.Count > 0 && ds.Tables["SUBPARTS_KEY"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["SUBPARTS_KEY"].Rows[0][0] != null)
                //        {
                //            for (int i = 0; i < ds.Tables["SUBPARTS_KEY"].Rows.Count; i++)
                //            {
                //                subpartskey = subpartskey + ds.Tables["SUBPARTS_KEY"].Rows[i][0].ToString();
                //            }
                //        }
                //        if (subpartskey != string.Empty && subpartskey != null)
                //        {
                //            if (subpartskey != "<?xml version=\"1.0\" standalone=\"yes\"?><SUBPARTS_KEY>")
                //            {
                //                subpartskey = subpartskey + "</SUBPARTS_KEY>";
                //                subpartskey = subpartskey.Replace("'", "''");


                //                string sqlstr11 = "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //                 + "  INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryID + "','" + subpartskey + "' , '" + User.Identity.Name + "'  , @CATEGORY_SHORT) ";
                //                cmd = new SqlCommand(sqlstr11, _SQLCon);
                //                cmd.ExecuteNonQuery();
                //            }
                //        }

                //        /*********************** sub product key end  ************  added by victor *****************/

                //    }
                //    if (catalog_xml != "<?xml version=\"1.0\" standalone=\"yes\"?><CATALOG_ASSOCIATES>")
                //    {
                //        catalog_xml = catalog_xml + "</CATALOG_ASSOCIATES>";
                //        catalog_xml = catalog_xml.Replace("'", "''");


                //        string sqlstr = "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + "  INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER, CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryID + "','" + catalog_xml + "' , '" + User.Identity.Name + "', @CATEGORY_SHORT) ";
                //        cmd = new SqlCommand(sqlstr, _SQLCon);
                //        cmd.ExecuteNonQuery();
                //    }
                //    if (familyids != string.Empty)
                //    {
                //        if (familyids[0] == ',')
                //        {
                //            familyids = familyids.Substring(1);
                //        }
                //        cmd =
                //            new SqlCommand(
                //                "SELECT FAMILY_ID,SORT_ORDER,RECORD_ID,SECTION_NAME FROM TB_PROJECT_SECTION_DETAILS WHERE FAMILY_ID in (" +
                //                familyids + ") for xml path ('Project_Sections'),type", _SQLCon);
                //        SqlDataAdapter daprojsec = new SqlDataAdapter(cmd);
                //        daprojsec.Fill(ds, "Project_Sections");
                //        if (ds.Tables["Project_Sections"].Rows.Count > 0 &&
                //            ds.Tables["Project_Sections"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["Project_Sections"].Rows[0][0] != null)
                //        {
                //            for (int i = 0; i < ds.Tables["Project_Sections"].Rows.Count; i++)
                //            {
                //                projectsec = projectsec + ds.Tables["Project_Sections"].Rows[i][0].ToString();
                //            }
                //        }
                //        cmd =
                //            new SqlCommand(
                //                "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + " SELECT ATTRIBUTE_ID,FAMILY_ID,CATEGORY_ID,SORT_ORDER,CATALOG_ID, @CATEGORY_SHORT as CATEGORY_SHORT FROM TB_CATEGORY_FAMILY_ATTR_LIST WHERE FAMILY_ID in (" +
                //                familyids + ") for xml path ('FAMILY_PUBLISH'),type", _SQLCon);
                //        daprojsec = new SqlDataAdapter(cmd);
                //        daprojsec.Fill(ds, "fam_attr_pub");
                //        if (ds.Tables["fam_attr_pub"].Rows.Count > 0 &&
                //            ds.Tables["fam_attr_pub"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["fam_attr_pub"].Rows[0][0] != null)
                //        {
                //            for (int i = 0; i < ds.Tables["fam_attr_pub"].Rows.Count; i++)
                //            {
                //                fampubattr = fampubattr + ds.Tables["fam_attr_pub"].Rows[i][0].ToString();
                //            }
                //        }
                //        if (fampubattr != string.Empty && fampubattr != null)
                //        {
                //            if (fampubattr != "<?xml version=\"1.0\" standalone=\"yes\"?><FAMILY_PUBLISHED_ATTR>")
                //            {
                //                fampubattr = fampubattr + "</FAMILY_PUBLISHED_ATTR>";
                //                fampubattr = fampubattr.Replace("'", "''");


                //                string projsec = "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + "  INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT ) VALUES('" + xml_id + "','" + categoryID + "','" + fampubattr + "' , '" + User.Identity.Name + "', @CATEGORY_SHORT) ";
                //                cmd = new SqlCommand(projsec, _SQLCon);
                //                cmd.ExecuteNonQuery();
                //            }
                //        }
                //    }
                //    if (projectsec != string.Empty && projectsec != null)
                //    {
                //        if (projectsec != "<?xml version=\"1.0\" standalone=\"yes\"?><PROJECTS>")
                //        {
                //            projectsec = projectsec + "</PROJECTS>";
                //            projectsec = projectsec.Replace("'", "''");

                //            string projsec = "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + "  INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryID + "','" + projectsec + "' , '" + User.Identity.Name + "', @CATEGORY_SHORT) ";
                //            cmd = new SqlCommand(projsec, _SQLCon);
                //            cmd.ExecuteNonQuery();
                //        }
                //    }
            }

            if (RmvType == "Family")
            {
                //    string family_cat = string.Empty;
                //    string _sqlFamilyLevel = " (SELECT FAMILY_ID,PARENT_FAMILY_ID FROM TB_FAMILY WHERE FAMILY_ID = " +
                //                             familyID + " ) UNION " +
                //                             " (SELECT SUBFAMILY_ID,FAMILY_ID FROM TB_SUBFAMILY  WHERE FAMILY_ID = " +
                //                             familyID +
                //                             " AND FAMILY_ID = (SELECT FAMILY_ID FROM TB_FAMILY WHERE FAMILY_ID = " +
                //                             RefID + " ))";
                //    SqlString = _sqlFamilyLevel;
                //    _dsRmvPmnt = CreateDataSet();
                //    xml_id = random.Next(1, 1000);
                //    foreach (DataRow _drFamily in _dsRmvPmnt.Tables[0].Rows)
                //    {
                //        family_cat = family_cat + "," + _drFamily["FAMILY_ID"];
                //        string productsql =
                //            "SELECT  CONVERT (NVARCHAR, PRODUCT_ID)  + ',' FROM TB_PROD_FAMILY WHERE FAMILY_ID='" +
                //            _drFamily[0] + "' for xml path('')";
                //        SqlCommand Command1 = new SqlCommand(productsql, _SQLCon);
                //        productID = Convert.ToString(Command1.ExecuteScalar());
                //        //DataSet productdeleteDS = new DataSet();
                //        //SqlDataAdapter productDA = new SqlDataAdapter(Command);
                //        //productDA.Fill(productdeleteDS);
                //        //if (productdeleteDS != null && productdeleteDS.Tables[0].Rows.Count > 0)
                //        //{
                //        //    foreach (DataRow drow in productdeleteDS.Tables[0].Rows)
                //        //    {
                //        //        productID = productID + "," + drow[0].ToString();
                //        //    }
                //        //    if (productID.Substring(0, 1) == ",")
                //        //    {
                //        //        productID = productID.Substring(1);
                //        //    }
                //        //    product_cat = product_cat + "," + productID;
                //        //}
                //        if (productID.Length > 0)
                //        {
                //            product_cat = product_cat + "," + productID.Substring(0, productID.Length - 1);
                //            productID = productID.Substring(0, productID.Length - 1);
                //        }
                //        else
                //        {
                //            product_cat = product_cat + ",0";
                //            ;
                //            productID = "0";
                //        }
                //        DataSet recycleDS = new DataSet();
                //        try
                //        {

                //            SqlCommand Command = new SqlCommand("[STP_CATALOGSTUDIO5_RECYCLE_TABLE_INSERT]", _SQLCon);
                //            Command.CommandType = CommandType.StoredProcedure;
                //            Command.Parameters.Add(new SqlParameter("@CATALOG_ID", _catalogID));
                //            Command.Parameters.Add(new SqlParameter("@CATEGORYID", categoryID));
                //            Command.Parameters.Add(new SqlParameter("@FAMILY_ID", _drFamily["FAMILY_ID"].ToString()));
                //            Command.Parameters.Add(new SqlParameter("@ALLOW_DUPLICATES", "N"));
                //            Command.Parameters.Add(new SqlParameter("@PRODUCTIDS", productID));
                //            DataAdapter = new SqlDataAdapter(Command);
                //            DataAdapter.SelectCommand.CommandTimeout = 0;
                //            DataAdapter.Fill(recycleDS);

                //            xml = recycleDS.Tables[0].Rows[0][0].ToString();
                //            xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" + recycleDS.Tables[0].Rows[0][0];
                //            xml = xml.Replace("'", "''");
                //            if (_drFamily["PARENT_FAMILY_ID"].ToString() != "0")
                //            {
                //                string sqlstr1 = " DECLARE @FAMILY_NAME NVARCHAR(MAX)"

                //                                + " DECLARE @SUBFAMILY_NAME NVARCHAR(MAX)"
                //                                + " SET @FAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["PARENT_FAMILY_ID"] + "')"
                //                                + " SET @SUBFAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["FAMILY_ID"] + "')"
                //                                + "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,FAMILY_ID,FAMILY_NAME,SUB_FAMILY_ID,SUB_FAMILY_NAME,XML_DOC,DELETED_USER) VALUES"
                //                                + "('" + xml_id + "'," + _catalogID + ","
                //                                + " '" + _drFamily["PARENT_FAMILY_ID"] + "',@FAMILY_NAME,'" + _drFamily["FAMILY_ID"] + "',@SUBFAMILY_NAME,'" + xml + "' , '" + User.Identity.Name + "') ";

                //                Command = new SqlCommand(sqlstr1, _SQLCon);
                //                Command.ExecuteNonQuery();
                //            }
                //            else if (_drFamily["PARENT_FAMILY_ID"].ToString() == "0")
                //            {
                //                string sqlstr1 = " DECLARE @FAMILY_NAME NVARCHAR(MAX)"

                //                                + " SET @FAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["FAMILY_ID"] + "')"
                //                                + "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,FAMILY_ID,FAMILY_NAME,XML_DOC,DELETED_USER) VALUES"
                //                               + "('" + xml_id + "'," + _catalogID + ","
                //                               + " '" + _drFamily["FAMILY_ID"] + "',@FAMILY_NAME,'" + xml + "' , '" + User.Identity.Name + "') ";

                //                Command = new SqlCommand(sqlstr1, _SQLCon);
                //                Command.ExecuteNonQuery();

                //            }
                //        }
                //        catch (Exception ex)
                //        {

                //        }
                //        productID = string.Empty;
                //        xml = string.Empty;
                //        recycleDS.Dispose();
                //    }
                //    if (family_cat != string.Empty)
                //    {
                //        if (family_cat[0] == ',')
                //        {
                //            family_cat = family_cat.Substring(1);
                //        }
                //        cmd =
                //            new SqlCommand(
                //                "SELECT CATALOG_ID,SORT_ORDER,FAMILY_ID,tcf.CATEGORY_ID,tcf.CREATED_USER,tcf.CREATED_DATE,tcf.ROOT_CATEGORY, tc.CATEGORY_SHORT, tc.CATEGORY_PARENT_SHORT FROM TB_CATALOG_FAMILY tcf JOIN TB_CATEGORY tc ON tc.CATEGORY_ID = tcf.CATEGORY_ID   WHERE CATALOG_ID!=1 AND FAMILY_ID in (" +
                //                family_cat + ") for xml path('CATALOG_FAMILY'),type", _SQLCon);
                //        SqlDataAdapter dafam = new SqlDataAdapter(cmd);
                //        dafam.Fill(ds, "CATALOG_FAMILY");
                //        if (ds.Tables["CATALOG_FAMILY"].Rows.Count > 0 &&
                //            ds.Tables["CATALOG_FAMILY"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["CATALOG_FAMILY"].Rows[0][0] != null)
                //        {
                //            catalog_xml = catalog_xml + ds.Tables["CATALOG_FAMILY"].Rows[0][0];
                //        }

                //        cmd =
                //            new SqlCommand(
                //                "SELECT FAMILY_ID,SORT_ORDER,RECORD_ID,SECTION_NAME FROM TB_PROJECT_SECTION_DETAILS WHERE FAMILY_ID in (" +
                //                family_cat + ") for xml path ('Project_Sections'),type", _SQLCon);
                //        SqlDataAdapter daprojsec = new SqlDataAdapter(cmd);
                //        daprojsec.Fill(ds, "Project_Sections");
                //        if (ds.Tables["Project_Sections"].Rows.Count > 0 &&
                //            ds.Tables["Project_Sections"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["Project_Sections"].Rows[0][0] != null)
                //        {
                //            for (int i = 0; i < ds.Tables["Project_Sections"].Rows.Count; i++)
                //            {
                //                projectsec = projectsec + ds.Tables["Project_Sections"].Rows[i][0];
                //            }
                //        }
                //        cmd =
                //            new SqlCommand(
                //                "SELECT CATALOG_ID,ATTRIBUTE_ID,FAMILY_ID,ATTRIBUTE_VALUE,CATEGORY_ID FROM TB_FAMILY_KEY WHERE FAMILY_ID in (" +
                //                family_cat + ") for xml path('FAMILY_KEY'),type", _SQLCon);
                //        SqlDataAdapter dafamkey = new SqlDataAdapter(cmd);
                //        dafamkey.Fill(ds, "FAMILY_KEY");
                //        if (ds.Tables["FAMILY_KEY"].Rows.Count > 0 && ds.Tables["FAMILY_KEY"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["FAMILY_KEY"].Rows[0][0] != null)
                //        {
                //            catalog_xml = catalog_xml + ds.Tables["FAMILY_KEY"].Rows[0][0];
                //        }

                //        cmd =
                //            new SqlCommand(
                //                "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + "SELECT ATTRIBUTE_ID,FAMILY_ID,CATEGORY_ID,SORT_ORDER,CATALOG_ID, @CATEGORY_SHORT as CATEGORY_SHORT  FROM TB_CATEGORY_FAMILY_ATTR_LIST WHERE FAMILY_ID in (" +
                //                family_cat + ") for xml path ('FAMILY_PUBLISH'),type", _SQLCon);
                //        daprojsec = new SqlDataAdapter(cmd);
                //        daprojsec.Fill(ds, "fam_attr_pub");
                //        if (ds.Tables["fam_attr_pub"].Rows.Count > 0 &&
                //            ds.Tables["fam_attr_pub"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["fam_attr_pub"].Rows[0][0] != null)
                //        {
                //            for (int i = 0; i < ds.Tables["fam_attr_pub"].Rows.Count; i++)
                //            {
                //                fampubattr = fampubattr + ds.Tables["fam_attr_pub"].Rows[i][0];
                //            }
                //        }
                //        if (fampubattr != string.Empty && fampubattr != null)
                //        {
                //            if (fampubattr != "<?xml version=\"1.0\" standalone=\"yes\"?><FAMILY_PUBLISHED_ATTR>")
                //            {
                //                fampubattr = fampubattr + "</FAMILY_PUBLISHED_ATTR>";
                //                fampubattr = fampubattr.Replace("'", "''");

                //                string projsec = " DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + "  INSERT INTO TB_RECYCLE_TABLE  (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryID + "','" + fampubattr + "', '" + User.Identity.Name + "' , @CATEGORY_SHORT) ";

                //                cmd = new SqlCommand(projsec, _SQLCon);
                //                cmd.ExecuteNonQuery();
                //            }
                //        }
                //    }
                //    if (product_cat != string.Empty)
                //    {
                //        if (product_cat[0] == ',')
                //        {
                //            product_cat = product_cat.Substring(1);
                //        }
                //        cmd =
                //            new SqlCommand(
                //                "SELECT CATALOG_ID,cp.PRODUCT_ID,P.CATEGORY_ID,cp.CREATED_USER,cp.CREATED_DATE, tc.CATEGORY_SHORT, tc.CATEGORY_PARENT_SHORT FROM TB_CATALOG_PRODUCT cp join tb_product p on p.product_id = cp.product_id JOIN TB_CATEGORY tc ON tc.CATEGORY_ID = p.CATEGORY_ID  WHERE CATALOG_ID!=1 AND cp.PRODUCT_ID IN(" + product_cat + ") for xml path('CATALOG_PRODUCT'),type", _SQLCon);
                //        var daprod = new SqlDataAdapter(cmd);
                //        daprod.Fill(ds, "CATALOG_PRODUCT");
                //        if (ds.Tables["CATALOG_PRODUCT"].Rows.Count > 0 &&
                //            ds.Tables["CATALOG_PRODUCT"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["CATALOG_PRODUCT"].Rows[0][0] != null)
                //        {
                //            for (int i = 0; i < ds.Tables["CATALOG_PRODUCT"].Rows.Count; i++)
                //            {
                //                catalog_xml = catalog_xml + ds.Tables["CATALOG_PRODUCT"].Rows[i][0];
                //            }
                //        }
                //        cmd =
                //            new SqlCommand(
                //                "SELECT ATTRIBUTE_ID,SORT_ORDER,FAMILY_ID,PRODUCT_ID FROM TB_PROD_FAMILY_ATTR_LIST WHERE PRODUCT_ID IN(" +
                //                product_cat + ") for xml path('PRODUCT_PUBLISH'),type", _SQLCon);
                //        daprod = new SqlDataAdapter(cmd);
                //        daprod.Fill(ds, "PROD_PUB_ATTR");
                //        if (ds.Tables["PROD_PUB_ATTR"].Rows.Count > 0 &&
                //            ds.Tables["PROD_PUB_ATTR"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["CATALOG_PRODUCT"].Rows[0][0] != null)
                //        {
                //            for (int i = 0; i < ds.Tables["PROD_PUB_ATTR"].Rows.Count; i++)
                //            {
                //                prodpubattr = prodpubattr + ds.Tables["PROD_PUB_ATTR"].Rows[i][0];
                //            }
                //        }
                //        if (prodpubattr != "<?xml version=\"1.0\" standalone=\"yes\"?><PRODUCT_PUBLISHED_ATTR>")
                //        {
                //            prodpubattr = prodpubattr + "</PRODUCT_PUBLISHED_ATTR>";
                //            prodpubattr = prodpubattr.Replace("'", "''");


                //            string sqlstr = "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + "  INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryID + "','" + prodpubattr + "' , '" + User.Identity.Name + "' , @CATEGORY_SHORT) ";

                //            cmd = new SqlCommand(sqlstr, _SQLCon);
                //            cmd.ExecuteNonQuery();
                //        }
                //        cmd =
                //            new SqlCommand(
                //                "DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + "  SELECT *, @CATEGORY_SHORT as CATEGORY_SHORT FROM TB_PARTS_KEY WHERE PRODUCT_ID IN (" + product_cat +
                //                ") for xml path('PARTSKEY')", _SQLCon);
                //        var daparts = new SqlDataAdapter(cmd);
                //        daparts.Fill(ds, "PARTS_KEY");
                //        if (ds.Tables["PARTS_KEY"].Rows.Count > 0 && ds.Tables["PARTS_KEY"].Rows[0][0].ToString() != "" &&
                //            ds.Tables["PARTS_KEY"].Rows[0][0] != null)
                //        {
                //            for (int i = 0; i < ds.Tables["PARTS_KEY"].Rows.Count; i++)
                //            {
                //                partskey = partskey + ds.Tables["PARTS_KEY"].Rows[i][0];
                //            }
                //        }
                //        if (!string.IsNullOrEmpty(partskey))
                //        {
                //            if (partskey != "<?xml version=\"1.0\" standalone=\"yes\"?><PARTS_KEY>")
                //            {
                //                partskey = partskey + "</PARTS_KEY>";
                //                partskey = partskey.Replace("'", "''");


                //                string sqlstr11 = "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + "  INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryID + "','" + partskey + "' , '" + User.Identity.Name + "', @CATEGORY_SHORT ) ";

                //                cmd = new SqlCommand(sqlstr11, _SQLCon);
                //                cmd.ExecuteNonQuery();
                //            }
                //        }
                //    }
                //    if (catalog_xml != "<?xml version=\"1.0\" standalone=\"yes\"?><CATALOG_ASSOCIATES>")
                //    {
                //        catalog_xml = catalog_xml + "</CATALOG_ASSOCIATES>";
                //        catalog_xml = catalog_xml.Replace("'", "''");

                //        string sqlstr = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,FAMILY_ID,XML_DOC,DELETED_USER) VALUES('" + xml_id + "','" + familyID + "','" + catalog_xml + "' , '" + User.Identity.Name + "' ) ";
                //        cmd = new SqlCommand(sqlstr, _SQLCon);
                //        cmd.ExecuteNonQuery();
                //    }
                //    if (!string.IsNullOrEmpty(projectsec))
                //    {
                //        if (projectsec != "<?xml version=\"1.0\" standalone=\"yes\"?><PROJECTS>")
                //        {
                //            projectsec = projectsec + "</PROJECTS>";
                //            projectsec = projectsec.Replace("'", "''");

                //            string projsec = "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                //                 + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                //            + "  INSERT INTO TB_RECYCLE_TABLE  (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryID + "','" + projectsec + "'  , '" + User.Identity.Name + "' , @CATEGORY_SHORT) ";
                //            cmd = new SqlCommand(projsec, _SQLCon);
                //            cmd.ExecuteNonQuery();
                //        }
                //    }


                //    string _categoryLevel = " exec STP_CATALOGSTUDIO5_CategoryLevel '" + categoryID + "'";
                //    SqlString = _categoryLevel;
                //    _dsCatlevel = CreateDataSet();
                //    foreach (DataRow drcat in _dsCatlevel.Tables[0].Rows)
                //    {
                //        string sqlstr2 = "INSERT INTO TB_RECYCLE_CATEGORY_REFERENCE (XML_ID,CAT_ID,LEVEL,CREATED_USER) VALUES"
                //                         + "('" + xml_id + "','" + drcat["CATEGORY_ID"].ToString() + "','" +
                //                         drcat["LEVEL"].ToString() + "', '" + User.Identity.Name + "')";
                //        SqlCommand Command = new SqlCommand(sqlstr2, _SQLCon);
                //        Command.ExecuteNonQuery();
                //    }
            }

            if (RmvType == "Product")
            {
                string _sqlFamilyLevel = " (SELECT FAMILY_ID,PARENT_FAMILY_ID FROM TB_FAMILY WHERE FAMILY_ID = " +
                                         familyID + " ) UNION " +
                                         " (SELECT SUBFAMILY_ID,FAMILY_ID FROM TB_SUBFAMILY  WHERE FAMILY_ID = " +
                                         familyID +
                                         " AND FAMILY_ID = (SELECT FAMILY_ID FROM TB_FAMILY WHERE FAMILY_ID = " +
                                         RefID.ToString() + " ))";
                SqlString = _sqlFamilyLevel;
                _dsRmvPmnt = CreateDataSet();
                xml_id = random.Next(1, 1000);

                DataSet recycleDS = new DataSet();
                SqlCommand Command = new SqlCommand("[STP_CATALOGSTUDIO5_RECYCLE_TABLE_INSERT]", _SQLCon);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Add(new SqlParameter("@CATALOG_ID", _catalogID));
                Command.Parameters.Add(new SqlParameter("@CATEGORYID", categoryID));
                Command.Parameters.Add(new SqlParameter("@FAMILY_ID", familyID));
                Command.Parameters.Add(new SqlParameter("@ALLOW_DUPLICATES", "N"));
                Command.Parameters.Add(new SqlParameter("@PRODUCTIDS", productID));
                DataAdapter = new SqlDataAdapter(Command);
                DataAdapter.SelectCommand.CommandTimeout = 0;
                DataAdapter.Fill(recycleDS);
                xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" + recycleDS.Tables[0].Rows[0][0].ToString();
                xml = xml.Replace("'", "''");
                foreach (DataRow _drFamily in _dsRmvPmnt.Tables[0].Rows)
                {
                    if (_drFamily["PARENT_FAMILY_ID"].ToString() != "0")
                    {
                        string sqlstr1 = " DECLARE @FAMILY_NAME NVARCHAR(MAX)"

                                        + " DECLARE @SUBFAMILY_NAME NVARCHAR(MAX)"
                                        + " DECLARE @PRODUCT_NAME NVARCHAR(MAX)"
                                        + " SET @FAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["PARENT_FAMILY_ID"].ToString() + "')"
                                        + " SET @SUBFAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["FAMILY_ID"].ToString() + "')"
                                        + " SET @PRODUCT_NAME = (SELECT STRING_VALUE FROM TB_PROD_SPECS WHERE PRODUCT_ID='" + productID + "' AND ATTRIBUTE_ID=1)"
                                        + " INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,FAMILY_ID,FAMILY_NAME,SUB_FAMILY_ID,SUB_FAMILY_NAME,PRODUCT_ID,PRODUCT_NAME,XML_DOC,DELETED_USER) VALUES"
                                        + "('" + xml_id + "'," + _catalogID + ","
                                        + " '" + _drFamily["PARENT_FAMILY_ID"].ToString() + "',@FAMILY_NAME,"
                                        + "'" + _drFamily["FAMILY_ID"].ToString() + "',@SUBFAMILY_NAME,"
                                        + "'" + productID + "',@PRODUCT_NAME,'" + xml + "'  , '" + User.Identity.Name + "') ";
                        Command = new SqlCommand(sqlstr1, _SQLCon);
                        Command.ExecuteNonQuery();
                    }
                    else if (_drFamily["PARENT_FAMILY_ID"].ToString() == "0")
                    {
                        string sqlstr1 = " DECLARE @FAMILY_NAME NVARCHAR(MAX)"

                                        + " DECLARE @PRODUCT_NAME NVARCHAR(MAX)"
                                        + " SET @FAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["FAMILY_ID"].ToString() + "')"
                                        + " SET @PRODUCT_NAME = (SELECT STRING_VALUE FROM TB_PROD_SPECS WHERE PRODUCT_ID='" + productID + "' AND ATTRIBUTE_ID=1)"
                                        + "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,FAMILY_ID,FAMILY_NAME,PRODUCT_ID,PRODUCT_NAME,XML_DOC,DELETED_USER) VALUES"
                                        + "('" + xml_id + "'," + _catalogID + ","
                                        + " '" + _drFamily["FAMILY_ID"].ToString() + "',@FAMILY_NAME,"
                                        + " '" + productID + "',@PRODUCT_NAME,'" + xml + "' , '" + User.Identity.Name + "') ";

                        Command = new SqlCommand(sqlstr1, _SQLCon);
                        Command.ExecuteNonQuery();

                    }
                }
                //string sqlstr2 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,PRODUCT_ID,PRODUCT_NAME,XML_DOC) VALUES"
                //                   + "('" + xml_id + "'," + _catalogID + ","
                //                   + " '" + productID + "',(SELECT STRING_VALUE FROM TB_PROD_SPECS WHERE PRODUCT_ID='" + productID + "' AND ATTRIBUTE_ID=1),'" + xml + "' ) ";
                //Command = new SqlCommand(sqlstr2, _SQLCon);
                //Command.ExecuteNonQuery();
                recycleDS.Dispose();

                cmd =
                    new SqlCommand(
                        "SELECT CATALOG_ID,cp.PRODUCT_ID,P.CATEGORY_ID,cp.CREATED_USER,cp.CREATED_DATE FROM TB_CATALOG_PRODUCT cp join tb_product p on p.product_id = cp.product_id WHERE CATALOG_ID!=1 AND cp.PRODUCT_ID IN(" + productID + ") for xml path('CATALOG_PRODUCT'),type", _SQLCon);

                SqlDataAdapter daprod = new SqlDataAdapter(cmd);
                daprod.Fill(ds, "CATALOG_PRODUCT");
                if (ds.Tables["CATALOG_PRODUCT"].Rows.Count > 0 &&
                    ds.Tables["CATALOG_PRODUCT"].Rows[0][0].ToString() != "" &&
                    ds.Tables["CATALOG_PRODUCT"].Rows[0][0] != null)
                {
                    for (int i = 0; i < ds.Tables["CATALOG_PRODUCT"].Rows.Count; i++)
                    {
                        catalog_xml = catalog_xml + ds.Tables["CATALOG_PRODUCT"].Rows[i][0].ToString();
                    }
                }
                if (catalog_xml != string.Empty && catalog_xml != null)
                {
                    if (catalog_xml != "<?xml version=\"1.0\" standalone=\"yes\"?><CATALOG_ASSOCIATES>")
                    {
                        catalog_xml = catalog_xml + "</CATALOG_ASSOCIATES>";
                        catalog_xml = catalog_xml.Replace("'", "''");


                        string sqlstr = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,PRODUCT_ID,XML_DOC,DELETED_USER) VALUES('" + xml_id + "','" + productID + "','" + catalog_xml + "' , '" + User.Identity.Name + "' ) ";

                        cmd = new SqlCommand(sqlstr, _SQLCon);
                        cmd.ExecuteNonQuery();
                    }
                }
                cmd =
                    new SqlCommand(
                        "SELECT tpfal.ATTRIBUTE_ID,tpfal.SORT_ORDER,tpfal.FAMILY_ID,tpf.PRODUCT_ID FROM TB_PROD_FAMILY_ATTR_LIST tpfal join tb_prod_family tpf on tpfal.family_id=tpf.family_id  WHERE tpf.PRODUCT_ID IN(" +
                        productID + ") for xml path('PRODUCT_PUBLISH'),type", _SQLCon);
                daprod = new SqlDataAdapter(cmd);
                daprod.Fill(ds, "PROD_PUB_ATTR");
                if (ds.Tables["PROD_PUB_ATTR"].Rows.Count > 0 && ds.Tables["PROD_PUB_ATTR"].Rows[0][0].ToString() != "" &&
                    ds.Tables["CATALOG_PRODUCT"].Rows[0][0] != null)
                {
                    for (int i = 0; i < ds.Tables["PROD_PUB_ATTR"].Rows.Count; i++)
                    {
                        prodpubattr = prodpubattr + ds.Tables["PROD_PUB_ATTR"].Rows[i][0].ToString();
                    }
                }
                if (prodpubattr != "<?xml version=\"1.0\" standalone=\"yes\"?><PRODUCT_PUBLISHED_ATTR>")
                {
                    prodpubattr = prodpubattr + "</PRODUCT_PUBLISHED_ATTR>";
                    prodpubattr = prodpubattr.Replace("'", "''");


                    string sqlstr = "  DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                             + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                        + "  INSERT INTO TB_RECYCLE_TABLE  (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryID + "','" + prodpubattr + "' , '" + User.Identity.Name + "', @CATEGORY_SHORT) ";

                    cmd = new SqlCommand(sqlstr, _SQLCon);
                    cmd.ExecuteNonQuery();
                }
                cmd =
                    new SqlCommand(
                        "select * from TB_PROD_FAMILY where PRODUCT_ID in(" + productID + ") for xml path ('PRD_FAM')",
                        _SQLCon);
                SqlDataAdapter daprodfam = new SqlDataAdapter(cmd);
                daprodfam.Fill(ds, "PROD_FAMILY");
                if (ds.Tables["PROD_FAMILY"].Rows.Count > 0 && ds.Tables["PROD_FAMILY"].Rows[0][0].ToString() != "" &&
                    ds.Tables["PROD_FAMILY"].Rows[0][0] != null)
                {
                    for (int i = 0; i < ds.Tables["PROD_FAMILY"].Rows.Count; i++)
                    {
                        prdfamassoc = prdfamassoc + ds.Tables["PROD_FAMILY"].Rows[i][0].ToString();
                    }
                }
                if (prdfamassoc != string.Empty && prdfamassoc != null)
                {
                    if (prdfamassoc != "<?xml version=\"1.0\" standalone=\"yes\"?><PROD_FAMILY>")
                    {
                        prdfamassoc = prdfamassoc + "</PROD_FAMILY>";
                        prdfamassoc = prdfamassoc.Replace("'", "''");


                        string sqlstr11 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,PRODUCT_ID,XML_DOC,DELETED_USER) VALUES('" + xml_id + "','" + productID + "','" + prdfamassoc + "', '" + User.Identity.Name + "' ) ";

                        cmd = new SqlCommand(sqlstr11, _SQLCon);
                        cmd.ExecuteNonQuery();
                    }
                }
                cmd =
                    new SqlCommand(
                        "DECLARE @CATEGORY_SHORT VARCHAR(100)   "
                             + " SET @CATEGORY_SHORT = (SELECT CATEGORY_SHORT FROM TB_CATEGORY WHERE CATEGORY_ID='" + categoryID + "')"
                        + "  SELECT *, @CATEGORY_SHORT as CATEGORY_SHORT FROM TB_PARTS_KEY WHERE PRODUCT_ID IN ('" + productID + "') for xml path('PARTSKEY')",
                        _SQLCon);
                SqlDataAdapter daparts = new SqlDataAdapter(cmd);
                daparts.Fill(ds, "PARTS_KEY");
                if (ds.Tables["PARTS_KEY"].Rows.Count > 0 && ds.Tables["PARTS_KEY"].Rows[0][0].ToString() != "" &&
                    ds.Tables["PARTS_KEY"].Rows[0][0] != null)
                {
                    for (int i = 0; i < ds.Tables["PARTS_KEY"].Rows.Count; i++)
                    {
                        partskey = partskey + ds.Tables["PARTS_KEY"].Rows[i][0].ToString();
                    }
                }
                if (partskey != string.Empty && partskey != null)
                {
                    if (partskey != "<?xml version=\"1.0\" standalone=\"yes\"?><PARTS_KEY>")
                    {
                        partskey = partskey + "</PARTS_KEY>";
                        partskey = partskey.Replace("'", "''");

                        string sqlstr11 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,PRODUCT_ID,XML_DOC,DELETED_USER) VALUES('" + xml_id + "','" + productID + "','" + partskey + "', '" + User.Identity.Name + "' ) ";

                        cmd = new SqlCommand(sqlstr11, _SQLCon);
                        cmd.ExecuteNonQuery();
                    }
                }
                string _categoryLevel = " exec STP_CATALOGSTUDIO5_CategoryLevel '" + categoryID + "'";
                SqlString = _categoryLevel;
                _dsCatlevel = CreateDataSet();
                foreach (DataRow drcat in _dsCatlevel.Tables[0].Rows)
                {
                    string sqlstr2 = "INSERT INTO TB_RECYCLE_CATEGORY_REFERENCE (XML_ID,CAT_ID,LEVEL,CREATED_USER) VALUES"
                                     + "('" + xml_id + "','" + drcat["CATEGORY_ID"].ToString() + "','" +
                                     drcat["LEVEL"].ToString() + "', '" + User.Identity.Name + "')";
                    Command = new SqlCommand(sqlstr2, _SQLCon);
                    Command.ExecuteNonQuery();
                }
            }

            if (RmvType == "Attribute")
            {
                int Attr_Type = 0;
                xml_id = random.Next(1, 1000);
                xml = "<?xml version=\"1.0\" standalone=\"yes\"?>";
                cmd = new SqlCommand("SELECT ATTRIBUTE_TYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID =" + attributeID,
                    _SQLCon);
                Attr_Type = Convert.ToInt32(cmd.ExecuteScalar());
                SqlDataAdapter da;
                if (Attr_Type == 7 || Attr_Type == 9 || Attr_Type == 11 || Attr_Type == 12 || Attr_Type == 13)
                {
                    cmd = new SqlCommand("SELECT * FROM ("
                                         + " SELECT (SELECT * FROM TB_FAMILY_SPECS WHERE ATTRIBUTE_ID = '" + attributeID +
                                         "' for xml path('FAMILY'),TYPE)XML"
                                         + " UNION ALL "
                                         + " SELECT (SELECT * FROM TB_FAMILY_KEY WHERE ATTRIBUTE_ID = '" + attributeID +
                                         "' for xml path('FAMILYKEY'),TYPE)"
                                         + " UNION ALL "
                                         + " SELECT (SELECT * FROM TB_CATEGORY_FAMILY_ATTR_LIST WHERE ISNULL(FLAG_RECYCLE,'A') ='A' AND  ATTRIBUTE_ID = '" +
                                         attributeID + "' for xml path('CATFAMATTR'),TYPE)"
                                         + " UNION ALL "
                                         + " SELECT (SELECT * FROM TB_CATALOG_ATTRIBUTES WHERE ATTRIBUTE_ID = '" +
                                         attributeID + "' for xml path('CATALOG'),TYPE)"
                                         + " ) B FOR XML PATH ('') , ROOT ('FAMILY_ATTRIBUTE'),type", _SQLCon);
                    da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "FAMATTR");
                    if (ds.Tables["FAMATTR"].Rows.Count > 0 && ds.Tables["FAMATTR"].Rows[0][0].ToString() != "" &&
                        ds.Tables["FAMATTR"].Rows[0][0] != null)
                    {
                        xml = xml + ds.Tables["FAMATTR"].Rows[0][0].ToString();
                        xml = xml.Replace("'", "''");
                        string sqlstr1 =
                            "INSERT INTO TB_RECYCLE_FOR_ATTRIBUTES([XML_ID],[XML_DOC],[ATTRIBUTE_NAME],[ATTRIBUTE_TYPE],[ATTRIBUTE_ID],[CREATE_BY_DEFAULT],[VALUE_REQUIRED],[STYLE_NAME],[STYLE_FORMAT],[DEFAULT_VALUE]" +
                            ",[PUBLISH2PRINT],[PUBLISH2WEB],[PUBLISH2CDROM],[PUBLISH2ODP],[USE_PICKLIST],[ATTRIBUTE_DATATYPE],[ATTRIBUTE_DATAFORMAT],[ATTRIBUTE_DATARULE],[UOM],[IS_CALCULATED],[ATTRIBUTE_CALC_FORMULA]" +
                            ",[PICKLIST_NAME],[CREATED_USER],[CREATED_DATE],MODIFIED_USER )SELECT '" + xml_id + "','" + xml +
                            "',[ATTRIBUTE_NAME],[ATTRIBUTE_TYPE],[ATTRIBUTE_ID],[CREATE_BY_DEFAULT],[VALUE_REQUIRED],[STYLE_NAME],[STYLE_FORMAT]," +
                            "[DEFAULT_VALUE],[PUBLISH2PRINT],[PUBLISH2WEB],[PUBLISH2CDROM],[PUBLISH2ODP],[USE_PICKLIST],[ATTRIBUTE_DATATYPE]," +
                            " [ATTRIBUTE_DATAFORMAT],[ATTRIBUTE_DATARULE],[UOM],[IS_CALCULATED],[ATTRIBUTE_CALC_FORMULA],[PICKLIST_NAME]," +
                            "[CREATED_USER],[CREATED_DATE],'" + User.Identity.Name + "' FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID='" + attributeID + "'";
                        cmd = new SqlCommand(sqlstr1, _SQLCon);
                        cmd.ExecuteNonQuery();
                        xml = string.Empty;
                        xml = "<?xml version=\"1.0\" standalone=\"yes\"?>";
                    }
                }
                else
                {
                    cmd = new SqlCommand("SELECT * FROM ("
                                         + " SELECT (SELECT * FROM TB_PROD_SPECS WHERE ATTRIBUTE_ID = '" + attributeID +
                                         "' for xml path('PRODUCT'),TYPE)XML"
                                         + " UNION ALL "
                                         + " SELECT (SELECT * FROM TB_PROD_FAMILY_ATTR_LIST WHERE ATTRIBUTE_ID = '" +
                                         attributeID + "' for xml path('PRODFAMATTR'),TYPE)"
                                         + " UNION ALL "
                                         + " SELECT (SELECT * FROM TB_PARTS_KEY WHERE ATTRIBUTE_ID = '" + attributeID +
                                         "' for xml path('PARTSKEY'),TYPE)"
                                         + " UNION ALL "
                                         + " SELECT (SELECT * FROM TB_CATALOG_ATTRIBUTES WHERE ATTRIBUTE_ID = '" +
                                         attributeID + "' for xml path('CATALOG'),TYPE)"
                                         + " UNION ALL "
                                         + " SELECT (SELECT * FROM TB_ATTRIBUTE_GROUP_SPECS WHERE ATTRIBUTE_ID = '" +
                                         attributeID + "' for xml path('GROUP'),TYPE)"
                                         + " ) B FOR XML PATH ('') , ROOT ('PRODUCT_ATTRIBUTES'),type", _SQLCon);
                    da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "PRODATTR");
                    if (ds.Tables["PRODATTR"].Rows.Count > 0 && ds.Tables["PRODATTR"].Rows[0][0].ToString() != "" &&
                        ds.Tables["PRODATTR"].Rows[0][0] != null)
                    {
                        xml = xml + ds.Tables["PRODATTR"].Rows[0][0].ToString();
                        xml = xml.Replace("'", "''");
                        string sqlstr2 =
                            "INSERT INTO TB_RECYCLE_FOR_ATTRIBUTES([XML_ID],[XML_DOC],[ATTRIBUTE_NAME],[ATTRIBUTE_TYPE],[ATTRIBUTE_ID],[CREATE_BY_DEFAULT],[VALUE_REQUIRED],[STYLE_NAME],[STYLE_FORMAT],[DEFAULT_VALUE]" +
                            ",[PUBLISH2PRINT],[PUBLISH2WEB],[PUBLISH2CDROM],[PUBLISH2ODP],[USE_PICKLIST],[ATTRIBUTE_DATATYPE],[ATTRIBUTE_DATAFORMAT],[ATTRIBUTE_DATARULE],[UOM],[IS_CALCULATED],[ATTRIBUTE_CALC_FORMULA]" +
                            ",[PICKLIST_NAME],[CREATED_USER],[CREATED_DATE],MODIFIED_USER )SELECT '" + xml_id + "','" + xml +
                            "',[ATTRIBUTE_NAME],[ATTRIBUTE_TYPE],[ATTRIBUTE_ID],[CREATE_BY_DEFAULT],[VALUE_REQUIRED],[STYLE_NAME],[STYLE_FORMAT]," +
                            "[DEFAULT_VALUE],[PUBLISH2PRINT],[PUBLISH2WEB],[PUBLISH2CDROM],[PUBLISH2ODP],[USE_PICKLIST],[ATTRIBUTE_DATATYPE]," +
                            " [ATTRIBUTE_DATAFORMAT],[ATTRIBUTE_DATARULE],[UOM],[IS_CALCULATED],[ATTRIBUTE_CALC_FORMULA],[PICKLIST_NAME]," +
                            "[CREATED_USER],[CREATED_DATE],'" + User.Identity.Name + "' FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID='" + attributeID + "'";
                        cmd = new SqlCommand(sqlstr2, _SQLCon);
                        cmd.ExecuteNonQuery();
                        xml = string.Empty;
                        xml = "<?xml version=\"1.0\" standalone=\"yes\"?>";
                    }
                }
            }
            _SQLCon.Close();
            return recycleDS1;
        }

        #endregion
        [System.Web.Http.HttpPost]
        public HttpResponseMessage Customtablexport()
        {
            DataSet Exportset = new DataSet();
            using (var conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                var cmd2 = new SqlDataAdapter("SELECT PRODUCT_GROUP_ID,PRODUCT_GROUP_DESC,TOP_CATEGORY FROM QS_CUSTOM order by ID", conn);
                cmd2.Fill(Exportset);

            }
            object context;
            if (Request.Properties.TryGetValue("MS_HttpContext", out context))
            {
                var httpContext = context as HttpContextBase;
                if (httpContext != null && httpContext.Session != null)
                {
                    httpContext.Session["ExportTable"] = Exportset.Tables[0];
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, "CUSTOMEXPORT" + ".xls");
        }
        [System.Web.Http.HttpPost]
        public string Customtablemgmt()
        {
            string importTemp;
            importTemp = Guid.NewGuid().ToString();
            int userProductCount = 0;
            int userSkuProductCount = 0;
            try
            {
                using (var conn = new SqlConnection(_connectionString))
                {
                    conn.Open();
                    string sqlString = string.Empty;
                    var sqlstring1 = new SqlCommand("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##CUSTOMTABLE" + importTemp + "]'')BEGIN DROP TABLE [##CUSTOMTABLE" + importTemp + "] END')", conn);
                    sqlstring1.ExecuteNonQuery();
                    var bulkCopy1 = new SqlBulkCopy(conn);
                    var cmd2 = new SqlCommand();
                    cmd2.Connection = conn;
                    sqlString = _ic.CreateTableToImport("[##CUSTOMTABLE" + importTemp + "]", Customtbl.Tables[0]);
                    cmd2.CommandText = sqlString;
                    cmd2.CommandType = CommandType.Text;
                    cmd2.ExecuteNonQuery();
                    bulkCopy1.DestinationTableName = "[##CUSTOMTABLE" + importTemp + "]";
                    bulkCopy1.WriteToServer(Customtbl.Tables[0]);
                    if (Customtbl.Tables[0].Rows.Count > 0)
                    {
                        //string column_value = catalog.Tables[0].Rows[0][0].ToString();
                        string attrmapp = "EXEC STP_LS_CUSTOMTABLEMAGNAGEMENT '" + importTemp + "'";
                        var dbCommandap = new SqlCommand(attrmapp, conn);
                        dbCommandap.ExecuteNonQuery();


                        return "Import Success~" + importTemp;
                    }
                    else
                    {
                        return "Import Failed~" + importTemp;

                    }
                }

            }

            catch (Exception ex)
            {
                Logger.Error("Error at AttributeMappingApiController : Customtablemgmt", ex);
                return "Import Failed~" + importTemp;
            }

        }

        [System.Web.Http.HttpGet]
        public IList Customerincentivetable()
        {
            //try
            //{
            //  //  var csinc = _dbcontext.CUSTOMER_INCENTIVE.ToList();
            //    return csinc;
            //}
            //catch (Exception)
            //{
            return null;
            //}

        }

        [System.Web.Http.HttpGet]
        public IList Sortmgmt()
        {
            //var sortmgmt = _dbcontext.TB_CATALOG_ATTRIBUTES.Select(x => x.CATALOG_ID == 4).ToList();

            //try
            //{
            //    //var sortmgmt = _dbcontext.TB_CATALOG_ATTRIBUTES
            //    //.Join(_dbcontext.TB_ATTRIBUTE,
            //    //    post => post.ATTRIBUTE_ID,
            //    //    meta => meta.ATTRIBUTE_ID,
            //    //    (post, meta) => new { Post = post, Meta = meta })
            //    //.Where(postAndMeta => postAndMeta.Post.CATALOG_ID == 4).Select(x => new { ATTRIBUTE_ID = x.Post.ATTRIBUTE_ID, ATTRIBUTE_NAME = x.Meta.ATTRIBUTE_NAME }).ToList();
            //   // var sortmgmt = _dbcontext.QS_CUSTOM_ATTRIBUTES.Select(x => new { ATTRIBUTE_ID = x.ATTRIBUTE_ID, ATTRIBUTE_NAME = x.ATTRIBUTE_NAME, SORT = x.SORT }).ToList();

            //    return sortmgmt;
            //}
            //catch (Exception)
            //{

            return null;
            //}
        }
        [System.Web.Http.HttpGet]
        public IList Sortmgmtdropdown()
        {
            //var sortmgmt = _dbcontext.TB_CATALOG_ATTRIBUTES.Select(x => x.CATALOG_ID == 4).ToList();
            //try
            //{
            //    //var sortmgmt = _dbcontext.TB_CATALOG_ATTRIBUTES
            //    //.Join(_dbcontext.TB_ATTRIBUTE,
            //    //    post => post.ATTRIBUTE_ID,
            //    //    meta => meta.ATTRIBUTE_ID,
            //    //    (post, meta) => new { Post = post, Meta = meta })
            //    //.Where(postAndMeta => postAndMeta.Post.CATALOG_ID == 4).Select(x => new { ATTRIBUTE_ID = x.Post.ATTRIBUTE_ID, ATTRIBUTE_NAME = x.Meta.ATTRIBUTE_NAME }).ToList();
            // //   var sortmgmt = _dbcontext.QS_CUSTOM_ATTRIBUTES.Select(x => new { SORT = x.SORT }).ToList();

            //    return sortmgmt;
            //}
            //catch (Exception)
            //{
            return null;
            //}
        }
        [System.Web.Http.HttpPost]
        public string Customerincentivetableimport()
        {
            string importTemp;
            importTemp = Guid.NewGuid().ToString();
            int userProductCount = 0;
            int userSkuProductCount = 0;
            try
            {
                if (!string.IsNullOrEmpty(customtableexcelpath))
                {
                    DataSet ds = new DataSet();
                    StreamReader sr = new StreamReader(customtableexcelpath);
                    string[] headers = sr.ReadLine().Split(',');
                    DataTable dt1 = new DataTable();
                    foreach (string header in headers)
                    {
                        dt1.Columns.Add(header);
                    }
                    while (!sr.EndOfStream)
                    {
                        string[] rows = sr.ReadLine().Split(',');
                        DataRow dr = dt1.NewRow();
                        for (int i = 0; i < headers.Length; i++)
                        {
                            dr[i] = rows[i];
                        }
                        dt1.Rows.Add(dr);
                    }
                    ds.Tables.Add(dt1);
                    using (var conn = new SqlConnection(_connectionString))
                    {
                        conn.Open();
                        string sqlString = string.Empty;
                        var sqlstring1 =
                            new SqlCommand(
                                "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##CUSTOMERINCENTIVE" +
                                importTemp + "]'')BEGIN DROP TABLE [##CUSTOMERINCENTIVE" + importTemp + "] END')", conn);
                        sqlstring1.ExecuteNonQuery();
                        var bulkCopy1 = new SqlBulkCopy(conn);
                        var cmd2 = new SqlCommand();
                        cmd2.Connection = conn;
                        sqlString = _ic.CreateTableToImport("[##CUSTOMERINCENTIVE" + importTemp + "]", ds.Tables[0]);
                        cmd2.CommandText = sqlString;
                        cmd2.CommandType = CommandType.Text;
                        cmd2.ExecuteNonQuery();
                        bulkCopy1.DestinationTableName = "[##CUSTOMERINCENTIVE" + importTemp + "]";
                        bulkCopy1.WriteToServer(ds.Tables[0]);

                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            //string column_value = catalog.Tables[0].Rows[0][0].ToString();
                            string attrmapp = "EXEC STP_LS_CUSTOMERINCENTIVE '" + importTemp + "'";
                            var dbCommandap = new SqlCommand(attrmapp, conn);
                            dbCommandap.ExecuteNonQuery();


                            return "Import Success~" + importTemp;
                        }
                        else
                        {
                            return "Import Failed~" + importTemp;

                        }

                    }
                }
                return "Import Failed~" + importTemp;
            }

            catch (Exception ex)
            {
                Logger.Error("Error at AttributeMappingApiController : Customtablemgmt", ex);
                return "Import Failed~" + importTemp;
            }

        }

    }
}