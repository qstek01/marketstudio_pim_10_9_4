﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Http;
using log4net;
using LS.Data;
using LS.Data.Model.CatalogSectionModels;
using LS.Data.Model.Export;
using LS.Data.Model.ProductPreview;
using LS.Data.Model.ProductView;
using System.Reflection;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using LS.Data.Utilities;
using System.Collections;
using System.Web;
using System.Xml.Serialization;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json.Linq;
using LS.Data.Model;
using System.Xml.Linq;
using System.Data.Entity.Core.Objects;
using System.Security.Cryptography;
using LS.Web.Utility;
using Kendo.DynamicLinq;
using Stimulsoft.Report;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Stimulsoft.Report.Export;
using System.Web.Mvc.Html;
using Missing = System.Reflection.Missing;
using System.Transactions;
using System.Data.OleDb;
using LS.Web.Models;
using Infragistics.Documents.Excel;


namespace LS.Web.Controllers
{
    public class InvertedProductController : ApiController
    {

        // GET: Home
        private static readonly ILog Logger = LogManager.GetLogger(typeof(HomeApiController));
        private readonly CSEntities _dbcontext = new CSEntities();
        private ImportController importController = new ImportController();
        readonly DataSet _dsSheets = new DataSet();
        DataTable _dtable;
        public string SqlString = "";
        public string RefID = "";
        string productTemp;
        string SQLString = string.Empty;
        string prodTemp;
        int allowDuplicate;


        private DataSet _dsRmvPmnt = new DataSet();
        private DataSet _recycleDSPerma = new DataSet();
        public CSEntities objLS = new CSEntities();
        public string Sqlstr = "";
        public string ImageMagickPath = string.Empty;
        private static string catIds = string.Empty;
        private static string FamilyIds = string.Empty;
        readonly string _imagepath = HttpContext.Current.Server.MapPath("~/Content/ProductImages");
        public string CustomerFolder = string.Empty;

        [System.Web.Http.HttpPost]
        public string GetInvertedProductsCount(int catalogId, string opt)
        {
            try
            {
                if (opt.Equals("TOTALCOUNT"))
                {
                    opt = "TOTALCOUNT1";
                }
                SqlDataAdapter da = null;
                string connction = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                string id = Guid.NewGuid().ToString();
                DataSet prodListNew = new DataSet();
                da = new SqlDataAdapter("EXEC STP_LS_InvertedProducts " + catalogId + ",id," + opt + "," + 0 + "," + 0, connction);
                da.SelectCommand.CommandTimeout = 0;
                da.Fill(prodListNew);
                string count = string.Empty;
                if (prodListNew != null && prodListNew.Tables[0].Rows.Count > 0)
                {
                    count = prodListNew.Tables[0].Rows[0]["COUNT"].ToString();
                }
                return count;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InvertedProductController : GetInvertedProductsCount", objexception);
                return null;
            }
        }


        [System.Web.Http.HttpPost]
        public JsonResult GetInvertedProducts(int catalogId, string Pageno, string Perpagecount, string selectedCategoryId, int selectedTemplateId)
        {
            try
            {
                if (selectedCategoryId == "0" || selectedCategoryId == "undefined")
                {
                    var dynamicColumns = new Dictionary<string, string>();
                    var sb = new StringBuilder();
                    var sw = new StringWriter(sb);
                    JsonWriter jsonWriter = new JsonTextWriter(sw);
                    var model = new DashBoardModel();
                    SqlDataAdapter da = null;
                    string connction = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                    string id = Guid.NewGuid().ToString();
                    DataSet prodListNew1 = new DataSet();


                    da = new SqlDataAdapter("EXEC STP_LS_InvertedProducts " + catalogId + ",id,ATTRIBUTELIST," + Pageno + "," + Perpagecount + "," + selectedTemplateId, connction);
                    var objDataTable1 = new DataSet();
                    da.SelectCommand.CommandTimeout = 0;
                    da.Fill(objDataTable1);
                    System.Web.HttpContext.Current.Session["ModifiedKendoProductCount"] = objDataTable1.Tables[1];
                    if (objDataTable1.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataColumn columns in objDataTable1.Tables[0].Columns)
                        {
                            PTColumns objPTColumns = new PTColumns();
                            if (columns.ToString() != "CATALOG_ID" && columns.ToString() != "PRODUCT_ID" && columns.ToString() != "CATALOG_NAME" && columns.ToString() != "FAMILY_ID" && columns.ToString() != "CATEGORY_ID")
                            {
                                string attributeName = string.Empty;

                                attributeName = columns.Caption.ToString();

                                int attribute_Id = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == attributeName).Select(y => y.ATTRIBUTE_ID).FirstOrDefault();
                                int attribute_Type = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == attributeName).Select(y => y.ATTRIBUTE_TYPE).FirstOrDefault();

                                objPTColumns.Caption = columns.Caption + "__OBJ__" + attribute_Id + "__" + attribute_Type + "__false__true";
                                objPTColumns.ColumnName = columns.ColumnName + "__OBJ__" + attribute_Id + "__" + attribute_Type + "__false__true";
                            }


                            else
                            {
                                objPTColumns.Caption = columns.Caption;
                                objPTColumns.ColumnName = columns.ColumnName;
                            }


                            model.Columns.Add(objPTColumns);
                        }
                    }

                    if (objDataTable1.Tables[0].Rows.Count > 0)
                    {

                        for (int i = 0; i < objDataTable1.Tables[0].Columns.Count; i++)
                        {
                            if (objDataTable1.Tables[0].Columns[i].ColumnName != "CATALOG_ID" &&
                                objDataTable1.Tables[0].Columns[i].ColumnName != "CATALOG_NAME" &&
                                objDataTable1.Tables[0].Columns[i].ColumnName != "PRODUCT_ID" &&
                                objDataTable1.Tables[0].Columns[i].ColumnName != "FAMILY_ID" &&
                                objDataTable1.Tables[0].Columns[i].ColumnName != "FAMILY_ID" &&
                                objDataTable1.Tables[0].Columns[i].ColumnName != "CATEGORY_ID"
                                )
                            {
                                string attributeName = string.Empty;

                                attributeName = objDataTable1.Tables[0].Columns[i].ColumnName.ToString();


                                int attribute_Id = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == attributeName).Select(y => y.ATTRIBUTE_ID).FirstOrDefault();
                                int attribute_Type = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == attributeName).Select(y => y.ATTRIBUTE_TYPE).FirstOrDefault();

                                objDataTable1.Tables[0].Columns[i].ColumnName = objDataTable1.Tables[0].Columns[i].ColumnName + "__OBJ__" + attribute_Id + "__" + attribute_Type + "__false__true";
                            }

                        }


                    }

                    string JSONString = string.Empty;
                    JSONString = JsonConvert.SerializeObject(objDataTable1.Tables[0]);
                    model.Data = JSONString;


                    return new JsonResult() { Data = model };

                }
                else
                {
                    string[] takeSelectedFamilyValues = selectedCategoryId.Split(',');
                    string combineSelectedFamilyID = "";
                    string combineSelectedCategoryID = "";
                    List<int> selectedFamilyIDS = new List<int>();
                    List<string> categoryIds = new List<string>();
                    for (int j = 0; j < takeSelectedFamilyValues.Length; j++)
                    {
                        if (takeSelectedFamilyValues[j].Contains("CAT") && !takeSelectedFamilyValues[j].Contains('!'))
                        {
                            string category_Id = Convert.ToString(takeSelectedFamilyValues[j]);
                            TB_CATEGORY tb_category = new TB_CATEGORY();
                            var tb_categoryList = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category_Id).ToList();
                            categoryIds.Add(category_Id);
                            foreach (var category in tb_categoryList)
                            {
                                categoryIds.Add(category.CATEGORY_ID);
                                var tb_categoryLists = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category.CATEGORY_ID).ToList();
                                foreach (var categorys in tb_categoryLists)
                                {
                                    categoryIds.Add(categorys.CATEGORY_ID);
                                    string category_ID = categorys.CATEGORY_ID;
                                    do
                                    {
                                        tb_category = _dbcontext.TB_CATEGORY.Where(s => s.PARENT_CATEGORY == category_ID).FirstOrDefault();
                                        if (tb_category != null)
                                        {
                                            category_ID = tb_category.CATEGORY_ID;
                                            categoryIds.Add(tb_category.CATEGORY_ID);
                                        }
                                    } while (tb_category != null);

                                }
                            }

                            var catalogFamilies = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                                   join family in _dbcontext.TB_FAMILY on catalogFamily.FAMILY_ID equals family.FAMILY_ID
                                                   where catalogFamily.CATALOG_ID == catalogId && family.FLAG_RECYCLE == "A" && catalogFamily.FLAG_RECYCLE == "A" && categoryIds.Contains(catalogFamily.CATEGORY_ID)
                                                   select new
                                                   {
                                                       catalogFamily.FAMILY_ID,
                                                       catalogFamily.CATALOG_ID,
                                                       catalogFamily.CATEGORY_ID,
                                                       catalogFamily.FLAG_RECYCLE,
                                                       catalogFamily.SORT_ORDER
                                                   }).Distinct().ToList();

                            foreach (var family in catalogFamilies)
                            {
                                if (combineSelectedFamilyID != "")
                                {
                                    combineSelectedFamilyID = combineSelectedFamilyID + "," + family.FAMILY_ID;
                                }
                                else
                                {
                                    combineSelectedFamilyID = (family.FAMILY_ID).ToString();
                                }

                            }
                        }
                    }
                    foreach (var selectingCategoryID in categoryIds)
                    {
                        if (combineSelectedCategoryID != "")
                        {
                            combineSelectedCategoryID = combineSelectedCategoryID + "," + selectingCategoryID;
                        }
                        else
                        {
                            combineSelectedCategoryID = selectingCategoryID;
                        }

                    }
                    string[] arrSelectedValues = selectedCategoryId.Split('~');
                    string seperateCategoryIDValues = combineSelectedCategoryID;
                    string seperateFmilyIDValues = combineSelectedFamilyID;
                    for (int i = 0; i < arrSelectedValues.Length; i++)
                    {
                        if (arrSelectedValues[i].Contains("CAT"))
                        {
                            if (seperateCategoryIDValues != "")
                            {
                                seperateCategoryIDValues = seperateCategoryIDValues + "," + Regex.Replace(arrSelectedValues[i], @"[^0-9a-zA-Z]+", ",");
                            }
                            else
                            {
                                seperateCategoryIDValues = Regex.Replace(arrSelectedValues[i], @"[^0-9a-zA-Z]+", ",");
                            }
                        }
                        else
                        {
                            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
                            if (arrSelectedValues[i] != "")
                            {
                                if (seperateFmilyIDValues != "")
                                {
                                    seperateFmilyIDValues = seperateFmilyIDValues + "," + arrSelectedValues[i];
                                }
                                else
                                {
                                    seperateFmilyIDValues = arrSelectedValues[i];
                                }
                            }
                        }
                    }
                    seperateFmilyIDValues = Regex.Replace(seperateFmilyIDValues, @"[^0-9a-zA-Z]+", ",");
                    seperateFmilyIDValues = seperateFmilyIDValues.TrimEnd(',');

                    if (seperateFmilyIDValues != null && seperateFmilyIDValues != "")
                    {
                        string[] familyIds = seperateFmilyIDValues.Split(',');
                        foreach (var familyId in familyIds)
                        {
                            int currentFamId = Convert.ToInt32(familyId);
                            var catalogSubFamilies = (from catalogFamily in _dbcontext.TB_CATALOG_FAMILY
                                                      join family in _dbcontext.TB_FAMILY on catalogFamily.FAMILY_ID equals family.FAMILY_ID
                                                      where catalogFamily.CATALOG_ID == catalogId && family.FLAG_RECYCLE == "A" && catalogFamily.FLAG_RECYCLE == "A" && family.PARENT_FAMILY_ID == currentFamId
                                                      select new
                                                      {
                                                          catalogFamily.FAMILY_ID,
                                                          catalogFamily.CATALOG_ID,
                                                          catalogFamily.CATEGORY_ID,
                                                          catalogFamily.FLAG_RECYCLE,
                                                          catalogFamily.SORT_ORDER
                                                      }).Distinct().ToList();

                            if (catalogSubFamilies != null)
                            {
                                foreach (var subFamId in catalogSubFamilies)
                                {
                                    string subFamilyId = "," + subFamId.FAMILY_ID + ",";
                                    if (!seperateFmilyIDValues.Contains(subFamilyId))
                                    {
                                        seperateFmilyIDValues = seperateFmilyIDValues + "," + subFamId.FAMILY_ID;
                                    }
                                }
                            }
                        }
                    }

                    seperateCategoryIDValues = seperateCategoryIDValues.TrimEnd(',');
                    var dsCategorySearchResult = new DataSet();
                    var model = new DashBoardModel();
                    if (seperateFmilyIDValues != "")
                    {
                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_LS_InvertedProducts";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.AddWithValue("@CATALOG_ID", catalogId);
                            objSqlCommand.Parameters.AddWithValue("@ID", "id");
                            objSqlCommand.Parameters.AddWithValue("@OPTION", "APPLYFILTER");
                            objSqlCommand.Parameters.AddWithValue("@PAGENO", Pageno);
                            objSqlCommand.Parameters.AddWithValue("@PERPAGECOUNT", Perpagecount);
                            objSqlCommand.Parameters.AddWithValue("@CATEGORYID", seperateCategoryIDValues);
                            objSqlCommand.Parameters.AddWithValue("@FAMILYID", seperateFmilyIDValues);
                            objSqlCommand.Parameters.AddWithValue("@TEMPLATE_ID", selectedTemplateId);
                            objSqlConnection.Open();
                            var da = new SqlDataAdapter(objSqlCommand);
                            da.Fill(dsCategorySearchResult);
                            objSqlConnection.Close();
                        }
                    }
                    else
                    {
                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_LS_InvertedProducts";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.AddWithValue("@CATALOG_ID", catalogId);
                            objSqlCommand.Parameters.AddWithValue("@ID", "id");
                            objSqlCommand.Parameters.AddWithValue("@OPTION", "APPLYFILTERCATEGORYID");
                            objSqlCommand.Parameters.AddWithValue("@PAGENO", Pageno);
                            objSqlCommand.Parameters.AddWithValue("@PERPAGECOUNT", Perpagecount);
                            objSqlCommand.Parameters.AddWithValue("@CATEGORYID", seperateCategoryIDValues);
                            objSqlCommand.Parameters.AddWithValue("@TEMPLATE_ID", selectedTemplateId);
                            objSqlConnection.Open();
                            var da = new SqlDataAdapter(objSqlCommand);
                            da.Fill(dsCategorySearchResult);
                            objSqlConnection.Close();
                        }
                    }
                    System.Web.HttpContext.Current.Session["ModifiedKendoProductCount"] = dsCategorySearchResult.Tables[1];
                    if (dsCategorySearchResult.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataColumn columns in dsCategorySearchResult.Tables[0].Columns)
                        {
                            PTColumns objPTColumns = new PTColumns();
                            objPTColumns.Caption = columns.Caption;
                            objPTColumns.ColumnName = columns.ColumnName;//.Split(new[] {"__"}, StringSplitOptions.None)[0];
                            model.Columns.Add(objPTColumns);
                        }
                    }
                    string JSONString = string.Empty;
                    JSONString = JsonConvert.SerializeObject(dsCategorySearchResult.Tables[0]);
                    model.Data = JSONString;

                    return new JsonResult() { Data = model };
                }


            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InvertedProductController : GetInvertedProducts", objexception);
                return null;
            }
        }


        [System.Web.Http.HttpPost]
        public JsonResult GetInvertedSearchProducts(int catalogId, int Pageno, int Perpagecount, int usewildcards, string selectedCategoryId, int selectedTemplateId, JArray searchContent)
        {
            try
            {
                if (selectedCategoryId == "0" || selectedCategoryId == "undefined")
                {
                    var searchText = searchContent[1].ToString();
                    var attrName = searchContent[0].ToString();
                    var dynamicColumns = new Dictionary<string, string>();
                    var sb = new StringBuilder();
                    var sw = new StringWriter(sb);
                    JsonWriter jsonWriter = new JsonTextWriter(sw);
                    var dashBoardmodel = new List<DashBoardModel>();
                    var model = new DashBoardModel();
                    SqlDataAdapter da = null;
                    string connction = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                    string id = Guid.NewGuid().ToString();
                    DataSet prodListNew1 = new DataSet();
                    da = new SqlDataAdapter("EXEC STP_LS_InvertedProducts " + catalogId + ",id,SEARCHRESULT," + Pageno + "," + Perpagecount + "," + selectedTemplateId + ",'" + searchText + "','" + attrName + "'," + usewildcards, connction);
                    var objDataTable1 = new DataSet();
                    da.Fill(objDataTable1);
                    System.Web.HttpContext.Current.Session["ModifiedKendoProductCount"] = objDataTable1.Tables[1];
                    if (objDataTable1.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataColumn columns in objDataTable1.Tables[0].Columns)
                        {
                            PTColumns objPTColumns = new PTColumns();
                            objPTColumns.Caption = columns.Caption;
                            objPTColumns.ColumnName = columns.ColumnName;//.Split(new[] {"__"}, StringSplitOptions.None)[0];
                            model.Columns.Add(objPTColumns);
                        }
                    }
                    string JSONString = string.Empty;
                    JSONString = JsonConvert.SerializeObject(objDataTable1.Tables[0]);
                    model.Data = JSONString;
                    dashBoardmodel.Add(model);
                    model = new DashBoardModel();
                    if (objDataTable1.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataColumn columns in objDataTable1.Tables[1].Columns)
                        {
                            PTColumns objPTColumns = new PTColumns();
                            objPTColumns.Caption = columns.Caption;
                            objPTColumns.ColumnName = columns.ColumnName;//.Split(new[] {"__"}, StringSplitOptions.None)[0];
                            model.Columns.Add(objPTColumns);
                        }
                    }

                    JSONString = JsonConvert.SerializeObject(objDataTable1.Tables[1]);
                    model.Data = JSONString;
                    dashBoardmodel.Add(model);

                    return new JsonResult() { Data = dashBoardmodel };
                }
                else
                {
                    var dashBoardmodel = new List<DashBoardModel>();
                    var searchText = searchContent[1].ToString();
                    var attrName = searchContent[0].ToString();
                    string[] arrSelectedValues = selectedCategoryId.Split('~');
                    string seperateCategoryIDValues = "";
                    string seperateFmilyIDValues = "";
                    for (int i = 0; i < arrSelectedValues.Length; i++)
                    {
                        if (arrSelectedValues[i].Contains("CAT"))
                        {
                            if (seperateCategoryIDValues != "")
                            {
                                seperateCategoryIDValues = seperateCategoryIDValues + "," + Regex.Replace(arrSelectedValues[i], @"[^0-9a-zA-Z]+", ",");
                            }
                            else
                            {
                                seperateCategoryIDValues = Regex.Replace(arrSelectedValues[i], @"[^0-9a-zA-Z]+", ",");
                            }
                        }
                        else
                        {
                            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
                            if (arrSelectedValues[i] != "")
                            {
                                if (seperateFmilyIDValues != "")
                                {
                                    seperateFmilyIDValues = seperateFmilyIDValues + "," + arrSelectedValues[i];
                                }
                                else
                                {
                                    seperateFmilyIDValues = arrSelectedValues[i];
                                }
                            }
                        }
                    }
                    seperateFmilyIDValues = Regex.Replace(seperateFmilyIDValues, @"[^0-9a-zA-Z]+", ",");
                    seperateFmilyIDValues = seperateFmilyIDValues.TrimEnd(',');
                    seperateCategoryIDValues = seperateCategoryIDValues.TrimEnd(',');
                    var dsCategorySearchResult = new DataSet();
                    var model = new DashBoardModel();
                    if (seperateFmilyIDValues != "")
                    {
                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_LS_InvertedProducts";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.AddWithValue("@CATALOG_ID", catalogId);
                            objSqlCommand.Parameters.AddWithValue("@ID", "id");
                            objSqlCommand.Parameters.AddWithValue("@OPTION", "APPLYSEARCHFILTERFAMILYIDCATEGORYID");
                            objSqlCommand.Parameters.AddWithValue("@PAGENO", Pageno);
                            objSqlCommand.Parameters.AddWithValue("@PERPAGECOUNT", Perpagecount);
                            objSqlCommand.Parameters.AddWithValue("@SEARCHTEXT", searchText);
                            objSqlCommand.Parameters.AddWithValue("@SEARCHATTRNAME", attrName);
                            objSqlCommand.Parameters.AddWithValue("@CATEGORYID", seperateCategoryIDValues);
                            objSqlCommand.Parameters.AddWithValue("@FAMILYID", seperateFmilyIDValues);
                            objSqlCommand.Parameters.AddWithValue("@TEMPLATE_ID", selectedTemplateId);
                            objSqlConnection.Open();
                            var da = new SqlDataAdapter(objSqlCommand);
                            da.Fill(dsCategorySearchResult);
                            objSqlConnection.Close();
                        }
                    }
                    else
                    {
                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_LS_InvertedProducts";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.AddWithValue("@CATALOG_ID", catalogId);
                            objSqlCommand.Parameters.AddWithValue("@ID", "id");
                            objSqlCommand.Parameters.AddWithValue("@OPTION", "APPLYSEARCHFILTERCATEGORYID");
                            objSqlCommand.Parameters.AddWithValue("@PAGENO", Pageno);
                            objSqlCommand.Parameters.AddWithValue("@PERPAGECOUNT", Perpagecount);
                            objSqlCommand.Parameters.AddWithValue("@SEARCHTEXT", searchText);
                            objSqlCommand.Parameters.AddWithValue("@SEARCHATTRNAME", attrName);
                            objSqlCommand.Parameters.AddWithValue("@CATEGORYID", seperateCategoryIDValues);
                            objSqlCommand.Parameters.AddWithValue("@FAMILYID", seperateFmilyIDValues);
                            objSqlCommand.Parameters.AddWithValue("@TEMPLATE_ID", selectedTemplateId);
                            objSqlConnection.Open();
                            var da = new SqlDataAdapter(objSqlCommand);
                            da.Fill(dsCategorySearchResult);
                            objSqlConnection.Close();
                        }
                    }
                    System.Web.HttpContext.Current.Session["ModifiedKendoProductCount"] = dsCategorySearchResult.Tables[1];
                    if (dsCategorySearchResult.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataColumn columns in dsCategorySearchResult.Tables[0].Columns)
                        {
                            PTColumns objPTColumns = new PTColumns();
                            objPTColumns.Caption = columns.Caption;
                            objPTColumns.ColumnName = columns.ColumnName;//.Split(new[] {"__"}, StringSplitOptions.None)[0];
                            model.Columns.Add(objPTColumns);
                        }
                    }
                    string JSONString = string.Empty;
                    JSONString = JsonConvert.SerializeObject(dsCategorySearchResult.Tables[0]);
                    model.Data = JSONString;
                    dashBoardmodel.Add(model);
                    model = new DashBoardModel();
                    if (dsCategorySearchResult.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataColumn columns in dsCategorySearchResult.Tables[1].Columns)
                        {
                            PTColumns objPTColumns = new PTColumns();
                            objPTColumns.Caption = columns.Caption;
                            objPTColumns.ColumnName = columns.ColumnName;//.Split(new[] {"__"}, StringSplitOptions.None)[0];
                            model.Columns.Add(objPTColumns);
                        }
                    }

                    JSONString = JsonConvert.SerializeObject(dsCategorySearchResult.Tables[1]);
                    model.Data = JSONString;
                    dashBoardmodel.Add(model);
                    return new JsonResult() { Data = dashBoardmodel };
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InvertedProductController : GetInvertedSearchProducts", objexception);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public JsonResult GetInvertedSearchSubProducts(int catalogId, int Pageno, int Perpagecount, int usewildcards, JArray searchContent)
        {
            try
            {
                var searchText = searchContent[1].ToString();
                var attrName = searchContent[0].ToString();
                var dynamicColumns = new Dictionary<string, string>();
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                JsonWriter jsonWriter = new JsonTextWriter(sw);
                var dashBoradModel = new List<DashBoardModel>();
                SqlDataAdapter da = null;
                string connction = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                string id = Guid.NewGuid().ToString();
                DataSet prodListNew1 = new DataSet();
                var model = new DashBoardModel();
                da = new SqlDataAdapter("EXEC STP_LS_InvertedProducts " + catalogId + ",id,SEARCHRESULTSUB," + Pageno + "," + Perpagecount + ",'" + searchText + "','" + attrName + "'," + usewildcards, connction);
                var objDataTable1 = new DataSet();
                da.Fill(objDataTable1);
                if (objDataTable1.Tables[0].Rows.Count > 0)
                {
                    foreach (DataColumn columns in objDataTable1.Tables[0].Columns)
                    {
                        PTColumns objPTColumns = new PTColumns();
                        objPTColumns.Caption = columns.Caption;
                        objPTColumns.ColumnName = columns.ColumnName;//.Split(new[] {"__"}, StringSplitOptions.None)[0];
                        model.Columns.Add(objPTColumns);
                    }
                }


                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(objDataTable1.Tables[0]);
                model.Data = JSONString;
                dashBoradModel.Add(model);
                model = new DashBoardModel();
                if (objDataTable1.Tables[1].Rows.Count > 0)
                {
                    foreach (DataColumn columns in objDataTable1.Tables[1].Columns)
                    {
                        PTColumns objPTColumns = new PTColumns();
                        objPTColumns.Caption = columns.Caption;
                        objPTColumns.ColumnName = columns.ColumnName;//.Split(new[] {"__"}, StringSplitOptions.None)[0];
                        model.Columns.Add(objPTColumns);
                    }
                }


                JSONString = JsonConvert.SerializeObject(objDataTable1.Tables[1]);
                model.Data = JSONString;
                dashBoradModel.Add(model);
                return new JsonResult() { Data = dashBoradModel };


            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InvertedProductController : GetInvertedSubProducts", objexception);
                return null;
            }
        }

        private DataTable GetTransposedProductTable(DataTable productInfo, DataTable productAttributes)
        {

            // To remove duplicates from datatable
            //Hashtable hTable = new Hashtable();
            //ArrayList duplicateList = new ArrayList();

            ////Add list of all the unique item value to hashtable, which stores combination of key, value pair.
            ////And add duplicate item value in arraylist.
            //foreach (DataRow drow in productInfo.Rows)
            //{
            //    if (hTable.Contains(drow["PRODUCT_ID"]))
            //        duplicateList.Add(drow);
            //    else
            //        hTable.Add(drow["PRODUCT_ID"], string.Empty);
            //}

            ////Removing a list of duplicate items from datatable.
            //foreach (DataRow dRow in duplicateList)
            //    productInfo.Rows.Remove(dRow);

            var dtnew = new DataTable();
            dtnew.Columns.Add("CATALOG_ID", typeof(int));
            dtnew.Columns.Add("FAMILY_ID", typeof(int));
            dtnew.Columns.Add("PRODUCT_ID", typeof(int));

            var dcpks = new DataColumn[3];

            {
                dcpks[0] = dtnew.Columns["CATALOG_ID"];
                dcpks[1] = dtnew.Columns["FAMILY_ID"];
                dcpks[2] = dtnew.Columns["PRODUCT_ID"];
            }

            // dtnew.Constraints.Add("PK", dcpks, true);
            foreach (DataRow dr in productAttributes.Rows)
            {
                {
                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                    dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                    {
                        productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    else
                        productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                    DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"]);
                    DataTable dtsel;
                    dtsel = productInfo.Clone();
                    for (int i = 0; i < drs.Length; i++)
                    {
                        dtsel.ImportRow(drs[i]);

                    }

                    try
                    {
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" && dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                            {
                                bool stringValue = false;
                                foreach (DataRow dr1 in dtsel.Rows)
                                {
                                    if (dr1["STRING_VALUE"].ToString().Length == 0)
                                    {
                                        stringValue = true;
                                        break;
                                    }
                                }
                                if (stringValue == false)
                                {
                                    dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                    dtsel.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                }
                            }
                    }
                    catch (Exception) { }
                    dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                    dtsel.Dispose();
                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                    dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                    {
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                    else
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";

                }
            }
            dtnew.Columns.Add("id");
            dtnew.Columns.Add("CATEGORY_ID");
            foreach (DataRow dr in dtnew.Rows)
            {
                int fid = Convert.ToInt32(dr["FAMILY_ID"]);
                int catalog_id = Convert.ToInt32(dr["CATALOG_ID"]);
                var category_id = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(a => a.FAMILY_ID == fid && a.CATALOG_ID == catalog_id);
                dr["id"] = dr["FAMILY_ID"].ToString();
                dr["CATEGORY_ID"] = category_id.CATEGORY_ID;
            }
            dtnew.DefaultView.Sort = "[PRODUCT_ID] ASC";
            return dtnew;
        }

        private DataTable GetTransposedProductTableSubproduct(DataTable productInfo, DataTable productAttributes)
        {

            var dtnew = new DataTable();
            dtnew.Columns.Add("CATALOG_ID", typeof(int));
            dtnew.Columns.Add("FAMILY_ID", typeof(int));
            dtnew.Columns.Add("MAINPRODUCT_ID", typeof(int));
            dtnew.Columns.Add("SUBPRODUCT_ID", typeof(int));


            var dcpks = new DataColumn[4];

            {
                dcpks[0] = dtnew.Columns["CATALOG_ID"];
                dcpks[1] = dtnew.Columns["FAMILY_ID"];
                dcpks[2] = dtnew.Columns["MAINPRODUCT_ID"];
                dcpks[3] = dtnew.Columns["SUBPRODUCT_ID"];

            }

            dtnew.Constraints.Add("PK", dcpks, true);
            foreach (DataRow dr in productAttributes.Rows)
            {
                {
                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                    dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                    {
                        productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    else
                        productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                    DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"]);
                    DataTable dtsel;
                    dtsel = productInfo.Clone();
                    for (int i = 0; i < drs.Length; i++)
                    {
                        dtsel.ImportRow(drs[i]);

                    }

                    try
                    {
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" && dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                            {
                                bool stringValue = false;
                                foreach (DataRow dr1 in dtsel.Rows)
                                {
                                    if (dr1["STRING_VALUE"].ToString().Length == 0)
                                    {
                                        stringValue = true;
                                        break;
                                    }
                                }
                                if (stringValue == false)
                                {
                                    dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                    dtsel.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                }
                            }
                    }
                    catch (Exception) { }
                    dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                    dtsel.Dispose();
                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                    dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                    {
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                    else
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";

                }
            }
            dtnew.Columns.Add("id");
            dtnew.Columns.Add("CATEGORY_ID");
            foreach (DataRow dr in dtnew.Rows)
            {
                int fid = Convert.ToInt32(dr["FAMILY_ID"]);
                int catalog_id = Convert.ToInt32(dr["CATALOG_ID"]);
                var category_id = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(a => a.FAMILY_ID == fid && a.CATALOG_ID == catalog_id).CATEGORY_ID;
                //dr["id"] = Convert.ToString("~" + dr["FAMILY_ID"].ToString());
                dr["id"] = dr["FAMILY_ID"].ToString();
                dr["CATEGORY_ID"] = category_id;
            }
            return dtnew;
        }

        [System.Web.Http.HttpGet]
        public JsonResult GetInvertedSubProducts(int catalogId, int Pageno, int Perpagecount)
        {
            try
            {
                var dynamicColumns = new Dictionary<string, string>();
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                JsonWriter jsonWriter = new JsonTextWriter(sw);
                var model = new DashBoardModel();
                SqlDataAdapter da = null;
                string connction = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                string id = Guid.NewGuid().ToString();
                DataSet prodListNew1 = new DataSet();
                da = new SqlDataAdapter("EXEC STP_LS_InvertedProducts " + catalogId + ",id,ATTRIBUTELISTSUBPRODUCTS," + Pageno + "," + Perpagecount, connction);
                var objDataTable1 = new DataTable();
                da.SelectCommand.CommandTimeout = 0;
                da.Fill(objDataTable1);
                if (objDataTable1.Rows.Count > 0)
                {
                    foreach (DataColumn columns in objDataTable1.Columns)
                    {
                        PTColumns objPTColumns = new PTColumns();
                        objPTColumns.Caption = columns.Caption;
                        objPTColumns.ColumnName = columns.ColumnName;//.Split(new[] {"__"}, StringSplitOptions.None)[0];
                        model.Columns.Add(objPTColumns);
                    }
                }


                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(objDataTable1);
                model.Data = JSONString;
                return new JsonResult() { Data = model };


            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InvertedProductController : GetInvertedSubProducts", objexception);
                return null;
            }
        }
        public static string CreateTable(string tableName, DataTable table)
        {
            try
            {
                string sqlsc = "CREATE TABLE " + tableName + "(";
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    if (!table.Columns[i].ColumnName.Contains('['))
                    {
                        sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                    }
                    else
                    {
                        sqlsc += "\n" + table.Columns[i].ColumnName + "";
                    }
                    if (table.Columns[i].DataType.ToString().Contains("System.String"))
                        sqlsc += "nvarchar(max) ";
                    else
                        sqlsc += "int";
                    sqlsc += ",";
                }
                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InvertedProductController : CreateTable", objexception);
                return null;
            }
        }


        [System.Web.Http.HttpPost]
        public string Getattribdatatype(int attrib_id)
        {
            try
            {
                var attributedatatype = _dbcontext.TB_ATTRIBUTE.Where(a => a.ATTRIBUTE_ID == attrib_id)
              .Select(a => a.ATTRIBUTE_DATATYPE).FirstOrDefault();


                return attributedatatype.ToString();
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InvertedProductController : Getattribdatatype", objexception);
                return null;
            }
        }

        #region Product list Export

        [System.Web.Http.HttpPost]
        public string ExportInvertedProducts(int catalogId, string opt, bool displayIdcolumns, string exportFormatType, int selectedTemplateId)
        {
            try
            {
                exportFormatType = exportFormatType.ToLower();
                SqlDataAdapter da = null;
                string connction = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                string id = Guid.NewGuid().ToString();
                DataSet prodListNew = new DataSet();
                da = new SqlDataAdapter("EXEC STP_LS_InvertedProducts " + catalogId + ",id," + opt + "," + 0 + "," + 0, connction);
                da.SelectCommand.CommandTimeout = 0;
                da.Fill(prodListNew);
                string count = string.Empty;
                if (prodListNew != null && prodListNew.Tables[0].Rows.Count > 0)
                {
                    count = prodListNew.Tables[0].Rows[0]["COUNT"].ToString();
                }

                int productCount = Convert.ToInt32(count);

                SqlDataAdapter dataAdaptor = null;

                dataAdaptor = new SqlDataAdapter("EXEC STP_LS_InvertedProducts " + catalogId + ",id,ATTRIBUTELIST," + 1 + "," + productCount + "," + selectedTemplateId, connction);
                var objdataset = new DataSet();
                dataAdaptor.SelectCommand.CommandTimeout = 0;
                dataAdaptor.Fill(objdataset);
                ExportProductDataSetToExcel(objdataset, displayIdcolumns);
                return "ItemListExport" + exportFormatType;

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InvertedProductController : GetInvertedProducts", objexception);
                return null;
            }
        }

        public void ExportProductDataSetToExcel(DataSet invertedProduct, bool displayIdColumns)
        {
            try
            {
                System.Web.HttpContext.Current.Session["ExportTable"] = null;
                invertedProduct.Tables[0].Columns.Add("Action").SetOrdinal(0);

                if (invertedProduct.Tables.Count > 0)
                {
                    if (invertedProduct.Tables[0].Columns.Contains("FAMILY_ID"))
                    {
                        invertedProduct.Tables[0].Columns["PRODUCT_ID"].SetOrdinal(invertedProduct.Tables[0].Columns.IndexOf("FAMILY_ID") + 1);
                        DataColumn Col = invertedProduct.Tables[0].Columns.Add("FAMILY_NAME", System.Type.GetType("System.String"));
                        int index = invertedProduct.Tables[0].Columns.IndexOf("FAMILY_ID");
                        Col.SetOrdinal(index + 1);
                    }
                    if (invertedProduct.Tables[0].Columns.Contains("CATALOG_NAME"))
                    {
                        invertedProduct.Tables[0].Columns["CATEGORY_ID"].SetOrdinal(invertedProduct.Tables[0].Columns.IndexOf("CATALOG_NAME") + 1);
                        DataColumn Col = invertedProduct.Tables[0].Columns.Add("CATEGORY_NAME", System.Type.GetType("System.String"));
                        int index = invertedProduct.Tables[0].Columns.IndexOf("CATEGORY_ID");
                        Col.SetOrdinal(index + 1);
                    }
                }



                if (invertedProduct.Tables.Count > 0)
                {
                    string connction = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                    string invertedproductsessionid, sqlString = string.Empty;
                    invertedproductsessionid = Guid.NewGuid().ToString();
                    if (invertedProduct.Tables[0].Columns.Contains("FAMILY_ID"))
                    {

                        using (var objSqlConnection = new SqlConnection(connction))
                        {

                            objSqlConnection.Open();
                            sqlString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##EXPORTINVERTEDPRODUCT" + invertedproductsessionid + "'')BEGIN DROP TABLE [##EXPORTINVERTEDPRODUCT" + invertedproductsessionid + "] END')";
                            SqlCommand _DBCommand = new SqlCommand(sqlString, objSqlConnection);
                            _DBCommand.ExecuteNonQuery();
                            sqlString = CreateTable("[##EXPORTINVERTEDPRODUCT" + invertedproductsessionid + "]", invertedProduct.Tables[0]);
                            SqlCommand cmdPick = new SqlCommand();
                            cmdPick.Connection = objSqlConnection;
                            cmdPick.CommandText = sqlString;
                            cmdPick.CommandType = CommandType.Text;
                            cmdPick.ExecuteNonQuery();
                            var bulkCopysp = new SqlBulkCopy(connction)
                            {
                                DestinationTableName = "[##EXPORTINVERTEDPRODUCT" + invertedproductsessionid + "]"
                            };
                            bulkCopysp.WriteToServer(invertedProduct.Tables[0]);

                            invertedProduct.Clear();
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "UpdateFamilyCategoryinvertedExport";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.Add("@sessionID", SqlDbType.VarChar).Value = invertedproductsessionid;
                            var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                            objSqlDataAdapter.Fill(invertedProduct);


                        }
                        //foreach (DataRow row in invertedProduct.Tables[0].Rows)
                        //{
                        //    var family_Id = row["FAMILY_ID"].ToString();
                        //    if (family_Id != null && family_Id != "")
                        //    {
                        //        int family_id = Convert.ToInt32(family_Id);
                        //        TB_FAMILY family = _dbcontext.TB_FAMILY.Where(s => s.FAMILY_ID == family_id).FirstOrDefault();
                        //        if (family != null && family.FAMILY_ID > 0)
                        //        {
                        //            row["FAMILY_NAME"] = family.FAMILY_NAME;
                        //        }
                        //    }
                        //    var category_Id = row["CATEGORY_ID"].ToString();
                        //    if (category_Id != null && category_Id != "")
                        //    {
                        //        string category_Name = Convert.ToString(category_Id);
                        //        TB_CATEGORY category = _dbcontext.TB_CATEGORY.Where(s => s.CATEGORY_SHORT == category_Name).FirstOrDefault();
                        //        if (category != null)
                        //        {
                        //            row["CATEGORY_NAME"] = category.CATEGORY_NAME;
                        //        }
                        //    }
                        //}
                    }
                }


                if (!displayIdColumns)
                {
                    if (invertedProduct.Tables[0].Columns.Contains("FAMILY_ID"))
                    {
                        invertedProduct.Tables[0].Columns.Remove("FAMILY_ID");
                    }
                    if (invertedProduct.Tables[0].Columns.Contains("CATALOG_ID"))
                    {
                        invertedProduct.Tables[0].Columns.Remove("CATALOG_ID");
                    }
                    if (invertedProduct.Tables[0].Columns.Contains("CATEGORY_ID"))
                    {
                        invertedProduct.Tables[0].Columns.Remove("CATEGORY_ID");
                    }
                    if (invertedProduct.Tables[0].Columns.Contains("PRODUCT_ID"))
                    {
                        invertedProduct.Tables[0].Columns.Remove("PRODUCT_ID");
                    }
                }
                System.Web.HttpContext.Current.Session["ExportTable"] = invertedProduct.Tables[0];
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        public string CreateTable1(string tableName, DataTable objtable)
        {
            try
            {

                if (objtable.Columns.Contains(System.Web.HttpContext.Current.Session["CustomerItemNo"].ToString()))
                {
                    objtable.Columns[System.Web.HttpContext.Current.Session["CustomerItemNo"].ToString()].ColumnName = "CATALOG_ITEM_NO";
                }
                if (objtable.Columns.Contains("SUBITEM#"))
                {
                    objtable.Columns["SUBITEM#"].ColumnName = "SUBCATALOG_ITEM_NO";
                }
                string sqlsc = "CREATE TABLE " + tableName + "(";
                if (objtable.Columns.Count > 0)
                {
                    for (int i = 0; i < objtable.Columns.Count; i++)
                    {
                        if (!objtable.Columns[i].ColumnName.Contains('['))
                        {
                            sqlsc += "\n [" + objtable.Columns[i].ColumnName + "] ";
                        }
                        else
                        {
                            sqlsc += "\n" + objtable.Columns[i].ColumnName + "";
                        }

                        if (objtable.Columns[i].ColumnName.Contains("CATEGORY_ID"))
                        {
                            sqlsc += "nvarchar(50) ";
                        }
                        else if (objtable.Columns[i].ColumnName.Contains("CATALOG_ID"))
                        {
                            sqlsc += "nvarchar(4000)";
                        }
                        else if (objtable.Columns[i].DataType.ToString().Contains("System.String") ||
                                 objtable.Columns[i].ColumnName.Contains("SUBCATID") ||
                                 objtable.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                        {
                            sqlsc += "varchar(8000) ";
                        }
                        else
                        {
                            sqlsc += "varchar(8000) ";
                        }
                        sqlsc += ",";
                    }
                }
                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : CreateTable", ex);
                return null;
            }
        }




        [System.Web.Http.HttpPost]
        public List<ProductView> CategorySearchFilterTree(int catalogId, string categoryId, int workingCatalogId)
        {
            try
            {
                if (catalogId != 0)
                {
                    return GetCategoriesForTree(catalogId, categoryId, workingCatalogId);
                }
                else
                {
                    var objpProductViews = new List<ProductView>();
                    return objpProductViews;
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InvertedProductController : CategorySearchFilterTree", objexception);
                return null;
            }
        }


        public List<ProductView> GetCategoriesForTree(int catalogId, string id, int workingCatalogId)
        {


            try
            {
                using (var tnso = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions
                {
                    IsolationLevel = System.Transactions.IsolationLevel.ReadUncommitted
                }))
                {
                    int customer_Id = 2;
                    if (id == "undefined")
                    {
                        if (catalogId != 1)
                        {
                            Stopwatch sw = Stopwatch.StartNew();
                            sw.Stop();

                            var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                                tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                                .Where(x => x.tpf.User_Name == User.Identity.Name)
                                .Select(x => x.tps);
                            var customerSettings = customersettings.FirstOrDefault();
                            bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                            var firstOrDefault = customersettings.FirstOrDefault();
                            bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                            customer_Id = Convert.ToInt32(customerSettings.CustomerId);
                            var aa =
                                _dbcontext.TB_CATALOG_SECTIONS.Where(
                                    a => a.CATALOG_ID == catalogId && _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.PARENT_CATEGORY).FirstOrDefault() == "0")
                                    .Select(
                                        a =>
                                            new ProductView
                                            {
                                                id = a.CATEGORY_ID,
                                                CATEGORY_ID = a.CATEGORY_ID,
                                                CATEGORY_SHORT = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.CATEGORY_SHORT).FirstOrDefault(),
                                                CUSTOMER_ID = _dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name).CustomerId,
                                                CATEGORY_NAME = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                                CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                                CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                                hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) || _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A").Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                                SORT_ORDER = a.SORT_ORDER,
                                                CategoryIdinNavigator = categoryidinnavigator,
                                                FamilyandRelatedFamily = familyandrelatedfamily,
                                                spriteCssClass = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.IS_CLONE).FirstOrDefault() == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category"
                                            }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();

                            return aa;
                        }
                        else
                        {

                            var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                              tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                              .Where(x => x.tpf.User_Name == User.Identity.Name)
                              .Select(x => x.tps);
                            int userroleid = 0;
                            int userroletype = 0;
                            var SuperAdminCheck = _dbcontext.vw_aspnet_UsersInRoles
                            .Join(_dbcontext.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                            .Join(_dbcontext.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                            .Where(x => x.tcptps.tpf.UserName == User.Identity.Name).ToList();
                            if (SuperAdminCheck.Any())
                            {
                                userroleid = Convert.ToInt16(SuperAdminCheck[0].tcp.Role_id);
                                userroletype = Convert.ToInt16(SuperAdminCheck[0].tcp.RoleType);
                            }

                            var customerSettings = customersettings.FirstOrDefault();
                            bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                            var firstOrDefault = customersettings.FirstOrDefault();
                            bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                            customer_Id = Convert.ToInt32(customerSettings.CustomerId);
                            var aa = _dbcontext.TB_CATALOG_SECTIONS.Where(
                                 a => a.CATALOG_ID == catalogId && _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.PARENT_CATEGORY).FirstOrDefault() == "0")
                                 .Select(
                                     a =>
                                         new ProductView
                                         {
                                             id = a.CATEGORY_ID,
                                             CATEGORY_ID = a.CATEGORY_ID,
                                             CATEGORY_SHORT = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_SHORT).FirstOrDefault(),
                                             CUSTOMER_ID = customer_Id,
                                             CATEGORY_NAME = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                             CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                             CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                             hasChildren =
                                                 _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID) || _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                                     .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && c.FLAG_RECYCLE == "A" && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1).Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                             SORT_ORDER = a.SORT_ORDER,
                                             CategoryIdinNavigator = categoryidinnavigator,
                                             FamilyandRelatedFamily = familyandrelatedfamily,
                                             spriteCssClass = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.IS_CLONE).FirstOrDefault() == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category",
                                             @checked = _dbcontext.TB_CATALOG_SECTIONS.Any(x => x.CATALOG_ID == workingCatalogId && x.CATEGORY_ID == a.CATEGORY_ID)
                                         }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id).ToList();




                            if (userroleid == 1)
                            {
                                var names = _dbcontext.TB_CATEGORY
                                  .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf })
                                  .Join(_dbcontext.Customer_Catalog, tcptps => tcptps.tpf.CATALOG_ID, tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                                  .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId, (tcptpscs, cs) => new { tcptpscs, cs })
                                  .Where(x => x.cs.User_Name == User.Identity.Name && x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" && x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" && x.tcptpscs.tcptps.tpf.CATALOG_ID != 1).Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);
                                var results = aa.Where(b => names.Contains(b.id)).ToList();
                                return results;
                            }
                            else
                            {
                                if (userroletype == 3)
                                {
                                    var names = _dbcontext.TB_CATEGORY
                                        .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID,
                                            (tps, tpf) => new { tps, tpf })
                                        .Join(_dbcontext.Customer_Role_Catalog, tcptps => tcptps.tpf.CATALOG_ID,
                                            tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                                        .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId,
                                            (tcptpscs, cs) => new { tcptpscs, cs })
                                        .Where(
                                            x =>
                                                x.cs.User_Name == User.Identity.Name &&
                                                x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" &&
                                                x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" &&
                                                x.tcptpscs.tcptps.tpf.CATALOG_ID != 1 && x.tcptpscs.tcp.RoleId == userroleid &&
                                                x.tcptpscs.tcp.IsActive == true)
                                        .Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);

                                    var results = aa.Where(b => names.Contains(b.id)).ToList();
                                    return results;
                                }
                                else
                                {
                                    var names = _dbcontext.TB_CATEGORY
                                       .Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID,
                                           (tps, tpf) => new { tps, tpf })
                                       .Join(_dbcontext.Customer_Role_Catalog, tcptps => tcptps.tpf.CATALOG_ID,
                                           tcp => tcp.CATALOG_ID, (tcptps, tcp) => new { tcptps, tcp })
                                       .Join(_dbcontext.Customer_User, tcptps => tcptps.tcp.CustomerId, cs => cs.CustomerId,
                                           (tcptpscs, cs) => new { tcptpscs, cs })
                                       .Where(
                                           x =>
                                               x.cs.User_Name == User.Identity.Name &&
                                               x.tcptpscs.tcptps.tps.FLAG_RECYCLE == "A" &&
                                               x.tcptpscs.tcptps.tps.PARENT_CATEGORY == "0" &&
                                               x.tcptpscs.tcptps.tpf.CATALOG_ID != 1 && x.tcptpscs.tcp.RoleId == userroleid)
                                       .Select(x => x.tcptpscs.tcptps.tps.CATEGORY_ID);

                                    var results = aa.Where(b => names.Contains(b.id)).ToList();
                                    return results;
                                }
                            }
                        }
                    }
                    else
                    {
                        string subfamilycategoryid = string.Empty;
                        if (id.Contains("~"))
                        {
                            var catgoryandfamilyid = id.Split('~');
                            subfamilycategoryid = catgoryandfamilyid[0];
                            id = catgoryandfamilyid[1];
                        }
                        var customersettings = _dbcontext.Customer_Settings.Join(_dbcontext.Customer_User, tps => tps.CustomerId,
                              tpf => tpf.CustomerId, (tps, tpf) => new { tps, tpf })
                              .Where(x => x.tpf.User_Name == User.Identity.Name)
                              .Select(x => x.tps);
                        var customerSettings = customersettings.FirstOrDefault();
                        bool categoryidinnavigator = customerSettings != null && customerSettings.DisplayCategoryIdinNavigator;
                        var firstOrDefault = customersettings.FirstOrDefault();
                        bool familyandrelatedfamily = firstOrDefault != null && firstOrDefault.DisplayFamilyandRelatedFamily;
                        customer_Id = Convert.ToInt32(customerSettings.CustomerId);
                        var aa = _dbcontext.TB_CATALOG_SECTIONS.Where(

                            a => a.CATALOG_ID == catalogId && _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == (_dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.PARENT_CATEGORY).FirstOrDefault() == id)
                            .Select(
                                a =>
                                    new ProductView
                                    {
                                        id = a.CATEGORY_ID,
                                        CATEGORY_ID = a.CATEGORY_ID,
                                        CATEGORY_SHORT = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_SHORT).FirstOrDefault(),
                                        CUSTOMER_ID = customer_Id,
                                        CATEGORY_NAME = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && tc.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                        CATALOG_ID = a.TB_CATALOG.CATALOG_ID,
                                        CATALOG_NAME = a.TB_CATALOG.CATALOG_NAME,
                                        hasChildren = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == catalogId && x.tps.PARENT_CATEGORY == a.CATEGORY_ID && x.tps.FLAG_RECYCLE == "A") || _dbcontext.TB_CATALOG_FAMILY.Where(b => b.CATALOG_ID == catalogId && b.FLAG_RECYCLE == "A")
                                                .Any(c => c.CATEGORY_ID == a.CATEGORY_ID && _dbcontext.TB_FAMILY.Where(x => x.ROOT_FAMILY == 1 && x.FLAG_RECYCLE == "A").Select(b => b.FAMILY_ID).Contains(c.FAMILY_ID)),
                                        SORT_ORDER = a.SORT_ORDER,
                                        CategoryIdinNavigator = categoryidinnavigator,
                                        FamilyandRelatedFamily = familyandrelatedfamily,
                                        spriteCssClass = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == a.CATEGORY_ID && a.FLAG_RECYCLE == "A" && tc.CUSTOMER_ID == customer_Id).Select(tcsel => tcsel.IS_CLONE).FirstOrDefault() == 1 || a.CATEGORY_ID.Contains("CLONE") ? "CategoryClone" : "category",
                                        //@checked = catalogId == 1 && _dbcontext.TB_CATALOG_SECTIONS.Where(x => x.CATALOG_ID == workingCatalogId && x.CATEGORY_ID == id).Any()
                                        @checked = catalogId == 1 && _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tps => tps.CATEGORY_ID, tpf => tpf.CATEGORY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == workingCatalogId && x.tps.CATEGORY_ID == a.CATEGORY_ID && x.tps.FLAG_RECYCLE == "A") ? true : false

                                    }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);


                        //var parentCategory = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == id && x.PARENT_CATEGORY!="0").Select(y => y.PARENT_CATEGORY).FirstOrDefault();
                        //if(parentCategory!="" && parentCategory!=null)
                        //{
                        //    id = parentCategory + "|" + id;
                        //}
                        var bb = Array.ConvertAll(
                                                 _dbcontext.TB_CATALOG_FAMILY.Where(
                                                     a => a.CATALOG_ID == catalogId && a.FLAG_RECYCLE == "A" && a.CATEGORY_ID == id && a.TB_FAMILY.ROOT_FAMILY == 1 && a.TB_FAMILY.FLAG_RECYCLE == "A")
                                                     .Select(a
                                                         =>
                                                         new
                                                         {
                                                             a.FAMILY_ID,
                                                             a.TB_FAMILY.FAMILY_NAME,
                                                             a.TB_CATALOG.CATALOG_ID,
                                                             a.TB_CATALOG.CATALOG_NAME,
                                                             hasChildren = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Join(_dbcontext.TB_SUBFAMILY, tcftf => tcftf.tpf.FAMILY_ID, tsf => tsf.SUBFAMILY_ID, (tcftf, tsf) => new { tcftf, tsf }).Any(x => x.tcftf.tps.CATALOG_ID == catalogId && x.tcftf.tpf.FLAG_RECYCLE == "A" && x.tcftf.tpf.PARENT_FAMILY_ID == a.FAMILY_ID && x.tcftf.tps.CATEGORY_ID == id && x.tcftf.tps.FLAG_RECYCLE == "A"),
                                                             ProdCnt =
                                                                 a.TB_FAMILY.TB_PROD_FAMILY.Join(_dbcontext.TB_PRODUCT, tpf => tpf.PRODUCT_ID, tp => tp.PRODUCT_ID, (tpf, tp) => new { tpf, tp }).Where(b => b.tpf.FAMILY_ID == a.FAMILY_ID && b.tpf.FLAG_RECYCLE == "A" && b.tp.FLAG_RECYCLE == "A")
                                                                     .Select(b => b.tpf.PRODUCT_ID)
                                                                     .Distinct()
                                                                     .Count(),
                                                             SubProdCnt =
                                                                 a.TB_FAMILY.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, ts => ts.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (ts, tpf) => new { ts, tpf }).Where(b => b.ts.TB_FAMILY.FAMILY_ID == a.FAMILY_ID && b.tpf.CATALOG_ID == catalogId && b.tpf.FLAG_RECYCLE == "A")
                                                                 .Select(b => b.tpf.SUBPRODUCT_ID)
                                                                 .Distinct()
                                                                 .Count(),
                                                             a.SORT_ORDER
                                                         }).ToArray(),
                                                 a =>
                                                     new ProductView
                                                     {
                                                         id = id + "~" + a.FAMILY_ID.ToString(CultureInfo.InvariantCulture) + "~!" + _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == _dbcontext.TB_CATEGORY.Where(xx => xx.CATEGORY_ID == _dbcontext.TB_CATEGORY.Where(z => z.CATEGORY_ID == id && z.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && xx.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() + "~" + _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == _dbcontext.TB_CATEGORY.Where(z => z.CATEGORY_ID == id && z.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault() + "~" + _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == id && x.PARENT_CATEGORY != "0").Select(y => y.PARENT_CATEGORY).FirstOrDefault(),
                                                         CATEGORY_ID = "~" + a.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                                         CATEGORY_NAME = familyandrelatedfamily ? a.SubProdCnt > 0 ? a.FAMILY_NAME /*+ " (" + a.ProdCnt.ToString(CultureInfo.InvariantCulture) + ")" */: a.FAMILY_NAME /*+ " (" + a.ProdCnt.ToString(CultureInfo.InvariantCulture) + ")"*/
                                                               : a.FAMILY_NAME,
                                                         CATALOG_ID = a.CATALOG_ID,
                                                         CATALOG_NAME = a.CATALOG_NAME,
                                                         hasChildren = a.hasChildren,
                                                         SORT_ORDER = a.SORT_ORDER,
                                                         CategoryIdinNavigator = false,
                                                         FamilyandRelatedFamily = familyandrelatedfamily,
                                                         spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.FAMILY_ID && x.tps.CATEGORY_ID == id && x.tps.FLAG_RECYCLE == "A") ?
                                                       "familyClone" : _dbcontext.TB_CATALOG.Join(_dbcontext.TB_CATALOG_FAMILY, tc => tc.CATALOG_ID, tcfa => tcfa.CATALOG_ID, (tc, tcfa) => new { tc, tcfa }).Any(x => x.tc.CATALOG_ID == catalogId && x.tcfa.CATEGORY_ID == id && x.tc.DEFAULT_FAMILY == a.FAMILY_ID) ? "defaultfamily" : "family",
                                                         @checked = catalogId == 1 && _dbcontext.TB_FAMILY.Join(_dbcontext.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == workingCatalogId && x.tps.FLAG_RECYCLE == "A" && x.tpf.FLAG_RECYCLE == "A" && x.tps.FAMILY_ID == a.FAMILY_ID)


                                                     }).OrderBy(t => t.SORT_ORDER).ThenBy(x => x.id);

                        if (aa.Any())
                        {
                            var res = aa.ToList().Union(bb.ToList());
                            return res.ToList();
                        }
                        if (bb.Any())
                        {

                            return bb.ToList();
                        }
                        else
                        {
                            int fid;
                            int.TryParse(id.Trim('~'), out fid);
                            var cc = Array.ConvertAll(
                                (_dbcontext.TB_SUBFAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID,
                                    tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf })
                                    .Join(_dbcontext.TB_CATALOG_FAMILY, tcptps => tcptps.tps.SUBFAMILY_ID, tcp => tcp.FAMILY_ID,
                                        (tcptps, tcp) => new { tcptps, tcp })
                                    .Where(x => x.tcp.CATALOG_ID == catalogId && x.tcp.FLAG_RECYCLE == "A" && x.tcptps.tpf.FLAG_RECYCLE == "A" && x.tcptps.tps.FAMILY_ID == fid && x.tcp.CATEGORY_ID == subfamilycategoryid)).ToArray(),
                                a => new
                                {
                                    FAMILY_ID = a.tcptps.tps.FAMILY_ID.ToString(CultureInfo.InvariantCulture),
                                    SUBFAMILY_ID = a.tcptps.tps.SUBFAMILY_ID,
                                    ProdCnt =
                                        _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_PRODUCT, tpf => tpf.PRODUCT_ID, tp => tp.PRODUCT_ID, (tpf, tp) => new { tpf, tp }).Where(b => b.tpf.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID && b.tpf.FLAG_RECYCLE == "A" && b.tp.FLAG_RECYCLE == "A").Select(x => x.tpf.PRODUCT_ID)
                                            .Distinct()
                                            .Count(),
                                    SubProdCnt = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, ts => ts.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (ts, tpf) => new { ts, tpf }).Where(b => b.ts.TB_FAMILY.FAMILY_ID == a.tcptps.tps.SUBFAMILY_ID && b.tpf.CATALOG_ID == catalogId && b.tpf.FLAG_RECYCLE == "A")
                                                 .Select(b => b.tpf.SUBPRODUCT_ID)
                                                 .Distinct()
                                                 .Count(),
                                    a.tcp.TB_CATALOG.CATALOG_ID,
                                    a.tcp.TB_CATALOG.CATALOG_NAME,
                                    a.tcp.TB_CATALOG.DESCRIPTION,
                                    a.tcp.TB_CATALOG.VERSION,
                                    a.tcptps.tps.SORT_ORDER
                                })
                                .Select(a => new ProductView
                                {
                                    id = subfamilycategoryid + "~" + a.SUBFAMILY_ID,
                                    CATEGORY_ID = "~" + a.SUBFAMILY_ID,
                                    CATEGORY_NAME = familyandrelatedfamily ? a.SubProdCnt > 0 ?
                                       _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID && b.FLAG_RECYCLE == "A").Select(b => b.FAMILY_NAME).FirstOrDefault() /*+ " (" + a.ProdCnt + ")"*/ : _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID && b.FLAG_RECYCLE == "A").Select(b => b.FAMILY_NAME).FirstOrDefault()/* + " (" + a.ProdCnt + ")"*/ : _dbcontext.TB_FAMILY.Where(b => b.FAMILY_ID == a.SUBFAMILY_ID).Select(b => b.FAMILY_NAME).FirstOrDefault(),
                                    CATALOG_ID = a.CATALOG_ID,
                                    CATALOG_NAME = a.CATALOG_NAME,
                                    SORT_ORDER = a.SORT_ORDER,
                                    CategoryIdinNavigator = false,
                                    FamilyandRelatedFamily = familyandrelatedfamily,
                                    hasChildren = false,
                                    spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.FLAG_RECYCLE == "A" && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.SUBFAMILY_ID && x.tps.CATEGORY_ID == subfamilycategoryid && x.tpf.FLAG_RECYCLE == "A") ?
                                    "subfamilyClone" : _dbcontext.TB_CATALOG.Join(_dbcontext.TB_CATALOG_FAMILY, tc => tc.DEFAULT_FAMILY, tcf => tcf.FAMILY_ID, (tc, tcf) => new { tc, tcf }).Any(x => x.tc.DEFAULT_FAMILY == a.SUBFAMILY_ID && x.tcf.FLAG_RECYCLE == "A") ? "defaultsubfamily" : "subfamily",
                                    @checked = catalogId == 1 && _dbcontext.TB_FAMILY.Join(_dbcontext.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tpf.CATALOG_ID == workingCatalogId && x.tps.FLAG_RECYCLE == "A" && x.tpf.FLAG_RECYCLE == "A" && x.tps.FAMILY_ID == a.SUBFAMILY_ID) ? true : false
                                });
                            return cc.OrderBy(x => x.SORT_ORDER).ThenBy(x => x.id).ToList();
                        }
                    }
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InvertedProductsController: GetCategoriesForTree", objexception);
                return null;
            }

        }

        [System.Web.Http.HttpPost]
        public int GetInvertedModifiedProductsCount()
        {
            try
            {
                var dt = (DataTable)System.Web.HttpContext.Current.Session["ModifiedKendoProductCount"];
                object field = dt.Rows[0][0];
                int prodCount = Convert.ToInt32(field);
                return prodCount;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : GetInvertedModifiedProductsCount", ex);
                return 1;
            }
        }

        [System.Web.Http.HttpPost]
        public IList GetAllAttributesUnderCatalaog(int catalogId, int selectTemplateId)
        {
            try
            {
                int customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                DataTable dt = new DataTable();
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_INVERTEDPRODUCT_ATTRIBUTELIST";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOGID", SqlDbType.NVarChar, 1000).Value = catalogId;
                    objSqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = customerid;
                    objSqlCommand.Parameters.Add("@TEMPLATEID", SqlDbType.Int).Value = selectTemplateId;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 1000).Value = "GETCATALOGUNDERATTRIBUTEDETAILS";
                    objSqlCommand.CommandTimeout = 0;
                    objSqlConnection.Open();
                    var da = new SqlDataAdapter(objSqlCommand);
                    da.Fill(dt);
                    objSqlConnection.Close();
                }
                List<AttributeList> attrField = new List<AttributeList>();
                foreach (DataRow value in dt.Rows)
                {
                    AttributeList obj = new AttributeList();
                    obj.ATTRIBUTE_ID = (int)value["ATTRIBUTE_ID"];
                    obj.ATTRIBUTE_NAME = value["ATTRIBUTE_NAME"].ToString();
                    obj.ISAvailable = false;
                    attrField.Add(obj);
                }
                return attrField.OrderBy(x => x.ATTRIBUTE_NAME).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : GetAllAttributesUnderCatalaog", ex);
                return null;
            }
        }

        //[System.Web.Http.HttpPost]
        //public Tuple<IList, string, string,string> GetHierarchyDetailsUnderInvertedProducts(int catalogId, object model)
        //{
        //    try
        //    {

        //        var invertedProductDetails = JObject.Parse(model.ToString());



        //        string familyName = string.Empty;
        //        string categoryID = invertedProductDetails["CATEGORY_ID"].ToString();
        //        int productID = (int)invertedProductDetails["PRODUCT_ID"];
        //        int familyID = (int)invertedProductDetails["FAMILY_ID"];
        //        string hierarchyStructure = invertedProductDetails["CATALOG_NAME"].ToString();
        //        List<CatalogDetails> fillCatalogDetails = new List<CatalogDetails>();
        //        List<string> hierarchyCatalogDetails = new List<string>();
        //        do
        //        {


        //            CatalogDetails obj = new CatalogDetails();
        //            var categoryValueDetails = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_FAMILY, tc => tc.CATEGORY_ID, TF => TF.CATEGORY_ID, (tc, tf) => new { tc, tf })
        //           .Where(x => x.tc.CATEGORY_SHORT == categoryID && x.tc.FLAG_RECYCLE == "A" && x.tf.FAMILY_ID == familyID)
        //           .Select(x => new { x.tc.CATEGORY_ID, x.tc.CATEGORY_NAME, x.tc.PARENT_CATEGORY, x.tc.CATEGORY_SHORT, x.tc.CATEGORY_PARENT_SHORT }).FirstOrDefault();

        //            if (categoryValueDetails != null)
        //            {
        //                obj.CATEGORY_ID = categoryValueDetails.CATEGORY_ID.ToString();
        //                obj.CATEGORY_NAME = categoryValueDetails.CATEGORY_NAME.ToString();
        //                obj.PARENT_CATEGORY = categoryValueDetails.PARENT_CATEGORY.ToString();
        //                obj.CATEGORY_SHORT = categoryValueDetails.CATEGORY_SHORT.ToString();
        //                obj.TYPE = "CATEGORY";
        //                fillCatalogDetails.Add(obj);
        //                categoryID = categoryValueDetails.CATEGORY_PARENT_SHORT.ToString();
        //            }
        //            else
        //            {
        //                categoryID = "0";
        //            }


        //        }
        //        while (categoryID != "0");

        //        for (int i = fillCatalogDetails.Count; i > 0; i--)
        //        {
        //            if (!String.IsNullOrEmpty(hierarchyStructure))
        //            {
        //                hierarchyStructure = hierarchyStructure + " " + "->" + " " + fillCatalogDetails[i - 1].CATEGORY_NAME;
        //            }
        //            else
        //            {
        //                hierarchyStructure = fillCatalogDetails[i - 1].CATEGORY_NAME;
        //            }
        //        }

        //        var familyValueDetails = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID == familyID && x.FLAG_RECYCLE == "A").Select(x => new { x.FAMILY_ID, x.FAMILY_NAME }).FirstOrDefault();

        //        if (!String.IsNullOrEmpty(hierarchyStructure))
        //        {
        //            hierarchyStructure = hierarchyStructure + " " + "->" + " " + familyValueDetails.FAMILY_NAME;
        //            familyName = familyValueDetails.FAMILY_NAME;

        //        }
        //        else
        //        {
        //            hierarchyStructure = familyValueDetails.FAMILY_NAME;
        //            familyName = familyValueDetails.FAMILY_NAME;
        //        }
        //        //   hierarchyStructure = hierarchyStructure + " " + "->" + " " + invertedProductDetails["ITEM#"].ToString();
        //        hierarchyCatalogDetails.Add(hierarchyStructure);
        //       // Tuple<IList, string, string> objtuple = new Tuple<IList, string, string>(hierarchyCatalogDetails, invertedProductDetails["ITEM#"].ToString(), "Content");

        //       // var ITEM =invertedProductDetails.GetValue
        //        Tuple<IList, string, string,string> objtuple = new Tuple<IList, string, string,string>(hierarchyCatalogDetails, invertedProductDetails["ITEM#__OBJ__1__1__false__true"].ToString(), "Content", familyName);



        //        return objtuple;
        //    }

        //    catch (Exception ex)
        //    {
        //        Logger.Error("Error at InvertedProductController : GetHierarchyDetailsUnderInvertedProducts", ex);
        //        return null;
        //    }
        //}

        //HIERARCHY function for DragDropFromDictionaryEventHandler down///
        public class familyValueDetails1
        {
            public int FAMILY_ID { get; set; }
            public string FAMILY_NAME { get; set; }

        }
        [System.Web.Http.HttpPost]
        public IList GetHierarchyDetailsUnderInvertedProducts(int catalogId, object model)
        {
            try
            {
                familyValueDetails1 objs = new familyValueDetails1();
                var invertedProductDetails = JObject.Parse(model.ToString());
                string categoryID = invertedProductDetails["CATEGORY_ID"].ToString();
                int familyID = (int)invertedProductDetails["FAMILY_ID"];
                int productID = (int)invertedProductDetails["PRODUCT_ID"];
                string hierarchyStructure = invertedProductDetails["CATALOG_NAME"].ToString();

                CatalogDetails obj = new CatalogDetails();
                var familyValueDetails1 = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_FAMILY, tc => tc.CATEGORY_ID, TF => TF.CATEGORY_ID, (tc, tf) => new { tc, tf }).Join
                    (_dbcontext.TB_PROD_FAMILY, tpf => tpf.tf.FAMILY_ID, tf => tf.FAMILY_ID,
                                     (tpf, tf) => new { tpf, tf }).Join(_dbcontext.TB_CATALOG_PRODUCT, tcf => tcf.tf.PRODUCT_ID, tf => tf.PRODUCT_ID,
                                     (tcf, tf) => new { tcf, tf }).Join(_dbcontext.TB_FAMILY, tbf => tbf.tcf.tf.FAMILY_ID, tf => tf.FAMILY_ID,
                                     (tbf, tf) => new { tbf, tf })
                 .Where(x => x.tbf.tcf.tpf.tc.FLAG_RECYCLE == "A" && x.tbf.tcf.tf.PRODUCT_ID == productID && x.tbf.tcf.tpf.tf.CATALOG_ID == catalogId)
                                   .Select(x => new { x.tbf.tcf.tpf.tc.CATEGORY_ID, x.tbf.tcf.tpf.tc.CATEGORY_NAME, x.tbf.tcf.tpf.tc.PARENT_CATEGORY, x.tbf.tcf.tpf.tc.CATEGORY_SHORT, x.tbf.tcf.tpf.tc.CATEGORY_PARENT_SHORT, x.tf.FAMILY_ID, x.tf.FAMILY_NAME }).Distinct().ToList();
                //.DistinctBy(x=> x.FAMILY_ID) 

                var hierarchyreturn = familyValueDetails1.ToList();

                return hierarchyreturn;

            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : GetInvertedProductsTemplateDetails", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public IList GetInvertedProductsTemplateDetails(int catalogId, int selectTemplateId)
        {
            try
            {
                List<AttributeList> publishDetails = new List<AttributeList>();
                int customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                var templateDetails = _dbcontext.TB_INVERTEDPRODUCTS_TEMPLATE.Join(_dbcontext.TB_ATTRIBUTE, tipt => tipt.ATTRIBUTE_ID, ta => ta.ATTRIBUTE_ID, (tipt, ta) => new { tipt, ta })
                    .Where(x => x.tipt.CUSTOMER_ID == customerid && x.tipt.CATALOG_ID == catalogId && x.tipt.TEMPLATE_ID == selectTemplateId).Select(x => new { x.ta.ATTRIBUTE_ID, x.ta.ATTRIBUTE_NAME });
                foreach (var itr in templateDetails)
                {
                    AttributeList details = new AttributeList();
                    details.ATTRIBUTE_ID = itr.ATTRIBUTE_ID;
                    details.ATTRIBUTE_NAME = itr.ATTRIBUTE_NAME;
                    details.ISAvailable = false;
                    publishDetails.Add(details);
                }
                return publishDetails.OrderBy(x => x.ATTRIBUTE_NAME).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : GetInvertedProductsTemplateDetails", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public string SaveSelectedInvertedProductAttributes(int catalogId, int selectTemplateId, string templateName, JArray model)
        {
            try
            {
                bool checkIsAvailable;
                var GetAlreadyExist = _dbcontext.TB_INVERTEDPRODUCTS_TEMPLATE.Where(x => x.TEMPLATE_NAME == templateName).Select(x => x).Count();
                if (GetAlreadyExist > 0 && selectTemplateId == 0)
                {
                    return "AlreadyExsits";
                }


                int customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                int lastTemplateID = _dbcontext.TB_INVERTEDPRODUCTS_TEMPLATE.OrderByDescending(x => x.TEMPLATE_ID).Select(x => (int)x.TEMPLATE_ID).FirstOrDefault();


                if (selectTemplateId != 0)
                {
                    lastTemplateID = selectTemplateId;
                }
                else
                {
                    lastTemplateID = lastTemplateID + 1;
                }
                var attributeGroup = ((JArray)model).Select(x => new InvertedAttribute()
                {
                    ATTRIBUTE_ID = (int)x["ATTRIBUTE_ID"],
                    ISAvailable = (bool)x["ISAvailable"]
                }).Where(s => s.ISAvailable).ToList();
                foreach (var itr in attributeGroup)
                {

                    var exists = _dbcontext.TB_INVERTEDPRODUCTS_TEMPLATE.Where(x => x.TEMPLATE_NAME == templateName && x.ATTRIBUTE_ID == itr.ATTRIBUTE_ID && x.CATALOG_ID == catalogId && x.CUSTOMER_ID == customerid).Select(x => x).Count();

                    if (exists > 0)
                    {
                        checkIsAvailable = false;
                    }
                    else
                    {
                        checkIsAvailable = true;
                    }


                    if (itr.ISAvailable && checkIsAvailable)
                    {
                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_LS_INVERTEDPRODUCT_ATTRIBUTELIST";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.Add("@CATALOGID", SqlDbType.NVarChar, 1000).Value = catalogId;
                            objSqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = customerid;
                            objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 1000).Value = "INSERTINTOTB_INVERTEDPRODUCTS_TEMPLATE";
                            objSqlCommand.Parameters.Add("@ATTRIBUTEID", SqlDbType.Int).Value = itr.ATTRIBUTE_ID;
                            objSqlCommand.Parameters.Add("@TEMPLATEID", SqlDbType.Int).Value = lastTemplateID;
                            objSqlCommand.Parameters.Add("@TEMPLATENAME", SqlDbType.NVarChar, 1000).Value = templateName;
                            objSqlCommand.CommandTimeout = 0;
                            objSqlConnection.Open();
                            objSqlCommand.ExecuteNonQuery();
                            objSqlConnection.Close();
                        }

                    }

                }

                string lastTemplateIDValue = _dbcontext.TB_INVERTEDPRODUCTS_TEMPLATE.OrderByDescending(x => x.TEMPLATE_ID).Select(x => (int)x.TEMPLATE_ID).FirstOrDefault().ToString();

                return lastTemplateIDValue;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : SaveSelectedInvertedProductAttributes", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public string DeleteInvertedProductPublishAttributes(int catalogId, int selectTemplateId, JArray model)
        {
            try
            {
                int customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                var attributeGroup = ((JArray)model).Select(x => new InvertedAttribute()
                {
                    ATTRIBUTE_ID = (int)x["ATTRIBUTE_ID"],
                    ISAvailable = (bool)x["ISAvailable"]
                }).Where(s => s.ISAvailable).ToList();
                foreach (var itr in attributeGroup)
                {
                    if (itr.ISAvailable)
                    {
                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_LS_INVERTEDPRODUCT_ATTRIBUTELIST";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.Add("@CATALOGID", SqlDbType.NVarChar, 1000).Value = catalogId;
                            objSqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = customerid;
                            objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 1000).Value = "REMOVEFROMTB_INVERTEDPRODUCTS_TEMPLATE";
                            objSqlCommand.Parameters.Add("@ATTRIBUTEID", SqlDbType.Int).Value = itr.ATTRIBUTE_ID;
                            objSqlCommand.Parameters.Add("@TEMPLATEID", SqlDbType.Int).Value = selectTemplateId;
                            objSqlCommand.Parameters.Add("@TEMPLATENAME", SqlDbType.NVarChar, 1000).Value = "0";
                            objSqlCommand.CommandTimeout = 0;
                            objSqlConnection.Open();
                            objSqlCommand.ExecuteNonQuery();
                            objSqlConnection.Close();
                        }

                    }
                }
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : SaveSelectedInvertedProductAttributes", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public IList InvertedProductsTemplateList(int catalogId)
        {
            try
            {
                int customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                List<TemplateDetails> templateDetailsUnderCatalog = new List<TemplateDetails>();
                var templateDetails = _dbcontext.TB_INVERTEDPRODUCTS_TEMPLATE.Where(x => x.CUSTOMER_ID == customerid && x.CATALOG_ID == catalogId).Select(x => new { x.TEMPLATE_ID, x.TEMPLATE_NAME }).Distinct().ToList();
                foreach (var itr in templateDetails)
                {
                    TemplateDetails values = new TemplateDetails();
                    values.TEMPLATE_ID = (int)itr.TEMPLATE_ID;
                    values.TEMPLATE_NAME = itr.TEMPLATE_NAME.ToString();
                    templateDetailsUnderCatalog.Add(values);
                }
                return templateDetailsUnderCatalog.OrderBy(x => x.TEMPLATE_ID).ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : InvertedProductsTemplateList", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public string SaveInvertedProductsDetails(int catalogId, string templateName)
        {
            try
            {
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : SaveInvertedProductsDetails", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public string DeleteSelectedTemplateName(int catalogId, int selectedTemplateID)
        {
            try
            {
                int customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_INVERTEDPRODUCT_ATTRIBUTELIST";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOGID", SqlDbType.NVarChar, 1000).Value = catalogId;
                    objSqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.Int).Value = customerid;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 1000).Value = "DELETE_TEMPLATEID";
                    objSqlCommand.Parameters.Add("@TEMPLATEID", SqlDbType.Int).Value = selectedTemplateID;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlConnection.Open();
                    objSqlCommand.ExecuteNonQuery();
                    objSqlConnection.Close();
                }
                return "SUCCESS";
            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : SaveInvertedProductsDetails", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public DataTable GetInvertedproductAttributeDetails(int catalogId, int familyId)
        {
            var model = new DashBoardModel();
            var dt_InvertedproductAttributeDetails = new DataTable();
            try
            {
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_Inverted_ProductPivotTableAttributes";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 1000).Value = catalogId;
                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                    objSqlCommand.Parameters.Add("@USER_ID", SqlDbType.NVarChar, 1000).Value = User.Identity.Name;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlConnection.Open();
                    var da = new SqlDataAdapter(objSqlCommand);
                    da.Fill(dt_InvertedproductAttributeDetails);


                    System.Web.HttpContext.Current.Session["ModifiedKendoProductCount"] = dt_InvertedproductAttributeDetails;
                    if (dt_InvertedproductAttributeDetails.Rows.Count > 0)
                    {
                        int i = 0;
                        foreach (DataColumn columns in dt_InvertedproductAttributeDetails.Columns)
                        {
                            PTColumns objPTColumns = new PTColumns();

                            if (columns.ToString() != "CATALOG_ID" && columns.ToString() != "PRODUCT_ID" && columns.ToString() != "CATALOG_NAME" && columns.ToString() != "FAMILY_ID" && columns.ToString() != "CATEGORY_ID")
                            {

                                string attributeName = string.Empty;

                                attributeName = columns.Caption.ToString();

                                int attribute_Id = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == attributeName).Select(y => y.ATTRIBUTE_ID).FirstOrDefault();
                                int attribute_Type = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == attributeName).Select(y => y.ATTRIBUTE_TYPE).FirstOrDefault();

                                objPTColumns.Caption = columns.Caption + "__OBJ__" + attribute_Id + "__" + attribute_Type + "__false__true";
                                objPTColumns.ColumnName = columns.ColumnName + "__OBJ__" + attribute_Id + "__" + attribute_Type + "__false__true";
                                dt_InvertedproductAttributeDetails.Columns[i].ColumnName = columns.ColumnName + "__OBJ__" + attribute_Id + "__" + attribute_Type + "__false__true";
                                dt_InvertedproductAttributeDetails.Columns[i].Caption = columns.Caption + "__OBJ__" + attribute_Id + "__" + attribute_Type + "__false__true";
                            }


                            else
                            {
                                objPTColumns.Caption = columns.Caption;
                                objPTColumns.ColumnName = columns.ColumnName;
                            }


                            model.Columns.Add(objPTColumns);
                            i++;
                        }
                    }
                }
                return dt_InvertedproductAttributeDetails;

            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : GetInvertedproductAttributeDetails", ex);
                return null;
            }
        }







        public class AttributeList
        {
            public int ATTRIBUTE_ID
            {
                get;
                set;
            }

            public string ATTRIBUTE_NAME
            {
                get;
                set;
            }

            public bool ISAvailable
            {
                get;
                set;
            }
        }

        public partial class CatalogDetails
        {
            public string CATEGORY_ID
            {
                get;
                set;
            }
            public string CATEGORY_NAME
            {
                get;
                set;
            }
            public string PARENT_CATEGORY
            {
                get;
                set;
            }
            public string CATEGORY_SHORT
            {
                get;
                set;
            }

            public string TYPE
            {
                get;
                set;
            }
        }


        public partial class InvertedAttribute
        {
            public int ATTRIBUTE_ID
            {
                get;
                set;
            }
            public bool ISAvailable
            {
                get;
                set;
            }
        }

        public partial class TemplateDetails
        {
            public int TEMPLATE_ID
            {
                get;
                set;
            }
            public string TEMPLATE_NAME
            {
                get;
                set;
            }
        }

        #region Product Import Validation

        [System.Web.Http.HttpPost]
        public string SheetValidationForProducts(string excelPath, string importtype)
        {
            try
            {
                DataTable sheetValue = new DataTable();

                importtype = importtype.ToUpper();
                int i = 0;
                //DataTable dtResult = null;
                int totalSheet = 0;
                using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();
                    DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    //   string sheetName = string.Empty;
                    if (dt != null)
                    {
                        var tempDataTable = (from dataRow in dt.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();
                        dt = tempDataTable;
                        totalSheet = dt.Rows.Count;

                    }
                    String[] excelSheets = new String[dt.Rows.Count];
                    foreach (DataRow dr in dt.Rows)
                    {
                        string tableName = dr["TABLE_NAME"].ToString();
                        tableName = tableName.Replace(@"$", string.Empty);
                        excelSheets[i] = tableName;
                        i++;
                    }
                    if (excelSheets.Length == 1 && importtype == "ITEM LIST")
                    {
                        if (excelSheets[0] != "'Item List'")
                        {
                            return "invalid sheet name";
                        }
                        else
                        {
                            sheetValue = ConvertExcelToDataTable(excelPath, "Item List");
                            if (sheetValue.Rows.Count == 0)
                            {
                                return "empty value";
                            }
                            //else
                            //{
                            //    DataTable checkSheet = new DataTable();
                            //    checkSheet = sheetValue.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                            //    if (checkSheet.Rows.Count == 0)
                            //    {
                            //        return "empty value";
                            //    }
                            //}
                        }
                    }


                    return (excelSheets.Length).ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at ChangeRoleViewLog ", ex);
                return "false";
            }
        }
        public static DataTable ConvertExcelToDataTable(string FileName, string Sheetname)
        {
            try
            {
                DataTable dtResult = null;
                string tableName = Sheetname;
                int totalSheet = 0; //No of sheets on excel file  
                using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();
                    DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = string.Empty;
                    if (dt != null)
                    {
                        var tempDataTable = (from dataRow in dt.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();
                        dt = tempDataTable;
                        totalSheet = dt.Rows.Count;
                        sheetName = Sheetname + "$";
                    }
                    cmd.Connection = objConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(ds, tableName);
                    dtResult = ds.Tables[tableName];
                    objConn.Close();
                    return dtResult; //Returning Dattable  
                }

            }
            catch (Exception ex)
            {
                Logger.Error("Error at ConvertExcelToDataTable ", ex);
                return null;
            }

        }
        #endregion
        [System.Web.Http.HttpPost]
        public string ValidateAttrImport(string importExcelSheetDDSelectionValue, string excelPath, string importFormat)
        {
            var customerId = 0;
            try
            {
                var customerName = User.Identity.Name;
                var customerDetails = objLS.Customers.Where(x => x.EMail == customerName).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();
                string[] itemArray = { customerDetails };
                string[] columnName = { "ACTION", "CATALOG NAME", "CATEGORY NAME", "FAMILY NAME" };
                columnName = columnName.Concat(itemArray).ToArray();
                productTemp = Guid.NewGuid().ToString();
                // string SheetName = "Attribute$";
                string[] missingColumn = new string[columnName.Length];
                int missingValues = 0, duplicateRows = 0, duplicateId = 0, datatype_Validation = 0;
                string errorMissingColumn = string.Empty;
                DataTable import_Table = ConvertExcelToDataTable(excelPath, importExcelSheetDDSelectionValue);
                DataTable prodImportTable = new DataTable();
                if (import_Table.Columns.Contains("CATALOG ID"))
                {
                    prodImportTable = import_Table;
                }
                else
                {

                    prodImportTable = import_Table.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                }
                DataColumnCollection columns = prodImportTable.Columns;
                int attrMissingColumn = 0, attrRefExists = 0;

                if (!prodImportTable.Columns.Contains("PRODUCT ID"))
                {
                    prodImportTable.Columns.Add("PRODUCT ID");
                }
                //Check the column is in the  sheet or not
                prodImportTable.Columns.Add("Missing Column", typeof(string));
                for (int i = 0; i < columnName.Length; i++)
                {
                    if (!columns.Contains(columnName[i]))
                    {
                        if (attrMissingColumn == 0)
                        {
                            missingColumn[attrMissingColumn] = columnName[i];
                            errorMissingColumn = errorMissingColumn + columnName[i];
                            attrMissingColumn++;
                        }
                        else
                        {
                            missingColumn[attrMissingColumn] = columnName[i];
                            errorMissingColumn = errorMissingColumn + "~" + columnName[i];
                            attrMissingColumn++;
                        }

                    }
                }
                //Write the missing column in datatable
                foreach (DataRow dr in prodImportTable.Rows)
                {

                    dr["Missing Column"] = errorMissingColumn;
                    break;
                }
                string[] valueOfcatalogItemNo = { customerDetails };
                string[] columnValues = { "CATALOG NAME", "CATEGORY NAME", "FAMILY NAME" };
                columnValues = columnValues.Concat(valueOfcatalogItemNo).ToArray();
                //Missing Values Validation

                prodImportTable.Columns.Add("Missing Values", typeof(string));
                for (int i = 0; i < columnValues.Length; i++)
                {
                    if (columns.Contains(columnName[i]))
                    {
                        foreach (DataRow dr in prodImportTable.Select("[" + columnValues[i] + "] is NULL or [" + columnValues[i] + "]=''"))
                        {
                            missingValues++;
                            string updateMissingValues = dr["Missing Values"].ToString();
                            if (updateMissingValues != "")
                            {
                                updateMissingValues = updateMissingValues + ".|";
                            }
                            dr["Missing Values"] = updateMissingValues + "" + columnValues[i] + " " + " has no values";
                            updateMissingValues = string.Empty;
                        }
                    }


                }
                prodImportTable.AcceptChanges();


                //Reference Exists

                foreach (DataRow delAttr in prodImportTable.Rows)
                {
                    string action = delAttr["ACTION"].ToString();
                    action = action.ToUpper();
                    if (action.Equals("DELETE"))
                    {
                        DataTable deleteAttrValues = new DataTable();
                        deleteAttrValues = prodImportTable.AsEnumerable().Where(x => x.Field<string>("ACTION") != null && x.Field<string>("ACTION").ToUpper().Contains("DELETE")).CopyToDataTable();


                        foreach (DataRow dr in deleteAttrValues.Rows)
                        {
                            int product_id = 0;

                            if (dr["PRODUCT ID"] == DBNull.Value)
                            {

                                string prod_Name = customerDetails;
                                product_id = _dbcontext.TB_PROD_SPECS.Where(x => x.STRING_VALUE == prod_Name).Select(x => x.PRODUCT_ID).FirstOrDefault();

                            }
                            else
                            {
                                product_id = Convert.ToInt32(dr["PRODUCT ID"]);
                            }

                            // var family_Attr = _dbcontext.TB_CATEGORY_FAMILY_ATTR_LIST.Where(x => x.ATTRIBUTE_ID == attribute_Id);
                            //var prod_Attr = _dbcontext.TB_PROD_FAMILY_ATTR_LIST.Where(y => y.ATTRIBUTE_ID == attribute_Id);
                            var family_Specs = _dbcontext.TB_FAMILY_SPECS.Where(z => z.STRING_VALUE != "NULL" || z.STRING_VALUE != "");
                            var prod_Specs = _dbcontext.TB_PROD_SPECS.Where(a => a.STRING_VALUE != "NULL" || a.STRING_VALUE != "");

                            //if (family_Attr.Count() > 0 && prod_Attr.Count() > 0 && family_Specs.Count() > 0 && prod_Specs.Count() > 0)
                            //{
                            //    attrRefExists++;
                            //    string updateMissingValues = dr["Missing Values"].ToString();
                            //    if (updateMissingValues != "")
                            //    {
                            //        updateMissingValues = updateMissingValues + ".|";
                            //    }
                            //    dr["Missing Values"] = updateMissingValues + "Attribute cannot be Deleted";
                            //}

                        }
                        prodImportTable.AcceptChanges();
                    }
                }
                //Duplicate Rows Checking


                var dupValues = prodImportTable.AsEnumerable()
                  .GroupBy(dr => dr.Field<string>(customerDetails)).Where(g => g.Count() > 1).Select(g => g.First()).ToList();

                //  var dup_Cap_Values = attrImportTable.AsEnumerable()
                //  .GroupBy(dr => dr.Field<string>("CAPTION")).Where(g.Count() > 1 ).Select(g => g.First()).ToList();

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    //objSqlConnection.Open();
                    //SQLString =
                    //    "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                    //    prodTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + prodTemp + "] END')";
                    //SqlCommand _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                    //_DBCommand.ExecuteNonQuery();

                    //SQLString = CreateTable("[##IMPORTTEMP" + prodTemp + "]", prodImportTable);
                    //_DBCommand = new SqlCommand(SQLString, objSqlConnection);
                    //_DBCommand.ExecuteNonQuery();
                    //var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    //{
                    //    DestinationTableName = "[##IMPORTTEMP" + prodTemp + "]"
                    //};
                    //bulkCopy.WriteToServer(prodImportTable);


                    //SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    //objSqlCommand.CommandText = " SELECT t.customerDetails FROM [##IMPORTTEMP" + prodTemp + "] t WHERE t.customerDetails is not null GROUP BY t.customerDetails HAVING COUNT(t.customerDetails) > 1";

                    //DataTable dup_Cap_Values = new DataTable();
                    //SqlDataAdapter sda = new SqlDataAdapter(objSqlCommand);
                    //sda.Fill(dup_Cap_Values);


                    foreach (var dup in dupValues)
                    {
                        string dupAttrValue = dup[1].ToString();
                        foreach (DataRow dr in prodImportTable.Rows)
                        {
                            string checkDuplicate = dr[customerDetails].ToString();
                            if (checkDuplicate.Equals(dupAttrValue))
                            {
                                string updateMissingValues = dr["Missing Values"].ToString();
                                if (updateMissingValues != "")
                                {
                                    updateMissingValues = updateMissingValues + ".|";
                                }
                                dr["Missing Values"] = updateMissingValues + "Duplicate Values";
                            }
                        }
                        duplicateRows++;

                    }

                    prodImportTable.AcceptChanges();
                }

                //// bool aExists = attrImportTable.Select().ToList().Exists(row => row["ATTRIBUTE_ID"].ToString() == "");
                ////   if (aExists)
                ////    {
                //var customerid = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                //using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                //{
                //    objSqlConnection.Open();
                //    SQLString =
                //        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                //        productTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + productTemp + "] END')";
                //    SqlCommand _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                //    _DBCommand.ExecuteNonQuery();

                //    SQLString = CreateTable("[##IMPORTTEMP" + productTemp + "]", prodImportTable);
                //    _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                //    _DBCommand.ExecuteNonQuery();
                //    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                //    {
                //        DestinationTableName = "[##IMPORTTEMP" + productTemp + "]"
                //    };
                //    bulkCopy.WriteToServer(prodImportTable);


                //    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                //    objSqlCommand.CommandText = "STP_LS_ATTRIBUTE_IMPORT";
                //    objSqlCommand.CommandType = CommandType.StoredProcedure;

                //    objSqlCommand.Connection = objSqlConnection;
                //    objSqlCommand.CommandTimeout = 0;
                //    objSqlCommand.Parameters.Add("@ATTRNAME", SqlDbType.NVarChar, 100).Value = "";
                //    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "VALIDATION ATTR_NAMES and caption";
                //    objSqlCommand.Parameters.Add("@CUSTOMER_ID", SqlDbType.Int, 100).Value = customerid;
                //    objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                //    objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                //    objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = productTemp;
                //    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int, 100).Value = 0;

                //    DataSet dup_Attrs_Name = new DataSet();
                //    SqlDataAdapter sda = new SqlDataAdapter(objSqlCommand);
                //    sda.Fill(dup_Attrs_Name);
                //    foreach (DataRow dupc in dup_Attrs_Name.Tables[0].Rows)
                //    {
                //        string dupCapValue = dupc["ATTRIBUTE_NAME"].ToString();
                //        foreach (DataRow dr in prodImportTable.Rows)
                //        {
                //            string checkDuplicate = dr["ATTRIBUTE_NAME"].ToString();
                //            if (checkDuplicate.Equals(dupCapValue))
                //            {
                //                string updateMissingValues = dr["Missing Values"].ToString();
                //                if (updateMissingValues != "")
                //                {
                //                    updateMissingValues = updateMissingValues + ".|";
                //                }
                //                dr["Missing Values"] = updateMissingValues + "Duplicate Values";
                //                duplicateRows++;
                //            }
                //        }

                //    }
                //    foreach (DataRow dupc in dup_Attrs_Name.Tables[1].Rows)
                //    {
                //        string dupCapValue = dupc["ATTRIBUTE_NAME"].ToString();
                //        foreach (DataRow dr in prodImportTable.Rows)
                //        {
                //            string checkDuplicate = dr["ATTRIBUTE_NAME"].ToString();
                //            if (checkDuplicate.Equals(dupCapValue))
                //            {
                //                string updateMissingValues = dr["Missing Values"].ToString();
                //                if (updateMissingValues != "")
                //                {
                //                    updateMissingValues = updateMissingValues + ".|";
                //                }
                //                dr["Missing Values"] = updateMissingValues + "Duplicate Values";
                //                duplicateRows++;
                //            }
                //        }

                //    }
                //}
                //// }


                ////.GroupBy(dr => dr.Field<string>("ATTRIBUTE_NAME"))


                //bool exists = prodImportTable.Select().ToList().Exists(row => row["ATTRIBUTE_ID"].ToString() == "");
                //if (!exists)
                //{
                //    var customer_id = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();


                //    using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                //    {
                //        objSqlConnection.Open();
                //        SQLString =
                //            "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                //            productTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + productTemp + "] END')";
                //        SqlCommand _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                //        _DBCommand.ExecuteNonQuery();

                //        SQLString = CreateTable("[##IMPORTTEMP" + productTemp + "]", prodImportTable);
                //        _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                //        _DBCommand.ExecuteNonQuery();
                //        var bulkCopy = new SqlBulkCopy(objSqlConnection)
                //        {
                //            DestinationTableName = "[##IMPORTTEMP" + productTemp + "]"
                //        };
                //        bulkCopy.WriteToServer(prodImportTable);


                //        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                //        objSqlCommand.CommandText = "STP_LS_ATTRIBUTE_IMPORT";
                //        objSqlCommand.CommandType = CommandType.StoredProcedure;

                //        objSqlCommand.Connection = objSqlConnection;
                //        objSqlCommand.CommandTimeout = 0;
                //        objSqlCommand.Parameters.Add("@ATTRNAME", SqlDbType.NVarChar, 100).Value = "";
                //        objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "VALIDATION DUP_ATTR_NAME";
                //        objSqlCommand.Parameters.Add("@CUSTOMER_ID", SqlDbType.Int, 100).Value = customer_id;
                //        objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                //        objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                //        objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = productTemp;
                //        objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int, 100).Value = 0;

                //        DataTable dup_Attr_Name = new DataTable();
                //        SqlDataAdapter sda = new SqlDataAdapter(objSqlCommand);
                //        sda.Fill(dup_Attr_Name);

                //        foreach (DataRow _duplicate_AttrNames in prodImportTable.Rows)
                //        {
                //            int attrId = int.Parse(_duplicate_AttrNames["ATTRIBUTE_ID"].ToString());

                //            var AttrNameCheck = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attrId).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();



                //            foreach (DataRow _duplicateAttrNames in dup_Attr_Name.Rows)
                //            {

                //                String duplicate_AttrNames = _duplicateAttrNames["ATTRIBUTE_NAME"].ToString();
                //                var Counts = 1;
                //                if (AttrNameCheck == duplicate_AttrNames)
                //                {
                //                    Counts = 0;
                //                }

                //                if (Counts != 0)
                //                {

                //                    foreach (DataRow dr in prodImportTable.Rows)
                //                    {
                //                        string check_DupId = dr["ATTRIBUTE_NAME"].ToString();
                //                        if (check_DupId.Equals(duplicate_AttrNames))
                //                        {
                //                            string updateMissingValues = dr["ATTRIBUTE_NAME"].ToString();
                //                            if (updateMissingValues != "")
                //                            {
                //                                updateMissingValues = updateMissingValues + ".|";
                //                            }
                //                            dr["Missing Values"] = updateMissingValues + "Attribute Name or Caption Name already exists.";
                //                            duplicateRows++;
                //                        }
                //                    }

                //                }
                //            }
                //        }

                //    }

                //    prodImportTable.AcceptChanges();
                //}



                //DataType Validation
                //var obj_SqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                //using (obj_SqlConnection)
                //{
                //    obj_SqlConnection.Open();
                //    SQLString =
                //        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                //        productTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + productTemp + "] END')";
                //    SqlCommand _DBCommand = new SqlCommand(SQLString, obj_SqlConnection);
                //    _DBCommand.ExecuteNonQuery();

                //    SQLString = CreateTable("[##IMPORTTEMP" + productTemp + "]", prodImportTable);
                //    _DBCommand = new SqlCommand(SQLString, obj_SqlConnection);
                //    _DBCommand.ExecuteNonQuery();
                //    var bulkCopy = new SqlBulkCopy(obj_SqlConnection)
                //    {
                //        DestinationTableName = "[##IMPORTTEMP" + productTemp + "]"
                //    };
                //    bulkCopy.WriteToServer(prodImportTable);

                //    SqlCommand objSqlCommand = obj_SqlConnection.CreateCommand();
                //    objSqlCommand.CommandText = "STP_LS_ATTRIBUTE_IMPORT";
                //    objSqlCommand.CommandType = CommandType.StoredProcedure;

                //    objSqlCommand.Connection = obj_SqlConnection;
                //    objSqlCommand.CommandTimeout = 0;
                //    objSqlCommand.Parameters.Add("@ATTRNAME", SqlDbType.NVarChar, 100).Value = "";
                //    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "DATATYPE VALIDATION";
                //    objSqlCommand.Parameters.Add("@CUSTOMER_ID", SqlDbType.Int, 100).Value = 0;
                //    objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                //    objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                //    objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = productTemp;
                //    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int, 100).Value = 0;

                //    DataTable attr_datatype = new DataTable();
                //    SqlDataAdapter sda = new SqlDataAdapter(objSqlCommand);
                //    sda.Fill(attr_datatype);

                //    foreach (DataRow _misMatchedAttrdatatype in attr_datatype.Rows)
                //    {
                //        String mis_MatchedDatatype = _misMatchedAttrdatatype["ATTRIBUTE_NAME"].ToString();
                //        foreach (DataRow dr in prodImportTable.Rows)
                //        {
                //            string check_Dup_Datatype = dr["ATTRIBUTE_NAME"].ToString();
                //            if (check_Dup_Datatype.Equals(mis_MatchedDatatype))
                //            {

                //                string updateMissingValues = dr["ATTRIBUTE_NAME"].ToString();
                //                if (updateMissingValues != "")
                //                {
                //                    updateMissingValues = updateMissingValues + ".|";
                //                }
                //                dr["Missing Values"] = updateMissingValues + "Datatype mismatched";
                //                datatype_Validation++;
                //            }
                //        }
                //    }
                //}


                ///view log page
                System.Web.HttpContext.Current.Session["attrImportTableLog"] = prodImportTable;
                ///Download view log 
                System.Web.HttpContext.Current.Session["ExportErrorTable"] = prodImportTable;

                return "true" + "~" + attrMissingColumn + "~" + missingValues + "~" + duplicateRows + "~" + attrRefExists + "~" + datatype_Validation;


            }
            catch (Exception ex)
            {
                Logger.Error("Error at validateUserImport ", ex);
                return "false";
            }
        }
        [System.Web.Http.HttpPost]
        public System.Web.Mvc.JsonResult AttributeViewLog()
        {
            DashBoardModel model = new DashBoardModel();
            var attrImportTableLog = (DataTable)System.Web.HttpContext.Current.Session["attrImportTableLog"];
            foreach (DataColumn columns in attrImportTableLog.Columns)
            {
                var objPTColumns = new PTColumns();
                objPTColumns.Caption = columns.Caption;
                objPTColumns.ColumnName = columns.ColumnName;
                model.Columns.Add(objPTColumns);
            }
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(attrImportTableLog);
            model.Data = JSONString;
            return new JsonResult() { Data = model };
        }
        public string GetSkuCount(int prodCount)
        {
            int userProductCount = 0, userSKUProductCount = 0;
            var skucnt = objLS.TB_PLAN
                  .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                  .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                  .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
            var SKUcount = skucnt.Select(a => a).ToList();
            if (SKUcount.Any())
            {
                userSKUProductCount = Convert.ToInt32(SKUcount[0].SKU_COUNT);
            }

            var productcount = objLS.STP_LS_GET_PRODUCTS_BY_USER("PRODUCT", User.Identity.Name, "").ToList();

            if (productcount.Any())
            {
                userProductCount = Convert.ToInt32(productcount[0]);
            }

            if ((userProductCount + prodCount) > userSKUProductCount)
            {

                return "SKU Exceed";
            }
            return "SKU Available";

        }
        public static Boolean IsCatalogStudioField(String enumFieldName)
        {
            if (enumFieldName.ToUpper() == "ITEM#")
            {
                enumFieldName = "ITEM_";
            }
            if (enumFieldName.ToUpper() == "SUBITEM#")
            {
                enumFieldName = "SUBITEM_";
            }
            return Enum.GetNames(typeof(LS.Web.Controllers.AttributeMappingApiController.CSF)).Any(val => enumFieldName == val);
        }
        #region Product Import

        [System.Web.Http.HttpPost]
        public string ProdImport(string importExcelSheetDDSelectionValue, string excelPath, string importType, string importFormat, int catalog_Id, JArray model)
        {
            try
            {
                int insertRecords = 0; int updateRecords = 0; int skippedRecords = 0; int deleteRecords = 0;
                var customerid = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var ImportTable = new DataTable();
                ImportTable = ConvertExcelToDataTable(excelPath, importExcelSheetDDSelectionValue);
                DataTable prodImportTable = new DataTable();
                if (ImportTable.Columns.Contains("CATALOG ID"))
                {
                    prodImportTable = ImportTable;
                }
                else
                {

                    prodImportTable = ImportTable.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                }
                // DataTable prodImportTable = ImportTable.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                var customerName = User.Identity.Name;
                Stopwatch sw = new Stopwatch();
                sw.Start();

                string Sheetname = importExcelSheetDDSelectionValue;
                //int allowDuplicate;
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    prodTemp = Guid.NewGuid().ToString();
                    objSqlConnection.Open();

                    var importReturn = InvertedProductsImport(ImportTable, Sheetname.ToString(), allowDuplicate.ToString(), excelPath.ToString(), catalog_Id, prodTemp.ToString(), model);


                    return importReturn;
                }

            }
            catch (Exception ex)
            {
                Logger.Error("Error at InvertedProductController : ProdImport ", ex);
                return "false";
            }
        }



        #endregion
        public OleDbConnection excelConnection(string excelPath)
        {

            bool hasHeaders = true;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                          ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            else
                //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                         ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            return conn;
        }
        public DataTable SetDataTable(DataTable oldSheet, string deleteflag)
        {
            DataTable newSheet = new DataTable();
            try
            {

                if (oldSheet.Columns[0].ColumnName.ToString().ToUpper() == "ACTION")
                {
                    foreach (DataColumn dcol in oldSheet.Columns)
                    {
                        if (dcol.ColumnName.ToString().ToUpper() != "ACTION" && !newSheet.Columns.Contains(dcol.ColumnName))
                            newSheet.Columns.Add(dcol.ColumnName);
                    }
                    foreach (DataRow dtrow in oldSheet.Rows)
                    {
                        DataRow drow = newSheet.NewRow();
                        if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && deleteflag == "")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (deleteflag == "Valid")
                        {
                            for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count - 1 < exl)
                                {
                                    break;
                                }
                                drow[exl] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() == "DELETE" && deleteflag == "DELETE")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() == "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && dtrow[0].ToString().ToUpper() != "" && deleteflag == "SKIP")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                    }
                    return newSheet;
                }
                foreach (DataRow dtrow in oldSheet.Rows)
                {
                    if (dtrow.ItemArray[0].ToString().ToUpper() == "ACTION")
                    {
                        foreach (var colName in dtrow.ItemArray)
                        {
                            if (colName.ToString().ToUpper() != "ACTION" && !newSheet.Columns.Contains(colName.ToString()) && !string.IsNullOrEmpty(colName.ToString()))
                                newSheet.Columns.Add(colName.ToString());
                            else if (deleteflag == "Valid" && !string.IsNullOrEmpty(colName.ToString()))
                                newSheet.Columns.Add(colName.ToString());
                        }

                    }
                    else
                    {
                        DataRow drow = newSheet.NewRow();

                        if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && deleteflag == "")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (deleteflag == "Valid")
                        {
                            for (int exl = 0; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count - 1 < exl)
                                {
                                    break;
                                }
                                drow[exl] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() != "SKIP" && dtrow[0].ToString().ToUpper() == "DELETE" && deleteflag == "DELETE")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                        else if (dtrow[0].ToString().ToUpper() == "SKIP" && dtrow[0].ToString().ToUpper() != "DELETE" && dtrow[0].ToString().ToUpper() != "" && deleteflag == "SKIP")
                        {
                            for (int exl = 1; exl < dtrow.ItemArray.Count(); exl++)
                            {
                                if (newSheet.Columns.Count < exl)
                                {
                                    break;
                                }
                                drow[exl - 1] = dtrow.ItemArray[exl];
                            }
                            newSheet.Rows.Add(drow);
                        }
                    }
                }

            }
            catch (Exception objException)
            {
                Logger.Error("Error at loading SetDataTable in AdvanceImportApiController", objException);
                return null;
            }
            return newSheet;
        }

        public DataTable GetDataFromExcel(string SheetName, string excelPath, string query, string deleteflag)
        {
            DataTable dtexcel = new DataTable();
            DataTable dtexcelNew = new DataTable();
            try
            {
                var fileName = Path.GetFileName(excelPath).Split('.')[0];
                var path = Path.GetFullPath(excelPath);
                var fileExtention = Path.GetExtension(excelPath);
                string excelPathCopy = path.Replace(Path.GetFileName(excelPath), "") + "//" + fileName + "_Copy" + "." + fileExtention;
                using (OleDbConnection conn = excelConnection(excelPath))
                {
                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                    DataTable dtColumns = new DataTable();
                    try
                    {
                        DataRow[] schemaRow = schemaTable.Select("TABLE_NAME like '" + SheetName + "%'");
                        string sheet = schemaRow[1]["TABLE_NAME"].ToString();
                        int selectedIndex = 1;
                        foreach (DataRow dRow in schemaTable.Rows)
                        {

                            if (sheet.ToUpper() == dRow["TABLE_NAME"].ToString().ToUpper())
                                break;
                            selectedIndex++;
                        }


                        if (!sheet.EndsWith("_"))
                        {
                            OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                            dtexcel.Locale = CultureInfo.CurrentCulture;


                            daexcel.Fill(dtColumns);

                        }
                        conn.Close();
                        conn.Dispose();
                        var workbook1 = new Workbook();

                        workbook1 = Workbook.Load(excelPath);

                        for (int i = 0; i < workbook1.Worksheets.Count; i++)
                        {
                            if (workbook1.Worksheets[i].Name.ToUpper() == SheetName.ToUpper())
                            {
                                selectedIndex = i;
                            }
                        }


                        //foreach (WorksheetColumn colSheet in workbook1.Worksheets[1].Columns)
                        //    dt11.Columns.Add(colSheet.ToString());
                        dtexcel = dtColumns.Clone();
                        bool skipFirstRow = false;
                        if (!deleteflag.ToUpper().Contains("DELETE"))
                        {
                            foreach (WorksheetRow rowSheet in workbook1.Worksheets[selectedIndex].Rows)
                            {
                                if (dtColumns.Columns.Contains("ACTION") && !skipFirstRow)
                                {
                                    skipFirstRow = true;
                                    continue;
                                }
                                DataRow dRow = dtexcel.NewRow();
                                for (int col = 0; col < dtexcel.Columns.Count; col++)
                                {

                                    if (rowSheet.Cells[col].Value == null)
                                    {
                                        dRow[col] = DBNull.Value;
                                    }
                                    else
                                    {
                                        dRow[col] = rowSheet.Cells[col].Value.ToString();
                                    }


                                }
                                //new
                                if (dRow.ItemArray[0].ToString() != "Action")
                                {

                                    dtexcel.Rows.Add(dRow);
                                }
                            }
                        }
                        else
                        {
                            dtexcel = new DataTable();
                            dtexcel = dtColumns;
                            objLS = new CSEntities();
                        }
                        workbook1.Worksheets.Clear();
                    }
                    catch (Exception ex)
                    {
                        dtexcel = new DataTable();
                        dtexcel = dtColumns;
                    }
                }

                //Write the validation code for without import subfamily id and subfamily_name
                //bool checkSubFamily = true;
                //if (dtexcel.Columns.Count != 0)
                //{                  
                //    foreach (DataColumn dc in dtexcel.Columns)
                //    {
                //        if (dc.ColumnName.ToUpper()!="FAMILY_NAME" && (dc.ColumnName.ToUpper() == "SUBFAMILY_ID" || dc.ColumnName.ToUpper() == "SUBFAMILY_NAME") )
                //        {
                //            checkSubFamily = false;
                //        }                       
                //    }
                //    if(checkSubFamily)
                //    {
                //        dtexcel.Columns.Add("SUBFAMILY_ID");
                //        dtexcel.Columns.Add("SUBFAMILY_NAME");
                //    }
                //}


                if (dtexcel.Columns[0].ColumnName.ToString().ToUpper() == "ACTION" && deleteflag.ToUpper() == "VALID")
                {
                    dtexcelNew = dtexcel;
                    goto Skip;
                }
                if ((dtexcel == null || dtexcel.Rows.Count == 0 || dtexcel.Rows.Count == 1) && deleteflag.ToUpper() != "SKIP" && deleteflag.ToUpper() != "DELETE")
                {
                    return dtexcel;
                }
                dtexcelNew = SetDataTable(dtexcel, deleteflag);
            Skip:

                var tbattrcol = objLS.TB_ATTRIBUTE.Find(1);
                Workbook workbook;
                try
                {
                    workbook = Workbook.Load(excelPath);
                    string sheetNameworkbook = SheetName.Replace("$", "").TrimEnd();
                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells.Count();
                    if (deleteflag == "Valid")
                    {
                        for (int i = 0; i < dtexcelNew.Columns.Count; i++)
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value != null)
                            {
                                String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i].ColumnName != sColName && sColName.ToUpper() != "ACTION" && !dtexcelNew.Columns.Contains(sColName))
                                {
                                    dtexcelNew.Columns[i].ColumnName = sColName;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= dtexcelNew.Columns.Count; i++)
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value != null)
                            {
                                String sColName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i - 1].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i - 1].ColumnName != sColName && !dtexcelNew.Columns.Contains(sColName))
                                {
                                    dtexcelNew.Columns[i - 1].ColumnName = sColName;
                                }
                            }
                        }
                    }
                }
                catch
                {
                    if (deleteflag == "Valid")
                    {
                        for (int i = 0; i < dtexcelNew.Columns.Count; i++)
                        {
                            if (dtexcel.Rows[0][i].ToString() != null)
                            {
                                String sColName = dtexcel.Rows[0][i].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                {
                                    dtexcelNew.Columns[i].ColumnName = "CATALOG_ITEM_NO";
                                }
                                else if (dtexcelNew.Columns[i].ColumnName != sColName && sColName.ToUpper() != "ACTION")
                                {
                                    dtexcelNew.Columns[i].ColumnName = sColName;
                                }
                            }
                        }
                    }
                    else
                    {
                        var validCheck = SheetName.ToUpper().Contains("FAM") ? dtexcelNew.Columns.Contains("FAMILY_ID") : dtexcelNew.Columns.Contains("PRODUCT_ID");
                        if (!validCheck)
                        {
                            for (int i = 1; i <= dtexcelNew.Columns.Count; i++)
                            {
                                if (dtexcel.Rows[0][i].ToString() != null)
                                {
                                    String sColName = dtexcel.Rows[0][i].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                                    if (sColName == tbattrcol.ATTRIBUTE_NAME)
                                    {
                                        dtexcelNew.Columns[i - 1].ColumnName = "CATALOG_ITEM_NO";
                                    }
                                    else if (dtexcelNew.Columns[i - 1].ColumnName != sColName)
                                    {
                                        dtexcelNew.Columns[i - 1].ColumnName = sColName;
                                    }
                                }
                            }
                        }
                    }
                }
                if (dtexcelNew.Columns.Count > 0 && dtexcelNew.Columns[0].ColumnName.ToString().ToUpper() == "ACTION" && deleteflag.ToUpper() != "VALID")
                {
                    dtexcelNew.Columns.Remove(dtexcelNew.Columns[0].ColumnName);
                }
                dtexcelNew.AcceptChanges();
            }
            catch (Exception objException)
            {
                Logger.Error("Error at loading GetDataFromExcel in AdvanceImportApiController", objException);
                return null;
            }
            return dtexcelNew;
        }
        public DataTable GetExcelColumn2CatalogMapping(String xlsSheetName)
        {
            try
            {
                DataTable dt = _dsSheets.Tables[xlsSheetName];
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    string sCheckAttributeName = dt.Rows[i][0].ToString().ToUpper();
                    //Check if this is a CatalogStudio system defined field name
                    Boolean vaild = IsCatalogStudioField(sCheckAttributeName);

                    if (sCheckAttributeName == "SUBITEM#" || sCheckAttributeName == "ITEM#")
                    {
                        vaild = true;
                    }
                    if (vaild) //CatalogStudio Field
                    {
                        dr["CatalogField"] = sCheckAttributeName;
                        dr["IsSystemField"] = true;
                        dr["FieldType"] = 0;
                        dr["SelectedToImport"] = true;
                    }
                    else //User defined attribute name
                    {
                        var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                        if (customerid != null)
                        {
                            var drAttr =
                                _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                .Where(x => x.tcp.ATTRIBUTE_NAME == sCheckAttributeName && x.tpf.CUSTOMER_ID == customerid.CustomerId).Select(x => x.tcp).ToList();


                            if (drAttr.Count > 0)
                            {
                                string sAttributeNameInCatalog = drAttr[0].ATTRIBUTE_NAME;
                                string sAttributeDatatype = drAttr[0].ATTRIBUTE_DATATYPE;
                                string sAttrType = drAttr[0].ATTRIBUTE_TYPE.ToString(CultureInfo.InvariantCulture);
                                if (sAttributeDatatype == "Date and Time")
                                {
                                    if (sAttrType == "6" || sAttrType == "1" || sAttrType == "3")
                                    {
                                        dr["FieldType"] = 10;
                                        dr["SelectedToImport"] = true;
                                        dr["IsSystemField"] = true;
                                    }
                                    else if (sAttrType == "7" || sAttrType == "11" || sAttrType == "13")
                                    {
                                        dr["FieldType"] = 14;
                                        dr["SelectedToImport"] = true;
                                        dr["IsSystemField"] = true;
                                    }
                                }
                                else
                                {
                                    dr["FieldType"] = drAttr[0].ATTRIBUTE_TYPE.ToString(CultureInfo.InvariantCulture);
                                    dr["SelectedToImport"] = true;
                                    dr["IsSystemField"] = true;
                                }
                                dr["CatalogField"] = sAttributeNameInCatalog;
                                //dr["IsSystemField"] = false;

                            }
                            else
                            {
                                dr["FieldType"] = "";
                                dr["SelectedToImport"] = false;
                            }
                        }
                    }
                    i++;
                }

                return dt;

            }
            catch (Exception objException)
            {
                Logger.Error("Error at loading GetExcelColumn2CatalogMapping in Import Controller", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public DataTable ImportSelectionColumnGrid(string sheetName, string excelPath)
        {
            var dt = new DataTable();
            try
            {
                if (!string.IsNullOrEmpty(excelPath))
                {
                    Workbook workbook = Workbook.Load(excelPath);
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    string strConn;
                    if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    else
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    // DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    // int sheetCount = schemaTable.Rows.Count;
                    //  int sheetColumnCount = 0;

                    if (!sheetName.Contains("$"))
                    {
                        sheetName = sheetName + "$";
                    }
                    dt = new DataTable(sheetName);
                    var dc = new DataColumn("ExcelColumn", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("CatalogField", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("SelectedToImport", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("IsSystemField", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("FieldType", typeof(string));
                    dt.Columns.Add(dc);
                    //DataRow schemaRow = schemaTable.Rows[0];
                    //string sheet = schemaRow["TABLE_NAME"].ToString();
                    //if (!sheet.EndsWith("_"))
                    //{
                    //    string query = "SELECT  * FROM [" + SheetName + "]";
                    //    OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                    //    dtexcel.Locale = CultureInfo.CurrentCulture;
                    //    daexcel.Fill(dtexcel);
                    //}
                    //sheetColumnCount = dtexcel.Columns.Count;

                    //int iCols = sheetColumnCount;
                    string sheetNameworkbook = sheetName.Replace("$", "").TrimEnd();
                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[0].Cells.Count();
                    for (int j = 0; j < iCols; j++)
                    {
                        if (workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[j].Value != null)
                        {
                            String sColName = workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[j].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                            DataRow dr = dt.NewRow();
                            dr[0] = sColName;
                            dt.Rows.Add(dr);
                        }
                    }

                    _dsSheets.Tables.Add(dt);
                    dt.Dispose();
                    _dtable = GetExcelColumn2CatalogMapping(sheetName);
                    conn.Close();
                }
                return _dtable;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at ImportApiController :importSelectionColumnGrid", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public JsonResult GetImportSpecs(string sheetName, string excelPath)
        {
            // ExcelPath = "C:\\Users\\Rakesh\\Downloads\\LS Export.xls";
            DataTable dtable = ImportSelectionColumnGrid(sheetName, excelPath);
            try
            {
                var dynamicColumns = new Dictionary<string, string>();
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                JsonWriter jsonWriter = new JsonTextWriter(sw);
                var model = new DashBoardModel();
                using (DataTableReader reader = dtable.CreateDataReader())
                {
                    jsonWriter.WriteStartObject();
                    jsonWriter.WritePropertyName("Data");
                    jsonWriter.WriteStartArray();
                    while (reader.Read())
                    {
                        jsonWriter.WriteStartObject();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            jsonWriter.WritePropertyName(reader.GetName(i));
                            jsonWriter.WriteValue(reader.GetValue(i).ToString());
                            if (!dynamicColumns.ContainsKey(reader.GetName(i)) && !string.IsNullOrEmpty(reader.GetName(i)))
                            {
                                dynamicColumns.Add(reader.GetName(i), reader.GetValue(i).ToString());
                            }
                        }
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WritePropertyName("Columns");
                    jsonWriter.WriteStartArray();
                    foreach (string key in dynamicColumns.Keys)
                    {
                        jsonWriter.WriteStartObject();
                        jsonWriter.WritePropertyName("title");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WritePropertyName("field");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WriteEndObject();
                }
                model.Data = sb.ToString();
                return new JsonResult { Data = model };

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : GetProdSpecs", objexception);
                return null;
            }
        }
        #region InvertedProductsImport
        /// <summary>
        /// Import the data based on the item#
        /// </summary>
        /// <param name="ImportTable">Datatable from import data</param>
        /// <param name="SheetName">sheet name from imported excel sheet name</param>
        /// <param name="allowDuplicate">allow duplication(0/1)</param>
        /// <param name="excelPath">imported excel sheet path</param>
        /// <param name="catalogId">Current catalog id</param>
        /// <param name="Sheetname">selected import type</param>
        /// <param name="model">Attribute list with name and type</param>
        /// <returns></returns>
        /// 

        public string InvertedProductsImport(DataTable ImportTable, string Sheetname, string allowDuplicate, string excelPath, int catalog_Id, string prodTemp, JArray model)
        {
            string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
            var customerName = User.Identity.Name;
            var customerDetails = objLS.Customers.Where(x => x.EMail == customerName).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            //string[] itemArray = { customerDetails };
            int skippedRecords = 0;
            foreach (DataRow dr in ImportTable.Rows)
            {
                string action = dr["ACTION"].ToString();
                action = action.ToUpper();
                if (action.Equals("SKIP"))
                {
                    skippedRecords = skippedRecords + 1;
                    DataTable skipped = GetDataFromExcel(Sheetname, excelPath, "SELECT * from [" + Sheetname + "$]", "SKIP");
                    //dr.Delete();
                }
            }
            try
            {
                QueryValues queryValues = new QueryValues();


                // CustomerItemNo
                if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
                {

                    int customerIds;
                    int.TryParse(customerDetails.ToString(), out customerIds);
                    string customerCatalog = queryValues.GetCatalogItemNo(customerIds);
                    System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerCatalog;
                }
                // CustomerSubItemNo
                if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
                {

                    int customerId;
                    int.TryParse(customerDetails.ToString(), out customerId);
                    string customerCatalog = queryValues.GetSubCatalogItemNo(customerId);
                    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerCatalog;
                }


                foreach (DataColumn dcItem in ImportTable.Columns)
                {
                    if (dcItem.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerSubItemNo"].ToString())
                        dcItem.ColumnName = "SUBCATALOG_ITEM_NO";
                    if (dcItem.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerItemNo"].ToString())
                        dcItem.ColumnName = "CATALOG_ITEM_NO";
                }
                if (Sheetname.ToUpper().Contains("SUB"))
                {
                    if (!ImportTable.Columns.Contains("SUBPRODUCT_PUBLISH2WEB"))
                    {
                        ImportTable.Columns.Add("SUBPRODUCT_PUBLISH2WEB");
                        ImportTable.Columns["SUBPRODUCT_PUBLISH2WEB"].SetOrdinal(ImportTable.Columns.IndexOf("SUBCATALOG_ITEM_NO"));
                    }
                    if (!ImportTable.Columns.Contains("SUBPRODUCT_PUBLISH2PDF"))
                    {
                        ImportTable.Columns.Add("SUBPRODUCT_PUBLISH2PDF");
                        ImportTable.Columns["SUBPRODUCT_PUBLISH2PDF"].SetOrdinal(ImportTable.Columns.IndexOf("SUBPRODUCT_PUBLISH2WEB"));
                    }
                    if (!ImportTable.Columns.Contains("SUBPRODUCT_PUBLISH2EXPORT"))
                    {
                        ImportTable.Columns.Add("SUBPRODUCT_PUBLISH2EXPORT");
                        ImportTable.Columns["SUBPRODUCT_PUBLISH2EXPORT"].SetOrdinal(ImportTable.Columns.IndexOf("SUBPRODUCT_PUBLISH2PDF"));
                    }
                    if (!ImportTable.Columns.Contains("SUBPRODUCT_PUBLISH2PRINT"))
                    {
                        ImportTable.Columns.Add("SUBPRODUCT_PUBLISH2PRINT");
                        ImportTable.Columns["SUBPRODUCT_PUBLISH2PRINT"].SetOrdinal(ImportTable.Columns.IndexOf("SUBPRODUCT_PUBLISH2EXPORT"));
                    }
                    if (!ImportTable.Columns.Contains("SUBPRODUCT_PUBLISH2PORTAL"))
                    {
                        ImportTable.Columns.Add("SUBPRODUCT_PUBLISH2PORTAL");
                        ImportTable.Columns["SUBPRODUCT_PUBLISH2PORTAL"].SetOrdinal(ImportTable.Columns.IndexOf("SUBPRODUCT_PUBLISH2PRINT"));
                    }
                }
                else
                {

                    //string[] itemArray = { customerDetails };
                    if (!ImportTable.Columns.Contains("PRODUCT_PUBLISH2WEB"))
                    {
                        ImportTable.Columns.Add("PRODUCT_PUBLISH2WEB");
                        ImportTable.Columns["PRODUCT_PUBLISH2WEB"].SetOrdinal(ImportTable.Columns.IndexOf(customerDetails));
                    }
                    if (!ImportTable.Columns.Contains("PRODUCT_PUBLISH2PDF"))
                    {
                        ImportTable.Columns.Add("PRODUCT_PUBLISH2PDF");
                        ImportTable.Columns["PRODUCT_PUBLISH2PDF"].SetOrdinal(ImportTable.Columns.IndexOf("PRODUCT_PUBLISH2WEB"));
                    }
                    if (!ImportTable.Columns.Contains("PRODUCT_PUBLISH2EXPORT"))
                    {
                        ImportTable.Columns.Add("PRODUCT_PUBLISH2EXPORT");
                        ImportTable.Columns["PRODUCT_PUBLISH2EXPORT"].SetOrdinal(ImportTable.Columns.IndexOf("PRODUCT_PUBLISH2PDF"));
                    }
                    if (!ImportTable.Columns.Contains("PRODUCT_PUBLISH2PRINT"))
                    {
                        ImportTable.Columns.Add("PRODUCT_PUBLISH2PRINT");
                        ImportTable.Columns["PRODUCT_PUBLISH2PRINT"].SetOrdinal(ImportTable.Columns.IndexOf("PRODUCT_PUBLISH2EXPORT"));
                    }
                    if (!ImportTable.Columns.Contains("PRODUCT_PUBLISH2PORTAL"))
                    {
                        ImportTable.Columns.Add("PRODUCT_PUBLISH2PORTAL");
                        ImportTable.Columns["PRODUCT_PUBLISH2PORTAL"].SetOrdinal(ImportTable.Columns.IndexOf("PRODUCT_PUBLISH2PRINT"));
                    }
                }

                var specs = Sheetname.ToUpper().Contains("SUBPRODUCT") ?
                    ImportTable.AsEnumerable().Select(item => item.Field<String>("SUBCATALOG_ITEM_NO")).Distinct().ToArray() : ImportTable.AsEnumerable().Select(item => item.Field<String>(customerDetails)).Distinct().ToArray();
                var product = Sheetname.ToUpper().Contains("SUBPRODUCT") ? objLS.TB_SUBPRODUCT.Join(objLS.TB_PROD_SPECS, ts => ts.SUBPRODUCT_ID, tps => tps.PRODUCT_ID, (ts, tps) => new { ts, tps }).Where(y => y.ts.CATALOG_ID == catalog_Id && y.tps.ATTRIBUTE_ID == 1 && specs.Contains(y.tps.STRING_VALUE)).Select(x => x.tps.PRODUCT_ID).Distinct().Count() :
                    objLS.TB_CATALOG_PRODUCT.Join(objLS.TB_PROD_SPECS, ts => ts.PRODUCT_ID, tps => tps.PRODUCT_ID, (ts, tps) => new { ts, tps }).Where(y => y.ts.CATALOG_ID == catalog_Id && y.tps.ATTRIBUTE_ID == 1 && specs.Contains(y.tps.STRING_VALUE)).Select(x => x.tps.PRODUCT_ID).Distinct().Count();

                if (GetSkuCount(product) == "SKU Exceed")
                {
                    return "SKU Exceed~" + prodTemp;
                }
                var customers = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();

                //-----------------------------------------------------------Import sheet count handling starting------------------------------------------------------//
                int maximumSheetLength = objLS.TB_APPLICATION_SETTINGS.Where(x => x.CustomerId == customers).Select(y => y.ImportSheetProductCount).FirstOrDefault();
                //int maximumSheetLength = Int32.Parse(ConfigurationManager.AppSettings["ImportSheetProductCount"].ToString());
                int currentSheetLength = ImportTable.Rows.Count;
                int noOfIteration = (currentSheetLength / maximumSheetLength) + 1;

                int importStartCount = 0;
                int importEndCount = maximumSheetLength;

                DataTable resultSet = new DataTable();
                DataSet resultSet1 = new DataSet();

                while (noOfIteration >= 1)
                {
                    DataTable copyItemImportValues = ImportTable.Copy();
                    DataTable loopImportData = new DataTable();
                    string sqlString = string.Empty;
                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        sqlString = string.Format("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTDATA{0}'')BEGIN DROP TABLE [##IMPORTDATA{0}] END')", prodTemp);
                        SqlCommand newCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlConnection.Open();
                        newCommand.ExecuteNonQuery();

                        sqlString = CreateTable("[##IMPORTDATA" + prodTemp + "]", copyItemImportValues);
                        newCommand = new SqlCommand(sqlString, sqlConnection);
                        newCommand.ExecuteNonQuery();

                        var bulkCopy = new SqlBulkCopy(sqlConnection)
                        {
                            DestinationTableName = "[##IMPORTDATA" + prodTemp + "]"
                        };
                        bulkCopy.WriteToServer(copyItemImportValues);

                        sqlString = string.Format("IF OBJECT_ID('IMPORTTEMPB4IMPORT{0}', 'U') IS NOT NULL   DROP TABLE [IMPORTTEMPB4IMPORT{0}] select IDENTITY(INT,1,1) AS RowID,* into [IMPORTTEMPB4IMPORT{0}] from [##IMPORTDATA{0}]", prodTemp);
                        newCommand = new SqlCommand(sqlString, sqlConnection);
                        newCommand.ExecuteNonQuery();

                        sqlString = string.Format("select * from [IMPORTTEMPB4IMPORT{0}] where RowID  between {1} and {2}", prodTemp, importStartCount, importEndCount);
                        newCommand = new SqlCommand(sqlString, sqlConnection);
                        SqlDataAdapter importDataDatatable = new SqlDataAdapter(newCommand);
                        importDataDatatable.Fill(loopImportData);
                        sqlConnection.Close();
                    }

                    DataTable importData = UnPivotTable(loopImportData, Sheetname);

                    DataTable attrType = SelectedColumnsToImportforProduct(ImportTable, model);
                    DataTable AttributeData = new DataTable();
                    AttributeData.Columns.Add("AttrtId");
                    AttributeData.Columns["AttrtId"].DataType = System.Type.GetType("System.Int32");
                    AttributeData.Columns["AttrtId"].AutoIncrement = true;
                    AttributeData.Columns["AttrtId"].AutoIncrementSeed = 1;
                    AttributeData.Columns.Add("ATTRIBUTENAME");
                    AttributeData.Columns.Add("ATTRIBUTETYPE");
                    for (int index = 0; index < attrType.Rows.Count; index++)
                    {
                        DataRow dr = AttributeData.NewRow();
                        dr["ATTRIBUTENAME"] = attrType.Rows[index]["ATTRIBUTE_NAME"].ToString();
                        dr["ATTRIBUTETYPE"] = attrType.Rows[index]["ATTRIBUTE_TYPE"].ToString();
                        AttributeData.Rows.Add(dr);
                    }

                    // -------------------------------------------------------- Mapping - Attributes - Start.                    

                    var Mappingattributes = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                    if (Mappingattributes.Count != 0)
                    {
                        for (int i = 0; i < Mappingattributes.Count; i++)
                        {
                            for (int j = 0; j < importData.Rows.Count; j++)
                            {
                                if (Mappingattributes[i].Excel_Column.ToString() == importData.Rows[j]["ATTRIBUTENAME"].ToString())
                                {
                                    importData.Rows[j]["ATTRIBUTENAME"] = Mappingattributes[i].Mapping_Name;
                                }
                            }
                        }
                    }

                    //Mapping Attribute - End ---------------------------------------------------------------------
                    var MappingattributesValue = objLS.QS_IMPORTMAPPING.Select(x => x).ToList();

                    if (MappingattributesValue.Count != 0)
                    {
                        for (int i = 0; i < MappingattributesValue.Count; i++)
                        {
                            for (int j = 0; j < AttributeData.Rows.Count; j++)
                            {
                                if (Mappingattributes[i].Excel_Column.ToString() == AttributeData.Rows[j]["ATTRIBUTENAME"].ToString())
                                {
                                    AttributeData.Rows[j]["ATTRIBUTENAME"] = Mappingattributes[i].Mapping_Name;
                                }
                            }
                        }
                    }



                    //Mapping Attribute - End ---------------------------------------------------------------------                    

                    SqlCommand sqlCommand = new SqlCommand();
                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        if (Sheetname.ToUpper().Contains("SUBPRODUCT"))
                        {
                            sqlCommand = new SqlCommand("STP_CATALOGSTUDIO5_SUBCATALOGITEMNUMBER_IMPORT", sqlConnection);
                        }
                        else
                        {
                            sqlCommand = new SqlCommand("STP_CATALOGSTUDIO5_CATALOGITEMNUMBER_IMPORT", sqlConnection);
                        }
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.CommandTimeout = 0;
                        sqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalog_Id;
                        sqlCommand.Parameters.Add("@SESSIONID", SqlDbType.NVarChar).Value = prodTemp;
                        sqlCommand.Parameters.Add("@CUSTOMERNAME", SqlDbType.VarChar).Value = User.Identity.Name;
                        sqlCommand.Parameters.Add("@CUSTOMERID", SqlDbType.VarChar).Value = customers.ToString();
                        sqlCommand.Parameters.Add("@TEMPTABLE", SqlDbType.Structured).Value = importData;
                        sqlCommand.Parameters.Add("@ATTRTEMP", SqlDbType.Structured).Value = AttributeData;
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sqlCommand);
                        sqlConnection.Open();
                        sqlDataAdapter.Fill(resultSet1);
                        sqlConnection.Close();
                    }

                    using (var sqlConnection = new SqlConnection(connectionString))
                    {
                        sqlString = string.Format("IF OBJECT_ID('IMPORTTEMPB4IMPORT{0}', 'U') IS NOT NULL   DROP TABLE [IMPORTTEMPB4IMPORT{0}]", prodTemp);
                        SqlCommand newCommand = new SqlCommand(sqlString, sqlConnection);
                        sqlConnection.Open();
                        newCommand.ExecuteNonQuery();
                        sqlConnection.Close();
                    }

                    noOfIteration = noOfIteration - 1;
                    importStartCount = importEndCount + 1;
                    importEndCount = importEndCount + maximumSheetLength;
                }
                //-----------------------------------------------------------Import sheet count handling ends------------------------------------------------------//

                resultSet = resultSet1.Tables[0];
                sw.Stop();
                string customer_Name = User.Identity.Name;
                string calcualteImportTiminings = string.Format("{0}:{1}:{2}", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds);

                //int skipped = 0;
                int deleteRecords = 0;
                DataTable skipped = GetDataFromExcel(Sheetname, excelPath, "SELECT * from [" + Sheetname + "$]", "SKIP");
                if (resultSet != null && resultSet.Columns.Contains("STATUS"))
                {
                    if (resultSet.Rows.Count > 0)
                    {
                        Logger.Error("New Count");
                        Logger.Error(resultSet.Rows.Count);
                        int insertedRecords = Sheetname.ToUpper().Contains("SUBPRODUCT") ? resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "INSERT").Select(cat => cat.Field<string>("SUBCATALOG_ITEM_NO")).Distinct().Count() : resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "INSERT").Select(cat => cat.Field<string>("CATALOG_ITEM_NO")).Distinct().Count();
                        int updatedRecords = Sheetname.ToUpper().Contains("SUBPRODUCT") ? resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "UPDATE").Select(cat => cat.Field<string>("SUBCATALOG_ITEM_NO")).Distinct().Count() : resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "UPDATE").Select(cat => cat.Field<int>("PRODUCT_ID")).Distinct().Count();
                        Logger.Error("New Update Count");
                        Logger.Error(updatedRecords);
                        //

                        // int updatedRecourds = importType.ToUpper().Contains("SUBPRODUCT") ? resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "UPDATE" || string.IsNullOrEmpty(x.Field<string>("STATUS"))).Select(cat => cat.Field<string>("SUBCATALOG_ITEM_NO")).Distinct().Count() : resultSet.AsEnumerable().Where(x => x.Field<string>("STATUS") == "UPDATE" || string.IsNullOrEmpty(x.Field<string>("STATUS"))).Select(cat => cat.Field<string>("CATALOG_ITEM_NO")).Distinct().Count();
                        if (skipped != null && skipped.Rows.Count > 0)
                        {
                            Logger.Error(skipped.Rows.Count);
                            return "Import Success~" + prodTemp + "~InsertRecords:" + insertedRecords + "~" + "UpdateRecords:" + updatedRecords + "~SkippedRecords:" + skipped.Rows.Count + "~ITEM#";
                        }
                        else
                        {
                            return "true" + "~" + insertedRecords + "~" + updatedRecords + "~" + calcualteImportTiminings + "~" + skippedRecords + "~" + deleteRecords + "~" + customer_Name; ;
                        }
                    }
                    else
                    {
                        if (skipped != null && skipped.Rows.Count > 0)
                        {
                            return "Import Success~" + prodTemp + "~InsertRecords:0~" + "UpdateRecords:0~SkippedRecords:" + skipped.Rows.Count + "~ITEM#";
                        }
                        else
                        {
                            return "Import Success~" + prodTemp + "~InsertRecords:0~" + "UpdateRecords:0~SkippedRecords:0" + "~ITEM#" + customerName;
                        }
                    }
                }

                else
                {
                    if (resultSet != null && resultSet.Rows.Count > 0)
                    {
                        HttpContext.Current.Session["ITEMIMPORTSESSION"] = resultSet;
                        return "Import Failed~" + prodTemp + "~ITEM#";
                    }
                    return "Import Failed~" + prodTemp + "~ITEMEMPTY";
                }
            }

            catch (Exception ex)
            {
                Logger.Error("Error at AdvanceImportApiController : CatalogItemNumberImport", ex);
                return "Import failed";
            }
        }
        #endregion
        public DataTable SelectedColumnsToImportforProduct(DataTable objdatatable, JArray model)
        {
            try
            {
                var customerName = User.Identity.Name;
                var customerDetails = objLS.Customers.Where(x => x.EMail == customerName).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();
                var functionAlloweditems = ((JArray)model).Select(x => new SelectedAttribute()
                {
                    ExcelColumn = (string)x["ExcelColumn"],
                    FieldType = (string)x["FieldType"],
                    SelectedToImport = x["SelectedToImport"].ToString().ToLower() == "true" ? true : false
                }).ToList();

                DataTable dt = new DataTable();
                dt.Columns.Add("ATTRIBUTE_TYPE");
                dt.Columns.Add("ATTRIBUTE_NAME");
                var tbattrcol = objLS.TB_ATTRIBUTE.Find(1);
                foreach (var item in functionAlloweditems)
                {
                    if (item.SelectedToImport)
                    {
                        if (item.ExcelColumn != System.Web.HttpContext.Current.Session["CustomerItemNo"].ToString())
                        {
                            DataRow dr = dt.NewRow();
                            dr["ATTRIBUTE_TYPE"] = item.FieldType;
                            dr["ATTRIBUTE_NAME"] = item.ExcelColumn;
                            dt.Rows.Add(dr);
                        }
                        else
                        {
                            DataRow dr = dt.NewRow();
                            dr["ATTRIBUTE_TYPE"] = item.FieldType;
                            dr["ATTRIBUTE_NAME"] = customerDetails;
                            dt.Rows.Add(dr);
                        }
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at SelectedColumnsToImportforProduct : exceldata", ex);
                return null;
            }
        }
        #region UnPivot DataTable
        /// <summary>
        /// UnPivot DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public DataTable UnPivotTable(DataTable dataItems, string importType)
        {
            var customerName = User.Identity.Name;
            var customerDetails = objLS.Customers.Where(x => x.EMail == customerName).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();
            try
            {
                string[] columns = dataItems.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();
                DataTable unPivotTable = new DataTable();
                unPivotTable.Columns.Add("ImportId");
                unPivotTable.Columns["ImportId"].DataType = System.Type.GetType("System.Int32");
                unPivotTable.Columns["ImportId"].AutoIncrement = true;
                unPivotTable.Columns["ImportId"].AutoIncrementSeed = 1;
                if (importType.ToUpper().Contains("BULK"))
                    unPivotTable.Columns.Add("ROW");
                unPivotTable.Columns.Add(customerDetails);
                if (importType.ToUpper().Contains("SUBPRODUCT"))
                {
                    unPivotTable.Columns.Add("SUBCATALOG_ITEM_NO");
                    unPivotTable.Columns.Add("SUBPRODUCT_ID");
                }

                unPivotTable.Columns.Add("ATTRIBUTENAME");
                unPivotTable.Columns.Add("ATTRIBUTEVALUE");
                unPivotTable.Columns.Add("PRODUCT ID");
                if (!dataItems.Columns.Contains("PRODUCT ID"))
                    dataItems.Columns.Add("PRODUCT ID");
                if (dataItems.Columns.Contains("PRODUCT ID"))
                    dataItems.Columns["PRODUCT ID"].SetOrdinal(0);
                if (importType.ToUpper().Contains("SUBPROD") && !dataItems.Columns.Contains("SUBPRODUCT_ID"))
                    dataItems.Columns.Add("SUBPRODUCT_ID");
                if (importType.ToUpper().Contains("SUBPROD") && dataItems.Columns.Contains("SUBPRODUCT_ID"))
                    dataItems.Columns["SUBPRODUCT_ID"].SetOrdinal(2);
                for (int rowIndex = 0; rowIndex < dataItems.Rows.Count; rowIndex++)
                {
                    for (int index = 0; index < columns.Length; index++)
                    {
                        if (!columns[index].ToUpper().Contains("CATALOG_ITEM_NO") && !columns[index].ToUpper().Contains("PRODUCT ID") && !columns[index].ToUpper().Contains("SUBPRODUCT_ID") && (!columns[index].ToUpper().Contains("ACTION") || importType.Contains("BATCH")))
                        {
                            DataRow dr = unPivotTable.NewRow();
                            dr[customerDetails] = dataItems.Rows[rowIndex][customerDetails];
                            if (importType.ToUpper().Contains("SUBPRODUCT"))
                            {
                                dr["SUBCATALOG_ITEM_NO"] = dataItems.Rows[rowIndex]["SUBCATALOG_ITEM_NO"];
                                if (dataItems.Columns[columns[index]].ColumnName.ToUpper() == "SUBPRODUCT_ID")
                                {
                                    dataItems.Columns[columns[index]].ColumnName = "SUBPRODUCT_ID";
                                }
                                dr["SUBPRODUCT_ID"] = dataItems.Rows[rowIndex]["SUBPRODUCT_ID"];

                            }
                            dr["ATTRIBUTENAME"] = columns[index];
                            dr["ATTRIBUTEVALUE"] = dataItems.Rows[rowIndex][columns[index]].ToString();
                            if (dataItems.Columns[columns[index]].ColumnName.ToUpper() == "PRODUCT ID")
                            {
                                dataItems.Columns[columns[index]].ColumnName = "PRODUCT ID";
                            }
                            dr["PRODUCT ID"] = dataItems.Rows[rowIndex]["PRODUCT ID"];
                            if (importType.ToUpper().Contains("BULK"))
                                dr["ROW"] = rowIndex;
                            unPivotTable.Rows.Add(dr);
                        }
                    }
                }
                if (unPivotTable.Columns.Contains("PRODUCT ID"))
                {
                    unPivotTable.Columns["PRODUCT ID"].SetOrdinal(unPivotTable.Columns.IndexOf(customerDetails));
                }
                if (unPivotTable.Columns.Contains("SUBPRODUCT_ID"))
                {
                    unPivotTable.Columns["SUBPRODUCT_ID"].SetOrdinal(unPivotTable.Columns.IndexOf("SUBCATALOG_ITEM_NO"));
                }

                return unPivotTable;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at AdvanceImportApiController : UnPivotTable", ex);
                return null;
            }
        }


        #endregion


    }
}
