﻿using log4net;
using LS.Data;
using LS.Data.Model;
using LS.Data.Model.ProductPreview;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Xml;



namespace LS.Web.Controllers
{
    public class InDesignApiController : ApiController
    {


        public string Attr_List = string.Empty;
        public string _SQLString = "";
        string _xmlpath = string.Empty;
        private static int _catalogId;
        private static int _categoryId;
        public bool Flg_Chk = false;
        private int _fileCreation;
        private string advtable;
        private string TMode = string.Empty;
        private static int ContinueCalclautedCols;
        private static int mfileNO;
        private static int mProjectID;

        public bool flg = false;
        public OleDbConnection myConn;
        public OleDbDataAdapter catDA;
        public OleDbCommand catSQL;
        public DataSet catDS;
        private int rowCnt, rowFamCnt, rowFamAttrCnt, rowCatAttrCnt, rowCatImgCnt, rowCatAttrLowLevelCnt, rowCatAttrLowLevelCnts, rowCatImgCntLowAttr, rowCatImgCntLow, rowSupp, dRow, dCol, TableHeader; //rowtrCnt, totRow, totCol, bpricecnt, 
        private int rowFamImgCnt, FamCnt, NumFam, rowSubFamCnt, rowPHCnt, totColCnt, totalRowCnt, totXMLColCnt; //totRowCnt, rowCurr,
        private string ImgFile, FPath, ImageName, SuppImg, SuppImgName;
        private string FamilyId, MainFamily, SubFamily, ProductData, CellName;
        string cellvalues; string XMLcell; string[] cellname; string valuee;
        int[] rowlevel; int[] cellposval; int[] cellposition; int[] GrpColno; int grpno;
        string XMLdes, GrpColItem; int levelpros; //, gropsize;
        int rowVal = -1;
        string columnName;
        private bool _loadXFamily;
        private bool _loadXProductTable;
        private string XMLStr, TableType; //FormatType, , strBasePrice
        private string AttrName; //, OptionValue , OptAttrId, CurSymbol, CurSymbolApply, CurEmpty, CurFormat

        private int _familyId;
        private string _categoryID = string.Empty;
        private int[] _attributeIdList;
        private bool _mergeFamilyWithProductTable;
        string _catName = string.Empty;
        private int _FamilyId = 0;
        private int[] _AttributeIdList;
        string CatId = string.Empty;
        public bool PDFCatalog = false;
        //private string AttributeValue,Publish2Print; 
        private static int CheckCalculatedCtr;
        private static bool fFilter1 = false;
        private static bool fFilter2 = false;
        private static bool fFilter3 = false;
        private static string sOperand1;
        private static string sOperand2;
        private static string sOperand3;
        public string convertionPath = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePath"].ToString();
        public string convertionPathREA = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePathREA"].ToString();
        string _prefix = string.Empty; string _suffix = string.Empty; string _emptyCondition = string.Empty; string _replaceText = string.Empty; string _headeroptions = string.Empty; string _fornumeric = string.Empty;
        string Prefix = string.Empty; string Suffix = string.Empty; string EmptyCondition = string.Empty; string ReplaceText = string.Empty; string Headeroptions = string.Empty;
        int Mergecellcount; int totalrows, totalcols, crow, AllinGroup = 0; int HeaderID;
        private StringBuilder strBuildXMLOutput = new StringBuilder();
        StringBuilder strBuildXMLOutput1 = new StringBuilder();
        readonly XMLGeneration _xmlgen = new XMLGeneration();
        Random RandomClass = new Random();
        DataSet oDsProdFilter = new DataSet();
        DataSet oDsFamilyFilter = new DataSet();
        DataSet oDSSubFam = new DataSet();
        DataSet oXMLDS_CatalogCategory = new DataSet();
        DataSet oDSFam = new DataSet();
        DataSet _dtAvailAttr = new DataSet();
        DataSet _selectattr = new DataSet();
        DataSet oXMLDS_FamilySpecs = new DataSet();
        DataSet oXMLDS_FamilyImages_ = new DataSet();
        DataSet oXMLDS_CategorySpecs = new DataSet();
        DataSet oXMLDS_CategoryImage = new DataSet();
        DataSet oXMLDS_CategorySpecsLowLevel = new DataSet();
        DataSet oXMLDS_CategoryImageLowLevel = new DataSet();
        DataSet oXMLDS_CategorySpecsAttLowLevel = new DataSet();
        DataSet oXMLDS_CategoryImageAttLowLevel = new DataSet();
        DataSet oDSSupp = new DataSet();
        DataSet oRefproductTable = new DataSet();
        string TableMode = string.Empty;
        DataSet DupDsPreview = new DataSet();
        string XML;
        public string CustomerFolder = string.Empty;
        public string _xmlpath1 = HttpContext.Current.Server.MapPath("~/Content/ProductImages");
        string ImageMagickPath = string.Empty;
        string converttype = string.Empty;
        string Exefile = string.Empty;

        readonly CSEntities _dbcontext = new CSEntities();
        static readonly ILog Logger = LogManager.GetLogger(typeof(TableDesignerApiController));
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage XmlDataFiles(int catalogId, bool flagallcreate, int selectedprojectId, bool idColumnFlag, Object model1) //
        {
            try
            {
                var model = JArray.FromObject(model1);
                if (flagallcreate)
                {


                    var attributes = model[1].Select(x => x);
                    var objAttributePublishesList = new List<AttributePublish>();
                    var updatedXmlGenerationDate = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.PROJECT_ID == selectedprojectId).Select(x => x).FirstOrDefault();
                    updatedXmlGenerationDate.MODIFIED_DATE = DateTime.Now;
                    _dbcontext.SaveChanges();
                    foreach (var selectattributes in attributes)
                    {
                        var objAttributedetails = new AttributePublish();
                        objAttributedetails.CATALOG_ID = (int)selectattributes.SelectToken("CATALOG_ID");

                        objAttributedetails.FAMILY_ID = (int)selectattributes.SelectToken("FAMILY_ID");
                        objAttributedetails.ATTRIBUTE_ID = (int)selectattributes.SelectToken("ATTRIBUTE_ID");
                        objAttributedetails.ATTRIBUTE_TYPE = (int)selectattributes.SelectToken("ATTRIBUTE_TYPE");
                        objAttributedetails.ATTRIBUTE_NAME = (string)selectattributes.SelectToken("ATTRIBUTE_NAME");
                        objAttributedetails.ISAvailable = (bool)selectattributes.SelectToken("ISAvailable");
                        objAttributePublishesList.Add(objAttributedetails);
                    }
                    XmlDataCreateAllDataFiles(catalogId, selectedprojectId, objAttributePublishesList, true, idColumnFlag);
                    updatedXmlGenerationDate.XML_GENERATED_DATE = DateTime.Now;
                    _dbcontext.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "XML generated successfully");
                }
                else
                {
                    var objProjectSections = new TB_PROJECT_SECTIONS();
                    objProjectSections.FILE_NO = (int)model[0].SelectToken("FILE_NO");
                    objProjectSections.PROJECT_ID = (int)model[0].SelectToken("PROJECT_ID");
                    objProjectSections.RECORD_ID = (int)model[0].SelectToken("RECORD_ID");
                    objProjectSections.PAGE_FILE_NAME = (string)model[0].SelectToken("PAGE_FILE_NAME");
                    objProjectSections.TEMPLATE_FILE_NAME = (string)model[0].SelectToken("TEMPLATE_FILE_NAME");
                    objProjectSections.TEMPLATE_FILE_NAME = (string)model[0].SelectToken("TEMPLATE_FILE_NAME");
                    objProjectSections.XML_FILE_NAME = (string)model[0].SelectToken("XML_FILE_NAME");
                    objProjectSections.XSD_FILE_NAME = (string)model[0].SelectToken("XSD_FILE_NAME");

                    var attributes = model[1].Select(x => x);
                    var objAttributePublishesList = new List<AttributePublish>();
                    
                    _dbcontext.SaveChanges();
                    foreach (var selectattributes in attributes)
                    {
                        var objAttributedetails = new AttributePublish();
                        objAttributedetails.CATALOG_ID = (int)selectattributes.SelectToken("CATALOG_ID");
                        objAttributedetails.FAMILY_ID = (int)selectattributes.SelectToken("FAMILY_ID");
                        objAttributedetails.ATTRIBUTE_ID = (int)selectattributes.SelectToken("ATTRIBUTE_ID");
                        objAttributedetails.ATTRIBUTE_TYPE = (int)selectattributes.SelectToken("ATTRIBUTE_TYPE");
                        objAttributedetails.ATTRIBUTE_NAME = (string)selectattributes.SelectToken("ATTRIBUTE_NAME");
                        objAttributedetails.ISAvailable = (bool)selectattributes.SelectToken("ISAvailable");
                        objAttributePublishesList.Add(objAttributedetails);
                    }
                    var staus = XmlDataFile(catalogId, objProjectSections.XML_FILE_NAME, 0, objProjectSections.XML_FILE_NAME,
                          objProjectSections.RECORD_ID, objProjectSections.FILE_NO, objProjectSections.PROJECT_ID, true, objAttributePublishesList, idColumnFlag);
                    if (staus.StatusCode == HttpStatusCode.OK)
                    {
                        var updatedXmlGenerationDate = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.PROJECT_ID == selectedprojectId && x.RECORD_ID == objProjectSections.RECORD_ID).Select(x => x).FirstOrDefault();
                        updatedXmlGenerationDate.XML_GENERATED_DATE = DateTime.Now;
                        _dbcontext.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, "XML generated successfully");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "One or few of the Families are no longer available in CatalogStudio");
                    }
                    // return Request.CreateResponse(HttpStatusCode.OK, "Success. XML Generated.");
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InDesignApiController : XmlDataFiles", objexception);
                return null;
            }

        }

        static long DirectorySize(DirectoryInfo dInfo, bool includeSubDir)
        {
            long totalSize = dInfo.EnumerateFiles()
                         .Sum(file => file.Length);
            if (includeSubDir)
            {
                totalSize += dInfo.EnumerateDirectories()
                         .Sum(dir => DirectorySize(dir, true));
            }
            return totalSize;
        }

        /// <summary>
        /// To compress the xml generated folder and to download
        /// Written by Jothipriya
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public string downloadXMLFilePath()
        {
            try
            {
                var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                int customerId = customerid.CustomerId;
                CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.CustomerId == customerId).Select(x => x.a.Comments).FirstOrDefault();
                string path = System.IO.Path.Combine(CustomerFolder, "Indesignxml");
                string xmlPathName =   path + @"\" ;
                return xmlPathName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage XmlDataFileswithoutAttr(int catalogId, bool flagallcreate, int selectedprojectId, bool idColumnFlag, JArray model) //
        {
            try
            {
                string alert = "";

                var productAttributeFilterList = model[1];

                HttpContext.Current.Session["productAttributeFilterList"] = productAttributeFilterList;

                HttpContext.Current.Session["userName"] = User.Identity.Name;

                var SpaceProvided = _dbcontext.TB_PLAN
                          .Join(_dbcontext.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                          .Join(_dbcontext.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                          .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => x.tcptps.tps.StorageGB).ToList();
                double alotment = (double)SpaceProvided[0];
                var userId = _dbcontext.Customer_User.Join(_dbcontext.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();
                double folderSize = 0;
                if (string.IsNullOrEmpty(userId))
                {
                    userId = "Questudio";
                }

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + ""));
                }
                var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "");
                DirectoryInfo dInfo = new DirectoryInfo(path);
                long sizeOfDir = DirectorySize(dInfo, true);
                folderSize = (((double)sizeOfDir) / (double)(1024 * 1024 * 1024));

                double availableSpace = alotment - folderSize;
                if (availableSpace > 0)
                {
                    var usedPercentage = ((folderSize / alotment) * 100);

                    if (flagallcreate)
                    {
                        XmlDataCreateAllDataFiles(catalogId, selectedprojectId, null, false, idColumnFlag);
                        var AssetLimitPercentage = _dbcontext.Customer_Settings.Where(x => x.CustomerId == 1);
                        if (AssetLimitPercentage.Any())
                        {
                            var assetLimitPercentageno = AssetLimitPercentage.FirstOrDefault();
                            double asset = Convert.ToDouble(assetLimitPercentageno.AssetLimitPercentage);
                            var updatedXmlGenerationDate = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.PROJECT_ID == selectedprojectId).Select(x => x).FirstOrDefault();
                            updatedXmlGenerationDate.MODIFIED_DATE = DateTime.Now;
                            _dbcontext.SaveChanges();
                            if (usedPercentage >= asset)
                            {
                                
                                updatedXmlGenerationDate.XML_GENERATED_DATE = DateTime.Now;
                                _dbcontext.SaveChanges();
                                alert = "XML generated successfully. You are about to reach the maximum memory";
                            }
                            else
                            {
                                updatedXmlGenerationDate.XML_GENERATED_DATE = DateTime.Now;
                                _dbcontext.SaveChanges();
                                alert = "XML generated successfully";
                            }
                        }

                    }
                    else
                    {

                        var objProjectSections = new TB_PROJECT_SECTIONS();
                        objProjectSections.FILE_NO = (int)model[0].SelectToken("FILE_NO");
                        objProjectSections.PROJECT_ID = (int)model[0].SelectToken("PROJECT_ID");
                        objProjectSections.RECORD_ID = (int)model[0].SelectToken("RECORD_ID");
                        objProjectSections.PAGE_FILE_NAME = (string)model[0].SelectToken("PAGE_FILE_NAME");
                        objProjectSections.TEMPLATE_FILE_NAME = (string)model[0].SelectToken("TEMPLATE_FILE_NAME");
                        objProjectSections.TEMPLATE_FILE_NAME = (string)model[0].SelectToken("TEMPLATE_FILE_NAME");
                        objProjectSections.XML_FILE_NAME = (string)model[0].SelectToken("XML_FILE_NAME");
                        objProjectSections.XSD_FILE_NAME = (string)model[0].SelectToken("XSD_FILE_NAME");
                        var staus = XmlDataFile(catalogId, objProjectSections.XML_FILE_NAME, 0, objProjectSections.XML_FILE_NAME,
                               objProjectSections.RECORD_ID, objProjectSections.FILE_NO, objProjectSections.PROJECT_ID, false, null, idColumnFlag);
                        if (staus.StatusCode == HttpStatusCode.OK)
                        {
                            var AssetLimitPercentage = _dbcontext.Customer_Settings.Where(x => x.CustomerId == 1);
                            if (AssetLimitPercentage.Any())
                            {
                                var updatedXmlGenerationDate = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.RECORD_ID == objProjectSections.RECORD_ID).Select(x => x).FirstOrDefault();
                                updatedXmlGenerationDate.MODIFIED_DATE = DateTime.Now;
                                _dbcontext.SaveChanges();
                                var assetLimitPercentageno = AssetLimitPercentage.FirstOrDefault();
                                double asset = Convert.ToDouble(assetLimitPercentageno.AssetLimitPercentage);
                                if (usedPercentage >= asset)
                                {
                                    updatedXmlGenerationDate.XML_GENERATED_DATE = DateTime.Now;
                                    _dbcontext.SaveChanges();
                                    alert = "XML generated successfully. You are about to reach the maximum memory";
                                }
                                else
                                {
                                    updatedXmlGenerationDate.XML_GENERATED_DATE = DateTime.Now;
                                    _dbcontext.SaveChanges();
                                    alert = "XML generated successfully";
                                }
                            }
                        }
                        else
                        {
                            alert = "One or few of the Families are no longer available in CatalogStudio";
                        }
                    }
                }
                else
                {
                    alert = "You have reached the maximum allotted memory, please contact Administrator";
                }
                return Request.CreateResponse(HttpStatusCode.OK, alert);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InDesignApiController : XmlDataFiles", objexception);
                return null;
            }

        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage XmlDataCreateAllDataFiles(int catalogId, int projectId, List<AttributePublish> attrlists, bool attrselect, bool idColumnFlag)
        {
            try
            {
                //  int fileCre = 0;
                //    bool fileExist = false;
                //string _ExistingfileName = "SELECT PROJECT_ID, XML_FILE_NAME FROM TB_PROJECT_SECTIONS WHERE PROJECT_ID = " + _ProjectID.ToString();
                //  const string existingfileName = "SELECT PROJECT_ID, XML_FILE_NAME FROM TB_PROJECT_SECTIONS";
                //   _dbConn.SQLString = existingfileName;
                //   _dsExistingFileName = _dbConn.CreateDataSet();

                // var allrecordsprojectsection = _dbcontext.TB_PROJECT_SECTIONS.Select(x => x).ToList();
                int cntFamily = 0;
                // int _xmlName = 0;

                var dsrecordId = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.PROJECT_ID == projectId).Select(x => x).ToList();

                int fileCount = -1;
                foreach (TB_PROJECT_SECTIONS drow in dsrecordId)
                {

                    //  grdFileOrg.ActiveRow = null;
                    // grdFileOrg.Rows[0].Selected = false;
                    //  _dbConn.SQLString = "SELECT FAMILY_ID FROM TB_PROJECT_SECTION_DETAILS WHERE RECORD_ID = " +
                    //                    drow["RECORD_ID"];
                    // DataSet dscntfamily = _dbConn.CreateDataSet();
                    var dscntfamily = _dbcontext.TB_PROJECT_SECTION_DETAILS.Where(x => x.RECORD_ID == drow.RECORD_ID);
                    if (dscntfamily.Any())
                    {
                        cntFamily++;
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Please add Families to this file and continue" + drow.XML_FILE_NAME);
                    }
                    fileCount++;
                }


                // var dscntfamilies = _dbcontext.TB_PROJECT_SECTION_DETAILS.Where(x => x.RECORD_ID == drow.RECORD_ID);
                if (dsrecordId.Any())
                {

                    foreach (TB_PROJECT_SECTIONS drow in dsrecordId)
                    {
                        if (!string.IsNullOrEmpty(drow.XML_FILE_NAME))
                        {
                            if (drow.XML_FILE_NAME.ToLower().Contains(".xml"))
                            {
                                _xmlpath = drow.XML_FILE_NAME;
                            }
                            else
                            {
                                _xmlpath = drow.XML_FILE_NAME + ".xml";
                                _xmlpath = drow.XML_FILE_NAME + ".xml";
                            }
                        }
                        XmlDataFile(catalogId, drow.XML_FILE_NAME, 0, drow.XML_FILE_NAME,
                      drow.RECORD_ID, drow.FILE_NO, drow.PROJECT_ID, attrselect, attrlists, idColumnFlag);
                        // XmlDataFile(_xmlpath, 1);
                        var UpdateGeneratedXML = _dbcontext.TB_PROJECT_SECTIONS.Where(x => x.RECORD_ID == drow.RECORD_ID).Select(x => x).ToList();
                        for(int count = 0; count < UpdateGeneratedXML.Count; count++)
                        {
                            UpdateGeneratedXML[count].MODIFIED_DATE = DateTime.Now;
                            UpdateGeneratedXML[count].XML_GENERATED_DATE = DateTime.Now;
                        }

                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, "XML generated successfully");

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InDesignApiController : XmlDataFiles", objexception);
                return null;
            }

        }


        [System.Web.Http.HttpPost]
        public HttpResponseMessage XmlDataFile(int catalogID, string xmlDataPath, int dataFileCount, string xmlFileName, int recordID, int fileNo, int projectID, bool attrselect, List<AttributePublish> attrlists, bool idColumnFlag) //
        {
            string inDesignResult = string.Empty;
            _catalogId = catalogID;
            var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);



            int customerId = customerid.CustomerId;
            var CustomizableItem = _dbcontext.Customers.Where(x => x.CustomerId == customerId).Select(x => x.CustomizeItemNo).FirstOrDefault();
            if (xmlFileName != "") // grdFileOrg.ActiveRow.Cells["XML_FILE_NAME"].Text.Trim() //grdFileOrg.ActiveRow != null &&
            {
                if (true) //grdSectionOrg.DisplayLayout.Rows.Count > 0
                {
                    //string recordId = RECORD_ID; // grdFileOrg.ActiveRow.Cells["RECORD_ID"].Value.ToString();

                    _xmlgen.CatalogId = Convert.ToInt32(_catalogId);
                    string strsql = "";
                    //testing1
                    strsql = "SELECT DISTINCT C.CATEGORY_ID FROM TB_CATALOG_FAMILY TCF,TB_FAMILY F,TB_CATEGORY C,TB_PROJECT_SECTION_DETAILS PSD " +
                    " WHERE" +
                    "  TCF.FAMILY_ID=F.FAMILY_ID AND TCF.CATEGORY_ID=C.CATEGORY_ID AND F.PUBLISH2PRINT=1 AND PSD.FAMILY_ID=F.FAMILY_ID AND TCF.ROOT_CATEGORY ='0' AND RECORD_ID=" + recordID + " AND C.Flag_recycle='A' AND TCF.Flag_recycle='A' AND F.Flag_recycle='A' AND TCF.CATALOG_ID=" + _xmlgen.CatalogId.ToString(CultureInfo.InvariantCulture);

                    _SQLString = strsql;
                    DataSet dsCategoryName = CreateDataSet();
                    var refCategoryID = new string[dsCategoryName.Tables[0].Rows.Count];

                    int ij = 0;

                    foreach (DataRow dr in dsCategoryName.Tables[0].Rows)
                    {
                        refCategoryID[ij] = dr["CATEGORY_ID"].ToString();
                        _xmlgen.CategoryId = dr["CATEGORY_ID"].ToString();  // grdSectionOrg.DisplayLayout.Rows[ij].Cells["CATEGORY_ID"].Value.ToString();
                        ij++;
                    }
                    if (dsCategoryName.Tables[0].Rows.Count > 0)
                    {
                        if (dataFileCount == 0)
                        {
                            _xmlgen.FileNO = fileNo; //grdFileOrg.ActiveRow.Cells["FILE_NO"].Text
                            _xmlgen.ProjectID = projectID; //grdFileOrg.ActiveRow.Cells["PROJECT_ID"].Text
                            _xmlgen.RecordID = recordID; //grdFileOrg.ActiveRow.Cells["RECORD_ID"].Text
                            _xmlgen.PageName = "INCAT";

                            var xmlfile = new StringBuilder(GenerateXML(attrselect, attrlists, idColumnFlag));

                            xmlfile = xmlfile.Replace("ITEM#", CustomizableItem).Replace("ITEM#", CustomizableItem).Replace("ITEM#", CustomizableItem);

                            xmlfile = xmlfile.Replace("/&lt;/br&gt;", "/&#x2028;").Replace("&amp;nbsp;", "&#160;");
                            if (!xmlDataPath.ToLower().Contains(".xml"))
                            {
                                xmlDataPath = xmlDataPath + ".xml";
                            }
                            //string xmlPathName = HttpContext.Current.Server.MapPath("~/Content/IndesignXML/") +
                            //                     xmlDataPath;
                            //string xmlDir = HttpContext.Current.Server.MapPath("~/Content/IndesignXML");
                            //var dirName = new DirectoryInfo(xmlDir);
                            CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.CustomerId == customerId).Select(x => x.a.Comments).FirstOrDefault();
                            string path = System.IO.Path.Combine(CustomerFolder, "Indesignxml");
                            var XMLProj = _dbcontext.TB_PROJECT.Where(x => x.PROJECT_ID == projectID).Select(x => x).FirstOrDefault();
                            string XMLProjectName = XMLProj.PROJECT_NAME;
                            //System.IO.Directory.CreateDirectory(path);
                            string xmlPathName = _xmlpath1 + @"\" + path + @"\" ;
                            if (!Directory.Exists(xmlPathName))
                            {
                                Directory.CreateDirectory(xmlPathName);
                            }
                            var dirName = new DirectoryInfo(xmlPathName);

                            //object context;
                            //if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                            //{
                            //    var httpContext = context as HttpContextBase;
                            //if (httpContext != null && httpContext.Session != null)
                            //{
                            //    httpContext.Session["Xmlpath"] = xmlPathName;
                            //}
                            //}

                            if (dirName.Exists)
                            {
                                _fileCreation = 1;
                                // DirectoryInfo di = Directory.CreateDirectory(CustomerFolder);
                                xmlPathName = xmlPathName + xmlDataPath;
                                var fs = new FileStream(xmlPathName, FileMode.Create, FileAccess.Write);
                                var xmlwrite = new StreamWriter(fs);
                                xmlwrite.Write(xmlfile);
                                xmlwrite.Close();
                            }
                            else
                            {
                                //  _exception.ShowCustomMessage("W013", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                _fileCreation = 0;
                            }
                        }

                        else if (dataFileCount == 1)
                        {
                            //for (int fileorgRows = 0; fileorgRows < grdFileOrg.Rows.Count; fileorgRows++)
                            //{
                            //    if (xmlDataPath.ToString() == grdFileOrg.DisplayLayout.Rows[fileorgRows].Cells["XML_FILE_NAME"].Text.Replace(".xml".ToLower(), "") + ".xml".ToString())
                            //    {
                            //        _xmlpath = grdFileOrg.DisplayLayout.Rows[fileorgRows].Cells["XML_FILE_NAME"].Text.Replace(".xml".ToLower(), "") + ".xml";
                            //        _xmlgen.FileNO = Convert.ToInt32(grdFileOrg.Rows[fileorgRows].Cells["FILE_NO"].Text);
                            //        _xmlgen.ProjectID = Convert.ToInt32(grdFileOrg.Rows[fileorgRows].Cells["PROJECT_ID"].Text);
                            //        _xmlgen.RecordID = Convert.ToInt32(grdFileOrg.Rows[fileorgRows].Cells["RECORD_ID"].Text);
                            //        _xmlgen.PageName = "INCAT";

                            //        var xmlfile = new StringBuilder(GenerateXML(attrselect,attrlists));
                            //        xmlfile = xmlfile.Replace("/&lt;/br&gt;", "/&#x2028;").Replace("&amp;nbsp;", "&#160;");
                            //        xmlPathName = HttpContext.Current.Server.MapPath("~/Content/IndesignXML") + _xmlpath;

                            //        var fs = new FileStream(xmlPathName, FileMode.Create, FileAccess.Write);
                            //        var xmlwrite = new StreamWriter(fs);
                            //        xmlwrite.Write(xmlfile);
                            //        xmlwrite.Close();
                            //    }
                            //}
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.ExpectationFailed, "One or few of the Families are no longer available in CatalogStudio");
                    }
                }
            }
            //else
            //    _exception.ShowCustomMessage("E063", MessageBoxButtons.OK, MessageBoxIcon.Error);

            return Request.CreateResponse(HttpStatusCode.OK, inDesignResult);
            // return null;
        }

        public string GenerateXML(bool attrSelect, List<AttributePublish> attrLists, bool idColumnFlag)
        {
            string ImageMagickPath = string.Empty;
            int user_id = 0;
            var user = _dbcontext.Customer_User.FirstOrDefault(a => a.User_Name.ToLower() == User.Identity.Name.ToString().ToLower());
            if (user != null)
            {
                user_id = user.CustomerId;
            }

            var imagemagickpath = _dbcontext.Customer_Settings.FirstOrDefault(a => a.CustomerId == user_id);
            //if (imagemagickpath != null)
            //{
            //    ImageMagickPath = imagemagickpath.ImageConversionUtilityPath;
            //    ImageMagickPath = ImageMagickPath.Replace("\\\\", "\\");
            //}
            //else
            //{
            //    ImageMagickPath = "C:\\Program Files\\ImageMagick-6.8.5-Q8";
            //}
            string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == user_id).Select(a => a.ConverterType).SingleOrDefault();
            if (imagemagickpath != null && Directory.Exists(imagemagickpath.ImageConversionUtilityPath.Trim()))
            {
                ImageMagickPath = convertionPath;
                ImageMagickPath = ImageMagickPath.Replace("\\\\", "\\");
            }
            else
            {
                ImageMagickPath = convertionPathREA;
                ImageMagickPath = ImageMagickPath.Replace("\\\\", "\\");
            }
            strBuildXMLOutput = new StringBuilder();

            string spath = HttpContext.Current.Server.MapPath("~/Content/IndesignXML");
            _xmlgen.UserImagePath = spath;

            ///v5
            DataSet oDsfamily = new DataSet();
            //_SQLString = " SELECT FAMILY_ID,CATEGORY_ID FROM TB_PROJECT_SECTION_DETAILS WHERE RECORD_ID IN (SELECT RECORD_ID FROM TB_PROJECT_SECTIONS WHERE  FILE_NO = " + _xmlgen.FileNO + " AND PROJECT_ID  = " + _xmlgen.ProjectID + ") ORDER BY SORT_ORDER";

            if (HttpContext.Current.Session["Filtervalue"] != null || HttpContext.Current.Session["Filtercategoryvalue"] != null)
            {
                //_SQLString = " SELECT FAMILY_ID,CATEGORY_ID FROM TB_PROJECT_SECTION_DETAILS WHERE RECORD_ID IN (SELECT RECORD_ID FROM TB_PROJECT_SECTIONS WHERE  FILE_NO = " + _xmlgen.FileNO + " AND PROJECT_ID  = " + _xmlgen.ProjectID + ") ORDER BY SORT_ORDER";
                _SQLString = "SELECT PSD.FAMILY_ID,FA.CATEGORY_ID FROM TB_PROJECT_SECTION_DETAILS PSD " +
                                            " JOIN TB_PROJECT_SECTIONS PS on PSD.RECORD_ID = ps.RECORD_ID " +
                                            " JOIN TB_FAMILY FA on FA.FAMILY_ID = PSD.FAMILY_ID AND FA.PUBLISH2PRINT=1" +
                                            " JOIN TB_CATEGORY CAT on CAT.CATEGORY_ID = FA.CATEGORY_ID  AND CAT.PUBLISH2PRINT=1" +
                                            " WHERE PS.RECORD_ID IN (SELECT RECORD_ID FROM TB_PROJECT_SECTIONS WHERE  FILE_NO = " + _xmlgen.FileNO + " AND PROJECT_ID  = " + _xmlgen.ProjectID + ") AND PSD.FILTER_STATUS=1 and FA.FLAG_RECYCLE = 'A'  ORDER BY SORT_ORDER";
            }

            else
            {
                //_SQLString = " SELECT FAMILY_ID,CATEGORY_ID FROM TB_PROJECT_SECTION_DETAILS WHERE RECORD_ID IN (SELECT RECORD_ID FROM TB_PROJECT_SECTIONS WHERE  FILE_NO = " + _xmlgen.FileNO + " AND PROJECT_ID  = " + _xmlgen.ProjectID + ") ORDER BY SORT_ORDER";
                _SQLString = "SELECT PSD.FAMILY_ID,FA.CATEGORY_ID FROM TB_PROJECT_SECTION_DETAILS PSD " +
                                            " JOIN TB_PROJECT_SECTIONS PS on PSD.RECORD_ID = ps.RECORD_ID " +
                                            " JOIN TB_FAMILY FA on FA.FAMILY_ID = PSD.FAMILY_ID AND FA.PUBLISH2PRINT=1" +
                                            " JOIN TB_CATEGORY CAT on CAT.CATEGORY_ID = FA.CATEGORY_ID  AND CAT.PUBLISH2PRINT=1" +
                                            " WHERE PS.RECORD_ID IN (SELECT RECORD_ID FROM TB_PROJECT_SECTIONS WHERE  FILE_NO = " + _xmlgen.FileNO + " AND PROJECT_ID  = " + _xmlgen.ProjectID + " )AND PSD.FILTER_STATUS=0 and FA.FLAG_RECYCLE = 'A'  ORDER BY SORT_ORDER";
            }
            oDsfamily = CreateDataSet();
            string familyids = string.Empty;
            string categoryids = string.Empty;
            foreach (DataRow Dr in oDsfamily.Tables[0].Rows)
            {
                familyids = familyids + "," + Dr[0];
                categoryids = categoryids + "," + Dr[1];
            }
            familyids = familyids.Substring(1);
            categoryids = categoryids.Substring(1);
            string[] categoryidsplit = categoryids.Split(new Char[] { ',' });
            _xmlgen.FamilyList = familyids;
            /////v5 
            string[] split = _xmlgen.FamilyList.Split(new Char[] { ',' });
            NumFam = split.Length;
            bool bfamilyCondition1 = false;
            bool bfamilyCondition2 = false;
            bool BcontinueFamily = false;

            //////////////////Category id,category name,parent category             
            //_SQLString = "SELECT CATEGORY_ID,PARENT_CATEGORY,CATEGORY_NAME from TB_CATEGORY WHERE Flag_recycle='A' AND CATEGORY_ID IN (select distinct category_id from tb_project_section_details where family_id IN ( " + familyids + ") and record_id = " + _xmlgen.RecordID + ")";

            // ------------------------------- AFTER CUT PASTE FAMILY FROM ONE INDESIGN FILE TO ANOTHER---------------------------------------//
            _SQLString = "SELECT DISTINCT FA.CATEGORY_ID,CA.PARENT_CATEGORY,CA.CATEGORY_NAME FROM TB_PROJECT_SECTION_DETAILS PSD " +
                        " JOIN TB_PROJECT_SECTIONS PS ON PSD.RECORD_ID = PS.RECORD_ID " +
                        " JOIN TB_FAMILY FA ON FA.FAMILY_ID = PSD.FAMILY_ID AND FA.PUBLISH2PRINT=1" +
                        " JOIN TB_CATEGORY CA ON FA.CATEGORY_ID = CA.CATEGORY_ID AND CA.PUBLISH2PRINT=1 " +
                        " WHERE CA.FLAG_RECYCLE = 'A' AND FA.FLAG_RECYCLE = 'A'  AND FA.FAMILY_ID IN (" + familyids + ")  AND PSD.RECORD_ID = " + _xmlgen.RecordID + " ";
            DataSet IDFields = CreateDataSet();
            if (IDFields.Tables[0].Rows.Count == 0)
            {
                for (int fid = 0; fid < split.Length; fid++)
                {
                    //_SQLString = "SELECT CATEGORY_ID,PARENT_CATEGORY,CATEGORY_NAME from TB_CATEGORY WHERE CATEGORY_ID IN (select distinct category_id from tb_project_section_details where family_id IN ( " + familyids + ") and record_id = " + _xmlgen.RecordID + ")";
                    // ------------------------------- AFTER CUT PASTE FAMILY FROM ONE INDESIGN FILE TO ANOTHER---------------------------------------//
                    _SQLString = "SELECT DISTINCT FA.CATEGORY_ID,CA.PARENT_CATEGORY,CA.CATEGORY_NAME FROM TB_PROJECT_SECTION_DETAILS PSD " +
                                " JOIN TB_PROJECT_SECTIONS PS ON PSD.RECORD_ID = PS.RECORD_ID " +
                                " JOIN TB_FAMILY FA ON FA.FAMILY_ID = PSD.FAMILY_ID AND FA.PUBLISH2PRINT=1" +
                                " JOIN TB_CATEGORY CA ON FA.CATEGORY_ID = CA.CATEGORY_ID AND CA.PUBLISH2PRINT=1" +
                                " WHERE CA.FLAG_RECYCLE = 'A' AND FA.FLAG_RECYCLE = 'A'  AND FA.FAMILY_ID IN (" + familyids + ")  AND PSD.RECORD_ID = " + _xmlgen.RecordID + " ";
                    IDFields = CreateDataSet();
                    if (IDFields.Tables[0].Rows.Count > 0)
                    {
                        fid = split.Length;
                    }
                }
            }
            if (IDFields.Tables[0].Rows.Count > 0)
            {
                string parentcat = IDFields.Tables[0].Rows[0].ItemArray[1].ToString();
                while (parentcat != "0")
                { //Category id, parent category,category name
                    _SQLString = "SELECT CATEGORY_ID,PARENT_CATEGORY,CATEGORY_NAME from TB_CATEGORY WHERE FLAG_RECYCLE='A' AND PUBLISH2PRINT=1 AND CATEGORY_ID = '" + parentcat + "'";
                    IDFields = CreateDataSet();
                    parentcat = IDFields.Tables[0].Rows[0].ItemArray[1].ToString();

                }
                parentcat = IDFields.Tables[0].Rows[0].ItemArray[0].ToString();
                ////////////////////

                //oXMLDS = new TradingBell.CatalogStudio.CoreDS.CatalogXML();
                //OCatalogCategory = new TradingBell.CatalogStudio.CoreDS.CatalogXMLTableAdapters.CatalogCategoryTableAdapter();
                //OCatalogCategory.Fill(oXMLDS.CatalogCategory, CatalogId, parentcat);
                //TESTING2
                _SQLString = "SELECT     TB_CATALOG.CATALOG_NAME, TB_CATALOG.VERSION, TB_CATALOG.DESCRIPTION, TB_CATEGORY.CATEGORY_NAME, TB_CATEGORY.SHORT_DESC, TB_CATEGORY.CUSTOM_NUM_FIELD1, TB_CATEGORY.CUSTOM_NUM_FIELD2, TB_CATEGORY.CUSTOM_NUM_FIELD3, TB_CATEGORY.CUSTOM_TEXT_FIELD1, TB_CATEGORY.CUSTOM_TEXT_FIELD3, " +
                             " TB_CATEGORY.CUSTOM_TEXT_FIELD2,TB_CATEGORY.IMAGE_FILE,TB_CATEGORY.IMAGE_TYPE,TB_CATEGORY.IMAGE_NAME,TB_CATEGORY.IMAGE_FILE2,TB_CATEGORY.IMAGE_TYPE2,TB_CATEGORY.IMAGE_NAME2 " +
                             " FROM         TB_CATALOG CROSS JOIN  TB_CATEGORY WHERE  TB_CATEGORY.Flag_recycle='A' AND TB_CATEGORY.PUBLISH2PRINT=1 AND  (TB_CATALOG.CATALOG_ID IN  (SELECT     CATALOG_ID FROM   TB_CATALOG_SECTIONS  WHERE  Flag_recycle='A' AND (CATALOG_ID =  " + _catalogId + " ))) AND (TB_CATEGORY.CATEGORY_ID IN (SELECT     CATEGORY_ID FROM          TB_CATALOG_SECTIONS AS TB_CATALOG_SECTIONS_1 WHERE flag_recycle ='A' and (CATEGORY_ID = '" + parentcat + "')))";
                oXMLDS_CatalogCategory = CreateDataSet();

                //xml construction 
                //-----------------*****Bind RootLevel Category Specs******-----------------

                _SQLString = "select DISTINCT TA.ATTRIBUTE_ID, REPLACE(TA.ATTRIBUTE_NAME,' ','_') ATTRIBUTE_NAME, TCS.STRING_VALUE, TA.ATTRIBUTE_TYPE,REPLACE(TA.STYLE_NAME, ' ','_') as STYLE_NAME,TCS.NUMERIC_VALUE,TCS.SORT_ORDER " +
                        " From TB_CATEGORY_SPECS TCS LEFT JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TCS.ATTRIBUTE_ID AND ATTRIBUTE_TYPE IN(21,25) AND PUBLISH2PRINT = 1 LEFT JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID =" + _catalogId + " " +
                         "WHERE TCS.CATEGORY_ID ='" + parentcat + "' AND TA.ATTRIBUTE_ID IS NOT NULL";
                oXMLDS_CategorySpecs = CreateDataSet();

                _SQLString = "select DISTINCT  REPLACE(TA.ATTRIBUTE_NAME,' ','_') ATTRIBUTE_NAME,TA.ATTRIBUTE_ID, TCS.STRING_VALUE, TA.ATTRIBUTE_TYPE,TCS.OBJECT_NAME, TCS.OBJECT_TYPE" +
                       " From TB_CATEGORY_SPECS TCS LEFT JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TCS.ATTRIBUTE_ID AND ATTRIBUTE_TYPE IN(23) AND PUBLISH2PRINT = 1 LEFT JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID =" + _catalogId + " " +
                        "WHERE TCS.CATEGORY_ID ='" + parentcat + "' AND TA.ATTRIBUTE_ID IS NOT NULL";
                oXMLDS_CategoryImage = CreateDataSet();

                //---------------***********Bind RootLevel Category Specs******---------------


                strBuildXMLOutput.Append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n");
                // if (idColumnFlag)
                strBuildXMLOutput.Append("<tradingbell_root catalog_id=\"" + _catalogId + "\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\">\n");

                //strBuildXMLOutput.Append("<tradingbell_root xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\">\n");
                // string Val = "true";//**************Check*************************** _settingMembers.GetValue(SystemSettingsCollection.SettingsList.SelectedXMLAttributes.ToString());
                Flg_Chk = false;
                if (attrSelect)
                {
                    ////////InCatAttributeManager Attribute_Manager = new InCatAttributeManager(_xmlgen.FamilyList);
                    ////////if (Attribute_Manager._Is_Attr_Available == true)
                    ////////{
                    ////////    Attribute_Manager.ShowDialog();
                    ////////}
                    Attr_List = string.Empty;
                    foreach (var attributeId in attrLists)
                    {
                        Attr_List = Attr_List + attributeId.ATTRIBUTE_ID + ",";

                    }
                    Attr_List = Attr_List.TrimEnd(',');
                    Flg_Chk = true;
                }

                var CategorySortByName = _dbcontext.TB_CATEGORY.Select(x => x).Where(x => x.CATEGORY_ID == _xmlgen.CategoryId).FirstOrDefault();
                _xmlgen.CATEGORY_SHORT = CategorySortByName.CATEGORY_SHORT;

                //Start Catalog and Category 
                for (rowCnt = 0; rowCnt < oXMLDS_CatalogCategory.Tables[0].Rows.Count; rowCnt++)
                {
                    strBuildXMLOutput.Append("<catalog_name  COTYPE=\"TEXT\" MSID=\"ID" + _catalogId + "CN\" TBGUID =\"CN" + RandomClass.Next() + "\" aid:pstyle=\"catalog_name\"><![CDATA[" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[0] + "]]></catalog_name>\n");
                    strBuildXMLOutput.Append("<version  COTYPE=\"TEXT\" MSID=\"ID" + _catalogId + "CV\" TBGUID =\"CV" + RandomClass.Next() + "\" aid:pstyle=\"version\"><![CDATA[" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[1].ToString().Trim() + "]]></version>\n");
                    strBuildXMLOutput.Append("<description  COTYPE=\"TEXT\" MSID=\"ID" + _catalogId + "CD\" TBGUID =\"CD" + RandomClass.Next() + "\" aid:pstyle=\"description\"><![CDATA[" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[2].ToString().Trim() + "]]></description>\n");
                    //  if (idColumnFlag)
                    strBuildXMLOutput.Append("<category  COTYPE=\"\" category_id=\"" + _xmlgen.CATEGORY_SHORT + "\" families=\"" + NumFam + "\" MSID=\"ID" + _xmlgen.CATEGORY_SHORT + "CI\" TBGUID =\"C" + RandomClass.Next() + "\" aid:pstyle=\"category\">\n");
                    //else
                    //    strBuildXMLOutput.Append("<category  COTYPE=\"\" families=\"" + NumFam + "\" TBGUID=\"C" + RandomClass.Next() + "\" aid:pstyle=\"category\">\n");
                    strBuildXMLOutput.Append("<category_section_id  COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CATEGORY_SHORT + "CS1\" TBGUID=\"CS1" + RandomClass.Next() + "\" aid:pstyle=\"category_section_id\"><![CDATA[" + parentcat + "]]></category_section_id>\n");
                    strBuildXMLOutput.Append("<category_section_id  COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CATEGORY_SHORT + "CS2\" TBGUID =\"CS1" + RandomClass.Next() + "\" aid:pstyle=\"category_section_id\"><![CDATA[" + parentcat + "]]></category_section_id>\n");
                    strBuildXMLOutput.Append("<category_section  COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CATEGORY_SHORT + "CS3\" TBGUID =\"CS1" + RandomClass.Next() + "\" aid:pstyle=\"category_section\"><![CDATA[" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[3].ToString().Trim() + "]]></category_section>\n");
                    strBuildXMLOutput.Append("<category_section  COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CATEGORY_SHORT + "CS4\" TBGUID =\"CS2" + RandomClass.Next() + "\" aid:pstyle=\"category_section\"><![CDATA[" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[3].ToString().Trim() + "]]></category_section>\n");
                    //strBuildXMLOutput.Append("<description  COTYPE=\"TEXT\" TBGUID=\"CTD" + RandomClass.Next() + "\" aid:pstyle=\"description\"><![CDATA[" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[4].ToString().Trim() + "]]></description>\n");
                    ////new Fields
                    //string NumVal1 = (Convert.ToString(oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[5]) == "" ? " " : string.Format("{0:G29}", decimal.Parse(oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[5].ToString())));
                    //string NumVal2 = (Convert.ToString(oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[6]) == "" ? " " : string.Format("{0:G29}", decimal.Parse(oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[6].ToString())));
                    //string NumVal3 = (Convert.ToString(oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[7]) == "" ? " " : string.Format("{0:G29}", decimal.Parse(oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[7].ToString())));
                    //strBuildXMLOutput.Append("<customnumfield1  COTYPE=\"TEXT\" TBGUID=\"CTD" + RandomClass.Next() + "\" aid:pstyle=\"customnumfield1\"><![CDATA[" + NumVal1 + "]]></customnumfield1>\n");
                    //strBuildXMLOutput.Append("<customnumfield2   COTYPE=\"TEXT\" TBGUID=\"CTD" + RandomClass.Next() + "\" aid:pstyle=\"customnumfield2\"><![CDATA[" + NumVal2 + "]]></customnumfield2>\n");
                    //strBuildXMLOutput.Append("<customnumfield3  COTYPE=\"TEXT\" TBGUID=\"CTD" + RandomClass.Next() + "\" aid:pstyle=\"customnumfield3\"><![CDATA[" + NumVal3 + "]]></customnumfield3>\n");
                    ////strBuildXMLOutput.Append("<customnumfield1  COTYPE=\"TEXT\" TBGUID=\"CTD" + RandomClass.Next() + "\" aid:pstyle=\"customnumfield1\"><![CDATA[" + oXMLDS.CatalogCategory.Rows[rowCnt].ItemArray[5].ToString().Trim() + "]]></customnumfield1>\n");
                    ////strBuildXMLOutput.Append("<customnumfield2   COTYPE=\"TEXT\" TBGUID=\"CTD" + RandomClass.Next() + "\" aid:pstyle=\"customnumfield2\"><![CDATA[" + oXMLDS.CatalogCategory.Rows[rowCnt].ItemArray[6].ToString().Trim() + "]]></customnumfield2>\n");
                    ////strBuildXMLOutput.Append("<customnumfield3  COTYPE=\"TEXT\" TBGUID=\"CTD" + RandomClass.Next() + "\" aid:pstyle=\"customnumfield3\"><![CDATA[" + oXMLDS.CatalogCategory.Rows[rowCnt].ItemArray[7].ToString().Trim() + "]]></customnumfield3>\n");
                    //strBuildXMLOutput.Append("<customtextfield1  COTYPE=\"TEXT\" TBGUID=\"CTD" + RandomClass.Next() + "\" aid:pstyle=\"customtextfield1\"><![CDATA[" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[8].ToString().Trim() + "]]></customtextfield1>\n");
                    //strBuildXMLOutput.Append("<customtextfield2  COTYPE=\"TEXT\" TBGUID=\"CTD" + RandomClass.Next() + "\" aid:pstyle=\"customtextfield2\"><![CDATA[" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[9].ToString().Trim() + "]]></customtextfield2>\n");
                    //strBuildXMLOutput.Append("<customtextfield3  COTYPE=\"TEXT\" TBGUID=\"CTD" + RandomClass.Next() + "\" aid:pstyle=\"customtextfield3\"><![CDATA[" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[10].ToString().Trim() + "]]></customtextfield3>\n");

                    //strBuildXMLOutput.Append("<image_type COTYPE=\"\" TBGUID=\"IT" + RandomClass.Next() + "\">\n");
                    //strBuildXMLOutput.Append("<category_image1" + " " + " IMAGE_FILE=\"" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[11].ToString().Trim().Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\" COTYPE=\"IMAGE\" TBGUID=\"SL" + RandomClass.Next() + "\"></category_image1>\n");
                    //strBuildXMLOutput.Append("<category_image1_name  COTYPE=\"TEXT\" TBGUID=\"IN" + RandomClass.Next() + "\" aid:pstyle=\"category_image1_name\"><![CDATA[" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[13].ToString().Trim() + "]]></category_image1_name>\n");
                    //strBuildXMLOutput.Append("<category_image2" + " " + " IMAGE_FILE=\"" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[14].ToString().Trim().Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\" COTYPE=\"IMAGE\" TBGUID=\"SL" + RandomClass.Next() + "\"></category_image2>\n");
                    //strBuildXMLOutput.Append("<category_image2_name  COTYPE=\"TEXT\" TBGUID=\"IN" + RandomClass.Next() + "\" aid:pstyle=\"category_image2_name\"><![CDATA[" + oXMLDS_CatalogCategory.Tables[0].Rows[rowCnt].ItemArray[16].ToString().Trim() + "]]></category_image2_name>\n");
                    //strBuildXMLOutput.Append("</image_type>\n");
                    //end new fields 
                }

                foreach (DataRow dtr in oXMLDS_CategorySpecs.Tables[0].Rows)
                {
                    if (dtr["STRING_VALUE"].ToString() != null && dtr["STRING_VALUE"].ToString() != "" && dtr["ATTRIBUTE_ID"].ToString() != null && dtr["ATTRIBUTE_ID"].ToString() != "")
                    {
                        _SQLString = "select ATTRIBUTE_DATATYPE, ATTRIBUTE_DATAFORMAT from tb_attribute where ATTRIBUTE_ID=" + dtr["ATTRIBUTE_ID"].ToString() + " and FLAG_RECYCLE='A'";
                        DataSet AttributeDataFormatDS = CreateDataSet();
                        if (AttributeDataFormatDS.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in AttributeDataFormatDS.Tables[0].Rows)
                            {
                                if (dr["ATTRIBUTE_DATATYPE"].ToString() == "Date and Time")
                                {
                                    string DateValue = dtr["STRING_VALUE"].ToString();
                                    if (dr["ATTRIBUTE_DATAFORMAT"].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                    {
                                        dtr["STRING_VALUE"] = DateValue.Substring(3, 2) + "/" + DateValue.Substring(0, 2) + "/" + DateValue.Substring(6, 4);

                                    }

                                }
                            }
                        }
                    }
                }

                strBuildXMLOutput.Append("<cat_specs_type COTYPE=\"\" MSID=\"ID" + _xmlgen.CategoryId + "CST\" TBGUID =\"ST" + RandomClass.Next() + "\">\n");
                for (rowCatAttrCnt = 0; rowCatAttrCnt < oXMLDS_CategorySpecs.Tables[0].Rows.Count; rowCatAttrCnt++)
                #region From PreviewHTMLDATA.cs
                {
                    AttrName = "";
                    string Style = string.Empty;
                    Style = oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[4].ToString();
                    if (Style == string.Empty || Style == "")
                    {
                        AttrName = oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[1].ToString();
                    }
                    if (Convert.ToInt32(oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[3].ToString()) != 23)
                    {
                        GetCurrencySymbol(oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString());
                        string sValue = string.Empty;
                        string dtype = oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[3].ToString();
                        int numberdecimel = 0;
                        if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                        {
                            numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                        }
                        bool styleFormat = false; // Declared Static Temporarily                           
                        _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                        DataSet AttrDatatype = CreateDataSet();

                        #region "Decimal Place Triming"
                        if (AttrDatatype.Tables[0].Rows.Count > 0)
                        {
                            if (oXMLDS_CategorySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString() != null && oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[5].ToString() != "")
                            {
                                foreach (DataRow dr in AttrDatatype.Tables[0].Rows)
                                {
                                    if (dr.ItemArray[0].ToString().StartsWith("Num"))
                                    {
                                        string tempStr = string.Empty;
                                        tempStr = oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[5].ToString();
                                        string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                        datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                        int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                        if (noofdecimalplace != 6)
                                        {
                                            tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                        }
                                        if (noofdecimalplace == 0)
                                        {
                                            tempStr = tempStr.TrimEnd('.');
                                        }
                                        oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt]["NUMERIC_VALUE"] = tempStr;
                                    }
                                }
                            }
                        }
                        #endregion

                        if (AttrDatatype.Tables[0].Rows.Count > 0)
                        {
                            if (AttrDatatype.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num"))
                            {
                                sValue = oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[5].ToString();
                            }
                            else
                            {
                                sValue = oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[2].ToString();
                            }
                            if (sValue == "" && (_emptyCondition == "Null" || _emptyCondition == "Empty"))
                            {
                                if (sValue == "" && _emptyCondition != "Empty")
                                {
                                    sValue = "Null";
                                }
                            }
                        }
                        else
                        {
                            sValue = oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[2].ToString();
                        }
                        //}
                        if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                        {
                            if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                            if (_replaceText != "")
                            {
                                if (sValue == "0")
                                {
                                    if (_emptyCondition == "0.000000")
                                    {
                                        sValue = _emptyCondition;
                                    }
                                }
                                if (sValue == "Null")
                                {
                                    _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                }
                                else
                                {
                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                }
                                if (_replaceText != "")
                                    //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                    //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                    //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                    if (Convert.ToString(_replaceText) != string.Empty)
                                    {
                                        if (Convert.ToString(_replaceText) != sValue)
                                        {
                                            if (_fornumeric == "1")
                                            {
                                                if (Isnumber(_replaceText.Replace(",", "")))
                                                {
                                                    if (_headeroptions == "All")
                                                    {
                                                        if (Style == string.Empty || Style == "")
                                                        {
                                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID =\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                        }
                                                        else
                                                        {
                                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID =\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (rowCatAttrCnt == 0)
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID =\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                }
                                            }
                                            else
                                            {
                                                if (_headeroptions == "All")
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                }
                                                else
                                                {
                                                    if (rowCatAttrCnt == 0)
                                                    {
                                                        if (Style == string.Empty || Style == "")
                                                            //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                        else
                                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                    }
                                                    else
                                                    {
                                                        if (Style == string.Empty || Style == "")
                                                            //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                        else
                                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\"  TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (rowCatAttrCnt == 0)
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\"  TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                }
                                                else
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                }
                                            }
                                        }
                                    }
                            }
                            else
                            {
                                _replaceText = sValue;
                            }
                        }
                        else if (oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[2].ToString().Trim() != "")
                        {
                            //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                            //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                            if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                            {
                                if (_headeroptions == "All")
                                {
                                    if (Style == string.Empty || Style == "")
                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                    else
                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\"  TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                }
                                else
                                {
                                    if (rowCatAttrCnt == 0)
                                    {
                                        if (Style == string.Empty || Style == "")
                                            //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\"  TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                        else
                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\"  TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                    }
                                    else
                                    {
                                        if (Style == string.Empty || Style == "")
                                            //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\"  TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                        else
                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                    }

                                }
                            }
                            else
                            {
                                if (Isnumber(sValue))
                                {
                                    sValue = DecPrecisionFill(sValue, numberdecimel);
                                    sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString()), sValue.Trim()) : sValue;
                                }
                                if (_headeroptions == "All")
                                {
                                    if (Style == string.Empty || Style == "")
                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                    else
                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                }
                                else
                                {
                                    if (rowCatAttrCnt == 0)
                                    {
                                        if (Style == string.Empty || Style == "")
                                            //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                        else
                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                    }
                                    else
                                    {
                                        if (Style == string.Empty || Style == "")
                                            //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                        else
                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                    }
                                }
                            }
                        }
                        else if (oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[5].ToString().Trim() != "")
                        {
                            _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                            DataSet dsSize = CreateDataSet();
                            sValue = oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[5].ToString();
                            if (dsSize != null)
                            {
                                if (dsSize.Tables.Count > 0)
                                {
                                    if (dsSize.Tables[0].Rows.Count > 0)
                                    {
                                        string Valsize = dsSize.Tables[0].Rows[0]["ATTRIBUTE_DATATYPE"].ToString();
                                        sValue = Valchk(sValue, Valsize); // Created Valchk function newly for this 
                                    }
                                }
                            }
                            //sValue = htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[2].ToString();
                            if (sValue != "")
                            {
                                //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                {
                                    if (_headeroptions == "All")
                                    {
                                        if (Style == string.Empty || Style == "")
                                            //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                        else
                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                    }
                                    else
                                    {
                                        if (rowCatAttrCnt == 0)
                                        {
                                            if (Style == string.Empty || Style == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                        }
                                        else
                                        {
                                            if (Style == string.Empty || Style == "")
                                                //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");

                                        }
                                    }
                                }
                                else
                                {
                                    if (_headeroptions == "All")
                                    {
                                        if (Style == string.Empty || Style == "")
                                            //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                        else
                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                    }
                                    else
                                    {
                                        if (rowCatAttrCnt == 0)
                                        {
                                            if (Style == string.Empty || Style == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                        }
                                        else
                                        {
                                            if (Style == string.Empty || Style == "")
                                                //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CST" + oXMLDS_CategorySpecs.Tables[0].Rows[rowCatAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                        }
                                    }
                                }
                            }
                        }



                    }
                }
                #endregion

                strBuildXMLOutput.Append("</cat_specs_type>\n");
                //Start Family Image 

                DateTime CurrTime = DateTime.Now;
                strBuildXMLOutput.Append("<image_type COTYPE=\"\" MSID=\"IDCIT001\" TBGUID=\"IT" + RandomClass.Next() + "\">\n");
                FPath = "href=\"file://";
                for (rowCatImgCnt = 0; rowCatImgCnt < oXMLDS_CategoryImage.Tables[0].Rows.Count; rowCatImgCnt++)
                {
                    ImgFile = "";
                    AttrName = "";
                    ImgFile = oXMLDS_CategoryImage.Tables[0].Rows[rowCatImgCnt].ItemArray[2].ToString();
                    _xmlgen.UserImagePath = _xmlgen.UserImagePath.Replace("&", "&amp;");
                    ImgFile = ImgFile.Replace("\\", "/");
                    ImageName = _xmlgen.UserImagePath + ImgFile;
                    ImageName = ImageName.Replace("/", "\\");
                    ImgFile = FPath + ImageName + "\"";

                    AttrName = oXMLDS_CategoryImage.Tables[0].Rows[rowCatImgCnt].ItemArray[0].ToString().Trim();
                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + " attribute_id=\"" + oXMLDS_CategoryImage.Tables[0].Rows[rowCatImgCnt].ItemArray[1].ToString().Trim() + "\"" + "  IMAGE_FILE=\"" + oXMLDS_CategoryImage.Tables[0].Rows[rowFamImgCnt].ItemArray[2].ToString().Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\" COTYPE=\"IMAGE\" MSID=\"ID" + _xmlgen.CategoryId + "CIT" + oXMLDS_CategoryImage.Tables[0].Rows[rowCatImgCnt].ItemArray[1].ToString().Trim() + "\" TBGUID=\"IV" + RandomClass.Next() + "\"></" + AttrName.ToLower() + ">\n");
                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "_name attribute_id=\"" + oXMLDS_CategoryImage.Tables[0].Rows[rowCatImgCnt].ItemArray[1].ToString().Trim() + "\"" + "  COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CIF" + oXMLDS_CategoryImage.Tables[0].Rows[rowCatImgCnt].ItemArray[1].ToString().Trim() + "\" TBGUID=\"IN" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "_name\"><![CDATA[" + oXMLDS_CategoryImage.Tables[0].Rows[rowCatImgCnt].ItemArray[4].ToString().Trim() + "]]></" + AttrName.ToLower() + "_name>\n");
                }
                strBuildXMLOutput.Append("</image_type>\n");




                // End Catalog 

                for (FamCnt = 0; FamCnt < split.Length; FamCnt++)
                {
                    bfamilyCondition1 = false;
                    bfamilyCondition2 = false;
                    BcontinueFamily = false;
                    FamilyId = split[FamCnt];
                    string categoryId = categoryidsplit[FamCnt];
                    //family filter 
                    _SQLString = " SELECT FAMILY_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + _catalogId + " ";
                    oDsFamilyFilter = CreateDataSet();
                    string sFamiilyFilter = string.Empty;
                    if (oDsFamilyFilter.Tables[0].Rows.Count > 0 && oDsFamilyFilter.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                    {
                        sFamiilyFilter = oDsFamilyFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                        XmlDocument xmlDOc = new XmlDocument();
                        xmlDOc.LoadXml(sFamiilyFilter);
                        XmlNode rNode = xmlDOc.DocumentElement;
                        StringBuilder FamilySQl = new StringBuilder();

                        fFilter1 = false;
                        fFilter2 = false;
                        fFilter3 = false;
                        sOperand1 = string.Empty;
                        sOperand2 = string.Empty;
                        sOperand3 = string.Empty;
                        if (rNode.ChildNodes.Count > 0)
                        {
                            bool fFiltercheck = false;
                            StringBuilder SQLstring = new StringBuilder();
                            for (int i = 0; i < rNode.ChildNodes.Count; i++)
                            {

                                XmlNode TableDataSetNode = rNode.ChildNodes[i];

                                if (TableDataSetNode.HasChildNodes)
                                {
                                    if (TableDataSetNode.ChildNodes[2].InnerText == " ")
                                    {
                                        TableDataSetNode.ChildNodes[2].InnerText = "=";
                                    }
                                    if (TableDataSetNode.ChildNodes[0].InnerText == " ")
                                    {
                                        TableDataSetNode.ChildNodes[0].InnerText = "0";
                                    }
                                    string stringval = TableDataSetNode.ChildNodes[3].InnerText.Replace("'", "''");
                                    SQLstring.Append("SELECT FAMILY_ID FROM TB_FAMILY_SPECS WHERE STRING_VALUE " + TableDataSetNode.ChildNodes[2].InnerText + " N'" + stringval + "'  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + " AND FLAG_RECYCLE='A' AND FAMILY_ID =" + FamilyId + "");
                                }
                                if (TableDataSetNode.ChildNodes[4].InnerText == "NONE")
                                {
                                    //SQLstring.Append(SQLstring);
                                    break;
                                }
                                // SQLstring.Append(SQLstring);
                                if (TableDataSetNode.ChildNodes[4].InnerText == "AND")
                                {

                                    SQLstring.Append(" INTERSECT \n");
                                }
                                if (TableDataSetNode.ChildNodes[4].InnerText == "OR")
                                {
                                    SQLstring.Append(" UNION \n");
                                }
                            }

                            fFiltercheck = Checkfamilyfilter(SQLstring.ToString());
                            if (fFiltercheck == true)
                            {
                                BcontinueFamily = true;
                            }


                        }
                        else
                        {
                            BcontinueFamily = true;
                        }
                    }
                    else
                    {
                        BcontinueFamily = true;
                    }
                    if (BcontinueFamily == true)
                    {
                        //Getting Sub Family
                        //NEED

                        _SQLString = "SELECT DISTINCT TB_FAMILY.FAMILY_ID as subfamily_id, TB_FAMILY.FAMILY_NAME, TB_FAMILY.FOOT_NOTES, TB_FAMILY.PARENT_FAMILY_ID, TB_FAMILY.ROOT_FAMILY,"
                                    + " TB_FAMILY.STATUS, TB_FAMILY_TABLE_STRUCTURE.FAMILY_TABLE_STRUCTURE, TB_FAMILY.CATEGORY_ID,  TB_SUBFAMILY.SORT_ORDER "
                                    + " FROM TB_FAMILY INNER JOIN TB_CATALOG_FAMILY ON TB_FAMILY.FAMILY_ID = TB_CATALOG_FAMILY.FAMILY_ID AND TB_FAMILY.PUBLISH2PRINT=1  and TB_FAMILY.flag_recycle='A' AND TB_CATALOG_FAMILY.flag_recycle='A' "
                                    + " INNER JOIN TB_CATALOG_SECTIONS ON TB_CATALOG_SECTIONS.flag_recycle='A' and TB_CATALOG_FAMILY.CATEGORY_ID = TB_CATALOG_SECTIONS.CATEGORY_ID AND TB_CATALOG_FAMILY.flag_recycle='A' AND TB_CATALOG_FAMILY.CATALOG_ID = TB_CATALOG_SECTIONS.CATALOG_ID "
                                    + " INNER JOIN TB_SUBFAMILY ON TB_FAMILY.FLAG_RECYCLE='A' AND TB_FAMILY.FAMILY_ID = TB_SUBFAMILY.SUBFAMILY_ID AND TB_CATALOG_FAMILY.FLAG_RECYCLE='A' AND TB_CATALOG_FAMILY.FAMILY_ID = TB_SUBFAMILY.SUBFAMILY_ID AND "
                                    + " TB_CATALOG_FAMILY.CATALOG_ID = TB_CATALOG_SECTIONS.CATALOG_ID,TB_FAMILY_TABLE_STRUCTURE WHERE TB_FAMILY_TABLE_STRUCTURE.CATALOG_ID=TB_CATALOG_SECTIONS.CATALOG_ID AND TB_CATALOG_SECTIONS.FLAG_RECYCLE='A' AND "
                                    + " TB_FAMILY_TABLE_STRUCTURE.FAMILY_ID=TB_FAMILY.FAMILY_ID AND  TB_FAMILY.FLAG_RECYCLE='A' AND TB_FAMILY_TABLE_STRUCTURE.IS_DEFAULT=1 AND  (TB_CATALOG_SECTIONS.CATALOG_ID =" + _catalogId + ") AND TB_CATALOG_SECTIONS.FLAG_RECYCLE='A' AND (TB_CATALOG_SECTIONS.CATALOG_ID = TB_CATALOG_SECTIONS.CATALOG_ID) AND "
                                    + " (TB_FAMILY.PARENT_FAMILY_ID = " + FamilyId + ") ORDER BY TB_SUBFAMILY.SORT_ORDER";

                        oDSSubFam = CreateDataSet();

                        // _SQLString = "SELECT CATEGORY_ID,PARENT_CATEGORY,CATEGORY_NAME from TB_CATEGORY WHERE FLAG_RECYCLE='A' AND CATEGORY_ID IN (select distinct category_id from tb_project_section_details where family_id IN (" + FamilyId + ") and RECORD_ID = " + _xmlgen.RecordID + " )";

                        // ------------------------------- AFTER CUT PASTE FAMILY FROM ONE INDESIGN FILE TO ANOTHER---------------------------------------//
                        _SQLString = "SELECT DISTINCT FA.CATEGORY_ID,CA.PARENT_CATEGORY,CA.CATEGORY_NAME FROM TB_PROJECT_SECTION_DETAILS PSD " +
                                    " JOIN TB_PROJECT_SECTIONS PS ON PSD.RECORD_ID = PS.RECORD_ID " +
                                    " JOIN TB_FAMILY FA ON FA.FAMILY_ID = PSD.FAMILY_ID AND FA.PUBLISH2PRINT=1 " +
                                    " JOIN TB_CATEGORY CA ON FA.CATEGORY_ID = CA.CATEGORY_ID AND CA.PUBLISH2PRINT=1" +
                                    " WHERE CA.FLAG_RECYCLE = 'A' AND FA.FLAG_RECYCLE = 'A'  AND FA.FAMILY_ID IN (" + FamilyId + ")  AND PSD.RECORD_ID = " + _xmlgen.RecordID + " ";




                        DataSet IDFieldss = CreateDataSet();
                        if (IDFieldss.Tables[0].Rows.Count > 0)
                        {
                            //if (idColumnFlag)
                            strBuildXMLOutput.Append("<product_family COTYPE=\"\" family_id=\"" + FamilyId + "\" sub_families=\"" + oDSSubFam.Tables[0].Rows.Count + "\" MSID=\"ID" + FamilyId + "PF\" TBGUID =\"PF" + RandomClass.Next() + "\">\n");
                            //else
                            //    strBuildXMLOutput.Append("<product_family COTYPE=\"\" sub_families=\"" + oDSSubFam.Tables[0].Rows.Count + "\" TBGUID=\"PF" + RandomClass.Next() + "\">\n");
                            //Call Function For Family Generation
                            if (Flg_Chk == false)
                            {

                                MainFamily = GetFamilyDetails(FamilyId, categoryId, "Main", idColumnFlag);

                                //}
                            }
                            else
                            {
                                if (Attr_List != "")
                                {
                                    MainFamily = GetFamilyDetails(FamilyId, categoryId, "Main", Attr_List, idColumnFlag);
                                }
                                else
                                {
                                    MainFamily = GetFamilyDetails(FamilyId, categoryId, "Main", idColumnFlag);

                                }
                            }
                            //LoadMultipleTables();
                            int familyid = Convert.ToInt32(FamilyId);
                            var multiplegroups = _dbcontext.TB_ATTRIBUTE_GROUP.Join(_dbcontext.TB_ATTRIBUTE_GROUP_SECTIONS, a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { a, b })
                                .Where(x => x.b.CATALOG_ID == _catalogId && x.b.FAMILY_ID == familyid).Select(z => new { z.a.GROUP_ID, z.a.GROUP_NAME }).Distinct();
                            if (multiplegroups.Any())
                            {
                                strBuildXMLOutput.Append("<ATTRIBUTE_GROUP    xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\"  COTYPE=\"\" TBGUID=\"M" + RandomClass.Next() + "\" aid:pstyle=\"attribute_group\">");
                                foreach (var multiplegroup in multiplegroups)
                                {
                                    var objSuperTable = new SuperTable();

                                    string XmlForSuperTable = objSuperTable.GenerateMultipleTablexml(multiplegroup.GROUP_ID, familyid, _catalogId, ImageMagickPath, Attr_List, User.Identity.Name, 0, "");
                                    strBuildXMLOutput.Append(XmlForSuperTable);
                                }
                                strBuildXMLOutput.Append("</ATTRIBUTE_GROUP>");
                            }

                            #region Attribute Pack

                            var attributePacks = (from apTableStructure in _dbcontext.TB_ATTRIBUTE_PACK_TABLE_STRUCTURE
                                                  join fTableStructure in _dbcontext.TB_FAMILY_TABLE_STRUCTURE on apTableStructure.FAMILY_STRUCTURE_ID equals fTableStructure.ID
                                                  where fTableStructure.FAMILY_ID == familyid && fTableStructure.IS_DEFAULT == true
                                                  select new
                                                  {
                                                      apTableStructure.PACK_ID,
                                                      apTableStructure.TABLE_STRUCTURE,
                                                      apTableStructure.STRUCTURE_NAME,
                                                      apTableStructure.FAMILY_STRUCTURE_ID,
                                                      apTableStructure.IS_DEFAULT,
                                                      apTableStructure.ID,
                                                  }).ToList();

                            if (attributePacks.Any())
                            {
                                strBuildXMLOutput.Append("<ATTRIBUTE_PACK    xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\"  COTYPE=\"\" TBGUID=\"M" + RandomClass.Next() + "\" aid:pstyle=\"attribute_pack\">");
                                foreach (var attributePack in attributePacks)
                                {
                                    var objSuperTable = new SuperTable();
                                    string layoutName = attributePack.STRUCTURE_NAME + ',' + attributePack.FAMILY_STRUCTURE_ID;
                                    string XmlForSuperTable = objSuperTable.GenerateMultipleTablexml(0, familyid, _catalogId, ImageMagickPath, Attr_List, User.Identity.Name, attributePack.PACK_ID, layoutName);
                                    strBuildXMLOutput.Append(XmlForSuperTable);
                                }
                                strBuildXMLOutput.Append("</ATTRIBUTE_PACK>");
                            }

                            #endregion

                            LoadReferenceTables();
                            //Executing Sub Family
                            for (rowSubFamCnt = 0; rowSubFamCnt < oDSSubFam.Tables[0].Rows.Count; rowSubFamCnt++)
                            {
                                //if (idColumnFlag)
                                strBuildXMLOutput.Append(" <sub_family COTYPE=\"\" family_id=\"" + oDSSubFam.Tables[0].Rows[rowSubFamCnt].ItemArray[0] + "\" MSID=\"ID" + oDSSubFam.Tables[0].Rows[rowSubFamCnt].ItemArray[0] + "SF\" TBGUID=\"SF" + RandomClass.Next() + "\">\n");
                                //else
                                //    strBuildXMLOutput.Append(" <sub_family COTYPE=\"\" TBGUID=\"SF" + RandomClass.Next() + "\">\n");
                                //Call Function for Family Generation
                                FamilyId = oDSSubFam.Tables[0].Rows[rowSubFamCnt].ItemArray[0].ToString();
                                if (Attr_List != "")
                                    SubFamily = GetFamilyDetails(oDSSubFam.Tables[0].Rows[rowSubFamCnt].ItemArray[0].ToString(), categoryId, "Sub", Attr_List, idColumnFlag);
                                else
                                    SubFamily = GetFamilyDetails(oDSSubFam.Tables[0].Rows[rowSubFamCnt].ItemArray[0].ToString(), categoryId, "Sub", idColumnFlag);
                                //LoadMultipleTables();
                                familyid = Convert.ToInt32(FamilyId);
                                multiplegroups = _dbcontext.TB_ATTRIBUTE_GROUP.Join(_dbcontext.TB_ATTRIBUTE_GROUP_SECTIONS, a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { a, b })
                                    .Where(x => x.b.CATALOG_ID == _catalogId && x.b.FAMILY_ID == familyid).Select(z => new { z.a.GROUP_ID, z.a.GROUP_NAME }).Distinct();
                                if (multiplegroups.Any())
                                {
                                    strBuildXMLOutput.Append("<ATTRIBUTE_GROUP    xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\"  COTYPE=\"\" TBGUID=\"M" + RandomClass.Next() + "\" aid:pstyle=\"attribute_group\">");
                                    foreach (var multiplegroup in multiplegroups)
                                    {
                                        var objSuperTable = new SuperTable();
                                        string XmlForSuperTable = objSuperTable.GenerateMultipleTablexml(multiplegroup.GROUP_ID, familyid, _catalogId, ImageMagickPath, Attr_List, User.Identity.Name, 0, "");
                                        strBuildXMLOutput.Append(XmlForSuperTable);
                                    }
                                    strBuildXMLOutput.Append("</ATTRIBUTE_GROUP>");
                                }
                                LoadReferenceTables();
                                strBuildXMLOutput.Append(" </sub_family>");
                            }




                            strBuildXMLOutput.Append("</product_family>\n");
                        }
                        else
                        {
                            //testing
                            // _SQLString = "SELECT CATEGORY_ID,PARENT_CATEGORY,CATEGORY_NAME from TB_CATEGORY WHERE Flag_recycle='A' and CATEGORY_ID IN (select distinct category_id from tb_project_section_details where family_id IN( " + FamilyId + ") and record_id = " + _xmlgen.RecordID + ")";

                            // ------------------------------- AFTER CUT PASTE FAMILY FROM ONE INDESIGN FILE TO ANOTHER---------------------------------------//
                            _SQLString = "SELECT DISTINCT FA.CATEGORY_ID,CA.PARENT_CATEGORY,CA.CATEGORY_NAME FROM TB_PROJECT_SECTION_DETAILS PSD " +
                                        " JOIN TB_PROJECT_SECTIONS PS ON PSD.RECORD_ID = PS.RECORD_ID " +
                                        " JOIN TB_FAMILY FA ON FA.FAMILY_ID = PSD.FAMILY_ID " +
                                        " JOIN TB_CATEGORY CA ON FA.CATEGORY_ID = CA.CATEGORY_ID " +
                                        " WHERE CA.FLAG_RECYCLE = 'A' AND FA.FLAG_RECYCLE = 'A'  AND FA.FAMILY_ID IN (" + FamilyId + ")  AND PSD.RECORD_ID = " + _xmlgen.RecordID + " ";
                            IDFieldss = CreateDataSet();
                            if (IDFieldss.Tables[0].Rows.Count > 0)
                            {
                                //if (idColumnFlag)
                                strBuildXMLOutput.Append("<product_family COTYPE=\"\" family_id=\"" + FamilyId + "\" sub_families=\"" + oDSSubFam.Tables[0].Rows.Count + "\" MSID=\"ID" + FamilyId + "PF\" TBGUID =\"PF" + RandomClass.Next() + "\">\n");
                                //else
                                //    strBuildXMLOutput.Append("<product_family COTYPE=\"\" sub_families=\"" + oDSSubFam.Tables[0].Rows.Count + "\" TBGUID=\"PF" + RandomClass.Next() + "\">\n");
                                //Call Function For Family Generation
                                if (Flg_Chk == false)
                                {
                                    if (attrSelect)
                                    {
                                        ////////InCatAttributeManager Attribute_Manager = new InCatAttributeManager(_xmlgen.FamilyList);
                                        ////////if (Attribute_Manager._Is_Attr_Available == true)
                                        ////////{
                                        ////////    Attribute_Manager.ShowDialog();
                                        ////////}
                                        Attr_List = string.Empty;
                                        foreach (var attributeId in attrLists)
                                        {
                                            Attr_List = Attr_List + attributeId.ATTRIBUTE_ID + ",";

                                        }
                                        Attr_List = Attr_List.TrimEnd(',');
                                        Flg_Chk = true;
                                        MainFamily = GetFamilyDetails(FamilyId, categoryId, "Main", Attr_List, idColumnFlag);
                                    }
                                    else
                                    {
                                        MainFamily = GetFamilyDetails(FamilyId, categoryId, "Main", idColumnFlag);
                                    }

                                }
                                else
                                {
                                    MainFamily = GetFamilyDetails(FamilyId, categoryId, "Main", Attr_List, idColumnFlag);
                                }
                                //   LoadMultipleTables();
                                int familyid = Convert.ToInt32(FamilyId);
                                var multiplegroups = _dbcontext.TB_ATTRIBUTE_GROUP.Join(_dbcontext.TB_ATTRIBUTE_GROUP_SECTIONS, a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { a, b })
                                    .Where(x => x.b.CATALOG_ID == _catalogId && x.b.FAMILY_ID == familyid).Select(z => new { z.a.GROUP_ID, z.a.GROUP_NAME }).Distinct();
                                if (multiplegroups.Any())
                                {
                                    strBuildXMLOutput.Append("<ATTRIBUTE_GROUP    xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\"  COTYPE=\"\" TBGUID=\"M" + RandomClass.Next() + "\" aid:pstyle=\"attribute_group\">");
                                    foreach (var multiplegroup in multiplegroups)
                                    {
                                        var objSuperTable = new SuperTable();
                                        string XmlForSuperTable = objSuperTable.GenerateMultipleTablexml(multiplegroup.GROUP_ID, familyid, _catalogId, ImageMagickPath, Attr_List, User.Identity.Name, 0, "");
                                        strBuildXMLOutput.Append(XmlForSuperTable);
                                    }
                                    strBuildXMLOutput.Append("</ATTRIBUTE_GROUP>");
                                }


                                LoadReferenceTables();
                                //Executing Sub Family
                                for (rowSubFamCnt = 0; rowSubFamCnt < oDSSubFam.Tables[0].Rows.Count; rowSubFamCnt++)
                                {
                                    //if (idColumnFlag)
                                    strBuildXMLOutput.Append(" <sub_family COTYPE=\"\" family_id=\"" + oDSSubFam.Tables[0].Rows[rowSubFamCnt].ItemArray[0] + "\" MSID=\"ID" + oDSSubFam.Tables[0].Rows[rowSubFamCnt].ItemArray[0] + "SF\" TBGUID=\"SF" + RandomClass.Next() + "\">\n");
                                    //else
                                    //    strBuildXMLOutput.Append(" <sub_family COTYPE=\"\" TBGUID=\"SF" + RandomClass.Next() + "\">\n");
                                    //Call Function for Family Generation
                                    FamilyId = oDSSubFam.Tables[0].Rows[rowSubFamCnt].ItemArray[0].ToString();
                                    if (Attr_List != "")
                                        SubFamily = GetFamilyDetails(oDSSubFam.Tables[0].Rows[rowSubFamCnt].ItemArray[0].ToString(), categoryId, "Sub", Attr_List, idColumnFlag);
                                    else
                                        SubFamily = GetFamilyDetails(oDSSubFam.Tables[0].Rows[rowSubFamCnt].ItemArray[0].ToString(), categoryId, "Sub", idColumnFlag);

                                    familyid = Convert.ToInt32(FamilyId);
                                    multiplegroups = _dbcontext.TB_ATTRIBUTE_GROUP.Join(_dbcontext.TB_ATTRIBUTE_GROUP_SECTIONS, a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { a, b })
                                        .Where(x => x.b.CATALOG_ID == _catalogId && x.b.FAMILY_ID == familyid).Select(z => new { z.a.GROUP_ID, z.a.GROUP_NAME }).Distinct();
                                    if (multiplegroups.Any())
                                    {
                                        strBuildXMLOutput.Append("<ATTRIBUTE_GROUP    xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\"  COTYPE=\"\" TBGUID=\"M" + RandomClass.Next() + "\" aid:pstyle=\"attribute_group\">");
                                        foreach (var multiplegroup in multiplegroups)
                                        {
                                            var objSuperTable = new SuperTable();
                                            string XmlForSuperTable = objSuperTable.GenerateMultipleTablexml(multiplegroup.GROUP_ID, familyid, _catalogId, ImageMagickPath, Attr_List, User.Identity.Name, 0, "");
                                            strBuildXMLOutput.Append(XmlForSuperTable);
                                        }
                                        strBuildXMLOutput.Append("</ATTRIBUTE_GROUP>");
                                    }

                                    // LoadMultipleTables();
                                    LoadReferenceTables();
                                    strBuildXMLOutput.Append(" </sub_family>");
                                }




                                strBuildXMLOutput.Append("</product_family>\n");
                            }
                        }
                        oDSSubFam.Dispose();
                        oDSSubFam = null;
                    }//end filter 
                }
                strBuildXMLOutput.Append("</category>\n");
                strBuildXMLOutput.Append("</tradingbell_root>");

            }

            return strBuildXMLOutput.ToString().Replace("\r\n\r\n", "\r\n");
            // return null;
        }
        public string GetFamilyDetails(string FamilyId, string categoryId, string FamilyType, bool idColumnFlag)
        {
            //Getting Family
            try
            {
                int cont = 0;
                if (FamilyType == "Sub")
                    _SQLString = "SELECT distinct f.family_name,c.category_name,f.foot_notes,SHORT_DESC,IMAGE_FILE,IMAGE_TYPE,IMAGE_NAME,IMAGE_NAME2,IMAGE_FILE2," +
                        " IMAGE_TYPE2,CUSTOM_NUM_FIELD1,CUSTOM_NUM_FIELD2,CUSTOM_NUM_FIELD3 ,CUSTOM_TEXT_FIELD1,CUSTOM_TEXT_FIELD2,CUSTOM_TEXT_FIELD3  " +
                       " FROM tb_family f, tb_category c WHERE C.CATEGORY_ID = " +
                       " (SELECT CATEGORY_ID FROM tb_project_section_details WHERE family_id =(SELECT FAMILY_ID FROM TB_SUBFAMILY WHERE SUBFAMILY_ID = " + FamilyId + ")  and f.FLAG_RECYCLE='A' and record_id =" + _xmlgen.RecordID + ")AND F.FAMILY_ID =  " + FamilyId + " ";
                else

                    //  _SQLString = "SELECT distinct f.family_name,f.category_name,f.foot_notes,SHORT_DESC,IMAGE_FILE,IMAGE_TYPE,IMAGE_NAME,IMAGE_NAME2,IMAGE_FILE2," +
                    // " IMAGE_TYPE2,CUSTOM_NUM_FIELD1,CUSTOM_NUM_FIELD2,CUSTOM_NUM_FIELD3 ,CUSTOM_TEXT_FIELD1,CUSTOM_TEXT_FIELD2,CUSTOM_TEXT_FIELD3  " +
                    //" FROM tb_family f, tb_category c WHERE C.CATEGORY_ID = " +
                    //" (SELECT CATEGORY_ID FROM tb_project_section_details WHERE family_id =  " + FamilyId + "  and f.FLAG_RECYCLE='A' and c.FLAG_RECYCLE='A' and record_id =" + _xmlgen.RecordID + ")AND F.FAMILY_ID =  " + FamilyId + " ";

                    _SQLString = "SELECT DISTINCT FA.FAMILY_NAME,CA.CATEGORY_NAME,FA.FOOT_NOTES,SHORT_DESC,IMAGE_FILE,IMAGE_TYPE,IMAGE_NAME,IMAGE_NAME2,IMAGE_FILE2,IMAGE_TYPE2,CUSTOM_NUM_FIELD1,CUSTOM_NUM_FIELD2,CUSTOM_NUM_FIELD3 ,CUSTOM_TEXT_FIELD1,CUSTOM_TEXT_FIELD2,CUSTOM_TEXT_FIELD3 FROM TB_PROJECT_SECTION_DETAILS PSD " +
                          " JOIN TB_PROJECT_SECTIONS PS ON PSD.RECORD_ID = PS.RECORD_ID " +
                          " JOIN TB_FAMILY FA ON FA.FAMILY_ID = PSD.FAMILY_ID  and FA.PUBLISH2PRINT=1 " +
                          " JOIN TB_CATEGORY CA ON FA.CATEGORY_ID = CA.CATEGORY_ID and CA.PUBLISH2PRINT=1" +
                          " WHERE CA.FLAG_RECYCLE = 'A' AND FA.FLAG_RECYCLE = 'A' AND  FA.FAMILY_ID IN ( " + FamilyId + ")  AND PSD.RECORD_ID = " + _xmlgen.RecordID + " ";
                oDSFam = CreateDataSet();
                //testing
                _SQLString = "SELECT DISTINCT TA.ATTRIBUTE_ID, REPLACE(TA.ATTRIBUTE_NAME, ' ','_') ATTRIBUTE_NAME, TFS.STRING_VALUE, TA.ATTRIBUTE_TYPE,REPLACE(TA.STYLE_NAME, ' ','_') as STYLE_NAME,TFS.NUMERIC_VALUE,A.SORT_ORDER  " +
                             "    FROM TB_FAMILY_SPECS TFS LEFT JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID AND ATTRIBUTE_TYPE IN(7,11,12) AND PUBLISH2PRINT = 1 LEFT JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID = " + _catalogId + " JOIN TB_CATEGORY_FAMILY_ATTR_LIST A ON A.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID " +
                             " WHERE TFS.FAMILY_ID = " + FamilyId + " AND TA.ATTRIBUTE_ID IS NOT NULL AND A.CATALOG_ID=" + _catalogId + " AND A.FAMILY_ID =  " + FamilyId + " UNION SELECT DISTINCT TA.ATTRIBUTE_ID, REPLACE(TA.ATTRIBUTE_NAME, ' ','_') ATTRIBUTE_NAME, TFS.ATTRIBUTE_VALUE, TA.ATTRIBUTE_TYPE,REPLACE(TA.STYLE_NAME, ' ','_') as STYLE_NAME,NULL,A.SORT_ORDER  FROM TB_FAMILY_KEY TFS LEFT JOIN TB_ATTRIBUTE TA " +
                             " ON TA.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID AND ATTRIBUTE_TYPE IN(13) AND PUBLISH2PRINT = 1 LEFT JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID = " + _catalogId + " JOIN TB_CATEGORY_FAMILY_ATTR_LIST A ON A.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID WHERE TFS.FAMILY_ID = " + FamilyId + " AND TA.ATTRIBUTE_ID IS NOT NULL " +
                             "  and TA.FLAG_RECYCLE='A' and A.FLAG_RECYCLE='A'  AND A.CATALOG_ID = TFS.CATALOG_ID AND A.CATALOG_ID=" + _catalogId + " AND A.FAMILY_ID = " + FamilyId + " ORDER BY A.SORT_ORDER";

                oXMLDS_FamilySpecs = CreateDataSet();


                //oFamilyimgAdap = new TradingBell.CatalogStudi o.CoreDS.CatalogXMLTableAdapters.FamilyImages_TableAdapter();
                //oFamilyimgAdap.Fill(oXMLDS_FamilyImages_.Tables[0], CatalogId, Convert.ToInt32(FamilyId));

                _SQLString = " SELECT  REPLACE(TA.ATTRIBUTE_NAME, ' ', '_') AS ATTRIBUTE_NAME,TA.ATTRIBUTE_ID, TFS.STRING_VALUE, TA.ATTRIBUTE_TYPE, TFS.OBJECT_NAME, TFS.OBJECT_TYPE" +
                            " FROM TB_FAMILY_SPECS AS TFS LEFT OUTER JOIN TB_ATTRIBUTE AS TA ON TA.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID AND TA.ATTRIBUTE_TYPE = 9 AND TA.PUBLISH2PRINT = 1 LEFT OUTER JOIN " +
                            " TB_CATALOG_ATTRIBUTES AS TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID =" + _catalogId + " JOIN TB_CATEGORY_FAMILY_ATTR_LIST A ON A.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID " +
                            " WHERE (TFS.FAMILY_ID = " + FamilyId + " ) and TA.FLAG_RECYCLE='A' and A.FLAG_RECYCLE='A' AND (TA.ATTRIBUTE_ID IS NOT NULL) AND A.CATALOG_ID=" + _catalogId + " AND A.FAMILY_ID = " + FamilyId + "  ORDER BY A.SORT_ORDER";
                oXMLDS_FamilyImages_ = CreateDataSet();



                for (rowFamCnt = 0; rowFamCnt < oDSFam.Tables[0].Rows.Count; rowFamCnt++)
                {
                    string catlevelEntry = "";
                    string parentcat = "0";
                    int val = 0;
                    string Sqlstrr = "";

                    if (FamilyType == "Main")
                    {
                        //_SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY from TB_CATEGORY WHERE FLAG_RECYCLE='A' AND CATEGORY_ID IN (select distinct category_id from tb_project_section_details where family_id IN( " + FamilyId + ")  and record_id = " + _xmlgen.RecordID + ")";

                        // ------------------------------- AFTER CUT PASTE FAMILY FROM ONE INDESIGN FILE TO ANOTHER---------------------------------------//
                        _SQLString = "SELECT DISTINCT CA.CATEGORY_NAME,CA.PARENT_CATEGORY FROM TB_PROJECT_SECTION_DETAILS PSD " +
                                    " JOIN TB_PROJECT_SECTIONS PS ON PSD.RECORD_ID = PS.RECORD_ID " +
                                    " JOIN TB_FAMILY FA ON FA.FAMILY_ID = PSD.FAMILY_ID AND FA.PUBLISH2PRINT=1" +
                                    " JOIN TB_CATEGORY CA ON FA.CATEGORY_ID = CA.CATEGORY_ID AND CA.PUBLISH2PRINT=1" +
                                    " WHERE CA.FLAG_RECYCLE = 'A' AND FA.FLAG_RECYCLE = 'A'  AND FA.FAMILY_ID IN (" + FamilyId + ")  AND PSD.RECORD_ID = " + _xmlgen.RecordID + " ";
                    }
                    else if (FamilyType == "Sub")
                    {
                        _SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY from TB_CATEGORY WHERE PUBLISH2PRINT=1 AND CATEGORY_ID IN (select distinct category_id from tb_project_section_details where family_id IN ( SELECT FAMILY_ID FROM TB_SUBFAMILY WHERE SUBFAMILY_ID IN (" + FamilyId + ") ) and FLAG_RECYCLE='A' and record_id = " + _xmlgen.RecordID + ")";
                    }
                    DataSet IDFields = CreateDataSet();
                    if (IDFields.Tables[0].Rows.Count > 0)
                    {
                        cont = 1;
                        parentcat = IDFields.Tables[0].Rows[0].ItemArray[1].ToString();
                        while (parentcat != "0")
                        {
                            val++;
                            _SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY from TB_CATEGORY WHERE  PUBLISH2PRINT=1 AND  CATEGORY_ID = '" + parentcat + "' and FLAG_RECYCLE='A'";
                            IDFields = CreateDataSet();
                            parentcat = IDFields.Tables[0].Rows[0].ItemArray[1].ToString();
                        }

                        if (FamilyType == "Main")
                        {
                            //_SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY from TB_CATEGORY WHERE  FLAG_RECYCLE='A' and  CATEGORY_ID IN  (select distinct category_id from tb_project_section_details where family_id IN ( " + FamilyId + ") and record_id = " + _xmlgen.RecordID + ")";
                            // ------------------------------- AFTER CUT PASTE FAMILY FROM ONE INDESIGN FILE TO ANOTHER---------------------------------------//
                            _SQLString = "SELECT DISTINCT CA.CATEGORY_NAME,CA.PARENT_CATEGORY FROM TB_PROJECT_SECTION_DETAILS PSD " +
                                        " JOIN TB_PROJECT_SECTIONS PS ON PSD.RECORD_ID = PS.RECORD_ID " +
                                        " JOIN TB_FAMILY FA ON FA.FAMILY_ID = PSD.FAMILY_ID AND FA.PUBLISH2PRINT=1 " +
                                        " JOIN TB_CATEGORY CA ON FA.CATEGORY_ID = CA.CATEGORY_ID AND CA.PUBLISH2PRINT=1 " +
                                        " WHERE CA.FLAG_RECYCLE = 'A' AND FA.FLAG_RECYCLE = 'A'  AND FA.FAMILY_ID IN (" + FamilyId + ")  AND PSD.RECORD_ID = " + _xmlgen.RecordID + " ";



                        }
                        else if (FamilyType == "Sub")
                        {
                            _SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY from TB_CATEGORY WHERE PUBLISH2PRINT=1 AND CATEGORY_ID IN (select distinct category_id from tb_project_section_details where family_id IN ( SELECT FAMILY_ID FROM TB_SUBFAMILY WHERE SUBFAMILY_ID IN (" + FamilyId + " )) and FLAG_RECYCLE='A' and record_id = " + _xmlgen.RecordID + ")";
                        }
                        IDFields = CreateDataSet();
                        parentcat = IDFields.Tables[0].Rows[0].ItemArray[1].ToString();
                        while (parentcat != "0")
                        {
                            _SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY,CATEGORY_ID from TB_CATEGORY WHERE PUBLISH2PRINT=1 AND CATEGORY_ID = '" + parentcat + "' and FLAG_RECYCLE='A'";
                            IDFields = CreateDataSet();
                            catlevelEntry = "<SUBCATNAME_L" + val.ToString() + " COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "SCN\" TBGUID =\"FCN" + RandomClass.Next() + "\" aid:pstyle=\"SUBCATNAME_L" + val.ToString() + "\"><![CDATA[" + IDFields.Tables[0].Rows[0].ItemArray[0].ToString().Trim() + "]]></SUBCATNAME_L" + val.ToString() + ">\n" + catlevelEntry;
                            // if (idColumnFlag)
                            catlevelEntry = "<SUBCATNAME_ID" + val.ToString() + " COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "SCI\" TBGUID=\"FCN" + RandomClass.Next() + "\" aid:pstyle=\"SUBCATNAME_ID" + val.ToString() + "\"><![CDATA[" + IDFields.Tables[0].Rows[0].ItemArray[2].ToString().Trim() + "]]></SUBCATNAME_ID" + val.ToString() + ">\n" + catlevelEntry;
                            parentcat = IDFields.Tables[0].Rows[0].ItemArray[1].ToString();
                            val--;
                        }
                        strBuildXMLOutput.Append(catlevelEntry);
                        if (FamilyType == "Main")
                        //testing
                        {
                            // _SQLString = "SELECT DISTINCT CATEGORY_ID FROM TB_PROJECT_SECTION_DETAILS WHERE FAMILY_ID IN ( " + FamilyId + ")  and RECORD_ID = " + _xmlgen.RecordID + "";
                            // ------------------------------- AFTER CUT PASTE FAMILY FROM ONE INDESIGN FILE TO ANOTHER---------------------------------------//
                            _SQLString = "SELECT DISTINCT FA.CATEGORY_ID FROM TB_PROJECT_SECTION_DETAILS PSD " +
                                        " JOIN TB_PROJECT_SECTIONS PS ON PSD.RECORD_ID = PS.RECORD_ID " +
                                        " JOIN TB_FAMILY FA ON FA.FAMILY_ID = PSD.FAMILY_ID AND FA.PUBLISH2PRINT=1 " +
                                        " JOIN TB_CATEGORY CA ON FA.CATEGORY_ID = CA.CATEGORY_ID AND CA.PUBLISH2PRINT=1 " +
                                        " WHERE CA.FLAG_RECYCLE = 'A' AND FA.FLAG_RECYCLE = 'A'  AND FA.FAMILY_ID IN (" + FamilyId + ")  AND PSD.RECORD_ID = " + _xmlgen.RecordID + " ";
                        }
                        else if (FamilyType == "Sub")
                        {
                            _SQLString = "select distinct category_id from tb_project_section_details where family_id IN ( SELECT FAMILY_ID FROM TB_SUBFAMILY WHERE SUBFAMILY_ID =" + FamilyId + " ) and record_id = " + _xmlgen.RecordID + "";
                        }
                        IDFields = CreateDataSet();
                        parentcat = IDFields.Tables[0].Rows[0].ItemArray[0].ToString();

                        _SQLString = "select DISTINCT TA.ATTRIBUTE_ID, REPLACE(TA.ATTRIBUTE_NAME,' ','_') ATTRIBUTE_NAME, TCS.STRING_VALUE, TA.ATTRIBUTE_TYPE,REPLACE(TA.STYLE_NAME, ' ','_') as STYLE_NAME,TCS.NUMERIC_VALUE,TCS.SORT_ORDER " +
                      " From TB_CATEGORY_SPECS TCS LEFT JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TCS.ATTRIBUTE_ID AND ATTRIBUTE_TYPE IN(21,25) AND PUBLISH2PRINT = 1 LEFT JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID =" + _catalogId + " " +
                       "WHERE TCS.CATEGORY_ID ='" + parentcat + "' AND TA.ATTRIBUTE_ID IS NOT NULL";
                        oXMLDS_CategorySpecsLowLevel = CreateDataSet();

                        _SQLString = "select DISTINCT REPLACE(TA.ATTRIBUTE_NAME,' ','_') ATTRIBUTE_NAME,TA.ATTRIBUTE_ID,TCS.STRING_VALUE, TA.ATTRIBUTE_TYPE,TCS.OBJECT_NAME, TCS.OBJECT_TYPE " +
                        " From TB_CATEGORY_SPECS TCS LEFT JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TCS.ATTRIBUTE_ID AND ATTRIBUTE_TYPE IN(23) AND PUBLISH2PRINT = 1 LEFT JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID =" + _catalogId + " " +
                       "WHERE TCS.CATEGORY_ID ='" + parentcat + "' AND TA.ATTRIBUTE_ID IS NOT NULL";
                        oXMLDS_CategoryImageLowLevel = CreateDataSet();


                        // if (idColumnFlag)

                        strBuildXMLOutput.Append("<category_id  COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CI1\" TBGUID =\"FCI" + RandomClass.Next() + "\" aid:pstyle=\"category_id\"><![CDATA[" + parentcat + "]]></category_id>\n");
                        strBuildXMLOutput.Append("<category_id  COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CI2\" TBGUID=\"FCI" + RandomClass.Next() + "\" aid:pstyle=\"category_id\"><![CDATA[" + parentcat + "]]></category_id>\n");

                        strBuildXMLOutput.Append("<category_name  COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CN1\" TBGUID=\"FCN" + RandomClass.Next() + "\" aid:pstyle=\"category_name\"><![CDATA[" + oDSFam.Tables[0].Rows[rowFamCnt].ItemArray[1].ToString().Trim() + "]]></category_name>\n");
                        strBuildXMLOutput.Append("<category_name  COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CN2\" TBGUID=\"FCN" + RandomClass.Next() + "\" aid:pstyle=\"category_name\"><![CDATA[" + oDSFam.Tables[0].Rows[rowFamCnt].ItemArray[1].ToString().Trim() + "]]></category_name>\n");


                        foreach (DataRow dtr in oXMLDS_CategorySpecsLowLevel.Tables[0].Rows)
                        {
                            if (dtr["STRING_VALUE"].ToString() != null && dtr["STRING_VALUE"].ToString() != "" && dtr["ATTRIBUTE_ID"].ToString() != null && dtr["ATTRIBUTE_ID"].ToString() != "")
                            {
                                _SQLString = "select ATTRIBUTE_DATATYPE, ATTRIBUTE_DATAFORMAT from tb_attribute where ATTRIBUTE_ID=" + dtr["ATTRIBUTE_ID"].ToString() + " and FLAG_RECYCLE='A'";
                                DataSet AttributeDataFormatDS = CreateDataSet();
                                if (AttributeDataFormatDS.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow dr in AttributeDataFormatDS.Tables[0].Rows)
                                    {
                                        if (dr["ATTRIBUTE_DATATYPE"].ToString() == "Date and Time")
                                        {
                                            string DateValue = dtr["STRING_VALUE"].ToString();
                                            if (dr["ATTRIBUTE_DATAFORMAT"].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                            {
                                                dtr["STRING_VALUE"] = DateValue.Substring(3, 2) + "/" + DateValue.Substring(0, 2) + "/" + DateValue.Substring(6, 4);

                                            }

                                        }
                                    }
                                }
                            }
                        }


                        strBuildXMLOutput.Append("<cat_specs_type COTYPE=\"\" MSID=\"ID" + _xmlgen.CategoryId + "CST\" TBGUID=\"ST" + RandomClass.Next() + "\">\n");
                        for (rowCatAttrLowLevelCnt = 0; rowCatAttrLowLevelCnt < oXMLDS_CategorySpecsLowLevel.Tables[0].Rows.Count; rowCatAttrLowLevelCnt++)
                        #region From PreviewHTMLDATA.cs
                        {
                            AttrName = "";
                            string Style = string.Empty;
                            Style = oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[4].ToString();
                            if (Style == string.Empty || Style == "")
                            {
                                AttrName = oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[1].ToString();
                            }
                            if (Convert.ToInt32(oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[3].ToString()) != 23)
                            {
                                GetCurrencySymbol(oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString());
                                string sValue = string.Empty;
                                string dtype = oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[3].ToString();
                                int numberdecimel = 0;
                                if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                                {
                                    numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                                }
                                bool styleFormat = false; // Declared Static Temporarily                           
                                _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                                DataSet AttrDatatype = CreateDataSet();

                                #region "Decimal Place Triming"
                                if (AttrDatatype.Tables[0].Rows.Count > 0)
                                {
                                    if (oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[5].ToString() != null && oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[5].ToString() != "")
                                    {
                                        foreach (DataRow dr in AttrDatatype.Tables[0].Rows)
                                        {
                                            if (dr.ItemArray[0].ToString().StartsWith("Num"))
                                            {
                                                string tempStr = string.Empty;
                                                tempStr = oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[5].ToString();
                                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt]["NUMERIC_VALUE"] = tempStr;
                                            }
                                        }
                                    }
                                }
                                #endregion

                                if (AttrDatatype.Tables[0].Rows.Count > 0)
                                {
                                    if (AttrDatatype.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num"))
                                    {
                                        sValue = oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[5].ToString();
                                    }
                                    else
                                    {
                                        sValue = oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[2].ToString();
                                    }
                                    if (sValue == "" && (_emptyCondition == "Null" || _emptyCondition == "Empty"))
                                    {
                                        if (sValue == "" && _emptyCondition != "Empty")
                                        {
                                            sValue = "Null";
                                        }
                                    }
                                }
                                else
                                {
                                    sValue = oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[2].ToString();
                                }
                                //}
                                if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                                {
                                    if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                    if (_replaceText != "")
                                    {
                                        if (sValue == "0")
                                        {
                                            if (_emptyCondition == "0.000000")
                                            {
                                                sValue = _emptyCondition;
                                            }
                                        }
                                        if (sValue == "Null")
                                        {
                                            _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                        }
                                        else
                                        {
                                            _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                        }
                                        if (_replaceText != "")
                                            //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                            //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                            //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                            if (Convert.ToString(_replaceText) != string.Empty)
                                            {
                                                if (Convert.ToString(_replaceText) != sValue)
                                                {
                                                    if (_fornumeric == "1")
                                                    {
                                                        if (Isnumber(_replaceText.Replace(",", "")))
                                                        {
                                                            if (_headeroptions == "All")
                                                            {
                                                                if (Style == string.Empty || Style == "")
                                                                {
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "CSA\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                }
                                                                else
                                                                {
                                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "CSA\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (rowFamAttrCnt == 0)
                                                                {
                                                                    if (Style == string.Empty || Style == "")
                                                                        //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                    else
                                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                                }
                                                                else
                                                                {
                                                                    if (Style == string.Empty || Style == "")
                                                                        //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                                    else
                                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (_headeroptions == "All")
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (rowCatAttrLowLevelCnt == 0)
                                                            {
                                                                if (Style == string.Empty || Style == "")
                                                                    //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                            }
                                                            else
                                                            {
                                                                if (Style == string.Empty || Style == "")
                                                                    //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_headeroptions == "All")
                                                    {
                                                        if (Style == string.Empty || Style == "")
                                                            //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                        else
                                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                    }
                                                    else
                                                    {
                                                        if (rowCatAttrLowLevelCnt == 0)
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID =\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                        }
                                                    }
                                                }
                                            }
                                    }
                                    else
                                    {
                                        _replaceText = sValue;
                                    }
                                }
                                else if (oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[2].ToString().Trim() != "")
                                {
                                    //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                    //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                    if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            if (Style == string.Empty || Style == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                        }
                                        else
                                        {
                                            if (rowCatAttrLowLevelCnt == 0)
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (Isnumber(sValue))
                                        {
                                            sValue = DecPrecisionFill(sValue, numberdecimel);
                                            sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString()), sValue.Trim()) : sValue;
                                        }
                                        if (_headeroptions == "All")
                                        {
                                            if (Style == string.Empty || Style == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                        }
                                        else
                                        {
                                            if (rowFamAttrCnt == 0)
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                            }
                                        }
                                    }
                                }
                                else if (oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[5].ToString().Trim() != "")
                                {
                                    _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                                    DataSet dsSize = CreateDataSet();
                                    sValue = oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[5].ToString();
                                    if (dsSize != null)
                                    {
                                        if (dsSize.Tables.Count > 0)
                                        {
                                            if (dsSize.Tables[0].Rows.Count > 0)
                                            {
                                                string Valsize = dsSize.Tables[0].Rows[0]["ATTRIBUTE_DATATYPE"].ToString();
                                                sValue = Valchk(sValue, Valsize); // Created Valchk function newly for this 
                                            }
                                        }
                                    }
                                    //sValue = htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[2].ToString();
                                    if (sValue != "")
                                    {
                                        //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                        //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                        if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (rowCatAttrLowLevelCnt == 0)
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                                }
                                                else
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");

                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (rowCatAttrLowLevelCnt == 0)
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                                }
                                                else
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CSA" + oXMLDS_CategorySpecsLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                                }
                                            }
                                        }
                                    }
                                }



                            }
                        }
                        #endregion

                        strBuildXMLOutput.Append("</cat_specs_type>\n");
                        //Start Family Image 


                        strBuildXMLOutput.Append("<image_type COTYPE=\"\" MSID=\"ID005IT\" TBGUID=\"IT" + RandomClass.Next() + "\">\n");
                        FPath = "href=\"file://";
                        for (rowCatImgCntLow = 0; rowCatImgCntLow < oXMLDS_CategoryImageLowLevel.Tables[0].Rows.Count; rowCatImgCntLow++)
                        {
                            ImgFile = "";
                            AttrName = "";
                            ImgFile = oXMLDS_CategoryImageLowLevel.Tables[0].Rows[rowCatImgCntLow].ItemArray[2].ToString();
                            _xmlgen.UserImagePath = _xmlgen.UserImagePath.Replace("&", "&amp;");
                            ImgFile = ImgFile.Replace("\\", "/");
                            ImageName = _xmlgen.UserImagePath + ImgFile;
                            ImageName = ImageName.Replace("/", "\\");
                            ImgFile = FPath + ImageName + "\"";

                            AttrName = oXMLDS_CategoryImageLowLevel.Tables[0].Rows[rowCatImgCntLow].ItemArray[0].ToString().Trim();
                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + " attribute_id=\"" + oXMLDS_CategoryImageLowLevel.Tables[0].Rows[rowCatImgCntLow].ItemArray[1].ToString().Trim() + "\"" + "  IMAGE_FILE=\"" + oXMLDS_CategoryImageLowLevel.Tables[0].Rows[rowCatImgCntLow].ItemArray[2].ToString().Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\" COTYPE=\"IMAGE\" MSID=\"ID" + _xmlgen.CategoryId + "CIT" + oXMLDS_CategoryImageLowLevel.Tables[0].Rows[rowCatImgCntLow].ItemArray[1].ToString().Trim() + "\" TBGUID=\"IV" + RandomClass.Next() + "\"></" + AttrName.ToLower() + ">\n");
                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "_name attribute_id=\"" + oXMLDS_CategoryImageLowLevel.Tables[0].Rows[rowCatImgCntLow].ItemArray[1].ToString().Trim() + "\"" + "  COTYPE=\"TEXT\" MSID=\"ID" + _xmlgen.CategoryId + "CIF" + oXMLDS_CategoryImageLowLevel.Tables[0].Rows[rowCatImgCntLow].ItemArray[1].ToString().Trim() + "\" TBGUID=\"IN" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "_name\"><![CDATA[" + oXMLDS_CategoryImageLowLevel.Tables[0].Rows[rowCatImgCntLow].ItemArray[4].ToString().Trim() + "]]></" + AttrName.ToLower() + "_name>\n");
                        }
                        strBuildXMLOutput.Append("</image_type>\n");





                        if (FamilyType == "Main")
                        {
                            strBuildXMLOutput.Append("<family_name  COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FN\" TBGUID=\"FN" + RandomClass.Next() + "\" aid:pstyle=\"family_name\"><![CDATA[" + oDSFam.Tables[0].Rows[rowFamCnt].ItemArray[0].ToString().Trim() + "]]></family_name>\n");
                        }
                        else if (FamilyType == "Sub")
                        {
                            strBuildXMLOutput.Append("<Subfamily_name  COTYPE=\"TEXT\" MSID=\"ID" + oDSSubFam.Tables[0].Rows[rowSubFamCnt].ItemArray[0] + "SFN\" TBGUID =\"SFN" + RandomClass.Next() + "\" aid:pstyle=\"Subfamily_name\"><![CDATA[" + oDSFam.Tables[0].Rows[rowFamCnt].ItemArray[0].ToString().Trim() + "]]></Subfamily_name>\n");
                        }
                    }
                    //strBuildXMLOutput.Append("<foot_notes aid:pstyle=\"foot_notes\" COTYPE=\"TEXT\" TBGUID=\"FFN" + RandomClass.Next() + "\"><![CDATA[" + oDSFam.Tables[0].Rows[rowFamCnt].ItemArray[2].ToString().Trim() + "]]></foot_notes>\n");
                }
                oDSFam.Dispose();
                oDSFam = null;
                //End family 
                #region "Date Format Process"
                //DataTable TempDS = oXMLDS_FamilySpecs.Tables[0];
                foreach (DataRow dtr in oXMLDS_FamilySpecs.Tables[0].Rows)
                {
                    if (dtr["STRING_VALUE"].ToString() != null && dtr["STRING_VALUE"].ToString() != "" && dtr["ATTRIBUTE_ID"].ToString() != null && dtr["ATTRIBUTE_ID"].ToString() != "")
                    {
                        _SQLString = "select ATTRIBUTE_DATATYPE, ATTRIBUTE_DATAFORMAT from tb_attribute where ATTRIBUTE_ID=" + dtr["ATTRIBUTE_ID"].ToString() + " and FLAG_RECYCLE='A'";
                        DataSet AttributeDataFormatDS = CreateDataSet();
                        if (AttributeDataFormatDS.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in AttributeDataFormatDS.Tables[0].Rows)
                            {
                                if (dr["ATTRIBUTE_DATATYPE"].ToString() == "Date and Time")
                                {
                                    string DateValue = dtr["STRING_VALUE"].ToString();
                                    if (dr["ATTRIBUTE_DATAFORMAT"].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                    {
                                        dtr["STRING_VALUE"] = DateValue.Substring(3, 2) + "/" + DateValue.Substring(0, 2) + "/" + DateValue.Substring(6, 4);

                                    }

                                }
                            }
                        }
                    }
                }
                #endregion
                if (cont == 1)
                {
                    //Start Family Specs and Desc 
                    strBuildXMLOutput.Append("<specs_type COTYPE=\"\" MSID=\"ID002FST" + _xmlgen.CategoryId + "\" TBGUID=\"ST" + RandomClass.Next() + "\">\n");

                    for (rowFamAttrCnt = 0; rowFamAttrCnt < oXMLDS_FamilySpecs.Tables[0].Rows.Count; rowFamAttrCnt++)
                    #region From PreviewHTMLDATA.cs
                    {
                        AttrName = "";
                        string Style = string.Empty;
                        Style = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[4].ToString();
                        if (Style == string.Empty || Style == "")
                        {
                            AttrName = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[1].ToString();
                        }
                        if (Convert.ToInt32(oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[3].ToString()) != 9)
                        {
                            GetCurrencySymbol(oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString());
                            string sValue = string.Empty;
                            string dtype = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[3].ToString();
                            int numberdecimel = 0;
                            if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                            {
                                numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                            }
                            bool styleFormat = false; // Declared Static Temporarily
                            //bool styleFormat = htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[9].ToString().Length > 0;
                            if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[3].ToString().Trim() != "12")
                            {
                                //if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[3].ToString().Trim() == "13")
                                //{
                                //    sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[2].ToString();
                                //}
                                //else
                                //{
                                _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                                DataSet AttrDatatype = CreateDataSet();

                                #region "Decimal Place Triming"
                                if (AttrDatatype.Tables[0].Rows.Count > 0)
                                {
                                    if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString() != null && oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString() != "")
                                    {
                                        foreach (DataRow dr in AttrDatatype.Tables[0].Rows)
                                        {
                                            if (dr.ItemArray[0].ToString().StartsWith("Num"))
                                            {
                                                string tempStr = string.Empty;
                                                tempStr = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString();
                                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt]["NUMERIC_VALUE"] = tempStr;
                                            }
                                        }
                                    }
                                }
                                #endregion

                                if (AttrDatatype.Tables[0].Rows.Count > 0)
                                {
                                    if (AttrDatatype.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num"))
                                    {
                                        if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[3].ToString().Trim() == "13")
                                        {
                                            sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[2].ToString();
                                        }
                                        else
                                        {
                                            sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString();
                                        }
                                    }
                                    else
                                    {
                                        sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[2].ToString();
                                    }
                                    if (sValue == "" && (_emptyCondition == "Null" || _emptyCondition == "Empty"))
                                    {
                                        if (sValue == "" && _emptyCondition != "Empty")
                                        {
                                            sValue = "Null";
                                        }
                                    }
                                }
                                else
                                {
                                    sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[2].ToString();
                                }
                                //}
                                if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                                {
                                    if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                    if (_replaceText != "")
                                    {
                                        if (sValue == "0")
                                        {
                                            if (_emptyCondition == "0.000000")
                                            {
                                                sValue = _emptyCondition;
                                            }
                                        }
                                        if (sValue == "Null")
                                        {
                                            _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                        }
                                        else
                                        {
                                            _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                        }
                                        if (_replaceText != "")
                                            //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                            //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                            //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                            if (Convert.ToString(_replaceText) != string.Empty)
                                            {
                                                if (Convert.ToString(_replaceText) != sValue)
                                                {
                                                    if (_fornumeric == "1")
                                                    {
                                                        if (Isnumber(_replaceText.Replace(",", "")))
                                                        {
                                                            if (_headeroptions == "All")
                                                            {
                                                                if (Style == string.Empty || Style == "")
                                                                {
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                }
                                                                else
                                                                {
                                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (rowFamAttrCnt == 0)
                                                                {
                                                                    if (Style == string.Empty || Style == "")
                                                                        //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                    else
                                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                                }
                                                                else
                                                                {
                                                                    if (Style == string.Empty || Style == "")
                                                                        //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                                    else
                                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (_headeroptions == "All")
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (rowFamAttrCnt == 0)
                                                            {
                                                                if (Style == string.Empty || Style == "")
                                                                    //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                            }
                                                            else
                                                            {
                                                                if (Style == string.Empty || Style == "")
                                                                    //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_headeroptions == "All")
                                                    {
                                                        if (Style == string.Empty || Style == "")
                                                            //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                        else
                                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                    }
                                                    else
                                                    {
                                                        if (rowFamAttrCnt == 0)
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                        }
                                                    }
                                                }
                                            }
                                    }
                                    else
                                    {
                                        _replaceText = sValue;
                                    }
                                }
                                else if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[2].ToString().Trim() != "")
                                {
                                    //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                    //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                    if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            if (Style == string.Empty || Style == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                        }
                                        else
                                        {
                                            if (rowFamAttrCnt == 0)
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (Isnumber(sValue))
                                        {
                                            sValue = DecPrecisionFill(sValue, numberdecimel);
                                            sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString()), sValue.Trim()) : sValue;
                                        }
                                        if (_headeroptions == "All")
                                        {
                                            if (Style == string.Empty || Style == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                        }
                                        else
                                        {
                                            if (rowFamAttrCnt == 0)
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                            }
                                        }
                                    }
                                }
                                else if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString().Trim() != "")
                                {
                                    _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                                    DataSet dsSize = CreateDataSet();
                                    sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString();
                                    if (dsSize != null)
                                    {
                                        if (dsSize.Tables.Count > 0)
                                        {
                                            if (dsSize.Tables[0].Rows.Count > 0)
                                            {
                                                string Valsize = dsSize.Tables[0].Rows[0]["ATTRIBUTE_DATATYPE"].ToString();
                                                sValue = Valchk(sValue, Valsize); // Created Valchk function newly for this 
                                            }
                                        }
                                    }
                                    //sValue = htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[2].ToString();
                                    if (sValue != "")
                                    {
                                        //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                        //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                        if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (rowFamAttrCnt == 0)
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                                }
                                                else
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");

                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (rowFamAttrCnt == 0)
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                                }
                                                else
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                            else
                            {

                                _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                                DataSet dsSize = CreateDataSet();
                                if (dsSize.Tables[0].Rows.Count > 0)
                                {
                                    if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString() != null && oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString() != "")
                                    {
                                        foreach (DataRow dr in dsSize.Tables[0].Rows)
                                        {
                                            if (dr.ItemArray[0].ToString().StartsWith("Num"))
                                            {
                                                string tempStr = string.Empty;
                                                tempStr = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString();
                                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt]["NUMERIC_VALUE"] = tempStr;
                                            }

                                        }
                                    }
                                }
                                if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5] != DBNull.Value)
                                {
                                    sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString();
                                    //if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000" && sValue != "Null")
                                    if (Convert.ToString(sValue) != string.Empty && sValue != _emptyCondition)
                                    {
                                        if (dsSize != null)
                                        {
                                            if (dsSize.Tables.Count > 0)
                                            {
                                                if (dsSize.Tables[0].Rows.Count > 0)
                                                {
                                                    string Valsize = dsSize.Tables[0].Rows[0]["ATTRIBUTE_DATATYPE"].ToString();
                                                    sValue = Valchk(sValue, Valsize);
                                                }
                                            }
                                        }
                                        // sValue = htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[2].ToString();
                                        if (Isnumber(sValue))
                                        {
                                            //sValue = DecPrecisionFill(sValue, numberdecimel);
                                            sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString()), sValue.Trim()) : sValue;
                                        }
                                    }
                                }
                                else
                                    sValue = "Null";
                                //if (sValue != "")
                                {
                                    if (Convert.ToString(sValue) != string.Empty && sValue != _emptyCondition && sValue != "Null")
                                    {
                                        //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                        //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                        if (_headeroptions == "All")
                                        {
                                            if (Style == string.Empty || Style == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                        }
                                        else
                                        {
                                            if (rowFamAttrCnt == 0)
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000" || _emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "")
                                        {
                                            if (_replaceText != "")
                                            {
                                                _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                            }
                                            else
                                            {
                                                _replaceText = "";
                                            }
                                            if (_replaceText != "")
                                            {
                                                //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                                //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                if (_fornumeric == "1")
                                                {
                                                    if (Isnumber(_replaceText.Replace(",", "")))
                                                    {
                                                        if (_headeroptions == "All")
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (rowFamAttrCnt == 0)
                                                            {
                                                                if (Style == string.Empty || Style == "")
                                                                    //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                            }
                                                            else
                                                            {
                                                                if (Style == string.Empty || Style == "")
                                                                    //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (Style == string.Empty || Style == "")
                                                            //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                        else
                                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                    }
                                                }
                                                else
                                                {
                                                    if (_headeroptions == "All")
                                                    {
                                                        if (Style == string.Empty || Style == "")
                                                            //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                        else
                                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                    }
                                                    else
                                                    {
                                                        if (rowFamAttrCnt == 0)
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                            //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                            if (_headeroptions == "All")
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (rowFamAttrCnt == 0)
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                                }
                                                else
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FSA" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    #region Working Part

                    #endregion
                    #region Ulla delivery

                    #endregion

                    strBuildXMLOutput.Append("</specs_type>\n");
                    //Start Family Image 

                    DateTime CurrTime = DateTime.Now;
                    strBuildXMLOutput.Append("<image_type COTYPE=\"\" MSID=\"IDCIT007\" TBGUID=\"IT" + RandomClass.Next() + "\">\n");
                    FPath = "href=\"file://";
                    for (rowFamImgCnt = 0; rowFamImgCnt < oXMLDS_FamilyImages_.Tables[0].Rows.Count; rowFamImgCnt++)
                    {
                        ImgFile = "";
                        AttrName = "";
                        ImgFile = oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[2].ToString();
                        _xmlgen.UserImagePath = _xmlgen.UserImagePath.Replace("&", "&amp;");
                        ImgFile = ImgFile.Replace("\\", "/");
                        ImageName = _xmlgen.UserImagePath + ImgFile;
                        ImageName = ImageName.Replace("/", "\\");
                        ImgFile = FPath + ImageName + "\"";

                        AttrName = oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[0].ToString().Trim();
                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + " attribute_id=\"" + oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[1].ToString().Trim() + "\"" + "  IMAGE_FILE=\"" + oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[2].ToString().Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\" COTYPE=\"IMAGE\" MSID=\"ID" + FamilyId + "FIT" + oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[1].ToString().Trim() + "\" TBGUID=\"IV" + RandomClass.Next() + "\"></" + AttrName.ToLower() + ">\n");
                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "_name attribute_id=\"" + oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[1].ToString().Trim() + "\"" + "  COTYPE=\"TEXT\" MSID=\"ID" + FamilyId + "FIF" + oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[1].ToString().Trim() + "\" TBGUID=\"IN" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "_name\"><![CDATA[" + oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[4].ToString().Trim() + "]]></" + AttrName.ToLower() + "_name>\n");
                    }
                    strBuildXMLOutput.Append("</image_type>\n");
                    //End Family Image

                    //Start Supplier Info

                    _SQLString = "SELECT supplier_name,url,replace(replace(logo_image_file,\'\\',\'/\'),\'&\',\'&amp;\') as image_file,logo_image_type " +
                            "FROM tb_supplier WHERE supplier_name in (SELECT DISTINCT p.string_value FROM tb_prod_specs p,tb_prod_family l " +
                            "WHERE p.product_id=l.product_id AND l.PUBLISH2PRINT=1 AND l.family_id=" + FamilyId + " and p.string_value <> '' and FLAG_RECYCLE='A')";
                    oDSSupp = CreateDataSet();

                    strBuildXMLOutput.Append("<vendor_info COTYPE=\"\" MSID=\"ID001" + FamilyId + "VI\" TBGUID=\"VI" + RandomClass.Next() + "\">\n");
                    for (rowSupp = 0; rowSupp < oDSSupp.Tables[0].Rows.Count; rowSupp++)
                    {
                        strBuildXMLOutput.Append("<vendor COTYPE=\"\" MSID=\"ID001" + FamilyId + "VCT\" TBGUID=\"V" + RandomClass.Next() + "\">\n");
                        strBuildXMLOutput.Append("<supplier_name  COTYPE=\"TEXT\" MSID=\"ID001" + FamilyId + "SN\" TBGUID=\"SN" + RandomClass.Next() + "\" aid:pstyle=\"supplier_name\"><![CDATA[" + oDSSupp.Tables[0].Rows[rowSupp].ItemArray[0].ToString().Trim() + "]]></supplier_name>\n");
                        strBuildXMLOutput.Append("<url  COTYPE=\"TEXT\" MSID=\"ID001" + FamilyId + "URL\" TBGUID=\"SU" + RandomClass.Next() + "\" aid:pstyle=\"url\"><![CDATA[" + oDSSupp.Tables[0].Rows[rowSupp].ItemArray[1].ToString().Trim() + "]]></url>\n");
                        SuppImg = "";
                        SuppImg = oDSSupp.Tables[0].Rows[rowSupp].ItemArray[2].ToString().Trim();
                        _xmlgen.UserImagePath = _xmlgen.UserImagePath.Replace("&", "&amp;");
                        SuppImg = SuppImg.Replace("\\", "/");
                        SuppImgName = _xmlgen.UserImagePath + SuppImg;
                        SuppImgName = SuppImgName.Replace("/", "\\");
                        SuppImg = FPath + SuppImgName + "\"";
                        strBuildXMLOutput.Append("<logo_image_file" + " " + " IMAGE_FILE=\"" + oDSSupp.Tables[0].Rows[rowSupp].ItemArray[2].ToString().Trim().Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\" COTYPE=\"IMAGE\" MSID=\"ID001" + FamilyId + "SL\" TBGUID=\"SL" + RandomClass.Next() + "\"></logo_image_file>\n");
                        strBuildXMLOutput.Append("<logo_image_type  COTYPE=\"TEXT\" MSID=\"ID001" + FamilyId + "SLI\" TBGUID=\"SLI" + RandomClass.Next() + "\" aid:pstyle=\"logo_image_type\"><![CDATA[" + oDSSupp.Tables[0].Rows[rowSupp].ItemArray[3].ToString().Trim() + "]]></logo_image_type>\n");
                        strBuildXMLOutput.Append("</vendor>\n");
                    }
                    strBuildXMLOutput.Append("</vendor_info>\n");
                    oDSSupp.Dispose();
                    oDSSupp = null;
                    ////End Supplier Info		

                    advtable = String.Empty;
                    //Advance Table
                    _SQLString = "SELECT FAMILY_TABLE_STRUCTURE  FROM TB_FAMILY_TABLE_STRUCTURE WHERE family_id=" + FamilyId + " AND CATALOG_ID=" + _catalogId + " AND IS_DEFAULT=1";
                    DataSet oDSATable = CreateDataSet();
                    if (oDSATable.Tables[0].Rows.Count > 0 && oDSATable != null)
                    {
                        if (oDSATable.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            advtable = oDSATable.Tables[0].Rows[0].ItemArray[0].ToString();
                        }
                    }
                    //Start Products
                    ///pivot table
                    XmlDocument xmlDOc = new XmlDocument();
                    //////// PivotTableGenerator.PivotTableGenerator OPivotGenerator = new PivotTableGenerator.PivotTableGenerator(FamilyId.ToString(), CatalogId.ToString());
                    ////// //TableGenerator.TableGenerator oTableGenerator = new TradingBell.CatalogStudio.TableGenerator.TableGenerator(FamilyId.ToString(), CatalogId.ToString());
                    ////////oTableGenerator.ApplicationStartupPath = Application.StartupPath;
                    if (advtable != string.Empty && advtable != null)
                    {
                        xmlDOc.LoadXml(advtable);
                        XmlNode rootNode = xmlDOc.DocumentElement;
                        XmlNodeList xmlNodeList;
                        xmlNodeList = rootNode.ChildNodes;
                        if (rootNode.Attributes["TableType"] != null)
                        {
                            if (rootNode.Attributes["TableType"].Value == "Pivot")
                            {
                                TMode = rootNode.Attributes["TableType"].Value;

                            }
                            if (rootNode.Attributes["TableType"].Value == "SuperTable")
                            {
                                TMode = rootNode.Attributes["TableType"].Value;

                            }
                            else if (rootNode.Attributes["TableType"].Value == "Horizontal")
                            {
                                TMode = rootNode.Attributes["TableType"].Value;

                            }
                            else if (rootNode.Attributes["TableType"].Value == "Grouped")
                            {
                                TMode = rootNode.Attributes["TableType"].Value;

                            }
                            else if (rootNode.Attributes["TableType"].Value == "Vertical")
                            {
                                TMode = rootNode.Attributes["TableType"].Value;

                            }
                        }
                        else
                        {
                            if (advtable.Contains("TableType=\"Pivot\""))
                            {
                                TMode = "Pivot";

                            }
                            else if (advtable.Contains("TableType=\"SuperTable\""))
                            {
                                TMode = "SuperTable";

                            }
                            else if (advtable.Contains("TableType=\"Horizontal\""))
                            {
                                TMode = "Horizontal";

                            }
                            else if (advtable.Contains("TableType=\"Grouped\""))
                            {
                                TMode = "Grouped";

                            }
                            else if (advtable.Contains("TableType=\"Vertical\""))
                            {
                                TMode = "Vertical";

                            }
                        }
                    }
                    ////}
                    if (advtable != string.Empty && advtable != null && _xmlgen.PageName == "INCAT" && TMode == "Grouped")
                    {
                        string xml = string.Empty;
                        xml = XMLStringOutput(Convert.ToInt32(FamilyId));

                    }

                    else if (advtable != string.Empty && advtable != null && _xmlgen.PageName == "INCAT" && TMode == "Pivot")
                    {
                        ////////string XmlForPivot = OPivotGenerator.GeneratePivotXML();
                        ////////strBuildXMLOutput.Append(XmlForPivot);
                    }
                    else if (advtable != string.Empty && advtable != null && _xmlgen.PageName == "INCAT" && TMode == "SuperTable")
                    {
                        SuperTable objSuperTable = new SuperTable();

                        string XmlForSuperTable = objSuperTable.GenerateTableXml(FamilyId, _catalogId, categoryId, Attr_List, User.Identity.Name);
                        strBuildXMLOutput.Append(XmlForSuperTable);
                    }

                    else
                    {
                        if (_xmlgen.PageName == "INCAT")
                        {
                            //strBuildXMLOutput.Append("<products TBGUID=\"P" + RandomClass.Next() + "\">");
                        }
                        else
                        {
                            strBuildXMLOutput.Append("<products>\n");
                        }
                        ProductData = GetProductDetails(FamilyId, _catalogId);
                        //if (ProductData.ToString() != "Null")
                        //    strBuildXMLOutput.Append("</products>\n");
                    }
                    //End Products
                }
                else
                {
                    string substrBuildXMLOutput = strBuildXMLOutput.ToString();
                    strBuildXMLOutput = new StringBuilder();
                    strBuildXMLOutput.Append(substrBuildXMLOutput.Substring(0, substrBuildXMLOutput.LastIndexOf("<product_family")));
                }
                return strBuildXMLOutput.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetProductDetails(string FamilyId, int CatalogId)
        {
            //  ServiceProvider.ProductValidationServices Oservices = new TradingBell.CatalogStudio.ServiceProvider.ProductValidationServices();
            // Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");
            DataSet oDTProductTable = new DataSet();


            try
            {

                //Getting Product Table Structure
                //SqlProdTable = "SELECT FAMILY_TABLE_STRUCTURE,display_table_header from tb_FAMILY_TABLE_STRUCTURE where family_id=" + FamilyId+" AND CATALOG_ID="+CatalogId+" AND IS_DEFAULT=1";
                //oDSProdTable = GetDataset(SqlProdTable);
                //if (oDSProdTable.Tables[0].Rows.Count > 0)
                //{
                //    XMLStr = oDSProdTable.Tables[0].Rows[0].ItemArray[0].ToString().Trim();
                //    if (oDSProdTable.Tables[0].Rows[0].ItemArray[1].ToString() != null && oDSProdTable.Tables[0].Rows[0].ItemArray[1].ToString() != " " && oDSProdTable.Tables[0].Rows[0].ItemArray[1].ToString().Length != 0)
                //    {
                //        TableHeader = Convert.ToInt16(oDSProdTable.Tables[0].Rows[0].ItemArray[1]);

                //    }
                //}
                //oDSProdTable.Dispose();
                //oDSProdTable = null;
                TableHeader = 1;
                if (XMLStr != null && XMLStr != " " && XMLStr.Length != 0)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(XMLStr);
                    XmlNode rootNode = doc.DocumentElement;
                    if (rootNode.Attributes["TableType"].Value != null)
                    {
                        TableType = rootNode.Attributes["TableType"].Value;

                    }
                }
                else
                {
                    TableType = "Horizontal";

                }

                ///product filter
                XmlDocument xmlDOc = new XmlDocument();
                StringBuilder DynamicSQl = new StringBuilder();
                _SQLString = " SELECT PRODUCT_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + _catalogId + " and FLAG_RECYCLE='A'";
                oDsProdFilter = CreateDataSet();

                string sProdFilter = string.Empty;
                sProdFilter = oDsProdFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                if (sProdFilter != string.Empty)
                {

                    xmlDOc.LoadXml(sProdFilter);
                    XmlNode rNode = xmlDOc.DocumentElement;

                    if (rNode.ChildNodes.Count > 0)
                    {
                        for (int i = 0; i < rNode.ChildNodes.Count; i++)
                        {
                            StringBuilder SQLstring = new StringBuilder();
                            XmlNode TableDataSetNode = rNode.ChildNodes[i];
                            if (TableDataSetNode.HasChildNodes)
                            {
                                string StrVal = TableDataSetNode.ChildNodes[3].InnerText.Trim().Replace("'", "''");
                                DataSet oDsattrtype = new DataSet();
                                // StringBuilder DynamicattrSQl = new StringBuilder();
                                _SQLString = " SELECT attribute_datatype FROM TB_attribute WHERE  attribute_ID = " + TableDataSetNode.ChildNodes[0].InnerText + " and FLAG_RECYCLE='A'";
                                oDsattrtype = CreateDataSet();
                                if (oDsattrtype.Tables[0].Rows[0][0].ToString().ToUpper().StartsWith("N") == false)
                                {
                                    //SQLstring.Append("SELECT PRODUCT_ID FROM TB_PROD_SPECS WHERE Numeric_VALUE  " + TableDataSetNode.ChildNodes[2].InnerText + " " + StrVal + "  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "");
                                    SQLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + _catalogId + ") WHERE (STRING_VALUE " + TableDataSetNode.ChildNodes[2].InnerText + " '" + StrVal + "' AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                }
                                else
                                {
                                    //SQLstring.Append("SELECT PRODUCT_ID FROM TB_PROD_SPECS WHERE STRING_VALUE  " + TableDataSetNode.ChildNodes[2].InnerText + " N'" + StrVal + "'  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "");
                                    SQLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + _catalogId + ") WHERE  (NUMERIC_VALUE " + TableDataSetNode.ChildNodes[2].InnerText + " '" + StrVal + "' AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");

                                }
                            }
                            if (TableDataSetNode.ChildNodes[4].InnerText == "None")
                            {
                                DynamicSQl.Append(SQLstring);
                                break;
                            }
                            DynamicSQl.Append(SQLstring);
                            if (TableDataSetNode.ChildNodes[4].InnerText == "AND")
                            {

                                DynamicSQl.Append(" INTERSECT \n");
                            }
                            if (TableDataSetNode.ChildNodes[4].InnerText == "OR")
                            {
                                DynamicSQl.Append(" UNION \n");
                            }

                            //if (i > 0)
                            //{
                            //    DynamicSQl.Append(" union ");
                            //}
                        }
                    }

                }
                DataSet DSfilter = new DataSet();
                _SQLString = DynamicSQl.ToString();
                if (DynamicSQl.ToString() != string.Empty)
                {
                    DSfilter = CreateDataSet();
                }


                ChkAttributeExistinFamily();

                int[] attrList = new int[0];
                string con = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                //TradingBell.CatalogX.CatalogXfunction ofrm = new TradingBell.CatalogX.CatalogXfunction();
                DataSet oDTProductTableTemp = new DataSet();
                oDTProductTableTemp = CatalogProductPreviewX(Convert.ToInt32(CatalogId), Convert.ToInt32(FamilyId), attrList, con);

                DataRow[] tempDsRows = oDTProductTableTemp.Tables["ProductTable"].Select("Family_id=" + Convert.ToInt32(FamilyId));

                DataSet DSGlobal = oDTProductTableTemp.Clone();
                foreach (DataRow row in tempDsRows)
                    DSGlobal.Tables["ProductTable"].ImportRow(row);
                DataRow[] tempsDsRows = oDTProductTableTemp.Tables["ExtendedProperties"].Select("Family_id=" + Convert.ToInt32(FamilyId));
                foreach (DataRow row in tempsDsRows)
                    DSGlobal.Tables["ExtendedProperties"].ImportRow(row);
                oDTProductTableTemp = DSGlobal;

                DataColumn DCSort = new DataColumn("Sort1", typeof(System.Int32));
                oDTProductTableTemp.Tables["ProductTable"].Columns.Add(DCSort);
                foreach (DataRow dc in oDTProductTableTemp.Tables["ProductTable"].Rows)
                {
                    dc["Sort1"] = Convert.ToInt32(dc["Sort"].ToString());
                }
                DataRow[] temROWS = oDTProductTableTemp.Tables["ProductTable"].Select("", "Sort1");
                oDTProductTable = oDTProductTableTemp.Copy();
                oDTProductTable.Tables["ProductTable"].Rows.Clear();

                foreach (DataRow dr in temROWS)
                {
                    oDTProductTable.Tables["ProductTable"].ImportRow(dr);
                }
                oDTProductTable.Tables["ProductTable"].Columns.Remove("Sort1");
                oRefproductTable = oDTProductTable.Copy();
                //oRefproductTable = ofrm.CatalogProductPreviewX(Convert.ToInt32(CatalogId), Convert.ToInt32(FamilyId), attrList, con);
                if (oDTProductTable.Tables[0].Rows.Count == 0)
                {
                    //strBuildXMLOutput.Remove(strBuildXMLOutput.ToString().LastIndexOf("<products TBGUID"), strBuildXMLOutput.ToString().Length - strBuildXMLOutput.ToString().LastIndexOf("<products TBGUID"));    
                    return "Null";
                }
                //filter 

                if (DSfilter != null && DSfilter.Tables.Count > 0)
                {
                    if (DynamicSQl.ToString() != string.Empty && DSfilter.Tables[0].Rows.Count >= 0)
                    {
                        DataSet temp = oDTProductTable;
                        DataSet Dspreviewaltered = new DataSet();
                        DataTable t1 = new DataTable();
                        Dspreviewaltered.Tables.Add(oDTProductTable.Tables[0].Clone());

                        foreach (DataRow DR in oDTProductTable.Tables[0].Rows)
                        {
                            DataRow[] DrfilteredRows = DSfilter.Tables[0].Select("PRODUCT_ID = " + DR["PRODUCT_ID"]);
                            if (DrfilteredRows != null && DrfilteredRows.Length != 0)
                            {
                                //DataRow datarw = t1.NewRow();
                                Dspreviewaltered.Tables[0].ImportRow(DR);
                            }

                        }
                        oDTProductTable = new DataSet();
                        oDTProductTable.Tables.Add(Dspreviewaltered.Tables[0].Clone());
                        foreach (DataRow DR in Dspreviewaltered.Tables[0].Rows)
                        {
                            oDTProductTable.Tables[0].ImportRow(DR);
                        }
                        DataTable dsf = temp.Tables[1].Clone();
                        foreach (DataRow Dr in temp.Tables[1].Rows)
                        {
                            DataRow DRTEMP = dsf.NewRow();
                            dsf.ImportRow(Dr);
                        }
                        oDTProductTable.Tables.Add(dsf);
                    }
                    else
                    {

                    }
                }
                //end filter 

                //for Publish
                _SQLString = " SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID IN ( SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_FAMILY_ATTR_LIST WHERE FAMILY_ID = " + FamilyId + "  )  AND PUBLISH2PRINT =1 and FLAG_RECYCLE='A'";
                DataSet DSpublish = new DataSet();
                DSpublish = CreateDataSet();

                if (DSpublish.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow Dr in DSpublish.Tables[0].Rows)
                    {
                        if (oDTProductTable.Tables[0].Columns.Contains(Dr[0].ToString()))
                        {
                            oDTProductTable.Tables[0].Columns[Dr[0].ToString()].Caption = "`" + oDTProductTable.Tables[0].Columns[Dr[0].ToString()].Caption;
                        }
                    }
                    for (int i = 0; i < oDTProductTable.Tables[0].Columns.Count; i++)
                    {
                        if (oDTProductTable.Tables[0].Columns[i].Caption.Contains("`") == false)
                        {
                            if (oDTProductTable.Tables[0].Columns[i].Caption != "CATALOG_ID" && oDTProductTable.Tables[0].Columns[i].Caption != "FAMILY_ID" && oDTProductTable.Tables[0].Columns[i].Caption != "PRODUCT_ID" && oDTProductTable.Tables[0].Columns[i].Caption != "Publish" && oDTProductTable.Tables[0].Columns[i].Caption != "Sort")
                            {
                                try
                                {
                                    oDTProductTable.Tables[0].Columns.RemoveAt(i);
                                    oDTProductTable.Tables[1].Columns.RemoveAt(i);
                                }
                                catch (Exception) { i = 0; continue; }
                                i = 0;
                            }

                        }

                    }
                    for (int i = 0; i < oDTProductTable.Tables[0].Columns.Count; i++)
                    {
                        if (oDTProductTable.Tables[0].Columns[i].Caption.Contains("`") == true)
                        {
                            if (oDTProductTable.Tables[0].Columns[i].Caption != "CATALOG_ID" && oDTProductTable.Tables[0].Columns[i].Caption != "FAMILY_ID" && oDTProductTable.Tables[0].Columns[i].Caption != "PRODUCT_ID" && oDTProductTable.Tables[0].Columns[i].Caption != "Publish" && oDTProductTable.Tables[0].Columns[i].Caption != "Sort")
                            {
                                oDTProductTable.Tables[0].Columns[i].Caption = oDTProductTable.Tables[0].Columns[i].Caption.Remove(0, 1);
                            }

                        }

                    }

                }

                //end for publish

                {
                    if (oDTProductTable.Tables[0].Columns.Count > 0)
                    {
                        oDTProductTable.EnforceConstraints = false;
                        oDTProductTable.Tables[0].PrimaryKey = null;
                        oDTProductTable.Tables[1].PrimaryKey = null;

                        oDTProductTable.Tables[0].Columns.Remove("FAMILY_ID");
                        oDTProductTable.Tables[0].Columns.Remove("PRODUCT_ID");
                        oDTProductTable.Tables[0].Columns.Remove("Sort");
                        oDTProductTable.Tables[0].Columns.Remove("CATALOG_ID");
                        oDTProductTable.Tables[0].Columns.Remove("Publish");

                    }
                    if (oRefproductTable.Tables[1].Columns.Count > 0)
                    {
                        oDTProductTable.Tables[1].Columns.Remove("CATALOG_ID");
                        oDTProductTable.Tables[1].Columns.Remove("FAMILY_ID");
                        oDTProductTable.Tables[1].Columns.Remove("PRODUCT_ID");
                        oDTProductTable.Tables[1].Columns.Remove("Sort");
                        oDTProductTable.Tables[1].Columns.Remove("Publish");
                    }
                }
                totalRowCnt = oDTProductTable.Tables[0].Rows.Count + 1;
                totColCnt = oDTProductTable.Tables[0].Columns.Count;
                totXMLColCnt = oDTProductTable.Tables[0].Columns.Count;

                if (oDTProductTable.Tables[0].Rows.Count > 0)
                {
                    strBuildXMLOutput.Append("<products TBGUID=\"P" + RandomClass.Next() + "\">");
                    if (TableType == "Horizontal")
                    {
                        if (_xmlgen.PageName == "INCAT")
                        {

                            if (TableHeader == 0)
                            {
                                totalRowCnt = totalRowCnt - 1;
                            }

                            strBuildXMLOutput.Append("<TABLE  Format=\"Horizontal\" nrows=\"" + totalRowCnt + "\" ncols=\"" + totXMLColCnt + "\" TBGUID=\"T" + RandomClass.Next() + "\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" aid5:tablestyle=\"TABLE\" aid:table=\"table\" aid:trows=\"" + totalRowCnt + "\" aid:tcols=\"" + totXMLColCnt + "\" >");
                        }
                        //header
                        for (int j = 0; j < oDTProductTable.Tables[1].Columns.Count; j++)
                        {
                            //CellName = oDTProductTable.Tables[1].Columns[j].ColumnName.ToString().Trim();
                            CellName = oDTProductTable.Tables[1].Rows[2].ItemArray[j].ToString();
                            if (CellName != null && CellName != " " && CellName.Length != 0)
                            {
                                // CellName = oDTProductTable.Tables[1].Columns[j].ColumnName.ToString().Trim().Replace(" ", "_");
                                CellName = oDTProductTable.Tables[1].Rows[2].ItemArray[j].ToString().Trim().Replace(" ", "_");
                            }
                            else
                            {
                                CellName = "Cell";
                            }
                            CellName = CellName + "_Header";
                            if (_xmlgen.PageName == "INCAT" && TableHeader == 1)
                            {
                                strBuildXMLOutput.Append("<" + CellName + " aid5:cellstyle=\"" + CellName + "\" aid:table=\"cell\" aid:theader=\"\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + oDTProductTable.Tables[0].Columns[j].ColumnName.ToString().Trim() + "]]></" + CellName + ">");
                            }
                        }
                        //header end 

                        for (dRow = 0; dRow < oDTProductTable.Tables[0].Rows.Count; dRow++)
                        {
                            for (dCol = 0; dCol < oDTProductTable.Tables[0].Columns.Count; dCol++)
                            {
                                CellName = oDTProductTable.Tables[1].Rows[2].ItemArray[dCol].ToString().Trim();
                                string ValueForTag = string.Empty;
                                string ImageType = oDTProductTable.Tables[1].Rows[1].ItemArray[dCol].ToString().Trim();
                                string AttrID = oDTProductTable.Tables[1].Rows[0].ItemArray[dCol].ToString();
                                _SQLString = "SELECT ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + AttrID + " and FLAG_RECYCLE='A'";
                                DataSet DSS = CreateDataSet();
                                if (CellName != null && CellName != " " && CellName.Length != 0)
                                {
                                    CellName = oDTProductTable.Tables[1].Rows[2].ItemArray[dCol].ToString().Trim().Replace(" ", "_");
                                }
                                else
                                {
                                    CellName = "Cell";
                                }
                                ExtractCurrenyFormat(Convert.ToInt16(oDTProductTable.Tables[1].Rows[0].ItemArray[dCol].ToString()));

                                if (ImageType != "3")
                                {
                                    string nValue = string.Empty;
                                    //if (ImageType == "4")
                                    {
                                        if (oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString().Length > 0)
                                            nValue = ApplyStyleFormat(Convert.ToInt32(oDTProductTable.Tables[1].Rows[0].ItemArray[dCol].ToString().Trim()), oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString());
                                    }
                                    if ((Headeroptions == "All") || (Headeroptions != "All" && dRow == 0))
                                    {
                                        if ((EmptyCondition == "Null" || EmptyCondition == "Empty" || EmptyCondition == null) && (oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString().Trim() == string.Empty))
                                        {
                                            ValueForTag = ReplaceText;
                                        }
                                        else if ((oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString()) == (EmptyCondition))
                                        {
                                            ValueForTag = ReplaceText;
                                        }
                                        else
                                        {
                                            if (Isnumber(oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString()) == true && DSS.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num") == true)
                                            {
                                                if (nValue != "0")
                                                {
                                                    ValueForTag = Prefix + "" + nValue + "" + Suffix;
                                                }
                                                else
                                                {
                                                    ValueForTag = Prefix + "" + oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString() + "" + Suffix;
                                                }
                                            }
                                            else
                                            {
                                                ValueForTag = Prefix + "" + oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString() + "" + Suffix;
                                            }

                                        }

                                    }
                                    else
                                    {
                                        if (Isnumber(oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString()) == true && DSS.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num") == true)
                                        {
                                            if (nValue != "0")
                                            {
                                                ValueForTag = nValue;
                                            }
                                            else
                                            {
                                                ValueForTag = oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ValueForTag = oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString();
                                        }

                                    }
                                    _SQLString = " SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID=" + oDTProductTable.Tables[1].Rows[0].ItemArray[dCol].ToString() + " and FLAG_RECYCLE='A'";
                                    DataSet DSattrType = new DataSet();
                                    DSattrType = CreateDataSet();

                                    if (DSattrType.Tables[0].Rows[0].ItemArray[0].ToString() == "Hyperlink")
                                    {
                                        strBuildXMLOutput.Append("<" + CellName + "  TBGUID=\"TPS" + RandomClass.Next() + "\" product_id = \"" + oRefproductTable.Tables[0].Rows[dRow]["PRODUCT_ID"].ToString() + "\" Attribute_type = \"" + oDTProductTable.Tables[1].Rows[1].ItemArray[dCol].ToString() + "\" Hyperlink = \"" + ValueForTag + "\" Attribute_id = \"" + oDTProductTable.Tables[1].Rows[0].ItemArray[dCol].ToString() + "\" aid5:cellstyle=\"" + CellName + "\" aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\">");
                                    }
                                    else
                                    {
                                        strBuildXMLOutput.Append("<" + CellName + " TBGUID=\"TPS" + RandomClass.Next() + "\" product_id = \"" + oRefproductTable.Tables[0].Rows[dRow]["PRODUCT_ID"].ToString() + "\" Attribute_type = \"" + oDTProductTable.Tables[1].Rows[1].ItemArray[dCol].ToString() + "\" Attribute_id = \"" + oDTProductTable.Tables[1].Rows[0].ItemArray[dCol].ToString() + "\" aid5:cellstyle=\"" + CellName + "\" aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\">");
                                    }
                                    strBuildXMLOutput.Append("<![CDATA[" + ValueForTag.Trim() + "]]>");
                                    strBuildXMLOutput.Append("</" + CellName + ">");

                                }
                                else if (ImageType == "3")
                                {
                                    //  strBuildXMLOutput.Append("<" + CellName + " aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\"   attribute_id=\"" + oDTProductTable.Tables[1].Rows[0].ItemArray[dCol].ToString() + "\"" + "  IMAGE_FILE=\"" + oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString().Trim()  + "\" COTYPE=\"IMAGE\" TBGUID=\"CI" + RandomClass.Next() + "\"></" + CellName + ">\n");
                                    //strBuildXMLOutput.Append("<" + CellName + " aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\"  Attribute_id=\"" + oDTProductTable.Tables[1].Rows[0].ItemArray[dCol].ToString() + "TBGUID=\"CI" + RandomClass.Next() + "\"> " < "cell_image>" + "TBGUID=\"CI" + RandomClass.Next() + "\" COTYPE=\"IMAGE\"" + "IMAGE_FILE=\"" + oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString() + "<cell_image" > " </" + CellName + ">\n");
                                    strBuildXMLOutput.Append("<" + CellName + "  aid5:cellstyle=\"" + CellName + "\" aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\" attribute_id=\"" + oDTProductTable.Tables[1].Rows[0].ItemArray[dCol].ToString() + "\"" + "  IMAGE_FILE=\"" + oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString().Trim().Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\"  COTYPE=\"IMAGE\"  TBGUID=\"CI" + RandomClass.Next() + "\"><cell_image TBGUID=\"CI" + RandomClass.Next() + "\" COTYPE=\"IMAGE\"   IMAGE_FILE=\"" + oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString() + "\"></cell_image></" + CellName + ">\n");
                                }
                                //else
                                //{
                                //     strBuildXMLOutput.Append("<" + CellName + " aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\"  TBGUID=\"TPS" + RandomClass.Next() + "\" product_id = \"" + oRefproductTable.Tables[0].Rows[dRow].ItemArray[0].ToString() + "\" Attribute_type = \"" + oDTProductTable.Tables[1].Rows[1].ItemArray[dCol].ToString() + "\" Attribute_id = \"" + oDTProductTable.Tables[1].Rows[0].ItemArray[dCol].ToString() + "\" >");    
                                //    strBuildXMLOutput.Append("<![CDATA[" + oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString().Trim() + "]]>");
                                //    strBuildXMLOutput.Append("</" + CellName + ">");
                                //}
                            }
                        }
                        if (_xmlgen.PageName == "INCAT")
                        {
                            strBuildXMLOutput.Append("</TABLE>");
                        }
                    }

                    //Vertical Table Format
                    if (TableType == "Vertical")
                    {
                        string ValueForTag = string.Empty;
                        if (TableHeader == 0)
                        {
                            totalRowCnt = totalRowCnt - 1;
                        }

                        strBuildXMLOutput.Append("<TABLE Format=\"Vertical\" nrows=\"" + totXMLColCnt + "\" ncols=\"" + totalRowCnt + "\" TBGUID=\"T" + RandomClass.Next() + "\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" aid5:tablestyle=\"TABLE\" aid:table=\"table\" aid:trows=\"" + totXMLColCnt + "\" aid:tcols=\"" + totalRowCnt + "\" > ");

                        for (int Col = 0; Col < oDTProductTable.Tables[0].Columns.Count; Col++)
                        {
                            // CellName = oDTProductTable.Tables[0].Columns[Col].ColumnName.ToString().Trim();
                            CellName = oDTProductTable.Tables[1].Rows[2].ItemArray[Col].ToString();
                            if (CellName != null && CellName != " " && CellName.Length != 0)
                            {
                                CellName = oDTProductTable.Tables[1].Rows[2].ItemArray[Col].ToString().Trim().Replace(" ", "_");
                            }
                            else
                            {
                                CellName = "Cell";
                            }
                            CellName = CellName + "_Header";
                            if (_xmlgen.PageName == "INCAT" && TableHeader == 1)
                            {
                                strBuildXMLOutput.Append("<" + CellName + " aid5:cellstyle=\"" + CellName + "\" aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\" ><![CDATA[" + oDTProductTable.Tables[0].Columns[Col].ColumnName.ToString().Trim() + "]]></" + CellName + "> ");
                            }
                            for (rowPHCnt = 0; rowPHCnt < oDTProductTable.Tables[0].Rows.Count; rowPHCnt++)
                            {
                                CellName = string.Empty;
                                string ImageType = string.Empty;
                                CellName = oDTProductTable.Tables[1].Rows[2].ItemArray[Col].ToString().Trim();
                                ImageType = oDTProductTable.Tables[1].Rows[1].ItemArray[Col].ToString().Trim();
                                string AttrID = oDTProductTable.Tables[1].Rows[0].ItemArray[Col].ToString();
                                _SQLString = "SELECT ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + AttrID + " and FLAG_RECYCLE='A'";
                                DataSet DSS = CreateDataSet();

                                if (CellName != null && CellName != " " && CellName.Length != 0)
                                {
                                    CellName = oDTProductTable.Tables[1].Rows[2].ItemArray[Col].ToString().Trim().Replace(" ", "_");
                                }
                                else
                                {
                                    CellName = "Cell";
                                }
                                ExtractCurrenyFormat(Convert.ToInt16(oDTProductTable.Tables[1].Rows[0].ItemArray[Col].ToString()));

                                if (ImageType != "3")
                                {
                                    string nValue = string.Empty;
                                    //if (ImageType == "4")
                                    {
                                        if (oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString().Length > 0)
                                            nValue = ApplyStyleFormat(Convert.ToInt32(oDTProductTable.Tables[1].Rows[0].ItemArray[Col].ToString().Trim()), oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString());
                                    }
                                    if ((Headeroptions == "All") || (Headeroptions != "All" && Col == 0))
                                    {
                                        if ((EmptyCondition == "Null" || EmptyCondition == "Empty" || EmptyCondition == null) && (oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString().Trim() == string.Empty))
                                        {
                                            ValueForTag = ReplaceText;
                                        }
                                        else if ((oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString()) == (EmptyCondition))
                                        {
                                            ValueForTag = ReplaceText;
                                        }
                                        else
                                        {
                                            if (Isnumber(oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString()) == true && DSS.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num") == true)
                                            {
                                                if (nValue != "0")
                                                {
                                                    ValueForTag = Prefix + nValue + Suffix;
                                                }
                                                else
                                                {
                                                    ValueForTag = Prefix + oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString() + Suffix;
                                                }
                                            }
                                            else
                                            {
                                                ValueForTag = Prefix + oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString() + Suffix;
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (Isnumber(oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString()) == true && DSS.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num") == true)
                                        {
                                            if (nValue != "0")
                                            {
                                                ValueForTag = nValue;
                                            }
                                            else
                                            {
                                                ValueForTag = oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString();
                                            }
                                        }
                                        else
                                        {
                                            ValueForTag = oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString();
                                        }

                                    }
                                    _SQLString = " SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID=" + oDTProductTable.Tables[1].Rows[0].ItemArray[Col].ToString() + " and FLAG_RECYCLE='A'";
                                    DataSet DSattrType = new DataSet();
                                    DSattrType = CreateDataSet();

                                    if (DSattrType.Tables[0].Rows[0].ItemArray[0].ToString() == "Hyperlink")
                                    {
                                        strBuildXMLOutput.Append("<" + CellName + " TBGUID=\"TPS" + RandomClass.Next() + "\"  product_id = \"" + oRefproductTable.Tables[0].Rows[rowPHCnt]["PRODUCT_ID"].ToString() + "\" Attribute_type = \"" + oDTProductTable.Tables[1].Rows[1].ItemArray[Col].ToString() + "\" Hyperlink = \"" + ValueForTag + "\" Attribute_id = \"" + oDTProductTable.Tables[1].Rows[0].ItemArray[Col].ToString() + "\" aid5:cellstyle=\"" + CellName + "\" aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + ValueForTag + "]]></" + CellName + "> ");
                                    }
                                    else
                                    {

                                        strBuildXMLOutput.Append("<" + CellName + "   TBGUID=\"TPS" + RandomClass.Next() + "\"  product_id = \"" + oRefproductTable.Tables[0].Rows[rowPHCnt]["PRODUCT_ID"].ToString() + "\" Attribute_type = \"" + oDTProductTable.Tables[1].Rows[1].ItemArray[Col].ToString() + "\" Attribute_id = \"" + oDTProductTable.Tables[1].Rows[0].ItemArray[Col].ToString() + "\" aid5:cellstyle=\"" + CellName + "\" aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + ValueForTag + "]]></" + CellName + "> ");
                                    }

                                }
                                else if (ImageType == "3")
                                {
                                    strBuildXMLOutput.Append("<" + CellName + " aid5:cellstyle=\"" + CellName + "\" aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\" attribute_id=\"" + oDTProductTable.Tables[1].Rows[0].ItemArray[Col].ToString() + "\"" + "  IMAGE_FILE=\"" + oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString().Trim().Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\" COTYPE=\"IMAGE\" TBGUID=\"CI" + RandomClass.Next() + "\"><cell_image  TBGUID=\"CI" + RandomClass.Next() + "\" COTYPE=\"IMAGE\" " + "IMAGE_FILE=\"" + oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString() + "\" ></cell_image></" + CellName + ">\n");
                                    //strBuildXMLOutput.Append("<" + CellName + "_image  TBGUID=\"CI" + RandomClass.Next() + "\" COTYPE=\"IMAGE\" " + "IMAGE_FILE=\"" + oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString() + "\"></" + CellName + "_image>\n");
                                }
                                //else
                                //{
                                //    strBuildXMLOutput.Append("<" + CellName + " aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\"  TBGUID=\"TPS" + RandomClass.Next() + "\"  product_id = \"" + oRefproductTable.Tables[0].Rows[rowPHCnt].ItemArray[0].ToString() + "\" Attribute_type = \"" + oDTProductTable.Tables[1].Rows[1].ItemArray[Col].ToString() + "\" Attribute_id = \"" + oDTProductTable.Tables[1].Rows[0].ItemArray[Col].ToString() + "\" ><![CDATA[" + oDTProductTable.Tables[0].Rows[rowPHCnt].ItemArray[Col].ToString() + "]]></" + CellName + "> ");
                                //}
                            }
                        }
                        strBuildXMLOutput.Append("</TABLE>");
                    }

                    strBuildXMLOutput.Append("</products>\n");
                }
                else
                {
                    return strBuildXMLOutput.ToString();
                }


            }//End Try
            catch (Exception)
            {

            }
            oDTProductTable.Dispose();
            oDTProductTable = null;

            return strBuildXMLOutput.ToString();
        }

        public string GetFamilyDetails(string FamilyId, string categoryId, string FamilyType, string AttList, bool idColumnFlag)
        {
            //Getting Family
            try
            {
                int cont = 0;
                if (FamilyType == "Sub")
                    _SQLString = "SELECT distinct f.family_name,c.category_name,f.foot_notes,SHORT_DESC,IMAGE_FILE,IMAGE_TYPE,IMAGE_NAME,IMAGE_NAME2,IMAGE_FILE2," +
                        " IMAGE_TYPE2,CUSTOM_NUM_FIELD1,CUSTOM_NUM_FIELD2,CUSTOM_NUM_FIELD3 ,CUSTOM_TEXT_FIELD1,CUSTOM_TEXT_FIELD2,CUSTOM_TEXT_FIELD3  " +
                       " FROM tb_family f, tb_category c WHERE C.CATEGORY_ID = " +
                       " (SELECT CATEGORY_ID FROM tb_project_section_details WHERE family_id =(SELECT FAMILY_ID FROM TB_SUBFAMILY WHERE SUBFAMILY_ID = " + FamilyId + ") and f.FLAG_RECYCLE='A'  and record_id =" + _xmlgen.RecordID + ")AND F.FAMILY_ID =  " + FamilyId + " ";
                else
                    _SQLString = "SELECT distinct f.family_name,c.category_name,f.foot_notes,SHORT_DESC,IMAGE_FILE,IMAGE_TYPE,IMAGE_NAME,IMAGE_NAME2,IMAGE_FILE2," +
                           " IMAGE_TYPE2,CUSTOM_NUM_FIELD1,CUSTOM_NUM_FIELD2,CUSTOM_NUM_FIELD3 ,CUSTOM_TEXT_FIELD1,CUSTOM_TEXT_FIELD2,CUSTOM_TEXT_FIELD3  " +
                          " FROM tb_family f, tb_category c WHERE C.CATEGORY_ID = " +
                          " (SELECT CATEGORY_ID FROM tb_project_section_details WHERE family_id =  " + FamilyId + "   and record_id =" + _xmlgen.RecordID + ") and f.FLAG_RECYCLE='A' AND F.FAMILY_ID =  " + FamilyId + " ";
                //SqlFam = "SELECT distinct f.family_name,c.category_name,f.foot_notes," +
                //" SHORT_DESC,IMAGE_FILE,IMAGE_TYPE,IMAGE_NAME,IMAGE_NAME2,IMAGE_FILE2,IMAGE_TYPE2,CUSTOM_NUM_FIELD1,CUSTOM_NUM_FIELD2,CUSTOM_NUM_FIELD3" +
                //" ,CUSTOM_TEXT_FIELD1,CUSTOM_TEXT_FIELD2,CUSTOM_TEXT_FIELD3 " +
                //" FROM tb_family f, tb_category c, tb_project_section_details psd	" +
                //    "WHERE f.family_id = " + FamilyId + " and psd.category_id=c.category_id and psd.record_id =" + _xmlgen.RecordID + "";

                oDSFam = CreateDataSet();

                //oXMLDS = new TradingBell.CatalogStudio.CoreDS.CatalogXML();
                //oSpecsadap = new TradingBell.CatalogStudio.CoreDS.CatalogXMLTableAdapters.FamilySpecsTableAdapter();
                //oSpecsadap.Fill(oXMLDS_FamilySpecs.Tables[0], CatalogId, Convert.ToInt32(FamilyId));

                //oFamilyimgAdap = new TradingBell.CatalogStudio.CoreDS.CatalogXMLTableAdapters.FamilyImages_TableAdapter();
                //oFamilyimgAdap.Fill(oXMLDS_FamilyImages_.Tables[0], CatalogId, Convert.ToInt32(FamilyId));
                _SQLString = "SELECT DISTINCT TA.ATTRIBUTE_ID, REPLACE(TA.ATTRIBUTE_NAME, ' ','_') ATTRIBUTE_NAME, TFS.STRING_VALUE, TA.ATTRIBUTE_TYPE,REPLACE(TA.STYLE_NAME, ' ','_') as STYLE_NAME,TFS.NUMERIC_VALUE,A.SORT_ORDER  " +
                           "    FROM TB_FAMILY_SPECS TFS LEFT JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID AND ATTRIBUTE_TYPE IN(7,11,12) AND PUBLISH2PRINT = 1 LEFT JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID = " + _catalogId + " JOIN TB_CATEGORY_FAMILY_ATTR_LIST A ON A.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID " +
                           " WHERE TFS.FAMILY_ID = " + FamilyId + " AND TA.ATTRIBUTE_ID IS NOT NULL AND A.CATALOG_ID=" + _catalogId + " AND A.FAMILY_ID =  " + FamilyId + " UNION SELECT DISTINCT TA.ATTRIBUTE_ID, REPLACE(TA.ATTRIBUTE_NAME, ' ','_') ATTRIBUTE_NAME, TFS.ATTRIBUTE_VALUE, TA.ATTRIBUTE_TYPE,REPLACE(TA.STYLE_NAME, ' ','_') as STYLE_NAME,NULL,A.SORT_ORDER  FROM TB_FAMILY_KEY TFS LEFT JOIN TB_ATTRIBUTE TA " +
                           " ON TA.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID AND ATTRIBUTE_TYPE IN(13) AND PUBLISH2PRINT = 1 LEFT JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID = " + _catalogId + " JOIN TB_CATEGORY_FAMILY_ATTR_LIST A ON A.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID WHERE TFS.FAMILY_ID = " + FamilyId + " AND TA.ATTRIBUTE_ID IS NOT NULL " +
                           " AND A.CATALOG_ID = TFS.CATALOG_ID AND A.CATALOG_ID=" + _catalogId + " AND A.FAMILY_ID = " + FamilyId + " and TA.FLAG_RECYCLE='A' ORDER BY TA.ATTRIBUTE_ID";

                oXMLDS_FamilySpecs = CreateDataSet();


                //oFamilyimgAdap = new TradingBell.CatalogStudio.CoreDS.CatalogXMLTableAdapters.FamilyImages_TableAdapter();
                //oFamilyimgAdap.Fill(oXMLDS_FamilyImages_.Tables[0], CatalogId, Convert.ToInt32(FamilyId));

                _SQLString = " SELECT TA.ATTRIBUTE_ID, REPLACE(TA.ATTRIBUTE_NAME, ' ', '_') AS ATTRIBUTE_NAME, TFS.STRING_VALUE, TA.ATTRIBUTE_TYPE, TFS.OBJECT_NAME, TFS.OBJECT_TYPE" +
                            " FROM TB_FAMILY_SPECS AS TFS LEFT OUTER JOIN TB_ATTRIBUTE AS TA ON TA.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID AND TA.ATTRIBUTE_TYPE = 9 AND TA.PUBLISH2PRINT = 1 LEFT OUTER JOIN " +
                            " TB_CATALOG_ATTRIBUTES AS TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID = " + _catalogId + " JOIN TB_CATEGORY_FAMILY_ATTR_LIST A ON A.ATTRIBUTE_ID = TFS.ATTRIBUTE_ID " +
                            " WHERE (TFS.FAMILY_ID = " + FamilyId + ") AND (TA.ATTRIBUTE_ID IS NOT NULL) AND A.CATALOG_ID=" + _catalogId + " and TA.FLAG_RECYCLE='A' AND A.FAMILY_ID = " + FamilyId + " ORDER BY TA.ATTRIBUTE_ID";
                oXMLDS_FamilyImages_ = CreateDataSet();

                for (rowFamCnt = 0; rowFamCnt < oDSFam.Tables[0].Rows.Count; rowFamCnt++)
                {
                    string catlevelEntry = "";
                    string parentcat = "0";
                    int val = 0;
                    //  string Sqlstrr = "";
                    if (FamilyType == "Main")
                    {
                        //_SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY from TB_CATEGORY WHERE CATEGORY_ID IN  (select distinct category_id from tb_project_section_details where family_id IN ( " + FamilyId + ") and record_id = " + _xmlgen.RecordID + ") and FLAG_RECYCLE='A'";
                        // ------------------------------- AFTER CUT PASTE FAMILY FROM ONE INDESIGN FILE TO ANOTHER---------------------------------------//
                        _SQLString = "SELECT DISTINCT FA.CATEGORY_ID,CA.PARENT_CATEGORY,CA.CATEGORY_NAME FROM TB_PROJECT_SECTION_DETAILS PSD " +
                                    " JOIN TB_PROJECT_SECTIONS PS ON PSD.RECORD_ID = PS.RECORD_ID " +
                                    " JOIN TB_FAMILY FA ON FA.FAMILY_ID = PSD.FAMILY_ID " +
                                    " JOIN TB_CATEGORY CA ON FA.CATEGORY_ID = CA.CATEGORY_ID " +
                                    " WHERE CA.FLAG_RECYCLE = 'A' AND FA.FLAG_RECYCLE = 'A'  AND FA.FAMILY_ID IN (" + FamilyId + ")  AND PSD.RECORD_ID = " + _xmlgen.RecordID + " ";
                    }
                    else if (FamilyType == "Sub")
                    {
                        _SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY from TB_CATEGORY WHERE CATEGORY_ID IN (select distinct category_id from tb_project_section_details where family_id IN ( SELECT FAMILY_ID FROM TB_SUBFAMILY WHERE SUBFAMILY_ID =" + FamilyId + " ) and record_id = " + _xmlgen.RecordID + ") and FLAG_RECYCLE='A'";
                    }
                    DataSet IDFields = CreateDataSet();
                    if (IDFields.Tables[0].Rows.Count > 0)
                    {
                        cont = 1;
                        parentcat = IDFields.Tables[0].Rows[0].ItemArray[1].ToString();
                        while (parentcat != "0")
                        {
                            val++;
                            _SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY from TB_CATEGORY WHERE CATEGORY_ID = '" + parentcat + "' and FLAG_RECYCLE='A'";
                            IDFields = CreateDataSet();
                            parentcat = IDFields.Tables[0].Rows[0].ItemArray[1].ToString();
                        }


                        if (FamilyType == "Main")
                        {
                            //_SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY from TB_CATEGORY WHERE CATEGORY_ID IN  (select distinct category_id from tb_project_section_details where family_id IN ( " + FamilyId + ") and record_id = " + _xmlgen.RecordID + ") and FLAG_RECYCLE='A'";
                            // ------------------------------- AFTER CUT PASTE FAMILY FROM ONE INDESIGN FILE TO ANOTHER---------------------------------------//
                            _SQLString = "SELECT DISTINCT CA.CATEGORY_NAME, CA.PARENT_CATEGORY FROM TB_PROJECT_SECTION_DETAILS PSD " +
                                        " JOIN TB_PROJECT_SECTIONS PS ON PSD.RECORD_ID = PS.RECORD_ID " +
                                        " JOIN TB_FAMILY FA ON FA.FAMILY_ID = PSD.FAMILY_ID " +
                                        " JOIN TB_CATEGORY CA ON FA.CATEGORY_ID = CA.CATEGORY_ID " +
                                        " WHERE CA.FLAG_RECYCLE = 'A' AND FA.FLAG_RECYCLE = 'A'  AND FA.FAMILY_ID IN (" + FamilyId + ")  AND PSD.RECORD_ID = " + _xmlgen.RecordID + " ";

                        }
                        else if (FamilyType == "Sub")
                        {
                            _SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY from TB_CATEGORY WHERE CATEGORY_ID IN (select distinct category_id from tb_project_section_details where family_id IN ( SELECT FAMILY_ID FROM TB_SUBFAMILY WHERE SUBFAMILY_ID IN (" + FamilyId + " )) and record_id = " + _xmlgen.RecordID + ") and FLAG_RECYCLE='A'";
                        }
                        IDFields = CreateDataSet();
                        parentcat = IDFields.Tables[0].Rows[0].ItemArray[1].ToString();
                        while (parentcat != "0")
                        {
                            _SQLString = "SELECT CATEGORY_NAME,PARENT_CATEGORY,CATEGORY_ID from TB_CATEGORY WHERE CATEGORY_ID = '" + parentcat + "' and FLAG_RECYCLE='A'";
                            IDFields = CreateDataSet();
                            catlevelEntry = "<SUBCATNAME_L" + val.ToString() + " COTYPE=\"TEXT\" TBGUID=\"FCN" + RandomClass.Next() + "\" aid:pstyle=\"SUBCATNAME_L" + val.ToString() + "\"><![CDATA[" + IDFields.Tables[0].Rows[0].ItemArray[0].ToString().Trim() + "]]></SUBCATNAME_L" + val.ToString() + ">\n" + catlevelEntry;
                            // if (idColumnFlag)
                            catlevelEntry = "<SUBCATNAME_ID" + val.ToString() + " COTYPE=\"TEXT\" TBGUID=\"FCN" + RandomClass.Next() + "\" aid:pstyle=\"SUBCATNAME_ID" + val.ToString() + "\"><![CDATA[" + IDFields.Tables[0].Rows[0].ItemArray[2].ToString().Trim() + "]]></SUBCATNAME_ID" + val.ToString() + ">\n" + catlevelEntry;
                            parentcat = IDFields.Tables[0].Rows[0].ItemArray[1].ToString();
                            val--;
                        }
                        strBuildXMLOutput.Append(catlevelEntry);
                        if (FamilyType == "Main")
                        {
                            //_SQLString = "select distinct category_id from tb_project_section_details where family_id IN (" + FamilyId + ") and RECORD_ID = " + _xmlgen.RecordID + " ";
                            // ------------------------------- AFTER CUT PASTE FAMILY FROM ONE INDESIGN FILE TO ANOTHER---------------------------------------//
                            _SQLString = "SELECT DISTINCT FA.CATEGORY_ID  FROM TB_PROJECT_SECTION_DETAILS PSD " +
                                        " JOIN TB_PROJECT_SECTIONS PS ON PSD.RECORD_ID = PS.RECORD_ID " +
                                        " JOIN TB_FAMILY FA ON FA.FAMILY_ID = PSD.FAMILY_ID " +
                                        " JOIN TB_CATEGORY CA ON FA.CATEGORY_ID = CA.CATEGORY_ID " +
                                        " WHERE CA.FLAG_RECYCLE = 'A' AND FA.FLAG_RECYCLE = 'A'  AND FA.FAMILY_ID IN (" + FamilyId + ")  AND PSD.RECORD_ID = " + _xmlgen.RecordID + " ";
                        }
                        else if (FamilyType == "Sub")
                        {
                            _SQLString = "select distinct category_id from tb_project_section_details where family_id IN ( SELECT FAMILY_ID FROM TB_SUBFAMILY WHERE SUBFAMILY_ID IN (" + FamilyId + " )) and record_id = " + _xmlgen.RecordID + "";
                        }
                        IDFields = CreateDataSet();
                        parentcat = IDFields.Tables[0].Rows[0].ItemArray[0].ToString();

                        _SQLString = "select DISTINCT TA.ATTRIBUTE_ID, REPLACE(TA.ATTRIBUTE_NAME, '','_') ATTRIBUTE_NAME, TCS.STRING_VALUE, TA.ATTRIBUTE_TYPE,REPLACE(TA.STYLE_NAME, ' ','_') as STYLE_NAME,TCS.NUMERIC_VALUE,TCS.SORT_ORDER " +
                  " From TB_CATEGORY_SPECS TCS LEFT JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TCS.ATTRIBUTE_ID AND ATTRIBUTE_TYPE IN(21,25) AND PUBLISH2PRINT = 1 LEFT JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID =" + _catalogId + " " +
                   "WHERE TCS.CATEGORY_ID ='" + parentcat + "' AND TA.ATTRIBUTE_ID IS NOT NULL";
                        oXMLDS_CategorySpecsAttLowLevel = CreateDataSet();

                        _SQLString = "select DISTINCT REPLACE(TA.ATTRIBUTE_NAME, '','_') ATTRIBUTE_NAME,TA.ATTRIBUTE_ID,TCS.STRING_VALUE, TA.ATTRIBUTE_TYPE,TCS.OBJECT_NAME, TCS.OBJECT_TYPE " +
                        " From TB_CATEGORY_SPECS TCS LEFT JOIN TB_ATTRIBUTE TA ON TA.ATTRIBUTE_ID = TCS.ATTRIBUTE_ID AND ATTRIBUTE_TYPE IN(23) AND PUBLISH2PRINT = 1 LEFT JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID = TA.ATTRIBUTE_ID AND TCA.CATALOG_ID =" + _catalogId + " " +
                       "WHERE TCS.CATEGORY_ID ='" + parentcat + "' AND TA.ATTRIBUTE_ID IS NOT NULL";
                        oXMLDS_CategoryImageAttLowLevel = CreateDataSet();

                        //string NumFieldValueCheck1 =Convert.ToInt64( oDSFam.Tables[0].Rows[rowFamCnt]["CUSTOM_NUM_FIELD1"])>0 "ss" :"RR";
                        // if (idColumnFlag)

                        strBuildXMLOutput.Append("<category_id  COTYPE=\"TEXT\" TBGUID=\"FCI" + RandomClass.Next() + "\" aid:pstyle=\"category_id\"><![CDATA[" + parentcat + "]]></category_id>\n");
                        strBuildXMLOutput.Append("<category_id  COTYPE=\"TEXT\" TBGUID=\"FCI" + RandomClass.Next() + "\" aid:pstyle=\"category_id\"><![CDATA[" + parentcat + "]]></category_id>\n");

                        strBuildXMLOutput.Append("<category_name  COTYPE=\"TEXT\" TBGUID=\"FCN" + RandomClass.Next() + "\" aid:pstyle=\"category_name\"><![CDATA[" + oDSFam.Tables[0].Rows[rowFamCnt].ItemArray[1].ToString().Trim() + "]]></category_name>\n");
                        strBuildXMLOutput.Append("<category_name  COTYPE=\"TEXT\" TBGUID=\"FCN" + RandomClass.Next() + "\" aid:pstyle=\"category_name\"><![CDATA[" + oDSFam.Tables[0].Rows[rowFamCnt].ItemArray[1].ToString().Trim() + "]]></category_name>\n");

                        foreach (DataRow dtr in oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows)
                        {
                            if (dtr["STRING_VALUE"].ToString() != null && dtr["STRING_VALUE"].ToString() != "" && dtr["ATTRIBUTE_ID"].ToString() != null && dtr["ATTRIBUTE_ID"].ToString() != "")
                            {
                                _SQLString = "select ATTRIBUTE_DATATYPE, ATTRIBUTE_DATAFORMAT from tb_attribute where ATTRIBUTE_ID=" + dtr["ATTRIBUTE_ID"].ToString() + " and FLAG_RECYCLE='A'";
                                DataSet AttributeDataFormatDS = CreateDataSet();
                                if (AttributeDataFormatDS.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow dr in AttributeDataFormatDS.Tables[0].Rows)
                                    {
                                        if (dr["ATTRIBUTE_DATATYPE"].ToString() == "Date and Time")
                                        {
                                            string DateValue = dtr["STRING_VALUE"].ToString();
                                            if (dr["ATTRIBUTE_DATAFORMAT"].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                            {
                                                dtr["STRING_VALUE"] = DateValue.Substring(3, 2) + "/" + DateValue.Substring(0, 2) + "/" + DateValue.Substring(6, 4);

                                            }

                                        }
                                    }
                                }
                            }
                        }



                        strBuildXMLOutput.Append("<cat_specs_type COTYPE=\"\" TBGUID=\"ST" + RandomClass.Next() + "\">\n");
                        for (rowCatAttrLowLevelCnts = 0; rowCatAttrLowLevelCnts < oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows.Count; rowCatAttrLowLevelCnts++)
                        #region From PreviewHTMLDATA.cs
                        {
                            AttrName = "";
                            string Style = string.Empty;
                            Style = oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[4].ToString();
                            if (Style == string.Empty || Style == "")
                            {
                                AttrName = oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[1].ToString();
                            }
                            if (Convert.ToInt32(oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[3].ToString()) != 23)
                            {
                                GetCurrencySymbol(oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString());
                                string sValue = string.Empty;
                                string dtype = oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[3].ToString();
                                int numberdecimel = 0;
                                if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                                {
                                    numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                                }
                                bool styleFormat = false; // Declared Static Temporarily                           
                                _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                                DataSet AttrDatatype = CreateDataSet();

                                #region "Decimal Place Triming"
                                if (AttrDatatype.Tables[0].Rows.Count > 0)
                                {
                                    if (oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[5].ToString() != null && oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[5].ToString() != "")
                                    {
                                        foreach (DataRow dr in AttrDatatype.Tables[0].Rows)
                                        {
                                            if (dr.ItemArray[0].ToString().StartsWith("Num"))
                                            {
                                                string tempStr = string.Empty;
                                                tempStr = oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[5].ToString();
                                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts]["NUMERIC_VALUE"] = tempStr;
                                            }
                                        }
                                    }
                                }
                                #endregion

                                if (AttrDatatype.Tables[0].Rows.Count > 0)
                                {
                                    if (AttrDatatype.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num"))
                                    {
                                        sValue = oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[5].ToString();
                                    }
                                    else
                                    {
                                        sValue = oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[2].ToString();
                                    }
                                    if (sValue == "" && (_emptyCondition == "Null" || _emptyCondition == "Empty"))
                                    {
                                        if (sValue == "" && _emptyCondition != "Empty")
                                        {
                                            sValue = "Null";
                                        }
                                    }
                                }
                                else
                                {
                                    sValue = oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[2].ToString();
                                }
                                //}
                                if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                                {
                                    if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                    if (_replaceText != "")
                                    {
                                        if (sValue == "0")
                                        {
                                            if (_emptyCondition == "0.000000")
                                            {
                                                sValue = _emptyCondition;
                                            }
                                        }
                                        if (sValue == "Null")
                                        {
                                            _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                        }
                                        else
                                        {
                                            _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                        }
                                        if (_replaceText != "")
                                            //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                            //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                            //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                            if (Convert.ToString(_replaceText) != string.Empty)
                                            {
                                                if (Convert.ToString(_replaceText) != sValue)
                                                {
                                                    if (_fornumeric == "1")
                                                    {
                                                        if (Isnumber(_replaceText.Replace(",", "")))
                                                        {
                                                            if (_headeroptions == "All")
                                                            {
                                                                if (Style == string.Empty || Style == "")
                                                                {
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                }
                                                                else
                                                                {
                                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                                }
                                                            }
                                                            else
                                                            {
                                                                if (rowCatAttrLowLevelCnts == 0)
                                                                {
                                                                    if (Style == string.Empty || Style == "")
                                                                        //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                    else
                                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                                }
                                                                else
                                                                {
                                                                    if (Style == string.Empty || Style == "")
                                                                        //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                                    else
                                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (_headeroptions == "All")
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (rowCatAttrLowLevelCnts == 0)
                                                            {
                                                                if (Style == string.Empty || Style == "")
                                                                    //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                            }
                                                            else
                                                            {
                                                                if (Style == string.Empty || Style == "")
                                                                    //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_headeroptions == "All")
                                                    {
                                                        if (Style == string.Empty || Style == "")
                                                            //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                        else
                                                            strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                    }
                                                    else
                                                    {
                                                        if (rowCatAttrLowLevelCnts == 0)
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (Style == string.Empty || Style == "")
                                                                //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _replaceText + "]]></" + Style + ">\n");
                                                        }
                                                    }
                                                }
                                            }
                                    }
                                    else
                                    {
                                        _replaceText = sValue;
                                    }
                                }
                                else if (oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[2].ToString().Trim() != "")
                                {
                                    //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                    //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                    if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            if (Style == string.Empty || Style == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                        }
                                        else
                                        {
                                            if (rowCatAttrLowLevelCnts == 0)
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                            }

                                        }
                                    }
                                    else
                                    {
                                        if (Isnumber(sValue))
                                        {
                                            sValue = DecPrecisionFill(sValue, numberdecimel);
                                            sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString()), sValue.Trim()) : sValue;
                                        }
                                        if (_headeroptions == "All")
                                        {
                                            if (Style == string.Empty || Style == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                        }
                                        else
                                        {
                                            if (rowCatAttrLowLevelCnts == 0)
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                            }
                                        }
                                    }
                                }
                                else if (oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[5].ToString().Trim() != "")
                                {
                                    _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                                    DataSet dsSize = CreateDataSet();
                                    sValue = oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[5].ToString();
                                    if (dsSize != null)
                                    {
                                        if (dsSize.Tables.Count > 0)
                                        {
                                            if (dsSize.Tables[0].Rows.Count > 0)
                                            {
                                                string Valsize = dsSize.Tables[0].Rows[0]["ATTRIBUTE_DATATYPE"].ToString();
                                                sValue = Valchk(sValue, Valsize); // Created Valchk function newly for this 
                                            }
                                        }
                                    }
                                    //sValue = htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[2].ToString();
                                    if (sValue != "")
                                    {
                                        //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                        //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                        if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (rowCatAttrLowLevelCnts == 0)
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                                }
                                                else
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");

                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                if (Style == string.Empty || Style == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                            }
                                            else
                                            {
                                                if (rowCatAttrLowLevelCnts == 0)
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style + ">\n");
                                                }
                                                else
                                                {
                                                    if (Style == string.Empty || Style == "")
                                                        //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style + "  attribute_id=\"" + oXMLDS_CategorySpecsAttLowLevel.Tables[0].Rows[rowCatAttrLowLevelCnts].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style + "\"><![CDATA[" + sValue + "]]></" + Style + ">\n");
                                                }
                                            }
                                        }
                                    }
                                }



                            }
                        }
                        #endregion

                        strBuildXMLOutput.Append("</cat_specs_type>\n");
                        //Start Family Image 


                        strBuildXMLOutput.Append("<image_type COTYPE=\"\" TBGUID=\"IT" + RandomClass.Next() + "\">\n");
                        FPath = "href=\"file://";
                        for (rowCatImgCntLowAttr = 0; rowCatImgCntLowAttr < oXMLDS_CategoryImageAttLowLevel.Tables[0].Rows.Count; rowCatImgCntLowAttr++)
                        {
                            ImgFile = "";
                            AttrName = "";
                            ImgFile = oXMLDS_CategoryImageAttLowLevel.Tables[0].Rows[rowCatImgCntLowAttr].ItemArray[2].ToString();
                            _xmlgen.UserImagePath = _xmlgen.UserImagePath.Replace("&", "&amp;");
                            ImgFile = ImgFile.Replace("\\", "/");
                            ImageName = _xmlgen.UserImagePath + ImgFile;
                            ImageName = ImageName.Replace("/", "\\");
                            ImgFile = FPath + ImageName + "\"";

                            AttrName = oXMLDS_CategoryImageAttLowLevel.Tables[0].Rows[rowCatImgCntLowAttr].ItemArray[0].ToString().Trim();
                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + " attribute_id=\"" + oXMLDS_CategoryImageAttLowLevel.Tables[0].Rows[rowCatImgCntLowAttr].ItemArray[1].ToString().Trim() + "\"" + "  IMAGE_FILE=\"" + oXMLDS_CategoryImageAttLowLevel.Tables[0].Rows[rowCatImgCntLowAttr].ItemArray[2].ToString().Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\" COTYPE=\"IMAGE\" TBGUID=\"IV" + RandomClass.Next() + "\"></" + AttrName.ToLower() + ">\n");
                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "_name attribute_id=\"" + oXMLDS_CategoryImageAttLowLevel.Tables[0].Rows[rowCatImgCntLowAttr].ItemArray[1].ToString().Trim() + "\"" + "  COTYPE=\"TEXT\" TBGUID=\"IN" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "_name\"><![CDATA[" + oXMLDS_CategoryImageAttLowLevel.Tables[0].Rows[rowCatImgCntLowAttr].ItemArray[4].ToString().Trim() + "]]></" + AttrName.ToLower() + "_name>\n");
                        }
                        strBuildXMLOutput.Append("</image_type>\n");

                        if (FamilyType == "Main")
                        {
                            strBuildXMLOutput.Append("<family_name  COTYPE=\"TEXT\" TBGUID=\"FN" + RandomClass.Next() + "\" aid:pstyle=\"family_name\"><![CDATA[" + oDSFam.Tables[0].Rows[rowFamCnt].ItemArray[0].ToString().Trim() + "]]></family_name>\n");
                        }
                        else if (FamilyType == "Sub")
                        {
                            strBuildXMLOutput.Append("<Subfamily_name  COTYPE=\"TEXT\" TBGUID=\"SFN" + RandomClass.Next() + "\" aid:pstyle=\"Subfamily_name\"><![CDATA[" + oDSFam.Tables[0].Rows[rowFamCnt].ItemArray[0].ToString().Trim() + "]]></Subfamily_name>\n");
                        }
                    }
                    //strBuildXMLOutput.Append("<foot_notes aid:pstyle=\"foot_notes\" COTYPE=\"TEXT\" TBGUID=\"FFN" + RandomClass.Next() + "\"><![CDATA[" + oDSFam.Tables[0].Rows[rowFamCnt].ItemArray[2].ToString().Trim() + "]]></foot_notes>\n");
                }
                oDSFam.Dispose();
                oDSFam = null;
                //End family 







                #region "Date Format Process"
                //DataTable TempDS = oXMLDS_FamilySpecs.Tables[0];
                foreach (DataRow dtr in oXMLDS_FamilySpecs.Tables[0].Rows)
                {
                    if (dtr["STRING_VALUE"].ToString() != null && dtr["STRING_VALUE"].ToString() != "" && dtr["ATTRIBUTE_ID"].ToString() != null && dtr["ATTRIBUTE_ID"].ToString() != "")
                    {
                        _SQLString = "select ATTRIBUTE_DATATYPE, ATTRIBUTE_DATAFORMAT from tb_attribute where ATTRIBUTE_ID=" + dtr["ATTRIBUTE_ID"].ToString() + " and FLAG_RECYCLE='A'";
                        DataSet AttributeDataFormatDS = CreateDataSet();
                        if (AttributeDataFormatDS.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in AttributeDataFormatDS.Tables[0].Rows)
                            {
                                if (dr["ATTRIBUTE_DATATYPE"].ToString() == "Date and Time")
                                {

                                    string DateValue = dtr["STRING_VALUE"].ToString();
                                    if (dr["ATTRIBUTE_DATAFORMAT"].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
                                    {



                                        dtr["STRING_VALUE"] = DateValue.Substring(3, 2) + "/" + DateValue.Substring(0, 2) + "/" + DateValue.Substring(6, 4);

                                    }

                                }
                            }
                        }
                    }
                }
                #endregion
                if (cont == 1)
                {
                    //Start Family Specs and Desc 
                    strBuildXMLOutput.Append("<specs_type COTYPE=\"\" TBGUID=\"ST" + RandomClass.Next() + "\">\n");

                    for (rowFamAttrCnt = 0; rowFamAttrCnt < oXMLDS_FamilySpecs.Tables[0].Rows.Count; rowFamAttrCnt++)
                    #region From PreviewHTMLDATA.cs
                    {
                        AttrName = "";
                        string Style_Value = string.Empty;
                        Style_Value = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[4].ToString();

                        if (Style_Value == string.Empty || Style_Value == "")
                        {
                            AttrName = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[1].ToString();
                        }

                        if (Convert.ToInt32(oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[3].ToString()) != 9)
                        {
                            GetCurrencySymbol(oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString());
                            string sValue = string.Empty;
                            string dtype = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[3].ToString();
                            int numberdecimel = 0;
                            if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
                            {
                                numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
                            }
                            bool styleFormat = false; // Declared Static Temporarily
                            //bool styleFormat = htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[9].ToString().Length > 0;
                            if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[3].ToString().Trim() != "12")
                            {
                                //if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[3].ToString().Trim() == "13")
                                //{
                                //    sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[2].ToString();
                                //}
                                //else
                                //{
                                _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                                DataSet AttrDatatype = CreateDataSet();

                                #region "Decimal Place Triming"
                                if (AttrDatatype.Tables[0].Rows.Count > 0)
                                {
                                    if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString() != null && oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString() != "")
                                    {
                                        foreach (DataRow dr in AttrDatatype.Tables[0].Rows)
                                        {
                                            if (dr.ItemArray[0].ToString().StartsWith("Num"))
                                            {
                                                string tempStr = string.Empty;
                                                tempStr = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString();
                                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt]["NUMERIC_VALUE"] = tempStr;
                                            }
                                        }
                                    }
                                }
                                #endregion

                                if (AttrDatatype.Tables[0].Rows.Count > 0)
                                {
                                    if (AttrDatatype.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("Num"))
                                    {
                                        if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[3].ToString().Trim() == "13")
                                        {
                                            sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[2].ToString();
                                        }
                                        else
                                        {
                                            sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString();
                                        }
                                    }
                                    else
                                    {
                                        sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[2].ToString();
                                    }
                                    if (sValue == "" && (_emptyCondition == "Null" || _emptyCondition == "Empty"))
                                    {
                                        if (sValue == "" && _emptyCondition != "Empty")
                                        {
                                            sValue = "Null";
                                        }
                                    }
                                }
                                else
                                {
                                    sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[2].ToString();
                                }
                                //}
                                if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
                                {
                                    if (_emptyCondition == "Empty") { _emptyCondition = ""; }
                                    if (_replaceText != "")
                                    {
                                        if (sValue == "0")
                                        {
                                            if (_emptyCondition == "0.000000")
                                            {
                                                sValue = _emptyCondition;
                                            }
                                        }
                                        if (sValue == "Null")
                                        {
                                            _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                        }
                                        else
                                        {
                                            _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
                                        }
                                        if (_replaceText != "")
                                            //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                            //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                            //if (Convert.ToString(_replaceText) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                            if (Convert.ToString(_replaceText) != string.Empty)
                                            {
                                                if (Convert.ToString(_replaceText) != sValue)
                                                {
                                                    if (_fornumeric == "1")
                                                    {
                                                        if (Isnumber(_replaceText.Replace(",", "")))
                                                        {
                                                            if (_headeroptions == "All")
                                                            {
                                                                if (Style_Value == string.Empty || Style_Value == "")

                                                                    //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style_Value + ">\n");
                                                            }
                                                            else
                                                            {
                                                                if (rowFamAttrCnt == 0)
                                                                {
                                                                    if (Style_Value == string.Empty || Style_Value == "")
                                                                        //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                    else
                                                                        strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style_Value + ">\n");
                                                                }
                                                                else
                                                                {
                                                                    if (Style_Value == string.Empty || Style_Value == "")
                                                                        //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                                    else
                                                                        strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _replaceText + "]]></" + Style_Value + ">\n");
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (Style_Value == string.Empty || Style_Value == "")
                                                                //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _replaceText + "]]></" + Style_Value + ">\n");
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (_headeroptions == "All")
                                                        {
                                                            if (Style_Value == string.Empty || Style_Value == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style_Value + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (rowFamAttrCnt == 0)
                                                            {
                                                                if (Style_Value == string.Empty || Style_Value == "")
                                                                    //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style_Value + ">\n");
                                                            }
                                                            else
                                                            {
                                                                if (Style_Value == string.Empty || Style_Value == "")
                                                                    //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _replaceText + "]]></" + Style_Value + ">\n");
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (_headeroptions == "All")
                                                    {
                                                        if (Style_Value == string.Empty || Style_Value == "")
                                                            //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                        else
                                                            strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style_Value + ">\n");
                                                    }
                                                    else
                                                    {
                                                        if (rowFamAttrCnt == 0)
                                                        {
                                                            if (Style_Value == string.Empty || Style_Value == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style_Value + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (Style_Value == string.Empty || Style_Value == "")
                                                                //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _replaceText + "]]></" + Style_Value + ">\n");
                                                        }
                                                    }
                                                }
                                            }
                                    }
                                    else
                                    {
                                        _replaceText = sValue;
                                    }
                                }
                                else if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[2].ToString().Trim() != "")
                                {
                                    //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                    //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                    if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                    {
                                        if (_headeroptions == "All")
                                        {
                                            if (Style_Value == string.Empty || Style_Value == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");
                                        }
                                        else
                                        {
                                            if (rowFamAttrCnt == 0)
                                            {
                                                if (Style_Value == string.Empty || Style_Value == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");

                                            }
                                            else
                                            {
                                                if (Style_Value == string.Empty || Style_Value == "")
                                                    //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + sValue + "]]></" + Style_Value + ">\n");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Isnumber(sValue))
                                        {
                                            sValue = DecPrecisionFill(sValue, numberdecimel);
                                            sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString()), sValue.Trim()) : sValue;
                                        }
                                        if (_headeroptions == "All")
                                        {
                                            if (Style_Value == string.Empty || Style_Value == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");
                                        }
                                        else
                                        {
                                            if (rowFamAttrCnt == 0)
                                            {
                                                if (Style_Value == string.Empty || Style_Value == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");
                                            }
                                            else
                                            {
                                                if (Style_Value == string.Empty || Style_Value == "")
                                                    //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + sValue + "]]></" + Style_Value + ">\n");
                                            }
                                        }
                                    }
                                }
                                else if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString().Trim() != "")
                                {
                                    _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                                    DataSet dsSize = CreateDataSet();
                                    sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString();
                                    if (dsSize != null)
                                    {
                                        if (dsSize.Tables.Count > 0)
                                        {
                                            if (dsSize.Tables[0].Rows.Count > 0)
                                            {
                                                string Valsize = dsSize.Tables[0].Rows[0]["ATTRIBUTE_DATATYPE"].ToString();
                                                sValue = Valchk(sValue, Valsize); // Created Valchk function newly for this 
                                            }
                                        }
                                    }
                                    //sValue = htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[2].ToString();
                                    if (sValue != "")
                                    {
                                        //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                        //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                        if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                if (Style_Value == string.Empty || Style_Value == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");
                                            }
                                            else
                                            {
                                                if (rowFamAttrCnt == 0)
                                                {
                                                    if (Style_Value == string.Empty || Style_Value == "")
                                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");
                                                }
                                                else
                                                {
                                                    if (Style_Value == string.Empty || Style_Value == "")
                                                        //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + sValue + "]]></" + Style_Value + ">\n");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (_headeroptions == "All")
                                            {
                                                if (Style_Value == string.Empty || Style_Value == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");
                                            }
                                            else
                                            {
                                                if (rowFamAttrCnt == 0)
                                                {
                                                    if (Style_Value == string.Empty || Style_Value == "")
                                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");
                                                }
                                                else
                                                {
                                                    if (Style_Value == string.Empty || Style_Value == "")
                                                        //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + sValue + "]]></" + Style_Value + ">\n");
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                            else
                            {

                                _SQLString = "SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID= " + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString() + " and FLAG_RECYCLE='A'";
                                DataSet dsSize = CreateDataSet();
                                if (dsSize.Tables[0].Rows.Count > 0)
                                {
                                    if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString() != null && oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString() != "")
                                    {
                                        foreach (DataRow dr in dsSize.Tables[0].Rows)
                                        {
                                            if (dr.ItemArray[0].ToString().StartsWith("Num"))
                                            {
                                                string tempStr = string.Empty;
                                                tempStr = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString();
                                                string datatype = dr["ATTRIBUTE_DATATYPE"].ToString();
                                                datatype = datatype.Remove(0, datatype.IndexOf(',') + 1);
                                                int noofdecimalplace = Convert.ToInt32(datatype.TrimEnd(')'));
                                                if (noofdecimalplace != 6)
                                                {
                                                    tempStr = tempStr.Remove(tempStr.IndexOf('.') + 1 + noofdecimalplace);
                                                }
                                                if (noofdecimalplace == 0)
                                                {
                                                    tempStr = tempStr.TrimEnd('.');
                                                }
                                                oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt]["NUMERIC_VALUE"] = tempStr;
                                            }

                                        }
                                    }
                                }
                                if (oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5] != DBNull.Value)
                                {
                                    sValue = oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[5].ToString();
                                    //if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000" && sValue != "Null")
                                    if (Convert.ToString(sValue) != string.Empty && sValue != _emptyCondition)
                                    {
                                        if (dsSize != null)
                                        {
                                            if (dsSize.Tables.Count > 0)
                                            {
                                                if (dsSize.Tables[0].Rows.Count > 0)
                                                {
                                                    string Valsize = dsSize.Tables[0].Rows[0]["ATTRIBUTE_DATATYPE"].ToString();
                                                    sValue = Valchk(sValue, Valsize);
                                                }
                                            }
                                        }
                                        // sValue = htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[2].ToString();
                                        if (Isnumber(sValue))
                                        {
                                            //sValue = DecPrecisionFill(sValue, numberdecimel);
                                            sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString()), sValue.Trim()) : sValue;
                                        }
                                    }
                                }
                                else
                                    sValue = "Null";
                                //if (sValue != "")
                                {
                                    if (Convert.ToString(sValue) != string.Empty && sValue != _emptyCondition && sValue != "Null")
                                    {
                                        //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                        //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                        if (_headeroptions == "All")
                                        {
                                            if (Style_Value == string.Empty || Style_Value == "")
                                                //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                            else
                                                strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");
                                        }
                                        else
                                        {
                                            if (rowFamAttrCnt == 0)
                                            {
                                                if (Style_Value == string.Empty || Style_Value == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");
                                            }
                                            else
                                            {
                                                if (Style_Value == string.Empty || Style_Value == "")
                                                    //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + sValue + "]]></" + Style_Value + ">\n");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000" || _emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "")
                                        {
                                            if (_replaceText != "")
                                            {
                                                _replaceText = _emptyCondition == sValue ? _replaceText : "";
                                            }
                                            else
                                            {
                                                _replaceText = "";
                                            }
                                            if (_replaceText != "")
                                            {
                                                //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                                //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                if (_fornumeric == "1")
                                                {
                                                    if (Isnumber(_replaceText.Replace(",", "")))
                                                    {
                                                        if (_headeroptions == "All")
                                                        {
                                                            if (Style_Value == string.Empty || Style_Value == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style_Value + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (rowFamAttrCnt == 0)
                                                            {
                                                                if (Style_Value == string.Empty || Style_Value == "")
                                                                    //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style_Value + ">\n");
                                                            }
                                                            else
                                                            {
                                                                if (Style_Value == string.Empty || Style_Value == "")
                                                                    //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                                else
                                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _replaceText + "]]></" + Style_Value + ">\n");
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (Style_Value == string.Empty || Style_Value == "")
                                                            //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                        else
                                                            strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _replaceText + "]]></" + Style_Value + ">\n");
                                                    }
                                                }
                                                else
                                                {
                                                    if (_headeroptions == "All")
                                                    {
                                                        if (Style_Value == string.Empty || Style_Value == "")
                                                            //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                            strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                        else
                                                            strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style_Value + ">\n");
                                                    }
                                                    else
                                                    {
                                                        if (rowFamAttrCnt == 0)
                                                        {
                                                            if (Style_Value == string.Empty || Style_Value == "")
                                                                //htmlData.Append(_prefix + _replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + _replaceText + _suffix + "]]></" + Style_Value + ">\n");
                                                        }
                                                        else
                                                        {
                                                            if (Style_Value == string.Empty || Style_Value == "")
                                                                //htmlData.Append(_replaceText.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                                strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _replaceText + "]]></" + AttrName.ToLower() + ">\n");
                                                            else
                                                                strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _replaceText + "]]></" + Style_Value + ">\n");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //htmlData.Append("<br><br><font face=\"Arial Unicode MS\"><b>" + htmlPreview1.FAMLIY_SPECS.Rows[rowCount].ItemArray[1].ToString().Trim() + ": </b><br>");
                                            //strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                            if (_headeroptions == "All")
                                            {
                                                if (Style_Value == string.Empty || Style_Value == "")
                                                    //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                    strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                else
                                                    strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");
                                            }
                                            else
                                            {
                                                if (rowFamAttrCnt == 0)
                                                {
                                                    if (Style_Value == string.Empty || Style_Value == "")
                                                        //htmlData.Append(_prefix + sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + _suffix + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + Style_Value + ">\n");
                                                }
                                                else
                                                {
                                                    if (Style_Value == string.Empty || Style_Value == "")
                                                        //htmlData.Append(sValue.Replace("<", "&lt;").Replace(">", "&gt;").Replace("\r\n", "<br/>").Replace("\r", "<br/>").Replace("\n", "<br/>").Replace("  ", "&nbsp;&nbsp;").Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") + "</font>");
                                                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "\"><![CDATA[" + sValue + "]]></" + AttrName.ToLower() + ">\n");
                                                    else
                                                        strBuildXMLOutput.Append("<" + Style_Value + "  attribute_id=\"" + oXMLDS_FamilySpecs.Tables[0].Rows[rowFamAttrCnt].ItemArray[0].ToString().Trim() + "\" COTYPE=\"TEXT\" TBGUID=\"SAV" + RandomClass.Next() + "\" aid:pstyle=\"" + Style_Value + "\"><![CDATA[" + sValue + "]]></" + Style_Value + ">\n");
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    strBuildXMLOutput.Append("</specs_type>\n");
                    //Start Family Image 

                    DateTime CurrTime = DateTime.Now;
                    strBuildXMLOutput.Append("<image_type COTYPE=\"\" TBGUID=\"IT" + RandomClass.Next() + "\">\n");
                    FPath = "href=\"file://";
                    for (rowFamImgCnt = 0; rowFamImgCnt < oXMLDS_FamilyImages_.Tables[0].Rows.Count; rowFamImgCnt++)
                    {
                        ImgFile = "";
                        AttrName = "";
                        ImgFile = oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[2].ToString();
                        _xmlgen.UserImagePath = _xmlgen.UserImagePath.Replace("&", "&amp;");
                        ImgFile = ImgFile.Replace("\\", "/");
                        ImageName = _xmlgen.UserImagePath + ImgFile;
                        ImageName = ImageName.Replace("/", "\\");
                        ImgFile = FPath + ImageName + "\"";

                        AttrName = oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[1].ToString().Trim();
                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + " attribute_id=\"" + oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[0].ToString().Trim() + "\"" + "  IMAGE_FILE=\"" + oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[2].ToString().Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\" COTYPE=\"IMAGE\" TBGUID=\"IV" + RandomClass.Next() + "\"></" + AttrName.ToLower() + ">\n");
                        strBuildXMLOutput.Append("<" + AttrName.ToLower() + "_name attribute_id=\"" + oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[0].ToString().Trim() + "\"" + "  COTYPE=\"TEXT\" TBGUID=\"IN" + RandomClass.Next() + "\" aid:pstyle=\"" + AttrName.ToLower() + "_name\"><![CDATA[" + oXMLDS_FamilyImages_.Tables[0].Rows[rowFamImgCnt].ItemArray[4].ToString().Trim() + "]]></" + AttrName.ToLower() + "_name>\n");
                    }
                    strBuildXMLOutput.Append("</image_type>\n");
                    //End Family Image

                    //Start Supplier Info

                    _SQLString = "SELECT supplier_name,url,replace(replace(logo_image_file,\'\\',\'/\'),\'&\',\'&amp;\') as image_file,logo_image_type " +
                            "FROM tb_supplier WHERE supplier_name in (SELECT DISTINCT p.string_value FROM tb_prod_specs p,tb_prod_family l " +
                            "WHERE p.product_id=l.product_id AND l.family_id=" + FamilyId + " and p.string_value <> '' and l.FLAG_RECYCLE='A') and FLAG_RECYCLE='A'";
                    oDSSupp = CreateDataSet();

                    strBuildXMLOutput.Append("<vendor_info COTYPE=\"\" TBGUID=\"VI" + RandomClass.Next() + "\">\n");
                    for (rowSupp = 0; rowSupp < oDSSupp.Tables[0].Rows.Count; rowSupp++)
                    {
                        strBuildXMLOutput.Append("<vendor COTYPE=\"\" TBGUID=\"V" + RandomClass.Next() + "\">\n");
                        strBuildXMLOutput.Append("<supplier_name  COTYPE=\"TEXT\" TBGUID=\"SN" + RandomClass.Next() + "\" aid:pstyle=\"supplier_name\"><![CDATA[" + oDSSupp.Tables[0].Rows[rowSupp].ItemArray[0].ToString().Trim() + "]]></supplier_name>\n");
                        strBuildXMLOutput.Append("<url  COTYPE=\"TEXT\" TBGUID=\"SU" + RandomClass.Next() + "\" aid:pstyle=\"url\"><![CDATA[" + oDSSupp.Tables[0].Rows[rowSupp].ItemArray[1].ToString().Trim() + "]]></url>\n");
                        SuppImg = "";
                        SuppImg = oDSSupp.Tables[0].Rows[rowSupp].ItemArray[2].ToString().Trim();
                        _xmlgen.UserImagePath = _xmlgen.UserImagePath.Replace("&", "&amp;");
                        SuppImg = SuppImg.Replace("\\", "/");
                        SuppImgName = _xmlgen.UserImagePath + SuppImg;
                        SuppImgName = SuppImgName.Replace("/", "\\");
                        SuppImg = FPath + SuppImgName + "\"";
                        strBuildXMLOutput.Append("<logo_image_file" + " " + " IMAGE_FILE=\"" + oDSSupp.Tables[0].Rows[rowSupp].ItemArray[2].ToString().Trim().Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\" COTYPE=\"IMAGE\" TBGUID=\"SL" + RandomClass.Next() + "\"></logo_image_file>\n");
                        strBuildXMLOutput.Append("<logo_image_type  COTYPE=\"TEXT\" TBGUID=\"SLI" + RandomClass.Next() + "\" aid:pstyle=\"logo_image_type\"><![CDATA[" + oDSSupp.Tables[0].Rows[rowSupp].ItemArray[3].ToString().Trim() + "]]></logo_image_type>\n");
                        strBuildXMLOutput.Append("</vendor>\n");
                    }
                    strBuildXMLOutput.Append("</vendor_info>\n");
                    oDSSupp.Dispose();
                    oDSSupp = null;
                    ////End Supplier Info		

                    advtable = String.Empty;
                    //Advance Table
                    _SQLString = "SELECT FAMILY_TABLE_STRUCTURE  FROM TB_FAMILY_TABLE_STRUCTURE WHERE family_id=" + FamilyId + " AND CATALOG_ID=" + _catalogId + " AND IS_DEFAULT=1";
                    DataSet oDSATable = CreateDataSet();
                    if (oDSATable.Tables[0].Rows.Count > 0 && oDSATable != null)
                    {
                        if (oDSATable.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                        {
                            advtable = oDSATable.Tables[0].Rows[0].ItemArray[0].ToString();
                        }
                    }
                    //Start Products
                    ///pivot table
                    XmlDocument xmlDOc = new XmlDocument();
                    //////////PivotTableGenerator.PivotTableGenerator OPivotGenerator = new PivotTableGenerator.PivotTableGenerator(FamilyId.ToString(), CatalogId.ToString());
                    //////////TableGenerator.TableGenerator oTableGenerator = new TradingBell.CatalogStudio.TableGenerator.TableGenerator(FamilyId.ToString(), CatalogId.ToString());
                    //////////oTableGenerator.ApplicationStartupPath = Application.StartupPath;
                    if (!string.IsNullOrEmpty(advtable))
                    {
                        xmlDOc.LoadXml(advtable);
                        XmlNode rootNode = xmlDOc.DocumentElement;
                        XmlNodeList xmlNodeList;
                        xmlNodeList = rootNode.ChildNodes;
                        if (rootNode.Attributes["TableType"] != null)
                        {
                            if (rootNode.Attributes["TableType"].Value == "Pivot")
                            {
                                TMode = rootNode.Attributes["TableType"].Value;

                            }
                            if (rootNode.Attributes["TableType"].Value == "SuperTable")
                            {
                                TMode = rootNode.Attributes["TableType"].Value;

                            }
                            else if (rootNode.Attributes["TableType"].Value == "Horizontal")
                            {
                                TMode = rootNode.Attributes["TableType"].Value;

                            }
                            else if (rootNode.Attributes["TableType"].Value == "Grouped")
                            {
                                TMode = rootNode.Attributes["TableType"].Value;

                            }
                            else if (rootNode.Attributes["TableType"].Value == "Vertical")
                            {
                                TMode = rootNode.Attributes["TableType"].Value;

                            }
                        }
                        else
                        {
                            if (advtable.Contains("TableType=\"Pivot\""))
                            {
                                TMode = "Pivot";

                            }
                            else if (advtable.Contains("TableType=\"SuperTable\""))
                            {
                                TMode = "SuperTable";

                            }
                            else if (advtable.Contains("TableType=\"Horizontal\""))
                            {
                                TMode = "Horizontal";

                            }
                            else if (advtable.Contains("TableType=\"Grouped\""))
                            {
                                TMode = "Grouped";

                            }
                            else if (advtable.Contains("TableType=\"Vertical\""))
                            {
                                TMode = "Vertical";

                            }
                        }
                    }
                    ////}
                    if (!string.IsNullOrEmpty(advtable) && _xmlgen.PageName == "INCAT" && TMode == "Grouped")
                    {
                        string xml = string.Empty;
                        xml = XMLStringOutput(Convert.ToInt32(FamilyId));

                    }

                    else if (!string.IsNullOrEmpty(advtable) && _xmlgen.PageName == "INCAT" && TMode == "Pivot")
                    {
                        ////////string XmlForPivot = OPivotGenerator.GeneratePivotXML();
                        ////////strBuildXMLOutput.Append(XmlForPivot);
                    }
                    else if (!string.IsNullOrEmpty(advtable) && _xmlgen.PageName == "INCAT" && TMode == "SuperTable")
                    {
                        SuperTable objSuperTable = new SuperTable();
                        string XmlForSuperTable = objSuperTable.GenerateTableXml(FamilyId, _catalogId, categoryId, Attr_List, User.Identity.Name);
                        strBuildXMLOutput.Append(XmlForSuperTable);
                    }

                    else
                    {
                        if (_xmlgen.PageName == "INCAT")
                        {
                            //strBuildXMLOutput.Append("<products TBGUID=\"P" + RandomClass.Next() + "\">");
                        }
                        else
                        {
                            strBuildXMLOutput.Append("<products>\n");
                        }
                        ProductData = GetProductDetails(FamilyId, _catalogId);
                        //if (ProductData.ToString() != "Null")
                        //    strBuildXMLOutput.Append("</products>\n");
                    }
                    //End Products
                }
                else
                {
                    string substrBuildXMLOutput = strBuildXMLOutput.ToString();
                    strBuildXMLOutput = new StringBuilder();
                    strBuildXMLOutput.Append(substrBuildXMLOutput.Substring(0, substrBuildXMLOutput.LastIndexOf("<product_family")));
                }
                return strBuildXMLOutput.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool Checkfamilyfilter(string SQL)
        {

            _SQLString = SQL;
            bool Retval = false;
            DataSet oDscheck = new DataSet();
            oDscheck = CreateDataSet();
            if (oDscheck.Tables[0].Rows.Count > 0)
            {
                Retval = true;
            }
            return Retval;
        }

        private void LoadReferenceTables()
        {
            #region ReferenceTables
            //reference tables

            /////////_SQLString = "DECLARE @SOURCE_ID VARCHAR(1000);SET @SOURCE_ID = '" + FamilyId + "';IF(NOT EXISTS(SELECT 1 FROM TB_REFERENCE_SECTIONS WHERE FAMILY_ID = @SOURCE_ID)) BEGIN SET @SOURCE_ID = (SELECT TOP 1 CATEGORY_ID FROM TB_CATALOG_FAMILY WHERE FAMILY_ID = CONVERT(INT,@SOURCE_ID))		END WHILE(NOT EXISTS(SELECT 1 FROM TB_REFERENCE_SECTIONS WHERE FAMILY_ID = @SOURCE_ID)) BEGIN	IF(@SOURCE_ID = '0') BEGIN BREAK; END ELSE BEGIN SET @SOURCE_ID = (SELECT TOP 1 PARENT_CATEGORY FROM TB_CATEGORY WHERE CATEGORY_ID = @SOURCE_ID)		END END SELECT A.TABLE_ID,A.TABLE_NAME FROM TB_REFERENCE_TABLE A JOIN TB_REFERENCE_SECTIONS B ON A.TABLE_ID = B.TABLE_ID WHERE B.FAMILY_ID = @SOURCE_ID";

            _SQLString = "SELECT A.TABLE_ID,B.TABLE_NAME FROM TB_REFERENCE_SECTIONS A JOIN TB_REFERENCE_TABLE B ON A.TABLE_ID = B.TABLE_ID AND A.FAMILY_ID = " + FamilyId + " AND A.CATALOG_ID =" + _catalogId + "";
            DataSet dsReferenceTable = CreateDataSet();
            if (dsReferenceTable.Tables.Count > 0)
            {
                if (dsReferenceTable.Tables[0].Rows.Count > 0)
                {
                    //  strBuildXMLOutput.Append("<REFERENCE_TABLE xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\" tables=\"" + dsReferenceTable.Tables[0].Rows.Count + "\"  COTYPE=\"\" TBGUID=\"M" + RandomClass.Next() + "\" aid:pstyle=\"reference_table\">");
                    foreach (DataRow item in dsReferenceTable.Tables[0].Rows)
                    {
                        _SQLString = "EXEC STP_CATALOGSTUDIO5_ReferenceTable " + Convert.ToString(item["TABLE_ID"]) + "";
                        DataSet dsValues = CreateDataSet();
                        if (dsValues.Tables.Count > 0)
                        {
                            if (dsValues.Tables[0].Rows.Count > 0)
                            {
                                strBuildXMLOutput.Append(
                                    "<REFERENCE_TABLE xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\" tables=\"" +
                                    dsReferenceTable.Tables[0].Rows.Count + "\"  COTYPE=\"\" TBGUID=\"M" +
                                    RandomClass.Next() + "\" aid:pstyle=\"reference_table\">");
                                //strBuildXMLOutput.Append("<" + Convert.ToString(item["TABLE_NAME"]) + " xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\" products=\"" + dsValues.Tables[0].Rows.Count + "\"  COTYPE=\"\" TBGUID=\"M" + RandomClass.Next() + "\" aid:pstyle=\"" + Convert.ToString(item["TABLE_NAME"]) + "\">\n");
                                strBuildXMLOutput.Append(
                                    "<REF_TABLE xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\" products=\"" +
                                    dsValues.Tables[0].Rows.Count + "\"  COTYPE=\"\" TBGUID=\"M" + RandomClass.Next() +
                                    "\" aid:pstyle=\"REF_TABLE\">");
                                strBuildXMLOutput.Append("<TABLE TBGUID=\"TA" + RandomClass.Next() +
                                                         "\" Format=\"SuperTable\" ncols=\"" +
                                                         (dsValues.Tables[0].Columns.Count - 1).ToString() +
                                                         "\" nrows=\"" +
                                                         dsValues.Tables[0].Rows.Count +
                                                         "\" aid:table=\"table\" aid:tcols=\"" +
                                                         (dsValues.Tables[0].Columns.Count - 1).ToString() +
                                                         "\" aid:trows=\"" + dsValues.Tables[0].Rows.Count +
                                                         "\" aid5:tablestyle=\"TABLE\" Transpose=\"0\">");

                                #region XmlChange for Refrencetable

                                foreach (DataRow item1 in dsValues.Tables[0].Rows)
                                {
                                    for (int k = 1; k < dsValues.Tables[0].Columns.Count; k++)
                                    {

                                        if (k == 1)
                                        {

                                            string Case = Convert.ToString(item1[k]).ToString();
                                            string value = Convert.ToString(item1[k]).ToLower();
                                            // if (value.EndsWith(".jpg"))
                                            // if(value.Substring(value.Length-4).Contains("."))
                                            if (value.EndsWith(".jpg") || value.EndsWith(".bmp") ||
                                                value.EndsWith(".eps") ||
                                                value.EndsWith(".psd") || value.EndsWith(".png") ||
                                                value.EndsWith(".tif") ||
                                                value.EndsWith(".tiff") || value.EndsWith(".gif"))
                                            {
                                                //SystemSettingsCollection SettingMembers = SystemSettingsConfiguration.GetConfig.Members;
                                                //string referenceTableImagePath = SettingMembers.GetValue(SystemSettingsCollection.SettingsList.REFERENCETABLEIMAGEPATH.ToString());
                                                //strBuildXMLOutput.Append("<Cell_start TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell_start\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + value + "]]></Cell_start>");
                                                strBuildXMLOutput.Append("<Images TBGUID=\"TPS" + RandomClass.Next() +
                                                                         "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Images\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[]]><Image_Name TBGUID=\"CI" +
                                                                         RandomClass.Next() + "\" IMAGE_FILE=\"" + Case +
                                                                         "\"/></Images>");

                                            }
                                            else
                                            {
                                                strBuildXMLOutput.Append("<Cell TBGUID=\"TPS" + RandomClass.Next() +
                                                                         "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" +
                                                                         Case + "]]></Cell>");
                                            }
                                        }
                                        else
                                        {
                                            string Case = Convert.ToString(item1[k]).ToString();
                                            string value = Convert.ToString(item1[k]).ToLower();
                                            //if (value.Substring(value.Length - 4).Contains("."))
                                            // if (value.EndsWith(".jpg"))
                                            if (value.EndsWith(".jpg") || value.EndsWith(".bmp") ||
                                                value.EndsWith(".eps") || value.EndsWith(".svg") ||
                                                value.EndsWith(".psd") || value.EndsWith(".png") ||
                                                value.EndsWith(".tif") ||
                                                value.EndsWith(".tiff") || value.EndsWith(".gif"))
                                            {
                                                //SystemSettingsCollection SettingMembers = SystemSettingsConfiguration.GetConfig.Members;
                                                //string referenceTableImagePath = SettingMembers.GetValue(SystemSettingsCollection.SettingsList.REFERENCETABLEIMAGEPATH.ToString());
                                                //strBuildXMLOutput.Append("<Cell TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + value + "]]></Cell>");
                                                strBuildXMLOutput.Append("<Images TBGUID=\"TPS" + RandomClass.Next() +
                                                                         "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Images\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[]]><Image_Name TBGUID=\"CI" +
                                                                         RandomClass.Next() + "\" IMAGE_FILE=\"" + Case +
                                                                         "\"/></Images>");
                                            }

                                            else
                                            {
                                                strBuildXMLOutput.Append("<Cell TBGUID=\"TPS" + RandomClass.Next() +
                                                                         "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" +
                                                                         Case + "]]></Cell>");
                                            }
                                        }
                                    }
                                }

                                #endregion







                                strBuildXMLOutput.Append("</TABLE>");
                                //strBuildXMLOutput.Append("</" + Convert.ToString(item["TABLE_NAME"]) + ">");
                                strBuildXMLOutput.Append("</REF_TABLE>");
                                strBuildXMLOutput.Append("</REFERENCE_TABLE>");
                            }
                        }
                    }
                    //  strBuildXMLOutput.Append("</REFERENCE_TABLE>");
                }
            }
            //end reference tables
            #endregion
        }



        //private void LoadMultipleTables()
        //{

        //    _SQLString = "EXEC STP_CATALOGSTUDIO5_MULTIPLE_TABLE_FAMILY_PREVIEW " + FamilyId + "," + _catalogId + ",'" + Attr_List + "'";

        //    DataSet dsAllMultipleTablesData = CreateDataSet();
        //    if (dsAllMultipleTablesData.Tables.Count > 0)
        //    {
        //        #region product filter
        //        StringBuilder DynamicSQl = new StringBuilder();
        //        DataSet oDsProdFilter = new DataSet();
        //        string sProdFilter = string.Empty;
        //        _SQLString = " SELECT PRODUCT_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + _catalogId + " ";
        //        oDsProdFilter = CreateDataSet();
        //        if (oDsProdFilter.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty && oDsProdFilter.Tables[0].Rows.Count > 0)
        //        {
        //            sProdFilter = oDsProdFilter.Tables[0].Rows[0].ItemArray[0].ToString();
        //            XmlDocument xmlDOc = new XmlDocument();
        //            xmlDOc.LoadXml(sProdFilter);
        //            XmlNode rNode = xmlDOc.DocumentElement;
        //            // StringBuilder SQLstring = new StringBuilder();

        //            if (rNode.ChildNodes.Count > 0)
        //            {
        //                for (int i = 0; i < rNode.ChildNodes.Count; i++)
        //                {
        //                    StringBuilder SQLstring = new StringBuilder();
        //                    XmlNode TableDataSetNode = rNode.ChildNodes[i];
        //                    if (TableDataSetNode.HasChildNodes)
        //                    {
        //                        if (TableDataSetNode.ChildNodes[2].InnerText == " ")
        //                        {
        //                            TableDataSetNode.ChildNodes[2].InnerText = "=";
        //                        }
        //                        string StrVal = TableDataSetNode.ChildNodes[3].InnerText.Trim().Replace("'", "''");
        //                        DataSet oDsattrtype = new DataSet();
        //                        // StringBuilder DynamicattrSQl = new StringBuilder();
        //                        _SQLString = " SELECT attribute_datatype FROM TB_attribute WHERE  attribute_ID = " + TableDataSetNode.ChildNodes[0].InnerText + " ";
        //                        oDsattrtype = CreateDataSet();
        //                        if (oDsattrtype.Tables[0].Rows[0][0].ToString().ToUpper().StartsWith("N") == false)
        //                        {
        //                            //SQLstring.Append("SELECT PRODUCT_ID FROM TB_PROD_SPECS WHERE Numeric_VALUE  " + TableDataSetNode.ChildNodes[2].InnerText + " " + StrVal + "  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "");
        //                            SQLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + _catalogId + ") WHERE (STRING_VALUE " + TableDataSetNode.ChildNodes[2].InnerText + " '" + StrVal + "' AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
        //                        }
        //                        else
        //                        {
        //                            //SQLstring.Append("SELECT PRODUCT_ID FROM TB_PROD_SPECS WHERE STRING_VALUE  " + TableDataSetNode.ChildNodes[2].InnerText + " N'" + StrVal + "'  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "");
        //                            SQLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + _catalogId + ") WHERE  (NUMERIC_VALUE " + TableDataSetNode.ChildNodes[2].InnerText + " '" + StrVal + "' AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");

        //                        }
        //                    }
        //                    if (TableDataSetNode.ChildNodes[4].InnerText == "NONE")
        //                    {
        //                        DynamicSQl.Append(SQLstring);
        //                        break;
        //                    }
        //                    DynamicSQl.Append(SQLstring);
        //                    if (TableDataSetNode.ChildNodes[4].InnerText == "AND")
        //                    {

        //                        DynamicSQl.Append(" INTERSECT \n");
        //                    }
        //                    if (TableDataSetNode.ChildNodes[4].InnerText == "OR")
        //                    {
        //                        DynamicSQl.Append(" UNION \n");
        //                    }

        //                    //if (i > 0)
        //                    //{
        //                    //    DynamicSQl.Append(" union ");
        //                    //}
        //                }
        //            }

        //        }
        //        DataSet DSfilter = new DataSet();

        //        _SQLString = DynamicSQl.ToString();
        //        if (DynamicSQl.ToString() != string.Empty)
        //        {
        //            DSfilter = CreateDataSet();
        //        }
        //        if (DSfilter != null && DSfilter.Tables.Count > 0)
        //        {
        //            if (DSfilter.Tables[0].Rows.Count > 0)
        //            {
        //                DataSet temp = dsAllMultipleTablesData;
        //                DataSet Dspreviewaltered = new DataSet();
        //                DataTable t1 = new DataTable();
        //                Dspreviewaltered.Tables.Add(dsAllMultipleTablesData.Tables[0].Clone());

        //                foreach (DataRow DR in dsAllMultipleTablesData.Tables[0].Rows)
        //                {
        //                    DataRow[] DrfilteredRows = DSfilter.Tables[0].Select("PRODUCT_ID = " + DR["PRODUCT_ID"]);
        //                    if (DrfilteredRows != null && DrfilteredRows.Length != 0)
        //                    {
        //                        //DataRow datarw = t1.NewRow();
        //                        Dspreviewaltered.Tables[0].ImportRow(DR);
        //                    }

        //                }
        //                dsAllMultipleTablesData = new DataSet();
        //                dsAllMultipleTablesData.Tables.Add(Dspreviewaltered.Tables[0].Clone());
        //                foreach (DataRow DR in Dspreviewaltered.Tables[0].Rows)
        //                {
        //                    dsAllMultipleTablesData.Tables[0].ImportRow(DR);
        //                }
        //            }
        //        }
        //        #endregion
        //    }
        //    //_SQLString = "SELECT FAMILY_TABLE_STRUCTURE FROM TB_FAMILY_TABLE_STRUCTURE WHERE FAMILY_ID = " + FamilyId + " AND CATALOG_ID = " + _catalogId + " AND IS_DEFAULT = 1";
        //    //DataSet dsTableLayout = CreateDataSet();     
        //    #region "Date and Time Formatting"
        //    string[] Attribute_IDS = Attr_List.Split(',');

        //    #region "Multipletabledefault"

        //    if (Attr_List.ToString() == "")
        //    {

        //        _SQLString = "select DISTINCT ATTRIBUTE_ID from TB_ATTRIBUTE_GROUP_SECTIONS TAGS INNER JOIN TB_ATTRIBUTE_GROUP_SPECS TAGSS ON TAGSS.GROUP_ID=TAGS.GROUP_ID INNER JOIN TB_ATTRIBUTE_GROUP TAG ON TAG.GROUP_ID=TAGS.GROUP_ID AND TAGSS.GROUP_ID=TAG.GROUP_ID where TAGS.family_id=" + FamilyId + " and TAGS.catalog_id=" + _catalogId + "";
        //        DataSet AttributeListDs = new DataSet();
        //        AttributeListDs = CreateDataSet();
        //        string AttributeList = "";
        //        for (int i = 0; i < AttributeListDs.Tables[0].Rows.Count; i++)
        //        {
        //            AttributeList = AttributeList + "," + AttributeListDs.Tables[0].Rows[i][0].ToString();
        //        }
        //        if (AttributeList.Length > 0)
        //        {
        //            AttributeList = AttributeList.Remove(0, 1);
        //            Attribute_IDS = AttributeList.Split(',');
        //        }
        //    }

        //    #endregion


        //    if (dsAllMultipleTablesData.Tables.Count > 0)
        //        if (dsAllMultipleTablesData.Tables[0].Rows.Count > 0)
        //        {
        //            for (int i = 0; i < Attribute_IDS.Length; i++)
        //            {
        //                _SQLString = "SELECT ATTRIBUTE_NAME,ATTRIBUTE_DATATYPE,ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID =" + Attribute_IDS[i] + "";
        //                DataSet AttrName = CreateDataSet();

        //                if (AttrName.Tables.Count > 0)
        //                    if (AttrName.Tables[0].Rows.Count > 0)
        //                        if (AttrName.Tables[0].Rows[0][1].ToString().StartsWith("Date"))
        //                        {
        //                            foreach (DataColumn dc in dsAllMultipleTablesData.Tables[0].Columns)
        //                            {
        //                                if (dc.ColumnName == AttrName.Tables[0].Rows[0][0].ToString())
        //                                {
        //                                    foreach (DataRow dr in dsAllMultipleTablesData.Tables[0].Rows)
        //                                    {
        //                                        if (AttrName.Tables[0].Rows[0][2].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}"))
        //                                        {
        //                                            string tempdate = dr[dc.ColumnName].ToString();
        //                                            dr[dc.ColumnName] = tempdate.Substring(3, 2) + "/" + tempdate.Substring(0, 2) + "/" + tempdate.Substring(6, 4);
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }
        //            }
        //        }

        //    #endregion

        //    bool isVericalTable = false;
        //    string cell_style_name = "";
        //    if (dsAllMultipleTablesData.Tables.Count > 0)
        //    {
        //        //if (dsTableLayout != null && dsTableLayout.Tables.Count > 0)
        //        //{
        //        //    if (dsTableLayout.Tables[0].Rows.Count > 0)
        //        //    {
        //        //        if (!string.IsNullOrEmpty(dsTableLayout.Tables[0].Rows[0][0].ToString()))
        //        //        {
        //        //            try
        //        //            {
        //        //                XmlDocument xmlDoc = new XmlDocument();
        //        //                xmlDoc.LoadXml(dsTableLayout.Tables[0].Rows[0][0].ToString());
        //        //                isVericalTable = Convert.ToBoolean(xmlDoc.DocumentElement.SelectSingleNode("VerticalTable").InnerText);
        //        //            }
        //        //            catch
        //        //            {
        //        //            }
        //        //        }
        //        //    }
        //        //}
        //        DataTable dtGroupNames = dsAllMultipleTablesData.Tables[0].DefaultView.ToTable(true, "GROUP_NAME", "GROUP_ID");
        //        strBuildXMLOutput.Append("<ATTRIBUTE_GROUP xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\"  COTYPE=\"\" TBGUID=\"M" + RandomClass.Next() + "\" aid:pstyle=\"attribute_group\">");
        //        if (dsAllMultipleTablesData.Tables[0].Rows.Count > 0)
        //        {
        //            foreach (DataRow item in dtGroupNames.Rows)
        //            {
        //                _SQLString = "SELECT CASE WHEN LAYOUT = 'Horizontal' THEN 'FALSE' ELSE 'TRUE' END FROM TB_ATTRIBUTE_GROUP WHERE GROUP_ID = " + item["GROUP_ID"];
        //                DataSet dsTableLayout = CreateDataSet();
        //                isVericalTable = Convert.ToBoolean(dsTableLayout.Tables[0].Rows[0][0].ToString());
        //                DataRow[] dtSingleTableData = dsAllMultipleTablesData.Tables[0].Select("GROUP_ID = " + item["GROUP_ID"] + "");
        //                // _SQLString = "SELECT A.ATTRIBUTE_ID,B.ATTRIBUTE_NAME,B.ATTRIBUTE_TYPE,B.STYLE_NAME,B.ATTRIBUTE_DATATYPE,B.STYLE_FORMAT FROM TB_ATTRIBUTE_GROUP_SPECS A,TB_ATTRIBUTE B WHERE GROUP_ID = " + item["GROUP_ID"] + " AND A.ATTRIBUTE_ID = B.ATTRIBUTE_ID ORDER BY A.SORT_ORDER";
        //                //if (flg != true)
        //                //{
        //                if (Attr_List != "")
        //                {
        //                    _SQLString = "EXEC('SELECT A.ATTRIBUTE_ID,B.ATTRIBUTE_NAME,B.ATTRIBUTE_TYPE,REPLACE(B.STYLE_NAME,'' '',''_'') as STYLE_NAME,B.ATTRIBUTE_DATATYPE,B.STYLE_FORMAT "
        //                                    + " FROM TB_ATTRIBUTE_GROUP_SPECS A JOIN TB_ATTRIBUTE B ON A.ATTRIBUTE_ID = B.ATTRIBUTE_ID "
        //                                    + " JOIN TB_CATALOG_ATTRIBUTES C ON A.ATTRIBUTE_ID=C.ATTRIBUTE_ID AND A.ATTRIBUTE_ID IN (" + Attr_List + ")"
        //                                    + " WHERE A.GROUP_ID = " + item["GROUP_ID"] + " AND A.ATTRIBUTE_ID = B.ATTRIBUTE_ID AND B.PUBLISH2PRINT = 1"
        //                                    + " AND C.CATALOG_ID=" + _catalogId + " ORDER BY A.SORT_ORDER')";

        //                }
        //                else
        //                {
        //                    _SQLString = "EXEC('SELECT A.ATTRIBUTE_ID,B.ATTRIBUTE_NAME,B.ATTRIBUTE_TYPE,REPLACE(B.STYLE_NAME,'' '',''_'') as STYLE_NAME,B.ATTRIBUTE_DATATYPE,B.STYLE_FORMAT "
        //                                    + " FROM TB_ATTRIBUTE_GROUP_SPECS A JOIN TB_ATTRIBUTE B ON A.ATTRIBUTE_ID = B.ATTRIBUTE_ID "
        //                                    + " JOIN TB_CATALOG_ATTRIBUTES C ON A.ATTRIBUTE_ID=C.ATTRIBUTE_ID "
        //                                    + " WHERE A.GROUP_ID = " + item["GROUP_ID"] + " AND A.ATTRIBUTE_ID = B.ATTRIBUTE_ID AND B.PUBLISH2PRINT = 1"
        //                                    + " AND C.CATALOG_ID=" + _catalogId + " ORDER BY A.SORT_ORDER')";
        //                }
        //                DataSet dsAttributeNames = CreateDataSet();
        //                strBuildXMLOutput.Append("<PRODUCT_GROUP xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" Format=\"SuperTable\" ncols=\"" + dsAttributeNames.Tables[0].Rows.Count + "\" nrows=\"" + (dtSingleTableData.Length + 1) + "\"   TBGUID=\"M" + RandomClass.Next() + "\" >");

        //                int i = 0;
        //                string sValue = "";
        //                if (!isVericalTable)
        //                {
        //                    strBuildXMLOutput.Append("<TABLE TBGUID=\"TA" + RandomClass.Next() + "\" Format=\"SuperTable\" nrows=\"" + (dtSingleTableData.Length + 1) + "\" ncols=\"" + dsAttributeNames.Tables[0].Rows.Count + "\" Transpose=\"0\" aid5:tablestyle=\"TABLE\" aid:trows=\"" + (dtSingleTableData.Length + 1) + "\" aid:table=\"table\" aid:tcols=\"" + dsAttributeNames.Tables[0].Rows.Count + "\"  >");
        //                    foreach (DataRow item1 in dsAttributeNames.Tables[0].Rows)
        //                    {
        //                        if (item1["STYLE_NAME"].ToString() == "")
        //                        {
        //                            cell_style_name = "Cell_Header";
        //                        }
        //                        else
        //                        {
        //                            cell_style_name = item1["STYLE_NAME"].ToString() + "_Header";
        //                        }

        //                        // DBConnection db = new DBConnection();
        //                        _SQLString = "IF OBJECT_ID('TEMPDB..##header') IS NOT NULL select 1 else select 2 ";
        //                        int result = ExecuteSQLScalarQuery();
        //                        if (result == 1)
        //                        {
        //                            strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid:theader=\"\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + item1["ATTRIBUTE_NAME"].ToString().Replace("&", "&amp;") + "</" + cell_style_name + ">");
        //                        }
        //                    }
        //                    foreach (DataRow dr in dtSingleTableData)
        //                    {
        //                        foreach (DataRow item4 in dsAttributeNames.Tables[0].Rows)
        //                        {
        //                            GetCurrencySymbol(item4["ATTRIBUTE_ID"].ToString());
        //                            int numberdecimel = 0;
        //                            if (item4["ATTRIBUTE_DATATYPE"].ToString().ToUpper().StartsWith("NUM"))
        //                            {
        //                                string dtype = item4["ATTRIBUTE_DATATYPE"].ToString();

        //                                if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
        //                                {
        //                                    numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
        //                                }
        //                                bool styleFormat = item4["STYLE_FORMAT"].ToString().Length > 0;
        //                                if (dr[item4["ATTRIBUTE_NAME"].ToString()] == DBNull.Value)
        //                                    sValue = null;
        //                                else
        //                                    sValue = dr[item4["ATTRIBUTE_NAME"].ToString()].ToString();
        //                                if (sValue != null)
        //                                {
        //                                    if (sValue.Trim().Length > 0)
        //                                    {
        //                                        if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
        //                                        {
        //                                            sValue = DecPrecisionFill(sValue, numberdecimel);
        //                                            sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(item4["ATTRIBUTE_ID"].ToString()), sValue.Trim()) : sValue;
        //                                        }
        //                                        else
        //                                        {
        //                                            if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000")
        //                                            {
        //                                                if (_replaceText != "")
        //                                                {
        //                                                    sValue = _emptyCondition == sValue ? _replaceText : sValue;
        //                                                }
        //                                            }
        //                                            if (Isnumber(sValue))
        //                                            {
        //                                                sValue = DecPrecisionFill(sValue, numberdecimel);
        //                                                sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(item4["ATTRIBUTE_ID"].ToString()), sValue.Trim()) : sValue;
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (dr[item4["ATTRIBUTE_NAME"].ToString()] == DBNull.Value)
        //                                    sValue = null;
        //                                else
        //                                {
        //                                    sValue = dr[item4["ATTRIBUTE_NAME"].ToString()].ToString();
        //                                    sValue = sValue.Replace("&", "&amp;");
        //                                }
        //                            }

        //                            if (item4["STYLE_NAME"].ToString() == "")
        //                            {
        //                                cell_style_name = "Cell";
        //                            }
        //                            else
        //                            {
        //                                cell_style_name = item4["STYLE_NAME"].ToString();
        //                            }
        //                            if (sValue != null)
        //                            {
        //                                if (dr[item4["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".jpg") || dr[item4["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".bmp") || dr[item4["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".eps") || dr[item4["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".psd") || dr[item4["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".png") || dr[item4["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".tif") || dr[item4["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".tiff") || dr[item4["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".gif"))
        //                                {
        //                                    strBuildXMLOutput.Append("<Cell TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item4["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" product_id=\"" + dr["PRODUCT_ID"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[]]><Image_Name TBGUID=\"CI" + RandomClass.Next() + "\" IMAGE_FILE=\"" + sValue + "\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" product_id=\"" + dr["PRODUCT_ID"] + "\"/></Cell>");
        //                                }
        //                                else
        //                                {
        //                                    if (cell_style_name == "Cell")
        //                                    {
        //                                        if (Convert.ToString(dr[item4["ATTRIBUTE_NAME"].ToString()]) != string.Empty && dr[item4["ATTRIBUTE_NAME"].ToString()] != "0.00" && dr[item4["ATTRIBUTE_NAME"].ToString()] != "0" && dr[item4["ATTRIBUTE_NAME"].ToString()].ToString() != "0.000000")
        //                                        {
        //                                            if (_headeroptions == "All")
        //                                                strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item4["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + cell_style_name + ">");
        //                                            else
        //                                            {
        //                                                if (i == 0)
        //                                                {
        //                                                    strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item4["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + cell_style_name + ">");
        //                                                }
        //                                                else
        //                                                {
        //                                                    strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item4["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + sValue + "]]></" + cell_style_name + ">");
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item4["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + _replaceText + "]]></" + cell_style_name + ">");
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        //GetCurrencySymbol(item4["ATTRIBUTE_ID"].ToString());
        //                                        if (Convert.ToString(dr[item4["ATTRIBUTE_NAME"].ToString()]) != string.Empty && dr[item4["ATTRIBUTE_NAME"].ToString()] != "0.00" && dr[item4["ATTRIBUTE_NAME"].ToString()] != "0" && dr[item4["ATTRIBUTE_NAME"].ToString()].ToString() != "0.000000")
        //                                        {
        //                                            if (_headeroptions == "All")
        //                                                strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item4["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _prefix + sValue + _suffix + "</" + cell_style_name + ">");
        //                                            else
        //                                            {
        //                                                if (i == 0)
        //                                                {
        //                                                    strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item4["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _prefix + sValue + _suffix + "</" + cell_style_name + ">");
        //                                                }
        //                                                else
        //                                                {
        //                                                    strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item4["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + sValue + "</" + cell_style_name + ">");
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
        //                                            {
        //                                                if (Isnumber(sValue))
        //                                                    sValue = DecPrecisionFill(sValue, 6);
        //                                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
        //                                                if (_replaceText != "")
        //                                                {
        //                                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
        //                                                }
        //                                                else
        //                                                {
        //                                                    _replaceText = sValue;
        //                                                }
        //                                                if (_fornumeric == "1")
        //                                                {
        //                                                    if (Isnumber(_replaceText.Replace(",", "")))
        //                                                        strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + _prefix + item4["ATTRIBUTE_ID"] + _suffix + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _prefix + _replaceText + _suffix + "</" + cell_style_name + ">");
        //                                                    else
        //                                                        strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item4["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _replaceText + "</" + cell_style_name + ">");
        //                                                }
        //                                                else
        //                                                    strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + _prefix + item4["ATTRIBUTE_ID"] + _suffix + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _prefix + _replaceText + _suffix + "</" + cell_style_name + ">");

        //                                            }
        //                                            strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item4["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _prefix + _replaceText + _suffix + "</" + cell_style_name + ">");
        //                                            //if (_headeroptions == "All")
        //                                            //    multipleTableValueToReplace += "<td class='reg'>" + _replaceText + "</td>";
        //                                            //else
        //                                            //{
        //                                            //    if (i == 0)
        //                                            //    {
        //                                            //        multipleTableValueToReplace += "<td class='reg'>" + _replaceText + "</td>";
        //                                            //    }
        //                                            //    else
        //                                            //    {
        //                                            //        multipleTableValueToReplace += "<td class='reg'>" + sValue + "</td>";
        //                                            //    }
        //                                            //}
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                //GetCurrencySymbol(item4["ATTRIBUTE_ID"].ToString());
        //                                if (_emptyCondition == "Null")
        //                                {
        //                                    sValue = _replaceText;
        //                                }
        //                                else
        //                                {
        //                                    sValue = "";
        //                                }
        //                                strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item4["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item4["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + sValue + "</" + cell_style_name + ">");
        //                            }
        //                        }
        //                        i++;
        //                    }
        //                    strBuildXMLOutput.Append("</TABLE>");
        //                    strBuildXMLOutput.Append("</PRODUCT_GROUP>");
        //                }
        //                else
        //                {
        //                    strBuildXMLOutput.Append("<TABLE TBGUID=\"TA" + RandomClass.Next() + "\" Format=\"SuperTable\" nrows=\"" + dsAttributeNames.Tables[0].Rows.Count + "\" ncols=\"" + (dtSingleTableData.Length + 1) + "\" Transpose=\"1\" aid5:tablestyle=\"TABLE\" aid:trows=\"" + dsAttributeNames.Tables[0].Rows.Count + "\" aid:table=\"table\" aid:tcols=\"" + (dtSingleTableData.Length + 1) + "\"  >");
        //                    foreach (DataRow item1 in dsAttributeNames.Tables[0].Rows)
        //                    {
        //                        if (item1["STYLE_NAME"].ToString() == "")
        //                        {
        //                            cell_style_name = "Cell_Header";
        //                        }
        //                        else
        //                        {
        //                            cell_style_name = item1["STYLE_NAME"].ToString() + "_Header";
        //                        }
        //                        strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid:theader=\"\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + item1["ATTRIBUTE_NAME"].ToString().Replace("&", "&amp;") + "</" + cell_style_name + ">");
        //                        if (item1["STYLE_NAME"].ToString() == "")
        //                        {
        //                            cell_style_name = "Cell";
        //                        }
        //                        else
        //                        {
        //                            cell_style_name = item1["STYLE_NAME"].ToString();
        //                        }
        //                        i = 0;
        //                        foreach (DataRow dr in dtSingleTableData)
        //                        {
        //                            GetCurrencySymbol(item1["ATTRIBUTE_ID"].ToString());
        //                            int numberdecimel = 0;
        //                            if (item1["ATTRIBUTE_DATATYPE"].ToString().ToUpper().StartsWith("NUM"))
        //                            {
        //                                string dtype = item1["ATTRIBUTE_DATATYPE"].ToString();

        //                                if (Isnumber(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1)))
        //                                {
        //                                    numberdecimel = Convert.ToInt16(dtype.Substring(dtype.IndexOf(",", StringComparison.Ordinal) + 1, 1));
        //                                }
        //                                bool styleFormat = item1["STYLE_FORMAT"].ToString().Length > 0;
        //                                if (dr[item1["ATTRIBUTE_NAME"].ToString()] == DBNull.Value)
        //                                    sValue = null;
        //                                else
        //                                    sValue = dr[item1["ATTRIBUTE_NAME"].ToString()].ToString();
        //                                if (sValue != null)
        //                                {
        //                                    if (sValue.Trim().Length > 0)
        //                                    {
        //                                        if (Convert.ToString(sValue) != string.Empty && sValue != "0.00" && sValue != "0" && sValue != "0.000000")
        //                                        {
        //                                            sValue = DecPrecisionFill(sValue, numberdecimel);
        //                                            sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(item1["ATTRIBUTE_ID"].ToString()), sValue.Trim()) : sValue;
        //                                        }
        //                                        else
        //                                        {
        //                                            if (_emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000")
        //                                            {
        //                                                if (_replaceText != "")
        //                                                {
        //                                                    sValue = _emptyCondition == sValue ? _replaceText : sValue;
        //                                                }
        //                                            }
        //                                            if (Isnumber(sValue))
        //                                            {
        //                                                sValue = DecPrecisionFill(sValue, numberdecimel);
        //                                                sValue = styleFormat ? ApplyStyleFormat(Convert.ToInt32(item1["ATTRIBUTE_ID"].ToString()), sValue.Trim()) : sValue;
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (dr[item1["ATTRIBUTE_NAME"].ToString()] == DBNull.Value)
        //                                    sValue = null;
        //                                else
        //                                {
        //                                    sValue = dr[item1["ATTRIBUTE_NAME"].ToString()].ToString();
        //                                    sValue = sValue.Replace("&", "&amp;");
        //                                }
        //                            }
        //                            if (sValue != null)
        //                            {
        //                                if (dr[item1["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".jpg") || dr[item1["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".bmp") || dr[item1["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".eps") || dr[item1["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".psd") || dr[item1["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".png") || dr[item1["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".tif") || dr[item1["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".tiff") || dr[item1["ATTRIBUTE_NAME"].ToString()].ToString().ToLower().EndsWith(".gif"))
        //                                {
        //                                    strBuildXMLOutput.Append("<Cell TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" product_id=\"" + dr["PRODUCT_ID"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[]]><Image_Name TBGUID=\"CI" + RandomClass.Next() + "\" IMAGE_FILE=\"" + sValue + "\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" product_id=\"" + dr["PRODUCT_ID"] + "\"/></Cell>");
        //                                }
        //                                else
        //                                {
        //                                    if (cell_style_name == "Cell")
        //                                    {
        //                                        if (Convert.ToString(dr[item1["ATTRIBUTE_NAME"].ToString()]) != string.Empty && dr[item1["ATTRIBUTE_NAME"].ToString()] != "0.00" && dr[item1["ATTRIBUTE_NAME"].ToString()] != "0" && dr[item1["ATTRIBUTE_NAME"].ToString()].ToString() != "0.000000")
        //                                        {
        //                                            if (_headeroptions == "All")
        //                                                strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + cell_style_name + ">");
        //                                            else
        //                                            {
        //                                                if (i == 0)
        //                                                {
        //                                                    strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + _prefix + sValue + _suffix + "]]></" + cell_style_name + ">");
        //                                                }
        //                                                else
        //                                                {
        //                                                    strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + sValue + "]]></" + cell_style_name + ">");
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + _replaceText + "]]></" + cell_style_name + ">");

        //                                        }
        //                                        //strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr[item1["ATTRIBUTE_NAME"].ToString()] + "]]></" + cell_style_name + ">");
        //                                    }
        //                                    else
        //                                    {
        //                                        //GetCurrencySymbol(item1["ATTRIBUTE_ID"].ToString());
        //                                        if (Convert.ToString(dr[item1["ATTRIBUTE_NAME"].ToString()]) != string.Empty && dr[item1["ATTRIBUTE_NAME"].ToString()] != "0.00" && dr[item1["ATTRIBUTE_NAME"].ToString()] != "0" && dr[item1["ATTRIBUTE_NAME"].ToString()].ToString() != "0.000000")
        //                                        {
        //                                            if (_headeroptions == "All")
        //                                                strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _prefix + sValue + _suffix + "</" + cell_style_name + ">");
        //                                            else
        //                                            {
        //                                                if (i == 0)
        //                                                {
        //                                                    strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _prefix + sValue + _suffix + "</" + cell_style_name + ">");
        //                                                }
        //                                                else
        //                                                {
        //                                                    strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + sValue + "</" + cell_style_name + ">");
        //                                                }
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            if ((_emptyCondition == "Null" || _emptyCondition == "Empty" || _emptyCondition == "0" || _emptyCondition == "0.00" || _emptyCondition == "0.000000"))
        //                                            {
        //                                                if (Isnumber(sValue))
        //                                                    sValue = DecPrecisionFill(sValue, 6);
        //                                                if (_emptyCondition == "Empty") { _emptyCondition = ""; }
        //                                                if (_replaceText != "")
        //                                                {
        //                                                    _replaceText = _emptyCondition == sValue ? _replaceText : sValue;
        //                                                }
        //                                                else
        //                                                {
        //                                                    _replaceText = sValue;
        //                                                }
        //                                                if (_fornumeric == "1")
        //                                                {
        //                                                    if (Isnumber(_replaceText.Replace(",", "")))
        //                                                        strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _prefix + _replaceText + _suffix + "</" + cell_style_name + ">");
        //                                                    else
        //                                                        strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _replaceText + "</" + cell_style_name + ">");
        //                                                }
        //                                                else
        //                                                    strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _prefix + _replaceText + _suffix + "</" + cell_style_name + ">");

        //                                            }
        //                                            //strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + _replaceText + "</" + cell_style_name + ">");

        //                                        }
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                //GetCurrencySymbol(item1["ATTRIBUTE_ID"].ToString());
        //                                if (_emptyCondition == "Null")
        //                                {
        //                                    sValue = _replaceText;
        //                                }
        //                                else
        //                                {
        //                                    sValue = "";
        //                                }
        //                                strBuildXMLOutput.Append("<" + cell_style_name + " TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" attribute_id=\"" + item1["ATTRIBUTE_ID"] + "\" aid:table=\"cell\" Attribute_type=\"" + item1["ATTRIBUTE_TYPE"] + "\" aid5:cellstyle=\"" + cell_style_name + "\" aid:crows=\"1\" aid:ccols=\"1\">" + sValue + "</" + cell_style_name + ">");
        //                            }
        //                            //strBuildXMLOutput.Append("<Cell TBGUID=\"TPS" + RandomClass.Next() + "\" COTYPE=\"CELL\" aid:table=\"cell\" aid5:cellstyle=\"Cell\" aid:crows=\"1\" aid:ccols=\"1\"><![CDATA[" + dr[item1["ATTRIBUTE_NAME"].ToString()] + "]]></Cell>");
        //                            i++;
        //                        }

        //                    }

        //                    strBuildXMLOutput.Append("</TABLE>");
        //                    strBuildXMLOutput.Append("</PRODUCT_GROUP>");
        //                }
        //            }
        //        }
        //        strBuildXMLOutput.Append("</ATTRIBUTE_GROUP>");
        //    }
        //}
        [HttpGet]
        public IList GetAllInDesignAttributes1(int catalogId, string familyId)
        {
            try
            {
                _SQLString = "  SELECT DISTINCT TA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME,TA.ATTRIBUTE_TYPE FROM TB_ATTRIBUTE TA JOIN TB_CATALOG_ATTRIBUTES TCA "
                           + " ON TCA.ATTRIBUTE_ID=TA.ATTRIBUTE_ID AND TCA.CATALOG_ID=" + catalogId + " "
                           + " JOIN TB_PROD_FAMILY_ATTR_LIST TPAL ON TPAL.ATTRIBUTE_ID=TA.ATTRIBUTE_ID WHERE TPAL.FAMILY_ID IN("
                           + " SELECT FAMILY_ID FROM TB_FAMILY WHERE FAMILY_ID IN (" + familyId + ") and FLAG_RECYCLE='A'"
                           + " UNION "
                           + " SELECT FAMILY_ID FROM TB_FAMILY WHERE PARENT_FAMILY_ID IN (" + familyId + ") and FLAG_RECYCLE='A') and FLAG_RECYCLE='A'";
                ////DataSet _dtAvalAttr = Con.CreateDataSet();
                _dtAvailAttr = CreateDataSet();
                var prodspecAttributesPublished = _dtAvailAttr.Tables[0].AsEnumerable().Select(dr =>
               new
               {
                   CATALOG_ID = catalogId,
                   FAMILY_ID = familyId,
                   ATTRIBUTE_ID = Convert.ToInt64(dr.Field<int>("ATTRIBUTE_ID")),
                   ATTRIBUTE_NAME = dr.Field<string>("ATTRIBUTE_NAME"),
                   ATTRIBUTE_TYPE = dr.Field<byte>("ATTRIBUTE_TYPE")//,
                   //  SORT_ORDER = Convert.ToInt64(dr.Field<int>("SORT_ORDER")),
                   // ATTRIBUTE_TYPE = Convert.ToInt64(dr.Field<int>("ATTRIBUTE_TYPE")),
                   //  ISAvailable = true
               }
               ).Distinct().ToList();

                int intFamilyId;
                int.TryParse(familyId.Trim('~'), out intFamilyId);
                int[] attributeType = { 9, 7, 11, 13 };
                var prodspecAttributes = _dbcontext.TB_FAMILY_SPECS.Join(_dbcontext.TB_CATALOG_ATTRIBUTES, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a, b })
                        .Join(_dbcontext.TB_ATTRIBUTE, x => x.a.ATTRIBUTE_ID, y => y.ATTRIBUTE_ID, (x, y) => new { x, y })
                        .Select(z => new
                        {
                            z.x.b.CATALOG_ID,
                            z.x.a.FAMILY_ID,
                            z.x.a.ATTRIBUTE_ID,
                            z.y.ATTRIBUTE_NAME,
                            z.y.ATTRIBUTE_TYPE,
                            ISAvailable = false
                        }).Distinct();

                var catalogs = _dbcontext.TB_CATALOG_ATTRIBUTES.Where(x => x.CATALOG_ID == catalogId && attributeType.Contains(x.TB_ATTRIBUTE.ATTRIBUTE_TYPE)).Select(x => new
                {
                    x.CATALOG_ID,
                    FAMILY_ID = intFamilyId,
                    x.TB_ATTRIBUTE.ATTRIBUTE_ID,
                    x.TB_ATTRIBUTE.ATTRIBUTE_NAME,
                    x.TB_ATTRIBUTE.ATTRIBUTE_TYPE,
                    ISAvailable = false
                }).Distinct();

                var catalogallAttribtues = catalogs.Except(prodspecAttributes).ToList();
                return catalogallAttribtues.OrderBy(y => y.ATTRIBUTE_NAME).ToList();
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController : GetAllCatalogattributes", objException);
                return null;
            }
        }
        private void GetCurrencySymbol(string attrID)
        {
            string sqlStr = "SELECT ATTRIBUTE_DATARULE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + attrID + " and FLAG_RECYCLE='A'";
            _SQLString = sqlStr;
            DataSet dscUrrency = CreateDataSet();
            _prefix = string.Empty; _suffix = string.Empty; _emptyCondition = string.Empty; _replaceText = string.Empty; _headeroptions = string.Empty; _fornumeric = string.Empty;
            if (dscUrrency.Tables[0].Rows.Count > 0)
            {
                if (dscUrrency.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                {
                    string xmLstr = dscUrrency.Tables[0].Rows[0].ItemArray[0].ToString();
                    var xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(xmLstr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    {
                        if (rootNode != null)
                        {
                            XmlNodeList xmlNodeList = rootNode.ChildNodes;

                            for (int xmlNode = 0; xmlNode < xmlNodeList.Count; xmlNode++)
                            {
                                if (xmlNodeList[xmlNode].ChildNodes.Count > 0)
                                {
                                    if (xmlNodeList[xmlNode].ChildNodes[0].LastChild != null)
                                    {
                                        _prefix = xmlNodeList[xmlNode].ChildNodes[0].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[1].LastChild != null)
                                    {
                                        _suffix = xmlNodeList[xmlNode].ChildNodes[1].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[2].LastChild != null)
                                    {
                                        _emptyCondition = xmlNodeList[xmlNode].ChildNodes[2].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[3].LastChild != null)
                                    {
                                        _replaceText = xmlNodeList[xmlNode].ChildNodes[3].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[4].LastChild != null)
                                    {
                                        _headeroptions = xmlNodeList[xmlNode].ChildNodes[4].LastChild.Value;
                                    }
                                    if (xmlNodeList[xmlNode].ChildNodes[5].LastChild != null)
                                    {
                                        _fornumeric = xmlNodeList[xmlNode].ChildNodes[5].LastChild.Value;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public bool Isnumber(string refValue)
        {
            if (refValue == "")
            {
                return false;
            }
            //const string strReg =
            //    @"^((\d?)|(([-+]?\d+\.?\d*)|([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d+\.?\d*))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d+\.?\d*\,\ ?)*([-+]?\d*\.?\d+))|(([-+]?\d*\.?\d+\,\ ?)*([-+]?\d+\.?\d*)))$";
            var retval = false;
            //var res = new Regex(strReg);
            //if (res.IsMatch(refValue))
            //{
            //    retval = true;
            //}
            //return retval;
            const string strRegx = @"^[0-9]*(\.)?[0-9]+$";
            var re = new Regex(strRegx);
            if (re.IsMatch(refValue))
            {
                retval = true;
            }
            return retval;
        }
        private string DecPrecisionFill(string strvalue, int noofdecimal)
        {
            if (strvalue.IndexOf(".", StringComparison.Ordinal) > 0)
            {
                if ((strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1) > 0)
                {
                    if (Convert.ToDecimal(strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1)) == 0)
                    {
                        strvalue = strvalue.Substring(0, strvalue.IndexOf(".", StringComparison.Ordinal));
                        string retVal = "";
                        for (int preadd = 0; preadd < noofdecimal; preadd++)
                        { retVal = retVal + "0"; }
                        if (retVal != "")
                        {
                            strvalue = strvalue + "." + retVal;
                        }
                    }
                    else if (Convert.ToDecimal(strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, strvalue.Length - strvalue.IndexOf(".", StringComparison.Ordinal) - 1)) > 0)
                    {
                        if (strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1).Length >= noofdecimal)
                            strvalue = strvalue.Substring(0, strvalue.IndexOf(".", StringComparison.Ordinal)) + "." + (strvalue.Substring(strvalue.IndexOf(".", StringComparison.Ordinal) + 1, noofdecimal));
                    }
                }
            }
            //noofdecimal = noofdecimal + 1;
            return strvalue;
        }
        private string ApplyStyleFormat(int Attribute_id, string NumericValue)
        {
            DataSet styleDS = new DataSet();
            double dt = 0;
            string style = string.Empty;
            _SQLString = "SELECT STYLE_FORMAT,ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + Attribute_id + " and FLAG_RECYCLE='A'";
            styleDS = CreateDataSet();
            style = styleDS.Tables[0].Rows[0].ItemArray[0].ToString();
            int index = 0;
            index = style.IndexOf("[");

            if (index != -1)
            {
                style = style.Substring(0, index - 1);
                dt = Convert.ToDouble(NumericValue.Trim());
            }
            else
            {
                if (NumericValue.Trim().Length > 0)
                {
                    try
                    {
                        dt = Convert.ToDouble(NumericValue.Trim());
                    }
                    catch (Exception)
                    {
                        return "0";
                    }
                    return dt.ToString();
                }
                else
                    return "0";
            }

            return dt.ToString(style.Trim());

        }
        public string Valchk(string val, string Vsize)
        {
            string dr12 = Vsize.Substring(6, (Vsize.Length - 6));
            dr12 = dr12.Replace("(", "").Replace(")", "");
            string[] _data_Val1 = dr12.Split(',');
            string tempValue1 = val;
            string[] _temp_Val1 = tempValue1.Split('.');
            if (_temp_Val1.Length > 1)
            {
                if ((_temp_Val1[0].Length > Convert.ToInt32(_data_Val1[0].ToString())))
                {
                    if (_data_Val1.Length > 1)
                    {
                        if (_temp_Val1[1].Length > Convert.ToInt32(_data_Val1[1].ToString()))
                        {
                            tempValue1 = _temp_Val1[0].ToString().Substring(0, Convert.ToInt32(_data_Val1[0].ToString())) + "." + _temp_Val1[1].ToString().Substring(0, Convert.ToInt32(_data_Val1[1].ToString()));
                        }
                    }
                    else
                    {
                        tempValue1 = _temp_Val1[0].ToString().Substring(0, Convert.ToInt32(_data_Val1[0].ToString())) + "." + _temp_Val1[1].ToString().Substring(0, Convert.ToInt32(_data_Val1[0].ToString()));
                    }
                }
                else
                {
                    if (_temp_Val1[1].Length > Convert.ToInt32(_data_Val1[1].ToString()))
                    {
                        tempValue1 = _temp_Val1[0].ToString() + "." + _temp_Val1[1].ToString().Substring(0, Convert.ToInt32(_data_Val1[1].ToString()));
                    }
                    else
                    {
                        tempValue1 = _temp_Val1[0].ToString() + "." + _temp_Val1[1].ToString();
                    }

                }
            }
            else
            {
                if ((_temp_Val1[0].Length > Convert.ToInt32(_data_Val1[0].ToString())))
                {
                    tempValue1 = _temp_Val1[0].ToString().Substring(0, Convert.ToInt32(_data_Val1[0].ToString()));
                }
                else
                {
                    if (_temp_Val1[0].ToString().Length > 0)
                        tempValue1 = _temp_Val1[0].ToString();
                }
            }
            if (_temp_Val1[0].ToString().Length > 0)
            {
                if (tempValue1.Substring(tempValue1.Length - 1) == ".")
                {
                    tempValue1 = tempValue1.Substring(0, tempValue1.Length - 1);
                }
            }
            return tempValue1;
        }
        public string XMLStringOutput(int FamilyId)
        {

            DataSet DScoreHeaderId = new DataSet();
            DataSet DScoreLayout = new DataSet();
            bool HeaderVal;
            try
            {
                //string SQLStr = " SELECT  DISPLAY_TABLE_HEADER FROM  TB_FAMILY where family_id=" + FamilyId;
                //AppLoader.DBConnection Ocon = new DBConnection();
                // AppLoader.DBConnection oConHelper = new DBConnection();

                //_SQLString = SQLStr;
                //DScoreHeaderId = CreateDataSet(); 

                //HeaderVal = Convert.ToBoolean(DScoreHeaderId.Tables[0].Rows[0].ItemArray[0].ToString());
                HeaderVal = true;
                if (HeaderVal == true)
                {
                    HeaderID = 1;
                }
                else { HeaderID = 0; }


                ///test
                XmlDocument xmlDOc = new XmlDocument();
                StringBuilder DynamicSQl = new StringBuilder();
                _SQLString = " SELECT PRODUCT_FILTERS FROM TB_CATALOG WHERE  CATALOG_ID = " + _catalogId + " and FLAG_RECYCLE='A'";
                oDsProdFilter = CreateDataSet();
                string sProdFilter = string.Empty;
                sProdFilter = oDsProdFilter.Tables[0].Rows[0].ItemArray[0].ToString();
                if (sProdFilter != string.Empty)
                {

                    xmlDOc.LoadXml(sProdFilter);
                    XmlNode rNode = xmlDOc.DocumentElement;

                    //if (rNode.ChildNodes.Count > 0)
                    //{
                    //    for (int i = 0; i < rNode.ChildNodes.Count; i++)
                    //    {
                    //        StringBuilder SQLstring = new StringBuilder();
                    //        XmlNode TableDataSetNode = rNode.ChildNodes[i];
                    //        if (TableDataSetNode.HasChildNodes)
                    //        {
                    //            SQLstring.Append("SELECT PRODUCT_ID FROM TB_PROD_SPECS WHERE STRING_VALUE " + TableDataSetNode.ChildNodes[2].InnerText + " '" + TableDataSetNode.ChildNodes[3].InnerText.Trim() + "'  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "");
                    //        }
                    //        if (TableDataSetNode.ChildNodes[4].InnerText == "None")
                    //        {
                    //            DynamicSQl.Append(SQLstring);
                    //            break;
                    //        }
                    //        if (i > 0)
                    //        {
                    //            DynamicSQl.Append(" union ");
                    //        }

                    //        DynamicSQl.Append(SQLstring);
                    //    }
                    //}
                    if (rNode.ChildNodes.Count > 0)
                    {
                        for (int i = 0; i < rNode.ChildNodes.Count; i++)
                        {
                            StringBuilder SQLstring = new StringBuilder();
                            XmlNode TableDataSetNode = rNode.ChildNodes[i];
                            if (TableDataSetNode.HasChildNodes)
                            {

                                if (TableDataSetNode.ChildNodes[2].InnerText == " ")
                                {
                                    TableDataSetNode.ChildNodes[2].InnerText = "=";
                                }
                                string StrVal = TableDataSetNode.ChildNodes[3].InnerText.Trim().Replace("'", "''");

                                DataSet oDsattrtype = new DataSet();
                                // StringBuilder DynamicattrSQl = new StringBuilder();
                                _SQLString = " SELECT attribute_datatype FROM TB_attribute WHERE  attribute_ID = " + TableDataSetNode.ChildNodes[0].InnerText + " and FLAG_RECYCLE='A'";
                                oDsattrtype = CreateDataSet();
                                if (oDsattrtype.Tables[0].Rows[0][0].ToString().ToUpper().StartsWith("N") == false)
                                {
                                    //SQLstring.Append("SELECT PRODUCT_ID FROM TB_PROD_SPECS WHERE Numeric_VALUE  " + TableDataSetNode.ChildNodes[2].InnerText + " " + StrVal + "  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "");
                                    SQLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + _catalogId + ") WHERE (STRING_VALUE " + TableDataSetNode.ChildNodes[2].InnerText + " '" + StrVal + "' AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");
                                }
                                else
                                {
                                    //SQLstring.Append("SELECT PRODUCT_ID FROM TB_PROD_SPECS WHERE STRING_VALUE  " + TableDataSetNode.ChildNodes[2].InnerText + " N'" + StrVal + "'  AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + "");
                                    SQLstring.Append("SELECT DISTINCT PRODUCT_ID FROM [PRODUCT SPECIFICATION](" + _catalogId + ") WHERE  (NUMERIC_VALUE " + TableDataSetNode.ChildNodes[2].InnerText + " '" + StrVal + "' AND ATTRIBUTE_ID = " + TableDataSetNode.ChildNodes[0].InnerText + ") " + "\n");

                                }
                            }
                            if (TableDataSetNode.ChildNodes[4].InnerText == "NONE")
                            {
                                DynamicSQl.Append(SQLstring);
                                break;
                            }
                            DynamicSQl.Append(SQLstring);
                            if (TableDataSetNode.ChildNodes[4].InnerText == "AND")
                            {

                                DynamicSQl.Append(" INTERSECT \n");
                            }
                            if (TableDataSetNode.ChildNodes[4].InnerText == "OR")
                            {
                                DynamicSQl.Append(" UNION \n");
                            }

                            //if (i > 0)
                            //{
                            //    DynamicSQl.Append(" union ");
                            //}
                        }
                    }

                }
                DataSet DSfilter = new DataSet();
                _SQLString = DynamicSQl.ToString();
                if (DynamicSQl.ToString() != string.Empty)
                {
                    DSfilter = CreateDataSet();
                }
                ChkAttributeExistinFamily();

                string SQLstr = " EXEC STP_CATALOGSTUDIO5_ProductTableXml  " + _catalogId + " , " + FamilyId + " , '" + DynamicSQl + "' , " + ContinueCalclautedCols + " ";
                //test

                //this.Controls.Add(GrdObj);
                // this.Controls.Add(GrdObjj);
                DataSet DS = new DataSet();
                int[] attrList = new int[0];
                string con = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                TradingBell.CatalogX.CatalogXfunction ofrm = new TradingBell.CatalogX.CatalogXfunction();
                DS = ofrm.CatalogProductPreviewX(Convert.ToInt32(_catalogId), Convert.ToInt32(FamilyId), attrList, con);


                if (DSfilter != null && DSfilter.Tables.Count > 0)
                {
                    if (DSfilter.Tables[0].Rows.Count > 0)
                    {
                        DataSet temp = DS;
                        DataSet Dspreviewaltered = new DataSet();
                        DataTable t1 = new DataTable();
                        Dspreviewaltered.Tables.Add(DS.Tables[0].Clone());

                        foreach (DataRow DR in DS.Tables[0].Rows)
                        {
                            DataRow[] DrfilteredRows = DSfilter.Tables[0].Select("PRODUCT_ID = " + DR["PRODUCT_ID"]);
                            if (DrfilteredRows != null && DrfilteredRows.Length != 0)
                            {
                                //DataRow datarw = t1.NewRow();
                                Dspreviewaltered.Tables[0].ImportRow(DR);
                            }

                        }
                        DS = new DataSet();
                        DS.Tables.Add(Dspreviewaltered.Tables[0].Clone());
                        foreach (DataRow DR in Dspreviewaltered.Tables[0].Rows)
                        {
                            DS.Tables[0].ImportRow(DR);
                        }
                        DataTable dsf = temp.Tables[1].Clone();
                        foreach (DataRow Dr in temp.Tables[1].Rows)
                        {
                            DataRow DRTEMP = dsf.NewRow();
                            dsf.ImportRow(Dr);
                        }
                        DS.Tables.Add(dsf);
                    }
                }
                //end filter 

                //for Publish
                _SQLString = " SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID IN ( SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_FAMILY_ATTR_LIST WHERE FAMILY_ID = " + FamilyId + "  ) and tp.FLAG_RECYCLE='A'";
                DataSet DSpublish = new DataSet();
                DSpublish = CreateDataSet();
                if (DSpublish.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow Dr in DSpublish.Tables[0].Rows)
                    {
                        if (DS.Tables[0].Columns.Contains(Dr[0].ToString()))
                        {
                            DS.Tables[0].Columns[Dr[0].ToString()].Caption = "`" + DS.Tables[0].Columns[Dr[0].ToString()].Caption;
                        }
                    }
                    for (int i = 0; i < DS.Tables[0].Columns.Count; i++)
                    {
                        if (DS.Tables[0].Columns[i].Caption.Contains("`") == false)
                        {
                            if (DS.Tables[0].Columns[i].Caption != "CATALOG_ID" && DS.Tables[0].Columns[i].Caption != "FAMILY_ID" && DS.Tables[0].Columns[i].Caption != "PRODUCT_ID" && DS.Tables[0].Columns[i].Caption != "Publish" && DS.Tables[0].Columns[i].Caption != "Sort")
                            {
                                DS.Tables[0].Columns.RemoveAt(i);
                                //DS.Tables[1].Columns.RemoveAt(i);
                                i = 0;
                            }

                        }

                    }
                    for (int i = 0; i < DS.Tables[0].Columns.Count; i++)
                    {
                        if (DS.Tables[0].Columns[i].Caption.Contains("`") == true)
                        {
                            if (DS.Tables[0].Columns[i].Caption != "CATALOG_ID" && DS.Tables[0].Columns[i].Caption != "FAMILY_ID" && DS.Tables[0].Columns[i].Caption != "PRODUCT_ID" && DS.Tables[0].Columns[i].Caption != "Publish" && DS.Tables[0].Columns[i].Caption != "Sort")
                            {
                                DS.Tables[0].Columns[i].Caption = DS.Tables[0].Columns[i].Caption.Remove(0, 1);
                            }

                        }

                    }

                }
                //end for publish
                /////////////////////
                if (DS.Tables[0].Rows.Count > 0)
                {
                    DS.Tables[0].Columns.Add(new DataColumn("Sort1", typeof(System.Int32)));
                    foreach (DataRow DR in DS.Tables[0].Rows)
                    {
                        DR["Sort1"] = Convert.ToInt32(DR["SORT"].ToString());
                    }
                }
                /////////////////////////
                DataSet Dsp = DS;
                /////////////////////////
                DataTable et = new DataTable("ProductTable");
                foreach (DataColumn DC in Dsp.Tables[0].Columns)
                {
                    if (DC.Caption != "CATALOG_ID" && DC.Caption != "Publish" && DC.Caption != "FAMILY_ID" && DC.Caption != "PRODUCT_ID" && DC.Caption != "Sort" && DC.Caption != "Sort1")
                        et.Columns.Add(DC.Caption, DC.DataType);
                }
                DataRow[] DRRR = DS.Tables[0].Select("SORT1 >0", "SORT1");
                foreach (DataRow DR in DRRR)
                    et.ImportRow(DR);
                DS = new DataSet();
                DS.Tables.Add(et);
                DS.Tables.Add(Dsp.Tables[1].Clone());
                foreach (DataRow DR in Dsp.Tables[1].Rows)
                    DS.Tables[1].ImportRow(DR);
                /////////////////////
                //DS.Tables[0].Columns.Remove("CATALOG_ID");
                //DS.Tables[0].Columns.Remove("FAMILY_ID");
                //DS.Tables[0].Columns.Remove("PRODUCT_ID");
                //DS.Tables[0].Columns.Remove("Sort");
                //DS.Tables[0].Columns.Remove("Publish");

                //GrdObj.DataSource = DS;


                string SQLStr = " SELECT  FAMILY_TABLE_STRUCTURE FROM  TB_FAMILY_TABLE_STRUCTURE where family_id = " + FamilyId + " AND CATALOG_ID=" + _catalogId + " AND IS_DEFAULT=1";
                _SQLString = SQLStr;
                DScoreLayout = CreateDataSet();

                string TMode = string.Empty;
                string LayoutXML = string.Empty;
                foreach (DataRow DrColumnLay in DScoreLayout.Tables[0].Rows)
                {
                    LayoutXML = DrColumnLay[0].ToString();
                }
                ///
                //  XmlDocument xmlDOc = new XmlDocument();
                xmlDOc.LoadXml(LayoutXML);
                XmlNode rootNode = xmlDOc.DocumentElement;
                if (rootNode.Attributes["TableType"].Value == "Grouped")
                {
                    XmlNodeList xmlNodeList;
                    xmlNodeList = rootNode.ChildNodes;

                    for (int i = 0; i < xmlNodeList.Count; i++)
                    {
                        if (xmlNodeList[i].Name == "LayoutXML")
                        {
                            if (xmlNodeList[i].ChildNodes.Count > 0)
                            {
                                LayoutXML = xmlNodeList[i].ChildNodes[0].Value;
                            }
                        }
                    }
                }
                if (rootNode.Attributes["TableType"].Value == "Grouped")
                {
                    TableMode = "Grouped";
                }
                else if (rootNode.Attributes["TableType"].Value == "Horizontal")
                {
                    TableMode = "Horizontal";
                }
                else
                {
                    TableMode = "vertical";
                }

                string xmlDir = HttpContext.Current.Server.MapPath("~/Content/IndesignXML");
                TextWriter sfile = new StreamWriter(xmlDir + "\\test.xml");
                sfile.WriteLine(LayoutXML);
                sfile.Close();
                DupDsPreview = DS.Copy();
                foreach (DataColumn DC in DupDsPreview.Tables[0].Columns)
                {
                    DC.ColumnName = DC.ColumnName.Replace("\r", "").Replace("\n", "");
                }
                foreach (DataColumn DC in DupDsPreview.Tables[1].Columns)
                {
                    DC.ColumnName = DC.ColumnName.Replace("\r", "").Replace("\n", "");
                }
                // GrdObjj.DataSource = DupDsPreview;
                // GrdObjj.DisplayLayout.LoadFromXml(Application.LocalUserAppDataPath + "\\test.xml");

                //GrdObj.DisplayLayout.LoadFromXml(Application.LocalUserAppDataPath + "\\test.xml");
                XML = "";
                XML = GenXML(DupDsPreview);
                // this.Controls.Remove(GrdObj);
            }
            catch (System.Exception)
            {

            }
            return XML;

        }


        private String GenXML(DataSet DupDsPreview)
        {
            //// if (GrdObj.DisplayLayout.Rows.Count == 0)
            //// return "";
            //cellvalues = ""; strBuildXMLOutput1.Remove(0, strBuildXMLOutput1.Length);
            //int GrpExtcheck; GrpExtcheck = 0; grpno = 0; string SQLString;
            //cellname = new string[DupDsPreview.Tables[0].Columns.Count]; //GrdObj.DisplayLayout.Bands[0].Columns.Count];
            //int[] a = new int[DupDsPreview.Tables[0].Columns.Count]; //GrdObj.DisplayLayout.Bands[0].Columns.Count];
            //int[] b = new int[DupDsPreview.Tables[0].Columns.Count]; //GrdObj.DisplayLayout.Bands[0].Columns.Count];
            //int[] GrpCol = new int[DupDsPreview.Tables[0].Columns.Count]; //GrdObj.DisplayLayout.Bands[0].Columns.Count];
            //cellposition = new int[DupDsPreview.Tables[0].Columns.Count]; //GrdObj.DisplayLayout.Bands[0].Columns.Count];
            //cellposval = new int[DupDsPreview.Tables[0].Columns.Count]; //GrdObj.DisplayLayout.Bands[0].Columns.Count];
            //GrpColno = new int[DupDsPreview.Tables[0].Columns.Count]; //GrdObj.DisplayLayout.Bands[0].Columns.Count];
            //rowlevel = new int[DupDsPreview.Tables[0].Columns.Count+1]; //GrdObj.DisplayLayout.Bands[0].Columns.Count + 1];
            //try
            //{
            //    for (int k = 0; k < DupDsPreview.Tables[0].Columns.Count; k++) //GrdObj.DisplayLayout.Bands[0].Columns.Count; k++)
            //    { b[k] = 0; }

            //    //groupby column finding 
            //    for (int GrpColcnt = 0, grpcol = 0; GrpColcnt < DupDsPreview.Tables[0].Columns.Count; GrpColcnt++) //GrdObjj.DisplayLayout.Bands[0].Columns.Count; GrpColcnt++)
            //    {
            //        if (GrdObjj.DisplayLayout.Bands[0].Columns[GrpColcnt].IsGroupByColumn)
            //        {
            //            GrpCol[grpcol] = 0; grpcol++;
            //            GrpExtcheck = GrpCol.Length;
            //            cellposval[GrpColcnt] = -1; GrpColno[GrpColcnt] = GrpColcnt;
            //        }
            //        else
            //        {
            //            GrpColno[GrpColcnt] = GrpColcnt; grpno++;
            //            cellposval[GrpColcnt] = 0;
            //        }
            //        cellposition[GrpColcnt] = GrdObjj.DisplayLayout.Bands[0].Columns[GrpColcnt].Header.VisiblePosition;
            //    }

            //    for (int CellPos = 0; CellPos < GrdObj.DisplayLayout.Bands[0].Columns.Count; CellPos++)
            //    {
            //        for (int CellValue = 0; CellValue < GrdObj.DisplayLayout.Bands[0].Columns.Count; CellValue++)
            //        {
            //            if (GrpColno[CellPos] == cellposition[CellValue])
            //            { GrpColno[CellPos] = CellValue; break; }
            //        }
            //    }
            //    for (int CellPos = 0; CellPos < GrdObj.DisplayLayout.Bands[0].Columns.Count; CellPos++)
            //    {
            //        if (cellposval[CellPos] == -1)
            //        {
            //            for (int CellValue = 0; CellValue < GrdObj.DisplayLayout.Bands[0].Columns.Count; CellValue++)
            //            {
            //                if (GrpColno[CellValue] == CellPos)
            //                { GrpColno[CellValue] = -1; break; }
            //            }
            //        }
            //    }
            //    int tottsize = GrdObj.DisplayLayout.Bands[0].Columns.Count;
            //    while (tottsize > 0)
            //    {
            //        for (int CellPos = 0; CellPos < GrdObj.DisplayLayout.Bands[0].Columns.Count; CellPos++)
            //        {
            //            if (GrpColno[CellPos] == -1)
            //            {
            //                for (int CellValue = CellPos; CellValue < tottsize - 1; CellValue++)
            //                {
            //                    GrpColno[CellValue] = GrpColno[CellValue + 1];
            //                }
            //            }
            //        } tottsize--;
            //    }

            //    SQLString = "select style_name,attribute_name from tb_attribute where attribute_id in " +
            //    "(SELECT  attribute_id FROM  TB_ATTRIBUTE WHERE " +
            //     " attribute_id IN  (SELECT DISTINCT attribute_id FROM " +
            //     " TB_PROD_FAMILY_ATTR_LIST WHERE family_id = " + FamilyId + "))";
            //    DataSet DScoreCell = new DataSet();
            //    //   AppLoader.DBConnection oConHelper = new DBConnection();
            //    _SQLString = SQLString;
            //    DScoreCell =  CreateDataSet();  

            //    for (int Cellna = 0; Cellna < grpno; Cellna++)
            //    {
            //        if (DScoreCell.Tables[0].Rows.Count != 0)
            //        {
            //            for (int CellnaPos = 0; CellnaPos < GrdObj.DisplayLayout.Bands[0].Columns.Count; CellnaPos++)
            //            {
            //                if ((GrdObj.DisplayLayout.Bands[0].Columns[GrpColno[Cellna]].ToString() == DScoreCell.Tables[0].Rows[CellnaPos].ItemArray[1].ToString()))
            //                {
            //                    if (DScoreCell.Tables[0].Rows[CellnaPos].ItemArray[0].ToString() != "")
            //                    {
            //                        cellname[Cellna] = DScoreCell.Tables[0].Rows[CellnaPos].ItemArray[0].ToString();
            //                    }
            //                    else
            //                    {
            //                        cellname[Cellna] = "Cell";
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {
            //            cellname[Cellna] = "Cell";
            //        }
            //        if (cellname[Cellna] == null)
            //        {
            //            cellname[Cellna] = "Cell";
            //        }
            //    }

            //    if ((GrdObj.DisplayLayout.Bands[0].Columns.Count - grpno) <= 4)
            //    {

            //        //header
            //        strBuildXMLOutput.Append("<products TBGUID=\"P" + RandomClass.Next() + "\">");
            //        if (HeaderID == 1)
            //        {
            //            for (int cellheader = 0; cellheader < grpno; cellheader++)
            //            {
            //                {
            //                    strBuildXMLOutput1.Append("<" + cellname[cellheader] + "_Header aid5:cellstyle=\"" + cellname[cellheader] + "_Header\" aid:table=\"cell\" aid:crows=\"" + "1" + "\" aid:ccols=\"" + "1" + "\" >");
            //                    strBuildXMLOutput1.Append("<![CDATA[" + GrdObj.DisplayLayout.Bands[0].Columns[GrpColno[cellheader]].ToString() + "]]>");
            //                    strBuildXMLOutput1.Append("</" + cellname[cellheader] + "_Header>");
            //                }
            //            }
            //        }
            //        if (grpno != 0) { totalcols = grpno; totalrows = HeaderID;/* + GrdObj.DisplayLayout.Rows.Count; } else { totalcols = 1; grpno = 1; totalrows = 0; AllinGroup = 1; }
            //        //endheader

            //        //Without grouping// Merging && Non // 
            //        if (GrpExtcheck == 0)
            //        {
            //            for (int i = 0; i < GrdObj.DisplayLayout.Rows.Count; i++)
            //            {
            //                totalrows++;
            //                for (int j = 0; j < GrdObj.DisplayLayout.Bands[0].Columns.Count; j++)
            //                {
            //                    if (GrdObj.DisplayLayout.Rows[i].Cells[GrpColno[j]].GetMergedCells() != null)
            //                    {
            //                        if (b[j] == 0)
            //                        {

            //                            a[j] = GrdObj.DisplayLayout.Rows[i].Cells[GrpColno[j]].GetMergedCells().Length;
            //                            b[j] = GrdObj.DisplayLayout.Rows[i].Cells[GrpColno[j]].GetMergedCells().Length - 1;
            //                            cellvalues = cellvalues + "//" + GrdObj.DisplayLayout.Rows[i].Cells[GrpColno[j]].Value.ToString();
            //                            if (a[j] == 0) { crow = 1; } else { crow = a[j]; }
            //                            strBuildXMLOutput1.Append("<" + cellname[j] + "   MergedCells=\"Vertical\" GroupedColumn=\"False\" TBGUID=\"TP" + RandomClass.Next() + "\" aid5:cellstyle=\"" + cellname[j] + "\" aid:table=\"cell\" aid:crows=\"" + crow + "\" aid:ccols=\"" + "1" + "\">");
            //                            //DBConnection connn = new DBConnection();
            //                            string ValueFortag = String.Empty;
            //                            string GrpColVal = GrdObj.DisplayLayout.Rows[i].Cells[GrpColno[j]].Value.ToString();
            //                            _SQLString = "SELECT ATTRIBUTE_ID,ATTRIBUTE_TYPE,ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_NAME = N'" + GrdObj.DisplayLayout.Bands[0].Columns[GrpColno[j]].ToString().Replace("'", "''") + "'";
            //                            DataSet DSS = CreateDataSet(); 
            //                            DataSet styleDS = new DataSet();
            //                            double dt = 0;
            //                            string style = string.Empty;
            //                            // if (DSS.Tables[0].Rows[0].ItemArray[1].ToString() == "4" || DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                            {
            //                                _SQLString = "SELECT STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_NAME = N'" + GrdObj.DisplayLayout.Bands[0].Columns[GrpColno[j]].ToString().Replace("'", "''") + "'";
            //                                styleDS =  CreateDataSet();  
            //                                style = styleDS.Tables[0].Rows[0].ItemArray[0].ToString();
            //                                int index = 0;
            //                                index = style.IndexOf("[");
            //                                if (index != -1 && GrdObj.DisplayLayout.Rows[i].Cells[GrpColno[j]].Value.ToString().Trim().Length > 0)
            //                                {
            //                                    style = style.Substring(0, index - 1);
            //                                    dt = Convert.ToDouble(GrdObj.DisplayLayout.Rows[i].Cells[GrpColno[j]].Value.ToString().Trim());
            //                                }
            //                            }
            //                            if (DSS.Tables[0].Rows.Count > 0)
            //                                ExtractCurrenyFormat(Convert.ToInt32(DSS.Tables[0].Rows[0].ItemArray[0].ToString()));
            //                            if ((Headeroptions == "All") || (Headeroptions != "All" && i == 0))
            //                            {
            //                                if ((EmptyCondition == "Null" || EmptyCondition == "Empty" || EmptyCondition == null) && (GrpColVal == string.Empty))
            //                                {
            //                                    ValueFortag = ReplaceText;
            //                                }
            //                                else if ((GrpColVal.ToString()) == (EmptyCondition))
            //                                {
            //                                    ValueFortag = ReplaceText;
            //                                }
            //                                else
            //                                {
            //                                    if (DSS.Tables[0].Rows[0].ItemArray[1].ToString() == "4" || DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                                    {
            //                                        if (dt != 0.0)
            //                                        {
            //                                            ValueFortag = Prefix + dt.ToString(style.Trim()) + Suffix;
            //                                        }
            //                                        else
            //                                        {
            //                                            if (GrpColVal.ToString().Length > 0 && DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                                            {
            //                                                ValueFortag = Prefix + Convert.ToDouble(GrpColVal.ToString()) + Suffix;
            //                                            }
            //                                            else
            //                                            {
            //                                                ValueFortag = Prefix + GrpColVal.ToString() + Suffix;
            //                                            }
            //                                        }
            //                                    }
            //                                    else
            //                                    {
            //                                        ValueFortag = Prefix + GrpColVal.ToString() + Suffix;
            //                                    }
            //                                }
            //                            }
            //                            else
            //                            {
            //                                if (DSS.Tables[0].Rows[0].ItemArray[1].ToString() == "4" || DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                                {
            //                                    ValueFortag = dt.ToString(style.Trim());
            //                                }
            //                                else
            //                                {
            //                                    if (GrpColVal.ToString().Length > 0 && DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                                    {
            //                                        ValueFortag = Convert.ToDouble(GrpColVal.ToString()).ToString();
            //                                    }
            //                                    else
            //                                    {
            //                                        ValueFortag = GrpColVal.ToString();
            //                                    }
            //                                }
            //                            }
            //                            strBuildXMLOutput1.Append("<![CDATA[" + ValueFortag.ToString().Trim() + "]]>");
            //                            strBuildXMLOutput1.Append("</" + cellname[j] + ">");
            //                        }
            //                        else
            //                        {
            //                            cellvalues = cellvalues + "/////";
            //                            a[j]--; b[j]--;
            //                        }
            //                    }
            //                    else
            //                    {
            //                        cellvalues = cellvalues + "//" + GrdObj.DisplayLayout.Rows[i].Cells[GrpColno[j]].Value.ToString();
            //                        if (a[j] == 0) { crow = 1; } else { crow = a[j]; }
            //                        strBuildXMLOutput1.Append("<" + cellname[j] + "   MergedCells=\"False\"  GroupedColumn=\"False\" TBGUID=\"TP" + RandomClass.Next() + "\" aid5:cellstyle=\"" + cellname[j] + "\"  aid:table=\"cell\" aid:crows=\"" + crow + "\" aid:ccols=\"" + "1" + "\">");
            //                        //DBConnection connn = new DBConnection();
            //                        string ValueFortag = String.Empty;
            //                        string GrpColVal = GrdObj.DisplayLayout.Rows[i].Cells[GrpColno[j]].Value.ToString();
            //                        _SQLString = "SELECT ATTRIBUTE_ID,ATTRIBUTE_TYPE,ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_NAME = N'" + GrdObj.DisplayLayout.Bands[0].Columns[GrpColno[j]].ToString().Replace("'", "''") + "'";
            //                        DataSet DSS =  CreateDataSet(); 
            //                        if (DSS.Tables[0].Rows.Count > 0)
            //                            ExtractCurrenyFormat(Convert.ToInt32(DSS.Tables[0].Rows[0].ItemArray[0].ToString()));
            //                        DataSet styleDS = new DataSet();
            //                        double dt = 0;
            //                        string style = string.Empty;
            //                        //if (DSS.Tables[0].Rows[0].ItemArray[1].ToString() == "4" || DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                        {
            //                            _SQLString = "SELECT STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_NAME = N'" + GrdObj.DisplayLayout.Bands[0].Columns[GrpColno[j]].ToString().Replace("'", "''") + "'";
            //                            styleDS = CreateDataSet(); 
            //                            style = styleDS.Tables[0].Rows[0].ItemArray[0].ToString();
            //                            int index = 0;
            //                            index = style.IndexOf("[");
            //                            if (index != -1 && GrdObj.DisplayLayout.Rows[i].Cells[GrpColno[j]].Value.ToString().Trim().Length > 0)
            //                            {
            //                                style = style.Substring(0, index - 1);
            //                                if (DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                                    dt = Convert.ToDouble(GrdObj.DisplayLayout.Rows[i].Cells[GrpColno[j]].Value.ToString().Trim());
            //                            }



            //                        }
            //                        if ((Headeroptions == "All") || (Headeroptions != "All" && i == 0))
            //                        {
            //                            if ((EmptyCondition == "Null" || EmptyCondition == "Empty" || EmptyCondition == null) && (GrpColVal == string.Empty))
            //                            {
            //                                ValueFortag = ReplaceText;
            //                            }
            //                            else if ((GrpColVal.ToString()) == (EmptyCondition))
            //                            {
            //                                ValueFortag = ReplaceText;
            //                            }
            //                            else
            //                            {
            //                                if (DSS.Tables[0].Rows[0].ItemArray[1].ToString() == "4" || DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                                {
            //                                    if (dt != 0.0)
            //                                    {
            //                                        ValueFortag = Prefix + dt.ToString(style.Trim()) + Suffix;
            //                                    }
            //                                    else if (DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                                    {
            //                                        ValueFortag = Prefix + Convert.ToDouble(GrpColVal.ToString()).ToString() + Suffix;
            //                                    }
            //                                }
            //                                else
            //                                {
            //                                    ValueFortag = Prefix + GrpColVal.ToString() + " " + Suffix;
            //                                }
            //                            }
            //                        }
            //                        else
            //                        {
            //                            if (DSS.Tables[0].Rows[0].ItemArray[1].ToString() == "4" || DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                            {
            //                                if (dt != 0.0)
            //                                {
            //                                    ValueFortag = dt.ToString(style.Trim());
            //                                }
            //                                else
            //                                {
            //                                    if (GrpColVal.ToString().Length > 0 && DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                                    {
            //                                        ValueFortag = Convert.ToDouble(GrpColVal.ToString()).ToString();
            //                                    }
            //                                    else
            //                                    {
            //                                        ValueFortag = "";
            //                                    }
            //                                }
            //                            }
            //                            else
            //                            {
            //                                ValueFortag = GrpColVal.ToString();
            //                            }
            //                        }
            //                        strBuildXMLOutput1.Append("<![CDATA[" + ValueFortag.Trim() + "]]>");
            //                        strBuildXMLOutput1.Append("</" + cellname[j] + ">");
            //                    }
            //                }
            //                cellvalues = cellvalues + "/nn/";
            //            }
            //            //Without grouping// Merging && Non // End /////////
            //            //strBuildXMLOutput.Append(strBuildXMLOutput1.ToString());  
            //        }
            //        else
            //        {
            //            //////////////Grouping//////////////Start//////////////////////
            //            GrdObj.DataSource = GrdObjj.DataSource;
            //            GrdObj.DisplayLayout.LoadFromXml(Application.LocalUserAppDataPath + "\\test.xml");
            //            for (int i1 = 0; i1 < GrdObj.Rows.Count; i1++)
            //            {
            //                rowVal = i1;
            //                string GrpColName, GrpColVal;
            //                int diff = GrpColno.Length - grpno;

            //                {
            //                    GrpColName = GrdObj.Rows[i1].Description.ToString();
            //                    GrpColVal = CharConverter(GrpColName);
            //                    GrpColItem = ItemConverter(GrpColName);
            //                    columnName = ColNameVal(GrpColName);
            //                    totalrows++;
            //                    SQLString = "select style_name,attribute_name from tb_attribute where attribute_id in " +
            //                                "(SELECT  attribute_id FROM  TB_ATTRIBUTE WHERE " +
            //                                " attribute_id IN  (SELECT DISTINCT attribute_id FROM " +
            //                                " TB_PROD_FAMILY_ATTR_LIST WHERE family_id = " + FamilyId +
            //                                ")) AND ATTRIBUTE_ID = " + DupDsPreview.Tables[1].Rows[0].ItemArray[DupDsPreview.Tables[1].Columns[columnName].Ordinal];
            //                    DataSet DScoreSylName = new DataSet();

            //                    _SQLString = SQLString;
            //                    DScoreSylName =  CreateDataSet();  

            //                    if (DScoreSylName != null)
            //                    {
            //                        if (DScoreSylName.Tables[0].Rows.Count > 0)
            //                        {
            //                            if (DScoreSylName.Tables[0].Rows[0].ItemArray[0].ToString().Trim() != "")
            //                            {
            //                                valuee = DScoreSylName.Tables[0].Rows[0].ItemArray[0].ToString().Trim();
            //                            }
            //                            else
            //                            {
            //                                valuee = "Cell";
            //                            }
            //                        }
            //                        else
            //                        {
            //                            valuee = "Cell";
            //                        }
            //                    }
            //                    else
            //                    {
            //                        valuee = "Cell";
            //                    }
            //                    //test
            //                    Boolean imagetype = false;
            //                    try
            //                    {
            //                        FileInfo chkFile = new FileInfo(GrpColVal);
            //                        if (chkFile.Extension.ToUpper() == ".BMP" || chkFile.Extension.ToUpper() == ".JPG" || chkFile.Extension.ToUpper() == ".GIF" || chkFile.Extension.ToUpper() == ".EPS")
            //                        {
            //                            imagetype = true;
            //                        }
            //                    }
            //                    catch (Exception)
            //                    {
            //                    }
            //                    if (imagetype == true)
            //                    {
            //                        //strBuildXMLOutput1.Append("<" + valuee + " aid5:cellstyle=\"" + valuee + "\" aid:table=\"cell\" aid:crows=\"" + Mergecellcount + "\" aid:ccols=\"" + "1" + "\"" + grpno.ToString() + "GroupedColumn=\"False\" TBGUID=\"TP" + RandomClass.Next() + "\">");
            //                        strBuildXMLOutput1.Append("<" + valuee + " GroupedColumn=\"False\" TBGUID=\"TP" + RandomClass.Next() + "\" aid5:cellstyle=\"" + valuee + "\" aid:table=\"cell\" aid:crows=\"" + Mergecellcount + "\" aid:ccols=\"" + grpno.ToString() + "\" >");
            //                        strBuildXMLOutput1.Append("<image_name COTYPE=\"IMAGE\"  TBGUID=\"CI" + RandomClass.Next() + "\"  IMAGE_FILE=\"" + GrpColName.Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\"></image_name>");
            //                        strBuildXMLOutput1.Append("</" + valuee + ">");
            //                    }
            //                    else
            //                    {
            //                        _SQLString = " SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + DupDsPreview.Tables[1].Rows[0].ItemArray[DupDsPreview.Tables[1].Columns[columnName].Ordinal];
            //                        DataSet DSattrType = new DataSet();
            //                        DSattrType =  CreateDataSet();  


            //                        //DBConnection connn = new DBConnection();
            //                        string ValueFortag = String.Empty;
            //                        _SQLString = "SELECT ATTRIBUTE_ID,ATTRIBUTE_TYPE,ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + DupDsPreview.Tables[1].Rows[0].ItemArray[DupDsPreview.Tables[1].Columns[columnName].Ordinal];
            //                        DataSet DSS =  CreateDataSet(); 
            //                        if (DSS.Tables[0].Rows.Count > 0)
            //                            ExtractCurrenyFormat(Convert.ToInt32(DSS.Tables[0].Rows[0].ItemArray[0].ToString()));
            //                        DataSet styleDS = new DataSet();
            //                        double dt = 0;
            //                        string style = string.Empty;
            //                        // if (DSS.Tables[0].Rows[0].ItemArray[1].ToString() == "4" || DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                        {
            //                            _SQLString = "SELECT STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + DupDsPreview.Tables[1].Rows[0].ItemArray[DupDsPreview.Tables[1].Columns[columnName].Ordinal];
            //                            styleDS =  CreateDataSet();  
            //                            if (styleDS.Tables[0].Rows.Count > 0)
            //                                style = styleDS.Tables[0].Rows[0].ItemArray[0].ToString();
            //                            int index = 0;
            //                            index = style.IndexOf("[");
            //                            if (index != -1 && GrpColVal.Trim().Length > 0)
            //                            {
            //                                style = style.Substring(0, index - 1);
            //                                dt = Convert.ToDouble(GrpColVal.Trim());
            //                            }
            //                        }
            //                        if ((EmptyCondition == "Null" || EmptyCondition == "Empty" || EmptyCondition == null) && (GrpColVal == string.Empty))
            //                        {
            //                            ValueFortag = ReplaceText;
            //                        }
            //                        else if ((GrpColVal.ToString()) == (EmptyCondition))
            //                        {
            //                            ValueFortag = ReplaceText;
            //                        }
            //                        else
            //                        {
            //                            if (DSS.Tables[0].Rows[0].ItemArray[1].ToString() == "4" || DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                            {
            //                                if (dt != 0.0)
            //                                {
            //                                    ValueFortag = Prefix + dt.ToString(style.Trim()) + Suffix;
            //                                }
            //                                else
            //                                {
            //                                    ValueFortag = Prefix + Convert.ToDouble(GrpColVal.ToString().Trim()).ToString() + Suffix;
            //                                }
            //                            }
            //                            else
            //                            {
            //                                ValueFortag = Prefix + GrpColVal.ToString().Trim() + Suffix;
            //                            }
            //                        }
            //                        if (DSattrType.Tables[0].Rows[0].ItemArray[0].ToString() == "Hyperlink")
            //                        {
            //                            strBuildXMLOutput1.Append("<" + valuee + "   MergedCells=\"Horizontal\" GroupedColumn=\"True\" TBGUID=\"TP" + RandomClass.Next() + "\" Hyperlink=\"" + ValueFortag + "\" aid5:cellstyle=\"" + valuee + "\" aid:table=\"cell\" aid:crows=\"" + "1" + "\" aid:ccols=\"" + grpno.ToString() + "\"  >");
            //                        }
            //                        else
            //                        {
            //                            strBuildXMLOutput1.Append("<" + valuee + "   MergedCells=\"Horizontal\" GroupedColumn=\"True\" TBGUID=\"TP" + RandomClass.Next() + "\" aid5:cellstyle=\"" + valuee + "\" aid:table=\"cell\" aid:crows=\"" + "1" + "\" aid:ccols=\"" + grpno.ToString() + "\"  >");
            //                        }
            //                        strBuildXMLOutput1.Append("<![CDATA[" + ValueFortag.Trim() + "]]>");
            //                        strBuildXMLOutput1.Append("</" + valuee + ">");
            //                    }
            //                    rowlevel[0] = i1; levelpros = 1;
            //                    {
            //                        constructXML(Convert.ToInt16(GrpColItem), i1, levelpros);
            //                    }

            //                }
            //            }
            //            ///////////////Grouping//////////////End//////////////////////
            //        }


            //        strBuildXMLOutput.Append("<TABLE Format=\"" + TableMode + "\" nrows=\"" + totalrows + "\" ncols=\"" + totalcols + "\" TBGUID=\"T" + RandomClass.Next() + "\" xmlns:aid=\"http://ns.adobe.com/AdobeInDesign/4.0/\" xmlns:aid5=\"http://ns.adobe.com/AdobeInDesign/5.0/\" aid5:tablestyle=\"TABLE\" aid:table=\"table\" aid:trows=\"" + totalrows + "\" aid:tcols=\"" + totalcols + "\"  >");
            //        strBuildXMLOutput.Append(strBuildXMLOutput1);
            //        strBuildXMLOutput.Append("</TABLE>");
            //        strBuildXMLOutput.Append("</products>");
            //        AllinGroup = 0;
            //    }
            //}
            //catch (System.Exception)
            //{
            //    // ex.Publish(e, false);
            //}
            //return strBuildXMLOutput.ToString();
            return "";
        }


        private void ChkAttributeExistinFamily()
        {
            // DBConnection OCon = new DBConnection();
            DataSet OdsCalc = new DataSet();
            DataSet Dschk = new DataSet();
            string AttrCalcFormaula = string.Empty;
            ContinueCalclautedCols = 0;
            string TempStr = string.Empty;
            CheckCalculatedCtr = 0;
            _SQLString = " SELECT ATTRIBUTE_CALC_FORMULA FROM TB_ATTRIBUTE WHERE IS_CALCULATED =1  AND ATTRIBUTE_CALC_FORMULA <> '' " +
                            "  AND ATTRIBUTE_ID IN (SELECT DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID IN " +
                            "  (SELECT PRODUCT_ID FROM TB_PROD_FAMILY WHERE FAMILY_ID = " + FamilyId + ")) and TB_ATTRIBUTE.FLAG_RECYCLE='A'";
            OdsCalc = CreateDataSet();
            List<string> attrList = new List<string>();
            foreach (DataRow Dr in OdsCalc.Tables[0].Rows)
            {
                AttrCalcFormaula = Dr[0].ToString();
                string tempAttrEq = AttrCalcFormaula;

                while (tempAttrEq.Contains("["))
                {
                    int startIndex = tempAttrEq.IndexOf('[');
                    int endIndex = tempAttrEq.IndexOf(']');
                    attrList.Add(tempAttrEq.Substring(startIndex + 1, endIndex - startIndex - 1));
                    tempAttrEq = tempAttrEq.Substring(endIndex + 1, tempAttrEq.Length - endIndex - 1);
                }
            }
            for (int j = 0; j < attrList.Count; j++)
            {
                _SQLString = " SELECT ATTRIBUTE_NAME FROM TB_ATTRIBUTE TA WHERE ATTRIBUTE_ID IN ( SELECT  DISTINCT ATTRIBUTE_ID FROM TB_PROD_SPECS WHERE PRODUCT_ID IN  " +
                                " (SELECT PRODUCT_ID FROM TB_PROD_fAMILY WHERE FAMILY_ID = " + FamilyId + ")) and TA.FLAG_RECYCLE='A' AND TA.ATTRIBUTE_NAME = '" + attrList[j].Replace("'", "''") + "' ";
                Dschk = CreateDataSet();
                if (Dschk.Tables[0].Rows.Count > 0)
                {
                    CheckCalculatedCtr++;
                }
            }

            if (CheckCalculatedCtr == attrList.Count)
            {
                ContinueCalclautedCols = 1;
            }
        }
        private void ExtractCurrenyFormat(int AttributeID)
        {

            string XMLstr = string.Empty;
            DataSet dscURRENCY = new DataSet();
            _SQLString = " SELECT ATTRIBUTE_DATARULE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID  =" + AttributeID + " and TA.FLAG_RECYCLE='A'";
            dscURRENCY = CreateDataSet();
            Prefix = string.Empty; Suffix = string.Empty; EmptyCondition = string.Empty; ReplaceText = string.Empty; Headeroptions = string.Empty;
            if (dscURRENCY.Tables[0].Rows.Count > 0)
            {
                if (dscURRENCY.Tables[0].Rows[0].ItemArray[0].ToString() != string.Empty)
                {
                    XMLstr = dscURRENCY.Tables[0].Rows[0].ItemArray[0].ToString();
                    XmlDocument xmlDOc = new XmlDocument();
                    xmlDOc.LoadXml(XMLstr);
                    XmlNode rootNode = xmlDOc.DocumentElement;
                    {
                        XmlNodeList xmlNodeList;
                        xmlNodeList = rootNode.ChildNodes;

                        for (int xmlNode = 0; xmlNode < xmlNodeList.Count; xmlNode++)
                        {
                            if (xmlNodeList[xmlNode].ChildNodes.Count > 0)
                            {
                                if (xmlNodeList[xmlNode].ChildNodes[0].LastChild != null)
                                {
                                    Prefix = xmlNodeList[xmlNode].ChildNodes[0].LastChild.Value;
                                }
                                if (xmlNodeList[xmlNode].ChildNodes[1].LastChild != null)
                                {
                                    Suffix = xmlNodeList[xmlNode].ChildNodes[1].LastChild.Value;
                                }
                                if (xmlNodeList[xmlNode].ChildNodes[2].LastChild != null)
                                {
                                    EmptyCondition = xmlNodeList[xmlNode].ChildNodes[2].LastChild.Value;
                                }
                                if (xmlNodeList[xmlNode].ChildNodes[3].LastChild != null)
                                {
                                    ReplaceText = xmlNodeList[xmlNode].ChildNodes[3].LastChild.Value;
                                }
                                if (xmlNodeList[xmlNode].ChildNodes[4].LastChild != null)
                                {
                                    Headeroptions = xmlNodeList[xmlNode].ChildNodes[4].LastChild.Value;
                                }

                            }
                        }
                    }



                }
            }

        }
        public int ExecuteSQLScalarQuery()
        {
            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                var objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = _SQLString;
                objSqlCommand.CommandType = CommandType.Text;
                objSqlCommand.Connection = objSqlConnection;
                objSqlConnection.Open();
                var rValues = objSqlCommand.ExecuteScalar();
                int rValue;
                int.TryParse(Convert.ToString(rValues), out rValue);
                return rValue;

            }
        }
        private string CharConverter(string GrpColName)
        {

            string TempColname; TempColname = GrpColName;
            string Temp = "";
            //try
            //{
            //    for (int i = 0; i < TempColname.Length - 2; i++)
            //    {
            //        if (TempColname.Substring(i, 2) == ": ")
            //        {
            //            i = i + 2;
            //            for (int j = TempColname.Length - 3; j > i; j--)
            //            {
            //                if (TempColname.Substring(j, 2) == " (")
            //                {
            //                    Temp = TempColname.Substring(i, j - i); j = i;
            //                }
            //            }
            //            i = TempColname.Length;
            //        }
            //    }
            //}
            //catch (System.Exception)
            //{
            //    // ex.Publish(e, false);
            //}
            //DataSet DSS = (DataSet)GrdObj.DataSource;
            //columnName = ColNameVal(GrpColName);
            //{
            //    foreach (DataRow DR in DSS.Tables[0].Rows)
            //    {
            //        if (DR[columnName].ToString().Replace("\n", "").Replace("\r", "") == Temp.Replace(" ", "").ToString())
            //        {
            //            Temp = DR[columnName].ToString();
            //        }
            //    }
            //}

            return Temp.Trim();
        }
        //Extracting the No. of Items in the group
        private string ItemConverter(string GrpColName)
        {
            string TempColname; TempColname = GrpColName;
            string Temp = "";
            try
            {
                for (int i = TempColname.Length - 2; i > 0; i--)
                {
                    if (TempColname.Substring(i, 2) == " (")
                    {
                        i = i + 2;
                        for (int j = i; j < TempColname.Length - 5; j++)
                        {
                            if (TempColname.Substring(j, 5) == " item")
                            {
                                Temp = TempColname.Substring(i, j - i); j = i + j; i = 0;
                            }
                        }
                    }
                }
            }
            catch (System.Exception)
            {

            }
            return Temp.Trim();
        }
        //Extracting the Column name of the  grouped item
        public string ColNameVal(string GrpColName)
        {
            string TempColname; TempColname = GrpColName;
            string Temp = ""; int chk = 0;
            try
            {
                for (int i = 0; i < TempColname.Length - 3; i++)
                {
                    if (TempColname.Substring(i, 3) == " : " && chk == 0)
                    {
                        Temp = TempColname.Substring(0, i); chk = 1;
                    }
                }
            }
            catch (System.Exception)
            {

            }
            return Temp.Trim();
        }
        private void constructXML(int ItraItem, int ProsVal, int levelofpros)
        {
            //string GrpColName, GrpColVal; //, levspace;

            //try
            //{
            //    for (int grpcount = 0; grpcount < ItraItem; grpcount++)
            //    {
            //        rowVal = grpcount;
            //        rowlevel[levelpros] = grpcount;
            //        GrpColName = XMLforGroup(levelofpros);
            //        if (GrpColName != "")
            //        {
            //            GrpColVal = CharConverter(GrpColName);
            //            GrpColItem = ItemConverter(GrpColName);
            //            columnName = ColNameVal(GrpColName);
            //            totalrows++;
            //            //DBConnection connn = new DBConnection();
            //            string ValueFortag = String.Empty;
            //            _SQLString = "SELECT ATTRIBUTE_ID,ATTRIBUTE_TYPE,ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + DupDsPreview.Tables[1].Rows[0].ItemArray[DupDsPreview.Tables[1].Columns[columnName].Ordinal];
            //            DataSet DSS =  CreateDataSet();  
            //            if (DSS.Tables[0].Rows.Count > 0)
            //                ExtractCurrenyFormat(Convert.ToInt32(DSS.Tables[0].Rows[0].ItemArray[0].ToString()));
            //            DataSet styleDS = new DataSet();
            //            double dt = 0;
            //            string style = string.Empty;
            //            // if (DSS.Tables[0].Rows[0].ItemArray[1].ToString() == "4" || DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //            {
            //                _SQLString = "SELECT STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + DupDsPreview.Tables[1].Rows[0].ItemArray[DupDsPreview.Tables[1].Columns[columnName].Ordinal];
            //                styleDS =  CreateDataSet();  
            //                style = styleDS.Tables[0].Rows[0].ItemArray[0].ToString();
            //                int index = 0;
            //                index = style.IndexOf("[");
            //                if (index != -1 && GrpColVal.Trim().Length > 0)
            //                {
            //                    style = style.Substring(0, index - 1);
            //                    dt = Convert.ToDouble(GrpColVal.Trim());
            //                }
            //            }
            //            if ((EmptyCondition == "Null" || EmptyCondition == "Empty" || EmptyCondition == null) && (GrpColVal == string.Empty))
            //            {
            //                ValueFortag = ReplaceText;
            //            }
            //            else if ((GrpColVal.ToString()) == (EmptyCondition))
            //            {
            //                ValueFortag = ReplaceText;
            //            }
            //            else
            //            {
            //                if (DSS.Tables[0].Rows[0].ItemArray[1].ToString() == "4" || DSS.Tables[0].Rows[0].ItemArray[2].ToString().Substring(0, 3).ToUpper() == "NUM")
            //                {
            //                    if (dt != 0.0)
            //                    {
            //                        ValueFortag = Prefix + dt.ToString(style.Trim()) + Suffix;
            //                    }
            //                    else
            //                    {
            //                        if (GrpColVal.ToString().Length > 0)
            //                        {
            //                            ValueFortag = Prefix + Convert.ToDouble(GrpColVal.ToString()).ToString() + Suffix;
            //                        }
            //                        else
            //                        {
            //                            ValueFortag = Prefix + GrpColVal.ToString() + Suffix;
            //                        }
            //                    }
            //                }
            //                else
            //                {
            //                    ValueFortag = Prefix + GrpColVal.ToString() + Suffix;
            //                }
            //            }
            //            string SQLString = "select style_name,attribute_name from tb_attribute where attribute_id in " +
            //                                "(SELECT  attribute_id FROM  TB_ATTRIBUTE WHERE " +
            //                                " attribute_id IN  (SELECT DISTINCT attribute_id FROM " +
            //                                " TB_PROD_FAMILY_ATTR_LIST WHERE family_id = " + FamilyId +
            //                                ")) AND ATTRIBUTE_ID = " + DupDsPreview.Tables[1].Rows[0].ItemArray[DupDsPreview.Tables[1].Columns[columnName].Ordinal];
            //            DataSet DScoreSylName = new DataSet();
            //            // AppLoader.DBConnection oConHelper = new DBConnection();
            //            _SQLString = SQLString;
            //            DScoreSylName =  CreateDataSet(); 

            //            if (DScoreSylName != null)
            //            {
            //                if (DScoreSylName.Tables[0].Rows.Count > 0)
            //                {
            //                    if (DScoreSylName.Tables[0].Rows[0].ItemArray[0].ToString().Trim() != "")
            //                    {
            //                        valuee = DScoreSylName.Tables[0].Rows[0].ItemArray[0].ToString().Trim();
            //                    }
            //                    else
            //                    {
            //                        valuee = "Cell";
            //                    }
            //                }
            //            }
            //            Boolean imagetype = false;
            //            try
            //            {
            //                FileInfo chkFile = new FileInfo(GrpColVal);
            //                if (chkFile.Extension.ToUpper() == ".BMP" || chkFile.Extension.ToUpper() == ".JPG" || chkFile.Extension.ToUpper() == ".GIF" || chkFile.Extension.ToUpper() == ".EPS")
            //                {
            //                    imagetype = true;
            //                }
            //            }
            //            catch (Exception)
            //            {
            //            }
            //            if (imagetype == true)
            //            {
            //                strBuildXMLOutput1.Append("<" + valuee + " GroupedColumn=\"False\" TBGUID=\"TP" + RandomClass.Next() + "\" aid5:cellstyle=\"" + valuee + "\" aid:table=\"cell\" aid:crows=\"" + Mergecellcount + "\" aid:ccols=\"" + "1" + "\"" + grpno.ToString() + ">");
            //                strBuildXMLOutput1.Append("<image_name COTYPE=\"IMAGE\"  TBGUID=\"CI" + RandomClass.Next() + "\"  IMAGE_FILE=\"" + GrpColName.Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\"></image_name>");
            //                strBuildXMLOutput1.Append("</" + valuee + ">");
            //            }
            //            else
            //            {
            //                strBuildXMLOutput1.Append("<" + valuee + "  MergedCells=\"Horizontal\" GroupedColumn=\"True\" TBGUID=\"TP" + RandomClass.Next() + "\" aid5:cellstyle=\"" + valuee + "\" aid:table=\"cell\" aid:crows=\"" + "1" + "\" aid:ccols=\"" + grpno.ToString() + "\"  >");
            //                strBuildXMLOutput1.Append("<![CDATA[" + ValueFortag.Trim() + "]]>");
            //                strBuildXMLOutput1.Append("</" + valuee + ">"); levelpros++;
            //            }
            //        }
            //        {
            //            if (GrpColName != "")
            //            {
            //                constructXML(Convert.ToInt16(GrpColItem), grpcount, levelpros);
            //            }
            //            else
            //            {
            //                int conrep = XMLforGroupChildcount(levelofpros);
            //                if (AllinGroup == 0)
            //                {
            //                    totalrows++;
            //                    {
            //                        rowlevel[levelpros] = grpcount;
            //                        constructXMLforCell(grpno, ProsVal, levelpros);
            //                    }
            //                }
            //                rowlevel[levelpros] = 0;
            //            }
            //        }
            //    }
            //    levelpros--;
            //}
            //catch (System.Exception)
            //{

            //}
        }
        private int XMLforGroupChildcount(int levelofpros)
        {
            //try
            //{
            //    if (levelofpros == 0)
            //    {
            //        XMLdes = GrdObj.Rows.Count.ToString();
            //    }
            //    else if (levelofpros == 1)
            //    {
            //        XMLdes = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows.Count.ToString();
            //    }
            //    else if (levelofpros == 2)
            //    {

            //        XMLdes = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows.Count.ToString();

            //    }
            //    else if (levelofpros == 3)
            //    {
            //        XMLdes = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows.Count.ToString();
            //    }
            //    else if (levelofpros == 4)
            //    {
            //        XMLdes = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].ChildBands[0].Rows.Count.ToString();
            //    }
            //}
            //catch (System.Exception)
            //{

            //}
            return Convert.ToInt16(XMLdes);
        }
        //Constructing the cell value for XML
        private void constructXMLforCell(int ItraItem, int ProsVal, int levelofpros)
        {
            string GrpColName, Megval; // GrpColItem, GrpColVal;
            //try
            //{
            //    for (int celldata = 0; celldata < ItraItem; celldata++)
            //    {
            //        bool check; string gmCheck;
            //        GrpColName = XMLforCell(levelofpros, GrpColno[celldata]);
            //        columnName = XMLforColName(levelofpros, GrpColno[celldata]);
            //        //DBConnection connn = new DBConnection();
            //        string ValueFortag = string.Empty;
            //        _SQLString = "SELECT ATTRIBUTE_ID,ATTRIBUTE_DATATYPE,ATTRIBUTE_TYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + DupDsPreview.Tables[1].Rows[0].ItemArray[DupDsPreview.Tables[1].Columns[GrdObj.DisplayLayout.Bands[0].Columns[GrpColno[celldata]].ToString()].Ordinal];
            //        DataSet DSS =  CreateDataSet();  
            //        if (DSS.Tables[0].Rows.Count > 0)
            //            ExtractCurrenyFormat(Convert.ToInt32(DSS.Tables[0].Rows[0].ItemArray[0].ToString()));
            //        DataSet styleDS = new DataSet();
            //        double dt = 0;
            //        string style = string.Empty;
            //        //if (DSS.Tables[0].Rows[0].ItemArray[2].ToString() == "4" || DSS.Tables[0].Rows[0].ItemArray[1].ToString().Substring(0, 3).ToUpper() == "NUM")
            //        {
            //            _SQLString = "SELECT STYLE_FORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + DupDsPreview.Tables[1].Rows[0].ItemArray[DupDsPreview.Tables[1].Columns[GrdObj.DisplayLayout.Bands[0].Columns[GrpColno[celldata]].ToString()].Ordinal];
            //            styleDS =  CreateDataSet();  
            //            style = styleDS.Tables[0].Rows[0].ItemArray[0].ToString();
            //            int index = 0;
            //            index = style.IndexOf("[");
            //            if (index != -1 && GrpColName.ToString().Trim().Length > 0)
            //            {
            //                style = style.Substring(0, index - 1);
            //                dt = Convert.ToDouble(GrpColName.ToString().Trim());
            //            }

            //        }
            //        if ((Headeroptions == "All") || (Headeroptions != "All" && rowVal == 0))
            //        {
            //            if ((EmptyCondition == "Null" || EmptyCondition == "Empty" || EmptyCondition == null) && (GrpColName == string.Empty))
            //            {
            //                ValueFortag = ReplaceText;
            //            }
            //            else if ((GrpColName.ToString()) == (EmptyCondition))
            //            {
            //                ValueFortag = ReplaceText;
            //            }
            //            else
            //            {
            //                if ((DSS.Tables[0].Rows[0].ItemArray[1].ToString().StartsWith("Num") == true) && (DSS.Tables[0].Rows[0].ItemArray[0].ToString().EndsWith(",0)") == true))
            //                {
            //                    ValueFortag = Prefix + Convert.ToInt32(GrpColName.ToString()) + Suffix;
            //                }
            //                else if ((DSS.Tables[0].Rows[0].ItemArray[1].ToString().StartsWith("Num") == true) && (DSS.Tables[0].Rows[0].ItemArray[0].ToString().EndsWith(",0)") == false))
            //                {
            //                    if (dt != 0.0)
            //                    {
            //                        ValueFortag = Prefix + dt.ToString(style.Trim()) + Suffix;
            //                    }
            //                    else
            //                    {
            //                        if (GrpColName.ToString().Length > 0)
            //                        {
            //                            ValueFortag = Prefix + Convert.ToDouble(GrpColName.ToString()).ToString() + Suffix;
            //                        }
            //                        else
            //                        {
            //                            ValueFortag = Prefix + GrpColName.ToString() + Suffix;
            //                        }
            //                    }
            //                }
            //                else
            //                {
            //                    ValueFortag = Prefix + GrpColName.ToString() + Suffix;
            //                }
            //            }
            //        }
            //        else
            //        {
            //            if ((DSS.Tables[0].Rows[0].ItemArray[1].ToString().StartsWith("Num") == true) && (DSS.Tables[0].Rows[0].ItemArray[0].ToString().EndsWith(",0)") == true))
            //            {
            //                ValueFortag = (Convert.ToInt32(GrpColName.ToString())).ToString();
            //            }
            //            else if ((DSS.Tables[0].Rows[0].ItemArray[1].ToString().StartsWith("Num") == true) && (DSS.Tables[0].Rows[0].ItemArray[0].ToString().EndsWith(",0)") == false))
            //            {
            //                if (dt != 0.0)
            //                {
            //                    ValueFortag = dt.ToString(style.Trim());
            //                }
            //                else
            //                {
            //                    if (GrpColName.ToString().Length > 0)
            //                    { ValueFortag = Convert.ToDouble(GrpColName.ToString()).ToString(); }
            //                    else
            //                    {
            //                        ValueFortag = GrpColName.ToString();
            //                    }
            //                }
            //            }
            //            else
            //                ValueFortag = GrpColName.ToString();
            //        }
            //        GrpColName = ValueFortag;
            //        Mergecellcount = XMLforMergedCell(levelofpros, GrpColno[celldata]);
            //        if (Mergecellcount > 1)
            //        {
            //            Megval = XMLforMergeCheck(levelofpros, GrpColno[celldata]);
            //            if (Megval == "True")
            //            { check = false; }
            //            else { check = true; }
            //        }
            //        else
            //        {
            //            check = true;
            //        }
            //        if (Mergecellcount == 1)
            //        {
            //            gmCheck = " MergedCells=\"False\" ";
            //        }
            //        else
            //        {
            //            gmCheck = " MergedCells=\"Vertical\" ";
            //        }
            //        if (check == true)
            //        {
            //            Boolean imagetype = false;
            //            try
            //            {
            //                FileInfo chkFile = new FileInfo(GrpColName);
            //                if (chkFile.Extension.ToUpper() == ".BMP" || chkFile.Extension.ToUpper() == ".JPG" || chkFile.Extension.ToUpper() == ".GIF" || chkFile.Extension.ToUpper() == ".EPS")
            //                {
            //                    imagetype = true;
            //                }
            //            }
            //            catch (Exception)
            //            {
            //            }
            //            if (imagetype == true)
            //            {
            //                _SQLString = " SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + DupDsPreview.Tables[1].Rows[0].ItemArray[DupDsPreview.Tables[1].Columns[columnName].Ordinal];
            //                DataSet DSattrType = new DataSet();
            //                DSattrType =  CreateDataSet(); 

            //                if (DSattrType.Tables[0].Rows[0].ItemArray[0].ToString() == "Hyperlink")
            //                {
            //                    strBuildXMLOutput1.Append("<" + cellname[celldata] + " GroupedColumn=\"False\" TBGUID=\"TP" + RandomClass.Next() + "\" Hyperlink=\"" + ValueFortag + "\" aid5:cellstyle=\"" + cellname[celldata] + "\" aid:table=\"cell\" aid:crows=\"" + Mergecellcount + "\" aid:ccols=\"" + "1" + "\"" + gmCheck + ">");
            //                }
            //                else
            //                {
            //                    strBuildXMLOutput1.Append("<" + cellname[celldata] + " GroupedColumn=\"False\" TBGUID=\"TP" + RandomClass.Next() + "\" aid5:cellstyle=\"" + cellname[celldata] + "\" aid:table=\"cell\" aid:crows=\"" + Mergecellcount + "\" aid:ccols=\"" + "1" + "\"" + gmCheck + ">");
            //                }
            //                //strBuildXMLOutput.Append("<" + CellName + " aid:table=\"cell\" aid:crows=\"1\" aid:ccols=\"1\"   attribute_id=\"" + oDTProductTable.Tables[1].Rows[0].ItemArray[dCol].ToString() + "\"" + "  IMAGE_FILE=\"" + oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString().Trim() + "\"  COTYPE=\"IMAGE\"  TBGUID=\"CI" + RandomClass.Next() + "\"><cell_image TBGUID=\"CI" + RandomClass.Next() + "\" COTYPE=\"IMAGE\"  IMAGE_FILE=\"" + oDTProductTable.Tables[0].Rows[dRow].ItemArray[dCol].ToString() + "\"></cell_image></" + CellName + ">\n");
            //                strBuildXMLOutput1.Append("<image_name COTYPE=\"IMAGE\"  TBGUID=\"CI" + RandomClass.Next() + "\"  IMAGE_FILE=\"" + GrpColName.Replace("&", "&amp;").Replace("&lt;", "&lt;").Replace("&gt;", "&gt;").Replace("\"", "&quot;").Replace("'", "&apos;") + "\"></image_name>");
            //                strBuildXMLOutput1.Append("</" + cellname[celldata] + ">");
            //            }
            //            else
            //            {

            //                _SQLString = " SELECT ATTRIBUTE_DATATYPE FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID = " + DupDsPreview.Tables[1].Rows[0].ItemArray[DupDsPreview.Tables[1].Columns[columnName].Ordinal];
            //                DataSet DSattrType = new DataSet();
            //                DSattrType =  CreateDataSet(); 

            //                if (DSattrType.Tables[0].Rows[0].ItemArray[0].ToString() == "Hyperlink")
            //                {
            //                    strBuildXMLOutput1.Append("<" + cellname[celldata] + " GroupedColumn=\"False\" TBGUID=\"TP" + RandomClass.Next() + "\" Hyperlink=\"" + ValueFortag + "\" aid5:cellstyle=\"" + cellname[celldata] + "\" aid:table=\"cell\" aid:crows=\"" + Mergecellcount + "\" aid:ccols=\"" + "1" + "\"" + gmCheck + ">");
            //                }
            //                else
            //                {
            //                    strBuildXMLOutput1.Append("<" + cellname[celldata] + " GroupedColumn=\"False\" TBGUID=\"TP" + RandomClass.Next() + "\" aid5:cellstyle=\"" + cellname[celldata] + "\" aid:table=\"cell\" aid:crows=\"" + Mergecellcount + "\" aid:ccols=\"" + "1" + "\"" + gmCheck + ">");
            //                }

            //                strBuildXMLOutput1.Append("<![CDATA[" + GrpColName.Trim() + "]]>");
            //                strBuildXMLOutput1.Append("</" + cellname[celldata] + ">");
            //            }
            //        }
            //    }
            //}
            //catch (System.Exception)
            //{

            //}

        }
        //Getting the count for mearged cell
        private int XMLforMergedCell(int levelofpros, int cellval)
        {
            //try
            //{
            //    XMLcell = "";
            //    if (levelofpros == 1)
            //    {
            //        if (GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].Cells[cellval].GetMergedCells() != null)
            //        {
            //            XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].Cells[cellval].GetMergedCells().Length.ToString();
            //        }
            //    }
            //    else if (levelofpros == 2)
            //    {
            //        if (GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].Cells[cellval].GetMergedCells() != null)
            //        {
            //            XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].Cells[cellval].GetMergedCells().Length.ToString();
            //        }
            //    }
            //    else if (levelofpros == 3)
            //    {
            //        if (GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].Cells[cellval].GetMergedCells() != null)
            //        {
            //            XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].Cells[cellval].GetMergedCells().Length.ToString();
            //        }
            //    }
            //    else if (levelofpros == 4)
            //    {
            //        if (GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].ChildBands[0].Rows[rowlevel[4]].Cells[cellval].GetMergedCells() != null)
            //        {
            //            XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].ChildBands[0].Rows[rowlevel[4]].Cells[cellval].GetMergedCells().Length.ToString();
            //        }
            //    }
            //    if (XMLcell == "")
            //    {
            //        XMLcell = "1";
            //    }
            //}
            //catch (System.Exception)
            //{
            //    // ex.Publish(e, false);
            //}
            return Convert.ToInt16(XMLcell);
        }

        //Checking cell merge
        private string XMLforMergeCheck(int levelofpros, int cellval)
        {
            string Megcell = "First";
            //try
            //{
            //    if (levelofpros == 1)
            //    {
            //        if (rowlevel[1] > 0)
            //        {
            //            Megcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].Cells[cellval].IsMergedWith(GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1] - 1].Cells[cellval]).ToString();
            //        }
            //    }
            //    else if (levelofpros == 2)
            //    {
            //        if (rowlevel[2] > 0)
            //        {
            //            Megcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].Cells[cellval].IsMergedWith(GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2] - 1].Cells[cellval]).ToString();
            //        }
            //    }
            //    else if (levelofpros == 3)
            //    {
            //        if (rowlevel[3] > 0)
            //        {
            //            Megcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].Cells[cellval].IsMergedWith(GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3] - 1].Cells[cellval]).ToString();
            //        }
            //    }
            //    else if (levelofpros == 4)
            //    {
            //        if (rowlevel[4] > 0)
            //        {
            //            Megcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].ChildBands[0].Rows[rowlevel[4]].Cells[cellval].IsMergedWith(GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].ChildBands[0].Rows[rowlevel[4] - 1].Cells[cellval]).ToString();
            //        }
            //    }
            //}
            //catch (System.Exception)
            //{
            //    // ex.Publish(e, false);
            //}
            return Megcell;
        }

        //Getting the value for the cell
        private string XMLforCell(int levelofpros, int cellval)
        {
            try
            {
                XMLcell = "";
                //if (levelofpros == 1)
                //{
                //    XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].Cells[cellval].Value.ToString();
                //}
                //else if (levelofpros == 2)
                //{
                //    XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].Cells[cellval].Value.ToString();
                //}
                //else if (levelofpros == 3)
                //{
                //    XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].Cells[cellval].Value.ToString();
                //}
                //else if (levelofpros == 4)
                //{
                //    XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].ChildBands[0].Rows[rowlevel[4]].Cells[cellval].Value.ToString();
                //}
            }
            catch (System.Exception)
            {

            }
            return XMLcell;
        }

        private string XMLforColName(int levelofpros, int cellval)
        {
            try
            {
                XMLcell = "";
                //if (levelofpros == 1)
                //{
                //    XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].Cells[cellval].Column.ToString();
                //}
                //else if (levelofpros == 2)
                //{
                //    XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].Cells[cellval].Column.ToString();
                //}
                //else if (levelofpros == 3)
                //{
                //    XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].Cells[cellval].Column.ToString();
                //}
                //else if (levelofpros == 4)
                //{
                //    XMLcell = GrdObj.Rows[rowlevel[0]].ChildBands[0].Rows[rowlevel[1]].ChildBands[0].Rows[rowlevel[2]].ChildBands[0].Rows[rowlevel[3]].ChildBands[0].Rows[rowlevel[4]].Cells[cellval].Column.ToString();
                //}
            }
            catch (System.Exception)
            {

            }
            return XMLcell;
        }
        public DataSet CatalogProductPreviewX(int catalogID, int familyID, int[] attributeList, string connvalue)
        {
            //////var asdf = new Connection();
            //////asdf.ConnSettings(connvalue);
            // ProductCatalog(catalogID, familyID, true, true, attributeList, true, "");
            _catName = "";
            _catalogId = catalogID;
            _familyId = familyID;
            _loadXFamily = true;
            _loadXProductTable = true;
            _attributeIdList = attributeList;
            _mergeFamilyWithProductTable = true;
            DataSet ds = GetProductCatalogPreview();
            ds.Tables.Add(ExtendedPropertiesPreview(catalogID, familyID));
            ds.Tables[0].Columns.Remove("FAMILYID");
            ds.Tables[1].Columns.Remove("FAMILYID");
            if (ds.Tables[0].Columns.Count < ds.Tables[1].Columns.Count)
            {
                int lastchkVal = ds.Tables[1].Columns.Count - ds.Tables[0].Columns.Count;
                for (int colCount = 0; colCount < lastchkVal; colCount++)
                {
                    ds.Tables[1].Columns.RemoveAt(ds.Tables[0].Columns.Count);
                }
            }
            return ds;
        }
        public DataSet GetProductCatalogPreview()
        {
            var dsPf = new DataSet { EnforceConstraints = false };
            if (_loadXProductTable)
            {
                //Load product table
                //var csProd = new ProductTable(_catalogId, _familyId, _attributeIdList, _catName);
                CatId = _catName;
                _FamilyId = _familyId;
                _AttributeIdList = _attributeIdList;
                DataTable dtProductTable = GetProductTablePreview(true);
                dtProductTable.TableName = "ProductTable";
                dsPf.Tables.Add(dtProductTable);
            }
            return dsPf;
        }
        public DataTable GetProductTablePreview(bool mainFamily)
        {
            //var taprod = new CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESTableAdapter();
            //var tblprod = new CSDBProvider.CSDS.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESDataTable();
            //var tacpa = new CSDBProvider.CSDSTableAdapters.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTTableAdapter();
            //var tblcpa = new CSDBProvider.CSDS.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTDataTable();
            DataSet dsprod = new DataSet();
            DataSet dscpa = new DataSet();
            DataTable taprod = new DataTable();
            DataTable tblprod = new DataTable();
            DataTable tacpa = new DataTable();
            DataTable tblcpa = new DataTable();
            _SQLString = "SELECT DISTINCT TB_PARTS_KEY.ATTRIBUTE_VALUE AS STRING_VALUE, TB_PARTS_KEY.PRODUCT_ID, TB_PARTS_KEY.ATTRIBUTE_ID, NULL AS NUMERIC_VALUE, " +
                " NULL AS OBJECT_TYPE, NULL AS OBJECT_NAME, TB_PROD_FAMILY.FAMILY_ID, CAST(" + _catalogId + " AS int) AS CATALOG_ID, (SELECT        ATTRIBUTE_DATATYPE  " +
                " FROM            TB_ATTRIBUTE AS TB_ATTRIBUTE_1 WHERE  TB_PROD_FAMILY.FLAG_RECYCLE='A' AND TB_ATTRIBUTE_1.FLAG_RECYCLE='A'  AND  (ATTRIBUTE_ID = TB_PARTS_KEY.ATTRIBUTE_ID) and FLAG_RECYCLE='A') AS ATTRIBUTE_DATATYPE FROM TB_PROD_FAMILY INNER JOIN " +
                " TB_PARTS_KEY ON TB_PROD_FAMILY.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN TB_CATALOG_PRODUCT ON TB_CATALOG_PRODUCT.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID AND  " +
                " TB_PROD_FAMILY.FAMILY_ID = TB_PARTS_KEY.FAMILY_ID WHERE  (TB_PARTS_KEY.FAMILY_ID = " + _FamilyId + ") AND (TB_CATALOG_PRODUCT.CATALOG_ID = CAST(" + _catalogId + " AS int)) AND  " +
                " (TB_PROD_FAMILY.PUBLISH2PRINT = 1) UNION SELECT DISTINCT  TB_PROD_SPECS.STRING_VALUE, TB_PROD_SPECS.PRODUCT_ID, TB_PROD_SPECS.ATTRIBUTE_ID, TB_PROD_SPECS.NUMERIC_VALUE,  " +
                " TB_PROD_SPECS.OBJECT_TYPE, TB_PROD_SPECS.OBJECT_NAME, TB_PROD_FAMILY_3.FAMILY_ID, CAST(" + _catalogId + " AS int) AS CATALOG_ID, (SELECT        ATTRIBUTE_DATATYPE " +
                " FROM            TB_ATTRIBUTE  WHERE        (ATTRIBUTE_ID = TB_PROD_SPECS.ATTRIBUTE_ID) and FLAG_RECYCLE='A') AS ATTRIBUTE_DATATYPE FROM TB_PROD_FAMILY AS TB_PROD_FAMILY_3 INNER JOIN " +
                " TB_PROD_SPECS ON TB_PROD_FAMILY_3.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN TB_CATALOG_PRODUCT AS TB_CATALOG_PRODUCT_3 ON TB_CATALOG_PRODUCT_3.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID " +
                " WHERE        (TB_PROD_FAMILY_3.FAMILY_ID = " + _FamilyId + ") AND (TB_CATALOG_PRODUCT_3.CATALOG_ID = CAST(" + _catalogId + " AS int)) AND  " +
                " (TB_PROD_FAMILY_3.PUBLISH2PRINT = 1) UNION SELECT DISTINCT  CAST(TB_PROD_FAMILY_2.SORT_ORDER AS nvarchar(MAX)) AS STRING_VALUE, TB_PROD_SPECS_2.PRODUCT_ID, 0 AS ATTRIBUTE_ID, NULL  " +
                " AS NUMERIC_VALUE, NULL AS OBJECT_TYPE, NULL AS OBJECT_NAME, TB_PROD_FAMILY_2.FAMILY_ID, CAST(" + _catalogId + " AS int) AS CATALOG_ID,  'Text' AS ATTRIBUTE_DATATYPE FROM            TB_PROD_FAMILY AS TB_PROD_FAMILY_2 INNER JOIN " +
                " TB_PROD_SPECS AS TB_PROD_SPECS_2 ON TB_PROD_FAMILY_2.PRODUCT_ID = TB_PROD_SPECS_2.PRODUCT_ID INNER JOIN TB_CATALOG_PRODUCT AS TB_CATALOG_PRODUCT_2 ON TB_CATALOG_PRODUCT_2.PRODUCT_ID = TB_PROD_SPECS_2.PRODUCT_ID " +
                " WHERE        (TB_CATALOG_PRODUCT_2.CATALOG_ID = CAST(" + _catalogId + " AS int)) AND (TB_PROD_FAMILY_2.FAMILY_ID = " + _FamilyId + ") AND  (TB_PROD_FAMILY_2.PUBLISH2PRINT = 1) UNION SELECT DISTINCT  " +
                " CAST(0 AS nvarchar(MAX)) AS STRING_VALUE, TB_PROD_SPECS_1.PRODUCT_ID, - 1 AS ATTRIBUTE_ID, NULL AS NUMERIC_VALUE, NULL AS OBJECT_TYPE, NULL  AS OBJECT_NAME, TB_PROD_FAMILY_1.FAMILY_ID, CAST(" + _catalogId + " AS int) AS CATALOG_ID, 'Text' AS ATTRIBUTE_DATATYPE " +
                " FROM   TB_PROD_FAMILY AS TB_PROD_FAMILY_1 INNER JOIN TB_PROD_SPECS AS TB_PROD_SPECS_1 ON TB_PROD_FAMILY_1.PRODUCT_ID = TB_PROD_SPECS_1.PRODUCT_ID INNER JOIN TB_CATALOG_PRODUCT AS TB_CATALOG_PRODUCT_1 ON TB_CATALOG_PRODUCT_1.PRODUCT_ID = TB_PROD_SPECS_1.PRODUCT_ID " +
                " WHERE        (TB_CATALOG_PRODUCT_1.CATALOG_ID = CAST(" + _catalogId + " AS int)) AND (TB_PROD_FAMILY_1.FAMILY_ID = " + _FamilyId + ") AND (TB_PROD_FAMILY_1.PUBLISH2PRINT = 1)";
            dsprod = CreateDataSet();
            taprod = dsprod.Tables[0];

            //taprod.FillByCatalogFamilyExPreview(tblprod, _catalogId, _FamilyId);
            #region "Changes for changing Time Format"
            //foreach (DataRow dr in tblprod)
            //{
            //    if (dr["ATTRIBUTE_DATATYPE"].ToString() == "Date and Time")
            //    {
            //        string AttrId = dr["ATTRIBUTE_ID"].ToString();
            //        DataSet DS = new DataSet();
            //        var conn = new TradingBell.CatalogX.CSDBProvider.Connection();
            //        SqlConnection ocon = new SqlConnection(conn.GetConnSettings());
            //        string cmd = "SELECT ATTRIBUTE_DATAFORMAT FROM TB_ATTRIBUTE WHERE ATTRIBUTE_ID=" + AttrId + "";
            //        SqlCommand sqlcmd = new SqlCommand(cmd, ocon);
            //        SqlDataAdapter DA = new SqlDataAdapter(sqlcmd);
            //        DA.Fill(DS);
            //        if (DS.Tables[0].Rows[0].ItemArray[0].ToString() != "" && DS.Tables[0].Rows[0].ItemArray[0].ToString() != null)
            //        {
            //            if (DS.Tables[0].Rows[0].ItemArray[0].ToString().StartsWith("(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}"))
            //            {
            //                if (dr["STRING_VALUE"].ToString() != null)
            //                {
            //                    string temp = dr["STRING_VALUE"].ToString();
            //                    dr["STRING_VALUE"] = temp.Substring(3, 2) + "/" + temp.Substring(0, 2) + "/" + temp.Substring(6, 4);
            //                }
            //            }
            //        }

            //    }
            //}
            #endregion

            _SQLString = "SELECT  0 AS ATTRIBUTE_ID, 'Sort' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 2 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE UNION " +
                        " SELECT        - 1 AS ATTRIBUTE_ID, 'Publish' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE, - 1 AS SORT_ORDER, 'Text' AS ATTRIBUTE_DATATYPE " +
                        " UNION SELECT DISTINCT  TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_PROD_FAMILY_ATTR_LIST.SORT_ORDER,  " +
                         " TB_ATTRIBUTE.ATTRIBUTE_DATATYPE FROM            TB_PROD_FAMILY INNER JOIN TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN " +
                         " TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.PUBLISH2PRINT = 1 INNER JOIN TB_PROD_FAMILY_ATTR_LIST ON " +
                         " TB_PROD_FAMILY.FAMILY_ID = TB_PROD_FAMILY_ATTR_LIST.FAMILY_ID AND  TB_PROD_SPECS.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST.ATTRIBUTE_ID INNER JOIN TB_CATALOG_ATTRIBUTES ON TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND  " +
                          "  TB_CATALOG_ATTRIBUTES.CATALOG_ID = " + _catalogId + " WHERE        (TB_PROD_FAMILY.FAMILY_ID = " + _FamilyId + ") AND (- 1 <> " + _catalogId + ") UNION SELECT DISTINCT TB_ATTRIBUTE_5.ATTRIBUTE_ID, TB_ATTRIBUTE_5.ATTRIBUTE_NAME, TB_ATTRIBUTE_5.ATTRIBUTE_TYPE,  " +
                         " TB_PROD_FAMILY_ATTR_LIST_3.SORT_ORDER, TB_ATTRIBUTE_5.ATTRIBUTE_DATATYPE FROM            TB_PROD_FAMILY AS TB_PROD_FAMILY_5 INNER JOIN  TB_PARTS_KEY ON TB_PROD_FAMILY_5.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN " +
                         " TB_ATTRIBUTE AS TB_ATTRIBUTE_5 ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE_5.ATTRIBUTE_ID AND  TB_ATTRIBUTE_5.PUBLISH2PRINT = 1 INNER JOIN TB_PROD_FAMILY_ATTR_LIST AS TB_PROD_FAMILY_ATTR_LIST_3 ON  " +
                         " TB_PROD_FAMILY_5.FAMILY_ID = TB_PROD_FAMILY_ATTR_LIST_3.FAMILY_ID AND  TB_PARTS_KEY.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST_3.ATTRIBUTE_ID INNER JOIN " +
                         " TB_CATALOG_ATTRIBUTES AS TB_CATALOG_ATTRIBUTES_5 ON TB_CATALOG_ATTRIBUTES_5.ATTRIBUTE_ID = TB_ATTRIBUTE_5.ATTRIBUTE_ID AND TB_CATALOG_ATTRIBUTES_5.CATALOG_ID = " + _catalogId + " " +
                         " WHERE        (TB_PROD_FAMILY_5.FAMILY_ID = " + _FamilyId + ") AND (- 1 <> " + _catalogId + ") ORDER BY SORT_ORDER";
            dscpa = CreateDataSet();
            tacpa = dscpa.Tables[0];
            //tacpa.FillByPreview(tblcpa, _catalogId, _FamilyId);


            DataTable dt = GetTransposedProductTable(taprod, tacpa);


            dt.Columns.Add("FAMILYID", typeof(int));

            foreach (DataRow dr in dt.Rows)
            {
                dr["FAMILYID"] = dr["FAMILY_ID"];
            }
            return dt;
        }
        private DataTable GetTransposedProductTable(DataTable productInfo, DataTable productAttributes)
        {

            var dtnew = new DataTable();
            dtnew.Columns.Add("CATALOG_ID", typeof(int));
            dtnew.Columns.Add("FAMILY_ID", typeof(int));
            dtnew.Columns.Add("PRODUCT_ID", typeof(int));
            if (PDFCatalog)
                dtnew.Columns.Add("SORT_ORDER", typeof(int));

            var dcpks = new DataColumn[3];
            if (PDFCatalog)
            {
                dcpks = new DataColumn[4];
                dcpks[0] = dtnew.Columns["CATALOG_ID"];
                dcpks[1] = dtnew.Columns["FAMILY_ID"];
                dcpks[2] = dtnew.Columns["PRODUCT_ID"];
                dcpks[3] = dtnew.Columns["SORT_ORDER"];
            }
            else
            {
                dcpks[0] = dtnew.Columns["CATALOG_ID"];
                dcpks[1] = dtnew.Columns["FAMILY_ID"];
                dcpks[2] = dtnew.Columns["PRODUCT_ID"];
            }

            dtnew.Constraints.Add("PK", dcpks, true);
            foreach (DataRow dr in productAttributes.Rows)
            {

                if (_AttributeIdList.Length > 0)
                {
                    bool exists = CheckExists(Convert.ToInt32(dr["ATTRIBUTE_ID"].ToString()));
                    if (exists)
                    {
                        //_AttributeIdList[iCtr]
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                        dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        {
                            productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                            dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                        }
                        else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        {
                            productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                            dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString(), typeof(Double));
                        }
                        else
                        {
                            productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                            dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                        }


                        //enhance - Make sure to check if the column exists in this table

                        //OLD CODE------------
                        //DataRow[] drs = ProductInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"], "FAMILY_ID,SORT_ORDER");
                        //DataTable dtsel;
                        //dtsel = ProductInfo.Clone();
                        //for (int i = 0; i < drs.Length; i++)
                        //{
                        //    dtsel.ImportRow(drs[i]);
                        //}
                        //-------------------
                        DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"], "FAMILY_ID,SORT_ORDER");
                        var dtsel = new DataTable();
                        if (drs.Any())
                        {
                            dtsel = drs.CopyToDataTable();
                        }

                        try
                        {
                            if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                                if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" && dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                                {
                                    bool stringValue = dtsel.Rows.Cast<DataRow>().Any(dr1 => dr1["STRING_VALUE"].ToString().Length == 0);
                                    if (stringValue == false)
                                    {
                                        dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                        dtsel.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                    }
                                }
                        }
                        catch (Exception) { }
                        dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                        dtsel.Dispose();
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                            dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        {
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                        }
                        else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                        else
                            productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                    }
                }
                else
                {
                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                    dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                    {
                        productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        productInfo.Columns["NUMERIC_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                    else
                        productInfo.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();




                    //enhance - Make sure to check if the column exists in this table
                    //if(dtnew.Columns[dr["ATTRIBUTE_NAME"]].ColumnName.Contains(dr["ATTRIBUTE_NAME"].ToString()))

                    dtnew.Columns.Add(dr["ATTRIBUTE_NAME"].ToString());
                    DataRow[] drs = productInfo.Select("ATTRIBUTE_ID = " + dr["ATTRIBUTE_ID"]);
                    DataTable dtsel;
                    dtsel = productInfo.Clone();
                    for (int i = 0; i < drs.Length; i++)
                    {
                        dtsel.ImportRow(drs[i]);
                        //    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                        //dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                        //    {
                        //        dtsel.Rows[i].ItemArray[7] = 1;// Convert.ToDecimal(dtsel.Rows[i].ItemArray[0]);
                        //    }
                    }
                    // int i = 0;
                    //foreach (DataRow dr2 in dtnew.Rows)
                    //{
                    //    foreach (DataRow dr1 in dtsel.Rows)
                    //    {
                    //        dr2[dr["ATTRIBUTE_NAME"].ToString()] = dr1["STRING_VALUE"].ToString();
                    //        break;
                    //    }
                    //}
                    try
                    {
                        if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                            if (dr["ATTRIBUTE_NAME"].ToString() != "Publish" && dr["ATTRIBUTE_NAME"].ToString() != "Sort")
                            {
                                bool stringValue = false;
                                foreach (DataRow dr1 in dtsel.Rows)
                                {
                                    if (dr1["STRING_VALUE"].ToString().Length == 0)
                                    {
                                        stringValue = true;
                                        break;
                                    }
                                }
                                if (stringValue == false)
                                {
                                    dtsel.Columns.Remove(dr["ATTRIBUTE_NAME"].ToString());
                                    dtsel.Columns["STRING_VALUE"].ColumnName = dr["ATTRIBUTE_NAME"].ToString();
                                }
                            }
                    }
                    catch (Exception) { }
                    dtnew.Merge(dtsel, false, MissingSchemaAction.Ignore);
                    dtsel.Dispose();
                    if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM" &&
                    dr["ATTRIBUTE_TYPE"].ToString().ToUpper() == "6")
                    {
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString().Substring(0, 3).ToUpper() == "NUM")
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "NUMERIC_VALUE";
                    else
                        productInfo.Columns[dr["ATTRIBUTE_NAME"].ToString()].ColumnName = "STRING_VALUE";

                }
            }

            return dtnew;
        }
        private DataTable ExtendedPropertiesPreview(int catalogID, int familyID)
        {
            var et = new DataTable("ExtendedProperties");
            var iAttrList = new int[0];

            //for (int i = 0; i < iAttrList.Length - 1; i++)
            //{
            //    //iAttrList[i] = (int) grdSelect.Rows[i].Cells["ATTRIBUTE_ID"].Value;
            //}
            //  var csPt = new CSDBProviderEX.ProductTable(catalogID, familyID, iAttrList, "");
            CatId = "";
            _catalogId = catalogID;
            _FamilyId = familyID;
            _AttributeIdList = iAttrList;
            DataTable eet = GetProductTableExPropertiesPreview(true);
            foreach (DataColumn dc in eet.Columns)
                et.Columns.Add(dc.Caption, dc.DataType);
            foreach (DataRow dr in eet.Rows)
                et.ImportRow(dr);
            return et;
        }
        public DataTable GetProductTableExPropertiesPreview(bool mainFamily)
        {
            DataSet dsprod = new DataSet();
            DataSet dscpa = new DataSet();
            DataTable taprod = new DataTable();
            DataTable tblprod = new DataTable();
            DataTable tacpa = new DataTable();
            DataTable tblcpa = new DataTable();

            DataTable dt2 = new DataTable("Product2");

            //TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESTableAdapter taprod = new TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESTableAdapter();
            //TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESDataTable tblprod = new TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_FAMILY_PRODUCT_ATTRIBUTESDataTable();


            //TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTTableAdapter tacpa = new TradingBell.CatalogX.CSDBProvider.CSDSTableAdapters.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTTableAdapter();
            //TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTDataTable tblcpa = new TradingBell.CatalogX.CSDBProvider.CSDS.EX_CATALOG_PRODUCT_ATTRIBUTE_LISTDataTable();
            _SQLString = @"SELECT DISTINCT  CAST(TB_ATTRIBUTE.ATTRIBUTE_ID AS nvarchar(MAX)) AS STRING_VALUE, - 3 AS PRODUCT_ID, TB_PARTS_KEY.ATTRIBUTE_ID as ATTRIBUTE_ID, " +
                         " TB_PARTS_KEY.ATTRIBUTE_ID AS NUMERIC_VALUE, null as OBJECT_TYPE, null as OBJECT_NAME, TB_PROD_FAMILY.FAMILY_ID,   " + _catalogId + "    AS CATALOG_ID " +
                         " FROM            TB_PROD_FAMILY INNER JOIN  TB_PARTS_KEY ON TB_PROD_FAMILY.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN TB_ATTRIBUTE ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID " +
                         " WHERE        (-1 <>  " + _catalogId + " ) and TB_PROD_FAMILY.FLAG_RECYCLE='A' AND (TB_PROD_FAMILY.FAMILY_ID =  " + _FamilyId + " ) UNION SELECT DISTINCT  CAST(TB_ATTRIBUTE.ATTRIBUTE_ID AS nvarchar(MAX)) AS STRING_VALUE, - 3 AS PRODUCT_ID, TB_PROD_SPECS.ATTRIBUTE_ID as ATTRIBUTE_ID,   TB_ATTRIBUTE.ATTRIBUTE_ID AS NUMERIC_VALUE,  null as OBJECT_TYPE, null as OBJECT_NAME,  " +
                         " TB_PROD_FAMILY.FAMILY_ID,  " + _catalogId + "  AS CATALOG_ID  FROM            TB_PROD_FAMILY INNER JOIN  TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN " +
                          " TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID  WHERE        (-1 <>  " + _catalogId + " ) AND (TB_PROD_FAMILY.FAMILY_ID =  " + _FamilyId + " ) " +
                         "   UNION SELECT DISTINCT  CAST(TB_ATTRIBUTE.ATTRIBUTE_TYPE AS nvarchar(MAX)) AS STRING_VALUE, - 2 AS PRODUCT_ID, TB_PROD_SPECS.ATTRIBUTE_ID as ATTRIBUTE_ID,  " +
                         "  TB_ATTRIBUTE.ATTRIBUTE_TYPE AS NUMERIC_VALUE,  null as OBJECT_TYPE, null as OBJECT_NAME,                           TB_PROD_FAMILY.FAMILY_ID,  " + _catalogId + "  AS CATALOG_ID " +
                         " FROM            TB_PROD_FAMILY INNER JOIN  TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN  TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID " +
                         " WHERE        (-1 <>  " + _catalogId + " ) and TB_PROD_FAMILY.FLAG_RECYCLE='A' AND (TB_PROD_FAMILY.FAMILY_ID =  " + _FamilyId + " )  UNION    SELECT DISTINCT  TB_ATTRIBUTE.STYLE_NAME AS STRING_VALUE, - 1 AS PRODUCT_ID, TB_PROD_SPECS.ATTRIBUTE_ID  as ATTRIBUTE_ID,  " +
                         "  null  AS NUMERIC_VALUE,  null as OBJECT_TYPE, null as OBJECT_NAME,  TB_PROD_FAMILY.FAMILY_ID,  " + _catalogId + "  AS CATALOG_ID  FROM            TB_PROD_FAMILY INNER JOIN                         " +
                         "  TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN  TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID  WHERE        (-1 <>  " + _catalogId + " ) AND (TB_PROD_FAMILY.FAMILY_ID =  " + _FamilyId + " ) " +
                         "  union  SELECT DISTINCT  CAST(TB_ATTRIBUTE.ATTRIBUTE_TYPE AS nvarchar(MAX)) AS STRING_VALUE, - 2 AS PRODUCT_ID, TB_PARTS_KEY.ATTRIBUTE_ID as ATTRIBUTE_ID,  TB_ATTRIBUTE.ATTRIBUTE_TYPE AS NUMERIC_VALUE, null as OBJECT_TYPE, null as OBJECT_NAME,  " +
                         "  TB_PROD_FAMILY.FAMILY_ID,  " + _catalogId + "  AS CATALOG_ID  FROM            TB_PROD_FAMILY INNER JOIN  TB_PARTS_KEY ON TB_PROD_FAMILY.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN " +
                         " TB_ATTRIBUTE ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <>  " + _catalogId + " ) AND (TB_PROD_FAMILY.FAMILY_ID =  " + _FamilyId + " )  UNION SELECT DISTINCT  TB_ATTRIBUTE.STYLE_NAME AS STRING_VALUE, - 1 AS PRODUCT_ID, TB_PARTS_KEY.ATTRIBUTE_ID  as ATTRIBUTE_ID,  " +
                         " null  AS NUMERIC_VALUE, null as OBJECT_TYPE, null as OBJECT_NAME,  TB_PROD_FAMILY.FAMILY_ID,  " + _catalogId + "  AS CATALOG_ID FROM            TB_PROD_FAMILY INNER JOIN   TB_PARTS_KEY ON TB_PROD_FAMILY.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN " +
                         " TB_ATTRIBUTE ON TB_PARTS_KEY.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID WHERE        (-1 <>  " + _catalogId + " ) AND (TB_PROD_FAMILY.FAMILY_ID =  " + _FamilyId + " ) ORDER BY PRODUCT_ID, ATTRIBUTE_ID";
            dsprod = CreateDataSet();
            taprod = dsprod.Tables[0];
            // taprod.FillByCatalogFamilyExProperties(tblprod, _catalogId, _FamilyId);
            _SQLString = "SELECT        0 AS ATTRIBUTE_ID, 'Sort' AS ATTRIBUTE_NAME, 0 AS ATTRIBUTE_TYPE,  - 2 AS SORT_ORDER, 'Text' AS  ATTRIBUTE_DATATYPE " +
                         " UNION SELECT        - 1 AS ATTRIBUTE_ID,  'Publish' AS ATTRIBUTE_NAME, 0 AS   ATTRIBUTE_TYPE, - 1 AS SORT_ORDER,  'Text' AS ATTRIBUTE_DATATYPE " +
                         " UNION SELECT DISTINCT  TB_ATTRIBUTE.ATTRIBUTE_ID, TB_ATTRIBUTE.ATTRIBUTE_NAME, TB_ATTRIBUTE.ATTRIBUTE_TYPE, TB_PROD_FAMILY_ATTR_LIST.SORT_ORDER, " +
                         " TB_ATTRIBUTE.ATTRIBUTE_DATATYPE FROM            TB_PROD_FAMILY INNER  JOIN  TB_PROD_SPECS ON TB_PROD_FAMILY.PRODUCT_ID = TB_PROD_SPECS.PRODUCT_ID INNER JOIN " +
                         " TB_ATTRIBUTE ON TB_PROD_SPECS.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_ATTRIBUTE.PUBLISH2PRINT = 1 INNER JOIN TB_PROD_FAMILY_ATTR_LIST ON " +
                         " TB_PROD_FAMILY.FAMILY_ID = TB_PROD_FAMILY_ATTR_LIST.FAMILY_ID AND  TB_PROD_SPECS.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST.ATTRIBUTE_ID  " +
                         " INNER JOIN TB_CATALOG_ATTRIBUTES ON  TB_CATALOG_ATTRIBUTES.ATTRIBUTE_ID = TB_ATTRIBUTE.ATTRIBUTE_ID AND TB_CATALOG_ATTRIBUTES.CATALOG_ID = " + _catalogId + " WHERE (TB_PROD_FAMILY.FAMILY_ID = " + _FamilyId + ") AND (- 1 <> " + _catalogId + ") " +
                         " UNION SELECT DISTINCT  TB_ATTRIBUTE_5.ATTRIBUTE_ID,  TB_ATTRIBUTE_5.ATTRIBUTE_NAME,  TB_ATTRIBUTE_5.ATTRIBUTE_TYPE,  TB_PROD_FAMILY_ATTR_LIST_3.SORT_ORDER, TB_ATTRIBUTE_5.ATTRIBUTE_DATATYPE " +
                         " FROM  TB_PROD_FAMILY AS  TB_PROD_FAMILY_5 INNER JOIN   TB_PARTS_KEY  ON TB_PROD_FAMILY_5.PRODUCT_ID = TB_PARTS_KEY.PRODUCT_ID INNER JOIN  TB_ATTRIBUTE  AS TB_ATTRIBUTE_5 ON TB_PARTS_KEY.ATTRIBUTE_ID = " +
                         " TB_ATTRIBUTE_5.ATTRIBUTE_ID AND TB_ATTRIBUTE_5.PUBLISH2PRINT = 1 INNER JOIN TB_PROD_FAMILY_ATTR_LIST AS  TB_PROD_FAMILY_ATTR_LIST_3 ON  " +
                         " TB_PROD_FAMILY_5.FAMILY_ID =  TB_PROD_FAMILY_ATTR_LIST_3.FAMILY_ID AND TB_PARTS_KEY.ATTRIBUTE_ID = TB_PROD_FAMILY_ATTR_LIST_3.ATTRIBUTE_ID INNER JOIN " +
                         " TB_CATALOG_ATTRIBUTES AS TB_CATALOG_ATTRIBUTES_5 ON TB_CATALOG_ATTRIBUTES_5.ATTRIBUTE_ID = TB_ATTRIBUTE_5.ATTRIBUTE_ID AND TB_CATALOG_ATTRIBUTES_5.CATALOG_ID = " + _catalogId + " " +
                         " WHERE (TB_PROD_FAMILY_5.FAMILY_ID = " + _FamilyId + ") AND (- 1 <> " + _catalogId + ") ORDER BY SORT_ORDER";
            dscpa = CreateDataSet();
            tacpa = dscpa.Tables[0];
            // tacpa.FillByPreview(tblcpa, _catalogId, _FamilyId);

            DataTable dt = GetTransposedProductTable(taprod, tacpa);

            dt.Columns.Add("FAMILYID", typeof(int));

            return dt;
        }
        private bool CheckExists(int iattrID)
        {
            if (iattrID == 0)
            {
                //  return true;
            }
            return _AttributeIdList.Any(t => iattrID == t);
        }
        public DataSet CreateDataSet()
        {
            DataSet dsReturn = new DataSet();
            using (
                var objSqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlDataAdapter _DBAdapter = new SqlDataAdapter(_SQLString, objSqlConnection);
                _DBAdapter.SelectCommand.CommandTimeout = 0;
                _DBAdapter.Fill(dsReturn);
                _DBAdapter.Dispose();
                return dsReturn;
            }
        }

        [System.Web.Http.HttpGet]
        public IList LoadAttributes(int catalogId, string familyId)
        {
            try
            {

                var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                if (customerid != null)
                {
                    var FamilyList = _dbcontext.TB_CATEGORY_FAMILY_ATTR_LIST.Where(x => x.FLAG_RECYCLE == null || x.FLAG_RECYCLE == "").ToList();
                    FamilyList.ForEach(a => a.FLAG_RECYCLE = "A");
                    _dbcontext.SaveChanges();
                    _SQLString = "  SELECT DISTINCT TA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME,TA.ATTRIBUTE_TYPE " +
                                 "FROM TB_ATTRIBUTE TA JOIN TB_CATALOG_ATTRIBUTES TCA ON TCA.ATTRIBUTE_ID=TA.ATTRIBUTE_ID AND TCA.CATALOG_ID=" + catalogId + " and TA.FLAG_RECYCLE='A'" +
                                 " JOIN TB_PROD_FAMILY_ATTR_LIST TPAL ON TPAL.ATTRIBUTE_ID=TA.ATTRIBUTE_ID " +
                                 "JOIN CUSTOMERATTRIBUTE CA ON CA.ATTRIBUTE_ID=TA.ATTRIBUTE_ID " +
                                 "WHERE CUSTOMER_ID=" + customerid.CustomerId + " AND TPAL.FAMILY_ID IN("
                                 + " SELECT FAMILY_ID FROM TB_FAMILY WHERE FAMILY_ID IN (" + familyId + ") "
                                 + " UNION "
                                 + " SELECT FAMILY_ID FROM TB_FAMILY WHERE PARENT_FAMILY_ID IN (" + familyId + "))";
                    ////DataSet _dtAvalAttr = Con.CreateDataSet();
                    _dtAvailAttr = CreateDataSet();
                    var prodspecAttributesPublished = _dtAvailAttr.Tables[0].AsEnumerable().Select(dr =>
                        new
                        {
                            ATTRIBUTE_ID = Convert.ToInt32(dr.Field<int>("ATTRIBUTE_ID")),
                            ATTRIBUTE_NAME = dr.Field<string>("ATTRIBUTE_NAME"),
                            ATTRIBUTE_TYPE = dr.Field<byte>("ATTRIBUTE_TYPE"),
                            CATALOG_ID = catalogId,
                            FAMILY_ID = 0,
                            ISAvailable = false
                        }
                        ).Distinct().ToList();


                    if (!familyId.Contains(','))
                    {
                        familyId = familyId + ",";
                    }

                    int _customerid = Convert.ToInt32(customerid.CustomerId);
                    int[] attributeType = { 1, 3, 4, 6 };
                    int[] _familyId = new int[familyId.Split(',').Length];
                    for (int i = 0; i < familyId.Split(',').Length - 1; i++)
                    {
                        _familyId[i] = int.Parse(familyId.Split(',')[i].ToString());
                    }
                    var catalogs = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(_dbcontext.TB_ATTRIBUTE, tca => tca.ATTRIBUTE_ID, ta => ta.ATTRIBUTE_ID, (tca, ta) => new { tca, ta })
                        .Join(_dbcontext.CUSTOMERATTRIBUTE, tcaa => tcaa.ta.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (tcaa, ca) => new { tcaa, ca })
                        .Join(_dbcontext.TB_PROD_SPECS, tcaac => tcaac.ca.ATTRIBUTE_ID, tps => tps.ATTRIBUTE_ID, (tcaac, tps) => new { tcaac, tps })
                        .Join(_dbcontext.TB_PROD_FAMILY, tcaact => tcaact.tps.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (tcaact, tpf) => new { tcaact, tpf })
                        .Where(x => x.tcaact.tcaac.tcaa.tca.CATALOG_ID == catalogId && attributeType.Contains(x.tcaact.tcaac.tcaa.ta.ATTRIBUTE_TYPE) && _familyId.Contains(x.tpf.FAMILY_ID) && x.tcaact.tcaac.ca.CUSTOMER_ID == _customerid)
                        .Select(x => new
                        {

                            x.tcaact.tcaac.tcaa.tca.ATTRIBUTE_ID,
                            x.tcaact.tcaac.tcaa.ta.ATTRIBUTE_NAME,
                            x.tcaact.tcaac.tcaa.ta.ATTRIBUTE_TYPE,
                            x.tcaact.tcaac.tcaa.tca.CATALOG_ID,
                            FAMILY_ID = 0,
                            ISAvailable = false
                        }).Distinct().ToList();
                    var catalogallAttribtues = prodspecAttributesPublished; //prodspecAttributesPublished.Except(catalogs).ToList();//prodspecAttributesPublished;
                    return catalogallAttribtues;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception objException)
            {
                Logger.Error("Error at InDesignApiController : LoadAttributes", objException);
                return null;
            }

        }


        [System.Web.Http.HttpPost]
        public IList GetAllInDesignAttributes(int catalogId, string familyId)
        {
            try
            {
                var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                if (customerid != null)
                {
                    int _customerid = Convert.ToInt32(customerid.CustomerId);
                    int[] attributeType = { 1, 3, 4, 6 };
                    int[] _familyId = new int[familyId.Split(',').Length];
                    for (int i = 0; i < familyId.Split(',').Length - 1; i++)
                    {
                        _familyId[i] = int.Parse(familyId.Split(',')[i].ToString());
                    }

                    //_SQLString = " SELECT DISTINCT TCA.ATTRIBUTE_ID,TA.ATTRIBUTE_NAME,TCA.ATTRIBUTE_TYPE FROM TB_CATALOG_ATTRIBUTES TCA " +
                    //             "JOIN TB_ATTRIBUTE TA  ON TA.ATTRIBUTE_ID=TCA.ATTRIBUTE_ID " +
                    //             "JOIN CUSTOMERATTRIBUTE CA ON CA.ATTRIBUTE_ID=TA.ATTRIBUTE_ID" +
                    //             "JOIN TB_PROD_SPECS TPS ON CA.ATTRIBUTE_ID=TPS.ATTRIBUTE_ID " +
                    //             "JOIN TB_PROD_FAMILY TPF ON TPS.PRODUCT_ID=TPF.PRODUCT_ID" +
                    //             "WHERE CATALOG_ID=" + catalogId + " AND TA.ATTRIBUTE_TYPE IN (1,3,4,6) AND FAMILY_ID IN (" + familyId + ") AND CA.CUSTOMER_ID " + customerid + "";
                    //_selectattr = CreateDataSet();

                    var catalogs = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(_dbcontext.TB_ATTRIBUTE, tca => tca.ATTRIBUTE_ID, ta => ta.ATTRIBUTE_ID, (tca, ta) => new { tca, ta })
                        .Join(_dbcontext.CUSTOMERATTRIBUTE, tcaa => tcaa.ta.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (tcaa, ca) => new { tcaa, ca })
                        .Join(_dbcontext.TB_PROD_SPECS, tcaac => tcaac.ca.ATTRIBUTE_ID, tps => tps.ATTRIBUTE_ID, (tcaac, tps) => new { tcaac, tps })
                        .Join(_dbcontext.TB_PROD_FAMILY, tcaact => tcaact.tps.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (tcaact, tpf) => new { tcaact, tpf })
                        .Where(x => x.tcaact.tcaac.tcaa.tca.CATALOG_ID == catalogId && attributeType.Contains(x.tcaact.tcaac.tcaa.ta.ATTRIBUTE_TYPE) && _familyId.Contains(x.tpf.FAMILY_ID) && x.tcaact.tcaac.ca.CUSTOMER_ID == _customerid)
                        .Select(x => new
                        {
                            x.tcaact.tcaac.tcaa.tca.CATALOG_ID,
                            FAMILY_ID = 0,
                            FAMILYID = 0,
                            x.tcaact.tcaac.tcaa.tca.ATTRIBUTE_ID,
                            x.tcaact.tcaac.tcaa.ta.ATTRIBUTE_NAME,
                            x.tcaact.tcaac.tcaa.ta.ATTRIBUTE_TYPE,
                            ISAvailable = false
                        }).Distinct();
                    //var catalogs =
                    //    _dbcontext.TB_CATALOG_ATTRIBUTES.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                    //        .Where(x => x.tcp.CATALOG_ID == catalogId && attributeType.Contains(x.tcp.TB_ATTRIBUTE.ATTRIBUTE_TYPE) && x.tpf.CUSTOMER_ID == customerid.CustomerId)
                    //        .Select(x => new
                    //        {
                    //            x.tcp.CATALOG_ID,
                    //            FAMILY_ID = 0,
                    //            x.tcp.TB_ATTRIBUTE.ATTRIBUTE_ID,
                    //            x.tcp.TB_ATTRIBUTE.ATTRIBUTE_NAME,
                    //            x.tcp.TB_ATTRIBUTE.ATTRIBUTE_TYPE,
                    //            ISAvailable = false
                    //        }).Distinct();

                    // var catalogallAttribtues = catalogs.Except(prodspecAttributes).ToList();
                    return catalogs.OrderBy(y => y.ATTRIBUTE_ID).ToList();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController : GetAllCatalogattributes", objException);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage GetIndesignFamilyValues(int catalogId, JArray model) //
        {
            try
            {
                var FamilyList = _dbcontext.TB_CATEGORY_FAMILY_ATTR_LIST.Where(x => x.FLAG_RECYCLE == null || x.FLAG_RECYCLE == "").ToList();
                FamilyList.ForEach(a => a.FLAG_RECYCLE = "A");
                _dbcontext.SaveChanges();

                var attributes = model.Select(x => x);
                var familyid = string.Empty;
                foreach (var selectattributes in attributes)
                {
                    // var objAttributedetails = new AttributePublish();
                    familyid += Convert.ToString(selectattributes.SelectToken("FAMILY_ID")) + ",";
                }
                familyid = familyid.TrimEnd(',');
                IList objlist = LoadAttributes(catalogId, familyid);
                if (objlist.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, familyid);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "0");
                }

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InDesignApiController : XmlDataFiles", objexception);
                return null;
            }

        }


        #region Indesign Attribute Wise Filter

        /// <summary>
        /// Added by Aswin kumar.M
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns>It returns the filter values</returns>

        public IList GetIndesignAllFiltersValue(string attributeName)
        {
            try
            {
                var Filters = new List<IndesignFilters>();

                if (attributeName != "undefined")
                {

                    string attribute_Datatype = _dbcontext.TB_ATTRIBUTE.Where(a => a.ATTRIBUTE_NAME == attributeName).Select(a => a.ATTRIBUTE_DATATYPE).FirstOrDefault();
                    if (attribute_Datatype != null)
                    {


                        if (attribute_Datatype.Contains("Number"))
                        {
                            var filtersName = new string[] { "Equal to", "Does Not Equal to", "Greater Than", "Greater Than Or Equal to", "Less Than", "Less Than Or Equal to", "Empty", "Exists" };
                            for (int i = 0; i < filtersName.Length; i++)
                            {
                                var itemSearch = new IndesignFilters
                                {

                                    FilterName = Convert.ToString(filtersName[i]),
                                    FilterId = i + 6,
                                };
                                Filters.Add(itemSearch);
                            }

                        }
                        else if (attribute_Datatype.Contains("Date and Time"))
                        {
                            var filtersName = new string[] { "Equal to", "Does Not Equal to", "Greater Than", "Greater Than Or Equal to", "Less Than", "Less Than Or Equal to", "Empty", "Exists" };
                            for (int i = 0; i < filtersName.Length; i++)
                            {
                                var itemSearch = new IndesignFilters
                                {

                                    FilterName = Convert.ToString(filtersName[i]),
                                    FilterId = i + 6,
                                };
                                Filters.Add(itemSearch);
                            }

                        }
                        else
                        {
                            var filtersName = new string[] { "Contains", "Exact", "Starts with", "Ends with", "Empty", "Exists" };
                            for (int i = 0; i < filtersName.Length; i++)
                            {
                                var itemSearch = new IndesignFilters
                                {

                                    FilterName = Convert.ToString(filtersName[i]),
                                    FilterId = i,
                                };
                                Filters.Add(itemSearch);
                            }

                        }

                        return Filters;
                    }
                    else
                    {
                        var filtersName = new string[] { "Contains", "Exact", "Starts with", "Ends with", "Empty", "Exists" };
                        for (int i = 0; i < filtersName.Length; i++)
                        {
                            var itemSearch = new IndesignFilters
                            {

                                FilterName = Convert.ToString(filtersName[i]),
                                FilterId = i,
                            };
                            Filters.Add(itemSearch);
                        }
                        return Filters;
                    }

                }
                else
                {
                    var filtersName = new string[] { "Contains", "Exact", "Starts with", "Ends with", "Empty", "Exists" };
                    for (int i = 0; i < filtersName.Length; i++)
                    {
                        var itemSearch = new IndesignFilters
                        {

                            FilterName = Convert.ToString(filtersName[i]),
                            FilterId = i,
                        };
                        Filters.Add(itemSearch);
                    }
                    return Filters;
                }
            }

            catch (Exception objException)
            {
                Logger.Error("Error at InDesignApiController :  GetIndesignAllFiltersValue", objException);
                return null;
            }

        }

        public IList getProductAttributeType()
        {
            try
            {
                var Filters = new List<productAttributeTypename>();
                var productAttributeTypename = new string[] { "Product Specfications", "Product Price", "Product Key" };
                for (int i = 0; i < productAttributeTypename.Length; i++)
                {
                    var itemSearch = new productAttributeTypename
                    {
                        productAttributeTypeFilterName = Convert.ToString(productAttributeTypename[i]),
                    };
                    Filters.Add(itemSearch);
                }
                return Filters;
            }

            catch (Exception ex)
            {
                Logger.Error("Error at HomeApiController :  attributeName", ex);
                return null;
            }
        }


        [HttpPost]
        public IList getIndesignProductAttributeNames(int productAttributeType)
        {
            try
            {
                var productAttributeFiltername = new List<productAttributeFiltername>();

                var productAttributeTypeName = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_TYPE == productAttributeType && x.FLAG_RECYCLE == "A" && x.ATTRIBUTE_ID != 1).Select(Y => Y.ATTRIBUTE_NAME).ToList();

                if (productAttributeType == 1)
                {
                    int customerId = Convert.ToInt32(_dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(a => a.CustomerId).FirstOrDefault());
                    var CustomizableItem = _dbcontext.Customers.Where(x => x.CustomerId == customerId).Select(x => x.CustomizeItemNo).FirstOrDefault();

                    var itemHash = new productAttributeFiltername
                    {
                        productAttributeProductFilterName = Convert.ToString(CustomizableItem),

                    };
                    productAttributeFiltername.Add(itemHash);


                    for (int i = 0; i < productAttributeTypeName.Count; i++)
                    {

                        var itemSearch = new productAttributeFiltername
                        {
                            productAttributeProductFilterName = Convert.ToString(productAttributeTypeName[i]),

                        };
                        productAttributeFiltername.Add(itemSearch);
                    }

                }

                else if (productAttributeType == 4)
                {

                    for (int i = 0; i < productAttributeTypeName.Count; i++)
                    {
                        var itemSearch = new productAttributeFiltername
                        {
                            productAttributeProductFilterName = Convert.ToString(productAttributeTypeName[i]),
                        };

                        productAttributeFiltername.Add(itemSearch);
                    }


                }

                else if (productAttributeType == 6)
                {

                    for (int i = 0; i < productAttributeTypeName.Count; i++)
                    {
                        var itemSearch = new productAttributeFiltername
                        {
                            productAttributeProductFilterName = Convert.ToString(productAttributeTypeName[i]),
                        };

                        productAttributeFiltername.Add(itemSearch);
                    }

                }


                return productAttributeFiltername;


            }

            catch (Exception ex)
            {
                Logger.Error("Error at HomeApiController :  attributeName", ex);
                return null;
            }
        }

        #endregion


        public class XMLGeneration
        {
            private int mCatalogId;
            private int mRecordID;
            private string mCategoryId;
            private string mFamilyList;
            private string mUserImagePath;
            private string mPTConv;
            private string mOSType;
            private string mPageName;
            private string mTempPath;
            private string mLayoutPath;
            private string mIndesign;
            private string mCategoryXML;
            private string mcategorySort;

            public int CatalogId
            {
                get { return mCatalogId; }
                set { mCatalogId = value; }
            }



            public int FileNO
            {
                get { return mfileNO; }
                set { mfileNO = value; }
            }

            public int ProjectID
            {
                get { return mProjectID; }
                set { mProjectID = value; }
            }

            public int RecordID
            {
                get { return mRecordID; }
                set { mRecordID = value; }
            }

            public string CategoryId
            {
                get { return mCategoryId; }
                set { mCategoryId = value; }
            }
            public string CATEGORY_SHORT
            {
                get { return mcategorySort; }
                set { mcategorySort = value; }
            }

            public string FamilyList
            {
                get { return mFamilyList; }
                set { mFamilyList = value; }
            }

            public string UserImagePath
            {
                get { return mUserImagePath; }
                set { mUserImagePath = value; }
            }

            public string PTConv
            {
                get { return mPTConv; }
                set { mPTConv = value; }
            }

            public string OSType
            {
                get { return mOSType; }
                set { mOSType = value; }
            }

            public string PageName
            {
                get { return mPageName; }
                set { mPageName = value; }
            }

            public string TempPath
            {
                get { return mTempPath; }
                set { mTempPath = value; }
            }

            public string LayoutPath
            {
                get { return mLayoutPath; }
                set { mLayoutPath = value; }
            }

            public string Indesign
            {
                get { return mIndesign; }
                set { mIndesign = value; }
            }

            public string CategoryXML
            {
                get { return mCategoryXML; }
                set { mCategoryXML = value; }
            }

        }



        #region Indesign Export

        [System.Web.Http.HttpGet]
        public string InDesigExport(int catalogId, bool displayIdColumns, string exportFormatType, int projectId)
        {
            exportFormatType = exportFormatType.ToLower();

            try
            {
                DataTable dt = new DataTable();
                Guid objGuid = Guid.NewGuid();
                int index = 0;
                string indexOfSubCat = string.Empty;
                string subcategorySplit = string.Empty;
                string indesign_STPName = string.Empty;
                int count = 0;
                string sessionid = Regex.Replace(objGuid.ToString(), "[^a-zA-Z0-9_]+", "");
                var objDataSet = new DataSet();
                if (exportFormatType == "true")
                {
                    indesign_STPName = "STP_CATALOGSTUDIO5_BACKUPINDESIGNEXPORT";
                }
                else
                {
                    indesign_STPName = "STP_CATALOGSTUDIO5_INDESIGNEXPORT";
                }
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = indesign_STPName;
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 10).Value = catalogId;
                    objSqlCommand.Parameters.Add("@SESSID", SqlDbType.NVarChar, 500).Value = sessionid;
                    objSqlCommand.Parameters.Add("@ATTRIBUTE_IDS", SqlDbType.NVarChar).Value = null;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = "All";
                    objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.NVarChar).Value = projectId;
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDataSet);

                    dt = objDataSet.Tables[0];
                    if (objDataSet.Tables.Count > 0)
                    {

                        index = objDataSet.Tables[0].Columns["FAMILY_ID"].Ordinal;
                        indexOfSubCat = objDataSet.Tables[0].Columns[index - 1].ToString();
                        if (indexOfSubCat.Contains("SUBCAT"))
                        {
                            subcategorySplit = indexOfSubCat.Split('_')[1];
                            count = Convert.ToInt32(subcategorySplit[1].ToString());
                        }
                        if (count > 0)
                        {
                            for (int i = 1; i <= count; i++)
                            {

                                if (dt.AsEnumerable().All(dr => dr.IsNull("SUBCATID_L" + i)))
                                    dt.Columns.Remove("SUBCATID_L" + i);
                                if (dt.AsEnumerable().All(dr => dr.IsNull("SUBCATNAME_L" + i)))
                                    dt.Columns.Remove("SUBCATNAME_L" + i);
                            }
                        }
                    }
                }

                objDataSet.Tables[0].Columns.Add("ACTION").SetOrdinal(0);
                ExportInDesignDataToExcel(objDataSet, displayIdColumns);


                return "InDesignExport" + exportFormatType;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at InDesignApiController : InDesigExport", objexception);
                return "InDesignExport" + exportFormatType;
            }
        }


        public void ExportInDesignDataToExcel(DataSet indesign, bool displayIdColumns)
        {
            try
            {
                DataTable dt = new DataTable();
                int index = 0; string indexOfSubCat = string.Empty;
                string subcategorySplit = string.Empty;
                int count = 0;
                dt = indesign.Tables[0];
                if (indesign.Tables.Count > 0)
                {
                    //if (!displayIdColumns)
                    //{
                    //    index = indesign.Tables[0].Columns["FAMILY_ID"].Ordinal;
                    //    indexOfSubCat = indesign.Tables[0].Columns[index - 1].ToString();
                    //    if (indexOfSubCat.Contains("SUBCAT"))
                    //    {
                    //        subcategorySplit = indexOfSubCat.Split('_')[1];
                    //        count = Convert.ToInt32(subcategorySplit[1].ToString());
                    //    }
                    //    if (count > 0)
                    //    {
                    //        for (int i = 1; i <= count; i++)
                    //        {
                    //            if (indesign.Tables[0].Columns.Contains("SUBCATID_L" + i))
                    //            {
                    //                indesign.Tables[0].Columns.Remove("SUBCATID_L" + i);
                    //            }
                    //        }
                    //    }
                    //}
                }
                if (indesign.Tables.Count > 0)
                {
                    //if (!displayIdColumns)
                    //{
                    //    if (indesign.Tables[0].Columns.Contains("FAMILY_ID"))
                    //    {
                    //        indesign.Tables[0].Columns.Remove("FAMILY_ID");
                    //    }
                    //    if (indesign.Tables[0].Columns.Contains("CATALOG_ID"))
                    //    {
                    //        indesign.Tables[0].Columns.Remove("CATALOG_ID");
                    //    }
                    //    if (indesign.Tables[0].Columns.Contains("CATEGORY_ID"))
                    //    {
                    //        indesign.Tables[0].Columns.Remove("CATEGORY_ID");
                    //    }
                    //    if (indesign.Tables[0].Columns.Contains("DATA_FILE_ID"))
                    //    {
                    //        indesign.Tables[0].Columns.Remove("DATA_FILE_ID");
                    //    }
                    //    if (indesign.Tables[0].Columns.Contains("PROJECT_ID"))
                    //    {
                    //        indesign.Tables[0].Columns.Remove("PROJECT_ID");
                    //    }
                    //}
                }

                System.Web.HttpContext.Current.Session["ExportTableInDesign"] = null;
                System.Web.HttpContext.Current.Session["ExportTableInDesign"] = indesign.Tables[0];
            }
            catch (Exception objexception)
            {

            }
        }




        #endregion


    }
}