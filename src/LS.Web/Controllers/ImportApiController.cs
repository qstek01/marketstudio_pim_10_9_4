﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using Infragistics.Documents.Excel;
using LS.Data;
using LS.Data.Model.CatalogSectionModels;
using log4net;
using LS.Data.Model;
using System.Web.Http;
using System.Collections;
using System.Net.Http;
using System.Net;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using System.Data.OleDb;
using System.Web.Mvc;
using System.Globalization;
using System.Xml;
using System.Xml.Linq;
using LS.Web.Models;
using System.Diagnostics;
using NinjaNye.SearchExtensions;
//using ClosedXML.Excel;

namespace LS.Web.Controllers
{
    public class ImportApiController : ApiController
    {
        #region variables
        private DataTable dtable;
        private DataTable objdatatable;
        DataSet _dsLogs;
        private DataTable basicprodImportData = new DataTable();
        string _ParentCategoryId = string.Empty;
        private static ILog _logger = LogManager.GetLogger(typeof(ImportApiController));
        private CSEntities objLS = new CSEntities();
        private DataSet _dsSheets = new DataSet();
        private string Message;
        private string _parentCategoryId = "0";
        private string _currentCategoryId = "";
        string _CatalogName = "";
        int _ProductSort = 0;
        int number = 0;
        private String _sExcelFileName = "";
        public static string _newcatitemno = string.Empty;
        AdvanceImportApiController adImport = new AdvanceImportApiController();
        public static DataTable sortfam = new DataTable();
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        //static ILog _logger = LogManager.GetLogger(typeof(FamilyApiController));
        private ImportController IC = new ImportController();
        private DataTable dtAttibuteTable = new DataTable();
        Actions _actions = Actions.None;
        DataTable _settingsDt;
        int _maxProcessOrder;
        private readonly CSEntities _dbcontext = new CSEntities();
        DataTable ExcelColumn2CatalogMapping = new DataTable();
        private bool _categoryLevelCheckPassed;
        #endregion

        #region enum variable CSF
        // GET: ImportApi
        public enum CSF
        {
            [Description("CATALOG_ID")]
            CATALOG_ID = 1,
            [Description("CATALOG_NAME")]
            CATALOG_NAME = 2,
            [Description("CATALOG_VERSION")]
            CATALOG_VERSION = 3,
            [Description("CATALOG_DESCRIPTION")]
            CATALOG_DESCRIPTION = 4,
            [Description("CATEGORY_ID")]
            CATEGORY_ID = 101,
            [Description("CATEGORY_NAME")]
            CATEGORY_NAME = 102,
            [Description("SUBCATID_L1")]
            SUBCATID_L1 = 103,
            [Description("SUBCATNAME_L1")]
            SUBCATNAME_L1 = 104,
            [Description("SUBCATID_L2")]
            SUBCATID_L2 = 105,
            [Description("SUBCATNAME_L2")]
            SUBCATNAME_L2 = 106,
            [Description("SUBCATID_L3")]
            SUBCATID_L3 = 107,
            [Description("SUBCATNAME_L3")]
            SUBCATNAME_L3 = 108,
            [Description("SUBCATID_L4")]
            SUBCATID_L4 = 109,
            [Description("SUBCATNAME_L4")]
            SUBCATNAME_L4 = 110,
            [Description("SUBCATID_L5")]
            SUBCATID_L5 = 111,
            [Description("SUBCATNAME_L5")]
            SUBCATNAME_L5 = 112,
            [Description("SUBCATID_L6")]
            SUBCATID_L6 = 113,
            [Description("SUBCATNAME_L6")]
            SUBCATNAME_L6 = 114,
            [Description("SUBCATID_L7")]
            SUBCATID_L7 = 115,
            [Description("SUBCATNAME_L7")]
            SUBCATNAME_L7 = 116,
            [Description("SUBCATID_L8")]
            SUBCATID_L8 = 117,
            [Description("SUBCATNAME_L8")]
            SUBCATNAME_L8 = 118,
            [Description("SUBCATID_L9")]
            SUBCATID_L9 = 119,
            [Description("SUBCATNAME_L9")]
            SUBCATNAME_L9 = 120,
            [Description("CATEGORY_SHORT_DESC")]
            CATEGORY_SHORT_DESC = 151,
            [Description("CATEGORY_IMAGE_FILE")]
            CATEGORY_IMAGE_FILE = 152,
            [Description("CATEGORY_IMAGE_NAME")]
            CATEGORY_IMAGE_NAME = 153,
            [Description("CATEGORY_IMAGE_TYPE")]
            CATEGORY_IMAGE_TYPE = 154,
            [Description("CATEGORY_IMAGE_FILE2")]
            CATEGORY_IMAGE_FILE2 = 155,
            [Description("CATEGORY_IMAGE_NAME2")]
            CATEGORY_IMAGE_NAME2 = 156,
            [Description("CATEGORY_IMAGE_TYPE2")]
            CATEGORY_IMAGE_TYPE2 = 157,
            [Description("CATEGORY_CUSTOM_NUM_FIELD1")]
            CATEGORY_CUSTOM_NUM_FIELD1 = 158,
            [Description("CATEGORY_CUSTOM_NUM_FIELD2")]
            CATEGORY_CUSTOM_NUM_FIELD2 = 159,
            [Description("CATEGORY_CUSTOM_NUM_FIELD3")]
            CATEGORY_CUSTOM_NUM_FIELD3 = 160,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD1")]
            CATEGORY_CUSTOM_TEXT_FIELD1 = 161,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD2")]
            CATEGORY_CUSTOM_TEXT_FIELD2 = 162,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD3")]
            CATEGORY_CUSTOM_TEXT_FIELD3 = 163,
            [Description("FAMILY_ID")]
            FAMILY_ID = 201,
            [Description("FAMILY_NAME")]
            FAMILY_NAME = 202,
            [Description("SUBFAMILY_ID")]
            SUBFAMILY_ID = 203,
            [Description("SUBFAMILY_NAME")]
            SUBFAMILY_NAME = 204,
            [Description("CATEGORY_ID")]
            FAMCATEGORY_ID = 205,
            [Description("FOOT_NOTES")]
            FOOT_NOTES = 206,
            [Description("STATUS")]
            STATUS = 207,
            [Description("FAMILY_SORT")]
            FAMILY_SORT = 208,
            [Description("PRODUCT_ID")]
            PRODUCT_ID = 301,
            [Description("CATALOG_ITEM_NO")] //[Description("CAT#")]
            CATALOG_ITEM_NO = 302,
            [Description("PRODUCT_SORT")]
            PRODUCT_SORT = 303,
            [Description("REMOVE_PRODUCT")]
            REMOVE_PRODUCT = 304,
            [Description("ATTRIBUTE_ID")]
            ATTRIBUTE_ID = 401
        };
        #endregion

        #region enum variable Actions
        public enum Actions
        {
            None = 0,
        }
        #endregion

        #region Properties
        public int TotalRow { get; set; }

        public int MaxProcessOrder
        {
            get
            {
                return _maxProcessOrder;
            }
        }

        public DataTable ColumnsToImportDt
        {
            set
            {
                _settingsDt = value;
            }
        }

        public Actions Action
        {
            get
            {
                return _actions;
            }
            set
            {
                _actions = value;
            }
        }

        public bool CategoryLevelCheckPassed
        {
            get
            {
                return _categoryLevelCheckPassed;
            }
        }
        #endregion

        [System.Web.Http.HttpGet]
        public IList importExcelSheetSelection(string excelPath)
        {
            List<ImportSheetName> newList = new List<ImportSheetName>();
            //  excelPath = "C:\\Users\\Rakesh\\Downloads\\LS Export.xls";
            //var myList = new List<ImportSheetName>();
            try
            {
                if (!string.IsNullOrEmpty(excelPath))
                {
                    if (excelPath.Contains("/Content/ImportTemplate/"))
                    {
                        string importFileName = Path.GetFileName(excelPath);
                        excelPath = System.Web.Hosting.HostingEnvironment.MapPath(excelPath);
                    }
                    // bool hasHeaders = true;
                    //  string HDR = hasHeaders ? "Yes" : "No";
                    // string strConn;
                    //if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                    //    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                    //              ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    //else
                    //    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                    //              ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";

                    Infragistics.Documents.Excel.Workbook workbook = Infragistics.Documents.Excel.Workbook.Load(excelPath);
                    // OleDbConnection conn = new OleDbConnection(strConn);
                    // conn.Open();
                    //  DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,null);
                    //int i = 0;
                    // string SheetName = string.Empty;
                    foreach (Worksheet rows in workbook.Worksheets)
                    {

                        //SheetName = Convert.ToString(rows[2]);
                        // if (!SheetName.EndsWith("_"))
                        // {
                        // if (SheetName.Contains("$"))
                        // {
                        //     SheetName = SheetName.Replace("$", "");
                        // }
                        // if (SheetName.Contains("'"))
                        //  {
                        //      SheetName = SheetName.Replace("'", "");
                        //  }




                        ImportSheetName objImportSheetName = new ImportSheetName();
                        if (rows.Rows.Count() > 0)
                        {
                            objImportSheetName.TABLE_NAME = rows.Name;
                            objImportSheetName.SHEET_PATH = excelPath;



                            DataTable excelData = ConvertExcelToDataTableWithSkipFirstRow(excelPath, rows.Name);

                            DataTable excelDatawithoutheader = ConvertExcelToDataTableWithoutHeader(excelPath, rows.Name);


                            var categoryIndex = excelDatawithoutheader.Rows[0].ItemArray
                   .Select((c, i) => new { c, i })
                   .Where(x => x.c.ToString().Equals("CATEGORY_NAME"))
                   .Select(x => x.i)
                   .ToArray();

                            if (categoryIndex.Count() > 0 && excelData.Rows.Count > 0 && excelDatawithoutheader.Rows.Count > 1)
                            {

                                string categoryValues = Convert.ToString(excelDatawithoutheader.Rows[1][categoryIndex[0]]).Trim();

                                if (categoryValues == "")
                                {
                                    excelData.Rows.Clear();
                                }

                            }





                            var familyIndex = excelDatawithoutheader.Rows[0].ItemArray
                       .Select((c, i) => new { c, i })
                       .Where(x => x.c.ToString().Equals("FAMILY_NAME"))
                       .Select(x => x.i)
                       .ToArray();

                            if (familyIndex.Count() > 0 && excelData.Rows.Count > 0 && excelDatawithoutheader.Rows.Count > 1)
                            {
                                string familyValues = excelDatawithoutheader.Rows[1][familyIndex[0]].ToString().Trim();

                                if (familyValues == "")
                                {
                                    excelData.Rows.Clear();
                                }

                            }



                            //int customerId = _dbcontext.Customers.Where(x => x.EMail == User.Identity.Name).Select(x => x.CustomerId).FirstOrDefault();
                            int customerId = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(x => x.CustomerId).FirstOrDefault();

                            string displayItemnumber = objLS.Customers.Where(x => x.CustomerId == customerId).Select(y => y.CustomizeItemNo).FirstOrDefault().ToString();


                            var displayItemnumberIndex = excelDatawithoutheader.Rows[0].ItemArray
                              .Select((c, i) => new { c, i })
                              .Where(x => x.c.ToString().Equals(displayItemnumber))
                              .Select(x => x.i)
                              .ToArray();

                            var itemNumberIndex = excelDatawithoutheader.Rows[0].ItemArray
                             .Select((c, i) => new { c, i })
                             .Where(x => x.c.ToString().Equals("ITEM#"))
                             .Select(x => x.i)
                             .ToArray();



                            if (displayItemnumberIndex.Count() > 0 && excelData.Rows.Count > 0 && excelDatawithoutheader.Rows.Count > 1)
                            {
                                string productValues = excelDatawithoutheader.Rows[1][displayItemnumberIndex[0]].ToString().Trim();

                                if (productValues == "")
                                {
                                    excelData.Rows.Clear();
                                }
                            }

                            else if (itemNumberIndex.Count() > 0 && excelData.Rows.Count > 0 && excelDatawithoutheader.Rows.Count > 1)
                            {
                                string productValues = excelDatawithoutheader.Rows[1][itemNumberIndex[0]].ToString().Trim();

                                if (productValues == "")
                                {
                                    excelData.Rows.Clear();
                                }
                            }


                            var tabledesignerIndex = excelDatawithoutheader.Rows[0].ItemArray
                   .Select((c, i) => new { c, i })
                   .Where(x => x.c.ToString().Equals("FAMILY_TABLE_STRUCTURE"))
                   .Select(x => x.i)
                   .ToArray();

                            if (tabledesignerIndex.Count() > 0 && excelData.Rows.Count > 0 && excelDatawithoutheader.Rows.Count > 1)
                            {
                                string tabledesignerValues = excelDatawithoutheader.Rows[1][tabledesignerIndex[0]].ToString().Trim();

                                if (tabledesignerValues == "")
                                {
                                    excelData.Rows.Clear();
                                }

                            }



                            if (excelData.Rows.Count > 0)
                            {
                                objImportSheetName.DATA_EXISTS = true;
                            }
                            else
                            {
                                objImportSheetName.DATA_EXISTS = false;
                            }
                            newList.Add(objImportSheetName);
                        }
                        //sheetnames[i] = schemaTable.Rows[i];
                        //  i++;
                    }
                }
                // conn.Close();


            }

            catch (Exception objException)
            {
                _logger.Error("Error at loading importExcelSheetSelection in Import Controller", objException);
                return null;
            }
            return newList;
        }

        [System.Web.Http.HttpGet]
        public DataTable importSelectionColumnGrid(string SheetName, string ExcelPath)
        {
            var dt = new DataTable();
            // DataTable dtexcel = new DataTable();
            // List<object> newList = new List<object>();
            try
            {
                if (!string.IsNullOrEmpty(ExcelPath))
                {
                    Workbook workbook = Workbook.Load(ExcelPath);
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    string strConn;
                    if (ExcelPath.Substring(ExcelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    else
                        //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + ExcelPath +
                        //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + ExcelPath +
                                 ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    // DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    // int sheetCount = schemaTable.Rows.Count;
                    //  int sheetColumnCount = 0;

                    if (!SheetName.Contains("$"))
                    {
                        SheetName = SheetName + "$";
                    }
                    dt = new DataTable(SheetName);
                    var dc = new DataColumn("ExcelColumn", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("CatalogField", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("SelectedToImport", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("IsSystemField", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("FieldType", typeof(string));
                    dt.Columns.Add(dc);
                    //DataRow schemaRow = schemaTable.Rows[0];
                    //string sheet = schemaRow["TABLE_NAME"].ToString();
                    //if (!sheet.EndsWith("_"))
                    //{
                    //    string query = "SELECT  * FROM [" + SheetName + "]";
                    //    OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                    //    dtexcel.Locale = CultureInfo.CurrentCulture;
                    //    daexcel.Fill(dtexcel);
                    //}
                    //sheetColumnCount = dtexcel.Columns.Count;

                    //int iCols = sheetColumnCount;
                    string sheetNameworkbook = SheetName.Replace("$", "").TrimEnd();

                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[0].Cells.Count();
                    for (int j = 0; j < iCols; j++)
                    {
                        if (workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[j].Value != null)
                        {
                            String sColName =
                                workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[j].Value.ToString()
                                    .Replace('[', ' ')
                                    .Replace(']', ' ')
                                    .Trim();

                            DataRow dr = dt.NewRow();
                            dr[0] = sColName;
                            dt.Rows.Add(dr);
                        }
                    }


                    _dsSheets.Tables.Add(dt);
                    dt.Dispose();
                    dtable = GetExcelColumn2CatalogMapping(SheetName);
                    conn.Close();
                    conn.Dispose();
                }
                return dtable;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at ImportApiController :importSelectionColumnGrid", objException);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public JsonResult GetImportSpecs(string SheetName, string ExcelPath)
        {
            // ExcelPath = "C:\\Users\\Rakesh\\Downloads\\LS Export.xls";
            DataTable dtable = importSelectionColumnGrid(SheetName, ExcelPath);
            try
            {
                var dynamicColumns = new Dictionary<string, string>();
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                JsonWriter jsonWriter = new JsonTextWriter(sw);
                var model = new DashBoardModel();
                using (DataTableReader reader = dtable.CreateDataReader())
                {
                    jsonWriter.WriteStartObject();
                    jsonWriter.WritePropertyName("Data");
                    jsonWriter.WriteStartArray();
                    while (reader.Read())
                    {
                        jsonWriter.WriteStartObject();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            jsonWriter.WritePropertyName(reader.GetName(i));
                            jsonWriter.WriteValue(reader.GetValue(i).ToString());
                            if (!dynamicColumns.ContainsKey(reader.GetName(i)) &&
                                !string.IsNullOrEmpty(reader.GetName(i)))
                            {
                                dynamicColumns.Add(reader.GetName(i), reader.GetValue(i).ToString());
                            }
                        }
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WritePropertyName("Columns");
                    jsonWriter.WriteStartArray();
                    foreach (string key in dynamicColumns.Keys)
                    {
                        jsonWriter.WriteStartObject();
                        jsonWriter.WritePropertyName("title");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WritePropertyName("field");
                        jsonWriter.WriteValue(key);
                        jsonWriter.WriteEndObject();
                    }
                    jsonWriter.WriteEndArray();
                    jsonWriter.WriteEndObject();
                }
                model.Data = sb.ToString();
                return new JsonResult() { Data = model };

            }
            catch (Exception objexception)
            {
                _logger.Error("Error at ImportApiController : GetProdSpecs", objexception);
                return null;
            }
        }

        public string DataTableToJsonObj(DataTable dt)
        {
            try
            {
                DataSet ds = new DataSet();
                ds.Merge(dt);
                StringBuilder JsonString = new StringBuilder();
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    JsonString.Append("[");
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        JsonString.Append("{");
                        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                        {
                            if (j < ds.Tables[0].Columns.Count - 1)
                            {
                                JsonString.Append("\"" + ds.Tables[0].Columns[j].ColumnName.ToString() + "\":" + "\"" +
                                                  ds.Tables[0].Rows[i][j].ToString() + "\",");
                            }
                            else if (j == ds.Tables[0].Columns.Count - 1)
                            {
                                JsonString.Append("\"" + ds.Tables[0].Columns[j].ColumnName.ToString() + "\":" + "\"" +
                                                  ds.Tables[0].Rows[i][j].ToString() + "\"");
                            }
                        }
                        if (i == ds.Tables[0].Rows.Count - 1)
                        {
                            JsonString.Append("}");
                        }
                        else
                        {
                            JsonString.Append("},");
                        }
                    }
                    JsonString.Append("]");
                    return JsonString.ToString();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading DataTableToJsonObj in Import Controller", objException);
                return null;
            }
        }

        public static Boolean IsCatalogStudioField(String enumFieldName)
        {
            return Enum.GetNames(typeof(CSF)).Any(val => enumFieldName == val);
        }

        public DataTable GetExcelColumn2CatalogMapping(String xlsSheetName)
        {
            try
            {
                DataTable dt = _dsSheets.Tables[xlsSheetName];
                int i = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    string sCheckAttributeName = dt.Rows[i][0].ToString().ToUpper();
                    //Check if this is a CatalogStudio system defined field name
                    Boolean vaild = IsCatalogStudioField(sCheckAttributeName);

                    if (vaild) //CatalogStudio Field
                    {
                        dr["CatalogField"] = sCheckAttributeName;
                        dr["IsSystemField"] = true;
                        dr["FieldType"] = 0;
                        dr["SelectedToImport"] = true;
                    }
                    else //User defined attribute name
                    {

                        string sAttributeNameInCatalog;
                        var customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                        if (customerid != null)
                        {
                            var drAttr =
                                objLS.TB_ATTRIBUTE.Join(objLS.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                                    tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                    .Where(
                                        x =>
                                            x.tcp.ATTRIBUTE_NAME == sCheckAttributeName &&
                                            x.tpf.CUSTOMER_ID == customerid.CustomerId).Select(x => x.tcp).ToList();


                            if (drAttr.Count > 0)
                            {
                                sAttributeNameInCatalog = drAttr[0].ATTRIBUTE_NAME;
                                string sAttributeDatatype = drAttr[0].ATTRIBUTE_DATATYPE;
                                string sAttrType = drAttr[0].ATTRIBUTE_TYPE.ToString(CultureInfo.InvariantCulture);
                                if (sAttributeDatatype == "Date and Time")
                                {
                                    if (sAttrType == "6" || sAttrType == "1" || sAttrType == "3")
                                    {
                                        dr["FieldType"] = 10;
                                        dr["SelectedToImport"] = true;
                                        dr["IsSystemField"] = true;
                                    }
                                    else if (sAttrType == "7" || sAttrType == "11" || sAttrType == "13")
                                    {
                                        dr["FieldType"] = 14;
                                        dr["SelectedToImport"] = true;
                                        dr["IsSystemField"] = true;
                                    }
                                }
                                else
                                {
                                    dr["FieldType"] = drAttr[0].ATTRIBUTE_TYPE.ToString();
                                    dr["SelectedToImport"] = true;
                                    dr["IsSystemField"] = true;
                                }
                                dr["CatalogField"] = sAttributeNameInCatalog;
                                //dr["IsSystemField"] = false;

                            }
                            else
                            {
                                sAttributeNameInCatalog = "";
                                dr["FieldType"] = "";
                                dr["SelectedToImport"] = false;
                            }
                        }
                    }
                    i++;
                }

                return dt;

            }
            catch (Exception objException)
            {
                _logger.Error("Error at loading GetExcelColumn2CatalogMapping in Import Controller", objException);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public string finishImport(string SheetName, string allowDuplicate, string excelPath, JArray model)
        {
            string ccoc;
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            DataSet ds = new DataSet();
            DataSet dsGetProdCnt = new DataSet();
            int importProductCount = 0;
            int userProductCount = 0;
            int userSKUProductCount = 0;
            try
            {
                DataTable oattType = new DataTable();
                DataSet replace = new DataSet();

                int ItemVal = Convert.ToInt32(allowDuplicate);
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SQLString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    IC.importExcelSheetSelection(excelPath, SheetName);
                    SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                    _DBCommand.ExecuteNonQuery();
                    objdatatable = IC.exceldata(excelPath, SheetName);

                    SQLString = IC.CreateTable("[##IMPORTTEMP" + importTemp + "]", excelPath, SheetName, objdatatable);
                    SqlCommand _DBCommandnew = new SqlCommand(SQLString, conn);
                    _DBCommandnew.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(conn)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(objdatatable);

                    if (allowDuplicate == "1")
                    {
                        IC._SQLString = @"SELECT COUNT (DISTINCT Catalog_item_no) FROM  [##IMPORTTEMP" + importTemp +
                                        "] WHERE  isnull(Catalog_item_no,'') <> '' ";
                    }
                    else
                    {
                        IC._SQLString = @"SELECT COUNT (DISTINCT Catalog_item_no) FROM  [##IMPORTTEMP" + importTemp +
                                        "] WHERE PRODUCT_ID IS NULL  and isnull(Catalog_item_no,'') <> '' ";
                    }
                    dsGetProdCnt = IC.CreateDataSet();
                    var customerId = 0;
                    var skucnt = objLS.TB_PLAN
                       .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                       .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                       .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                    var SKUcount = skucnt.Select(a => a).ToList();

                    if (SKUcount.Any())
                    {
                        userSKUProductCount = Convert.ToInt32(SKUcount[0].SKU_COUNT);
                        customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                    }

                    if (Convert.ToInt32(dsGetProdCnt.Tables[0].Rows[0][0]) > 0)
                    {
                        importProductCount = Convert.ToInt32(dsGetProdCnt.Tables[0].Rows[0][0]);
                        var productcount = objLS.STP_LS_GET_PRODUCTS_BY_USER("PRODUCT", User.Identity.Name, "").ToList();

                        if (productcount.Any())
                        {
                            userProductCount = Convert.ToInt32(productcount[0]);
                        }

                        if ((userProductCount + importProductCount) > userSKUProductCount)
                        {

                            return "SKU Exceed~" + importTemp;
                        }
                    }
                    oattType = IC.SelectedColumnsToImport(objdatatable, model);
                    var cmd1 =
                        new SqlCommand(
                            "ALTER TABLE [##IMPORTTEMP" + importTemp +
                            "] ADD Family_Status nvarchar(10),FStatusUI nvarchar(10),StatusUI nvarchar(10),SFSort_Order int,Sort_Order int",
                            conn);
                    cmd1.ExecuteNonQuery();
                    var sqlstring1 =
                        new SqlCommand(
                            "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##AttributeTemp" +
                            importTemp + "]'')BEGIN DROP TABLE [##AttributeTemp" + importTemp + "] END')", conn);
                    sqlstring1.ExecuteNonQuery();
                    var cmd2 = new SqlCommand();
                    cmd2.Connection = conn;
                    SQLString = IC.CreateTableToImport("[##AttributeTemp" + importTemp + "]", oattType);
                    cmd2.CommandText = SQLString;
                    cmd2.CommandType = CommandType.Text;
                    cmd2.ExecuteNonQuery();
                    bulkCopy.DestinationTableName = "[##AttributeTemp" + importTemp + "]";
                    bulkCopy.WriteToServer(oattType);
                    var cmbpk10 = new SqlCommand("update [##AttributeTemp" + importTemp + "] set ATTRIBUTE_TYPE = 1 where ATTRIBUTE_TYPE = 10", conn);
                    cmbpk10.ExecuteNonQuery();
                    var cmbpk1 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1'') IS NOT NULL  DROP TABLE  ##t1') ", conn);
                    cmbpk1.ExecuteNonQuery();
                    var cmbpk2 = new SqlCommand("select * into ##t1  from [##AttributeTemp" + importTemp + "] where attribute_type <>0", conn);
                    cmbpk2.ExecuteNonQuery();
                    var cmbpk3 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2'') IS NOT NULL  DROP TABLE  ##t2') ", conn);
                    cmbpk3.ExecuteNonQuery();
                    var cmbpk4 = new SqlCommand("select * into ##t2  from [##importtemp" + importTemp + "]", conn);
                    cmbpk4.ExecuteNonQuery();
                    //---------------------------------------FOR REPLACING EMPTY VALUES IN TEMP TABLE -----------VINOTHKUMAR ----------FEB-2017 ------------//
                    var cmd1f12 = new SqlCommand(
                              "SELECT * FROM [##importtemp" + importTemp + "]", conn);
                    var daf12 = new SqlDataAdapter(cmd1f12);
                    daf12.Fill(replace);
                    if (replace.Tables[0].Rows.Count > 0)
                    {
                        foreach (var item1 in replace.Tables[0].Columns)
                        {
                            string column_name2 = item1.ToString();
                            if (column_name2.Contains("'"))
                            {
                                column_name2 = column_name2.Replace("'", "''''");
                            }
                            var cmbpk41 = new SqlCommand(" EXEC ('STP_LS_IMPORT_REPLACE ''" + importTemp + "'',''" + column_name2 + "''')", conn);
                            cmbpk41.ExecuteNonQuery();
                        }
                    }


                    var Checkpicklistupdate = objLS.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerId);
                    if (Checkpicklistupdate != null && !Checkpicklistupdate.ShowCustomAPPS)
                    {
                        DataSet pkname = new DataSet();
                        SqlDataAdapter daa = new SqlDataAdapter("select PICKLIST_NAME,ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select attribute_name  from ##t1 where attribute_type <>0) and USE_PICKLIST =1", conn);
                        daa.Fill(pkname);
                        if (pkname.Tables[0].Rows.Count > 0)
                        {
                            DataTable Errortable = new DataTable();
                            Errortable.Columns.Add("STATUS");
                            Errortable.Columns.Add("ITEM_NO");
                            Errortable.Columns.Add("ATTRIBUTE_NAME");
                            Errortable.Columns.Add("PICKLIST_VALUE");

                            foreach (DataRow vr in pkname.Tables[0].Rows)
                            {
                                DataTable _pickListValue = new DataTable();
                                DataSet newsheetval = new DataSet();
                                _pickListValue.Columns.Add("Value");
                                string picklistname = vr["PICKLIST_NAME"].ToString();
                                DataSet dpicklistdata = new DataSet();
                                if (!string.IsNullOrEmpty(picklistname))
                                {
                                    var pickList = objLS.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                                    {
                                        PICKLIST_DATA = s.PICKLIST_DATA,
                                        PICKLIST_NAME = s.PICKLIST_NAME,
                                        PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                                    }).FirstOrDefault(d => d.PICKLIST_NAME == picklistname);
                                    var objPick = new List<PickList>();
                                    if (pickList != null && !string.IsNullOrEmpty(pickList.PICKLIST_DATA))
                                    {
                                        var pkxmlData = pickList.PICKLIST_DATA;

                                        if (pkxmlData.Contains("<?xml version"))
                                        {
                                            pkxmlData = pkxmlData.Replace("\\r\\n", "");
                                            pkxmlData = pkxmlData.Replace("\r\n", "");
                                            pkxmlData = pkxmlData.Replace("\\\"", "\"");
                                            pkxmlData = pkxmlData.Replace("<?xml version=\"1.0\" standalone=\"yes\"?>", "");
                                            XDocument objXml = XDocument.Parse(pkxmlData);

                                            objPick =
                                                objXml.Descendants("NewDataSet")
                                                    .Descendants("Table1")
                                                    .Select(d =>
                                                    {
                                                        var xElement = d.Element("ListItem");
                                                        return xElement != null
                                                            ? new PickList
                                                            {
                                                                ListItem = xElement.Value
                                                            }
                                                            : null;
                                                    }).ToList();
                                        }
                                    }
                                    SqlDataAdapter daa2 =
                                            new SqlDataAdapter("Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  ['+ @colName+'],CATALOG_ITEM_NO   from [##t2]') ",
                                                conn);
                                    daa2.Fill(newsheetval);
                                    SqlCommand cvb = new SqlCommand("DELETE TOP(1) From ##t1", conn);
                                    cvb.ExecuteNonQuery();
                                    DataSet picklistdataDS = new DataSet();
                                    DataTable newTable = new DataTable();
                                    // DataSet newsheetval1 = new DataSet();
                                    for (int rowCount = 0; rowCount < newsheetval.Tables[0].Rows.Count; rowCount++)
                                    {
                                        if (newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim() != "")
                                        {
                                            var picklistvalue = newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim();
                                            var dd = objPick.Where(x => x.ListItem == picklistvalue);
                                            if (!dd.Any())
                                            {

                                                // SqlDataAdapter daa3 =
                                                //  new SqlDataAdapter("Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  CATALOG_ITEM_NO   from [##t2] where  ['+ @colName+'] = ''" + picklistvalue + "''') ",
                                                //    conn);
                                                //       //  daa3.Fill(newsheetval1);
                                                string CAT = string.Empty;
                                                //if (newsheetval1.Tables[0].Rows.Count > 0)
                                                //{
                                                CAT = newsheetval.Tables[0].Rows[rowCount][1].ToString();
                                                // }

                                                Errortable.Rows.Add("Import failed due to invalid Pick List value.", CAT, vr["ATTRIBUTE_NAME"], picklistvalue);
                                                // return "Import failed due to invalid Pick List value\nAttribute Name : " + vr["ATTRIBUTE_NAME"] + "\nPicklist Value : " + picklistvalue + "\nCat # : " + CAT;
                                            }

                                        }
                                    }


                                }
                            }
                            if (Errortable.Rows.Count > 0)
                            {
                                string sqlString = IC.CreateTableToImport("[Picklistlog" + importTemp + "]", Errortable);
                                SqlCommand cmdPick = new SqlCommand();
                                cmdPick.Connection = conn;
                                cmdPick.CommandText = sqlString;
                                cmdPick.CommandType = CommandType.Text;
                                cmdPick.ExecuteNonQuery();
                                var bulkCopysp = new SqlBulkCopy(conn)
                                {
                                    DestinationTableName = "[Picklistlog" + importTemp + "]"
                                };
                                bulkCopysp.WriteToServer(Errortable);
                                var removetemp = new SqlCommand("IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'errorlog" + importTemp + "')Begin drop table [errorlog" + importTemp + "] End ", conn);
                                cmbpk1.ExecuteNonQuery();
                                IC._SQLString = @"select * into [errorlog" + importTemp + "] from [Picklistlog" + importTemp +
                           "]";
                                cmd1.CommandText = IC._SQLString;
                                cmd1.CommandType = CommandType.Text;
                                cmd1.ExecuteNonQuery();
                                return "Import failed due to invalid Pick List value~" + importTemp;
                            }
                        }
                    }
                    var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                    cmd.ExecuteNonQuery();
                    IC._SQLString = @"select * from [##LOGTEMPTABLE" + importTemp + "]";
                    ds = IC.CreateDataSet();
                    IC._SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp +
                                    "]";
                    cmd1.CommandText = IC._SQLString;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.ExecuteNonQuery();
                    var picklistvaluecreation = objLS.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerId);
                    //------------------Created by vinothkumar for picklist data addition of new values -------------------------------
                    if (picklistvaluecreation != null && picklistvaluecreation.ShowCustomAPPS)
                    {
                        try
                        {
                            DataSet checkPICK = new DataSet();
                            var cmd1f = new SqlCommand(
                                "SELECT * FROM [##LOGTEMPTABLE" + importTemp + "]", conn);
                            var daf = new SqlDataAdapter(cmd1f);
                            daf.Fill(checkPICK);
                            string check = string.Empty;
                            if (checkPICK.Tables[0].Rows.Count > 0)
                                foreach (DataColumn column1 in checkPICK.Tables[0].Columns)
                                {
                                    if (column1.ToString() == "STATUS")
                                    {
                                        check = checkPICK.Tables[0].Rows[0][column1.ToString()].ToString();//"Action completed";

                                    }
                                }
                            if (check == "Success" || check == "UPDATED")
                            {
                                // if (checkPICK.Tables[0].Rows[0][0].ToString() == "UPDATED")
                                // {

                                DataSet pkname = new DataSet();
                                SqlDataAdapter daa =
                                    new SqlDataAdapter(
                                        "select PICKLIST_NAME,ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select attribute_name  from ##t1 where attribute_type <>0) and USE_PICKLIST =1",
                                        conn);
                                daa.Fill(pkname);
                                if (pkname.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow vr in pkname.Tables[0].Rows)
                                    {
                                        DataTable _pickListValue = new DataTable();
                                        DataSet newsheetval = new DataSet();
                                        _pickListValue.Columns.Add("Value");
                                        string picklistname = vr["PICKLIST_NAME"].ToString();
                                        DataSet dpicklistdata = new DataSet();
                                        if (!string.IsNullOrEmpty(picklistname))
                                        {
                                            var pickList = objLS.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                                            {
                                                PICKLIST_DATA = s.PICKLIST_DATA,
                                                PICKLIST_NAME = s.PICKLIST_NAME,
                                                PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                                            }).FirstOrDefault(d => d.PICKLIST_NAME == picklistname);
                                            var objPick = new List<PickList>();
                                            if (pickList != null && !string.IsNullOrEmpty(pickList.PICKLIST_DATA))
                                            {
                                                var pkxmlData = pickList.PICKLIST_DATA;

                                                if (pkxmlData.Contains("<?xml version"))
                                                {
                                                    // pkxmlData = pkxmlData.Remove(0, 1);
                                                    //pkxmlData = pkxmlData.Remove(pkxmlData.Length - 1);
                                                    pkxmlData = pkxmlData.Replace("\\r\\n", "");
                                                    pkxmlData = pkxmlData.Replace("\r\n", "");
                                                    pkxmlData = pkxmlData.Replace("\\\"", "\"");
                                                    pkxmlData =
                                                        pkxmlData.Replace(
                                                            "<?xml version=\"1.0\" standalone=\"yes\"?>",
                                                            "");
                                                    //PKXMLData = PKXMLData.Replace("ListItem", "Value");

                                                    XDocument objXml = XDocument.Parse(pkxmlData);

                                                    objPick =
                                                        objXml.Descendants("NewDataSet")
                                                            .Descendants("Table1")
                                                            .Select(d =>
                                                            {
                                                                var xElement = d.Element("ListItem");
                                                                return xElement != null
                                                                    ? new PickList
                                                                    {
                                                                        ListItem = xElement.Value
                                                                    }
                                                                    : null;
                                                            }).ToList();
                                                }
                                            }
                                            SqlDataAdapter daa2 =
                                      new SqlDataAdapter("Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  ['+ @colName+'],CATALOG_ITEM_NO   from [##t2]') ",
                                          conn);
                                            daa2.Fill(newsheetval);
                                            SqlCommand cvb = new SqlCommand("DELETE TOP(1) From ##t1", conn);
                                            cvb.ExecuteNonQuery();
                                            DataSet picklistdataDS = new DataSet();
                                            DataTable newTable = new DataTable();
                                            // newTable.Columns.Add("value");
                                            for (int rowCount = 0;
                                                rowCount < newsheetval.Tables[0].Rows.Count;
                                                rowCount++)
                                            {
                                                //DataRow DRow = _pickListValue.NewRow();
                                                if (newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim() != "")
                                                {
                                                    var picklistvalue =
                                                        newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim();
                                                    PickList objlist = new PickList();
                                                    objlist.ListItem = picklistvalue;
                                                    if (!objPick.Contains(objlist))
                                                    {
                                                        objPick.Add(objlist);
                                                    }
                                                }
                                            }
                                            var picklistdatas = objPick.DistinctBy(x => x.ListItem).ToList();
                                            foreach (var item in picklistdatas)
                                            {
                                                if (string.IsNullOrEmpty(item.ListItem))
                                                {
                                                    picklistdatas.Remove(item);
                                                }
                                                else if (item.ListItem.Trim() == "")
                                                {
                                                    picklistdatas.Remove(item);
                                                }
                                                else
                                                {
                                                    item.ListItem = item.ListItem.Trim();
                                                }
                                            }
                                            var serializer1 = new JavaScriptSerializer();
                                            var json = serializer1.Serialize(picklistdatas);
                                            XmlDocument doc = JsonConvert.DeserializeXmlNode(
                                                "{\"Table1\":" + json + "}", "NewDataSet");
                                            string picklistdataxml =
                                                "<?xml version=\"1.0\" standalone=\"yes\"?>" +
                                                doc.InnerXml;
                                            var objPicklist =
                                                objLS.TB_PICKLIST.FirstOrDefault(
                                                    s => s.PICKLIST_NAME == picklistname);
                                            if (objPicklist != null)
                                                objPicklist.PICKLIST_DATA = picklistdataxml;
                                            objLS.SaveChanges();
                                        }
                                    }
                                    //DataTable distinctTable =
                                    //    _pickListValue.DefaultView.ToTable( /*distinct*/ true);
                                    //picklistdataDS.Tables.Add(distinctTable);
                                    // picklistdataDS.WriteXml(Application.StartupPath + "\\tempxml.xml");
                                    //FileStream fileStream =
                                    //    new FileStream(Application.StartupPath + "\\tempxml.xml",
                                    //        FileMode.Open);
                                    //System.IO.StreamReader streamWriter = new System.IO.StreamReader(fileStream);
                                    //string dataString = streamWriter.ReadToEnd();
                                    //streamWriter.Close();
                                    //fileStream.Close();
                                }
                                //}
                            }

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    //----------------------end 
                    if (ds.Tables[0].Columns[0].ColumnName == "ErrorMessage")
                    {
                        return "Import Failed~" + importTemp;
                    }
                    else
                    {
                        var custmorids = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name);
                        if (custmorids.Any())
                        {
                            var firstOrDefault = custmorids.FirstOrDefault();
                            if (firstOrDefault != null)
                            {
                                int customerid = firstOrDefault.CustomerId;
                                var catalogids = objLS.TB_CATALOG.Max(x => x.CATALOG_ID);
                                var objcustomercatalogcount =
                                    objLS.Customer_Catalog.Where(
                                        x => x.CATALOG_ID == catalogids && x.CustomerId == customerid);
                                if (!objcustomercatalogcount.Any())
                                {
                                    var objcustomercatalog = new Customer_Catalog
                                    {
                                        CustomerId = customerid,
                                        CATALOG_ID = catalogids,
                                        DateCreated = DateTime.Now,
                                        DateUpdated = DateTime.Now
                                    };
                                    objLS.Customer_Catalog.Add(objcustomercatalog);
                                    objLS.SaveChanges();
                                }
                                var objCustomerRoles =
                                    objLS.aspnet_Roles.Where(x => x.CustomerId == customerid);
                                if (objCustomerRoles.Any())
                                {
                                    foreach (var objobjCustomerRole in objCustomerRoles)
                                    {
                                        var rolecatalog = objLS.Customer_Role_Catalog.Where(x => x.CATALOG_ID == catalogids && x.RoleId == objobjCustomerRole.Role_id && x.CustomerId == customerid);
                                        if (!rolecatalog.Any())
                                        {
                                            var objcustomerrolecatalog = new Customer_Role_Catalog
                                            {
                                                RoleId = objobjCustomerRole.Role_id,
                                                CustomerId = customerid,
                                                CATALOG_ID = catalogids,
                                                DateCreated = DateTime.Now,
                                                IsActive = true,
                                                DateUpdated = DateTime.Now
                                            };
                                            objLS.Customer_Role_Catalog.Add(objcustomerrolecatalog);
                                            // _dbcontext.SaveChanges();
                                        }
                                    }
                                    objLS.SaveChanges();
                                }
                            }
                        }


                        return "Import Success~" + importTemp;
                    }

                }


            }

            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : FinishImport", ex);
                return "Import Failed~" + importTemp;
            }

        }

        [System.Web.Http.HttpGet]
        public string enableitemvalidationsub()
        {

            try
            {
                var customerId = 0;
                var skucnt = objLS.TB_PLAN
                   .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                   .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                var SKUcount = skucnt.Select(a => a).ToList();

                if (SKUcount.Any())
                {
                    //userSKUProductCount = Convert.ToInt32(SKUcount[0].SKU_COUNT);
                    customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                }
                string validate = objLS.Customer_Settings.Where(x => x.CustomerId == customerId).Select(x => x.SubCatalog_Items).FirstOrDefault().ToString();
                if (validate == "1")
                {
                    return "true";
                }
                else
                {
                    return "false";
                }

                ;

            }

            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : enableitemvalidation", ex);
                return "false";
            }

        }

        [System.Web.Http.HttpGet]
        public string enableitemvalidation()
        {

            try
            {
                var customerId = 0;
                var skucnt = objLS.TB_PLAN
                   .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                   .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                   .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                var SKUcount = skucnt.Select(a => a).ToList();

                if (SKUcount.Any())
                {
                    //userSKUProductCount = Convert.ToInt32(SKUcount[0].SKU_COUNT);
                    customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                }
                string validate = objLS.Customer_Settings.Where(x => x.CustomerId == customerId).Select(x => x.SubCatalog_Items).FirstOrDefault().ToString();
                if (validate == "1")
                {
                    return "true";
                }
                else
                {
                    return "false";
                }

                ;

            }

            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : enableitemvalidation", ex);
                return "false";
            }

        }

        [System.Web.Http.HttpPost]
        public string Validateimport(string SheetName, string allowDuplicate, string excelPath, int catalogId, string importtype, string Errorlogoutputformat, int templateId, JArray modeldata)
        {


            string validateResults = string.Empty;
            HttpContext.Current.Session["FAMILYVALIDATIONSESSION"] = null;
            QueryValues queryValues = new QueryValues();
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            var jarray = (modeldata[1]).Select(x => x).ToList();
            DataTable records = new DataTable();
            var fileName = Path.GetFileName(excelPath).Split('.')[0];
            var path = Path.GetFullPath(excelPath);
            var fileExtention = Path.GetExtension(excelPath);
            string excelPathCopy = path.Replace(Path.GetFileName(excelPath), "") + "//" + fileName + "_Copy" + "." + fileExtention;
            if (System.IO.File.Exists(excelPathCopy))
            {
                System.IO.File.Delete(excelPathCopy);

            }
            JArray model = JArray.Parse(modeldata.ToString());
            foreach (var modeldatav in modeldata)
            {

                model = (JArray)modeldatav;
                break;

            }
            JArray validate;

            ValidationList valdationList = new ValidationList();
            bool[] valList = new bool[jarray.Count];

            foreach (var ff in jarray)
            {

                if (ff.ToString().Split(':')[0].Replace("\"", "").ToUpper().Contains("COLUMN"))
                    valdationList.column = ff.ToString().Split(':')[1].Replace("\"", "").Trim().ToUpper() == "TRUE" ? true : false;
                else if (ff.ToString().Split(':')[0].Replace("\"", "").ToUpper().Contains("ID"))
                    valdationList.id = ff.ToString().Split(':')[1].Replace("\"", "").Trim().ToUpper() == "TRUE" ? true : false;
                else if (ff.ToString().Split(':')[0].Replace("\"", "").ToUpper().Contains("PICKLIST"))
                    valdationList.pickList = ff.ToString().Split(':')[1].Replace("\"", "").Trim().ToUpper() == "TRUE" ? true : false;
                else if (ff.ToString().Split(':')[0].Replace("\"", "").ToUpper().Contains("DATATYPE"))
                    valdationList.dataType = ff.ToString().Split(':')[1].Replace("\"", "").Trim().ToUpper() == "TRUE" ? true : false;
                else if (ff.ToString().Split(':')[0].Replace("\"", "").ToUpper().Contains("SUBCATALOG"))
                    valdationList.subCatalog = ff.ToString().Split(':')[1].Replace("\"", "").Trim().ToUpper() == "TRUE" ? true : false;
            }
            _logger.Error("Error at allowDuplicate1 ");
            allowDuplicate = objLS.Customer_Settings.Join(objLS.Customer_User, cs => cs.CustomerId, cu => cu.CustomerId, (cs, cu) => new { cs, cu }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.cs.AllowDuplicateItem_PartNum).FirstOrDefault().ToString().ToUpper() == "TRUE" ? "1" : "0";
            _logger.Error("Error at allowDuplicate2");
            DataSet ds = new DataSet();
            DataSet dsGetProdCnt = new DataSet();
            int importProductCount = 0;
            int userProductCount = 0;
            int userSKUProductCount = 0;
            try
            {

                // CustomerItemNo
                if (System.Web.HttpContext.Current.Session["CustomerItemNo"] == null)
                {
                    var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    int customerId;
                    int.TryParse(customerDetails.ToString(), out customerId);
                    string customerCatalog = queryValues.GetCatalogItemNo(customerId);
                    System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerCatalog;
                }

                // CustomerSubItemNo
                if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
                {
                    var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    int customerId;
                    int.TryParse(customerDetails.ToString(), out customerId);
                    string customerCatalog = queryValues.GetSubCatalogItemNo(customerId);
                    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerCatalog;
                }



                string customAttributeName = HttpContext.Current.Session["CustomerItemNo"].ToString();
                DataTable oattType = new DataTable();
                DataSet replace = new DataSet();
                DataTable missingColumns = new DataTable();
                int ItemVal = Convert.ToInt32(allowDuplicate);
                using (var connnew = new SqlConnection(connectionString))
                {

                    connnew.Open();
                    SQLString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    // adImport.importExcelSheetSelection(excelPath, SheetName);
                    SqlCommand _DBCommand = new SqlCommand(SQLString, connnew);
                    _DBCommand.ExecuteNonQuery();
                    _logger.Error("Error at dt1");

                    DataTable dt = adImport.GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "");
                    _logger.Error("Error at dt2");
                    foreach (DataColumn dcReplaceColumn in dt.Columns)
                    {
                        if (dcReplaceColumn.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerItemNo"].ToString().ToUpper())
                        {
                            dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                            break;
                        }
                        else
                        {
                            if (dcReplaceColumn.ColumnName.ToUpper() == "ITEM#")
                            {
                                dt.Columns.Remove(dcReplaceColumn.ColumnName);
                                break;
                            }

                        }

                    }

                    foreach (DataColumn dcReplaceColumn in dt.Columns)
                    {
                        if (dcReplaceColumn.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerSubItemNo"].ToString().ToUpper())
                        {
                            dcReplaceColumn.ColumnName = "SUBCATALOG_ITEM_NO";
                            break;
                        }
                        else
                        {
                            if (dcReplaceColumn.ColumnName.ToUpper() == "SUBITEM#")
                            {
                                dt.Columns.Remove(dcReplaceColumn.ColumnName);
                                break;
                            }
                        }

                    }
                    _logger.Error("Error at checkCategoryValidation");
                    DataTable checkCategoryValidation = adImport.ValidateCategoryData(dt);
                    DataTable checkFamilyvalidation = adImport.ValidateFamilyData(dt);
                    DataTable checkItemValidation = adImport.ValidateItemNumber(dt);
                    _logger.Error("Error at checkCategoryValidation1");

                    //Check duplicate Attribute name in mapping table/////
                    validateResults = CheckDuplicateMappingAttributeNames(templateId, SheetName, importtype, importTemp);
                    if (validateResults != string.Empty)
                    {
                        return validateResults;
                    }
                    //Check duplicate Attribute name in mapping table/////

                    if (checkItemValidation != null && checkItemValidation.Rows.Count > 0)
                    {
                        return ValidateItemNo(checkItemValidation, importtype, model, excelPath, SheetName, catalogId, Errorlogoutputformat);//return to Item# import
                    }
                    if (checkFamilyvalidation != null && checkFamilyvalidation.Rows.Count > 0)
                    {
                        return ValidateFamilyHierarchey(checkFamilyvalidation, importtype, model, excelPath, SheetName, catalogId, Errorlogoutputformat);//return to Familywithouthierarchey# import
                    }
                    objdatatable = adImport.AddIdColumns(dt, importtype);

                    if (objdatatable == null)
                    {
                        _logger.Error("Error at objdatatable");
                        objdatatable = adImport.GetDataFromExcel(SheetName, excelPathCopy, " SELECT * from [" + SheetName + "$]", "");
                        _logger.Error("Error at objdatatable1");
                        foreach (DataColumn dcReplaceColumn in objdatatable.Columns)
                        {
                            if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                            {
                                dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                                break;
                            }
                            else
                            {
                                if (dcReplaceColumn.ColumnName.ToUpper() == "ITEM#")
                                {
                                    dt.Columns.Remove(dcReplaceColumn.ColumnName);
                                }

                            }

                        }
                        foreach (DataColumn dcReplaceColumn in objdatatable.Columns)
                        {
                            if (dcReplaceColumn.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerSubItemNo"].ToString().ToUpper())
                            {
                                dcReplaceColumn.ColumnName = "SUBCATALOG_ITEM_NO";
                                break;
                            }
                            else
                            {
                                if (dcReplaceColumn.ColumnName.ToUpper() == "SUBITEM#")
                                {
                                    dt.Columns.Remove(dcReplaceColumn.ColumnName);
                                }
                            }

                        }

                    }
                    _logger.Error("Error at UpdateCatalogDetails5");
                    objdatatable = UpdateCatalogDetails(importtype, catalogId, objdatatable);
                    _logger.Error("Error at UpdateCatalogDetails5");

                    //Change _ Start.
                    if (importtype.ToUpper().Contains("SUBPRODUCT") && objdatatable.Columns.Contains("SUBITEM#"))
                    {
                        objdatatable.Columns["SUBITEM#"].ColumnName = "SUBCATALOG_ITEM_NO";
                    }
                    else if (importtype.ToUpper().Contains("PRODUCT") && objdatatable.Columns.Contains("ITEM#"))
                    {
                        objdatatable.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                    }
                    //Change _ End.
                    string[] hierarchyColumns = { "CATALOG_ID",
                                                    "CATALOG_NAME",
                                                  "CATEGORY_ID",
                                                  "FAMILY_ID",
                                                  "FAMILY_NAME",
                                                  "SUBFAMILY_ID",
                                                  "PRODUCT_ID",
                                                  "CATALOG_ITEM_NO","ITEM#"};
                    if (importtype.ToUpper() == "SUBPRODUCTS")
                    {
                        hierarchyColumns = new string[]{ "CATALOG_ID",
                                                  "CATALOG_NAME",
                                                  "CATEGORY_ID",
                                                  "FAMILY_ID",
                                                  "SUBFAMILY_ID",
                                                  "PRODUCT_ID",
                                                  "CATALOG_ITEM_NO","ITEM#","SUBPRODUCT_ID","SUBCATALOG_ITEM_NO","SUBITEM#"};
                    }
                    if (importtype.ToUpper() == "FAMILIES")
                    {
                        hierarchyColumns = new string[]{ "CATALOG_ID",
                                                    "CATALOG_NAME",
                                                  "CATEGORY_ID",
                                                  "FAMILY_ID",
                                                  "SUBFAMILY_ID"};

                    }
                    if (importtype.ToUpper() == "CATEGORIES")
                    {
                        hierarchyColumns = new string[]{ "CATALOG_ID",
                                                    "CATALOG_NAME",
                                                  "CATEGORY_ID"
                                                };

                    }
                    //---------------------------------For Mapping existing attribute -----------------------------------
                    DataTable rename = new DataTable();
                    if (objdatatable.Rows.Count > 0)
                    {
                        rename = objdatatable;
                        List<String> stringArr = new List<String>();

                        //-------------------------------Missing Columns validation ------------------------------------------
                        if (valdationList.column == true)
                        {
                            foreach (var items in hierarchyColumns)
                            {
                                if (!objdatatable.Columns.Contains(items))
                                {
                                    if ((items.Contains("CATALOG_ITEM_NO") && objdatatable.Columns.Contains("ITEM#")) || (items.Contains("ITEM#") && objdatatable.Columns.Contains("CATALOG_ITEM_NO")))
                                    {
                                        continue;
                                    }
                                    if (items.Contains("SUBCATALOG_ITEM_NO") && objdatatable.Columns.Contains("SUBITEM#") || (items.Contains("SUBITEM#") && objdatatable.Columns.Contains("SUBCATALOG_ITEM_NO")))
                                    {
                                        continue;
                                    }
                                    if (items != "CATALOG_ID" && items != "CATALOG_NAME")
                                    {
                                        if (objdatatable.Columns.Contains("Missing_Columns"))
                                        {
                                            foreach (DataRow dr in objdatatable.Rows)
                                            {
                                                dr["Missing_Columns"] = dr["Missing_Columns"].ToString() + "," + items.ToString();
                                            }

                                        }
                                        else
                                        {
                                            objdatatable.Columns.Add("Missing_Columns");
                                            foreach (DataRow dr in objdatatable.Rows)
                                            {
                                                dr["Missing_Columns"] = items.ToString();
                                            }
                                        }
                                        var cmd2 = new SqlCommand();
                                        cmd2.Connection = connnew;
                                        cmd2.CommandText = " EXEC('if exists(select NAME from sys.objects where type=''u'' and name=''Missingcolumns" + importTemp + "'')BEGIN DROP TABLE [Missingcolumns" + importTemp + "] END')";
                                        cmd2.CommandType = CommandType.Text;
                                        cmd2.ExecuteNonQuery();
                                        SQLString = IC.CreateTableToImport("[Missingcolumns" + importTemp + "]", objdatatable);
                                        var cmd22 = new SqlCommand();
                                        cmd22.Connection = connnew;
                                        cmd22.CommandText = SQLString;
                                        cmd22.CommandType = CommandType.Text;
                                        cmd22.ExecuteNonQuery();
                                        var bulkCopysp = new SqlBulkCopy(connnew)
                                        {
                                            DestinationTableName = "[Missingcolumns" + importTemp + "]"
                                        };
                                        bulkCopysp.WriteToServer(objdatatable);
                                        validateResults = "Missingcolumns~";
                                        connnew.Close();
                                        connnew.Open();
                                    }

                                }
                                else
                                {
                                    if (importtype.ToUpper().Contains("SUBPRODUCT"))
                                    {
                                        string[] emptyColumnValues = new string[] { "CATEGORY_ID",
                                                  "FAMILY_ID" };

                                        if (objdatatable.Columns.Contains(items) && emptyColumnValues.Contains(items))
                                        {

                                            if (items.Contains("SUBCATNAME") && !string.IsNullOrEmpty(objdatatable.Rows[0][items].ToString()))
                                            {

                                            }
                                            if (string.IsNullOrEmpty(objdatatable.Rows[0][items].ToString()))
                                            {
                                                if (objdatatable.Columns.Contains("Missing_Columns"))
                                                {
                                                    foreach (DataRow dr in objdatatable.Rows)
                                                    {
                                                        dr["Missing_Columns"] = dr["Missing_Columns"].ToString() + "," + items.ToString();
                                                    }

                                                }
                                                else
                                                {
                                                    objdatatable.Columns.Add("Missing_Columns");
                                                    foreach (DataRow dr in objdatatable.Rows)
                                                    {
                                                        dr["Missing_Columns"] = items.ToString();
                                                    }

                                                }
                                                var cmd2 = new SqlCommand();
                                                cmd2.Connection = connnew;
                                                cmd2.CommandText = " EXEC('if exists(select NAME from sys.tables where type=''u'' and name=''Missingcolumns" + importTemp + "'')BEGIN DROP TABLE [Missingcolumns" + importTemp + "] END')";
                                                cmd2.CommandType = CommandType.Text;
                                                cmd2.ExecuteNonQuery();
                                                SQLString = IC.CreateTableToImport("[Missingcolumns" + importTemp + "]", objdatatable);
                                                var cmd22 = new SqlCommand();
                                                cmd22.Connection = connnew;
                                                cmd22.CommandText = SQLString;
                                                cmd22.CommandType = CommandType.Text;
                                                cmd22.ExecuteNonQuery();
                                                var bulkCopysp = new SqlBulkCopy(connnew)
                                                {
                                                    DestinationTableName = "[Missingcolumns" + importTemp + "]"
                                                };
                                                bulkCopysp.WriteToServer(objdatatable);
                                                validateResults = "Missingcolumns~";
                                                connnew.Close();
                                                connnew.Open();
                                            }
                                        }
                                    }
                                }
                            }




                        }
                    }

                    SQLString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''##IMPORTTEMP" +
                        importTemp + "'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    _DBCommand = new SqlCommand(SQLString, connnew);
                    _DBCommand.ExecuteNonQuery();
                    if (importtype.ToUpper().Contains("SUBPRODUCT") && objdatatable.Columns.Contains("SUBCATALOG_ITEM_NO"))
                    {
                        objdatatable.Columns["SUBCATALOG_ITEM_NO"].ColumnName = "SUBITEM#";
                    }
                    else if (importtype.ToUpper().Contains("PRODUCT") && objdatatable.Columns.Contains("ITEM#"))
                    {
                        objdatatable.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                    }
                    if (importtype.ToUpper().Contains("FAML") && !objdatatable.Columns.Contains("FAMILY_NAME"))
                    {
                        objdatatable.Columns.Add("FAMILY_NAME");
                    }
                    if (importtype.ToUpper().Contains("CATEGORIES") && !objdatatable.Columns.Contains("CATEGORY_NAME"))
                    {
                        objdatatable.Columns.Add("CATEGORY_NAME");
                    }

                    SQLString = IC.CreateTable("[##IMPORTTEMP" + importTemp + "]", excelPath, SheetName, objdatatable);
                    _DBCommand = new SqlCommand(SQLString, connnew);
                    _DBCommand.ExecuteNonQuery();

                    var bulkCopy = new SqlBulkCopy(connnew)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(objdatatable);
                    SQLString = "select *  from [##IMPORTTEMP" + importTemp + "]";
                    _DBCommand = new SqlCommand(SQLString, connnew);
                    _DBCommand.ExecuteNonQuery();

                    var customerId = 0;
                    var skucnt = objLS.TB_PLAN
                       .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                       .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                       .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                    var SKUcount = skucnt.Select(a => a).ToList();
                    if (SKUcount.Any())
                    {
                        userSKUProductCount = Convert.ToInt32(SKUcount[0].SKU_COUNT);
                        customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                    }
                    var Itemnumbervalidation = objLS.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerId);
                    //------------------Created by vinothkumar for Validation based on catalogsettings June 2017 -------------------------------

                    string validation = Itemnumbervalidation.SubCatalog_Items.ToString();
                    if (Itemnumbervalidation != null && validation == "1" && valdationList.subCatalog == true && importtype.ToUpper().Contains("PRODUCT"))
                    {
                        string cat = objLS.Customer_Settings.Where(x => x.CustomerId == customerId).Select(x => x.Customer_MasterCatalog).FirstOrDefault().ToString();
                        DataSet validate_subitems = new DataSet();
                        SQLString = "SELECT CATALOG_NAME as CATALOG_NAME,CATEGORY_NAME as CATEGORY_NAME,FAMILY_NAME as FAMILY_NAME,CATALOG_ITEM_NO  into [validatedresult" + importTemp + "] FROM [##IMPORTTEMP" + importTemp + "] WHERE CATALOG_ITEM_NO IN  (SELECT CATALOG_ITEM_NO FROM [##IMPORTTEMP" + importTemp + "] EXCEPT SELECT DISTINCT TPS.STRING_VALUE FROM TB_CATALOG_PRODUCT TCP JOIN TB_PROD_SPECS TPS ON TPS.PRODUCT_ID = TCP.PRODUCT_ID  WHERE TCP.CATALOG_ID = " + cat + " AND TPS.ATTRIBUTE_ID = 1 )";
                        if (importtype.ToUpper().Contains("SUBPRODUCT"))
                        {
                            SQLString = "SELECT CATALOG_NAME as CATALOG_NAME,CATEGORY_NAME as CATEGORY_NAME,FAMILY_NAME as FAMILY_NAME,CATALOG_ITEM_NO,SUBITEM# as SUBITEM into [validatedresult" + importTemp + "] FROM [##IMPORTTEMP" + importTemp + "] WHERE SUBITEM# IN " +

                                " (SELECT SUBITEM# FROM [##IMPORTTEMP" + importTemp + "] " +
                                         " except " +
                                         " select distinct tps.STRING_VALUE from TB_PROD_SPECS tps " +
                                         " join TB_CATALOG_PRODUCT tc on tc.PRODUCT_ID = tps.PRODUCT_ID " +
                                         " join TB_SUBPRODUCT ts on ts.SUBPRODUCT_ID = tps.PRODUCT_ID " +
                                          " where tps.ATTRIBUTE_ID = 1 and ts.CATALOG_ID = " + cat + " )";
                        }
                        SqlCommand _DBCommandvv = new SqlCommand(SQLString, connnew);
                        _DBCommandvv.CommandTimeout = 0;
                        _DBCommandvv.ExecuteNonQuery();
                        var cmd1fsub = new SqlCommand("select * from [validatedresult" + importTemp + "]", connnew);
                        var dafa = new SqlDataAdapter(cmd1fsub);
                        dafa.Fill(validate_subitems);
                        if (validate_subitems.Tables.Count > 0 && validate_subitems.Tables[0].Rows.Count > 0)
                        {
                            validateResults = validateResults + "CatalogValidationFailed~";
                        }
                        else
                        {
                            validateResults = validateResults + "CatalogValidationPassed~";
                        }
                    }
                    var sqlstring1 = new SqlCommand("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##AttributeTemp" + importTemp + "]'')BEGIN DROP TABLE [##AttributeTemp" + importTemp + "] END')", connnew);
                    sqlstring1.ExecuteNonQuery();
                    var sqlcmd = new SqlCommand();
                    sqlcmd.Connection = connnew;
                    oattType = IC.SelectedColumnsToImport(objdatatable, model);

                    SQLString = IC.CreateTableToImport("[##AttributeTemp" + importTemp + "]", oattType);
                    sqlcmd.CommandText = SQLString;
                    sqlcmd.CommandType = CommandType.Text;
                    sqlcmd.ExecuteNonQuery();
                    bulkCopy.DestinationTableName = "[##AttributeTemp" + importTemp + "]";
                    bulkCopy.WriteToServer(oattType);
                    var cmbpk10 = new SqlCommand("update [##AttributeTemp" + importTemp + "] set ATTRIBUTE_TYPE = 1 where ATTRIBUTE_TYPE = 10", connnew);
                    cmbpk10.ExecuteNonQuery();
                    var cmbpk1 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1'') IS NOT NULL  DROP TABLE  ##t1') ", connnew);
                    cmbpk1.ExecuteNonQuery();
                    var cmbpk2 = new SqlCommand("select * into ##t1  from [##AttributeTemp" + importTemp + "] where attribute_type <>0", connnew);
                    cmbpk2.ExecuteNonQuery();
                    var cmbpk3 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2'') IS NOT NULL  DROP TABLE  ##t2') ", connnew);
                    cmbpk3.ExecuteNonQuery();
                    var cmbpk4 = new SqlCommand("select * into ##t2  from [##IMPORTTEMP" + importTemp + "]", connnew);
                    cmbpk4.ExecuteNonQuery();
                    var Checkpicklistupdate = objLS.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerId);
                    if (Checkpicklistupdate != null && Checkpicklistupdate.ShowCustomAPPS && valdationList.pickList == true && importtype.ToUpper().Contains("PRODUCT"))
                    {

                        string attributeName = string.Join(",", (oattType.AsEnumerable().Where(x => x.Field<string>("ATTRIBUTE_TYPE") != "0").Select(attrName => attrName.Field<string>("ATTRIBUTE_NAME")).ToArray()));
                        string pickListValidation = adImport.CheckPickList(importTemp, importtype);
                        if (pickListValidation == "Validation Failed")
                        {
                            validateResults = validateResults + "PickListFailed~";
                        }


                    }
                    SQLString =
                           "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##validationResult" +
                           importTemp + "]'')BEGIN DROP TABLE [##validationResult" + importTemp + "] END')";
                    // adImport.importExcelSheetSelection(excelPath, SheetName);
                    _DBCommand = new SqlCommand(SQLString, connnew);
                    _DBCommand.ExecuteNonQuery();
                    objdatatable = adImport.GetDataFromExcel(SheetName, excelPath, " SELECT * from [" + SheetName + "$]", "Valid");
                    foreach (DataColumn dcReplaceColumn in objdatatable.Columns)
                    {
                        if (dcReplaceColumn.ColumnName.ToUpper() == customAttributeName.ToUpper())
                        {
                            dcReplaceColumn.ColumnName = "CATALOG_ITEM_NO";
                            break;
                        }

                    }

                    foreach (DataColumn dcReplaceColumn in objdatatable.Columns)
                    {
                        if (dcReplaceColumn.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerSubItemNo"].ToString().ToUpper())
                        {
                            dcReplaceColumn.ColumnName = "SUBCATALOG_ITEM_NO";
                            break;
                        }

                    }

                    if (importtype.ToUpper().Contains("PRODUCT") && objdatatable.Columns.Contains("ITEM#"))
                    {
                        objdatatable.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                    }
                    if (importtype.ToUpper().Contains("FAMI") && !objdatatable.Columns.Contains("FAMILY_NAME"))
                    {
                        objdatatable.Columns.Add("FAMILY_NAME");
                    }
                    if (!objdatatable.Columns.Contains("CATEGORY_NAME"))
                    {
                        objdatatable.Columns.Add("CATEGORY_NAME");
                    }
                    if (objdatatable == null)
                    {
                        objdatatable = adImport.GetDataFromExcel(SheetName, excelPathCopy, " SELECT * from [" + SheetName + "$]", "");
                    }
                    _logger.Error("Error at UpdateCatalogDetails3 ");
                    objdatatable = UpdateCatalogDetails(importtype, catalogId, objdatatable);
                    _logger.Error("Error at UpdateCatalogDetails4 ");
                    if (objdatatable.Columns.Contains("ACTION"))
                    {
                        objdatatable.Columns["ACTION"].SetOrdinal(0);
                    }
                    if (importtype.ToUpper().Contains("SUBPRODUCT") && objdatatable.Columns.Contains("SUBITEM#"))
                    {
                        objdatatable.Columns["SUBITEM#"].ColumnName = "SUBCATALOG_ITEM_NO";
                    }
                    SQLString = IC.CreateTable("[##validationResult" + importTemp + "]", excelPath, SheetName, objdatatable);
                    _DBCommand = new SqlCommand(SQLString, connnew);
                    _DBCommand.ExecuteNonQuery();
                    var bulkCopyvalid = new SqlBulkCopy(connnew)
                    {
                        DestinationTableName = "[##validationResult" + importTemp + "]"
                    };
                    bulkCopyvalid.WriteToServer(objdatatable);

                    if (valdationList.dataType == true)
                    {
                        if (importtype.ToUpper().Contains("SUBPRODUCT"))
                        {
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO_IMPORTVALIsDATIONSUB ''" + importTemp + "''')", connnew) { CommandTimeout = 0 };
                            cmd.ExecuteNonQuery();
                        }
                        else if (importtype.ToUpper().Contains("PRODUCT"))
                        {
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO_IMPORTVALIDATION ''" + importTemp + "''')", connnew) { CommandTimeout = 0 };
                            cmd.ExecuteNonQuery();
                        }
                        else if (importtype.ToUpper().Contains("FAMIL"))
                        {
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO_FAMILYIMPORTVALIDATION ''" + importTemp + "''')", connnew) { CommandTimeout = 0 };
                            cmd.ExecuteNonQuery();
                        }
                        else if (importtype.ToUpper().Contains("CATEGORIES"))
                        {
                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO_CATEGORYIMPORTVALIDATIONATTRIBUTE ''" + importTemp + "''')", connnew) { CommandTimeout = 0 };
                            cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        var sqlString = " if exists(select 1  from sys.tables where name like 'FinalvalidationResult'+@SESSIONID+'%') begin exec('Drop table [FinalvalidationResult' + @SESSIONID + ']')End if exists(select distinct t.name from tempdb.sys.tables t  where t.name like '%validationResult%')Begin if exists(select 1  from sys.tables where name like 'Missingcolumns' + @SESSIONID + '%')begin if exists(select 1  from sys.tables where name like 'validatedresult' + @SESSIONID + '%')" +
                            "begin exec('Select distinct val.*,Missing_Columns,valid.CATALOG_ITEM_NO as New_catalog_Item into [FinalvalidationResult' + @SESSIONID + '] from [##validationResult' + @SESSIONID + '] val left join [Missingcolumns' + @SESSIONID + '] mis on val.catalog_item_no=mis.catalog_item_no left join [validatedresult' + @SESSIONID + '] valid on valid.catalog_item_no=mis.catalog_item_no') end else Begin exec('Select distinct val.*,Missing_Columns into [FinalvalidationResult' + @SESSIONID + '] from [##validationResult' + @SESSIONID + '] val left join [Missingcolumns' + @SESSIONID + '] mis on val.catalog_item_no=mis.catalog_item_no')" +
                            "End end else if exists(select 1  from sys.tables where name like 'validatedresult' + @SESSIONID + '%') Begin exec('Select distinct val.*,valid.CATALOG_ITEM_NO as New_catalog_Item into [FinalvalidationResult' + @SESSIONID + ']  from [##validationResult' + @SESSIONID + '] val left join [validatedresult' + @SESSIONID + '] valid on valid.catalog_item_no=val.catalog_item_no') End else Begin exec('Select distinct * into [FinalvalidationResult' + @SESSIONID + '] from [##validationResult' + @SESSIONID + '] val ') End End";
                        var cmd = new SqlCommand(sqlString, connnew) { CommandTimeout = 0 };
                        cmd.Parameters.Add("@SESSIONID", SqlDbType.VarChar);
                        cmd.Parameters["@SESSIONID"].Value = importTemp;
                        cmd.ExecuteNonQuery();
                    }
                    DataSet finalValidationResult = new DataSet();
                    var getFinalValidation = new SqlCommand("if exists(select 1  from sys.tables where name like 'FinalvalidationResult" + importTemp + "%') begin select * from [FinalvalidationResult" + importTemp + "] end", connnew);
                    var dagetFinalValidation = new SqlDataAdapter(getFinalValidation);
                    dagetFinalValidation.Fill(finalValidationResult);

                    var sqlstring1exist =
                        new SqlCommand(
                            "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''ValidateFullList" +
                            importTemp + "'')BEGIN DROP TABLE [ValidateFullList" + importTemp + "] END')", connnew);
                    sqlstring1exist.ExecuteNonQuery();
                    string sqlStringval = string.Empty;

                    if (finalValidationResult != null && finalValidationResult.Tables.Count > 0 && finalValidationResult.Tables[0].Rows.Count > 0)
                    {
                        sqlStringval = "select * from [FinalvalidationResult" + importTemp + "] where VALIDATION_TYPE is not null and VALIDATION_TYPE<>'' ; select * into [ValidateFullList" + importTemp + "]  from [FinalvalidationResult" + importTemp + "] where VALIDATION_TYPE is not null and VALIDATION_TYPE<>''";
                        DataSet dsvalidateResult = new DataSet();
                        var cmbpk14 = new SqlCommand(sqlStringval, connnew);
                        dagetFinalValidation = new SqlDataAdapter(cmbpk14);
                        dagetFinalValidation.Fill(dsvalidateResult);


                        var cmbpk12 = new SqlCommand("drop table [FinalvalidationResult" + importTemp + "];", connnew);
                        cmbpk12.ExecuteNonQuery();

                        if (allowDuplicate == "1")
                        {
                            int removedRecords = 0;
                            for (int drow = 0; drow < dsvalidateResult.Tables[0].Rows.Count; drow++)
                            {
                                DataRow dr = dsvalidateResult.Tables[0].Rows[drow];
                                if (dr["VALIDATION_TYPE"].ToString().ToUpper().Contains("DUPLICATE_ITEM_NO"))
                                {
                                    removedRecords = removedRecords + 1;
                                    if (dsvalidateResult.Tables[0].Rows.Count == removedRecords)
                                    {
                                        dsvalidateResult.Tables[0].Rows.Clear();
                                    }
                                    else
                                    {
                                        dr.Delete();
                                    }

                                }
                            }
                        }
                        if (dsvalidateResult.Tables.Count > 0 && dsvalidateResult.Tables[0].Rows.Count > 0)
                        {
                            if (finalValidationResult.Tables.Count > 0 && finalValidationResult.Tables[0].Rows.Count > 0)
                            {

                                string filepath = HttpContext.Current.Server.MapPath("~/Content/" + SheetName + "");
                                string format = filepath + ".csv";
                                CreateCSVFile(finalValidationResult.Tables[0], format);
                                string txtformat = filepath + ".txt";
                                CreateTextFile(finalValidationResult.Tables[0], txtformat, "\t");
                                _logger.Error("Error at ExportDataSetToExcel1 ");
                                ExportDataSetToExcel(finalValidationResult, SheetName);
                                _logger.Error("Error at ExportDataSetToExcel2 ");
                                ExportErrorLogDataSetToExcel(finalValidationResult, importTemp, excelPath, SheetName, Errorlogoutputformat);
                                validateResults = string.Empty;
                                foreach (DataRow dr in finalValidationResult.Tables[0].Rows)
                                {
                                    if (!validateResults.Contains(dr["VALIDATION_TYPE"].ToString()))
                                        validateResults = validateResults + dr["VALIDATION_TYPE"].ToString() + "~";
                                }
                            }
                        }
                    }
                    sqlstring1exist =
                        new SqlCommand(
                            "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''ValidateFullList" +
                            importTemp + "'')BEGIN DROP TABLE [ValidateFullList" + importTemp + "] END')", connnew);
                    sqlstring1exist.ExecuteNonQuery();

                    sqlstring1exist =
                       new SqlCommand(
                           "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''errorlog" +
                           importTemp + "'')BEGIN DROP TABLE [errorlog" + importTemp + "] END')", connnew);
                    sqlstring1exist.ExecuteNonQuery();

                    sqlstring1exist =
                       new SqlCommand(
                           "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''Missingcolumns" +
                           importTemp + "'')BEGIN DROP TABLE [Missingcolumns" + importTemp + "] END')", connnew);
                    sqlstring1exist.ExecuteNonQuery();

                    sqlstring1exist =
                       new SqlCommand(
                           "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''validatedresult" +
                           importTemp + "'')BEGIN DROP TABLE [validatedresult" + importTemp + "] END')", connnew);
                    sqlstring1exist.ExecuteNonQuery();

                    sqlstring1exist =
                       new SqlCommand(
                           "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''Picklistlog" +
                           importTemp + "'')BEGIN DROP TABLE [Picklistlog" + importTemp + "] END')", connnew);
                    sqlstring1exist.ExecuteNonQuery();

                    sqlstring1exist =
                      new SqlCommand(
                          "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''tempresult" +
                          importTemp + "'')BEGIN DROP TABLE [tempresult" + importTemp + "] END')", connnew);
                    sqlstring1exist.ExecuteNonQuery();
                    sqlstring1exist =
                new SqlCommand(
                    "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''tempresultsub" +
                    importTemp + "'')BEGIN DROP TABLE [tempresultsub" + importTemp + "] END')", connnew);
                    sqlstring1exist.ExecuteNonQuery();

                    string P_Count = Convert.ToString(objdatatable.Rows.Count);
                    //To get the importProcess value.
                    int importProcessRecordsValue = Convert.ToInt32(ConfigurationManager.AppSettings["ImportProcessRecords"]);


                    // PdfXpress Validation   - - - - -  Start. Mariya Vijayan

                    validateResults = PdfXpressValidation(objdatatable, validateResults);


                    // PdfXpress Validation   - - - - -  End.


                    if (validateResults == "" || validateResults.Contains("Passed"))
                    {
                        return "Import success~" + importTemp + "~" + P_Count + "~" + importProcessRecordsValue;
                    }
                    else
                    {
                        return validateResults + importTemp;
                    }


                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : Validateimport", ex);
                return "Import Failed~" + importTemp;
            }

        }



        /// <summary>
        /// To validate All the scnario ;
        /// </summary>
        /// <param name="objdatatable"></param>
        /// <param name="validateResults"></param>
        /// <returns>
        /// returs validation
        /// by : Mariya vijayan.
        /// </returns>
        public string PdfXpressValidation(DataTable objdatatable, string validateResults)
        {

            try
            {


                DataTable CatalogValidationData = new DataTable();
                DataTable CategoryValidationData = new DataTable();
                DataTable FamilyValidationData = new DataTable();
                DataTable ProductValidationData = new DataTable();


                if (objdatatable.Columns.Contains("Catalog_PdfTemplate") & objdatatable.Columns.Contains("Category_PdfTemplate") & objdatatable.Columns.Contains("Family_PdfTemplate") & objdatatable.Columns.Contains("Product_PdfTemplate"))
                {
                    // Catalog pDf Validation


                    CatalogValidationData = objdatatable.AsEnumerable().Where(x => x.Field<string>("Catalog_PdfTemplate") != null
                                                              && x.Field<string>("Catalog_PdfTemplate").ToUpper().Contains(".MRT")).CopyToDataTable();

                    var catdataCount = objdatatable.AsEnumerable().Where(x => x.Field<string>("Catalog_PdfTemplate") != null).Select(x => x.Field<string>("FAMILY_NAME")).Count();
                    if (CatalogValidationData.Rows.Count > 0)
                    {
                        if (CatalogValidationData.Rows.Count != catdataCount)
                        {

                            validateResults = "CalogPdfXpressError";
                        }

                    }
                    // Category pDf Validation


                    CategoryValidationData = objdatatable.AsEnumerable().Where(x => x.Field<string>("Category_PdfTemplate") != null
                                                              && x.Field<string>("Category_PdfTemplate").ToUpper().Contains(".MRT")
                                                           ).CopyToDataTable();

                    var catedataCount = objdatatable.AsEnumerable().Where(x => x.Field<string>("Category_PdfTemplate") != null).Select(x => x.Field<string>("FAMILY_NAME")).Count();
                    if (CategoryValidationData.Rows.Count > 0)
                    {
                        if (CategoryValidationData.Rows.Count != catedataCount)
                        {

                            validateResults = "CategoryPdfXpressError";
                        }
                        else
                        {// To check duplicate in one category
                            // var duplicateCategoryId = objdatatable.AsEnumerable().Where(x => x.Field<string>("Category_PdfTemplate") != null
                            //        && x.Field<string>("Category_PdfTemplate").ToUpper().Contains(".MRT")                                                                                                                   
                            //       ).CopyToDataTable();

                            //  if(objdatatable.Columns.Contains("CATEGORY_ID"))  // Check only with id coloums import
                            // {

                            //   var checkcategory_id = objdatatable.AsEnumerable().Where(x => x.Field<string>("CATEGORY_ID") != null).Select(x => x.Field<string>("CATEGORY_ID")).FirstOrDefault();


                            //if(!string.IsNullOrEmpty(checkcategory_id))
                            //{
                            //                                        var duplicates = duplicateCategoryId.AsEnumerable()
                            //    .Select(dr => dr.Field<string>("CATEGORY_ID"))
                            //    .GroupBy(x => x)
                            //    .Where(g => g.Count() > 1)
                            //    .Select(g => g.Key)
                            //    .ToList();


                            //   if (!string.IsNullOrEmpty(duplicates.ToString()))
                            //   {
                            //       validateResults = "Category_PdfXpress_DuplicateRecords";
                            //   }

                            //}

                            //   }


                        }

                    }

                    ///   To family Validation  -- start



                    FamilyValidationData = objdatatable.AsEnumerable().Where(x => x.Field<string>("Family_PdfTemplate") != null
                                                              && x.Field<string>("Family_PdfTemplate").ToUpper().Contains(".MRT")
                                                           ).CopyToDataTable();

                    var familydataCount = objdatatable.AsEnumerable().Where(x => x.Field<string>("Family_PdfTemplate") != null).Select(x => x.Field<string>("FAMILY_NAME")).Count();
                    if (CategoryValidationData.Rows.Count > 0)
                    {
                        if (FamilyValidationData.Rows.Count != familydataCount)
                        {

                            validateResults = "CalogPdfXpressError";
                        }
                        else
                        {
                        }





                    }

                    /// To family Validation  -- End
                    /// 


                    ///   To Product Validation  -- start



                    ProductValidationData = objdatatable.AsEnumerable().Where(x => x.Field<string>("Product_PdfTemplate") != null
                                                              && x.Field<string>("Product_PdfTemplate").ToUpper().Contains(".MRT")
                                                           ).CopyToDataTable();

                    var productdataCount = objdatatable.AsEnumerable().Where(x => x.Field<string>("Product_PdfTemplate") != null).Select(x => x.Field<string>("FAMILY_NAME")).Count();
                    if (CategoryValidationData.Rows.Count > 0)
                    {
                        if (ProductValidationData.Rows.Count != productdataCount)
                        {

                            validateResults = "ProductPdfXpressError";
                        }
                        else
                        {
                        }
                    }

                    /// To Product Validation  -- End





                }
            }
            catch (Exception ex)
            {


            }



            return validateResults;
        }







        [System.Web.Http.HttpPost]
        public string Validatetabledesignerimport(string sheetName, string allowDuplicate, string excelPath, int catalogId, string importtype, string Errorlogoutputformat, JArray modeldata)
        {
            string validateResults = string.Empty;
            QueryValues queryValues = new QueryValues();
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            string validateCategoryNameMissing = "";
            string validateFamilyNameMissing = "";
            string validateMissingColumns = "";
            string catalogName = string.Empty;
            string catalogid = string.Empty;
            int rowcount = 0;
            List<string> columnToRemoveFromTableDesigner = new List<string>();
            int missingDataRowCount = 0;
            DataTable excelDatas = adImport.GetDataFromExcel(sheetName, excelPath, "select * from [" + sheetName + "$]", "");

            DataTable excelData = excelDatas.Clone(); //just copy structure, no data
            for (int j = 0; j < excelDatas.Columns.Count; j++)
            {
                if (excelData.Columns[j].DataType != typeof(string))
                    excelData.Columns[j].DataType = typeof(string);
            }

            foreach (DataRow dr in excelDatas.Rows)
            {
                excelData.ImportRow(dr);
            }




            DataTable deletedRecords = adImport.GetDataFromExcel(sheetName, excelPath, " SELECT * from [" + sheetName + "$]", "DELETE");
            AddIdColumnsForTableDesigner(deletedRecords, importtype);  //Add ID columns if it not exists
            DataTable categoryDistinctValues = new DataTable();
            DataTable deleteRecordslog = new DataTable();
            deleteRecordslog = deletedRecords.Clone();
            deleteRecordslog.Columns.Add("VALIDATION TYPE", typeof(System.String));
            deleteRecordslog.Columns.Add("MESSAGE", typeof(System.String));
            if (excelData != null)
            {
                if (excelData.Columns[0].ColumnName.ToString() == "Action")
                {
                    excelData.Columns.Remove("Action");
                }
            }

            AddIdColumnsForTableDesigner(excelData, importtype);  //Add ID columns if it not exists

            DataTable dt = excelData.Clone();

            DataTable filterMandatoryColumns = excelData.Copy();

            string sqlConn = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
            dt.Columns.Add("VALIDATION TYPE", typeof(System.String));
            dt.Columns.Add("MESSAGE", typeof(System.String));
            int count = 0;

            string[] mandatoryColumnNames = new string[] { "CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_NAME", "FAMILY_ID", "FAMILY_NAME", "FAMILY_TABLE_STRUCTURE", "STRUCTURE_NAME", "IS_DEFAULT" };

            foreach (var item in mandatoryColumnNames)
            {
                DataColumnCollection columns = excelData.Columns;
                if (columns.Contains(item))
                {

                }
                else
                {

                    if (dt.Rows.Count == 0)
                    {
                        var rows = excelData.AsEnumerable().Take(1).ToList();
                        dt.Rows.Add(rows[0].ItemArray);
                    }


                    foreach (DataRow dr in dt.Rows)
                    {

                        string columnNamemissing = item;
                        validateMissingColumns = "MISSINGCOLUMNS";



                        if (count == 0)
                        {
                            dr["VALIDATION TYPE"] = "Invalid Column Name.";
                            dr["MESSAGE"] = columnNamemissing + " Column name Missing/Invalid.";
                        }
                        else
                        {
                            dr["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                            dr["MESSAGE"] = dr["MESSAGE"].ToString() + "\n" + columnNamemissing + "  Column name Missing/Invalid.";
                        }
                        count++;
                    }

                }

            }



            if (validateMissingColumns == string.Empty)
            {

                foreach (DataRow rows in excelData.Rows)
                {

                    int missingDataCount = 0;
                    string excelCatalogName = rows["CATALOG_NAME"].ToString();
                    string excelCategoryName = rows["CATEGORY_NAME"].ToString();
                    string excelFamilyName = rows["FAMILY_NAME"].ToString();
                    string excelFamilyTableStructure = rows["FAMILY_TABLE_STRUCTURE"].ToString();
                    string excelStrucutureName = rows["STRUCTURE_NAME"].ToString();
                    string excelIsDefault = rows["IS_DEFAULT"].ToString();
                    dt.Rows.Add(rows.ItemArray);

                    if (excelCatalogName == string.Empty)
                    {
                        validateMissingColumns = "MISSINGCOLUMNS";
                        if (missingDataCount == 0)
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = "Catalog name should not be blank.";
                        }
                        else
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Catalog name should not be blank.";
                        }
                        missingDataCount++;

                    }

                    if (excelCategoryName == string.Empty)
                    {
                        validateMissingColumns = "MISSINGCOLUMNS";
                        if (missingDataCount == 0)
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = "Category name should not be blank.";
                        }
                        else
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Category name should not be blank.";
                        }
                        missingDataCount++;
                    }


                    if (excelFamilyName == string.Empty)
                    {
                        validateMissingColumns = "MISSINGCOLUMNS";
                        if (missingDataCount == 0)
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = "Family name should not be blank.";
                        }
                        else
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Family name should not be blank.";
                        }
                        missingDataCount++;
                    }

                    if (excelFamilyTableStructure == string.Empty)
                    {
                        validateMissingColumns = "MISSINGCOLUMNS";
                        if (missingDataCount == 0)
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = "Family table structure should not be blank.";
                        }
                        else
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Family table structure should not be blank.";
                        }
                        missingDataCount++;
                    }

                    if (excelStrucutureName == string.Empty)
                    {
                        validateMissingColumns = "MISSINGCOLUMNS";
                        if (missingDataCount == 0)
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = "Strucutre Name should not be blank.";
                        }
                        else
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Strucutre Name should not be blank.";
                        }
                        missingDataCount++;
                    }

                    if (excelIsDefault == string.Empty)
                    {
                        validateMissingColumns = "MISSINGCOLUMNS";
                        if (missingDataCount == 0)
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = "Is default should not be blank please give 'True' or 'False'.";
                        }
                        else
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Is default should not be blank please give 'True' or 'False'.";
                        }
                        missingDataCount++;
                    }

                    else if (!excelIsDefault.Equals("True") && excelIsDefault.Equals(1) && excelIsDefault.Equals(0) && !excelIsDefault.Equals("False"))
                    {
                        validateMissingColumns = "MISSINGCOLUMNS";
                        if (missingDataCount == 0)
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = "Is default should not be blank please give 'True' or 'False'.";
                        }
                        else
                        {
                            dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                            dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Is default should not be blank please give 'True' or 'False'.";
                        }
                        missingDataCount++;
                    }

                    missingDataRowCount++;
                }
            }



            if (validateMissingColumns == string.Empty)
            {
                foreach (DataRow deleteRows in deletedRecords.Rows)
                {
                    int deleteCount = 0;
                    string deleteStrucutureName = deleteRows["STRUCTURE_NAME"].ToString();
                    if (deleteStrucutureName == "Default Layout")
                    {
                        validateMissingColumns = "MISSINGCOLUMNS";
                        deleteRecordslog.Rows.Add(deleteRows.ItemArray);
                        deleteRecordslog.Columns.Add("Action", typeof(System.String)).SetOrdinal(0);
                        deleteRecordslog.Rows[deleteCount]["Action"] = "DELETE";
                        deleteRecordslog.Rows[deleteCount]["VALIDATION TYPE"] = "Action Column.";
                        deleteRecordslog.Rows[deleteCount]["MESSAGE"] = "Default layout can not be deleted.";

                        deleteCount++;
                    }
                }
                dt = deleteRecordslog.Copy();
            }


            //get catalog id
            if (validateMissingColumns == string.Empty)
            {

                var excelDistinctCatalogName = excelData.AsEnumerable()
                       .Select(s => new
                       {
                           catalogName = s.Field<string>("CATALOG_NAME"),
                       })
                       .Distinct().ToList();

                catalogName = excelDistinctCatalogName[0].catalogName.ToString();

                catalogid = _dbcontext.TB_CATALOG.Where(s => s.CATALOG_NAME == catalogName && s.FLAG_RECYCLE == "A").Select(a => a.CATALOG_ID).FirstOrDefault().ToString();


                categoryDistinctValues.Columns.Add("CATALOG_ID", typeof(System.String));


                string subCategoryname = "SUBCATNAME_L1";
                int subCategorycount = 1;
                categoryDistinctValues.Columns.Add("CATEGORY_NAME", typeof(System.String));


                for (int i = 0; i < excelData.Columns.Count; i++)
                {


                    if (excelData.Columns[i].ColumnName == subCategoryname)
                    {
                        subCategorycount = subCategorycount + 1;
                        categoryDistinctValues.Columns.Add(excelData.Columns[i].ColumnName, typeof(System.String));
                        subCategoryname = "SUBCATNAME_L" + subCategorycount;
                    }

                }

            }
            if (validateMissingColumns == string.Empty)
            {
                for (int i = 0; i < categoryDistinctValues.Columns.Count; i++)
                {

                    string values = categoryDistinctValues.Columns[i].ColumnName;
                    Type fieldType = excelData.Columns[values].DataType;

                    if (fieldType.Name == "Double")
                    {
                        values = values.ToString();
                    }
                    else
                    {

                    }


                    DataTable dtClone = excelData.Clone(); //just copy structure, no data
                    for (int j = 0; j < excelData.Columns.Count; j++)
                    {
                        if (dtClone.Columns[j].DataType != typeof(string))
                            dtClone.Columns[j].DataType = typeof(string);
                    }

                    foreach (DataRow dr in excelData.Rows)
                    {
                        dtClone.ImportRow(dr);
                    }




                    var a = dtClone.AsEnumerable()
                   .Select(r => r.Field<string>(values))
                   .Distinct().ToList();

                    DataRow datarow = null;

                    for (int j = 0; j <= a.Count - 1; j++)
                    {
                        if (a[j] != null)
                        {

                            datarow = categoryDistinctValues.NewRow();

                            datarow[values] = a[j].ToString();

                            categoryDistinctValues.Rows.Add(datarow);
                        }

                    }

                }

                Dictionary<string, List<string>> categoryDictionaries = new Dictionary<string, List<string>>();


                for (int j = 0; j < categoryDistinctValues.Columns.Count; j++)
                {

                    string columnName = categoryDistinctValues.Columns[j].ColumnName.ToString();


                    List<string> list = categoryDistinctValues.AsEnumerable().Select(s => s.Field<string>(columnName)).ToList();
                    list.RemoveAll(item => item == null);

                    Dictionary<string, List<string>> dictionary = Enumerable.Range(0, 1)
                        .ToDictionary(i => columnName, x => list);
                    categoryDictionaries.Add(columnName, list);
                }


                string checkCategoryCatalogId = string.Empty;
                string categoryName = string.Empty;

                foreach (KeyValuePair<String, List<string>> values in categoryDictionaries)
                {

                    for (int j = 0; j < values.Value.Count; j++)
                    {

                        if (values.Key == "CATALOG_ID")
                        {
                            checkCategoryCatalogId = values.Value[j].ToString();
                        }

                        if (values.Key == "CATEGORY_NAME")
                        {
                            categoryName = values.Value[j].ToString();
                        }

                        else if (values.Key != "CATALOG_ID")
                        {
                            categoryName = values.Value[j].ToString();
                        }

                        if (checkCategoryCatalogId == string.Empty)
                        {
                            checkCategoryCatalogId = catalogid;
                        }
                        if (categoryName.Contains("'"))
                        {
                            string[] arrCategoryName = categoryName.Split('\'');
                            string changeCategoryName = "";
                            for (int i = 0; i < arrCategoryName.Length; i++)
                            {
                                if (changeCategoryName != "")
                                {
                                    changeCategoryName = changeCategoryName + "''" + arrCategoryName[i];
                                }
                                else
                                {
                                    changeCategoryName = arrCategoryName[i];
                                }

                            }
                            categoryName = changeCategoryName;
                        }

                        if (categoryName != string.Empty)
                        {

                            using (var objSqlConnection = new SqlConnection(sqlConn))
                            {


                                string checkColumnname = "CATEGORY_NAME";
                                objSqlConnection.Open();
                                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                                objSqlCommand.CommandType = CommandType.StoredProcedure;
                                objSqlCommand.CommandTimeout = 0;
                                objSqlCommand.Connection = objSqlConnection;
                                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = checkCategoryCatalogId;
                                objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                                objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = categoryName ?? "";
                                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = "";
                                objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = checkColumnname ?? "";
                                objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.NVarChar, 500).Value = "";
                                objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.NVarChar, -1).Value = "";
                                objSqlCommand.Parameters.Add("@Action", SqlDbType.NVarChar, 50).Value = "";
                                objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                                objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                                using (SqlDataReader rdr = objSqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                                {
                                    while (rdr.Read())
                                    {

                                        validateResults = rdr.GetString(rdr.GetOrdinal("Result"));



                                        if (validateResults == "FALSE")
                                        {

                                            string columnName = values.Key;
                                            var rows = excelData.AsEnumerable()
                                            .Where(r => r.Field<string>(columnName) == categoryName).ToList();

                                            DataTable dt_log = new DataTable();
                                            dt_log = excelData.Copy();
                                            dt_log.Columns.Add("MESSAGE").DataType.ToString();
                                            dt_log.Columns.Add("VALIDATION TYPE").DataType.ToString();
                                            // DataRow dr_log = dt_log.NewRow();

                                            //dt_log = excelData.Clone();
                                            //dt_log.Rows.Add(excelData.Rows[0]);


                                            if (excelData.Columns.Count != dt.Columns.Count)
                                            {
                                                for (int i = 0; i < excelData.Columns.Count; i++)
                                                {
                                                    if (!dt.Columns.Contains(excelData.Columns[i].ColumnName))
                                                    {
                                                        dt_log.Columns.Remove(excelData.Columns[i].ColumnName);
                                                    }
                                                }

                                                dt.ImportRow(dt_log.Rows[0]);
                                            }
                                            else
                                            {
                                                dt.Rows.Add(rows[0].ItemArray);
                                            }



                                            validateCategoryNameMissing = "CATEGORYNAMEMISSING";

                                            if (count == 0)
                                            {

                                                dt.Rows[rowcount]["VALIDATION TYPE"] = "Invalid Category Name.";
                                                dt.Rows[rowcount]["MESSAGE"] = "Category " + categoryName + " is not valid.";
                                            }
                                            else
                                            {
                                                dt.Rows[rowcount]["VALIDATION TYPE"] = dt.Rows[rowcount]["VALIDATION TYPE"].ToString() + "\n" + "Invalid Category Name.";
                                                dt.Rows[rowcount]["MESSAGE"] = dt.Rows[rowcount]["MESSAGE"].ToString() + "\n" + " Category " + categoryName + " is not valid.";
                                            }
                                            count++;
                                            rowcount++;
                                        }

                                    }
                                    rdr.Close();
                                }

                            }

                        }
                    }

                }


                //Family name validation

                DataTable familyNameDistinctValues = new DataTable();

                familyNameDistinctValues.Columns.Add("CATALOG_ID", typeof(System.String));
                familyNameDistinctValues.Columns.Add("FAMILY_NAME", typeof(System.String));

                string subFamilyname = "SUBFAMNAME_L1";
                int subfamilycount = 1;

                for (int i = 0; i < excelData.Columns.Count; i++)
                {



                    if (excelData.Columns[i].ColumnName == subFamilyname)
                    {
                        subfamilycount = subfamilycount + 1;
                        familyNameDistinctValues.Columns.Add(excelData.Columns[i].ColumnName, typeof(System.String));
                        subFamilyname = "SUBFAMNAME_L" + subfamilycount;
                    }



                }

                for (int i = 0; i < familyNameDistinctValues.Columns.Count; i++)
                {

                    string values = familyNameDistinctValues.Columns[i].ColumnName;


                    //DataTable dt_clone = excelData.Clone();

                    //for (int k = 0; k < excelData.Columns.Count; k++)
                    //{
                    //    if (dt_clone.Columns[k].DataType != typeof(string))
                    //        dt_clone.Columns[k].DataType = typeof(string);
                    //}


                    //foreach(DataRow dr in excelData.Rows)
                    //{
                    //    dt_clone.ImportRow(dr);
                    //}



                    DataTable dtClone = excelData.Clone(); //just copy structure, no data
                    for (int j = 0; j < excelData.Columns.Count; j++)
                    {
                        if (dtClone.Columns[j].DataType != typeof(string))
                            dtClone.Columns[j].DataType = typeof(string);
                    }

                    foreach (DataRow dr in excelData.Rows)
                    {
                        dtClone.ImportRow(dr);
                    }




                    var a = dtClone.AsEnumerable()
                   .Select(r => r.Field<string>(values))
                      .Distinct().ToList();

                    DataRow datarow = null;

                    for (int j = 0; j <= a.Count - 1; j++)
                    {
                        if (a[j] != null)
                        {

                            datarow = familyNameDistinctValues.NewRow();

                            datarow[values] = a[j].ToString();

                            familyNameDistinctValues.Rows.Add(datarow);
                        }


                    }


                }


                Dictionary<string, List<string>> familyDictionaries = new Dictionary<string, List<string>>();


                for (int j = 0; j < familyNameDistinctValues.Columns.Count; j++)
                {

                    string columnName = familyNameDistinctValues.Columns[j].ColumnName.ToString();


                    List<string> list = familyNameDistinctValues.AsEnumerable().Select(s => s.Field<string>(columnName)).ToList();
                    list.RemoveAll(item => item == null);

                    Dictionary<string, List<string>> dictionary = Enumerable.Range(0, 1)
                        .ToDictionary(i => columnName, x => list);
                    familyDictionaries.Add(columnName, list);
                }


                string checkFamilyCatalogId = string.Empty;
                string familyName = string.Empty;

                foreach (KeyValuePair<String, List<string>> values in familyDictionaries)
                {


                    for (int j = 0; j < values.Value.Count; j++)
                    {

                        if (values.Key == "CATALOG_ID")
                        {
                            checkFamilyCatalogId = values.Value[j].ToString();
                        }

                        if (values.Key == "FAMILY_NAME")
                        {
                            familyName = values.Value[j].ToString();
                        }

                        else if (values.Key != "CATALOG_ID")
                        {
                            familyName = values.Value[j].ToString();
                        }


                        if (checkFamilyCatalogId == string.Empty)
                        {
                            checkFamilyCatalogId = catalogid;
                        }
                        if (familyName.Contains("'"))
                        {
                            string[] arrFamilyName = familyName.Split('\'');
                            string changeFamilyName = "";
                            for (int i = 0; i < arrFamilyName.Length; i++)
                            {
                                if (changeFamilyName != "")
                                {
                                    changeFamilyName = changeFamilyName + "''" + arrFamilyName[i];
                                }
                                else
                                {
                                    changeFamilyName = arrFamilyName[i];
                                }

                            }
                            familyName = changeFamilyName;

                        }

                        if (familyName != string.Empty)
                        {

                            using (var objSqlConnection = new SqlConnection(sqlConn))
                            {

                                string checkColumnname = "FAMILY_NAME";
                                objSqlConnection.Open();
                                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                                objSqlCommand.CommandType = CommandType.StoredProcedure;
                                objSqlCommand.CommandTimeout = 0;
                                objSqlCommand.Connection = objSqlConnection;
                                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = checkFamilyCatalogId;
                                objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                                objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = "";
                                objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = familyName ?? "";
                                objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = checkColumnname ?? "";
                                objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.NVarChar, 500).Value = "";
                                objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.NVarChar, -1).Value = "";
                                objSqlCommand.Parameters.Add("@Action", SqlDbType.NVarChar, 50).Value = "";
                                objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                                objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                                using (SqlDataReader rdr = objSqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                                {

                                    while (rdr.Read())
                                    {
                                        validateResults = rdr.GetString(rdr.GetOrdinal("Result"));
                                        if (validateResults == "FALSE")
                                        {
                                            string columnName = values.Key;
                                            var rows = excelData.AsEnumerable()
                                            .Where(r => r.Field<string>(columnName) == familyName).ToList();

                                            //dt.Rows.Add(rows[0].ItemArray);
                                            DataTable dt_log = new DataTable();
                                            dt_log = excelData.Copy();

                                            dt_log.Columns.Add("MESSAGE").DataType.ToString();
                                            dt_log.Columns.Add("VALIDATION TYPE").DataType.ToString();
                                            // DataRow dr_log = dt_log.NewRow();

                                            //dt_log = excelData.Clone();
                                            //dt_log.Rows.Add(excelData.Rows[0]);


                                            if (excelData.Columns.Count != dt.Columns.Count)
                                            {
                                                for (int i = 0; i < excelData.Columns.Count; i++)
                                                {
                                                    if (!dt.Columns.Contains(excelData.Columns[i].ColumnName))
                                                    {
                                                        dt_log.Columns.Remove(excelData.Columns[i].ColumnName);
                                                    }
                                                }

                                                dt.ImportRow(dt_log.Rows[0]);
                                            }
                                            else
                                            {
                                                dt.Rows.Add(rows[0].ItemArray);
                                            }

                                            validateFamilyNameMissing = "FAMILYNAMEMISSING";
                                            if (count == 0)
                                            {
                                                dt.Rows[rowcount]["VALIDATION TYPE"] = "Invalid Family Name";
                                                dt.Rows[rowcount]["MESSAGE"] = "Family " + familyName + " is not valid";
                                            }
                                            else
                                            {
                                                dt.Rows[rowcount]["VALIDATION TYPE"] = dt.Rows[rowcount]["VALIDATION TYPE"].ToString() + "\n" + "Invalid Family Name";
                                                dt.Rows[rowcount]["MESSAGE"] = dt.Rows[rowcount]["MESSAGE"].ToString() + "\n" + " Family " + familyName + " is not valid";
                                            }

                                            count++;
                                            rowcount++;
                                        }
                                    }
                                    rdr.Close();
                                }
                                objSqlConnection.Close();
                            }
                        }
                    }
                }
            }

            count = 0;
            rowcount = 0;

            var errorLogDataTableRowRemove = dt.AsEnumerable().Where(row => row.Field<string>("VALIDATION TYPE") == null && row.Field<string>("MESSAGE") == null);

            foreach (var row in errorLogDataTableRowRemove.ToList())
            {
                row.Delete();
            }
            HttpContext.Current.Session["ValidateTableDesigner"] = dt;


            if (validateFamilyNameMissing == string.Empty && validateCategoryNameMissing == string.Empty && validateMissingColumns == string.Empty)
            {
                validateResults = "Passed";
            }
            else
            {
                validateResults = "Failed";
            }

            try
            {


                if (validateResults.Contains("Passed"))
                {
                    return "Import success~" + importTemp;
                }
                else
                {
                    return "Import success~" + importTemp + "~" + validateFamilyNameMissing + "~" + validateCategoryNameMissing + "~" + validateMissingColumns;
                }
            }

            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : Validatetabledesignerimport", ex);
                return "Import Failed~" + importTemp;
            }

        }




        public DataTable UpdateCatalogDetails(string importType, int catalogId, DataTable catalogUpdate)
        {
            try
            {


                if (importType.ToUpper().Contains("PRODUCT") || importType.ToUpper().Contains("SUB"))
                {
                    if (!catalogUpdate.Columns.Contains("CATALOG_ID"))
                    {
                        catalogUpdate.Columns.Add("CATALOG_ID");
                        catalogUpdate.Select("CATALOG_ITEM_NO<>''")
                            .ToList<DataRow>()
                            .ForEach(x => x["CATALOG_ID"] = catalogId);

                        catalogUpdate.Columns["CATALOG_ID"].SetOrdinal(0);
                    }
                    else if (catalogUpdate.Columns.Contains("CATALOG_ID"))
                    {
                        foreach (DataRow dr in catalogUpdate.Rows)
                        {
                            if (dr["CATALOG_ITEM_NO"] != null || dr["CATALOG_ITEM_NO"].ToString() != "")
                            {
                                dr["CATALOG_ID"] = catalogId;
                            }
                            else
                            {

                            }

                        }
                        //catalogUpdate.Select("CATALOG_ITEM_NO<>''")
                        //    .ToList<DataRow>()
                        //    .ForEach(x => x["CATALOG_ID"] = catalogId.ToString());
                        catalogUpdate.Columns["CATALOG_ID"].SetOrdinal(0);
                    }
                    if (!catalogUpdate.Columns.Contains("CATALOG_NAME"))
                    {
                        catalogUpdate.Columns.Add("CATALOG_NAME");
                        catalogUpdate.Select("CATALOG_ITEM_NO<>''")
                            .ToList<DataRow>()
                            .ForEach(
                                x =>
                                    x["CATALOG_NAME"] =
                                        objLS.TB_CATALOG.Where(xx => xx.CATALOG_ID == catalogId)
                                            .Select(y => y.CATALOG_NAME)
                                            .FirstOrDefault());
                        catalogUpdate.Columns["CATALOG_NAME"].SetOrdinal(1);

                    }
                    else if (catalogUpdate.Columns.Contains("CATALOG_NAME"))
                    {
                        catalogUpdate.Select("CATALOG_ITEM_NO<>''")
                            .ToList<DataRow>()
                            .ForEach(
                                x =>
                                    x["CATALOG_NAME"] =
                                        objLS.TB_CATALOG.Where(xx => xx.CATALOG_ID == catalogId)
                                            .Select(y => y.CATALOG_NAME)
                                            .FirstOrDefault());

                        catalogUpdate.Columns["CATALOG_NAME"].SetOrdinal(1);
                    }
                }
                else if (importType.ToUpper().Contains("FAMILY_IMPORT_WITHOUT_HIERARCHY"))
                {
                    if (!catalogUpdate.Columns.Contains("CATALOG_ID"))
                    {
                        catalogUpdate.Columns.Add("CATALOG_ID");
                        catalogUpdate.Select("FAMILY_NAME<>''")
                            .ToList<DataRow>()
                            .ForEach(x => x["CATALOG_ID"] = catalogId);
                        catalogUpdate.Columns["CATALOG_ID"].SetOrdinal(0);
                    }
                    else if (catalogUpdate.Columns.Contains("CATALOG_ID"))
                    {
                        catalogUpdate.Select("FAMILY_NAME<>''")
                            .ToList<DataRow>()
                            .ForEach(x => x["CATALOG_ID"] = catalogId);
                        catalogUpdate.Columns["CATALOG_ID"].SetOrdinal(0);
                    }
                    if (!catalogUpdate.Columns.Contains("CATALOG_NAME"))
                    {
                        catalogUpdate.Columns.Add("CATALOG_NAME");
                        catalogUpdate.Select("FAMILY_NAME<>''")
                            .ToList<DataRow>()
                            .ForEach(
                                x =>
                                    x["CATALOG_NAME"] =
                                        objLS.TB_CATALOG.Where(xx => xx.CATALOG_ID == catalogId)
                                            .Select(y => y.CATALOG_NAME)
                                            .FirstOrDefault());
                        catalogUpdate.Columns["CATALOG_NAME"].SetOrdinal(1);

                    }
                    else if (catalogUpdate.Columns.Contains("CATALOG_NAME"))
                    {
                        catalogUpdate.Select("FAMILY_NAME<>''")
                            .ToList<DataRow>()
                            .ForEach(
                                x =>
                                    x["CATALOG_NAME"] =
                                        objLS.TB_CATALOG.Where(xx => xx.CATALOG_ID == catalogId)
                                            .Select(y => y.CATALOG_NAME)
                                            .FirstOrDefault());

                        catalogUpdate.Columns["CATALOG_NAME"].SetOrdinal(1);
                    }
                }
                else
                {
                    if (!catalogUpdate.Columns.Contains("CATALOG_ID"))
                    {
                        catalogUpdate.Columns.Add("CATALOG_ID");
                        catalogUpdate.Select("CATEGORY_NAME<>''")
                            .ToList<DataRow>()
                            .ForEach(x => x["CATALOG_ID"] = catalogId);
                        catalogUpdate.Columns["CATALOG_ID"].SetOrdinal(0);
                    }
                    else if (catalogUpdate.Columns.Contains("CATALOG_ID"))
                    {
                        catalogUpdate.Select("CATEGORY_NAME<>''")
                            .ToList<DataRow>()
                            .ForEach(x => x["CATALOG_ID"] = catalogId);

                        catalogUpdate.Columns["CATALOG_ID"].SetOrdinal(0);
                    }
                    if (!catalogUpdate.Columns.Contains("CATALOG_NAME"))
                    {
                        catalogUpdate.Columns.Add("CATALOG_NAME");
                        catalogUpdate.Select("CATEGORY_NAME<>''")
                            .ToList<DataRow>()
                            .ForEach(
                                x =>
                                    x["CATALOG_NAME"] =
                                        objLS.TB_CATALOG.Where(xx => xx.CATALOG_ID == catalogId)
                                            .Select(y => y.CATALOG_NAME)
                                            .FirstOrDefault());

                        catalogUpdate.Columns["CATALOG_NAME"].SetOrdinal(1);

                    }
                    else if (catalogUpdate.Columns.Contains("CATALOG_NAME"))
                    {
                        catalogUpdate.Select("CATEGORY_NAME<>''")
                            .ToList<DataRow>()
                            .ForEach(
                                x =>
                                    x["CATALOG_NAME"] =
                                        objLS.TB_CATALOG.Where(xx => xx.CATALOG_ID == catalogId)
                                            .Select(y => y.CATALOG_NAME)
                                            .FirstOrDefault());

                        catalogUpdate.Columns["CATALOG_NAME"].SetOrdinal(1);
                    }
                }

            }
            catch (Exception objexception)
            {
                _logger.Error("Error at ImportApiController : ExportDataSetToExcelerrorlog", objexception);
                //return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
            return catalogUpdate;
        }

        public void CreateTextFile(DataTable dt_DataTable, string strFilePath, string Delimeter)
        {
            // Create the Text file to which grid data will be exported.    

            if (Delimeter == "\\t")
            {
                Delimeter = "\t";
            }
            else if (Delimeter == "\\s")
            {
                Delimeter = "  ";
            }

            StreamWriter sw = new StreamWriter(strFilePath, false);
            //First we will write the headers.
            int iColCount = dt_DataTable.Columns.Count;
            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dt_DataTable.Columns[i]);
                if (i < iColCount - 1)
                {
                    sw.Write(Delimeter);
                }
            }
            sw.Write(sw.NewLine);

            // Now write all the rows.

            foreach (DataRow dr in dt_DataTable.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }
                    if (i < iColCount - 1)
                    {
                        sw.Write(Delimeter);
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();



            //if (FTPdetails[0] != null && FTPdetails[1] != null && FTPdetails[2] != null)
            //{
            //    UploadFTP(strFilePath, FTPdetails);
            //}
        }

        //----------------------For export of new items logs vinothkumar june17--------------------------------//
        [System.Web.Http.HttpPost]
        public HttpResponseMessage importlogs(string Errorlogoutputformat, string fileName)
        {
            try
            {
                if (Errorlogoutputformat.ToLower() == "csv")
                {

                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".csv" : fileName + ".csv");
                }
                else if (Errorlogoutputformat.ToLower() == "text")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".txt" : fileName + ".txt");
                }
                else if (Errorlogoutputformat.ToLower() == "xml")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".xml" : fileName + ".xml");
                }
                else if (Errorlogoutputformat.ToLower() == "xlsx")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".xlsx" : fileName + ".xlsx");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".xls" : fileName + ".xls");
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at importApiController : importlogs", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        public void CreateCSVFile(DataTable dt_DataTable, string strFilePath)
        {
            // Create the CSV file to which grid data will be exported.
            StreamWriter sw = new StreamWriter(strFilePath, false);
            //First we will write the headers.
            int iColCount = dt_DataTable.Columns.Count;
            for (int i = 0; i < iColCount; i++)
            {
                sw.Write(dt_DataTable.Columns[i]);
                if (i < iColCount - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
            // Now write all the rows.
            foreach (DataRow dr in dt_DataTable.Rows)
            {
                for (int i = 0; i < iColCount; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        sw.Write(dr[i].ToString());
                    }
                    if (i < iColCount - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();



        }


        public DataSet GetDataSetExcelColumns2CatalogMapping(DataTable ddTable, JArray model)
        {

            string[] coldefs = new string[ddTable.Columns.Count];
            int i = 0;
            foreach (DataColumn coldef in ddTable.Columns)
            {
                coldefs[i] = coldef.ToString();
                i++;
            }
            if (coldefs == null)
                return null;

            DataSet ds = new DataSet();
            DataTable table = ds.Tables.Add("ExcelColumns2CatalogMapping");
            DataColumn[] cols = new DataColumn[8];
            cols[0] = new DataColumn("ExcelColumn", System.Type.GetType("System.String"));
            cols[1] = new DataColumn("EntryType", System.Type.GetType("System.String"));
            cols[2] = new DataColumn("Selected2Import", System.Type.GetType("System.Boolean"));
            cols[3] = new DataColumn("CatalogMapField", System.Type.GetType("System.String"));
            cols[4] = new DataColumn("IsSystemField", System.Type.GetType("System.Boolean"));
            cols[5] = new DataColumn("ExcelRange", System.Type.GetType("System.String"));
            cols[6] = new DataColumn("ColumnProcessOrder", System.Type.GetType("System.Int32"));
            cols[7] = new DataColumn("FieldType", System.Type.GetType("System.Int32"));

            table.Columns.AddRange(cols);

            DataTable dt = (DataTable)JsonConvert.DeserializeObject(model.ToString(), (typeof(DataTable)));

            foreach (DataRow drow in dt.Rows)
            {
                if (drow[2].ToString() != "False")
                {
                    DataRow dr = table.NewRow();
                    dr["ExcelColumn"] = drow[0];
                    dr["EntryType"] = "ExcelColumn";
                    dr["Selected2Import"] = drow[2];
                    dr["CatalogMapField"] = drow[1].ToString() == string.Empty ? drow[0] : drow[1] == null ? drow[0] : drow[1].ToString();
                    dr["IsSystemField"] = drow[3].ToString() == "True" ? "True" : "False";
                    dr["FieldType"] = drow[4];
                    table.Rows.Add(dr);
                }
            }

            return ds;
        }

        public string DataSetToXml(DataSet dsDataSet)
        {
            string xmlString = string.Empty;
            xmlString = "<NewDataSet>";
            foreach (DataTable dt in dsDataSet.Tables)
            {
                string[] strcolumn = new string[dt.Columns.Count];
                int i = 0;
                foreach (DataColumn dcColumn in dt.Columns)
                {
                    strcolumn[i] = dcColumn.ColumnName.ToString();
                    i = i + 1;
                }
                foreach (DataRow drRow in dt.Rows)
                {
                    xmlString = xmlString + "<" + dt.TableName.Replace(" ", "_x0020_").Replace("&", "&amp;").Replace("<", "&lt;").Replace("(", "_x0028_").Replace(")", "_x0029_").Replace("\"", "U+0022").Replace(">", "&gt;") + ">";
                    //foreach (var s in strcolumn)
                    //{

                    //}
                    for (int j = 0; j < strcolumn.Length; j++)
                    {

                        xmlString = xmlString + "<" + strcolumn[j].Replace(" ", "_x0020_").Replace("&", "&amp;").Replace("<", "&lt;").Replace("(", "_x0028_").Replace(")", "_x0029_").Replace("\"", "U+0022").Replace(">", "&gt;") + ">" + drRow[strcolumn[j]].ToString().Replace("&", "&amp;").Replace("<", "&lt;").Replace("\"", "U+0022").Replace(">", "&gt;") + "</" + strcolumn[j].Replace(" ", "_x0020_").Replace("&", "&amp;").Replace("<", "&lt;").Replace("\"", "U+0022").Replace(">", "&gt;") + ">";

                    }
                    xmlString = xmlString + "</" + dt.TableName.Replace(" ", "_x0020_").Replace("&", "&amp;").Replace("<", "&lt;").Replace("(", "_x0028_").Replace(")", "_x0029_").Replace("\"", "U+0022").Replace(">", "&gt;") + ">";
                }
            }

            xmlString = xmlString.Replace("(", "_x0028_").Replace(")", "_x0029_").Replace("%", "0x25").Replace("#", "%23").Replace("?", "0x3F");
            xmlString = xmlString + "</NewDataSet>";
            return xmlString;
        }
        [System.Web.Http.HttpPost]
        public JsonResult SaveAsTemplate(string allowDuplicate, string sheetName, string excelPath, JArray data)
        {
            DataTable importSheeTable = IC.BasicImportExcelSheetSelection(excelPath, sheetName) as DataTable;
            DataSet importSheet = GetDataSetExcelColumns2CatalogMapping(importSheeTable, data);
            DataTable dtAllowDuplicate = new DataTable();
            dtAllowDuplicate.Columns.Add("AlloWDuplicate");
            DataRow drRow = dtAllowDuplicate.NewRow();
            drRow[0] = allowDuplicate;
            dtAllowDuplicate.Rows.Add(drRow);
            importSheet.Tables.Add(importSheeTable);
            importSheet.Tables.Add(dtAllowDuplicate);
            StringWriter sw = new StringWriter();
            importSheet.WriteXml(sw);
            sw.Close();
            string result = DataSetToXml(importSheet);
            XmlDocument xdoc = new XmlDocument();

            try
            {
                xdoc.LoadXml(result);

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("/Content/XMLTemplate/")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("/Content/XMLTemplate/"));
                }
                xdoc.Save(System.Web.HttpContext.Current.Server.MapPath("/Content/XMLTemplate/myfilename.xml"));
                string fileName = System.Web.HttpContext.Current.Server.MapPath("/Content/XMLTemplate/myfilename.xml");
                string[] file = new string[3];
                file[0] = fileName;
                file[1] = Path.GetFileName(fileName);


                return new JsonResult() { Data = file };
            }
            catch (Exception ex)
            {
                return new JsonResult() { Data = "" };
            }


        }

        [System.Web.Http.HttpGet]
        public JsonResult LoadXmlFileTSheet(string templatePath)
        {
            DataSet dsXmlSheet = new DataSet();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(templatePath);

                string xmldoc = doc.InnerXml.ToString();
                StringReader stream = new StringReader(xmldoc);
                XmlTextReader reader = new XmlTextReader(stream);
                dsXmlSheet.ReadXml(reader);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at LoadXmlFileTSheet : ", ex);

            }
            return new JsonResult() { Data = dsXmlSheet.Tables[0].Rows.Cast<DataRow>().ToList() };
        }

        [System.Web.Http.HttpGet]
        public JsonResult LoadXmlFile(string templatePath, string importType)
        {
            DataSet dtResult = new DataSet();
            DataSet dsXmlSheet = new DataSet();
            var excelPath = string.Empty;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(templatePath);

                string xmldoc = doc.InnerXml.ToString();
                StringReader stream = new StringReader(xmldoc);
                XmlTextReader reader = new XmlTextReader(stream);
                dsXmlSheet.ReadXml(reader);
                excelPath = Path.GetFullPath(templatePath).Replace(Path.GetFileName(templatePath), "") + Guid.NewGuid() + ".xls";
                // Microsoft.Office.Interop.Excel.Application excelApp = new Microsoft.Office.Interop.Excel.Application();
                DataTable Tbl = dsXmlSheet.Tables[1];

                if (Tbl == null || Tbl.Columns.Count == 0)
                    throw new Exception("ExportToExcel: Null or empty input table!\n");

                // load excel, and create a new workbook
                //Excel.Application excelApp = new Excel.Application();
                //excelApp.Workbooks.Add();

                //// single worksheet
                //Excel._Worksheet workSheet = excelApp.ActiveSheet;

                // column headings
                //for (int i = 0; i < Tbl.Columns.Count; i++)
                //{
                //    workSheet.Cells[1, (i + 1)] = Tbl.Columns[i].ColumnName;
                //}

                //// rows
                //for (int i = 0; i < Tbl.Rows.Count; i++)
                //{
                //    // to do: format datetime values before printing
                //    for (int j = 0; j < Tbl.Columns.Count; j++)
                //    {
                //        workSheet.Cells[(i + 2), (j + 1)] = Tbl.Rows[i][j];
                //    }
                //}

                Workbook workbook1 = new Workbook();
                //  workbook1.SetCurrentFormat(WorkbookFormat.Excel2007);
                Worksheet workSheet = workbook1.Worksheets.Add("Sheet1");
                // Create column headers for each column
                for (int columnIndex = 0; columnIndex < Tbl.Columns.Count; columnIndex++)
                {
                    workSheet.Rows[0].Cells[columnIndex].Value = Tbl.Columns[columnIndex].ColumnName;
                }

                // Starting at row index 1, copy all data rows in
                // the data table to the worksheet
                int rowIndex = 1;
                foreach (DataRow dataRow in Tbl.Rows)
                {
                    Infragistics.Documents.Excel.WorksheetRow row = workSheet.Rows[rowIndex++];

                    for (int columnIndex = 0; columnIndex < dataRow.ItemArray.Length; columnIndex++)
                    {
                        row.Cells[columnIndex].Value = dataRow.ItemArray[columnIndex];
                    }
                }

                // check fielpath
                if (excelPath != null && excelPath != "")
                {
                    try
                    {
                        workbook1.Save(excelPath);

                    }
                    catch (Exception ex)
                    {
                        throw new Exception("ExportToExcel: Excel file could not be saved! Check filepath.\n"
                            + ex.Message);
                    }
                }
                else    // no filepath is given
                {
                    //excelApp.Visible = true;
                }
                //using (ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook())
                //{
                //    wb.Worksheets.Add(dsXmlSheet.Tables[1], "Customers");
                //    wb.SaveAs(excelPath);
                //}

                // Save the spreadsheet

                //using (ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook())
                //{
                //    wb.Worksheets.Add(dsXmlSheet.Tables[1], "Customers");
                //    wb.SaveAs(excelPath);
                //}
                //Create an Excel workbook instance and open it from the predefined location
                //Microsoft.Office.Interop.Excel.Workbook excelWorkBook = excelApp.Workbooks.Open(excelPath);
                //int count = 0;
                //foreach (DataTable table in dsXmlSheet.Tables)
                //{
                //    if (count != 0)
                //    {
                //        //Add a new worksheet to workbook with the Datatable name
                //        Microsoft.Office.Interop.Excel.Worksheet excelWorkSheet = excelWorkBook.Sheets.Add();
                //        excelWorkSheet.Name = table.TableName;

                //        for (int i = 1; i < table.Columns.Count + 1; i++)
                //        {
                //            excelWorkSheet.Cells[1, i] = table.Columns[i - 1].ColumnName;
                //        }

                //        for (int j = 0; j < table.Rows.Count; j++)
                //        {
                //            for (int k = 0; k < table.Columns.Count; k++)
                //            {
                //                excelWorkSheet.Cells[j + 2, k + 1] = table.Rows[j].ItemArray[k].ToString();
                //            }
                //        }
                //    }
                //    count = count + 1;
                //}

                //excelWorkBook.Save();
                //excelWorkBook.Close();
                //excelApp.Quit();
                if (importType == "Bulk")
                {
                    string result = finishImportTemplate(dsXmlSheet, excelPath);
                    return new JsonResult() { Data = result };
                }
                else if (importType == "Sub")
                {
                    SubProductsImportApiController subProducts = new SubProductsImportApiController();
                    string result = subProducts.FinishImportTemplate(dsXmlSheet, excelPath);
                    return new JsonResult() { Data = result };
                }
                else
                {
                    dtResult = BasicImportSheetToDatatableTemplate(dsXmlSheet, excelPath);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at LoadXmlFile : ", ex);
            }
            if (dtResult != null && dtResult.Tables.Count > 0)
            {
                return new JsonResult() { Data = dtResult.Tables[0].Rows.Cast<DataRow>().ToList() };
            }
            else
            {
                return new JsonResult() { Data = "Import Failed" };
            }
        }

        public string finishImportTemplate(DataSet dsSet, string excelPath)
        {
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            DataSet ds = new DataSet();
            DataSet dsGetProdCnt = new DataSet();
            int importProductCount = 0;
            int userProductCount = 0;
            int userSKUProductCount = 0;
            string allowDuplicate = dsSet.Tables[2].Rows[0][0].ToString();
            string SheetName = "Sheet1$";
            try
            {
                DataTable oattType = new DataTable();
                DataSet emptyreplace = new DataSet();

                int ItemVal = Convert.ToInt32(allowDuplicate);
                using (var conn = new SqlConnection(connectionString))
                {

                    conn.Open();
                    SQLString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
                    IC.importExcelSheetSelection(excelPath, SheetName);
                    SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
                    _DBCommand.ExecuteNonQuery();
                    objdatatable = dsSet.Tables[1];
                    SQLString = IC.CreateTable("[##IMPORTTEMP" + importTemp + "]", excelPath, SheetName, objdatatable);
                    _DBCommand = new SqlCommand(SQLString, conn);
                    _DBCommand.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(conn)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
                    };
                    bulkCopy.WriteToServer(objdatatable);

                    if (allowDuplicate == "1")
                    {
                        IC._SQLString = @"SELECT COUNT (DISTINCT Catalog_item_no) FROM  [##IMPORTTEMP" + importTemp +
                                        "] WHERE  isnull(Catalog_item_no,'') <> '' ";
                    }
                    else
                    {
                        IC._SQLString = @"SELECT COUNT (DISTINCT Catalog_item_no) FROM  [##IMPORTTEMP" + importTemp +
                                        "] WHERE PRODUCT_ID IS NULL  and isnull(Catalog_item_no,'') <> '' ";
                    }
                    dsGetProdCnt = IC.CreateDataSet();
                    var customerId = 0;
                    var skucnt = objLS.TB_PLAN
                       .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                       .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                       .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => new { x.tcptps.tps.SKU_COUNT, x.tcp.CustomerId });
                    var SKUcount = skucnt.Select(a => a).ToList();

                    if (SKUcount.Any())
                    {
                        userSKUProductCount = Convert.ToInt32(SKUcount[0].SKU_COUNT);
                        customerId = Convert.ToInt32(SKUcount[0].CustomerId);
                    }
                    if (Convert.ToInt32(dsGetProdCnt.Tables[0].Rows[0][0]) > 0)
                    {
                        importProductCount = Convert.ToInt32(dsGetProdCnt.Tables[0].Rows[0][0]);
                        var productcount = objLS.STP_LS_GET_PRODUCTS_BY_USER("PRODUCT", User.Identity.Name, "").ToList();

                        if (productcount.Any())
                        {
                            userProductCount = Convert.ToInt32(productcount[0]);
                        }

                        if ((userProductCount + importProductCount) > userSKUProductCount)
                        {

                            return "SKU Exceed~" + importTemp;
                        }
                    }
                    oattType = IC.SelectedColumnsToImportTemplate(objdatatable, dsSet.Tables[0]);
                    var cmd1 =
                        new SqlCommand(
                            "ALTER TABLE [##IMPORTTEMP" + importTemp +
                            "] ADD Family_Status nvarchar(10),FStatusUI nvarchar(10),StatusUI nvarchar(10),SFSort_Order int,Sort_Order int",
                            conn);
                    cmd1.ExecuteNonQuery();
                    var sqlstring1 =
                        new SqlCommand(
                            "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##AttributeTemp" +
                            importTemp + "]'')BEGIN DROP TABLE [##AttributeTemp" + importTemp + "] END')", conn);
                    sqlstring1.ExecuteNonQuery();
                    var cmd2 = new SqlCommand();
                    cmd2.Connection = conn;
                    SQLString = IC.CreateTableToImport("[##AttributeTemp" + importTemp + "]", oattType);
                    cmd2.CommandText = SQLString;
                    cmd2.CommandType = CommandType.Text;
                    cmd2.ExecuteNonQuery();
                    bulkCopy.DestinationTableName = "[##AttributeTemp" + importTemp + "]";
                    bulkCopy.WriteToServer(oattType);
                    var cmbpk1 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t1'') IS NOT NULL  DROP TABLE  ##t1') ", conn);
                    cmbpk1.ExecuteNonQuery();
                    var cmbpk2 = new SqlCommand("select * into ##t1  from [##AttributeTemp" + importTemp + "] where attribute_type <>0", conn);
                    cmbpk2.ExecuteNonQuery();
                    var cmbpk3 = new SqlCommand("EXEC('IF OBJECT_ID(''TEMPDB..##t2'') IS NOT NULL  DROP TABLE  ##t2') ", conn);
                    cmbpk3.ExecuteNonQuery();
                    var cmbpk4 = new SqlCommand("select * into ##t2  from [##importtemp" + importTemp + "]", conn);
                    cmbpk4.ExecuteNonQuery();
                    //-- -----------FOR REPLACING EMPTY VALUES IN TEMP TABLE -----------VINOTHKUMAR ----------FEB-2017 ------------//
                    var cmd1f1 = new SqlCommand(
                              "SELECT * FROM [##importtemp" + importTemp + "]", conn);
                    var daf1 = new SqlDataAdapter(cmd1f1);
                    daf1.Fill(emptyreplace);
                    if (emptyreplace.Tables[0].Rows.Count > 0)
                    {
                        foreach (var item1 in emptyreplace.Tables[0].Columns)
                        {
                            string column_name = item1.ToString();
                            var cmbpk41 = new SqlCommand(" EXEC ('STP_LS_IMPORT_REPLACE ''" + importTemp + "'',''" + column_name + "''')", conn);
                            cmbpk41.ExecuteNonQuery();
                        }
                    }
                    var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT ''" + importTemp + "'',''" + ItemVal + "'', " + customerId + ",''" + User.Identity.Name + "''')", conn) { CommandTimeout = 0 };
                    cmd.ExecuteNonQuery();
                    IC._SQLString = @"select * from [##LOGTEMPTABLE" + importTemp + "]";
                    ds = IC.CreateDataSet();
                    IC._SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp +
                                    "]";
                    cmd1.CommandText = IC._SQLString;
                    cmd1.CommandType = CommandType.Text;
                    cmd1.ExecuteNonQuery();
                    var picklistvaluecreation = objLS.Customer_Settings.FirstOrDefault(x => x.CustomerId == customerId);
                    //------------------Created by vinothkumar for picklist data addition of new values -------------------------------
                    if (picklistvaluecreation != null && picklistvaluecreation.ShowCustomAPPS)
                    {
                        try
                        {
                            DataSet checkPICK = new DataSet();
                            var cmd1f = new SqlCommand(
                                "SELECT * FROM [##LOGTEMPTABLE" + importTemp + "]", conn);
                            var daf = new SqlDataAdapter(cmd1f);
                            daf.Fill(checkPICK);
                            string check = string.Empty;
                            if (checkPICK.Tables[0].Rows.Count > 0)
                                foreach (DataColumn column1 in checkPICK.Tables[0].Columns)
                                {
                                    if (column1.ToString() == "STATUS")
                                    {
                                        check = "Success";

                                    }
                                }
                            if (check == "Success")
                            {
                                // if (checkPICK.Tables[0].Rows[0][0].ToString() == "UPDATED")
                                {
                                    DataSet pkname = new DataSet();
                                    SqlDataAdapter daa =
                                        new SqlDataAdapter(
                                            "select PICKLIST_NAME,ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select attribute_name  from ##t1 where attribute_type <>0) and USE_PICKLIST =1",
                                            conn);
                                    daa.Fill(pkname);
                                    if (pkname.Tables[0].Rows.Count > 0)
                                    {
                                        foreach (DataRow vr in pkname.Tables[0].Rows)
                                        {
                                            DataTable _pickListValue = new DataTable();
                                            DataSet newsheetval = new DataSet();
                                            _pickListValue.Columns.Add("Value");
                                            string picklistname = vr["PICKLIST_NAME"].ToString();
                                            DataSet dpicklistdata = new DataSet();
                                            if (!string.IsNullOrEmpty(picklistname))
                                            {
                                                var pickList = objLS.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                                                {
                                                    PICKLIST_DATA = s.PICKLIST_DATA,
                                                    PICKLIST_NAME = s.PICKLIST_NAME,
                                                    PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                                                }).FirstOrDefault(d => d.PICKLIST_NAME == picklistname);
                                                var objPick = new List<PickList>();
                                                if (pickList != null && !string.IsNullOrEmpty(pickList.PICKLIST_DATA))
                                                {
                                                    var pkxmlData = pickList.PICKLIST_DATA;

                                                    if (pkxmlData.Contains("<?xml version"))
                                                    {
                                                        // pkxmlData = pkxmlData.Remove(0, 1);
                                                        //pkxmlData = pkxmlData.Remove(pkxmlData.Length - 1);
                                                        pkxmlData = pkxmlData.Replace("\\r\\n", "");
                                                        pkxmlData = pkxmlData.Replace("\r\n", "");
                                                        pkxmlData = pkxmlData.Replace("\\\"", "\"");
                                                        pkxmlData =
                                                            pkxmlData.Replace(
                                                                "<?xml version=\"1.0\" standalone=\"yes\"?>",
                                                                "");
                                                        //PKXMLData = PKXMLData.Replace("ListItem", "Value");

                                                        XDocument objXml = XDocument.Parse(pkxmlData);

                                                        objPick =
                                                            objXml.Descendants("NewDataSet")
                                                                .Descendants("Table1")
                                                                .Select(d =>
                                                                {
                                                                    var xElement = d.Element("ListItem");
                                                                    return xElement != null
                                                                        ? new PickList
                                                                        {
                                                                            ListItem = xElement.Value
                                                                        }
                                                                        : null;
                                                                }).ToList();
                                                    }
                                                }
                                                SqlDataAdapter daa2 =
                                                        new SqlDataAdapter(" Declare @colName VARCHAR(max) " +
                                                                           "SET @colName = (select ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (select top(1)attribute_name  from ##t1 where attribute_type <>0) and USE_PICKLIST =1) " +
                                                                           " Exec('select distinct ['+ @colName+']   from [##t2]') ",
                                                            conn);
                                                daa2.Fill(newsheetval);
                                                SqlCommand cvb = new SqlCommand("DELETE TOP(1) From ##t1", conn);
                                                cvb.ExecuteNonQuery();
                                                DataSet picklistdataDS = new DataSet();
                                                DataTable newTable = new DataTable();
                                                // newTable.Columns.Add("value");
                                                for (int rowCount = 0;
                                                    rowCount < newsheetval.Tables[0].Rows.Count;
                                                    rowCount++)
                                                {
                                                    //DataRow DRow = _pickListValue.NewRow();
                                                    if (newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim() != "")
                                                    {
                                                        var picklistvalue =
                                                            newsheetval.Tables[0].Rows[rowCount][0].ToString().Trim();
                                                        PickList objlist = new PickList();
                                                        objlist.ListItem = picklistvalue;
                                                        if (!objPick.Contains(objlist))
                                                        {
                                                            objPick.Add(objlist);
                                                        }

                                                    }
                                                }
                                                var picklistdatas = objPick.DistinctBy(x => x.ListItem).ToList();
                                                foreach (var item in picklistdatas)
                                                {
                                                    if (string.IsNullOrEmpty(item.ListItem))
                                                    {
                                                        picklistdatas.Remove(item);
                                                    }
                                                    else if (item.ListItem.Trim() == "")
                                                    {
                                                        picklistdatas.Remove(item);
                                                    }
                                                    else
                                                    {
                                                        item.ListItem = item.ListItem.Trim();
                                                    }
                                                }
                                                var serializer1 = new JavaScriptSerializer();
                                                var json = serializer1.Serialize(picklistdatas);
                                                XmlDocument doc = JsonConvert.DeserializeXmlNode(
                                                    "{\"Table1\":" + json + "}", "NewDataSet");
                                                string picklistdataxml =
                                                    "<?xml version=\"1.0\" standalone=\"yes\"?>" +
                                                    doc.InnerXml;
                                                var objPicklist =
                                                    objLS.TB_PICKLIST.FirstOrDefault(
                                                        s => s.PICKLIST_NAME == picklistname);
                                                if (objPicklist != null)
                                                    objPicklist.PICKLIST_DATA = picklistdataxml;
                                                objLS.SaveChanges();
                                            }
                                        }


                                        //DataTable distinctTable =
                                        //    _pickListValue.DefaultView.ToTable( /*distinct*/ true);
                                        //picklistdataDS.Tables.Add(distinctTable);
                                        // picklistdataDS.WriteXml(Application.StartupPath + "\\tempxml.xml");
                                        //FileStream fileStream =
                                        //    new FileStream(Application.StartupPath + "\\tempxml.xml",
                                        //        FileMode.Open);
                                        //System.IO.StreamReader streamWriter = new System.IO.StreamReader(fileStream);
                                        //string dataString = streamWriter.ReadToEnd();
                                        //streamWriter.Close();
                                        //fileStream.Close();




                                    }
                                }
                            }

                        }
                        catch (Exception)
                        {

                        }
                    }
                    //----------------------end 
                    if (ds.Tables[0].Columns[0].ColumnName == "ErrorMessage")
                    {
                        return "Import Failed~" + importTemp;
                    }
                    else
                    {
                        var custmorids = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name);
                        if (custmorids.Any())
                        {
                            var firstOrDefault = custmorids.FirstOrDefault();
                            if (firstOrDefault != null)
                            {
                                int customerid = firstOrDefault.CustomerId;
                                var catalogids = objLS.TB_CATALOG.Max(x => x.CATALOG_ID);
                                var objcustomercatalogcount =
                                    objLS.Customer_Catalog.Where(
                                        x => x.CATALOG_ID == catalogids && x.CustomerId == customerid);
                                if (!objcustomercatalogcount.Any())
                                {
                                    var objcustomercatalog = new Customer_Catalog
                                    {
                                        CustomerId = customerid,
                                        CATALOG_ID = catalogids,
                                        DateCreated = DateTime.Now,
                                        DateUpdated = DateTime.Now
                                    };
                                    objLS.Customer_Catalog.Add(objcustomercatalog);
                                    objLS.SaveChanges();
                                }
                                var objCustomerRoles =
                                    objLS.aspnet_Roles.Where(x => x.CustomerId == customerid);
                                if (objCustomerRoles.Any())
                                {
                                    foreach (var objobjCustomerRole in objCustomerRoles)
                                    {
                                        var rolecatalog = objLS.Customer_Role_Catalog.Where(x => x.CATALOG_ID == catalogids && x.RoleId == objobjCustomerRole.Role_id && x.CustomerId == customerid);
                                        if (!rolecatalog.Any())
                                        {
                                            var objcustomerrolecatalog = new Customer_Role_Catalog
                                            {
                                                RoleId = objobjCustomerRole.Role_id,
                                                CustomerId = customerid,
                                                CATALOG_ID = catalogids,
                                                DateCreated = DateTime.Now,
                                                IsActive = true,
                                                DateUpdated = DateTime.Now
                                            };
                                            objLS.Customer_Role_Catalog.Add(objcustomerrolecatalog);
                                            // _dbcontext.SaveChanges();
                                        }
                                    }
                                    objLS.SaveChanges();
                                }
                            }
                        }


                        return "Import Success~" + importTemp;
                    }

                }


            }

            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : FinishImport", ex);
                return "Import Failed~" + importTemp;
            }

        }
        [System.Web.Http.HttpGet]
        public ActionResult DownloadTemplate(string fileName)
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(fileName);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(fileName));

        }

        private ActionResult File(byte[] fileBytes, string p1, string p2)
        {
            throw new NotImplementedException();
        }

        public DataSet BasicImportSheetToDatatableTemplate(DataSet dsSet, string excelPath)
        {
            string returnFile = "";
            DataTable basicImportSheet = dsSet.Tables[1];
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            SpreadsheetGear.IWorkbook wb = SpreadsheetGear.Factory.GetWorkbook(excelPath);
            DataTable dtDataTable = new DataTable();
            SpreadsheetGear.IWorksheet ws = wb.Worksheets["Sheet1"];
            dtDataTable = dsSet.Tables != null ? dsSet.Tables.Count > 0 ? dsSet.Tables[0] : null : null;

            SpreadsheetGear.IRange wc = ws.Cells;
            int iColCount = wc.ColumnCount;
            int coli = 0;
            Boolean bPass = true;
            _duplicateItemno = dsSet.Tables[2].Rows[0][0] == "1" ? true : false;
            foreach (DataRow dr in dtDataTable.Rows)
            {
                int iFindDup = 0;
                //Excel Range is not defined, so lets find the actual position of the column and set it
                dr["ExcelRange"] = "";
                for (int i = 0; i < iColCount; i++)
                {
                    if (wc.Columns[0, i].Value != null)
                    {
                        String sColName = wc.Columns[0, i].Value.ToString().Trim();
                        if (sColName == dr["ExcelColumn"].ToString())
                        {
                            coli = i;
                            String sRange = wc.Range.WorkbookSet.GetAddress(0, i);
                            if (dr["ExcelRange"].ToString() != "")
                            {
                                dr["ExcelRange"] = sRange + ", " + dr["ExcelRange"];
                            }
                            else
                            {
                                dr["ExcelRange"] = sRange;
                            }

                            //Lets check if the CatalogMapField is set in the import file
                            //if it is mapped, then attribute exists in tb_attribute and also catalogmapfield will have a value
                            //if not, the we need to create it, only if it is a non system attribute
                            if (dr["CatalogMapField"].ToString() == "")
                            {
                                //check if it is a system attribute
                                if (!(bool)dr["IsSystemField"])
                                {
                                    //Create the custom defined attribute in the database

                                    AttributeName = dr["ExcelColumn"].ToString();
                                    AttributeType = (int)dr["FieldType"];
                                    AttributeDataType = AttributeType == 4 ? "Number(13,6)" : "Text";
                                    int recordsAffected = AttributeIU();
                                    if (recordsAffected > 0)
                                    {
                                        LogTable(0, 0, "Column (Attribute) created in database or it exists in database",
                                            dr["ExcelColumn"].ToString(), 1007);
                                    }
                                    else
                                    {
                                        LogTable(0, 0, "Failed to create column (Attribute) in database",
                                            dr["ExcelColumn"].ToString(), 1008);
                                    }
                                }
                            }
                            iFindDup++;
                        }
                    }
                    else
                    {
                        iColCount = i;
                    }
                }

                //Set the range as empty if the column is defined more than once in the excel file
                //We cannot import these types of columns if they are duplicated
                if (iFindDup > 1)
                {
                    LogTable(0, coli, "Duplicate column found in Excel file", dr["ExcelColumn"].ToString(), 1009);
                    dr["ExcelRange"] = "";
                    bPass = false;
                }
                else
                {
                    LogTable(0, coli, "Column definition OK", dr["ExcelColumn"].ToString(), 1009);
                }
                //SetValue("CatalogDataImportInfo", "ExcelFilePreScreenPassed", bPass);
            }

            if (bPass)
            {
                //All set, lets the order of the columsn to be processed 
                //This is a strict system defined process
                //We can only process columns in a certain order.
                DataTable dt = new DataTable();
                dt = dsSet.Tables != null ? dsSet.Tables.Count > 0 ? dsSet.Tables[0] : null : null;
                dt = OrganizeNow(dt, true);
                _maxOrder = MaxProcessOrder;
                int stopflag = dt.Rows.Cast<DataRow>().Count(rows1 => rows1["ExcelRange"].ToString() == "");

                if (stopflag == 0)
                {
                    if (CategoryLevelCheckPassed == false)
                    {
                        LogTable(0, coli, "Incorrect Category Hierarchy", "-", 1010);
                    }
                    //continue processing only if the category levels does not have any hole 
                    if (CategoryLevelCheckPassed)
                    {
                        //Returns a single cell at the end of the current contiguous non-empty range, or at the start of the next non-empty range, starting with the top-left cell of this range.  
                        //Only the first column will be scanned to get the max rows
                        int iRows = wc.EndDown.CurrentRegion.RowCount == 1 ? ws.UsedRange.RowCount : wc.EndDown.CurrentRegion.RowCount;
                        //Send the total rows and columns to the client
                        LogTable(0, coli, "Total Record Count", "", 1011);


                        //For each row in the excel file
                        int iRow = 1;
                        int stRow = 1;
                        const string sqlstr = "";
                        _removeDs = new string[iRows];
                        for (; iRow < iRows; iRow++)
                        {

                            if (_bStopImport)
                            {
                                LogTable(0, 0, "Import Interrupted by User", "", 1001);

                                break;
                            }
                            DuplicateItemno = _duplicateItemno;
                            IC._parentCategoryId = "0";
                            IC.ParentCategoryId = "0";
                            _ParentCategoryId = "0";
                            _parentCategoryId = "0";
                            _ProductId = 0;
                            _categoryId = "0";
                            DataView dv = dt.DefaultView;
                            dv.Sort = "ExcelRange";
                            ColumnsToImportDt = dv.ToTable();
                            DsLogs = _dsLogs;
                            CurrentRow = iRow;
                            RowRange = wc;
                            _parentFamilyId = 0;
                            _FamilyId = 0;
                            _productId = 0;
                            _ProductId = 0;
                            MaxProcOrder = _maxOrder;
                            RemoveDs = _removeDs;


                            ProcessRow();

                            if (iRow == stRow)
                            {
                                _actions = 0;
                                Row = iRow;
                                TotalRow = iRows;
                                stRow = stRow + 50;
                            }
                        }
                        var custmorids = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name);
                        if (custmorids.Any())
                        {
                            var firstOrDefault = custmorids.FirstOrDefault();
                            if (firstOrDefault != null)
                            {
                                int customerid = firstOrDefault.CustomerId;
                                var catalogids = _catalogId;
                                var objcustomercatalogcount =
                                    objLS.Customer_Catalog.Where(
                                        x => x.CATALOG_ID == catalogids && x.CustomerId == customerid);
                                if (!objcustomercatalogcount.Any())
                                {
                                    var objcustomercatalog = new Customer_Catalog
                                    {
                                        CustomerId = customerid,
                                        CATALOG_ID = catalogids,
                                        DateCreated = DateTime.Now,
                                        DateUpdated = DateTime.Now
                                    };
                                    objLS.Customer_Catalog.Add(objcustomercatalog);
                                    objLS.SaveChanges();
                                }
                                var objCustomerRoles =
                                    objLS.aspnet_Roles.Where(x => x.CustomerId == customerid);
                                if (objCustomerRoles.Any())
                                {
                                    foreach (var objobjCustomerRole in objCustomerRoles)
                                    {
                                        var rolecatalog = objLS.Customer_Role_Catalog.Where(x => x.CATALOG_ID == catalogids && x.RoleId == objobjCustomerRole.Role_id && x.CustomerId == customerid);
                                        if (!rolecatalog.Any())
                                        {
                                            var objcustomerrolecatalog = new Customer_Role_Catalog
                                            {
                                                RoleId = objobjCustomerRole.Role_id,
                                                CustomerId = customerid,
                                                CATALOG_ID = catalogids,
                                                DateCreated = DateTime.Now,
                                                IsActive = true,
                                                DateUpdated = DateTime.Now
                                            };
                                            objLS.Customer_Role_Catalog.Add(objcustomerrolecatalog);
                                            // _dbcontext.SaveChanges();
                                        }
                                    }
                                    objLS.SaveChanges();
                                }
                            }
                        }
                        var cspf = new DataTable();
                        var tbProdfam = new DataTable();
                        string[] familyidscheck = { "" };
                        foreach (string t in RemoveDs.Where(t => t != null).Where(t => familyidscheck[0] != t))
                        {
                            var ss = objLS.TB_PROD_FAMILY.Where(x => x.FAMILY_ID == _FamilyId).Select(y => new { y.FAMILY_ID, y.CREATED_DATE, y.CREATED_USER, y.MODIFIED_DATE, y.MODIFIED_USER, y.PRODUCT_ID, y.PUBLISH, y.PUBLISH2CD, y.PUBLISH2PRINT, y.SORT_ORDER }).ToList();
                            tbProdfam = ToDataTable(ss);

                            familyidscheck[0] = t;
                            for (int j = 0; j < tbProdfam.Rows.Count; j++)
                            {
                                TB_PROD_FAMILY tbProdFamily =
                                    objLS.TB_PROD_FAMILY.Where(
                                        x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == _ProductId).FirstOrDefault();
                                tbProdFamily.SORT_ORDER = j + 1;
                                objLS.SaveChanges();
                            }
                        }

                        if (iRows == iRow)
                        {
                            returnFile = _sExcelFileName;
                        }
                    }
                }

            }
            //  }
            string sessionid = Guid.NewGuid().ToString();
            sessionid = sessionid.Replace("-", "");

            DataTable tbCategory = new DataTable();
            using (SqlConnection SqlCon = new SqlConnection(connectionString))
            {
                SqlCon.Open();

                string SQLString1 = null;

                SQLString1 = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SORT" +
                           sessionid + "]'')BEGIN DROP TABLE [##SORT" + sessionid + "] END')";
                SqlCommand _DBCommand11 = new SqlCommand(SQLString1, SqlCon);
                _DBCommand11.ExecuteNonQuery();
                SQLString1 = null;
                SQLString1 = CreateTable("[##SORT" + sessionid + "]", sortfam);
                SqlCommand _DBCommand12 = new SqlCommand(SQLString1, SqlCon);
                _DBCommand12.ExecuteNonQuery();
                var bulkCopy_cp = new SqlBulkCopy(SqlCon)
                {
                    DestinationTableName = "[##SORT" + sessionid + "]"
                };
                bulkCopy_cp.WriteToServer(sortfam);


                SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                int flag = Sqlcom1.ExecuteNonQuery();
                SqlCommand Sqlcom = new SqlCommand("Exec STP_LS_CATEGORYINSERTION '','','','','SORTORDER','" + sessionid + "'", SqlCon);
                Sqlcom.CommandTimeout = 0;
                int flag1 = Sqlcom.ExecuteNonQuery();
                SqlCon.Close();
            }
            wb.Close();
            basicImportSheet.Dispose();


            return _dsLogs;


        }

        public static string CreateTable(string tableName, DataTable table)
        {
            string sqlsc = "CREATE TABLE " + tableName + "(";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                if (!table.Columns[i].ColumnName.Contains("["))
                {
                    sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                }
                else
                {
                    sqlsc += "\n" + table.Columns[i].ColumnName + "";
                }
                if (table.Columns[i].DataType.ToString().Contains("System.String"))
                    sqlsc += "nvarchar(max) ";
                else
                    sqlsc += "int";
                sqlsc += ",";
            }
            return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
        }

        [System.Web.Http.HttpPost]
        public JsonResult BasicImportSheetToDatatable(string allowDuplicate, string sheetName, string excelPath, JArray model1)
        {
            _sExcelFileName = excelPath;
            string returnFile = "";
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            int importProductCount = 0;
            int userProductCount = 0;
            int userSKUProductCount = 0;
            DataTable basicImportSheet = IC.BasicImportExcelSheetSelection(excelPath, sheetName) as DataTable;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            SpreadsheetGear.IWorkbook wb = SpreadsheetGear.Factory.GetWorkbook(excelPath);
            DataSet ds = GetDataSetExcelColumns2CatalogMapping(basicImportSheet, model1);
            DataTable dtDataTable = new DataTable();
            SpreadsheetGear.IWorksheet ws = wb.Worksheets[sheetName];
            dtDataTable = ds.Tables != null ? ds.Tables.Count > 0 ? ds.Tables[0] : null : null;
            sortfam = new DataTable();
            if (sortfam.Columns.Count == 0)
            {
                sortfam.Columns.Add("FAMILY_ID");
            }
            SpreadsheetGear.IRange wc = ws.Cells;
            int iColCount = wc.ColumnCount;
            int coli = 0;
            Boolean bPass = true;
            _duplicateItemno = allowDuplicate == "1" ? true : false;
            foreach (DataRow dr in dtDataTable.Rows)
            {
                int iFindDup = 0;
                //Excel Range is not defined, so lets find the actual position of the column and set it
                dr["ExcelRange"] = "";
                for (int i = 0; i < iColCount; i++)
                {
                    if (wc.Columns[0, i].Value != null)
                    {
                        String sColName = wc.Columns[0, i].Value.ToString().Trim();
                        if (sColName == dr["ExcelColumn"].ToString())
                        {
                            coli = i;
                            String sRange = wc.Range.WorkbookSet.GetAddress(0, i);
                            if (dr["ExcelRange"].ToString() != "")
                            {
                                dr["ExcelRange"] = sRange + ", " + dr["ExcelRange"];
                            }
                            else
                            {
                                dr["ExcelRange"] = sRange;
                            }

                            //Lets check if the CatalogMapField is set in the import file
                            //if it is mapped, then attribute exists in tb_attribute and also catalogmapfield will have a value
                            //if not, the we need to create it, only if it is a non system attribute
                            if (dr["CatalogMapField"].ToString() == "")
                            {
                                //check if it is a system attribute
                                if (!(bool)dr["IsSystemField"])
                                {
                                    //Create the custom defined attribute in the database

                                    AttributeName = dr["ExcelColumn"].ToString();
                                    AttributeType = (int)dr["FieldType"];
                                    AttributeDataType = AttributeType == 4 ? "Number(13,6)" : "Text";
                                    int recordsAffected = AttributeIU();
                                    if (recordsAffected > 0)
                                    {
                                        LogTable(0, 0, "Column (Attribute) created in database or it exists in database",
                                            dr["ExcelColumn"].ToString(), 1007);
                                    }
                                    else
                                    {
                                        LogTable(0, 0, "Failed to create column (Attribute) in database",
                                            dr["ExcelColumn"].ToString(), 1008);
                                    }
                                }
                            }
                            iFindDup++;
                        }
                    }
                    else
                    {
                        iColCount = i;
                    }
                }

                //Set the range as empty if the column is defined more than once in the excel file
                //We cannot import these types of columns if they are duplicated
                if (iFindDup > 1)
                {
                    LogTable(0, coli, "Duplicate column found in Excel file", dr["ExcelColumn"].ToString(), 1009);
                    dr["ExcelRange"] = "";
                    bPass = false;
                }
                else
                {
                    LogTable(0, coli, "Column definition OK", dr["ExcelColumn"].ToString(), 1009);
                }
                //SetValue("CatalogDataImportInfo", "ExcelFilePreScreenPassed", bPass);
            }

            if (bPass)
            {
                //All set, lets the order of the columsn to be processed 
                //This is a strict system defined process
                //We can only process columns in a certain order.
                DataTable dt = new DataTable();
                dt = ds.Tables != null ? ds.Tables.Count > 0 ? ds.Tables[0] : null : null;
                dt = OrganizeNow(dt, true);
                _maxOrder = MaxProcessOrder;
                int stopflag = dt.Rows.Cast<DataRow>().Count(rows1 => rows1["ExcelRange"].ToString() == "");

                if (stopflag == 0)
                {
                    if (CategoryLevelCheckPassed == false)
                    {
                        LogTable(0, coli, "Incorrect Category Hierarchy", "-", 1010);
                    }
                    //continue processing only if the category levels does not have any hole 
                    if (CategoryLevelCheckPassed)
                    {
                        //Returns a single cell at the end of the current contiguous non-empty range, or at the start of the next non-empty range, starting with the top-left cell of this range.  
                        //Only the first column will be scanned to get the max rows
                        int iRows = wc.EndDown.CurrentRegion.RowCount == 1 ? ws.UsedRange.RowCount : wc.EndDown.CurrentRegion.RowCount;
                        //Send the total rows and columns to the client
                        LogTable(0, coli, "Total Record Count", "", 1011);


                        //For each row in the excel file
                        int iRow = 1;
                        int stRow = 1;
                        const string sqlstr = "";
                        _removeDs = new string[iRows];
                        for (; iRow < iRows; iRow++)
                        {
                            if (_bStopImport)
                            {
                                LogTable(0, 0, "Import Interrupted by User", "", 1001);
                                break;
                            }
                            DuplicateItemno = _duplicateItemno;
                            IC._parentCategoryId = "0";
                            IC.ParentCategoryId = "0";
                            _ParentCategoryId = "0";
                            _parentCategoryId = "0";
                            _ProductId = 0;
                            _categoryId = "0";
                            ColumnsToImportDt = dt;
                            DsLogs = _dsLogs;
                            CurrentRow = iRow;
                            RowRange = wc;
                            _parentFamilyId = 0;
                            _FamilyId = 0;
                            _productId = 0;
                            _ProductId = 0;
                            MaxProcOrder = _maxOrder;
                            RemoveDs = _removeDs;
                            ProcessRow();
                            if (iRow == stRow)
                            {
                                _actions = 0;
                                Row = iRow;
                                TotalRow = iRows;
                                stRow = stRow + 50;
                            }
                        }
                        var custmorids = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name);
                        if (custmorids.Any())
                        {
                            var firstOrDefault = custmorids.FirstOrDefault();
                            if (firstOrDefault != null)
                            {
                                int customerid = firstOrDefault.CustomerId;
                                var catalogids = _catalogId;
                                var objcustomercatalogcount =
                                    objLS.Customer_Catalog.Where(
                                        x => x.CATALOG_ID == catalogids && x.CustomerId == customerid);
                                if (!objcustomercatalogcount.Any() && catalogids != 0)
                                {
                                    var objcustomercatalog = new Customer_Catalog
                                    {
                                        CustomerId = customerid,
                                        CATALOG_ID = catalogids,
                                        DateCreated = DateTime.Now,
                                        DateUpdated = DateTime.Now
                                    };
                                    objLS.Customer_Catalog.Add(objcustomercatalog);
                                    objLS.SaveChanges();
                                }
                                var objCustomerRoles =
                                    objLS.aspnet_Roles.Where(x => x.CustomerId == customerid);
                                if (objCustomerRoles.Any() && catalogids != 0)
                                {
                                    foreach (var objobjCustomerRole in objCustomerRoles)
                                    {
                                        var rolecatalog = objLS.Customer_Role_Catalog.Where(x => x.CATALOG_ID == catalogids && x.RoleId == objobjCustomerRole.Role_id && x.CustomerId == customerid);
                                        if (!rolecatalog.Any())
                                        {
                                            var objcustomerrolecatalog = new Customer_Role_Catalog
                                            {
                                                RoleId = objobjCustomerRole.Role_id,
                                                CustomerId = customerid,
                                                CATALOG_ID = catalogids,
                                                DateCreated = DateTime.Now,
                                                IsActive = true,
                                                DateUpdated = DateTime.Now
                                            };
                                            objLS.Customer_Role_Catalog.Add(objcustomerrolecatalog);
                                            // _dbcontext.SaveChanges();
                                        }
                                    }
                                    objLS.SaveChanges();
                                }
                            }
                        }
                        var cspf = new DataTable();
                        var tbProdfam = new DataTable();
                        string[] familyidscheck = { "" };
                        foreach (string t in RemoveDs.Where(t => t != null).Where(t => familyidscheck[0] != t))
                        {
                            var ss = objLS.TB_PROD_FAMILY.Where(x => x.FAMILY_ID == _FamilyId).Select(y => new { y.FAMILY_ID, y.CREATED_DATE, y.CREATED_USER, y.MODIFIED_DATE, y.MODIFIED_USER, y.PRODUCT_ID, y.PUBLISH, y.PUBLISH2CD, y.PUBLISH2PRINT, y.SORT_ORDER }).ToList();
                            tbProdfam = ToDataTable(ss);
                            familyidscheck[0] = t;
                            for (int j = 0; j < tbProdfam.Rows.Count; j++)
                            {
                                TB_PROD_FAMILY tbProdFamily =
                                    objLS.TB_PROD_FAMILY.Where(
                                        x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == _ProductId).FirstOrDefault();
                                tbProdFamily.SORT_ORDER = j + 1;
                                objLS.SaveChanges();
                            }
                        }
                        if (iRows == iRow)
                        {
                            returnFile = _sExcelFileName;
                        }
                    }
                }
            }
            //  }
            string sessionid1 = Guid.NewGuid().ToString();
            sessionid1 = sessionid1.Replace("-", "");
            DataTable tbCategory = new DataTable();
            using (SqlConnection SqlCon = new SqlConnection(connectionString))
            {
                SqlCon.Open();
                string SQLString1 = null;
                SQLString1 = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##SORT" +
                           sessionid1 + "]'')BEGIN DROP TABLE [##SORT" + sessionid1 + "] END')";
                SqlCommand _DBCommand11 = new SqlCommand(SQLString1, SqlCon);
                _DBCommand11.ExecuteNonQuery();
                SQLString1 = null;
                SQLString1 = CreateTable("[##SORT" + sessionid1 + "]", sortfam);
                SqlCommand _DBCommand12 = new SqlCommand(SQLString1, SqlCon);
                _DBCommand12.ExecuteNonQuery();
                var bulkCopy_cp = new SqlBulkCopy(SqlCon)
                {
                    DestinationTableName = "[##SORT" + sessionid1 + "]"
                };
                bulkCopy_cp.WriteToServer(sortfam);
                SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                int flag = Sqlcom1.ExecuteNonQuery();
                SqlCommand Sqlcom = new SqlCommand("Exec STP_LS_CATEGORYINSERTION '','','','','SORTORDER','" + sessionid1 + "'", SqlCon);
                Sqlcom.CommandTimeout = 0;
                int flag1 = Sqlcom.ExecuteNonQuery();
                SqlCon.Close();
            }
            wb.Close();
            basicImportSheet.Dispose();
            // var s1s = _dsLogs.Tables[0].Rows.Cast<DataRow>().ToList();
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(_dsLogs.Tables[0]);
            // model.Data = JSONString;
            return new JsonResult() { Data = JSONString };
        }

        public DataTable OrganizeNow(DataTable dt, bool doCategoryLevelCheck)
        {
            //Set the column processing order
            //For all system defined columns we set the order from the ENUM CSF FieldHelper
            //For all custom attributes and extra columns, we set it as 9999999 (ie) columns should be processed after 
            //processing system columns
            foreach (DataRow dr in dt.Rows)
            {
                string sCol = dr["ExcelColumn"].ToString().ToUpper();
                int iOrd = GetEnumVal(sCol);
                dr["ColumnProcessOrder"] = iOrd;
            }
            if (doCategoryLevelCheck)
            {
                foreach (DataRow dr in dt.Select("ColumnProcessOrder=max(ColumnProcessOrder)"))
                {
                    _maxProcessOrder = Convert.ToInt32(dr["ColumnProcessOrder"]);
                }
                if (_maxProcessOrder == 2)
                {
                    _categoryLevelCheckPassed = true;
                }
                else if (_maxProcessOrder > 2)
                {
                    _categoryLevelCheckPassed = true;
                }
            }
            DataTable dtnew = dt.Clone();
            DataRow[] drs = dt.Select("", "ColumnProcessOrder ASC");
            foreach (DataRow dr in drs)
            {
                dtnew.ImportRow(dr);
            }
            return dtnew;
        }

        public static int GetEnumVal(String enumFieldName)
        {
            try
            {
                return (int)Enum.Parse(typeof(CSF), enumFieldName, true);
            }
            catch (Exception)
            {
                return 9999999;
            }
        }

        private void LogTable(int row, int col, string message, string data, int status)
        {
            try
            {
                if (_dsLogs == null)
                {
                    _dsLogs = new DataSet();
                    _dsLogs.Tables.Add("dtLog");
                    _dsLogs.Tables[0].Columns.Add("Row");
                    _dsLogs.Tables[0].Columns.Add("Column");
                    _dsLogs.Tables[0].Columns.Add("Message");
                    _dsLogs.Tables[0].Columns.Add("Data");
                    _dsLogs.Tables[0].Columns.Add("Status");
                }
                DataRow dr = _dsLogs.Tables["dtLog"].NewRow();
                dr["Row"] = row + 1;
                dr["Column"] = col + 1;
                dr["Message"] = message;
                dr["Data"] = data;
                dr["Status"] = status;
                _dsLogs.Tables["dtLog"].Rows.Add(dr);
            }
            catch
            {
            }
        }

        #region Basic Import
        //Basic Import Process
        string _AttributeName;
        public string AttributeName
        {
            get
            {
                return _AttributeName;
            }
            set
            {
                _AttributeName = value;
            }
        }
        int _AttributeType;
        public int AttributeType
        {
            get
            {
                return _AttributeType;
            }
            set
            {
                _AttributeType = value;
            }
        }
        string _AttributeDataType;
        public string AttributeDataType
        {
            get
            {
                return _AttributeDataType;
            }
            set
            {
                _AttributeDataType = value;
            }
        }

        int _maxOrder;
        string[] _removeDs;

        public string[] RemoveDs
        {
            get
            {
                return _removeDs;
            }
            set
            {
                _removeDs = value;
            }
        }
        private Boolean _bStopImport;
        public Boolean StopImport
        {
            set
            {
                _bStopImport = value;
            }
        }

        bool _duplicateItemno;
        public bool DuplicateItemno
        {
            get
            {
                return _duplicateItemno;
            }
            set
            {
                _duplicateItemno = value;
            }
        }

        public DataSet DsLogs
        {
            get
            {
                return _dsLogs;
            }
            set
            {
                _dsLogs = value;
            }
        }

        private int _maxProcOrder;
        public int MaxProcOrder
        {
            set
            {
                _maxProcOrder = value;
            }
        }

        SpreadsheetGear.IRange _rr;
        public SpreadsheetGear.IRange RowRange
        {
            set
            {
                _rr = value;
            }
        }

        private int _iRow;
        public int CurrentRow
        {
            set
            {
                _iRow = value;
            }
        }
        public bool duplicateItemno
        {
            set
            {
                _duplicateItemno = value;
            }
        }
        public JsonResult GetBasicImportSpecs(string sheetName, string excelPath)
        {

            try
            {
                DataTable dtDataTable = importSelectionColumnGrid(sheetName, excelPath);
                var dynamicColumns = new Dictionary<string, string>();
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                JsonWriter jwWriter = new JsonTextWriter(sw);

                var model = new DashBoardModel();
                using (DataTableReader dataTableReader = dtDataTable.CreateDataReader())
                {
                    jwWriter.WriteStartObject();
                    jwWriter.WritePropertyName("Data");
                    jwWriter.WriteStartArray();
                    while (dataTableReader.Read())
                    {
                        jwWriter.WriteStartObject();
                        for (int i = 0; i < dataTableReader.FieldCount; i++)
                        {
                            jwWriter.WritePropertyName(dataTableReader.GetName(i));
                            jwWriter.WriteValue(dataTableReader.GetValue(i).ToString());
                            if (!dynamicColumns.ContainsKey(dataTableReader.GetName(i)) && !string.IsNullOrEmpty(dataTableReader.GetName(i)))
                            {
                                dynamicColumns.Add(dataTableReader.GetName(i), dataTableReader.GetValue(i).ToString());
                            }
                        }
                        jwWriter.WriteEndObject();
                    }
                    jwWriter.WriteEndArray();
                    jwWriter.WritePropertyName("Columns");
                    jwWriter.WriteStartArray();
                    foreach (var key in dynamicColumns.Keys)
                    {
                        jwWriter.WriteStartObject();
                        jwWriter.WritePropertyName("title");
                        jwWriter.WriteValue(key);
                        jwWriter.WritePropertyName("field");
                        jwWriter.WriteValue(key);
                        jwWriter.WriteEndObject();
                    }
                    jwWriter.WriteEndArray();
                    jwWriter.WriteEndObject();
                }
                model.Data = sb.ToString();
                return new JsonResult() { Data = model };
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at ImportApiController : GetBasicImportSpecs", objexception);
                return null;
            }

        }

        int _fieldId;
        int _row;
        int _col;
        object _celldata;
        int _attributetype;
        int _catalogId;
        public object CellData
        {
            get
            {
                return _celldata;
            }
            set
            {
                _celldata = value;
            }
        }


        public int FieldId
        {
            get
            {
                return _fieldId;
            }
            set
            {
                _fieldId = value;
            }
        }
        public int Row
        {
            get
            {
                return _row;
            }
            set
            {
                _row = value;
            }
        }

        public int Column
        {
            get
            {
                return _col;
            }
            set
            {
                _col = value;
            }
        }
        public void SetCellDataProcessor(object cellData, int attributeType, int fieldId, int rowValue, int columnValue, DataSet dsset)
        {
            _celldata = cellData;
            _attributetype = attributeType;
            _fieldId = fieldId;
            _row = rowValue;
            _col = columnValue;
            _dsLogs = dsset;
        }
        public void SetFieldId(int fieldId)
        {
            _fieldId = fieldId;
        }

        private string _ParentCategoryName;
        public string ParentCategoryName
        {
            get
            {
                return _ParentCategoryName;
            }
            set
            {
                _ParentCategoryName = value;
            }
        }
        int _familyId;

        public string FamilyName1
        {
            get
            {
                return _familyName;
            }
            set
            {
                _familyName = value;
            }
        }

        private int _arrpoint;
        public int arrpoint
        {
            get
            {
                return _arrpoint;
            }
            set
            {
                _arrpoint = value;
            }
        }
        public void ProcessCell()
        {
            if (_fieldId >= 101 && _fieldId <= 118)
            {
                _fieldId = 101;
            }
            else if (_fieldId >= 201 && _fieldId <= 203)
            {
                _fieldId = 201;
            }
            switch (_fieldId)
            {
                case 1:
                    // var ct1 = new TradingBell.ExIm.CSDBProviderIM.Catalog { CatalogId = _catalogId };
                    if (CatalogIU() > 0)
                    {
                        _isDataValid = true;
                        _catalogId = CatalogId;
                    }
                    else
                    {

                    }
                    CatalogDescription = null;
                    CatalogName = null;
                    CatalogVersion = null;
                    break;
                case 2:

                    if (_catalogId > 0) CatalogId = _catalogId;
                    CatalogName = Convert.ToString(_celldata);
                    if (CatalogIU() > 0)
                    {
                        _isDataValid = true;
                        _catalogId = CatalogId;
                    }
                    else
                    {

                    }
                    CatalogDescription = null;
                    CatalogName = null;
                    CatalogVersion = null;
                    break;
                case 3:

                    if (_catalogId > 0) CatalogId = _catalogId;
                    CatalogDescription = _catalogDescription;
                    CatalogVersion = _catalogVersion;
                    if (CatalogExtra() > 0)
                    {
                        _isDataValid = true;
                        _catalogId = CatalogId;
                    }
                    else
                    {

                    }
                    CatalogDescription = null;
                    CatalogName = null;
                    CatalogVersion = null;
                    break;
                case 101:

                    CategoryId = _categoryId;
                    ParentCategoryId = _parentCategoryId;
                    CategoryName = _categoryName;
                    //  Catalog = { CatalogId = _catalogId }
                    CatalogId = _catalogId;

                    if (CategoryIU() > 0)
                    {
                        _isDataValid = true;
                    }
                    else
                    {

                    }
                    _parentCategoryId = CategoryId;
                    _ParentCategoryId = CategoryId;
                    IC._parentCategoryId = CategoryId;
                    //Catalog = null;
                    CategoryCustomText1 = null;
                    CategoryCustomText2 = null;
                    CategoryCustomText3 = null;
                    CategoryId = null;
                    CategoryImageFile = null;
                    CategoryImageFile2 = null;
                    CategoryImageName = null;
                    CategoryImageName2 = null;
                    CategoryImageType = null;
                    CategoryImageType2 = null;
                    CategoryImageType2 = null;
                    CategoryName = null;
                    CategoryShortDesc = null;
                    ParentCategoryId = null;
                    ParentCategoryName = null;
                    break;
                case 151:

                    CategoryId = _parentCategoryId;
                    CategoryShortDesc = _categoryShortDesc;
                    CategoryImageFile = _categoryImageFile;
                    CategoryImageFile2 = _categoryImageFile2;
                    CategoryImageName = _categoryImageName;
                    CategoryImageName2 = _categoryImageName2;
                    CategoryImageType = _categoryImageType;
                    CategoryImageType2 = _categoryImageType2;
                    CategoryCustomNum1 = _categoryCustomNum1;
                    CategoryCustomNum2 = _categoryCustomNum2;
                    CategoryCustomNum3 = _categoryCustomNum3;
                    CategoryCustomText1 = _categoryCustomText1;
                    CategoryCustomText2 = _categoryCustomText2;
                    CategoryCustomText3 = _categoryCustomText3;

                    if (UpdateCagtegoryField() > 0)
                    {
                        _isDataValid = true;
                    }
                    else
                    {

                    }
                    _parentCategoryId = CategoryId;

                    CategoryCustomText1 = null;
                    CategoryCustomText2 = null;
                    CategoryCustomText3 = null;
                    CategoryId = null;
                    CategoryImageFile = null;
                    CategoryImageFile2 = null;
                    CategoryImageName = null;
                    CategoryImageName2 = null;
                    CategoryImageType = null;
                    CategoryImageType2 = null;
                    CategoryImageType2 = null;
                    CategoryName = null;
                    CategoryShortDesc = null;
                    ParentCategoryId = null;
                    ParentCategoryName = null;
                    break;
                case 201:

                    CatalogId = _catalogId;
                    CategoryId = _ParentCategoryId;
                    FamilyId = _FamilyId;
                    FamilyName = _familyName;

                    ParentFamilyId = _parentFamilyId;
                    if (FamilyIU() > 0)
                        _isDataValid = true;
                    _parentFamilyId = FamilyId;
                    CategoryId = null;
                    FamilyName = null;
                    FootNotes = null;
                    Status = null;
                    break;
                case 206:

                    FamilyId = _parentFamilyId;
                    FootNotes = _footNotes;
                    Status = _status;

                    if (FamilyExtraFields() > 0)
                    {
                        _isDataValid = true;
                    }
                    else
                    {

                    }
                    _parentFamilyId = FamilyId;
                    CategoryId = null;
                    FamilyName = null;
                    FootNotes = null;
                    Status = null;
                    break;
                case 301:

                    CatalogId = _catalogId;
                    CategoryId = _ParentCategoryId;
                    FamilyId = _parentFamilyId;
                    ProductId = _productId;
                    _newcatitemno = _catalogItemNo;
                    CatalogItemNo = _catalogItemNo;
                    ProductSort = _productSort;
                    RemoveProduct = _removeProduct;
                    RemoveDs = _removeDs;
                    arrpoint = _row;
                    duplicateItemno = DuplicateItemno;

                    if (ProductUI() > 0)
                    {
                        _isDataValid = true;
                    }
                    else
                    {

                    }
                    _productId = ProductId;
                    _parentFamilyId = FamilyId;
                    _catalogItemNo = CatalogItemNo;
                    CatalogItemNo = null;
                    CategoryId = null;
                    _removeProduct = null;
                    break;
                case 9999999:

                    AttributeName = _AttributeName;
                    _AttributeValue = AttributeValue;
                    AttributeType = _attributetype;
                    FamilyId = _parentFamilyId;
                    ProductId = _productId;
                    CatalogId = _catalogId;
                    CatalogItemno = _catalogItemNo;
                    FamilyName = _familyName;

                    if (AttributeIU() > 0)
                    {
                        _isDataValid = true;
                    }
                    else
                    {

                    }
                    AttributeDataType = null;
                    AttributeName = null;
                    AttributeValue = null;
                    FamilyName = null;

                    break;
                case 210:

                    CatalogId = _catalogId;
                    CategoryId = _categoryId;
                    FamilyId = _parentFamilyId;
                    DoFamilyTableStructureInsertUpdate();

                    CategoryId = null;
                    break;
            }

            if (_isDataValid)
            {
                LogTable(_row, _col, "Cell processed successfully...", (string)_celldata, 1003);
                //XLSProcessEventArgs _XLSProArgs = new XLSProcessEventArgs(2, _Row, _Col, FieldHelper.GetEnumName(FieldHelper.IMPStatus.CELL_PROCESS_SUCCESS), (string)_celldata, (int)FieldHelper.IMPStatus.CELL_PROCESS_SUCCESS);
                //InvokeCPEvent(_XLSProArgs);
            }
            else
            {
                LogTable(_row, _col, "Unable to process cell... or already exists.....", (string)_celldata, 1004);
                //XLSProcessEventArgs _XLSProArgs = new XLSProcessEventArgs(2, _Row, _Col, FieldHelper.GetEnumName(FieldHelper.IMPStatus.CELL_PROCESS_FAILED), (string)_celldata,(int)FieldHelper.IMPStatus.CELL_PROCESS_FAILED);
                //InvokeCPEvent(_XLSProArgs);
            }
        }

        public int CatalogExtra()
        {
            int count = 0;
            int customer_Id = Convert.ToInt32(objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(a => a.CustomerId).FirstOrDefault());
            //count = objLS.TB_CATALOG.Count(x => x.CATALOG_ID == _CatalogId);
            count = objLS.TB_CATALOG.Join(objLS.Customer_Catalog, cc => cc.CATALOG_ID, tc => tc.CATALOG_ID, (cc, tc) => new { cc, tc }).Count(x => x.cc.CATALOG_ID == _CatalogId && x.tc.CustomerId == customer_Id);
            if (count == 1)
            {
                TB_CATALOG tbCatalog = objLS.TB_CATALOG.Where(x => x.CATALOG_ID == _CatalogId).FirstOrDefault();
                tbCatalog.VERSION = _catalogVersion;
                tbCatalog.DESCRIPTION = _catalogDescription;
                objLS.SaveChanges();
            }

            return _RecordsAffected;
        }

        public int CatalogIU()
        {
            int count;
            int customer_Id = Convert.ToInt32(objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(a => a.CustomerId).FirstOrDefault());
            if (_CatalogId > 0 & _CatalogName != "")
            {
                // count = objLS.TB_CATALOG.Count(x => x.CATALOG_ID == _CatalogId);
                count = objLS.TB_CATALOG.Join(objLS.Customer_Catalog, cc => cc.CATALOG_ID, tc => tc.CATALOG_ID, (cc, tc) => new { cc, tc }).Count(x => x.cc.CATALOG_ID == _CatalogId && x.tc.CustomerId == customer_Id);
                if (count == 1 && _CatalogId != 1)
                {
                    try
                    {
                        TB_CATALOG tbCatalog = objLS.TB_CATALOG.Where(x => x.CATALOG_ID == _CatalogId).FirstOrDefault();
                        tbCatalog.CATALOG_NAME = _CatalogName;
                        // objLS.Entry(tbCatalog).State = EntityState.Modified;
                        objLS.SaveChanges();
                        _RecordsAffected = 1;
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }
            else if (_catalogName != "")
            {
                _CatalogName = _catalogName;
                //count = objLS.TB_CATALOG.Count(x => x.CATALOG_NAME == _CatalogName);
                //_CatalogId = objLS.TB_CATALOG.Join(objLS.Customer_Catalog, cc => cc.CATALOG_ID, tc => tc.CATALOG_ID, (cc, tc) => new { cc, tc }).Where(x => x.cc.CATALOG_NAME == _CatalogName && x.tc.CustomerId == customer_Id).Select(c=> c.cc.CATALOG_ID).FirstOrDefault();
                count = objLS.TB_CATALOG.Join(objLS.Customer_Catalog, cc => cc.CATALOG_ID, tc => tc.CATALOG_ID, (cc, tc) => new { cc, tc }).Count(x => x.cc.CATALOG_NAME == _CatalogName && x.tc.CustomerId == customer_Id);
                if (count == 0)
                {
                    TB_CATALOG tbCatalog = new TB_CATALOG
                    {
                        CATALOG_NAME = _CatalogName,
                        CREATED_DATE = DateTime.Now,
                        VERSION = null,
                        DESCRIPTION = null,
                        FAMILY_FILTERS = null,
                        PRODUCT_FILTERS = null,
                        DEFAULT_FAMILY = null,
                        CREATED_USER = User.Identity.Name,
                        MODIFIED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now
                    };
                    objLS.TB_CATALOG.Add(tbCatalog);
                    objLS.SaveChanges();
                    _CatalogId = tbCatalog.CATALOG_ID;
                    _RecordsAffected = 1;
                    var objcustomercatalogcount =
                                    objLS.Customer_Catalog.Where(
                                        x => x.CATALOG_ID == _CatalogId && x.CustomerId == customer_Id);
                    if (!objcustomercatalogcount.Any())
                    {
                        var objcustomercatalog = new Customer_Catalog
                        {
                            CustomerId = customer_Id,
                            CATALOG_ID = _CatalogId,
                            DateCreated = DateTime.Now,
                            DateUpdated = DateTime.Now
                        };
                        objLS.Customer_Catalog.Add(objcustomercatalog);
                        objLS.SaveChanges();
                    }
                    int attrcount =
                        objLS.TB_CATALOG_ATTRIBUTES.Count(x => x.ATTRIBUTE_ID == 1 && x.CATALOG_ID == _CatalogId);
                    if (attrcount == 0)
                    {
                        TB_CATALOG_ATTRIBUTES tbCatalogAttributes = new TB_CATALOG_ATTRIBUTES
                        {
                            ATTRIBUTE_ID = 1,
                            CATALOG_ID = _CatalogId,
                            CREATED_DATE = DateTime.Now,
                            CREATED_USER = User.Identity.Name,
                            MODIFIED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now
                        };
                        objLS.TB_CATALOG_ATTRIBUTES.Add(tbCatalogAttributes);
                        objLS.SaveChanges();
                    }
                }
                else
                {
                    //We do not need to update the catalog with the same name.
                    //  int.TryParse(objLS.TB_CATALOG.OrderByDescending(x => x.CATALOG_NAME == _CatalogName).Select(y => y.CATALOG_ID).ToList()[0].ToString(), out _CatalogId);
                    _CatalogId =
                        Convert.ToInt32(
                            objLS.TB_CATALOG.Join(objLS.Customer_Catalog, cc => cc.CATALOG_ID, tc => tc.CATALOG_ID,
                                (cc, tc) => new { cc, tc })
                                .Where(x => x.cc.CATALOG_NAME == _CatalogName && x.tc.CustomerId == customer_Id)
                                .Select(a => a.cc.CATALOG_ID).FirstOrDefault());
                    _RecordsAffected = 1;
                }
            }
            else if (_CatalogId > 0)
            {
                count = objLS.TB_CATALOG.Count(x => x.CATALOG_ID == _CatalogId);
                return count;
            }



            return _RecordsAffected;
        }

        private string _catalogName;
        public string CatalogName
        {
            get
            {
                return _catalogName;
            }
            set
            {
                _catalogName = value;
            }
        }
        public void ProcessRow()
        {
            //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.PROCESSING_ROW), "-",(int)FieldHelper.IMPStatus.PROCESSING_ROW);
            //InvokeRPEvent(XLSProAgrs);

            if (_maxProcOrder > 0)
            {
                if (ProcessCatalogCols())
                {
                    ProcessCatalogExtraFields(3);
                    if (_maxProcOrder > 100)
                        if (ProcessCategoryCols(101))
                        {
                            if (_maxProcOrder > 102)
                                if (ProcessCategoryExtraCols(103))
                                {
                                    ProcessCategoryExtraFields(151);
                                    if (_maxProcOrder > 200)
                                        if (ProcessFamilyCols(201))
                                        {
                                            ProcessFamilyFamilyExtraFields(206);
                                            if (_maxProcOrder > 300)
                                                if (ProcessProductCols(301))
                                                {
                                                    if (_maxProcOrder > 400)
                                                        if (ProcessProductAttributeCols(9999999))
                                                        {
                                                        }
                                                }
                                        }
                                }
                        }
                }
            }
            if (_maxProcOrder > 201)
            {
                ProcessFamilyStructure();
            }

            AttributeName = null;
            AttributeValue = null;
            CatalogDescription = null;
            CatalogItemNo = null;
            CatalogName = null;
            CatalogVersion = null;
            CategoryCustomText1 = null;
            CategoryCustomText2 = null;
            CategoryCustomText3 = null;
            CategoryId = null;
            CategoryImageFile = null;
            CategoryImageFile2 = null;
            CategoryImageName = null;
            CategoryImageName2 = null;
            CategoryImageType = null;
            CategoryImageType2 = null;
            CategoryName = null;
            CategoryShortDesc = null;
            FamilyName = null;
            FootNotes = null;
            ParentCategoryId = null;
            Status = null;
            CellData = null;
        }

        private bool _IsValid;
        private string GetCellData(int row, int col)
        {
            object objval = _rr[row, col].Value ?? "";
            return Convert.ToString(objval);
        }
        private int _currentCatalogId;
        private bool _isDataValid;
        public bool IsValid
        {
            get
            {
                return _isDataValid;
            }
            set
            {
                _isDataValid = value;
            }
        }
        private bool ProcessCatalogCols()
        {
            string celldata;
            int iCol;
            _IsValid = false;
            //Catalog_Id
            DataRow[] dr = _settingsDt.Select("ColumnProcessOrder = 1");
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    try
                    {
                        int nTemp = Convert.ToInt32(dr[0]["FieldType"]);
                        SetCellDataProcessor(celldata, nTemp, 1, _iRow, iCol, _dsLogs);

                    }
                    catch (Exception ex)
                    {
                        int fieldTT = 0;
                        int.TryParse(dr[0]["FieldType"].ToString(), out fieldTT);
                        SetCellDataProcessor(celldata, fieldTT, 1, _iRow, iCol, _dsLogs);
                    }
                    if (celldata == "") celldata = "0";
                    try
                    {
                        CatalogId = Convert.ToInt32(celldata);
                        _currentCatalogId = Convert.ToInt32(celldata);
                    }
                    catch (Exception)
                    {
                        LogTable(_iRow, 0, "rowValue skipped, Catalog ID not found", celldata, 1012);
                        return false;
                    }

                }
            }

            //Catalog_Name
            dr = _settingsDt.Select("ColumnProcessOrder = '2'");
            if (dr.Length == 0)
            {
                dr = _settingsDt.Select("ColumnProcessOrder = 2");
            }
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    //CatalogId is supplied
                    if (_currentCatalogId > 0)
                    {
                        try
                        {
                            SetCellDataProcessor(celldata, (int)dr[0]["FieldType"], 2, _iRow, iCol, _dsLogs);
                        }
                        catch
                        {
                            int fieldTT = 0;
                            int.TryParse(dr[0]["FieldType"].ToString(), out fieldTT);
                            SetCellDataProcessor(celldata, fieldTT, 2, _iRow, iCol, _dsLogs);
                        }
                        CatalogId = _currentCatalogId;
                        ProcessCell();
                        if (IsValid)
                        {
                            //Catalog Id and Name Combination is OK
                            //Just move on
                        }
                        else
                        {
                            _currentCatalogId = 1;
                            LogTable(_iRow, 0, "rowValue skipped, Catatalog Name & Id combination not found", celldata, 1013);
                            //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.INVALID_CATALOG_ID_COMBINATION_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.INVALID_CATALOG_ID_COMBINATION_SKIP_ROW);
                            //InvokeRPEvent(XLSProAgrs);
                            return false;
                        }
                    }
                    //CatalogId not supplied - just name is supplied
                    else
                    {
                        try
                        {
                            SetCellDataProcessor(celldata, (int)dr[0]["FieldType"], 2, _iRow, iCol, _dsLogs);
                        }
                        catch
                        {
                            int fieldTT = 0;
                            int.TryParse(dr[0]["FieldType"].ToString(), out fieldTT);
                            SetCellDataProcessor(celldata, (int)fieldTT, 2, _iRow, iCol, _dsLogs);
                        }
                        ProcessCell();
                        if (IsValid)
                        {
                            //Since we do not know the catalog id, we need to get it from the db
                            _currentCatalogId = CatalogId;
                        }
                        else
                        {
                            _currentCatalogId = 1;
                            LogTable(_iRow, 0, "Unable to create Catalog. Skipping rowValue. It is possible, that you have an invalid name or special characters in the catalog name", celldata, 1016);
                            //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.FAILED_TO_CREATE_CATALOG_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.FAILED_TO_CREATE_CATALOG_SKIP_ROW);
                            //InvokeRPEvent(XLSProAgrs);
                            // return false;
                        }
                    }
                }
            }
            //We could have reached here, even if the catalog_id or name is provided. which is perfectly ok and return true to process other columns. Reason is this could be just an update of the product information.
            if (_currentCatalogId == 0)
            {
                //No catalog name or id is supplied, so set it to master catalog = 1
                _currentCatalogId = 1;
            }
            return true;
        }

        private string _catalogVersion;
        public string CatalogVersion
        {
            get
            {
                return _catalogVersion;
            }
            set
            {
                _catalogVersion = value;
            }
        }

        private string _catalogDescription;
        public string CatalogDescription
        {
            get
            {
                return _catalogDescription;
            }
            set
            {
                _catalogDescription = value;
            }
        }
        private void ProcessCatalogExtraFields(int extraFieldId)
        {
            IsValid = false;
            string celldata = "";
            int iCol = 0;
            DataRow[] dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(extraFieldId));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CatalogVersion = celldata;
                    //else
                    //{
                    //    XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW);
                    //    InvokeRPEvent(XLSProAgrs);
                    //    return false;
                    //}
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(extraFieldId + 1));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CatalogDescription = celldata;
                    //else
                    //{
                    //    XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW);
                    //    InvokeRPEvent(XLSProAgrs);
                    //    //return false;
                    //}
                }
            }
            if (CatalogVersion != "" || CatalogDescription != "")
            {
                SetCellDataProcessor(celldata, extraFieldId, (extraFieldId), _iRow, iCol, _dsLogs);
                ProcessCell();
                if (!IsValid)
                {
                    _currentCatalogId = 1;
                    //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.FAILED_TO_CREATE_CATALOG_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.FAILED_TO_CREATE_CATALOG_SKIP_ROW);
                    //InvokeRPEvent(XLSProAgrs);
                    // return false;
                }

            }

        }

        private string _categoryId;
        public string CategoryId
        {
            get
            {
                return _categoryId;
            }
            set
            {
                _categoryId = value;
            }
        }

        private string _categoryName;
        public string CategoryName
        {
            get
            {
                return _categoryName;
            }
            set
            {
                _categoryName = value;
            }
        }
        public string ParentCategoryId
        {
            get
            {
                return _parentCategoryId;
            }
            set
            {
                _parentCategoryId = value;
            }
        }
        private bool ProcessCategoryCols(int categoryColumnId)
        {
            IsValid = false;
            string celldata = "";
            int iCol = 0;
            //Start processing category columns with the assumption of parent exists

            DataRow[] dr = _settingsDt.Select("ColumnProcessOrder = '" + Convert.ToString(categoryColumnId) + "'");
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    string celldata1 = GetCellData(_iRow, iCol);
                    if (celldata1 != "") CategoryId = celldata1;
                    else
                    {
                        LogTable(_iRow, 0, "Category ID cannot be empty", "-", 1017);
                        //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW);
                        //InvokeRPEvent(XLSProAgrs);
                        //return false;
                    }
                }
            }

            //Category Name
            try
            {
                dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryColumnId + 1));
            }
            catch (Exception ex)
            {
                dr = _settingsDt.Select("ColumnProcessOrder = '" + Convert.ToString(categoryColumnId + 1) + "'");
            }
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CategoryName = celldata;
                    else
                    {
                        LogTable(_iRow, 0, "Category Name cannot be empty", "-", 1018);
                        //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_CATEGORY_NAME_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_CATEGORY_NAME_SKIP_ROW);
                        //InvokeRPEvent(XLSProAgrs);
                        // return false;
                    }
                }
            }
            //Process cell only if there is something in the category id or name, if not move on. There may be other columns such as family etc

            //very first time for 101, we always assume it to be 0 as the parent category id, set at declaration
            ParentCategoryId = IC._parentCategoryId;
            CatalogId = _currentCatalogId;

            if (celldata != "")
            {
                if (dr.Length > 0)
                {
                    try
                    {
                        SetCellDataProcessor(celldata, (int)dr[0]["FieldType"], categoryColumnId, _iRow, iCol, _dsLogs);
                    }
                    catch
                    {
                        int fieldTT = 0;
                        int.TryParse(dr[0]["FieldType"].ToString(), out fieldTT);
                        SetCellDataProcessor(celldata, fieldTT, categoryColumnId, _iRow, iCol, _dsLogs);
                    }
                }
                ProcessCell();
            }


            if (IsValid)
            {
                _currentCategoryId = _ParentCategoryId;
                _parentCategoryId = _ParentCategoryId;
            }
            else
            {
                if (!string.IsNullOrEmpty(CategoryId) && !string.IsNullOrEmpty(CategoryName))
                    LogTable(_iRow, 0,
                             "Unable to create or update category. Skipping rowValue. It is possible, that you have an invalid name or special characters in the cateogry id or name or already exists",
                             "-", 1019);
                //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.FAILED_TO_CREATE_CATEGORY_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.FAILED_TO_CREATE_CATEGORY_SKIP_ROW);
                //InvokeRPEvent(XLSProAgrs);
                //return false;
            }
            return true;
        }

        private bool ProcessCategoryExtraCols(int categoryColumnId)
        {
            for (; categoryColumnId <= 120;)
            {
                IsValid = false;
                string celldata = "";
                CategoryName = "";
                // _ParentCategoryId = "";
                //_CurrentCategoryId = "";
                CategoryId = "";
                int iCol = 0;
                DataRow[] dr = _settingsDt.Select("ColumnProcessOrder = '" + Convert.ToString(categoryColumnId) + "'");
                if (dr.Length > 0)
                {
                    if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                    {
                        iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                        celldata = GetCellData(_iRow, iCol);
                        if (celldata != "") CategoryId = celldata;
                        else
                        {
                            LogTable(_iRow, 0, "Category ID cannot be empty", "-", 1017);
                            // XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW);
                            // InvokeRPEvent(XLSProAgrs);
                            //return false;
                        }
                    }
                }
                dr = _settingsDt.Select("ColumnProcessOrder = '" + Convert.ToString(categoryColumnId + 1) + "'");
                if (dr.Length > 0)
                {
                    if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                    {
                        iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                        celldata = GetCellData(_iRow, iCol);
                        if (celldata != "") CategoryName = celldata;
                        else
                        {
                            LogTable(_iRow, 0, "Category Name cannot be empty", "-", 1018);
                            //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_CATEGORY_NAME_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_CATEGORY_NAME_SKIP_ROW);
                            // InvokeRPEvent(XLSProAgrs);
                            //return false;
                        }
                    }
                }
                DataRow[] drs = _settingsDt.Select("ColumnProcessOrder = '" + Convert.ToString(categoryColumnId - 2) + "'");
                if (drs.Length > 0)
                {
                    if (_rr[drs[0]["ExcelRange"].ToString()].Cells.Count != 0)
                    {
                        iCol = _rr[drs[0]["ExcelRange"].ToString()].Column;
                        _parentCategoryId = GetCellData(_iRow, iCol) == "" ? _ParentCategoryId : _parentCategoryId;
                        if (_parentCategoryId != "") ParentCategoryId = _parentCategoryId;
                        else
                        {
                            LogTable(_iRow, 0, "Category ID cannot be empty", "-", 1017);
                            // XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW);
                            // InvokeRPEvent(XLSProAgrs);
                            //return false;
                        }
                    }
                }
                //Process cell only if there is something in the category id or name, if not move on. There may be other columns such as family etc



                //very first time for 101, we always assume it to be 0 as the parent category id, set at declaration
                CatalogId = _currentCatalogId;

                if (celldata != "" && _parentCategoryId != "0" && _parentCategoryId != "")
                {
                    if (dr.Length > 0)
                    {
                        try
                        {
                            SetCellDataProcessor(celldata, (int)dr[0]["FieldType"], categoryColumnId, _iRow, iCol, _dsLogs);
                        }
                        catch
                        {
                            int fieldTT = 0;
                            int.TryParse(dr[0]["FieldType"].ToString(), out fieldTT);
                            SetCellDataProcessor(celldata, fieldTT, categoryColumnId, _iRow, iCol, _dsLogs);
                        }
                    }

                    ProcessCell();

                    if (IsValid)
                    {
                        _currentCategoryId = _ParentCategoryId;
                        _parentCategoryId = _ParentCategoryId;
                    }
                    else
                    {
                        LogTable(_iRow, 0, "Unable to create or update category. Skipping rowValue. It is possible, that you have an invalid name or special characters in the cateogry id or name", "-", 1019);
                        // XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.FAILED_TO_CREATE_CATEGORY_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.FAILED_TO_CREATE_CATEGORY_SKIP_ROW);
                        //InvokeRPEvent(XLSProAgrs);
                        return false;
                    }
                }
                else
                {
                    return true;
                }
                categoryColumnId += 2;
            }
            return true;
        }


        private string _categoryShortDesc;
        public string CategoryShortDesc
        {
            get
            {
                return _categoryShortDesc;
            }
            set
            {
                _categoryShortDesc = value;
            }
        }
        private string _categoryImageFile;
        public string CategoryImageFile
        {
            get
            {
                return _categoryImageFile;
            }
            set
            {
                _categoryImageFile = value;
            }
        }
        private string _categoryImageFile2;
        public string CategoryImageFile2
        {
            get
            {
                return _categoryImageFile2;
            }
            set
            {
                _categoryImageFile2 = value;
            }
        }
        private string _categoryImageName;
        public string CategoryImageName
        {
            get
            {
                return _categoryImageName;
            }
            set
            {
                _categoryImageName = value;
            }
        }
        private string _categoryImageName2;
        public string CategoryImageName2
        {
            get
            {
                return _categoryImageName2;
            }
            set
            {
                _categoryImageName2 = value;
            }
        }
        private string _categoryImageType;
        public string CategoryImageType
        {
            get
            {
                return _categoryImageType;
            }
            set
            {
                _categoryImageType = value;
            }
        }
        private string _categoryImageType2;
        public string CategoryImageType2
        {
            get
            {
                return _categoryImageType2;
            }
            set
            {
                _categoryImageType2 = value;
            }
        }

        private double _categoryCustomNum1;
        public double CategoryCustomNum1
        {
            get
            {
                return _categoryCustomNum1;
            }
            set
            {
                _categoryCustomNum1 = value;
            }
        }
        private string _categoryCustomText1;
        public string CategoryCustomText1
        {
            get
            {
                return _categoryCustomText1;
            }
            set
            {
                _categoryCustomText1 = value;
            }
        }
        private string _categoryCustomText2;
        public string CategoryCustomText2
        {
            get
            {
                return _categoryCustomText2;
            }
            set
            {
                _categoryCustomText2 = value;
            }
        }
        private string _categoryCustomText3;
        public string CategoryCustomText3
        {
            get
            {
                return _categoryCustomText3;
            }
            set
            {
                _categoryCustomText3 = value;
            }
        }
        private double _categoryCustomNum3;
        public double CategoryCustomNum3
        {
            get
            {
                return _categoryCustomNum3;
            }
            set
            {
                _categoryCustomNum3 = value;
            }
        }
        private double _categoryCustomNum2;
        public double CategoryCustomNum2
        {
            get
            {
                return _categoryCustomNum2;
            }
            set
            {
                _categoryCustomNum2 = value;
            }
        }
        private void ProcessCategoryExtraFields(int categoryFieldId)
        {
            IsValid = false;
            string celldata = "";
            int iCol = 0;
            DataRow[] dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CategoryShortDesc = celldata;
                }
            }

            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 1));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CategoryImageFile = celldata;
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 2));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CategoryImageName = celldata;
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 3));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CategoryImageType = celldata;
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 4));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CategoryImageFile2 = celldata;
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 5));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CategoryImageName2 = celldata;
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 6));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CategoryImageType2 = celldata;
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 7));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "")
                    {
                        try
                        {
                            CategoryCustomNum1 = Convert.ToDouble(celldata);
                        }
                        catch (Exception) { }
                    }
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 8));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "")
                    {
                        try
                        {
                            CategoryCustomNum2 = Convert.ToDouble(celldata);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 9));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "")
                    {
                        try
                        {
                            CategoryCustomNum3 = Convert.ToDouble(celldata);
                        }
                        catch (Exception)
                        {
                        }

                    }
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 10));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CategoryCustomText1 = celldata;
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 11));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CategoryCustomText2 = celldata;
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(categoryFieldId + 12));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CategoryCustomText3 = celldata;
                }
            }
            if (CategoryCustomNum1 > 0 || CategoryCustomNum2 > 0 || CategoryCustomNum3 > 0 || CategoryCustomText1 != "" ||
                CategoryCustomText2 != "" || CategoryCustomText3 != "" || CategoryImageFile != "" || CategoryImageFile2 != "" ||
                CategoryImageName != "" || CategoryImageName2 != "" || CategoryImageType != "" || CategoryImageType2 != "" ||
                CategoryShortDesc != "")
            {

                SetCellDataProcessor(celldata, categoryFieldId, categoryFieldId, _iRow, iCol, _dsLogs);
                ProcessCell();
            }
        }

        private int _parentFamilyId;
        public int ParentFamilyId
        {
            get
            {
                return _parentFamilyId;
            }
            set
            {
                _parentFamilyId = value;
            }
        }

        private bool ProcessFamilyCols(int familyColumnId)
        {
            for (; familyColumnId <= 203;)
            {
                IsValid = false;
                string celldata = "";
                _FamilyName1 = "";
                _FamilyId = 0;
                FamilyId = 0;
                int iCol = 0;
                DataRow[] dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(familyColumnId));
                if (dr.Length > 0)
                {
                    if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                    {
                        iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                        string celldatas = GetCellData(_iRow, iCol);
                        if (celldatas != "")
                        {
                            try
                            {
                                FamilyId = Convert.ToInt32(celldatas);
                            }
                            catch (Exception)
                            {
                                LogTable(_iRow, 0, "rowValue skipped, Family ID not found", celldatas, 1025);
                                //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.INVALID_FAMILY_ID_SKIP_ROW), _celldata, (int)FieldHelper.IMPStatus.INVALID_FAMILY_ID_SKIP_ROW);
                                //InvokeRPEvent(XLSProAgrs);
                                return false;
                            }
                            //  CDP.SetCellDataProcessor(_celldata, (int)dr[0]["FieldType"], FamilyColumnId, _iRow, _iCol);
                        }
                        else
                        {
                            LogTable(_iRow, 0, "Family ID cannot be empty", "-", 1020);
                            //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_FAMILY_ID_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_FAMILY_ID_SKIP_ROW);
                            //InvokeRPEvent(XLSProAgrs);
                            //return false;
                        }
                    }
                }
                dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(familyColumnId + 1));
                if (dr.Length > 0)
                {
                    if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                    {
                        iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                        celldata = GetCellData(_iRow, iCol);
                        if (celldata != "") _FamilyName1 = celldata;
                        else
                        {
                            LogTable(_iRow, 0, "Family Name cannot be empty", "-", 1021);
                            //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_FAMILY_NAME_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_FAMILY_NAME_SKIP_ROW);
                            // InvokeRPEvent(XLSProAgrs);
                            //return false;
                        }
                    }
                }
                if (ParentCategoryId == null || ParentCategoryId == "0" || ParentCategoryId == '0'.ToString(CultureInfo.InvariantCulture))
                {
                    DataRow[] drs = _settingsDt.Select("ColumnProcessOrder = 205");
                    if (drs.Length > 0)
                    {
                        if (_rr[drs[0]["ExcelRange"].ToString()].Cells.Count != 0)
                        {
                            iCol = _rr[drs[0]["ExcelRange"].ToString()].Column;
                            _currentCategoryId = GetCellData(_iRow, iCol);
                            if (_currentCategoryId != "")
                            {
                                CategoryId = _currentCategoryId;
                                ParentCategoryId = _currentCategoryId;
                            }
                            else
                            {
                                LogTable(_iRow, 0, "Category ID cannot be empty", "-", 1017);
                                //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_CATEGORY_ID_SKIP_ROW);
                                //InvokeRPEvent(XLSProAgrs);
                                //return false;
                            }
                        }
                    }
                }
                //Process cell only if there is something in the category id or name, if not move on. There may be other columns such as family etc

                /* if(_CurrentCategoryId =="" &&_CurrentCategoryId =="0")
                 {
                     _CurrentCategoryId = _ParentCategoryId;
                 }*/

                CatalogId = _currentCatalogId;
                CategoryId = _currentCategoryId;

                if (celldata != "" && (familyColumnId != 203 || _parentFamilyId != 0))
                {

                    if (dr.Length > 0)
                    {
                        try
                        {

                            SetCellDataProcessor(celldata, (int)dr[0]["FieldType"], familyColumnId, _iRow, iCol, _dsLogs);
                        }
                        catch
                        {
                            int fieldTT = 0;
                            int.TryParse(dr[0]["FieldType"].ToString(), out fieldTT);
                            SetCellDataProcessor(celldata, fieldTT, familyColumnId, _iRow, iCol, _dsLogs);
                        }
                    }
                    ProcessCell();
                }
                if (IsValid)
                {
                    _parentFamilyId = ParentFamilyId;
                }
                else
                {
                    if (FamilyId != 0)
                        ParentFamilyId = FamilyId;
                    if (FamilyId > 0 && !string.IsNullOrEmpty(_FamilyName1))

                        LogTable(_iRow, 0, "Unable to create or update Family. Skipping rowValue. It is possible, that you have an invalid name or special characters in the Family id or name", "-", 1024);
                    //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.FAILED_TO_CREATE_Family_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.FAILED_TO_CREATE_Family_SKIP_ROW);
                    //  InvokeRPEvent(XLSProAgrs);
                    // return false;
                }






                familyColumnId += 2;
            }
            return true;
        }

        private string _footNotes;
        public string FootNotes
        {
            get
            {
                return _footNotes;
            }
            set
            {
                _footNotes = value;
            }
        }
        public int FamilySort { get; set; }
        private string _status;
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }
        private bool ProcessFamilyFamilyExtraFields(int familyExtraId)
        {
            string celldata = "";
            IsValid = false;
            int iCol = 0;
            DataRow[] dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(familyExtraId));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") FootNotes = celldata;
                }
            }

            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(familyExtraId + 1));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") Status = celldata;
                }
            }
            dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(familyExtraId + 4));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "")
                    {
                        try
                        {
                            FamilySort = Convert.ToInt32(celldata);
                        }
                        catch (Exception)
                        {
                            LogTable(_iRow, 0, "Family sort not found", celldata, 1027);
                            //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.INVALID_FAMILY_SORT_SKIP_ROW), _celldata, (int)FieldHelper.IMPStatus.INVALID_FAMILY_SORT_SKIP_ROW);
                            //InvokeRPEvent(XLSProAgrs);
                        }
                    }
                }
            }
            if (FootNotes != "" || Status != "" || FamilySort != 0)
            {
                SetCellDataProcessor(celldata, familyExtraId, familyExtraId, _iRow, iCol, _dsLogs);
                ProcessCell();
            }

            return true;
        }

        private int _productId;
        public int ProductId
        {
            get
            {
                return _productId;
            }
            set
            {
                _productId = value;
            }
        }

        private string _catalogItemNo;
        public string CatalogItemNo
        {
            get
            {
                return _catalogItemNo;
            }
            set
            {
                _catalogItemNo = value;
            }
        }

        private int _productSort;
        public int ProductSort
        {
            get
            {
                return _productSort;
            }
            set
            {
                _productSort = value;
            }
        }

        private string _removeProduct;
        public string RemoveProduct
        {
            get
            {
                return _removeProduct;
            }
            set
            {
                _removeProduct = value;
            }
        }
        private bool ProcessProductCols(int productColumnId)
        {
            string celldata = "";
            IsValid = false;
            int iCol = 0;
            DataRow[] dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(productColumnId));
            if (dr.Length > 0)
            {
                if (_rr[dr[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[dr[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "")
                    {
                        try
                        {
                            ProductId = Convert.ToInt32(celldata);
                        }
                        catch (Exception)
                        {
                            LogTable(_iRow, 0, "rowValue skipped, Product ID not found", celldata, 1026);
                            //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.INVALID_PRODUCT_ID_SKIP_ROW), _celldata, (int)FieldHelper.IMPStatus.INVALID_PRODUCT_ID_SKIP_ROW);
                            //InvokeRPEvent(XLSProAgrs);
                            return false;
                        }
                    }
                    else
                    {
                        LogTable(_iRow, 0, "Product ID cannot be empty", "-", 1024);
                        //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_PRODUCT_ID_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_PRODUCT_ID_SKIP_ROW);
                        //InvokeRPEvent(XLSProAgrs);
                        //return false;
                    }
                }
            }
            DataRow[] drc = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(productColumnId + 1));
            if (drc.Length > 0)
            {
                if (_rr[drc[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[drc[0]["ExcelRange"].ToString()].Column;
                    celldata = GetCellData(_iRow, iCol);
                    if (celldata != "") CatalogItemNo = celldata;
                    else
                    {
                        LogTable(_iRow, 0, "Catalog Item No cannot be empty", "-", 1024);
                        //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.EMPTY_CATALOG_ITEM_NO_SKIP_ROW), "-", (int)FieldHelper.IMPStatus.EMPTY_CATALOG_ITEM_NO_SKIP_ROW);
                        //InvokeRPEvent(XLSProAgrs);
                        // return false;
                    }
                }
            }
            DataRow[] drS = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(productColumnId + 2));
            if (drS.Length > 0)
            {
                if (_rr[drS[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[drS[0]["ExcelRange"].ToString()].Column;
                    string celldatasort = GetCellData(_iRow, iCol);
                    if (celldata != "")
                        try
                        {
                            ProductSort = Convert.ToInt32(celldatasort);
                        }
                        catch (Exception)
                        {
                            LogTable(_iRow, 0, "Product sort not found", "-", 1029);
                            //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, _iRow, 0, FieldHelper.GetEnumName(FieldHelper.IMPStatus.INVALID_PRODUCT_SORT_SKIP_ROW), _celldatasort, (int)FieldHelper.IMPStatus.INVALID_PRODUCT_SORT_SKIP_ROW);
                            //InvokeRPEvent(XLSProAgrs);
                        }
                }
            }

            DataRow[] drt = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(productColumnId + 3));
            if (drt.Length > 0)
            {
                if (_rr[drt[0]["ExcelRange"].ToString()].Cells.Count != 0)
                {
                    iCol = _rr[drt[0]["ExcelRange"].ToString()].Column;
                    string celldataremove = GetCellData(_iRow, iCol);
                    if (celldata != "")
                    {
                        RemoveProduct = celldataremove;
                    }

                }
            }
            CatalogId = _currentCatalogId;
            CategoryId = _currentCategoryId;
            //CDP.FamilyId = _CurrentFamilyId;
            RemoveDs = _removeDs;
            if (celldata != "")
            {

                if (dr.Length > 0)
                {
                    int fieldTT = 0;
                    int.TryParse(dr[0]["FieldType"].ToString(), out fieldTT);
                    SetCellDataProcessor(celldata, fieldTT, productColumnId, _iRow, iCol, _dsLogs);
                }
                if (drc.Length > 0)
                {
                    int fieldTT = 0;
                    int.TryParse(drc[0]["FieldType"].ToString(), out fieldTT);
                    SetCellDataProcessor(celldata, fieldTT, productColumnId, _iRow, iCol, _dsLogs);
                }

                ProcessCell();
            }

            return true;
        }

        private bool ProcessProductAttributeCols(int attributeColumnId)
        {
            DataRow[] dr = _settingsDt.Select("ColumnProcessOrder = " + Convert.ToString(attributeColumnId));
            foreach (DataRow t in dr)
            {
                string celldata = "";
                IsValid = false;
                int iCol = 0;

                if (dr.Length > 0)
                {
                    if (_rr[t["ExcelRange"].ToString()].Cells.Count != 0)
                    {
                        iCol = _rr[t["ExcelRange"].ToString()].Column;
                        celldata = GetCellData(_iRow, iCol);
                        AttributeValue = celldata;
                        AttributeName = t[0].ToString();
                        AttributeType = Convert.ToInt32(t["FieldType"]);

                    }
                }


                if (dr.Length > 0)
                {
                    try
                    {
                        SetCellDataProcessor(celldata, (int)t["FieldType"], attributeColumnId, _iRow, iCol, _dsLogs);
                    }
                    catch
                    {
                        int fieldTT = 0;
                        int.TryParse(t["FieldType"].ToString(), out fieldTT);
                        SetCellDataProcessor(celldata, fieldTT, attributeColumnId, _iRow, iCol, _dsLogs);
                    }
                }
                ProcessCell();
            }
            return true;

        }
        private void ProcessFamilyStructure()
        {
            SetFieldId(210);
            ProcessCell();
        }
        int _SortOrder = 1;
        public int CategoryIU()
        {
            //No ids provided, we have to check the combination in the db to get the id, if it exists
            if (_categoryId == "" && _ParentCategoryId == "")
            {
                if (_categoryName != "" && _ParentCategoryName != "")
                {
                    DataTable tbCategory = new DataTable();
                    var ss = objLS.TB_CATEGORY.Join(objLS.TB_CATEGORY, tc => tc.CATEGORY_ID, tc1 => tc1.PARENT_CATEGORY,
                         (tc, tc1) => new { tc, tc1 })
                         .Union(objLS.TB_CATEGORY.Join(objLS.TB_CATEGORY, tc => tc.CATEGORY_ID,
                             tc1 => tc1.PARENT_CATEGORY, (tc, tc1) => new { tc, tc1 })).ToList();

                    tbCategory = ToDataTable(ss);
                    DataRow drparent = tbCategory.Rows[0];
                    DataRow drchild = tbCategory.Rows[1];
                    _ParentCategoryId = drparent["CATEGORY_ID"].ToString();
                    _categoryId = drchild["CATEGORY_ID"].ToString();

                    if (_categoryId == "" || _ParentCategoryId == "")
                    {
                        _RecordsAffected = 0;
                        return _RecordsAffected;
                        // throw new TradingBell.ExIm.CSUtil.CSException("Category id cannot be found");
                    }
                }
                else
                {
                    _RecordsAffected = 0;
                    return _RecordsAffected;
                    //throw new TradingBell.ExIm.CSUtil.CSException("Category names cannot be empty");
                }
            }


            if (_categoryName != "")
            {
                _RecordsAffected = DoCategoryInsertUpdate();
            }
            else
            {
                _RecordsAffected = 0;
                return _RecordsAffected;
                //throw new TradingBell.ExIm.CSUtil.CSException("Category name cannot be empty");
            }

            if (_RecordsAffected > 0)
            {
                //lets insert or update the catalog sections only when catalog object is set
                if (CatalogId > 0)
                {
                    //check first
                    int count = objLS.TB_CATALOG_SECTIONS.Count(x => x.CATALOG_ID == _catalogId && x.CATEGORY_ID == _categoryId);
                    if (count == 0)
                    {

                        try
                        {
                            int maxsort;
                            int.TryParse(
                                objLS.TB_CATALOG_SECTIONS.OrderByDescending(
                                    z => z.SORT_ORDER)
                                    .Where(
                                        x =>
                                            x.CATALOG_ID == _catalogId)
                                    .Select(y => y.SORT_ORDER)
                                    .FirstOrDefault()
                                    .ToString(), out maxsort);
                            _SortOrder = maxsort + 1;
                        }
                        catch (Exception) { }
                        //insert the catalog section record and set the sort order as 0 by default
                        DataTable tbCategory = new DataTable();
                        try
                        {

                            using (SqlConnection SqlCon = new SqlConnection(connectionString))
                            {
                                SqlCon.Open();
                                SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                                int flag = Sqlcom1.ExecuteNonQuery();
                                SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_CATALOG_SECTIONS"
                         + "(CATALOG_ID, CATEGORY_ID, SORT_ORDER)"
        + "VALUES        (" + _catalogId + ",'" + _categoryId + "', " + _SortOrder + ");"
        + "SELECT CATALOG_ID, CATEGORY_ID, SORT_ORDER, CREATED_USER, CREATED_DATE, MODIFIED_USER, MODIFIED_DATE FROM TB_CATALOG_SECTIONS WHERE (CATALOG_ID = " + _catalogId + ") AND (CATEGORY_ID = '" + _categoryId + "')", SqlCon);
                                SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                da.Fill(tbCategory);
                                //int flag = Sqlcom.ExecuteNonQuery();
                                SqlCon.Close();
                            }
                            //TB_CATALOG_SECTIONS tbCatalogSections = new TB_CATALOG_SECTIONS
                            //{
                            //    CATALOG_ID = _catalogId,
                            //    CATEGORY_ID = _categoryId,
                            //    SORT_ORDER = _SortOrder,
                            //    CREATED_USER = User.Identity.Name,
                            //    CREATED_DATE = DateTime.Now,
                            //    MODIFIED_USER = User.Identity.Name,
                            //    MODIFIED_DATE = DateTime.Now
                            //};
                            //objLS.TB_CATALOG_SECTIONS.Add(tbCatalogSections);
                            //objLS.SaveChanges();
                        }
                        catch (Exception ex)
                        {

                        }

                        _RecordsAffected = objLS.TB_CATALOG_SECTIONS.Count(
                            x =>
                                x.CATALOG_ID == _catalogId && x.CATEGORY_ID == _categoryId && x.SORT_ORDER == _SortOrder);
                    }
                }
            }

            return _RecordsAffected;
        }
        private int DoCategoryInsertUpdate()
        {
            string _categorysort = string.Empty;
            _ParentCategoryId = ParentCategoryId;

            if (_ParentCategoryId != "0" & _ParentCategoryId != "" && _ParentCategoryId != null)
            {
                //Check if parent category exists in the database

                int count = objLS.TB_CATALOG_SECTIONS.Count(x => x.CATALOG_ID == _catalogId && x.CATEGORY_ID == ParentCategoryId);
                if (count > 0)
                {
                    var customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                    if (_categoryId == null || _categoryId == '0'.ToString() || _categoryId == "")
                    {
                        var category =
                            objLS.TB_CATEGORY.Where(
                                x => x.CATEGORY_NAME == _categoryName && x.PARENT_CATEGORY == _ParentCategoryId && x.CUSTOMER_ID == customerid.CustomerId).Select(y => y.CATEGORY_ID).ToList();
                        if (category.Any())
                        {
                            _categoryId = category[0];
                        }
                    }

                    if (string.IsNullOrEmpty(_categoryId) || _categoryId == "0")
                    {
                        count = 1;
                        while (count == 1)
                        {
                            Random rnd = new Random();
                            _categorysort = rnd.Next().ToString();
                            count = objLS.TB_CATEGORY.Count(x => x.CATEGORY_SHORT == _categorysort && x.CUSTOMER_ID == customerid.CustomerId);
                        }

                    }
                    else
                    {
                        var catsort = objLS.TB_CATEGORY.Where(x => x.CATEGORY_ID == _categoryId && x.CUSTOMER_ID == customerid.CustomerId).Select(y => y.CATEGORY_SHORT).FirstOrDefault();
                        if (catsort != null)
                        {
                            if (catsort.Any())
                            {
                                _categorysort = catsort.ToString();
                            }
                        }
                        else
                        {
                            _categorysort = _categoryId;
                        }
                        _categoryId = objLS.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == _categorysort && x.CUSTOMER_ID == customerid.CustomerId).Select(y => y.CATEGORY_ID).FirstOrDefault();
                    }
                    count = objLS.TB_CATEGORY.Count(x => x.CATEGORY_SHORT == _categorysort && x.CUSTOMER_ID == customerid.CustomerId);
                    if (count > 0)
                    {
                        if (_categoryName.Contains("'"))
                        {
                            _categoryName = _categoryName.Replace("'", "''");
                        }
                        using (SqlConnection SqlCon = new SqlConnection(connectionString))
                        {
                            SqlCon.Open();
                            SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                            int flag1 = Sqlcom1.ExecuteNonQuery();
                            SqlCommand Sqlcom = new SqlCommand("UPDATE       TB_CATEGORY\r\nSET                CATEGORY_NAME = '" + _categoryName + "', PARE" +
                    "NT_CATEGORY ='" + _ParentCategoryId + "'\r\nWHERE        (CATEGORY_ID ='" + _categoryId + "')", SqlCon);
                            //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                            //da.Fill(tbCategory);
                            int flag = Sqlcom.ExecuteNonQuery();
                            SqlCon.Close();
                        }
                        //var tbCategory = objLS.TB_CATEGORY.FirstOrDefault(x => x.CATEGORY_SHORT == _categorysort);
                        //if (tbCategory != null)
                        //{
                        //    tbCategory.CATEGORY_NAME = _categoryName;
                        //    tbCategory.PARENT_CATEGORY = _ParentCategoryId;
                        //}
                        //objLS.SaveChanges();
                        //_categoryId = tbCategory.CATEGORY_ID;
                        _RecordsAffected = objLS.TB_CATEGORY.Count(x => x.CATEGORY_ID == _categoryId);
                    }
                    else
                    {
                        if (_categoryName.Contains("'"))
                        {
                            _categoryName = _categoryName.Replace("'", "''");
                        }
                        customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                        DataTable tbCategory = new DataTable();
                        using (SqlConnection SqlCon = new SqlConnection(connectionString))
                        {
                            SqlCon.Open();
                            SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                            int flag = Sqlcom1.ExecuteNonQuery();
                            SqlCommand Sqlcom = new SqlCommand("Exec STP_LS_CATEGORYINSERTION '" + _categorysort + "','" + _categoryName + "','" + _ParentCategoryId + "','" + customerid.CustomerId + "','INSERT',''", SqlCon);
                            Sqlcom.CommandTimeout = 0;
                            SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                            da.Fill(tbCategory);
                            //int flag = Sqlcom.ExecuteNonQuery();
                            SqlCon.Close();
                        }
                        //var tbCategory = new TB_CATEGORY
                        //{
                        //    CATEGORY_SHORT = _categorysort,
                        //    CATEGORY_NAME = _categoryName,
                        //    PARENT_CATEGORY = _ParentCategoryId,
                        //    SHORT_DESC = string.Empty,
                        //    IMAGE_FILE = string.Empty,
                        //    IMAGE_TYPE = string.Empty,
                        //    IMAGE_NAME = string.Empty,
                        //    IMAGE_FILE2 = string.Empty,
                        //    IMAGE_TYPE2 = string.Empty,
                        //    IMAGE_NAME2 = string.Empty,
                        //    CUSTOM_NUM_FIELD1 = null,
                        //    CUSTOM_TEXT_FIELD1 = string.Empty,
                        //    CUSTOM_NUM_FIELD2 = null,
                        //    CUSTOM_TEXT_FIELD2 = string.Empty,
                        //    CUSTOM_NUM_FIELD3 = null,
                        //    CUSTOM_TEXT_FIELD3 = string.Empty,
                        //    CREATED_USER = User.Identity.Name,
                        //    CREATED_DATE = DateTime.Now,
                        //    MODIFIED_USER = User.Identity.Name,
                        //    MODIFIED_DATE = DateTime.Now,
                        //    PUBLISH = true,
                        //    PUBLISH2PRINT = true,
                        //    PUBLISH2CD = true,
                        //    WORKFLOW_STATUS = 4,
                        //    WORKFLOW_COMMENTS = null,
                        //    IS_CLONE = 0,
                        //    CLONE_LOCK = false
                        //};
                        //objLS.TB_CATEGORY.Add(tbCategory);
                        //objLS.SaveChanges();
                        if (tbCategory != null && tbCategory.Rows.Count > 0)
                        {
                            _RecordsAffected = tbCategory.Rows.Count;
                            _categoryId = tbCategory.Rows[0][0].ToString();
                            _ParentCategoryId = tbCategory.Rows[0][0].ToString();
                        }

                    }

                }
                else
                {
                    _RecordsAffected = 0;
                    //throw new //"Parent Category [" + _ParentCategoryId + "] does not exist in the database";
                }
            }
            else if (_ParentCategoryId == "0")
            {
                //root category
                var customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                if (string.IsNullOrEmpty(_categoryId) || _categoryId == "0")
                {
                    var category =
                            objLS.TB_CATEGORY.Where(
                                x => x.CATEGORY_NAME == _categoryName && x.PARENT_CATEGORY == _ParentCategoryId && x.CUSTOMER_ID == customerid.CustomerId).Select(y => y.CATEGORY_ID).ToList();
                    if (category.Any())
                    {
                        _categoryId = category[0].ToString();
                    }
                }
                int count = 0;
                if (string.IsNullOrEmpty(_categoryId) || _categoryId == "0")
                {
                    count = 1;
                    while (count == 1)
                    {
                        var rnd = new Random();
                        _categorysort = rnd.Next().ToString();
                        count = objLS.TB_CATEGORY.Count(x => x.CATEGORY_SHORT == _categorysort && x.CUSTOMER_ID == customerid.CustomerId);
                    }
                }
                else
                {
                    var catsort = objLS.TB_CATEGORY.Where(x => x.CATEGORY_ID == _categoryId && x.CUSTOMER_ID == customerid.CustomerId).Select(y => y.CATEGORY_SHORT).FirstOrDefault();
                    if (catsort != null)
                    {
                        if (catsort.Any())
                        {
                            _categorysort = catsort.ToString();
                        }
                    }
                    else
                    {
                        _categorysort = _categoryId;
                    }

                    _categoryId = objLS.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == _categorysort && x.CUSTOMER_ID == customerid.CustomerId).Select(y => y.CATEGORY_ID).FirstOrDefault();
                }
                count = objLS.TB_CATEGORY.Count(x => x.CATEGORY_SHORT == _categorysort && x.CUSTOMER_ID == customerid.CustomerId);
                if (count > 0)
                {
                    if (_categoryName.Contains("'"))
                    {
                        _categoryName = _categoryName.Replace("'", "''");
                    }
                    using (SqlConnection SqlCon = new SqlConnection(connectionString))
                    {
                        SqlCon.Open();
                        SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                        int flag1 = Sqlcom1.ExecuteNonQuery();
                        SqlCommand Sqlcom = new SqlCommand("UPDATE       TB_CATEGORY\r\nSET                CATEGORY_NAME = '" + _categoryName + "', PARE" +
                "NT_CATEGORY ='" + _ParentCategoryId + "'\r\nWHERE        (CATEGORY_ID ='" + _categoryId + "')", SqlCon);
                        //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                        //da.Fill(tbCategory);
                        int flag = Sqlcom.ExecuteNonQuery();
                        SqlCon.Close();
                    }
                    //TB_CATEGORY tbCategory = objLS.TB_CATEGORY.FirstOrDefault(x => x.CATEGORY_ID == _categoryId);
                    //if (tbCategory != null)
                    //{
                    //    tbCategory.CATEGORY_NAME = _categoryName;
                    //    tbCategory.PARENT_CATEGORY = _ParentCategoryId;
                    //}
                    //objLS.SaveChanges();



                    int.TryParse(objLS.TB_CATEGORY.Count(x => x.CATEGORY_ID == _categoryId && x.CUSTOMER_ID == customerid.CustomerId).ToString(),
                        out _RecordsAffected);
                    // _categoryId = tbCategory.CATEGORY_ID;

                }
                else
                {
                    customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                    DataTable tbCategory = new DataTable();
                    if (_categoryName.Contains("'"))
                    {
                        _categoryName = _categoryName.Replace("'", "''");
                    }
                    using (SqlConnection SqlCon = new SqlConnection(connectionString))
                    {
                        SqlCon.Open();
                        SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                        int flag = Sqlcom1.ExecuteNonQuery();
                        SqlCommand Sqlcom = new SqlCommand("Exec STP_LS_CATEGORYINSERTION '" + _categorysort + "','" + _categoryName + "','" + _ParentCategoryId + "','" + customerid.CustomerId + "','INSERT',''", SqlCon);
                        Sqlcom.CommandTimeout = 0;
                        SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                        da.Fill(tbCategory);
                        //int flag = Sqlcom.ExecuteNonQuery();
                        SqlCon.Close();
                    }
                    //var tbCategory = new TB_CATEGORY
                    //{
                    //    CATEGORY_SHORT = _categorysort,
                    //    CATEGORY_NAME = _categoryName,
                    //    PARENT_CATEGORY = _ParentCategoryId,
                    //    SHORT_DESC = string.Empty,
                    //    IMAGE_FILE = string.Empty,
                    //    IMAGE_TYPE = string.Empty,
                    //    IMAGE_NAME = string.Empty,
                    //    IMAGE_FILE2 = string.Empty,
                    //    IMAGE_TYPE2 = string.Empty,
                    //    IMAGE_NAME2 = string.Empty,
                    //    CUSTOM_NUM_FIELD1 = null,
                    //    CUSTOM_TEXT_FIELD1 = string.Empty,
                    //    CUSTOM_NUM_FIELD2 = null,
                    //    CUSTOM_TEXT_FIELD2 = string.Empty,
                    //    CUSTOM_NUM_FIELD3 = null,
                    //    CUSTOM_TEXT_FIELD3 = string.Empty,
                    //    CREATED_USER = User.Identity.Name,
                    //    CREATED_DATE = DateTime.Now,
                    //    MODIFIED_USER = User.Identity.Name,
                    //    MODIFIED_DATE = DateTime.Now,
                    //    PUBLISH = true,
                    //    PUBLISH2PRINT = true,
                    //    PUBLISH2CD = true,
                    //    WORKFLOW_STATUS = 4,
                    //    WORKFLOW_COMMENTS = null,
                    //    IS_CLONE = 0,
                    //    CLONE_LOCK = false
                    //};
                    //objLS.TB_CATEGORY.Add(tbCategory);
                    //objLS.SaveChanges();
                    if (tbCategory != null && tbCategory.Rows.Count > 0)
                    {
                        _RecordsAffected = tbCategory.Rows.Count;
                        _categoryId = tbCategory.Rows[0][0].ToString();
                        _ParentCategoryId = tbCategory.Rows[0][0].ToString();
                    }

                    ////no it is not there, lets insert it.
                    //var tbCategory = new TB_CATEGORY
                    //{
                    //    CATEGORY_SHORT = _categorysort,
                    //    CATEGORY_NAME = _categoryName,
                    //    PARENT_CATEGORY = _ParentCategoryId,
                    //    SHORT_DESC = string.Empty,
                    //    IMAGE_FILE = string.Empty,
                    //    IMAGE_TYPE = string.Empty,
                    //    IMAGE_NAME = string.Empty,
                    //    IMAGE_FILE2 = string.Empty,
                    //    IMAGE_TYPE2 = string.Empty,
                    //    IMAGE_NAME2 = string.Empty,
                    //    CUSTOM_NUM_FIELD1 = null,
                    //    CUSTOM_TEXT_FIELD1 = string.Empty,
                    //    CUSTOM_NUM_FIELD2 = null,
                    //    CUSTOM_TEXT_FIELD2 = string.Empty,
                    //    CUSTOM_NUM_FIELD3 = null,
                    //    CUSTOM_TEXT_FIELD3 = string.Empty,
                    //    CREATED_USER = User.Identity.Name,
                    //    CREATED_DATE = DateTime.Now,
                    //    MODIFIED_USER = User.Identity.Name,
                    //    MODIFIED_DATE = DateTime.Now,
                    //    PUBLISH = true,
                    //    PUBLISH2PRINT = true,
                    //    PUBLISH2CD = true,
                    //    WORKFLOW_STATUS = 4,
                    //    WORKFLOW_COMMENTS = null,
                    //    IS_CLONE = 0,
                    //    CLONE_LOCK = false
                    //};
                    //objLS.TB_CATEGORY.Add(tbCategory);
                    //objLS.SaveChanges();
                    //int.TryParse(objLS.TB_CATEGORY.Count(x => x.CATEGORY_ID == tbCategory.CATEGORY_ID).ToString(),
                    //    out _RecordsAffected);
                    //_ParentCategoryId = tbCategory.CATEGORY_ID;
                    //_categoryId = tbCategory.CATEGORY_ID;
                }
                // _ParentCategoryId = _categoryId;
            }
            else if (_ParentCategoryId == "")
            {
                //Parent id not provided.
                _RecordsAffected = 0;
                // throw new TradingBell.ExIm.CSUtil.CSException("Parent Category must be provided");
            }
            return _RecordsAffected;
        }

        public int UpdateCagtegoryField()
        {
            int count = objLS.TB_CATEGORY.Count(x => x.CATEGORY_ID == _categoryId);
            if (count > 0)
            {
                var ctbl = new DataTable();
                var query = objLS.TB_CATEGORY.Where(x => x.CATEGORY_ID == _categoryId).ToList();

                //DataRow drtemp = ctbl.Rows[0];
                if (_categoryShortDesc == "")
                {
                    _categoryShortDesc = query[0].SHORT_DESC;
                }
                if (_categoryImageFile == "")
                {
                    _categoryImageFile = query[0].IMAGE_FILE;
                }
                if (_categoryImageType == "")
                {
                    _categoryImageType = query[0].IMAGE_TYPE;
                }
                if (_categoryImageName == "")
                {
                    _categoryImageName = query[0].IMAGE_NAME;
                }
                if (_categoryImageName2 == "")
                {
                    _categoryImageName2 = query[0].IMAGE_NAME2;
                }
                if (_categoryImageFile2 == "")
                {
                    _categoryImageFile2 = query[0].IMAGE_FILE2;
                }
                if (_categoryImageType2 == "")
                {
                    _categoryImageType2 = query[0].IMAGE_TYPE2;
                }
                if (_categoryCustomNum1 == 0)
                {
                    double.TryParse(query[0].CUSTOM_NUM_FIELD1.ToString(), out _categoryCustomNum1);
                }
                if (_categoryCustomNum2 == 0)
                {
                    double.TryParse(query[0].CUSTOM_NUM_FIELD2.ToString(), out _categoryCustomNum2);
                }
                if (_categoryCustomNum3 == 0)
                {
                    double.TryParse(query[0].CUSTOM_NUM_FIELD3.ToString(), out _categoryCustomNum3);
                }
                if (_categoryCustomText1 == "")
                {
                    _categoryCustomText1 = query[0].CUSTOM_TEXT_FIELD1;
                }
                if (_categoryCustomText2 == "")
                {
                    _categoryCustomText2 = query[0].CUSTOM_TEXT_FIELD2;
                }
                if (_categoryCustomText3 == "")
                {
                    _categoryCustomText3 = query[0].CUSTOM_TEXT_FIELD3;
                }
                DataTable tbCategory = new DataTable();
                try
                {
                    if (_categoryShortDesc.Contains("'"))
                    {
                        _categoryShortDesc = _categoryShortDesc.Replace("'", "''");
                    }
                    if (_categoryImageFile.Contains("'"))
                    {
                        _categoryImageFile = _categoryImageFile.Replace("'", "''");
                    }
                    if (_categoryImageType.Contains("'"))
                    {
                        _categoryImageType = _categoryImageType.Replace("'", "''");
                    }
                    if (_categoryImageName.Contains("'"))
                    {
                        _categoryImageName = _categoryImageName.Replace("'", "''");
                    }

                    if (_categoryImageName2.Contains("'"))
                    {
                        _categoryImageName2 = _categoryImageName2.Replace("'", "''");
                    }

                    if (_categoryImageFile2.Contains("'"))
                    {
                        _categoryImageFile2 = _categoryImageFile2.Replace("'", "''");
                    }

                    if (_categoryImageType2.Contains("'"))
                    {
                        _categoryImageType2 = _categoryImageType2.Replace("'", "''");
                    }


                    if (_categoryCustomText1.Contains("'"))
                    {
                        _categoryCustomText1 = _categoryCustomText1.Replace("'", "''");
                    }
                    if (_categoryCustomText2.Contains("'"))
                    {
                        _categoryCustomText2 = _categoryCustomText2.Replace("'", "''");
                    }
                    if (_categoryCustomText3.Contains("'"))
                    {
                        _categoryCustomText3 = _categoryCustomText3.Replace("'", "''");
                    }
                    using (SqlConnection SqlCon = new SqlConnection(connectionString))
                    {
                        SqlCon.Open();
                        SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                        int flag = Sqlcom1.ExecuteNonQuery();
                        SqlCommand Sqlcom = new SqlCommand("UPDATE    TB_CATEGORY "
        + "SET  SHORT_DESC = '" + _categoryShortDesc + "', IMAGE_FILE = '" + _categoryImageFile + "', IMAGE_TYPE = '" + _categoryImageType + "', IMAGE_NAME = '" + _categoryImageName + "', "
                      + "IMAGE_NAME2 = '" + _categoryImageName2 + "', IMAGE_FILE2 = '" + _categoryImageFile2 + "', IMAGE_TYPE2 = '" + _categoryImageType2 + "', "
                      + "CUSTOM_NUM_FIELD1 = " + (decimal)_categoryCustomNum1 + ", CUSTOM_NUM_FIELD2 = " + (decimal)_categoryCustomNum2 + ", "
                      + "CUSTOM_NUM_FIELD3 = " + (decimal)_categoryCustomNum3 + ", CUSTOM_TEXT_FIELD1 = '" + _categoryCustomText1 + "', "
                      + "CUSTOM_TEXT_FIELD2 = '" + _categoryCustomText2 + "', CUSTOM_TEXT_FIELD3 = '" + _categoryCustomText3 + "'"
        + "WHERE     (CATEGORY_ID = '" + _categoryId + "') ", SqlCon);
                        SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                        da.Fill(tbCategory);
                        //int flag = Sqlcom.ExecuteNonQuery();
                        SqlCon.Close();
                    }
                    //TB_CATEGORY tbCategory = objLS.TB_CATEGORY.FirstOrDefault(x => x.CATEGORY_ID == _categoryId);
                    //tbCategory.IMAGE_FILE = _categoryImageFile;
                    //tbCategory.IMAGE_TYPE = _categoryImageType;
                    //tbCategory.IMAGE_NAME = _categoryImageName;
                    //tbCategory.IMAGE_FILE2 = _categoryImageFile2;
                    //tbCategory.IMAGE_TYPE2 = _categoryImageType2;
                    //tbCategory.IMAGE_NAME2 = _categoryImageName2;
                    //tbCategory.CUSTOM_NUM_FIELD1 = (decimal)_categoryCustomNum1;
                    //tbCategory.CUSTOM_NUM_FIELD2 = (decimal)_categoryCustomNum2;
                    //tbCategory.CUSTOM_NUM_FIELD3 = (decimal)_categoryCustomNum3;
                    //tbCategory.CUSTOM_TEXT_FIELD1 = _categoryCustomText1;
                    //tbCategory.CUSTOM_TEXT_FIELD2 = _categoryCustomText2;
                    //tbCategory.CUSTOM_TEXT_FIELD3 = _categoryCustomText3;

                    //objLS.SaveChanges();
                    _RecordsAffected = objLS.TB_CATEGORY.Count(x => x.CATEGORY_ID == _categoryId);
                }
                catch (Exception)
                {
                    return _RecordsAffected;
                }


            }
            return _RecordsAffected;
        }
        int ParentFamilyids = 0;
        public int FamilyIU()
        {
            /* if (_FamilyId != null)
             {
                 Random rnd = new Random();
                 _FamilyId = rnd.Next(1000, 100000);
             }*/
            if (_FamilyName != null)
            {
                int catcount = objLS.TB_CATEGORY.Count(x => x.CATEGORY_ID == _categoryId);
                if (_categoryId != null && _categoryId != "0" && _categoryId != "" && catcount != 0)
                {
                    if (_parentFamilyId == 0)
                    {
                        /* if (_CategoryId != null && _CategoryId != "0" && _CategoryId != "")
                         {*/
                        int iRoot = 1;
                        // int count=  cfa.GetFirstMatchingFamilyId(_FamilyId);
                        int countfams = 1;
                        if (FamilyId > 0)
                            countfams = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);
                        if (countfams == 1)
                        {

                            if (FamilyId <= 0)
                            {
                                try
                                {
                                    _FamilyId = objLS.TB_FAMILY.Join(objLS.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tfs => tfs.FAMILY_ID,
                            (tps, tfs) => new { tps, tfs }).Where(x =>
                                                x.tps.CATEGORY_ID == _categoryId && x.tps.FAMILY_NAME == _FamilyName &&
                                                x.tps.PARENT_FAMILY_ID == _parentFamilyId && x.tfs.CATALOG_ID == _catalogId)
                                        .Select(y => y.tps.FAMILY_ID)
                                        .FirstOrDefault();

                                }
                                catch (Exception)
                                {
                                    _FamilyId = 0;
                                }
                            }
                            int count = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);
                            if (count == 0)
                            {
                                try
                                {
                                    if (_FamilyName.Contains("'"))
                                    {
                                        _FamilyName = _FamilyName.Replace("'", "''");
                                    }
                                    using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                    {
                                        SqlCon.Open();
                                        SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                                        int flag = Sqlcom1.ExecuteNonQuery();
                                        SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_FAMILY "
                                                  + "(FAMILY_NAME, PARENT_FAMILY_ID, ROOT_FAMILY, CATEGORY_ID, STATUS)"
                                           + "VALUES     ('" + _FamilyName + "'," + _parentFamilyId + "," + Convert.ToByte(iRoot) + ",'" + _categoryId + "','CREATED');", SqlCon);
                                        //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                        // da.Fill(tbCategory);
                                        int flag1 = Sqlcom.ExecuteNonQuery();
                                        SqlCon.Close();
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                                //TB_FAMILY tbFamily = new TB_FAMILY
                                //{
                                //    FAMILY_NAME = _FamilyName,
                                //    PARENT_FAMILY_ID = _parentFamilyId,
                                //    ROOT_FAMILY = Convert.ToByte(iRoot),
                                //    CATEGORY_ID = _categoryId,
                                //    STATUS = "CREATED",
                                //    FOOT_NOTES = null,
                                //    PRODUCT_TABLE_STRUCTURE = null,
                                //    DISPLAY_TABLE_HEADER = true,
                                //    CREATED_USER = User.Identity.Name,
                                //    CREATED_DATE = DateTime.Now,
                                //    MODIFIED_USER = User.Identity.Name,
                                //    MODIFIED_DATE = DateTime.Now,
                                //    PUBLISH = true,
                                //    PUBLISH2PRINT = true,
                                //    PUBLISH2CD = true,
                                //    WORKFLOW_STATUS = 4,
                                //    WORKFLOW_COMMENTS = null
                                //};
                                //objLS.TB_FAMILY.Add(tbFamily);
                                //objLS.SaveChanges();

                                //_RecordsAffected =
                                //    objLS.TB_FAMILY.Count(x => x.CATEGORY_ID == _categoryId && x.FAMILY_ID == _FamilyId);

                                ////    _FamilyId =
                                ////        objLS.TB_FAMILY.Join(objLS.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tfs => tfs.FAMILY_ID,
                                ////(tps, tfs) => new { tps, tfs }).Where(
                                ////            x =>
                                ////                x.tps.CATEGORY_ID == _categoryId && x.tps.FAMILY_NAME == _FamilyName &&
                                ////                x.tps.PARENT_FAMILY_ID == _parentFamilyId && x.tfs.CATALOG_ID == _catalogId).Select(y => y.tps.FAMILY_ID).FirstOrDefault();
                                _FamilyId =
                             objLS.TB_FAMILY.Where(
                                 x =>
                                     x.FAMILY_NAME == _FamilyName && x.CATEGORY_ID == _categoryId &&
                                     x.PARENT_FAMILY_ID == _parentFamilyId).OrderByDescending(y => y.FAMILY_ID).Select(y => y.FAMILY_ID).FirstOrDefault();
                                ParentFamilyids = _FamilyId;
                                _RecordsAffected =
                                    objLS.TB_FAMILY.Count(x => x.CATEGORY_ID == _categoryId && x.FAMILY_ID == _FamilyId);
                                int maxsorder = 1;
                                int sort_ =
                                    objLS.TB_CATALOG_FAMILY.Where(
                                        x => x.CATALOG_ID == _CatalogId && x.CATEGORY_ID == _categoryId)
                                        .OrderByDescending(y => y.SORT_ORDER).Select(z => z.SORT_ORDER)
                                        .FirstOrDefault();
                                if (sort_ != null)
                                    maxsorder = sort_ + 1;
                                if (_CatalogId != 1)
                                {
                                    using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                    {
                                        SqlCon.Open();
                                        SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                                        int flag = Sqlcom1.ExecuteNonQuery();
                                        SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_CATALOG_FAMILY\r\n                      (CATALOG_ID, FAMILY_ID, SORT"
               + "_ORDER, CATEGORY_ID)\r\nVALUES     (" + _catalogId + "," + _FamilyId + "," + maxsorder + ",'" + _categoryId + "'); \r\n", SqlCon);
                                        //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                        // da.Fill(tbCategory);
                                        int flag1 = Sqlcom.ExecuteNonQuery();
                                        SqlCon.Close();
                                    }
                                    //TB_CATALOG_FAMILY tbCatalogFamily = new TB_CATALOG_FAMILY
                                    //{
                                    //    CATALOG_ID = _catalogId,
                                    //    FAMILY_ID = _FamilyId,
                                    //    SORT_ORDER = maxsorder,
                                    //    CATEGORY_ID = _categoryId,
                                    //    CREATED_USER = User.Identity.Name,
                                    //    CREATED_DATE = DateTime.Now,
                                    //    MODIFIED_USER = User.Identity.Name,
                                    //    MODIFIED_DATE = DateTime.Now,
                                    //    ROOT_CATEGORY = "0"
                                    //};
                                    //objLS.TB_CATALOG_FAMILY.Add(tbCatalogFamily);
                                    //objLS.SaveChanges();

                                    _RecordsAffected =
                                        objLS.TB_CATALOG_FAMILY.Count(
                                            x =>
                                                x.CATALOG_ID == _catalogId && x.CATEGORY_ID == _categoryId &&
                                                x.FAMILY_ID == _FamilyId && x.SORT_ORDER == maxsorder);
                                }
                            }
                            else
                            {
                                int famcount =
                                    objLS.TB_CATALOG_FAMILY.Count(
                                        x =>
                                            x.CATALOG_ID == _catalogId && x.CATEGORY_ID == _categoryId &&
                                            x.FAMILY_ID == _FamilyId);
                                int maxsorder = 1;
                                int sort_ =
                                   objLS.TB_CATALOG_FAMILY.Where(
                                       x => x.CATALOG_ID == _CatalogId && x.CATEGORY_ID == _categoryId)
                                       .OrderByDescending(y => y.SORT_ORDER).Select(z => z.SORT_ORDER)
                                       .FirstOrDefault();
                                if (sort_ != null)
                                    maxsorder = sort_ + 1;
                                if (famcount == 1)
                                {
                                    //update family
                                    objLS = new CSEntities();
                                    TB_FAMILY tbFamily =
                                        objLS.TB_FAMILY.Where(x => x.FAMILY_ID == _FamilyId).FirstOrDefault();
                                    tbFamily.FAMILY_NAME = _FamilyName;
                                    tbFamily.PARENT_FAMILY_ID = _parentFamilyId;
                                    tbFamily.ROOT_FAMILY = Convert.ToByte(iRoot);
                                    objLS.SaveChanges();

                                    _RecordsAffected = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);
                                }
                                else
                                {
                                    if (_CatalogId != 1)
                                    {
                                        using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                        {
                                            SqlCon.Open();
                                            SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                                            int flag = Sqlcom1.ExecuteNonQuery();
                                            SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_CATALOG_FAMILY\r\n                      (CATALOG_ID, FAMILY_ID, SORT"
                   + "_ORDER, CATEGORY_ID)\r\nVALUES     (" + _catalogId + "," + _FamilyId + "," + maxsorder + ",'" + _categoryId + "'); \r\n", SqlCon);
                                            //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                            // da.Fill(tbCategory);
                                            int flag1 = Sqlcom.ExecuteNonQuery();
                                            SqlCon.Close();
                                        }
                                        //TB_CATALOG_FAMILY tbCatalogFamily = new TB_CATALOG_FAMILY
                                        //{
                                        //    CATALOG_ID = _CatalogId,
                                        //    FAMILY_ID = _FamilyId,
                                        //    SORT_ORDER = maxsorder,
                                        //    CATEGORY_ID = _categoryId,
                                        //    CREATED_USER = User.Identity.Name,
                                        //    CREATED_DATE = DateTime.Now,
                                        //    MODIFIED_USER = User.Identity.Name,
                                        //    MODIFIED_DATE = DateTime.Now,
                                        //    ROOT_CATEGORY = "0"
                                        //};
                                        //objLS.TB_CATALOG_FAMILY.Add(tbCatalogFamily);
                                        //objLS.SaveChanges();

                                        _RecordsAffected =
                                            objLS.TB_CATALOG_FAMILY.Count(
                                                x =>
                                                    x.CATALOG_ID == _CatalogId && x.CATEGORY_ID ==
                                                        _categoryId && x.FAMILY_ID == _FamilyId &&
                                                        x.SORT_ORDER == maxsorder);
                                    }
                                }
                            }
                        }
                        /* }
                         else
                         {
                            // throw new TradingBell.ExIm.CSUtil.CSException("Category cannot be empty");
                         }*/
                    }
                    else
                    {
                        /* if (_CategoryId != null && _CategoryId != "0")
                         {*/
                        int iRoot = 0;
                        int countfams = 1;
                        if (FamilyId > 0)
                            countfams = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);
                        if (countfams == 1)
                        {
                            if (_FamilyId <= 0)
                            {
                                try
                                {
                                    _FamilyId = objLS.TB_FAMILY.Join(objLS.TB_CATALOG_FAMILY, tps => tps.FAMILY_ID, tfs => tfs.FAMILY_ID,
                            (tps, tfs) => new { tps, tfs }).Where(x =>
                                                x.tps.CATEGORY_ID == _categoryId && x.tps.FAMILY_NAME == _FamilyName &&
                                                x.tps.PARENT_FAMILY_ID == _parentFamilyId && x.tfs.CATALOG_ID == _catalogId)
                                        .Select(y => y.tps.FAMILY_ID)
                                        .FirstOrDefault();

                                    //_FamilyId =
                                    //    objLS.TB_FAMILY.Where(
                                    //        x =>
                                    //            x.FAMILY_NAME == _FamilyName && x.CATEGORY_ID == _categoryId &&
                                    //            x.PARENT_FAMILY_ID == _parentFamilyId).Select(y => y.FAMILY_ID).FirstOrDefault();
                                }
                                catch (Exception)
                                {
                                    _FamilyId = 0;
                                }
                            }
                            int parentcount = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _parentFamilyId);
                            if (parentcount != 0)
                            {
                                int count = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);
                                if (count == 0)
                                {
                                    // insert subfamily
                                    if (_FamilyName.Contains("'"))
                                    {
                                        _FamilyName = _FamilyName.Replace("'", "''");
                                    }
                                    using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                    {
                                        SqlCon.Open();
                                        SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                                        int flag = Sqlcom1.ExecuteNonQuery();
                                        SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_FAMILY "
                          + "(FAMILY_NAME, PARENT_FAMILY_ID, ROOT_FAMILY, CATEGORY_ID, STATUS)"
        + "VALUES     ('" + _FamilyName + "'," + _parentFamilyId + "," + Convert.ToByte(iRoot) + ",'" + _categoryId + "','CREATED');", SqlCon);
                                        //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                        // da.Fill(tbCategory);
                                        int flag1 = Sqlcom.ExecuteNonQuery();
                                        SqlCon.Close();
                                    }
                                    //TB_FAMILY tbFamily = new TB_FAMILY
                                    //{
                                    //    FAMILY_NAME = _FamilyName,
                                    //    PARENT_FAMILY_ID = _parentFamilyId,
                                    //    ROOT_FAMILY = Convert.ToByte(iRoot),
                                    //    CATEGORY_ID = _categoryId,
                                    //    STATUS = "CREATED",
                                    //    FOOT_NOTES = null,
                                    //    PRODUCT_TABLE_STRUCTURE = null,
                                    //    DISPLAY_TABLE_HEADER = true,
                                    //    CREATED_USER = User.Identity.Name,
                                    //    CREATED_DATE = DateTime.Now,
                                    //    MODIFIED_USER = User.Identity.Name,
                                    //    MODIFIED_DATE = DateTime.Now,
                                    //    PUBLISH = true,
                                    //    PUBLISH2PRINT = true,
                                    //    PUBLISH2CD = true,
                                    //    WORKFLOW_STATUS = 4,
                                    //    WORKFLOW_COMMENTS = null
                                    //};
                                    //objLS.TB_FAMILY.Add(tbFamily);
                                    //objLS.SaveChanges();


                                    _FamilyId =
                                        objLS.TB_FAMILY.Where(
                                            x =>
                                                x.CATEGORY_ID == _categoryId && x.FAMILY_NAME == _FamilyName &&
                                                x.PARENT_FAMILY_ID == _parentFamilyId).OrderByDescending(y => y.FAMILY_ID).Select(y => y.FAMILY_ID).FirstOrDefault();
                                    ParentFamilyids = _FamilyId;
                                    _RecordsAffected =
                                   objLS.TB_FAMILY.Count(x => x.CATEGORY_ID == _categoryId && x.FAMILY_ID == _FamilyId);
                                    int maxsorder = 1;
                                    int sort_ =
                                        objLS.TB_CATALOG_FAMILY.Where(
                                            x => x.CATALOG_ID == _CatalogId && x.CATEGORY_ID == _categoryId)
                                            .OrderByDescending(y => y.SORT_ORDER).Select(z => z.SORT_ORDER)
                                            .FirstOrDefault();
                                    if (sort_ != null)
                                        maxsorder = sort_ + 1;
                                    if (_CatalogId != 1)
                                    {
                                        using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                        {
                                            SqlCon.Open();
                                            SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                                            int flag = Sqlcom1.ExecuteNonQuery();
                                            SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_CATALOG_FAMILY\r\n                      (CATALOG_ID, FAMILY_ID, SORT"
                   + "_ORDER, CATEGORY_ID)\r\nVALUES     (" + _catalogId + "," + _FamilyId + "," + maxsorder + ",'" + _categoryId + "'); \r\n", SqlCon);
                                            //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                            // da.Fill(tbCategory);
                                            int flag1 = Sqlcom.ExecuteNonQuery();
                                            SqlCon.Close();
                                        }
                                        //TB_CATALOG_FAMILY tbCatalogFamily = new TB_CATALOG_FAMILY
                                        //{
                                        //    CATALOG_ID = _catalogId,
                                        //    FAMILY_ID = _FamilyId,
                                        //    SORT_ORDER = maxsorder,
                                        //    CATEGORY_ID = _categoryId,
                                        //    CREATED_USER = User.Identity.Name,
                                        //    CREATED_DATE = DateTime.Now,
                                        //    MODIFIED_USER = User.Identity.Name,
                                        //    MODIFIED_DATE = DateTime.Now,
                                        //    ROOT_CATEGORY = "0"
                                        //};
                                        //objLS.TB_CATALOG_FAMILY.Add(tbCatalogFamily);
                                        //objLS.SaveChanges();

                                        _RecordsAffected =
                                            objLS.TB_CATALOG_FAMILY.Count(
                                                x =>
                                                    x.CATALOG_ID == _catalogId && x.CATEGORY_ID == _categoryId &&
                                                    x.FAMILY_ID == _FamilyId && x.SORT_ORDER == maxsorder);
                                    }
                                    int maxsortsub = 1;
                                    int sort1_ =
                                       objLS.TB_CATALOG_FAMILY.Where(
                                           x => x.CATALOG_ID == _CatalogId && x.CATEGORY_ID == _categoryId)
                                           .OrderByDescending(y => y.SORT_ORDER).Select(z => z.SORT_ORDER)
                                           .FirstOrDefault();
                                    if (sort1_ != null)
                                    {
                                        maxsortsub = sort1_ + 1;
                                    }
                                    TB_SUBFAMILY tbSubFamily = new TB_SUBFAMILY
                                    {
                                        FAMILY_ID = _parentFamilyId,
                                        SUBFAMILY_ID = _FamilyId,
                                        SORT_ORDER = maxsorder,
                                        CREATED_USER = User.Identity.Name,
                                        CREATED_DATE = DateTime.Now,
                                        MODIFIED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now,
                                    };
                                    objLS.TB_SUBFAMILY.Add(tbSubFamily);
                                    objLS.SaveChanges();
                                    _RecordsAffected =
                                        objLS.TB_SUBFAMILY.Count(
                                            x =>
                                                x.FAMILY_ID == _parentFamilyId && x.SUBFAMILY_ID == _FamilyId &&
                                                x.SORT_ORDER == maxsorder);
                                }
                                else
                                {
                                    int famcount = objLS.TB_CATALOG_FAMILY.Count(x => x.CATALOG_ID == _catalogId && x.FAMILY_ID == _FamilyId && x.CATEGORY_ID == _categoryId);
                                    int maxsorder = 1;
                                    int sort =
                                        objLS.TB_CATALOG_FAMILY.Where(
                                            x => x.CATALOG_ID == _catalogId && x.CATEGORY_ID == _categoryId).OrderByDescending(z => z.SORT_ORDER)
                                            .Select(y => y.SORT_ORDER).FirstOrDefault();
                                    if (sort != null)
                                        maxsorder = sort + 1;
                                    if (famcount == 1)
                                    {
                                        TB_FAMILY tbFamily =
                                            objLS.TB_FAMILY.Where(x => x.FAMILY_ID == _FamilyId).FirstOrDefault();

                                        tbFamily.FAMILY_NAME = _FamilyName;
                                        tbFamily.PARENT_FAMILY_ID = _FamilyId;
                                        tbFamily.ROOT_FAMILY = Convert.ToByte(iRoot);
                                        //tbFamily.CATEGORY_ID = _categoryId;
                                        objLS.SaveChanges();
                                        _RecordsAffected = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);

                                        using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                        {
                                            SqlCon.Open();
                                            SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                                            int flag = Sqlcom1.ExecuteNonQuery();
                                            SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_CATALOG_FAMILY\r\n                      (CATALOG_ID, FAMILY_ID, SORT"
                   + "_ORDER, CATEGORY_ID)\r\nVALUES     (" + _catalogId + "," + _FamilyId + "," + maxsorder + ",'" + _categoryId + "'); \r\n", SqlCon);
                                            //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                            // da.Fill(tbCategory);
                                            int flag1 = Sqlcom.ExecuteNonQuery();
                                            SqlCon.Close();
                                        }
                                        //TB_CATALOG_FAMILY tbCatalogFamily =
                                        //    objLS.TB_CATALOG_FAMILY.Where(
                                        //        x => x.CATALOG_ID == _catalogId && x.FAMILY_ID == _FamilyId)
                                        //        .FirstOrDefault();
                                        //tbCatalogFamily.CATEGORY_ID = _categoryId;
                                        //objLS.SaveChanges();
                                    }
                                    else
                                    {
                                        if (_CatalogId != 1)
                                        {
                                            using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                            {
                                                SqlCon.Open();
                                                SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                                                int flag = Sqlcom1.ExecuteNonQuery();
                                                SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_CATALOG_FAMILY\r\n                      (CATALOG_ID, FAMILY_ID, SORT"
                       + "_ORDER, CATEGORY_ID)\r\nVALUES     (" + _catalogId + "," + _FamilyId + "," + maxsorder + "," + _categoryId + "); \r\n", SqlCon);
                                                //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                                // da.Fill(tbCategory);
                                                int flag1 = Sqlcom.ExecuteNonQuery();
                                                SqlCon.Close();
                                            }
                                            //TB_CATALOG_FAMILY tbCatalogFamily = new TB_CATALOG_FAMILY
                                            //{
                                            //    CATALOG_ID = _catalogId,
                                            //    FAMILY_ID = _FamilyId,
                                            //    SORT_ORDER = maxsorder,
                                            //    CATEGORY_ID = _categoryId,
                                            //    CREATED_USER = User.Identity.Name,
                                            //    CREATED_DATE = DateTime.Now,
                                            //    MODIFIED_USER = User.Identity.Name,
                                            //    MODIFIED_DATE = DateTime.Now,
                                            //    ROOT_CATEGORY = "0"
                                            //};
                                            //objLS.TB_CATALOG_FAMILY.Add(tbCatalogFamily);
                                            //objLS.SaveChanges();

                                            _RecordsAffected =
                                                objLS.TB_CATALOG_FAMILY.Count(
                                                    x =>
                                                        x.CATALOG_ID == _catalogId && x.CATEGORY_ID == _categoryId &&
                                                        x.FAMILY_ID == _FamilyId && x.SORT_ORDER == maxsorder);
                                        }
                                    }
                                }

                            }
                        }

                    }
                }
            }
            else
            {
                _RecordsAffected = 0;
                return _RecordsAffected;
            }

            return _RecordsAffected;
        }

        int _FamilySort = 0;
        public int FamilyExtraFields()
        {
            int count = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);
            if (count == 1)
            {

                try
                {
                    if (_FamilySort != 0)
                        if (objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId) == 0)
                        {
                            TB_CATALOG_FAMILY tbCatalogFamily =
                                objLS.TB_CATALOG_FAMILY.Where(
                                    x => x.CATALOG_ID == _catalogId && x.FAMILY_ID == _FamilyId).FirstOrDefault();

                            tbCatalogFamily.SORT_ORDER = _FamilySort;
                            objLS.SaveChanges();
                        }
                        else
                        {
                            if (objLS.TB_SUBFAMILY.Count(x => x.SUBFAMILY_ID == _FamilyId) != 0)
                            {
                                TB_SUBFAMILY tbSubfamily =
                               objLS.TB_SUBFAMILY.Where(
                                   x => x.FAMILY_ID == _parentFamilyId && x.SUBFAMILY_ID == _FamilyId).FirstOrDefault();

                                tbSubfamily.SORT_ORDER = 1;
                                objLS.SaveChanges();
                            }

                        }

                    objLS = new CSEntities();
                    TB_FAMILY tbFamily =
                               objLS.TB_FAMILY.Where(
                                   x => x.FAMILY_ID == _FamilyId).FirstOrDefault();
                    tbFamily.FOOT_NOTES = _footNotes;
                    tbFamily.STATUS = _status;
                    objLS.SaveChanges();
                    _RecordsAffected = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);
                }
                catch (Exception)
                { }
                return _RecordsAffected;
            }
            return _RecordsAffected;
        }
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public int ProductUI()
        {
            _ProductId = _productId;
            if (RemoveProduct != null && RemoveProduct.ToUpper() == "YES")
            {

                if (_ProductId > 0)
                {
                    DataTable fam = new DataTable();
                    var ss = objLS.TB_PROD_FAMILY.Where(x => x.PRODUCT_ID == _ProductId).ToList();
                    fam = ToDataTable(ss);
                    TB_PRODUCT tbProduct = objLS.TB_PRODUCT.Where(x => x.PRODUCT_ID == _ProductId).FirstOrDefault();

                    int result = objLS.TB_PRODUCT.Count(x => x.PRODUCT_ID == _ProductId);
                    objLS.TB_PRODUCT.Remove(tbProduct);
                    for (int i = 0; i < fam.Rows.Count; i++)
                    {
                        _removeDs[_arrpoint] = fam.Rows[i]["FAMILY_ID"].ToString();
                    }
                    return result;
                }
                else
                {
                    int result = 0;
                    var prod =
                        objLS.TB_PROD_SPECS.Join(objLS.TB_PROD_FAMILY, tps => tps.PRODUCT_ID, tfs => tfs.PRODUCT_ID,
                            (tps, tfs) => new { tps, tfs })
                            .Join(objLS.TB_CATALOG_FAMILY, tfstcf => tfstcf.tfs.FAMILY_ID, tcf => tcf.FAMILY_ID,
                                (tfstcf, tcf) => new { tfstcf, tcf })
                            .Where(x => x.tfstcf.tps.ATTRIBUTE_ID == 1 &&
                                        x.tfstcf.tps.STRING_VALUE == _catalogItemNo && x.tcf.CATALOG_ID == _catalogId);
                    int prodcount = prod.Count(x => x.tcf.CATALOG_ID == _catalogId);
                    DataTable tb_prodspecs = new DataTable();
                    if (prodcount > 0)
                    {
                        try
                        {
                            var ss = prod.Select(
                                x =>
                                    new
                                    {
                                        x.tfstcf.tps.STRING_VALUE,
                                        x.tfstcf.tps.PRODUCT_ID,
                                        x.tfstcf.tps.ATTRIBUTE_ID,
                                        x.tfstcf.tps.NUMERIC_VALUE,
                                        x.tfstcf.tps.OBJECT_TYPE,
                                        x.tfstcf.tps.OBJECT_NAME,
                                        x.tfstcf.tps.CREATED_DATE,
                                        x.tfstcf.tps.MODIFIED_DATE,
                                        x.tfstcf.tfs.FAMILY_ID
                                    }).ToList();
                            tb_prodspecs = ToDataTable(ss);

                        }
                        catch (Exception) { }
                        if (tb_prodspecs.Rows.Count > 0)
                        {
                            for (int i = 0; i < tb_prodspecs.Rows.Count; i++)
                            {

                                _ProductId = Convert.ToInt32(tb_prodspecs.Rows[i]["PRODUCT_ID"]);
                                if (_FamilyId > 0)
                                {
                                    _removeDs[_arrpoint] = _FamilyId.ToString();
                                    int countpf = objLS.TB_PROD_FAMILY.Count(x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == _productId);
                                    if (countpf == 1)
                                    {
                                        TB_PRODUCT tbProduct = objLS.TB_PRODUCT.Where(x => x.PRODUCT_ID == _ProductId).FirstOrDefault();

                                        result = objLS.TB_PRODUCT.Count(x => x.PRODUCT_ID == _ProductId);
                                        objLS.TB_PRODUCT.Remove(tbProduct);
                                    }
                                }
                                else
                                {
                                    _removeDs[_arrpoint] = tb_prodspecs.Rows[i]["FAMILY_ID"].ToString();
                                    TB_PRODUCT tbProduct = objLS.TB_PRODUCT.Where(x => x.PRODUCT_ID == _ProductId).FirstOrDefault();

                                    result = objLS.TB_PRODUCT.Count(x => x.PRODUCT_ID == _ProductId);
                                    objLS.TB_PRODUCT.Remove(tbProduct);
                                }

                                /*cspf.FillBy(tb_prodfam, Convert.ToInt32(tb_prodspecs.Rows[i]["FAMILY_ID"]));
                                for (int j = 0; j < tb_prodfam.Rows.Count; j++)
                                {
                                    cspf.Updatefamproductsort((j + 1), Convert.ToInt32(tb_prodspecs.Rows[i]["FAMILY_ID"]), Convert.ToInt32(tb_prodfam.Rows[j]["PRODUCT_ID"]));
                                }*/
                            }
                        }

                    }
                    return result;
                }

            }
            int _RecordsAffected = 0;
            if (_ProductId <= 0)
            {
                if (_catalogItemNo != "")
                {
                    if (_categoryId != "0" && _categoryId != "")
                    {
                        if (_CatalogId != 0 && _FamilyId > 0)
                        {
                            if (_CatalogId != 0)
                            {
                                int attrcount = objLS.TB_CATALOG_ATTRIBUTES.Count(x => x.ATTRIBUTE_ID == 1 && x.CATALOG_ID == _catalogId);
                                if (attrcount == 0)
                                {
                                    objLS = new CSEntities();
                                    TB_CATALOG_ATTRIBUTES tbCatalogAttributes = new TB_CATALOG_ATTRIBUTES
                                    {
                                        CATALOG_ID = _catalogId,
                                        ATTRIBUTE_ID = 1,
                                        CREATED_USER = User.Identity.Name,
                                        CREATED_DATE = DateTime.Now,
                                        MODIFIED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now
                                    };
                                    objLS.TB_CATALOG_ATTRIBUTES.Add(tbCatalogAttributes);
                                    objLS.SaveChanges();
                                }
                            }
                            int catcount = objLS.TB_CATEGORY.Count(x => x.CATEGORY_ID == _categoryId);
                            int famcount = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);
                            int dupitemno = 0;
                            if (_duplicateItemno == false)
                            {
                                //        var prod =
                                //objLS.TB_PROD_SPECS.Join(objLS.TB_PROD_FAMILY, tps => tps.PRODUCT_ID, tfs => tfs.PRODUCT_ID,
                                //    (tps, tfs) => new { tps, tfs })
                                //    .Join(objLS.TB_CATALOG_FAMILY, tfstcf => tfstcf.tfs.FAMILY_ID, tcf => tcf.FAMILY_ID,
                                //        (tfstcf, tcf) => new { tfstcf, tcf })
                                //    .Where(x => x.tfstcf.tps.ATTRIBUTE_ID == 1 &&
                                //                x.tfstcf.tps.STRING_VALUE == _catalogItemNo && x.tcf.CATALOG_ID == _catalogId);
                                var prod =
               objLS.TB_PROD_SPECS.Join(objLS.TB_PROD_FAMILY, tps => tps.PRODUCT_ID, tfs => tfs.PRODUCT_ID,
                   (tps, tfs) => new { tps, tfs })
                   .Join(objLS.TB_CATALOG_FAMILY, tfstcf => tfstcf.tfs.FAMILY_ID, tcf => tcf.FAMILY_ID,
                       (tfstcf, tcf) => new { tfstcf, tcf })
                   .Where(x => x.tfstcf.tps.ATTRIBUTE_ID == 1 &&
                               x.tfstcf.tps.STRING_VALUE == _catalogItemNo);
                                if (prod.Any())
                                {
                                    dupitemno = prod.Count(x => x.tcf.CATALOG_ID == _catalogId);

                                    DataTable tb_prodspecs = new DataTable();
                                    if (dupitemno > 0)
                                    {
                                        try
                                        {
                                            var ss =
                                    prod.Select(
                                        x =>
                                            new
                                            {
                                                x.tfstcf.tps.STRING_VALUE,
                                                x.tfstcf.tps.PRODUCT_ID,
                                                x.tfstcf.tps.ATTRIBUTE_ID,
                                                x.tfstcf.tps.NUMERIC_VALUE,
                                                x.tfstcf.tps.OBJECT_TYPE,
                                                x.tfstcf.tps.OBJECT_NAME,
                                                x.tfstcf.tps.CREATED_DATE,
                                                x.tfstcf.tps.MODIFIED_DATE,
                                                x.tfstcf.tfs.FAMILY_ID
                                            }).ToList();

                                            tb_prodspecs = ToDataTable(ss);
                                        }
                                        catch (Exception) { }
                                        if (tb_prodspecs.Rows.Count > 0)
                                        {
                                            _ProductId = Convert.ToInt32(tb_prodspecs.Rows[0]["PRODUCT_ID"]);
                                            _productId = _ProductId;
                                            int countpf = objLS.TB_PROD_FAMILY.Count(x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == _productId);
                                            if (countpf == 0)
                                            {
                                                int _sort = 1;
                                                try
                                                {
                                                    int sort_ =
                                                        objLS.TB_PROD_FAMILY.Where(x => x.FAMILY_ID == _FamilyId)
                                                            .OrderByDescending(y => y.SORT_ORDER).Select(z => z.SORT_ORDER)
                                                            .FirstOrDefault();
                                                    _sort = sort_ + 1;
                                                }
                                                catch (Exception)
                                                { }
                                                var tbProdFamily = new TB_PROD_FAMILY
                                                {
                                                    SORT_ORDER = _sort,
                                                    FAMILY_ID = _FamilyId,
                                                    PRODUCT_ID = _ProductId,
                                                    CREATED_USER = User.Identity.Name,
                                                    CREATED_DATE = DateTime.Now,
                                                    MODIFIED_USER = User.Identity.Name,
                                                    MODIFIED_DATE = DateTime.Now,
                                                    PUBLISH = true,
                                                    PUBLISH2PRINT = true,
                                                    PUBLISH2CD = true,
                                                    WORKFLOW_STATUS = 4,
                                                    WORKFLOW_COMMENTS = null,
                                                    FLAG_RECYCLE = "A"
                                                };
                                                objLS.TB_PROD_FAMILY.Add(tbProdFamily);
                                                objLS.SaveChanges();
                                                _RecordsAffected =
                                                    objLS.TB_PROD_FAMILY.Count(x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == _ProductId && x.SORT_ORDER == _sort);

                                                //return _RecordsAffected;
                                            }
                                        }
                                    }
                                }
                            }
                            if (catcount != 0 && famcount != 0 && dupitemno == 0)
                            {
                                try
                                {
                                    using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                    {
                                        SqlCon.Open();
                                        SqlCommand Sqlcom1 = new SqlCommand("SET NUMERIC_ROUNDABORT OFF ", SqlCon);
                                        int flag = Sqlcom1.ExecuteNonQuery();
                                        SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_PRODUCT\r\n                      ( CATEGORY_ID)\r\nVALUES     ('" + _categoryId + "')\r\n", SqlCon);
                                        //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                        // da.Fill(tbCategory);
                                        int flag1 = Sqlcom.ExecuteNonQuery();
                                        SqlCon.Close();
                                    }
                                    //var tbProduct = new TB_PRODUCT
                                    //{
                                    //    CATEGORY_ID = _categoryId,
                                    //    SINGLE_OR_KIT = false,
                                    //    STATUS = null,
                                    //    CREATED_USER = User.Identity.Name,
                                    //    CREATED_DATE = DateTime.Now,
                                    //    MODIFIED_USER = User.Identity.Name,
                                    //    MODIFIED_DATE = DateTime.Now,
                                    //};
                                    //objLS.TB_PRODUCT.Add(tbProduct);
                                    //objLS.SaveChanges();

                                    _RecordsAffected = objLS.TB_PRODUCT.Count(x => x.CATEGORY_ID == _categoryId);
                                    _ProductId = objLS.TB_PRODUCT.Where(x => x.CATEGORY_ID == _categoryId).Max(y => y.PRODUCT_ID);
                                    // objLS.Dispose();
                                    objLS = new CSEntities();
                                    var tbProdSpecs = new TB_PROD_SPECS
                                    {
                                        STRING_VALUE = _catalogItemNo,
                                        PRODUCT_ID = _ProductId,
                                        ATTRIBUTE_ID = 1,
                                        OBJECT_TYPE = null,
                                        NUMERIC_VALUE = null,
                                        OBJECT_NAME = null,
                                        CREATED_USER = User.Identity.Name,
                                        CREATED_DATE = DateTime.Now,
                                        MODIFIED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now,
                                    };
                                    objLS.TB_PROD_SPECS.Add(tbProdSpecs);
                                    objLS.SaveChanges();

                                    _RecordsAffected =
                                        objLS.TB_PROD_SPECS.Count(
                                            x =>
                                                x.ATTRIBUTE_ID == 1 && x.STRING_VALUE == _catalogItemNo &&
                                                x.PRODUCT_ID == _ProductId);
                                }
                                catch (Exception ex)
                                {
                                }
                                int _sort = 1;
                                if (_productSort == 0)
                                {

                                    try
                                    {
                                        var sort = objLS.TB_PROD_FAMILY.Where(y => y.FAMILY_ID == _FamilyId).OrderByDescending(x => x.SORT_ORDER).Select(z => z.SORT_ORDER).FirstOrDefault();
                                        _sort = sort + 1;
                                    }
                                    catch (Exception)
                                    { }
                                }
                                else
                                {
                                    _sort = _ProductSort;
                                }

                                try
                                {
                                    var tbProdFamily = new TB_PROD_FAMILY
                                    {
                                        SORT_ORDER = _sort,
                                        FAMILY_ID = _FamilyId,
                                        PRODUCT_ID = _ProductId,
                                        CREATED_USER = User.Identity.Name,
                                        CREATED_DATE = DateTime.Now,
                                        MODIFIED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now,
                                        PUBLISH = true,
                                        PUBLISH2PRINT = true,
                                        PUBLISH2CD = true,
                                        WORKFLOW_STATUS = 4,
                                        WORKFLOW_COMMENTS = null,
                                        FLAG_RECYCLE = "A"
                                    };
                                    objLS.TB_PROD_FAMILY.Add(tbProdFamily);
                                    objLS.SaveChanges();
                                    _RecordsAffected =
                                               objLS.TB_PROD_FAMILY.Count(x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == _ProductId && x.SORT_ORDER == _sort);
                                    _productId = _ProductId;
                                }
                                catch (Exception)
                                { }
                            }
                            objLS = new CSEntities();
                            objLS.Database.CommandTimeout = 0;
                            if ((objLS.TB_PROD_FAMILY_ATTR_LIST.Count(x => x.FAMILY_ID == _FamilyId)) == 0 || (objLS.TB_PROD_FAMILY_ATTR_LIST.Count(x => x.FAMILY_ID == _FamilyId && x.ATTRIBUTE_ID == 1)) > 0)
                            {
                                int Sort = 0;
                                try
                                {
                                    var sort =
                                        objLS.TB_PROD_FAMILY_ATTR_LIST.Where(
                                            x => x.ATTRIBUTE_ID == 1 && x.FAMILY_ID == _FamilyId).OrderByDescending(x => x.SORT_ORDER).Select(y => y.SORT_ORDER).FirstOrDefault();

                                    Sort = sort;
                                }
                                catch (Exception)
                                {
                                    try
                                    {
                                        Sort = objLS.TB_PROD_FAMILY_ATTR_LIST.Where(
                                            x => x.ATTRIBUTE_ID == 1 && x.FAMILY_ID == _FamilyId).OrderByDescending(z => z.SORT_ORDER)
                                            .Max(y => y.SORT_ORDER) + 1;
                                    }
                                    catch (Exception)
                                    {
                                        Sort = 1;
                                    }
                                }
                                try
                                {
                                    var prodattr =
                                        objLS.TB_PROD_FAMILY_ATTR_LIST.Where(
                                            x =>
                                                x.ATTRIBUTE_ID == 1 && x.FAMILY_ID == _FamilyId &&
                                                x.PRODUCT_ID == 0).Select(y => y.SORT_ORDER).ToList();
                                    if (!prodattr.Any())
                                    {
                                        if (Sort == 0)
                                        {
                                            Sort = 1;
                                        }
                                        objLS = new CSEntities();
                                        var tbProdFamilyAttrList = new TB_PROD_FAMILY_ATTR_LIST
                                        {
                                            ATTRIBUTE_ID = 1,
                                            SORT_ORDER = Sort,
                                            FAMILY_ID = _FamilyId,
                                            PRODUCT_ID = 0,
                                            CREATED_USER = User.Identity.Name,
                                            CREATED_DATE = DateTime.Now,
                                            MODIFIED_USER = User.Identity.Name,
                                            MODIFIED_DATE = DateTime.Now
                                        };
                                        objLS.TB_PROD_FAMILY_ATTR_LIST.Add(tbProdFamilyAttrList);
                                        objLS.SaveChanges();
                                    }
                                }
                                catch (Exception)
                                {
                                }
                            }
                            DataTable cscftb = new DataTable();
                            if (_CatalogId != 1 && _ProductId > 0)
                                if (objLS.TB_CATALOG_PRODUCT.Count(x => x.CATALOG_ID == _catalogId && x.PRODUCT_ID == _ProductId) == 0)
                                {
                                    objLS = new CSEntities();
                                    TB_CATALOG_PRODUCT tbCatalogProduct = new TB_CATALOG_PRODUCT
                                    {
                                        CATALOG_ID = _catalogId,
                                        PRODUCT_ID = _ProductId,
                                        CREATED_USER = User.Identity.Name,
                                        CREATED_DATE = DateTime.Now,
                                        MODIFIED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now
                                    };
                                    objLS.TB_CATALOG_PRODUCT.Add(tbCatalogProduct);
                                    objLS.SaveChanges();
                                    _RecordsAffected =
                                        objLS.TB_CATALOG_PRODUCT.Count(
                                            x => x.CATALOG_ID == _catalogId && x.PRODUCT_ID == _ProductId);
                                }

                            var ss1 =
                                objLS.TB_CATALOG_FAMILY.Where(
                                    x => x.FAMILY_ID == _FamilyId && x.CATALOG_ID != _catalogId && x.CATALOG_ID != 1).ToList();
                            cscftb = ToDataTable(ss1);
                            if (cscftb.Rows.Count > 0)
                            {
                                foreach (DataRow Drow in cscftb.Rows)
                                {
                                    try
                                    {
                                        int _catalogId1;
                                        int.TryParse(Drow["Catalog_id"].ToString(), out _catalogId1);
                                        if (objLS.TB_CATALOG_PRODUCT.Count(x => x.CATALOG_ID == _catalogId1 && x.PRODUCT_ID == _ProductId) == 0)
                                        {
                                            var tbCatalogProduct = new TB_CATALOG_PRODUCT
                                            {
                                                CATALOG_ID = _catalogId1,
                                                PRODUCT_ID = _ProductId,
                                                CREATED_USER = User.Identity.Name,
                                                CREATED_DATE = DateTime.Now,
                                                MODIFIED_USER = User.Identity.Name,
                                                MODIFIED_DATE = DateTime.Now
                                            };
                                            objLS.TB_CATALOG_PRODUCT.Add(tbCatalogProduct);
                                            objLS.SaveChanges();
                                            _RecordsAffected =
                                                objLS.TB_CATALOG_PRODUCT.Count(
                                                    x => x.CATALOG_ID == _catalogId && x.PRODUCT_ID == _ProductId);
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }
                            }

                            return _RecordsAffected;

                        }
                        //else
                        // throw new TradingBell.ExIm.CSUtil.CSException("Family ID cannot be empty");
                    }

                }
                // else
                //throw new TradingBell.ExIm.CSUtil.CSException("Catalog Item No cannot be empty");
            }
            else
                if (_catalogItemNo != "")
            {
                TB_PROD_SPECS tbProdSpecs = objLS.TB_PROD_SPECS.Where(x => x.ATTRIBUTE_ID == 1 && x.PRODUCT_ID == _ProductId)
                        .FirstOrDefault();
                if (tbProdSpecs != null)
                {
                    tbProdSpecs.STRING_VALUE = _catalogItemNo;
                    tbProdSpecs.OBJECT_TYPE = null;
                }
                objLS.SaveChanges();

                _RecordsAffected = objLS.TB_PROD_SPECS.Count(x => x.ATTRIBUTE_ID == 1 && x.PRODUCT_ID == _ProductId);
                return _RecordsAffected;
            }

            return _RecordsAffected;
        }


        int _AttributeId = 0;
        int _RecordsAffected = 0;
        public string InsertUpdateAttribute(DataTable dt)
        {
            int coli = 0;
            foreach (DataRow dr in dt.Rows)
            {
                int iFindDup = 0;
                //Excel Range is not defined, so lets find the actual position of the column and set it
                dr["ExcelRange"] = "";
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (dt.Columns[i].ToString() != null)
                    {
                        String sColName = dt.Columns[i].ToString().Trim();
                        if (sColName == dr["ExcelColumn"].ToString())
                        {
                            //coli = i;
                            //String sRange = wc.Range.WorkbookSet.GetAddress(0, i);
                            //if (dr["ExcelRange"].ToString() != "")
                            //{
                            //    dr["ExcelRange"] = sRange + ", " + dr["ExcelRange"];
                            //}
                            //else
                            //{
                            //    dr["ExcelRange"] = sRange;
                            //}
                            //MImpXml.SetValue(sColName, "ExcelRange", sRange);

                            //Lets check if the CatalogMapField is set in the import file
                            //if it is mapped, then attribute exists in tb_attribute and also catalogmapfield will have a value
                            //if not, the we need to create it, only if it is a non system attribute
                            if (dr["CatalogMapField"].ToString() == "")
                            {
                                //check if it is a system attribute
                                if (!(bool)dr["IsSystemField"])
                                {
                                    //Create the custom defined attribute in the database

                                    string AttributeName = dr["ExcelColumn"].ToString();
                                    int AttributeType = (int)dr["FieldType"];

                                    AttributeDataType = AttributeType == 4 ? "Number(13,6)" : "Text";
                                    int recordsAffected = AttributeIU();
                                    //if (recordsAffected > 0)
                                    //{
                                    //    LogTable(0, 0, "Column (Attribute) created in database or it exists in database", dr["ExcelColumn"].ToString(), 1007);
                                    //    //XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, 0, 0, "[" + dr["ExcelRange"].ToString() + "] " + FieldHelper.GetEnumName(FieldHelper.IMPStatus.NEW_ATTR_CREATED_OR_IN_DB), dr["ExcelColumn"].ToString(), (int)FieldHelper.IMPStatus.NEW_ATTR_CREATED_OR_IN_DB);
                                    //    //ImportProcessLogWriter(XLSProAgrs);
                                    //}
                                    //else
                                    //{
                                    //    LogTable(0, 0, "Failed to create column (Attribute) in database", dr["ExcelColumn"].ToString(), 1008);
                                    //    // XLSProcessEventArgs XLSProAgrs = new XLSProcessEventArgs(2, 0, 0, "[" + dr["ExcelRange"].ToString() + "] " + FieldHelper.GetEnumName(FieldHelper.IMPStatus.ATTR_CREATE_FAILED), dr["ExcelColumn"].ToString(), (int)FieldHelper.IMPStatus.ATTR_CREATE_FAILED);
                                    //    // ImportProcessLogWriter(XLSProAgrs);
                                    //    // bPass = false;
                                }
                            }
                        }
                        iFindDup++;
                    }
                    //}
                    //else
                    //{
                    //    iColCount = i;
                    //}
                }
            }
            return null;
        }
        int _ProductId = 0;
        public string AttributeValue
        {
            get
            {
                return attributeValue;
            }
            set
            {
                attributeValue = value;
            }
        }

        private int _FamilyId;
        public int FamilyId
        {
            get
            {
                return _FamilyId;
            }
            set
            {
                _FamilyId = value;
            }
        }

        private string _FamilyName = "";
        public string FamilyName
        {
            set
            {
                _FamilyName = value;
            }
        }

        private string _familyName = "";
        public string _FamilyName1
        {
            get
            {
                return _familyName;
            }
            set
            {
                _familyName = value;
            }
        }
        string _AttributeValue = "";

        private string _CatalogItemno;
        public string CatalogItemno
        {
            get
            {
                return _CatalogItemno;
            }
            set
            {
                _CatalogItemno = value;
            }
        }

        private int _CatalogId;
        public int CatalogId
        {
            get
            {
                return _CatalogId;
            }
            set
            {
                _CatalogId = value;
            }
        }

        public int AttributeIU()
        {
            int count;
            if (_AttributeName != "")
            {
                var customId_ =
                                   objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name)
                                       .Select(y => y.CustomerId)
                                       .ToList();
                int customerId_ = 0;
                if (customId_.Any())
                {
                    int.TryParse(customId_[0].ToString(), out customerId_);

                    int.TryParse(
                        objLS.TB_ATTRIBUTE.Join(objLS.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                            tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf }).Where(
                                x =>
                                    x.tcp.ATTRIBUTE_NAME == _AttributeName &&
                                    x.tpf.CUSTOMER_ID == customerId_).Select(y => y.tpf.ATTRIBUTE_ID)
                            .FirstOrDefault().ToString(), out _AttributeId);
                }
                if (_AttributeId == 0)
                {
                    string dformat = "";
                    if (_AttributeType != 4)
                        dformat = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
                    else
                        dformat = "^-{0,1}?\\d*\\.{0,1}\\d{0,6}$";

                    _AttributeDataType = AttributeType == 4 ? "Number(13,6)" : "Text";
                    if (AttributeType != 0 && _AttributeDataType != null)
                    {
                        byte attrType = (byte)AttributeType;
                        try
                        {
                            DataTable att = new DataTable();
                            using (SqlConnection SqlCon = new SqlConnection(connectionString))
                            {
                                SqlCon.Open();
                                SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_ATTRIBUTE (ATTRIBUTE_NAME, ATTRIBUTE_TYPE, " +
                "ATTRIBUTE_DATATYPE, ATTRIBUTE_DATAFORMAT,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" + _AttributeName + "'," + attrType + ",'" + _AttributeDataType + "','" + dformat + "','" + User.Identity.Name + "',Getdate(),'" + User.Identity.Name + "',Getdate()); select Attribute_id from TB_ATTRIBUTE where Attribute_name='" + _AttributeName + "'", SqlCon);
                                SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                da.Fill(att);
                                //int flag = Sqlcom.ExecuteNonQuery();
                                SqlCon.Close();
                            }
                            //TB_ATTRIBUTE tbAttribute = new TB_ATTRIBUTE
                            //{
                            //    ATTRIBUTE_NAME = _AttributeName,
                            //    ATTRIBUTE_TYPE = attrType,
                            //    CREATE_BY_DEFAULT = true,
                            //    VALUE_REQUIRED = false,
                            //    STYLE_NAME = string.Empty,
                            //    STYLE_FORMAT = null,
                            //    DEFAULT_VALUE = null,
                            //    PUBLISH2PRINT = true,
                            //    PUBLISH2WEB = true,
                            //    PUBLISH2CDROM = true,
                            //    PUBLISH2ODP = true,
                            //    USE_PICKLIST = false,
                            //    ATTRIBUTE_DATATYPE = _AttributeDataType,
                            //    ATTRIBUTE_DATAFORMAT = dformat,
                            //    ATTRIBUTE_DATARULE = null,
                            //    UOM = null,
                            //    IS_CALCULATED = false,
                            //    ATTRIBUTE_CALC_FORMULA = null,
                            //    PICKLIST_NAME = null,
                            //    CREATED_USER = User.Identity.Name,
                            //    CREATED_DATE = DateTime.Now,
                            //    MODIFIED_USER = User.Identity.Name,
                            //    MODIFIED_DATE = DateTime.Now
                            //};
                            //objLS.TB_ATTRIBUTE.Add(tbAttribute);
                            //objLS.SaveChanges();
                            //_AttributeId = tbAttribute.ATTRIBUTE_ID;
                            if (att != null && att.Rows.Count > 0)
                            {
                                int.TryParse(att.Rows[0][0].ToString(), out _AttributeId);
                                var customId =
                                    objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name)
                                        .Select(y => y.CustomerId)
                                        .ToList();
                                int customerId = 0;
                                if (customId.Any())
                                {
                                    int.TryParse(customId[0].ToString(), out customerId);
                                    if (customerId != 0)
                                    {
                                        var customerAttribute = new CUSTOMERATTRIBUTE
                                        {
                                            CUSTOMER_ID = customerId,
                                            ATTRIBUTE_ID = _AttributeId,
                                            CREATED_USER = User.Identity.Name,
                                            CREATED_DATE = DateTime.Now,
                                            MODIFIED_USER = User.Identity.Name,
                                            MODIFIED_DATE = DateTime.Now
                                        };
                                        objLS.CUSTOMERATTRIBUTE.Add(customerAttribute);
                                        objLS.SaveChanges();
                                    }
                                }
                                _RecordsAffected = 1;
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }

                customId_ =
                                   objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name)
                                       .Select(y => y.CustomerId)
                                       .ToList();
                customerId_ = 0;
                if (customId_.Any())
                {
                    int.TryParse(customId_[0].ToString(), out customerId_);
                    int.TryParse(
                        objLS.TB_ATTRIBUTE.Join(objLS.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                            tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf }).Where(
                                x =>
                                    x.tcp.ATTRIBUTE_NAME == _AttributeName &&
                                    x.tpf.CUSTOMER_ID == customerId_).Select(y => y.tpf.ATTRIBUTE_ID)
                            .FirstOrDefault().ToString(), out _AttributeId);
                }
                //int.TryParse(
                //    objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == _AttributeName)
                //        .Select(y => y.ATTRIBUTE_ID)
                //        .FirstOrDefault().ToString(), out _AttributeId);

                if (_AttributeId == 3)
                {
                    if (!objLS.TB_ATTRIBUTE.Select(x => x.ATTRIBUTE_ID == 3 && x.DEFAULT_VALUE == AttributeValue)
                        .ToList()
                        .Any())
                    {
                        using (SqlConnection SqlCon = new SqlConnection(connectionString))
                        {
                            SqlCon.Open();
                            SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_SUPPLIER\r\n  (SUPPLIER_NAME,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" + _AttributeValue + "','" + User.Identity.Name + "',Getdate(),'" + User.Identity.Name + "',Getdate());", SqlCon);
                            //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                            //da.Fill(att);
                            int flag = Sqlcom.ExecuteNonQuery();
                            SqlCon.Close();
                        }
                        //TB_SUPPLIER tbSupplier = new TB_SUPPLIER
                        //{
                        //    SUPPLIER_NAME = _AttributeValue,
                        //    ADDRESS_LINE_1 = null,
                        //    ADDRESS_LINE_2 = null,
                        //    ADDRESS_LINE_3 = null,
                        //    CITY = null,
                        //    EMAIL = null,
                        //    FAX = null,
                        //    LOGO_IMAGE_FILE = null,
                        //    LOGO_IMAGE_TYPE = null,
                        //    PHONE_1 = null,
                        //    STATE = null,
                        //    SUPPLIER_COMPANY_NAME = null,
                        //    URL = null,
                        //    ZIP = null,
                        //    CREATED_USER = User.Identity.Name,
                        //    CREATED_DATE = DateTime.Now,
                        //    MODIFIED_USER = User.Identity.Name,
                        //    MODIFIED_DATE = DateTime.Now
                        //};
                    }
                }
                ////************update the attribute value *****************////
                if (_CatalogId != 0)
                {

                    if (!objLS.TB_CATALOG_ATTRIBUTES.Where(
                        x => x.ATTRIBUTE_ID == _AttributeId && x.CATALOG_ID == _CatalogId).ToList().Any())
                    {
                        //try
                        //{
                        using (SqlConnection SqlCon = new SqlConnection(connectionString))
                        {
                            SqlCon.Open();
                            SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_CATALOG_ATTRIBUTES\r\n  (CATALOG_ID, ATTRIBUTE_ID,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" + _CatalogId + "','" + _AttributeId + "','" + User.Identity.Name + "',Getdate(),'" + User.Identity.Name + "',Getdate());", SqlCon);
                            //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                            //da.Fill(att);
                            int flag = Sqlcom.ExecuteNonQuery();
                            SqlCon.Close();
                        }
                        //var objCatalogAttributes = new TB_CATALOG_ATTRIBUTES
                        //{
                        //    CATALOG_ID = _CatalogId,
                        //    ATTRIBUTE_ID = _AttributeId,
                        //    CREATED_USER = User.Identity.Name,
                        //    CREATED_DATE = DateTime.Now,
                        //    MODIFIED_USER = User.Identity.Name,
                        //    MODIFIED_DATE = DateTime.Now
                        //};
                        //objLS.TB_CATALOG_ATTRIBUTES.Add(objCatalogAttributes);
                        //objLS.SaveChanges();
                        //}
                        //catch(Exception ex)
                        //{

                        //}
                    }
                    var customId =
                                objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name)
                                    .Select(y => y.CustomerId)
                                    .ToList();
                    int customerId = 0;
                    if (customId.Any())
                    {
                        int.TryParse(customId[0].ToString(), out customerId);
                        if (customerId != 0)
                        {
                            var custAtt =
                                objLS.CUSTOMERATTRIBUTE.Where(
                                    x => x.ATTRIBUTE_ID == _AttributeId && x.CUSTOMER_ID == customerId).ToList();
                            if (!custAtt.Any())
                            {
                                var customerAttribute = new CUSTOMERATTRIBUTE
                                {
                                    CUSTOMER_ID = customerId,
                                    ATTRIBUTE_ID = _AttributeId,
                                    CREATED_USER = User.Identity.Name,
                                    CREATED_DATE = DateTime.Now,
                                    MODIFIED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now
                                };
                                objLS.CUSTOMERATTRIBUTE.Add(customerAttribute);
                                objLS.SaveChanges();
                            }
                        }
                    }
                }
                int loopproduct = 0;
                DataTable tb_fam = new DataTable();
                if (_FamilyId != 0)
                {
                    sortfam.Rows.Add(_FamilyId);

                }

                if (_FamilyId == 0)
                {
                    if (_FamilyName != "")
                    {
                        var ss =
                            objLS.TB_FAMILY.Join(objLS.TB_FAMILY_TABLE_STRUCTURE, tf => tf.FAMILY_ID,
                                tfts => tfts.FAMILY_ID, (tf, tfts) => new { tf, tfts })
                                .Where(x => x.tf.FAMILY_NAME == _FamilyName).ToList();
                        tb_fam = ToDataTable(ss);
                        int.TryParse(tb_fam.Rows.Count.ToString(), out loopproduct);
                    }
                }


                int i = 0;
                do
                {
                    if (loopproduct > 0)
                    {
                        _FamilyId = Convert.ToInt32(tb_fam.Rows[i]["FAMILY_ID"]);
                    }
                    string type = objLS.TB_ATTRIBUTE.Select(x => x.ATTRIBUTE_ID == _AttributeId).ToList()[0].ToString();
                    _AttributeType = Convert.ToInt32(objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == _AttributeId).Select(a => a.ATTRIBUTE_TYPE).FirstOrDefault());
                    string atdtyp = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == _AttributeId).Select(a => a.ATTRIBUTE_DATATYPE).FirstOrDefault();
                    if (_AttributeType > 6)
                    {
                        if (_FamilyId != 0)
                        {
                            if (objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId) != 0)
                            {
                                int counts =
                                    objLS.TB_FAMILY_SPECS.Count(
                                        x => x.ATTRIBUTE_ID == _AttributeId && x.FAMILY_ID == _FamilyId);
                                if (type.Substring(0, 1).ToString().ToUpper() != 'N'.ToString())
                                {
                                    string imgtype = null;
                                    string imgfilename = null;
                                    if (_AttributeType == 9)
                                    {
                                        string[] images = _AttributeValue.Split('.');
                                        if (images.Length > 1)
                                        {
                                            string[] imagename = _AttributeValue.Split('[');
                                            images = images[images.Length - 1].Split('[');
                                            imgtype = images[0];
                                            if (imagename.Length > 1)
                                            {
                                                _AttributeValue = imagename[0];
                                                imagename = imagename[1].Split(':');
                                                imgfilename = imagename[1].Remove(imagename[1].Length - 1);
                                            }
                                        }
                                    }
                                    if (counts == 0)
                                    {
                                        try
                                        {
                                            //    if (_AttributeId == 0)
                                            //    {
                                            //        var tbAttribute = new TB_ATTRIBUTE
                                            //        {
                                            //            ATTRIBUTE_NAME = _AttributeName,
                                            //            ATTRIBUTE_TYPE = _AttributeType,
                                            //            CREATE_BY_DEFAULT = false,
                                            //            VALUE_REQUIRED = false,
                                            //            STYLE_NAME = string.Empty,
                                            //            STYLE_FORMAT = null,
                                            //            DEFAULT_VALUE = null,
                                            //        };
                                            //    }
                                            //_RecordsAffected = Fspec.InsertStrFamilySpecs(_AttributeValue, _FamilyId, _AttributeId, imgtype, imgfilename);
                                            // imgtype = Path.GetExtension(_AttributeValue).Replace(".","");
                                            using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                            {
                                                SqlCon.Open();
                                                SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_FAMILY_SPECS\r\n  (STRING_VALUE, FAMILY_ID, ATTR" +
            "IBUTE_ID, OBJECT_TYPE, OBJECT_NAME,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" + _AttributeValue + "'," + _FamilyId + "," + _AttributeId + ",'" + imgtype + "','" + imgfilename + "','" + User.Identity.Name + "',Getdate(),'" + User.Identity.Name + "',Getdate());", SqlCon);
                                                //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                                //da.Fill(att);
                                                int flag = Sqlcom.ExecuteNonQuery();
                                                SqlCon.Close();
                                            }
                                            //TB_FAMILY_SPECS tbFamilySpecs = new TB_FAMILY_SPECS
                                            //{
                                            //    STRING_VALUE = _AttributeValue,
                                            //    FAMILY_ID = _FamilyId,
                                            //    ATTRIBUTE_ID = _AttributeType,
                                            //    OBJECT_TYPE = imgtype,
                                            //    OBJECT_NAME = imgfilename,
                                            //    NUMERIC_VALUE = null,
                                            //    CREATED_USER = User.Identity.Name,
                                            //    CREATED_DATE = DateTime.Now,
                                            //    MODIFIED_USER = User.Identity.Name,
                                            //    MODIFIED_DATE = DateTime.Now
                                            //};
                                            //objLS.TB_FAMILY_SPECS.Add(tbFamilySpecs);
                                            //objLS.SaveChanges();
                                            _RecordsAffected = objLS.TB_FAMILY_SPECS.Count(
                                                x =>
                                                    x.ATTRIBUTE_ID == _AttributeId && x.FAMILY_ID == _FamilyId &&
                                                    x.STRING_VALUE == _AttributeValue);
                                            //return _RecordsAffected;
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {

                                            TB_FAMILY_SPECS tbFamilySpecs =
                                                objLS.TB_FAMILY_SPECS.Where(
                                                    x => x.FAMILY_ID == _FamilyId && x.ATTRIBUTE_ID == _AttributeId)
                                                    .First();
                                            tbFamilySpecs.STRING_VALUE = _AttributeValue;
                                            tbFamilySpecs.OBJECT_TYPE = imgtype;
                                            tbFamilySpecs.OBJECT_NAME = imgfilename;
                                            objLS.SaveChanges();
                                            _RecordsAffected = objLS.TB_FAMILY_SPECS.Count(
                                                x =>
                                                    x.ATTRIBUTE_ID == _AttributeId && x.FAMILY_ID == _FamilyId &&
                                                    x.STRING_VALUE == _AttributeValue && x.OBJECT_TYPE == imgtype &&
                                                    x.OBJECT_NAME == imgfilename);

                                            // return _RecordsAffected;
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                }
                                else
                                {
                                    if (counts == 0)
                                    {
                                        try
                                        {
                                            System.Nullable<decimal> num = Convert.ToDecimal(_AttributeValue);
                                            //_RecordsAffected = Fspec.InsertNumFamilySpecs((decimal)num, _FamilyId, _AttributeId);


                                            using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                            {
                                                SqlCon.Open();
                                                SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_FAMILY_SPECS\r\n  (STRING_VALUE, FAMILY_ID, ATTR" +
            "IBUTE_ID, OBJECT_TYPE, OBJECT_NAME,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" + _AttributeValue + "'," + _FamilyId + "," + _AttributeId + ",'','','" + User.Identity.Name + "',Getdate(),'" + User.Identity.Name + "',Getdate());", SqlCon);
                                                //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                                //da.Fill(att);
                                                int flag = Sqlcom.ExecuteNonQuery();
                                                SqlCon.Close();
                                            }
                                            //TB_FAMILY_SPECS tbFamilySpecs = new TB_FAMILY_SPECS
                                            //{
                                            //    NUMERIC_VALUE = num,
                                            //    FAMILY_ID = _FamilyId,
                                            //    ATTRIBUTE_ID = _AttributeType,
                                            //    OBJECT_TYPE = null,
                                            //    OBJECT_NAME = null,
                                            //    STRING_VALUE = null,
                                            //    CREATED_USER = User.Identity.Name,
                                            //    CREATED_DATE = DateTime.Now,
                                            //    MODIFIED_USER = User.Identity.Name,
                                            //    MODIFIED_DATE = DateTime.Now

                                            //};
                                            //objLS.TB_FAMILY_SPECS.Add(tbFamilySpecs);
                                            //objLS.SaveChanges();
                                            _RecordsAffected = objLS.TB_FAMILY_SPECS.Count(
                                                x =>
                                                    x.ATTRIBUTE_ID == _AttributeId && x.FAMILY_ID == _FamilyId &&
                                                    x.NUMERIC_VALUE == num);
                                            //return _RecordsAffected;

                                            //return _RecordsAffected;
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            System.Nullable<decimal> num = Convert.ToDecimal(_AttributeValue);
                                            TB_FAMILY_SPECS tbFamilySpecs =
                                                objLS.TB_FAMILY_SPECS.Where(
                                                    x => x.FAMILY_ID == _FamilyId && x.ATTRIBUTE_ID == _AttributeId)
                                                    .First();
                                            tbFamilySpecs.NUMERIC_VALUE = num;

                                            objLS.SaveChanges();
                                            _RecordsAffected = objLS.TB_FAMILY_SPECS.Count(
                                                x =>
                                                    x.ATTRIBUTE_ID == _AttributeId && x.FAMILY_ID == _FamilyId &&
                                                    x.NUMERIC_VALUE == num);


                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (_AttributeType == 6)
                    {
                        if (_ProductId != 0)
                        {
                            int isproduct = objLS.TB_PRODUCT.Count(x => x.PRODUCT_ID == _ProductId);
                            int famcount = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);

                            if (isproduct == 1 && famcount != 0)
                            {
                                int PartCount =
                                    objLS.TB_PARTS_KEY.Count(
                                        x =>
                                            x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_ID == _AttributeId &&
                                            x.FAMILY_ID == _FamilyId);
                                if (PartCount == 0)
                                {
                                    using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                    {
                                        SqlCon.Open();
                                        SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_PARTS_KEY\r\n (ATTRIBUTE_ID, PRODUCT_ID, FAMILY_ID" +
            ", ATTRIBUTE_VALUE,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE],[CATALOG_ID],[CATEGORY_ID])\r\n values(" + _AttributeId + "," + _ProductId + "," + _FamilyId + ",'" + _AttributeValue + "','" + User.Identity.Name + "',GETDATE(),'" + User.Identity.Name + "',GETDATE()," + _currentCatalogId + ",'" + _currentCategoryId + "');", SqlCon);
                                        //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                        //da.Fill(att);
                                        int flag = Sqlcom.ExecuteNonQuery();
                                        SqlCon.Close();
                                    }
                                    //TB_PARTS_KEY tbPartsKey = new TB_PARTS_KEY
                                    //{
                                    //    ATTRIBUTE_ID = _AttributeId,
                                    //    PRODUCT_ID = _ProductId,
                                    //    FAMILY_ID = _FamilyId,
                                    //    ATTRIBUTE_VALUE = _AttributeValue,
                                    //    CATEGORY_ID = _categoryId,
                                    //    CREATED_USER = User.Identity.Name,
                                    //    CREATED_DATE = DateTime.Now,
                                    //    MODIFIED_USER = User.Identity.Name,
                                    //    MODIFIED_DATE = DateTime.Now
                                    //};
                                    //objLS.TB_PARTS_KEY.Add(tbPartsKey);
                                    //objLS.SaveChanges();
                                    _RecordsAffected = objLS.TB_PARTS_KEY.Count(
                                        x =>
                                            x.ATTRIBUTE_ID == _AttributeId && x.FAMILY_ID == _FamilyId &&
                                            x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_VALUE == _AttributeValue);


                                    if (_FamilyId != 0)
                                    {
                                        int countpf =
                                            objLS.TB_PROD_FAMILY.Count(
                                                x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == _ProductId);
                                        if (countpf == 0)
                                        {
                                            using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                            {
                                                SqlCon.Open();
                                                SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_PROD_FAMILY\r\n   (SORT_ORDER, FAMILY_ID, PRODUCT" +
            "_ID, PUBLISH,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values(" + 1 + "," + _FamilyId + "," + _ProductId + ",'" + Convert.ToBoolean(1) + "','" + User.Identity.Name + "',Getdate(),'" + User.Identity.Name + "',Getdate());", SqlCon);
                                                //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                                //da.Fill(att);
                                                int flag = Sqlcom.ExecuteNonQuery();
                                                SqlCon.Close();
                                            }
                                            //TB_PROD_FAMILY tbProdFamily = new TB_PROD_FAMILY
                                            //{
                                            //    SORT_ORDER = 1,
                                            //    FAMILY_ID = _FamilyId,
                                            //    PRODUCT_ID = _ProductId,
                                            //    PUBLISH = Convert.ToBoolean(1),
                                            //    CREATED_USER = User.Identity.Name,
                                            //    CREATED_DATE = DateTime.Now,
                                            //    MODIFIED_USER = User.Identity.Name,
                                            //    MODIFIED_DATE = DateTime.Now,
                                            //    PUBLISH2PRINT = true,
                                            //    PUBLISH2CD = true,
                                            //    WORKFLOW_STATUS = 4,
                                            //    WORKFLOW_COMMENTS = null
                                            //};
                                            //objLS.TB_PROD_FAMILY.Add(tbProdFamily);
                                            //objLS.SaveChanges();
                                        }
                                        //pft.InsertProdFamily(1, _FamilyId, _ProductId, Convert.ToBoolean(1));
                                        // pfat.InsertProdFamilyAttr(_AttributeId, sort, _FamilyId, _ProductId);

                                    }
                                    // return _RecordsAffected;
                                }
                                else
                                {
                                    TB_PARTS_KEY tbPartsKey =
                                        objLS.TB_PARTS_KEY.Where(
                                            x =>
                                                x.ATTRIBUTE_ID == _AttributeId && x.FAMILY_ID == _FamilyId &&
                                                x.PRODUCT_ID == _ProductId).First();
                                    tbPartsKey.ATTRIBUTE_VALUE = _AttributeValue;
                                    objLS.SaveChanges();
                                    _RecordsAffected = objLS.TB_PARTS_KEY.Count(
                                        x =>
                                            x.ATTRIBUTE_ID == _AttributeId && x.FAMILY_ID == _FamilyId &&
                                            x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_VALUE == _AttributeValue);
                                    // return _RecordsAffected;
                                }

                                if (_FamilyId != 0 && PartCount == 0)
                                {
                                    if (
                                        (objLS.TB_PROD_FAMILY_ATTR_LIST.Count(
                                            x => x.FAMILY_ID == _FamilyId && x.ATTRIBUTE_ID == _AttributeId) > 0) ||
                                        (objLS.TB_PROD_FAMILY_ATTR_LIST.Count(
                                            x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == 0) > 0))
                                    {
                                        int sort = 1;
                                        try
                                        {
                                            int.TryParse(
                                                objLS.TB_PROD_FAMILY_ATTR_LIST.Where(
                                                    x => x.FAMILY_ID == _FamilyId && x.ATTRIBUTE_ID == _AttributeId)
                                                    .Select(y => y.SORT_ORDER)
                                                    .ToList()[0].ToString(), out sort);

                                        }
                                        catch (Exception)
                                        {
                                            try
                                            {
                                                int maxsort;
                                                int.TryParse(
                                                    objLS.TB_PROD_FAMILY_ATTR_LIST.OrderByDescending(
                                                        z => z.SORT_ORDER)
                                                        .Where(
                                                            x =>
                                                                x.FAMILY_ID == _FamilyId &&
                                                                x.ATTRIBUTE_ID == _AttributeId)
                                                        .Select(y => y.SORT_ORDER)
                                                        .FirstOrDefault()
                                                        .ToString(), out maxsort);
                                                sort = maxsort + 1;

                                            }
                                            catch (Exception)
                                            {
                                                sort = 1;
                                            }

                                        }
                                        try
                                        {
                                            using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                            {
                                                SqlCon.Open();
                                                SqlCommand Sqlcom = new SqlCommand("INSERT INTO TB_PROD_FAMILY_ATTR_LIST\r\n (ATTRIBUTE_ID, SORT_ORDER" +
            ", FAMILY_ID, PRODUCT_ID,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" + _AttributeId + "'," + sort + "," + _FamilyId + ",0,'" + User.Identity.Name + "',Getdate(),'" + User.Identity.Name + "',Getdate());", SqlCon);
                                                //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                                //da.Fill(att);
                                                int flag = Sqlcom.ExecuteNonQuery();
                                                SqlCon.Close();
                                            }
                                            //TB_PROD_FAMILY_ATTR_LIST tbProdFamilyAttrList =
                                            //    new TB_PROD_FAMILY_ATTR_LIST
                                            //    {
                                            //        ATTRIBUTE_ID = _AttributeId,
                                            //        SORT_ORDER = sort,
                                            //        FAMILY_ID = _FamilyId,
                                            //        PRODUCT_ID = _ProductId,
                                            //        CREATED_USER = User.Identity.Name,
                                            //        CREATED_DATE = DateTime.Now,
                                            //        MODIFIED_USER = User.Identity.Name,
                                            //        MODIFIED_DATE = DateTime.Now
                                            //    };
                                            //objLS.TB_PROD_FAMILY_ATTR_LIST.Add(tbProdFamilyAttrList);
                                            //objLS.SaveChanges();
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        string imgtype = null;
                        if (_AttributeType == 3)
                        {
                            string[] images = _AttributeValue.Split('.');
                            if (images.Length > 1)
                            {
                                imgtype = images[1];
                            }
                        }
                        if (_ProductId != 0)
                        {
                            int isproduct = objLS.TB_PRODUCT.Count(x => x.PRODUCT_ID == _ProductId);
                            if (isproduct == 1)
                            {

                                int count1 =
                                    objLS.TB_PROD_SPECS.Count(
                                        x => x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_ID == _AttributeId);
                                var datatype =
                                    objLS.TB_ATTRIBUTE.Where(c => c.ATTRIBUTE_ID == _AttributeId)
                                        .Select(y => y).FirstOrDefault();

                                if (datatype != null)
                                    if (type.Substring(0, 1).ToString().ToUpper() != 'N'.ToString())
                                    {
                                        if (count1 == 0)
                                        {
                                            try
                                            {

                                                using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                                {
                                                    SqlCon.Open();

                                                    if (_AttributeType != 4 && atdtyp.StartsWith("Text"))
                                                    {
                                                        SqlCommand Sqlcom =
                                                            new SqlCommand(
                                                                "INSERT INTO TB_PROD_SPECS (STRING_VALUE, PRODUCT_ID, " +
                                                                "ATTRIBUTE_ID, OBJECT_TYPE,OBJECT_NAME,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" +
                                                                _AttributeValue + "'," + _ProductId + ",'" +
                                                                _AttributeId +
                                                                "','" + imgtype + "',null,'" + User.Identity.Name +
                                                                "',Getdate(),'" + User.Identity.Name + "',Getdate());",
                                                                SqlCon);

                                                        int flag = Sqlcom.ExecuteNonQuery();
                                                    }
                                                    else
                                                    {
                                                        SqlCommand Sqlcom =
                                                            new SqlCommand(
                                                                "INSERT INTO TB_PROD_SPECS (NUMERIC_VALUE, PRODUCT_ID, " +
                                                                "ATTRIBUTE_ID, OBJECT_TYPE,OBJECT_NAME,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" +
                                                                _AttributeValue + "'," + _ProductId + ",'" +
                                                                _AttributeId +
                                                                "','" + imgtype + "',null,'" + User.Identity.Name +
                                                                "',Getdate(),'" + User.Identity.Name + "',Getdate());",
                                                                SqlCon);

                                                        int flag = Sqlcom.ExecuteNonQuery();
                                                    }
                                                    SqlCon.Close();
                                                }
                                                //var tbProdSpecs = new TB_PROD_SPECS
                                                //{
                                                //    STRING_VALUE = _AttributeValue,
                                                //    PRODUCT_ID = _ProductId,
                                                //    ATTRIBUTE_ID = _AttributeId,
                                                //    OBJECT_TYPE = imgtype,
                                                //    NUMERIC_VALUE = null,
                                                //    OBJECT_NAME = null,
                                                //    CREATED_USER = User.Identity.Name,
                                                //    CREATED_DATE = DateTime.Now,
                                                //    MODIFIED_USER = User.Identity.Name,
                                                //    MODIFIED_DATE = DateTime.Now

                                                //};
                                                //objLS.TB_PROD_SPECS.Add(tbProdSpecs);
                                                //objLS.SaveChanges();
                                                _RecordsAffected =
                                                    objLS.TB_PROD_SPECS.Count(
                                                        x =>
                                                            x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_ID == _AttributeId &&
                                                            x.STRING_VALUE == _AttributeValue);
                                            }

                                            catch (Exception)
                                            {
                                            }
                                            int famcount = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);
                                            if (_FamilyId != 0 && famcount != 0)
                                            {
                                                int countpf =
                                                    objLS.TB_PROD_FAMILY.Count(
                                                        x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == _ProductId);
                                                int _sort = 1;
                                                try
                                                {
                                                    int maxsort;
                                                    int.TryParse(
                                                        objLS.TB_PROD_FAMILY.OrderByDescending(x => x.SORT_ORDER)
                                                            .Where(y => y.FAMILY_ID == _FamilyId)
                                                            .FirstOrDefault()
                                                            .ToString(), out maxsort);
                                                    _sort = maxsort + 1;
                                                }
                                                catch (Exception)
                                                {
                                                }
                                                if (countpf == 0)
                                                    try
                                                    {
                                                        using (
                                                            SqlConnection SqlCon = new SqlConnection(connectionString))
                                                        {
                                                            SqlCon.Open();
                                                            SqlCommand Sqlcom =
                                                                new SqlCommand(
                                                                    "INSERT INTO TB_PROD_FAMILY\r\n   (SORT_ORDER, FAMILY_ID, PRODUCT" +
                                                                    "_ID, PUBLISH,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values(" +
                                                                    _sort + "," + _FamilyId + "," + _ProductId + ",'" +
                                                                    Convert.ToBoolean(1) + "','" + User.Identity.Name +
                                                                    "',Getdate(),'" + User.Identity.Name +
                                                                    "',Getdate());", SqlCon);
                                                            //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                                            //da.Fill(att);
                                                            int flag = Sqlcom.ExecuteNonQuery();
                                                            SqlCon.Close();
                                                        }
                                                        //TB_PROD_FAMILY tbProdFamily = new TB_PROD_FAMILY
                                                        //{
                                                        //    SORT_ORDER = _sort,
                                                        //    FAMILY_ID = _FamilyId,
                                                        //    PRODUCT_ID = _ProductId,
                                                        //    PUBLISH = Convert.ToBoolean(1),
                                                        //    CREATED_USER = User.Identity.Name,
                                                        //    CREATED_DATE = DateTime.Now,
                                                        //    MODIFIED_USER = User.Identity.Name,
                                                        //    MODIFIED_DATE = DateTime.Now,
                                                        //    PUBLISH2PRINT = true,
                                                        //    PUBLISH2CD = true,
                                                        //    WORKFLOW_STATUS = 4,
                                                        //    WORKFLOW_COMMENTS = null
                                                        //};
                                                        //objLS.TB_PROD_FAMILY.Add(tbProdFamily);
                                                        //objLS.SaveChanges();
                                                    }
                                                    catch (Exception)
                                                    {
                                                    }
                                            }
                                            //return _RecordsAffected;
                                        }
                                        else
                                        {
                                            try
                                            {
                                                TB_PROD_SPECS tbProdSpecs =
                                                    objLS.TB_PROD_SPECS.Where(
                                                        x =>
                                                            x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_ID == _AttributeId)
                                                        .First();
                                                if (_attributetype == 4 && atdtyp.StartsWith("Num"))
                                                {
                                                    double num;
                                                    if (double.TryParse(_AttributeValue, out num))
                                                    {
                                                        tbProdSpecs.NUMERIC_VALUE = Convert.ToDecimal(num);
                                                    }
                                                    else
                                                    {
                                                        tbProdSpecs.STRING_VALUE = _AttributeValue;
                                                    }
                                                }
                                                else
                                                {

                                                    if (atdtyp.StartsWith("Num"))
                                                    {
                                                        double num;
                                                        if (double.TryParse(_AttributeValue, out num))
                                                        {
                                                            tbProdSpecs.NUMERIC_VALUE = Convert.ToDecimal(num);
                                                        }
                                                        else
                                                        {
                                                            tbProdSpecs.STRING_VALUE = _AttributeValue;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        tbProdSpecs.STRING_VALUE = _AttributeValue;
                                                    }
                                                }

                                                tbProdSpecs.OBJECT_TYPE = imgtype;
                                                objLS.SaveChanges();
                                                _RecordsAffected =
                                                    objLS.TB_PROD_SPECS.Count(
                                                        x =>
                                                            x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_ID == _AttributeId &&
                                                            x.STRING_VALUE == _AttributeValue &&
                                                            x.OBJECT_TYPE == imgtype);
                                                // return _RecordsAffected;
                                            }
                                            catch (Exception)
                                            {
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (count1 == 0)
                                        {
                                            try
                                            {
                                                Nullable<decimal> num = Convert.ToDecimal(_AttributeValue);

                                                using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                                {
                                                    SqlCon.Open();
                                                    SqlCommand Sqlcom =
                                                        new SqlCommand(
                                                            "INSERT INTO TB_PROD_SPECS (NUMERIC_VALUE, PRODUCT_ID, " +
                                                            "ATTRIBUTE_ID, OBJECT_TYPE,OBJECT_NAME,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" +
                                                            num + "'," + _ProductId + ",'" + _AttributeId + "','" +
                                                            imgtype + "',null,'" + User.Identity.Name + "',Getdate(),'" +
                                                            User.Identity.Name + "',Getdate());", SqlCon);

                                                    int flag = Sqlcom.ExecuteNonQuery();
                                                    SqlCon.Close();
                                                }
                                                //TB_PROD_SPECS tbProdSpecs = new TB_PROD_SPECS
                                                //{
                                                //    NUMERIC_VALUE = num,
                                                //    PRODUCT_ID = _ProductId,
                                                //    ATTRIBUTE_ID = _AttributeId,
                                                //    OBJECT_TYPE = null,
                                                //    STRING_VALUE = null,
                                                //    OBJECT_NAME = null,
                                                //    CREATED_USER = User.Identity.Name,
                                                //    CREATED_DATE = DateTime.Now,
                                                //    MODIFIED_USER = User.Identity.Name,
                                                //    MODIFIED_DATE = DateTime.Now
                                                //};
                                                //objLS.TB_PROD_SPECS.Add(tbProdSpecs);
                                                //objLS.SaveChanges();
                                                _RecordsAffected =
                                                    objLS.TB_PROD_SPECS.Count(
                                                        x =>
                                                            x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_ID == _AttributeId &&
                                                            x.STRING_VALUE == _AttributeValue &&
                                                            x.OBJECT_TYPE == imgtype);

                                                int famcount = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);
                                                if (_FamilyId != 0 && famcount != 0)
                                                {
                                                    int countpf =
                                                        objLS.TB_PROD_FAMILY.Count(
                                                            x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == _ProductId);
                                                    int _sort = 1;
                                                    try
                                                    {
                                                        int maxsort;
                                                        int.TryParse(
                                                            objLS.TB_PROD_FAMILY.OrderByDescending(x => x.SORT_ORDER)
                                                                .Where(y => y.FAMILY_ID == _FamilyId)
                                                                .FirstOrDefault()
                                                                .ToString(), out maxsort);
                                                        _sort = maxsort + 1;
                                                    }
                                                    catch (Exception)
                                                    {
                                                    }
                                                    if (countpf == 0)
                                                        try
                                                        {
                                                            using (
                                                                SqlConnection SqlCon =
                                                                    new SqlConnection(connectionString))
                                                            {
                                                                SqlCon.Open();
                                                                SqlCommand Sqlcom =
                                                                    new SqlCommand(
                                                                        "INSERT INTO TB_PROD_FAMILY\r\n   (SORT_ORDER, FAMILY_ID, PRODUCT" +
                                                                        "_ID, PUBLISH,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values(" +
                                                                        _sort + "," + _FamilyId + "," + _ProductId +
                                                                        ",'" + Convert.ToBoolean(1) + "','" +
                                                                        User.Identity.Name + "',Getdate(),'" +
                                                                        User.Identity.Name + "',Getdate());", SqlCon);
                                                                //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                                                //da.Fill(att);
                                                                int flag = Sqlcom.ExecuteNonQuery();
                                                                SqlCon.Close();
                                                            }
                                                            //TB_PROD_FAMILY tbProdFamily = new TB_PROD_FAMILY
                                                            //{
                                                            //    SORT_ORDER = _sort,
                                                            //    FAMILY_ID = _FamilyId,
                                                            //    PRODUCT_ID = _ProductId,
                                                            //    PUBLISH = Convert.ToBoolean(1),
                                                            //    CREATED_USER = User.Identity.Name,
                                                            //    CREATED_DATE = DateTime.Now,
                                                            //    MODIFIED_USER = User.Identity.Name,
                                                            //    MODIFIED_DATE = DateTime.Now,
                                                            //    PUBLISH2PRINT = true,
                                                            //    PUBLISH2CD = true,
                                                            //    WORKFLOW_STATUS = 4,
                                                            //    WORKFLOW_COMMENTS = null
                                                            //};
                                                            //objLS.TB_PROD_FAMILY.Add(tbProdFamily);
                                                            //objLS.SaveChanges();
                                                        }
                                                        catch (Exception)
                                                        {
                                                        }
                                                }
                                            }
                                            catch (Exception)
                                            {
                                            }
                                        }
                                        else
                                        {
                                            try
                                            {
                                                Nullable<decimal> num = Convert.ToDecimal(_AttributeValue);

                                                TB_PROD_SPECS tbProdSpecs =
                                                    objLS.TB_PROD_SPECS.Where(
                                                        x =>
                                                            x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_ID == _AttributeId)
                                                        .First();
                                                tbProdSpecs.NUMERIC_VALUE = num;
                                                objLS.SaveChanges();
                                                _RecordsAffected =
                                                    objLS.TB_PROD_SPECS.Count(
                                                        x =>
                                                            x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_ID == _AttributeId &&
                                                            x.STRING_VALUE == _AttributeValue);

                                            }
                                            catch (Exception)
                                            {
                                            }
                                        }
                                    }
                                int sort = 1;
                                if (_FamilyId != 0 && count1 == 0)
                                {
                                    if (
                                        (objLS.TB_PROD_FAMILY_ATTR_LIST.Count(
                                            x => x.FAMILY_ID == _FamilyId && x.ATTRIBUTE_ID == _AttributeId) > 0) ||
                                        (objLS.TB_PROD_FAMILY_ATTR_LIST.Count(
                                            x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == 0) > 0))
                                    {

                                        try
                                        {

                                            int.TryParse(
                                                objLS.TB_PROD_FAMILY_ATTR_LIST.OrderByDescending(
                                                    z => z.SORT_ORDER)
                                                    .Where(
                                                        x =>
                                                            x.FAMILY_ID == _FamilyId &&
                                                            x.ATTRIBUTE_ID == _AttributeId)
                                                    .Select(y => y.SORT_ORDER)
                                                    .FirstOrDefault()
                                                    .ToString(), out sort);

                                            if (sort == 0)
                                            {
                                                int maxsort;
                                                int.TryParse(
                                                    objLS.TB_PROD_FAMILY_ATTR_LIST.OrderByDescending(
                                                        z => z.SORT_ORDER)
                                                        .Where(
                                                            x =>
                                                                x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == 0)
                                                        .Select(y => y.SORT_ORDER)
                                                        .FirstOrDefault()
                                                        .ToString(), out maxsort);
                                                sort = maxsort + 1;
                                            }
                                            else
                                            {
                                                sort = sort + 1;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            try
                                            {
                                                int maxsort;
                                                int.TryParse(
                                                    objLS.TB_PROD_FAMILY_ATTR_LIST.OrderByDescending(
                                                        z => z.SORT_ORDER)
                                                        .Where(
                                                            x =>
                                                                x.FAMILY_ID == _FamilyId &&
                                                                x.ATTRIBUTE_ID == _AttributeId)
                                                        .Select(y => y.SORT_ORDER)
                                                        .FirstOrDefault()
                                                        .ToString(), out maxsort);
                                                sort = maxsort + 1;

                                            }
                                            catch (Exception)
                                            {
                                                sort = 1;
                                            }

                                        }
                                        try
                                        {
                                            //if (sort==0)
                                            //{
                                            //    sort = 1;
                                            //}
                                            using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                            {
                                                SqlCon.Open();
                                                SqlCommand Sqlcom =
                                                    new SqlCommand(
                                                        "INSERT INTO TB_PROD_FAMILY_ATTR_LIST\r\n (ATTRIBUTE_ID, SORT_ORDER" +
                                                        ", FAMILY_ID, PRODUCT_ID,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" +
                                                        _AttributeId + "'," + sort + "," + _FamilyId + ",0,'" + User.Identity.Name + "',Getdate(),'" +
                                                        User.Identity.Name + "',Getdate());", SqlCon);
                                                //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                                //da.Fill(att);
                                                int flag = Sqlcom.ExecuteNonQuery();
                                                SqlCon.Close();
                                            }
                                            //var tbProdFamilyAttrList =
                                            //    new TB_PROD_FAMILY_ATTR_LIST
                                            //    {
                                            //        ATTRIBUTE_ID = _AttributeId,
                                            //        SORT_ORDER = sort,
                                            //        FAMILY_ID = _FamilyId,
                                            //        PRODUCT_ID = _ProductId,
                                            //        CREATED_USER = User.Identity.Name,
                                            //        CREATED_DATE = DateTime.Now,
                                            //        MODIFIED_USER = User.Identity.Name,
                                            //        MODIFIED_DATE = DateTime.Now
                                            //    };
                                            //objLS.TB_PROD_FAMILY_ATTR_LIST.Add(tbProdFamilyAttrList);
                                            //objLS.SaveChanges();
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                }
                            }
                            else if (_CatalogItemno != null && _CatalogItemno != "")
                            {
                                DataTable tb_prodspecs = new DataTable();
                                try
                                {
                                    var ss =
                                        objLS.TB_PROD_SPECS.Join(objLS.TB_PROD_FAMILY, tps => tps.PRODUCT_ID,
                                            tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                                            .Join(objLS.TB_CATALOG_FAMILY, tcf => tcf.tpf.FAMILY_ID,
                                                tcftpf => tcftpf.FAMILY_ID, (tcf, tcftpf) => new { tcf, tcftpf })
                                            .Where(
                                                x =>
                                                    x.tcf.tps.ATTRIBUTE_ID == 1 &&
                                                    x.tcf.tps.STRING_VALUE == CatalogItemno &&
                                                    x.tcftpf.CATALOG_ID == CatalogId).ToList();

                                    tb_prodspecs = ToDataTable(ss);
                                }
                                catch (Exception)
                                {
                                }
                                DataTable prodspecs = null;
                                if (_FamilyId > 0)
                                {
                                    DataRow[] prodspecsrows = tb_prodspecs.Select("FAMILY_ID=" + _FamilyId);
                                    prodspecs = tb_prodspecs.Clone();
                                    foreach (DataRow row in prodspecsrows)
                                        prodspecs.ImportRow(row);
                                }
                                else
                                {
                                    prodspecs = tb_prodspecs.Clone();
                                    prodspecs = tb_prodspecs;
                                }

                                foreach (DataRow row in prodspecs.Rows)
                                {
                                    _ProductId = Convert.ToInt32(row["PRODUCT_ID"]);
                                    int countproduct =
                                        objLS.TB_PROD_SPECS.Count(
                                            x => x.ATTRIBUTE_ID == _AttributeId && x.PRODUCT_ID == _ProductId);
                                    if (countproduct == 0)
                                    {
                                        if (type.Substring(0, 1).ToString().ToUpper() != 'N'.ToString())
                                        {
                                            using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                            {
                                                SqlCon.Open();
                                                SqlCommand Sqlcom =
                                                    new SqlCommand(
                                                        "INSERT INTO TB_PROD_SPECS (STRING_VALUE, PRODUCT_ID, " +
                                                        "ATTRIBUTE_ID, OBJECT_TYPE,OBJECT_NAME,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" +
                                                        _AttributeValue + "'," + _ProductId + ",'" + _AttributeId +
                                                        "','" + imgtype + "',null,'" + User.Identity.Name +
                                                        "',Getdate(),'" + User.Identity.Name + "',Getdate());", SqlCon);

                                                int flag = Sqlcom.ExecuteNonQuery();
                                                SqlCon.Close();
                                            }
                                            //TB_PROD_SPECS tbProdSpecs = new TB_PROD_SPECS
                                            //{
                                            //    STRING_VALUE = _AttributeValue,
                                            //    PRODUCT_ID = _ProductId,
                                            //    ATTRIBUTE_ID = _AttributeId,
                                            //    OBJECT_TYPE = imgtype,
                                            //    NUMERIC_VALUE = null,
                                            //    OBJECT_NAME = null,
                                            //    CREATED_USER = User.Identity.Name,
                                            //    CREATED_DATE = DateTime.Now,
                                            //    MODIFIED_USER = User.Identity.Name,
                                            //    MODIFIED_DATE = DateTime.Now

                                            //};
                                            //objLS.TB_PROD_SPECS.Add(tbProdSpecs);
                                            //objLS.SaveChanges();
                                            _RecordsAffected =
                                                objLS.TB_PROD_SPECS.Count(
                                                    x =>
                                                        x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_ID == _AttributeId &&
                                                        x.STRING_VALUE == _AttributeValue &&
                                                        x.OBJECT_TYPE == imgtype);
                                        }
                                        else
                                        {
                                            Nullable<decimal> num = Convert.ToDecimal(_AttributeValue);

                                            using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                            {
                                                SqlCon.Open();
                                                SqlCommand Sqlcom =
                                                    new SqlCommand(
                                                        "INSERT INTO TB_PROD_SPECS (NUMERIC_VALUE, PRODUCT_ID, " +
                                                        "ATTRIBUTE_ID, OBJECT_TYPE,OBJECT_NAME,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" +
                                                        num + "'," + _ProductId + ",'" + _AttributeId + "','" + imgtype +
                                                        "',null,'" + User.Identity.Name + "',Getdate(),'" +
                                                        User.Identity.Name + "',Getdate());", SqlCon);

                                                int flag = Sqlcom.ExecuteNonQuery();
                                                SqlCon.Close();
                                            }

                                            //TB_PROD_SPECS tbProdSpecs = new TB_PROD_SPECS
                                            //{
                                            //    NUMERIC_VALUE = num,
                                            //    PRODUCT_ID = _ProductId,
                                            //    ATTRIBUTE_ID = _AttributeId,
                                            //    OBJECT_TYPE = null,
                                            //    STRING_VALUE = null,
                                            //    OBJECT_NAME = null,
                                            //    CREATED_USER = User.Identity.Name,
                                            //    CREATED_DATE = DateTime.Now,
                                            //    MODIFIED_USER = User.Identity.Name,
                                            //    MODIFIED_DATE = DateTime.Now
                                            //};
                                            //objLS.TB_PROD_SPECS.Add(tbProdSpecs);
                                            //objLS.SaveChanges();
                                            _RecordsAffected =
                                                objLS.TB_PROD_SPECS.Count(
                                                    x =>
                                                        x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_ID == _AttributeId &&
                                                        x.STRING_VALUE == _AttributeValue &&
                                                        x.OBJECT_TYPE == imgtype);
                                        }
                                        int famcount = objLS.TB_FAMILY.Count(x => x.FAMILY_ID == _FamilyId);
                                        if (_FamilyId != 0 && famcount != 0)
                                        {
                                            int countpf =
                                                objLS.TB_PROD_FAMILY.Count(
                                                    x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == _ProductId);
                                            int _sort = 1;
                                            try
                                            {
                                                int maxsort;
                                                int.TryParse(
                                                    objLS.TB_PROD_FAMILY.OrderByDescending(x => x.SORT_ORDER)
                                                        .Where(y => y.FAMILY_ID == _FamilyId)
                                                        .FirstOrDefault()
                                                        .ToString(), out maxsort);
                                                _sort = maxsort + 1;
                                            }
                                            catch (Exception)
                                            {
                                            }
                                            if (countpf == 0)
                                                try
                                                {
                                                    using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                                    {
                                                        SqlCon.Open();
                                                        SqlCommand Sqlcom =
                                                            new SqlCommand(
                                                                "INSERT INTO TB_PROD_FAMILY\r\n   (SORT_ORDER, FAMILY_ID, PRODUCT" +
                                                                "_ID, PUBLISH,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values(" +
                                                                _sort + "," + _FamilyId + "," + _ProductId + ",'" +
                                                                Convert.ToBoolean(1) + "','" + User.Identity.Name +
                                                                "',Getdate(),'" + User.Identity.Name + "',Getdate());",
                                                                SqlCon);
                                                        //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                                        //da.Fill(att);
                                                        int flag = Sqlcom.ExecuteNonQuery();
                                                        SqlCon.Close();
                                                    }
                                                    //TB_PROD_FAMILY tbProdFamily = new TB_PROD_FAMILY
                                                    //{
                                                    //    SORT_ORDER = _sort,
                                                    //    FAMILY_ID = _FamilyId,
                                                    //    PRODUCT_ID = _ProductId,
                                                    //    PUBLISH = Convert.ToBoolean(1),
                                                    //    CREATED_USER = User.Identity.Name,
                                                    //    CREATED_DATE = DateTime.Now,
                                                    //    MODIFIED_USER = User.Identity.Name,
                                                    //    MODIFIED_DATE = DateTime.Now,
                                                    //    PUBLISH2PRINT = true,
                                                    //    PUBLISH2CD = true,
                                                    //    WORKFLOW_STATUS = 4,
                                                    //    WORKFLOW_COMMENTS = null
                                                    //};
                                                    //objLS.TB_PROD_FAMILY.Add(tbProdFamily);
                                                    //objLS.SaveChanges();
                                                }
                                                catch (Exception)
                                                {
                                                }

                                        }
                                        else
                                        {
                                            if (type.Substring(0, 1).ToString().ToUpper() != 'N'.ToString())
                                            {
                                                TB_PROD_SPECS tbProdSpecs =
                                                    objLS.TB_PROD_SPECS.Where(
                                                        x =>
                                                            x.PRODUCT_ID == _ProductId &&
                                                            x.ATTRIBUTE_ID == _AttributeId)
                                                        .First();
                                                tbProdSpecs.STRING_VALUE = _AttributeValue;
                                                tbProdSpecs.OBJECT_TYPE = imgtype;
                                                objLS.SaveChanges();
                                                _RecordsAffected =
                                                    objLS.TB_PROD_SPECS.Count(
                                                        x =>
                                                            x.PRODUCT_ID == _ProductId &&
                                                            x.ATTRIBUTE_ID == _AttributeId &&
                                                            x.STRING_VALUE == _AttributeValue &&
                                                            x.OBJECT_TYPE == imgtype);
                                            }
                                            else
                                            {
                                                try
                                                {
                                                    Nullable<decimal> num = Convert.ToDecimal(_AttributeValue);

                                                    TB_PROD_SPECS tbProdSpecs =
                                                        objLS.TB_PROD_SPECS.Where(
                                                            x =>
                                                                x.PRODUCT_ID == _ProductId &&
                                                                x.ATTRIBUTE_ID == _AttributeId)
                                                            .First();
                                                    tbProdSpecs.NUMERIC_VALUE = num;
                                                    objLS.SaveChanges();
                                                    _RecordsAffected =
                                                        objLS.TB_PROD_SPECS.Count(
                                                            x =>
                                                                x.PRODUCT_ID == _ProductId &&
                                                                x.ATTRIBUTE_ID == _AttributeId &&
                                                                x.STRING_VALUE == _AttributeValue &&
                                                                x.OBJECT_TYPE == imgtype);
                                                }
                                                catch (Exception)
                                                {
                                                }

                                            }
                                        }
                                        int sort = 1;
                                        if (_FamilyId != 0)
                                        {
                                            if (
                                                (objLS.TB_PROD_FAMILY_ATTR_LIST.Count(
                                                    x => x.FAMILY_ID == _FamilyId && x.ATTRIBUTE_ID == _AttributeId) >
                                                 0) ||
                                                (objLS.TB_PROD_FAMILY_ATTR_LIST.Count(
                                                    x => x.FAMILY_ID == _FamilyId && x.PRODUCT_ID == 0) > 0))
                                            {
                                                sort = 1;
                                                try
                                                {
                                                    int.TryParse(
                                                        objLS.TB_PROD_FAMILY_ATTR_LIST.Where(
                                                            x =>
                                                                x.FAMILY_ID == _FamilyId &&
                                                                x.ATTRIBUTE_ID == _AttributeId)
                                                            .Select(y => y.SORT_ORDER)
                                                            .ToList()[0].ToString(), out sort);

                                                }
                                                catch (Exception)
                                                {
                                                    try
                                                    {
                                                        int maxsort;
                                                        int.TryParse(
                                                            objLS.TB_PROD_FAMILY_ATTR_LIST.OrderByDescending(
                                                                z => z.SORT_ORDER)
                                                                .Where(
                                                                    x =>
                                                                        x.FAMILY_ID == _FamilyId &&
                                                                        x.ATTRIBUTE_ID == _AttributeId)
                                                                .Select(y => y.SORT_ORDER)
                                                                .FirstOrDefault()
                                                                .ToString(), out maxsort);
                                                        sort = maxsort + 1;

                                                    }
                                                    catch (Exception)
                                                    {
                                                        sort = 1;
                                                    }

                                                }
                                                try
                                                {
                                                    using (SqlConnection SqlCon = new SqlConnection(connectionString))
                                                    {
                                                        SqlCon.Open();
                                                        SqlCommand Sqlcom =
                                                            new SqlCommand(
                                                                "INSERT INTO TB_PROD_FAMILY_ATTR_LIST\r\n (ATTRIBUTE_ID, SORT_ORDER" +
                                                                ", FAMILY_ID, PRODUCT_ID,[CREATED_USER],[CREATED_DATE],[MODIFIED_USER],[MODIFIED_DATE])\r\n values('" +
                                                                _AttributeId + "'," + sort + "," + _FamilyId + ",0,'" + User.Identity.Name +
                                                                "',Getdate(),'" + User.Identity.Name + "',Getdate());",
                                                                SqlCon);
                                                        //SqlDataAdapter da = new SqlDataAdapter(Sqlcom);
                                                        //da.Fill(att);
                                                        int flag = Sqlcom.ExecuteNonQuery();
                                                        SqlCon.Close();
                                                    }
                                                    //TB_PROD_FAMILY_ATTR_LIST tbProdFamilyAttrList =
                                                    //    new TB_PROD_FAMILY_ATTR_LIST
                                                    //    {
                                                    //        ATTRIBUTE_ID = _AttributeId,
                                                    //        SORT_ORDER = sort,
                                                    //        FAMILY_ID = _FamilyId,
                                                    //        PRODUCT_ID = _ProductId,
                                                    //        CREATED_USER = User.Identity.Name,
                                                    //        CREATED_DATE = DateTime.Now,
                                                    //        MODIFIED_USER = User.Identity.Name,
                                                    //        MODIFIED_DATE = DateTime.Now
                                                    //    };
                                                    //objLS.TB_PROD_FAMILY_ATTR_LIST.Add(tbProdFamilyAttrList);
                                                    //objLS.SaveChanges();
                                                }
                                                catch (Exception)
                                                {
                                                }
                                            }
                                        }
                                    }
                                    // return _RecordsAffected;

                                }


                            }
                            i++;
                        }
                        else
                        {
                            if (_newcatitemno != "")
                            {

                                try
                                {
                                    int _forupdateproduct_id = 0;
                                    var oldproductid = objLS.TB_PROD_SPECS.Where(
                                        x => x.ATTRIBUTE_ID == 1 && x.STRING_VALUE == _newcatitemno).ToArray();

                                    if (oldproductid != null)
                                    {
                                        for (int ij = 0; ij < oldproductid.Length; ij++)
                                        {
                                            _forupdateproduct_id = oldproductid[ij].PRODUCT_ID;

                                            TB_PROD_SPECS tbProdSpecs =
                                                objLS.TB_PROD_SPECS.Where(
                                                    x =>
                                                        x.PRODUCT_ID == _forupdateproduct_id &&
                                                        x.ATTRIBUTE_ID == _AttributeId)
                                                    .First();
                                            if (_attributetype == 4 && atdtyp.StartsWith("Num"))
                                            {
                                                double num;
                                                if (double.TryParse(_AttributeValue, out num))
                                                {
                                                    tbProdSpecs.NUMERIC_VALUE = Convert.ToDecimal(num);
                                                }
                                                else
                                                {
                                                    tbProdSpecs.STRING_VALUE = _AttributeValue;
                                                }
                                            }
                                            else
                                            {

                                                if (atdtyp.StartsWith("Num"))
                                                {
                                                    double num;
                                                    if (double.TryParse(_AttributeValue, out num))
                                                    {
                                                        tbProdSpecs.NUMERIC_VALUE = Convert.ToDecimal(num);
                                                    }
                                                    else
                                                    {
                                                        tbProdSpecs.STRING_VALUE = _AttributeValue;
                                                    }
                                                }
                                                else
                                                {
                                                    tbProdSpecs.STRING_VALUE = _AttributeValue;
                                                }
                                            }

                                            tbProdSpecs.OBJECT_TYPE = imgtype;
                                            objLS.SaveChanges();
                                            _RecordsAffected =
                                                objLS.TB_PROD_SPECS.Count(
                                                    x =>
                                                        x.PRODUCT_ID == _ProductId && x.ATTRIBUTE_ID == _AttributeId &&
                                                        x.STRING_VALUE == _AttributeValue &&
                                                        x.OBJECT_TYPE == imgtype);
                                            // return _RecordsAffected;
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                }


                            }
                        }

                    }
                } while (i < loopproduct);


            }
            return _RecordsAffected;
        }

        //public int AttributeIU()
        //{
        //    int count=0;
        //    if (_AttributeName != "")
        //    {
        //        if (!objLS.TB_ATTRIBUTE.Select(x => x.ATTRIBUTE_NAME == _AttributeName).ToList().Any())
        //        {
        //             string dformat = "";
        //             if (AttributeType != 4)
        //                    dformat = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
        //                else
        //                    dformat = "^-{0,1}?\\d*\\.{0,1}\\d{0,6}$";
        //             if (AttributeType != 0 && _AttributeDataType != null)
        //             {
        //                 TB_ATTRIBUTE tbAttribute = new TB_ATTRIBUTE
        //                 {
        //                     ATTRIBUTE_NAME = _AttributeName,
        //                     ATTRIBUTE_TYPE = (byte)AttributeType,
        //                     ATTRIBUTE_DATATYPE = _AttributeDataType,
        //                     ATTRIBUTE_DATAFORMAT = dformat
        //                 };
        //                 objLS.TB_ATTRIBUTE.Add(tbAttribute);
        //                 objLS.SaveChanges();
        //                 _AttributeId = tbAttribute.ATTRIBUTE_ID;
        //                 _RecordsAffected = 1;
        //             }
        //        }
        //        else
        //        {
        //            int.TryParse(objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == _AttributeName).Select(y => y.ATTRIBUTE_ID).ToList()[0].ToString(), out _AttributeId);
        //            if (_AttributeId == 3)
        //            {
        //                if (!objLS.TB_ATTRIBUTE.Select(x => x.ATTRIBUTE_ID == 3 && x.DEFAULT_VALUE == AttributeValue)
        //                        .ToList()
        //                        .Any())
        //                {
        //                    TB_SUPPLIER tbSupplier = new TB_SUPPLIER
        //                    {
        //                        SUPPLIER_NAME = attributeValue
        //                    };
        //                }
        //            }
        //            if (catalogID != 0)
        //            {
        //                if (!objLS.TB_CATALOG_ATTRIBUTES.Select(
        //                        x => x.ATTRIBUTE_ID == _AttributeId && x.CATALOG_ID == catalogID).ToList().Any())
        //                {
        //                    TB_CATALOG_ATTRIBUTES tbCatalogAttributes = new TB_CATALOG_ATTRIBUTES
        //                    {
        //                        CATALOG_ID = catalogID,
        //                        ATTRIBUTE_ID = _AttributeId,
        //                        CREATED_USER = User.Identity.Name,
        //                        MODIFIED_USER = User.Identity.Name
        //                    };
        //                    objLS.TB_CATALOG_ATTRIBUTES.Add(tbCatalogAttributes);
        //                    objLS.SaveChanges();
        //                }
        //            }
        //            int loopproduct = 0;
        //            DataTable tb_fam=new DataTable();
        //            if (_FamilyId == 0)
        //            {
        //                if (_FamilyName != "")
        //                {

        //                    tb_fam = objLS.TB_FAMILY.Join(objLS.TB_FAMILY_TABLE_STRUCTURE, tf => tf.FAMILY_ID, tfts => tfts.FAMILY_ID, (tf,tfts)=>new {tf,tfts}).Where(x => x.tf.FAMILY_NAME == _FamilyName) as DataTable;
        //                    int.TryParse(tb_fam.Rows.Count.ToString(), out loopproduct);
        //                }
        //            }
        //            int i = 0;
        //            do
        //            {
        //                if (loopproduct > 0)
        //                {
        //                    _FamilyId = Convert.ToInt32(tb_fam.Rows[i]["FAMILY_ID"]);
        //                }
        //                string type =
        //                    objLS.TB_ATTRIBUTE.Select(x => x.ATTRIBUTE_ID == _AttributeId).ToList()[0].ToString();
        //                if (_AttributeType > 6)
        //                {
        //                    if (_FamilyId != 0)
        //                    {
        //                    }
        //                }
        //                else if (_AttributeType == 6)
        //                {

        //                }
        //                else
        //                {
        //                    string imgtype = null;
        //                    if (_AttributeType == 3)
        //                    {
        //                        string[] images = _AttributeValue.Split('.');
        //                        if (images.Length > 1)
        //                        {
        //                            imgtype = images[1];
        //                        }
        //                    }
        //                    if (_ProductId != 0)
        //                    {
        //                        int isproduct;
        //                        int.TryParse(objLS.TB_PRODUCT.Count(x => x.PRODUCT_ID == _ProductId).ToString(),
        //                            out isproduct);
        //                        if (isproduct == 1)
        //                        {

        //                            int count1;// = (int)Pspec.CheckIfProdspecsExists(_ProductId, _AttributeId);
        //                            int.TryParse(
        //                                objLS.TB_PROD_SPECS.Count(
        //                                    x => x.ATTRIBUTE_ID == _AttributeId && x.PRODUCT_ID == _ProductId)
        //                                    .ToString(), out count1);

        //                            if (type.Substring(0, 1).ToString().ToUpper() != 'N'.ToString())
        //                            {
        //                                if (count1 == 0)
        //                                {
        //                                    try
        //                                    {
        //                                        TB_PROD_SPECS tbProdSpecs = new TB_PROD_SPECS
        //                                        {

        //                                        };
        //                                        _RecordsAffected = Pspec.InsertStringAttribute(_AttributeValue, _ProductId, _AttributeId, imgtype);
        //                                    }

        //                                    catch (Exception)
        //                                    {
        //                                    }
        //                                    int famcount = (int)cfa.GetFirstMatchingFamilyId(_FamilyId);
        //                                    if (_FamilyId != 0 && famcount != 0)
        //                                    {
        //                                        int countpf = (int)pft.CheckIfProdFamily(_FamilyId, _ProductId);
        //                                        int _sort = 1;
        //                                        try
        //                                        {
        //                                            _sort = (int)pft.GetMaxSortOrderprodfamily(_FamilyId) + 1;
        //                                        }
        //                                        catch (Exception)
        //                                        { }
        //                                        if (countpf == 0)
        //                                            try
        //                                            {
        //                                                pft.InsertProdFamily(_sort, _FamilyId, _ProductId, Convert.ToBoolean(1));
        //                                                // pfat.InsertProdFamilyAttr(_AttributeId, sort, _FamilyId, _ProductId);
        //                                            }
        //                                            catch (Exception)
        //                                            { }
        //                                    }
        //                                    //return _RecordsAffected;
        //                                }
        //                                else
        //                                {
        //                                    try
        //                                    {
        //                                        _RecordsAffected = Pspec.UpdateStringAttribute(_AttributeValue, imgtype, _ProductId, _AttributeId);
        //                                        // return _RecordsAffected;
        //                                    }
        //                                    catch (Exception)
        //                                    { }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (count1 == 0)
        //                                {
        //                                    try
        //                                    {
        //                                        double num = Convert.ToDouble(_AttributeValue);
        //                                        _RecordsAffected = Pspec.InserNumericAttribute((decimal)num, _ProductId, _AttributeId);
        //                                        int famcount = (int)cfa.GetFirstMatchingFamilyId(_FamilyId);
        //                                        if (_FamilyId != 0 && famcount != 0)
        //                                        {
        //                                            int countpf = (int)pft.CheckIfProdFamily(_FamilyId, _ProductId);
        //                                            int _sort = 1;
        //                                            try
        //                                            {
        //                                                _sort = (int)pft.GetMaxSortOrderprodfamily(_FamilyId) + 1;
        //                                            }
        //                                            catch (Exception)
        //                                            { }
        //                                            if (countpf == 0)
        //                                                pft.InsertProdFamily(_sort, _FamilyId, _ProductId, Convert.ToBoolean(1));

        //                                        }
        //                                    }
        //                                    catch (Exception)
        //                                    { }
        //                                }
        //                                else
        //                                {
        //                                    try
        //                                    {
        //                                        double num = Convert.ToDouble(_AttributeValue);
        //                                        _RecordsAffected = Pspec.UpdateNumericAttribute((decimal)num, _ProductId, _AttributeId);

        //                                    }
        //                                    catch (Exception)
        //                                    { }
        //                                }
        //                            }
        //                            int sort = 1;
        //                            if (_FamilyId != 0 && count1 == 0)
        //                            {
        //                                if (((int)Qta.GetAttrFamilyCount(_AttributeId, _FamilyId) > 0) || ((int)Qta.GetattrprodfamCount(_ProductId, _FamilyId) == 0))
        //                                {
        //                                    try
        //                                    {
        //                                        sort = ((int)pfat.GetProdFamilyAttrListSort(_AttributeId, _FamilyId));
        //                                    }
        //                                    catch (Exception)
        //                                    {
        //                                        try
        //                                        {
        //                                            sort = ((int)pfat.GetMaxSortOrderAttr(_FamilyId) + 1);
        //                                        }
        //                                        catch (Exception)
        //                                        {
        //                                            sort = 1;
        //                                        }
        //                                    }

        //                                    try
        //                                    {
        //                                        pfat.InsertProdFamilyAttr(_AttributeId, sort, _FamilyId, _ProductId);
        //                                    }
        //                                    catch (Exception) { }
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            } while (i > 0);
        //        }
        //    }
        //    return count;
        //}

        private int catalogID;
        public int Catalog_id
        {
            get
            {
                return catalogID;
            }
            set
            {
                catalogID = value;
            }
        }
        private string attributeValue = string.Empty;

        public string NewCategory(int catalogId, string categoryName, string parentId)
        {
            var categoryId = string.Empty;
            int count = 1;
            while (count > 0)
            {
                Random rnd = new Random();
                categoryId = rnd.Next().ToString();
                int.TryParse(objLS.TB_CATEGORY.Count(x => x.CATEGORY_ID == categoryId).ToString(), out count);

            }
            TB_CATEGORY newCategory = new TB_CATEGORY
            {
                CATEGORY_ID = categoryId,
                CATEGORY_NAME = categoryName,
                PARENT_CATEGORY = parentId,
                CREATED_USER = User.Identity.Name,
                CREATED_DATE = DateTime.Now,
                MODIFIED_USER = User.Identity.Name,
                MODIFIED_DATE = DateTime.Now,
                FLAG_RECYCLE = "A"
            };
            objLS.TB_CATEGORY.Add(newCategory);
            objLS.SaveChanges();
            categoryId = newCategory.CATEGORY_ID;
            _ParentCategoryId = categoryId;
            if (objLS.TB_CATALOG_SECTIONS.Select(x => x.CATALOG_ID == catalogId && x.CATEGORY_ID == categoryId)
                    .ToList()
                    .Any())
            {
                TB_CATALOG_SECTIONS tbCatalogSections = new TB_CATALOG_SECTIONS
                {
                    CATALOG_ID = catalogId,
                    CATEGORY_ID = categoryId,
                    MODIFIED_USER = User.Identity.Name,
                    CREATED_USER = User.Identity.Name,
                    FLAG_RECYCLE = "A"
                };
                objLS.TB_CATALOG_SECTIONS.Add(tbCatalogSections);
                objLS.SaveChanges();
            }
            return categoryId;
        }

        public string NewCatalog(string catalogName)
        {
            {
                TB_CATALOG newCatalog = new TB_CATALOG
                {
                    CATALOG_NAME = catalogName,
                    CREATED_USER = User.Identity.Name,
                    MODIFIED_USER = User.Identity.Name
                };
                objLS.TB_CATALOG.Add(newCatalog);
                objLS.SaveChanges();
                int catalog_ID = newCatalog.CATALOG_ID;
                var catalogAttrCount =
                    objLS.TB_CATALOG_ATTRIBUTES.Count(
                        x =>
                            x.ATTRIBUTE_ID == 1 &&
                            x.CATALOG_ID ==
                            (objLS.TB_CATALOG.Where(z => z.CATALOG_NAME == catalogName)
                                .Select(y => y.CATALOG_ID)).ToList()[0]);

                if (objLS.TB_CATALOG_ATTRIBUTES.Select(x => x.CATALOG_ID == catalog_ID && x.ATTRIBUTE_ID == 1)
                        .ToList()
                        .Any())
                {
                    TB_CATALOG_ATTRIBUTES tbCatalogAttributes = new TB_CATALOG_ATTRIBUTES
                    {
                        CATALOG_ID = catalog_ID,
                        ATTRIBUTE_ID = 1,
                        CREATED_USER = User.Identity.Name,
                        MODIFIED_USER = User.Identity.Name
                    };
                    objLS.TB_CATALOG_ATTRIBUTES.Add(tbCatalogAttributes);
                    objLS.SaveChanges();
                }
                return catalog_ID.ToString();
            }
        }
        [System.Web.Http.HttpPost]
        public string BasicImport(string allowDuplicate, JArray model)
        {
            DataTable records = new DataTable();
            foreach (JArray mod in model)
            {
                if (mod.Contains("ExcelColumn"))
                {
                    basicprodImportData = IC.SelectedColumnsToImport(basicprodImportData, mod);
                }
                else
                {
                    records = IC.SelectedColumnsToImport(basicprodImportData, mod);
                }
            }
            return null;
        }
        #endregion

        #region Import error list - Class
        public class IMPORT_ERROR_LIST
        {
            public string ErrorMessage { get; set; }
            public string ErrorProcedure { get; set; }
            public string ErrorSeverity { get; set; }
            public string ErrorState { get; set; }
            public string ErrorNumber { get; set; }
            public string ErrorLine { get; set; }
        }
        #endregion

        #region Import error list del - Class
        public class IMPORT_ERROR_LISTdel
        {
            public string CATALOG_ITEM_NO { get; set; }
            public string STATUS { get; set; }
        }
        #endregion

        #region Import success list - Class
        public class IMPORT_SUCCESS_LIST
        {
            public string CATALOG_NAME { get; set; }
            public string CATEGORY_DETAILS { get; set; }
            public string FAMILY_DETAILS { get; set; }
            public string SUBFAMILY_DETAILS { get; set; }
            public string PRODUCT_DETAILS { get; set; }
            public string SUB_PRODUCT_DETAILS { get; set; }
            public string STATUS { get; set; }
        }
        #endregion

        #region Basic import error list - Class
        public class BasicIMPORT_SUCCESS_LIST
        {
            public string Row { get; set; }
            public string Column { get; set; }
            public string Message { get; set; }
            public string Data { get; set; }
            public string Status { get; set; }
        }
        #endregion

        #region Validation success list - Class
        public class VALIDATEMPORT_SUCCESS_LIST
        {
            public string CATALOG_NAME { get; set; }
            public string CATEGORY_NAME { get; set; }
            public string FAMILY_NAME { get; set; }
            public string CATALOG_ITEM_NO { get; set; }
        }
        #endregion

        #region Object with properties - Class
        public class ObjectWithProperties
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            public object this[string name]
            {
                get
                {
                    if (properties.ContainsKey(name))
                    {
                        return properties[name];
                    }
                    return null;
                }
                set
                {
                    properties[name] = value;
                }
            }

        }
        #endregion

        #region IComparer - Class
        class Comparer<T> : IComparer<ObjectWithProperties> where T : IComparable
        {
            string m_attributeName;
            public Comparer(string attributeName)
            {
                m_attributeName = attributeName;
            }
            public int Compare(ObjectWithProperties x, ObjectWithProperties y)
            {
                return ((T)x[m_attributeName]).CompareTo((T)y[m_attributeName]);
            }
        }
        #endregion

        #region Validation success list sub product - Class
        public class VALIDATEMPORT_SUCCESS_LIST_SUB
        {
            public string CATALOG_NAME { get; set; }
            public string CATEGORY_NAME { get; set; }
            public string FAMILY_NAME { get; set; }
            public string CATALOG_ITEM_NO { get; set; }
            public string SUBITEM { get; set; }
        }
        #endregion

        [System.Web.Http.HttpGet]
        public IList getFinishImportResults(string SessionId)
        {
            DataSet ds = new DataSet();
            List<IMPORT_ERROR_LIST> errorList = new List<IMPORT_ERROR_LIST>();
            try
            {
                if (!string.IsNullOrEmpty(SessionId))
                {
                    string[] Session = SessionId.Split(',');
                    IC._SQLString = @"select * from [tempresult" + Session[Session.Length - 1] + "]";
                    ds = IC.CreateDataSet();
                    IC._SQLString = "drop table [tempresult" + Session[Session.Length - 1] + "]";
                    var Connection = new SqlConnection(connectionString);
                    var cmd = new SqlCommand(IC._SQLString, Connection);
                    Connection.Open();
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    errorList = (from DataRow row in ds.Tables[0].Rows
                                 select new IMPORT_ERROR_LIST
                                 {
                                     ErrorMessage = row["ErrorMessage"].ToString(),
                                     ErrorProcedure = row["ErrorProcedure"].ToString(),
                                     ErrorSeverity = row["ErrorSeverity"].ToString(),
                                     ErrorState = row["ErrorState"].ToString(),
                                     ErrorNumber = row["ErrorNumber"].ToString(),
                                     ErrorLine = row["ErrorLine"].ToString()
                                 }).ToList();
                    return errorList;
                }
                else
                {
                    return errorList;
                }
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at ImportController : getFinishImportResults", ex);
                return errorList;
            }
        }

        [System.Web.Http.HttpGet]
        public IList ValidateImportResultssub(string SessionId)
        {
            DataSet ds = new DataSet();
            List<VALIDATEMPORT_SUCCESS_LIST_SUB> errorList = new List<VALIDATEMPORT_SUCCESS_LIST_SUB>();
            try
            {
                if (!string.IsNullOrEmpty(SessionId))
                {
                    string[] Session = SessionId.Split(',');
                    IC._SQLString = @"select * from [validatedresult" + Session[Session.Length - 1] + "]";
                    ds = IC.CreateDataSet();
                    IC._SQLString = "drop table [validatedresult" + Session[Session.Length - 1] + "]";
                    var Connection = new SqlConnection(connectionString);
                    var cmd = new SqlCommand(IC._SQLString, Connection);
                    Connection.Open();
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    errorList = (from DataRow row in ds.Tables[0].Rows
                                 select new VALIDATEMPORT_SUCCESS_LIST_SUB
                                 {
                                     CATALOG_NAME = row["CATALOG_NAME"].ToString(),
                                     CATEGORY_NAME = row["CATEGORY_NAME"].ToString(),
                                     FAMILY_NAME = row["FAMILY_NAME"].ToString(),
                                     CATALOG_ITEM_NO = row["CATALOG_ITEM_NO"].ToString(),
                                     SUBITEM = row["SUBITEM"].ToString()
                                 }).ToList();
                    return errorList;
                }
                else
                {
                    return errorList;
                }
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at ImportController : ValidateImportResults", ex);
                return errorList;
            }
        }

        [System.Web.Http.HttpGet]
        public JsonResult ValidateImportResults(string SessionId, string importType)
        {
            DataSet ds = new DataSet();
            List<VALIDATEMPORT_SUCCESS_LIST> errorList = new List<VALIDATEMPORT_SUCCESS_LIST>();
            try
            {
                DashBoardModel model = new DashBoardModel();
                if (!string.IsNullOrEmpty(SessionId))
                {
                    string[] Session = SessionId.Split(',');
                    var Connection = new SqlConnection(connectionString);
                    DataSet finalValidationResult = new DataSet();
                    if (HttpContext.Current.Session["ITEMVALIDATIONSESSION"] != null)
                    {
                        finalValidationResult = HttpContext.Current.Session["ITEMVALIDATIONSESSION"] as DataSet;
                    }
                    else if (HttpContext.Current.Session["FAMILYVALIDATIONSESSION"] != null)
                    {
                        finalValidationResult = HttpContext.Current.Session["FAMILYVALIDATIONSESSION"] as DataSet;
                    }
                    else
                    {
                        var getFinalValidation = new SqlCommand("select '" + importType + "' as Import_Type,* from [ValidateFullList" + Session[Session.Length - 1] + "]", Connection);
                        var dagetFinalValidation = new SqlDataAdapter(getFinalValidation);
                        dagetFinalValidation.Fill(finalValidationResult);
                        Connection.Open();
                        var cmbpk14 = new SqlCommand("drop table [ValidateFullList" + Session[Session.Length - 1] + "]", Connection);
                        cmbpk14.ExecuteNonQuery();
                        Connection.Close();
                    }
                    foreach (DataColumn columns in finalValidationResult.Tables[0].Columns)
                    {
                        var objPTColumns = new PTColumns();
                        objPTColumns.Caption = columns.Caption;
                        objPTColumns.ColumnName = columns.ColumnName;
                        model.Columns.Add(objPTColumns);
                    }
                    string JSONString = string.Empty;
                    JSONString = JsonConvert.SerializeObject(finalValidationResult.Tables[0]);
                    model.Data = JSONString;
                    return new JsonResult() { Data = model };
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at ImportController : ValidateImportResults", ex);
                return null;
            }
        }


        [System.Web.Http.HttpGet]
        public JsonResult validatetabledesignerImportResults(string SessionId, string importType)
        {
            DataSet ds = new DataSet();
            List<VALIDATEMPORT_SUCCESS_LIST> errorList = new List<VALIDATEMPORT_SUCCESS_LIST>();
            try
            {
                DashBoardModel model = new DashBoardModel();

                string[] Session = SessionId.Split(',');
                var Connection = new SqlConnection(connectionString);
                DataSet finalValidationResult = new DataSet();
                DataTable dt = new DataTable();
                dt = (DataTable)HttpContext.Current.Session["ValidateTableDesigner"];


                ds.Tables.Add(dt);
                //if (HttpContext.Current.Session["ITEMVALIDATIONSESSION"] != null)
                //{
                //    finalValidationResult = HttpContext.Current.Session["ITEMVALIDATIONSESSION"] as DataSet;
                //}
                //else if (HttpContext.Current.Session["FAMILYVALIDATIONSESSION"] != null)
                //{
                //    finalValidationResult = HttpContext.Current.Session["FAMILYVALIDATIONSESSION"] as DataSet;
                //}
                //else
                //{
                //    var getFinalValidation = new SqlCommand("select '" + importType + "' as Import_Type,* from [ValidateFullList" + Session[Session.Length - 1] + "]", Connection);
                //    var dagetFinalValidation = new SqlDataAdapter(getFinalValidation);
                //    dagetFinalValidation.Fill(finalValidationResult);
                //    Connection.Open();
                //    var cmbpk14 = new SqlCommand("drop table [ValidateFullList" + Session[Session.Length - 1] + "]", Connection);
                //    cmbpk14.ExecuteNonQuery();
                //    Connection.Close();
                //}
                foreach (DataColumn columns in ds.Tables[0].Columns)
                {
                    var objPTColumns = new PTColumns();
                    objPTColumns.Caption = columns.Caption;
                    objPTColumns.ColumnName = columns.ColumnName;
                    model.Columns.Add(objPTColumns);
                }
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(ds.Tables[0]);
                model.Data = JSONString;
                return new JsonResult() { Data = model };

            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at ImportController : ValidateImportResults", ex);
                return null;
            }
        }




        public void ExportErrorLogDataSetToExcel(DataSet finalDs, string fileName, string excelPath, string sheetName, string extension)
        {
            try
            {
                DataTable dtexcel = new DataTable();
                DataTable dt = new DataTable();
                OleDbConnection conn = adImport.excelConnection(excelPath);
                DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
                DataTable ColumnRowIds = new DataTable();
                ColumnRowIds.Columns.Add("ColumnId");
                ColumnRowIds.Columns.Add("RowId");
                ColumnRowIds.Columns.Add("Message");
                DataRow schemaRow = schemaTable.Rows[0];
                string sheet = schemaRow["TABLE_NAME"].ToString();
                if (!sheet.EndsWith("_"))
                {
                    string query = "SELECT  * FROM [" + sheetName + "$]";
                    OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                    dt.Locale = CultureInfo.CurrentCulture;
                    daexcel.Fill(dt);
                }
                conn.Close();
                conn.Dispose();
                if (dt.Columns[0].ColumnName.ToUpper() == "ACTION")
                {
                    if (dt.Columns.Contains("ITEM#"))
                    {
                        dt.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                    }
                    //dtexcel = dt.Clone();
                    foreach (DataColumn dc in dt.Columns)
                    {
                        dtexcel.Columns.Add(dc.ColumnName);
                    }
                    DataRow dr = dtexcel.NewRow();
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        dr[i] = dt.Columns[i].ColumnName;
                    }
                    dtexcel.Rows.Add(dr);
                    foreach (DataRow drow in dt.Rows)
                    {
                        dtexcel.Rows.Add(drow.ItemArray);
                    }
                }
                else
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        dtexcel.Columns.Add(dc.ColumnName);
                    }
                    int i = 0;
                    foreach (DataRow drow in dt.Rows)
                    {
                        if (i == 0)
                        {
                            foreach (var attrValue in drow.ItemArray)
                            {
                                if (attrValue.ToString().ToUpper() == "ITEM#")
                                {
                                    drow.ItemArray[i] = "CATALOG_ITEM_NO";
                                }
                                i++;
                            }
                        }
                        dtexcel.Rows.Add(drow.ItemArray);
                    }
                }
                foreach (DataRow dr in finalDs.Tables[0].Rows)
                {
                    int rowId = int.Parse(dr["ROWID"].ToString());
                    int i = 0;
                    DataRow dr1 = dtexcel.Rows[rowId - 2];
                    if (dr["ATTRIBUTE_VALUES"].ToString() != "-")
                    {
                        foreach (var attrValue in dr1.ItemArray)
                        {
                            var attName = dr["ATTRIBUTE_NAME"].ToString();
                            var attrName1 = dtexcel.Rows[0].ItemArray[i].ToString();
                            if (dr["ATTRIBUTE_VALUES"].ToString() == attrValue.ToString() && dtexcel.Rows[0].ItemArray[i].ToString().Replace("_", " ").ToUpper() == attName.Replace("_", " ").ToUpper())
                            {
                                DataRow drColumnRowIds = ColumnRowIds.NewRow();
                                drColumnRowIds[0] = i;
                                drColumnRowIds[1] = rowId;
                                drColumnRowIds[2] = dr["MESSAGE"].ToString();
                                ColumnRowIds.Rows.Add(drColumnRowIds);
                            }
                            i = i + 1;
                        }
                    }
                }
                CreateExcelDataTable(dtexcel, sheetName, ColumnRowIds, fileName, extension);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : ExportErrorLogDataSetToExcel", ex);
            }
        }

        public void CreateExcelDataTable(DataTable table, string sheetName, DataTable ColumnRowIds, string fileName, string extension)
        {
            var workbook = new Workbook();
            if (table.Rows.Count > 0)
            {
                string tableName = sheetName;
                int rowcntTemp = table.Rows.Count;
                int j = 0;
                int runningcnt = 0;
                do
                {
                    int rowcnt;
                    if (rowcntTemp <= 65000)
                    {
                        rowcnt = rowcntTemp;
                        rowcntTemp = rowcnt - 65000;
                    }
                    else
                    {
                        rowcnt = 65000;
                        rowcntTemp = table.Rows.Count - (runningcnt + 65000);
                    }
                    j++;
                    if (j != 1)
                    {
                        tableName = sheetName + (j - 1);
                    }
                    var worksheet = workbook.Worksheets.Add(tableName);
                    workbook.WindowOptions.SelectedWorksheet = worksheet;
                    string[] firestRow = new string[] { "PROCESS", "YES", "NO", "PRODUCT", "PRODUCTS", "IMPORTTYPE", "FAMILIES", "SUBPRODUCTS", "SUBPRODUCT" };
                    for (int jj = 1; jj <= rowcnt; jj++)
                    {
                        // Create the worksheet to represent this data table
                        // Create column headers for each column
                        for (int columnIndex = 0; columnIndex < table.Columns.Count; columnIndex++)
                        {
                            if (firestRow.Contains(table.Columns[columnIndex].ColumnName.ToString().ToUpper()))
                            {
                                worksheet.Rows[0].Cells[columnIndex].Value = HttpUtility.HtmlDecode(table.Columns[columnIndex].ColumnName);
                            }
                            else
                            {
                                worksheet.Rows[0].Cells[columnIndex].Value = null;
                            }
                        }
                    }
                    // Starting at row index 1, copy all data rows in
                    // the data table to the worksheet
                    int rowIndex = 1;
                    int temprunningcnt = runningcnt;
                    for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                    {
                        var row = worksheet.Rows[rowIndex++];
                        runningcnt++;
                        if (table.Columns[0].ColumnName.ToString().ToUpper().Contains("ACTION") && k == 0)
                        {
                            int columnIndex = 0;
                            foreach (DataColumn dc in table.Columns)
                            {
                                row.Cells[columnIndex].Value = dc.ColumnName;
                                columnIndex++;
                            }
                            continue;
                        }
                        for (int columnIndex = 0; columnIndex < table.Rows[k].ItemArray.Length; columnIndex++)
                        {
                            row.Cells[columnIndex].Value = table.Rows[k].ItemArray[columnIndex];
                        }
                    }
                } while (rowcntTemp > 0);
                if (workbook.Worksheets[0] != null)
                {
                    var worksheet1 = workbook.Worksheets[0];
                    workbook.WindowOptions.SelectedWorksheet = worksheet1;
                    foreach (DataRow dr in ColumnRowIds.Rows)
                    {
                        int rowId = int.Parse(dr["RowId"].ToString()) - 1;
                        int columnId = int.Parse(dr["ColumnId"].ToString());
                        worksheet1.Rows[rowId].Cells[columnId].CellFormat.Font.ColorInfo = System.Drawing.Color.White;
                        worksheet1.Rows[rowId].Cells[columnId].Comment = new WorksheetCellComment { Text = new FormattedString(dr["Message"].ToString()) };
                        // cellComments.Text= new FormattedString(dr["Message"].ToString());
                        IWorksheetCellFormat cellFormat = worksheet1.Rows[rowId].Cells[columnId].CellFormat;
                        cellFormat.FillPatternBackgroundColor = System.Drawing.Color.Red;
                        //   worksheet1.Rows[rowId].Cells[columnId].CellFormat.Fill = System.Drawing.Color.Red;
                        //     CellFillPattern cellFill = (CellFillPattern)worksheet1.Rows[rowId].Cells[columnId].CellFormat.Style.StyleFormat.Fill;
                        //cellFill.BackgroundColorInfo = XLColor.Red;
                        //   cellFill.BackgroundColorInfo.Color.Value.IsNamedColor = System.Drawing.Color.Red;
                    }
                }
                if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/Content/ValidationLog")))
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/Content/ValidationLog"));
                }
                if (extension.ToLower() == "xlsx")
                {
                    workbook.SetCurrentFormat(WorkbookFormat.Excel2007);
                }
                workbook.WindowOptions.SelectedWorksheet.Protected = true;
                workbook.Save(HttpContext.Current.Server.MapPath("~/Content/ValidationLog/" + fileName + "." + extension.ToLower()));
                //if (System.IO.File.Exists(HttpContext.Current.Server.MapPath("~/Content/ValidationLog/" + sheetName)))
                //{
                //    HttpContext.Current.Response.ClearContent();
                //    HttpContext.Current.Response.Clear();
                //    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                //    //response.AddHeader("Content-Disposition","attachment; filename=" + filename + ";");
                //    HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", sheetName));
                //    HttpContext.Current.Response.TransmitFile(HttpContext.Current.Server.MapPath("~/Content/ValidationLog/" + sheetName));
                //    HttpContext.Current.Response.Flush();
                //    HttpContext.Current.Response.End();
            }
            else
            {
                HttpContext.Current.Response.ContentType = "text/plain";
                HttpContext.Current.Response.Write("File not found!");
            }
        }

        public void ExportDataSetToExcel(DataSet finalDs, string fileName)
        {
            try
            {
                if (finalDs.Tables[0].Columns.Contains("FAMILY_ID_1"))
                {
                    finalDs.Tables[0].Columns.Remove("FAMILY_ID_1");
                }
                if (finalDs.Tables[0].Columns.Contains("FAMILY_FOOT_NOTES"))
                {
                    finalDs.Tables[0].Columns.Remove("FAMILY_FOOT_NOTES");
                }
                if (finalDs.Tables[0].Columns.Contains("FAMILY_STATUS"))
                {
                    finalDs.Tables[0].Columns.Remove("FAMILY_STATUS");
                }
                if (fileName.Trim() != "")
                {
                    object context;
                    if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                    {
                        var httpContext = context as HttpContextBase;
                        if (httpContext != null && httpContext.Session != null)
                        {
                            if (finalDs.Tables[0].Rows.Count > 0)
                            {
                                httpContext.Session["ExportTable"] = finalDs.Tables[0];
                            }
                            if (finalDs.Tables[0].Rows.Count > 1 & finalDs.Tables.Count > 2)
                            {
                                httpContext.Session["ExportTableSubProduct"] = finalDs.Tables[1];
                            }
                        }
                    }
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at ImportApiController : ExportXls", objexception);
                //return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        public void ExportLogDataSetToExcel(DataSet finalDs, string fileName)
        {
            try
            {
                if (finalDs.Tables[0].Columns.Contains("FAMILY_ID_1"))
                {
                    finalDs.Tables[0].Columns.Remove("FAMILY_ID_1");
                }
                if (finalDs.Tables[0].Columns.Contains("FAMILY_FOOT_NOTES"))
                {
                    finalDs.Tables[0].Columns.Remove("FAMILY_FOOT_NOTES");
                }
                if (finalDs.Tables[0].Columns.Contains("FAMILY_STATUS"))
                {
                    finalDs.Tables[0].Columns.Remove("FAMILY_STATUS");
                }
                if (fileName.Trim() != "")
                {
                    object context;
                    if (Request.Properties.TryGetValue("MS_HttpContext", out context))
                    {
                        var httpContext = context as HttpContextBase;
                        if (httpContext != null && httpContext.Session != null)
                        {
                            if (finalDs.Tables[0].Rows.Count > 0)
                            {
                                httpContext.Session["ExportLogTable"] = finalDs;
                                httpContext.Session["ExportTable"] = null;
                            }
                        }
                    }
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at ImportApiController : ExportXls", objexception);
                //return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [System.Web.Http.HttpGet]
        public IList GetTestRecord()
        {
            var objSuccesslist = "testRecord";
            return objSuccesslist.ToList();
        }

        [System.Web.Http.HttpGet]
        public IList getFinishImportSuccessResults(string SessionId, string option)
        {
            DataSet ds = new DataSet();
            List<IMPORT_SUCCESS_LIST> successList = new List<IMPORT_SUCCESS_LIST>();
            try
            {
                if (!string.IsNullOrEmpty(SessionId))
                {
                    string[] Session = SessionId.Split(',');
                    IC._SQLString = @"IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'tempresult" + Session[Session.Length - 1] + "')Begin select * from [tempresult" + Session[Session.Length - 1] + "]End else IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'tempresultsub" + Session[Session.Length - 1] + "')Begin select * from [tempresultsub" + Session[Session.Length - 1] + "]End";
                    ds = IC.CreateDataSet();
                    IC._SQLString = @"IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'tempresult" + Session[Session.Length - 1] + "')Begin drop table [tempresult" + Session[Session.Length - 1] + "]End else IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'tempresultsub" + Session[Session.Length - 1] + "')Begin drop table [tempresultsub" + Session[Session.Length - 1] + "]End";
                    var Connection = new SqlConnection(connectionString);
                    var cmd = new SqlCommand(IC._SQLString, Connection);
                    Connection.Open();
                    cmd.CommandText = IC._SQLString;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    if (option.ToUpper().Contains("SUBPRODUCT"))
                    {
                        successList = (from DataRow row in ds.Tables[0].Rows
                                       select new IMPORT_SUCCESS_LIST
                                       {
                                           CATALOG_NAME = row["CATALOG_NAME"].ToString(),
                                           CATEGORY_DETAILS = row["CATEGORY_DETAILS"].ToString(),
                                           FAMILY_DETAILS = row["FAMILY_DETAILS"].ToString(),
                                           SUB_PRODUCT_DETAILS = row["SUB_PRODUCT_DETAILS"].ToString(),
                                           PRODUCT_DETAILS = row["PRODUCT_DETAILS"].ToString(),
                                           STATUS = row["STATUS"].ToString()
                                       }).ToList();
                    }
                    else if (option.ToUpper().Contains("PRODUCT"))
                    {
                        successList = (from DataRow row in ds.Tables[0].Rows
                                       select new IMPORT_SUCCESS_LIST
                                       {
                                           CATALOG_NAME = row["CATALOG_NAME"].ToString(),
                                           CATEGORY_DETAILS = row["CATEGORY_DETAILS"].ToString(),
                                           FAMILY_DETAILS = row["FAMILY_DETAILS"].ToString(),
                                           SUBFAMILY_DETAILS = row["SUBFAMILY_DETAILS"].ToString(),
                                           PRODUCT_DETAILS = row["PRODUCT_DETAILS"].ToString(),
                                           STATUS = row["STATUS"].ToString()
                                       }).ToList();
                    }
                    else
                    {
                        successList = (from DataRow row in ds.Tables[0].Rows
                                       select new IMPORT_SUCCESS_LIST
                                       {
                                           CATALOG_NAME = row["CATALOG_NAME"].ToString(),
                                           CATEGORY_DETAILS = row["CATEGORY_DETAILS"].ToString(),
                                           FAMILY_DETAILS = row["FAMILY_DETAILS"].ToString(),
                                           STATUS = row["STATUS"].ToString()
                                       }).ToList();
                    }
                }
                System.Web.HttpContext.Current.Session["downloadLogFamilyPage"] = ds.Tables[0];
                //JsonResult jsonResult = new JsonResult();
                //jsonResult.MaxJsonLength = int.MaxValue;
                //jsonResult = Json(successList, JsonRequestBehavior.AllowGet);
                ////return jsonResult;
                //return Json(jsonResult, JsonRequestBehavior.AllowGet);
                return successList;
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at ImportController : getFinishImportResults", ex);
                return successList;
            }
        }

        [System.Web.Http.HttpGet]
        public IList getFinishImportFailedpicklistResults(string SessionId)
        {
            DataSet ds = new DataSet();
            if (SessionId == "undefined")
            {
                return null;
            }
            List<IMPORT_ERROR_LIST> errorList = new List<IMPORT_ERROR_LIST>();
            try
            {
                if (!string.IsNullOrEmpty(SessionId))
                {
                    string[] Session = SessionId.Split(',');
                    IC._SQLString = @"select * from [Picklistlog" + Session[Session.Length - 1] + "]";
                    ds = IC.CreateDataSet();
                    IC._SQLString = "drop table [Picklistlog" + Session[Session.Length - 1] + "]";
                    var Connection = new SqlConnection(connectionString);
                    var cmd = new SqlCommand(IC._SQLString, Connection);
                    Connection.Open();
                    cmd.CommandText = IC._SQLString;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    var errorList3 = (from DataRow row in ds.Tables[0].Rows
                                      select new
                                      {
                                          STATUS = row["STATUS"].ToString(),
                                          ITEM_NO = row["ITEM_NO"].ToString(),
                                          ATTRIBUTE_NAME = row["ATTRIBUTE_NAME"].ToString(),
                                          PICKLIST_VALUE = row["PICKLIST_VALUE"].ToString()
                                      }).ToList();
                    return errorList3;
                }
                else
                {
                    return errorList;
                }
                //JsonResult jsonResult = new JsonResult();
                //jsonResult.MaxJsonLength = int.MaxValue;
                //jsonResult = Json(successList, JsonRequestBehavior.AllowGet);
                ////return jsonResult;
                //return Json(jsonResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at ImportController : getFinishImportFailedpicklistResults", ex);
                return errorList;
            }
        }

        [System.Web.Http.HttpGet]
        public IList getFinishFamilyImportResults(string SessionId)
        {
            DataSet ds = new DataSet();
            if (SessionId == "undefined")
            {
                return null;
            }
            List<IMPORT_ERROR_LIST> errorList = new List<IMPORT_ERROR_LIST>();
            try
            {
                if (!string.IsNullOrEmpty(SessionId))
                {
                    string[] Session = SessionId.Split(',');
                    IC._SQLString = @"if exists(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='[tempresult" + Session[Session.Length - 1] + "]')Begin select * from [tempresult" + Session[Session.Length - 1] + "]End else if if exists(SELECT NAME FROM TEMPDB.SYS.OBJECTS WHERE TYPE='U' AND NAME='[tempresultsub" + Session[Session.Length - 1] + "]')Begin select * from [tempresultsub" + Session[Session.Length - 1] + "]End";
                    ds = IC.CreateDataSet();
                    IC._SQLString = "drop table [tempresult" + Session[Session.Length - 1] + "]";
                    var Connection = new SqlConnection(connectionString);
                    var cmd = new SqlCommand(IC._SQLString, Connection);
                    Connection.Open();
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    var errorList1 = (from DataRow row in ds.Tables[0].Rows
                                      select new
                                      {
                                          ErrorMessage = row["ErrorMessage"].ToString(),
                                          CATALOG_NAME = row["CATALOG_NAME"].ToString(),
                                          CATEGORY_NAME = row["CATEGORY_NAME"].ToString(),
                                          PRODUCT_ID = row["PRODUCT_ID"].ToString(),
                                          FAMILY_NAME = row["FAMILY_NAME"].ToString(),
                                          STATUS = row["STATUS"].ToString(),
                                          ATTRIBUTE_NAME = row["ATTRIBUTE_NAME"].ToString(),
                                          ATTRIBUTE_VALUE = row["ATTRIBUTE_VALUE"].ToString()
                                      }).ToList();
                    System.Web.HttpContext.Current.Session["downloadLogFamilyPage"] = ds.Tables[0];
                    return errorList1;
                }
                else
                {
                    return errorList;
                }
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at ImportController : getFinishImportResults", ex);
                return errorList;
            }
        }

        [System.Web.Http.HttpGet]
        public IList getFinishImportFailedpicklistResults12(string SessionId)
        {
            DataSet ds = new DataSet();
            if (SessionId == "undefined")
            {
                return null;
            }
            List<IMPORT_ERROR_LIST> errorList = new List<IMPORT_ERROR_LIST>();
            try
            {
                if (!string.IsNullOrEmpty(SessionId))
                {
                    string[] Session = SessionId.Split(',');
                    IC._SQLString = @"select * from [errorlog" + Session[Session.Length - 1] + "]";
                    ds = IC.CreateDataSet();
                    IC._SQLString = "drop table [errorlog" + Session[Session.Length - 1] + "]";
                    var Connection = new SqlConnection(connectionString);
                    var cmd = new SqlCommand(IC._SQLString, Connection);
                    Connection.Open();
                    cmd.CommandText = IC._SQLString;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    var errorList3 = (from DataRow row in ds.Tables[0].Rows
                                      select new
                                      {
                                          STATUS = row["STATUS"].ToString(),
                                          PRODUCT_ID = row["PRODUCT_ID"].ToString(),
                                          ITEM_NO = row["ITEM_NO"].ToString()
                                      }).ToList();
                    return errorList3;
                }
                else
                {
                    return errorList;
                }
                //JsonResult jsonResult = new JsonResult();
                //jsonResult.MaxJsonLength = int.MaxValue;
                //jsonResult = Json(successList, JsonRequestBehavior.AllowGet);
                ////return jsonResult;
                //return Json(jsonResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at ImportController : getFinishImportResults", ex);
                return errorList;
            }
        }

        [System.Web.Http.HttpGet]
        public IList getFinishImportFailedpicklistResultssubproducts(string SessionId)
        {
            DataSet ds = new DataSet();
            if (SessionId == "undefined")
            {
                return null;
            }
            List<IMPORT_ERROR_LIST> errorList = new List<IMPORT_ERROR_LIST>();
            try
            {
                if (!string.IsNullOrEmpty(SessionId))
                {
                    string[] Session = SessionId.Split(',');
                    IC._SQLString = @"select * from [tempresultsub" + Session[Session.Length - 1] + "]";
                    ds = IC.CreateDataSet();
                    IC._SQLString = "drop table [tempresultsub" + Session[Session.Length - 1] + "]";
                    var Connection = new SqlConnection(connectionString);
                    var cmd = new SqlCommand(IC._SQLString, Connection);
                    Connection.Open();
                    cmd.CommandText = IC._SQLString;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    var errorList3 = (from DataRow row in ds.Tables[0].Rows
                                      select new
                                      {
                                          CATALOG_NAME = row["CATALOG_NAME"].ToString(),
                                          PRODUCT_DETAILS = row["PRODUCT_DETAILS"].ToString(),
                                          SUBPRODUCT_DETAILS = row["SUB_PRODUCT_DETAILS"].ToString(),
                                          STATUS = row["STATUS"].ToString()
                                      }).ToList();
                    return errorList3;
                }
                else
                {
                    return errorList;
                }
                //JsonResult jsonResult = new JsonResult();
                //jsonResult.MaxJsonLength = int.MaxValue;
                //jsonResult = Json(successList, JsonRequestBehavior.AllowGet);
                ////return jsonResult;
                //return Json(jsonResult, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at ImportController : getFinishImportResults", ex);
                return errorList;
            }
        }

        [System.Web.Http.HttpGet]
        public IList getFinishImportFailedpicklistResultssubproductserrordelete(string SessionId)
        {
            DataSet ds = new DataSet();
            if (SessionId == "undefined")
            {
                return null;
            }
            List<IMPORT_ERROR_LISTdel> errorList = new List<IMPORT_ERROR_LISTdel>();
            try
            {
                if (!string.IsNullOrEmpty(SessionId))
                {
                    string[] Session = SessionId.Split(',');
                    IC._SQLString = @"select * from [deleteresult" + Session[Session.Length - 1] + "]";
                    ds = IC.CreateDataSet();
                    IC._SQLString = "drop table [deleteresult" + Session[Session.Length - 1] + "]";
                    var Connection = new SqlConnection(connectionString);
                    var cmd = new SqlCommand(IC._SQLString, Connection);
                    Connection.Open();
                    cmd.CommandText = IC._SQLString;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    var errorList3 = (from DataRow row in ds.Tables[0].Rows

                                      select new
                                      {
                                          CATALOG_ITEM_NO = row["CATALOG_ITEM_NO"].ToString(),
                                          STATUS = row["STATUS"].ToString()

                                      }).ToList();
                    return errorList3;
                }
                else
                {
                    return errorList;
                }
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at ImportController : getFinishImportResults", ex);
                return errorList;
            }
        }

        [System.Web.Http.HttpGet]
        public IList getFinishImportFailedpicklistResultssubproductserror(string SessionId)
        {
            DataSet ds = new DataSet();
            if (SessionId == "undefined")
            {
                return null;
            }
            List<IMPORT_ERROR_LIST> errorList = new List<IMPORT_ERROR_LIST>();
            try
            {
                if (!string.IsNullOrEmpty(SessionId))
                {
                    string[] Session = SessionId.Split(',');
                    IC._SQLString = @"select * from [tempresultsub" + Session[Session.Length - 1] + "]";
                    ds = IC.CreateDataSet();
                    IC._SQLString = "drop table [tempresultsub" + Session[Session.Length - 1] + "]";
                    var Connection = new SqlConnection(connectionString);
                    var cmd = new SqlCommand(IC._SQLString, Connection);
                    Connection.Open();
                    cmd.CommandText = IC._SQLString;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                    Connection.Close();
                    var errorList3 = (from DataRow row in ds.Tables[0].Rows
                                      select new
                                      {
                                          ErrorMessage = row["ErrorMessage"].ToString(),
                                          CATALOG_NAME = row["CATALOG_NAME"].ToString(),
                                          CATEGORY_NAME = row["CATEGORY_NAME"].ToString(),
                                          PRODUCT_ID = row["PRODUCT_ID"].ToString(),
                                          STATUS = row["STATUS"].ToString(),
                                          ATTRIBUTE_NAME = row["ATTRIBUTE_NAME"].ToString(),
                                          ATTRIBUTE_VALUE = row["ATTRIBUTE_VALUE"].ToString()
                                      }).ToList();
                    return errorList3;
                }
                else
                {
                    return errorList;
                }
            }
            catch (Exception ex)
            {
                Message = "Import Failed, please try again";
                _logger.Error("Error at ImportApiController : getFinishImportResults", ex);
                return errorList;
            }
        }

        public void DoFamilyTableStructureInsertUpdate()
        {
            try
            {
                SqlConnection SqlCon = new SqlConnection(connectionString);
                SqlCon.Open();
                SqlCommand Sqlcom = new SqlCommand("Exec STP_CATALOGSTUDIO5_InsertFamilyTableStructure " + _FamilyId + "," + _CatalogId + ",'" + _categoryId + "','','',0", SqlCon);
                int flag = Sqlcom.ExecuteNonQuery();
                SqlCon.Close();
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : DoFamilyTableStructureInsertUpdate", ex);
            }
        }

        #region Item# import Process - Validation
        // Item# import Process - Validation Start
        #region ValidateItemNo Function
        /// <summary>
        /// Validation process for Item# only
        /// </summary>
        /// <param name="validateItems">Datatable from excel sheet</param>
        /// <param name="importtype">PRODUCT/SUBPRODUCT</param>
        /// <param name="model">Attribute list(with name name and type)</param>
        /// <param name="excelPath">uploaded the excel sheet path</param>
        /// <param name="SheetName">uploaded sheet name</param>
        /// <param name="catalogId">current catalog id</param>
        /// <param name="Errorlogoutputformat">download output format(xls,xlsx,csv,text)</param>
        /// <returns></returns>
        public string ValidateItemNo(DataTable validateItems, string importtype, JArray model, string excelPath, string SheetName, int catalogId, string Errorlogoutputformat)
        {
            string validateResults = string.Empty;
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            string validateResultsforitemno = string.Empty;
            ValidationList validationListForItemno = new ValidationList();
            DataTable oattType = new DataTable();
            DataTable oattType1 = new DataTable();
            ValidationList valdationList = new ValidationList();
            DataTable objdatatable = validateItems.Clone();
            DataTable duplicateCatalogItemNo = new DataTable();
            try
            {
                objdatatable.Columns.Add("ROW_ID");
                objdatatable.Columns["ROW_ID"].AutoIncrement = true;
                objdatatable.Columns["ROW_ID"].AutoIncrementSeed = 1;
                foreach (DataRow drItems in validateItems.Rows)
                {
                    objdatatable.Rows.Add(drItems.ItemArray);
                }
                int customerId;
                int.TryParse(objLS.Customer_User.Where(customer => customer.User_Name == User.Identity.Name).Select(custId => custId.CustomerId).FirstOrDefault().ToString(), out customerId);
                foreach (DataColumn replaceColumn in objdatatable.Columns)
                {
                    if (replaceColumn.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerItemNo"].ToString())
                        objdatatable.Columns[HttpContext.Current.Session["CustomerItemNo"].ToString()].ColumnName = "CATALOG_ITEM_NO";
                    if (replaceColumn.ColumnName.ToUpper() == HttpContext.Current.Session["CustomerSubItemNo"].ToString())
                        objdatatable.Columns[HttpContext.Current.Session["CustomerSubItemNo"].ToString()].ColumnName = "SUBCATALOG_ITEM_NO";
                }
                if (!objdatatable.Columns.Contains("SUBPRODUCT_ID"))
                {
                    objdatatable.Columns.Add("SUBPRODUCT_ID");
                    if (objdatatable.Columns.Contains("SUBCATALOG_ITEM_NO"))
                        objdatatable.Columns["SUBPRODUCT_ID"].SetOrdinal(objdatatable.Columns.IndexOf("SUBCATALOG_ITEM_NO"));
                    else
                        objdatatable.Columns["SUBPRODUCT_ID"].SetOrdinal(2);
                }
                else
                {
                    if (objdatatable.Columns.Contains("SUBCATALOG_ITEM_NO"))
                        objdatatable.Columns["SUBPRODUCT_ID"].SetOrdinal(objdatatable.Columns.IndexOf("SUBCATALOG_ITEM_NO"));
                    else
                        objdatatable.Columns["SUBPRODUCT_ID"].SetOrdinal(2);
                }
                if (!objdatatable.Columns.Contains("PRODUCT_ID"))
                {
                    objdatatable.Columns.Add("PRODUCT_ID");
                    if (objdatatable.Columns.Contains("CATALOG_ITEM_NO"))
                        objdatatable.Columns["PRODUCT_ID"].SetOrdinal(objdatatable.Columns.IndexOf("CATALOG_ITEM_NO"));
                    else
                        objdatatable.Columns["PRODUCT_ID"].SetOrdinal(2);
                }
                else
                {
                    if (objdatatable.Columns.Contains("CATALOG_ITEM_NO"))
                        objdatatable.Columns["PRODUCT_ID"].SetOrdinal(objdatatable.Columns.IndexOf("CATALOG_ITEM_NO"));
                    else
                        objdatatable.Columns["PRODUCT_ID"].SetOrdinal(2);
                }
                // Get Missing Column List
                string[] manadoryColumns = { "CATALOG_ITEM_NO" };
                if (importtype.ToUpper().Contains("SUBPRODUCT"))
                {
                    manadoryColumns = new string[] { "CATALOG_ITEM_NO", "SUBCATALOG_ITEM_NO" };
                }

                DataTable MissingColumns = MissingColumnName(objdatatable, manadoryColumns, importtype);
                if (!validateItems.Columns.Contains("CATALOG_ITEM_NO"))
                {
                    return ErrorListCatalog(MissingColumns, importtype, SheetName, importTemp, excelPath, Errorlogoutputformat);
                }
                if (!validateItems.Columns.Contains("SUBCATALOG_ITEM_NO") && importtype.ToUpper().Contains("SUBPRODUCT"))
                {
                    return ErrorListCatalog(MissingColumns, importtype, SheetName, importTemp, excelPath, Errorlogoutputformat);
                }
                // Get Duplicate Item Number
                if (importtype.ToUpper().Contains("SUBPRODUCT"))
                {
                    duplicateCatalogItemNo = DuplicateSubCatalogItemNo(objdatatable, catalogId);
                }
                else
                {
                    duplicateCatalogItemNo = DuplicateCatalogItemNo(objdatatable, catalogId);
                }
                DataTable unPivotTable = adImport.UnPivotTable(objdatatable, importtype);
                // Get New Catalog Item Number
                var defaultFamily = objLS.TB_FAMILY.Join(objLS.TB_CATALOG, TF => TF.FAMILY_ID, TC => TC.DEFAULT_FAMILY, (TF, TC) => new { TF, TC }).Join(objLS.Customer_Catalog, tc1 => tc1.TC.CATALOG_ID, cc => cc.CATALOG_ID, (tc1, cc) => new { tc1, cc }).Where(df => df.cc.CustomerId == customerId && df.tc1.TC.CATALOG_ID == catalogId && df.tc1.TF.FLAG_RECYCLE == "A").Select(def => def.tc1.TC.DEFAULT_FAMILY).FirstOrDefault().ToString();
                defaultFamily = defaultFamily.Replace("0", "").Replace("null", "");
                DataTable newCatalogItemNo = new DataTable();
                if (duplicateCatalogItemNo != null)//To check duplicateCatalogItemNo is not null
                {
                    newCatalogItemNo = duplicateCatalogItemNo.Clone();
                }
                if (importtype.ToUpper().Contains("SUBPRODUCT"))
                {
                    DataTable newSubCatalogItemNo = NewCatalogItemNo(objdatatable, catalogId, "PRODUCT");
                    if (newSubCatalogItemNo != null && newSubCatalogItemNo.Rows.Count > 0)
                    {
                        DataTable newCatalogItemNoExists = NewCatalogItemNo(objdatatable, catalogId, importtype);
                        if (newCatalogItemNoExists.Columns.Contains("CATALOG_ITEM_NO"))
                            newCatalogItemNoExists.Columns["CATALOG_ITEM_NO"].SetOrdinal(1);
                        newCatalogItemNo = newCatalogItemNoExists.Clone();
                        foreach (DataRow dRow in newCatalogItemNoExists.Rows)
                        {
                            DataRow[] newItem = newSubCatalogItemNo.Select("CATALOG_ITEM_NO='" + dRow["CATALOG_ITEM_NO"].ToString() + "'");
                            if (newItem.Length > 0)
                                newCatalogItemNo.Rows.Add(dRow.ItemArray);
                        }
                        //  newCatalogItemNo = newSubCatalogItemNo.AsEnumerable().Intersect(newCatalogItemNoExists.AsEnumerable(), DataRowComparer.Default).CopyToDataTable();
                        //newSubCatalogItemNo.Columns.Add("SUBCATALOG_ITEM_NO");
                        //newCatalogItemNo = newSubCatalogItemNo.Clone();
                        //foreach (DataRow drNewItem in newSubCatalogItemNo.Rows)
                        //{
                        //    var catalogNo = drNewItem["CATALOG_ITEM_NO"].ToString();
                        //    var subCatalogItems = unPivotTable.AsEnumerable().Where(item => item.Field<string>("CATALOG_ITEM_NO") == catalogNo).Select(getItem => new { SUBCATALOG_ITEM_NO = getItem.Field<string>("SUBCATALOG_ITEM_NO"), ROWID = getItem.Field<int>("ROW_ID") }).ToList();
                        //    foreach (var subCatalogItem in subCatalogItems)
                        //    {
                        //        DataRow drItemNo = newCatalogItemNo.NewRow();
                        //        drItemNo["SUBCATALOG_ITEM_NO"] = subCatalogItem.SUBCATALOG_ITEM_NO.ToString();
                        //        drItemNo["CATALOG_ITEM_NO"] = drNewItem["CATALOG_ITEM_NO"];
                        //        drItemNo["ATTRIBUTE_NAME"] = "SUBCATALOG_ITEM_NO";
                        //        drItemNo["ATTRIBUTE_VALUES"] = drNewItem["SUBCATALOG_ITEM_NO"];
                        //        drItemNo["VALIDATION_TYPE"] = "NEW_CATALOG_ITEM_NO";
                        //        drItemNo["MESSAGE"] = " New Sub Catalog Item no";
                        //        drItemNo["ROW_ID"] = subCatalogItem.ROWID.ToString();
                        //        newCatalogItemNo.Rows.Add(drItemNo);
                        //    }
                        //}
                    }
                    if (newCatalogItemNo == null)
                        newCatalogItemNo = duplicateCatalogItemNo.Clone();
                }
                else
                {
                    if (string.IsNullOrEmpty(defaultFamily) || defaultFamily.ToUpper() == "NULL")
                        newCatalogItemNo = NewCatalogItemNo(objdatatable, catalogId, importtype);
                    if (newCatalogItemNo == null)
                        newCatalogItemNo = duplicateCatalogItemNo.Clone();
                }
                // Get New PickList Item
                DataTable attrType = IC.SelectedColumnsToImport(objdatatable, model);
                string attributeName = string.Join(",", attrType.AsEnumerable().Select(pickList => "'" + pickList.Field<string>("ATTRIBUTE_NAME") + "'").ToArray());
                DataTable newPickList = NewPickList(objdatatable, attributeName, catalogId, importtype);
                // Get Parts Key Items
                DataTable newPartsKeyValue = NewPartsKeyValue(unPivotTable, attributeName, catalogId, importtype, attrType, objdatatable);
                //Get Datatype validation
                foreach (DataColumn removeColumn in objdatatable.Columns)
                {
                    if (removeColumn.ColumnName.ToUpper().Trim() == "ACTION")
                    {
                        objdatatable.Columns.Remove(removeColumn);
                        break;
                    }
                }
                DataTable dataTypeValidation = DataTypeValidation(unPivotTable, customerId, importtype);
                //Merge Validation List
                var validationList = MissingColumns.AsEnumerable()
                    .Union(duplicateCatalogItemNo.AsEnumerable())
                    .Union(newCatalogItemNo.AsEnumerable())
                    .Union(newPickList.AsEnumerable())
                    .Union(newPartsKeyValue.AsEnumerable())
                    .Union(dataTypeValidation.AsEnumerable());
                //To get the importProcess value.
                int importProcessRecordsValue = Convert.ToInt32(ConfigurationManager.AppSettings["ImportProcessRecords"]);
                if (validationList == null || !validationList.Any())
                {
                    return "Import success~" + importTemp + "~" + objdatatable.Rows.Count + "~" + importProcessRecordsValue;
                }
                else
                {
                    DataSet dtValidation = new DataSet();
                    dtValidation.Tables.Add("VALIDATIONLIST");
                    dtValidation.Tables[0].Columns.Add("VALID");
                    dtValidation.Tables[0].Columns.Add("CATALOG_ITEM_NO");
                    if (importtype.ToUpper().Contains("SUBPRODUCT"))
                    {
                        dtValidation.Tables[0].Columns.Add("SUBCATALOG_ITEM_NO");
                    }
                    dtValidation.Tables[0].Columns.Add("ATTRIBUTE_NAME");
                    dtValidation.Tables[0].Columns.Add("ATTRIBUTE_VALUES");
                    dtValidation.Tables[0].Columns.Add("VALIDATION_TYPE");
                    dtValidation.Tables[0].Columns.Add("MESSAGE");
                    dtValidation.Tables[0].Columns.Add("ROWID");
                    foreach (DataRow drValid in validationList)
                    {
                        dtValidation.Tables[0].Rows.Add(drValid.ItemArray);
                    }
                    return ErrorListCatalog(dtValidation.Tables[0], importtype, SheetName, importTemp, excelPath, Errorlogoutputformat);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : ValidateItemNo", ex);
            }
            return null;
        }
        #endregion

        public string ErrorListCatalog(DataTable validationList, string importtype, string SheetName, string importTemp, string excelPath, string Errorlogoutputformat)
        {
            try
            {
                DataSet dtValidation = new DataSet();
                dtValidation.Tables.Add("VALIDATIONLIST");
                dtValidation.Tables[0].Columns.Add("VALID");
                dtValidation.Tables[0].Columns.Add("CATALOG_ITEM_NO");
                if (importtype.ToUpper().Contains("SUBPRODUCT"))
                {
                    dtValidation.Tables[0].Columns.Add("SUBCATALOG_ITEM_NO");
                }
                dtValidation.Tables[0].Columns.Add("ATTRIBUTE_NAME");
                dtValidation.Tables[0].Columns.Add("ATTRIBUTE_VALUES");
                dtValidation.Tables[0].Columns.Add("VALIDATION_TYPE");
                dtValidation.Tables[0].Columns.Add("MESSAGE");
                dtValidation.Tables[0].Columns.Add("ROWID");
                foreach (DataRow drValid in validationList.Rows)
                {
                    dtValidation.Tables[0].Rows.Add(drValid.ItemArray);
                }
                if (dtValidation.Tables[0].Columns.Contains("VALID"))
                {
                    dtValidation.Tables[0].Columns.Remove("VALID");
                }
                HttpContext.Current.Session["ITEMVALIDATIONSESSION"] = dtValidation;
                string filepath = HttpContext.Current.Server.MapPath("~/Content/" + SheetName + "");
                string format = filepath + ".csv";
                CreateCSVFile(dtValidation.Tables[0], format);
                string txtformat = filepath + ".txt";
                CreateTextFile(dtValidation.Tables[0], txtformat, "\t");
                ExportDataSetToExcel(dtValidation as DataSet, SheetName);
                ExportErrorLogDataSetToExcel(dtValidation, importTemp, excelPath, SheetName, Errorlogoutputformat);
                return string.Join("~", dtValidation.Tables[0].AsEnumerable().Select(val => val.Field<string>("VALIDATION_TYPE")).Distinct().ToArray()) + "~ITEM#" + "~" + importTemp;
            }
            catch (Exception ex)
            { }
            return null;
        }

        #region MissingColumnName Function
        /// <summary>
        /// Get Missing column List
        /// </summary>
        /// <param name="objdatatable">Data table from imported sheet</param>
        /// <param name="columns">Manadory column fields in array format</param>
        /// <returns></returns>
        public DataTable MissingColumnName(DataTable objdatatable, string[] columns, string importtype)
        {
            try
            {
                if (objdatatable.Columns.Contains("ITEM#"))
                {
                    objdatatable.Columns["ITEM#"].ColumnName = "CATALOG_ITEM_NO";
                }
                DataTable missingColumnList = new DataTable();
                missingColumnList.Columns.Add("VALID");
                missingColumnList.Columns["VALID"].AutoIncrement = true;
                missingColumnList.Columns["VALID"].AutoIncrementSeed = 1;
                missingColumnList.Columns.Add("CATALOG_ITEM_NO");
                if (importtype.ToUpper().Contains("SUBPRODUCT"))
                {
                    missingColumnList.Columns.Add("SUBCATALOG_ITEM_NO");
                }
                missingColumnList.Columns.Add("ATTRIBUTE_NAME");
                missingColumnList.Columns.Add("ATTRIBUTE_VALUES");
                missingColumnList.Columns.Add("VALIDATION_TYPE");
                missingColumnList.Columns.Add("MESSAGE");
                missingColumnList.Columns.Add("ROW_ID");
                string customAttributeName = "ITEM#";
                if (HttpContext.Current.Session["CustomerItemNo"] != null)
                    customAttributeName = HttpContext.Current.Session["CustomerItemNo"].ToString();
                string customSubAttributeName = "SUBITEM#";
                if (HttpContext.Current.Session["CustomerSubItemNo"] != null)
                    customSubAttributeName = HttpContext.Current.Session["CustomerSubItemNo"].ToString();
                foreach (string missColumn in columns)
                {
                    if (!objdatatable.Columns.Contains(missColumn) || (importtype.ToUpper() == "SUBPRODUCT" && !objdatatable.Columns.Contains("SUBITEM#") && !objdatatable.Columns.Contains("SUBCATALOG_ITEM_NO")) || (!objdatatable.Columns.Contains("ITEM#") && !objdatatable.Columns.Contains("CATALOG_ITEM_NO")))
                    {
                        DataRow missRowList = missingColumnList.NewRow();
                        missRowList["CATALOG_ITEM_NO"] = !objdatatable.Columns.Contains("CATALOG_ITEM_NO") ? null : objdatatable.Rows[0]["CATALOG_ITEM_NO"].ToString();
                        if (importtype.ToUpper().Contains("SUBPRODUCT"))
                        {
                            missRowList["SUBCATALOG_ITEM_NO"] = !objdatatable.Columns.Contains("SUBCATALOG_ITEM_NO") ? null : objdatatable.Rows[0]["SUBCATALOG_ITEM_NO"].ToString();
                        }
                        missRowList["ATTRIBUTE_NAME"] = "-";
                        missRowList["ATTRIBUTE_VALUES"] = importtype.ToUpper().Contains("SUBPRODUCT") ? missColumn.ToUpper() == "SUBCATALOG_ITEM_NO" ? customSubAttributeName : missColumn : missColumn.ToUpper() == "CATALOG_ITEM_NO" ? customAttributeName : missColumn;
                        missRowList["VALIDATION_TYPE"] = "MISSING_COLUMNS";
                        missRowList["MESSAGE"] = "Columns / values not found";
                        missRowList["ROW_ID"] = "2";
                        missingColumnList.Rows.Add(missRowList);
                    }
                }

                return missingColumnList;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : MissingColumnName", ex);
                return null;
            }

        }
        #endregion

        #region NewCatalogItemNo
        /// <summary>
        /// Get New Catalog Items from import data
        /// </summary>
        /// <param name="catalogItemNo">DataTable from import sheet</param>
        /// <param name="catalogId">current catalog id</param>
        /// <returns></returns>
        public DataTable NewCatalogItemNo(DataTable catalogItemNo, int catalogId, string importType)
        {
            try
            {
                var existingItemNo = importType.ToUpper().Contains("SUBPRODUCT") ? objLS.TB_SUBPRODUCT.Join(objLS.TB_PROD_SPECS, tcp => tcp.SUBPRODUCT_ID, tps => tps.PRODUCT_ID, (tcp, tps) => new { tcp, tps }).Where(x => x.tps.ATTRIBUTE_ID == 1 && x.tcp.CATALOG_ID == catalogId).Select(y => y.tps).ToList() : objLS.TB_CATALOG_PRODUCT.Join(objLS.TB_PROD_SPECS, tcp => tcp.PRODUCT_ID, tps => tps.PRODUCT_ID, (tcp, tps) => new { tcp, tps }).Join(objLS.TB_PROD_FAMILY, tcptps => tcptps.tps.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (tcptps, tpf) => new { tcptps, tpf }).Where(x => x.tcptps.tps.ATTRIBUTE_ID == 1 && x.tcptps.tcp.CATALOG_ID == catalogId && x.tpf.FLAG_RECYCLE == "A").Select(y => y.tcptps.tps).ToList();
                var newItemNo = importType.ToUpper().Contains("SUBPRODUCT") ? existingItemNo.Join(catalogItemNo.AsEnumerable(), ext => ext.STRING_VALUE, newItem => newItem.Field<string>("SUBCATALOG_ITEM_NO"), (ext, newItem) => new { ext, newItem }).Where(empty => string.IsNullOrEmpty(empty.newItem.Field<string>("CATALOG_ITEM_NO")) == false).Select(x => x.newItem).ToList() :
                    existingItemNo.Join(catalogItemNo.AsEnumerable(), ext => ext.STRING_VALUE, newItem => newItem.Field<string>("CATALOG_ITEM_NO"), (ext, newItem) => new { ext, newItem }).Select(x => x.newItem).ToList();
                DataTable newCatalogItemNo = new DataTable();
                newCatalogItemNo.Columns.Add("VALID");
                newCatalogItemNo.Columns["VALID"].AutoIncrement = true;
                newCatalogItemNo.Columns["VALID"].AutoIncrementSeed = 1;
                if (importType.ToUpper().Contains("SUBPRODUCT"))
                {
                    newCatalogItemNo.Columns.Add("SUBCATALOG_ITEM_NO");
                }
                newCatalogItemNo.Columns.Add("CATALOG_ITEM_NO");
                newCatalogItemNo.Columns.Add("ATTRIBUTE_NAME");
                newCatalogItemNo.Columns.Add("ATTRIBUTE_VALUES");
                newCatalogItemNo.Columns.Add("VALIDATION_TYPE");
                newCatalogItemNo.Columns.Add("MESSAGE");
                newCatalogItemNo.Columns.Add("ROW_ID");

                string customAttributeName = "ITEM#";
                if (HttpContext.Current.Session["CustomerItemNo"] != null)
                    customAttributeName = HttpContext.Current.Session["CustomerItemNo"].ToString();
                string customSubAttributeName = "SUBITEM#";
                if (HttpContext.Current.Session["CustomerSubItemNo"] != null)
                    customSubAttributeName = HttpContext.Current.Session["CustomerSubItemNo"].ToString();

                foreach (DataRow itemNo in catalogItemNo.Rows)
                {
                    string catItemNo = importType.ToUpper().Contains("SUBPRODUCT") ? itemNo["SUBCATALOG_ITEM_NO"].ToString() : itemNo["CATALOG_ITEM_NO"].ToString();
                    if (string.IsNullOrEmpty(catItemNo))
                        continue;
                    var itemExists = importType.ToUpper().Contains("SUBPRODUCT") ? newItemNo.Count(item => item.Field<string>("SUBCATALOG_ITEM_NO") == catItemNo && !string.IsNullOrEmpty(item.Field<string>("CATALOG_ITEM_NO"))) : newItemNo.Count(item => item.Field<string>("CATALOG_ITEM_NO") == catItemNo);
                    if (itemExists == 0)
                    {
                        DataRow drItemNo = newCatalogItemNo.NewRow();
                        drItemNo["CATALOG_ITEM_NO"] = string.IsNullOrEmpty(itemNo.Field<string>("CATALOG_ITEM_NO")) ? "-" : itemNo.Field<string>("CATALOG_ITEM_NO");
                        if (importType.ToUpper().Contains("SUBPRODUCT"))
                            drItemNo["SUBCATALOG_ITEM_NO"] = itemNo.Field<string>("SUBCATALOG_ITEM_NO");
                        drItemNo["ATTRIBUTE_NAME"] = importType.ToUpper().Contains("SUBPRODUCT") ? customSubAttributeName : customAttributeName;
                        drItemNo["ATTRIBUTE_VALUES"] = importType.ToUpper().Contains("SUBPRODUCT") ? itemNo.Field<string>("SUBCATALOG_ITEM_NO") : itemNo.Field<string>("CATALOG_ITEM_NO");
                        drItemNo["VALIDATION_TYPE"] = "NEW_CATALOG_ITEM_NO";
                        drItemNo["MESSAGE"] = importType.ToUpper().Contains("SUBPRODUCT") ? "Please provide a main product for inserting new items" : "Please select a default family for inserting new items";
                        drItemNo["ROW_ID"] = itemNo.Field<int>("ROW_ID") + 2;
                        newCatalogItemNo.Rows.Add(drItemNo);
                    }
                }
                return newCatalogItemNo;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : NewCatalogItemNo", ex);
                return null;
            }

        }
        #endregion

        #region DuplicateCatalogItemNo

        /// <summary>
        /// Get duplicate catalog items from import data
        /// </summary>
        /// <param name="catalogItemNo">DataTable from import sheet</param>
        /// <param name="catalogId">current catalog id</param>
        /// <returns></returns>

        public DataTable DuplicateCatalogItemNo(DataTable catalogItemNo, int catalogId)
        {
            try
            {
                DataTable duplicateCatalogItemNo = new DataTable();
                duplicateCatalogItemNo.Columns.Add("VALID");
                duplicateCatalogItemNo.Columns["VALID"].AutoIncrement = true;
                duplicateCatalogItemNo.Columns["VALID"].AutoIncrementSeed = 1;
                duplicateCatalogItemNo.Columns.Add("CATALOG_ITEM_NO");
                duplicateCatalogItemNo.Columns.Add("ATTRIBUTE_NAME");
                duplicateCatalogItemNo.Columns.Add("ATTRIBUTE_VALUES");
                duplicateCatalogItemNo.Columns.Add("VALIDATION_TYPE");
                duplicateCatalogItemNo.Columns.Add("MESSAGE");
                duplicateCatalogItemNo.Columns.Add("ROW_ID");
                if (catalogItemNo.Columns.Contains("CATALOG_ITEM_NO"))
                {
                    var productExists = catalogItemNo.Columns.Contains("PRODUCT_ID") ? catalogItemNo.AsEnumerable().Count(dup => dup.Field<string>("PRODUCT_ID") != null) : 0;
                    var duplicateItem = productExists > 0 ? catalogItemNo.AsEnumerable().Select(dup => new { PRODUCT_ID = dup.Field<string>("PRODUCT_ID"), CATALOG_ITEM_NO = dup.Field<string>("CATALOG_ITEM_NO") }).GroupBy(x => x).Where(records => records.Count() > 1).Select(records => records.Key).Where(field => field.CATALOG_ITEM_NO != null) : catalogItemNo.AsEnumerable().Select(dup => new { PRODUCT_ID = "", CATALOG_ITEM_NO = dup.Field<string>("CATALOG_ITEM_NO") }).GroupBy(x => x).Where(records => records.Count() > 1).Select(records => records.Key).Where(Field => Field.CATALOG_ITEM_NO != null);
                    foreach (var items in duplicateItem)
                    {
                        DataRow drDuplicate = duplicateCatalogItemNo.NewRow();
                        drDuplicate["CATALOG_ITEM_NO"] = items.CATALOG_ITEM_NO.ToString();
                        drDuplicate["ATTRIBUTE_NAME"] = "ITEM#";
                        drDuplicate["ATTRIBUTE_VALUES"] = items.CATALOG_ITEM_NO.ToString();
                        drDuplicate["VALIDATION_TYPE"] = "DUPLICATE_ITEM_NO";
                        drDuplicate["MESSAGE"] = "Duplicate Records";
                        drDuplicate["ROW_ID"] = catalogItemNo.AsEnumerable().Where(records => records.Field<string>("CATALOG_ITEM_NO") == items.CATALOG_ITEM_NO).Select(dup => dup.Field<int>("ROW_ID")).FirstOrDefault() + 2;
                        duplicateCatalogItemNo.Rows.Add(drDuplicate);
                    }
                }
                return duplicateCatalogItemNo;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : DuplicateCatalogItemNo", ex);
                return null;
            }
        }
        #endregion

        #region NewPickList
        /// <summary>
        /// Get new picklist values from import data
        /// </summary>
        /// <param name="objdatatable">DataTable from import sheet</param>
        /// <param name="attributeName">Picklist attribute name</param>
        /// <param name="catalogId">current catalog id</param>
        /// <param name="importType"> import type(Product/Subproduct)</param>
        /// <returns></returns>
        public DataTable NewPickList(DataTable objdatatable, string attributeName, int catalogId, string importType)
        {
            try
            {
                DataTable Errortable = new DataTable();
                Errortable.Columns.Add("VALID");
                Errortable.Columns["VALID"].AutoIncrement = true;
                Errortable.Columns["VALID"].AutoIncrementSeed = 1;
                Errortable.Columns.Add("CATALOG_ITEM_NO");
                if (importType.ToUpper().Contains("SUBPRODUCT"))
                {
                    Errortable.Columns.Add("SUBCATALOG_ITEM_NO");
                }
                Errortable.Columns.Add("ATTRIBUTE_NAME");
                Errortable.Columns.Add("ATTRIBUTE_VALUES");
                Errortable.Columns.Add("VALIDATION_TYPE");
                Errortable.Columns.Add("MESSAGE");
                Errortable.Columns.Add("ROW_ID");
                using (var conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    DataSet pkname = new DataSet();
                    SqlDataAdapter daa = new SqlDataAdapter("select PICKLIST_NAME,ATTRIBUTE_NAME from TB_ATTRIBUTE where ATTRIBUTE_NAME in (" + attributeName + ") and USE_PICKLIST =1", conn);
                    daa.Fill(pkname);
                    if (pkname.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow vr in pkname.Tables[0].Rows)
                        {
                            DataTable _pickListValue = new DataTable();
                            DataSet newsheetval = new DataSet();
                            _pickListValue.Columns.Add("Value");
                            string picklistname = vr["PICKLIST_NAME"].ToString();
                            DataSet dpicklistdata = new DataSet();
                            if (!string.IsNullOrEmpty(picklistname))
                            {
                                var pickList = objLS.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                                {
                                    PICKLIST_DATA = s.PICKLIST_DATA,
                                    PICKLIST_NAME = s.PICKLIST_NAME,
                                    PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                                }).FirstOrDefault(d => d.PICKLIST_NAME == picklistname);
                                var objPick = new List<PickList>();
                                if (pickList != null && !string.IsNullOrEmpty(pickList.PICKLIST_DATA))
                                {
                                    var pkxmlData = pickList.PICKLIST_DATA;
                                    if (pkxmlData.Contains("<?xml version"))
                                    {
                                        pkxmlData = pkxmlData.Replace("\\r\\n", "");
                                        pkxmlData = pkxmlData.Replace("\r\n", "");
                                        pkxmlData = pkxmlData.Replace("\\\"", "\"");
                                        pkxmlData = pkxmlData.Replace("<?xml version=\"1.0\" standalone=\"yes\"?>", "");
                                        XDocument objXml = XDocument.Parse(pkxmlData);
                                        objPick =
                                            objXml.Descendants("NewDataSet")
                                                .Descendants("Table1")
                                                .Select(d =>
                                                {
                                                    var xElement = d.Element("ListItem");
                                                    return xElement != null
                                                        ? new PickList
                                                        {
                                                            ListItem = xElement.Value
                                                        }
                                                        : null;
                                                }).ToList();
                                    }
                                }
                                var drItemList = objdatatable.AsEnumerable().Where(x => x.Field<string>(vr["ATTRIBUTE_NAME"].ToString()) != null).Select(pick => pick.Field<string>(vr["ATTRIBUTE_NAME"].ToString())).ToList();
                                // string sqlString = "Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  ['+ @colName+'],CATALOG_ITEM_NO   from [##t2] where  ['+ @colName+']<>''@null''') ";
                                if (importType.ToUpper().Contains("SUBPRODUCT"))
                                {
                                    drItemList = objdatatable.AsEnumerable().Where(x => x.Field<string>(vr["ATTRIBUTE_NAME"].ToString()) != null).Select(pick => pick.Field<string>(vr["ATTRIBUTE_NAME"].ToString())).ToList();
                                    // sqlString = "Declare @colName VARCHAR(max) SET @colName ='" + vr["ATTRIBUTE_NAME"] + "' Exec('select  ['+ @colName+'],[SUBITEM#] as SUBITEM   from [##t2]  where  ['+ @colName+']<>''@null''') ";
                                }
                                //SqlDataAdapter daa2 =
                                //        new SqlDataAdapter(sqlString, conn);
                                //daa2.Fill(newsheetval);
                                //SqlCommand cvb = new SqlCommand("DELETE TOP(1) From ##t1", conn);
                                //cvb.ExecuteNonQuery();
                                DataSet picklistdataDS = new DataSet();
                                DataTable newTable = new DataTable();
                                // DataSet newsheetval1 = new DataSet();
                                for (int rowCount = 0; rowCount < drItemList.Count; rowCount++)
                                {
                                    if (drItemList[rowCount].ToString().Trim() != "")
                                    {
                                        var picklistvalue = drItemList[rowCount].ToString().Trim();
                                        var dd = objPick.Where(x => x.ListItem == picklistvalue);
                                        if (!dd.Any())
                                        {
                                            string CAT = vr["ATTRIBUTE_NAME"].ToString();
                                            DataRow drItemNo = Errortable.NewRow();
                                            drItemNo["CATALOG_ITEM_NO"] = objdatatable.AsEnumerable().Where(records => records.Field<string>(vr["ATTRIBUTE_NAME"].ToString()) == picklistvalue).Select(dup => dup.Field<string>("CATALOG_ITEM_NO")).FirstOrDefault();
                                            if (importType.ToUpper().Contains("SUBPRODUCT"))
                                            {
                                                drItemNo["SUBCATALOG_ITEM_NO"] = objdatatable.AsEnumerable().Where(records => records.Field<string>(vr["ATTRIBUTE_NAME"].ToString()) == picklistvalue).Select(dup => dup.Field<string>("SUBCATALOG_ITEM_NO")).FirstOrDefault();
                                            }
                                            drItemNo["ATTRIBUTE_NAME"] = vr["ATTRIBUTE_NAME"].ToString() == "CATALOG_ITEM_NO" ? "ITEM#" : vr["ATTRIBUTE_NAME"].ToString() == "SUBCATALOG_ITEM_NO" ? "SUBITEM#" : vr["ATTRIBUTE_NAME"];
                                            drItemNo["ATTRIBUTE_VALUES"] = picklistvalue;
                                            drItemNo["VALIDATION_TYPE"] = "PICKLIST_VALUE";
                                            drItemNo["MESSAGE"] = "New picklist value found";
                                            drItemNo["ROW_ID"] = objdatatable.AsEnumerable().Where(records => records.Field<string>(vr["ATTRIBUTE_NAME"].ToString()) == picklistvalue).Select(dup => dup.Field<int>("ROW_ID")).FirstOrDefault() + 2;
                                            Errortable.Rows.Add(drItemNo);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return Errortable;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : NewPickList", ex);
                return null;
            }
        }
        #endregion

        #region NewPartsKeyValue
        /// <summary>
        /// Get new partskey values from import data
        /// </summary>
        /// <param name="partsKey">DataTable from import sheet</param>
        /// <param name="attributeName">Partskey attribute name</param>
        /// <param name="catalogId">current catalog id</param>
        /// <param name="importType">import type(Product/Subproduct)</param>
        /// <returns></returns>
        public DataTable NewPartsKeyValue(DataTable partsKey, string attributeName, int catalogId, string importType, DataTable attrType, DataTable objdatatable)
        {
            try
            {
                DataTable newPartsKey = new DataTable();
                if (importType.ToUpper().Contains("SUBPRODUCTS"))
                {
                    newPartsKey = NewSubPartsKeyValue(partsKey, attributeName, catalogId, importType, attrType, objdatatable);
                }
                else
                {
                    newPartsKey.Columns.Add("VALID");
                    newPartsKey.Columns["VALID"].AutoIncrement = true;
                    newPartsKey.Columns["VALID"].AutoIncrementSeed = 1;
                    newPartsKey.Columns.Add("CATALOG_ITEM_NO");
                    newPartsKey.Columns.Add("ATTRIBUTE_NAME");
                    newPartsKey.Columns.Add("ATTRIBUTE_VALUES");
                    newPartsKey.Columns.Add("VALIDATION_TYPE");
                    newPartsKey.Columns.Add("MESSAGE");
                    newPartsKey.Columns.Add("ROW_ID");

                    var existingItemNo = objLS.TB_CATALOG_PRODUCT.Join(objLS.TB_PARTS_KEY, tcp => tcp.PRODUCT_ID, tps => tps.PRODUCT_ID, (tcp, tps) => new { tcp, tps }).Join(objLS.TB_ATTRIBUTE, tpk => tpk.tps.ATTRIBUTE_ID, ta => ta.ATTRIBUTE_ID, (tpk, ta) => new { tpk, ta }).Where(x => x.tpk.tcp.CATALOG_ID == catalogId && x.ta.ATTRIBUTE_TYPE == 6).Select(y => new { ATTRIBUTE_ID = y.tpk.tps.ATTRIBUTE_ID, ATTRIBUTE_VALUE = y.tpk.tps.ATTRIBUTE_VALUE, ATTRIBUTE_NAME = y.ta.ATTRIBUTE_NAME }).Distinct().ToList();
                    if (!existingItemNo.Any())
                    {
                        var newPartsKeyValues = partsKey.AsEnumerable().Join(attrType.AsEnumerable(), pk => pk.Field<string>("ATTRIBUTENAME"), at => at.Field<string>("ATTRIBUTE_NAME"), (pk, at) => new { pk, at }).Where(newPart => newPart.at.Field<string>("ATTRIBUTE_TYPE") == "6" && !string.IsNullOrEmpty(newPart.pk.Field<string>("ATTRIBUTEVALUE"))).Select(getParts => getParts.pk);
                        if (newPartsKeyValues.Any())
                        {
                            foreach (var itemNo in newPartsKeyValues)
                            {
                                DataRow drItemNo = newPartsKey.NewRow();
                                drItemNo["CATALOG_ITEM_NO"] = itemNo.Field<string>("CATALOG_ITEM_NO");
                                drItemNo["ATTRIBUTE_NAME"] = itemNo.Field<string>("ATTRIBUTENAME") == "CATALOG_ITEM_NO" ? "ITEM#" : itemNo.Field<string>("ATTRIBUTENAME") == "SUBCATALOG_ITEM_NO" ? "SUBITEM#" : itemNo.Field<string>("ATTRIBUTENAME");
                                drItemNo["ATTRIBUTE_VALUES"] = itemNo.Field<string>("ATTRIBUTEVALUE");
                                drItemNo["VALIDATION_TYPE"] = "PRODUCT_KEY";
                                drItemNo["MESSAGE"] = "New product key value found";
                                drItemNo["ROW_ID"] = objdatatable.AsEnumerable().Where(records => records.Field<string>("CATALOG_ITEM_NO") == itemNo.Field<string>("CATALOG_ITEM_NO").ToString()).Select(dup => dup.Field<int>("ROW_ID")).FirstOrDefault() + 2;
                                newPartsKey.Rows.Add(drItemNo);
                            }
                        }
                        return newPartsKey;
                    }
                    var newItemNo = existingItemNo.Join(partsKey.AsEnumerable(), ext => ext.ATTRIBUTE_NAME, newItem => newItem.Field<string>("ATTRIBUTENAME"), (ext, newItem) => new { ext, newItem }).Select(x => x.newItem).Distinct().ToList();
                    if (!newItemNo.Any())
                    {
                        return newPartsKey;
                    }

                    foreach (DataRow itemNo in partsKey.Rows)
                    {
                        string catItemNo = itemNo["ATTRIBUTEVALUE"].ToString();
                        string attrName = itemNo["ATTRIBUTENAME"].ToString();
                        if (string.IsNullOrEmpty(itemNo.Field<string>("CATALOG_ITEM_NO")) || itemNo.Field<string>("ATTRIBUTENAME").ToString().ToUpper().Contains("ROW_ID"))
                            continue;
                        var itemExists = newItemNo.Count(item => item.Field<string>("ATTRIBUTEVALUE") == catItemNo && item.Field<string>("ATTRIBUTENAME") == attrName);
                        if (itemExists != 0)
                        {
                            DataRow drItemNo = newPartsKey.NewRow();
                            drItemNo["CATALOG_ITEM_NO"] = itemNo.Field<string>("CATALOG_ITEM_NO");
                            drItemNo["ATTRIBUTE_NAME"] = itemNo.Field<string>("ATTRIBUTENAME") == "CATALOG_ITEM_NO" ? "ITEM#" : itemNo.Field<string>("ATTRIBUTENAME") == "SUBCATALOG_ITEM_NO" ? "SUBITEM#" : itemNo.Field<string>("ATTRIBUTENAME");
                            drItemNo["ATTRIBUTE_VALUES"] = itemNo.Field<string>("ATTRIBUTEVALUE");
                            drItemNo["VALIDATION_TYPE"] = "PRODUCT_KEY";
                            drItemNo["MESSAGE"] = "New partskey value found";
                            drItemNo["ROW_ID"] = objdatatable.AsEnumerable().Where(records => records.Field<string>("CATALOG_ITEM_NO") == itemNo.Field<string>("CATALOG_ITEM_NO").ToString()).Select(dup => dup.Field<int>("ROW_ID")).FirstOrDefault() + 2;
                            newPartsKey.Rows.Add(drItemNo);
                        }
                    }

                    //    return newPartsKey;
                    //}
                    //var newItemNo = existingItemNo.Join(partsKey.AsEnumerable(), ext => ext.ATTRIBUTE_VALUE, newItem => newItem.Field<string>("ATTRIBUTE_VALUE"), (ext, newItem) => new { ext, newItem }).Select(x => x.newItem).ToList();
                    //if (!newItemNo.Any())
                    //{
                    //    return newPartsKey;
                    //}

                    //foreach (DataRow itemNo in partsKey.Rows)
                    //{
                    //    string catItemNo = itemNo["ATTRIBUTEVALUE"].ToString();
                    //    var itemExists = newItemNo.Count(item => item.Field<string>("ATTRIBUTE_VALUE") == catItemNo);
                    //    if (itemExists == 0)
                    //    {
                    //        DataRow drItemNo = newPartsKey.NewRow();
                    //        drItemNo["CATALOG_ITEM_NO"] = itemNo.Field<string>("CATALOG_ITEM_NO");
                    //        drItemNo["ATTRIBUTE_NAME"] = itemNo.Field<string>("ATTRIBUTENAME") == "CATALOG_ITEM_NO" ? "ITEM#" : itemNo.Field<string>("ATTRIBUTENAME") == "SUBCATALOG_ITEM_NO" ? "SUBITEM#" : itemNo.Field<string>("ATTRIBUTENAME");
                    //        drItemNo["ATTRIBUTE_VALUES"] = itemNo.Field<string>("ATTRIBUTE_VALUE");
                    //        drItemNo["VALIDATION_TYPE"] = "PARTSKEY";
                    //        drItemNo["MESSAGE"] = "New partskey value found";
                    //        drItemNo["ROW_ID"] = objdatatable.AsEnumerable().Where(records => records.Field<string>("CATALOG_ITEM_NO") == itemNo.Field<string>("CATALOG_ITEM_NO").ToString()).Select(dup => dup.Field<int>("ROW_ID")).FirstOrDefault() + 2;
                    //        newPartsKey.Rows.Add(drItemNo);
                    //    }

                }
                return newPartsKey;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : NewPartsKeyValue", ex);
                return null;
            }
        }
        #endregion

        #region NewSubproductkeyValue
        /// <summary>
        /// Get new Subproduct partskey values from import data
        /// </summary>
        /// <param name="partsKey">DataTable from import sheet</param>
        /// <param name="attributeName">Subproduct Partskey attribute name</param>
        /// <param name="catalogId">current catalog id</param>
        /// <param name="importType">import type(Subproduct)</param>
        /// <returns></returns>
        public DataTable NewSubPartsKeyValue(DataTable partsKey, string attributeName, int catalogId, string importType, DataTable attrType, DataTable objdatatable)
        {
            try
            {
                DataTable newPartsKey = new DataTable();
                newPartsKey.Columns.Add("VALID");
                newPartsKey.Columns["VALID"].AutoIncrement = true;
                newPartsKey.Columns["VALID"].AutoIncrementSeed = 1;
                newPartsKey.Columns.Add("CATALOG_ITEM_NO");
                newPartsKey.Columns.Add("SUBCATALOG_ITEM_NO");
                newPartsKey.Columns.Add("ATTRIBUTE_NAME");
                newPartsKey.Columns.Add("ATTRIBUTE_VALUES");
                newPartsKey.Columns.Add("VALIDATION_TYPE");
                newPartsKey.Columns.Add("MESSAGE");
                newPartsKey.Columns.Add("ROW_ID");

                var existingItemNo = objLS.TB_CATALOG_PRODUCT.Join(objLS.TB_SUBPRODUCT_KEY, tcp => tcp.PRODUCT_ID, tps => tps.PRODUCT_ID, (tcp, tps) => new { tcp, tps }).Join(objLS.TB_ATTRIBUTE, tpk => tpk.tps.ATTRIBUTE_ID, ta => ta.ATTRIBUTE_ID, (tpk, ta) => new { tpk, ta }).Where(x => x.tpk.tcp.CATALOG_ID == catalogId && x.ta.ATTRIBUTE_TYPE == 6).Select(y => y.tpk.tps).ToList();
                if (!existingItemNo.Any())
                {
                    var newPartsKeyValues = partsKey.AsEnumerable().Join(attrType.AsEnumerable(), pk => pk.Field<string>("ATTRIBUTENAME"), at => at.Field<string>("ATTRIBUTE_NAME"), (pk, at) => new { pk, at }).Where(newPart => newPart.at.Field<string>("ATTRIBUTE_TYPE") == "6" && !string.IsNullOrEmpty(newPart.pk.Field<string>("ATTRIBUTEVALUE"))).Select(getParts => getParts.pk);
                    if (newPartsKeyValues.Any())
                    {
                        foreach (var itemNo in newPartsKeyValues)
                        {
                            DataRow drItemNo = newPartsKey.NewRow();
                            drItemNo["CATALOG_ITEM_NO"] = itemNo.Field<string>("CATALOG_ITEM_NO");
                            drItemNo["SUBCATALOG_ITEM_NO"] = itemNo.Field<string>("SUBCATALOG_ITEM_NO");
                            drItemNo["ATTRIBUTE_NAME"] = itemNo.Field<string>("ATTRIBUTENAME");
                            drItemNo["ATTRIBUTE_VALUES"] = itemNo.Field<string>("ATTRIBUTEVALUE");
                            drItemNo["VALIDATION_TYPE"] = "PARTSKEY";
                            drItemNo["MESSAGE"] = "New partskey value found";
                            drItemNo["ROW_ID"] = objdatatable.AsEnumerable().Where(records => records.Field<string>("SUBCATALOG_ITEM_NO") == itemNo.Field<string>("SUBCATALOG_ITEM_NO").ToString()).Select(dup => dup.Field<int>("ROW_ID")).FirstOrDefault() + 2;
                            newPartsKey.Rows.Add(drItemNo);
                        }
                    }
                    return newPartsKey;
                }
                var newItemNo = existingItemNo.Join(partsKey.AsEnumerable(), ext => ext.ATTRIBUTE_VALUE, newItem => newItem.Field<string>("ATTRIBUTEVALUE"), (ext, newItem) => new { ext, newItem }).Select(x => x.newItem).ToList();
                if (!newItemNo.Any())
                {
                    return newPartsKey;
                }

                foreach (DataRow itemNo in partsKey.Rows)
                {
                    string catItemNo = itemNo["ATTRIBUTEVALUE"].ToString();
                    var itemExists = newItemNo.Count(item => item.Field<string>("ATTRIBUTEVALUE") == catItemNo);
                    if (itemExists != 0)
                    {
                        DataRow drItemNo = newPartsKey.NewRow();
                        drItemNo["CATALOG_ITEM_NO"] = itemNo.Field<string>("CATALOG_ITEM_NO");
                        drItemNo["SUBCATALOG_ITEM_NO"] = itemNo.Field<string>("SUBCATALOG_ITEM_NO");
                        drItemNo["ATTRIBUTE_NAME"] = itemNo.Field<string>("ATTRIBUTENAME");
                        drItemNo["ATTRIBUTE_VALUES"] = itemNo.Field<string>("ATTRIBUTEVALUE");
                        drItemNo["VALIDATION_TYPE"] = "PARTSKEY";
                        drItemNo["MESSAGE"] = "New partskey value found";
                        drItemNo["ROW_ID"] = objdatatable.AsEnumerable().Where(records => records.Field<string>("SUBCATALOG_ITEM_NO") == itemNo.Field<string>("SUBCATALOG_ITEM_NO").ToString()).Select(dup => dup.Field<int>("ROW_ID")).FirstOrDefault() + 2;
                        newPartsKey.Rows.Add(drItemNo);
                    }
                }

                return newPartsKey;

            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : NewSubPartsKeyValue", ex);
                return null;
            }
        }
        #endregion
        #region DataTypeValidation
        /// <summary>
        ///  Get invalid datatype values from import data
        /// </summary>
        /// <param name="unPivotTable">DataTable from import sheet</param>
        /// <param name="customerId">current catalog id</param>
        /// <returns></returns>
        public DataTable DataTypeValidation(DataTable unPivotTable, int customerId, string importType)
        {
            try
            {
                var customerAttributes = objLS.TB_ATTRIBUTE.Join(objLS.CUSTOMERATTRIBUTE, ta => ta.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (ta, ca) => new { ta, ca }).Where(cus => cus.ca.CUSTOMER_ID == customerId).Select(attrList => attrList.ta);
                int number;
                decimal number1;
                var numericField = unPivotTable.AsEnumerable().Join(customerAttributes, srcData => srcData.Field<string>("ATTRIBUTENAME"), ca => ca.ATTRIBUTE_NAME, (srcData, ca) => new { srcData, ca }).
                                   Where(x => (x.ca.ATTRIBUTE_TYPE == 4 || x.ca.ATTRIBUTE_DATATYPE.ToUpper().Contains("NUMBER") || x.srcData.Field<string>("ATTRIBUTENAME").ToUpper().Contains("PUBLISH2"))).Select(num => new
                                   {
                                       isValid = (decimal.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out number1) || int.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out number)),
                                       Message = !(decimal.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out number1) || int.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out number)) ? "Invalid numeric field" : num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(),
                                       num.srcData
                                   });

                string sqlString = string.Empty;
                DataRow[] dRow = unPivotTable.Select("ATTRIBUTENAME like '%PUBLISH2%'");
                DataTable dtPublish = new DataTable();
                if (dRow.Length > 0)
                {
                    int Publish = 0;
                    foreach (DataRow dr in dRow)
                    {
                        if (importType.ToUpper().Contains("SUB"))
                        {
                            sqlString = sqlString + "select '" + dr.ItemArray[5].ToString() + "' as AttributeName,ISNUMERIC('" + dr.ItemArray[6].ToString() + "') as isValid,case when ISNUMERIC('" + dr.ItemArray[6].ToString() + "') = 0  then 'Invalid numeric field' end Message,'" + dr.ItemArray[6].ToString() + "' Value union all ";
                        }
                        else
                        {
                            if (dr.ItemArray[4].ToString() == "Y")
                            {
                                Publish = 1;
                            }
                            else
                            {
                                Publish = 0;
                            }
                            sqlString = sqlString + "select '" + dr.ItemArray[3].ToString() + "' as AttributeName,ISNUMERIC('" + Publish + "') as isValid,case when ISNUMERIC('" + Publish + "') = 0  then 'Invalid numeric field' end Message,'" + Publish + "' Value union all ";
                        }
                    }
                    sqlString = sqlString.Substring(0, sqlString.Length - 10);
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd = new SqlCommand(sqlString, con);
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        da.Fill(dtPublish);
                    }
                    var numericFieldPublish = dtPublish.AsEnumerable().Join(unPivotTable.AsEnumerable(), pub => pub.Field<string>("ATTRIBUTENAME"), srcData => srcData.Field<string>("ATTRIBUTENAME"), (pub, srcData) => new { pub, srcData }).Where(x => x.pub.Field<int>("isValid") == 0).Select(
                        num => new
                        {
                            isValid = false,
                            Message = "Invalid numeric field",
                            num.srcData
                        });
                    if (numericField.Any())
                        numericField.Union(numericFieldPublish);
                    else if (numericFieldPublish.Any())
                        numericField = numericFieldPublish;

                }
                DateTime dateTime = new DateTime();
                var dateTimeField = unPivotTable.AsEnumerable().Join(customerAttributes, srcData => srcData.Field<string>("ATTRIBUTENAME"), ca => ca.ATTRIBUTE_NAME, (srcData, ca) => new { srcData, ca }).
                                   Where(x => x.ca.ATTRIBUTE_DATATYPE == "Date and Time").Select(num => new { isValid = DateTime.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out dateTime), Message = DateTime.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out dateTime) == true ? "Invalid date field" : num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), num.srcData });
                DataTable dataTypeValidation = new DataTable();
                dataTypeValidation.Columns.Add("VALID");
                dataTypeValidation.Columns["VALID"].AutoIncrement = true;
                dataTypeValidation.Columns["VALID"].AutoIncrementSeed = 1;
                dataTypeValidation.Columns.Add("CATALOG_ITEM_NO");
                if (importType.ToUpper().Contains("SUBPRODUCT"))
                {
                    dataTypeValidation.Columns.Add("SUBCATALOG_ITEM_NO");
                }
                dataTypeValidation.Columns.Add("ATTRIBUTE_NAME");
                dataTypeValidation.Columns.Add("ATTRIBUTE_VALUES");
                dataTypeValidation.Columns.Add("VALIDATION_TYPE");
                dataTypeValidation.Columns.Add("MESSAGE");
                dataTypeValidation.Columns.Add("ROW_ID");
                var dataTypevalidationList = numericField.Union(dateTimeField);
                foreach (var validList in dataTypevalidationList)
                {
                    if (validList.isValid == false)
                    {
                        DataRow drDateTime = dataTypeValidation.NewRow();
                        var catalogNo = importType.ToUpper().Contains("SUBPRODUCT") ? validList.srcData.Field<string>("SUBCATALOG_ITEM_NO").ToString() : validList.srcData.Field<string>("CATALOG_ITEM_NO").ToString();
                        var rowId = importType.ToUpper().Contains("SUBPRODUCT") ? int.Parse(unPivotTable.AsEnumerable().Where(records => records.Field<string>("SUBCATALOG_ITEM_NO") == catalogNo && records.Field<string>("ATTRIBUTENAME") == "ROW_ID").Select(dup => dup.Field<string>("ATTRIBUTEVALUE")).FirstOrDefault()) : int.Parse(unPivotTable.AsEnumerable().Where(records => records.Field<string>("CATALOG_ITEM_NO") == catalogNo && records.Field<string>("ATTRIBUTENAME") == "ROW_ID").Select(dup => dup.Field<string>("ATTRIBUTEVALUE")).FirstOrDefault());
                        drDateTime["CATALOG_ITEM_NO"] = validList.srcData.Field<string>("CATALOG_ITEM_NO");
                        if (importType.ToUpper().Contains("SUBPRODUCT"))
                        {
                            drDateTime["SUBCATALOG_ITEM_NO"] = validList.srcData.Field<string>("SUBCATALOG_ITEM_NO");
                            drDateTime["ATTRIBUTE_NAME"] = validList.srcData.Field<string>("ATTRIBUTENAME") == "SUBCATALOG_ITEM_NO" ? "SUBITEM#" : validList.srcData.Field<string>("ATTRIBUTENAME");
                        }
                        else
                        {
                            drDateTime["ATTRIBUTE_NAME"] = validList.srcData.Field<string>("ATTRIBUTENAME") == "CATALOG_ITEM_NO" ? "ITEM#" : validList.srcData.Field<string>("ATTRIBUTENAME");
                        }
                        drDateTime["ATTRIBUTE_VALUES"] = validList.srcData.Field<string>("ATTRIBUTEVALUE");
                        drDateTime["VALIDATION_TYPE"] = "DATATYPE_VALIDATION";
                        drDateTime["MESSAGE"] = validList.Message;
                        drDateTime["ROW_ID"] = rowId + 2;
                        dataTypeValidation.Rows.Add(drDateTime);
                    }
                }

                return dataTypeValidation;


            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : DataTypeValidation", ex);
                return null;
            }
        }
        #endregion

        #region DuplicateSubCatalogItemNo
        /// <summary>
        /// Get duplicate sub catalog items from import data
        /// </summary>
        /// <param name="catalogItemNo">DataTable from import sheet</param>
        /// <param name="catalogId">current catalog id</param>
        /// <returns></returns>
        public DataTable DuplicateSubCatalogItemNo(DataTable catalogItemNo, int catalogId)
        {
            try
            {
                var duplicateItem = catalogItemNo.AsEnumerable().Select(dup => new { CATALOG_ITEM_NO = dup.Field<string>("CATALOG_ITEM_NO"), SUBCATALOG_ITEM_NO = dup.Field<string>("SUBCATALOG_ITEM_NO") }).GroupBy(x => x).Where(records => records.Count() > 1).Select(records => records.Key);
                DataTable duplicateCatalogItemNo = new DataTable();
                duplicateCatalogItemNo.Columns.Add("VALID");
                duplicateCatalogItemNo.Columns["VALID"].AutoIncrement = true;
                duplicateCatalogItemNo.Columns["VALID"].AutoIncrementSeed = 1;
                duplicateCatalogItemNo.Columns.Add("CATALOG_ITEM_NO");
                duplicateCatalogItemNo.Columns.Add("SUBCATALOG_ITEM_NO");
                duplicateCatalogItemNo.Columns.Add("ATTRIBUTE_NAME");
                duplicateCatalogItemNo.Columns.Add("ATTRIBUTE_VALUES");
                duplicateCatalogItemNo.Columns.Add("VALIDATION_TYPE");
                duplicateCatalogItemNo.Columns.Add("MESSAGE");
                duplicateCatalogItemNo.Columns.Add("ROW_ID");
                foreach (var items in duplicateItem)
                {
                    DataRow drDuplicate = duplicateCatalogItemNo.NewRow();
                    drDuplicate["CATALOG_ITEM_NO"] = items.CATALOG_ITEM_NO.ToString();
                    drDuplicate["SUBCATALOG_ITEM_NO"] = items.SUBCATALOG_ITEM_NO.ToString();
                    drDuplicate["ATTRIBUTE_NAME"] = "SUBITEM#";
                    drDuplicate["ATTRIBUTE_VALUES"] = items.SUBCATALOG_ITEM_NO.ToString();
                    drDuplicate["VALIDATION_TYPE"] = "DUPLICATE_ITEM_NO";
                    drDuplicate["MESSAGE"] = "Duplicate Records";
                    drDuplicate["ROW_ID"] = catalogItemNo.AsEnumerable().Where(records => records.Field<string>("SUBCATALOG_ITEM_NO") == items.SUBCATALOG_ITEM_NO).Select(dup => dup.Field<int>("ROW_ID")).FirstOrDefault() + 2;
                    duplicateCatalogItemNo.Rows.Add(drDuplicate);
                }

                return duplicateCatalogItemNo;

            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : DuplicateCatalogItemNo", ex);
                return null;
            }
        }
        #endregion
        // Item# import Process - Validation End
        #endregion

        #region Validate Family without hierarchey

        public string ValidateFamilyHierarchey(DataTable validateItems, string importtype, JArray model, string excelPath, string SheetName, int catalogId, string Errorlogoutputformat)
        {
            string validateResults = string.Empty;
            string importTemp, SQLString = string.Empty;
            importTemp = Guid.NewGuid().ToString();
            string validateResultsforitemno = string.Empty;
            ValidationList validationListForItemno = new ValidationList();
            DataTable oattType = new DataTable();
            DataTable oattType1 = new DataTable();
            ValidationList valdationList = new ValidationList();
            DataTable objdatatable = validateItems.Clone();
            DataTable DuplicateFamilyNames = new DataTable();
            try
            {

                objdatatable.Columns.Add("ROW_ID");
                objdatatable.Columns["ROW_ID"].AutoIncrement = true;
                objdatatable.Columns["ROW_ID"].AutoIncrementSeed = 1;
                foreach (DataRow drItems in validateItems.Rows)
                {
                    objdatatable.Rows.Add(drItems.ItemArray);
                }
                int customerId;
                int.TryParse(objLS.Customer_User.Where(customer => customer.User_Name == User.Identity.Name).Select(custId => custId.CustomerId).FirstOrDefault().ToString(), out customerId);
                if (!objdatatable.Columns.Contains("FAMILY_ID"))
                {
                    objdatatable.Columns.Add("FAMILY_ID");
                }
                if (!objdatatable.Columns.Contains("SUBFAMILY_ID"))
                {
                    objdatatable.Columns.Add("SUBFAMILY_ID");
                }
                if (!objdatatable.Columns.Contains("SUBFAMILY_NAME"))
                {
                    objdatatable.Columns.Add("SUBFAMILY_NAME");
                }
                // Get Missing Column List
                string[] manadoryColumns = { "FAMILY_NAME" };

                DataTable MissingColumns = MissingFamilyColumn(objdatatable, manadoryColumns, importtype);
                if (!validateItems.Columns.Contains("FAMILY_NAME"))
                {
                    return ErrorListCatalog(MissingColumns, importtype, SheetName, importTemp, excelPath, Errorlogoutputformat);
                }
                DuplicateFamilyNames = DuplicateFamilyName(objdatatable, catalogId);
                DataTable unPivotTable = adImport.UnPivotTableFamily(objdatatable, importtype);
                DataTable newCatalogItemNo = DuplicateFamilyNames.Clone();

                DataTable newFamilyNameDetails = NewFamilyName(objdatatable, catalogId, importtype);
                // Get Parts Key Items
                DataTable attrType = IC.SelectedFamilyColumnsToImport(objdatatable, model);
                string attributeName = string.Join(",", attrType.AsEnumerable().Select(pickList => "'" + pickList.Field<string>("ATTRIBUTE_NAME") + "'").ToArray());
                DataTable newFamilyKeyValues = NewFamilyKeyValue(unPivotTable, attributeName, catalogId, importtype, attrType, objdatatable);

                //Get Datatype validation
                foreach (DataColumn removeColumn in objdatatable.Columns)
                {
                    if (removeColumn.ColumnName.ToUpper().Trim() == "ACTION")
                    {
                        objdatatable.Columns.Remove(removeColumn);
                        break;
                    }
                }

                DataTable dataTypeValidation = FamilyDataTypeValidation(unPivotTable, customerId, importtype);

                var validationList = MissingColumns.AsEnumerable()
                    .Union(DuplicateFamilyNames.AsEnumerable())
                    .Union(newFamilyNameDetails.AsEnumerable())
                    .Union(newFamilyKeyValues.AsEnumerable())
                    .Union(dataTypeValidation.AsEnumerable());
                if (validationList == null || !validationList.Any())
                {

                    return "Import success~" + importTemp + "~" + objdatatable.Rows.Count;
                }
                else
                {
                    DataSet dtValidation = new DataSet();
                    dtValidation.Tables.Add("VALIDATIONLIST");
                    dtValidation.Tables[0].Columns.Add("VALID");
                    dtValidation.Tables[0].Columns.Add("FAMILY_NAME");
                    dtValidation.Tables[0].Columns.Add("ATTRIBUTE_NAME");
                    dtValidation.Tables[0].Columns.Add("ATTRIBUTE_VALUES");
                    dtValidation.Tables[0].Columns.Add("VALIDATION_TYPE");
                    dtValidation.Tables[0].Columns.Add("MESSAGE");
                    dtValidation.Tables[0].Columns.Add("ROWID");
                    foreach (DataRow drValid in validationList)
                    {
                        dtValidation.Tables[0].Rows.Add(drValid.ItemArray);
                    }
                    return ErrorListFamily(dtValidation.Tables[0], importtype, SheetName, importTemp, excelPath, Errorlogoutputformat);
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : ValidateItemNo", ex);
            }
            return null;
        }


        #region MissingFamilyColumn
        public DataTable MissingFamilyColumn(DataTable objdatatable, string[] columns, string importtype)
        {
            try
            {
                DataTable missingColumnList = new DataTable();
                missingColumnList.Columns.Add("VALID");
                missingColumnList.Columns["VALID"].AutoIncrement = true;
                missingColumnList.Columns["VALID"].AutoIncrementSeed = 1;
                missingColumnList.Columns.Add("FAMILY_NAME");
                missingColumnList.Columns.Add("ATTRIBUTE_NAME");
                missingColumnList.Columns.Add("ATTRIBUTE_VALUES");
                missingColumnList.Columns.Add("VALIDATION_TYPE");
                missingColumnList.Columns.Add("MESSAGE");
                missingColumnList.Columns.Add("ROW_ID");
                foreach (string missColumn in columns)
                {
                    if (!objdatatable.Columns.Contains(missColumn) || (importtype.ToUpper() == "FAMILIES" && !objdatatable.Columns.Contains("FAMILY_NAME")))
                    {
                        DataRow missRowList = missingColumnList.NewRow();
                        missRowList["FAMILY_NAME"] = !objdatatable.Columns.Contains("FAMILY_NAME") ? null : objdatatable.Rows[0]["FAMILY_NAME"].ToString();
                        missRowList["ATTRIBUTE_NAME"] = "-";
                        missRowList["ATTRIBUTE_VALUES"] = "FAMILY_NAME";
                        missRowList["VALIDATION_TYPE"] = "MISSING_COLUMNS";
                        missRowList["MESSAGE"] = "Columns / values not found";
                        missRowList["ROW_ID"] = "2";
                        missingColumnList.Rows.Add(missRowList);
                    }
                }

                return missingColumnList;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : MissingColumnName", ex);
                return null;
            }

        }
        #endregion

        #region DuplicateFamilyValidation
        public DataTable DuplicateFamilyName(DataTable catalogItemNo, int catalogId)
        {
            try
            {




                var duplicateItem = catalogItemNo.AsEnumerable().Select(dup => new { Family_Name = dup.Field<string>("FAMILY_NAME"), SubFamily = dup.Field<string>("SUBFAMILY_NAME") }).GroupBy(x => x).Where(records => records.Count() > 1).Select(records => records.Key);
                DataTable duplicateCatalogItemNo = new DataTable();
                duplicateCatalogItemNo.Columns.Add("VALID");
                duplicateCatalogItemNo.Columns["VALID"].AutoIncrement = true;
                duplicateCatalogItemNo.Columns["VALID"].AutoIncrementSeed = 1;
                duplicateCatalogItemNo.Columns.Add("FAMILY_NAME");
                duplicateCatalogItemNo.Columns.Add("ATTRIBUTE_NAME");
                duplicateCatalogItemNo.Columns.Add("ATTRIBUTE_VALUES");
                duplicateCatalogItemNo.Columns.Add("VALIDATION_TYPE");
                duplicateCatalogItemNo.Columns.Add("MESSAGE");
                duplicateCatalogItemNo.Columns.Add("ROW_ID");
                foreach (var items in duplicateItem)
                {
                    DataRow drDuplicate = duplicateCatalogItemNo.NewRow();
                    drDuplicate["FAMILY_NAME"] = items.Family_Name.ToString();
                    drDuplicate["ATTRIBUTE_NAME"] = "FAMILY_NAME";
                    drDuplicate["VALIDATION_TYPE"] = "DUPLICATE_FAMILY_NAME";
                    drDuplicate["MESSAGE"] = "Duplicate Records";
                    //drDuplicate["ROW_ID"] = catalogItemNo.AsEnumerable().Where(records => records.Field<string>("FAMILY_NAME") == items && records.Field<string>("SUBFAMILY_NAME") == items).Select(dup => dup.Field<int>("ROW_ID")).FirstOrDefault() + 2;
                    duplicateCatalogItemNo.Rows.Add(drDuplicate);
                }

                return duplicateCatalogItemNo;

            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : DuplicateCatalogItemNo", ex);
                return null;
            }
        }
        #endregion
        #region NewFamilyKey
        public DataTable NewFamilyKeyValue(DataTable familyKey, string attributeName, int catalogId, string importType, DataTable attrType, DataTable objdatatable)
        {
            try
            {
                DataTable newFamilyKey = new DataTable();
                newFamilyKey.Columns.Add("VALID");
                newFamilyKey.Columns["VALID"].AutoIncrement = true;
                newFamilyKey.Columns["VALID"].AutoIncrementSeed = 1;
                newFamilyKey.Columns.Add("FAMILY_NAME");
                newFamilyKey.Columns.Add("ATTRIBUTE_NAME");
                newFamilyKey.Columns.Add("ATTRIBUTE_VALUES");
                newFamilyKey.Columns.Add("VALIDATION_TYPE");
                newFamilyKey.Columns.Add("MESSAGE");
                newFamilyKey.Columns.Add("ROW_ID");

                var existingFamilyName = objLS.TB_CATALOG_FAMILY.Join(objLS.TB_FAMILY_KEY, tcp => tcp.FAMILY_ID, tps => tps.FAMILY_ID, (tcp, tps) => new { tcp, tps }).Join(objLS.TB_ATTRIBUTE, tpk => tpk.tps.ATTRIBUTE_ID, ta => ta.ATTRIBUTE_ID, (tpk, ta) => new { tpk, ta }).Where(x => x.tpk.tcp.CATALOG_ID == catalogId && x.ta.ATTRIBUTE_TYPE == 13).Select(y => new { y.tpk.tps, y.ta.ATTRIBUTE_NAME }).ToList();
                if (!existingFamilyName.Any())
                {
                    var newFamilyKeyValues = familyKey.AsEnumerable().Join(attrType.AsEnumerable(), pk => pk.Field<string>("ATTRIBUTENAME"), at => at.Field<string>("ATTRIBUTE_NAME"), (pk, at) => new { pk, at }).Where(newPart => newPart.at.Field<string>("ATTRIBUTE_TYPE") == "13" && !string.IsNullOrEmpty(newPart.pk.Field<string>("ATTRIBUTEVALUE"))).Select(getParts => getParts.pk);
                    if (newFamilyKeyValues.Any())
                    {
                        foreach (var itemNo in newFamilyKeyValues)
                        {
                            DataRow drItemNo = newFamilyKey.NewRow();
                            drItemNo["FAMILY_NAME"] = itemNo.Field<string>("FAMILY_NAME");
                            drItemNo["ATTRIBUTE_NAME"] = itemNo.Field<string>("ATTRIBUTENAME") == "FAMILY_NAME";
                            drItemNo["ATTRIBUTE_VALUES"] = itemNo.Field<string>("ATTRIBUTEVALUE");
                            drItemNo["VALIDATION_TYPE"] = "FAMILY_KEY";
                            drItemNo["MESSAGE"] = "New FAMILY key value found";
                            drItemNo["ROW_ID"] = objdatatable.AsEnumerable().Where(records => records.Field<string>("FAMILY_NAME") == itemNo.Field<string>("FAMILY_NAME").ToString()).Select(dup => dup.Field<int>("ROW_ID")).FirstOrDefault() + 2;
                            newFamilyKey.Rows.Add(drItemNo);
                        }
                    }
                    return newFamilyKey;
                }
                var newFamilyName = existingFamilyName.Join(familyKey.AsEnumerable(), ext => ext.ATTRIBUTE_NAME, newItem => newItem.Field<string>("ATTRIBUTENAME"), (ext, newItem) => new { ext, newItem }).Select(x => x.newItem).Distinct().ToList();
                if (!newFamilyName.Any())
                {
                    return newFamilyKey;
                }
                foreach (DataRow itemNo in familyKey.Rows)
                {
                    string familyName = itemNo["ATTRIBUTEVALUE"].ToString();
                    if (string.IsNullOrEmpty(itemNo.Field<string>("FAMILY_NAME")) || itemNo.Field<string>("ATTRIBUTENAME").ToString().ToUpper().Contains("ROW_ID"))
                        continue;
                    var itemExists = newFamilyName.Count(item => item.Field<string>("ATTRIBUTEVALUE") == familyName);
                    if (itemExists != 0)
                    {
                        DataRow drItemNo = newFamilyKey.NewRow();
                        drItemNo["FAMILY_NAME"] = itemNo.Field<string>("FAMILY_NAME");
                        drItemNo["ATTRIBUTE_NAME"] = itemNo.Field<string>("ATTRIBUTENAME");
                        drItemNo["ATTRIBUTE_VALUES"] = itemNo.Field<string>("ATTRIBUTEVALUE");
                        drItemNo["VALIDATION_TYPE"] = "FAMILY_KEY";
                        drItemNo["MESSAGE"] = "New familykey value found";
                        drItemNo["ROW_ID"] = objdatatable.AsEnumerable().Where(records => records.Field<string>("FAMILY_NAME") == itemNo.Field<string>("FAMILY_NAME").ToString()).Select(dup => dup.Field<int>("ROW_ID")).FirstOrDefault() + 2;
                        newFamilyKey.Rows.Add(drItemNo);
                    }
                }


                return newFamilyKey;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : NewPartsKeyValue", ex);
                return null;
            }
        }
        #endregion

        #region Family DataType Validation
        public DataTable FamilyDataTypeValidation(DataTable unPivotTable, int customerId, string importType)
        {
            try
            {
                var customerAttributes = objLS.TB_ATTRIBUTE.Join(objLS.CUSTOMERATTRIBUTE, ta => ta.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (ta, ca) => new { ta, ca }).Where(cus => cus.ca.CUSTOMER_ID == customerId).Select(attrList => attrList.ta);
                int number;
                decimal number1;
                string[] publishOptions = new string[] { "FAMILY_PUBLISH2PORTAL", "FAMILY_PUBLISH2PRINT", "FAMILY_PUBLISH2EXPORT", "FAMILY_PUBLISH2PDF", "FAMILY_PUBLISH2WEB" };

                var numericField = unPivotTable.AsEnumerable().Join(customerAttributes, srcData => srcData.Field<string>("ATTRIBUTENAME"), ca => ca.ATTRIBUTE_NAME, (srcData, ca) => new { srcData, ca }).
                                   Where(x => (x.ca.ATTRIBUTE_TYPE == 4 || x.ca.ATTRIBUTE_DATATYPE.ToUpper().Contains("NUMBER"))).Select(num => new
                                   {
                                       isValid = (decimal.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out number1) || int.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out number)),
                                       Message = !(decimal.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out number1) || int.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out number)) ? "Invalid numeric field" : num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(),
                                       num.srcData
                                   });


                string sqlString = string.Empty;
                DataRow[] dRow = unPivotTable.Select("ATTRIBUTENAME like '%PUBLISH2%'");
                DataTable dtPublish = new DataTable();
                foreach (DataRow dr in dRow)
                {
                    int Publish = 0;
                    if (dr.ItemArray[6].ToString() == "Y")
                    {
                        Publish = 1;
                    }
                    else if (dr.ItemArray[6].ToString() == "N")
                    {
                        Publish = 0;
                    }
                    sqlString = sqlString + "select '" + dr.ItemArray[5].ToString() + "' as AttributeName,ISNUMERIC('" + Publish + "') as isValid,case when ISNUMERIC('" + Publish + "') = 0  then 'Invalid numeric field' end Message,'" + Publish + "' Value union all ";
                }
                sqlString = sqlString.Substring(0, sqlString.Length - 10);
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    SqlCommand cmd = new SqlCommand(sqlString, con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dtPublish);
                }
                var numericFieldPublish = dtPublish.AsEnumerable().Join(unPivotTable.AsEnumerable(), pub => pub.Field<string>("ATTRIBUTENAME"), srcData => srcData.Field<string>("ATTRIBUTENAME"), (pub, srcData) => new { pub, srcData }).Where(x => x.pub.Field<int>("isValid") == 0).Select(
                    num => new
                    {
                        isValid = false,
                        Message = "Invalid numeric field",
                        num.srcData
                    });
                if (numericField.Any())
                    numericField.Union(numericFieldPublish);
                else
                    numericField = numericFieldPublish;

                DateTime dateTime = new DateTime();
                var dateTimeField = unPivotTable.AsEnumerable().Join(customerAttributes, srcData => srcData.Field<string>("ATTRIBUTENAME"), ca => ca.ATTRIBUTE_NAME, (srcData, ca) => new { srcData, ca }).
                                   Where(x => x.ca.ATTRIBUTE_DATATYPE == "Date and Time").Select(num => new { isValid = DateTime.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out dateTime), Message = DateTime.TryParse(num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), out dateTime) == true ? "Invalid date field" : num.srcData.Field<string>("ATTRIBUTEVALUE").ToString(), num.srcData });
                DataTable dataTypeValidation = new DataTable();
                dataTypeValidation.Columns.Add("VALID");
                dataTypeValidation.Columns["VALID"].AutoIncrement = true;
                dataTypeValidation.Columns["VALID"].AutoIncrementSeed = 1;
                dataTypeValidation.Columns.Add("FAMILY_NAME");
                dataTypeValidation.Columns.Add("ATTRIBUTE_NAME");
                dataTypeValidation.Columns.Add("ATTRIBUTE_VALUES");
                dataTypeValidation.Columns.Add("VALIDATION_TYPE");
                dataTypeValidation.Columns.Add("MESSAGE");
                dataTypeValidation.Columns.Add("ROW_ID");
                var dataTypevalidationList = numericField.Union(dateTimeField);
                foreach (var validList in dataTypevalidationList)
                {
                    if (validList.isValid == false)
                    {
                        DataRow drDateTime = dataTypeValidation.NewRow();
                        var catalogNo = validList.srcData.Field<string>("FAMILY_NAME").ToString();
                        var rowId = importType.ToUpper().Contains("FAMILIES") ? int.Parse(unPivotTable.AsEnumerable().Where(records => records.Field<string>("FAMILY_NAME") == catalogNo && records.Field<string>("ATTRIBUTENAME") == "ROW_ID").Select(dup => dup.Field<string>("ATTRIBUTEVALUE")).FirstOrDefault()) : int.Parse(unPivotTable.AsEnumerable().Where(records => records.Field<string>("FAMILY_NAME") == catalogNo && records.Field<string>("ATTRIBUTENAME") == "ROW_ID").Select(dup => dup.Field<string>("ATTRIBUTEVALUE")).FirstOrDefault());
                        drDateTime["FAMILY_NAME"] = validList.srcData.Field<string>("FAMILY_NAME");
                        drDateTime["ATTRIBUTE_NAME"] = validList.srcData.Field<string>("ATTRIBUTENAME");
                        drDateTime["ATTRIBUTE_VALUES"] = validList.srcData.Field<string>("ATTRIBUTEVALUE");
                        drDateTime["VALIDATION_TYPE"] = "DATATYPE_VALIDATION";
                        drDateTime["MESSAGE"] = validList.Message;
                        drDateTime["ROW_ID"] = rowId + 2;
                        dataTypeValidation.Rows.Add(drDateTime);
                    }
                }

                return dataTypeValidation;


            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : DataTypeValidation", ex);
                return null;
            }
        }

        #endregion

        #region Error list
        public string ErrorListFamily(DataTable validationList, string importtype, string SheetName, string importTemp, string excelPath, string Errorlogoutputformat)
        {
            try
            {
                DataSet dtValidation = new DataSet();
                dtValidation.Tables.Add("VALIDATIONLIST");
                dtValidation.Tables[0].Columns.Add("VALID");
                dtValidation.Tables[0].Columns.Add("FAMILY_NAME");
                dtValidation.Tables[0].Columns.Add("ATTRIBUTE_NAME");
                dtValidation.Tables[0].Columns.Add("ATTRIBUTE_VALUES");
                dtValidation.Tables[0].Columns.Add("VALIDATION_TYPE");
                dtValidation.Tables[0].Columns.Add("MESSAGE");
                dtValidation.Tables[0].Columns.Add("ROWID");
                foreach (DataRow drValid in validationList.Rows)
                {
                    dtValidation.Tables[0].Rows.Add(drValid.ItemArray);
                }
                if (dtValidation.Tables[0].Columns.Contains("VALID"))
                {
                    dtValidation.Tables[0].Columns.Remove("VALID");
                }
                HttpContext.Current.Session["FAMILYVALIDATIONSESSION"] = dtValidation;
                string filepath = HttpContext.Current.Server.MapPath("~/Content/" + SheetName + "");
                string format = filepath + ".csv";
                CreateCSVFile(dtValidation.Tables[0], format);
                string txtformat = filepath + ".txt";
                CreateTextFile(dtValidation.Tables[0], txtformat, "\t");
                ExportDataSetToExcel(dtValidation as DataSet, SheetName);
                ExportErrorLogDataSetToExcel(dtValidation, importTemp, excelPath, SheetName, Errorlogoutputformat);
                return string.Join("~", dtValidation.Tables[0].AsEnumerable().Select(val => val.Field<string>("VALIDATION_TYPE")).Distinct().ToArray()) + "~FAMILY_NAME" + "~" + importTemp;
            }
            catch (Exception ex)
            { }
            return null;
        }
        #endregion
        #region NewFamilyName
        /// <summary>
        /// Get New Catalog Items from import data
        /// </summary>
        /// <param name="catalogItemNo">DataTable from import sheet</param>
        /// <param name="catalogId">current catalog id</param>
        /// <returns></returns>
        public DataTable NewFamilyName(DataTable catalogItemNo, int catalogId, string importType)
        {
            try
            {
                var existingItemNo = objLS.TB_CATALOG_FAMILY.Join(objLS.TB_FAMILY, tcp => tcp.FAMILY_ID, tps => tps.FAMILY_ID, (tcp, tps) => new { tcp, tps }).Where(x => x.tcp.CATALOG_ID == catalogId).Select(y => y.tps).ToList();
                var newItemNo = existingItemNo.Join(catalogItemNo.AsEnumerable(), ext => ext.FAMILY_NAME, newItem => newItem.Field<string>("FAMILY_NAME"), (ext, newItem) => new { ext, newItem }).Select(x => x.newItem).ToList();
                DataTable newCatalogItemNo = new DataTable();
                newCatalogItemNo.Columns.Add("VALID");
                newCatalogItemNo.Columns["VALID"].AutoIncrement = true;
                newCatalogItemNo.Columns["VALID"].AutoIncrementSeed = 1;
                newCatalogItemNo.Columns.Add("FAMILY_NAME");
                newCatalogItemNo.Columns.Add("ATTRIBUTE_NAME");
                newCatalogItemNo.Columns.Add("ATTRIBUTE_VALUES");
                newCatalogItemNo.Columns.Add("VALIDATION_TYPE");
                newCatalogItemNo.Columns.Add("MESSAGE");
                newCatalogItemNo.Columns.Add("ROW_ID");
                //if(newItemNo.Count==0)
                //    return newCatalogItemNo;
                foreach (DataRow itemNo in catalogItemNo.Rows)
                {
                    string catItemNo = itemNo["FAMILY_NAME"].ToString();

                    if (string.IsNullOrEmpty(catItemNo))
                        continue;

                    var itemExists = newItemNo.Count(item => item.Field<string>("FAMILY_NAME") == catItemNo);

                    if (itemExists == 0)
                    {
                        DataRow drItemNo = newCatalogItemNo.NewRow();
                        drItemNo["FAMILY_NAME"] = string.IsNullOrEmpty(itemNo.Field<string>("FAMILY_NAME")) ? "-" : itemNo.Field<string>("FAMILY_NAME");
                        drItemNo["ATTRIBUTE_NAME"] = "FAMILYNAME";
                        drItemNo["ATTRIBUTE_VALUES"] = itemNo.Field<string>("FAMILY_NAME");
                        drItemNo["VALIDATION_TYPE"] = "NEW_FAMILY_NAME";
                        drItemNo["MESSAGE"] = "New family is not insert in this import. ";
                        drItemNo["ROW_ID"] = itemNo.Field<int>("ROW_ID") + 2;
                        newCatalogItemNo.Rows.Add(drItemNo);
                    }
                }
                return newCatalogItemNo;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : NEW_FAMILY_NAME", ex);
                return null;
            }

        }
        #endregion

        #endregion

        #region User Import
        /// <summary>
        /// excel sheet(.xls format is converted into datatable)
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="Sheetname"></param>
        /// <returns>returns the datatable in the sheet values</returns>
        public static DataTable ConvertExcelToDataTable(string FileName, string Sheetname)
        {
            try
            {
                DataTable dtResult = null;
                string tableName = Sheetname;
                int totalSheet = 0; //No of sheets on excel file  
                string strConn;
                if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName +
                              ";Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1\"";
                else
                    //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName +
                    //          ";Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1\"";
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName +
                             ";Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1\"";
                //  using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1;';"))
                using (OleDbConnection objConn = new OleDbConnection(strConn))

                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();
                    DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = string.Empty;
                    if (dt != null)
                    {
                        var tempDataTable = (from dataRow in dt.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();

                        dt = tempDataTable;
                        totalSheet = dt.Rows.Count;
                        sheetName = Sheetname + "$";
                    }
                    cmd.Connection = objConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(ds, tableName);
                    dtResult = ds.Tables[tableName];
                    objConn.Close();
                    return dtResult; //Returning Dattable  
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error at ConvertExcelToDataTable ", ex);
                return null;
            }

        }

        public DataTable PasswordCheckUserImport(DataTable userImportTable)
        {
            try
            {
                int customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                //foreach (DataRow checkPassword in userImportTable.Rows)
                //{
                //    string securePassword1 = checkPassword["SECUREPASSWORD1"].ToString();
                //    string securePassword2 = checkPassword["SECUREPASSWORD2"].ToString();
                foreach (DataRow checkPassword in userImportTable.Rows)
                {
                    string securePassword1 = ConfigurationManager.AppSettings["DefaultCatalogStudioPassword"].ToString();
                    string securePassword2 = ConfigurationManager.AppSettings["DefaultCatalogStudioPasswordSalt"].ToString();

                    if (!userImportTable.Columns.Contains("SECUREPASSWORD1"))
                    {
                        userImportTable.Columns.Add("SECUREPASSWORD1");
                    }

                    if (!userImportTable.Columns.Contains("SECUREPASSWORD2"))
                    {
                        userImportTable.Columns.Add("SECUREPASSWORD2");
                    }
                    checkPassword["SECUREPASSWORD1"] = securePassword1;
                    checkPassword["SECUREPASSWORD2"] = securePassword2;


                    if (securePassword1 != "" && securePassword2 != "")
                    {
                        string emailId = checkPassword["EMAIL"].ToString();
                        var checkExisitingUser = objLS.aspnet_Membership.Where(x => x.Email == emailId).Select(x => x.Email).FirstOrDefault();
                        if (checkExisitingUser == null)
                        {
                            checkPassword["SECUREPASSWORD1"] = "WxnoeWnZwGxC1e1zngqD+YeIDww=";
                            checkPassword["SECUREPASSWORD2"] = "T1Kb81FZdpb3TTTikcPgdQ==";
                        }
                        userImportTable.AcceptChanges();
                    }
                    else
                    {
                        checkPassword["SECUREPASSWORD1"] = "WxnoeWnZwGxC1e1zngqD+YeIDww=";
                        checkPassword["SECUREPASSWORD2"] = "T1Kb81FZdpb3TTTikcPgdQ==";
                    }
                    userImportTable.AcceptChanges();
                }
                return userImportTable;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : PasswordCheckUserImport", ex);
                return null;
            }
        }



        /// <summary>
        /// Import the sheet
        /// Datatable values are the inserted into the table
        /// </summary>
        /// <param name="importExcelSheetDDSelectionValue">temp sheet name is stored in server -> app folder</param>
        /// <param name="excelPath">path contains the temp sheet folder name</param>
        /// <param name="importType">Import type(User or Product or Families)</param>
        /// <param name="importFormat">Import format(.xls format)</param>
        /// <returns>returns how many values inserted ,updated, skipped, deleted</returns>
        [System.Web.Http.HttpPost]
        public string UserImport(string importExcelSheetDDSelectionValue, string excelPath, string importType, string importFormat)
        {
            try
            {

                int customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                int insertRecords = 0; int updateRecords = 0; int skippedRecords = 0; int deleteRecords = 0;
                var ImportTable = new DataTable();
                bool isAnonymous;
                ImportTable = ConvertExcelToDataTable(excelPath, importExcelSheetDDSelectionValue);
                DataTable userImportTable = ImportTable.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                Stopwatch sw = new Stopwatch();
                sw.Start();
                foreach (DataRow dr in userImportTable.Rows)
                {
                    string action = dr["Action"].ToString();
                    action = action.ToUpper();
                    if (action.Equals("SKIPPED"))
                    {
                        skippedRecords = skippedRecords + 1;
                        dr.Delete();
                    }
                }
                userImportTable.AcceptChanges();
                userImportTable = PasswordCheckUserImport(userImportTable);
                foreach (DataRow dr in userImportTable.Rows)
                {
                    string action = dr["Action"].ToString();
                    action = action.ToUpper();
                    string inactiveEmailid = dr["EMAIL"].ToString();
                    var chkExsistUser = objLS.aspnet_Membership.Where(x => x.Email == inactiveEmailid).Select(x => x.Email).FirstOrDefault();
                    if (chkExsistUser == null && action.Equals("DELETE"))
                    {
                        action = "";
                        dr.Delete();
                    }
                    if (action.Equals("DELETE"))
                    {
                        deleteRecords = deleteRecords + 1;
                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_LS_USER_IMPORT";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.CommandTimeout = 0;
                            objSqlCommand.Parameters.Add("@INACTIVEEMAIL", SqlDbType.NVarChar, 100).Value = inactiveEmailid;
                            objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "DELETE";
                            objSqlConnection.Open();
                            objSqlCommand.ExecuteNonQuery();
                            objSqlConnection.Close();
                        }
                        dr.Delete();
                    }

                }
                userImportTable.AcceptChanges();
                foreach (DataRow dr in userImportTable.Rows)
                {

                    var userId = Guid.NewGuid().ToString();
                    string email = dr["EMAIL"].ToString();
                    string username = dr["USER_NAME"].ToString();
                    var CheckUserName = objLS.Customer_User.Where(x => x.Email == email).Select(x => x.User_Name).FirstOrDefault();
                    var checkuser = objLS.aspnet_Membership.Join(objLS.aspnet_Users, ams => ams.UserId, aus => aus.UserId, (ams, aus) => new { ams, aus })
                        .Where(x => x.ams.Email == (email) && x.aus.UserName == (CheckUserName)).Select(x => x.ams.UserId).FirstOrDefault();
                    string strCheckUser = checkuser.ToString();
                    string status = dr["STATUS"].ToString();
                    bool statusbool = string.Equals(status, "ACTIVE", StringComparison.CurrentCultureIgnoreCase);
                    if ((strCheckUser != null) || (strCheckUser != ""))
                    {
                        if (!strCheckUser.Equals("00000000-0000-0000-0000-000000000000"))
                        {
                            userId = strCheckUser;
                            updateRecords++;
                        }
                        else
                        {
                            insertRecords++;
                        }

                    }
                    else
                    {
                        insertRecords++;
                    }
                    if (statusbool == true)
                    {
                        isAnonymous = false;
                    }
                    else if (statusbool == false)
                    {
                        isAnonymous = true;
                    }
                    else
                    {
                        isAnonymous = false;
                    }
                    string securepassword1 = dr["SECUREPASSWORD1"].ToString();
                    string securepassword2 = dr["SECUREPASSWORD2"].ToString();
                    string password1 = string.Empty;
                    string password2 = string.Empty;
                    if (securepassword1 == null || securepassword1 == "")
                    {
                        if ((strCheckUser != null) || (strCheckUser != ""))
                        {
                            if (!strCheckUser.Equals("00000000-0000-0000-0000-000000000000"))
                            {
                                password1 = objLS.aspnet_Membership.Where(x => x.UserId == (new Guid(userId)) && x.Email == email).Select(x => x.Password).FirstOrDefault().ToString();
                                password2 = objLS.aspnet_Membership.Where(x => x.UserId == (new Guid(userId)) && x.Email == email).Select(x => x.PasswordSalt).FirstOrDefault().ToString();
                            }
                            else
                            {
                                password1 = string.Empty;
                                password2 = string.Empty;
                            }
                        }
                        else
                        {
                            password1 = string.Empty;
                            password2 = string.Empty;
                        }
                        bool checkpassword1 = string.IsNullOrEmpty(password1);
                        bool checkpassword2 = string.IsNullOrEmpty(password2);
                        if (checkpassword1 == false)
                        {
                            if ((password1 != null) || (password1 != ""))
                            {

                                securepassword1 = password1;
                            }
                            else
                            {
                                securepassword1 = "WxnoeWnZwGxC1e1zngqD+YeIDww=";
                            }

                        }
                        else
                        {
                            securepassword1 = "WxnoeWnZwGxC1e1zngqD+YeIDww=";
                        }
                        if (checkpassword2 == false)
                        {
                            if ((password2 != null) || (password2 != ""))
                            {
                                securepassword2 = password2;
                            }
                            else
                            {
                                securepassword2 = "T1Kb81FZdpb3TTTikcPgdQ==";
                            }
                        }
                        else
                        {
                            securepassword2 = "T1Kb81FZdpb3TTTikcPgdQ==";
                        }

                    }

                    string sameUser = objLS.aspnet_Membership.Where(x => x.Email == User.Identity.Name).Select(x => x.UserId).FirstOrDefault().ToString();

                    if ((!sameUser.Equals(userId)) && (!email.Equals(User.Identity.Name)))
                    {
                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_LS_USER_IMPORT";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.CommandTimeout = 0;
                            objSqlCommand.Parameters.Add("@UserId", SqlDbType.NVarChar, 100).Value = userId.ToString();
                            objSqlCommand.Parameters.Add("@customerId", SqlDbType.Int).Value = customerid;
                            objSqlCommand.Parameters.Add("@rolename", SqlDbType.NVarChar, 100).Value = dr["ROLE_NAME"].ToString();
                            objSqlCommand.Parameters.Add("@IsAnonymous", SqlDbType.Bit).Value = isAnonymous;
                            objSqlCommand.Parameters.Add("@User_Name", SqlDbType.NVarChar, 100).Value = dr["USER_NAME"].ToString();
                            objSqlCommand.Parameters.Add("@STATUS", SqlDbType.NVarChar, 100).Value = dr["STATUS"].ToString();
                            objSqlCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar, 100).Value = dr["EMAIL"].ToString();
                            objSqlCommand.Parameters.Add("@PASSWORD", SqlDbType.NVarChar, 100).Value = securepassword1;
                            objSqlCommand.Parameters.Add("@PASSWORDSALT", SqlDbType.NVarChar, 100).Value = securepassword2;
                            objSqlCommand.Parameters.Add("@ADDRESS", SqlDbType.NVarChar, 100).Value = dr["ADDRESS"].ToString();
                            objSqlCommand.Parameters.Add("@FIRSTNAME", SqlDbType.NVarChar, 100).Value = dr["FIRSTNAME"].ToString();
                            objSqlCommand.Parameters.Add("@LASTNAME", SqlDbType.NVarChar, 100).Value = dr["LASTNAME"].ToString();
                            objSqlCommand.Parameters.Add("@CITY", SqlDbType.NVarChar, 100).Value = dr["CITY"].ToString();
                            objSqlCommand.Parameters.Add("@STATE", SqlDbType.NVarChar, 100).Value = dr["STATE"].ToString();
                            objSqlCommand.Parameters.Add("@ZIP", SqlDbType.NVarChar, 100).Value = dr["ZIP"].ToString();
                            objSqlCommand.Parameters.Add("@FAX", SqlDbType.NVarChar, 100).Value = dr["FAX"].ToString();
                            objSqlCommand.Parameters.Add("@COUNTRY", SqlDbType.NVarChar, 100).Value = dr["COUNTRY"].ToString();
                            objSqlCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar, 100).Value = dr["PHONE"].ToString();
                            objSqlCommand.Parameters.Add("@CATALOGNAME", SqlDbType.NVarChar, 100).Value = dr["DEFAULTCATALOG"].ToString();
                            objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "INSERT";
                            objSqlConnection.Open();
                            objSqlCommand.ExecuteNonQuery();
                            objSqlConnection.Close();

                        }
                    }
                    else
                    {
                        dr["Action"] = "Skipped";
                        skippedRecords++;
                    }


                }
                sw.Stop();
                string calcualteImportTiminings = string.Format("{0}:{1}:{2}", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds);
                return "true" + "~" + insertRecords + "~" + updateRecords + "~" + calcualteImportTiminings + "~" + skippedRecords + "~" + deleteRecords + "~" + User.Identity.Name;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at getUserExportDetails ", ex);
                return "false";
            }
        }
        /// <summary>
        /// Validating the User Import Sheet 
        /// Check the columns and values in the sheet
        /// </summary>
        /// <param name="importExcelSheetDDSelectionValue">temp sheet name is stored in server -> app folder</param>
        /// <param name="excelPath">path contains the temp sheet folder name</param>
        /// <param name="importType">Import type(User or Product or Families)</param>
        /// <param name="importFormat">Import format(.xls format)</param>
        /// <returns>returns the error log values</returns>
        [System.Web.Http.HttpPost]
        public string ValidateUserImport(string importExcelSheetDDSelectionValue, string excelPath, string importType, string importFormat)
        {

            try
            {
                string[] columnName = new string[]{ "STATUS",
                                                    "ROLE_NAME",
                                                    "EMAIL",
                                                    "FIRSTNAME",
                                                    //"SECUREPASSWORD1",
                                                   // "SECUREPASSWORD2",
                                                    "USER_NAME",
                                                    "DEFAULTCATALOG"};
                string[] columnValues = new string[]{ "STATUS",
                                                    "ROLE_NAME",
                                                    "FIRSTNAME",
                                                    "EMAIL",
                                                    "USER_NAME",
                                                    "DEFAULTCATALOG"};

                string[] missingColumn = new string[columnName.Length];
                int missingValues = 0;
                int emailidval = 0; int duplicateRows = 0;
                int customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                string errorMissingColumn = string.Empty;
                var ImportTable = new DataTable();
                ImportTable = ConvertExcelToDataTable(excelPath, importExcelSheetDDSelectionValue);
                DataTable userImportTable = ImportTable.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                DataColumnCollection columns = userImportTable.Columns;
                int j = 0;

                //Check the column is given in the  sheet or not
                userImportTable.Columns.Add("Missing Column", typeof(string));
                for (int i = 0; i < columnName.Length; i++)
                {
                    if (!columns.Contains(columnName[i]))
                    {
                        if (j == 0)
                        {
                            missingColumn[j] = columnName[i];
                            errorMissingColumn = errorMissingColumn + columnName[i];
                            j++;
                        }
                        else
                        {
                            missingColumn[j] = columnName[i];
                            errorMissingColumn = errorMissingColumn + "~" + columnName[i];
                            j++;
                        }

                    }
                }

                //Write the missing column in datatable
                foreach (DataRow dr in userImportTable.Rows)
                {

                    dr["Missing Column"] = errorMissingColumn;
                    break;
                }

                //Check the status is active or Inactive
                userImportTable.Columns.Add("Missing Values", typeof(string));
                foreach (DataRow dr in userImportTable.Rows)
                {
                    if (columns.Contains("STATUS"))
                    {
                        string checkRow = dr["STATUS"].ToString();
                        if ((!checkRow.Equals("ACTIVE")) && (!checkRow.Equals("INACTIVE")))
                        {

                            missingValues++;
                            string updateMissingValues = dr["Missing Values"].ToString();
                            if (updateMissingValues != "")
                            {
                                updateMissingValues = updateMissingValues + ".|";
                            }
                            dr["Missing Values"] = updateMissingValues + "Please give Vaild Status";
                            userImportTable.AcceptChanges();
                        }

                    }

                }
                userImportTable.AcceptChanges();

                //Check the email validation
                foreach (DataRow dr in userImportTable.Rows)
                {
                    if (columns.Contains("EMAIL"))
                    {
                        string checkEmail = dr["EMAIL"].ToString();
                        try
                        {
                            var addr = new System.Net.Mail.MailAddress(checkEmail);
                            if (addr.Address == checkEmail) { }
                            else
                            {
                                emailidval++;
                                string updateMissingValues = dr["Missing Values"].ToString();
                                if (updateMissingValues != "")
                                {
                                    updateMissingValues = updateMissingValues + ".|";
                                }
                                dr["Missing Values"] = updateMissingValues + "Please give Vaild Email Id";
                            }
                        }
                        catch
                        {
                            emailidval++;
                            string updateMissingValues = dr["Missing Values"].ToString();
                            if (updateMissingValues != "")
                            {
                                updateMissingValues = updateMissingValues + ".|";
                            }
                            dr["Missing Values"] = updateMissingValues + "Please give Vaild Email Id";
                            userImportTable.AcceptChanges();
                        }
                    }


                }
                userImportTable.AcceptChanges();

                //Missing Values Validation
                for (int i = 0; i < columnValues.Length; i++)
                {
                    if (columns.Contains(columnValues[i]))
                    {
                        foreach (DataRow dr in userImportTable.Select("" + columnValues[i] + " is NULL or " + columnValues[i] + "=''"))
                        {
                            missingValues++;
                            string updateMissingValues = dr["Missing Values"].ToString();
                            if (updateMissingValues != "")
                            {
                                updateMissingValues = updateMissingValues + ".|";
                            }
                            dr["Missing Values"] = updateMissingValues + "" + columnValues[i] + " " + " has no values";
                            updateMissingValues = string.Empty;
                        }
                    }


                }
                userImportTable.AcceptChanges();

                //Role name Validation

                string[] roleName = objLS.aspnet_Roles.Where(x => x.CustomerId == customerid).Select(x => x.RoleName).ToArray();
                int valRoleName = 0;
                for (int i = 0; i < roleName.Length; i++)
                {
                    string[] divRoleName = roleName[i].Split(new[] { '_' }, 2);
                    roleName[i] = divRoleName[1];
                }
                foreach (DataRow dr in userImportTable.Rows)
                {
                    string checkRoleName = dr["ROLE_NAME"].ToString();

                    if (!roleName.Contains(checkRoleName) && checkRoleName != "")
                    {
                        valRoleName++;
                        string updateMissingValues = dr["Missing Values"].ToString();
                        if (updateMissingValues != "")
                        {
                            updateMissingValues = updateMissingValues + ".|";
                        }
                        dr["Missing Values"] = updateMissingValues + "Please Enter the valid RoleName";
                    }


                }
                userImportTable.AcceptChanges();

                //Duplicate Rows Checking
                var duplicates = userImportTable.AsEnumerable().GroupBy(dr => dr.Field<string>("EMAIL")).Where(g => g.Count() > 1).Select(g => g.First()).ToList();
                foreach (var itr in duplicates)
                {
                    string duplicateEmail = itr[11].ToString();
                    foreach (DataRow dr in userImportTable.Rows)
                    {
                        string checkDuplicate = dr["EMAIL"].ToString();
                        if (checkDuplicate.Equals(duplicateEmail))
                        {
                            string updateMissingValues = dr["Missing Values"].ToString();
                            if (updateMissingValues != "")
                            {
                                updateMissingValues = updateMissingValues + ".|";
                            }
                            dr["Missing Values"] = updateMissingValues + "Duplicate Values";
                        }
                    }
                }
                duplicateRows = duplicates.Count;
                foreach (DataRow dr in userImportTable.Rows)
                {
                    if (userImportTable.Columns.Contains("EMAIL") && (userImportTable.Columns.Contains("ACTION")))
                    {
                        string chkExsistsUser = dr["EMAIL"].ToString();
                        string action = dr["ACTION"].ToString();
                        action = action.ToUpper();
                        var email = objLS.aspnet_Membership.Where(x => x.Email == chkExsistsUser).Select(x => x.Email).FirstOrDefault();
                        if (email != null && action.Equals("NEW"))
                        {
                            missingValues++;
                            string updateMissingValues = dr["Missing Values"].ToString();
                            if (updateMissingValues != "")
                            {
                                updateMissingValues = updateMissingValues + ".|";
                            }
                            dr["Missing Values"] = updateMissingValues + "User is already exists";
                            userImportTable.AcceptChanges();
                        }
                        if (email == null && (action.Equals("UPDATE") || action.Equals("DELETE")))
                        {
                            missingValues++;
                            string updateMissingValues = dr["Missing Values"].ToString();
                            if (updateMissingValues != "")
                            {
                                updateMissingValues = updateMissingValues + ".|";
                            }
                            dr["Missing Values"] = updateMissingValues + "User not exists";
                            userImportTable.AcceptChanges();
                        }
                    }

                }
                userImportTable.AcceptChanges();

                //CHECK THE CATALOG NAME UNDER THE CUSTOMER OR NOT
                foreach (DataRow dr in userImportTable.Rows)
                {
                    string catalogName = dr["DEFAULTCATALOG"].ToString();

                    //var catalogId = objLS.TB_CATALOG.Where(x => x.CATALOG_NAME == catalogName).Select(x => x.CATALOG_ID).FirstOrDefault();

                    var catalogId = objLS.TB_CATALOG.Join(objLS.Customer_Catalog, tc => tc.CATALOG_ID, cc => cc.CATALOG_ID, (tc, cc) => new { tc, cc }).Where(x => x.tc.CATALOG_NAME == catalogName && x.cc.CustomerId == customerid).Select(x => x.tc.CATALOG_ID).FirstOrDefault();

                    if (catalogId != 0)
                    {
                        var chkCatalogId = objLS.Customer_Catalog.Where(x => x.CATALOG_ID == catalogId && x.CustomerId == customerid).Select(x => x.CATALOG_ID).FirstOrDefault();
                        if (chkCatalogId == 0 || chkCatalogId == null)
                        {
                            missingValues++;
                            string updateMissingValues = dr["Missing Values"].ToString();
                            if (updateMissingValues != "")
                            {
                                updateMissingValues = updateMissingValues + ".|";
                            }
                            dr["Missing Values"] = updateMissingValues + "These Catalog Name is not provided under the customer";
                        }
                    }
                    else
                    {
                        missingValues++;
                        string updateMissingValues = dr["Missing Values"].ToString();
                        if (updateMissingValues != "")
                        {
                            updateMissingValues = updateMissingValues + ".|";
                        }
                        dr["Missing Values"] = updateMissingValues + "Please give the valid Catalog Name";
                    }
                }
                userImportTable.AcceptChanges();

                var returnMultipleValues = EmailIdValidationUserImport(userImportTable, missingValues);
                userImportTable = returnMultipleValues.Item1;
                missingValues = returnMultipleValues.Item2;
                System.Web.HttpContext.Current.Session["userImportTableLog"] = userImportTable;
                System.Web.HttpContext.Current.Session["ExportErrorTable"] = userImportTable;

                return "true" + "~" + j + "~" + missingValues + "~" + emailidval + "~" + valRoleName + "~" + duplicateRows;


            }
            catch (Exception ex)
            {
                _logger.Error("Error at validateUserImport ", ex);
                return "false";
            }
        }

        public Tuple<DataTable, int> EmailIdValidationUserImport(DataTable userImportTable, int missingValues)
        {
            try
            {
                int customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                foreach (DataRow checkEmailId in userImportTable.Rows)
                {
                    string checkEmailIdVerification = checkEmailId["EMAIL"].ToString();
                    int customerUserId = objLS.Customer_User.Where(x => x.Email == checkEmailIdVerification).Select(x => x.CustomerId).FirstOrDefault();
                    if (customerid != customerUserId && customerUserId != 0)
                    {
                        if (userImportTable.Columns.Contains("Missing Values"))
                        {
                            missingValues = missingValues + 1;
                            string missingColumnValue = checkEmailId["Missing Values"].ToString();
                            if (missingColumnValue == "")
                            {
                                checkEmailId["Missing Values"] = "These Email id is already exists in another customer,Please Change the EmailId";
                            }
                            else
                            {
                                checkEmailId["Missing Values"] = missingColumnValue + "|." + "These Email id is already exists in another customer,Please Change the EmailId";
                            }
                        }
                        else
                        {
                            userImportTable.Columns.Add("Missing Values");
                            string missingColumnValue = checkEmailId["Missing Values"].ToString();
                            missingValues = missingValues + 1;
                            if (missingColumnValue == "")
                            {
                                checkEmailId["Missing Values"] = "These Email id is already exists in another customer,Please Change the EmailId";
                            }
                            else
                            {
                                checkEmailId["Missing Values"] = missingColumnValue + "|." + "These Email id is already exists in another customer,Please Change the EmailId";
                            }
                        }
                    }
                    userImportTable.AcceptChanges();
                }
                return Tuple.Create(userImportTable, missingValues);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : EmailIdValidationUserImport", ex);
                return null;
            }
        }
        /// <summary>
        /// User view log is take the values in datatable (Stored in the session -> method ValidateUserImport())
        /// </summary>
        /// <returns>returns the datatable values in json format for view the error log</returns>
        [System.Web.Http.HttpPost]
        public JsonResult UserViewLog()
        {
            DashBoardModel model = new DashBoardModel();
            var userImportTableLog = (DataTable)System.Web.HttpContext.Current.Session["userImportTableLog"];
            foreach (DataColumn columns in userImportTableLog.Columns)
            {
                var objPTColumns = new PTColumns();
                objPTColumns.Caption = columns.Caption;
                objPTColumns.ColumnName = columns.ColumnName;
                model.Columns.Add(objPTColumns);
            }
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(userImportTableLog);
            model.Data = JSONString;
            return new JsonResult() { Data = model };
        }
        /// <summary>
        /// Check the User role type for dispaly the import type(product or family or user) in mapping attribute page
        /// </summary>
        /// <returns>return the login user role type</returns>

        #endregion

        #region Role Import
        /// <summary>
        /// insert and update the rolename values in tables
        /// if new rolename is added in the sheet it will be inserted otherwise it be updated
        /// </summary>
        /// <param name="importExcelSheetDDSelectionValue">temp import sheet name</param>
        /// <param name="excelPath">path of the temp import sheet</param>
        /// <param name="ImportType">(User or Role)</param>
        /// <param name="ImportFormat">(.xls format)</param>
        /// <returns>returns the insertedvalues,updated values,importing time,if successfully executed returns true</returns>
        [System.Web.Http.HttpPost]
        public string RoleImport(string importExcelSheetDDSelectionValue, string excelPath, string ImportType, string ImportFormat)
        {
            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                int customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                int roleType = 3;
                int insertValues = 0, updateValues = 0;
                string customer_name = string.Empty;
                var SuperAdminCheck = objLS.vw_aspnet_UsersInRoles
                       .Join(objLS.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                       .Join(objLS.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                       .Where(x => x.tcptps.tpf.UserName == User.Identity.Name).ToList();
                if (SuperAdminCheck.Any())
                {
                    var sd = SuperAdminCheck.FirstOrDefault();
                    if (sd.tcp.RoleType == 1)
                    {
                        roleType = 2;
                    }
                    else if (sd.tcp.RoleType == 2)
                    {
                        roleType = 3;

                    }
                }
                var customerList = objLS.Customers.Join(objLS.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name);
                var custlist = customerList.Select(x => new { x.a.CompanyName, x.a.CustomerId }).ToList();
                if (custlist.Any())
                {
                    customer_name = custlist.Select(a => a.CompanyName).FirstOrDefault().Replace(" ", "").Replace("_", "").ToString();
                }
                string[] importRole = { "RoleCatalog", "RoleGroup_name", "RoleAttribute" };
                string[] arrRoleName = new string[50];
                DataTable addRole = new DataTable();
                DataTable cpyAddRole = new DataTable();
                DataSet RoleCatalog = new DataSet();
                for (int i = 0; i < importRole.Length; i++)
                {
                    addRole = (ConvertExcelToDataTable(excelPath, importRole[i]));
                    cpyAddRole = addRole.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                    RoleCatalog.Tables.Add(cpyAddRole);
                }
                RoleCatalog.AcceptChanges();
                DataTable roleSheetCount = RoleImportSheetCount(excelPath, ImportType);
                if (roleSheetCount.Rows.Count != 0)
                {
                    RoleCatalog.Tables[2].Merge(roleSheetCount);
                }
                var CheckCurrentLoginRoleNameValue = CheckCurrentLoginRoleName(RoleCatalog);
                RoleCatalog = CheckCurrentLoginRoleNameValue.Item1;
                int skippedRoleNameCount = CheckCurrentLoginRoleNameValue.Item2;
                RoleCatalog = AddNewRole(RoleCatalog);
                string[] mantoryValueFields = { "ACTION_VIEW", "ACTION_MODIFY", "ACTION_ADD", "ACTION_REMOVE", "ACTION_DETACH" };
                DataColumnCollection roleFunctionColumns = RoleCatalog.Tables[1].Columns;
                for (int i = 0; i < mantoryValueFields.Length; i++)
                {
                    foreach (DataRow dr in RoleCatalog.Tables[1].Rows)
                    {
                        if (roleFunctionColumns.Contains(mantoryValueFields[i]))
                        {
                            string emptyVal = dr[mantoryValueFields[i]].ToString();
                            if (emptyVal == null || emptyVal == "")
                            {
                                dr[mantoryValueFields[i]] = "TRUE";
                            }
                        }
                    }
                }
                //new rolename is inserted  in the corresponding table
                foreach (DataTable dt in RoleCatalog.Tables)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string status = dr["STATUS"].ToString();
                        status = status.ToUpper();
                        if (status == "")
                        {
                            dr["STATUS"] = "update";
                        }
                    }
                }
                var countDistinctValues = RoleCatalog.Tables[0].AsEnumerable().Select(row => new { role_name = row.Field<string>("ROLE_NAME") }).ToArray().Distinct().ToList();
                foreach (var itr in countDistinctValues)
                {
                    var roleId = Guid.NewGuid().ToString();
                    string insertRoleName = itr.role_name;
                    var chkRoleName = objLS.aspnet_Roles.Where(x => x.RoleName == insertRoleName).Select(x => x.RoleName).FirstOrDefault();
                    if (chkRoleName == null)
                    {
                        insertValues = insertValues + 1;
                    }
                    else
                    {
                        updateValues = updateValues + 1;
                    }
                }

                foreach (DataTable dt in RoleCatalog.Tables)
                {
                    var distinctValues = dt.AsEnumerable().Select(row => new { role_name = row.Field<string>("ROLE_NAME") }).ToArray().Distinct().ToList();
                    foreach (var itr in distinctValues)
                    {
                        var roleId = Guid.NewGuid().ToString();
                        string insertRoleName = itr.role_name;
                        var chkRoleName = objLS.aspnet_Roles.Where(x => x.RoleName == insertRoleName).Select(x => x.RoleName).FirstOrDefault();
                        if (chkRoleName == null)
                        {
                            using (var con = new SqlConnection(connectionString))
                            {
                                con.Open();
                                var sqlCommand = new SqlCommand("STP_LS_ROLE_IMPORT", con);
                                sqlCommand.CommandType = CommandType.StoredProcedure;
                                sqlCommand.Parameters.Add("@ROLE_CATALOG_TABLE", RoleCatalog.Tables[0]);
                                sqlCommand.Parameters.Add("@ROLE_GROUPNAME_TABLE", RoleCatalog.Tables[1]);
                                sqlCommand.Parameters.Add("@ROLE_ATTRIBUTE_TABLE", RoleCatalog.Tables[2]);
                                sqlCommand.Parameters.Add("@CUSTOMERID", customerid);
                                sqlCommand.Parameters.Add("@OPTION", "ROLENAMEINSERT");
                                sqlCommand.Parameters.Add("@GUID", roleId);
                                sqlCommand.Parameters.Add("@ROLETYPE", roleType);
                                sqlCommand.Parameters.Add("@COMPANYNAME", customer_name);
                                sqlCommand.Parameters.Add("@ROLENAME", insertRoleName);
                                sqlCommand.ExecuteNonQuery();
                                con.Close();
                            }
                        }
                    }
                }

                RoleCatalog = valuesCheckRoleFunctionImport(RoleCatalog);
                // new Catalog values are inserted in the under new rolename
                if (!RoleCatalog.Tables[0].Rows.Count.Equals(0) && !RoleCatalog.Tables[1].Rows.Count.Equals(0) && !RoleCatalog.Tables[2].Rows.Count.Equals(0))
                {
                    using (var con = new SqlConnection(connectionString))
                    {
                        con.Open();
                        var sqlCommand = new SqlCommand("STP_LS_ROLE_IMPORT", con);
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@ROLE_CATALOG_TABLE", RoleCatalog.Tables[0]);
                        sqlCommand.Parameters.Add("@ROLE_GROUPNAME_TABLE", RoleCatalog.Tables[1]);
                        sqlCommand.Parameters.Add("@ROLE_ATTRIBUTE_TABLE", RoleCatalog.Tables[2]);
                        sqlCommand.Parameters.Add("@CUSTOMERID", customerid);
                        sqlCommand.Parameters.Add("@OPTION", "ROLEINSERT");
                        sqlCommand.ExecuteNonQuery();
                        con.Close();

                    }
                }
                bool chkReadOnlyAttributeImport = ReadOnlyAttributeImport(RoleCatalog, customerid);

                if (!chkReadOnlyAttributeImport == true)
                {
                    return "false";
                }
                sw.Stop();
                string calcualteImportTiminings = string.Format("{0}:{1}:{2}", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds);
                return "true" + "~" + insertValues + "~" + updateValues + "~" + calcualteImportTiminings + "~" + User.Identity.Name + "~" + skippedRoleNameCount;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at RoleImport ", ex);
                return null;
            }
        }


        /// <summary>
        /// if the attribute values under rolename is automatically created while created in the rolename
        /// if the user change the values yes or no these method used to update the values in corresponding
        /// </summary>
        /// <param name="roleAttribute">dataset table name</param>
        /// <param name="customerid">for checking the attribute value</param>
        /// <returns>returns true successfully exectued ortherwise returns false</returns>
        public bool ReadOnlyAttributeImport(DataSet roleAttribute, int customerid)
        {
            try
            {
                foreach (DataRow dr in roleAttribute.Tables[2].Rows)
                {
                    string attributeName = dr["ATTRIBUTE_NAME"].ToString();
                    string roleName = dr["ROLE_NAME"].ToString();
                    var roleID = objLS.TB_ROLE.Where(x => x.ROLE_NAME == roleName).Select(x => x.ROLE_ID).FirstOrDefault();
                    var attributeID = objLS.CUSTOMERATTRIBUTE.Join(objLS.TB_ATTRIBUTE, ca => ca.ATTRIBUTE_ID, ta => ta.ATTRIBUTE_ID, (ca, ta) => new { ca, ta })
                        .Where(x => x.ta.ATTRIBUTE_NAME == attributeName && x.ta.CREATED_USER == User.Identity.Name && x.ca.CUSTOMER_ID == customerid)
                        .Select(x => x.ca.ATTRIBUTE_ID).FirstOrDefault();

                    if (attributeID == 0)
                    {
                        attributeID = objLS.TB_ATTRIBUTE.Join(objLS.CUSTOMERATTRIBUTE, ta => ta.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (ta, ca) => new { ta, ca })
                            .Where(x => x.ca.CUSTOMER_ID == customerid && x.ta.ATTRIBUTE_NAME == attributeName).Select(x => x.ta.ATTRIBUTE_ID).FirstOrDefault();
                    }
                    var chkReadOnlyAtrribute = objLS.TB_READONLY_ATTRIBUTES.Where(x => x.ATTRIBUTE_ID == attributeID && x.ROLE_ID == roleID).Select(x => x.ROLE_ID).FirstOrDefault();
                    string chkAllowedit = dr["Allow Edit"].ToString();
                    chkAllowedit = chkAllowedit.ToUpper();
                    if (chkReadOnlyAtrribute == null)
                    {
                        chkReadOnlyAtrribute = 0;
                    }
                    if (chkAllowedit.Equals("NO"))
                    {
                        if (chkReadOnlyAtrribute == 0)
                        {
                            using (var con = new SqlConnection(connectionString))
                            {
                                con.Open();
                                var sqlCommand = new SqlCommand("STP_LS_ROLE_IMPORT", con);
                                sqlCommand.CommandType = CommandType.StoredProcedure;
                                sqlCommand.Parameters.Add("@ROLE_CATALOG_TABLE", roleAttribute.Tables[0]);
                                sqlCommand.Parameters.Add("@ROLE_GROUPNAME_TABLE", roleAttribute.Tables[1]);
                                sqlCommand.Parameters.Add("@ROLE_ATTRIBUTE_TABLE", roleAttribute.Tables[2]);
                                sqlCommand.Parameters.Add("@CUSTOMERID", customerid);
                                sqlCommand.Parameters.Add("@ROLENAME", roleName);
                                sqlCommand.Parameters.Add("@ATTRIBUTEID", attributeID);
                                sqlCommand.Parameters.Add("@USER", User.Identity.Name);
                                sqlCommand.Parameters.Add("@OPTION", "READONLYATTRIBUTE");
                                sqlCommand.ExecuteNonQuery();
                                con.Close();

                            }
                        }

                    }
                    else if (chkAllowedit.Equals("YES"))
                    {
                        if (chkReadOnlyAtrribute != 0)
                        {
                            using (var con = new SqlConnection(connectionString))
                            {
                                con.Open();
                                var sqlCommand = new SqlCommand("STP_LS_ROLE_IMPORT", con);
                                sqlCommand.CommandType = CommandType.StoredProcedure;
                                sqlCommand.Parameters.Add("@ROLE_CATALOG_TABLE", roleAttribute.Tables[0]);
                                sqlCommand.Parameters.Add("@ROLE_GROUPNAME_TABLE", roleAttribute.Tables[1]);
                                sqlCommand.Parameters.Add("@ROLE_ATTRIBUTE_TABLE", roleAttribute.Tables[2]);
                                sqlCommand.Parameters.Add("@CUSTOMERID", customerid);
                                sqlCommand.Parameters.Add("@ROLENAME", roleName);
                                sqlCommand.Parameters.Add("@ATTRIBUTEID", attributeID);
                                sqlCommand.Parameters.Add("@USER", User.Identity.Name);
                                sqlCommand.Parameters.Add("@OPTION", "DELETEREADONLYATTRIBUTE");
                                sqlCommand.ExecuteNonQuery();
                                con.Close();

                            }
                        }
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ReadOnlyAttributeImport ", ex);
                return false;
            }

        }
        /// <summary>
        /// for changing the action  column values
        /// </summary>
        /// <param name="roleAttribute">dataset values</param>
        /// <returns>return the dataset value</returns>
        public DataSet valuesCheckRoleFunctionImport(DataSet roleAttribute)
        {
            try
            {
                foreach (DataRow dr in roleAttribute.Tables[1].Rows)
                {
                    string chkActionView = dr["ACTION_VIEW"].ToString();
                    chkActionView = chkActionView.ToUpper();
                    string chkActionModify = dr["ACTION_MODIFY"].ToString();
                    chkActionModify = chkActionModify.ToUpper();
                    string chkActionAdd = dr["ACTION_ADD"].ToString();
                    chkActionAdd = chkActionAdd.ToUpper();
                    string chkActionRemove = dr["ACTION_REMOVE"].ToString();
                    chkActionRemove = chkActionRemove.ToUpper();
                    if (chkActionView.Equals("FALSE"))
                    {
                        dr["ACTION_MODIFY"] = "FALSE";
                        dr["ACTION_ADD"] = "FALSE";
                        dr["ACTION_REMOVE"] = "FALSE";
                        dr["ACTION_DETACH"] = "FALSE";
                    }
                    else if (chkActionModify.Equals("FALSE"))
                    {
                        dr["ACTION_ADD"] = "FALSE";
                    }
                    else if (chkActionModify.Equals("TRUE"))
                    {
                        dr["ACTION_VIEW"] = "TRUE";
                    }
                    else if (chkActionAdd.Equals("TRUE"))
                    {
                        dr["ACTION_MODIFY"] = "TRUE";
                        dr["ACTION_VIEW"] = "TRUE";
                    }
                    else if (chkActionRemove.Equals("TRUE"))
                    {
                        dr["ACTION_VIEW"] = "TRUE";
                    }
                    roleAttribute.Tables[1].AcceptChanges();
                }
                roleAttribute.Tables[1].AcceptChanges();
                roleAttribute.AcceptChanges();
                return roleAttribute;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at valuesCheckRoleFunctionImport ", ex);
                return null;
            }
        }

        /// <summary>
        /// validate the rolename values whether if is true or false
        /// validate the manitory column name values
        /// to check the empty row or not
        /// </summary>
        /// <param name="importExcelSheetDDSelectionValue">temp import sheet name</param>
        /// <param name="excelPath">path of the temp import sheet</param>
        /// <param name="ImportType">(User or Role)</param>
        /// <param name="ImportFormat">(.xls format)</param>
        /// <returns>values are stord in the session for show the view log</returns>
        [System.Web.Http.HttpPost]
        public string ValidateRoleImport(string importExcelSheetDDSelectionValue, string excelPath, string ImportType, string ImportFormat)
        {
            try
            {
                string[] importRole = { "RoleCatalog", "RoleGroup_name", "RoleAttribute" };
                int missingValues = 0; int missingColumn = 0;
                DataTable addRole = new DataTable();
                DataTable copyAddRole = new DataTable();
                DataSet RoleCatalog = new DataSet();
                for (int i = 0; i < importRole.Length; i++)
                {
                    addRole = ConvertExcelToDataTable(excelPath, importRole[i]);
                    copyAddRole = addRole.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                    RoleCatalog.Tables.Add(copyAddRole);
                }
                RoleCatalog.AcceptChanges();
                DataTable roleSheetCount = RoleImportSheetCount(excelPath, ImportType);
                if (roleSheetCount.Rows.Count != 0)
                {
                    RoleCatalog.Tables[2].Merge(roleSheetCount);
                }
                var validationSheetColumnNamesResult = CheckColumnsNamesRoleImport(RoleCatalog);
                RoleCatalog = validationSheetColumnNamesResult.Item1;
                if ((validationSheetColumnNamesResult.Item2).Equals("fail"))
                {
                    return "fail Column name validation";
                }
                RoleCatalog = ValidateRoleGroup(RoleCatalog);
                RoleCatalog = ValidateRoleCatalog(RoleCatalog);
                RoleCatalog = ValidateRoleAttribute(RoleCatalog);

                System.Web.HttpContext.Current.Session["RoleImportTableLog"] = RoleCatalog;
                System.Web.HttpContext.Current.Session["ExportErrorTable"] = RoleCatalog.Tables[0];

                int missingRoleValues = (int)System.Web.HttpContext.Current.Session["calculateMissingRoleValues"];
                int missingRoleColumn = (int)System.Web.HttpContext.Current.Session["calculateMissingRoleColumn"];
                int roleCatalogMissingValues = (int)System.Web.HttpContext.Current.Session["calculateRoleCatalogMissingValues"];
                int roleCatalogMissingColumn = (int)System.Web.HttpContext.Current.Session["calculateRoleCatalogMissingColumn"];
                int calculateRoleDuplicateValues = (int)System.Web.HttpContext.Current.Session["calculateRoleDuplicateValues"];
                int calculateRoleGroupNameDuplicateValues = (int)System.Web.HttpContext.Current.Session["calculateRoleGroupNameDuplicateValues"];
                int duplicateValues = calculateRoleDuplicateValues + calculateRoleGroupNameDuplicateValues;

                missingValues = missingRoleValues + roleCatalogMissingValues;
                missingColumn = missingRoleColumn + roleCatalogMissingColumn;
                return "true" + "~" + missingValues + "~" + missingColumn + "~" + duplicateValues;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ValidateRoleImport ", ex);
                return "fasle";
            }

        }
        /// <summary>
        /// to take the values in session and datatable values is converted into model data.
        /// </summary>
        /// <returns>return the model values</returns>
        [System.Web.Http.HttpPost]
        public JsonResult RoleViewLog()
        {
            try
            {
                DashBoardModel model = new DashBoardModel();
                var RoleImportTableLog = (DataSet)System.Web.HttpContext.Current.Session["RoleImportTableLog"];
                foreach (DataColumn columns in RoleImportTableLog.Tables[0].Columns)
                {
                    var objPTColumns = new PTColumns();
                    objPTColumns.Caption = columns.Caption;
                    objPTColumns.ColumnName = columns.ColumnName;
                    model.Columns.Add(objPTColumns);
                }
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(RoleImportTableLog.Tables[0]);
                model.Data = JSONString;
                return new JsonResult() { Data = model };
            }
            catch (Exception ex)
            {
                _logger.Error("Error at RoleViewLog ", ex);
                return null;
            }

        }

        /// <summary>
        /// changing the error log  dynamically get the error log values
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public JsonResult ChangeRoleViewLog(int value)
        {
            try
            {
                DashBoardModel model = new DashBoardModel();
                var RoleImportTableLog = (DataSet)System.Web.HttpContext.Current.Session["RoleImportTableLog"];
                System.Web.HttpContext.Current.Session["ExportErrorTable"] = RoleImportTableLog.Tables[value];
                foreach (DataColumn columns in RoleImportTableLog.Tables[value].Columns)
                {
                    var objPTColumns = new PTColumns();
                    objPTColumns.Caption = columns.Caption;
                    objPTColumns.ColumnName = columns.ColumnName;
                    model.Columns.Add(objPTColumns);
                }
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(RoleImportTableLog.Tables[value]);
                model.Data = JSONString;
                return new JsonResult() { Data = model };
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ChangeRoleViewLog ", ex);
                return null;
            }

        }
        /// <summary>
        /// Check the user has given correct sheet for User and Role Import type
        /// </summary>
        /// <param name="importExcelSheetDDSelectionValue">sheet name</param>
        /// <param name="excelPath">path of the sheet stored in server</param>
        /// <returns>Succesfully executed returns table count ortherwise returns false</returns>
        [System.Web.Http.HttpPost]
        public string SheetValidation(string excelPath, string importtype)
        {
            try
            {
                DataTable sheetValue = new DataTable();
                DataTable checkSheet = new DataTable();
                importtype = importtype.ToUpper();
                int i = 0;
                DataTable dtResult = null;
                int totalSheet = 0;
                using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();
                    DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = string.Empty;
                    if (dt != null)
                    {
                        var tempDataTable = (from dataRow in dt.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();
                        dt = tempDataTable;
                        totalSheet = dt.Rows.Count;

                    }
                    String[] excelSheets = new String[dt.Rows.Count];
                    foreach (DataRow dr in dt.Rows)
                    {
                        string tableName = dr["TABLE_NAME"].ToString();
                        tableName = tableName.Replace(@"$", string.Empty);
                        excelSheets[i] = tableName;
                        i++;
                    }
                    if (excelSheets.Length == 1 && importtype == "USER")
                    {
                        if (excelSheets[0] != "UserExport")
                        {
                            return "invalid sheet name";
                        }
                        else
                        {
                            sheetValue = ConvertExcelToDataTable(excelPath, "UserExport");
                            if (sheetValue.Rows.Count == 0)
                            {
                                return "empty value";
                            }
                            else
                            {
                                checkSheet = sheetValue.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                                if (checkSheet.Rows.Count == 0)
                                {
                                    return "empty value";
                                }
                            }
                        }
                    }
                    else if (excelSheets.Length == 3 && importtype == "ROLE")
                    {
                        if (excelSheets.Contains("RoleCatalog") && excelSheets.Contains("RoleGroup_name") && excelSheets.Contains("RoleAttribute"))
                        {
                            sheetValue = ConvertExcelToDataTable(excelPath, "RoleCatalog");
                            if (sheetValue.Rows.Count == 0)
                            {
                                return "empty value";
                            }
                            else
                            {
                                checkSheet = sheetValue.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                                if (checkSheet.Rows.Count == 0)
                                {
                                    return "empty value";
                                }
                            }
                        }
                        else
                        {
                            return "invalid sheet name";
                        }
                    }
                    else if (excelSheets.Length > 3 && importtype == "ROLE")
                    {
                        if (excelSheets.Contains("RoleCatalog") && excelSheets.Contains("RoleGroup_name") && excelSheets.Contains("RoleAttribute"))
                        {
                            sheetValue = ConvertExcelToDataTable(excelPath, "RoleCatalog");
                            if (sheetValue.Rows.Count == 0)
                            {
                                return "empty value";
                            }
                            else
                            {
                                checkSheet = sheetValue.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                                if (checkSheet.Rows.Count == 0)
                                {
                                    return "empty value";
                                }
                                else
                                {
                                    return "true";
                                }
                            }
                        }
                        else
                        {
                            return "invalid sheet name";
                        }
                    }
                    return (excelSheets.Length).ToString();
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ChangeRoleViewLog ", ex);
                return "false";
            }
        }

        public DataTable RoleImportSheetCount(string excelPath, string importtype)
        {
            try
            {
                DataTable sheetValue = new DataTable();
                DataTable checkSheet = new DataTable();
                DataTable attributeSheet = new DataTable();
                DataTable CombineRoleAttribute = new DataTable();
                importtype = importtype.ToUpper();
                DataTable dtResult = null;
                int totalSheet = 0;
                using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();
                    int i = 0;
                    DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = string.Empty;
                    if (dt != null)
                    {
                        var tempDataTable = (from dataRow in dt.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();
                        dt = tempDataTable;
                        totalSheet = dt.Rows.Count;

                    }
                    String[] excelSheets = new String[dt.Rows.Count];
                    foreach (DataRow dr in dt.Rows)
                    {
                        string tableName = dr["TABLE_NAME"].ToString();
                        tableName = tableName.Replace(@"$", string.Empty);
                        excelSheets[i] = tableName;
                        i++;
                    }
                    string incRoleAttributeValue = "RoleAttribute";
                    int inc = 1;
                    for (int j = 0; j < excelSheets.Length; j++)
                    {
                        if (excelSheets[j].Contains("RoleAttribute"))
                        {
                            if (excelSheets[j] != "RoleAttribute" && excelSheets[j] != "RoleCatalog" && excelSheets[j] != "RoleGroup_name")
                            {
                                incRoleAttributeValue = incRoleAttributeValue + inc;
                                if (excelSheets[j].Equals(incRoleAttributeValue))
                                {
                                    attributeSheet = (ConvertExcelToDataTable(excelPath, incRoleAttributeValue)).Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                                    inc++;
                                }
                                if (CombineRoleAttribute.Rows.Count == 0)
                                {
                                    CombineRoleAttribute = attributeSheet;
                                }
                                else
                                {
                                    CombineRoleAttribute.Merge(attributeSheet);
                                }
                            }
                        }
                    }

                }
                return CombineRoleAttribute;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at RoleImportSheetCount ", ex);
                return null;
            }
        }

        public DataSet ValidateRoleGroup(DataSet RoleCatalog)
        {
            try
            {
                string[] roleFunctionColumnValues = { "ROLE_NAME", "FUNCTION_NAME", "FUNCTION_GROUP_NAME" };
                string[] boolRoleFunctionColumnValues = { "ACTION_VIEW", "ACTION_MODIFY", "ACTION_ADD", "ACTION_REMOVE", "ACTION_DETACH" };
                string[] mandatoryRoleFunctions = { "ROLE_NAME", "FUNCTION_NAME", "ACTION_VIEW", "ACTION_MODIFY", "ACTION_ADD", "ACTION_REMOVE", "ACTION_DETACH", "FUNCTION_GROUP_NAME" };
                string functionErrorMissingColumn = string.Empty;
                int roleMissingValues = 0;
                int roleMissingColumn = 0;
                int roleDuplicateValues = 0;
                DataColumnCollection roleFunctionColumns = RoleCatalog.Tables[1].Columns;
                RoleCatalog.Tables[1].Columns.Add("Missing Values");

                //Check the empty row in roleFunctionColumnValues columns
                for (int i = 0; i < roleFunctionColumnValues.Length; i++)
                {
                    if (roleFunctionColumns.Contains(roleFunctionColumnValues[i]))
                    {
                        foreach (DataRow dr in RoleCatalog.Tables[1].Rows)
                        {
                            string chkValues = dr[roleFunctionColumnValues[i]].ToString();
                            if (chkValues == "")
                            {
                                roleMissingValues++;
                                string writeMisssingValues = dr["Missing Values"].ToString();
                                if (writeMisssingValues != "")
                                {
                                    writeMisssingValues = writeMisssingValues + "|";
                                }
                                dr["Missing Values"] = writeMisssingValues + "" + roleFunctionColumnValues[i] + " " + " has no values";
                                RoleCatalog.Tables[1].AcceptChanges();
                            }

                        }
                    }
                }
                RoleCatalog.Tables[1].AcceptChanges();

                //Check the True or False values in the boolRoleFunctionColumnValues columns
                for (int i = 0; i < boolRoleFunctionColumnValues.Length; i++)
                {
                    if (roleFunctionColumns.Contains(boolRoleFunctionColumnValues[i]))
                    {
                        foreach (DataRow dr in RoleCatalog.Tables[1].Rows)
                        {
                            string chkValues = dr[boolRoleFunctionColumnValues[i]].ToString();
                            chkValues = chkValues.ToUpper();
                            if (chkValues.Equals("TRUE") || chkValues.Equals("FALSE"))
                            {
                            }
                            else
                            {
                                roleMissingValues++;
                                string writeMisssingValues = dr["Missing Values"].ToString();
                                if (writeMisssingValues != "")
                                {
                                    writeMisssingValues = writeMisssingValues + "|";
                                }
                                dr["Missing Values"] = writeMisssingValues + "" + boolRoleFunctionColumnValues[i] + " " + " has no values";
                                RoleCatalog.Tables[1].AcceptChanges();
                            }
                        }
                    }
                }
                RoleCatalog.Tables[1].AcceptChanges();

                //Check the mandatory columns in  mandatoryRoleFunctions
                RoleCatalog.Tables[1].Columns.Add("Missing Column", typeof(string));
                for (int i = 0; i < mandatoryRoleFunctions.Length; i++)
                {
                    if (!roleFunctionColumns.Contains(mandatoryRoleFunctions[i]))
                    {
                        if (roleMissingColumn == 0)
                        {
                            functionErrorMissingColumn = functionErrorMissingColumn + mandatoryRoleFunctions[i];
                            roleMissingColumn++;
                        }
                        else
                        {
                            functionErrorMissingColumn = functionErrorMissingColumn + "~" + mandatoryRoleFunctions[i];
                            roleMissingColumn++;
                        }

                    }
                    RoleCatalog.AcceptChanges();
                }
                foreach (DataRow dr in RoleCatalog.Tables[1].Rows)
                {
                    dr["Missing Column"] = functionErrorMissingColumn;
                    break;
                }

                foreach (DataRow checkFuctionName in RoleCatalog.Tables[1].Rows)
                {
                    string functionGroupName = checkFuctionName["FUNCTION_GROUP_NAME"].ToString();
                    string vaildFunctionName = checkFuctionName["FUNCTION_NAME"].ToString();
                    string[] validFunctionName = new string[100]; int inc = 0;
                    if (functionGroupName != "" && vaildFunctionName != "")
                    {
                        var functionGroupId = objLS.TB_FUNCTIONGROUP.Where(x => x.FUNCTION_GROUP_NAME == functionGroupName).Select(x => x.FUNCTION_GROUP_ID).FirstOrDefault();
                        if (functionGroupId != 0)
                        {
                            var functionName = objLS.TB_FUNCTION.Where(x => x.FUNCTION_GROUP_ID == functionGroupId && x.FUNCTION_NAME == vaildFunctionName).Select(x => x.FUNCTION_NAME).FirstOrDefault();
                            if (functionName == null)
                            {
                                roleMissingValues++;
                                checkFuctionName["Missing Values"] = "Please Enter valid FUNCTION_GROUP_NAME";
                            }
                        }
                        else
                        {
                            roleMissingValues++;
                            checkFuctionName["Missing Values"] = "Please Enter valid FUNCTION_GROUP_NAME";
                        }
                    }
                }
                var duplicatesFunctionValues = RoleCatalog.Tables[1].AsEnumerable().GroupBy(dr => new { FUNCTION_NAME = dr.Field<string>("FUNCTION_NAME"), ROLE_NAME = dr.Field<string>("ROLE_NAME"), FUNCTION_GROUP_NAME = dr.Field<string>("FUNCTION_GROUP_NAME") }).Where(g => g.Count() > 1).Select(g => g.First()).ToList();
                roleDuplicateValues = duplicatesFunctionValues.Count;
                foreach (var itr in duplicatesFunctionValues)
                {
                    string duplicateFuctionName = itr[2].ToString();
                    string duplicateRoleName = itr[1].ToString();
                    string duplicateFuctionGroupName = itr[8].ToString();
                    foreach (DataRow dr in RoleCatalog.Tables[1].Rows)
                    {
                        string checkduplicateFuctionName = dr["FUNCTION_NAME"].ToString();
                        string checkduplicateRoleName = dr["ROLE_NAME"].ToString();
                        string checkduplicateFuctionGroupName = dr["FUNCTION_GROUP_NAME"].ToString();
                        if (duplicateRoleName.Equals(checkduplicateRoleName) && checkduplicateFuctionName.Equals(duplicateFuctionName) && checkduplicateFuctionGroupName.Equals(duplicateFuctionGroupName))
                        {
                            string writeMisssingValues = dr["Missing Values"].ToString();
                            if (writeMisssingValues != "")
                            {
                                writeMisssingValues = writeMisssingValues + "|";
                            }
                            dr["Missing Values"] = writeMisssingValues + "Duplicate Values";
                        }
                    }
                }
                RoleCatalog.AcceptChanges();

                System.Web.HttpContext.Current.Session["calculateRoleGroupNameDuplicateValues"] = roleDuplicateValues;
                System.Web.HttpContext.Current.Session["calculateMissingRoleValues"] = roleMissingValues;
                System.Web.HttpContext.Current.Session["calculateMissingRoleColumn"] = roleMissingColumn;
                return RoleCatalog;

            }
            catch (Exception ex)
            {
                _logger.Error("Error at ValidateRoleGroup ", ex);
                return null;
            }


        }

        public DataSet ValidateRoleCatalog(DataSet RoleCatalog)
        {
            try
            {
                string[] roleCatalogColumnsValues = { "ROLE_NAME", "CATALOG_NAME" };
                string[] mandatoryRoleCatalog = { "ROLE_NAME", "CATALOG_NAME", "IsActive", "IsRead" };
                string catalogErrorMissingColumn = string.Empty;
                int roleCatalogMissingValues = 0; int roleCatalogMissingColumn = 0;
                int customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                DataColumnCollection roleCatalogColumns = RoleCatalog.Tables[0].Columns;
                RoleCatalog.Tables[0].Columns.Add("Missing Values");

                //Check the missing values in the RoleCatalog Table in the cloumns of roleCatalogColumnsValues
                for (int i = 0; i < roleCatalogColumnsValues.Length; i++)
                {
                    if (roleCatalogColumns.Contains(roleCatalogColumnsValues[i]))
                    {
                        foreach (DataRow dr in RoleCatalog.Tables[0].Rows)
                        {
                            string chkValues = dr[roleCatalogColumnsValues[i]].ToString();
                            if (chkValues == "")
                            {
                                roleCatalogMissingValues = roleCatalogMissingValues + 1;
                                string writeMisssingValues = dr["Missing Values"].ToString();
                                if (writeMisssingValues != "")
                                {
                                    writeMisssingValues = writeMisssingValues + "|";
                                }
                                dr["Missing Values"] = writeMisssingValues + "" + roleCatalogColumnsValues[i] + " " + " has no values";
                                RoleCatalog.Tables[0].AcceptChanges();
                            }

                        }
                    }
                }
                RoleCatalog.Tables[0].AcceptChanges();

                //Check the RoleCatalog table ISACTIVE column has only true or false value 
                if (roleCatalogColumns.Contains("IsActive"))
                {
                    foreach (DataRow dr in RoleCatalog.Tables[0].Rows)
                    {
                        string chkValues = dr["IsActive"].ToString();
                        if (chkValues != " ")
                        {
                            chkValues = chkValues.ToUpper();
                            if (chkValues.Equals("TRUE") || chkValues.Equals("FALSE"))
                            {
                            }
                            else
                            {
                                if (chkValues == null || chkValues == "")
                                { }
                                else
                                {
                                    string writeMisssingValues = dr["Missing Values"].ToString();
                                    if (writeMisssingValues != "")
                                    {
                                        writeMisssingValues = writeMisssingValues + "|";
                                    }
                                    roleCatalogMissingValues = roleCatalogMissingValues + 1;
                                    dr["Missing Values"] = writeMisssingValues + "Please Enter the IsActive Column name is True or False";
                                    RoleCatalog.Tables[0].AcceptChanges();
                                }
                            }
                        }
                    }
                }

                //Check the RoleCatalog table ISREAD column has only true or false value
                if (roleCatalogColumns.Contains("IsRead"))
                {
                    foreach (DataRow dr in RoleCatalog.Tables[0].Rows)
                    {
                        string chkValues = dr["IsRead"].ToString();
                        if (chkValues != " ")
                        {
                            chkValues = chkValues.ToUpper();
                            if (chkValues.Equals("TRUE") || chkValues.Equals("FALSE"))
                            { }
                            else
                            {
                                if (chkValues == null || chkValues == "")
                                { }
                                else
                                {
                                    roleCatalogMissingValues = roleCatalogMissingValues + 1;
                                    string writeMisssingValues = dr["Missing Values"].ToString();
                                    if (writeMisssingValues != "")
                                    {
                                        writeMisssingValues = writeMisssingValues + "|";
                                    }
                                    dr["Missing Values"] = writeMisssingValues + "Please Enter the IsRead name is True or False";
                                    RoleCatalog.Tables[0].AcceptChanges();
                                }
                            }
                        }
                    }
                }
                RoleCatalog.Tables[0].AcceptChanges();

                //Check the mandatory Columns in the RoleCatalog Table 
                RoleCatalog.Tables[0].Columns.Add("Missing Column", typeof(string));
                for (int i = 0; i < mandatoryRoleCatalog.Length; i++)
                {
                    if (!roleCatalogColumns.Contains(mandatoryRoleCatalog[i]))
                    {
                        if (roleCatalogMissingColumn == 0)
                        {
                            catalogErrorMissingColumn = mandatoryRoleCatalog[i];
                            roleCatalogMissingColumn = roleCatalogMissingColumn + 1;
                        }
                        else
                        {
                            catalogErrorMissingColumn = catalogErrorMissingColumn + "~" + mandatoryRoleCatalog[i];
                            roleCatalogMissingColumn = roleCatalogMissingColumn + 1;
                        }

                    }
                    RoleCatalog.AcceptChanges();
                }
                foreach (DataRow dr in RoleCatalog.Tables[0].Rows)
                {
                    dr["Missing Column"] = catalogErrorMissingColumn;
                    break;
                }

                //Duplicate Value columns
                var duplicatesCatalogValues = RoleCatalog.Tables[0].AsEnumerable().GroupBy(dr => new { CATALOG_NAME = dr.Field<string>("CATALOG_NAME"), ROLE_NAME = dr.Field<string>("ROLE_NAME") }).Where(g => g.Count() > 1).Select(g => g.First()).ToList();
                int roleDuplicateValues = duplicatesCatalogValues.Count;
                foreach (var itr in duplicatesCatalogValues)
                {
                    string duplicateRoleName = itr[1].ToString();
                    string duplicateCatalogName = itr[2].ToString();
                    foreach (DataRow dr in RoleCatalog.Tables[0].Rows)
                    {
                        string checkDuplicateRoleName = dr["ROLE_NAME"].ToString();
                        string checkDuplicateCatalogName = dr["CATALOG_NAME"].ToString();
                        if (duplicateRoleName.Equals(checkDuplicateRoleName) && duplicateCatalogName.Equals(checkDuplicateCatalogName))
                        {
                            string writeMisssingValues = dr["Missing Values"].ToString();
                            if (writeMisssingValues != "")
                            {
                                writeMisssingValues = writeMisssingValues + "|";
                            }
                            dr["Missing Values"] = writeMisssingValues + "Duplicate Values";
                            RoleCatalog.Tables[0].AcceptChanges();
                        }
                    }
                }

                //Check the given Catalog values in the table is provided under the user or not
                foreach (DataRow catalogValues in RoleCatalog.Tables[0].Rows)
                {
                    string catalogName = catalogValues["CATALOG_NAME"].ToString();
                    if (catalogName != null && catalogName != " ")
                    {
                        //var catalogId = objLS.TB_CATALOG.Where(x => x.CATALOG_NAME == catalogName && x.CREATED_USER == User.Identity.Name).Select(x => x.CATALOG_ID).FirstOrDefault();

                        var catalogId = objLS.TB_CATALOG.Join(objLS.Customer_Catalog, tc => tc.CATALOG_ID, cc => cc.CATALOG_ID, (tc, cc) => new { tc, cc }).Where(x => x.tc.CATALOG_NAME == catalogName && x.cc.CustomerId == customerid).Select(x => x.cc.CATALOG_ID).FirstOrDefault();

                        if (catalogId != 0)
                        {
                            var vaildCatalogName = objLS.Customer_Catalog.Where(x => x.CATALOG_ID == catalogId && x.CustomerId == customerid).Select(x => x.CATALOG_ID).FirstOrDefault();
                            if (vaildCatalogName == null)
                            {
                                roleCatalogMissingValues = roleCatalogMissingValues + 1;
                                string writeMisssingValues = catalogValues["Missing Values"].ToString();
                                if (writeMisssingValues != "")
                                {
                                    writeMisssingValues = writeMisssingValues + "|";
                                }
                                catalogValues["Missing Values"] = writeMisssingValues + "These Catalog Name is not provided under the customer";
                            }
                        }
                        else
                        {
                            roleCatalogMissingValues = roleCatalogMissingValues + 1;
                            string writeMisssingValues = catalogValues["Missing Values"].ToString();
                            if (writeMisssingValues != "")
                            {
                                writeMisssingValues = writeMisssingValues + "|";
                            }
                            catalogValues["Missing Values"] = writeMisssingValues + "Please Enter the valid Catalog Name";
                        }

                    }

                }
                RoleCatalog.AcceptChanges();
                System.Web.HttpContext.Current.Session["calculateRoleDuplicateValues"] = roleDuplicateValues;
                System.Web.HttpContext.Current.Session["calculateRoleCatalogMissingValues"] = roleCatalogMissingValues;
                System.Web.HttpContext.Current.Session["calculateRoleCatalogMissingColumn"] = roleCatalogMissingColumn;
                return RoleCatalog;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ValidateRoleCatalog ", ex);
                return null;
            }
        }

        public DataSet ValidateRoleAttribute(DataSet RoleCatalog)
        {
            try
            {
                string[] roleAttributeColumnValues = { "ROLE_NAME", "ATTRIBUTE_NAME", "Allow Edit" };
                string[] mandatoryRoleAttribute = { "ROLE_NAME", "ATTRIBUTE_NAME", "Allow Edit" };
                string AttributeErrorMissingColumn = string.Empty;
                int roleAttributeMissingValues = 0; int roleAttributeMissingColumn = 0;
                int customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                DataColumnCollection roleAttributeColumns = RoleCatalog.Tables[2].Columns;
                RoleCatalog.Tables[2].Columns.Add("Missing Values");

                //Check missing values in the RoleAttribute Table  
                for (int i = 0; i < roleAttributeColumnValues.Length; i++)
                {
                    if (roleAttributeColumns.Contains(roleAttributeColumnValues[i]))
                    {
                        foreach (DataRow dr in RoleCatalog.Tables[2].Rows)
                        {
                            string chkValues = dr[roleAttributeColumnValues[i]].ToString();
                            if (chkValues == "")
                            {
                                roleAttributeMissingValues = roleAttributeMissingValues + 1;
                                string writeMisssingValues = dr["Missing Values"].ToString();
                                if (writeMisssingValues != "")
                                {
                                    writeMisssingValues = writeMisssingValues + "|";
                                }
                                dr["Missing Values"] = writeMisssingValues + "" + roleAttributeColumnValues[i] + " " + " has no values";
                                RoleCatalog.Tables[2].AcceptChanges();
                            }
                        }
                    }
                }
                RoleCatalog.Tables[2].AcceptChanges();

                //Check the AllowEdit column values has yes or no in the RoleAttribute Table
                if (roleAttributeColumns.Contains("Allow Edit"))
                {
                    foreach (DataRow dr in RoleCatalog.Tables[2].Rows)
                    {
                        string chkValues = dr["Allow Edit"].ToString();
                        if (chkValues != " ")
                        {
                            chkValues = chkValues.ToUpper();
                            if (chkValues.Equals("YES") || chkValues.Equals("NO"))
                            { }
                            else
                            {
                                if (chkValues == null || chkValues == "")
                                { }
                                else
                                {
                                    roleAttributeMissingValues = roleAttributeMissingValues + 1;
                                    string writeMisssingValues = dr["Missing Values"].ToString();
                                    if (writeMisssingValues != "")
                                    {
                                        writeMisssingValues = writeMisssingValues + "|";
                                    }
                                    dr["Missing Values"] = writeMisssingValues + "Please Enter the Allow Edit name is Yes or No";
                                    RoleCatalog.Tables[2].AcceptChanges();
                                }
                            }
                        }
                    }
                }
                RoleCatalog.Tables[2].AcceptChanges();
                RoleCatalog.Tables[2].Columns.Add("Missing Column", typeof(string));

                //Check the Mandatory Column in the Role Attribute Table
                for (int i = 0; i < mandatoryRoleAttribute.Length; i++)
                {
                    if (!roleAttributeColumns.Contains(mandatoryRoleAttribute[i]))
                    {
                        if (roleAttributeMissingColumn == 0)
                        {
                            AttributeErrorMissingColumn = mandatoryRoleAttribute[i];
                            roleAttributeMissingColumn = roleAttributeMissingColumn + 1;
                        }
                        else
                        {
                            AttributeErrorMissingColumn = AttributeErrorMissingColumn + "~" + mandatoryRoleAttribute[i];
                            roleAttributeMissingColumn = roleAttributeMissingColumn + 1;
                        }

                    }
                    RoleCatalog.AcceptChanges();
                }

                foreach (DataRow dr in RoleCatalog.Tables[2].Rows)
                {
                    dr["Missing Column"] = AttributeErrorMissingColumn;
                    break;
                }

                foreach (DataRow roleAttribute in RoleCatalog.Tables[2].Rows)
                {
                    string attributeName = roleAttribute["ATTRIBUTE_NAME"].ToString();
                    if (attributeName != "")
                    {
                        int inc = 0; int count = 0;
                        var attributeId = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == attributeName).Select(x => x.ATTRIBUTE_ID).ToList();
                        if (attributeId.Count == 0)
                        {
                            roleAttributeMissingValues = roleAttributeMissingValues + 1;
                            string writeMisssingValues = roleAttribute["Missing Values"].ToString();
                            if (writeMisssingValues != "")
                            {
                                writeMisssingValues = writeMisssingValues + "|";
                            }
                            roleAttribute["Missing Values"] = writeMisssingValues + "Please Enter the valid Attribute Name";
                        }
                        else
                        {
                            foreach (var itr in attributeId)
                            {
                                inc = objLS.CUSTOMERATTRIBUTE.Where(x => x.ATTRIBUTE_ID == itr && x.CUSTOMER_ID == customerid).Select(x => x.ATTRIBUTE_ID).FirstOrDefault();
                                if (inc != 0)
                                {
                                    count = count + 1;
                                }
                            }
                            if (count == 0)
                            {
                                roleAttributeMissingValues = roleAttributeMissingValues + 1;
                                string writeMisssingValues = roleAttribute["Missing Values"].ToString();
                                if (writeMisssingValues != "")
                                {
                                    writeMisssingValues = writeMisssingValues + "|";
                                }
                                roleAttribute["Missing Values"] = writeMisssingValues + "These Attribute Name is not provided under the Customer";
                            }

                        }
                    }

                }
                //Duplicate values find
                var duplicatesAttributeValues = RoleCatalog.Tables[2].AsEnumerable().GroupBy(dr => new { ROLE_NAME = dr.Field<string>("ROLE_NAME"), ATTRIBUTE_NAME = dr.Field<string>("ATTRIBUTE_NAME") }).Where(g => g.Count() > 1).Select(g => g.First()).ToList();
                int attributeDuplicateValues = duplicatesAttributeValues.Count;
                foreach (var itr in duplicatesAttributeValues)
                {
                    string duplicateRoleName = itr[1].ToString();
                    string duplicateCatalogName = itr[2].ToString();
                    foreach (DataRow dr in RoleCatalog.Tables[2].Rows)
                    {
                        string checkDuplicateRoleName = dr["ROLE_NAME"].ToString();
                        string checkDuplicateCatalogName = dr["ATTRIBUTE_NAME"].ToString();
                        if (duplicateRoleName.Equals(checkDuplicateRoleName) && duplicateCatalogName.Equals(checkDuplicateCatalogName))
                        {
                            string writeMisssingValues = dr["Missing Values"].ToString();
                            if (writeMisssingValues != "")
                            {
                                writeMisssingValues = writeMisssingValues + "|";
                            }
                            dr["Missing Values"] = writeMisssingValues + "Duplicate Values";
                            RoleCatalog.Tables[2].AcceptChanges();
                        }
                    }
                }

                RoleCatalog.AcceptChanges();
                System.Web.HttpContext.Current.Session["calculateRoleAttributeMissingColumn"] = roleAttributeMissingColumn;
                System.Web.HttpContext.Current.Session["calculateRoleAttributeMissingValues"] = roleAttributeMissingValues;
                return RoleCatalog;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ValidateRoleAttribute ", ex);
                return null;
            }
        }

        public DataSet AddNewRole(DataSet RoleCatalog)
        {
            try
            {
                string newRoleName = string.Empty;
                int updateValues = 0; int insertValues = 0;
                if (!RoleCatalog.Tables[0].Rows.Count.Equals(0) && !RoleCatalog.Tables[1].Rows.Count.Equals(0) && !RoleCatalog.Tables[2].Rows.Count.Equals(0))
                {
                    RoleCatalog.Tables[0].Columns.Add("STATUS");
                    RoleCatalog.Tables[1].Columns.Add("STATUS");
                    RoleCatalog.Tables[2].Columns.Add("STATUS");
                    foreach (DataTable dt in RoleCatalog.Tables)
                    {
                        int inc = 0;
                        var distinctValues = dt.AsEnumerable().Select(row => new { role_name = row.Field<string>("ROLE_NAME") }).ToArray().Distinct().ToList();
                        string[] insertRoleName = new string[distinctValues.Count];
                        foreach (var itr in distinctValues)
                        {
                            var chkRoleName = objLS.aspnet_Roles.Where(x => x.RoleName == itr.role_name).Select(x => x.RoleName).FirstOrDefault();
                            if (chkRoleName == null)
                            {
                                insertRoleName[inc] = itr.role_name;
                                if (newRoleName == "")
                                {
                                    newRoleName = itr.role_name;
                                }
                                else
                                {
                                    newRoleName = newRoleName + "~" + itr.role_name;
                                }
                                inc++;
                            }
                        }
                        for (int i = 0; i < inc; i++)
                        {
                            string addNewRoleName = insertRoleName[i];
                            if (addNewRoleName != null)
                            {
                                foreach (DataRow dr in dt.Rows)
                                {
                                    string chkOldRoleName = dr["ROLE_NAME"].ToString();
                                    if (addNewRoleName.Equals(chkOldRoleName))
                                    {
                                        insertValues = insertValues + 1;
                                        dr["STATUS"] = "insert";
                                        dt.AcceptChanges();
                                    }
                                    else
                                    {
                                        updateValues = updateValues + 1;
                                        dr["STATUS"] = "update";
                                        dt.AcceptChanges();
                                    }
                                }
                            }

                        }
                        RoleCatalog.AcceptChanges();
                    }
                    if (newRoleName != "")
                    {
                        RoleCatalog = CreateRowsRoleImport(RoleCatalog, newRoleName);
                    }
                }
                System.Web.HttpContext.Current.Session["calculateUpdateValues"] = updateValues;
                System.Web.HttpContext.Current.Session["calculateInsertValues"] = insertValues;
                return RoleCatalog;

            }
            catch (Exception ex)
            {
                _logger.Error("Error at AddNewRole ", ex);
                return null;
            }
        }

        public DataSet CreateRowsRoleImport(DataSet RoleCatalog, string newRoleName)
        {
            try
            {
                string[] split = newRoleName.Split('~');
                string removeDuplicateRoleName = string.Empty;
                int customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                string[] finalRoleName = split.Distinct().ToArray<string>();
                int roleUpdateValues = 0; int roleInsertValues = 0;
                string addfinalRoleName = string.Empty;
                foreach (DataTable dt in RoleCatalog.Tables)
                {
                    for (int i = 0; i < finalRoleName.Length; i++)
                    {
                        int checkRoleNameValues = 0;
                        string chkRoleName = finalRoleName[i];
                        foreach (DataRow dr in dt.Rows)
                        {
                            string dtRoleName = dr["ROLE_NAME"].ToString();
                            if (chkRoleName.Equals(dtRoleName))
                            {
                                checkRoleNameValues = checkRoleNameValues + 1;
                            }
                        }
                        if (checkRoleNameValues == 0)
                        {
                            for (int j = 0; j < finalRoleName.Length; j++)
                            {
                                if (finalRoleName[j] != "")
                                {
                                    if (dt.Columns.Contains("CATALOG_NAME"))
                                    {
                                        var catalogValues = objLS.Customer_Catalog.Where(x => x.CustomerId == customerid).Select(x => x.CATALOG_ID).ToList();
                                        foreach (var itr in catalogValues)
                                        {
                                            //string catalogNameValues = objLS.TB_CATALOG.Where(x => x.CATALOG_ID == itr).Select(x => x.CATALOG_NAME).FirstOrDefault().ToString();

                                            string catalogNameValues = objLS.TB_CATALOG.Join(objLS.Customer_Catalog, tc => tc.CATALOG_ID, cc => cc.CATALOG_ID, (tc, cc) => new { tc, cc }).Where(x => x.tc.CATALOG_ID == itr && x.cc.CustomerId == customerid).Select(x => x.tc.CATALOG_NAME).FirstOrDefault().ToString();

                                            dt.Rows.Add(new Object[] { "", finalRoleName[j], catalogNameValues, "false", "false", "insert" });
                                            roleInsertValues = roleInsertValues + 1;
                                            dt.AcceptChanges();
                                        }
                                    }

                                    if (dt.Columns.Contains("FUNCTION_NAME"))
                                    {
                                        var functionGroupId = objLS.TB_FUNCTIONGROUP.Select(x => x.FUNCTION_GROUP_ID).ToList();
                                        foreach (var functionGroupIdValues in functionGroupId)
                                        {
                                            string fuctionGroupName = objLS.TB_FUNCTIONGROUP.Where(x => x.FUNCTION_GROUP_ID == functionGroupIdValues).Select(x => x.FUNCTION_GROUP_NAME).FirstOrDefault().ToString();
                                            var functionName = objLS.TB_FUNCTION.Where(x => x.FUNCTION_GROUP_ID == functionGroupIdValues).Select(x => x.FUNCTION_NAME).ToList();
                                            foreach (var functionNameValues in functionName)
                                            {
                                                dt.Rows.Add(new Object[] { "", finalRoleName[j], functionNameValues, "false", "false", "false", "false", "false", fuctionGroupName, "insert" });
                                                roleInsertValues = roleInsertValues + 1;
                                                dt.AcceptChanges();
                                            }
                                        }
                                    }

                                    if (dt.Columns.Contains("ATTRIBUTE_NAME"))
                                    {
                                        var attributeId = objLS.CUSTOMERATTRIBUTE.Where(x => x.CUSTOMER_ID == customerid).Select(x => x.ATTRIBUTE_ID).ToList();
                                        foreach (var attributeIdValues in attributeId)
                                        {
                                            var attributeName = objLS.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attributeIdValues).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();
                                            if (attributeName != null)
                                            {
                                                dt.Rows.Add(new Object[] { "", finalRoleName[j], attributeName.ToString(), "no", "insert" });
                                                roleInsertValues = roleInsertValues + 1;
                                                dt.AcceptChanges();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                foreach (DataTable dt in RoleCatalog.Tables)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string status = dr["STATUS"].ToString();
                        if (status == "")
                        {
                            roleUpdateValues = roleUpdateValues + 1;
                            dr["STATUS"] = "update";
                        }
                    }
                    dt.AcceptChanges();
                }
                System.Web.HttpContext.Current.Session["calculateRoleUpdateValues"] = roleUpdateValues;
                System.Web.HttpContext.Current.Session["calculateRoleInsertValues"] = roleInsertValues;
                return RoleCatalog;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CreateRowsRoleImport ", ex);
                return null;
            }
        }

        public Tuple<DataSet, int> CheckCurrentLoginRoleName(DataSet RoleCatalog)
        {
            try
            {
                int skippedRoleNameCount = 0; int count = 0;
                int customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId;
                var emailId = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).Email;
                var guidUserId = objLS.aspnet_Membership.Where(x => x.Email == (emailId.ToString())).Select(x => x.UserId).FirstOrDefault();
                var guidRoleId = objLS.vw_aspnet_UsersInRoles.Where(x => x.UserId == guidUserId).Select(x => x.RoleId).FirstOrDefault();
                var roleName = objLS.aspnet_Roles.Where(x => x.RoleId == guidRoleId).Select(x => x.RoleName).FirstOrDefault();
                string strRoleName = roleName.ToString();

                foreach (DataTable itrTables in RoleCatalog.Tables)
                {
                    foreach (DataRow checkRoleName in itrTables.Rows)
                    {
                        string roleNameTableValue = checkRoleName["ROLE_NAME"].ToString();
                        if (strRoleName.Equals(roleNameTableValue))
                        {
                            checkRoleName.Delete();
                            count = count + 1;
                        }
                    }
                    RoleCatalog.AcceptChanges();
                }
                RoleCatalog.AcceptChanges();

                if (count == 0)
                {
                    skippedRoleNameCount = 0;
                }
                else
                {
                    skippedRoleNameCount = 1;
                }
                return Tuple.Create(RoleCatalog, skippedRoleNameCount);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController: CheckCurrentLoginRoleName ", ex);
                return null;
            }
        }

        public Tuple<DataSet, string> CheckColumnsNamesRoleImport(DataSet RoleCatalog)
        {
            try
            {
                string[] roleCatalogColumns = { "ACTION", "ROLE_NAME", "CATALOG_NAME", "IsActive", "IsRead" };
                string checkColumnRoleNameResult = string.Empty;
                foreach (DataColumn roleCatalogNames in RoleCatalog.Tables[0].Columns)
                {
                    string roleName = roleCatalogNames.ToString();
                    int pos = Array.IndexOf(roleCatalogColumns, roleName);
                    if (pos > -1)
                    { }
                    else
                    {
                        checkColumnRoleNameResult = "fail";
                        return Tuple.Create(RoleCatalog, checkColumnRoleNameResult);
                    }
                }

                string[] roleGroupNamecolumns = { "ACTION", "ROLE_NAME", "FUNCTION_NAME", "ACTION_VIEW", "ACTION_MODIFY", "ACTION_ADD", "ACTION_REMOVE", "ACTION_DETACH", "FUNCTION_GROUP_NAME" };
                foreach (DataColumn groupFunctionNames in RoleCatalog.Tables[1].Columns)
                {
                    string roleName = groupFunctionNames.ToString();
                    int pos = Array.IndexOf(roleGroupNamecolumns, roleName);
                    if (pos > -1)
                    { }
                    else
                    {
                        checkColumnRoleNameResult = "fail";
                        return Tuple.Create(RoleCatalog, checkColumnRoleNameResult);
                    }
                }

                string[] roleAttributeNameColumns = { "ACTION", "ROLE_NAME", "ATTRIBUTE_NAME", "Allow Edit" };
                foreach (DataColumn catalogAttributesName in RoleCatalog.Tables[2].Columns)
                {
                    string roleName = catalogAttributesName.ToString();
                    int pos = Array.IndexOf(roleAttributeNameColumns, roleName);
                    if (pos > -1)
                    { }
                    else
                    {
                        checkColumnRoleNameResult = "fail";
                        return Tuple.Create(RoleCatalog, checkColumnRoleNameResult);
                    }
                }
                checkColumnRoleNameResult = "success";
                return Tuple.Create(RoleCatalog, checkColumnRoleNameResult);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController: CheckColumnsNamesRoleImport ", ex);
                return null;
            }
        }
        #endregion

        #region Table designer in family page
        [System.Web.Http.HttpPost]

        public string FinishTableDesignerImport(string excelSheetPath, string sheetName)
        {
            try
            {
                string validationResult = ValidateTableDesignerFamilyLevelImport(excelSheetPath, sheetName);
                string[] arrValidationResult = validationResult.Split('~');
                if (arrValidationResult[0].Equals("Validation success"))
                {
                    string importResult = adImport.FinishImportFamilyLevelTableDesigner(excelSheetPath, sheetName);
                    return "Import Success";
                }
                else if (arrValidationResult[0].Equals("Validation failed"))
                {
                    return "Validation failed";
                }
                else if (arrValidationResult[0].Equals("False"))
                {
                    return "false";
                }
                return "false";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at FinishTableDesignerImport ", ex);
                return "false";
            }
        }


        public string ValidateTableDesignerFamilyLevelImport(string excelSheetPath, string sheetName)
        {
            try
            {
                string validateResults = string.Empty;
                QueryValues queryValues = new QueryValues();
                string importTemp, SQLString = string.Empty;
                string importtype = "TableDesigner";
                importTemp = Guid.NewGuid().ToString();
                string validateCategoryNameMissing = "";
                string validateFamilyNameMissing = "";
                string validateMissingColumns = "";
                string catalogName = string.Empty;
                string catalogid = string.Empty;
                int rowcount = 0;
                List<string> columnToRemoveFromTableDesigner = new List<string>();
                int missingDataRowCount = 0;
                DataTable excelDatas = adImport.GetDataFromExcel(sheetName, excelSheetPath, "select * from [" + sheetName + "$]", "");


                DataTable excelData = excelDatas.Clone(); //just copy structure, no data
                for (int j = 0; j < excelDatas.Columns.Count; j++)
                {
                    if (excelData.Columns[j].DataType != typeof(string))
                        excelData.Columns[j].DataType = typeof(string);
                }

                foreach (DataRow dr in excelDatas.Rows)
                {
                    excelData.ImportRow(dr);
                }





                DataTable deletedRecords = adImport.GetDataFromExcel(sheetName, excelSheetPath, " SELECT * from [" + sheetName + "$]", "DELETE");
                AddIdColumnsForTableDesigner(deletedRecords, importtype);  //Add ID columns if it not exists
                DataTable categoryDistinctValues = new DataTable();
                DataTable deleteRecordslog = new DataTable();
                deleteRecordslog = deletedRecords.Clone();
                deleteRecordslog.Columns.Add("VALIDATION TYPE", typeof(System.String));
                deleteRecordslog.Columns.Add("MESSAGE", typeof(System.String));
                if (excelData != null)
                {
                    if (excelData.Columns[0].ColumnName.ToString() == "Action")
                    {
                        excelData.Columns.Remove("Action");
                    }
                }
                AddIdColumnsForTableDesigner(excelData, importtype);  //Add ID columns if it not exists
                DataTable dt = excelData.Clone();
                DataTable filterMandatoryColumns = excelData.Copy();
                string sqlConn = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
                dt.Columns.Add("VALIDATION TYPE", typeof(System.String));
                dt.Columns.Add("MESSAGE", typeof(System.String));
                int count = 0;
                string[] mandatoryColumnNames = new string[] { "CATALOG_ID", "CATALOG_NAME", "CATEGORY_ID", "CATEGORY_NAME", "FAMILY_ID", "FAMILY_NAME", "FAMILY_TABLE_STRUCTURE", "STRUCTURE_NAME", "IS_DEFAULT" };
                foreach (var item in mandatoryColumnNames)
                {
                    DataColumnCollection columns = excelData.Columns;
                    if (columns.Contains(item))
                    {

                    }
                    else
                    {
                        if (dt.Rows.Count == 0)
                        {
                            var rows = excelData.AsEnumerable().Take(1).ToList();
                            dt.Rows.Add(rows[0].ItemArray);
                        }
                        foreach (DataRow dr in dt.Rows)
                        {
                            string columnNamemissing = item;
                            validateMissingColumns = "MISSINGCOLUMNS";
                            if (count == 0)
                            {
                                dr["VALIDATION TYPE"] = "Invalid Column Name.";
                                dr["MESSAGE"] = columnNamemissing + " Column name Missing/Invalid.";
                            }
                            else
                            {
                                dr["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                                dr["MESSAGE"] = dr["MESSAGE"].ToString() + "\n" + columnNamemissing + "  Column name Missing/Invalid.";
                            }
                            count++;
                        }
                    }
                }

                if (validateMissingColumns == string.Empty)
                {
                    foreach (DataRow rows in excelData.Rows)
                    {
                        int missingDataCount = 0;
                        string excelCatalogName = rows["CATALOG_NAME"].ToString();
                        string excelCategoryName = rows["CATEGORY_NAME"].ToString();
                        string excelFamilyName = rows["FAMILY_NAME"].ToString();
                        string excelFamilyTableStructure = rows["FAMILY_TABLE_STRUCTURE"].ToString();
                        string excelStrucutureName = rows["STRUCTURE_NAME"].ToString();
                        string excelIsDefault = rows["IS_DEFAULT"].ToString();
                        dt.Rows.Add(rows.ItemArray);
                        if (excelCatalogName == string.Empty)
                        {
                            validateMissingColumns = "MISSINGCOLUMNS";
                            if (missingDataCount == 0)
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = "Catalog name should not be blank.";
                            }
                            else
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Catalog name should not be blank.";
                            }
                            missingDataCount++;

                        }

                        if (excelCategoryName == string.Empty)
                        {
                            validateMissingColumns = "MISSINGCOLUMNS";
                            if (missingDataCount == 0)
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = "Category name should not be blank.";
                            }
                            else
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Category name should not be blank.";
                            }
                            missingDataCount++;
                        }


                        if (excelFamilyName == string.Empty)
                        {
                            validateMissingColumns = "MISSINGCOLUMNS";
                            if (missingDataCount == 0)
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = "Family name should not be blank.";
                            }
                            else
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Family name should not be blank.";
                            }
                            missingDataCount++;
                        }

                        if (excelFamilyTableStructure == string.Empty)
                        {
                            validateMissingColumns = "MISSINGCOLUMNS";
                            if (missingDataCount == 0)
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = "Family table structure should not be blank.";
                            }
                            else
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Family table structure should not be blank.";
                            }
                            missingDataCount++;
                        }

                        if (excelStrucutureName == string.Empty)
                        {
                            validateMissingColumns = "MISSINGCOLUMNS";
                            if (missingDataCount == 0)
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = "Strucutre Name should not be blank.";
                            }
                            else
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Strucutre Name should not be blank.";
                            }
                            missingDataCount++;
                        }

                        if (excelIsDefault == string.Empty)
                        {
                            validateMissingColumns = "MISSINGCOLUMNS";
                            if (missingDataCount == 0)
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = "Is default should not be blank please give 'True' or 'False'.";
                            }
                            else
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Is default should not be blank please give 'True' or 'False'.";
                            }
                            missingDataCount++;
                        }

                        else if (!excelIsDefault.Equals("True") && !excelIsDefault.Equals("False"))
                        {
                            validateMissingColumns = "MISSINGCOLUMNS";
                            if (missingDataCount == 0)
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = "Is default should not be blank please give 'True' or 'False'.";
                            }
                            else
                            {
                                dt.Rows[missingDataRowCount]["VALIDATION TYPE"] = "\n" + "Invalid Column Name.";
                                dt.Rows[missingDataRowCount]["MESSAGE"] = dt.Rows[missingDataRowCount]["MESSAGE"].ToString() + "\n" + "Is default should not be blank please give 'True' or 'False'.";
                            }
                            missingDataCount++;
                        }

                        missingDataRowCount++;
                    }
                }



                if (validateMissingColumns == string.Empty)
                {
                    foreach (DataRow deleteRows in deletedRecords.Rows)
                    {
                        int deleteCount = 0;
                        string deleteStrucutureName = deleteRows["STRUCTURE_NAME"].ToString();
                        if (deleteStrucutureName == "Default Layout")
                        {
                            validateMissingColumns = "MISSINGCOLUMNS";
                            deleteRecordslog.Rows.Add(deleteRows.ItemArray);
                            deleteRecordslog.Columns.Add("Action", typeof(System.String)).SetOrdinal(0);
                            deleteRecordslog.Rows[deleteCount]["Action"] = "DELETE";
                            deleteRecordslog.Rows[deleteCount]["VALIDATION TYPE"] = "Action Column.";
                            deleteRecordslog.Rows[deleteCount]["MESSAGE"] = "Default layout can not be deleted.";
                            deleteCount++;
                        }
                    }
                    dt = deleteRecordslog.Copy();
                }


                //get catalog id
                if (validateMissingColumns == string.Empty)
                {

                    var excelDistinctCatalogName = excelData.AsEnumerable()
                           .Select(s => new
                           {
                               catalogName = s.Field<string>("CATALOG_NAME"),
                           })
                           .Distinct().ToList();

                    catalogName = excelDistinctCatalogName[0].catalogName.ToString();
                    catalogid = objLS.TB_CATALOG.Where(s => s.CATALOG_NAME == catalogName && s.FLAG_RECYCLE == "A").Select(a => a.CATALOG_ID).SingleOrDefault().ToString();
                    categoryDistinctValues.Columns.Add("CATALOG_ID", typeof(System.String));
                    string subCategoryname = "SUBCATNAME_L1";
                    int subCategorycount = 1;
                    categoryDistinctValues.Columns.Add("CATEGORY_NAME", typeof(System.String));
                    for (int i = 0; i < excelData.Columns.Count; i++)
                    {
                        if (excelData.Columns[i].ColumnName == subCategoryname)
                        {
                            subCategorycount = subCategorycount + 1;
                            categoryDistinctValues.Columns.Add(excelData.Columns[i].ColumnName, typeof(System.String));
                            subCategoryname = "SUBCATNAME_L" + subCategorycount;
                        }
                    }
                }

                DataTable dtCloned = excelData.Clone();
                dtCloned.Columns[0].DataType = typeof(string);
                dtCloned.Columns[1].DataType = typeof(string);
                dtCloned.Columns[2].DataType = typeof(string);
                dtCloned.Columns[3].DataType = typeof(string);
                dtCloned.Columns[4].DataType = typeof(string);
                foreach (DataRow row in excelData.Rows)
                {
                    dtCloned.ImportRow(row);
                }
                excelData.Clear();
                excelData = dtCloned;
                if (validateMissingColumns == string.Empty)
                {
                    for (int i = 0; i < categoryDistinctValues.Columns.Count; i++)
                    {
                        string values = categoryDistinctValues.Columns[i].ColumnName;
                        Type fieldType = excelData.Columns[values].DataType;
                        var a = excelData.AsEnumerable()
                       .Select(r => r.Field<string>(values))
                       .Distinct().ToList();
                        DataRow datarow = null;
                        for (int j = 0; j <= a.Count - 1; j++)
                        {
                            if (a[j] != null)
                            {

                                datarow = categoryDistinctValues.NewRow();

                                datarow[values] = a[j].ToString();

                                categoryDistinctValues.Rows.Add(datarow);
                            }

                        }

                    }
                    Dictionary<string, List<string>> categoryDictionaries = new Dictionary<string, List<string>>();
                    for (int j = 0; j < categoryDistinctValues.Columns.Count; j++)
                    {

                        string columnName = categoryDistinctValues.Columns[j].ColumnName.ToString();
                        List<string> list = categoryDistinctValues.AsEnumerable().Select(s => s.Field<string>(columnName)).ToList();
                        list.RemoveAll(item => item == null);
                        Dictionary<string, List<string>> dictionary = Enumerable.Range(0, 1)
                            .ToDictionary(i => columnName, x => list);
                        categoryDictionaries.Add(columnName, list);
                    }
                    string checkCategoryCatalogId = string.Empty;
                    string categoryName = string.Empty;
                    foreach (KeyValuePair<String, List<string>> values in categoryDictionaries)
                    {

                        for (int j = 0; j < values.Value.Count; j++)
                        {

                            if (values.Key == "CATALOG_ID")
                            {
                                checkCategoryCatalogId = values.Value[j].ToString();
                            }

                            if (values.Key == "CATEGORY_NAME")
                            {
                                categoryName = values.Value[j].ToString();
                            }

                            else if (values.Key != "CATALOG_ID")
                            {
                                categoryName = values.Value[j].ToString();
                            }

                            if (checkCategoryCatalogId == string.Empty)
                            {
                                checkCategoryCatalogId = catalogid;
                            }

                            if (categoryName.Contains("'"))
                            {
                                string[] arrCategoryName = categoryName.Split('\'');
                                string changeCategoryName = "";
                                for (int i = 0; i < arrCategoryName.Length; i++)
                                {
                                    if (changeCategoryName != "")
                                    {
                                        changeCategoryName = changeCategoryName + "''" + arrCategoryName[i];
                                    }
                                    else
                                    {
                                        changeCategoryName = arrCategoryName[i];
                                    }

                                }
                                categoryName = changeCategoryName;
                            }

                            if (categoryName != string.Empty)
                            {

                                using (var objSqlConnection = new SqlConnection(sqlConn))
                                {
                                    string checkColumnname = "CATEGORY_NAME";
                                    objSqlConnection.Open();
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.CommandTimeout = 0;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = checkCategoryCatalogId;
                                    objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                                    objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = categoryName ?? "";
                                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = "";
                                    objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                    objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = checkColumnname ?? "";
                                    objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                    objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.NVarChar, 500).Value = "";
                                    objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.NVarChar, -1).Value = "";
                                    objSqlCommand.Parameters.Add("@Action", SqlDbType.NVarChar, 50).Value = "";
                                    objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                                    objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                                    using (SqlDataReader rdr = objSqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                                    {
                                        while (rdr.Read())
                                        {

                                            validateResults = rdr.GetString(rdr.GetOrdinal("Result"));



                                            if (validateResults == "FALSE")
                                            {

                                                string columnName = values.Key;
                                                var rows = excelData.AsEnumerable()
                                                .Where(r => r.Field<string>(columnName) == categoryName).ToList();
                                                dt.Rows.Add(rows[0].ItemArray);

                                                validateCategoryNameMissing = "CATEGORYNAMEMISSING";

                                                if (count == 0)
                                                {

                                                    dt.Rows[rowcount]["VALIDATION TYPE"] = "Invalid Category Name.";
                                                    dt.Rows[rowcount]["MESSAGE"] = "Category " + categoryName + " is not valid.";
                                                }
                                                else
                                                {
                                                    dt.Rows[rowcount]["VALIDATION TYPE"] = dt.Rows[rowcount]["VALIDATION TYPE"].ToString() + "\n" + "Invalid Category Name.";
                                                    dt.Rows[rowcount]["MESSAGE"] = dt.Rows[rowcount]["MESSAGE"].ToString() + "\n" + " Category " + categoryName + " is not valid.";
                                                }
                                                count++;
                                                rowcount++;
                                            }
                                        }
                                        rdr.Close();
                                    }
                                }
                            }
                        }
                    }
                    //Family name validation
                    DataTable familyNameDistinctValues = new DataTable();
                    familyNameDistinctValues.Columns.Add("CATALOG_ID", typeof(System.String));
                    familyNameDistinctValues.Columns.Add("FAMILY_NAME", typeof(System.String));
                    string subFamilyname = "SUBFAMNAME_L1";
                    int subfamilycount = 1;
                    for (int i = 0; i < excelData.Columns.Count; i++)
                    {
                        if (excelData.Columns[i].ColumnName == subFamilyname)
                        {
                            subfamilycount = subfamilycount + 1;
                            familyNameDistinctValues.Columns.Add(excelData.Columns[i].ColumnName, typeof(System.String));
                            subFamilyname = "SUBFAMNAME_L" + subfamilycount;
                        }
                    }
                    for (int i = 0; i < familyNameDistinctValues.Columns.Count; i++)
                    {
                        string values = familyNameDistinctValues.Columns[i].ColumnName;
                        var a = excelData.AsEnumerable()
                       .Select(r => r.Field<string>(values))
                          .Distinct().ToList();
                        DataRow datarow = null;
                        for (int j = 0; j <= a.Count - 1; j++)
                        {
                            if (a[j] != null)
                            {
                                datarow = familyNameDistinctValues.NewRow();
                                datarow[values] = a[j].ToString();
                                familyNameDistinctValues.Rows.Add(datarow);
                            }
                        }
                    }
                    Dictionary<string, List<string>> familyDictionaries = new Dictionary<string, List<string>>();
                    for (int j = 0; j < familyNameDistinctValues.Columns.Count; j++)
                    {

                        string columnName = familyNameDistinctValues.Columns[j].ColumnName.ToString();
                        List<string> list = familyNameDistinctValues.AsEnumerable().Select(s => s.Field<string>(columnName)).ToList();
                        list.RemoveAll(item => item == null);
                        Dictionary<string, List<string>> dictionary = Enumerable.Range(0, 1)
                            .ToDictionary(i => columnName, x => list);
                        familyDictionaries.Add(columnName, list);
                    }
                    string checkFamilyCatalogId = string.Empty;
                    string familyName = string.Empty;
                    foreach (KeyValuePair<String, List<string>> values in familyDictionaries)
                    {
                        for (int j = 0; j < values.Value.Count; j++)
                        {

                            if (values.Key == "CATALOG_ID")
                            {
                                checkFamilyCatalogId = values.Value[j].ToString();
                            }

                            if (values.Key == "FAMILY_NAME")
                            {
                                familyName = values.Value[j].ToString();
                            }

                            else if (values.Key != "CATALOG_ID")
                            {
                                familyName = values.Value[j].ToString();
                            }


                            if (checkFamilyCatalogId == string.Empty)
                            {
                                checkFamilyCatalogId = catalogid;
                            }

                            if (familyName.Contains("'"))
                            {
                                string[] arrFamilyName = familyName.Split('\'');
                                string changeFamilyName = "";
                                for (int i = 0; i < arrFamilyName.Length; i++)
                                {
                                    if (changeFamilyName != "")
                                    {
                                        changeFamilyName = changeFamilyName + "''" + arrFamilyName[i];
                                    }
                                    else
                                    {
                                        changeFamilyName = arrFamilyName[i];
                                    }

                                }
                                familyName = changeFamilyName;
                            }

                            if (familyName != string.Empty)
                            {
                                using (var objSqlConnection = new SqlConnection(sqlConn))
                                {

                                    string checkColumnname = "FAMILY_NAME";
                                    objSqlConnection.Open();
                                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO_TABLEDESIGNERFAMILYIMPORTVALIDATION";
                                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                                    objSqlCommand.CommandTimeout = 0;
                                    objSqlCommand.Connection = objSqlConnection;
                                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 50).Value = checkFamilyCatalogId;
                                    objSqlCommand.Parameters.Add("@CATEGORY_SHORT", SqlDbType.NVarChar, 10).Value = "";
                                    objSqlCommand.Parameters.Add("@CATEGORY_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 500).Value = "";
                                    objSqlCommand.Parameters.Add("@FAMILY_NAME", SqlDbType.NVarChar, 4000).Value = familyName ?? "";
                                    objSqlCommand.Parameters.Add("@ColumnName", SqlDbType.NVarChar, 50).Value = checkColumnname ?? "";
                                    objSqlCommand.Parameters.Add("@STRUCTURE_NAME", SqlDbType.NVarChar, 4000).Value = "";
                                    objSqlCommand.Parameters.Add("@IS_DEFAULT", SqlDbType.NVarChar, 500).Value = "";
                                    objSqlCommand.Parameters.Add("@FAMILY_TABLE_STRUCTURE", SqlDbType.NVarChar, -1).Value = "";
                                    objSqlCommand.Parameters.Add("@Action", SqlDbType.NVarChar, 50).Value = "";
                                    objSqlCommand.Parameters.Add("@Result", SqlDbType.VarChar, 10);
                                    objSqlCommand.Parameters["@Result"].Direction = ParameterDirection.Output;
                                    using (SqlDataReader rdr = objSqlCommand.ExecuteReader(CommandBehavior.CloseConnection))
                                    {
                                        while (rdr.Read())
                                        {
                                            validateResults = rdr.GetString(rdr.GetOrdinal("Result"));
                                            if (validateResults == "FALSE")
                                            {
                                                string columnName = values.Key;
                                                var rows = excelData.AsEnumerable()
                                                .Where(r => r.Field<string>(columnName) == familyName).ToList();
                                                dt.Rows.Add(rows[0].ItemArray);
                                                validateFamilyNameMissing = "FAMILYNAMEMISSING";
                                                if (count == 0)
                                                {
                                                    dt.Rows[rowcount]["VALIDATION TYPE"] = "Invalid Family Name";
                                                    dt.Rows[rowcount]["MESSAGE"] = "Family " + familyName + " is not valid";
                                                }
                                                else
                                                {
                                                    dt.Rows[rowcount]["VALIDATION TYPE"] = dt.Rows[rowcount]["VALIDATION TYPE"].ToString() + "\n" + "Invalid Family Name";
                                                    dt.Rows[rowcount]["MESSAGE"] = dt.Rows[rowcount]["MESSAGE"].ToString() + "\n" + " Family " + familyName + " is not valid";
                                                }
                                                count++;
                                                rowcount++;
                                            }
                                        }
                                        rdr.Close();
                                    }
                                }
                            }
                        }
                    }
                }
                count = 0;
                rowcount = 0;
                var errorLogDataTableRowRemove = dt.AsEnumerable().Where(row => row.Field<string>("VALIDATION TYPE") == null && row.Field<string>("MESSAGE") == null);
                foreach (var row in errorLogDataTableRowRemove.ToList())
                {
                    row.Delete();
                }
                HttpContext.Current.Session["ValidateTableDesignerTable"] = dt;
                System.Web.HttpContext.Current.Session["downloadLogFamilyPage"] = dt;
                if (validateFamilyNameMissing == string.Empty && validateCategoryNameMissing == string.Empty && validateMissingColumns == string.Empty)
                {
                    validateResults = "Passed";
                }
                else
                {
                    validateResults = "Failed";
                }

                if (validateResults.Contains("Passed"))
                {
                    return "Validation success";
                }
                else
                {
                    return "Validation failed" + "~" + validateFamilyNameMissing + "~" + validateCategoryNameMissing + "~" + validateMissingColumns;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : Validatetabledesignerimport", ex);
                return "False";
            }

        }


        public DataTable AddIdColumnsForTableDesigner(DataTable dtGetTableDesigner, string importType)
        {
            try
            {
                if (dtGetTableDesigner == null || dtGetTableDesigner.Rows.Count == 0)
                {
                    return dtGetTableDesigner;
                }
                if (!dtGetTableDesigner.Columns.Contains("CATALOG_ID"))
                {
                    dtGetTableDesigner.Columns.Add("CATALOG_ID");
                    dtGetTableDesigner.Columns["CATALOG_ID"].SetOrdinal(0);
                }
                if (!dtGetTableDesigner.Columns.Contains("CATALOG_NAME"))
                {
                    dtGetTableDesigner.Columns.Add("CATALOG_NAME");
                    dtGetTableDesigner.Columns["CATALOG_NAME"].SetOrdinal(1);
                }
                if (!dtGetTableDesigner.Columns.Contains("CATEGORY_ID"))
                {
                    dtGetTableDesigner.Columns.Add("CATEGORY_ID");
                    dtGetTableDesigner.Columns["CATEGORY_ID"].SetOrdinal(dtGetTableDesigner.Columns.IndexOf("CATEGORY_NAME"));

                }
                int col = 1;
                for (int colindex = 0; colindex < dtGetTableDesigner.Columns.Count; colindex++)
                {
                    if (dtGetTableDesigner.Columns[colindex].ColumnName.ToUpper().Contains("SUBCATNAME"))
                    {
                        if (!dtGetTableDesigner.Columns.Contains("SUBCATID_L" + col))
                        {
                            dtGetTableDesigner.Columns.Add("SUBCATID_L" + col);
                            if (dtGetTableDesigner.Columns.Contains("SUBCATNAME_L" + col))
                            {
                                dtGetTableDesigner.Columns["SUBCATID_L" + col].SetOrdinal(dtGetTableDesigner.Columns.IndexOf("SUBCATNAME_L" + col));
                            }
                            colindex = colindex + 1;
                        }
                        if (dtGetTableDesigner.Columns.Contains("SUBCATID_L" + col))
                        {
                            col = col + 1;
                        }
                    }
                }
                if (!dtGetTableDesigner.Columns.Contains("FAMILY_ID"))
                {
                    dtGetTableDesigner.Columns.Add("FAMILY_ID");
                    if (dtGetTableDesigner.Columns.Contains("FAMILY_NAME"))
                    {
                        dtGetTableDesigner.Columns["FAMILY_ID"].SetOrdinal(dtGetTableDesigner.Columns.IndexOf("FAMILY_NAME"));
                    }
                }
                if (!dtGetTableDesigner.Columns.Contains("SUBFAMILY_ID"))
                {
                    dtGetTableDesigner.Columns.Add("SUBFAMILY_ID");
                    if (dtGetTableDesigner.Columns.Contains("FAMILY_NAME"))
                    {
                        dtGetTableDesigner.Columns["SUBFAMILY_ID"].SetOrdinal(dtGetTableDesigner.Columns.IndexOf("FAMILY_NAME") + 1);
                    }
                }
                if (!dtGetTableDesigner.Columns.Contains("SUBFAMILY_NAME"))
                {
                    dtGetTableDesigner.Columns.Add("SUBFAMILY_NAME");
                    dtGetTableDesigner.Columns["SUBFAMILY_NAME"].SetOrdinal(dtGetTableDesigner.Columns.IndexOf("SUBFAMILY_ID") + 1);
                }
                return dtGetTableDesigner;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportApiController : AddIdColumnsForTableDesigner", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]

        public JsonResult FamilyLevelTableDesignerImportViewLog(string tableName)
        {
            try
            {
                DashBoardModel model = new DashBoardModel();
                var ImportTableLog = (DataTable)System.Web.HttpContext.Current.Session[tableName];
                foreach (DataColumn columns in ImportTableLog.Columns)
                {
                    var objPTColumns = new PTColumns();
                    objPTColumns.Caption = columns.Caption;
                    objPTColumns.ColumnName = columns.ColumnName;
                    model.Columns.Add(objPTColumns);
                }
                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(ImportTableLog);
                model.Data = JSONString;
                return new JsonResult() { Data = model };
            }
            catch (Exception ex)
            {
                _logger.Error("Error at FamilyLevelTableDesignerImportViewLog ", ex);
                return null;
            }

        }

        #endregion

        #region Restore catalog

        public string RestoreUserDetails(int customerId, DataTable userImportTable)
        {
            try
            {
                string result = string.Empty;
                if (userImportTable != null && userImportTable.Rows.Count > 0)
                {
                    int insertRecords = 0; int updateRecords = 0; int skippedRecords = 0;
                    bool isAnonymous;
                    foreach (DataRow dr in userImportTable.Rows)
                    {
                        var userId = Guid.NewGuid().ToString();
                        string email = dr["EMAIL"].ToString();
                        string username = dr["USER_NAME"].ToString();
                        var checkuser = objLS.aspnet_Membership.Join(objLS.aspnet_Users, ams => ams.UserId, aus => aus.UserId, (ams, aus) => new { ams, aus })
                            .Where(x => x.ams.Email == (email) && x.aus.UserName == (username)).Select(x => x.ams.UserId).FirstOrDefault();
                        string strCheckUser = checkuser.ToString();
                        string status = dr["STATUS"].ToString();
                        bool statusbool = string.Equals(status, "ACTIVE", StringComparison.CurrentCultureIgnoreCase);
                        if ((strCheckUser != null) || (strCheckUser != ""))
                        {
                            if (!strCheckUser.Equals("00000000-0000-0000-0000-000000000000"))
                            {
                                userId = strCheckUser;
                                updateRecords++;
                            }
                            else
                            {
                                insertRecords++;
                            }
                        }
                        else
                        {
                            insertRecords++;
                        }
                        if (statusbool == true)
                        {
                            isAnonymous = false;
                        }
                        else if (statusbool == false)
                        {
                            isAnonymous = true;
                        }
                        else
                        {
                            isAnonymous = false;
                        }
                        //  string securepassword1 = dr["SECUREPASSWORD1"].ToString();
                        //string securepassword2 = dr["SECUREPASSWORD2"].ToString();

                        string securepassword1 = ConfigurationManager.AppSettings["DefaultCatalogStudioPassword"].ToString();
                        string securepassword2 = ConfigurationManager.AppSettings["DefaultCatalogStudioPasswordSalt"].ToString();

                        if (!userImportTable.Columns.Contains("SECUREPASSWORD1"))
                        {
                            userImportTable.Columns.Add("SECUREPASSWORD1");
                        }

                        if (!userImportTable.Columns.Contains("SECUREPASSWORD2"))
                        {
                            userImportTable.Columns.Add("SECUREPASSWORD2");
                        }
                        dr["SECUREPASSWORD1"] = securepassword1;
                        dr["SECUREPASSWORD2"] = securepassword2;

                        string password1 = string.Empty;
                        string password2 = string.Empty;

                        if (securepassword1 == null || securepassword1 == "")
                        {
                            if ((strCheckUser != null) || (strCheckUser != ""))
                            {
                                if (!strCheckUser.Equals("00000000-0000-0000-0000-000000000000"))
                                {
                                    password1 = objLS.aspnet_Membership.Where(x => x.UserId == (new Guid(userId)) && x.Email == email).Select(x => x.Password).FirstOrDefault().ToString();
                                    password2 = objLS.aspnet_Membership.Where(x => x.UserId == (new Guid(userId)) && x.Email == email).Select(x => x.PasswordSalt).FirstOrDefault().ToString();
                                }
                                else
                                {
                                    password1 = string.Empty;
                                    password2 = string.Empty;
                                }
                            }
                            else
                            {
                                password1 = string.Empty;
                                password2 = string.Empty;
                            }
                            bool checkpassword1 = string.IsNullOrEmpty(password1);
                            bool checkpassword2 = string.IsNullOrEmpty(password2);
                            if (checkpassword1 == false)
                            {
                                if ((password1 != null) || (password1 != ""))
                                {
                                    securepassword1 = password1;
                                }
                                else
                                {
                                    securepassword1 = "WxnoeWnZwGxC1e1zngqD+YeIDww=";
                                }

                            }
                            else
                            {
                                securepassword1 = "WxnoeWnZwGxC1e1zngqD+YeIDww=";
                            }
                            if (checkpassword2 == false)
                            {
                                if ((password2 != null) || (password2 != ""))
                                {
                                    securepassword2 = password2;
                                }
                                else
                                {
                                    securepassword2 = "T1Kb81FZdpb3TTTikcPgdQ==";
                                }
                            }
                            else
                            {
                                securepassword2 = "T1Kb81FZdpb3TTTikcPgdQ==";
                            }
                        }

                        string sameUser = objLS.aspnet_Membership.Where(x => x.Email == User.Identity.Name).Select(x => x.UserId).FirstOrDefault().ToString();

                        if ((!sameUser.Equals(userId)) && (!email.Equals(User.Identity.Name)))
                        {
                            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                            {
                                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                objSqlCommand.CommandText = "STP_LS_USER_IMPORT";
                                objSqlCommand.CommandType = CommandType.StoredProcedure;
                                objSqlCommand.Connection = objSqlConnection;
                                objSqlCommand.CommandTimeout = 0;
                                objSqlCommand.Parameters.Add("@UserId", SqlDbType.NVarChar, 100).Value = userId.ToString();
                                objSqlCommand.Parameters.Add("@customerId", SqlDbType.Int).Value = customerId;
                                objSqlCommand.Parameters.Add("@rolename", SqlDbType.NVarChar, 100).Value = dr["ROLE_NAME"].ToString();
                                objSqlCommand.Parameters.Add("@IsAnonymous", SqlDbType.Bit).Value = isAnonymous;
                                objSqlCommand.Parameters.Add("@User_Name", SqlDbType.NVarChar, 100).Value = dr["USER_NAME"].ToString();
                                objSqlCommand.Parameters.Add("@STATUS", SqlDbType.NVarChar, 100).Value = dr["STATUS"].ToString();
                                objSqlCommand.Parameters.Add("@EMAIL", SqlDbType.NVarChar, 100).Value = dr["EMAIL"].ToString();
                                objSqlCommand.Parameters.Add("@PASSWORD", SqlDbType.NVarChar, 100).Value = securepassword1;
                                objSqlCommand.Parameters.Add("@PASSWORDSALT", SqlDbType.NVarChar, 100).Value = securepassword2;
                                objSqlCommand.Parameters.Add("@ADDRESS", SqlDbType.NVarChar, 100).Value = dr["ADDRESS"].ToString();
                                objSqlCommand.Parameters.Add("@FIRSTNAME", SqlDbType.NVarChar, 100).Value = dr["FIRSTNAME"].ToString();
                                objSqlCommand.Parameters.Add("@LASTNAME", SqlDbType.NVarChar, 100).Value = dr["LASTNAME"].ToString();
                                objSqlCommand.Parameters.Add("@CITY", SqlDbType.NVarChar, 100).Value = dr["CITY"].ToString();
                                objSqlCommand.Parameters.Add("@STATE", SqlDbType.NVarChar, 100).Value = dr["STATE"].ToString();
                                objSqlCommand.Parameters.Add("@ZIP", SqlDbType.NVarChar, 100).Value = dr["ZIP"].ToString();
                                objSqlCommand.Parameters.Add("@FAX", SqlDbType.NVarChar, 100).Value = dr["FAX"].ToString();
                                objSqlCommand.Parameters.Add("@COUNTRY", SqlDbType.NVarChar, 100).Value = dr["COUNTRY"].ToString();
                                objSqlCommand.Parameters.Add("@PHONE", SqlDbType.NVarChar, 100).Value = dr["PHONE"].ToString();
                                objSqlCommand.Parameters.Add("@CATALOGNAME", SqlDbType.NVarChar, 100).Value = dr["DEFAULTCATALOG"].ToString();
                                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "INSERT";
                                objSqlConnection.Open();
                                objSqlCommand.ExecuteNonQuery();
                                objSqlConnection.Close();
                            }
                        }
                        else
                        {
                            dr["Action"] = "Skipped";
                            skippedRecords++;
                        }

                        result = "Saved";
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at FamilyLevelTableDesignerImportViewLog ", ex);
                return "Action not Completed";
            }
        }

        #endregion

        [System.Web.Http.HttpGet]

        public JsonResult ExcelImportType(string excelPath, string sheetName)
        {
            var dt = new DataTable();
            DataTable dtable = new DataTable();
            OleDbConnection conn;
            try
            {
                if (!string.IsNullOrEmpty(excelPath))
                {
                    Workbook workbook = Workbook.Load(excelPath);
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    string strConn;
                    if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=2\"";
                    else
                        //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                        //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=2\"";
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                                ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=2\"";
                    conn = new OleDbConnection(strConn);
                    if (conn.State == ConnectionState.Closed)
                    {
                        conn.Open();
                    }
                    if (!sheetName.Contains("$"))
                    {
                        sheetName = sheetName + "$";
                    }
                    dt = new DataTable(sheetName);
                    var dc = new DataColumn("ExcelColumn", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("CatalogField", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("SelectedToImport", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("IsSystemField", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("FieldType", typeof(string));
                    dt.Columns.Add(dc);


                    // For Mapping Attribute'


                    dc = new DataColumn("MappedAttributeName", typeof(string));
                    dt.Columns.Add(dc);

                    // End

                    string sheetNameworkbook = sheetName.Replace("$", "").TrimEnd();

                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells.Count();
                    for (int j = 0; j < iCols; j++)
                    {
                        // Name Changes Product(Family) in November 11 2022
                        if (sheetNameworkbook == "Products")
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null)
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace("PRODUCT_ID", "FAMILY_ID")
                                        .Replace("PRODUCT_NAME", "FAMILY_NAME")
                                        .Replace("SUBPRODUCT_ID", "SUBFAMILY_ID")
                                        .Replace("SUBPRODUCT_NAME", "SUBFAMILY_NAME")
                                        .Replace("Product_PdfTemplate", "Family_PdfTemplate")
                                        .Replace("Item_PdfTemplate", "Product_PdfTemplate")
                                        .Replace("PRODUCT_PUBLISH2PDF", "FAMILY_PUBLISH2PDF")
                                        .Replace("PRODUCT_PUBLISH2PRINT", "FAMILY_PUBLISH2PRINT")
                                        .Replace("PRODUCT_PUBLISH2WEB", "FAMILY_PUBLISH2WEB")
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();

                                if (sColName.ToUpper() != "ACTION")
                                {
                                    dr[0] = sColName;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                        else if (sheetNameworkbook == "Items")
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null)
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace("PRODUCT_ID", "FAMILY_ID")
                                        .Replace("ITEM_ID", "PRODUCT_ID")
                                        .Replace("PRODUCT_NAME", "FAMILY_NAME")
                                        .Replace("SUBPRODUCT_ID", "SUBFAMILY_ID")
                                        .Replace("SUBPRODUCT_NAME", "SUBFAMILY_NAME")
                                        .Replace("ITEM_PUBLISH2WEB", "PRODUCT_PUBLISH2WEB")
                                        .Replace("ITEM_PUBLISH2PRINT", "PRODUCT_PUBLISH2PRINT")
                                        .Replace("ITEM_PUBLISH2PDF", "PRODUCT_PUBLISH2PDF")
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();

                                if (sColName.ToUpper() != "ACTION")
                                {
                                    dr[0] = sColName;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                        else if (sheetNameworkbook == "ProductItems")
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null)
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace("PRODUCT_ID", "FAMILY_ID")
                                        .Replace("ITEM_ID", "PRODUCT_ID")
                                        .Replace("PRODUCT_NAME", "FAMILY_NAME")
                                        .Replace("SUBPRODUCT_ID", "SUBFAMILY_ID")
                                        .Replace("SUBPRODUCT_NAME", "SUBFAMILY_NAME")
                                        .Replace("ITEM_PUBLISH2WEB", "PRODUCT_PUBLISH2WEB")
                                        .Replace("ITEM_PUBLISH2PRINT", "PRODUCT_PUBLISH2PRINT")
                                        .Replace("ITEM_PUBLISH2PDF", "PRODUCT_PUBLISH2PDF")
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();

                                if (sColName.ToUpper() != "ACTION")
                                {
                                    dr[0] = sColName;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                        else if (sheetNameworkbook == "TableDesigner")
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null)
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace("PRODUCT_ID", "FAMILY_ID")
                                        .Replace("PRODUCT_NAME", "FAMILY_NAME")
                                        .Replace("SUBPRODUCT_ID", "SUBFAMILY_ID")
                                        .Replace("SUBPRODUCT_NAME", "SUBFAMILY_NAME")
                                        .Replace("PRODUCT_TABLE_STRUCTURE", "FAMILY_TABLE_STRUCTURE")
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();

                                if (sColName.ToUpper() != "ACTION")
                                {
                                    dr[0] = sColName;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }
                        else
                        {
                            if (workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value != null)
                            {
                                String sColName =
                                    workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[j].Value.ToString()
                                        .Replace('[', ' ')
                                        .Replace(']', ' ')
                                        .Trim();

                                DataRow dr = dt.NewRow();

                                if (sColName.ToUpper() != "ACTION")
                                {
                                    dr[0] = sColName;
                                    dt.Rows.Add(dr);
                                }
                            }
                        }

                        // Name Changes Product(Family) in November 11 2022 End

                    }






                    _dsSheets.Tables.Add(dt);
                    conn.Close();
                    conn.Dispose();

                }
                return new JsonResult() { Data = dt };
            }
            catch (Exception objException)
            {
                _logger.Error("Error at ImportApiController :ExcelImportType", objException);
                return null;
                conn.Close();
                conn.Dispose();
            }


        }
        /// <summary>
        /// Added by Aswin kumar
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns>This function returns Validation sheet names</returns>
        [System.Web.Http.HttpGet]
        public JsonResult ValidationSheetname(int templateId)
        {

            try
            {
                string[] str_ImportTypes = new string[] { "CATEGORY", "PRODUCT", "ITEM", "TABLEDESIGNER" };


                var validation_Sheets = _dbcontext.TB_TEMPLATE_SHEET_DETAILS_IMPORT.Search(t => t.IMPORT_TYPE)
                           .Containing(str_ImportTypes).Where(x => x.TEMPLATE_ID == templateId && x.FLAG_MAPPING == true && x.FLAG_IMPORT == false && x.TEMPLATE_ID != 0)
                           .Select(t => new { t.SHEET_NAME, t.IMPORT_TYPE, t.IMPORT_SHEET_ID }).OrderBy(x => x.IMPORT_SHEET_ID)
                           .ToList();



                return new JsonResult() { Data = validation_Sheets };
            }

            catch (Exception objException)
            {
                _logger.Error("Error at loading ImportApiController in ValidationSheetname", objException);
                return null;
            }

        }

        /// <summary>
        /// Added by Awin kumar
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns></returns>
        [System.Web.Http.HttpGet]
        public JsonResult GetMappingSheetNames(int templateId)
        {

            try
            {

                List<TB_TEMPLATE_SHEET_DETAILS_IMPORT> list_ImportSheetDetails = objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT.Where
                   (x => x.TEMPLATE_ID == templateId && x.FLAG_IMPORT == true).ToList();

                List<TB_TEMPLATE_SHEET_DETAILS_IMPORT> list_MappingSheetDetails = objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT.Where
                   (x => x.TEMPLATE_ID == templateId && x.FLAG_MAPPING == true).ToList();

                List<List<TB_TEMPLATE_SHEET_DETAILS_IMPORT>> list_Import_MappingDetails = new List<List<TB_TEMPLATE_SHEET_DETAILS_IMPORT>>
                {
                  list_ImportSheetDetails, list_MappingSheetDetails
                };
                return new JsonResult() { Data = list_Import_MappingDetails };


            }

            catch (Exception objException)
            {
                _logger.Error("Error at loading ImportApiController in GetMappingSheetNames", objException);
                return null;
            }

        }

        public static DataTable ConvertExcelToDataTableWithoutHeader(string FileName, string Sheetname)
        {
            DataTable dataTableResult = null;
            int totalSheet = 0; //No of sheets on excel file  
            try
            {
                string strConn;
                if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName +
                              ";Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1\"";
                else
                    //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName +
                    //          ";Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1\"";
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName +
                              ";Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1\"";
                //   using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=No;IMEX=1;';"))
                using (OleDbConnection objConn = new OleDbConnection(strConn))
                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet oleDataSet = new DataSet();
                    DataTable oleDatatable = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = string.Empty;
                    if (oleDatatable != null)
                    {
                        var tempDataTable = (from dataRow in oleDatatable.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();
                        oleDatatable = tempDataTable;
                        totalSheet = oleDatatable.Rows.Count;
                        sheetName = Sheetname + "$";
                    }
                    cmd.Connection = objConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "] ";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(oleDataSet, "excelData");
                    dataTableResult = oleDataSet.Tables["excelData"];
                    return dataTableResult; //Returning Dattable  
                }
            }

            catch (Exception ex)
            {
                _logger.Error("Error at AdvanceImportApiController : ConvertExcelToDataTableWithoutHeader", ex);
                return null;
            }
        }
        /// <summary>
        /// excel sheet(.xls format is converted into datatable)
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="Sheetname"></param>
        /// <returns>returns the datatable in the sheet values</returns>
        public static DataTable ConvertExcelToDataTableWithoutEmptyRows(string FileName, string Sheetname)
        {
            try
            {
                DataTable dtResult = null;
                string tableName = Sheetname;
                int totalSheet = 0; //No of sheets on excel file 


                //using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
                //{
                //    objConn.Open();
                //    OleDbCommand cmd = new OleDbCommand();
                //    OleDbDataAdapter oleda = new OleDbDataAdapter();
                //    DataSet ds = new DataSet();
                //    DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                //    string sheetName = string.Empty;
                //    System.Data.DataTable dt_Schema = objConn.GetSchema("COLUMNS");

                //    if (dt != null)
                //    {




                //        var tempDataTable = (from dataRow in dt.AsEnumerable()
                //                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                //                             select dataRow).CopyToDataTable();

                //        dt = tempDataTable;
                //        totalSheet = dt.Rows.Count;
                //        sheetName = Sheetname + "$";
                //    }
                //    cmd.Connection = objConn;
                //    cmd.CommandType = CommandType.Text;
                //    cmd = new System.Data.OleDb.OleDbCommand(String.Format("SELECT * FROM [{0}]",
                //                      dt_Schema.Rows[0]["TABLE_NAME"].ToString()), objConn);
                //    // cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                //    oleda = new OleDbDataAdapter(cmd);
                //    oleda.Fill(ds, tableName);
                //    dtResult = ds.Tables[tableName];
                //    objConn.Close();
                //    return dtResult; //Returning Dattable  
                //}
                String str_Connection = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName +
                                ";Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1;\"";
                DataTable dt_Parsed = new DataTable();
                using (System.Data.OleDb.OleDbConnection oledb_Connection = new System.Data.OleDb.OleDbConnection(str_Connection))
                {
                    // OPEN DB CONNECTION
                    oledb_Connection.Open();

                    // READS FILE DB SCHEMA

                    System.Data.DataTable dt_Schema = oledb_Connection.GetSchema("COLUMNS");

                    // CHECKS THAT DB SCHEMA IS NOT NULL
                    if (dt_Schema != null)
                    {
                        string sheetName = Sheetname + "$";
                        // CREATES DB COMMAND TO READ FILE
                        System.Data.OleDb.OleDbCommand odbc_Command = new System.Data.OleDb.OleDbCommand(String.Format("SELECT * FROM [{0}]",
                           sheetName.ToString()), oledb_Connection);

                        // SETS DB COMMAND TYPE
                        odbc_Command.CommandType = CommandType.Text;

                        // PARSES FILE ACCORDINGLY
                        new System.Data.OleDb.OleDbDataAdapter(odbc_Command).Fill(dt_Parsed);
                    }

                    // CLOSE DB CONNECTION
                    oledb_Connection.Close();
                }
                return dt_Parsed;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ConvertExcelToDataTable ", ex);
                return null;
            }

        }


        /// <summary>
        /// Added by Aswin kumar
        /// </summary>
        /// <param name="templateId"></param>
        /// <returns>This function returns group attributes sheet names</returns>
        [System.Web.Http.HttpGet]
        public JsonResult GroupAttributeSheetnames(int templateId)
        {

            try
            {
                string[] str_ImportTypes = new string[] { "PRODUCT", "ITEM" };



                var groupAttribute_Sheets = _dbcontext.TB_TEMPLATE_SHEET_DETAILS_IMPORT.Search(t => t.IMPORT_TYPE)
                           .Containing(str_ImportTypes).Where(x => x.TEMPLATE_ID == templateId && x.FLAG_MAPPING == true && x.FLAG_IMPORT == false && x.TEMPLATE_ID != 0)
                           .Select(t => new { t.SHEET_NAME, t.IMPORT_TYPE, t.IMPORT_SHEET_ID }).OrderBy(x => x.IMPORT_SHEET_ID)
                           .ToList();



                return new JsonResult() { Data = groupAttribute_Sheets };
            }

            catch (Exception objException)
            {
                _logger.Error("Error at loading ImportApiController in GroupAttributeSheetnames", objException);
                return null;
            }

        }

        /// <summary>
        /// excel sheet(.xls format is converted into datatable)
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="Sheetname"></param>
        /// <returns>returns the datatable in the sheet values</returns>
        public static DataTable ConvertExcelToDataTableWithSkipFirstRow(string FileName, string Sheetname)
        {
            try
            {
                DataTable dtResult = null;
                string tableName = Sheetname;
                int totalSheet = 0; //No of sheets on excel file  
                string strConn;
                if (FileName.Substring(FileName.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName +
                              ";Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1\"";
                else
                    //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName +
                    //          ";Extended Properties=\"Excel 8.0;HDR=YES;IMEX=1\"";
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName +
                              ";Extended Properties=\"Excel 12.0;HDR=YES;IMEX=1\"";
                //  using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1;';"))
                using (OleDbConnection objConn = new OleDbConnection(strConn))

                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();
                    DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = string.Empty;
                    DataTable dt_checkHeaderColumns = new DataTable();
                    if (dt != null)
                    {
                        var tempDataTable = (from dataRow in dt.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();

                        dt = tempDataTable;
                        totalSheet = dt.Rows.Count;
                        sheetName = Sheetname + "$";
                    }

                    string str_getExcelRowQuery = "SELECT * FROM [" + sheetName + "]";
                    OleDbDataAdapter da_checkHeaderexcel = new OleDbDataAdapter(str_getExcelRowQuery, objConn);
                    dt_checkHeaderColumns.Locale = CultureInfo.CurrentCulture;
                    da_checkHeaderexcel.Fill(dt_checkHeaderColumns);
                    int columnsCount = dt_checkHeaderColumns.Columns.Count;
                    string str_Columnname = ExcelColumnFromNumber(columnsCount);
                    str_getExcelRowQuery = "select * from [" + sheetName + "A2:" + str_Columnname + "65000]";

                    cmd.Connection = objConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = str_getExcelRowQuery;
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(ds, tableName);
                    dtResult = ds.Tables[tableName];
                    objConn.Close();
                    return dtResult; //Returning Dattable  
                }

            }
            catch (Exception ex)
            {
                _logger.Error("Error at ConvertExcelToDataTable ", ex);
                return null;
            }

        }

        /// <summary>
        /// Added by Aswin kumar.M
        /// </summary>
        /// <param name="templateId"></param>
        /// <param name="sheetName"></param>
        /// <param name="importType"></param>
        /// <param name="sessionId"></param>
        /// <returns>It returns a string value if the template has duplicate attribute names with session id </returns>

        public string CheckDuplicateMappingAttributeNames(int templateId, string sheetName, string importType, string sessionId)
        {
            string str_DuplicateMappingAttributesResult = string.Empty;
            HttpContext.Current.Session["DuplicateMappingAttributes"] = null;
            try
            {
                //var lt_MappingAttributes = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT, cs => cs.IMPORT_SHEET_ID, cu => cu.IMPORT_SHEET_ID, (cs, cu) => new { cs, cu }).
                //Where(x => x.cs.TEMPLATE_ID == templateId && x.cu.SHEET_NAME == sheetName && x.cs.ATTRIBUTE_ID>0).Distinct().Select(x => new { x.cu.IMPORT_TYPE, x.cs.CAPTION, x.cu.SHEET_NAME, x.cs.ATTRIBUTE_ID }).ToList();


                var lt_MappingAttributes = objLS.TB_TEMPLATE_MAPPING_DETAILS_IMPORT.Join(objLS.TB_TEMPLATE_SHEET_DETAILS_IMPORT,
                    TMD => TMD.IMPORT_SHEET_ID, TSD => TSD.IMPORT_SHEET_ID, (TMD, TSD) => new { TMD, TSD }).Join
               (objLS.TB_ATTRIBUTE, TA => TA.TMD.ATTRIBUTE_ID, TMD => TMD.ATTRIBUTE_ID, (TA, TMD) => new { TA, TMD }).
                Where(x => x.TA.TMD.TEMPLATE_ID == templateId && x.TA.TSD.SHEET_NAME == sheetName && x.TA.TMD.ATTRIBUTE_ID > 0).Distinct()
                .Select(x => new { x.TA.TSD.IMPORT_TYPE, x.TA.TMD.CAPTION, x.TA.TSD.SHEET_NAME, x.TA.TMD.ATTRIBUTE_ID, x.TMD.ATTRIBUTE_NAME }).ToList();

                var lt_DuplicateAttributeNames = lt_MappingAttributes.GroupBy(x => new { x.ATTRIBUTE_ID })
                           .Where(g => g.Count() > 1)
                           .Select(g => new { g.First().IMPORT_TYPE, g.First().SHEET_NAME, g.First().CAPTION, g.First().ATTRIBUTE_NAME }).ToList();


                //var duplicates = (from jd in lt_MappingAttributes
                //                  join mms in objLS.TB_ATTRIBUTE
                // on jd.ATTRIBUTE_ID equals mms.ATTRIBUTE_ID
                // select mms.ATTRIBUTE_NAME).ToList();

                if (lt_DuplicateAttributeNames.Count > 0 && lt_DuplicateAttributeNames != null)
                {
                    string jsonResults = @"{'Table1':" + JsonConvert.SerializeObject(lt_DuplicateAttributeNames);
                    DataSet ds_dupliacteAttributeName = JsonConvert.DeserializeObject<DataSet>(jsonResults);



                    DataColumn dc_ErrorColumn = new System.Data.DataColumn("Error message", typeof(System.String));
                    dc_ErrorColumn.DefaultValue = "An attribute is associated with one or more captions";
                    ds_dupliacteAttributeName.Tables[0].Columns.Add(dc_ErrorColumn);

                    HttpContext.Current.Session["DuplicateMappingAttributes"] = ds_dupliacteAttributeName;

                    str_DuplicateMappingAttributesResult = "DUPLICATE_MAPPING_ATTRIBUTES~" + sessionId;



                }

                return str_DuplicateMappingAttributesResult;
            }


            catch (Exception ex)
            {
                _logger.Error("Error at CheckDuplicateMappingAttributeNames function:ImportApiController", ex);
                return null;
            }



        }

        public static string ExcelColumnFromNumber(int column)
        {
            string columnString = "";
            decimal columnNumber = column;
            while (columnNumber > 0)
            {
                decimal currentLetterNumber = (columnNumber - 1) % 26;
                char currentLetter = (char)(currentLetterNumber + 65);
                columnString = currentLetter + columnString;
                columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
            }
            return columnString;
        }

    }

}