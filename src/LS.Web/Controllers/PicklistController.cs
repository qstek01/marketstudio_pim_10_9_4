﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using log4net;
using LS.Data;
using LS.Data.Utilities;
using Newtonsoft.Json;
using System.Xml;
using System.Xml.Linq;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using System.Web;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;
using System.Configuration;

namespace LS.Web.Controllers
{
    class PickListCollection
    {
        public IEnumerable<PickList> Values { get; set; }
    }

    public class PickList
    {
        public string ListItem { get; set; }
    }
    public class PickListAll
    {
        public string ListItem { get; set; }
        public string Sort { get; set; }
    }
    public class DatePickList
    {
        public DateTime ListItem { get; set; }
        public string Sort { get; set; }
    }
    public class NumberPickList
    {
        public decimal ListItem { get; set; }
        public string Sort { get; set; }
    }

    public class Picklist_Values
    {
        public string PICKLIST_DATA { get; set; }
        public string PICKLIST_NAME { get; set; }
        public string PICKLIST_DATA_TYPE { get; set; }
        
    }
    public class PicklistController : Controller
    {
        static readonly ILog Logger = LogManager.GetLogger(typeof(HomeApiController));
        readonly CSEntities _dbcontext = new CSEntities();

        public JsonResult LoadPickList()
        {
            try
            {
                var pickList = _dbcontext.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                {
                    PICKLIST_DATA = s.PICKLIST_DATA,
                    PICKLIST_NAME = s.PICKLIST_NAME,
                    PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                }).ToList();
                return Json(pickList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objException)
            {
                Logger.Error("Error at PicklistController :  SavePickList", objException);
                return null;
            }
        }

        public JsonResult LoadSelectedPickList(string name)
        {
            try
            {
                var pickList = _dbcontext.TB_PICKLIST.Select(s => new LS_TB_PICKLIST
                {
                    PICKLIST_DATA = s.PICKLIST_DATA,
                    PICKLIST_NAME = s.PICKLIST_NAME,
                    PICKLIST_DATA_TYPE = s.PICKLIST_DATA_TYPE
                }).FirstOrDefault(d => d.PICKLIST_NAME == name);




                //var pickList = _dbcontext.TB_PICKLIST.Join(_dbcontext.TB_PICKLIST_DATA, Tp => Tp.PICKLIST_NAME, TPD => TPD.PICKLIST_NAME, (Tp, TPD) => new { Tp, TPD })
                //      .Where(a => a.Tp.PICKLIST_NAME == name)
                //      .Select(x => new LS_TB_PICKLIST
                //      {
                //          PICKLIST_DATA = x.TPD.PICKLIST_DATA,
                //          PICKLIST_NAME = x.Tp.PICKLIST_NAME,
                //          PICKLIST_DATA_TYPE = x.Tp.PICKLIST_DATA_TYPE,

                //      });

                //  string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;

                //  string sqlSelect = "SELECT tpd.PICKLIST_DATA,tp.PICKLIST_NAME,tp.PICKLIST_DATA_TYPE FROM TB_PICKLIST tp left join TB_PICKLIST_DATA tpd on tp.PICKLIST_NAME = tpd.PICKLIST_NAME  WHERE tp.PICKLIST_NAME= '" + name + "'";

                //  SqlConnection sqlConnection = new SqlConnection(connectionString);
                //  SqlCommand sqlCommand = new SqlCommand(sqlSelect, sqlConnection);



                //  SqlDataAdapter sqlDa = new SqlDataAdapter(sqlCommand);
                //  DataTable sqlDt = new DataTable();
                //  sqlDa.Fill(sqlDt);

                ////  List<LS_TB_PICKLIST> pickList = new List<LS_TB_PICKLIST>();
                //var pickList = (from DataRow dr in sqlDt.Rows
                //                    select new LS_TB_PICKLIST()
                //                    {
                //                        PICKLIST_DATA = dr["PICKLIST_DATA"].ToString(),
                //                        PICKLIST_NAME = dr["PICKLIST_NAME"].ToString(),
                //                        PICKLIST_DATA_TYPE = dr["PICKLIST_DATA_TYPE"].ToString()

                //                    }).ToList();
                return Json(pickList, JsonRequestBehavior.AllowGet);
                

                
            }
            catch (Exception objException)
            {
                var obLsTbPicklists = new List<LS_TB_PICKLIST>();
                Logger.Error("Error at PicklistController :  SavePickList", objException);
                return Json(obLsTbPicklists, JsonRequestBehavior.AllowGet);
            }
        }
        //public void GetValue(string val)
        //{
        //    //Json js = val;
        //    //JObject temp = JObject.Parse(val);
        //    XmlDocument doc = JsonConvert.DeserializeXmlNode("{\"Table1\":" + val + "}", "NewDataSet");
        //    string temp = doc.ToString();
        //    //XmlDocument xdoc = JsonConvert.DeserializeXmlNode(“{\”root\”:” + JsonString + “}”, “root”);
        //    //XmlDocument doc = (XmlDocument)JsonConvert.DeserializeXmlNode(temp);            
        // }
        public JsonResult SaveNewPickList(string name, string datatype)
        {
            try
            {

                var pKavail = _dbcontext.TB_PICKLIST.SingleOrDefault
                                (s => s.PICKLIST_NAME == name);
                if (pKavail == null)
                {
                    
                    var objPickList = new TB_PICKLIST
                    {
                        PICKLIST_DATA_TYPE = datatype,
                        PICKLIST_NAME = name,
                        PICKLIST_DATA = "",
                        CREATED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now,
                        CREATED_DATE = DateTime.Now,
                        MODIFIED_USER = User.Identity.Name
                    };
                    _dbcontext.TB_PICKLIST.Add(objPickList);
                    _dbcontext.SaveChanges();

                    var objPickListsort = new TB_PICKLIST_SORT
                    {
                        PICKLIST_NAME = name,
                        PICKLIST_SORT = "asc",
                    };
                    _dbcontext.TB_PICKLIST_SORT.Add(objPickListsort);
                    _dbcontext.SaveChanges();
                    return LoadPickList();
                }
                else
                {
                    return null;

                }
              
            }
            catch (Exception objException)
            {
                Logger.Error("Error at PicklistController :  SavePickList", objException);
                 return null;
            }
        
        }


        public void SavePickList(string name, string selectedpicklisttype, string[] data)
        {
            try
            {
                if (data.Length > 0)
                {
                    var picklistdata = JsonConvert.DeserializeObject<List<PickList>>(data[0]);
                    var picklistdatas = picklistdata;
                    foreach (var item in picklistdata)
                    {

                        if (string.IsNullOrEmpty(item.ListItem))
                        {
                            picklistdatas.Remove(item);
                        }
                        else if (item.ListItem.Trim() == "")
                        {
                            picklistdatas.Remove(item);
                        }
                        else
                        {
                            item.ListItem = item.ListItem.Trim();
                        }

                    }
                    if (selectedpicklisttype != null)
                    {
                        if (selectedpicklisttype.ToLower().Contains("datetime"))
                        {
                            foreach (var items in picklistdatas)
                            {
                                DateTime d22 = DateTime.Parse(items.ListItem, null,
                                    System.Globalization.DateTimeStyles.RoundtripKind);
                                string pickd = d22.ToString("MM/dd/yyyy hh:mm tt");
                                items.ListItem = pickd.Replace('-', '/');
                            }
                        }
                    }
                    var serializer1 = new JavaScriptSerializer();
                    var json = serializer1.Serialize(picklistdatas);
                    XmlDocument doc = JsonConvert.DeserializeXmlNode("{\"Table1\":" + json + "}", "NewDataSet");
                    string picklistdataxml = "<?xml version=\"1.0\" standalone=\"yes\"?>" + doc.InnerXml;
                    var objPicklist = _dbcontext.TB_PICKLIST.FirstOrDefault(s => s.PICKLIST_NAME == name);
                    if (objPicklist != null) objPicklist.PICKLIST_DATA = picklistdataxml;
                    _dbcontext.SaveChanges();





                }

            }
            catch (Exception objException)
            {
                Logger.Error("Error at PicklistController :  SavePickList", objException);
            }
        }


    }



    public class ListtoDataTable
    {
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
    }

}