﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace LS.Web.Controllers
{
    public class SuperAdminConfigController : Controller
    {
        private static ILog _logger = null;
        public SuperAdminConfigController()
        {
            _logger = LogManager.GetLogger(typeof(SuperAdminConfigController));
        }

        public ActionResult Index()
        {
            return View();
        }
        public ViewResult SuperAdminConfig()
        {
            return View("~/Views/App/SuperAdminConfig/SuperAdminConfig.cshtml");

        }

        // GET: SuperAdminConfig
        //public ActionResult SuperAdminConfig()
        //{
        //    try
        //    {
        //        return View("~/Views/App/SuperAdminConfig/SuperAdminConfig.cshtml");
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        return null;
        //    }
        //}

        //public ActionResult Customers()
        //{
        //    return View("~/Views/App/SuperAdminConfig/Customers.cshtml");
        //}

       

        //public ViewResult Customers()
        //{
        //    try
        //    {
        //        return View("~/Views/App/SuperAdminConfig/Customers.cshtml");
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error(ex);
        //        return null;
        //    }
        //}
    }
}