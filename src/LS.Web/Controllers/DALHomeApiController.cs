﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Http;
using log4net;
using LS.Data;
using LS.Data.Model.CatalogSectionModels;
using System.Reflection;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using LS.Data.Utilities;
using System.Collections;
using System.Web;
using System.Xml.Serialization;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml;
using Newtonsoft.Json.Linq;
using LS.Data.Model;
using System.Xml.Linq;
using System.Data.Entity.Core.Objects;
using System.Security.Cryptography;
using LS.Web.Utility;
using Kendo.DynamicLinq;
using Stimulsoft.Report;
using System.Text.RegularExpressions;
using System.Diagnostics;
using Stimulsoft.Report.Export;
using System.Web.Mvc.Html;
using System.Web.UI.WebControls;
using WebGrease.Css.Extensions;
using Missing = System.Reflection.Missing;
using LS.Data.Model.Reports;
using LS.Web.Models;
using System.Transactions;
using System.Data.Entity.Validation;
using System.Linq.Dynamic;
using LS.Web.Common;
using System.Net.Mail;
using LS.Data.Model.Accounts;
using Ionic.Zip;
using Infragistics.Documents.Excel;
using Stimulsoft.Report.Dictionary;
using SpreadsheetGear.Drawing;
using Stimulsoft.Report.Components;
using LS.Web.Utility;
using DataEncrypterDecrypter;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;


namespace LS.Web.Controllers
{
    public class DALHomeApiController : ApiController
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(HomeApiController));
        private readonly CSEntities _dbcontext = new CSEntities();
        private int catalogId = Convert.ToInt32(ConfigurationManager.AppSettings["CatalogID"]);
        private string exportPath = Convert.ToString(ConfigurationManager.AppSettings["Export_Path"]);
        private string hostedUrl = Convert.ToString(ConfigurationManager.AppSettings["Hosted_URL"]);
        private string registryUrl = Convert.ToString(ConfigurationManager.AppSettings["Registry_URL"]);
        private string imagePath = Convert.ToString(ConfigurationManager.AppSettings["Image_Path"]);
        private string highRes = Convert.ToString(ConfigurationManager.AppSettings["Image_HighResolution_Path"]);
        private string highRes_Psd = Convert.ToString(ConfigurationManager.AppSettings["Image_HighResolution_Psd_Path"]);

        private string imageDriveFlag = Convert.ToString(ConfigurationManager.AppSettings["External_Drive_Flag"]);
        private string imageHostedURL = Convert.ToString(ConfigurationManager.AppSettings["Image_Hosted_URL"]);
        private string imageHostedSharedPath = Convert.ToString(ConfigurationManager.AppSettings["Image_Hosted_Shared_Path"]);

        DBAccess objDBAccess = new DBAccess();
        private string Resource_Group = "RESOURCES";
        //    private static StiReport Report = new StiReport();
        //AmazonUploader uploader = new AmazonUploader();


        [System.Web.Http.HttpGet]
        public DataSet GetCategoryFamilyList(string categoryId, string option, string resouceTypeId, int familyId, int userGroupId, int userId)
        {
            try
            {
                var objDataSet = new DataSet();
                string imgHostedUrl = "/Images/";
                if (imageDriveFlag == "true")
                {
                    imgHostedUrl = imageHostedURL;
                }
                if (catalogId != 0)
                {
                    List<SqlParameter> ParameterList = new List<SqlParameter>();
                    SqlParameter objSqlParameter = null;
                    objSqlParameter = new SqlParameter("@OPTION", SqlDbType.VarChar);
                    objSqlParameter.Value = option;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@PARENT_CATEGORY", SqlDbType.VarChar);
                    objSqlParameter.Value = categoryId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@CATALOG_ID", SqlDbType.Int);
                    objSqlParameter.Value = catalogId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@FAMILY_ID", SqlDbType.Int);
                    objSqlParameter.Value = familyId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@RESOURCE_TYPE_ID", SqlDbType.VarChar);
                    objSqlParameter.Value = resouceTypeId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@CATEGORY_ID", SqlDbType.VarChar);
                    objSqlParameter.Value = categoryId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@USER_GROUP_ID", SqlDbType.Int);
                    objSqlParameter.Value = userGroupId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@USER_ID", SqlDbType.Int);
                    objSqlParameter.Value = userId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@HIGH_RES_IMAGE_PATH", SqlDbType.VarChar);
                    objSqlParameter.Value = highRes;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@IMAGE_HOSTED_URL", SqlDbType.VarChar);
                    objSqlParameter.Value = imgHostedUrl;
                    ParameterList.Add(objSqlParameter);

                    Object getObject = objDBAccess.ExcecuteSTP("STP_QSWS_CATEGORY_FMAILY_PRODUCT_NAVIGATION", ParameterList.ToArray(), UseCommandType.ExecuteDataSet);

                    objDataSet = (DataSet)getObject;
                    if (option.ToUpper() != "MENU_CATEGORY")
                    {
                        int i = 0;
                        if (objDataSet.Tables.Count > 0 && objDataSet.Tables[0].Rows.Count > 0)
                        {
                            objDataSet.Tables[0].Columns.Add("ENABLE_DOWNLOAD", typeof(Int32));
                            foreach (DataRow dr in objDataSet.Tables[0].Rows)
                            {
                                objDataSet.Tables[0].Rows[i]["IMAGE_FILE"] = ConstructImgUrl(Convert.ToString(dr["IMAGE_FILE"]));
                                if (objDataSet.Tables[0].Rows[i]["IMAGE_FILE"].ToString().ToLower().Contains("noimage"))
                                {
                                    objDataSet.Tables[0].Rows[i]["ENABLE_DOWNLOAD"] = 0;
                                }
                                else
                                {
                                    objDataSet.Tables[0].Rows[i]["ENABLE_DOWNLOAD"] = 1;
                                }
                                i++;
                            }
                        }
                    }
                }

                return objDataSet;

            }
            catch (Exception objexception)
            {
                _logger.Error("Error at HomeApiController : GetCategoryFamilyList", objexception);
                return null;
            }

        }



        /// <summary>
        /// To get the breadcrumb list data
        /// </summary>
        /// <param name="categoryId">passes the category id</param>
        /// <param name="familyId">passes the family id</param>
        /// <param name="productId">passes the product id</param>
        /// <param name="breadcrumbOption">passes the breadcrumb option</param>
        /// <returns>returns breadcrumb data as datatable</returns>
        [System.Web.Http.HttpGet]
        public DataTable GetBreadCrumbList(string categoryId, int familyId, int productId, string breadcrumbOption)
        {
            try
            {
                var objDataTable = new DataTable();

                if (catalogId != 0)
                {
                    List<SqlParameter> ParameterList = new List<SqlParameter>();
                    SqlParameter objSqlParameter = null;
                    objSqlParameter = new SqlParameter("@OPTION", SqlDbType.VarChar);
                    objSqlParameter.Value = breadcrumbOption;
                    ParameterList.Add(objSqlParameter);

                    objSqlParameter = new SqlParameter("@CATALOG_ID", SqlDbType.Int);
                    objSqlParameter.Value = catalogId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@FAMILY_ID", SqlDbType.Int);
                    objSqlParameter.Value = familyId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@PRODUCTID", SqlDbType.VarChar);
                    objSqlParameter.Value = productId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@CATEGORY_ID", SqlDbType.VarChar);
                    objSqlParameter.Value = categoryId;
                    ParameterList.Add(objSqlParameter);


                    Object getObject = objDBAccess.ExcecuteSTP("STP_QSWS_GET_BREADCRUMB_DATA", ParameterList.ToArray(), UseCommandType.ExecuteDataTable);
                    objDataTable.Clear();
                    objDataTable = (DataTable)getObject;
                }

                return objDataTable;

            }
            catch (Exception objexception)
            {
                _logger.Error("Error at HomeApiController : GetBreadCrumbList", objexception);
                return null;
            }

        }

        /// <summary>
        /// To get the product details
        /// </summary>
        /// <param name="categoryId">passes the categoryid</param>
        /// <param name="familyId">passes the familyid</param>
        /// <param name="productId">passes the product id</param>
        /// <returns>returns product details as datatable</returns>
        [System.Web.Http.HttpGet]
        public DataSet GetProductDetails(string categoryId, int familyId, int productId, int userId)
        {
            try
            {
                var objDataSet = new DataSet();
                string imgHostedUrl = "/Images/";
                if (imageDriveFlag == "true")
                {
                    imgHostedUrl = imageHostedURL;
                }
                if (catalogId != 0)
                {
                    List<SqlParameter> ParameterList = new List<SqlParameter>();
                    SqlParameter objSqlParameter = null;
                    objSqlParameter = new SqlParameter("@CATALOG_ID", SqlDbType.Int);
                    objSqlParameter.Value = catalogId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@USER_ID", SqlDbType.Int);
                    objSqlParameter.Value = userId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@FAMILY_ID", SqlDbType.Int);
                    objSqlParameter.Value = familyId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@PRODUCT_ID", SqlDbType.VarChar);
                    objSqlParameter.Value = productId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@CATEGORY_ID", SqlDbType.VarChar);
                    objSqlParameter.Value = categoryId;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@HIGH_RES_IMAGE_PATH", SqlDbType.VarChar);
                    objSqlParameter.Value = highRes;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@HIGH_RES_PSD_IMAGE_PATH", SqlDbType.VarChar);
                    objSqlParameter.Value = highRes_Psd;
                    ParameterList.Add(objSqlParameter);
                    objSqlParameter = new SqlParameter("@IMAGE_HOSTED_URL", SqlDbType.VarChar);
                    objSqlParameter.Value = imgHostedUrl;
                    ParameterList.Add(objSqlParameter);

                    Object getObject = objDBAccess.ExcecuteSTP("STP_QSWS_GETPRODUCT_DETAILS", ParameterList.ToArray(), UseCommandType.ExecuteDataSet);
                    objDataSet.Clear();
                    objDataSet = (DataSet)getObject;

                    int i = 0;
                    if (objDataSet.Tables.Count > 0 && objDataSet.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow dr in objDataSet.Tables[2].Rows)
                        {
                            objDataSet.Tables[2].Rows[i]["STRING_VALUE"] = ConstructImgUrl(Convert.ToString(dr["STRING_VALUE"]));
                            objDataSet.Tables[2].Rows[i]["HIGH_RESOLUTION"] = ConstructImgUrl(Convert.ToString(dr["HIGH_RESOLUTION"]));
                            objDataSet.Tables[2].Rows[i]["HIGH_RESOLUTION_PSD"] = ConstructImgUrl(Convert.ToString(dr["HIGH_RESOLUTION_PSD"]));
                            i++;
                        }
                    }
                    i = 0;
                    //if (objDataSet.Tables.Count > 0 && objDataSet.Tables[1].Rows.Count > 0)
                    //{
                    //    foreach (DataRow dr in objDataSet.Tables[1].Rows)
                    //    {
                    //        if (Convert.ToString(dr["ATTRIBUTE_NAME"]).ToLower().Contains("specsheet"))
                    //        {
                    //            if (Convert.ToString(objDataSet.Tables[1].Rows[i]["STRING_VALUE"]) != null && Convert.ToString(objDataSet.Tables[1].Rows[i]["STRING_VALUE"]) != "")
                    //            {
                    //                objDataSet.Tables[1].Rows[i]["STRING_VALUE"] = ConstructImgUrl(Convert.ToString(dr["STRING_VALUE"]));
                    //            }
                    //            break;
                    //        }
                    //        //objDataSet.Tables[2].Rows[i]["STRING_VALUE"] = ConstructImgUrl(Convert.ToString(dr["STRING_VALUE"]));
                    //        //objDataSet.Tables[2].Rows[i]["HIGH_RESOLUTION"] = ConstructImgUrl(Convert.ToString(dr["HIGH_RESOLUTION"]));
                    //        i++;
                    //    }
                    //}
                }

                return objDataSet;

            }
            catch (Exception objexception)
            {
                _logger.Error("Error at HomeApiController : GetProductDetails", objexception);
                return null;
            }

        }


        /// <summary>
        /// To check the cart item type 
        /// </summary>
        /// <param name="categoryId">Passes the categoryId</param>
        /// <param name="familyId">passes the familyId</param>
        /// <param name="productId">passes the productId</param>
        /// <returns>returns the item details as the datatable</returns>
        [System.Web.Http.HttpPost]
        public DataTable CartItemTypeCheck(string categoryId, int familyId, int productId)
        {
            try
            {
                DataTable dt_ItemType = new DataTable();
                List<SqlParameter> parameterList = new List<SqlParameter>();
                SqlParameter parameterDetails = new SqlParameter();

                parameterDetails = new SqlParameter("@OPTION", SqlDbType.VarChar) { Value = "GET_CART_ITEM_TYPE" };
                parameterList.Add(parameterDetails);

                parameterDetails = new SqlParameter("@FAMILY_ID", SqlDbType.Int) { Value = familyId };
                parameterList.Add(parameterDetails);

                object response = objDBAccess.ExcecuteSTP("STP_QSWS_SHOPPING_CART", parameterList.ToArray(), UseCommandType.ExecuteDataTable);

                if (response != null)
                {
                    dt_ItemType = (DataTable)response;
                }
                return dt_ItemType;
            }
            catch (Exception exception)
            {
                _logger.Error("Error at HomeApiController : CartItemCheck", exception);
                return null;
            }
        }


        [System.Web.Http.HttpPost]
        public string AddDetailsToCart(int userId, int attributeId, string pricingClass, int familyId, int productId, string filePath)
        {
            try
            {
                string result = string.Empty;
                //var productCheck = _dbcontext.QSWS_SHOPPING_CART.Where(x => x.ATTRIBUTE_ID == attributeId && x.USER_ID == userId && x.PRODUCT_ID == productId && x.IS_PLACED == 0).ToList();
                int orderId = _dbcontext.QSWS_SHOPPING_CART_ORDER.Where(x => x.USER_ID == userId && x.IS_PLACED == 0).Select(x => x.ORDER_ID).FirstOrDefault();
                var productCheck = _dbcontext.QSWS_SHOPPING_CART_ORDER_ITEM.Where(x => x.ATTRIBUTE_ID == attributeId && x.PRODUCT_ID == productId && x.ORDER_ID == orderId).ToList();
                if (productCheck.Count() == 0)
                {
                    DataTable dt_CartData = new DataTable();
                    List<SqlParameter> obj_ParamList = new List<SqlParameter>();
                    SqlParameter obj_Param = new SqlParameter();

                    obj_Param = new SqlParameter("@OPTION", SqlDbType.VarChar);
                    obj_Param.Value = "ADD_TO_CART";
                    obj_ParamList.Add(obj_Param);

                    obj_Param = new SqlParameter("@USER_ID", SqlDbType.VarChar);
                    obj_Param.Value = userId;
                    obj_ParamList.Add(obj_Param);

                    obj_Param = new SqlParameter("@ATTRIBUTE_ID", SqlDbType.Int);
                    obj_Param.Value = attributeId;
                    obj_ParamList.Add(obj_Param);

                    obj_Param = new SqlParameter("@FAMILY_ID", SqlDbType.VarChar);
                    obj_Param.Value = familyId;
                    obj_ParamList.Add(obj_Param);

                    obj_Param = new SqlParameter("@PRODUCT_ID", SqlDbType.VarChar);
                    obj_Param.Value = productId;
                    obj_ParamList.Add(obj_Param);

                    obj_Param = new SqlParameter("@FILE_PATH", SqlDbType.VarChar);
                    obj_Param.Value = filePath;
                    obj_ParamList.Add(obj_Param);

                    object rightsRes = objDBAccess.ExcecuteSTP("STP_QSWS_SHOPPING_CART", obj_ParamList.ToArray(), UseCommandType.ExecuteNonQuery);

                    result = "success";
                }
                else
                {
                    result = "This file already exist.";
                }
                return result;
            }
            catch (Exception exce)
            {
                _logger.Error("Error at HomeApiController : AddDetailsToCart", exce);
                return null;
            }
        }

        /// To generate the resource PDF for the selected product
        /// </summary>
        /// <param name="familyId">passes the familyid as input</param>
        /// <param name="productId">passes the productId as input</param>
        /// <param name="pdfType">passes the pdfType as input</param>
        [System.Web.Http.HttpPost]
        public string GenerateResourcePDF(int familyId, int productId, string pdfType, string pdfFor, int attributeId, int userId, int cobrandFlag)
        {
            try
            {
                string logoImagePath = ConfigurationManager.AppSettings["MRT_Logo_Image_Path"].ToString();
                string solarlogoImagePath = ConfigurationManager.AppSettings["MRT_SolarLogo_Image_Path"].ToString();
                if (pdfFor == "thumbnail")
                {
                    pdfFor = "preview";
                }
                if (cobrandFlag == 1)
                {
                    string directoryPath = Path.Combine(HttpContext.Current.Server.MapPath("~/TempAttachments/Cobranding/" + User.Identity.Name));
                    if (Directory.Exists(directoryPath))
                    {
                        string[] filePaths = Directory.GetFiles(directoryPath);
                        if (filePaths.Length > 0)
                        {
                            logoImagePath = filePaths[0].ToString();
                        }
                        else
                        {
                            return "Kindly select co-branding logo to proceed.";
                        }
                    }
                    else
                    {
                        return "Kindly select co-branding logo to proceed.";
                    }
                }
                string response = string.Empty;
                DataSet dS_UserData = new DataSet();
                string userName = User.Identity.Name;
                string optionValue = string.Empty;
                string MRTpath = string.Empty;
                int resolutionValue = 300;
                int cropBoxEnable = 0;
                string templateType = string.Empty;
                int familyProdCount = 0;
                int solarFamilyProdCount = 0;
                //var productCheck = _dbcontext.QSWS_SHOPPING_CART.Where(x => x.ATTRIBUTE_ID == attributeId && x.PRODUCT_ID == productId && x.FAMILY_ID == x.FAMILY_ID && x.USER_ID == userId && x.IS_PLACED == 0).ToList();
                int orderId = _dbcontext.QSWS_SHOPPING_CART_ORDER.Where(x => x.USER_ID == userId && x.IS_PLACED == 0).Select(x => x.ORDER_ID).FirstOrDefault();
                var productCheck = _dbcontext.QSWS_SHOPPING_CART_ORDER_ITEM.Where(x => x.ATTRIBUTE_ID == attributeId && x.PRODUCT_ID == productId && x.ORDER_ID == orderId).ToList();
                if (productCheck.Count == 0 || pdfFor != "print")
                {
                    Guid currId = Guid.NewGuid();
                    string sessionId = Regex.Replace(currId.ToString(), "[^a-zA-Z0-9_]+", "");
                    if (pdfType.Trim().ToLower() == "spec_card")
                    {
                        optionValue = "GET_SPEC_CARD_DATA";
                        MRTpath = Path.Combine(ConfigurationManager.AppSettings["SpecCard"]);
                        templateType = "SpecCard";
                    }
                    else if (pdfType.Trim().ToLower() == "spec_sheet")
                    {
                        optionValue = "GET_SPEC_SHEET_DATA";
                    }
                    //string savedName=_dbcontext.QSWS_USER.Where(x=>(x.USER_FNAME+x.USER_LNAME))
                    List<SqlParameter> obj_ParamList = new List<SqlParameter>();
                    SqlParameter obj_Param = new SqlParameter();

                    obj_Param = new SqlParameter("@OPTION", SqlDbType.VarChar);
                    obj_Param.Value = optionValue;
                    obj_ParamList.Add(obj_Param);

                    obj_Param = new SqlParameter("@FAMILY_ID", SqlDbType.VarChar);
                    obj_Param.Value = familyId;
                    obj_ParamList.Add(obj_Param);

                    obj_Param = new SqlParameter("@PRODUCT_ID", SqlDbType.VarChar);
                    obj_Param.Value = productId;
                    obj_ParamList.Add(obj_Param);

                    obj_Param = new SqlParameter("@SESSION_ID", SqlDbType.VarChar);
                    obj_Param.Value = sessionId;
                    obj_ParamList.Add(obj_Param);

                    obj_Param = new SqlParameter("@GENERATION_TYPE", SqlDbType.VarChar);
                    obj_Param.Value = pdfFor;
                    obj_ParamList.Add(obj_Param);

                    obj_Param = new SqlParameter("@HIGH_RES_IMAGE_PATH", SqlDbType.VarChar);
                    obj_Param.Value = highRes;
                    obj_ParamList.Add(obj_Param);

                    object rightsRes = objDBAccess.ExcecuteSTP("STP_PDF_GENERATION_DATA", obj_ParamList.ToArray(), UseCommandType.ExecuteDataSet);

                    if (rightsRes != null)
                    {
                        dS_UserData = (DataSet)rightsRes;
                    }

                    if (pdfType.Trim().ToLower() == "spec_sheet")
                    {
                        DataTable dt_templateType = new DataTable();
                        dt_templateType = dS_UserData.Tables[0];
                        templateType = dt_templateType.Rows[0]["TEMPLATE_TYPE"].ToString();
                        MRTpath = string.Format("{0}/{1}_LR.mrt", Path.Combine(ConfigurationManager.AppSettings["TemplatePath"]), templateType);
                        //dS_UserData.Tables.RemoveAt(0);
                        if (templateType.ToLower().Contains("solar") && cobrandFlag == 0)
                        {
                            logoImagePath = solarlogoImagePath;
                        }
                        if (pdfFor.Trim().ToLower() == "print")
                        {
                            //string NewTemplateType = string.Format("{0}{1}", templateType, "_crop");
                            //MRTpath = Path.Combine(ConfigurationManager.AppSettings[NewTemplateType]);
                            MRTpath = string.Format("{0}/{1}_HR.mrt", Path.Combine(ConfigurationManager.AppSettings["TemplatePath"]), templateType);
                        }

                    }
                    //Guid currId = Guid.NewGuid();
                    //string sessionId = Regex.Replace(currId.ToString(), "[^a-zA-Z0-9_]+", "");
                    string itemNo = string.Empty;
                    string familyName = string.Empty;
                    string date = string.Empty;
                    string prefix = string.Empty;
                    string resolutionType = string.Empty;
                    string fileNameType = string.Empty;
                    if (dS_UserData.Tables.Count > 0 && dS_UserData.Tables[0].Rows.Count > 0)
                    {
                        //------------------------get date format --------------------------------//
                        string dateFormat = dS_UserData.Tables[0].Rows[0]["SUFFIX"].ToString();

                        if (dateFormat.ToLower() == "mmddyy")
                        {
                            date = DateTime.Today.ToString("MM/dd/yy").Replace("/", "");
                        }
                        else if (dateFormat.ToLower() == "ddmmyy")
                        {
                            date = DateTime.Today.ToString("dd/MM/yy").Replace("/", "");
                        }
                        else if (dateFormat.ToLower() == "ddmmyyyy")
                        {
                            date = DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "");
                        }
                        else if (dateFormat.ToLower() == "mmddyyyy")
                        {
                            date = DateTime.Today.ToString("MM/dd/yyyy").Replace("/", "");
                        }
                        date = date != string.Empty ? "_" + date : date;
                        //------------------------get  prefix --------------------------------//

                        prefix = dS_UserData.Tables[0].Rows[0]["PREFIX"].ToString();

                        //------------------------get resolution type --------------------------------//
                        string lowRes = dS_UserData.Tables[0].Rows[0]["PDF_LOW_RES"].ToString();
                        highRes = dS_UserData.Tables[0].Rows[0]["PDF_HIGH_RES"].ToString();
                        //string highRes = dS_UserData.Tables[0].Rows[0]["PDF_HIGH_RES"].ToString();
                        resolutionType = pdfFor.Trim().ToLower().Equals("print") ? highRes : lowRes;
                        resolutionType = resolutionType != string.Empty ? "_" + resolutionType : resolutionType;
                        //------------------------get name--------------------------------//
                        fileNameType = dS_UserData.Tables[0].Rows[0]["FILE_NAME"].ToString();
                        //---------------------------------------Group count check-------------------------------------------//
                        if (templateType.ToLower().Contains("8_page"))
                        {
                            familyProdCount = Int32.Parse(dS_UserData.Tables[4].Rows[0]["PRODUCT_SIZE"].ToString());
                            if (dS_UserData.Tables[2].Rows.Count == 0)
                            {
                                response = "File generation cancelled~nogroup";
                                return response;
                            }
                        }
                        else if (templateType.ToLower().Contains("2_page") && templateType.ToLower().Contains("solar"))
                        {
                            solarFamilyProdCount = Int32.Parse(dS_UserData.Tables[5].Rows[0]["PRODUCT_SIZE"].ToString());
                            if (dS_UserData.Tables[1].Rows.Count == 0)
                            {
                                response = "File generation cancelled~nogroup";
                                return response;
                            }
                        }
                        else if (templateType.ToLower().Contains("2_page"))
                        {
                            if (dS_UserData.Tables[1].Rows.Count == 0)
                            {
                                response = "File generation cancelled~nogroup";
                                return response;
                            }
                        }
                        //---------------------------------------Group count check-------------------------------------------//
                        if ((pdfType.Trim().ToLower().Equals("speccard") || pdfType.Trim().ToLower().Equals("spec_card")) && dS_UserData.Tables.Count > 0 && dS_UserData.Tables[1].Rows.Count > 0)
                        {
                            if (fileNameType.ToLower().Equals("product model"))
                            {
                                familyName = Convert.ToString(dS_UserData.Tables[1].Rows[0]["ITEM#"]);
                            }
                            else
                            {
                                familyName = Convert.ToString(dS_UserData.Tables[1].Rows[0]["FILE_NAME"]);
                            }

                        }
                        else if (dS_UserData.Tables.Count > 1 && pdfType.Trim().ToLower().Equals("spec_sheet"))
                        {
                            // familyName = string.Format("{0}_{1}", Convert.ToString(dS_UserData.Tables[2].Rows[0]["FILE_NAME"]), date);
                            if (fileNameType.ToLower().Equals("product model") && dS_UserData.Tables[3].Rows.Count > 0)
                            {
                                DataTable getFileNameDT = new DataTable();
                                getFileNameDT = templateType.ToLower().Contains("2_page") ? dS_UserData.Tables[2] : dS_UserData.Tables[3];

                                foreach (DataRow dr in getFileNameDT.Rows)
                                {
                                    familyName = familyName.Equals(string.Empty) ? dr["ITEM#"].ToString() : string.Format("{0}-{1}", familyName, dr["ITEM#"].ToString());
                                }
                            }
                            else
                            {
                                familyName = Convert.ToString(dS_UserData.Tables[2].Rows[0]["FILE_NAME"]);
                            }
                        }
                        familyName = familyName != string.Empty ? "_" + familyName : familyName;
                    }
                    string FilePath = Path.Combine(ConfigurationManager.AppSettings["FilePath"]);
                    //string templateName = pdfType.Trim().ToLower().Equals("spec_card") ? string.Format(" LG_5x7_EVENT_SPEC_CARD_{0}_{1}_LR", itemNo, date) : string.Format("LG_SPEC-SHEET_{0}_LR", familyName);
                    string templateName = string.Format("{0}{1}{2}{3}", prefix, familyName, date, resolutionType);
                    if (pdfFor.Trim().ToLower() == "print")
                    {
                        resolutionValue = 1200;
                        cropBoxEnable = 1;
                        // templateName = pdfType.Trim().ToLower().Equals("spec_card") ? string.Format(" LG_5x7_EVENT_SPEC_CARD_{0}_{1}_HR", itemNo, date) : string.Format("LG_SPEC-SHEET_{0}_HR", familyName);
                    }
                    response = PDFGeneration(MRTpath, FilePath, sessionId, templateName, resolutionValue, templateType, cropBoxEnable, logoImagePath, dS_UserData, pdfFor);
                    if (familyProdCount > 4)
                    {
                        response = response + "~exceeded4";
                    }
                    else if (solarFamilyProdCount > 2)
                    {
                        response = response + "~exceeded2";
                    }

                }
                else
                {
                    response = "This file already exist.";
                }
                return response;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at HomeApiController : GenerateResourcePDF", ex);
                return null;
            }
        }


        /// <summary>
        /// To check weather the file exists in the server path before accessing
        /// </summary>
        /// <param name="filePath">Passes the file path which need to check for existance</param>
        /// <returns>returns true or false as boolean</returns>
        [System.Web.Http.HttpPost]
        public bool CheckFileExits(string filePath)
        {
            try
            {
                bool result = false;
                string fullPath = string.Empty;

                if (imageDriveFlag == "true" && filePath.ToLower().Contains("http"))
                {
                    fullPath = filePath;
                    //string localPath = new Uri(fullPath).LocalPath;
                    if (filePath.ToLower().Contains("http"))
                    {
                        fullPath = new Uri(fullPath).LocalPath;
                    }
                    fullPath = string.Format("{1}{0}", fullPath, imageHostedSharedPath);
                }
                //if (imageDriveFlag == "false" || filePath.ToLower().Contains("pdfstore/"))
                else
                {
                    fullPath = HttpContext.Current.Server.MapPath("~") + filePath;
                }
                if (File.Exists(fullPath))
                {
                    result = true;
                }
                return result;
            }
            catch (Exception excep)
            {
                _logger.Error("Error at HomeApiController : CustomerFeedBackProcess", excep);
                return false;
            }
        }



        /// <summary>
        /// To compress the images in the product
        /// </summary>
        /// <param name="itemNumber">passes the item number as input</param>
        /// <param name="imageList">passes image list array as input</param>
        /// <returns>returns compressed path or null</returns>
        [System.Web.Http.HttpPost]
        public string GetImageCompressDetails(string itemNumber, string downloadFormat, List<ImageList> imageList)
        {
            //  string[] files = path.Split(',');
            try
            {
                string zipName = String.Format("LG_{0}_{1}.zip", itemNumber, downloadFormat); //, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss")
                Guid currId = Guid.NewGuid();
                string sessionId = Regex.Replace(currId.ToString(), "[^a-zA-Z0-9_]+", "");
                string compressedPath = HttpContext.Current.Server.MapPath("~") + "Compress//" + sessionId + "//" + zipName;
                string outputPath = "Compress//" + sessionId + "//" + zipName;
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    zip.AddDirectoryByName("Files");
                    foreach (var image in imageList)
                    {
                        string filePath = string.Empty;
                        if (downloadFormat.ToLower() == "jpg")
                        {
                            filePath = image.HIGH_RESOLUTION;
                        }
                        else if (downloadFormat.ToLower() == "psd")
                        {
                            filePath = image.HIGH_RESOLUTION_PSD;
                        }
                        if (imageDriveFlag == "true" && filePath.ToLower().Contains("http"))
                        {
                            string localPath = new Uri(filePath).LocalPath;
                            filePath = string.Format("{1}{0}", localPath, imageHostedSharedPath);
                        }
                        //if (imageDriveFlag == "false" || path.ToLower().Contains("pdfstore/"))
                        else
                        {
                            filePath = HttpContext.Current.Server.MapPath("~") + filePath.ToString().TrimStart('/');
                        }
                        if (File.Exists(filePath))
                            zip.AddFile(filePath, "Files");
                    }
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.BufferOutput = false;
                    HttpContext.Current.Response.ContentType = "application/zip";
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~") + "Compress//" + sessionId))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~") + "Compress//" + sessionId);
                    }
                    if (File.Exists(compressedPath))
                    {
                        File.Delete(compressedPath);
                    }
                    //    zip.AddDirectory(compressedPath);
                    zip.Save(compressedPath);
                    //   HttpContext.Current.Response.End();
                    //ī
                }
                return outputPath;
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at HomeApiController : FormatSize", objexception);
                return null;
            }
        }






        [System.Web.Http.HttpPost]
        //public string UpdateLinkDetails(int familyId, int productId, string option, int userId, int userGroupId)
        public string UpdateLinkDetails(List<LINK_DETAILS> list)
        {
            //int result = 0;
            try
            {
                string product_Link = string.Empty;
                int productId = Convert.ToInt32(list[0].PRODUCT_ID);
                int attributeId = Convert.ToInt32(list[0].ATTRIBUTE_ID);
                int familyId = Convert.ToInt32(list[0].FAMILY_ID);
                int userId = Convert.ToInt32(list[0].USER_ID);
                string imageName = Path.GetFileName(list[0].IMAGE);
                if (list[0].IMAGE == null)
                {
                    list[0].IMAGE = "";
                }
                string encryProductId = Convert.ToString(list[0].PRODUCT_ID);
                encryProductId = HttpUtility.UrlEncode(CryptoEngine.Encrypt(encryProductId, "sblw-3hn8-sqoy19"));
                string encryFamilyId = Convert.ToString(list[0].FAMILY_ID);
                encryFamilyId = HttpUtility.UrlEncode(CryptoEngine.Encrypt(encryFamilyId, "sblw-3hn8-sqoy19"));
                string encryCategoryId = Convert.ToString(list[0].CATEGORY_ID);
                encryCategoryId = HttpUtility.UrlEncode(CryptoEngine.Encrypt(encryCategoryId, "sblw-3hn8-sqoy19"));
                var objProdCount = _dbcontext.QSWS_PRODUCT_LINK_DETAILS.ToList().Where(z => z.PRODUCT_ID == productId && z.USER_ID == userId && z.ATTRIBUTE_ID == attributeId && Convert.ToDateTime(z.CREATED_DATE).ToString("MM/dd/yyyy") == DateTime.Now.ToString("MM/dd/yyyy")).ToList();

                //  int userId = Convert.ToInt32(list[0].USER_ID);
                var userDetails = _dbcontext.QSWS_USER.Where(x => x.USER_ID == userId).FirstOrDefault();
                //var res = _dbcontext.QSWS_PRODUCT_LINK_DETAILS.Where(a => DateTime.ParseExact(a.CREATED_DATE, "dd-MM-yyyy", CultureInfo.InvariantCulture) > DateTime.now).ToList();
                if (!objProdCount.Any())
                {
                    int lastcount = 0;
                    var linkExistsCheck = _dbcontext.QSWS_PRODUCT_LINK_DETAILS.ToList();
                    if (linkExistsCheck.Count() > 0)
                    {
                        lastcount = _dbcontext.QSWS_PRODUCT_LINK_DETAILS.OrderByDescending(x => x.ID).FirstOrDefault().ID;
                    }
                    lastcount = lastcount + 1;

                    string encryLinkId = Convert.ToString(lastcount);
                    encryLinkId = HttpUtility.UrlEncode(CryptoEngine.Encrypt(encryLinkId, "sblw-3hn8-sqoy19"));
                    if (list[0].TYPE.ToLower() == "productasset")
                    {


                        if (list[0].STRING_VALUE != null && list[0].STRING_VALUE != "")
                        {
                            bool resultflag = false;

                            string filePath = list[0].STRING_VALUE;
                            string fileNewPath = string.Empty;
                            string localPath = filePath;
                            Guid currId = Guid.NewGuid();
                            string sessionId = Regex.Replace(currId.ToString(), "[^a-zA-Z0-9_]+", "");

                            if (imageDriveFlag == "true" && filePath.ToLower().Contains("http"))
                            {

                                if (localPath.ToLower().Contains("http"))
                                {
                                    localPath = new Uri(localPath).LocalPath;
                                }
                                localPath = string.Format("{1}{0}", localPath, imageHostedSharedPath);
                                if (File.Exists(localPath))
                                {
                                    resultflag = true;
                                }

                            }
                            else
                            {
                                filePath = HttpContext.Current.Server.MapPath(filePath);

                                localPath = filePath;
                                if (File.Exists(filePath))
                                {
                                    resultflag = true;
                                }
                            }
                            string combinedPath = Path.Combine(HttpContext.Current.Server.MapPath("~") + "ShareAsset\\" + userDetails.USER_EMAIL);
                            if (!Directory.Exists(combinedPath))
                            {
                                Directory.CreateDirectory(combinedPath);
                            }

                            string extension = Path.GetExtension(localPath);
                            string filename = sessionId + extension;
                            imageHostedSharedPath = Path.Combine("ShareAsset\\" + userDetails.USER_EMAIL + "\\" + filename);
                            fileNewPath = Path.Combine(combinedPath + "\\" + filename);
                            File.Copy(localPath, fileNewPath);
                            product_Link = string.Format("{0}LinkPreview.ashx?linkid={2}&path={1}", ConfigurationManager.AppSettings["Hosted_URL"], imageHostedSharedPath, encryLinkId);
                            //  product_Link = list[0].STRING_VALUE;

                        }
                        else
                        {
                            string path = GenerateResourcePDF(list[0].FAMILY_ID, list[0].PRODUCT_ID, list[0].ATTRIBUTE_NAME, "productassetlink", attributeId, userId, list[0].CO_BRAND_FLAG);
                            list[0].STRING_VALUE = path;
                            product_Link = string.Format("{0}LinkPreview.ashx?linkid={2}&path={1}", ConfigurationManager.AppSettings["Hosted_URL"], list[0].STRING_VALUE, encryLinkId);

                        }
                    }
                    else
                    {
                        product_Link = string.Format("{0}?productid={1}&familyid={2}&categoryid={3}&linkid={4}", ConfigurationManager.AppSettings["PRODUCT_LINK"], encryProductId, encryFamilyId, encryCategoryId, encryLinkId);
                    }
                    SqlParameter obj_Param = new SqlParameter();
                    List<SqlParameter> obj_ParamList = new List<SqlParameter>();
                    obj_Param = new SqlParameter("@OPTION", SqlDbType.VarChar);
                    obj_Param.Value = list[0].OPTION;
                    obj_ParamList.Add(obj_Param);
                    obj_Param = new SqlParameter("@PRODUCT_LINK", SqlDbType.VarChar);
                    obj_Param.Value = product_Link;
                    obj_ParamList.Add(obj_Param);
                    obj_Param = new SqlParameter("@FAMILY_ID", SqlDbType.Int);
                    obj_Param.Value = list[0].FAMILY_ID;
                    obj_ParamList.Add(obj_Param);
                    obj_Param = new SqlParameter("@PRODUCT_ID", SqlDbType.Int);
                    obj_Param.Value = list[0].PRODUCT_ID;
                    obj_ParamList.Add(obj_Param);
                    obj_Param = new SqlParameter("@ATTRIBUTE_ID", SqlDbType.Int);
                    obj_Param.Value = list[0].ATTRIBUTE_ID;
                    obj_ParamList.Add(obj_Param);
                    obj_Param = new SqlParameter("@USER_ID", SqlDbType.Int);
                    obj_Param.Value = list[0].USER_ID;
                    obj_ParamList.Add(obj_Param);
                    obj_Param = new SqlParameter("@USER_GROUP_ID", SqlDbType.Int);
                    obj_Param.Value = list[0].USER_GROUP;
                    obj_ParamList.Add(obj_Param);
                    obj_Param = new SqlParameter("@TYPE", SqlDbType.VarChar);
                    obj_Param.Value = list[0].TYPE;
                    obj_ParamList.Add(obj_Param);
                    obj_Param = new SqlParameter("@VISITED_IMAGE", SqlDbType.VarChar);
                    obj_Param.Value = list[0].IMAGE;
                    obj_ParamList.Add(obj_Param);
                    obj_Param = new SqlParameter("@CATALOG_ID", SqlDbType.VarChar);
                    obj_Param.Value = catalogId;
                    obj_ParamList.Add(obj_Param);
                    object getresult = objDBAccess.ExcecuteSTP("STP_QSWS_INSERT_LINK_DETAILS", obj_ParamList.ToArray(), UseCommandType.ExecuteNonQuery);

                }
                else
                {
                    var checkCount = _dbcontext.QSWS_PRODUCT_LINK_DETAILS.ToList().Where(z => z.PRODUCT_ID == productId && z.USER_ID == userId && z.ATTRIBUTE_ID == attributeId && Convert.ToDateTime(z.CREATED_DATE).ToString("MM/dd/yyyy") == DateTime.Now.ToString("MM/dd/yyyy")).ToList();

                    int lastcount = 0;
                    if (checkCount.Any())
                    {
                        lastcount = checkCount[0].ID;

                    }
                    //_dbcontext.QSWS_PRODUCT_LINK_DETAILS.Where(z => z.PRODUCT_ID == productId && z.USER_ID == userId && z.ATTRIBUTE_ID==attributeId &&  Convert.ToDateTime(z.CREATED_DATE).ToString("MM/dd/yyyy") == DateTime.Now.ToString("MM/dd/yyyy")).Select(x => x.ID).FirstOrDefault();
                    string encryLinkId = Convert.ToString(lastcount);
                    encryLinkId = HttpUtility.UrlEncode(CryptoEngine.Encrypt(encryLinkId, "sblw-3hn8-sqoy19"));
                    if (list[0].TYPE.ToLower() == "productasset")
                    {
                        if (list[0].STRING_VALUE != null && list[0].STRING_VALUE != "")
                        {
                            // product_Link = list[0].STRING_VALUE;
                            if (objProdCount[0].PRODUCT_LINK.ToLower().Contains("linkpreview"))
                                product_Link = objProdCount[0].PRODUCT_LINK;
                            else
                                product_Link = string.Format("{0}LinkPreview.ashx?linkid={2}&path={1}", ConfigurationManager.AppSettings["Hosted_URL"], list[0].STRING_VALUE, encryLinkId);

                        }
                        else
                        {
                            product_Link = objProdCount[0].PRODUCT_LINK;
                        }
                    }
                    else
                    {

                        product_Link = string.Format("{0}?productid={1}&familyid={2}&categoryid={3}&linkid={4}", ConfigurationManager.AppSettings["PRODUCT_LINK"], encryProductId, encryFamilyId, encryCategoryId, encryLinkId);
                    }
                }

                return product_Link;

            }
            catch (Exception ex)
            {
                _logger.Error("Error at HomeApiController : GetSearchResults", ex);
                return "Please try again later...";
            }
        }




        /// <summary>
        /// To update the user logged in flag
        /// </summary>
        /// <param name="userId">passes userId as input</param>
        /// <returns>returns success / null</returns>
        [System.Web.Http.HttpPost]
        public string UserAgreementUpdate(int userId)
        {
            try
            {
                string response = "";
                var userDetails = _dbcontext.QSWS_USER.Where(x => x.USER_ID == userId).FirstOrDefault();
                if (userDetails != null)
                {
                   // userDetails.LOGGED_IN_FLAG = 1;
                    _dbcontext.SaveChanges();
                    response = "success";
                }
                return response;
            }
            catch (Exception excep)
            {
                _logger.Error("Error at HomeApiController : UserAgreementUpdate", excep);
                return null;
            }
        }
        /// <summary>
        /// To construct the image URL for the imagepath
        /// </summary>
        /// <param name="filePath">passes the file path</param>
        /// <returns>returns thr file url</returns>
        public string ConstructImgUrl(string filePath)
        {
            try
            {
                string imageUrl = string.Empty;
                string fullPath = string.Empty;
                imageUrl = filePath;
                if (imageDriveFlag == "true" && filePath.ToLower().Contains("http"))
                {
                    fullPath = filePath;
                    //string localPath = new Uri(fullPath).LocalPath;
                    if (filePath.ToLower().Contains("http"))
                    {
                        fullPath = new Uri(fullPath).LocalPath;
                    }
                    fullPath = string.Format("{1}{0}", fullPath, imageHostedSharedPath);
                }
                //if (imageDriveFlag == "false" || filePath.ToLower().Contains("pdfstore/"))
                else
                {
                    fullPath = filePath;
                }
                if (imageDriveFlag == "false" && !File.Exists(string.Format("{0}\\{1}", HttpContext.Current.Server.MapPath("~"), fullPath)))
                {
                    imageUrl = "\\images\\NoImage.jpg";
                }
                else if (imageDriveFlag == "true" && !File.Exists(fullPath))
                {
                    imageUrl = "\\images\\NoImage.jpg";
                }

                return imageUrl; //+ ImageUrl;
            }

            catch (Exception ex)
            {
                _logger.Error("Error at HomeApiController : ConstructImgUrl", ex);
                return null;
            }
        }

        /// <summary>
        /// PDF generation process
        /// </summary>
        /// <param name="MRTpath">passes templatepath with template name</param>
        /// <param name="FilePath">passes the filepath where the generated PDF to be placed</param>
        /// <param name="sessionId">passes the sessionId</param>
        /// <param name="templateName">passes the name for the generated PDF</param>
        /// <param name="objPdfDataset">passes the datasource for the template</param>
        [System.Web.Http.HttpPost]
        public string PDFGeneration(string MRTpath, string FilePath, string sessionId, string templateName, int resolutionValue, string templateType, int cropBoxEnable, string logoImagePath, DataSet objPdfDataset, string pdfFor)
        {
            try
            {
                if (pdfFor == "productassetlink")
                {
                    templateName = sessionId;

                }
                string imagePath = Path.Combine(ConfigurationManager.AppSettings["Image_Path"]);
                string newFilePath = string.Empty;
                string serverPath = string.Empty;
                string staticImagePath = ConfigurationManager.AppSettings["MRT_Static_Image_Path"].ToString();
                if (imageDriveFlag == "true")
                {
                    imagePath = imageHostedURL;
                }
                string productSize = "";
                //Stimulsoft.Base.StiLicense.Key = "6vJhGtLLLz2GNviWmUTrhSqnOItdDwjBylQzQcAOiHnr2qFO+d2hfjhwpsUCOAtL6kw20sZKyFsTgshTbHHEHxfMwtG4DIL1OeU0LXIf18zR8GYiLo5NSg1eeyZl0rDJepNnEEL2sXPnbN3H0xPCX2Des0uLLhLaFH/Uj8N2knfXcqtV4oO9kMDRq6sDyKzzoGhgH8ulxr2EHC5R1gcHGwcnpIzVLqiQ7p5sVpmU29kPkvAKts6Mcrxk5W+bspqiZPS9gd6hyTjgG/U39GsP9VRFE1GKu+DSYo2uwoWRSXtMdhfOJqCLavJYXHhZC+/ct2Wh5Dq7YvgrOrG7+g77vW1pkTfZMkKKacH+bkf3D5Q2Pvo8quQJNUxLoiaBTpJkUTtp8Ms6Hcf9AdpIdXtyNmESOmM7foUzzuXuAxlymXj3S4T6cpViCn5t8Gl5yvR/TDLxoVqhwLaeI/xtk8ebIeoIjs4BiYQL42dAZK2DQSqGjdksaV5qDrFlwS6wttIUZx0M6iPWMY011A1eLRusum60v2Nql4dBd4ECpEF0gPT1GWpwJjmWbAc5SArH7Xhf+fe1bDF2VZhzfmgl8qimNsFtypUN6dQgwyh2JTdE3FUTpgwZGNKctm+3ecZYxZImtq9PoauCCpVClByBY++vHV6BG4iDYMKRO6HWoqukI2nY6K83mb0bd3xQBcVJqiuG";
                var Report = new StiReport();
                Report.Load(MRTpath);
                int pagesCount = Report.Pages.Count;
                _logger.Debug("Error at HomeApiController : pagesCount" + pagesCount);
                if (templateType.ToLower().Contains("speccard"))
                {
                    _logger.Debug("Error at HomeApiController :----------" + "SpecCard");
                    objPdfDataset.Tables[1].TableName = "PROD_ITEM_DATA";
                    objPdfDataset.Tables[2].TableName = "PROD_SORT_DATA";
                    _logger.Debug("Error at HomeApiController : TableName---------" + "PROD_ITEM_DATA");
                    _logger.Debug("Error at HomeApiController : TableName--------" + "PROD_ITEM_DATA");
                    _logger.Debug("Error at HomeApiController : TableName0-------" + objPdfDataset.Tables[1].Rows[0]["FAMILY_ID"].ToString());
                    _logger.Debug("Error at HomeApiController : TableName0-------" + objPdfDataset.Tables[1].Rows[0]["FAMILY_NAME"].ToString());
                    _logger.Debug("Error at HomeApiController : TableName0-------" + objPdfDataset.Tables[1].Rows[0]["PRODUCT_ID"].ToString());
                    _logger.Debug("Error at HomeApiController : TableName0-------" + objPdfDataset.Tables[1].Rows[0]["ITEM#"].ToString());
                    _logger.Debug("Error at HomeApiController : TableName1-------FAMILY_ID-------" + objPdfDataset.Tables[2].Rows[0]["FAMILY_ID"].ToString());
                    _logger.Debug("Error at HomeApiController : TableName1------- PRODUCT_ID-----" + objPdfDataset.Tables[2].Rows[0]["PRODUCT_ID"].ToString());
                    _logger.Debug("Error at HomeApiController : TableName1-------ATTRIBUTE_ID----" + objPdfDataset.Tables[2].Rows[0]["ATTRIBUTE_ID"].ToString());
                    _logger.Debug("Error at HomeApiController : TableName1-------STRING_VALUE---" + objPdfDataset.Tables[2].Rows[0]["STRING_VALUE"].ToString());
                }
                else if (templateType.ToLower().Contains("2_page"))
                {
                    _logger.Debug("Error at HomeApiController : pagesCount---" + "2_page");
                    _logger.Debug("Error at HomeApiController : TableName----" + "PROD_SPECS");
                    _logger.Debug("Error at HomeApiController : TableName----" + "PROD_ITEM");
                    _logger.Debug("Error at HomeApiController : TableName----" + "FAM_COVER_PAGE");
                    objPdfDataset.Tables[1].TableName = "PROD_SPECS";
                    objPdfDataset.Tables[2].TableName = "PROD_ITEM";
                    objPdfDataset.Tables[3].TableName = "FAM_COVER_PAGE";
                    _logger.Debug("Error at HomeApiController : TableName1-------" + objPdfDataset.Tables[1].Rows[0][0].ToString());
                    _logger.Debug("Error at HomeApiController : TableName2-------" + objPdfDataset.Tables[2].Rows[0][0].ToString());
                    _logger.Debug("Error at HomeApiController : TableName3---------" + objPdfDataset.Tables[3].Rows[0][0].ToString());
                }
                else if (templateType.ToLower().Contains("8_page"))
                {
                    _logger.Debug("Error at HomeApiController :" + "8_page");
                    _logger.Debug("Error at HomeApiController : TableName-----" + "FAMILY_DATA");
                    _logger.Debug("Error at HomeApiController : TableName-----" + "PROD_SPEC_DATA");
                    _logger.Debug("Error at HomeApiController : TableName-----" + "PROD_ITEM_DATA");
                    _logger.Debug("Error at HomeApiController : TableName1-----" + objPdfDataset.Tables[1].Rows[0][0].ToString());
                    _logger.Debug("Error at HomeApiController : TableName2------" + objPdfDataset.Tables[2].Rows[0][0].ToString());
                    _logger.Debug("Error at HomeApiController : TableName3-----" + objPdfDataset.Tables[3].Rows[0][0].ToString());
                    objPdfDataset.Tables[1].TableName = "FAMILY_DATA";
                    objPdfDataset.Tables[2].TableName = "PROD_SPEC_DATA";
                    objPdfDataset.Tables[3].TableName = "PROD_ITEM_DATA";
                    DataTable dt_ProductSize = new DataTable();
                    dt_ProductSize = objPdfDataset.Tables[4];
                    productSize = dt_ProductSize.Rows[0]["PRODUCT_SIZE"].ToString();
                    _logger.Debug("Error at HomeApiController : TableName" + productSize);
                }
                Report.Dictionary.Databases.Clear();
                Report.Dictionary.DataSources.Clear();
                //Report.Dictionary.Variables["PAGE_NUMBER"].Value = PageNumber;
                Report.RegData(objPdfDataset);
                Report.Dictionary.Synchronize();
                if (templateType.ToLower().Contains("8_page"))
                {
                    StiDataRelation dataRelation = new StiDataRelation("Item_Relation", Report.Dictionary.DataSources["PROD_ITEM_DATA"],
                        Report.Dictionary.DataSources["PROD_SPEC_DATA"], new System.String[] { "PRODUCT_ID", "FAMILY_ID" }, new System.String[] { "PRODUCT_ID", "FAMILY_ID" });
                    Report.Dictionary.RegRelations();
                    Report.Dictionary.Relations.Add(dataRelation);
                }
                if (Report.Dictionary.Variables.Contains("IMAGE_PATH") == true)
                    Report.Dictionary.Variables["IMAGE_PATH"].Value = imagePath;
                if (Report.Dictionary.Variables.Contains("PRODUCT_SIZE") == true)
                    Report.Dictionary.Variables["PRODUCT_SIZE"].Value = productSize;
                if (Report.Dictionary.Variables.Contains("CROPBOX_FLAG") == true)
                    Report.Dictionary.Variables["CROPBOX_FLAG"].Value = cropBoxEnable.ToString();
                if (Report.Dictionary.Variables.Contains("STATIC_IMAGE_PATH") == true)
                    Report.Dictionary.Variables["STATIC_IMAGE_PATH"].Value = staticImagePath;
                if (Report.Dictionary.Variables.Contains("LOGO_IMAGE") == true)
                    Report.Dictionary.Variables["LOGO_IMAGE"].Value = logoImagePath;
                ///////Harish-07July
                if (templateType.ToLower().Contains("solar"))
                {
                    objPdfDataset.Tables[6].TableName = "TABLE_SORT_ORDER";
                    _logger.Debug("Error at HomeApiController : TableName6-------" + objPdfDataset.Tables[6].Rows[0][0].ToString());
                    DataTable dtGroupHeader = objPdfDataset.Tables[6];//objPdfDataset.Tables[6].TableName
                    int nuRightcnt = 0;
                    int nuLeftcnt = 0;
                    string strGroupHeader = "";
                    for (int cntGH = 0; cntGH < dtGroupHeader.Rows.Count; cntGH++)
                    {
                        if (cntGH % 2 == 0)
                        {
                            nuLeftcnt++;
                            strGroupHeader = "GroupHeader_Left" + nuLeftcnt;
                            //     if (Report.Dictionary.Variables.Contains(strGroupHeader) == true)
                            //     Report.Dictionary.Variables[strGroupHeader].Value = dtGroupHeader.Rows[cntGH]["GROUP_NAME"].ToString();   
                        }
                        else
                        {
                            nuRightcnt++;
                            strGroupHeader = "GroupHeader_Right" + nuRightcnt;
                            // Report.Dictionary.Variables[strGroupHeader].Value = dtGroupHeader.Rows[cntGH]["GROUP_NAME"].ToString();   
                        }
                        if (Report.Dictionary.Variables.Contains(strGroupHeader) == true)
                            Report.Dictionary.Variables[strGroupHeader].Value = dtGroupHeader.Rows[cntGH]["GROUP_NAME"].ToString();
                    }
                }
                ////////////
                Report.Render(false);

                FilePath = HttpContext.Current.Server.MapPath("~");
                if (cropBoxEnable == 1)
                {
                    newFilePath = Path.Combine(FilePath + "/PdfStore/" + sessionId);
                    serverPath = "/PdfStore/" + sessionId;
                }
                else
                {
                    string folderStore = "/TempPDF/";
                    if (pdfFor == "productassetlink")
                    {
                        folderStore = "/ShareAsset/";
                    }
                    string tempDirectoryPath = Path.Combine(FilePath + folderStore + User.Identity.Name + "/");
                    if (pdfFor != "productassetlink" && Directory.Exists(tempDirectoryPath))
                    {
                        Directory.Delete(tempDirectoryPath, true);
                    }
                    newFilePath = pdfFor != "productassetlink" ? Path.Combine(FilePath + folderStore + User.Identity.Name + "/" + sessionId) : Path.Combine(FilePath + folderStore + User.Identity.Name);
                    serverPath = pdfFor != "productassetlink" ? folderStore + User.Identity.Name + "/" + sessionId : folderStore + User.Identity.Name;
                }
                if (!Directory.Exists(newFilePath))
                {
                    Directory.CreateDirectory(newFilePath);
                }
                string fileName = string.Empty;
                if (pdfFor == "productassetlink")
                {
                    fileName = newFilePath + "/" + sessionId + ".pdf";
                    serverPath = serverPath + "/" + sessionId + ".pdf";
                }
                else
                {
                    fileName = newFilePath + "/" + templateName + ".pdf";
                    serverPath = serverPath + "/" + templateName + ".pdf";
                }
                _logger.Debug("Error at HomeApiController : ReportRender start -----" + templateName + ".pdf");
                Stream ostream = new FileStream(newFilePath + "/" + templateName + ".pdf", FileMode.OpenOrCreate, FileAccess.Write);
                var pdfSettings = new StiPdfExportSettings
                {
                    EmbeddedFonts = true,
                    ImageQuality = 0.9f,
                    ImageResolution = resolutionValue,
                    Compressed = true,
                    UseUnicode = true,
                    ImageCompressionMethod = StiPdfImageCompressionMethod.Jpeg
                };
                Report.ExportDocument(StiExportFormat.Pdf, ostream, pdfSettings);
                ostream.Dispose();
                ostream.Close();
                _logger.Debug("Error at HomeApiController : ReportRender Successfully -----" + templateName + ".pdf");
                return serverPath;
            }
            catch (Exception excep)
            {
                _logger.Debug("Error at HomeApiController" + excep.Message);
                _logger.Error("Error at HomeApiController : PDFGeneration", excep);
                return null;
            }
        }

        /// <summary>
        /// To obtain the logged in user details
        /// </summary>
        /// <returns>Returns user details as datatable</returns>
        [System.Web.Http.HttpPost]
        public Tuple<DataTable, string, string, string> GetLoggedUserInfo()
        {
            try
            {
                DataTable dt_UserData = new DataTable();
               // string userName = User.Identity.Name;
                string userName = "scott.kelley@lge.com";
                //string savedName=_dbcontext.QSWS_USER.Where(x=>(x.USER_FNAME+x.USER_LNAME))
                List<SqlParameter> obj_ParamList = new List<SqlParameter>();
                SqlParameter obj_Param = new SqlParameter();

                obj_Param = new SqlParameter("@OPTION", SqlDbType.VarChar);
                obj_Param.Value = "GET_LOGGEDIN_USER";
                obj_ParamList.Add(obj_Param);

                obj_Param = new SqlParameter("@GROUP_NAME", SqlDbType.VarChar);
                obj_Param.Value = userName;
                obj_ParamList.Add(obj_Param);

                object rightsRes = objDBAccess.ExcecuteSTP("STP_QSWS_USERDTAILS", obj_ParamList.ToArray(), UseCommandType.ExecuteDataTable);

                if (rightsRes != null)
                {
                    dt_UserData = (DataTable)rightsRes;
                }
                if (dt_UserData.Rows.Count > 0)
                {

                }
                else
                {
                    dt_UserData = null;
                }
                Tuple<DataTable, string, string, string> result = new Tuple<DataTable, string, string, string>(dt_UserData, hostedUrl, imageHostedURL, imageHostedSharedPath);
                return result;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at HomeApiController : GetLoggedUserInfo");
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public string LineCardPDFGeneration(string generationType, List<LineCardData> lineCardData)
        {
            string pdfRes = string.Empty;
            try
            {
                string lineCardAttr = ConfigurationManager.AppSettings["Linecard_Atrribute"].ToString();

                int resolutionValue = 300;
                string MRTpath = string.Empty;
                string templateType = "LR";
                int templateId = lineCardData[0].TEMPLATE_ID;
                string categoryId = lineCardData[0].CATEGORY_ID;
                string lineCardTemplate = _dbcontext.TB_CATEGORY_SPECS.Join(_dbcontext.TB_ATTRIBUTE, TCS => TCS.ATTRIBUTE_ID, TA => TA.ATTRIBUTE_ID, (TCS, TA) => new { TCS, TA }).
                    Where(TOT => TOT.TA.ATTRIBUTE_NAME == lineCardAttr && TOT.TCS.CATEGORY_ID == categoryId).Select(TOT => TOT.TCS.STRING_VALUE).FirstOrDefault().ToString();
                DataSet dS_lineCardData = new DataSet();
                Guid currId = Guid.NewGuid();
                string sessionId = Regex.Replace(currId.ToString(), "[^a-zA-Z0-9_]+", "");
                string resolutionType = string.Empty;
                string templateName = string.Empty;
                string templatePath = string.Empty;
                string logoImagePath = Path.Combine(ConfigurationManager.AppSettings["Linecard_Logo"]);
                string logoImageVal = string.Empty;
                string FilePath = Path.Combine(ConfigurationManager.AppSettings["FilePath"]);
                if (generationType.Trim().ToLower() == "print")
                {
                    resolutionValue = 1200;
                    templateType = "HR";
                }
                //====================================================================set iteration calculation===============================================//
                int productCount = lineCardData.Count;
                int totalSet = 0;
                int set1_Product = 0;
                int set2_Product = 0;
                int set3_Product = 0;
                int set4_Product = 0;
                if (productCount <= 4)
                {
                    totalSet = 1;
                    set1_Product = productCount;
                }
                else
                {
                    if (productCount % 4 == 0)
                    {
                        totalSet = productCount / 4;
                        set1_Product = 4;
                        set2_Product = 4;
                        set3_Product = 4;
                        set4_Product = 4;
                    }
                    else if (productCount % 3 == 0)
                    {
                        totalSet = productCount / 3;
                        set1_Product = 3;
                        set2_Product = 3;
                        set3_Product = 3;
                        set4_Product = 3;
                    }
                    else
                    {
                        switch (productCount)
                        {
                            case 5:
                                {
                                    totalSet = 2;
                                    set1_Product = 3;
                                    set2_Product = 2;
                                    break;
                                }
                            case 7:
                                {
                                    totalSet = 2;
                                    set1_Product = 4;
                                    set2_Product = 3;
                                    break;
                                }
                            case 10:
                                {
                                    totalSet = 3;
                                    set1_Product = 4;
                                    set2_Product = 4;
                                    set3_Product = 2;
                                    break;
                                }
                            case 11:
                                {
                                    totalSet = 3;
                                    set1_Product = 4;
                                    set2_Product = 4;
                                    set3_Product = 3;
                                    break;
                                }
                            case 13:
                                {
                                    totalSet = 4;
                                    set1_Product = 4;
                                    set2_Product = 3;
                                    set3_Product = 3;
                                    set4_Product = 3;
                                    break;
                                }
                            case 14:
                                {
                                    totalSet = 4;
                                    set1_Product = 4;
                                    set2_Product = 4;
                                    set3_Product = 4;
                                    set4_Product = 2;
                                    break;
                                }
                        }
                    }
                }
                //====================================================================set iteration calculation===============================================//  
                for (int iterationCount = 0; iterationCount <= totalSet; iterationCount++)
                {
                    int productIterationCount = 0;
                    if (iterationCount != 0)
                    {
                        int startCount = 0;
                        int endCount = 0;
                        if (iterationCount == 1)
                        {
                            productIterationCount = set1_Product;
                            startCount = 1;
                            endCount = set1_Product;
                        }
                        else if (iterationCount == 2)
                        {
                            productIterationCount = set2_Product;
                            startCount = 1 + set1_Product;
                            endCount = set1_Product + set2_Product;
                        }
                        else if (iterationCount == 3)
                        {
                            productIterationCount = set3_Product;
                            startCount = 1 + set1_Product + set2_Product;
                            endCount = set1_Product + set2_Product + set3_Product;
                        }
                        else if (iterationCount == 4)
                        {
                            productIterationCount = set4_Product;
                            startCount = 1 + set1_Product + set2_Product + set3_Product;
                            endCount = set1_Product + set2_Product + set3_Product + set4_Product;
                        }
                        string productIds = string.Empty;
                        for (int prodCount = startCount; prodCount <= endCount; prodCount++)
                        {
                            productIds = productIds + "," + lineCardData[prodCount - 1].PRODUCT_ID;
                        }
                        productIds = productIds.TrimStart(',');
                        List<SqlParameter> obj_ParamList = new List<SqlParameter>();
                        SqlParameter obj_Param = new SqlParameter();

                        obj_Param = new SqlParameter("@OPTION", SqlDbType.VarChar);
                        obj_Param.Value = "GET_LINECARD_PDF_DATA";
                        obj_ParamList.Add(obj_Param);

                        obj_Param = new SqlParameter("@TEMPLATE_ID", SqlDbType.Int);
                        obj_Param.Value = templateId;
                        obj_ParamList.Add(obj_Param);

                        obj_Param = new SqlParameter("@CATEGORY_ID", SqlDbType.VarChar);
                        obj_Param.Value = categoryId;
                        obj_ParamList.Add(obj_Param);

                        obj_Param = new SqlParameter("@PRODUCT_IDS", SqlDbType.VarChar);
                        obj_Param.Value = productIds;
                        obj_ParamList.Add(obj_Param);

                        obj_Param = new SqlParameter("@SESSION_ID", SqlDbType.VarChar);
                        obj_Param.Value = sessionId;
                        obj_ParamList.Add(obj_Param);

                        obj_Param = new SqlParameter("@GENERATION_TYPE", SqlDbType.VarChar);
                        obj_Param.Value = generationType;
                        obj_ParamList.Add(obj_Param);

                        obj_Param = new SqlParameter("@HIGH_RES_IMAGE_PATH", SqlDbType.VarChar);
                        obj_Param.Value = highRes;
                        obj_ParamList.Add(obj_Param);

                        object rightsRes = objDBAccess.ExcecuteSTP("STP_QSWS_LINECARD_COMPARE", obj_ParamList.ToArray(), UseCommandType.ExecuteDataSet);
                        if (rightsRes != null)
                        {
                            dS_lineCardData = (DataSet)rightsRes;
                        }
                        if (dS_lineCardData.Tables.Count > 0 && dS_lineCardData.Tables[0].Rows.Count > 0)
                        {
                            if (lineCardTemplate != null && lineCardTemplate != "")
                            {
                                templatePath = string.Format("{0}_Page_Template.mrt", lineCardTemplate);
                            }
                            else
                            {
                                templatePath = string.Format("LineCard_Page_Template.mrt");
                            }
                        }
                    }
                    else
                    {
                        DataTable d1 = new DataTable();
                        DataTable d2 = new DataTable();
                        dS_lineCardData.Tables.Add(d1);
                        dS_lineCardData.Tables.Add(d2);
                        productIterationCount = 0;
                        templatePath = "LineCard_Logo_Template.mrt";
                        string[] imageList = Directory.GetFiles(logoImagePath);
                        foreach (string fileNameVal in imageList)
                        {
                            if (fileNameVal.Contains(lineCardTemplate))
                            {
                                logoImageVal = fileNameVal;
                            }
                        }
                    }
                    templateName = iterationCount.ToString();
                    MRTpath = string.Format("{0}/Line_Card/{1}", Path.Combine(ConfigurationManager.AppSettings["TemplatePath"]), templatePath);
                    pdfRes = PDFGeneration(MRTpath, FilePath, sessionId, templateName, resolutionValue, dS_lineCardData, generationType, productIterationCount, logoImageVal);
                }
                string date = string.Empty;
                string prefix = string.Empty;
                string fileName = string.Empty;
                string dateFormat = dS_lineCardData.Tables[2].Rows[0]["SUFFIX"].ToString();

                if (dateFormat.ToLower() == "mmddyy")
                {
                    date = DateTime.Today.ToString("MM/dd/yy").Replace("/", "");
                }
                else if (dateFormat.ToLower() == "ddmmyy")
                {
                    date = DateTime.Today.ToString("dd/MM/yy").Replace("/", "");
                }
                else if (dateFormat.ToLower() == "ddmmyyyy")
                {
                    date = DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "");
                }
                else if (dateFormat.ToLower() == "mmddyyyy")
                {
                    date = DateTime.Today.ToString("MM/dd/yyyy").Replace("/", "");
                }
                date = date != string.Empty ? "_" + date : date;
                //------------------------get  prefix --------------------------------//
                prefix = dS_lineCardData.Tables[2].Rows[0]["PREFIX"].ToString();
                fileName = string.Format("{0}_{1}", prefix, date);
                pdfRes = mergePDF(sessionId, FilePath, totalSet, fileName);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at LineCardApi : LineCardPDFGeneration", ex);
                pdfRes = null;
            }
            return pdfRes;
        }

        [System.Web.Http.HttpPost]
        public string mergePDF(string sessionId, string filePath, int pdfCount, string fileName)
        {
            string finalRes = string.Empty;
            try
            {
                PdfDocument readdoc;
                var mergedDocument = new PdfDocument();
                string newFilePath = Path.Combine(filePath + @"\Linecard_PdfStore\" + sessionId);
                string serverPath = @"\Linecard_PdfStore\" + sessionId;
                string[] pdfFiles = Directory.GetFiles(newFilePath, "*.pdf").OrderBy(x => new FileInfo(x).CreationTime).ToArray();
                if (pdfFiles.Length > pdfCount)
                {
                    foreach (var file in pdfFiles)
                    {
                        readdoc = PdfReader.Open(file, PdfDocumentOpenMode.Import);
                        foreach (PdfPage pg in readdoc.Pages)
                        {
                            mergedDocument.AddPage(pg);
                        }
                    }
                    mergedDocument.Save(newFilePath + @"\" + fileName + "_" + sessionId + ".pdf");
                    mergedDocument.Dispose();
                    foreach (var file in pdfFiles)
                    {
                        File.Delete(file);
                    }
                    finalRes = serverPath + @"\" + fileName + "_" + sessionId + ".pdf";
                }
                else
                {
                    finalRes = "null";
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at HomeApiController : DeleteGridValues", objexception);
                finalRes = "null";
            }
            return finalRes;
        }

        [System.Web.Http.HttpPost]
        public string PDFGeneration(string MRTpath, string FilePath, string sessionId, string templateName, int resolutionValue, DataSet objPdfDataset, string pdfFor, int productCount, string logoImageVal)
        {
            try
            {
                if (pdfFor == "productassetlink")
                {
                    templateName = sessionId;

                }
                string imagePath = Path.Combine(ConfigurationManager.AppSettings["Image_Path"]);
                string newFilePath = string.Empty;
                string serverPath = string.Empty;
                string staticImagePath = logoImageVal.Replace(imageHostedSharedPath, imageHostedURL);
                if (imageDriveFlag == "true")
                {
                    imagePath = imageHostedURL;
                }
                //Stimulsoft.Base.StiLicense.Key = "6vJhGtLLLz2GNviWmUTrhSqnOItdDwjBylQzQcAOiHnr2qFO+d2hfjhwpsUCOAtL6kw20sZKyFsTgshTbHHEHxfMwtG4DIL1OeU0LXIf18zR8GYiLo5NSg1eeyZl0rDJepNnEEL2sXPnbN3H0xPCX2Des0uLLhLaFH/Uj8N2knfXcqtV4oO9kMDRq6sDyKzzoGhgH8ulxr2EHC5R1gcHGwcnpIzVLqiQ7p5sVpmU29kPkvAKts6Mcrxk5W+bspqiZPS9gd6hyTjgG/U39GsP9VRFE1GKu+DSYo2uwoWRSXtMdhfOJqCLavJYXHhZC+/ct2Wh5Dq7YvgrOrG7+g77vW1pkTfZMkKKacH+bkf3D5Q2Pvo8quQJNUxLoiaBTpJkUTtp8Ms6Hcf9AdpIdXtyNmESOmM7foUzzuXuAxlymXj3S4T6cpViCn5t8Gl5yvR/TDLxoVqhwLaeI/xtk8ebIeoIjs4BiYQL42dAZK2DQSqGjdksaV5qDrFlwS6wttIUZx0M6iPWMY011A1eLRusum60v2Nql4dBd4ECpEF0gPT1GWpwJjmWbAc5SArH7Xhf+fe1bDF2VZhzfmgl8qimNsFtypUN6dQgwyh2JTdE3FUTpgwZGNKctm+3ecZYxZImtq9PoauCCpVClByBY++vHV6BG4iDYMKRO6HWoqukI2nY6K83mb0bd3xQBcVJqiuG";
                var Report = new StiReport();
                Report.Load(MRTpath);
                int pagesCount = Report.Pages.Count;
                _logger.Debug("Error at LinecardApi : pagesCount" + pagesCount);

                objPdfDataset.Tables[0].TableName = "TEMP_LINECART_HEADER";
                objPdfDataset.Tables[1].TableName = "TEMP_LINECART_DATA";

                Report.Dictionary.Databases.Clear();
                Report.Dictionary.DataSources.Clear();
                //Report.Dictionary.Variables["PAGE_NUMBER"].Value = PageNumber;
                Report.RegData(objPdfDataset);
                Report.Dictionary.Synchronize();
                if (templateName != "0")
                {
                    StiDataRelation dataRelation = new StiDataRelation("Name", Report.Dictionary.DataSources["TEMP_LINECART_HEADER"],
                            Report.Dictionary.DataSources["TEMP_LINECART_DATA"], new System.String[] { "PRODUCT_SET" }, new System.String[] { "PRODUCT_SET" });
                    Report.Dictionary.RegRelations();
                    Report.Dictionary.Relations.Add(dataRelation);
                }
                if (Report.Dictionary.Variables.Contains("IMAGE_PATH") == true)
                    Report.Dictionary.Variables["IMAGE_PATH"].Value = imagePath;
                if (Report.Dictionary.Variables.Contains("LOGO_IMAGE") == true)
                    Report.Dictionary.Variables["LOGO_IMAGE"].Value = staticImagePath;
                if (Report.Dictionary.Variables.Contains("PROD_COUNT") == true)
                    Report.Dictionary.Variables["PROD_COUNT"].Value = productCount.ToString();

                Report.Render(false);
                FilePath = HttpContext.Current.Server.MapPath("~");
                newFilePath = Path.Combine(FilePath + "/Linecard_PdfStore/" + sessionId);
                serverPath = "/Linecard_PdfStore/" + sessionId;
                if (!Directory.Exists(newFilePath))
                {
                    Directory.CreateDirectory(newFilePath);
                }
                string fileName = string.Empty;
                fileName = newFilePath + "/" + templateName + ".pdf";
                serverPath = serverPath + "/" + templateName + ".pdf";

                _logger.Debug("Error at LinecardApi : ReportRender start -----" + templateName + ".pdf");
                Stream ostream = new FileStream(newFilePath + "/" + templateName + ".pdf", FileMode.OpenOrCreate, FileAccess.Write);
                var pdfSettings = new StiPdfExportSettings
                {
                    EmbeddedFonts = true,
                    ImageQuality = 0.9f,
                    ImageResolution = resolutionValue,
                    Compressed = true,
                    UseUnicode = true,
                    ImageCompressionMethod = StiPdfImageCompressionMethod.Jpeg
                };
                Report.ExportDocument(StiExportFormat.Pdf, ostream, pdfSettings);
                ostream.Dispose();
                ostream.Close();
                _logger.Debug("Error at LinecardApi : ReportRender Successfully -----" + templateName + ".pdf");
                return serverPath;
            }
            catch (Exception excep)
            {
                _logger.Debug("Error at LineCardApi" + excep.Message);
                _logger.Error("Error at LineCardApi : PDFGeneration", excep);
                return null;
            }
        }
      
    }
}
