﻿using System.Globalization;
using LS.Data;
using LS.Data.Model;
using System;
using System.Configuration;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Mvc;
using LS.Data.Model.ProductPreview;
using Newtonsoft.Json;
using System.Data.SqlClient;
using LS.Data.Model.CatalogSectionModels;
using System.IO;
using System.Reflection;
using System.CodeDom;
using System.CodeDom.Compiler;
using log4net;
using System.Collections;
using System.Xml.Serialization;
using System.Web;
using LS.Web.Utility;
using System.Web.Security;

namespace LS.Web.Controllers
{
    public class FamilySpecs
    {
        public IEnumerable<FSValue> StringValue { get; set; }

    }


    public static class Fsattr
    {
        //    public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
        //    {
        //        try
        //        {
        //            List<T> list = new List<T>();

        //            foreach (var row in table.AsEnumerable())
        //            {
        //                T obj = new T();

        //                foreach (var prop in obj.GetType().GetProperties())
        //                {
        //                    try
        //                    {
        //                        PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
        //                        propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
        //                    }
        //                    catch
        //                    {
        //                        continue;
        //                    }
        //                }

        //                list.Add(obj);
        //            }

        //            return list;
        //        }
        //        catch
        //        {
        //            return null;
        //        }
        //    }
    }
    public class FSValue
    {
        public string FamilyId { get; set; }
        public string AttributeName { get; set; }
        public string VALUE { get; set; }
        public string AttrId { get; set; }
    }
    //public class ColumnDataType
    //{
    //    public string Title { get; set; }
    //    public Type Type { get; set; }
    //}
    //public class TotalProducts
    //{
    //    public List<ColumnDataType> Columntype { get; set; }
    //    public List<Dictionary<string, object>> GetProductsRows { get; set; }
    //}
    public class FamilyController : Controller
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        static readonly ILog Logger = LogManager.GetLogger(typeof(FamilyController));
        readonly CSEntities _dbcontext = new CSEntities();
        private readonly ILog _logger = LogManager.GetLogger(typeof(AccountsController));
        public string SqlString = "";
        private string _mFamilyId = "2", _mCatalogId = "2", _mCategoryId = "";
        public string CustomerFolder = string.Empty;
        private static readonly char[] SpecialChars = "'#&".ToCharArray();
        public string convertionPath = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePath"].ToString();
        public string convertionPathREA = System.Web.Configuration.WebConfigurationManager.AppSettings["ConvertedImagePathREA"].ToString();
        public string converttype = string.Empty;
        public string Exefile = string.Empty;

        public class RootObject
        {
            public int FamilyId { get; set; }
            public string AttributeName { get; set; }
            public string VALUE { get; set; }
            public int AttrId { get; set; }
        }

        // GET: Family
        CSEntities objLS = new CSEntities();
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetFamily(string familyId, string categoryId, int catalogId)
        {
            int famId = 0;
            if (!string.IsNullOrEmpty(familyId))
            {
                if (!familyId.Contains("~"))
                {
                    int.TryParse(familyId.Trim('~'), out famId);
                    // = Convert.ToInt32();
                }
                else
                {
                    int.TryParse(familyId.Trim('~'), out famId);
                    // string[] split = familyId.Split('~');
                    // famId = Convert.ToInt32(split[1]);
                }
            }
            //var familyDetails = objLS.TB_FAMILY.Where(s => s.FAMILY_ID == famId).Select(x => new
            //{
            //    x.FAMILY_ID,
            //    x.FAMILY_NAME,
            //    x.CATEGORY_ID,
            //    x.STATUS,
            //    x.PUBLISH,
            //    x.PUBLISH2PRINT,
            //    x.PARENT_FAMILY_ID,
            //    STATUS_NAME = objLS.TB_WORKFLOW_STATUS.FirstOrDefault(z => z.STATUS_CODE == x.WORKFLOW_STATUS).STATUS_NAME,
            //}).ToList();
            var familyDetails = objLS.TB_FAMILY.Join(objLS.TB_CATALOG_FAMILY, a => a.FAMILY_ID, b => b.FAMILY_ID, (a, b) => new { a, b })
                .Where(z => z.a.FAMILY_ID == famId && z.b.CATALOG_ID == catalogId && z.b.CATEGORY_ID == categoryId)
                .Select(x => new
            {
                x.a.FAMILY_ID,
                x.a.FAMILY_NAME,
                x.b.CATEGORY_ID,
                x.a.STATUS,
                x.a.PUBLISH,
                x.a.PUBLISH2PRINT,
                x.a.PUBLISH2WEB,
                x.a.PUBLISH2PDF,
                x.a.PUBLISH2EXPORT,
                x.a.PUBLISH2PORTAL,
                x.a.PARENT_FAMILY_ID,
                objLS.TB_WORKFLOW_STATUS.FirstOrDefault(z => z.STATUS_CODE == x.a.WORKFLOW_STATUS).STATUS_NAME,
                objLS.TB_CATEGORY.FirstOrDefault(y=>y.CATEGORY_ID==x.b.CATEGORY_ID).CATEGORY_NAME
            }).ToList();
            if (familyDetails.Count > 0)
            {
                return Json(familyDetails.ElementAt(0), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(familyDetails, JsonRequestBehavior.AllowGet);
            }
        }

        //public bool CutPasteFamily(string cutFamId, string pasteFamId, int Famid)
        //{

        //    int sortOrder = 0;
        //    var td = objLS.TB_FAMILY.Where(z => z.PARENT_FAMILY_ID == Famid)
        //        .Join(objLS.TB_CATALOG_FAMILY, a => a.CATEGORY_ID, b => b.CATEGORY_ID, (a, b) => new { b.SORT_ORDER })
        //        .Take(1).OrderByDescending(x => x.SORT_ORDER).ToList();

        //    if (td.Count == 0)
        //        sortOrder = 1;
        //    else
        //        sortOrder = td[0].SORT_ORDER + 1;


        //    var objCutCat = objLS.TB_FAMILY.FirstOrDefault(s => s.FAMILY_ID == Famid);
        //    objCutCat.PARENT_FAMILY_ID = 0;

        //    var objCutCs = objLS.TB_CATALOG_FAMILY.FirstOrDefault(s => s.CATEGORY_ID == cutFamId && s.CATALOG_ID != 1);
        //    objCutCs.SORT_ORDER = sortOrder;

        //    objLS.SaveChanges();
        //    return true;
        //}


        [HttpGet]
        public JsonResult GetAllFamilyId(int projectId, int recordId, int fileNo)
        {

            // int ProjectId = 263, RecordId = 111, FileNo = 2;
            var objSqlConnection =
        new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("Select TPSD.FAMILY_ID,TPSD.CATEGORY_ID FROM TB_PROJECT as TP join TB_PROJECT_SECTIONS as TPS on TP.PROJECT_ID =TPS.PROJECT_ID and TPS.PROJECT_ID =" + projectId + "join TB_PROJECT_SECTION_DETAILS AS TPSD on TPS.RECORD_ID=TPSD.RECORD_ID and TPS.RECORD_ID = " + recordId + " and TPS.FILE_NO =" + fileNo + "", objSqlConnection);

            objSqlConnection.Open();
            cmd.CommandTimeout = 0;
            var prodData = new SqlDataAdapter(cmd);
            DataSet prodSpecsData = new DataSet();
            prodData.Fill(prodSpecsData);
            List<string> familyIds = new List<string>();
            if (prodSpecsData != null && prodSpecsData.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < prodSpecsData.Tables[0].Rows.Count; i++)
                {
                    string familyId;
                    familyId = prodSpecsData.Tables[0].Rows[i][0].ToString(); //(prodSpecsData.Rows[i][0]).ToString();
                    familyIds.Add(familyId);
                }

                familyIds.Insert(0, prodSpecsData.Tables[0].Rows[0][1].ToString());
            }
            return Json(familyIds, JsonRequestBehavior.AllowGet);
        }


        public bool CutPasteFamily(string copiedId, string rightClickedCategoryId, int catalogID, int previousCatalogID, string multipleFamilyids, string multipleFamilyidsForAnotherCatalog, string CatIdPartsKey)
        {
            bool res = false;
            try
            {
                int flag = 1;
                bool checkFam = true;
                int copiedfamilyIdId;
                string familyCategoryId = string.Empty;
                if (!string.IsNullOrEmpty(CatIdPartsKey))
                {
                    var id = CatIdPartsKey.Split('~');
                    familyCategoryId = Convert.ToString(id[0]);
                }
                if (rightClickedCategoryId.StartsWith("~"))
                {
                    string[] multipleFamilyIds = null;
                    if (multipleFamilyids != null && multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog != null && multipleFamilyidsForAnotherCatalog.Trim() != "")
                    {
                        if (multipleFamilyids != null)
                            multipleFamilyIds = catalogID == previousCatalogID ? multipleFamilyids.Split(',') : multipleFamilyidsForAnotherCatalog.Split(',');

                        if (multipleFamilyIds != null)
                            foreach (string t in multipleFamilyIds)
                            {
                                int.TryParse(t.Trim('~'), out copiedfamilyIdId);
                                int categoryId = Convert.ToInt32(copiedfamilyIdId);
                                rightClickedCategoryId = rightClickedCategoryId.Trim('~');
                                flag = 2;
                                int pastId = Convert.ToInt32(rightClickedCategoryId);
                                checkFam = CopyPasteFamily_Check(t, rightClickedCategoryId);
                                if (checkFam)
                                {
                                    var catId = objLS.TB_CATALOG_FAMILY.Where(a => a.FAMILY_ID == pastId && a.CATALOG_ID == catalogID).Join(objLS.TB_CATEGORY, a => a.CATEGORY_ID, b => b.CATEGORY_ID, (a, b) => new { a, b })
                                        .Select(s => s.b.CATEGORY_ID).ToList();
                                    string cId = catId[0];
                                    var stp2 = objLS.STP_CATALOGSTUDIO5_Cut(cId, copiedfamilyIdId, catalogID, previousCatalogID.ToString(CultureInfo.InvariantCulture),flag);
                                    objLS.SaveChanges();
                                    //parent family_id update
                                    var objFamily = objLS.TB_FAMILY.Single(s => s.FAMILY_ID == copiedfamilyIdId);
                                    objFamily.CATEGORY_ID = cId;
                                    objFamily.PARENT_FAMILY_ID = pastId;
                                    objFamily.ROOT_FAMILY = 0;
                                    objLS.SaveChanges();
                                    //subfamily entry
                                    int sort = 1;
                                    var subfamily = objLS.TB_SUBFAMILY.FirstOrDefault(a => a.SUBFAMILY_ID == copiedfamilyIdId);
                                    var subfamilyCheck = objLS.TB_SUBFAMILY.FirstOrDefault(a => a.FAMILY_ID == pastId);
                                    if (subfamily != null)
                                    {
                                        subfamily = objLS.TB_SUBFAMILY.Remove(subfamily);
                                        var objSubFamily = new TB_SUBFAMILY
                                        {
                                            FAMILY_ID = pastId,
                                            SUBFAMILY_ID = copiedfamilyIdId,
                                            SORT_ORDER = sort + 1,
                                            CREATED_USER = User.Identity.Name,
                                            CREATED_DATE = DateTime.Now,
                                            MODIFIED_USER = User.Identity.Name,
                                            MODIFIED_DATE = DateTime.Now
                                        };
                                        objLS.TB_SUBFAMILY.Add(objSubFamily);
                                        objLS.SaveChanges();
                                        PartsKeyEntry(copiedfamilyIdId, catalogID, cId, familyCategoryId,previousCatalogID);
                                    }
                                    else
                                    {
                                        if (subfamilyCheck != null)
                                        {
                                            sort = _dbcontext.TB_SUBFAMILY.Where(y => y.FAMILY_ID == pastId).Max(x => x.SORT_ORDER) + 1;
                                        }
                                        var objSubFamily = new TB_SUBFAMILY
                                        {
                                            FAMILY_ID = pastId,
                                            SUBFAMILY_ID = copiedfamilyIdId,
                                            SORT_ORDER = sort,
                                            CREATED_USER = User.Identity.Name,
                                            CREATED_DATE = DateTime.Now,
                                            MODIFIED_USER = User.Identity.Name,
                                            MODIFIED_DATE = DateTime.Now
                                        };
                                        objLS.TB_SUBFAMILY.Add(objSubFamily);
                                        objLS.SaveChanges();
                                        res = true;
                                    }
                                    PartsKeyEntry(copiedfamilyIdId, catalogID, cId, familyCategoryId,previousCatalogID);
                                }
                                else
                                {
                                    // res = false;
                                    return res;
                                }
                            }
                        return res;

                    }
                    else
                    {
                        int.TryParse(copiedId.Trim('~'), out copiedfamilyIdId);
                        int categoryId = Convert.ToInt32(copiedfamilyIdId);
                        rightClickedCategoryId = rightClickedCategoryId.Trim('~');
                        flag = 2;
                        int pastId = Convert.ToInt32(rightClickedCategoryId);
                        checkFam = CopyPasteFamily_Check(copiedId, rightClickedCategoryId);
                        if (checkFam == true)
                        {
                            var catId = objLS.TB_CATALOG_FAMILY.Where(a => a.FAMILY_ID == pastId && a.CATALOG_ID == catalogID).Join(objLS.TB_CATEGORY, a => a.CATEGORY_ID, b => b.CATEGORY_ID, (a, b) => new { a, b })
                                .Select(s => s.b.CATEGORY_ID).ToList();
                            string cId = catId[0];
                            var stp2 = objLS.STP_CATALOGSTUDIO5_Cut(cId, copiedfamilyIdId,catalogID,previousCatalogID.ToString(CultureInfo.InvariantCulture), flag);
                            objLS.SaveChanges();
                            //parent family_id update
                            var objFamily = objLS.TB_FAMILY.Single(s => s.FAMILY_ID == copiedfamilyIdId);
                            objFamily.CATEGORY_ID = cId;
                            objFamily.PARENT_FAMILY_ID = pastId;
                            objFamily.ROOT_FAMILY = 0;
                            objLS.SaveChanges();
                            //subfamily entry
                            int sort = 1;
                            var subfamily = objLS.TB_SUBFAMILY.FirstOrDefault(a => a.SUBFAMILY_ID == copiedfamilyIdId);
                            var subfamilyCheck = objLS.TB_SUBFAMILY.FirstOrDefault(a => a.FAMILY_ID == pastId);
                            if (subfamily != null)
                            {
                                subfamily = objLS.TB_SUBFAMILY.Remove(subfamily);
                                var objSubFamily = new TB_SUBFAMILY
                                {
                                    FAMILY_ID = pastId,
                                    SUBFAMILY_ID = copiedfamilyIdId,
                                    SORT_ORDER = sort + 1,
                                    CREATED_USER = User.Identity.Name,
                                    CREATED_DATE = DateTime.Now,
                                    MODIFIED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now
                                };
                                objLS.TB_SUBFAMILY.Add(objSubFamily);
                                objLS.SaveChanges();
                                PartsKeyEntry(copiedfamilyIdId, catalogID, cId, familyCategoryId,previousCatalogID);
                            }
                            else
                            {
                                if (subfamilyCheck != null)
                                {
                                    sort = _dbcontext.TB_SUBFAMILY.Where(y => y.FAMILY_ID == pastId).Max(x => x.SORT_ORDER) + 1;
                                }
                                var objSubFamily = new TB_SUBFAMILY
                                {
                                    FAMILY_ID = pastId,
                                    SUBFAMILY_ID = copiedfamilyIdId,
                                    SORT_ORDER = sort,
                                    CREATED_USER = User.Identity.Name,
                                    CREATED_DATE = DateTime.Now,
                                    MODIFIED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now
                                };
                                objLS.TB_SUBFAMILY.Add(objSubFamily);
                                objLS.SaveChanges();
                                PartsKeyEntry(copiedfamilyIdId, catalogID, cId, familyCategoryId,previousCatalogID);
                                return true;
                            }
                        }
                        else
                        {
                            // res = false;
                            return false;
                        }
                    }
                }
                else
                {
                    if (multipleFamilyids != null && multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog != null && multipleFamilyidsForAnotherCatalog.Trim() != "")
                    {
                        string[] multipleFamilyIds = null;
                        if (multipleFamilyids != null)
                            multipleFamilyIds = catalogID == previousCatalogID ? multipleFamilyids.Split(',') : multipleFamilyidsForAnotherCatalog.Split(',');
                        if (multipleFamilyIds != null)
                            foreach (string t in multipleFamilyIds)
                            {
                                int.TryParse(t.Trim('~'), out copiedfamilyIdId);
                                int categoryId = Convert.ToInt32(copiedfamilyIdId);
                                if (!t.Contains("~")) continue;
                                
                                var copiedFamily = copiedId.Contains("~") ? copiedId.Replace("~", "") : copiedId;
                                var subfamily_check = objLS.TB_FAMILY.Where(x => x.FAMILY_ID == copiedfamilyIdId);
                                if (subfamily_check.Count()!=0 && subfamily_check.ToList()[0].PARENT_FAMILY_ID.ToString() != copiedFamily)
                                {

                                    var firstOrDefault = _dbcontext.TB_CATEGORY.FirstOrDefault(a => a.CATEGORY_ID == rightClickedCategoryId);
                                    if (firstOrDefault != null)
                                    {
                                        var parentCategoryId = firstOrDefault.PARENT_CATEGORY;
                                    }
                                    var tbFamily = _dbcontext.TB_FAMILY.FirstOrDefault(x => x.FAMILY_ID == categoryId);
                                    {
                                        if (tbFamily != null)
                                        {
                                            familyCategoryId = tbFamily.CATEGORY_ID;

                                            var stp1 = objLS.STP_CATALOGSTUDIO5_Cut_Category_Family(rightClickedCategoryId, familyCategoryId, copiedfamilyIdId, catalogID, previousCatalogID, flag);

                                        }

                                        objLS.SaveChanges();
                                        int val = _dbcontext.TB_SUBFAMILY.Where(z => z.FAMILY_ID == copiedfamilyIdId).Select(z => new { z.SUBFAMILY_ID }).Distinct().Count();

                                        var objFamily = objLS.TB_FAMILY.Where(s => s.PARENT_FAMILY_ID == copiedfamilyIdId);

                                        if (objFamily.Any())
                                        {
                                            foreach (var tt in objFamily)
                                            {

                                                tt.CATEGORY_ID = rightClickedCategoryId;
                                            }


                                        }
                                        objLS.SaveChanges();

                                        PartsKeyEntry(copiedfamilyIdId, catalogID, familyCategoryId, rightClickedCategoryId, previousCatalogID);
                                        res = true;

                                    }
                                }
                            }
                        return res;
                    }
                    else
                    {
                        int.TryParse(copiedId.Trim('~'), out copiedfamilyIdId);
                        int categoryId = Convert.ToInt32(copiedfamilyIdId);
                        var firstOrDefault = _dbcontext.TB_CATEGORY.FirstOrDefault(a => a.CATEGORY_ID == rightClickedCategoryId);
                        if (firstOrDefault != null)
                        {
                            var parentCategoryId = firstOrDefault.PARENT_CATEGORY;
                        }
                        var tbFamily = _dbcontext.TB_FAMILY.FirstOrDefault(x => x.FAMILY_ID == categoryId);
                        if (checkFam == true)
                        {
                            if (tbFamily != null)
                            {
                                familyCategoryId = tbFamily.CATEGORY_ID;

                                var stp1 = objLS.STP_CATALOGSTUDIO5_Cut_Category_Family(rightClickedCategoryId, familyCategoryId, copiedfamilyIdId, catalogID, previousCatalogID, flag);

                            }

                            objLS.SaveChanges();
                            int val = _dbcontext.TB_SUBFAMILY.Where(z => z.FAMILY_ID == copiedfamilyIdId).Select(z => new { z.SUBFAMILY_ID }).Distinct().Count();

                            var objFamily = objLS.TB_FAMILY.Where(s => s.PARENT_FAMILY_ID == copiedfamilyIdId);

                            if (objFamily.Any())
                            {
                                foreach (var tt in objFamily)
                                {

                                    tt.CATEGORY_ID = rightClickedCategoryId;
                                }


                            }
                            else
                            {
                                // subfamily remove
                                var objsubfamily = objLS.TB_SUBFAMILY.FirstOrDefault(a => a.SUBFAMILY_ID == copiedfamilyIdId);
                                if (objsubfamily != null)
                                {
                                    objLS.TB_SUBFAMILY.Remove(objsubfamily);
                                }
                            }
                            objLS.SaveChanges();
                            // PratsKey Entry
                            PartsKeyEntry(copiedfamilyIdId, catalogID, familyCategoryId, rightClickedCategoryId,previousCatalogID);
                            return true;
                        }
                    }
                }
                return false;

            }
            catch (Exception ex)
            {
                _logger.Error("Error at Family/CopyPasteFamily", ex);
                return false;
            }

        }
        public bool CopyPasteFamily_Check(string copiedId, string rightClickedCategoryId)
        {
            bool res = false;
            try
            {

                int copiedfamilyIdId;
                int.TryParse(copiedId.Trim('~'), out copiedfamilyIdId);
                int pastefamid;
                int.TryParse(rightClickedCategoryId.Trim('~'), out pastefamid);
                int val = _dbcontext.TB_SUBFAMILY.Where(z => z.FAMILY_ID == copiedfamilyIdId).Select(z => new { z.SUBFAMILY_ID }).Distinct().Count();
                var valPastefamid = _dbcontext.TB_FAMILY.Where(z => z.FAMILY_ID == pastefamid).Select(z => new { z.ROOT_FAMILY }).Distinct().ToList();
                res = val <= 0 && valPastefamid[0].ROOT_FAMILY != 0;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Family/CopyPasteFamily_Check", ex);
            }
            return res;
        }

        public void PartsKeyEntry(int copiedfamilyIdId, int catalogID, string familyCategoryId, string rightClickedCategoryId,int origninalcatalogid)
        {
            var objParts = _dbcontext.TB_PARTS_KEY.Where(a => a.FAMILY_ID == copiedfamilyIdId && a.CATALOG_ID == origninalcatalogid && a.CATEGORY_ID == familyCategoryId);
            if (objParts.Any())
            {
                var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                objSqlConnection.Open();
                SqlCommand Command = new SqlCommand("",objSqlConnection);
                string sqlstr1 = " UPDATE TB_PARTS_KEY SET CATEGORY_ID = '" + rightClickedCategoryId + "' , CATALOG_ID = " + catalogID + " where CATEGORY_ID = '" + familyCategoryId + "' AND CATALOG_ID = " + origninalcatalogid + " ";
                Command = new SqlCommand(sqlstr1, objSqlConnection);
                Command.ExecuteNonQuery();

                //foreach (var item in objParts)
                //{
                //    var result = _dbcontext.TB_PARTS_KEY.FirstOrDefault(x => x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.FAMILY_ID == copiedfamilyIdId && x.PRODUCT_ID == item.PRODUCT_ID && x.CATEGORY_ID == rightClickedCategoryId);
                //    if (result == null)
                //    {
                //        var objPartsKey = new TB_PARTS_KEY
                //        {
                //            ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                //            PRODUCT_ID = item.PRODUCT_ID,
                //            FAMILY_ID = item.FAMILY_ID,
                //            ATTRIBUTE_VALUE = item.ATTRIBUTE_VALUE,
                //            CREATED_USER = User.Identity.Name,
                //            CREATED_DATE = DateTime.Now,
                //            MODIFIED_USER = User.Identity.Name,
                //            MODIFIED_DATE = DateTime.Now,
                //            CATALOG_ID = catalogID,
                //            CATEGORY_ID = rightClickedCategoryId
                //        };
                //        _dbcontext.TB_PARTS_KEY.Add(objPartsKey);
                //    }

                //    result = _dbcontext.TB_PARTS_KEY.FirstOrDefault(x => x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.FAMILY_ID == copiedfamilyIdId && x.CATALOG_ID == origninalcatalogid && x.PRODUCT_ID == item.PRODUCT_ID && x.CATEGORY_ID == item.CATEGORY_ID);
                //    if (rightClickedCategoryId != item.CATEGORY_ID)
                //    {
                //        if (result != null)
                //        {
                //            _dbcontext.TB_PARTS_KEY.Remove(result);
                //        }
                //    }

                //}
                //_dbcontext.SaveChanges();

            }

            // subproducts key 
            #region

            var subproducts = _dbcontext.TB_CATALOG_PRODUCT
                          .Join(_dbcontext.TB_PROD_FAMILY, tps => tps.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                          .Join(_dbcontext.TB_SUBPRODUCT, tcptps => tcptps.tpf.PRODUCT_ID, tcp => tcp.PRODUCT_ID, (tcptps, tcp) => new { tcptps, tcp })
                          .Where(x => x.tcptps.tps.CATALOG_ID == origninalcatalogid && x.tcptps.tpf.FAMILY_ID == copiedfamilyIdId).Select(x => new { x.tcp.SUBPRODUCT_ID, x.tcp.PRODUCT_ID, x.tcptps.tpf.FAMILY_ID }).ToList();

            if (subproducts.Any())
            {
                foreach (var item in subproducts)
                {
                    var subproductkey = _dbcontext.TB_SUBPRODUCT_KEY.Where(a => a.CATALOG_ID == origninalcatalogid && a.CATEGORY_ID == familyCategoryId && a.FAMILY_ID == item.FAMILY_ID && a.PRODUCT_ID == item.PRODUCT_ID && a.SUBPRODUCT_ID == item.SUBPRODUCT_ID);
                    if (subproductkey.Any())
                    {
                        foreach (var values in subproductkey)
                        {
                            var obj = new TB_SUBPRODUCT_KEY
                            {
                                ATTRIBUTE_ID = values.ATTRIBUTE_ID,
                                ATTRIBUTE_VALUE = values.ATTRIBUTE_VALUE,
                                SUBPRODUCT_ID = values.SUBPRODUCT_ID,
                                PRODUCT_ID = values.PRODUCT_ID,
                                CATALOG_ID = catalogID,
                                FAMILY_ID = values.FAMILY_ID,
                                CATEGORY_ID = rightClickedCategoryId,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                            };
                            _dbcontext.TB_SUBPRODUCT_KEY.Add(obj);
                        }
                        _dbcontext.SaveChanges();

                        var subproductkeydelete = _dbcontext.TB_SUBPRODUCT_KEY.Where(a => a.CATALOG_ID == origninalcatalogid && a.CATEGORY_ID == familyCategoryId && a.FAMILY_ID == item.FAMILY_ID && a.PRODUCT_ID == item.PRODUCT_ID && a.SUBPRODUCT_ID == item.SUBPRODUCT_ID);
                        _dbcontext.TB_SUBPRODUCT_KEY.RemoveRange(subproductkeydelete);

                        _dbcontext.SaveChanges();
                    }
                }
            }

            #endregion
        }

        public string CopyPasteFamily(string copiedId, string rightClickedCategoryId, int catalogID, int previousCatalogID, string multipleFamilyids, string multipleFamilyidsForAnotherCatalog, bool SubProductCFM)
        {
            try
            {
                int flag = 1;
                int pasteType = 0;
                int allowduplicate = 0;
                var alloduplicate_check = _dbcontext.Customer_Settings.FirstOrDefault(a => a.CustomerId == _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name).CustomerId);
                if (alloduplicate_check != null)
                {
                    if (alloduplicate_check.AllowDuplicateItem_PartNum == true)
                    { allowduplicate = 1; }
                    else
                    { allowduplicate = 0; }

                }
                bool checkFam = true;
                int copiedfamilyIdId;
                // ***************************************************
                //  Check Product Count Before insert 
                // ***************************************************
                int FamilyProductCount = 0;
                int familyId = 0;

                int productcountExist = 0;
                var productcountall = _dbcontext.STP_LS_GET_PRODUCTS_BY_USER("PRODUCT", User.Identity.Name, "").ToList();

                if (productcountall.Any())
                {
                    productcountExist = Convert.ToInt32(productcountall[0]);
                }

                var skucnt = _dbcontext.TB_PLAN
                          .Join(_dbcontext.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                          .Join(_dbcontext.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                          .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => x.tcptps.tps.SKU_COUNT).ToList();
                int productSkuCount = skucnt[0];


                if (multipleFamilyids != null && multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog != null && multipleFamilyidsForAnotherCatalog.Trim() != "")
                {
                    string[] multipleFamilyIds = null;
                    if (catalogID == previousCatalogID)
                    {
                        if (multipleFamilyids != null) multipleFamilyIds = multipleFamilyids.Split(',');
                    }
                    else
                    {
                        multipleFamilyIds = multipleFamilyidsForAnotherCatalog.Split(',');
                    }
                    if (multipleFamilyIds == null) return "Some data is missing - please check";
                    foreach (string t in multipleFamilyIds.Where(t => t.Contains("~")))
                    {

                        int.TryParse(t.Trim('~'), out familyId);
                        string Id = Convert.ToString(familyId);

                        var productcount = _dbcontext.STP_LS_GET_PRODUCTS_BY_USER("FAMILY", User.Identity.Name, Id).ToList();

                        if (productcount.Any())
                        {
                            FamilyProductCount = FamilyProductCount + Convert.ToInt32(productcount[0]);
                        }
                    }
                }
                else
                {
                    int.TryParse(copiedId.Trim('~'), out familyId);
                    string Id = Convert.ToString(familyId);
                    var productcount = _dbcontext.STP_LS_GET_PRODUCTS_BY_USER("FAMILY", User.Identity.Name, Id).ToList();

                    if (productcount.Any())
                    {
                        FamilyProductCount = Convert.ToInt32(productcount[0]);
                    }

                    var subfamily_check = objLS.TB_FAMILY.Where(x => x.PARENT_FAMILY_ID == familyId && x.PARENT_FAMILY_ID != 0).Select(a => a.FAMILY_ID).ToList();
                    if (subfamily_check.Any())
                    {
                        foreach (int Sub_Ids in subfamily_check)
                        {
                            string Idsub = Convert.ToString(Sub_Ids);
                            var productcount1 = _dbcontext.STP_LS_GET_PRODUCTS_BY_USER("FAMILY", User.Identity.Name, Idsub).ToList();

                            if (productcount1.Any())
                            {
                                FamilyProductCount = FamilyProductCount + Convert.ToInt32(productcount1[0]);
                            }
                        }
                    }
                }

                if ((productcountExist + FamilyProductCount) > productSkuCount && allowduplicate == 1)
                {
                    return "You have exceeded the Maximum No. of SKUs as per your Plan";
                }
                else
                {
                    // ***************************************************
                    //  Check Product Count Before insert end
                    // ***************************************************

                    if (multipleFamilyids != null && multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog != null && multipleFamilyidsForAnotherCatalog.Trim() != "")
                    {
                        string[] multipleFamilyIds = null;
                        if (catalogID == previousCatalogID)
                        {
                            if (multipleFamilyids != null) multipleFamilyIds = multipleFamilyids.Split(',');
                        }
                        else
                        {
                            multipleFamilyIds = multipleFamilyidsForAnotherCatalog.Split(',');
                        }
                        if (multipleFamilyIds == null) return "Failed."; ;
                        foreach (string t in multipleFamilyIds.Where(t => t.Contains("~")))
                        {
                            int.TryParse(t.Trim('~'), out copiedfamilyIdId);
                            string Id = t;
                            if (rightClickedCategoryId.StartsWith("~"))
                            {
                                rightClickedCategoryId = rightClickedCategoryId.Trim('~');
                                flag = 2;
                                pasteType = 0;
                                checkFam = CopyPasteFamily_Check(t, rightClickedCategoryId);
                            }
                            if (checkFam == true)
                            {
                                var firstOrDefault = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(x => x.FAMILY_ID == copiedfamilyIdId && x.CATALOG_ID == previousCatalogID && x.CATALOG_ID != 1);
                                if (firstOrDefault != null)
                                {
                                    string categoryID = firstOrDefault.CATEGORY_ID;
                                    var subfamily_check = objLS.TB_FAMILY.Where(x => x.FAMILY_ID == copiedfamilyIdId && x.PARENT_FAMILY_ID != 0);
                                    if (subfamily_check.Any())
                                    {
                                        var copiedFamily = copiedId.Contains("~") ? copiedId.Replace("~", "") : copiedId;
                                        if (subfamily_check.ToList()[0].PARENT_FAMILY_ID.ToString() != copiedFamily)
                                        {
                                            // objLS.Database.CommandTimeout = 0;
                                            TB_FAMILY family = objLS.TB_FAMILY.Where(s => s.PARENT_FAMILY_ID == 0 && s.FAMILY_ID == copiedfamilyIdId).FirstOrDefault();

                                            if (family != null && family.FAMILY_ID > 0)
                                            {
                                                using (var conn = new SqlConnection(connectionString))
                                                {
                                                    conn.Open();
                                                    var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_Copy_Category_Family ''" + rightClickedCategoryId + "'',''" + categoryID + "'',''" + copiedfamilyIdId + "'',''" + catalogID + "'',''" + previousCatalogID + "'',''" + flag + "'',''" + pasteType + "'',''" + allowduplicate + "'',''" + User.Identity.Name + "''')", conn);
                                                    cmd.CommandTimeout = 0;
                                                    // cmd.ExecuteNonQuery();
                                                    //int value = (Int32)cmd.ExecuteScalar();

                                                    var val = cmd.ExecuteScalar();
                                                    int famId = 0;
                                                    string returnval = val.ToString();
                                                    if (returnval.Contains("#"))
                                                    {
                                                        int index = returnval.IndexOf("#");
                                                        returnval = returnval.Substring(index + 1);
                                                    }
                                                    bool result = int.TryParse(returnval, out famId);


                                                    //var stp1 = objLS.STP_CATALOGSTUDIO5_Copy_Category_Family(rightClickedCategoryId, categoryID, copiedfamilyIdId, catalogID, previousCatalogID, flag, pasteType, allowduplicate, User.Identity.Name).ToList();
                                                    if (famId > 0 && flag == 1)
                                                    {
                                                        int newFid = 0;
                                                        //var results = value.Split('#');
                                                        newFid = famId;
                                                        var objFamily = objLS.TB_FAMILY.Single(s => s.FAMILY_ID == newFid);
                                                        objFamily.ROOT_FAMILY = 1;
                                                        //main-family---Ayyappan------12-09-2016---
                                                        if (catalogID == previousCatalogID)
                                                        {
                                                            SubproductsCreation(copiedfamilyIdId, newFid, "FAMILY", allowduplicate,
                                                               User.Identity.Name, previousCatalogID, catalogID,
                                                               rightClickedCategoryId, categoryID);
                                                        }
                                                        else if (SubProductCFM)
                                                        {
                                                            SubproductsCreation(copiedfamilyIdId, newFid, "FAMILY", allowduplicate,
                                                                User.Identity.Name, previousCatalogID, catalogID,
                                                                rightClickedCategoryId, categoryID);
                                                        }
                                                    }
                                                }

                                            }


                                        }
                                    }
                                    else
                                    {
                                        //var stp1 = objLS.STP_CATALOGSTUDIO5_Copy_Category_Family(rightClickedCategoryId, categoryID, copiedfamilyIdId, catalogID, previousCatalogID, flag, pasteType, allowduplicate).ToList();
                                        if (Id.Contains("~"))
                                        {
                                            TB_FAMILY family = objLS.TB_FAMILY.Where(s => s.PARENT_FAMILY_ID == 0 && s.FAMILY_ID == copiedfamilyIdId).FirstOrDefault();

                                            if (family != null && family.FAMILY_ID > 0)
                                            {


                                                using (var conn = new SqlConnection(connectionString))
                                                {
                                                    conn.Open();
                                                    var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_Copy_Category_Family ''" + rightClickedCategoryId + "'',''" + categoryID + "'',''" + copiedfamilyIdId + "'',''" + catalogID + "'',''" + previousCatalogID + "'',''" + flag + "'',''" + pasteType + "'',''" + allowduplicate + "'',''" + User.Identity.Name + "''')", conn);
                                                    // cmd.ExecuteNonQuery();
                                                    cmd.CommandTimeout = 0;
                                                    //int value = (Int32)cmd.ExecuteScalar();
                                                    var val = cmd.ExecuteScalar();
                                                    int famId = 0;
                                                    string returnval = val.ToString();
                                                    if (returnval.Contains("#"))
                                                    {
                                                        int index = returnval.IndexOf("#");
                                                        returnval = returnval.Substring(index + 1);
                                                    }
                                                    bool result = int.TryParse(returnval, out famId);

                                                    //var stp1 = objLS.STP_CATALOGSTUDIO5_Copy_Category_Family(rightClickedCategoryId, categoryID, copiedfamilyIdId, catalogID, previousCatalogID, flag, pasteType, allowduplicate, User.Identity.Name).ToList();
                                                    if (famId > 0 && flag == 2)
                                                    {
                                                        int newFid = 0;
                                                        //var results = value.Split('#');
                                                        newFid = famId;
                                                        var objFamily = objLS.TB_FAMILY.Single(s => s.FAMILY_ID == newFid);
                                                        objFamily.ROOT_FAMILY = 0;
                                                        if (catalogID == previousCatalogID)
                                                        {
                                                            SubproductsCreation(copiedfamilyIdId, newFid, "FAMILY", allowduplicate,
                                                               User.Identity.Name, previousCatalogID, catalogID,
                                                               rightClickedCategoryId, categoryID);
                                                        }
                                                        else if (SubProductCFM)
                                                        {
                                                            SubproductsCreation(copiedfamilyIdId, newFid, "FAMILY",
                                                                allowduplicate, User.Identity.Name, previousCatalogID, catalogID,
                                                                rightClickedCategoryId, categoryID);
                                                        }
                                                    }
                                                }


                                            }


                                        }
                                        else
                                        {
                                            //objLS.Database.CommandTimeout = 0;

                                            TB_FAMILY family = objLS.TB_FAMILY.Where(s => s.PARENT_FAMILY_ID == 0 && s.FAMILY_ID == copiedfamilyIdId).FirstOrDefault();

                                            if (family != null && family.FAMILY_ID > 0)
                                            {

                                                using (var conn = new SqlConnection(connectionString))
                                                {
                                                    conn.Open();
                                                    var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_Copy_Category_Family ''" + rightClickedCategoryId + "'',''" + categoryID + "'',''" + copiedfamilyIdId + "'',''" + catalogID + "'',''" + previousCatalogID + "'',''" + flag + "'',''" + pasteType + "'',''" + allowduplicate + "'',''" + User.Identity.Name + "''')", conn);
                                                    cmd.CommandTimeout = 0;
                                                    var val = cmd.ExecuteScalar();
                                                    int famId = 0;
                                                    string returnval = val.ToString();
                                                    if (returnval.Contains("#"))
                                                    {
                                                        int index = returnval.IndexOf("#");
                                                        returnval = returnval.Substring(index + 1);
                                                    }
                                                    bool result = int.TryParse(returnval, out famId);

                                                    // cmd.ExecuteNonQuery();

                                                    //var stp1 = objLS.STP_CATALOGSTUDIO5_Copy_Category_Family(rightClickedCategoryId, categoryID, copiedfamilyIdId, catalogID, previousCatalogID, flag, pasteType, allowduplicate, User.Identity.Name).ToList();
                                                    if (famId > 0)
                                                    {
                                                        int fid = 0;
                                                        //var results = value.Split('#');
                                                        fid = famId;
                                                        if (catalogID == previousCatalogID)
                                                        {
                                                            SubproductsCreation(copiedfamilyIdId, fid, "FAMILY", allowduplicate,
                                                               User.Identity.Name, previousCatalogID, catalogID,
                                                               rightClickedCategoryId, categoryID);
                                                        }
                                                        else if (SubProductCFM)
                                                        {
                                                            SubproductsCreation(copiedfamilyIdId, fid, "FAMILY",
                                                                allowduplicate, User.Identity.Name, previousCatalogID, catalogID,
                                                                rightClickedCategoryId, categoryID);
                                                        }

                                                    }
                                                }

                                            }

                                        }
                                    }
                                }
                                objLS.SaveChanges();
                                //  return "Success.";
                            }
                            else
                            {
                                return "Action not successful";

                            }
                        }
                        return "Paste successfully";
                    }
                    else
                    {
                        string Id = rightClickedCategoryId;
                        int.TryParse(copiedId.Trim('~'), out copiedfamilyIdId);
                        if (rightClickedCategoryId.StartsWith("~"))
                        {
                            rightClickedCategoryId = rightClickedCategoryId.Trim('~');
                            flag = 2;
                            pasteType = 0;
                            checkFam = CopyPasteFamily_Check(copiedId, rightClickedCategoryId);
                        }
                        if (checkFam == true)
                        {
                            // var parentCategoryID = _dbcontext.TB_CATEGORY.FirstOrDefault(a => a.CATEGORY_ID == RightClickedCategoryId).PARENT_CATEGORY;
                            var firstOrDefault = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(x => x.FAMILY_ID == copiedfamilyIdId && x.CATALOG_ID == previousCatalogID && x.CATALOG_ID != 1);
                            if (firstOrDefault != null)
                            {
                                string categoryID = firstOrDefault.CATEGORY_ID;
                                var subfamily_check = objLS.TB_FAMILY.Where(x => x.FAMILY_ID == copiedfamilyIdId && x.PARENT_FAMILY_ID != 0);
                                if (subfamily_check.Any())
                                {
                                    objLS.Database.CommandTimeout = 0;

                                    using (var conn = new SqlConnection(connectionString))
                                    {
                                        conn.Open();
                                        var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_Copy_Category_Family ''" + rightClickedCategoryId + "'',''" + categoryID + "'',''" + copiedfamilyIdId + "'',''" + catalogID + "'',''" + previousCatalogID + "'',''" + flag + "'',''" + pasteType + "'',''" + allowduplicate + "'',''" + User.Identity.Name + "''')", conn);
                                        cmd.CommandTimeout = 0;
                                        var val = cmd.ExecuteScalar();

                                        int famId = 0;
                                        string returnval = val.ToString();
                                        if (returnval.Contains("#"))
                                        {
                                            int index = returnval.IndexOf("#");
                                            returnval = returnval.Substring(index + 1);
                                        }
                                        bool result = int.TryParse(returnval, out famId);


                                        //var stp1 = objLS.STP_CATALOGSTUDIO5_Copy_Category_Family(rightClickedCategoryId, categoryID, copiedfamilyIdId, catalogID, previousCatalogID, flag, pasteType, allowduplicate, User.Identity.Name).ToList();

                                        if (famId > 0 && flag == 1)
                                        {
                                            int newFid = 0;
                                            //var results = stp1[0].Split('#');
                                            newFid = Convert.ToInt32(famId);
                                            var objFamily = objLS.TB_FAMILY.Single(s => s.FAMILY_ID == newFid);
                                            objFamily.ROOT_FAMILY = 1;
                                            if (catalogID == previousCatalogID)
                                            {
                                                SubproductsCreation(copiedfamilyIdId, newFid, "FAMILY", allowduplicate,
                                                   User.Identity.Name, previousCatalogID, catalogID,
                                                   rightClickedCategoryId, categoryID);
                                            }
                                            else if (SubProductCFM)
                                            {
                                                SubproductsCreation(copiedfamilyIdId, newFid, "FAMILY",
                                                    allowduplicate, User.Identity.Name, previousCatalogID, catalogID,
                                                    rightClickedCategoryId, categoryID);
                                            }

                                        }


                                    }


                                }
                                else
                                {
                                    if (Id.Contains("~"))
                                    {
                                        objLS.Database.CommandTimeout = 0;

                                        using (var conn = new SqlConnection(connectionString))
                                        {
                                            conn.Open();
                                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_Copy_Category_Family ''" + rightClickedCategoryId + "'',''" + categoryID + "'',''" + copiedfamilyIdId + "'',''" + catalogID + "'',''" + previousCatalogID + "'',''" + flag + "'',''" + pasteType + "'',''" + allowduplicate + "'',''" + User.Identity.Name + "''')", conn);
                                            cmd.CommandTimeout = 0;
                                            var val = cmd.ExecuteScalar();

                                            int famId = 0;
                                            string returnval = val.ToString();
                                            if (returnval.Contains("#"))
                                            {
                                                int index = returnval.IndexOf("#");
                                                returnval = returnval.Substring(index + 1);
                                            }
                                            bool result = int.TryParse(returnval, out famId);

                                            //var stp1 = objLS.STP_CATALOGSTUDIO5_Copy_Category_Family(rightClickedCategoryId, categoryID, copiedfamilyIdId, catalogID, previousCatalogID, flag, pasteType, allowduplicate, User.Identity.Name).ToList();
                                            if (famId > 0 && flag == 2)
                                            {
                                                int newFid = 0;
                                                //var results = stp1[0].Split('#');

                                                // if (results.Length > 1)
                                                //{
                                                newFid = Convert.ToInt32(famId);
                                                var objFamily = objLS.TB_FAMILY.Single(s => s.FAMILY_ID == newFid);
                                                objFamily.ROOT_FAMILY = 0;
                                                if (catalogID == previousCatalogID)
                                                {
                                                    SubproductsCreation(copiedfamilyIdId, newFid, "FAMILY",
                                                        allowduplicate,
                                                        User.Identity.Name, previousCatalogID, catalogID,
                                                        rightClickedCategoryId, categoryID);
                                                }
                                                else if (SubProductCFM)
                                                {
                                                    SubproductsCreation(copiedfamilyIdId, newFid, "FAMILY",
                                                        allowduplicate, User.Identity.Name, previousCatalogID, catalogID,
                                                        rightClickedCategoryId, categoryID);
                                                }
                                                // }


                                            }


                                        }
                                    }
                                    else
                                    {
                                        objLS.Database.CommandTimeout = 0;

                                        using (var conn = new SqlConnection(connectionString))
                                        {
                                            conn.Open();
                                            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_Copy_Category_Family ''" + rightClickedCategoryId + "'',''" + categoryID + "'',''" + copiedfamilyIdId + "'',''" + catalogID + "'',''" + previousCatalogID + "'',''" + flag + "'',''" + pasteType + "'',''" + allowduplicate + "'',''" + User.Identity.Name + "''')", conn);
                                            cmd.CommandTimeout = 0;
                                            var val = cmd.ExecuteScalar();

                                            //var stp1 = objLS.STP_CATALOGSTUDIO5_Copy_Category_Family(rightClickedCategoryId, categoryID, copiedfamilyIdId, catalogID, previousCatalogID, flag, pasteType, allowduplicate, User.Identity.Name).ToList();

                                            int famId = 0;
                                            string returnval = val.ToString();
                                            if (returnval.Contains("#"))
                                            {
                                                int index = returnval.IndexOf("#");
                                                returnval = returnval.Substring(index + 1);
                                            }
                                            bool result = int.TryParse(returnval, out famId);

                                            if (famId > 0)
                                            {
                                                int fid = 0;
                                                // var results = stp1[0].Split('#');

                                                // if (results.Length > 1)
                                                //{
                                                fid = Convert.ToInt32(famId);
                                                if (catalogID == previousCatalogID)
                                                {
                                                    SubproductsCreation(copiedfamilyIdId, fid, "FAMILY", allowduplicate,
                                                        User.Identity.Name, previousCatalogID, catalogID,
                                                        rightClickedCategoryId, categoryID);
                                                }
                                                else if (SubProductCFM)
                                                {
                                                    SubproductsCreation(copiedfamilyIdId, fid, "FAMILY",
                                                        allowduplicate, User.Identity.Name, previousCatalogID, catalogID,
                                                        rightClickedCategoryId, categoryID);
                                                }
                                                // }
                                            }

                                        }

                                    }
                                }
                            }
                            objLS.SaveChanges();
                            return "Paste successfully";
                        }
                        else
                        {
                            // res = false;
                            return "Action not successful";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Family/CopyPasteFamily", ex);
                return "Action not successful";
            }

        }


        public void SubproductsCreation(int copiedfamilyIdId,int newFid,string option,int allowduplicate,string name,int previousCatalogID,int catalogID,string rightClickedCategoryId,string categoryID)
        {
            try
            {
                objLS.STP_LS_SUBPRODUCTS_COPY_PASTE_FAMILY(copiedfamilyIdId, newFid, option, allowduplicate, name, previousCatalogID, catalogID, rightClickedCategoryId, categoryID);
            objLS.SaveChanges();

            //sub-family
            var subfamilyOld = _dbcontext.TB_FAMILY.Where(a => a.PARENT_FAMILY_ID == copiedfamilyIdId).OrderBy(b => b.FAMILY_ID).ToArray();
            var subfamilyCheckforsubproducts = _dbcontext.TB_FAMILY.Where(a => a.PARENT_FAMILY_ID == newFid).OrderBy(a => a.FAMILY_ID).ToArray();
            if (subfamilyOld.Any())
            {
                for (int i = 0; i < subfamilyOld.Count(); i++)
                {
                    objLS.STP_LS_SUBPRODUCTS_COPY_PASTE_FAMILY(subfamilyOld[i].FAMILY_ID, subfamilyCheckforsubproducts[i].FAMILY_ID, option, allowduplicate, name, previousCatalogID, catalogID, rightClickedCategoryId, categoryID);
                    objLS.SaveChanges();
                }

            }
            }
            catch (Exception ex)
            {

                _logger.Error("Error at SubProductscreation:HomeApiController", ex);
               // return "Sub Products Error.";
            }
        }
        public JsonResult GetFamilyAttr(string familyId, string attrId)
        {
            try
            {
                int famId;
                int.TryParse(familyId.Trim('~'), out famId);
                // int famId = Convert.ToInt32(familyId);
                int attrid = 0;
                if (!string.IsNullOrEmpty(attrId))
                {
                    attrid = Convert.ToInt32(attrId);
                }
                var rolename1 = Roles.GetRolesForUser(User.Identity.Name.ToString());
                string rolename = rolename1[0];
                //var ss = objLS.TB_READONLY_ATTRIBUTES.Join(objLS.aspnet_Roles, tra => tra.ROLE_ID, ar => ar.Role_id, (tra, ar) => new { tra, ar }).Any(c => c.tra.ROLE_ID == c.ar.Role_id && c.tra.ATTRIBUTE_ID == attrid && c.ar.RoleName == rolename);
                var familyAttrVal1 = objLS.TB_FAMILY_SPECS.Where(s => s.FAMILY_ID == famId && s.ATTRIBUTE_ID == attrid).Select(x => new
                {
                    STRING_VALUE = x.STRING_VALUE,
                    //spriteCssClass = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tps => tps.FAMILY_ID, tpf => tpf.FAMILY_ID, (tps, tpf) => new { tps, tpf }).Any(x => x.tps.CATALOG_ID == catalogId && x.tps.ROOT_CATEGORY != "0" && x.tpf.FAMILY_ID == a.FAMILY_ID) ?
                    ATTRIBUTE_DISABLE = objLS.TB_READONLY_ATTRIBUTES.Join(objLS.aspnet_Roles, tra => tra.ROLE_ID, ar => ar.Role_id, (tra, ar) => new { tra, ar }).Any(c => c.tra.ROLE_ID == c.ar.Role_id && c.tra.ATTRIBUTE_ID == attrid && c.ar.RoleName == rolename),
                }).ToList();
                if (familyAttrVal1.Any())
                {
                    return Json(familyAttrVal1.ElementAt(0), JsonRequestBehavior.AllowGet);
                }
                return Json(familyAttrVal1, JsonRequestBehavior.AllowGet);
                //  return Json(familyAttrVal1.ElementAt(0), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Family/GetFamilyAttr", ex);
                return null;
            }
        }
        public JsonResult GetFamilyImgAttr(string familyId, string attrId)
        {
            try
            {
                int userID = 0;
                string _imageMagickPath = string.Empty;
                var customer_det = objLS.Customer_User.FirstOrDefault(a => a.User_Name == User.Identity.Name);
                if (customer_det != null)
                {
                    userID = customer_det.CustomerId;
                }
                var imagemagickpath = objLS.Customer_Settings.FirstOrDefault(a => a.CustomerId == userID);
                //if (imagemagickpath != null)
                //{
                //    _imageMagickPath = imagemagickpath.ImageConversionUtilityPath;
                //    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");
                //}
                //else
                //{
                //    _imageMagickPath = "C:\\Program Files\\ImageMagick-6.8.5-Q8";
                //}
                string converttype = _dbcontext.Customer_Settings.Where(a => a.CustomerId == userID).Select(a => a.ConverterType).SingleOrDefault();
                if (imagemagickpath != null && converttype == "Image Magic")
                {
                    _imageMagickPath = convertionPath;
                    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");
                }
                else
                {
                    _imageMagickPath = convertionPathREA;
                    _imageMagickPath = _imageMagickPath.Replace("\\\\", "\\");
                }
                int famId;
                int.TryParse(familyId.Trim('~'), out famId);
                // int famId = Convert.ToInt32(familyId);
                int attrid = 0;
                var rolename1 = Roles.GetRolesForUser(User.Identity.Name.ToString());
                string rolename = rolename1[0];
                if (attrId == "undefined" || attrId == "")
                {
                    attrId = "0";
                }
                if (!string.IsNullOrEmpty(attrId))
                {
                    attrid = Convert.ToInt32(attrId);
                }

                var familyAttrVal1 = objLS.TB_FAMILY_SPECS.Where(s => s.FAMILY_ID == famId && s.ATTRIBUTE_ID == attrid)
                    .Join(objLS.TB_ATTRIBUTE, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a, b }).ToList()
                    .Select(x => new
                {
                    STRING_VALUE = x.a.STRING_VALUE,
                    ATTRIBUTE_NAME = x.b.ATTRIBUTE_NAME,
                    ATTRIBUTE_ID = x.b.ATTRIBUTE_ID,
                    OBJECT_NAME = x.a.OBJECT_NAME,
                    OBJECT_TYPE = x.a.OBJECT_TYPE,
                    // PREVIEW_PATH=string.IsNullOrEmpty(x.a.STRING_VALUE) ? x.a.STRING_VALUE: (System.IO.File.Exists(Server.MapPath("~/Content/ProductImages") + x.a.STRING_VALUE) ? x.a.STRING_VALUE : "Images/unsupportedImageformat.jpg")
                    PREVIEW_PATH = x.a.STRING_VALUE,
                    ATTRIBUTE_DISABLE = objLS.TB_READONLY_ATTRIBUTES.Join(objLS.aspnet_Roles, tra => tra.ROLE_ID, ar => ar.Role_id, (tra, ar) => new { tra, ar }).Any(c => c.tra.ROLE_ID == c.ar.Role_id && c.tra.ATTRIBUTE_ID == attrid && c.ar.RoleName == rolename),
                }).ToList();
                return familyAttrVal1.Any() ? Json(familyAttrVal1.ElementAt(0), JsonRequestBehavior.AllowGet) : Json(familyAttrVal1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Family/GetFamilyImgAttr", ex);
                return null;
            }
        }
        public string MissingImagePath(string imageFile, string id, int attrid, string imagemagick)
        {
            var ImageConversion = new FamilyPreviews();
            try
            {
                CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }

                //return string.IsNullOrEmpty(imageFile)
                //    //  ? (!imageFile.Contains(".jpg") || !imageFile.Contains(".bmp") || !imageFile.Contains(".gif") || !imageFile.Contains(".png"))?"/Content/ProductImages/Images/unsupportedImageformat.jpg"
                //    ? imageFile
                //    : (System.IO.File.Exists(Server.MapPath("~/Content/ProductImages") + imageFile)
                //    ? (imageFile.Contains(".jpg") || imageFile.Contains(".bmp") || imageFile.Contains(".gif") || imageFile.Contains(".png")) ? imageFile: "/Images/unsupportedImageformat.jpg" 
                //        : "/Images/unsupportedImageformat.jpg");
                string val;
                if (imageFile == null)
                {
                    //val = "/Images/unsupportedImageformat.jpg";
                    val = "";
                }
                else if (System.IO.File.Exists(Server.MapPath("../Content/ProductImages" + CustomerFolder) + imageFile))
                {
                    var chkFile = new FileInfo(Server.MapPath("../Content/ProductImages" + CustomerFolder) + imageFile);
                    if (chkFile.Extension.ToUpper() == ".JPG" || chkFile.Extension.ToUpper() == ".BMP" || chkFile.Extension.ToUpper() == ".GIF" || chkFile.Extension.ToUpper() == ".PNG")
                    {
                        val = "../Content/ProductImages" + CustomerFolder + imageFile;
                    }
                    else
                    {
                        //val = "/Images/unsupportedImageformat.jpg";
                        val = ImageConversion.ImageConversionForAll(imageFile, id, attrid, imagemagick, CustomerFolder);
                    }
                }
                else
                {
                    // val = "/Images/unsupportedImageformat.jpg";
                    val = "";
                }
                return val;
            }

            catch (Exception objException)
            {
                Logger.Error("Error at FamilyApiController : MissingImagePath", objException);
                return string.Empty;
            }
        }

        public bool SaveDescription(string fmlyAttrDesc, int fmlyAttrDescId, string familyName, int familyId)
        {
            var objFamilyDescAttr = objLS.TB_FAMILY_SPECS.Single(s => s.FAMILY_ID == familyId && s.ATTRIBUTE_ID == fmlyAttrDescId);
            objFamilyDescAttr.STRING_VALUE = fmlyAttrDesc;
            try
            {
                objLS.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController : SaveDescription", ex);
                return false;
            }

        }

        public int SaveFamily(string familyName, string categoryId, string status, bool publishToWeb, bool publishToPrint, int catalogId, string workflowstatus, string option, bool publishToPdf, bool publishToExport, bool publishToPortal)
        {
            int familyId = 0;

            int workflowstatuscode = 0;
            if (familyName != "")
                familyName = AesEncrytDecry.DecryptStringAes(familyName);

            if (workflowstatus != "")
            {
                var objworkflow = _dbcontext.TB_WORKFLOW_STATUS.FirstOrDefault(k => k.STATUS_NAME == workflowstatus);
                if (objworkflow != null)
                    workflowstatuscode = objworkflow.STATUS_CODE;
            }

            var workflowCheck = _dbcontext.Customer_Settings.Where(x => x.CustomerId == (_dbcontext.Customer_User.FirstOrDefault(a => a.User_Name == User.Identity.Name)).CustomerId);
            if (workflowCheck.Any())
            {
                var worflowStatus = workflowCheck.FirstOrDefault();
                if (worflowStatus != null)
                {
                    var value = worflowStatus.EnableWorkFlow;
                    if (!value)
                        workflowstatuscode = 99;
                }
            }


            #region To Associate Attribute Pack

            List<string> categoryIds = new List<string>();
            string category_Id = Convert.ToString(categoryId);
            TB_CATEGORY tb_category = new TB_CATEGORY();
            categoryIds.Add(category_Id);
            string catalogIdstring = Convert.ToString(catalogId);
            categoryIds.Add(catalogIdstring);
            var ds = new DataSet();

            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESIGNEXPORT_IMPORT";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = 0;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETPARENT";
                objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.Int).Value = 0;
                objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.VarChar).Value = categoryId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(ds);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {
                    if (dtRow["CATEGORY_ID"].ToString() != null)
                    {
                        categoryIds.Add(dtRow["CATEGORY_ID"].ToString());
                    }
                }
            }


            List<TB_ATTRIBUTE_HIERARCHY> attributeHierarchyList = _dbcontext.TB_ATTRIBUTE_HIERARCHY.Where(s => categoryIds.Contains(s.ASSIGN_TO)).ToList();

            #endregion

            if (option != "Clone")
            {
                var objFam = new TB_FAMILY
                {
                    FAMILY_NAME = familyName,
                    CATEGORY_ID = categoryId,
                    STATUS = status,
                    ROOT_FAMILY = 1,
                    PARENT_FAMILY_ID = 0,
                    PUBLISH2WEB = publishToWeb,
                    PUBLISH2PRINT = publishToPrint,
                    PUBLISH2PDF = publishToPdf,
                    PUBLISH2EXPORT = publishToExport,
                    PUBLISH2PORTAL = publishToPortal,
                    DISPLAY_TABLE_HEADER = true,
                    MODIFIED_USER = User.Identity.Name,
                    CREATED_USER = User.Identity.Name,
                    CREATED_DATE = DateTime.Now,
                    MODIFIED_DATE = DateTime.Now,
                    WORKFLOW_STATUS = workflowstatuscode,
                    FLAG_RECYCLE = "A"

                };
                try
                {
                    objLS.TB_FAMILY.Add(objFam);
                    objLS.SaveChanges();
                    familyId = _dbcontext.TB_FAMILY.Max(x => x.FAMILY_ID);
                    int sortorder = 0;
                    var catalogfamily = _dbcontext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == catalogId && x.CATEGORY_ID == categoryId);
                    if (catalogfamily.Any())
                        sortorder = catalogfamily.Max(x => x.SORT_ORDER);

                    if (familyId == 0) return familyId;
                    var objCatalogFamily = new TB_CATALOG_FAMILY
                    {
                        FAMILY_ID = familyId,
                        CATEGORY_ID = categoryId,
                        CATALOG_ID = catalogId,
                        SORT_ORDER = sortorder + 1,
                        ROOT_CATEGORY = "0",
                        MODIFIED_USER = User.Identity.Name,
                        CREATED_USER = User.Identity.Name,
                        CREATED_DATE = DateTime.Now,
                        MODIFIED_DATE = DateTime.Now,
                        FLAG_RECYCLE = "A"
                    };
                    objLS.TB_CATALOG_FAMILY.Add(objCatalogFamily);
                    objLS.SaveChanges();
                    string familyTableXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><TradingBell TableType=\"SuperTable\"><LeftRowField AttrID=\"Attr:1\" Merge=\"unchecked\" Level=\"0\"/><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>";
                    var objFamilTableStructure = new TB_FAMILY_TABLE_STRUCTURE
                    {
                        //  ID = id + 1,
                        CATALOG_ID = catalogId,
                        FAMILY_ID = familyId,
                        FAMILY_TABLE_STRUCTURE = familyTableXml,
                        STRUCTURE_NAME = "Default Layout",
                        IS_DEFAULT = true,
                        CREATED_USER = User.Identity.Name,
                        CREATED_DATE = DateTime.Now,
                        MODIFIED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now
                    };
                    objLS.TB_FAMILY_TABLE_STRUCTURE.Add(objFamilTableStructure);
                    objLS.SaveChanges();

                    int table_Id = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Max(x => x.ID);
                    HomeApiController apiObj = new HomeApiController();
                    string structureName = "Default Layout";
                    List<TB_PACKAGE_MASTER> packs = _dbcontext.TB_PACKAGE_MASTER.Where(s => s.CATALOG_ID == catalogId && s.IS_FAMILY.ToLower() == "product").ToList();
                    foreach (var pack in packs)
                    {
                        if (pack != null)
                        {
                            string xmlString = apiObj.CreateXMLAttributePack(pack.GROUP_ID);
                            string sqlStr = "Insert into TB_ATTRIBUTE_PACK_TABLE_STRUCTURE (FAMILY_STRUCTURE_ID,PACK_ID,TABLE_STRUCTURE,STRUCTURE_NAME,IS_DEFAULT,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) values (" + table_Id + "," + pack.GROUP_ID + ",'" + xmlString.Replace("'", "''") + "','" + structureName + "'," + 1 + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                            using (var objConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                            {
                                objConnection.Open();
                                SqlCommand objSqlCommand = objConnection.CreateCommand();
                                objSqlCommand.CommandText = sqlStr;
                                objSqlCommand.CommandType = CommandType.Text;
                                objSqlCommand.Connection = objConnection;
                                objSqlCommand.ExecuteNonQuery();
                            }
                        }
                    }

                    int id = familyId;
                    var familytable = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(x => x.CATALOG_ID == 1 && x.FAMILY_ID == id && x.STRUCTURE_NAME == "Default Layout");
                    if (!familytable.Any())
                    {
                        var objFamilTableStructures = new TB_FAMILY_TABLE_STRUCTURE
                        {
                            //  ID = id + 1,
                            CATALOG_ID = 1,
                            FAMILY_ID = id,
                            FAMILY_TABLE_STRUCTURE = familyTableXml,
                            STRUCTURE_NAME = "Default Layout",
                            IS_DEFAULT = true,
                            CREATED_USER = User.Identity.Name,
                            CREATED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now
                        };
                        objLS.TB_FAMILY_TABLE_STRUCTURE.Add(objFamilTableStructures);
                    }
                    //createdByDefault check//
                    CreatedByDefaultCheck(catalogId, familyId);

                    #region To associate attribute Pack

                    if (attributeHierarchyList.Count > 0)
                    {
                        foreach (var item in attributeHierarchyList)
                        {
                            if (item != null && item.ID > 0)
                            {
                                int packId = Convert.ToInt32(item.PACK_ID);
                                var packageMaster = _dbcontext.TB_PACKAGE_MASTER.Where(s => s.GROUP_ID == packId && s.IS_FAMILY.ToLower()=="family").FirstOrDefault();
                                if (packageMaster != null && packageMaster.GROUP_ID > 0)
                                {
                                    AssociateFamilyAttributePack(catalogId, familyId, packId,categoryId);
                                }
                            }
                        }
                    }

                    #endregion
                    objLS.SaveChanges();
                    
                    //  return "Success. Family Saved.";
                    return familyId;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = (from validationErrors in dbEx.EntityValidationErrors from validationError in validationErrors.ValidationErrors select string.Format("{0}:{1}", validationErrors.Entry.Entity, validationError.ErrorMessage)).Aggregate<string, Exception>(dbEx, (current, message) => new InvalidOperationException(message, current));
                    _logger.Error("Error at FamilyController :SaveFamily", raise);
                    familyId = 0;
                    // throw raise;
                    return familyId;
                }
            }
            else
            {
                try
                {
                    var id = _dbcontext.STP_CATALOGSTUDIO5_InsertFamily(familyName, categoryId, 0, 1, status, catalogId, publishToWeb, publishToPrint, true,publishToPdf,publishToExport,publishToPortal, workflowstatus,User.Identity.Name).ToList();
                    familyId = Convert.ToInt32(id[0]);
                    using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        objSqlConnection.Open();
                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_LS_INSERTCLONEDCATFAMILY";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.Parameters.Add("@CATALOGID", SqlDbType.Int).Value = catalogId;
                        objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                        objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                        objSqlCommand.Parameters.Add("@USERNAME", SqlDbType.NVarChar).Value = User.Identity.Name;
                        objSqlCommand.ExecuteNonQuery();
                    }
                        string familyTableXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><TradingBell TableType=\"SuperTable\"><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>";
                    var objFamilTableStructure = new TB_FAMILY_TABLE_STRUCTURE
                    {
                        //  ID = id + 1,
                        CATALOG_ID = catalogId,
                        FAMILY_ID = familyId,
                        FAMILY_TABLE_STRUCTURE = familyTableXml,
                        STRUCTURE_NAME = "Default Layout",
                        IS_DEFAULT = true,
                        CREATED_USER = User.Identity.Name,
                        CREATED_DATE = DateTime.Now,
                        MODIFIED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now
                    };
                    objLS.TB_FAMILY_TABLE_STRUCTURE.Add(objFamilTableStructure);

                    var familytable = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(x => x.CATALOG_ID == 1 && x.FAMILY_ID == familyId && x.STRUCTURE_NAME == "Default Layout");
                    if (!familytable.Any())
                    {
                        var objFamilTableStructures = new TB_FAMILY_TABLE_STRUCTURE
                        {
                            //  ID = id + 1,
                            CATALOG_ID = 1,
                            FAMILY_ID = familyId,
                            FAMILY_TABLE_STRUCTURE = familyTableXml,
                            STRUCTURE_NAME = "Default Layout",
                            IS_DEFAULT = true,
                            CREATED_USER = User.Identity.Name,
                            CREATED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now
                        };
                        objLS.TB_FAMILY_TABLE_STRUCTURE.Add(objFamilTableStructures);
                    }
                    //createdByDefault check//
                    CreatedByDefaultCheck(catalogId, familyId);

                    #region To associate attribute Pack

                    if (attributeHierarchyList.Count > 0)
                    {
                        foreach (var item in attributeHierarchyList)
                        {
                            if (item != null && item.ID > 0)
                            {
                                int packId = Convert.ToInt32(item.PACK_ID);
                                AssociateFamilyAttributePack(catalogId, familyId, packId, categoryId);
                            }
                        }
                    } 

                    #endregion


                    _dbcontext.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = (from validationErrors in dbEx.EntityValidationErrors from validationError in validationErrors.ValidationErrors select string.Format("{0}:{1}", validationErrors.Entry.Entity, validationError.ErrorMessage)).Aggregate<string, Exception>(dbEx, (current, message) => new InvalidOperationException(message, current));
                    _logger.Error("Error at FamilyController :SaveFamily", raise);
                    familyId = 0;
                    // throw raise;
                    return familyId;
                }
            }
            // }
            // familyId = 0;
            return familyId;
        }
        public void CreatedByDefaultCheck(int catalogId, int familyId)
        {
            int[] type = { 13, 12, 11, 9, 7, 11 };
            var attributeDetails = objLS.TB_ATTRIBUTE.Join(objLS.TB_CATALOG_ATTRIBUTES, tps => tps.ATTRIBUTE_ID,
                        tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                        .Where(x => x.tpf.CATALOG_ID == catalogId && x.tps.CREATE_BY_DEFAULT == true && type.Contains(x.tps.ATTRIBUTE_TYPE))
                        .Select(x => x.tps);

            if (attributeDetails.Any())
            {
                foreach (var item in attributeDetails)
                {
                    var checkFamilySpecs = objLS.TB_FAMILY_SPECS.Where(x => x.FAMILY_ID == familyId && x.ATTRIBUTE_ID == item.ATTRIBUTE_ID);
                    var checkFamilyKey = objLS.TB_FAMILY_KEY.Where(a => a.CATALOG_ID == catalogId && a.FAMILY_ID == familyId && a.ATTRIBUTE_ID == item.ATTRIBUTE_ID);
                    if (item.ATTRIBUTE_TYPE == 13)
                    {
                        if (!checkFamilyKey.Any())
                        {
                            var objFamilyKey = new TB_FAMILY_KEY
                            {
                                CATALOG_ID = catalogId,
                                ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                                FAMILY_ID = familyId,
                                ATTRIBUTE_VALUE = null,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name
                            };
                            objLS.TB_FAMILY_KEY.Add(objFamilyKey);
                        }
                    }
                    else
                    {
                        if (!checkFamilySpecs.Any())
                        {
                            var objFamilyspecs = new TB_FAMILY_SPECS
                            {
                                STRING_VALUE = null,
                                FAMILY_ID = familyId,
                                ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                                NUMERIC_VALUE = null,
                                OBJECT_TYPE = null,
                                OBJECT_NAME = null,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name

                            };
                            objLS.TB_FAMILY_SPECS.Add(objFamilyspecs);
                        }
                    }
                }
                objLS.SaveChanges();
            }
        }

        public void AssociateFamilyAttributePack(int catalogId, int familyId, int packId, string categoryId)
        {
            string familyIdstring = Convert.ToString(familyId);

             TB_ATTRIBUTE_HIERARCHY attributeHierarchyInsert = new TB_ATTRIBUTE_HIERARCHY();
                        attributeHierarchyInsert = _dbcontext.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == familyIdstring && s.PACK_ID == packId && s.TYPE == "Family").FirstOrDefault();
                        if (attributeHierarchyInsert == null)
                        {
                            int customer_Id = 0;
                            HomeApiController objHome = new HomeApiController();
                            customer_Id = objHome.GetCustomerId();

                            string qry_String = "insert into TB_ATTRIBUTE_HIERARCHY values(" + packId + ",'" + "Family" + "','" + familyId + "'," + customer_Id + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                            SqlConnection con = new SqlConnection(connectionString);
                            SqlCommand cmdObj = new SqlCommand(qry_String, con);
                            con.Open();
                            cmdObj.ExecuteNonQuery();
                            con.Close();
                        }
            int[] type = { 13, 12, 11, 9, 7, 11 };
            var attributeDetails = objLS.TB_ATTRIBUTE.Join(objLS.TB_PACKAGE_DETAILS, tps => tps.ATTRIBUTE_ID,
                        tpf => tpf.ATTRIBUTE_ID, (tps, tpf) => new { tps, tpf })
                        .Where(x => type.Contains(x.tps.ATTRIBUTE_TYPE) && x.tpf.GROUP_ID == packId).Select(x => x.tps);


            if (attributeDetails.Any())
            {
                foreach (var item in attributeDetails)
                {
                    var checkFamilySpecs = objLS.TB_FAMILY_SPECS.Where(x => x.FAMILY_ID == familyId && x.ATTRIBUTE_ID == item.ATTRIBUTE_ID);
                    var checkFamilyKey = objLS.TB_FAMILY_KEY.Where(a => a.CATALOG_ID == catalogId && a.FAMILY_ID == familyId && a.ATTRIBUTE_ID == item.ATTRIBUTE_ID);
                    if (item.ATTRIBUTE_TYPE == 13)
                    {
                        if (!checkFamilyKey.Any())
                        {
                            var objFamilyKey = new TB_FAMILY_KEY
                            {
                                CATALOG_ID = catalogId,
                                CATEGORY_ID = categoryId,
                                ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                                FAMILY_ID = familyId,
                                ATTRIBUTE_VALUE = null,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name
                            };
                            objLS.TB_FAMILY_KEY.Add(objFamilyKey);
                        }
                    }
                    else
                    {
                        if (!checkFamilySpecs.Any())
                        {
                            var objFamilyspecs = new TB_FAMILY_SPECS
                            {
                                STRING_VALUE = null,
                                FAMILY_ID = familyId,
                                ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                                NUMERIC_VALUE = null,
                                OBJECT_TYPE = null,
                                OBJECT_NAME = null,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name

                            };
                            objLS.TB_FAMILY_SPECS.Add(objFamilyspecs);
                        }
                    }
                }
                objLS.SaveChanges();
            }
        }


        public int SaveSubFamily(string familyName, string categoryId, string status, bool publishToWeb, bool publishToPrint, int catalogId, int parentFamilyId, string workflowstatus, string Option, bool publishToPdf, bool publishToExport, bool publishToPortal)
        {
            int familyId = 0;
            int workflowstatuscode = 0;
            if (familyName != "")
            {
                familyName = AesEncrytDecry.DecryptStringAes(familyName);
            }
            if (workflowstatus != "")
            {
                var objworkflow = _dbcontext.TB_WORKFLOW_STATUS.FirstOrDefault(k => k.STATUS_NAME == workflowstatus);
                if (objworkflow != null)
                {
                    workflowstatuscode = objworkflow.STATUS_CODE;

                }
            }
            var workflowCheck = _dbcontext.Customer_Settings.Where(x => x.CustomerId == (_dbcontext.Customer_User.FirstOrDefault(a => a.User_Name == User.Identity.Name)).CustomerId);
            if (workflowCheck.Any())
            {
                var worflowStatus = workflowCheck.FirstOrDefault();
                if (worflowStatus != null)
                {
                    var value = worflowStatus.EnableWorkFlow;
                    if (!value)
                    {
                        workflowstatuscode = 99;
                    }

                }
            }

            #region To Associate Attribute Pack

            List<string> categoryIds = new List<string>();
            string category_Id = Convert.ToString(categoryId);
            TB_CATEGORY tb_category = new TB_CATEGORY();
            categoryIds.Add(category_Id);
            string catalogIdstring = Convert.ToString(catalogId);
            categoryIds.Add(catalogIdstring);
            string parentFamilyIdString = Convert.ToString(parentFamilyId);
            categoryIds.Add(parentFamilyIdString);
            var ds = new DataSet();

            using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESIGNEXPORT_IMPORT";
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Connection = objSqlConnection;
                objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = 0;
                objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETPARENT";
                objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.Int).Value = 0;
                objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.VarChar).Value = categoryId;
                objSqlConnection.Open();
                var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                objSqlDataAdapter.Fill(ds);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dtRow in ds.Tables[0].Rows)
                {
                    if (dtRow["CATEGORY_ID"].ToString() != null)
                    {
                        categoryIds.Add(dtRow["CATEGORY_ID"].ToString());
                    }
                }
            }

            List<TB_ATTRIBUTE_HIERARCHY> attributeHierarchyList = _dbcontext.TB_ATTRIBUTE_HIERARCHY.Where(s => categoryIds.Contains(s.ASSIGN_TO)).ToList();

            #endregion

            if (Option != "Clone")
            {
                var objFam = new TB_FAMILY
                {
                    FAMILY_NAME = familyName,
                    CATEGORY_ID = categoryId,
                    STATUS = status,
                    ROOT_FAMILY = 0,
                    PARENT_FAMILY_ID = parentFamilyId,
                    PUBLISH2WEB = publishToWeb,
                    PUBLISH2PRINT = publishToPrint,
                    PUBLISH2PDF = publishToPdf,
                    PUBLISH2EXPORT = publishToExport,
                    PUBLISH2PORTAL = publishToPortal,
                    DISPLAY_TABLE_HEADER = true,
                    MODIFIED_USER = User.Identity.Name,
                    CREATED_USER = User.Identity.Name,
                    CREATED_DATE = DateTime.Now,
                    MODIFIED_DATE = DateTime.Now,
                    WORKFLOW_STATUS = workflowstatuscode,
                    FLAG_RECYCLE = "A"
                };
                try
                {
                    objLS.TB_FAMILY.Add(objFam);
                    objLS.SaveChanges();
                    familyId = _dbcontext.TB_FAMILY.Max(x => x.FAMILY_ID);
                    int sortorder = 0;
                    var catalogfamily = _dbcontext.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == catalogId && x.CATEGORY_ID == categoryId);
                    if (catalogfamily.Any())
                    {
                        sortorder = catalogfamily.Max(x => x.SORT_ORDER);
                    }
                    if (familyId != 0)
                    {
                        var objCatalogFamily = new TB_CATALOG_FAMILY
                        {
                            FAMILY_ID = familyId,
                            CATEGORY_ID = categoryId,
                            CATALOG_ID = catalogId,
                            SORT_ORDER = sortorder + 1,
                            ROOT_CATEGORY = "0",
                            MODIFIED_USER = User.Identity.Name,
                            CREATED_USER = User.Identity.Name,
                            CREATED_DATE = DateTime.Now,
                            MODIFIED_DATE = DateTime.Now,
                            FLAG_RECYCLE = "A"
                        };
                        objLS.TB_CATALOG_FAMILY.Add(objCatalogFamily);
                        objLS.SaveChanges();
                        if (_dbcontext.TB_SUBFAMILY.FirstOrDefault(x => x.FAMILY_ID == parentFamilyId) != null)
                        {
                            int subfamilysortorder =
                                _dbcontext.TB_SUBFAMILY.Where(x => x.FAMILY_ID == parentFamilyId)
                                    .Max(x => x.SORT_ORDER);
                            var objSubFamily = new TB_SUBFAMILY
                            {
                                FAMILY_ID = parentFamilyId,
                                SUBFAMILY_ID = familyId,
                                SORT_ORDER = subfamilysortorder + 1,
                                MODIFIED_USER = User.Identity.Name,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_DATE = DateTime.Now
                            };

                            objLS.TB_SUBFAMILY.Add(objSubFamily);
                            objLS.SaveChanges();
                        }
                        else
                        {
                            var objSubFamily = new TB_SUBFAMILY
                            {
                                FAMILY_ID = parentFamilyId,
                                SUBFAMILY_ID = familyId,
                                SORT_ORDER = 1,
                                MODIFIED_USER = User.Identity.Name,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_DATE = DateTime.Now
                            };

                            objLS.TB_SUBFAMILY.Add(objSubFamily);
                            objLS.SaveChanges();

                        }


                        string familyTableXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><TradingBell TableType=\"SuperTable\"><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>";
                        var objFamilTableStructure = new TB_FAMILY_TABLE_STRUCTURE
                        {
                            //  ID = id + 1,
                            CATALOG_ID = catalogId,
                            FAMILY_ID = familyId,
                            FAMILY_TABLE_STRUCTURE = familyTableXml,
                            STRUCTURE_NAME = "Default Layout",
                            IS_DEFAULT = true,
                            CREATED_USER = User.Identity.Name,
                            CREATED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now
                        };
                        objLS.TB_FAMILY_TABLE_STRUCTURE.Add(objFamilTableStructure);
                        objLS.SaveChanges();

                        int table_Id = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Max(x => x.ID);
                        HomeApiController apiObj = new HomeApiController();
                        string structureName = "Default Layout";
                        List<TB_PACKAGE_MASTER> packs = _dbcontext.TB_PACKAGE_MASTER.Where(s => s.CATALOG_ID == catalogId && s.IS_FAMILY.ToLower() == "product").ToList();
                        foreach (var pack in packs)
                        {
                            if (pack != null)
                            {
                                string xmlString = apiObj.CreateXMLAttributePack(pack.GROUP_ID);
                                string sqlStr = "Insert into TB_ATTRIBUTE_PACK_TABLE_STRUCTURE (FAMILY_STRUCTURE_ID,PACK_ID,TABLE_STRUCTURE,STRUCTURE_NAME,IS_DEFAULT,CREATED_USER,CREATED_DATE,MODIFIED_USER,MODIFIED_DATE) values (" + table_Id + "," + pack.GROUP_ID + ",'" + xmlString.Replace("'", "''") + "','" + structureName + "'," + 1 + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                using (var objConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                {
                                    objConnection.Open();
                                    SqlCommand objSqlCommand = objConnection.CreateCommand();
                                    objSqlCommand.CommandText = sqlStr;
                                    objSqlCommand.CommandType = CommandType.Text;
                                    objSqlCommand.Connection = objConnection;
                                    objSqlCommand.ExecuteNonQuery();
                                }
                            }
                        }


                        int id = familyId;
                        var familytable = _dbcontext.TB_FAMILY_TABLE_STRUCTURE.Where(x => x.CATALOG_ID == 1 && x.FAMILY_ID == id && x.STRUCTURE_NAME == "Default Layout");
                        if (!familytable.Any())
                        {
                            var objFamilTableStructures = new TB_FAMILY_TABLE_STRUCTURE
                            {
                                //  ID = id + 1,
                                CATALOG_ID = 1,
                                FAMILY_ID = id,
                                FAMILY_TABLE_STRUCTURE = familyTableXml,
                                STRUCTURE_NAME = "Default Layout",
                                IS_DEFAULT = true,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now
                            };
                            objLS.TB_FAMILY_TABLE_STRUCTURE.Add(objFamilTableStructures);
                        }
                        objLS.SaveChanges();
                        CreatedByDefaultCheck(catalogId, familyId);

                        #region To associate attribute Pack

                        if (attributeHierarchyList.Count > 0)
                        {
                            foreach (var item in attributeHierarchyList)
                            {
                                if (item != null && item.ID > 0)
                                {
                                    int packId = Convert.ToInt32(item.PACK_ID);
                                    var packageMaster = _dbcontext.TB_PACKAGE_MASTER.Where(s => s.GROUP_ID == packId && s.IS_FAMILY.ToLower() == "family").FirstOrDefault();
                                    if (packageMaster != null && packageMaster.GROUP_ID > 0)
                                    {
                                        AssociateFamilyAttributePack(catalogId, familyId, packId, categoryId);
                                    }
                                }
                            }
                        }

                        #endregion
                    }
                    return familyId;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = (from validationErrors in dbEx.EntityValidationErrors from validationError in validationErrors.ValidationErrors select string.Format("{0}:{1}", validationErrors.Entry.Entity, validationError.ErrorMessage)).Aggregate<string, Exception>(dbEx, (current, message) => new InvalidOperationException(message, current));
                    _logger.Error("Error at FamilyController :GetAllCatalogAttributes", raise);
                    return familyId;
                    // throw raise;
                }
            }
            else
            {
                try
                {
                    var Clone = _dbcontext.STP_CATALOGSTUDIO5_InsertFamily(familyName, categoryId, parentFamilyId, 0, status, catalogId, publishToWeb, publishToWeb, true, publishToPdf,publishToExport,publishToPortal, workflowstatus,User.Identity.Name).ToList();
                    familyId = Convert.ToInt32(Clone[0]);
                    _dbcontext.SaveChanges();
                    return familyId;
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
                {
                    Exception raise = (from validationErrors in dbEx.EntityValidationErrors from validationError in validationErrors.ValidationErrors select string.Format("{0}:{1}", validationErrors.Entry.Entity, validationError.ErrorMessage)).Aggregate<string, Exception>(dbEx, (current, message) => new InvalidOperationException(message, current));
                    _logger.Error("Error at FamilyController :GetAllCatalogAttributes", raise);
                    return familyId;
                    // throw raise;
                }
            }
            //}
            //return familyId;
        }

        public bool UpdateFamily(int familyId, string familyName, string categoryId, string status, bool publishToWeb, bool publishToPrint, string workflowstatus, bool publishToPdf, bool publishToExport, bool publishToPortal)
        {
            int workflowstatuscode = 0;
            try
            {

                if (familyName != "")
                {
                    familyName = AesEncrytDecry.DecryptStringAes(familyName);
                }

               if (workflowstatus != "" && workflowstatus != "undefined")
                {
                    var objworkflow = _dbcontext.TB_WORKFLOW_STATUS.FirstOrDefault(k => k.STATUS_NAME == workflowstatus);
                    if (objworkflow != null)
                    {
                        workflowstatuscode = objworkflow.STATUS_CODE;

                    }
                }
               else
               {  
                   workflowstatuscode = 99;
               }
                var objFamily = objLS.TB_FAMILY.Single(s => s.FAMILY_ID == familyId);
                objFamily.FAMILY_NAME = familyName;
                objFamily.CATEGORY_ID = categoryId;
                objFamily.STATUS = status;
                objFamily.PUBLISH = publishToWeb;
                objFamily.PUBLISH2PRINT = publishToPrint;
                objFamily.WORKFLOW_STATUS = workflowstatuscode;
                objFamily.MODIFIED_USER = User.Identity.Name;
                objFamily.PUBLISH2WEB = publishToWeb;
                objFamily.PUBLISH2PDF = publishToPdf;
                objFamily.PUBLISH2EXPORT = publishToExport;
                objFamily.PUBLISH2PORTAL = publishToPortal;
                objLS.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at UpdateFamily", ex);
                return false;
            }
        }
        public IList GetAllCatalogAttributes(int catalogid)
        {
            try
            {
                var attributes = _dbcontext.TB_CATALOG_ATTRIBUTES.Where(x => x.CATALOG_ID == catalogid).OrderBy(x => x.ATTRIBUTE_ID).ToList().Select(x => new NewDataSet
                {
                    ATTRIBUTE_NAME = x.TB_ATTRIBUTE.ATTRIBUTE_NAME,
                    ATTRIBUTE_TYPE = x.TB_ATTRIBUTE.ATTRIBUTE_TYPE,
                    ATTRIBUTE_ID = x.TB_ATTRIBUTE.ATTRIBUTE_ID,
                    CREATE_BY_DEFAULT = x.TB_ATTRIBUTE.CREATE_BY_DEFAULT,
                    VALUE_REQUIRED = x.TB_ATTRIBUTE.VALUE_REQUIRED,
                    STYLE_NAME = x.TB_ATTRIBUTE.STYLE_NAME,
                    STYLE_FORMAT = x.TB_ATTRIBUTE.STYLE_FORMAT,
                    DEFAULT_VALUE = x.TB_ATTRIBUTE.DEFAULT_VALUE,
                    PUBLISH2PRINT = x.TB_ATTRIBUTE.PUBLISH2PRINT,
                    PUBLISH2WEB = x.TB_ATTRIBUTE.PUBLISH2WEB,
                    PUBLISH2CDROM = x.TB_ATTRIBUTE.PUBLISH2CDROM,
                    PUBLISH2ODP = x.TB_ATTRIBUTE.PUBLISH2ODP,
                    USE_PICKLIST = x.TB_ATTRIBUTE.USE_PICKLIST,
                    ATTRIBUTE_DATATYPE = x.TB_ATTRIBUTE.ATTRIBUTE_DATATYPE,
                    ATTRIBUTE_DATAFORMAT = x.TB_ATTRIBUTE.ATTRIBUTE_DATAFORMAT,
                    ATTRIBUTE_DATARULE = x.TB_ATTRIBUTE.ATTRIBUTE_DATARULE,
                    dataRuleList = XmlDeserializefunction(x.TB_ATTRIBUTE.ATTRIBUTE_DATARULE),
                    UOM = x.TB_ATTRIBUTE.UOM,
                    IS_CALCULATED = x.TB_ATTRIBUTE.IS_CALCULATED,
                    ATTRIBUTE_CALC_FORMULA = x.TB_ATTRIBUTE.ATTRIBUTE_CALC_FORMULA,
                    PICKLIST_NAME = x.TB_ATTRIBUTE.PICKLIST_NAME

                }).ToList();
                //  HttpContext.Current.Session["attributeList"] = attributes;
                return attributes;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at GetAllCatalogAttributes", objException);
                return null;
            }


        }
        private List<DataRule> XmlDeserializefunction(string attributeDatarule)
        {
            try
            {
                if (!string.IsNullOrEmpty(attributeDatarule))
                {
                    var deserializer = new XmlSerializer(typeof(NewDataSet));
                    TextReader reader = new StringReader(attributeDatarule);
                    object obj = deserializer.Deserialize(reader);
                    var xmlData = (NewDataSet)obj;
                    return xmlData.dataRuleList;
                }
                return null;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XmlDeserializefunction", objException);

                return null;
            }


        }
        public IList GetCatalogFamilyFilter(int catalogid)
        {
            try
            {
                var catalogFilter = _dbcontext.TB_CATALOG.Where(x => x.CATALOG_ID == catalogid).ToList().Select(x => new CatalogFamilyOption
                {
                    CATALOG_ID = x.CATALOG_ID,
                    CATALOG_NAME = x.CATALOG_NAME,
                    VERSION = x.VERSION,
                    DESCRIPTION = x.DESCRIPTION,
                    FAMILY_FILTERS = x.FAMILY_FILTERS,
                    familyfilter = XmlDeserializeFamilyfunction(x.FAMILY_FILTERS),
                    PRODUCT_FILTERS = x.PRODUCT_FILTERS,
                    productfilter = XmlDeserializeProductfunction(x.PRODUCT_FILTERS),
                    CREATED_USER = x.CREATED_USER,
                    CREATED_DATE = x.CREATED_DATE,
                    MODIFIED_USER = x.MODIFIED_USER,
                    MODIFIED_DATE = x.MODIFIED_DATE
                }).ToList();
                return catalogFilter;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at GetCatalogFamilyFilter", objException);
                return null;
            }
        }
        private List<FamilyFilters> XmlDeserializeFamilyfunction(string familyfilter)
        {
            try
            {
                if (!string.IsNullOrEmpty(familyfilter))
                {
                    var deserializer = new XmlSerializer(typeof(CatalogFamilyOption));
                    TextReader reader = new StringReader(familyfilter);
                    object obj = deserializer.Deserialize(reader);
                    var xmlData = (CatalogFamilyOption)obj;
                    return xmlData.familyfilter;
                }
                return null;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XmlDeserializefunction", objException);
                return null;
            }


        }
        private List<ProductFilter> XmlDeserializeProductfunction(string productfilter)
        {
            try
            {
                if (!string.IsNullOrEmpty(productfilter))
                {
                    var deserializers = new XmlSerializer(typeof(CatalogFamilyOption));
                    if (productfilter.Contains("<CatalogProductOption>"))
                    {
                        productfilter = productfilter.Replace("<CatalogProductOption>", "<CatalogFamilyOption>");
                        productfilter = productfilter.Replace("</CatalogProductOption>", "</CatalogFamilyOption>");
                    }
                    TextReader txtreader = new StringReader(productfilter);

                    object objt = deserializers.Deserialize(txtreader);
                    var xmlDatas = (CatalogFamilyOption)objt;
                    return xmlDatas.productfilter;
                }
                return null;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at XmlDeserializeProductfunction", objException);
                return null;
            }
        }

        public string ConstructHtmlTags(DataTable prodSpecsData, string val, int catalogId, int value)
        {
            try
            {


                var startConstructTable = "<table class='" + "table ng-scope ng-table" + "'>";
                var startTableRow = "<tr>";
                var endTableRow = "</tr>";
                var startTablehead = "<th>";
                var endTablehead = "</th>";
                var startTableCell = "<td>";
                var endTableCell = "</td>";
                var endConstructTable = "</table>";
                var breakTag = "<br/>";
                var constructHtmltags = val;
                if (prodSpecsData.Rows.Count > 0)
                {
                    DataRow row;
                    var prodSpecsConditionTable = new DataTable();
                    constructHtmltags = constructHtmltags + startConstructTable;
                    constructHtmltags = constructHtmltags + startTableRow;
                    var prodSpecsColumnValue = "";
                    //   constructHtmltags = constructHtmltags + startTableCell + prodSpecsColumnValue + endTableCell;
                    for (int column = 0; column < prodSpecsData.Columns.Count; column++)
                    {
                        prodSpecsColumnValue = prodSpecsData.Columns[column].Caption;
                        prodSpecsConditionTable.Columns.Add(prodSpecsColumnValue);
                    }

                    for (int addrow = 0; addrow < prodSpecsData.Rows.Count; addrow++)
                    {
                        row = prodSpecsConditionTable.NewRow();
                        for (int column = 0; column < prodSpecsConditionTable.Columns.Count; column++)
                        {
                            row[prodSpecsConditionTable.Columns[column].ToString()] = prodSpecsData.Rows[addrow][column];
                        }
                        prodSpecsConditionTable.Rows.Add(row);
                    }
                    for (int remvecln = 0; remvecln <= value; remvecln++)
                    {
                        prodSpecsConditionTable.Columns.RemoveAt(0);
                    }
                    var attributelist = (List<NewDataSet>)GetAllCatalogAttributes(catalogId);
                    if (attributelist != null)
                    {
                        for (int column = 0; column < prodSpecsConditionTable.Columns.Count; column++)
                        {
                            prodSpecsColumnValue = prodSpecsConditionTable.Columns[column].Caption;
                            constructHtmltags = constructHtmltags + startTablehead + prodSpecsColumnValue + endTablehead;
                            foreach (var iAtrrList in attributelist)
                            {
                                if (prodSpecsColumnValue != iAtrrList.ATTRIBUTE_NAME) continue;
                                var pList = (List<DataRule>)iAtrrList.dataRuleList;
                                if (pList != null)
                                {
                                    foreach (var condition in pList)
                                    {
                                        if (condition.ApplyTo == "All")
                                        {

                                            for (int roworder = 0; roworder <= prodSpecsConditionTable.Rows.Count - 1; roworder++)
                                            {
                                                var rowvalue = prodSpecsConditionTable.Rows[roworder][prodSpecsColumnValue].ToString();
                                                if (rowvalue == condition.Condition)
                                                {
                                                    switch (condition.ApplyForNumericOnly)
                                                    {
                                                        case "":
                                                            prodSpecsConditionTable.Rows[roworder][prodSpecsColumnValue] = rowvalue.Replace(prodSpecsData.Rows[roworder][prodSpecsColumnValue].ToString(), condition.Condition);
                                                            break;
                                                        case "1":
                                                            prodSpecsConditionTable.Rows[roworder][prodSpecsColumnValue] = condition.Prefix + rowvalue.Replace(prodSpecsData.Rows[roworder][prodSpecsColumnValue].ToString(), condition.Condition) + condition.Suffix;
                                                            break;
                                                    }
                                                }
                                                else
                                                {
                                                    prodSpecsConditionTable.Rows[roworder][prodSpecsColumnValue] = condition.Prefix + prodSpecsData.Rows[roworder][prodSpecsColumnValue] + condition.Suffix;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            for (int roworder = 0; roworder < 1; roworder++)
                                            {
                                                prodSpecsConditionTable.Rows[roworder][prodSpecsColumnValue] = condition.Prefix + prodSpecsData.Rows[roworder][prodSpecsColumnValue] + condition.Suffix;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    constructHtmltags = constructHtmltags + endTableRow;
                    var selectcolumn = prodSpecsConditionTable.Columns[0].Caption;
                    DataRow[] prodSpecsRow = prodSpecsConditionTable.Select("'" + selectcolumn + "'<>'" + 0 + "'");
                    foreach (var iListItem in prodSpecsRow)
                    {
                        constructHtmltags = constructHtmltags + startTableRow;
                        foreach (object t in iListItem.ItemArray)
                        {
                            if (t.ToString().Contains(".jpg") || t.ToString().Contains(".JPG") || t.ToString().Contains(".png") || t.ToString().Contains(".gif") || t.ToString().Contains(".eds") || t.ToString().Contains(".bmp"))
                            {
                                constructHtmltags = constructHtmltags + startTableCell + "<img width=" + "50" + " height=" + "50" + " src=";
                                if (System.IO.File.Exists(t.ToString()))
                                {
                                    constructHtmltags = constructHtmltags + '"' + t.ToString() + '"' + "/>" + breakTag + breakTag + t.ToString() + endTableCell;
                                }
                                else
                                {
                                    constructHtmltags = constructHtmltags + '"' + @"\Images\noimage.jpg" + '"' + "/>" + breakTag + t.ToString() + endTableCell;
                                }
                            }
                            else
                            {
                                constructHtmltags = constructHtmltags + startTableCell;
                                constructHtmltags = constructHtmltags + t.ToString() + endTableCell;
                            }
                        }
                        constructHtmltags = constructHtmltags + endTableRow;
                    }
                    constructHtmltags = constructHtmltags + endConstructTable;
                }
                constructHtmltags = constructHtmltags + breakTag + breakTag;
                return constructHtmltags;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at ConstructHtmlTags", objException);
            }
            return null;
        }

        public string GetFamilyPreviewList(int familyId, string categoryId, int catalogId)
        {
            try
            {
                // familyId = 20; categoryId = "ABC002";
                var catalogFilter = (List<CatalogFamilyOption>)GetCatalogFamilyFilter(catalogId);
                if (!familyId.Equals(null))
                {
                    using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        var constructHtmltags = string.Empty;
                        string val = string.Empty;
                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_FAMILY_SKU_ATTRIBUTES_LS";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.Parameters.Add("@CAT_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                        objSqlCommand.Parameters.Add("@FAM_IDS", SqlDbType.NVarChar, 100).Value = familyId;
                        objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                        var da = new SqlDataAdapter(objSqlCommand);
                        var familySpecsData = new DataTable();
                        da.Fill(familySpecsData);
                        string attributename;
                        var attributevalue = string.Empty;
                        if (familySpecsData.Rows.Count <= 0) return val;
                        if (familySpecsData.Rows[0][0].ToString() != "0")
                        {
                            for (int column = 0; column < familySpecsData.Columns.Count - 1; column++)
                            {
                                attributename = familySpecsData.Columns[column].Caption;
                                if (attributename != null)
                                {
                                    for (int row = 0; row < familySpecsData.Rows.Count; row++)
                                    {
                                        attributevalue = familySpecsData.Rows[row][attributename].ToString();
                                    }
                                }
                                if (catalogFilter.Count > 0)
                                {
                                    foreach (var filt in catalogFilter.Select(iFilters => (List<FamilyFilters>)iFilters.familyfilter).SelectMany(fList => fList))
                                    {
                                        if (attributename == filt.Attribute)
                                        {
                                            if (attributevalue == filt.Value)
                                            {
                                                var familylist = objLS.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == catalogId && x.CATEGORY_ID == categoryId && x.FAMILY_ID == familyId).Select(y => new
                                                {
                                                    y.TB_CATALOG.CATALOG_NAME,
                                                    CATEGORY_NAME = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == y.CATEGORY_ID && tc.CUSTOMER_ID == (_dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                                    y.TB_FAMILY.FAMILY_NAME
                                                }).ToList();
                                                foreach (var iListItem in familylist)
                                                {
                                                    constructHtmltags = "<div class='" + "titleblue" + "'>";
                                                    constructHtmltags = constructHtmltags + iListItem.CATALOG_NAME + "&nbsp;&gt;&nbsp;" + iListItem.CATEGORY_NAME + "&nbsp;&gt;&nbsp;" + iListItem.FAMILY_NAME + "</div>";
                                                }
                                                val = ConstructHtmlTags(familySpecsData, constructHtmltags, catalogId, 0);
                                                constructHtmltags = val;

                                            }
                                            else
                                            {
                                                val = "<h2>Preview is not available</h2>";
                                            }
                                        }
                                        else
                                        {
                                            val = "<h2>Preview is not available</h2>";
                                        }
                                    }
                                }
                                else
                                {
                                    var familylist = objLS.TB_CATALOG_FAMILY.Where(x => x.CATALOG_ID == catalogId && x.CATEGORY_ID == categoryId && x.FAMILY_ID == familyId).Select(y => new
                                    {
                                        y.TB_CATALOG.CATALOG_NAME,
                                        CATEGORY_NAME = _dbcontext.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == y.CATEGORY_ID && tc.CUSTOMER_ID == (_dbcontext.Customer_User.FirstOrDefault(usr => usr.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                        y.TB_FAMILY.FAMILY_NAME
                                    }).ToList();
                                    if (familylist.Count <= 0) continue;
                                    foreach (var iListItem in familylist)
                                    {
                                        constructHtmltags = "<div class='" + "titleblue" + "'>";
                                        constructHtmltags = constructHtmltags + iListItem.CATALOG_NAME + "&nbsp;&gt;&nbsp;" + iListItem.CATEGORY_NAME + "&nbsp;&gt;&nbsp;" + iListItem.FAMILY_NAME + "</div>";
                                    }
                                    val = ConstructHtmlTags(familySpecsData, constructHtmltags, catalogId, 0);
                                    constructHtmltags = val;
                                }
                            }
                            if (val.Equals("<h2>Preview is not available</h2>")) return val;
                            objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ProductTablePreview";
                            objSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                            objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 100).Value = familyId;
                            var prodData = new SqlDataAdapter(objSqlCommand);
                            DataTable prodSpecsData = new DataTable();
                            prodData.Fill(prodSpecsData);
                            if (prodSpecsData.Rows.Count > 0)
                            {
                                for (int prodcolumn = 0; prodcolumn < prodSpecsData.Columns.Count - 1; prodcolumn++)
                                {
                                    attributename = prodSpecsData.Columns[prodcolumn].Caption;
                                    if (attributename != null)
                                    {
                                        for (int row = 0; row < prodSpecsData.Rows.Count; row++)
                                        {
                                            attributevalue = prodSpecsData.Rows[row][attributename].ToString();
                                        }
                                    }
                                    if (catalogFilter.Count > 0)
                                    {
                                        foreach (var prodfilt in catalogFilter.Select(iprodFilters => (List<ProductFilter>)iprodFilters.productfilter).SelectMany(pList => pList))
                                        {
                                            if (attributename == prodfilt.Attribute)
                                            {
                                                val = attributevalue == prodfilt.Value ? ConstructHtmlTags(prodSpecsData, constructHtmltags, catalogId, 1) : "<p>Product Preview  is not available</p>";
                                            }
                                            else
                                            {
                                                val = "<p>Item Preview is not available</p>";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        val = ConstructHtmlTags(prodSpecsData, constructHtmltags, catalogId, 1);
                                    }

                                }
                                constructHtmltags = val;
                            }
                            val = ConstructMultipletable(familyId, catalogId, constructHtmltags);
                        }
                        else
                        {
                            val = "<h2>Preview is not available</h2>";
                        }
                        return val;
                    }

                }
            }
            catch (Exception exp)
            {
                _logger.Error("Error at GetFamilyPreviewList", exp);
                Console.WriteLine(exp.Message);
            }
            return string.Empty;
        }
        public string ConstructMultipletable(int familyId, int catalogId, string constructHtmltags)
        {
            try
            {
                var attributeIds = string.Empty;
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    // familyId = 2; catalogId = 2;
                    var multitable = objLS.TB_ATTRIBUTE_GROUP_SECTIONS.Where(x => x.CATALOG_ID == catalogId && x.FAMILY_ID == familyId)
                       .Join(objLS.TB_ATTRIBUTE_GROUP_SPECS, a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { b.ATTRIBUTE_ID }).ToList();
                    if (multitable.Count > 0)
                    {
                        attributeIds = multitable.Aggregate(attributeIds, (current, ids) => current + ids.ATTRIBUTE_ID + ",");
                        attributeIds = attributeIds.Substring(0, attributeIds.Length - 1);
                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_MULTIPLE_TABLE_FAMILY_PREVIEW";
                        objSqlCommand.CommandType = System.Data.CommandType.StoredProcedure;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyId;
                        objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                        objSqlCommand.Parameters.Add("@ATTRIDS", SqlDbType.VarChar, 500).Value = attributeIds;
                        var prodData = new SqlDataAdapter(objSqlCommand);
                        DataTable multipleSpecsData = new DataTable();
                        DataTable multipleSpecsData1 = new DataTable();
                        prodData.Fill(multipleSpecsData);
                        prodData.Fill(multipleSpecsData1);
                        multipleSpecsData1.Columns.Remove("GROUP_ID");
                        multipleSpecsData1.Columns.Remove("GROUP_NAME");
                        multipleSpecsData1.Columns.Remove("PRODUCT_ID");
                        multipleSpecsData1.Columns.Remove("SORT_ORDER");
                        var startConstructTable = "<table class='" + "table ng-scope ng-table" + "'>";
                        var startTableRow = "<tr>";
                        var endTableRow = "</tr>";
                        var startTablehead = "<th>";
                        var endTablehead = "</th>";
                        var startTableCell = "<td>";
                        var endTableCell = "</td>";
                        var endConstructTable = "</table>";
                        if (multipleSpecsData.Rows.Count > 0)
                        {
                            if (Convert.ToInt32(multipleSpecsData.Rows[0][0]) != 0)
                            {
                                var selectcolumn = multipleSpecsData.Columns[0].Caption;
                                for (int row = 0; row <= multipleSpecsData.Rows.Count; row++)
                                {
                                    DataRow[] familySpecsRow = multipleSpecsData.Select("'" + selectcolumn + "'<>'" + multipleSpecsData.Rows[row][selectcolumn] + "'");
                                    constructHtmltags = constructHtmltags + "<h3>" + multipleSpecsData.Rows[row]["GROUP_NAME"] + ":" + "</h3>";
                                    row = familySpecsRow.Length;

                                    constructHtmltags = constructHtmltags + startConstructTable;
                                    constructHtmltags = constructHtmltags + startTableRow;
                                    for (int column = 0; column < multipleSpecsData1.Columns.Count - 1; column++)
                                    {
                                        var familySpecsColumnValue = multipleSpecsData1.Columns[column].Caption;
                                        constructHtmltags = constructHtmltags + startTablehead + familySpecsColumnValue + endTablehead;
                                    }
                                    constructHtmltags = constructHtmltags + endTableRow;
                                    foreach (var iListItem in familySpecsRow)
                                    {
                                        constructHtmltags = constructHtmltags + startTableRow;
                                        for (int item = 4; item < iListItem.ItemArray.Length - 1; item++)
                                        {
                                            constructHtmltags = constructHtmltags + startTableCell;
                                            constructHtmltags = constructHtmltags + iListItem.ItemArray[item] + endTableCell;
                                        }
                                        constructHtmltags = constructHtmltags + endTableRow;
                                    }
                                    constructHtmltags = constructHtmltags + endConstructTable;
                                    objSqlCommand.Dispose();
                                }
                            }
                            else
                            {
                                constructHtmltags = constructHtmltags + "<p>Multiple Table Preview is not available</p>";
                            }
                        }
                        else
                        {
                            constructHtmltags = constructHtmltags + "<p>Multiple Table Preview is not available</p>";
                        }
                        // constructHtmltags=constructHtmltags+
                    }
                    else
                    {
                        constructHtmltags = constructHtmltags + "<p>Multiple Table Preview is not available</p>";
                    }
                }

                return constructHtmltags;
            }
            catch (Exception exp)
            {
                _logger.Error("Error at ConstructMultipletable", exp);
                Console.WriteLine(exp.Message);
            }
            return string.Empty;
        }
        public JsonResult GetFamilySpecs(string familyId, string categoryId, string catalogId)
        {
            try
            {
                if (familyId != null)
                {
                    int famid = Convert.ToInt32(familyId);
                    //var FamSku = objLS.STP_CATALOGSTUDIO5_FAMILY_SKU_ATTRIBUTES(FamilyId, CatalogId, CategoryId);
                    // var objFamSpecs = objLS.STP_LS_FAMILY_SPECS(famid, 3, 11).ToList();
                    //var familySpecs = objLS.TB_FAMILY_SPECS.Where(s => s.FAMILY_ID == famid).Select(s => new LS_TB_FAMILY_SPECS
                    //{
                    //    STRING_VALUE = s.STRING_VALUE,
                    //    FAMILY_ID = s.FAMILY_ID,
                    //    ATTRIBUTE_ID = s.ATTRIBUTE_ID
                    //}).ToList();

                    //var query = from item in objLS.TB_FAMILY_SPECS
                    //            let key = new { family_id = item.FAMILY_ID }
                    //            group new { string_value = item.STRING_VALUE, attribute_id = item.ATTRIBUTE_ID } by key;
                    //l.Pivot(emp => emp.Function, emp2 => emp2.Department, lst => lst.Count());
                    #region "Pivot Linq"
                    //        var dys = Enumerable.Range(1, objFamSpecs.Max(p => p.ATTRIBUTE_NAME));

                    //        var grps =
                    //from p in objFamSpecs
                    //group p by new
                    //{
                    //    p.FAMILY_ID,
                    //    p.ATTR_ID,
                    //} into g
                    //let lookup = g.ToLookup(x => x.ATTRIBUTE_NAME, x => x.VALUE)
                    //select new
                    //{
                    //    g.Key.FAMILY_ID,
                    //    g.Key.ATTR_ID,
                    //    DYs = dys.Select(x => lookup[x].Max()).ToArray(),
                    //};
                    #endregion
                    #region Pivot Table

                    var dynamicColumns = new Dictionary<string, string>();
                    var sb = new StringBuilder();
                    var sw = new StringWriter(sb);
                    JsonWriter jsonWriter = new JsonTextWriter(sw);
                    var model = new DashBoardModel();

                    using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_FAMILY_SKU_ATTRIBUTES_LS";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.Parameters.Add("@CAT_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                        objSqlCommand.Parameters.Add("@FAM_IDS", SqlDbType.NVarChar, 100).Value = familyId;
                        objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                        objSqlConnection.Open();
                        var da = new SqlDataAdapter(objSqlCommand);
                        var fs = new DataSet();
                        da.Fill(fs);
                        fs.Tables[0].Columns.Remove(fs.Tables[0].Columns[4].ColumnName);
                        fs.Tables[0].Columns.Remove(fs.Tables[0].Columns[3].ColumnName);

                        //var fs2;
                        //var fs1 = FSATTR.DataTableToList<FSATTR>(FS.Tables[0]);
                        //IEnumerable<DataRow> sequence = FS.Tables[0].AsEnumerable();

                        using (SqlDataReader reader = objSqlCommand.ExecuteReader())
                        {
                            jsonWriter.WriteStartObject();
                            jsonWriter.WritePropertyName("Data");
                            jsonWriter.WriteStartArray();
                            while (reader.Read())
                            {
                                jsonWriter.WriteStartObject();
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    jsonWriter.WritePropertyName(reader.GetName(i));
                                    jsonWriter.WriteValue(reader.GetValue(i).ToString());
                                    if (!dynamicColumns.ContainsKey(reader.GetName(i)) && !string.IsNullOrEmpty(reader.GetName(i)))
                                    {
                                        dynamicColumns.Add(reader.GetName(i), reader.GetValue(i).ToString());
                                    }
                                }
                                jsonWriter.WriteEndObject();
                            }
                            jsonWriter.WriteEndArray();
                            jsonWriter.WritePropertyName("Columns");
                            jsonWriter.WriteStartArray();
                            foreach (string key in dynamicColumns.Keys)
                            {
                                jsonWriter.WriteStartObject();
                                jsonWriter.WritePropertyName("title");
                                jsonWriter.WriteValue(key);
                                jsonWriter.WritePropertyName("field");
                                jsonWriter.WriteValue(key);
                                jsonWriter.WriteEndObject();
                            }
                            jsonWriter.WriteEndArray();
                            jsonWriter.WriteEndObject();
                        }
                        model.Data = sb.ToString();
                    }
                    //return new JsonResult() { Data = model, JsonRequestBehavior.AllowGet };
                    //if (1 == 1)
                    // {
                    var temp = model.Data.ToList();
                    return Json(model, JsonRequestBehavior.AllowGet);
                    //return List(model);
                    // }
                    // else
                    //     return Json(objFamSpecs, JsonRequestBehavior.AllowGet);
                    #endregion
                }
                return null;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at GetFamilySpecs", ex);
                return null;
            }

        }

        public void SaveFamilySpecs(string data, string editeddataref)
        {
            try
            {
                var familySpecsDataSource = JsonConvert.DeserializeObject<List<RootObject>>(data);
                var editedFamilySpecs = JsonConvert.DeserializeObject<List<RootObject>>(editeddataref);

                var editedAttr = new HashSet<int>(editedFamilySpecs.Select(s => s.AttrId));
                var editedList = familySpecsDataSource.Where(s => editedAttr.Contains(s.AttrId)).ToList();
                foreach (RootObject t in editedList)
                {
                    var objFamSpecs = objLS.TB_FAMILY_SPECS.SingleOrDefault(s => s.FAMILY_ID == t.FamilyId && s.ATTRIBUTE_ID == t.AttrId);
                    if (objFamSpecs != null) objFamSpecs.STRING_VALUE = t.VALUE;
                    objLS.SaveChanges();
                    break;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at SaveFamilySpecs", ex);
            }
        }

        CodeCompileUnit _targetUnit;
        CodeTypeDeclaration _targetClass;

        public void Sample()
        {
            _targetUnit = new CodeCompileUnit();
            var samples = new CodeNamespace("CodeDOMSample");
            samples.Imports.Add(new CodeNamespaceImport("System"));
            _targetClass = new CodeTypeDeclaration("CodeDOMCreatedClass")
            {
                IsClass = true,
                TypeAttributes = TypeAttributes.Public | TypeAttributes.Sealed
            };
            samples.Types.Add(_targetClass);
            _targetUnit.Namespaces.Add(samples);
        }

        public void AddFields()
        {
            // Declare the widthValue field.
            var widthValueField = new CodeMemberField
            {
                Attributes = MemberAttributes.Private,
                Name = "widthValue",
                Type = new CodeTypeReference(typeof(Double))
            };
            widthValueField.Comments.Add(new CodeCommentStatement(
                "The width of the object."));
            _targetClass.Members.Add(widthValueField);

            // Declare the heightValue field
            var heightValueField = new CodeMemberField
            {
                Attributes = MemberAttributes.Private,
                Name = "heightValue",
                Type = new CodeTypeReference(typeof(Double))
            };
            heightValueField.Comments.Add(new CodeCommentStatement(
                "The height of the object."));
            _targetClass.Members.Add(heightValueField);
        }

        public void AddProperties()
        {
            // Declare the read-only Width property.
            var widthProperty = new CodeMemberProperty
            {
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                Name = "Width",
                HasGet = true,
                Type = new CodeTypeReference(typeof(Double))
            };
            widthProperty.Comments.Add(new CodeCommentStatement(
                "The Width property for the object."));
            widthProperty.GetStatements.Add(new CodeMethodReturnStatement(
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "widthValue")));
            _targetClass.Members.Add(widthProperty);

            // Declare the read-only Height property.
            var heightProperty = new CodeMemberProperty
            {
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                Name = "Height",
                HasGet = true,
                Type = new CodeTypeReference(typeof(Double))
            };
            heightProperty.Comments.Add(new CodeCommentStatement(
                "The Height property for the object."));
            heightProperty.GetStatements.Add(new CodeMethodReturnStatement(
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "heightValue")));
            _targetClass.Members.Add(heightProperty);

            // Declare the read only Area property.
            var areaProperty = new CodeMemberProperty
            {
                Attributes = MemberAttributes.Public | MemberAttributes.Final,
                Name = "Area",
                HasGet = true,
                Type = new CodeTypeReference(typeof(Double))
            };
            areaProperty.Comments.Add(new CodeCommentStatement(
                "The Area property for the object."));

            // Create an expression to calculate the area for the get accessor  
            // of the Area property.
            var areaExpression =
                new CodeBinaryOperatorExpression(
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "widthValue"),
                CodeBinaryOperatorType.Multiply,
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "heightValue"));
            areaProperty.GetStatements.Add(
                new CodeMethodReturnStatement(areaExpression));
            _targetClass.Members.Add(areaProperty);
        }

        public void AddEntryPoint()
        {
            var start = new CodeEntryPointMethod();
            var objectCreate =
                new CodeObjectCreateExpression(
                new CodeTypeReference("CodeDOMCreatedClass"),
                new CodePrimitiveExpression(5.3),
                new CodePrimitiveExpression(6.9));

            // Add the statement: 
            // "CodeDOMCreatedClass testClass =  
            //     new CodeDOMCreatedClass(5.3, 6.9);"
            start.Statements.Add(new CodeVariableDeclarationStatement(
                new CodeTypeReference("CodeDOMCreatedClass"), "testClass",
                objectCreate));

            // Creat the expression: 
            // "testClass.ToString()"
            var toStringInvoke =
                new CodeMethodInvokeExpression(
                new CodeVariableReferenceExpression("testClass"), "ToString");

            // Add a System.Console.WriteLine statement with the previous  
            // expression as a parameter.
            start.Statements.Add(new CodeMethodInvokeExpression(
                new CodeTypeReferenceExpression("System.Console"),
                "WriteLine", toStringInvoke));
            _targetClass.Members.Add(start);
        }

        public void GenerateCSharpCode(string fileName)
        {
            CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");
            var options = new CodeGeneratorOptions { BracingStyle = "C" };
            using (var sourceWriter = new StreamWriter(fileName))
            {
                provider.GenerateCodeFromCompileUnit(
                    _targetUnit, sourceWriter, options);
            }
        }

        public void AddConstructor()
        {
            // Declare the constructor
            var constructor = new CodeConstructor { Attributes = MemberAttributes.Public | MemberAttributes.Final };

            // Add parameters.
            constructor.Parameters.Add(new CodeParameterDeclarationExpression(
                typeof(System.Double), "width"));
            constructor.Parameters.Add(new CodeParameterDeclarationExpression(
                typeof(System.Double), "height"));

            // Add field initialization logic
            var widthReference =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "widthValue");
            constructor.Statements.Add(new CodeAssignStatement(widthReference,
                new CodeArgumentReferenceExpression("width")));
            var heightReference =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "heightValue");
            constructor.Statements.Add(new CodeAssignStatement(heightReference,
                new CodeArgumentReferenceExpression("height")));
            _targetClass.Members.Add(constructor);
        }

        public void AddMethod()
        {
            // Declaring a ToString method
            var toStringMethod = new CodeMemberMethod
            {
                Attributes = MemberAttributes.Public | MemberAttributes.Override,
                Name = "ToString",
                ReturnType = new CodeTypeReference(typeof(System.String))
            };

            var widthReference =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "Width");
            var heightReference =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "Height");
            var areaReference =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "Area");

            // Declaring a return statement for method ToString.
            var returnStatement =
                new CodeMethodReturnStatement();

            // This statement returns a string representation of the width, 
            // height, and area. 
            string formattedOutput = "The object:" + Environment.NewLine +
                " width = {0}," + Environment.NewLine +
                " height = {1}," + Environment.NewLine +
                " area = {2}";
            returnStatement.Expression =
                new CodeMethodInvokeExpression(
                new CodeTypeReferenceExpression("System.String"), "Format",
                new CodePrimitiveExpression(formattedOutput),
                widthReference, heightReference, areaReference);
            toStringMethod.Statements.Add(returnStatement);
            _targetClass.Members.Add(toStringMethod);
        }

        public JsonResult GetkendoProdSpecs(int catalogId, string familyIdString)
        {
            try
            {
                int familyId;
                int.TryParse(familyIdString.Trim('~'), out familyId);
                var objDataTable = new DataTable();
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_ProductPivotTable";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 100).Value = familyId;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(objDataTable);

                    var objresult = GetTableRows(objDataTable);
                    //{
                    //    var dataList = new List<System.Dynamic.ExpandoObject>();
                    //    while (reader.Read())
                    //    {
                    //        for (int i = 0; i < reader.FieldCount; i++)
                    //        {

                    //            //jsonWriter.WritePropertyName(reader.GetName(i));
                    //            //jsonWriter.WriteValue(reader.GetValue(i).ToString());
                    //            //if (!dynamicColumns.ContainsKey(reader.GetName(i)) && !string.IsNullOrEmpty(reader.GetName(i)))
                    //            //{
                    //            //    dynamicColumns.Add(reader.GetName(i), reader.GetValue(i).ToString());
                    //            //}
                    //        }
                    //    }

                    //}
                    //var objListColumnDataType = new List<ColumnDataType>();
                    //foreach (DataColumn objDataColumn in objDataTable.Columns)
                    //{
                    //    var objColumnDataType = new ColumnDataType();
                    //    objColumnDataType.Title = objDataColumn.ColumnName;
                    //    objColumnDataType.Type = objDataColumn.DataType;
                    //    objListColumnDataType.Add(objColumnDataType);
                    //}
                    //var objListTotoalProducts = new List<TotalProducts>();
                    //var objTotoalProducts=new TotalProducts();
                    //objTotoalProducts.GetProductsRows = objresult;
                    //objTotoalProducts.Columntype = objListColumnDataType;
                    //objListTotoalProducts.Add(objTotoalProducts);
                    var objfinal = Json(objresult, JsonRequestBehavior.AllowGet);
                    ViewBag.Column = "[{ field: \"FAMILY_ID\", title: \"Value\" },{ field: \"PRODUCT_ID\", title: \"Track\" },{ field: \"CATALOG_ID\", title: \"Track\" }";
                    return objfinal;
                }
            }
            catch (Exception objexception)
            {
                _logger.Error("Error at HomeApiController : GetProdSpecs", objexception);
                return null;
            }
        }

        public List<Dictionary<string, object>> GetTableRows(DataTable dtData)
        {
            return (from DataRow dr in dtData.Rows select dtData.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => dr[col])).ToList();
        }

        static long DirectorySize(DirectoryInfo dInfo, bool includeSubDir)
        {
            long totalSize = dInfo.EnumerateFiles()
                         .Sum(file => file.Length);
            if (includeSubDir)
            {
                totalSize += dInfo.EnumerateDirectories()
                         .Sum(dir => DirectorySize(dir, true));
            }
            return totalSize;
        }
        //Family Image & Attachments
        public ActionResult SaveFamilyImage(IEnumerable<HttpPostedFileBase> files)
        {
            if (files != null)
            {
                var SpaceProvided = objLS.TB_PLAN
                          .Join(objLS.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                          .Join(objLS.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                          .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => x.tcptps.tps.StorageGB).ToList();
                double alotment = (double)SpaceProvided[0];
                var userId = objLS.Customer_User.Join(objLS.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();
                double folderSize = 0;
                if (string.IsNullOrEmpty(userId))
                {
                    userId = "Questudio";
                }

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + ""));
                }
                var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "");
                double availableSpace = alotment - folderSize;
                if (availableSpace > 0)
                {
                    CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
                    if (string.IsNullOrEmpty(CustomerFolder))
                    { CustomerFolder = "/STUDIOSOFT"; }
                    else
                    { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }

                    foreach (var file in files)
                    {
                        var fileName = Path.GetFileName(file.FileName);
                        if (fileName != null && fileName.IndexOfAny(SpecialChars) == -1)
                        {
                            if (Directory.Exists(Server.MapPath("~/Content/ProductImages" + CustomerFolder + "/Images")))
                            {
                                var physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages" + CustomerFolder + "/Images"), fileName);
                                file.SaveAs(physicalPath);
                            }
                            else
                            {
                                Directory.CreateDirectory(Server.MapPath("~/Content/ProductImages" + CustomerFolder + "/Images"));
                                var physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages" + CustomerFolder + "/Images"), fileName);
                                file.SaveAs(physicalPath);
                            }

                        }
                    }
                }
            }
            return Content("");
        }

        public ActionResult RemoveFamilyImage(string[] fileNames)
        {
            if (fileNames != null)
            {
                CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name).Select(x => x.a.Comments).FirstOrDefault();
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }


                foreach (var fullName in fileNames)
                {
                    var fileName = Path.GetFileName(fullName);
                    if (fileName != null)
                    {
                        var physicalPath = Path.Combine(Server.MapPath("~/Content/ProductImages" + CustomerFolder + "/Images"), fileName);
                        if (System.IO.File.Exists(physicalPath))
                        {
                            System.IO.File.Delete(physicalPath);
                        }
                    }
                }
            }
            return Content("");
        }
        public string GetCsFamilyPreviewList(int familyId, string categoryId, int catalogId, bool familyLevelMultipletablePreview, bool EnableSubProduct)
        {
            try
            {
                int userID = 0;
                var userdetails = _dbcontext.Customer_User.FirstOrDefault(a => a.User_Name.ToLower() == User.Identity.Name.ToLower());
                if (userdetails != null)
                { userID = userdetails.CustomerId; }
                var objFamilyPreviews = new FamilyPreviews();
                return objFamilyPreviews.FamilyPreview(catalogId, categoryId, familyId, 0, 0, familyLevelMultipletablePreview, userID, EnableSubProduct);
            }
            catch (Exception exp)
            {
                _logger.Error("Error at GetCSFamilyPreviewList", exp);
                return "Preview is not available";
                // Console.WriteLine(exp.Message);
            }
        }

        public string GetCsFamilyPreviewListCheck(int familyId, string categoryId, int catalogId, bool familyLevelMultipletablePreview)
        {
            try
            {
                int userID = 0;
                var userdetails = _dbcontext.Customer_User.FirstOrDefault(a => a.User_Name.ToLower() == User.Identity.Name.ToLower());
                if (userdetails != null)
                { userID = userdetails.CustomerId; }
                var objFamilyPreviews = new FamilyPreviews();
                string prodhtml = "";
                prodhtml = objFamilyPreviews.GenerateFamilyPreview(catalogId, familyId, categoryId, userID);
                if (prodhtml.Contains("<html><h6>Table Preview Not Available!</h6><html>"))
                {
                    return "Preview is not available";
                }
                else
                {
                    return "Preview available";
                }

                //        string tableLayout = objFamilyPreviews.FetchTableLayout(catalogId, familyId);
                //        var objSuperTable = new SuperTable();
                //        _mCategoryId = categoryId;
                //    _mFamilyId = Convert.ToString(familyId);
                //    _mCatalogId = Convert.ToString(catalogId);
                ////    TableLayoutName = _mTableLayoutName;

                //    string xmlStr = string.Empty;
                //    //Retrieve the Pivot Layout from Database
                //    objSuperTable.DecodeLayoutXml(objSuperTable.RetrieveTableLayout());
                //    if (objSuperTable._mRowFields.Count <= 0)
                //    {
                //        return "No Preview Available";
                //    }
                //        // string tableLayout = objFamilyPreviews.FetchTableLayout_MultipleTable(catalogId, familyId, categoryId);
                //      return "Preview Available";

                // return objFamilyPreviews.FamilyPreview(catalogId, categoryId, familyId, 0, 0, familyLevelMultipletablePreview, userID);
            }
            catch (Exception exp)
            {
                _logger.Error("Error at GetCSFamilyPreviewList", exp);
                return "Preview is not available";
                // Console.WriteLine(exp.Message);
            }
        }



        public string GetCsFamilyPreviewListPTM(int familyId, string categoryId, int catalogId, bool familyLevelMultipletablePreview)
        {
            try
            {
                int userID = 0;

                var userdetails = _dbcontext.Customer_User.FirstOrDefault(a => a.User_Name.ToLower() == User.Identity.Name.ToLower());
                if (userdetails != null)
                { userID = userdetails.CustomerId; }

                var categorydetails = _dbcontext.TB_FAMILY.FirstOrDefault(a => a.FAMILY_ID == familyId);
                if (categorydetails != null)
                { categoryId = categorydetails.CATEGORY_ID; }

                var objFamilyPreviews = new FamilyPreviews();
                return objFamilyPreviews.FamilyPreviewPTM(catalogId, categoryId, familyId, 0, 0, familyLevelMultipletablePreview, userID);
            }
            catch (Exception exp)
            {
                _logger.Error("Error at GetCSFamilyPreviewList", exp);
                return "Preview is not available";
                // Console.WriteLine(exp.Message);
            }
        }

        public string CloneFamily(string copiedId, string rightClickedCategoryId, int catalogID, int previousCatalogID, string multipleFamilyids, string multipleFamilyidsForAnotherCatalog)
        {
            try
            {
                string result = string.Empty;

                if (multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog.Trim() != "")
                {
                    if (catalogID == previousCatalogID)
                    {
                        var multiplsids = multipleFamilyids.Split(',');
                        foreach (var item in multiplsids)
                        {
                            int family_id;
                            string itemValue = item.Trim('~');
                            bool isFamilyId = int.TryParse(itemValue, out family_id);

                            if (isFamilyId)
                            {
                                //int family_id = Convert.ToInt32(item.Trim('~'));

                                TB_FAMILY family = _dbcontext.TB_FAMILY.Where(a => a.FAMILY_ID == family_id && a.PARENT_FAMILY_ID == 0).FirstOrDefault();

                                if (family != null && family.FAMILY_ID > 0)
                                {

                                    //    var subfamily_check = _dbcontext.TB_FAMILY.Where(a => a.FAMILY_ID == family_id && a.PARENT_FAMILY_ID == 0);
                                    //if (!subfamily_check.Any())
                                    //{ return ""; }

                                    string category_id = rightClickedCategoryId;
                                    var family_check = _dbcontext.TB_CATALOG_FAMILY.Where(a => a.CATALOG_ID == catalogID && a.FAMILY_ID == family_id && a.CATEGORY_ID == category_id);
                                    var oldCat = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(a => a.CATALOG_ID == previousCatalogID && a.FAMILY_ID == family_id && a.ROOT_CATEGORY == "0");
                                    string OldCategoy = string.Empty;
                                    if (oldCat != null)
                                    {
                                        OldCategoy = oldCat.CATEGORY_ID;
                                    }
                                    if (!family_check.Any())
                                    {

                                        var id = _dbcontext.STP_CATALOGSTUDIO5_CLONE(family_id, catalogID, previousCatalogID, category_id, OldCategoy, User.Identity.Name);
                                        _dbcontext.SaveChanges();

                                    }
                                    else
                                    {
                                        result = "Product already exists";
                                        return result;
                                    }

                                }

                            }

                        }
                        return "Product Cloned successfully";
                    }
                    else
                    {
                        if (multipleFamilyids.Trim() == "")
                        {
                            multipleFamilyids = multipleFamilyidsForAnotherCatalog;
                        }
                        var multiplsids = multipleFamilyids.Split(',');
                        foreach (var item in multiplsids)
                        {
                            int family_id;
                            string itemValue = item.Trim('~');
                            bool isFamilyId = int.TryParse(itemValue, out family_id);

                            if (isFamilyId)
                            {
                                TB_FAMILY family = _dbcontext.TB_FAMILY.Where(a => a.FAMILY_ID == family_id && a.PARENT_FAMILY_ID == 0).FirstOrDefault();

                                if (family != null && family.FAMILY_ID > 0)
                                {
                                    string category_id = rightClickedCategoryId;
                                    var family_check = _dbcontext.TB_CATALOG_FAMILY.Where(a => a.CATALOG_ID == previousCatalogID && a.FAMILY_ID == family_id && a.CATEGORY_ID == category_id);
                                    var oldCat = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(a => a.CATALOG_ID == previousCatalogID && a.FAMILY_ID == family_id && a.ROOT_CATEGORY == "0");
                                    string OldCategoy = string.Empty;
                                    if (oldCat != null)
                                    {
                                        OldCategoy = oldCat.CATEGORY_ID;
                                    }
                                    if (!family_check.Any())
                                    {

                                        var id = _dbcontext.STP_CATALOGSTUDIO5_CLONE(family_id, catalogID, previousCatalogID, category_id, OldCategoy, User.Identity.Name);
                                        _dbcontext.SaveChanges();

                                    }
                                    else
                                    {
                                        result = "Product already exists";
                                        return result;
                                    }

                                }


                            }


                        }
                        return "Product Cloned successfully";
                    }
                }
                else
                {
                    if (catalogID == previousCatalogID)
                    {
                        int family_id = Convert.ToInt32(copiedId.Trim('~'));
                        var subfamily_check = _dbcontext.TB_FAMILY.Where(a => a.FAMILY_ID == family_id && a.PARENT_FAMILY_ID == 0);
                        if (!subfamily_check.Any())
                        { return ""; }
                        string category_id = rightClickedCategoryId;
                        var family_check = _dbcontext.TB_CATALOG_FAMILY.Where(a => a.CATALOG_ID == catalogID && a.FAMILY_ID == family_id && a.CATEGORY_ID == category_id && a.FLAG_RECYCLE == "A");
                        var oldCat = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(a => a.CATALOG_ID == previousCatalogID && a.FAMILY_ID == family_id && a.ROOT_CATEGORY == "0");
                        string OldCategoy = string.Empty;
                        if (oldCat != null)
                        {
                            OldCategoy = oldCat.CATEGORY_ID;
                        }
                        if (!family_check.Any())
                        {

                            var id = _dbcontext.STP_CATALOGSTUDIO5_CLONE(family_id, catalogID, previousCatalogID, category_id, OldCategoy, User.Identity.Name);
                            _dbcontext.SaveChanges();
                            result = "Product Cloned successfully";
                        }
                        else
                        {
                            result = "Product already exists";
                        }
                    }
                    else
                    {

                        int family_id = Convert.ToInt32(copiedId.Trim('~'));
                        string category_id = rightClickedCategoryId;
                        var family_check = _dbcontext.TB_CATALOG_FAMILY.Where(a => a.CATALOG_ID == previousCatalogID && a.FAMILY_ID == family_id && a.CATEGORY_ID == category_id);
                        var oldCat = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(a => a.CATALOG_ID == previousCatalogID && a.FAMILY_ID == family_id && a.ROOT_CATEGORY == "0");
                        string OldCategoy = string.Empty;
                        if (oldCat != null)
                        {
                            OldCategoy = oldCat.CATEGORY_ID;
                        }
                        if (!family_check.Any())
                        {

                            var id = _dbcontext.STP_CATALOGSTUDIO5_CLONE(family_id, catalogID, previousCatalogID, category_id, OldCategoy, User.Identity.Name);
                            _dbcontext.SaveChanges();
                            result = "Product Cloned successfully";
                        }
                        else
                        {
                            result = "Product already exists";
                        }
                    }
                }
                return result;

            }
            catch (Exception ex)
            {
                _logger.Error("Error at Family/CloneFamily", ex);
                return "Action not successful";
            }

        }

        public string CloneRemoveFamily(string copiedId, string rightClickedCategoryId, int catalogID, string MultipleFamilyids)
        {
            try
            {


                if (MultipleFamilyids != null && MultipleFamilyids.Trim() != "")
                {
                    var family_ids = MultipleFamilyids.Split(',');
                    foreach (var item in family_ids)
                    {
                        var objSqlConnection =
             new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                        string deleteNode = item.Trim('~');
                        int famid = Convert.ToInt32(deleteNode);
                        var catid = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(a => a.FAMILY_ID == famid && a.CATALOG_ID == catalogID && a.ROOT_CATEGORY != "0");
                        string categoryId = "";
                        string categoryShort = "";
                        if (catid != null)
                        {
                            categoryId = catid.CATEGORY_ID;
                            categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == categoryId).Select(y => y.CATEGORY_SHORT).FirstOrDefault();  
                        }
                        else
                        {
                            return "";
                        }

                        string xml = "<?xml version=\"1.0\" standalone=\"yes\"?><Deleted_Items>";
                        Random random = new Random();
                        int xml_id = random.Next(1, 1000);
                        string category_cat = string.Empty;
                        string product_cat = string.Empty;
                        string familyids = string.Empty;
                        string catalog_xml = "<?xml version=\"1.0\" standalone=\"yes\"?><CATALOG_ASSOCIATES>";
                        string prdfamassoc = "<?xml version=\"1.0\" standalone=\"yes\"?><PROD_FAMILY>";
                        string projectsec = "<?xml version=\"1.0\" standalone=\"yes\"?><PROJECTS>";
                        string partskey = "<?xml version=\"1.0\" standalone=\"yes\"?><PARTS_KEY>";
                        string prodpubattr = "<?xml version=\"1.0\" standalone=\"yes\"?><PRODUCT_PUBLISHED_ATTR>";
                        string fampubattr = "<?xml version=\"1.0\" standalone=\"yes\"?><FAMILY_PUBLISHED_ATTR>";

                        string productID = "";

                        string family_cat = string.Empty;
                        string _sqlFamilyLevel = " (SELECT FAMILY_ID,PARENT_FAMILY_ID FROM TB_FAMILY WHERE FAMILY_ID = " + deleteNode + " ) UNION " +
                                                 " (SELECT SUBFAMILY_ID,FAMILY_ID FROM TB_SUBFAMILY  WHERE FAMILY_ID = " + deleteNode + " AND FAMILY_ID = (SELECT FAMILY_ID FROM TB_FAMILY WHERE FAMILY_ID = " + deleteNode + " ))";

                        SqlString = _sqlFamilyLevel;
                        DataSet _dsRmvPmnt = CreateDataSet();
                        xml_id = random.Next(1, 1000);
                        objSqlConnection.Open();
                        foreach (DataRow _drFamily in _dsRmvPmnt.Tables[0].Rows)
                        {
                            family_cat = family_cat + "," + _drFamily["FAMILY_ID"].ToString();
                            string productsql = "SELECT  CONVERT (NVARCHAR, PRODUCT_ID)  + ',' FROM TB_PROD_FAMILY WHERE FAMILY_ID='" + _drFamily[0].ToString() + "' for xml path('')";
                            SqlCommand Command = new SqlCommand(productsql, objSqlConnection);
                            productID = Convert.ToString(Command.ExecuteScalar());

                            if (productID.Length > 0)
                            {
                                product_cat = product_cat + "," + productID.Substring(0, productID.Length - 1);
                                productID = productID.Substring(0, productID.Length - 1);
                            }
                            else
                            {
                                product_cat = product_cat + ",0"; ;
                                productID = "0";
                            }
                            DataSet recycleDS = new DataSet();
                            Command = new SqlCommand("[STP_CATALOGSTUDIO5_RECYCLE_TABLE_INSERT]", objSqlConnection);
                            Command.CommandType = CommandType.StoredProcedure;
                            Command.Parameters.Add(new SqlParameter("@CATALOG_ID", catalogID));
                            Command.Parameters.Add(new SqlParameter("@CATEGORYID", categoryId));
                            Command.Parameters.Add(new SqlParameter("@FAMILY_ID", _drFamily["FAMILY_ID"].ToString()));
                            Command.Parameters.Add(new SqlParameter("@ALLOW_DUPLICATES", "N"));
                            Command.Parameters.Add(new SqlParameter("@PRODUCTIDS", productID.ToString()));
                            SqlDataAdapter DataAdapter = new SqlDataAdapter(Command);
                            DataAdapter.SelectCommand.CommandTimeout = 0;
                            DataAdapter.Fill(recycleDS);
                            xml = recycleDS.Tables[0].Rows[0][0].ToString();
                            xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" + recycleDS.Tables[0].Rows[0][0].ToString();
                            xml = xml.Replace("'", "''");
                            if (_drFamily["PARENT_FAMILY_ID"].ToString() != "0")
                            {
                                string sqlstr1 = " DECLARE @FAMILY_NAME NVARCHAR(MAX)"
                                                + " DECLARE @SUBFAMILY_NAME NVARCHAR(MAX)"
                                                + " SET @FAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["PARENT_FAMILY_ID"].ToString() + "')"
                                                + " SET @SUBFAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["FAMILY_ID"].ToString() + "')"
                                                + "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,FAMILY_ID,FAMILY_NAME,SUB_FAMILY_ID,SUB_FAMILY_NAME,XML_DOC,DELETED_USER) VALUES"
                                                + "('" + xml_id + "'," + catalogID + ","
                                                + " '" + _drFamily["PARENT_FAMILY_ID"].ToString() + "',@FAMILY_NAME,'" + _drFamily["FAMILY_ID"].ToString() + "',@SUBFAMILY_NAME,'" + xml + "','" + User.Identity.Name.ToString() + "' ) ";
                                Command = new SqlCommand(sqlstr1, objSqlConnection);
                                Command.ExecuteNonQuery();
                            }
                            else if (_drFamily["PARENT_FAMILY_ID"].ToString() == "0")
                            {
                                string sqlstr1 = " DECLARE @FAMILY_NAME NVARCHAR(MAX)"
                                                + " SET @FAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["FAMILY_ID"].ToString() + "')"
                                                + "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,FAMILY_ID,FAMILY_NAME,XML_DOC,DELETED_USER) VALUES"
                                               + "('" + xml_id + "'," + catalogID + ","
                                               + " '" + _drFamily["FAMILY_ID"].ToString() + "',@FAMILY_NAME,'" + xml + "','" + User.Identity.Name.ToString() + "' ) ";
                                Command = new SqlCommand(sqlstr1, objSqlConnection);
                                Command.ExecuteNonQuery();

                            }

                            productID = string.Empty;
                            xml = string.Empty;
                            recycleDS.Dispose();
                        }
                        DataSet ds = new DataSet();
                        if (family_cat != string.Empty)
                        {
                            if (family_cat[0] == ',')
                            {
                                family_cat = family_cat.Substring(1);
                            }
                            SqlCommand cmd = new SqlCommand("SELECT CATALOG_ID,SORT_ORDER,FAMILY_ID,CATEGORY_ID,CREATED_USER,CREATED_DATE,ROOT_CATEGORY FROM TB_CATALOG_FAMILY WHERE CATALOG_ID!=1 AND FAMILY_ID in (" + family_cat + ")    and category_id='" + categoryId + "' union  SELECT CATALOG_ID,SORT_ORDER,FAMILY_ID,CATEGORY_ID,CREATED_USER,CREATED_DATE,ROOT_CATEGORY FROM TB_CATALOG_FAMILY WHERE CATALOG_ID!=1 AND FAMILY_ID in (" + family_cat + ") and root_category='" + 0 + " '    for xml path('CATALOG_FAMILY'),type", objSqlConnection);
                            SqlDataAdapter dafam = new SqlDataAdapter(cmd);
                            dafam.Fill(ds, "CATALOG_FAMILY");
                            if (ds.Tables["CATALOG_FAMILY"].Rows.Count > 0 && ds.Tables["CATALOG_FAMILY"].Rows[0][0].ToString() != "" && ds.Tables["CATALOG_FAMILY"].Rows[0][0] != null)
                            {
                                catalog_xml = catalog_xml + ds.Tables["CATALOG_FAMILY"].Rows[0][0].ToString();
                            }

                            cmd = new SqlCommand("SELECT FAMILY_ID,SORT_ORDER,RECORD_ID,SECTION_NAME FROM TB_PROJECT_SECTION_DETAILS WHERE FAMILY_ID in (" + family_cat + ")  for xml path ('Project_Sections'),type", objSqlConnection);
                            SqlDataAdapter daprojsec = new SqlDataAdapter(cmd);
                            daprojsec.Fill(ds, "Project_Sections");
                            if (ds.Tables["Project_Sections"].Rows.Count > 0 && ds.Tables["Project_Sections"].Rows[0][0].ToString() != "" && ds.Tables["Project_Sections"].Rows[0][0] != null)
                            {
                                for (int i = 0; i < ds.Tables["Project_Sections"].Rows.Count; i++)
                                {
                                    projectsec = projectsec + ds.Tables["Project_Sections"].Rows[i][0].ToString();
                                }
                            }
                            cmd = new SqlCommand("SELECT CATALOG_ID,ATTRIBUTE_ID,FAMILY_ID,ATTRIBUTE_VALUE FROM TB_FAMILY_KEY WHERE FAMILY_ID in (" + family_cat + ") for xml path('FAMILY_KEY'),type", objSqlConnection);
                            SqlDataAdapter dafamkey = new SqlDataAdapter(cmd);
                            dafamkey.Fill(ds, "FAMILY_KEY");
                            if (ds.Tables["FAMILY_KEY"].Rows.Count > 0 && ds.Tables["FAMILY_KEY"].Rows[0][0].ToString() != "" && ds.Tables["FAMILY_KEY"].Rows[0][0] != null)
                            {
                                catalog_xml = catalog_xml + ds.Tables["FAMILY_KEY"].Rows[0][0].ToString();
                            }

                            cmd = new SqlCommand("SELECT ATTRIBUTE_ID,FAMILY_ID,CATEGORY_ID,SORT_ORDER,CATALOG_ID FROM TB_CATEGORY_FAMILY_ATTR_LIST WHERE FAMILY_ID in (" + family_cat + ") for xml path ('FAMILY_PUBLISH'),type", objSqlConnection);
                            daprojsec = new SqlDataAdapter(cmd);
                            daprojsec.Fill(ds, "fam_attr_pub");
                            if (ds.Tables["fam_attr_pub"].Rows.Count > 0 && ds.Tables["fam_attr_pub"].Rows[0][0].ToString() != "" && ds.Tables["fam_attr_pub"].Rows[0][0] != null)
                            {
                                for (int i = 0; i < ds.Tables["fam_attr_pub"].Rows.Count; i++)
                                {
                                    fampubattr = fampubattr + ds.Tables["fam_attr_pub"].Rows[i][0].ToString();
                                }
                            }
                            if (fampubattr != string.Empty && fampubattr != null)
                            {
                                if (fampubattr != "<?xml version=\"1.0\" standalone=\"yes\"?><FAMILY_PUBLISHED_ATTR>")
                                {
                                    fampubattr = fampubattr + "</FAMILY_PUBLISHED_ATTR>";
                                    fampubattr = fampubattr.Replace("'", "''");

                                    string projsec = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER, CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryId + "','" + fampubattr + "','" + User.Identity.Name.ToString() + "' , '" + categoryShort +  "' ) ";
                                    cmd = new SqlCommand(projsec, objSqlConnection);
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        if (product_cat != string.Empty)
                        {
                            if (product_cat[0] == ',')
                            {
                                product_cat = product_cat.Substring(1);
                            }
                            SqlCommand cmd = new SqlCommand("SELECT CATALOG_ID,cp.PRODUCT_ID,P.CATEGORY_ID,cp.CREATED_USER,cp.CREATED_DATE FROM TB_CATALOG_PRODUCT cp join tb_product p on p.product_id = cp.product_id WHERE CATALOG_ID!=1 AND cp.PRODUCT_ID IN(" + product_cat + ") for xml path('CATALOG_PRODUCT'),type", objSqlConnection);
                            SqlDataAdapter daprod = new SqlDataAdapter(cmd);
                            daprod.Fill(ds, "CATALOG_PRODUCT");
                            if (ds.Tables["CATALOG_PRODUCT"].Rows.Count > 0 && ds.Tables["CATALOG_PRODUCT"].Rows[0][0].ToString() != "" && ds.Tables["CATALOG_PRODUCT"].Rows[0][0] != null)
                            {
                                for (int i = 0; i < ds.Tables["CATALOG_PRODUCT"].Rows.Count; i++)
                                {
                                    catalog_xml = catalog_xml + ds.Tables["CATALOG_PRODUCT"].Rows[i][0].ToString();
                                }
                            }
                            cmd = new SqlCommand("SELECT ATTRIBUTE_ID,SORT_ORDER,FAMILY_ID,PRODUCT_ID FROM TB_PROD_FAMILY_ATTR_LIST WHERE PRODUCT_ID IN(" + product_cat + ") for xml path('PRODUCT_PUBLISH'),type", objSqlConnection);
                            daprod = new SqlDataAdapter(cmd);
                            daprod.Fill(ds, "PROD_PUB_ATTR");
                            if (ds.Tables["PROD_PUB_ATTR"].Rows.Count > 0 && ds.Tables["PROD_PUB_ATTR"].Rows[0][0].ToString() != "" && ds.Tables["CATALOG_PRODUCT"].Rows[0][0] != null)
                            {
                                for (int i = 0; i < ds.Tables["PROD_PUB_ATTR"].Rows.Count; i++)
                                {
                                    prodpubattr = prodpubattr + ds.Tables["PROD_PUB_ATTR"].Rows[i][0].ToString();
                                }
                            }
                            if (prodpubattr != "<?xml version=\"1.0\" standalone=\"yes\"?><PRODUCT_PUBLISHED_ATTR>")
                            {
                                prodpubattr = prodpubattr + "</PRODUCT_PUBLISHED_ATTR>";
                                prodpubattr = prodpubattr.Replace("'", "''");

                                string sqlstr = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryId + "','" + prodpubattr + "','" + User.Identity.Name.ToString() + "' , '" + categoryShort + "'  ) ";
                                cmd = new SqlCommand(sqlstr, objSqlConnection);
                                cmd.ExecuteNonQuery();
                            }
                            cmd = new SqlCommand("SELECT * FROM TB_PARTS_KEY WHERE PRODUCT_ID IN (" + product_cat + ") for xml path('PARTSKEY')", objSqlConnection);
                            SqlDataAdapter daparts = new SqlDataAdapter(cmd);
                            daparts.Fill(ds, "PARTS_KEY");
                            if (ds.Tables["PARTS_KEY"].Rows.Count > 0 && ds.Tables["PARTS_KEY"].Rows[0][0].ToString() != "" && ds.Tables["PARTS_KEY"].Rows[0][0] != null)
                            {
                                for (int i = 0; i < ds.Tables["PARTS_KEY"].Rows.Count; i++)
                                {
                                    partskey = partskey + ds.Tables["PARTS_KEY"].Rows[i][0].ToString();
                                }
                            }
                            if (partskey != string.Empty && partskey != null)
                            {
                                if (partskey != "<?xml version=\"1.0\" standalone=\"yes\"?><PARTS_KEY>")
                                {
                                    partskey = partskey + "</PARTS_KEY>";
                                    partskey = partskey.Replace("'", "''");

                                    string sqlstr11 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryId + "','" + partskey + "' ,'" + User.Identity.Name.ToString() + "' , '" + categoryShort + "' ) ";
                                    cmd = new SqlCommand(sqlstr11, objSqlConnection);
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        if (catalog_xml != "<?xml version=\"1.0\" standalone=\"yes\"?><CATALOG_ASSOCIATES>")
                        {
                            catalog_xml = catalog_xml + "</CATALOG_ASSOCIATES>";
                            catalog_xml = catalog_xml.Replace("'", "''");
                            string sqlstr = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,FAMILY_ID,XML_DOC,DELETED_USER) VALUES('" + xml_id + "','" + deleteNode + "','" + catalog_xml + "','" + User.Identity.Name.ToString() + "' ) ";
                            SqlCommand cmd = new SqlCommand(sqlstr, objSqlConnection);
                            cmd.ExecuteNonQuery();
                        }
                        if (projectsec != string.Empty && projectsec != null)
                        {
                            if (projectsec != "<?xml version=\"1.0\" standalone=\"yes\"?><PROJECTS>")
                            {
                                projectsec = projectsec + "</PROJECTS>";
                                projectsec = projectsec.Replace("'", "''");
                                string projsec = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER,CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryId + "','" + projectsec + "','" + User.Identity.Name.ToString() + "' , '" + categoryShort + "' ) ";
                                SqlCommand cmd = new SqlCommand(projsec, objSqlConnection);
                                cmd.ExecuteNonQuery();
                            }
                        }


                        string _categoryLevel = " exec STP_CATALOGSTUDIO5_CategoryLevel '" + categoryId + "'";
                        SqlString = _categoryLevel;
                        DataSet _dsCatlevel = CreateDataSet();
                        foreach (DataRow drcat in _dsCatlevel.Tables[0].Rows)
                        {
                            string sqlstr2 = "INSERT INTO TB_RECYCLE_CATEGORY_REFERENCE (XML_ID,CAT_ID,LEVEL,CREATED_USER) VALUES"
                                                      + "('" + xml_id + "','" + drcat["CATEGORY_ID"].ToString() + "','" + drcat["LEVEL"].ToString() + "','" + User.Identity.Name + "')";
                            SqlCommand Command = new SqlCommand(sqlstr2, objSqlConnection);
                            Command.ExecuteNonQuery();
                        }

                        var cmdClone = new SqlCommand("DELETE FROM TB_CATALOG_FAMILY WHERE FAMILY_ID IN ( "
                                                    + "SELECT FAMILY_ID FROM TB_CATALOG_FAMILY WHERE FAMILY_ID IN (" + deleteNode + ") AND CATEGORY_ID='" + categoryId + "' "
                                                    + " UNION "
                                                    + " SELECT FAMILY_ID FROM TB_CATALOG_FAMILY WHERE FAMILY_ID IN (SELECT FAMILY_ID FROM TB_FAMILY WHERE PARENT_FAMILY_ID = " + deleteNode + ") AND CATEGORY_ID='" + categoryId + "' "
                                                    + " ) AND CATEGORY_ID='" + categoryId + "'", objSqlConnection);
                        cmdClone.ExecuteNonQuery();
                    }
                }
                else
                {
                    var objSqlConnection =
             new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                    string deleteNode = rightClickedCategoryId.Trim('~');
                    int famid = Convert.ToInt32(deleteNode);
                    var catid = _dbcontext.TB_CATALOG_FAMILY.FirstOrDefault(a => a.FAMILY_ID == famid && a.CATALOG_ID == catalogID && a.ROOT_CATEGORY != "0");
                    string categoryId = "";
                    string categoryShort = "";
                    if (catid != null)
                    {
                        categoryId = catid.CATEGORY_ID;
                        categoryShort = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == categoryId).Select(y => y.CATEGORY_SHORT).FirstOrDefault();  
                    }
                    else
                    {
                        return "";
                    }

                    string xml = "<?xml version=\"1.0\" standalone=\"yes\"?><Deleted_Items>";
                    Random random = new Random();
                    int xml_id = random.Next(1, 1000);
                    string category_cat = string.Empty;
                    string product_cat = string.Empty;
                    string familyids = string.Empty;
                    string catalog_xml = "<?xml version=\"1.0\" standalone=\"yes\"?><CATALOG_ASSOCIATES>";
                    string prdfamassoc = "<?xml version=\"1.0\" standalone=\"yes\"?><PROD_FAMILY>";
                    string projectsec = "<?xml version=\"1.0\" standalone=\"yes\"?><PROJECTS>";
                    string partskey = "<?xml version=\"1.0\" standalone=\"yes\"?><PARTS_KEY>";
                    string prodpubattr = "<?xml version=\"1.0\" standalone=\"yes\"?><PRODUCT_PUBLISHED_ATTR>";
                    string fampubattr = "<?xml version=\"1.0\" standalone=\"yes\"?><FAMILY_PUBLISHED_ATTR>";

                    string productID = "";

                    string family_cat = string.Empty;
                    string _sqlFamilyLevel = " (SELECT FAMILY_ID,PARENT_FAMILY_ID FROM TB_FAMILY WHERE FAMILY_ID = " + deleteNode + " ) UNION " +
                                             " (SELECT SUBFAMILY_ID,FAMILY_ID FROM TB_SUBFAMILY  WHERE FAMILY_ID = " + deleteNode + " AND FAMILY_ID = (SELECT FAMILY_ID FROM TB_FAMILY WHERE FAMILY_ID = " + deleteNode + " ))";

                    SqlString = _sqlFamilyLevel;
                    DataSet _dsRmvPmnt = CreateDataSet();
                    xml_id = random.Next(1, 1000);
                    objSqlConnection.Open();
                    foreach (DataRow _drFamily in _dsRmvPmnt.Tables[0].Rows)
                    {
                        family_cat = family_cat + "," + _drFamily["FAMILY_ID"].ToString();
                        string productsql = "SELECT  CONVERT (NVARCHAR, PRODUCT_ID)  + ',' FROM TB_PROD_FAMILY WHERE FAMILY_ID='" + _drFamily[0].ToString() + "' for xml path('')";
                        SqlCommand Command = new SqlCommand(productsql, objSqlConnection);
                        productID = Convert.ToString(Command.ExecuteScalar());

                        if (productID.Length > 0)
                        {
                            product_cat = product_cat + "," + productID.Substring(0, productID.Length - 1);
                            productID = productID.Substring(0, productID.Length - 1);
                        }
                        else
                        {
                            product_cat = product_cat + ",0"; ;
                            productID = "0";
                        }
                        DataSet recycleDS = new DataSet();
                        Command = new SqlCommand("[STP_CATALOGSTUDIO5_RECYCLE_TABLE_INSERT]", objSqlConnection);
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add(new SqlParameter("@CATALOG_ID", catalogID));
                        Command.Parameters.Add(new SqlParameter("@CATEGORYID", categoryId));
                        Command.Parameters.Add(new SqlParameter("@FAMILY_ID", _drFamily["FAMILY_ID"].ToString()));
                        Command.Parameters.Add(new SqlParameter("@ALLOW_DUPLICATES", "N"));
                        Command.Parameters.Add(new SqlParameter("@PRODUCTIDS", productID.ToString()));
                        SqlDataAdapter DataAdapter = new SqlDataAdapter(Command);
                        DataAdapter.SelectCommand.CommandTimeout = 0;
                        DataAdapter.Fill(recycleDS);
                        xml = recycleDS.Tables[0].Rows[0][0].ToString();
                        xml = "<?xml version=\"1.0\" standalone=\"yes\"?>" + recycleDS.Tables[0].Rows[0][0].ToString();
                        xml = xml.Replace("'", "''");
                        if (_drFamily["PARENT_FAMILY_ID"].ToString() != "0")
                        {
                            string sqlstr1 = " DECLARE @FAMILY_NAME NVARCHAR(MAX)"
                                            + " DECLARE @SUBFAMILY_NAME NVARCHAR(MAX)"
                                            + " SET @FAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["PARENT_FAMILY_ID"].ToString() + "')"
                                            + " SET @SUBFAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["FAMILY_ID"].ToString() + "')"
                                            + "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,FAMILY_ID,FAMILY_NAME,SUB_FAMILY_ID,SUB_FAMILY_NAME,XML_DOC,DELETED_USER) VALUES"
                                            + "('" + xml_id + "'," + catalogID + ","
                                            + " '" + _drFamily["PARENT_FAMILY_ID"].ToString() + "',@FAMILY_NAME,'" + _drFamily["FAMILY_ID"].ToString() + "',@SUBFAMILY_NAME,'" + xml + "','" + User.Identity.Name.ToString() + "' ) ";
                            Command = new SqlCommand(sqlstr1, objSqlConnection);
                            Command.ExecuteNonQuery();
                        }
                        else if (_drFamily["PARENT_FAMILY_ID"].ToString() == "0")
                        {
                            string sqlstr1 = " DECLARE @FAMILY_NAME NVARCHAR(MAX)"
                                            + " SET @FAMILY_NAME = (SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + _drFamily["FAMILY_ID"].ToString() + "')"
                                            + "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,FAMILY_ID,FAMILY_NAME,XML_DOC,DELETED_USER) VALUES"
                                           + "('" + xml_id + "'," + catalogID + ","
                                           + " '" + _drFamily["FAMILY_ID"].ToString() + "',@FAMILY_NAME,'" + xml + "','" + User.Identity.Name.ToString() + "' ) ";
                            Command = new SqlCommand(sqlstr1, objSqlConnection);
                            Command.ExecuteNonQuery();

                        }

                        productID = string.Empty;
                        xml = string.Empty;
                        recycleDS.Dispose();
                    }
                    DataSet ds = new DataSet();
                    if (family_cat != string.Empty)
                    {
                        if (family_cat[0] == ',')
                        {
                            family_cat = family_cat.Substring(1);
                        }
                        SqlCommand cmd = new SqlCommand("SELECT CATALOG_ID,SORT_ORDER,FAMILY_ID,CATEGORY_ID,CREATED_USER,CREATED_DATE,ROOT_CATEGORY FROM TB_CATALOG_FAMILY WHERE CATALOG_ID!=1 AND FAMILY_ID in (" + family_cat + ")    and category_id='" + categoryId + "' union  SELECT CATALOG_ID,SORT_ORDER,FAMILY_ID,CATEGORY_ID,CREATED_USER,CREATED_DATE,ROOT_CATEGORY FROM TB_CATALOG_FAMILY WHERE CATALOG_ID!=1 AND FAMILY_ID in (" + family_cat + ") and root_category='" + 0 + " '    for xml path('CATALOG_FAMILY'),type", objSqlConnection);
                        SqlDataAdapter dafam = new SqlDataAdapter(cmd);
                        dafam.Fill(ds, "CATALOG_FAMILY");
                        if (ds.Tables["CATALOG_FAMILY"].Rows.Count > 0 && ds.Tables["CATALOG_FAMILY"].Rows[0][0].ToString() != "" && ds.Tables["CATALOG_FAMILY"].Rows[0][0] != null)
                        {
                            catalog_xml = catalog_xml + ds.Tables["CATALOG_FAMILY"].Rows[0][0].ToString();
                        }

                        cmd = new SqlCommand("SELECT FAMILY_ID,SORT_ORDER,RECORD_ID,SECTION_NAME FROM TB_PROJECT_SECTION_DETAILS WHERE FAMILY_ID in (" + family_cat + ")  for xml path ('Project_Sections'),type", objSqlConnection);
                        SqlDataAdapter daprojsec = new SqlDataAdapter(cmd);
                        daprojsec.Fill(ds, "Project_Sections");
                        if (ds.Tables["Project_Sections"].Rows.Count > 0 && ds.Tables["Project_Sections"].Rows[0][0].ToString() != "" && ds.Tables["Project_Sections"].Rows[0][0] != null)
                        {
                            for (int i = 0; i < ds.Tables["Project_Sections"].Rows.Count; i++)
                            {
                                projectsec = projectsec + ds.Tables["Project_Sections"].Rows[i][0].ToString();
                            }
                        }
                        cmd = new SqlCommand("SELECT CATALOG_ID,ATTRIBUTE_ID,FAMILY_ID,CATEGORY_ID,ATTRIBUTE_VALUE FROM TB_FAMILY_KEY WHERE FAMILY_ID in (" + family_cat + ") and catalog_id = " + catalogID + " and category_id = '" + categoryId + "' for xml path('FAMILY_KEY'),type", objSqlConnection);
                        SqlDataAdapter dafamkey = new SqlDataAdapter(cmd);
                        dafamkey.Fill(ds, "FAMILY_KEY");
                        if (ds.Tables["FAMILY_KEY"].Rows.Count > 0 && ds.Tables["FAMILY_KEY"].Rows[0][0].ToString() != "" && ds.Tables["FAMILY_KEY"].Rows[0][0] != null)
                        {
                            catalog_xml = catalog_xml + ds.Tables["FAMILY_KEY"].Rows[0][0].ToString();
                        }

                        cmd = new SqlCommand("SELECT ATTRIBUTE_ID,FAMILY_ID,CATEGORY_ID,SORT_ORDER,CATALOG_ID FROM TB_CATEGORY_FAMILY_ATTR_LIST WHERE FAMILY_ID in (" + family_cat + ") and catalog_id = " + catalogID + " and category_id = '" + categoryId + "' for xml path ('FAMILY_PUBLISH'),type", objSqlConnection);
                        daprojsec = new SqlDataAdapter(cmd);
                        daprojsec.Fill(ds, "fam_attr_pub");
                        if (ds.Tables["fam_attr_pub"].Rows.Count > 0 && ds.Tables["fam_attr_pub"].Rows[0][0].ToString() != "" && ds.Tables["fam_attr_pub"].Rows[0][0] != null)
                        {
                            for (int i = 0; i < ds.Tables["fam_attr_pub"].Rows.Count; i++)
                            {
                                fampubattr = fampubattr + ds.Tables["fam_attr_pub"].Rows[i][0].ToString();
                            }
                        }
                        if (fampubattr != string.Empty && fampubattr != null)
                        {
                            if (fampubattr != "<?xml version=\"1.0\" standalone=\"yes\"?><FAMILY_PUBLISHED_ATTR>")
                            {
                                fampubattr = fampubattr + "</FAMILY_PUBLISHED_ATTR>";
                                fampubattr = fampubattr.Replace("'", "''");

                                string projsec = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER, CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryId + "','" + fampubattr + "','" + User.Identity.Name.ToString() + "' , '" + categoryShort + "' ) ";
                                cmd = new SqlCommand(projsec, objSqlConnection);
                                cmd.ExecuteNonQuery();
                            }
                        }

                        string projsec1 = "delete from tb_family_key where family_id in (" + family_cat + ") and catalog_id = " + catalogID + " and category_id = '" + categoryId + "'";
                        cmd = new SqlCommand(projsec1, objSqlConnection);
                        cmd.ExecuteNonQuery();

                    }
                    if (product_cat != string.Empty)
                    {
                        if (product_cat[0] == ',')
                        {
                            product_cat = product_cat.Substring(1);
                        }
                        //SqlCommand cmd = new SqlCommand("SELECT CATALOG_ID,PRODUCT_ID,CREATED_USER,CREATED_DATE FROM TB_CATALOG_PRODUCT WHERE CATALOG_ID!=1 AND PRODUCT_ID IN(" + product_cat + ") for xml path('CATALOG_PRODUCT'),type", objSqlConnection);
                        SqlCommand cmd = new SqlCommand("SELECT CATALOG_ID,cp.PRODUCT_ID,P.CATEGORY_ID,cp.CREATED_USER,cp.CREATED_DATE FROM TB_CATALOG_PRODUCT cp join tb_product p on p.product_id = cp.product_id WHERE CATALOG_ID!=1 AND cp.PRODUCT_ID IN(" + product_cat + ") for xml path('CATALOG_PRODUCT'),type", objSqlConnection);
                        SqlDataAdapter daprod = new SqlDataAdapter(cmd);
                        daprod.Fill(ds, "CATALOG_PRODUCT");
                        if (ds.Tables["CATALOG_PRODUCT"].Rows.Count > 0 && ds.Tables["CATALOG_PRODUCT"].Rows[0][0].ToString() != "" && ds.Tables["CATALOG_PRODUCT"].Rows[0][0] != null)
                        {
                            for (int i = 0; i < ds.Tables["CATALOG_PRODUCT"].Rows.Count; i++)
                            {
                                catalog_xml = catalog_xml + ds.Tables["CATALOG_PRODUCT"].Rows[i][0].ToString();
                            }
                        }
                        cmd = new SqlCommand("SELECT ATTRIBUTE_ID,SORT_ORDER,FAMILY_ID,PRODUCT_ID FROM TB_PROD_FAMILY_ATTR_LIST WHERE PRODUCT_ID IN(" + product_cat + ") for xml path('PRODUCT_PUBLISH'),type", objSqlConnection);
                        daprod = new SqlDataAdapter(cmd);
                        daprod.Fill(ds, "PROD_PUB_ATTR");
                        if (ds.Tables["PROD_PUB_ATTR"].Rows.Count > 0 && ds.Tables["PROD_PUB_ATTR"].Rows[0][0].ToString() != "" && ds.Tables["CATALOG_PRODUCT"].Rows[0][0] != null)
                        {
                            for (int i = 0; i < ds.Tables["PROD_PUB_ATTR"].Rows.Count; i++)
                            {
                                prodpubattr = prodpubattr + ds.Tables["PROD_PUB_ATTR"].Rows[i][0].ToString();
                            }
                        }
                        if (prodpubattr != "<?xml version=\"1.0\" standalone=\"yes\"?><PRODUCT_PUBLISHED_ATTR>")
                        {
                            prodpubattr = prodpubattr + "</PRODUCT_PUBLISHED_ATTR>";
                            prodpubattr = prodpubattr.Replace("'", "''");

                            string sqlstr = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER, CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryId + "','" + prodpubattr + "','" + User.Identity.Name.ToString() + "'  , '" + categoryShort + "') ";
                            cmd = new SqlCommand(sqlstr, objSqlConnection);
                            cmd.ExecuteNonQuery();
                        }
                        cmd = new SqlCommand("SELECT * FROM TB_PARTS_KEY WHERE PRODUCT_ID IN (" + product_cat + ") and catalog_id = " + catalogID + " and category_id = '" + categoryId + "' for xml path('PARTSKEY')", objSqlConnection);
                        SqlDataAdapter daparts = new SqlDataAdapter(cmd);
                        daparts.Fill(ds, "PARTS_KEY");
                        if (ds.Tables["PARTS_KEY"].Rows.Count > 0 && ds.Tables["PARTS_KEY"].Rows[0][0].ToString() != "" && ds.Tables["PARTS_KEY"].Rows[0][0] != null)
                        {
                            for (int i = 0; i < ds.Tables["PARTS_KEY"].Rows.Count; i++)
                            {
                                partskey = partskey + ds.Tables["PARTS_KEY"].Rows[i][0].ToString();
                            }
                        }
                        if (partskey != string.Empty && partskey != null)
                        {
                            if (partskey != "<?xml version=\"1.0\" standalone=\"yes\"?><PARTS_KEY>")
                            {
                                partskey = partskey + "</PARTS_KEY>";
                                partskey = partskey.Replace("'", "''");

                                string sqlstr11 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER, CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryId + "','" + partskey + "' ,'" + User.Identity.Name.ToString() + "', '" + categoryShort +"') ";
                                cmd = new SqlCommand(sqlstr11, objSqlConnection);
                                cmd.ExecuteNonQuery();
                            }
                            string sqlstrdelete = "delete FROM TB_PARTS_KEY WHERE PRODUCT_ID IN (" + product_cat + ") and catalog_id = " + catalogID + " and category_id = '" + categoryId + "'";
                            cmd = new SqlCommand(sqlstrdelete, objSqlConnection);
                            cmd.ExecuteNonQuery();


                        }

                    }
                    if (catalog_xml != "<?xml version=\"1.0\" standalone=\"yes\"?><CATALOG_ASSOCIATES>")
                    {
                        catalog_xml = catalog_xml + "</CATALOG_ASSOCIATES>";
                        catalog_xml = catalog_xml.Replace("'", "''");
                        string sqlstr = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,FAMILY_ID,XML_DOC,DELETED_USER) VALUES('" + xml_id + "','" + deleteNode + "','" + catalog_xml + "','" + User.Identity.Name.ToString() + "' ) ";
                        SqlCommand cmd = new SqlCommand(sqlstr, objSqlConnection);
                        cmd.ExecuteNonQuery();
                    }
                    if (projectsec != string.Empty && projectsec != null)
                    {
                        if (projectsec != "<?xml version=\"1.0\" standalone=\"yes\"?><PROJECTS>")
                        {
                            projectsec = projectsec + "</PROJECTS>";
                            projectsec = projectsec.Replace("'", "''");
                            string projsec = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATEGORY_ID,XML_DOC,DELETED_USER, CATEGORY_SHORT) VALUES('" + xml_id + "','" + categoryId + "','" + projectsec + "','" + User.Identity.Name.ToString() + "'  , '" + categoryShort + "') ";
                            SqlCommand cmd = new SqlCommand(projsec, objSqlConnection);
                            cmd.ExecuteNonQuery();
                        }
                    }


                    string _categoryLevel = " exec STP_CATALOGSTUDIO5_CategoryLevel '" + categoryId + "'";
                    SqlString = _categoryLevel;
                    DataSet _dsCatlevel = CreateDataSet();
                    foreach (DataRow drcat in _dsCatlevel.Tables[0].Rows)
                    {
                        string sqlstr2 = "INSERT INTO TB_RECYCLE_CATEGORY_REFERENCE (XML_ID,CAT_ID,LEVEL,CREATED_USER) VALUES"
                                                  + "('" + xml_id + "','" + drcat["CATEGORY_ID"].ToString() + "','" + drcat["LEVEL"].ToString() + "','" + User.Identity.Name + "')";
                        SqlCommand Command = new SqlCommand(sqlstr2, objSqlConnection);
                        Command.ExecuteNonQuery();
                    }

                    var cmdClone = new SqlCommand("DELETE FROM TB_CATALOG_FAMILY WHERE FAMILY_ID IN ( "
                                                + "SELECT FAMILY_ID FROM TB_CATALOG_FAMILY WHERE FAMILY_ID IN (" + deleteNode + ") AND CATEGORY_ID='" + categoryId + "' "
                                                + " UNION "
                                                + " SELECT FAMILY_ID FROM TB_CATALOG_FAMILY WHERE FAMILY_ID IN (SELECT FAMILY_ID FROM TB_FAMILY WHERE PARENT_FAMILY_ID = " + deleteNode + ") AND CATEGORY_ID='" + categoryId + "' "
                                                + " ) AND CATEGORY_ID='" + categoryId + "'", objSqlConnection);
                    cmdClone.ExecuteNonQuery();
                }

                return "";

            }
            catch (Exception ex)
            {
                _logger.Error("Error at Family/CloneRemoveFamily", ex);
                return "Action not successful";
            }

        }

        public string CloneCheck(string category_id, int catalog_id, int option, string PranetFamily)
        {
            string Category_ID = string.Empty;
            int ParentFamily_Id = 0;
            if (!string.IsNullOrEmpty(PranetFamily) && PranetFamily.Contains('~'))
            {
                var id = PranetFamily.Split('~');
                ParentFamily_Id = Convert.ToInt32(id[1]);
            }
            else
                ParentFamily_Id = Convert.ToInt32(PranetFamily);

            if (!string.IsNullOrEmpty(category_id))
            {
                if (category_id.Contains("~"))
                {
                    var id = category_id.Split('~');
                    Category_ID = Convert.ToString(id[0]);
                }
                else
                    Category_ID = category_id;
            }

            var categoryList = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == Category_ID).Select(y => new { CATEGORY_SHORT = y.CATEGORY_SHORT }).FirstOrDefault();
            string categoryShort = Category_ID;
            bool cloneLock = false;
            if (categoryList != null)
            {
                categoryShort = categoryList.CATEGORY_SHORT.ToString();
                //cloneLock= categoryList.CLONE_LOCK.ToString().ToUpper()=="TRUE"?true:false;
            }
            int Catalog_ID = catalog_id;
            if (Convert.ToInt32(option) == 1)
            {
                DataSet cloneCnt = new DataSet();
                SqlString = "SELECT COUNT(ROOT_CATEGORY) ,CLONE_LOCK FROM TB_CATALOG_FAMILY CF JOIN TB_CATEGORY C"
                                + " ON CF.ROOT_CATEGORY = C.CATEGORY_ID"
                                + " WHERE CF.CATEGORY_ID ='" + Category_ID + "' "
                                + " AND ROOT_CATEGORY !='0' AND CATALOG_ID = " + Catalog_ID + " GROUP BY C.CLONE_LOCK";
                cloneCnt = CreateDataSet();
               
                if (cloneCnt.Tables[0].Rows.Count > 0)
                {
                    if (categoryShort.ToUpper().StartsWith("CLONE") && categoryShort.ToString().Contains("-"))
                    {
                        if ((Convert.ToInt32(cloneCnt.Tables[0].Rows[0][0].ToString()) > 0) && !(Convert.ToBoolean(cloneCnt.Tables[0].Rows[0][1].ToString())))
                            return "Product added to this cloned Category will not be associated to the main Category or its cloned Categories";

                        else if ((Convert.ToInt32(cloneCnt.Tables[0].Rows[0][0].ToString()) > 0) && (Convert.ToBoolean(cloneCnt.Tables[0].Rows[0][1].ToString())))
                            return "Product added to this cloned Category will be associated to the main Category";
                    }
                }
                else if (categoryShort.ToString().ToUpper().StartsWith("CLONE") && categoryShort.Contains("-"))
                {
                    cloneCnt = new DataSet();
                    SqlString = "SELECT 1,CLONE_LOCK FROM TB_CATEGORY WHERE CATEGORY_SHORT =(SELECT SUBSTRING('" + categoryShort.ToString()
                                    + "',CHARINDEX('-','" + categoryShort + "')+1,LEN('" + categoryShort.ToString()
                                    + "')))";
                    cloneCnt = CreateDataSet();
                    if (cloneCnt.Tables[0].Rows.Count > 0)
                    {
                        if ((Convert.ToInt32(cloneCnt.Tables[0].Rows[0][0].ToString()) > 0) && !(Convert.ToBoolean(cloneCnt.Tables[0].Rows[0][1].ToString())))
                            return "Product added to this cloned Category will not be associated to the main Category or its cloned Categories";

                        else if ((Convert.ToInt32(cloneCnt.Tables[0].Rows[0][0].ToString()) > 0) && (Convert.ToBoolean(cloneCnt.Tables[0].Rows[0][1].ToString())))
                            return "Product added to this cloned Category will be associated to the main Category";
                    }
                }
                else
                {
                    cloneCnt.Dispose();
                    cloneCnt = new DataSet();
                    SqlString = "SELECT DISTINCT CF.CATALOG_ID,CF.CATEGORY_ID FROM TB_CATALOG_FAMILY CF " +
                                "INNER JOIN TB_CATEGORY C ON C.CATEGORY_ID=CF.CATEGORY_ID " +
                                "WHERE C.IS_CLONE=1 AND CF.CATALOG_ID <>1 AND C.CATEGORY_SHORT LIKE 'Clone%-'+'" + categoryShort + "'";
                    cloneCnt = CreateDataSet();

                    if (Convert.ToInt32(cloneCnt.Tables[0].Rows.Count) > 0 &&!cloneLock)
                            return "Product added to this main category will be associated under all of its cloned Categories";
                    //else
                    //{
                    //    cloneCnt.Dispose();
                    //    cloneCnt = new DataSet();
                    //    SqlString = "SELECT DISTINCT C.CATEGORY_ID FROM TB_CATEGORY C  " +
                    //           "WHERE C.IS_CLONE=1 AND C.CATEGORY_SHORT LIKE 'Clone%-'+'" + categoryShort + "'";
                    //    cloneCnt = CreateDataSet();
                    //    if (Convert.ToInt32(cloneCnt.Tables[0].Rows.Count) > 0 && !cloneLock)
                    //        return "Family added to this main category will be associated under all of its cloned Categories";
                    //}

                }
            }
            else if (Convert.ToInt32(option) == 2)
            {
                DataSet cloneCnt = new DataSet();
                SqlString = "SELECT COUNT(ROOT_CATEGORY) ,CLONE_LOCK FROM TB_CATALOG_FAMILY CF JOIN TB_CATEGORY C"
                                + " ON CF.ROOT_CATEGORY = C.CATEGORY_ID"
                                + " WHERE CF.CATEGORY_ID ='" + Category_ID + "' "
                                + " AND ROOT_CATEGORY !='0' AND CATALOG_ID = " + Catalog_ID + " GROUP BY C.CLONE_LOCK";
                cloneCnt = CreateDataSet();
                if (cloneCnt.Tables[0].Rows.Count > 0)
                {
                    DataSet cloneSubCnt = new DataSet();
                    SqlString = "SELECT COUNT(ROOT_CATEGORY) FROM TB_CATALOG_FAMILY CF "
                                    + " WHERE CF.FAMILY_ID =" + ParentFamily_Id.ToString() + " "
                                    + " AND CF.CATEGORY_ID ='" + Category_ID + "' "
                                    + " AND ROOT_CATEGORY !='0' AND CATALOG_ID = " + Catalog_ID;
                    cloneSubCnt = CreateDataSet();
                    if (cloneSubCnt.Tables[0].Rows.Count > 0)
                    {
                        if (Convert.ToInt32(cloneSubCnt.Tables[0].Rows[0][0].ToString()) > 0 && !cloneLock)
                            return "Subfamily added to this Product will be associated to all main and cloned Category";
                    }

                }
                else if (Category_ID.ToString().ToUpper().StartsWith("CLONE") && Category_ID.Contains("-"))
                {
                    cloneCnt = new DataSet();
                    SqlString = "SELECT 1,CLONE_LOCK FROM TB_CATEGORY WHERE CATEGORY_SHORT =(SELECT SUBSTRING('" + categoryShort.ToString()
                                    + "',CHARINDEX('-','" + categoryShort + "')+1,LEN('" + categoryShort.ToString()
                                    + "')))";
                    cloneCnt = CreateDataSet();
                    if (cloneCnt.Tables[0].Rows.Count > 0)
                    {
                        DataSet cloneSubCnt = new DataSet();
                        SqlString = "SELECT COUNT(ROOT_CATEGORY) FROM TB_CATALOG_FAMILY CF "
                                        + " WHERE CF.FAMILY_ID =" + ParentFamily_Id.ToString() + " "
                                        + " AND CF.CATEGORY_ID ='" + Category_ID.ToString() + "' "
                                        + " AND ROOT_CATEGORY !='0' AND CATALOG_ID = " + Catalog_ID;
                        cloneSubCnt = CreateDataSet();
                        if (cloneSubCnt.Tables[0].Rows.Count > 0)
                        {
                            if (Convert.ToInt32(cloneSubCnt.Tables[0].Rows[0][0].ToString()) > 0 && !cloneLock)
                                return "Subfamily added to this Product will be associated to all main and cloned Category";
                        }
                    }
                }
                else
                {
                    cloneCnt.Dispose();
                    cloneCnt = new DataSet();
                    SqlString = "SELECT DISTINCT CF.CATALOG_ID,CF.CATEGORY_ID FROM TB_CATALOG_FAMILY CF " +
                                "INNER JOIN TB_CATEGORY C ON C.CATEGORY_ID=CF.CATEGORY_ID " +
                                "WHERE C.IS_CLONE=1 AND CF.CATALOG_ID <>1 AND C.CATEGORY_SHORT LIKE 'Clone%-'+'" + categoryShort + "'";
                    cloneCnt = CreateDataSet();

                    if (Convert.ToInt32(cloneCnt.Tables[0].Rows.Count) > 0)
                    {
                        DataSet cloneSubCnt = new DataSet();
                        SqlString = "SELECT COUNT(ROOT_CATEGORY) FROM TB_CATALOG_FAMILY CF "
                                        + " WHERE CF.FAMILY_ID =" + ParentFamily_Id.ToString() + " "
                                        + " AND CF.CATEGORY_ID ='" + Category_ID + "' "
                                        + " AND ROOT_CATEGORY !='0' AND CATALOG_ID = " + Catalog_ID;
                        cloneSubCnt = CreateDataSet();
                        if (cloneSubCnt.Tables[0].Rows.Count > 0)
                        {
                            if (Convert.ToInt32(cloneSubCnt.Tables[0].Rows[0][0].ToString()) > 0 && !cloneLock)
                                return "Subfamily added to this Product will be associated to all main and cloned Category";
                        }
                    }
                }
            }
            return "";
        }


        public DataSet CreateDataSet()
        {
            DataSet dsReturn = new DataSet();
            using (
                var objSqlConnection =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {
                DataSet ds = new DataSet();

                SqlDataAdapter _DBAdapter = new SqlDataAdapter(SqlString, objSqlConnection);
                _DBAdapter.SelectCommand.CommandTimeout = 0;
                _DBAdapter.Fill(dsReturn);
                _DBAdapter.Dispose();
                return dsReturn;
            }
        }

        public bool SubproductCheck(string family_id, string multipleFamilyids, string multipleFamilyidsForAnotherCatalog, int catalog_id, string option)
        {
            bool value = false;
            if (option.ToLower() == "family")
            {
                if (multipleFamilyids != null && multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog != null && multipleFamilyidsForAnotherCatalog.Trim() != "")
                {
                    string[] multipleFamilyIds = null;

                    if (multipleFamilyids != null)
                    {
                        multipleFamilyids = multipleFamilyids.Replace("~", "");
                        multipleFamilyIds = multipleFamilyids.Split(',');
                    }

                    else
                    {
                        multipleFamilyidsForAnotherCatalog = multipleFamilyidsForAnotherCatalog.Replace("~", "");
                        multipleFamilyIds = multipleFamilyidsForAnotherCatalog.Trim('~').Split(',');
                    }

                    var result = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, pf => pf.PRODUCT_ID, sp => sp.PRODUCT_ID, (pf, sp) => new { pf, sp }).Where(x => multipleFamilyIds.Contains(x.pf.FAMILY_ID.ToString()) && x.sp.CATALOG_ID == catalog_id);
                    if (result.Any())
                        value = true;

                }
                else
                {
                    int fid = Convert.ToInt32(family_id.Trim('~'));
                    var result = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, pf => pf.PRODUCT_ID, sp => sp.PRODUCT_ID, (pf, sp) => new { pf, sp }).Where(x => x.pf.FAMILY_ID == fid && x.sp.CATALOG_ID == catalog_id);
                    if (result.Any())
                        value = true;
                }
                return value;
            }
            else if (option.ToLower() == "category")
            {
                if (multipleFamilyids != null && multipleFamilyids.Trim() != "" || multipleFamilyidsForAnotherCatalog != null && multipleFamilyidsForAnotherCatalog.Trim() != "")
                {
                    string[] multipleFamilyIds = null;

                    if (multipleFamilyids != null)
                    {
                        multipleFamilyIds = multipleFamilyids.Split(',');
                    }

                    else
                    {
                        multipleFamilyIds = multipleFamilyidsForAnotherCatalog.Split(',');
                    }

                    var family_ids = _dbcontext.TB_CATALOG_FAMILY.Where(a => multipleFamilyIds.Contains(a.CATEGORY_ID) && a.CATALOG_ID == catalog_id).Select(a => a.CATEGORY_ID).ToArray();
                    var result = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, pf => pf.PRODUCT_ID, sp => sp.PRODUCT_ID, (pf, sp) => new { pf, sp }).Where(x => family_ids.Contains(x.pf.FAMILY_ID.ToString()) && x.sp.CATALOG_ID == catalog_id);
                    if (result.Any())
                        value = true;
                    else
                        value = false;
                }
                else
                {
                    var family_ids = _dbcontext.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == family_id && a.CATALOG_ID == catalog_id).Select(a => a.FAMILY_ID.ToString()).ToArray();
                    if(family_ids.Any())
                    { }
                    else
                    {
                       var catids = _dbcontext.Category_Function(catalog_id, family_id).Select(a => a.CATEGORY_ID).ToArray();
                       family_ids = _dbcontext.TB_CATALOG_FAMILY.Where(a => catids.Contains(a.CATEGORY_ID) && a.CATALOG_ID == catalog_id).Select(a => a.FAMILY_ID.ToString()).ToArray();
                    }

                    var result = _dbcontext.TB_PROD_FAMILY.Join(_dbcontext.TB_SUBPRODUCT, pf => pf.PRODUCT_ID, sp => sp.PRODUCT_ID, (pf, sp) => new { pf, sp }).Where(x => family_ids.Contains(x.pf.FAMILY_ID.ToString()) && x.sp.CATALOG_ID == catalog_id);
                    if (result.Any())
                        value = true;
                    else
                        value = false;
                }
            }
            else if (option.ToLower() == "product")
            {
                var productchk = _dbcontext.TB_PROD_SPECS.FirstOrDefault(a => a.STRING_VALUE == family_id && a.ATTRIBUTE_ID == 1);
                if (productchk != null)
                {
                    var subproductchk = _dbcontext.TB_SUBPRODUCT.Where(a => a.PRODUCT_ID == productchk.PRODUCT_ID && a.CATALOG_ID == catalog_id);
                    if (subproductchk.Any())
                        value = true;
                    else
                        value = false;
                }
                else
                    value = false;
            }
            return value;
        }

    }
}