﻿using System;
using System.Web.Http;
using LS.Data;
using LS.Web.Models;
using log4net;

namespace LS.Web.Controllers
{
    public class CodeTableApiController : ApiController
    {
        private ILog _logger = LogManager.GetLogger(typeof(CodeTableApiController));


        //public HttpResponseMessage GetCountries()
        //{
        //    try
        //    {
        //        using (var objLs = new CSEntities())
        //        {
        //            List<Country> entityList = objLs.Countries.OrderBy(x => x.CountryName).ToList();
        //            List<CountryModel> response = entityList.Select(x => CountryModel.GetModel(x)).ToList();
        //            return Request.CreateResponse(HttpStatusCode.OK, response);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at CodeTableController - GetCountries", ex);
        //        return Request.CreateResponse(HttpStatusCode.BadRequest);
        //    }
        //}

        //public IQueryable<CodeTable> GetCodeTables()
        //{
        //    try
        //    {
        //        _logger.Info("Inside  at CodeTableController - GetCodeTables");
        //        return CodeTableRepository.DC.CodeTables;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at CodeTableController - GetCodeTables", ex);
        //        throw;
        //    }
        //}
        ////[HttpGet]
        //public IList<CodeTableModel> GetCodeTablesByCodeName(string codeName)
        //{
        //    try
        //    {
        //        _logger.Info("Inside  at CodeTableController - GetCodeTablesByCodeName");
        //        //var codeTables = (from codeTable in CodeTableRepository.DC.CodeTables
        //        //                  where codeTable.CodeName.Trim().ToLower() == codeName.Trim().ToLower()
        //        //                  select codeTable).OrderBy(x => x.CodeSequence).ToList();

        //        return (from x in CodeTableRepository.DC.CodeTables
        //                where x.CodeName.Trim().ToLower() == codeName.Trim().ToLower()
        //                orderby x.CodeSequence ascending
        //                select new CodeTableModel()
        //                {
        //                    CodeName = x.CodeName,
        //                    CodeDisplayValue = x.CodeDisplayValue,
        //                    CodeSequence = x.CodeSequence,
        //                    CodeValue = x.CodeValue,
        //                    CodeValueDescription = x.CodeValueDescription
        //                }).ToList<CodeTableModel>();

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at CodeTableController - GetCodeTablesByCodeName", ex);
        //        throw;
        //    }
        //}

        //[HttpGet]
        //public IList<CodeTableModel> GetCodeTableValuesByCodeNames(string codeNames)
        //{

        //    string[] codeNameList = codeNames.Split(',');

        //    try
        //    {
        //        _logger.Info("Inside  at CodeTableController - GetCodeTablesByCodeName");                
        //        return (from x in CodeTableRepository.DC.CodeTables
        //                where codeNameList.Contains(x.CodeName.Trim().ToLower())
        //                orderby x.CodeSequence ascending
        //                select new CodeTableModel()
        //                {
        //                    CodeName = x.CodeName,
        //                    CodeDisplayValue = x.CodeDisplayValue,
        //                    CodeSequence = x.CodeSequence,
        //                    CodeValue = x.CodeValue,
        //                    CodeValueDescription = x.CodeValueDescription
        //                }).ToList<CodeTableModel>();

        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at CodeTableController - GetCodeTablesByCodeName", ex);
        //        throw;
        //    }
        //}


        //public HttpResponseMessage GetCountries()
        //{
        //    try
        //    {
        //        List<Country> entityList = CodeTableRepository.DC.Countries.ToList();
        //        List<CountryModel> response = entityList.Select(x => CountryModel.GetModel(x)).ToList();
        //        return Request.CreateResponse(HttpStatusCode.OK, response);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at CodeTableController - GetCountries", ex);
        //        return Request.CreateResponse(HttpStatusCode.BadRequest);                   
        //    }
        //}

        [HttpPost]
        public bool JavaScriptErrorLogger(JavaScriptError error)
        {
            try
            {
                Exception innerJsException = new Exception("Cause:" + error.Cause + " StackTrace:" + string.Join("\n", error.StackTrace.ToArray()) + "\n<---JS Exception - Ends Here--->");
                Exception jsException = new Exception("<---JS Exception - Starts Here--->\n" + error.ErrorMessage, innerJsException);
                _logger.Error("JavaScriptError", jsException);
                return true;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at JavaScriptErrorLogger", ex);
                return false;
            }
        }

        protected override void Dispose(bool disposing)
        {
            try
            {
                _logger.Info("Inside  at CodeTableController - Dispose");
                if (disposing)
                {
                    CodeTableRepository.DC.Dispose();
                }
                base.Dispose(disposing);
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CodeTableController - Dispose", ex);
                throw;
            }
        }
    }
}