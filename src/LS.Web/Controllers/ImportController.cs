﻿using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Kendo.DynamicLinq;
using LS.Data;
using LS.Data.Model.CatalogSectionModels;
using log4net;
using LS.Data.Model;
using System.Web.Http;
using System.Collections;
using System.Net.Http;
using System.Net;
using LS.Data.Utilities;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using LS.Data.Model.ProductView;
using System.Data.OleDb;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Data;
using System;
using System.Globalization;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Linq;
using System.Configuration;
using System.Web.Script.Serialization;
using Infragistics.Documents.Excel;
using System.Web.SessionState;
using LS.Web.Models;

namespace LS.Web.Controllers
{
    public class ImportController : Controller
    {
        private DataTable objdatatable;
        public string _SQLString = "";

        public enum CSF
        {
            [Description("CATALOG_ID")]
            CATALOG_ID = 1,
            [Description("CATALOG_NAME")]
            CATALOG_NAME = 2,
            [Description("CATALOG_VERSION")]
            CATALOG_VERSION = 3,
            [Description("CATALOG_DESCRIPTION")]
            CATALOG_DESCRIPTION = 4,
            [Description("CATEGORY_ID")]
            CATEGORY_ID = 101,
            [Description("CATEGORY_NAME")]
            CATEGORY_NAME = 102,
            [Description("SUBCATID_L1")]
            SUBCATID_L1 = 103,
            [Description("SUBCATNAME_L1")]
            SUBCATNAME_L1 = 104,
            [Description("SUBCATID_L2")]
            SUBCATID_L2 = 105,
            [Description("SUBCATNAME_L2")]
            SUBCATNAME_L2 = 106,
            [Description("SUBCATID_L3")]
            SUBCATID_L3 = 107,
            [Description("SUBCATNAME_L3")]
            SUBCATNAME_L3 = 108,
            [Description("SUBCATID_L4")]
            SUBCATID_L4 = 109,
            [Description("SUBCATNAME_L4")]
            SUBCATNAME_L4 = 110,
            [Description("SUBCATID_L5")]
            SUBCATID_L5 = 111,
            [Description("SUBCATNAME_L5")]
            SUBCATNAME_L5 = 112,
            [Description("SUBCATID_L6")]
            SUBCATID_L6 = 113,
            [Description("SUBCATNAME_L6")]
            SUBCATNAME_L6 = 114,
            [Description("SUBCATID_L7")]
            SUBCATID_L7 = 115,
            [Description("SUBCATNAME_L7")]
            SUBCATNAME_L7 = 116,
            [Description("SUBCATID_L8")]
            SUBCATID_L8 = 117,
            [Description("SUBCATNAME_L8")]
            SUBCATNAME_L8 = 118,
            [Description("SUBCATID_L9")]
            SUBCATID_L9 = 119,
            [Description("SUBCATNAME_L9")]
            SUBCATNAME_L9 = 120,
            [Description("CATEGORY_SHORT_DESC")]
            CATEGORY_SHORT_DESC = 151,
            [Description("CATEGORY_IMAGE_FILE")]
            CATEGORY_IMAGE_FILE = 152,
            [Description("CATEGORY_IMAGE_NAME")]
            CATEGORY_IMAGE_NAME = 153,
            [Description("CATEGORY_IMAGE_TYPE")]
            CATEGORY_IMAGE_TYPE = 154,
            [Description("CATEGORY_IMAGE_FILE2")]
            CATEGORY_IMAGE_FILE2 = 155,
            [Description("CATEGORY_IMAGE_NAME2")]
            CATEGORY_IMAGE_NAME2 = 156,
            [Description("CATEGORY_IMAGE_TYPE2")]
            CATEGORY_IMAGE_TYPE2 = 157,
            [Description("CATEGORY_CUSTOM_NUM_FIELD1")]
            CATEGORY_CUSTOM_NUM_FIELD1 = 158,
            [Description("CATEGORY_CUSTOM_NUM_FIELD2")]
            CATEGORY_CUSTOM_NUM_FIELD2 = 159,
            [Description("CATEGORY_CUSTOM_NUM_FIELD3")]
            CATEGORY_CUSTOM_NUM_FIELD3 = 160,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD1")]
            CATEGORY_CUSTOM_TEXT_FIELD1 = 161,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD2")]
            CATEGORY_CUSTOM_TEXT_FIELD2 = 162,
            [Description("CATEGORY_CUSTOM_TEXT_FIELD3")]
            CATEGORY_CUSTOM_TEXT_FIELD3 = 163,
            [Description("FAMILY_ID")]
            FAMILY_ID = 201,
            [Description("FAMILY_NAME")]
            FAMILY_NAME = 202,
            [Description("SUBFAMILY_ID")]
            SUBFAMILY_ID = 203,
            [Description("SUBFAMILY_NAME")]
            SUBFAMILY_NAME = 204,
            [Description("CATEGORY_ID")]
            FAMCATEGORY_ID = 205,
            [Description("FOOT_NOTES")]
            FOOT_NOTES = 206,
            [Description("STATUS")]
            STATUS = 207,
            [Description("FAMILY_SORT")]
            FAMILY_SORT = 208,
            [Description("PRODUCT_ID")]
            PRODUCT_ID = 301,

            [Description("CATALOG_ITEM_NO")] //[Description("CAT#")]
            CATALOG_ITEM_NO = 302,
            [Description("PRODUCT_SORT")]
            PRODUCT_SORT = 303,
            [Description("REMOVE_PRODUCT")]
            REMOVE_PRODUCT = 304,
            [Description("ATTRIBUTE_ID")]
            ATTRIBUTE_ID = 401
        };

        private DataSet _dsSheets = new DataSet();
        private string Message;
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        private static ILog _logger = LogManager.GetLogger(typeof(FamilyApiController));
        private CSEntities objLS = new CSEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ImportData()
        {
            var customerId = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
            var sampleTemplate = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.CUSTOMER_ID == 0 && x.CATALOG_ID == 0).OrderBy(y => y.TEMPLATE_ID).ToList();
            var withoutSampleTemplate = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.CUSTOMER_ID == customerId || x.CUSTOMER_ID == 0 && x.CATALOG_ID != 0).OrderByDescending(y => y.CREATED_DATE).ToList();
            var templateDetails = sampleTemplate.Union(withoutSampleTemplate);
            bool isAvailable = templateDetails.Any(s => s.TEMPLATE_NAME == "ImportTemplate" && s.CUSTOMER_ID == customerId);
            if (isAvailable)
            {
                var templateExists = objLS.TB_TEMPLATE_DETAILS_IMPORT.Where(x => x.TEMPLATE_NAME == "ImportTemplate" && x.CUSTOMER_ID == customerId).FirstOrDefault();
                if (templateExists != null)
                {
                    objLS.TB_TEMPLATE_DETAILS_IMPORT.Remove(templateExists);
                    objLS.SaveChanges();
                }
            }
            return View();
        }

        public ActionResult Import()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = objLS.vw_aspnet_UsersInRoles
                .Join(objLS.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                .Join(objLS.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                    (tcptps, tcp) => new { tcptps, tcp })
                .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {

                return View("~/Views/App/Import/Import.cshtml");
            }
            var rolefunctionresults = objLS.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 40303).ToList();
            if (rolefunctionresults.Count > 0)
            {
                return View("~/Views/App/Import/Import.cshtml");
            }
            else
            {
                return RedirectToAction("LogOn", "Accounts");
            }

        }

        public ActionResult ImportType()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            var SuperAdminCheck = objLS.vw_aspnet_UsersInRoles
                .Join(objLS.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                .Join(objLS.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                    (tcptps, tcp) => new { tcptps, tcp })
                .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
            if (SuperAdminCheck.Any())
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Import/ImportType.cshtml");
            }
            var rolefunctionresults = objLS.STP_LS_GET_USER_ROLE_FUNCTION(User.Identity.Name, 40303).ToList();
            if (rolefunctionresults.Count > 0)
            {
                TempData.Keep("WelcomeName");
                return View("~/Views/App/Import/ImportType.cshtml");
            }
            else
            {
                return RedirectToAction("LogOn", "Accounts");
            }

        }

        public DataTable ConvertJarryToDataTable(JArray modelArray)
        {
            var result = new DataTable();
            foreach (var jRow in modelArray)
            {
                foreach (var jToken in jRow)
                {
                    var jProperty = jToken as JProperty;
                    if (jProperty == null) continue;
                    if (result.Columns[jProperty.Name] == null)
                    {
                        result.Columns.Add(jProperty.Name, typeof(string));
                    }
                }
            }
            foreach (var jRow in modelArray)
            {
                var dataRow = result.NewRow();
                foreach (var jToken in jRow)
                {
                    var jProperty = jToken as JProperty;
                    if (jProperty == null) continue;
                    dataRow[jProperty.Name] = jProperty.Value.ToString();
                }
            }
            return result;
        }

        public void ImportStatus(string status)
        {
            ViewBag.importStatus = "status";
        }

        [System.Web.Http.HttpGet]
        public IList importExcelSheetSelection(string excelPath, string SheetName)
        {
            var dt = new DataTable();
            DataTable dtexcel = new DataTable();
            List<object> newList = new List<object>();
            try
            {
                if (!string.IsNullOrEmpty(excelPath))
                {
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    string strConn;
                    if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    else
                        //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                        //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,
                        new object[] { null, null, null, "TABLE" });
                    int jk = 0;
                    int sheetCount = schemaTable.Rows.Count;
                    int sheetColumnCount = 0;
                    if (!SheetName.Contains("$"))
                    {
                        SheetName = SheetName + "$";
                    }
                    foreach (DataRow rows in schemaTable.Rows)
                    {
                        ImportSheetName objImportSheetName = new ImportSheetName();
                        objImportSheetName.TABLE_NAME = Convert.ToString(rows[2]);
                        objImportSheetName.SHEET_PATH = excelPath;
                        newList.Add(objImportSheetName); //sheetnames[i] = schemaTable.Rows[i];
                        jk++;
                    }
                    for (int i = 0; i < sheetCount; i++)
                    {
                        dt = new DataTable(newList[i].ToString());

                        var dc = new DataColumn("ExcelColumn", typeof(string));
                        dt.Columns.Add(dc);

                        dc = new DataColumn("CatalogField", typeof(string));
                        dt.Columns.Add(dc);

                        dc = new DataColumn("SelectedToImport", typeof(bool));
                        dt.Columns.Add(dc);

                        dc = new DataColumn("IsSystemField", typeof(bool));
                        dt.Columns.Add(dc);

                        dc = new DataColumn("FieldType", typeof(string));
                        dt.Columns.Add(dc);


                        DataRow schemaRow = schemaTable.Rows[0];
                        string sheet = schemaRow["TABLE_NAME"].ToString();
                        if (!sheet.EndsWith("_"))
                        {
                            string query = "SELECT  * FROM [" + SheetName + "]";
                            OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                            dtexcel.Locale = CultureInfo.CurrentCulture;
                            daexcel.Fill(dtexcel);
                        }

                        sheetColumnCount = dtexcel.Columns.Count;

                    }
                    //  var wr = dtexcel.Rows[0][jk];
                    for (int j = 0; j < sheetColumnCount; j++)
                    {
                        if (dtexcel.Columns[j].ToString() != null)
                        {
                            String sColName = dtexcel.Columns[j].ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                            DataRow dr = dt.NewRow();
                            dr[0] = sColName;
                            dt.Rows.Add(dr);
                        }
                    }
                    _dsSheets.Tables.Add(dt);
                    GetExcelColumn2CatalogMapping(SheetName);
                    dt.Dispose();
                    conn.Close();
                    conn.Dispose();

                }
            }


            catch (Exception objException)
            {
                _logger.Error("Error at loading importExcelSheetSelection in Import Controller", objException);
                return null;
            }
            return newList;
        }

        public DataTable GetExcelColumn2CatalogMapping(String xlsSheetName)
        {
            try
            {
                DataTable dt = _dsSheets.Tables[xlsSheetName];
                int i = 0;
                if (dt != null)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        string sCheckAttributeName = dr[i].ToString().ToUpper();
                        //Check if this is a CatalogStudio system defined field name
                        Boolean vaild = IsCatalogStudioField(sCheckAttributeName);


                        if (sCheckAttributeName == "SUBITEM#" || sCheckAttributeName == "ITEM#")
                        {
                            vaild = true;
                        }
                        if (vaild) //CatalogStudio Field
                        {
                            dr["CatalogField"] = sCheckAttributeName;
                            dr["CatalogField"] = true;
                            dr["FieldType"] = 0;
                        }
                        else //User defined attribute name
                        {
                            string sAttributeNameInCatalog;
                            var customerid = objLS.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                            if (customerid != null)
                            {
                                var result =
                                    objLS.TB_ATTRIBUTE.Join(objLS.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                                        tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                        .Where(
                                            x =>
                                                x.tcp.ATTRIBUTE_NAME == sCheckAttributeName &&
                                                x.tpf.CUSTOMER_ID == customerid.CustomerId).Select(x => x.tcp).ToList();

                                var reResult = (IEnumerable<DataRow>)result;
                                DataTable drAttr = reResult.CopyToDataTable();

                                if (drAttr.Rows.Count > 0)
                                {
                                    sAttributeNameInCatalog = drAttr.Rows[0]["ATTRIBUTE_NAME"].ToString();
                                    string sAttributeDatatype = drAttr.Rows[0]["ATTRIBUTE_DATATYPE"].ToString();
                                    string sAttrType = drAttr.Rows[0]["ATTRIBUTE_TYPE"].ToString();
                                    if (sAttributeDatatype == "Date and Time")
                                    {
                                        if (sAttrType == "6" || sAttrType == "1" || sAttrType == "3")
                                        {
                                            dr["FieldType"] = 10;
                                        }
                                        else if (sAttrType == "7" || sAttrType == "11" || sAttrType == "13")
                                        {
                                            dr["FieldType"] = 14;
                                        }
                                    }
                                    else
                                    {
                                        dr["FieldType"] = drAttr.Rows[0]["ATTRIBUTE_TYPE"].ToString();
                                    }
                                }
                                else
                                {
                                    sAttributeNameInCatalog = "";
                                    dr["FieldType"] = "";
                                }
                                dr["CatalogField"] = sAttributeNameInCatalog;
                                dr["IsSystemField"] = false;
                            }
                        }
                        i++;
                    }
                }

                return dt;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportController :GetExcelColumn2CatalogMapping", ex);
                return null;
            }
        }
        public static Boolean IsCatalogStudioField(String enumFieldName)
        {
            return Enum.GetNames(typeof(CSF)).Any(val => enumFieldName == val);
        }
        [System.Web.Http.HttpGet]
        public bool CheckImportSheet(string excelPath)
        {
            var dt = new DataTable();
            bool sheetValid = false;
            // DataTable dtexcel = new DataTable();
            // List<object> newList = new List<object>();
            try
            {
                List<ImportSheetName> newList = new List<ImportSheetName>();
                if (!string.IsNullOrEmpty(excelPath))
                {

                    Workbook workbook = Workbook.Load(excelPath);
                    foreach (Worksheet rows in workbook.Worksheets)
                    {
                        ImportSheetName objImportSheetName = new ImportSheetName();
                        objImportSheetName.TABLE_NAME = rows.Name;
                        objImportSheetName.SHEET_PATH = excelPath;
                        newList.Add(objImportSheetName);
                    }
                    string SheetName = newList[0].TABLE_NAME.ToString();
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    string strConn;
                    if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    else
                        //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                        //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                                ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    // DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                    // int sheetCount = schemaTable.Rows.Count;
                    //  int sheetColumnCount = 0;

                    if (!SheetName.Contains("$"))
                    {
                        SheetName = SheetName + "$";
                    }
                    dt = new DataTable(SheetName);
                    var dc = new DataColumn("ExcelColumn", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("CatalogField", typeof(string));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("SelectedToImport", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("IsSystemField", typeof(bool));
                    dt.Columns.Add(dc);

                    dc = new DataColumn("FieldType", typeof(string));
                    dt.Columns.Add(dc);
                    //DataRow schemaRow = schemaTable.Rows[0];
                    //string sheet = schemaRow["TABLE_NAME"].ToString();
                    //if (!sheet.EndsWith("_"))
                    //{
                    //    string query = "SELECT  * FROM [" + SheetName + "]";
                    //    OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                    //    dtexcel.Locale = CultureInfo.CurrentCulture;
                    //    daexcel.Fill(dtexcel);
                    //}
                    //sheetColumnCount = dtexcel.Columns.Count;

                    //int iCols = sheetColumnCount;
                    string sheetNameworkbook = SheetName.Replace("$", "").TrimEnd();
                    if (workbook.Worksheets[sheetNameworkbook].Rows.Count() > 2 && workbook.Worksheets[sheetNameworkbook].Rows[0].Cells.Count() > 4)
                    {
                        if (workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[0].ToString() == "IMPORTTYPE" && workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[1].ToString() == "P" && workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[0].ToString() == "FLAG")
                        {
                            sheetValid = true;
                        }
                    }
                    int iCols = workbook.Worksheets[sheetNameworkbook].Rows[0].Cells.Count();

                    for (int j = 0; j < iCols; j++)
                    {
                        if (workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[j].Value != null)
                        {
                            String sColName =
                                workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[j].Value.ToString()
                                    .Replace('[', ' ')
                                    .Replace(']', ' ')
                                    .Trim();

                            DataRow dr = dt.NewRow();
                            dr[0] = sColName;
                            dt.Rows.Add(dr);
                        }
                    }


                    _dsSheets.Tables.Add(dt);
                    dt.Dispose();

                    conn.Close();
                    conn.Dispose();
                }
                return sheetValid;
            }
            catch (Exception objException)
            {
                _logger.Error("Error at ImportApiController :importSelectionColumnGrid", objException);
                return sheetValid;
            }
        }

        public JsonResult SaveFiles()
        {
            try
            {
                string fileName, actualFileName;
                string FilePath = string.Empty;
                Message = fileName = actualFileName = string.Empty;
                bool flag = false;
                if (Request.Files != null)
                {
                    var file = Request.Files[0];
                    actualFileName = file.FileName;
                    fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    int size = file.ContentLength;
                    try
                    {
                        file.SaveAs(Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName));
                        FilePath = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName);
                        CheckImportSheet(FilePath);
                        FilePath = FilePath;
                    }
                    catch (Exception)
                    {
                        Message = "File upload failed, please try again";
                    }

                }
                return Json(FilePath, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objex)
            {
                _logger.Error("Error at ImportController : SaveFiles", objex);
                return null;
            }
        }


        public JsonResult SaveFilesFullImport()
        {
            try
            {
                string fileName, actualFileName,newFileName;
                string FilePath = string.Empty;
                Message = fileName = actualFileName = string.Empty;
                bool flag = false;
                if (Request.Files != null)
                {
                    var file = Request.Files[0];
                    actualFileName = file.FileName;
                    if (file.InputStream.CanWrite == false)
                    {
                        fileName = Guid.NewGuid() +"_copy"+ Path.GetExtension(file.FileName);
                        newFileName= Guid.NewGuid() + Path.GetExtension(file.FileName);
                    }
                    else
                    {
                        fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                        newFileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
                    }
                    int size = file.ContentLength;
                    try
                    {
                      
                        file.SaveAs(Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName));

                        if (file.InputStream.CanWrite == false)
                        {
                            string excelPathCopy = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), newFileName);
                            System.IO.File.Copy(Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName), excelPathCopy);
                            if (System.IO.File.Exists(Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName)))
                            {
                                System.IO.File.Delete(Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName));

                            }
                            fileName = newFileName;
                        }
                        FilePath = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName);
                        FilePath = FilePath + "," + CheckImportSheet(FilePath);
                    }
                    catch (Exception ex)
                    {
                        Message = "File upload failed, please try again";
                    }

                }
                return Json(FilePath, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objex)
            {
                _logger.Error("Error at ImportController : SaveFiles", objex);
                return null;
            }
        }

        public JsonResult SaveFilesTemplate(string XMLName, HttpPostedFileBase fileDetails)
        {
            try
            {
                string fileName, actualFileName;
                string FilePath = string.Empty;
                Message = fileName = actualFileName = string.Empty;
                bool flag = false;
                if (XMLName != null)
                {
                    var file = XMLName;
                    actualFileName = Path.GetFileName(file);
                    fileName = Guid.NewGuid() + Path.GetExtension(actualFileName);
                    //int size = file.ContentLength;
                    try
                    {
                        fileDetails.SaveAs(Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName));
                        FilePath = Path.Combine(Server.MapPath("~/Views/App/ImportSheets"), fileName);
                    }
                    catch (Exception)
                    {
                        Message = "File upload failed, please try again";
                    }

                }
                return Json(FilePath, JsonRequestBehavior.AllowGet);
            }
            catch (Exception objex)
            {
                _logger.Error("Error at ImportController : SaveFiles", objex);
                return null;
            }
        }

        public DataTable getSheetData(string strConn, string sheet)
        {
            try
            {
                string query = "select * from [" + sheet + "]";
                OleDbConnection objConn;
                OleDbDataAdapter oleDA;
                DataTable dt = new DataTable();
                objConn = new OleDbConnection(strConn);
                objConn.Open();
                oleDA = new OleDbDataAdapter(query, objConn);
                oleDA.Fill(dt);
                objConn.Close();
                oleDA.Dispose();
                objConn.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportController : getSheetData", ex);
                return null;
            }
        }

        public DataTable getSheetData1(string strConn, string sheet)
        {
            try
            {
                string query = "select * from [" + sheet + "]";
                OleDbConnection objConn;
                OleDbDataAdapter oleDA;
                DataTable dt = new DataTable();
                objConn = new OleDbConnection(strConn);
                objConn.Open();
                oleDA = new OleDbDataAdapter(query, objConn);
                oleDA.Fill(dt);
                objConn.Close();
                oleDA.Dispose();
                objConn.Dispose();
                return dt;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportController : getSheetData1", ex);
                return null;
            }
        }


        public void deletefile(string SheetPath)
        {
            try
            {
                if (System.IO.File.Exists(SheetPath))
                {
                    System.IO.File.Delete(SheetPath);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportController:  DeleteFile", ex);
            }
        }

        //[System.Web.Http.HttpPost]
        //public JsonResult finishImport(string SheetName, string allowDuplicate, string excelPath)
        //{
        //    string importTemp, SQLString = string.Empty;
        //    importTemp = Guid.NewGuid().ToString();
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        var oattType = new DataTable();


        //        int ItemVal = Convert.ToInt32(allowDuplicate);
        //        using (var conn = new SqlConnection(connectionString))
        //        {

        //            conn.Open();
        //            SQLString = "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" + importTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + importTemp + "] END')";
        //            importExcelSheetSelection(excelPath, SheetName);
        //            SqlCommand _DBCommand = new SqlCommand(SQLString, conn);
        //            _DBCommand.ExecuteNonQuery();
        //            SQLString = CreateTable("[##IMPORTTEMP" + importTemp + "]", excelPath, SheetName);
        //            _DBCommand = new SqlCommand(SQLString, conn);
        //            _DBCommand.ExecuteNonQuery();
        //            var bulkCopy = new SqlBulkCopy(conn)
        //            {
        //                DestinationTableName = "[##IMPORTTEMP" + importTemp + "]"
        //            };
        //            bulkCopy.WriteToServer(objdatatable);
        //            JArray model = null;
        //            oattType = SelectedColumnsToImport(objdatatable, model);
        //            var cmd1 = new SqlCommand("ALTER TABLE [##IMPORTTEMP" + importTemp + "] ADD Family_Status nvarchar(10),FStatusUI nvarchar(10),StatusUI nvarchar(10),SFSort_Order int,Sort_Order int", conn);
        //            cmd1.ExecuteNonQuery();
        //            var sqlstring1 = new SqlCommand("EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##AttributeTemp" + importTemp + "]'')BEGIN DROP TABLE [##AttributeTemp" + importTemp + "] END')", conn);
        //            sqlstring1.ExecuteNonQuery();
        //            var cmd2 = new SqlCommand();
        //            cmd2.Connection = conn;
        //            SQLString = CreateTableToImport("[##AttributeTemp" + importTemp + "]", oattType);
        //            cmd2.CommandText = SQLString;
        //            cmd2.CommandType = CommandType.Text;
        //            cmd2.ExecuteNonQuery();
        //            bulkCopy.DestinationTableName = "[##AttributeTemp" + importTemp + "]";
        //            bulkCopy.WriteToServer(oattType);
        //            var cmd = new SqlCommand("EXEC('STP_CATALOGSTUDIO5_IMPORT ''" + importTemp + "'',''" + ItemVal + "''')", conn) { CommandTimeout = 0 };
        //            cmd.ExecuteNonQuery();
        //            _SQLString = @"select * from [##LOGTEMPTABLE" + importTemp + "]";
        //            ds = CreateDataSet();
        //            _SQLString = @"select * into [tempresult" + importTemp + "] from [##LOGTEMPTABLE" + importTemp + "]";
        //            cmd1.CommandText = _SQLString;
        //            cmd1.CommandType = CommandType.Text;
        //            cmd1.ExecuteNonQuery();
        //            if (ds.Tables[0].Columns[0].ColumnName == "ErrorMessage")
        //            {
        //                return Json("Import Failed~" + importTemp, JsonRequestBehavior.AllowGet);

        //            }
        //            else
        //            {
        //                var custmorids = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name);
        //                if (custmorids.Any())
        //                {
        //                    var firstOrDefault = custmorids.FirstOrDefault();
        //                    if (firstOrDefault != null)
        //                    {
        //                        int customerid = firstOrDefault.CustomerId;
        //                        var catalogids = objLS.TB_CATALOG.Max(x => x.CATALOG_ID);
        //                        var objcustomercatalogcount =
        //                            objLS.Customer_Catalog.Where(
        //                                x => x.CATALOG_ID == catalogids && x.CustomerId == customerid);
        //                        if (!objcustomercatalogcount.Any())
        //                        {
        //                            var objcustomercatalog = new Customer_Catalog
        //                            {
        //                                CustomerId = customerid,
        //                                CATALOG_ID = catalogids,
        //                                DateCreated = DateTime.Now,
        //                                DateUpdated = DateTime.Now
        //                            };
        //                            objLS.Customer_Catalog.Add(objcustomercatalog);
        //                            objLS.SaveChanges();
        //                        }
        //                    }
        //                }
        //                return Json("Import Success~" + importTemp, JsonRequestBehavior.AllowGet);
        //            }

        //        }


        //    }

        //    catch (Exception ex)
        //    {
        //        Message = "Import failed! Please try again";
        //        _logger.Error("Error at ImportController : FinishImport", ex);
        //        return Json("Import Failed~" + importTemp, JsonRequestBehavior.AllowGet);
        //    }

        //}
        public DataTable SelectedColumnsToImportTemplate(DataTable objdatatable, DataTable model)
        {
            try
            {
                //var functionAlloweditems = model.ToArray().Select()

                DataTable dt = new DataTable();
                dt.Columns.Add("ATTRIBUTE_TYPE");
                dt.Columns.Add("ATTRIBUTE_NAME");

                foreach (DataRow item in model.Rows)
                {
                    if (item["Selected2Import"].ToString() == "True")
                    {
                        DataRow dr = dt.NewRow();
                        dr["ATTRIBUTE_TYPE"] = item["FieldType"];
                        dr["ATTRIBUTE_NAME"] = item["ExcelColumn"];
                        dt.Rows.Add(dr);
                    }
                    //{
                    //    DataRow dr = dt.NewRow();
                    //    dr["ATTRIBUTE_TYPE"] = item.FieldType;
                    //    dr["ATTRIBUTE_NAME"] = item.ExcelColumn;
                    //    dt.Rows.Add(dr);
                    //}
                }
                return dt;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at SelectedColumnsToImport : exceldata", ex);
                return null;
            }
        }
        public DataTable SelectedColumnsToImport(DataTable objdatatable, JArray model)
        {
            try
            {
                var functionAlloweditems = ((JArray)model).Select(x => new SelectedAttribute()
                {
                    ExcelColumn = (string)x["ExcelColumn"],
                    FieldType = (string)x["FieldType"],
                    SelectedToImport = x["SelectedToImport"].ToString().ToLower() == "true" ? true : false
                }).ToList();

                DataTable dt = new DataTable();
                dt.Columns.Add("ATTRIBUTE_TYPE");
                dt.Columns.Add("ATTRIBUTE_NAME");
                var tbattrcol = objLS.TB_ATTRIBUTE.Find(1);
                foreach (var item in functionAlloweditems)
                {
                    if (item.SelectedToImport)
                    {
                        if (item.ExcelColumn != System.Web.HttpContext.Current.Session["CustomerItemNo"].ToString() && item.ExcelColumn != "ITEM#")
                        {
                            DataRow dr = dt.NewRow();
                            dr["ATTRIBUTE_TYPE"] = item.FieldType;
                            dr["ATTRIBUTE_NAME"] = item.ExcelColumn;
                            dt.Rows.Add(dr);
                        }
                        else
                        {
                            DataRow dr = dt.NewRow();
                            dr["ATTRIBUTE_TYPE"] = item.FieldType;
                            dr["ATTRIBUTE_NAME"] = "CATALOG_ITEM_NO";
                            dt.Rows.Add(dr);
                        }
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at SelectedColumnsToImport : exceldata", ex);
                return null;
            }
        }

        public string CreateTableToImport(string tableName, DataTable table)
        {
            try
            {
                string sqlsc = "CREATE TABLE " + tableName + "(";
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    if (!table.Columns[i].ColumnName.Contains('['))
                    {
                        sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                    }
                    else
                    {
                        sqlsc += "\n" + table.Columns[i].ColumnName + "";
                    }
                    if (table.Columns[i].ColumnName.Contains("CATEGORY_ID"))
                        sqlsc += "nvarchar(50) ";
                    else if (table.Columns[i].DataType.ToString().Contains("System.String") ||
                             table.Columns[i].ColumnName.Contains("SUBCATID") ||
                             table.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                        sqlsc += "nvarchar(max) ";
                    else
                        sqlsc += "nvarchar(max) ";
                    sqlsc += ",";
                }
                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CreateTableToImport : exceldata", ex);
                return null;
            }
        }

        public DataTable exceldata(string filePath, string SheetName)
         {
            try
            {
                DataTable dtexcel = new DataTable();
                DataTable dvt = new DataTable();
                QueryValues queryValues = new QueryValues();
                bool hasHeaders = true;
                string HDR = hasHeaders ? "Yes" : "No";
                if (!SheetName.Contains("$"))
                { SheetName = SheetName + "$"; }
                string strConn;
                if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                else
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                OleDbConnection conn = new OleDbConnection(strConn);
                conn.Open();
                DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });

                DataRow schemaRow = schemaTable.Rows[0];
                Workbook workbook = Workbook.Load(filePath);
                string columnFields = "";
                string sheetNameworkbook = SheetName.Replace("$", "").TrimEnd();
                foreach (var columns in workbook.Worksheets[sheetNameworkbook].Rows[0].Cells)
                {
                    columnFields = columnFields + "[" + columns.Value.ToString().Replace(".", "#") + "],";
                }
                 columnFields = columnFields.TrimEnd(',');
              
                string sheet = schemaRow["TABLE_NAME"].ToString();
                if (!sheet.EndsWith("_"))
                {
                    if (SheetName=="SubProducts$")
                    {
                        OleDbCommand cmd = new OleDbCommand(@"SELECT " + columnFields + " FROM [" + SheetName + "]", conn);
                        OleDbDataAdapter daexcel = new OleDbDataAdapter(cmd);                       
                        dtexcel.Locale = CultureInfo.CurrentCulture;                       
                        daexcel.Fill(dtexcel);
                    }
                    else
                    {
                        OleDbCommand cmd = new OleDbCommand(@"SELECT * FROM [" + SheetName + "]", conn);
                        OleDbDataAdapter daexcel = new OleDbDataAdapter(cmd);                     
                        dtexcel.Locale = CultureInfo.CurrentCulture;                    
                        daexcel.Fill(dtexcel);
                    }

                }
                conn.Close();
                conn.Dispose();
                // Workbook workbook = Workbook.Load(filePath);

                int iCols = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells.Count();

                var tbattrcol = objLS.TB_ATTRIBUTE.Find(1);
                for (int i = 0; i < dtexcel.Columns.Count; i++)
                {
                    dtexcel.Columns[i].ColumnName = workbook.Worksheets[sheetNameworkbook].Rows[1].Cells[i].Value.ToString();
                }

                // _dsSheets.Tables.Add(dt);
                for (int i = 0; i < dtexcel.Columns.Count; i++)
                {
                    if (workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[i].Value != null)
                    {
                        String sColName = workbook.Worksheets[sheetNameworkbook].Rows[0].Cells[i].Value.ToString().Replace('[', ' ').Replace(']', ' ').Trim();
                        //if (sColName == tbattrcol.ATTRIBUTE_NAME)  


                        if(System.Web.HttpContext.Current.Session["CustomerItemNo"]==null)
                        {
                            var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                            int customerId;
                            int.TryParse(customerDetails.ToString(), out customerId);
                            string customerCatalog = queryValues.GetCatalogItemNo(customerId);
                            System.Web.HttpContext.Current.Session["CustomerItemNo"] = customerCatalog;
                        }

                        if (sColName == System.Web.HttpContext.Current.Session["CustomerItemNo"].ToString()) 
                        {
                            dtexcel.Columns[i].ColumnName = "CATALOG_ITEM_NO";
                        }
                        else if (dtexcel.Columns[i].ColumnName != sColName)
                        {
                            dtexcel.Columns[i].ColumnName = sColName;
                        }
                    }


                }
                dtexcel.AcceptChanges();

                // CustomerSubItemNo
                if (System.Web.HttpContext.Current.Session["CustomerSubItemNo"] == null)
                {
                    var customerDetails = objLS.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                    int customerId;
                    int.TryParse(customerDetails.ToString(), out customerId);
                    string customerCatalog = queryValues.GetSubCatalogItemNo(customerId);
                    System.Web.HttpContext.Current.Session["CustomerSubItemNo"] = customerCatalog;
                }


                if (dtexcel.Columns.Contains(System.Web.HttpContext.Current.Session["CustomerSubItemNo"].ToString()))
                {
                    dtexcel.Columns[System.Web.HttpContext.Current.Session["CustomerSubItemNo"].ToString()].ColumnName = "SUBITEM#";
                } 

                return dtexcel;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportController : exceldata", ex);
                return null;
            }
        }

        public string CreateTable(string tableName, string excelPath, string SheetName, DataTable objtable)
        {
            try
            {

            
               // string CustomerItemValue = System.Web.HttpContext.Current.Session["CustomerItemNo"].ToString();

                if (objtable.Columns.Contains(System.Web.HttpContext.Current.Session["CustomerItemNo"].ToString()))
                {
                    objtable.Columns[System.Web.HttpContext.Current.Session["CustomerItemNo"].ToString()].ColumnName = "CATALOG_ITEM_NO";
                }
                if (objtable.Columns.Contains("SUBITEM#"))
                {
                    objtable.Columns["SUBITEM#"].ColumnName = "SUBCATALOG_ITEM_NO";
                } 
                //if (!SheetName.Contains("$"))
                //{ SheetName = SheetName + "$"; }
                // objdatatable = exceldata(excelPath, SheetName);
                string sqlsc = "CREATE TABLE " + tableName + "(";
                if (objtable.Columns.Count > 0)
                {
                    for (int i = 0; i < objtable.Columns.Count; i++)
                    {
                        if (!objtable.Columns[i].ColumnName.Contains('['))
                        {
                            sqlsc += "\n [" + objtable.Columns[i].ColumnName + "] ";
                        }
                        else
                        {
                            sqlsc += "\n" + objtable.Columns[i].ColumnName + "";
                        }
                        if (objtable.Columns[i].ColumnName.Contains("CATEGORY_ID"))
                            sqlsc += "nvarchar(50) ";
                        else if (objtable.Columns[i].DataType.ToString().Contains("System.String") ||
                                 objtable.Columns[i].ColumnName.Contains("SUBCATID") ||
                                 objtable.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                            sqlsc += "varchar(8000) ";
                        else
                            sqlsc += "varchar(8000) ";
                        sqlsc += ",";
                    }
                }
                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportController : CreateTable", ex);
                return null;
            }
        }


        public string CreateTableImport(string tableName, string excelPath, string SheetName, DataTable objtable)
        {
            try
            {
                //if (!SheetName.Contains("$"))
                //{ SheetName = SheetName + "$"; }
                // objdatatable = exceldata(excelPath, SheetName);
                string sqlsc = "CREATE TABLE " + tableName + "(";
                for (int i = 0; i < objtable.Columns.Count; i++)
                {
                    if (!objtable.Columns[i].ColumnName.Contains('['))
                    {
                        sqlsc += "\n [" + objtable.Columns[i].ColumnName + "] ";
                    }
                    else
                    {
                        sqlsc += "\n" + objtable.Columns[i].ColumnName + "";
                    }
                    if (objtable.Columns[i].ColumnName.Contains("CATEGORY_ID"))
                        sqlsc += "nvarchar(50) ";
                    else if (objtable.Columns[i].DataType.ToString().Contains("System.String") ||
                             objtable.Columns[i].ColumnName.Contains("SUBCATID") ||
                             objtable.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                        sqlsc += "nvarchar(max) ";
                    else
                        sqlsc += "nvarchar(max) ";
                    sqlsc += ",";
                }
                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportController : CreateTable", ex);
                return null;
            }
        }

        public DataSet CreateDataSet()
        {
            var dsReturn = new DataSet();
            using (var objSqlConnection = new SqlConnection(connectionString))
            using (var conn = new SqlConnection(connectionString))
            {
                var dbAdapter = new SqlDataAdapter(_SQLString, objSqlConnection);
                dbAdapter.SelectCommand.CommandTimeout = 0;
                dbAdapter.Fill(dsReturn);
                dbAdapter.Dispose();
                return dsReturn;
            }
        }

        public DataTable BasicImportExcelSheetSelection(string excelPath, string SheetName)
        {
            var dt = new DataTable();
            DataTable dtexcel = new DataTable();
            List<object> newList = new List<object>();
            try
            {
                if (!string.IsNullOrEmpty(excelPath))
                {
                    bool hasHeaders = true;
                    string HDR = hasHeaders ? "Yes" : "No";
                    string strConn;
                    if (excelPath.Substring(excelPath.LastIndexOf('.')).ToLower() == ".xlsx")
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                                  ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    else
                        //strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelPath +
                        //          ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=1\"";
                        strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath +
                                 ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=1\"";
                    OleDbConnection conn = new OleDbConnection(strConn);
                    conn.Open();
                    DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,
                        new object[] { null, null, null, "TABLE" });
                    DataRow schemaRow = schemaTable.Rows[0];
                    string sheet = schemaRow["TABLE_NAME"].ToString();
                    if (!sheet.EndsWith("_"))
                    {
                        string query = "SELECT  * FROM [" + SheetName + "$]";
                        OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                        dtexcel.Locale = CultureInfo.CurrentCulture;
                        daexcel.Fill(dtexcel);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at ImportController : CreateTable", ex);
                return null;
            }
            return dtexcel;
        }

        public string _parentCategoryId = "0";
        public string ParentCategoryId
        {
            get
            {
                return _parentCategoryId;
            }
            set
            {
                _parentCategoryId = value;
            }
        }

        public DataTable SelectedFamilyColumnsToImport(DataTable objdatatable, JArray model)
        {
            try
            {
                var functionAlloweditems = ((JArray)model).Select(x => new SelectedAttribute()
                {
                    ExcelColumn = (string)x["ExcelColumn"],
                    FieldType = (string)x["FieldType"],
                    SelectedToImport = x["SelectedToImport"].ToString().ToLower() == "true" ? true : false
                }).ToList();

                DataTable dt = new DataTable();
                dt.Columns.Add("ATTRIBUTE_TYPE");
                dt.Columns.Add("ATTRIBUTE_NAME");
                var tbattrcol = objLS.TB_ATTRIBUTE.Find(1);
                foreach (var item in functionAlloweditems)
                {
                    if (item.SelectedToImport)
                    {
                        if (item.ExcelColumn != tbattrcol.ATTRIBUTE_NAME)
                        {
                            DataRow dr = dt.NewRow();
                            dr["ATTRIBUTE_TYPE"] = item.FieldType;
                            dr["ATTRIBUTE_NAME"] = item.ExcelColumn;
                            dt.Rows.Add(dr);
                        }
                        else
                        {
                            DataRow dr = dt.NewRow();
                            dr["ATTRIBUTE_TYPE"] = item.FieldType;
                            dr["ATTRIBUTE_NAME"] = "FAMILY_NAME";
                            dt.Rows.Add(dr);
                        }
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at SelectedColumnsToImport : exceldata", ex);
                return null;
            }
        }

    }
}

