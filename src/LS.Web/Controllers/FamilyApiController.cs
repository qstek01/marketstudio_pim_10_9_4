﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using LS.Data;
using LS.Data.Model.CatalogSectionModels;
using log4net;
using LS.Data.Model;
using System.Web.Http;
using System.Collections;
using System.Net.Http;
using System.Net;
using LS.Data.Utilities;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Kendo.DynamicLinq;
using System.Web;
using LS.Data.Model.ProductPreview;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.Security;
using LS.Data.Model.Accounts;
using LS.Web.Resources;
using System.Web.Mvc;
using Dapper;
using LS.Web.Models;

namespace LS.Web.Controllers
{
    public class FamilyApiController : ApiController
    {
        // GET: Family
        static readonly ILog Logger = LogManager.GetLogger(typeof(FamilyApiController));
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;

        readonly CSEntities _objLs = new CSEntities();
        private string _sqlString = "";
        private int _gaattrid;
        public int customerId_ = 0;
        public class RootObject
        {
            public int FamilyId { get; set; }
            public string AttributeName { get; set; }
            public string VALUE { get; set; }
            public int AttrId { get; set; }
        }

        [System.Web.Http.HttpGet]
        public IList<CatalogSectionModels> Getprodspecs(string id)
        {
            try
            {
                int familyId;
                if (id.Contains("~"))
                {
                    int.TryParse(id.Trim('~'), out familyId);
                    return (from x in new CSEntities().STP_CATALOGSTUDIO5_ProductTablekendo(familyId, Convert.ToInt32(id)) select x).ToList().Select(i => new CatalogSectionModels
                    {
                        // if(dt==null)
                        CATALOG_ID = i.FAMILY_ID,
                        CATEGORY_ID = i.Part_No_,
                        SORT_ORDER = i.SORTORDER,
                        Description = i.Description,
                        FAMILY_ID = i.FAMILY_ID,
                        PRODUCT_ID = i.PRODUCT_ID,
                        PUBLISH = i.PUBLISH,
                        Part_No_ = i.Part_No_,
                        Size = i.Size,
                        Qty_ = i.Qty_
                    }).ToList();
                }
                //return dt;

            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetProdSpecs", ex);
                return null;
            }
            return null;
        }

        [System.Web.Http.HttpGet]
        public IList<Family> Getfamilyspecs(string id, int catalogId)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    int familyId;
                    int.TryParse(id.Trim('~'), out familyId);
                    var lx1 = _objLs.STP_LS_FAMILY_SPECS(familyId, catalogId, 7).Distinct().ToList();

                    var lx = lx1.Select(i => new Family
                    {
                        FAMILY_ID = i.FAMILY_ID,
                        ATTRIBUTE_NAME = i.ATTRIBUTE_NAME,
                        ATTR_VALUE = Convert.ToString(i.VALUE),
                        ATTR_ID = i.ATTR_ID
                    }).ToList();
                    return lx;
                }
                else
                {
                    var objFamilies = new List<Family>();
                    return objFamilies;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: Getfamilyspecs", ex);
                return null;
            }
            // return null;
        }

        [System.Web.Http.HttpGet]
        public IList<Family> GetfamilyspecsAttr(string id)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    int familyId;
                    int.TryParse(id.Trim('~'), out familyId);
                    var lx1 = _objLs.STP_LS_FAMILY_SPECS(familyId, 3, 7).Distinct().ToList();
                    var lx = lx1.Select(i => new Family
                    {
                        FAMILY_ID = i.FAMILY_ID,
                        ATTRIBUTE_NAME = i.ATTRIBUTE_NAME,
                        ATTR_VALUE = Convert.ToString(i.VALUE),
                        ATTR_ID = i.ATTR_ID
                    }).ToList();
                    return lx;
                }
                else
                {
                    var objFamilies = new List<Family>();
                    return objFamilies;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetfamilyspecsAttr", ex);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public IList<Family> GetfamilyspecsImgAttr(string id, int catalogId)
        {
            try
            {
                if (!string.IsNullOrEmpty(id))
                {
                    int familyId;
                    int.TryParse(id.Trim('~'), out familyId);
                    var lx1 = _objLs.STP_LS_FAMILY_SPECS(familyId, catalogId, 9).Distinct().ToList();
                    var lx = lx1.Select(i => new Family
                    {
                        FAMILY_ID = i.FAMILY_ID,
                        ATTRIBUTE_NAME = i.ATTRIBUTE_NAME,
                        ATTR_VALUE = Convert.ToString(i.VALUE),
                        ATTR_ID = i.ATTR_ID
                    }).ToList();
                    return lx;
                }
                else
                {
                    var objFamilies = new List<Family>();
                    return objFamilies;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetfamilyspecsImgAttr", ex);
                return null;
            }
            // return null;
        }

        [System.Web.Http.HttpGet]
        public IList GetRelatedfamily(string id, string catalogId)
        {
            try
            {
                Logger.Info("Inside  at  FamilyApiController: GetRelatedfamily");
                int familyIdInt;
                int catalogid;
                int.TryParse(id.Trim('~'), out familyIdInt);
                int.TryParse(catalogId.Trim('~'), out catalogid);
                var sadf = (from x in new CSEntities().STP_LS_FAMILY_GET_RELATED_FAMILY(familyIdInt, catalogid) select x).OrderBy(x => x.SORT_ORDER).ToList();
                return sadf;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetRelatedfamily", ex);
                return null;
            }
            // return null;
        }


        [System.Web.Http.HttpPost]
        public DataSourceResult GetRelatedfamilyDetails(string id, string catalogId, string categoryid, DataSourceRequest request)
        {

            if (request.Sort == null || !request.Sort.Any())
            {
                request.Sort = new List<Sort> { new Sort { Field = "SORT_ORDER", Dir = "asc" } };
            }
            try
            {
                int familyIdInt;
                int catalogids;
                string category_id = "0";
                if (id == null)
                    return null;
                int.TryParse(id.Trim('~'), out familyIdInt);
                int.TryParse(catalogId.Trim('~'), out catalogids);
                var catid = categoryid.Split('~');
                if (catid.Length > 0)
                {
                    category_id = catid[0];
                }
                var objSubFamily = _objLs.TB_SUBFAMILY.Where(a => a.SUBFAMILY_ID == familyIdInt);
                if (objSubFamily.Count() == 1)
                {

                    var objProjectSectionss =
                             _objLs.TB_SUBFAMILY.Join(_objLs.TB_FAMILY, tf => tf.FAMILY_ID, tsf => tsf.FAMILY_ID,
                                 (tf, tsf) => new { tf, tsf })
                                 .Join(_objLs.TB_CATALOG_FAMILY, tcatf => tcatf.tsf.FAMILY_ID, tcp => tcp.FAMILY_ID,
                                     (tcatf, tcf) => new { tcatf, tcf }).Where(x =>
                                         (x.tcatf.tf.SUBFAMILY_ID == familyIdInt && x.tcf.CATALOG_ID == catalogids && x.tcf.CATEGORY_ID == category_id ))
                                 .Select(z => new
                                 {
                                     z.tcatf.tsf.FAMILY_NAME,
                                     PARENT_FAMILY_ID = 0,
                                     z.tcatf.tf.FAMILY_ID,
                                     z.tcatf.tf.SORT_ORDER,
                                     z.tcf.TB_CATEGORY.CATEGORY_NAME,
                                     z.tcf.TB_CATEGORY.CATEGORY_ID
                                 }).OrderBy(y => y.SORT_ORDER).Distinct();
                    return objProjectSectionss.ToDataSourceResult(request);


                }

                else
                {
                    //  Logger.Info("Inside  at  FamilyApiController: GetRelatedfamily");

                    //var objProjectSectionss = (from x in new CSEntities().STP_LS_FAMILY_GET_RELATED_FAMILY(familyIdInt, catalogid) select x).Distinct();

                    //return objProjectSectionss.AsQueryable().ToDataSourceResult(request);
                    var objProjectSectionss =
                            _objLs.TB_SUBFAMILY.Join(_objLs.TB_FAMILY, tf => tf.SUBFAMILY_ID, tsf => tsf.FAMILY_ID,
                                (tf, tsf) => new { tf, tsf })
                                .Join(_objLs.TB_CATALOG_FAMILY, tcatf => tcatf.tsf.FAMILY_ID, tcp => tcp.FAMILY_ID,
                                    (tcatf, tcf) => new { tcatf, tcf }).Where(x =>
                                        (x.tcatf.tf.FAMILY_ID == familyIdInt && x.tcf.CATALOG_ID == catalogids && x.tcf.CATEGORY_ID == category_id && x.tcf.FLAG_RECYCLE == "A"))
                                .Select(z => new
                                {
                                    z.tcatf.tsf.FAMILY_NAME,
                                    PARENT_FAMILY_ID = z.tcatf.tf.FAMILY_ID,
                                    FAMILY_ID = z.tcatf.tf.SUBFAMILY_ID,
                                    z.tcatf.tf.SORT_ORDER,
                                    CATEGORY_NAME = _objLs.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == z.tcf.CATEGORY_ID && tc.CUSTOMER_ID == (_objLs.Customer_User.FirstOrDefault(a => a.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.CATEGORY_NAME).FirstOrDefault(),
                                    CATEGORY_ID = _objLs.TB_CATEGORY.Where(tc => tc.CATEGORY_ID == z.tcf.CATEGORY_ID && tc.CUSTOMER_ID == (_objLs.Customer_User.FirstOrDefault(a => a.User_Name == User.Identity.Name)).CustomerId).Select(tcsel => tcsel.CATEGORY_ID).FirstOrDefault()

                                }).OrderBy(y => y.SORT_ORDER).Distinct();
                    return objProjectSectionss.ToDataSourceResult(request);

                }

            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetRelatedfamily", ex);
                return null;
            }
        }

        [System.Web.Http.HttpGet]
        public IList GetUsers(int CustomerId,int filter)
        {
            try
            {
                // return (from x in new CSEntities().GET_USER_LIST_VIEW select x).ToList();
                var userList = _objLs.STP_LS_GET_CUSTOMER_USERS_BY_LOGGEDIN_USER(User.Identity.Name, CustomerId, filter).GroupBy(f => f.USER_NAME).ToList();
                List<STP_LS_GET_CUSTOMER_USERS_BY_LOGGEDIN_USER_Result> users = new List<STP_LS_GET_CUSTOMER_USERS_BY_LOGGEDIN_USER_Result>();
                foreach (var item in userList)
                {
                    List<string> groupList = new List<string>();
                    foreach (var val in item)
                    {
                        groupList.Add(val.ROLE_NAME);
                    }
                    var user = item.First();
                    user.ROLE_NAME = string.Join(", ", groupList);
                    users.Add(user);
                }
               
            
                return users;
               


                //var lx = (from x in new CSEntities().STP_LS_GET_USER_LIST() select x).ToList();
                //return lx;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetUsers", ex);
                return null;
            }
            // return null;
        }

        [System.Web.Http.HttpGet]
        public IList getMyprofileList(int Customerid)
        {
            try
            {
                if (Customerid == 1)
                {
                    var myProfile =
                      _objLs.Customer_User.Join(_objLs.aspnet_Users, tps => tps.User_Name,
                          tpf => tpf.UserName, (tps, tpf) => new { tps, tpf })
                          .Join(_objLs.vw_aspnet_UsersInRoles, tcptps => tcptps.tpf.UserId, tcp => tcp.UserId,
                              (tcptps, tcp) => new { tcptps, tcp })
                          .Join(_objLs.aspnet_Roles, tcatcp => tcatcp.tcp.RoleId, tca => tca.RoleId,
                              (tcatcp, tca) => new { tcatcp, tca })
                          .Join(_objLs.Customers, tpfatpf => tpfatpf.tcatcp.tcptps.tps.CustomerId,
                              tpfa => tpfa.CustomerId, (tpfatpf, tpfa) => new { tpfatpf, tpfa })
                          .Where(x => x.tpfatpf.tcatcp.tcptps.tps.CustomerId != 0).Select(a => new MyProfile
                          {
                              CustomerId = a.tpfatpf.tcatcp.tcptps.tps.CustomerId,
                              CustomerName = a.tpfatpf.tcatcp.tcptps.tps.User_Name,
                              RoleName = a.tpfatpf.tca.RoleName,
                              Active = a.tpfa.IsActive
                          }).ToList();

                    return myProfile;
                }
                else
                {
                    var CustomerAdminCheck = _objLs.vw_aspnet_UsersInRoles
                       .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                       .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                       .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 2).ToList();
                    if (CustomerAdminCheck.Any())
                    {
                        var myProfile =
                             _objLs.Customer_User.Join(_objLs.aspnet_Users, tps => tps.User_Name,
                                 tpf => tpf.UserName, (tps, tpf) => new { tps, tpf })
                                 .Join(_objLs.vw_aspnet_UsersInRoles, tcptps => tcptps.tpf.UserId, tcp => tcp.UserId,
                                     (tcptps, tcp) => new { tcptps, tcp })
                                 .Join(_objLs.aspnet_Roles, tcatcp => tcatcp.tcp.RoleId, tca => tca.RoleId,
                                     (tcatcp, tca) => new { tcatcp, tca })
                                 .Join(_objLs.Customers, tpfatpf => tpfatpf.tcatcp.tcptps.tps.CustomerId,
                                     tpfa => tpfa.CustomerId, (tpfatpf, tpfa) => new { tpfatpf, tpfa })
                                 .Where(x => x.tpfatpf.tcatcp.tcptps.tps.CustomerId == Customerid).Select(a => new MyProfile
                                 {
                                     CustomerId = a.tpfatpf.tcatcp.tcptps.tps.CustomerId,
                                     CustomerName = a.tpfatpf.tcatcp.tcptps.tps.User_Name,
                                     RoleName = a.tpfatpf.tca.RoleName,
                                     Active = a.tpfa.IsActive
                                 }).ToList();

                        return myProfile;
                    }
                    else
                    {
                        var myProfile =
                                 _objLs.Customer_User.Join(_objLs.aspnet_Users, tps => tps.User_Name,
                                     tpf => tpf.UserName, (tps, tpf) => new { tps, tpf })
                                     .Join(_objLs.vw_aspnet_UsersInRoles, tcptps => tcptps.tpf.UserId, tcp => tcp.UserId,
                                         (tcptps, tcp) => new { tcptps, tcp })
                                     .Join(_objLs.aspnet_Roles, tcatcp => tcatcp.tcp.RoleId, tca => tca.RoleId,
                                         (tcatcp, tca) => new { tcatcp, tca })
                                     .Join(_objLs.Customers, tpfatpf => tpfatpf.tcatcp.tcptps.tps.CustomerId,
                                         tpfa => tpfa.CustomerId, (tpfatpf, tpfa) => new { tpfatpf, tpfa })
                                     .Where(x => x.tpfatpf.tcatcp.tcptps.tps.CustomerId == Customerid && x.tpfatpf.tcatcp.tcptps.tps.User_Name == User.Identity.Name).Select(a => new MyProfile
                                     {
                                         CustomerId = a.tpfatpf.tcatcp.tcptps.tps.CustomerId,
                                         CustomerName = a.tpfatpf.tcatcp.tcptps.tps.User_Name,
                                         RoleName = a.tpfatpf.tca.RoleName,
                                         Active = a.tpfa.IsActive
                                     }).ToList();

                        return myProfile;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: getMyprofileList", ex);
                return null;
            }
            // return null;
        }

        [System.Web.Http.HttpGet]
        public IList getMyprofileListStatus(string username)
        {
            try
            {
                var myProfile =
                  _objLs.Customer_User.Join(_objLs.aspnet_Users, tps => tps.User_Name,
                      tpf => tpf.UserName, (tps, tpf) => new { tps, tpf })
                      .Join(_objLs.vw_aspnet_UsersInRoles, tcptps => tcptps.tpf.UserId, tcp => tcp.UserId,
                          (tcptps, tcp) => new { tcptps, tcp })
                      .Join(_objLs.aspnet_Roles, tcatcp => tcatcp.tcp.RoleId, tca => tca.RoleId,
                          (tcatcp, tca) => new { tcatcp, tca })
                      .Join(_objLs.Customers, tpfatpf => tpfatpf.tcatcp.tcptps.tps.CustomerId,
                          tpfa => tpfa.CustomerId, (tpfatpf, tpfa) => new { tpfatpf, tpfa })
                      .Where(x => x.tpfatpf.tcatcp.tcptps.tps.User_Name == username).Select(a => new MyProfile
                      {
                          CustomerId = a.tpfatpf.tcatcp.tcptps.tps.CustomerId,
                          CustomerName = a.tpfatpf.tcatcp.tcptps.tps.User_Name,
                          RoleName = a.tpfatpf.tca.RoleName,
                          Active = a.tpfatpf.tcatcp.tcptps.tpf.IsAnonymous ? false : true
                      }).ToList();

                return myProfile;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: getMyprofileListStatus", ex);
                return null;
            }
            // return null;
        }


        [System.Web.Http.HttpGet]
        public IList GetUsersFordropdown()
        {
            try
            {
                int customer_id = 0;
                var customerList = _objLs.Customers.Join(_objLs.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name && z.a.CompanyName.ToUpper() != "STUDIOSOFT");
                var custlist = customerList.Select(x => new { x.a.CompanyName, x.a.CustomerId }).ToList();
                if (custlist.Any())
                {
                    customer_id = custlist.Select(a => a.CustomerId).FirstOrDefault();
                }

                List<STP_LS_GET_USER_LIST_Result> lx = (from x in new CSEntities().STP_LS_GET_USER_LIST(User.Identity.Name, customer_id) select x).ToList();

                return lx;


            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetUsers", ex);
                return null;
            }
            // return null;
        }

        [System.Web.Http.HttpGet]
        public IList GetUsersForWorkFlow()
        {
            try
            {
                //var workflowUsers1 = _objLs.TB_USER.Join(_objLs.TB_USER_ROLE, a => a.TB_USER_ID, b => b.TB_USER_ID, (a, b) => new { a, b })
                //     .Where(x => x.a.STATUS == "Active").OrderBy(x => x.a.TB_USER_ID)
                //    .Select(x => new
                //     {
                //         checkbxUserSel = x.a.TB_USER_ID == User.Identity.Name ? 1 : 0,
                //         x.a.TB_USER_ID
                //     }).ToList();
                var workflowUsers = _objLs.aspnet_Users
                     .Where(x => x.IsAnonymous == false).OrderBy(x => x.UserName).Select(x => new
                     {
                         checkbxUserSel = x.UserName == User.Identity.Name ? 1 : 0,
                         x.UserName
                     }).ToList();
                return workflowUsers;

            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetUsersForWorkFlow", ex);
                return null;
            }
            // return null;
        }

        [System.Web.Http.HttpGet]
        public IHttpActionResult GetUsersRolesList(int Customerid)
        {
            try
            {
                string role = string.Empty;
                DataTable dt = new DataTable();
                var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                        .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                        .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                        .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();
                if (SuperAdminCheck.Any())
                {
                    //var sd = SuperAdminCheck.FirstOrDefault();
                    //if (sd.tcp.RoleName.Contains("SuperAdmin"))
                    //{
                    List<STP_LS_GET_USER_LIST_Result> lx = (from x in new CSEntities().STP_LS_GET_USER_LIST(User.Identity.Name, 0).Where(a => a.STATUS == "ACTIVE") select x).ToList();
                    dt = ToDataTable(lx);
                }
                else
                {
                    //List<STP_LS_GET_USER_LIST_Result> lx = (from x in new CSEntities().STP_LS_GET_USER_LIST(User.Identity.Name, Customerid).Where(a => a.STATUS == "ACTIVE" && !a.ROLE_NAME.Contains("Super")) select x).ToList();
                    List<STP_LS_GET_USER_LIST_Result> lx = (from x in new CSEntities().STP_LS_GET_USER_LIST(User.Identity.Name, Customerid).Where(a => a.STATUS == "ACTIVE") select x).ToList();
                    dt = ToDataTable(lx);
                }
                //}
                //List<STP_LS_GET_USER_LIST_Result> lx = (from x in new CSEntities().STP_LS_GET_USER_LIST().Where(a=>a.STATUS=="Active") select x).ToList();
                //dt = ToDataTable(lx);
                return Ok(dt);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetUsersRolesList", ex);
                return null;
            }
            // return null;
        }

        public static DataTable ToDataTable<T>(List<T> iList)
        {
            DataTable dataTable = new DataTable();
            PropertyDescriptorCollection propertyDescriptorCollection =
                TypeDescriptor.GetProperties(typeof(T));
            for (int i = 0; i < propertyDescriptorCollection.Count; i++)
            {
                PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
                Type type = propertyDescriptor.PropertyType;

                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    type = Nullable.GetUnderlyingType(type);


                dataTable.Columns.Add(propertyDescriptor.Name, type);
            }
            object[] values = new object[propertyDescriptorCollection.Count];
            foreach (T iListItem in iList)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        [System.Web.Http.HttpGet]
        public IList GetUsersRole()
        {
            try
            {
                var lx = (from x in new CSEntities().STP_LS_GET_USER_ROLE() select x).ToList();
                return lx;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetUsersRole", ex);
                return null;
            }
            // return null;
        }

        //[System.Web.Http.HttpPost]
        //public DataSourceResult GetUsersFunctionAllowed(DataSourceRequest request)
        //{
        //    try
        //    {
        //        return new UserAdminRepository().GetUsersFunctionAllowed(request);
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at GetUsersFunctionAllowed", ex);
        //    }
        //    return null;
        //}

        //[System.Web.Http.HttpGet]
        //public IList GetUsersFunctionAllowed()
        //{
        //    try
        //    {
        //        var lx = (from x in new CSEntities().STP_LS_GET_USER_FUNCTION_ALLOWED() select x).ToList();
        //        return lx;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at FamilyApiController: GetUsersFunctionAllowed", ex);
        //        return null;
        //    }
        //    // return null;
        //}

        //[System.Web.Http.HttpGet]
        //public IList GetUsersFunctionAllowed(string roleid)
        //{
        //    try
        //    {
        //        int roleId;
        //        int.TryParse(roleid.Trim('~'), out roleId);
        //        var lx = (from x in new CSEntities().STP_LS_GET_USER_FUNCTION_ALLOWED(roleId) select x).ToList();
        //        return lx;
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error at GetUsersFunctionAllowed", ex);
        //        return null;
        //    }
        //    // return null;
        //}

        [System.Web.Http.HttpGet]
        public IList GetUsersReadOnlyAttributeList(string username)
        {
            try
            {
                var customerid = _objLs.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                if (customerid != null)
                {
                    int intRoleId = 0;
                    var getroleid = _objLs.vw_aspnet_UsersInRoles
                        .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                        .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId,
                            (tcptps, tcp) => new { tcptps, tcp })
                        .Where(x => x.tcptps.tpf.UserName == username).ToList();
                    if (getroleid.Any())
                    {
                        var sd = getroleid.FirstOrDefault();
                        intRoleId = Convert.ToInt32(sd.tcp.Role_id);
                    }
                    var lx =
                        _objLs.STP_LS_GET_USER_READ_ONLY_ATTRIBUTE_LIST(intRoleId)
                            .Join(_objLs.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID,
                                (tcp, tpf) => new { tcp, tpf })
                            .Where(x => x.tpf.CUSTOMER_ID == customerid.CustomerId).Select(x => x.tcp).ToList();
                    return lx;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetUsersReadOnlyAttributeList", ex);
                return null;
            }
            // return null;
        }

        [System.Web.Http.HttpGet]
        public IList GetUserWorkFlow(int CustomerId)
        {
            try
            {

                var lx = (from x in new CSEntities().STP_LS_GET_USER_WORKFLOW(User.Identity.Name, CustomerId) select x).ToList();
                return lx;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetUserWorkFlow", ex);
                return null;
            }
            // return null;
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveUser(TB_USER1 model, string roleNames)
        {
            int allowedUserCount = 0;
            string customer_name = "";
            int customer_id = 0;
            string From_roleName = "";
            //int To_roleName = Convert.ToInt32(roleId);
            string User_Name = "";
            string PrevUser_Name = "";
            bool flagChkUser = false;
            int funRoleId;
            int result;


            try
            {

                if (model.TB_USER_ID != "" && model.TB_USER_ID != string.Empty)
                {
                    //var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                    //  .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                    //  .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                    //  .Where(x => x.tcptps.tpf.UserName == User.Identity.Name && x.tcp.RoleType == 1).ToList();

                    var username = "";

                    var objusername = _objLs.Customer_User.Where(x => x.Id == model.Customer_User_Id).Select(a => a.User_Name).FirstOrDefault();
                    if (objusername == "" || objusername == null)
                    {
                        username = User.Identity.Name;
                    }
                    else if (objusername == "questudio" || objusername.ToUpper() == "QUESTUDIO")
                    {
                        username = objusername;
                    }
                    else
                    {
                        username = objusername;
                    }
                    if (model.User_Name.ToUpper() == "QUESTUDIO")
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "You do not have permission to edit this User");
                    }
                    User_Name = model.User_Name;
                    PrevUser_Name = model.User_Name;


                    if (model.CATALOG_ID == "" || model.CATALOG_ID == "0" || model.CATALOG_ID == string.Empty)
                        model.CATALOG_ID = "1";
                    int usercount = _objLs.TB_USER.Count(x => x.TB_USER_ID == model.TB_USER_ID);
                    if (usercount == 0)
                    {

                        var allowedusers = from cu in _objLs.Customer_User
                                           from cust in _objLs.Customers
                                           from plan in _objLs.TB_PLAN
                                               //where (cu.User_Name == User.Identity.Name && cust.CustomerId == cu.CustomerId && cust.PlanId == plan.PLAN_ID)
                                           where (cu.User_Name == username && cust.CustomerId == cu.CustomerId && cust.PlanId == plan.PLAN_ID)
                                           select new CustomerUsers() { NO_OF_USERS = plan.NO_OF_USERS, CustomerId = cu.CustomerId };
                        allowedUserCount = allowedusers.FirstOrDefault().NO_OF_USERS;

                        var activeusers = (from company in _objLs.Customer_User
                                           from user in _objLs.aspnet_Users
                                           where (company.CustomerId == ((IQueryable<CustomerUsers>)allowedusers).FirstOrDefault().CustomerId && company.User_Name == user.UserName && !user.IsAnonymous)
                                           select user);
                        var activeuserCount = activeusers.Count();
                        if (activeuserCount + 1 > allowedUserCount)
                        {
                            if (model.Customer_User_Id > 0)
                            {
                                var customerList = _objLs.Customers.Join(_objLs.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name);
                                var custlist = customerList.Select(x => new { x.a.CompanyName, x.a.CustomerId }).ToList();
                                if (custlist.Any())
                                {
                                    customer_name = custlist.Select(a => a.CompanyName).FirstOrDefault().Replace(" ", "").Replace("_", "");
                                    customer_id = custlist.Select(a => a.CustomerId).FirstOrDefault();
                                }
                                From_roleName = customer_name + "_" + model.UserRoleName;


                                Customer_User objuser1 = _objLs.Customer_User.Find(model.Customer_User_Id);
                                var tempEmail = objuser1.Email;
                                if (model.Customer_User_Id > 0)
                                {
                                    //Update Email to User Invite


                                    //if( objuser1.User_Name != User_Name)
                                    //{
                                    //     var objuserchk = _objLs.Customer_User.Where(x => x.User_Name == User_Name).Select(a=>a.User_Name);

                                    // if (objuserchk != null)
                                    // {
                                    //     if (objuserchk.Count() == 0)
                                    //     {
                                    //         PrevUser_Name = objuser1.User_Name;
                                    //         objuser1.User_Name = User_Name;
                                    //     }
                                    // }
                                    //}

                                    objuser1.FirstName = model.FirstName;
                                    objuser1.LastName = model.LastName;
                                    objuser1.Title = model.Title;
                                    objuser1.Address = model.Address;
                                    objuser1.City = model.City;
                                    objuser1.State = model.State;
                                    objuser1.Zip = model.Zip;
                                    objuser1.Country = model.Country;
                                    objuser1.Email = model.Email;
                                    objuser1.Phone = model.Phone;
                                    objuser1.Fax = model.Fax;
                                    objuser1.CatalogID = Convert.ToInt32(model.CATALOG_ID);
                                    _objLs.SaveChanges();
                                }
                                //Update Email to User Invite

                                TB_USERINVITE userinvite = _objLs.TB_USERINVITE.Where(x => x.EMAIL == tempEmail).FirstOrDefault();
                                if (userinvite != null && userinvite.EMAIL != null)
                                {

                                    userinvite.EMAIL = model.Email;
                                    //userinvite.USER_STATUS = "Registered";
                                    userinvite.DATEUPDATED = DateTime.Now;
                                    _objLs.SaveChanges();
                                }

                                var objuserset = _objLs.aspnet_Users.FirstOrDefault(x => x.UserName == PrevUser_Name);

                                if (objuserset != null)
                                {
                                    if (model.Active.ToUpper() == "FALSE" &&
                                        model.User_Name.ToUpper() != User.Identity.Name.ToUpper() && activeuserCount > 1)
                                    {
                                        objuserset.IsAnonymous = Convert.ToBoolean(model.Active) ? false : true;
                                    }
                                    else if (model.Active.ToUpper() == "FALSE" &&
                                             model.User_Name.ToUpper() == User.Identity.Name.ToUpper() && activeuserCount > 1)
                                    {
                                        flagChkUser = true;
                                    }
                                    // objuserset.UserName = User_Name;
                                }
                                _objLs.SaveChanges();

                                //var objtbuserset = _objLs.TB_USER.FirstOrDefault(x => x.TB_USER_ID == PrevUser_Name);

                                //if (objtbuserset != null)
                                //{
                                //    objtbuserset.TB_USER_ID = User_Name;
                                //}
                                //_objLs.SaveChanges();


                                var objroleupdate = _objLs.vw_aspnet_UsersInRoles.FirstOrDefault(x => x.UserId == (_objLs.aspnet_Users.FirstOrDefault(a => a.UserName == User_Name)).UserId && x.RoleId == (_objLs.aspnet_Roles.FirstOrDefault(a => a.RoleName == From_roleName)).RoleId);
                                // int changeRoleid = Convert.ToInt32(role_Names);
                                if (objroleupdate != null)
                                {
                                    //var objroledelete = _objLs.vw_aspnet_UsersInRoles.FirstOrDefault(x => x.UserId == (_objLs.aspnet_Users.FirstOrDefault(a => a.UserName == model.User_Name)).UserId && x.RoleId == (_objLs.aspnet_Roles.FirstOrDefault(a => a.RoleName == From_roleName)).RoleId);
                                    //_objLs.vw_aspnet_UsersInRoles.Remove(objroledelete);
                                    //string TRoleId = Convert.ToString(_objLs.aspnet_Roles.FirstOrDefault(a => a.Role_id == To_roleName).RoleId);
                                    string FRoleId = Convert.ToString(_objLs.aspnet_Roles.FirstOrDefault(a => a.RoleName == From_roleName).RoleId);
                                    string UserId = Convert.ToString(_objLs.aspnet_Users.FirstOrDefault(a => a.UserName == User_Name).UserId);
                                    //_sqlString = "update tb_parts_key set attribute_value ='" + attrValue + "' where product_id in (select  product_id from tb_parts_key where family_id=" + gafamid + " and catalog_id =" + workingCatalogID1 + " and attribute_id=" + _gaattrid + "  )and  attribute_id=" + _gaattrid + " and catalog_id =" + workingCatalogID1 + "";
                                    //_sqlString = "UPDATE aspnet_UsersInRoles set RoleId='" + TRoleId + "' where   userid = '" + UserId + "' and RoleId ='" + FRoleId + "'";
                                    //ExecuteSqlQuery();

                                }
                                _objLs.SaveChanges();



                                allowedusers = from cu in _objLs.Customer_User
                                               from cust in _objLs.Customers
                                               from plan in _objLs.TB_PLAN
                                                   //where (cu.User_Name == User.Identity.Name && cust.CustomerId == cu.CustomerId && cust.PlanId == plan.PLAN_ID)
                                               where (cu.User_Name == username && cust.CustomerId == cu.CustomerId && cust.PlanId == plan.PLAN_ID)
                                               select new CustomerUsers() { NO_OF_USERS = plan.NO_OF_USERS, CustomerId = cu.CustomerId };
                                allowedUserCount = allowedusers.FirstOrDefault().NO_OF_USERS;

                                activeusers = (from company in _objLs.Customer_User
                                               from user in _objLs.aspnet_Users
                                               where (company.CustomerId == ((IQueryable<CustomerUsers>)allowedusers).FirstOrDefault().CustomerId && company.User_Name == user.UserName && !user.IsAnonymous)
                                               select user);
                                activeuserCount = activeusers.Count();

                                if (activeuserCount + 1 > allowedUserCount)
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, "You have reached the maximum number of Users per your Plan. Please inactivate some User and try again or contact support@questudio.com.");

                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, "Details updated successfully");
                                }




                            }

                            else
                            {

                                model.Active = "false";
                            }



                        }
                        if (1 == 1)
                        {
                            //Validate and Save Users

                            var customerList = _objLs.Customers.Join(_objLs.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name);
                            var custlist = customerList.Select(x => new { x.a.CompanyName, x.a.CustomerId }).ToList();
                            if (custlist.Any())
                            {
                                customer_name = custlist.Select(a => a.CompanyName).FirstOrDefault().Replace(" ", "").Replace("_", "");
                                customer_id = custlist.Select(a => a.CustomerId).FirstOrDefault();
                            }
                            From_roleName = customer_name + "_" + model.UserRoleName;
                            var emailchk = _objLs.aspnet_Membership.Join(_objLs.aspnet_Users, a => a.UserId, b => b.UserId, (a, b) => new { a, b }).Where(z => z.b.UserName != model.User_Name && z.a.Email == model.Email).Select(x => x.a.UserId).ToList();
                            if (emailchk.Any())
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, "A user name for that e-mail address already exists, please enter a different e-mail address");
                            }
                            //Save Membership Details
                            MembershipCreateStatus createStatus;
                            Membership.CreateUser(User_Name, model.Password, model.Email, null, null, true, null, out createStatus);
                            if (createStatus.ToString() == "DuplicateUserName" && model.Customer_User_Id == 0)
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, "User name already exists, please enter a different Name");
                            }
                            var objuserset = _objLs.aspnet_Users.FirstOrDefault(x => x.UserName == User_Name);

                            if (objuserset != null)
                            {
                                if (model.Active.ToUpper() == "FALSE" &&
                                    model.User_Name.ToUpper() != User.Identity.Name.ToUpper() && activeuserCount > 1)
                                {
                                    objuserset.IsAnonymous = Convert.ToBoolean(model.Active) ? false : true;
                                }
                                else if (model.Active.ToUpper() == "FALSE" &&
                                         model.User_Name.ToUpper() == User.Identity.Name.ToUpper() && activeuserCount > 1)
                                {
                                    objuserset.IsAnonymous = Convert.ToBoolean(model.Active) ? true : false;
                                    flagChkUser = true;
                                }
                                else if (model.Active.ToUpper() == "TRUE")
                                {
                                    objuserset.IsAnonymous = Convert.ToBoolean(model.Active) ? false : true;
                                }
                                // objuserset.UserName = User_Name;
                            }
                            _objLs.SaveChanges();


                            // var workflowCheck = _objLs.aspnet_Roles.FirstOrDefault(a => a.Role_id == Convert.ToInt32(roleId)).RoleId;
                            var objroleupdate = _objLs.vw_aspnet_UsersInRoles.FirstOrDefault(x => x.UserId == (_objLs.aspnet_Users.FirstOrDefault(a => a.UserName == User_Name)).UserId && x.RoleId == (_objLs.aspnet_Roles.FirstOrDefault(a => a.RoleName == From_roleName)).RoleId);
                            // int changeRoleid = Convert.ToInt32(roleId);
                            if (objroleupdate != null)
                            {
                                //var objroledelete = _objLs.vw_aspnet_UsersInRoles.FirstOrDefault(x => x.UserId == (_objLs.aspnet_Users.FirstOrDefault(a => a.UserName == model.User_Name)).UserId && x.RoleId == (_objLs.aspnet_Roles.FirstOrDefault(a => a.RoleName == From_roleName)).RoleId);
                                //_objLs.vw_aspnet_UsersInRoles.Remove(objroledelete);
                                // string TRoleId = Convert.ToString(_objLs.aspnet_Roles.FirstOrDefault(a => a.Role_id == To_roleName).RoleId);
                                string FRoleId = Convert.ToString(_objLs.aspnet_Roles.FirstOrDefault(a => a.RoleName == From_roleName).RoleId);
                                string UserId = Convert.ToString(_objLs.aspnet_Users.FirstOrDefault(a => a.UserName == User_Name).UserId);
                                //_sqlString = "update tb_parts_key set attribute_value ='" + attrValue + "' where product_id in (select  product_id from tb_parts_key where family_id=" + gafamid + " and catalog_id =" + workingCatalogID1 + " and attribute_id=" + _gaattrid + "  )and  attribute_id=" + _gaattrid + " and catalog_id =" + workingCatalogID1 + "";
                                //_sqlString = "UPDATE aspnet_UsersInRoles set RoleId='" + TRoleId + "' where   userid = '" + UserId + "' and RoleId ='" + FRoleId + "'";
                                //ExecuteSqlQuery();

                            }
                            _objLs.SaveChanges();


                            //Save customer Profile
                            if (createStatus == MembershipCreateStatus.Success)
                            {
                                var profile = ProfileModel.GetProfile(User_Name);
                                profile.CustomerId = model.CustomerId;
                                profile.FirstName = model.FirstName;
                                profile.LastName = model.LastName;
                                profile.Title = model.Title;
                                profile.Address1 = model.Address;
                                profile.Address2 = model.Address;
                                profile.City = model.City;
                                profile.State = model.State;
                                profile.Zip = model.Zip;
                                profile.Country = model.Country;
                                profile.Phone = model.Phone;
                                profile.Fax = model.Fax;
                                profile.EMail = model.Email;
                                profile.Comments = model.Comments;
                                profile.DateUpdated = DateTime.Now;
                                profile.DateCreated = DateTime.Now;
                                profile.Save();

                                if (customer_id == 1)
                                {
                                    string[] role_Names = Convert.ToString(roleNames).Split(',');
                                    foreach (var v in role_Names)
                                    {
                                        int roleids = Convert.ToInt32(v);
                                        var customerList1 = _objLs.aspnet_Roles.Where(z => z.Role_id == roleids);
                                        var strcustId = customerList1.Select(x => x.CustomerId).ToList();
                                        if (strcustId.Any())
                                        {
                                            customer_id = strcustId.Select(a => a.Value).FirstOrDefault();
                                        }
                                    }
                                }
                                // Save Customer User 
                                var currUser = _objLs.Customer_User.Where(x => x.Id == model.Customer_User_Id);
                                if (!currUser.Any())
                                {
                                    var objuser = new Customer_User();
                                    objuser.CustomerId = Convert.ToInt32(customer_id);
                                    objuser.User_Name = User_Name;
                                    objuser.FirstName = model.FirstName;
                                    objuser.LastName = model.LastName;
                                    objuser.Title = model.Title;
                                    objuser.Address = model.Address;
                                    objuser.City = model.City;
                                    objuser.State = model.State;
                                    objuser.Zip = model.Zip;
                                    objuser.Country = model.Country;
                                    objuser.Email = model.Email;
                                    objuser.Phone = model.Phone;
                                    objuser.Fax = model.Fax;
                                    objuser.DateCreated = DateTime.Now;
                                    objuser.DateUpdated = DateTime.Now;
                                    objuser.CatalogID = Convert.ToInt32(model.CATALOG_ID);
                                    _objLs.Customer_User.Add(objuser);

                                    _objLs.SaveChanges();
                                    AddOrganisationDetails(model);

                                    //Umbracomemberentry in CMSMember Table
                                    //Added by Jothipriya on April 20 2022
                                    using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                    {
                                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                                        objSqlCommand.CommandText = "STP_CATALOGSTUDIO_UmbracoMembershipEntry";
                                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                                        objSqlCommand.CommandTimeout = 0;
                                        objSqlCommand.Connection = objSqlConnection;
                                        objSqlCommand.Parameters.Add("@UserName", SqlDbType.VarChar, 100).Value = User_Name;
                                        objSqlCommand.Parameters.Add("@Password", SqlDbType.VarChar, 50).Value = model.Password;
                                        objSqlCommand.Parameters.Add("@FirstName", SqlDbType.VarChar, 100).Value = model.FirstName;
                                        objSqlCommand.Parameters.Add("@LastName", SqlDbType.VarChar, 100).Value = model.LastName;
                                        objSqlCommand.Parameters.Add("@RoleName", SqlDbType.VarChar, 100).Value = roleNames;
                                        objSqlConnection.Open();
                                        objSqlCommand.ExecuteNonQuery();
                                    }

                                    //multiple group starts//
                                    var userId = _objLs.aspnet_Users.Where(q => q.UserName == model.Email).Select(w => w.UserId).FirstOrDefault();
                                    string[] role_Names = Convert.ToString(roleNames).Split(',');
                                    foreach (var a in role_Names)
                                    {
                                        string fullRoleName = customer_name + "_" + a;
                                        var getRoleIdFromSelectedGroups = _objLs.aspnet_Roles.Where(r => r.RoleName == fullRoleName).Select(p => p.Role_id).ToList();
                                        foreach (var c in getRoleIdFromSelectedGroups)
                                        {
                                            if (c.Value == 1)
                                            {
                                                funRoleId = c.Value;
                                            }
                                            else
                                            {
                                                funRoleId = 0;
                                            }
                                            int result1 = NewSqlUser(User_Name, model.Password, Convert.ToInt16(funRoleId));
                                            result = result1;
                                            if (result == 1)
                                            {
                                                foreach (var b in role_Names)
                                                {
                                                    string fullRoleName1 = customer_name + "_" + b;
                                                    var Id = _objLs.aspnet_Roles.Where(v => v.RoleName == fullRoleName1).Select(s => s.Role_id).ToList();
                                                    var result2 = _objLs.STP_CATALOGSTUDIO5_AddNewUser(User_Name, "Active", Convert.ToInt16(Id[0]));
                                                    _objLs.SaveChanges();
                                                }
                                            }
                                        }
                                    }
                                    _objLs.Entry(objuser).GetDatabaseValues();

                                    //Save Email to User Invite
                                    var userinvitemodel = new TB_USERINVITE
                                    {
                                        CUSTOMERID = objuser.CustomerId,
                                        EMAIL = model.Email,
                                        USER_STATUS = "Registered",
                                        DATECREATED = DateTime.Now,
                                        DATEUPDATED = DateTime.Now


                                    };
                                    _objLs.TB_USERINVITE.Add(userinvitemodel);
                                    _objLs.SaveChanges();
                                }

                                int custid = _objLs.Customer_User.Max(x => x.Id);
                                var objusernames = _objLs.Customer_User.Where(x => x.Id == custid).Select(a => a.User_Name).FirstOrDefault();
                                if (objusernames == "" || objusernames == null)
                                {
                                    username = User.Identity.Name;
                                }
                                else if (objusernames == "questudio" || objusernames.ToUpper() == "QUESTUDIO")
                                {
                                    username = objusernames;
                                }
                                else
                                {
                                    username = objusernames;
                                }
                                allowedusers = from cu in _objLs.Customer_User
                                               from cust in _objLs.Customers
                                               from plan in _objLs.TB_PLAN
                                                   //where (cu.User_Name == User.Identity.Name && cust.CustomerId == cu.CustomerId && cust.PlanId == plan.PLAN_ID)
                                               where (cu.User_Name == username && cust.CustomerId == cu.CustomerId && cust.PlanId == plan.PLAN_ID)
                                               select new CustomerUsers() { NO_OF_USERS = plan.NO_OF_USERS, CustomerId = cu.CustomerId };
                                allowedUserCount = allowedusers.FirstOrDefault().NO_OF_USERS;

                                activeusers = (from company in _objLs.Customer_User
                                               from user in _objLs.aspnet_Users
                                               where (company.CustomerId == ((IQueryable<CustomerUsers>)allowedusers).FirstOrDefault().CustomerId && company.User_Name == user.UserName && !user.IsAnonymous)
                                               select user);
                                activeuserCount = activeusers.Count();

                            }
                            else
                            {
                                //Update Customer Users
                                Customer_User objuser1 = _objLs.Customer_User.Find(model.Customer_User_Id);
                                var tempEmail = objuser1.Email;
                                if (model.Customer_User_Id > 0)
                                {

                                    objuser1.User_Name = User_Name;
                                    objuser1.FirstName = model.FirstName;
                                    objuser1.LastName = model.LastName;
                                    objuser1.Title = model.Title;
                                    objuser1.Address = model.Address;
                                    objuser1.City = model.City;
                                    objuser1.State = model.State;
                                    objuser1.Zip = model.Zip;
                                    objuser1.Country = model.Country;
                                    objuser1.Email = model.Email;
                                    objuser1.Phone = model.Phone;
                                    objuser1.Fax = model.Fax;
                                    objuser1.CatalogID = Convert.ToInt32(model.CATALOG_ID);
                                    _objLs.SaveChanges();

                                    //Update Email to User Invite
                                    _objLs.Entry(objuser1).GetDatabaseValues();
                                    TB_USERINVITE userinvite = _objLs.TB_USERINVITE.Where(x => x.EMAIL == tempEmail).FirstOrDefault();
                                    if (userinvite != null && userinvite.EMAIL != null)
                                    {


                                        userinvite.EMAIL = model.Email;
                                        //userinvite.USER_STATUS = "Registered";
                                        userinvite.DATEUPDATED = DateTime.Now;
                                        _objLs.SaveChanges();
                                    }
                                }
                            }

                            //Update Users 
                            //if (model.Email != null)
                            //{   

                            //    const string subject = "Register successfully for Catalog Studio";
                            //    var body = "Thanks for registering for Catalog Studio Account,";
                            //    body += "Kinldy Wait for an admin Approve and Once get Approved you will get access for using Catalog Studio,";
                            //    body +="<br/> <b>UserName:</b>"+  model.User_Name ;
                            //    body += "<br/><b>Password:</b>" + model.Password;
                            //    body += "<br/><br/><b>Regards, ,</b>";
                            //    body += "<br/><br/> CatalogStudio Team";
                            //    CommonUtil.SendEmailForCompanyandUserRegister(subject, model.Email, body, "New Registeration");
                            //}
                            //else
                            //{
                            //    ModelState.AddModelError("Error", Resource.AppController_ForgotPassword_User_Name_is_InCorrect__Please_Check);
                            //}
                            //if (activeusers + 1 > allowedUserCount)
                            if (activeuserCount + 1 > allowedUserCount)
                            {
                                objuserset = _objLs.aspnet_Users.FirstOrDefault(x => x.UserName == User_Name);

                                if (objuserset != null)
                                {
                                    objuserset.IsAnonymous = true;
                                }
                                _objLs.SaveChanges();
                                return Request.CreateResponse(HttpStatusCode.OK, "Active User(s) Limit Reached. New user created with Inactive status.");
                            }
                            else
                            {
                                if (flagChkUser)
                                { return Request.CreateResponse(HttpStatusCode.OK, "You do not have permission to edit this User"); }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);
                                }
                            }

                        }

                    }

                }
                string messages = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                return Request.CreateResponse(HttpStatusCode.BadRequest, messages);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: SaveUser", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        public class GroupListItem
        {
            public Guid RoleId { get; set; }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage UpdateUser(TB_USER1 model, string roleNames)
        {
            int allowedUserCount = 0;
            string customer_name = "";
            int customer_id = 0;
            string From_roleName = "";
            //string To_roleName = roleNames;
            string[] role_Names = Convert.ToString(roleNames).Split(',');
            string User_Name = "";
            string PrevUser_Name = "";
            bool flagChkUser = false;
            try
            {
                AddOrganisationDetails(model);
                if (model.TB_USER_ID != "" && model.TB_USER_ID != string.Empty)
                {
                    var username = "";
                    var objusername = _objLs.Customer_User.Where(x => x.Id == model.Customer_User_Id).Select(a => a.User_Name).FirstOrDefault();
                    if (objusername == "" || objusername == null)
                    {
                        username = User.Identity.Name;
                    }
                    else if (objusername == "questudio" || objusername.ToUpper() == "QUESTUDIO")
                    {
                        username = objusername;
                    }
                    else
                    {
                        username = objusername;
                    }
                    if (model.User_Name.ToUpper() == "QUESTUDIO")
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "You do not have permission to edit this User");
                    }
                    User_Name = model.User_Name;
                    PrevUser_Name = model.User_Name;
                    if (model.CATALOG_ID == "" || model.CATALOG_ID == "0" || model.CATALOG_ID == string.Empty)
                        model.CATALOG_ID = "1";
                    int usercount = _objLs.Customer_User.Count(x => x.Id == model.Customer_User_Id);
                    if (usercount != 0)
                    {
                        var allowedusers = from cu in _objLs.Customer_User
                                           from cust in _objLs.Customers
                                           from plan in _objLs.TB_PLAN
                                           where (cu.User_Name == username && cust.CustomerId == cu.CustomerId && cust.PlanId == plan.PLAN_ID)
                                           select new CustomerUsers() { NO_OF_USERS = plan.NO_OF_USERS, CustomerId = cu.CustomerId };
                        allowedUserCount = allowedusers.FirstOrDefault().NO_OF_USERS;
                        var activeusers = (from company in _objLs.Customer_User
                                           from user in _objLs.aspnet_Users
                                           where (company.CustomerId == ((IQueryable<CustomerUsers>)allowedusers).FirstOrDefault().CustomerId && company.User_Name == user.UserName && !user.IsAnonymous)
                                           select user);
                        var activeuserCount = activeusers.Count();
                        if (activeuserCount + 1 > allowedUserCount && model.Active.ToLower() == "true")
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "You have reached the maximum number of Users per your Plan. Please inactivate some User and try again or contact support@questudio.com.");
                        }
                        else
                        {
                            if (model.Customer_User_Id > 0)
                            {
                                var customerList = _objLs.Customers.Join(_objLs.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name);
                                var custlist = customerList.Select(x => new { x.a.CompanyName, x.a.CustomerId }).ToList();
                                if (custlist.Any())
                                {
                                    customer_name = custlist.Select(a => a.CompanyName).FirstOrDefault().Replace(" ", "").Replace("_", "");
                                    customer_id = custlist.Select(a => a.CustomerId).FirstOrDefault();
                                }
                                From_roleName = customer_name + "_" + model.UserRoleName;
                                Customer_User objuser1 = _objLs.Customer_User.Find(model.Customer_User_Id);
                                var tempEmail = objuser1.Email;
                                if (model.Customer_User_Id > 0)
                                {
                                    objuser1.FirstName = model.FirstName;
                                    objuser1.LastName = model.LastName;
                                    objuser1.Title = model.Title;
                                    objuser1.Address = model.Address;
                                    objuser1.City = model.City;
                                    objuser1.State = model.State;
                                    objuser1.Zip = model.Zip;
                                    objuser1.Country = model.Country;
                                    objuser1.Phone = model.Phone;
                                    objuser1.Fax = model.Fax;
                                    objuser1.CatalogID = Convert.ToInt32(model.CATALOG_ID);
                                    _objLs.SaveChanges();
                                }

                                var objuserset = _objLs.aspnet_Users.FirstOrDefault(x => x.UserName == PrevUser_Name);
                                if (objuserset == null)
                                {
                                    objuserset = _objLs.aspnet_Users.FirstOrDefault(x => x.UserName == username);
                                }
                                if (objuserset != null)
                                {
                                    if (model.User_Name.ToUpper() != User.Identity.Name.ToUpper() && activeuserCount > 1)
                                    {
                                        objuserset.IsAnonymous = Convert.ToBoolean(false);
                                    }
                                    else if (model.User_Name.ToUpper() == User.Identity.Name.ToUpper() && activeuserCount > 1)
                                    {
                                        flagChkUser = true;
                                    }

                                }
                                _objLs.SaveChanges();
                                //var newRoleId = Guid.Parse(_objLs.aspnet_Roles.FirstOrDefault(a => a.RoleName == From_roleName).RoleId.ToString());

                                var userdetails = _objLs.aspnet_Users.Where(a => a.UserName == User_Name).FirstOrDefault();
                                if (userdetails == null)
                                {
                                    var userName = User_Name.Split('@')[0].ToString();
                                    userdetails = _objLs.aspnet_Users.FirstOrDefault(a => a.UserName == userName);
                                    if (userdetails == null)
                                    {
                                        userName = _objLs.Customer_User.Where(a => a.Email == User_Name).Select(x => x.User_Name).FirstOrDefault();
                                        userdetails = _objLs.aspnet_Users.FirstOrDefault(a => a.UserName == userName);
                                    }

                                }
                                var userId = Guid.Parse(userdetails.UserId.ToString());
                                // var objroleupdate = _objLs.vw_aspnet_UsersInRoles.FirstOrDefault(x => x.UserId == userId && x.RoleId == newRoleId);
                                //int changeRoleid = Convert.ToInt32(roleId);
                                if (model.User_Name != User.Identity.Name) { 
                                var fromDB = new HashSet<GroupListItem>();
                                var fromUi = new HashSet<GroupListItem>();
                               
                                    foreach (var a in role_Names)
                                    {
                                        string fullRoleName = customer_name + "_" + a;
                                        var Id = _objLs.aspnet_Roles.Where(v => v.RoleName == fullRoleName).Select(s => s.RoleId).ToList();
                                        foreach (var cust in Id)
                                        {
                                            fromUi.Add(new GroupListItem() { RoleId = cust });
                                        }


                                    }

                                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                    {

                                        string getExisitingRoleId = @"SELECT RoleId from aspnet_UsersInRoles WHERE UserId=@userId";
                                        var roleIds = db.Query<Role>(getExisitingRoleId, new
                                        {
                                            userId

                                        }).ToList();

                                        foreach (Role role in roleIds)
                                        {
                                            fromDB.Add(new GroupListItem() { RoleId = role.RoleId });
                                        }
                                    }
                                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                    {
                                        string deleteQuery = "delete from aspnet_UsersInRoles WHERE UserId = @UserId";
                                        int affectedRows = db.Execute(deleteQuery, new
                                        {
                                            userId

                                        });

                                    }

                                    var all = fromUi.Union(fromDB);
                                    var finalIntersection = all.Intersect(fromUi);
                                    foreach (var a in finalIntersection)
                                    {
                                        var roleid = a.RoleId;
                                        using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                                        {
                                            string insertQuery = @"INSERT INTO aspnet_UsersInRoles ([UserId],[RoleId]) values (@userId,@roleid)";
                                            var getResult = db.Execute(insertQuery, new
                                            {
                                                userId,
                                                a.RoleId

                                            });
                                        }

                                    }
                               
                                _objLs.SaveChanges();
                                return Request.CreateResponse(HttpStatusCode.OK, "Details updated successfully");

                            }
                                }
                            else
                            {
                                model.Active = "false";
                            }

                        }
                    }

                }
               
                return Request.CreateResponse(HttpStatusCode.OK, "Details updated successfully");
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: UpdateUser", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }




        public void AddOrganisationDetails(TB_USER1 model)
        {
            string orgId = model.organisation;
            string email = model.Email;
            var userIdforOrg = _objLs.aspnet_Membership.Where(a => a.Email == email).Select(b => b.UserId).SingleOrDefault();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
            {

                string checkUser = @"SELECT * FROM TB_ORGANIZATION_USER WHERE userId=@userIdforOrg";
                var result = db.Query(checkUser, new
                {

                    userIdforOrg

                }).ToList();
                if (result.Count != 0)
                {
                    string updateOrganisation = @"UPDATE TB_ORGANIZATION_USER SET ORGANIZATION_ID=@orgId WHERE userid=@userIdforOrg";
                    var result2 = db.Execute(updateOrganisation, new
                    {
                        orgId,
                        userIdforOrg

                    });
                }
                else
                {
                    string insertToOrgUser = @"INSERT INTO TB_ORGANIZATION_USER ([ORGANIZATION_ID],[userId]) values (@orgId,@userIdforOrg)";
                    var result1 = db.Execute(insertToOrgUser, new
                    {
                        orgId,
                        userIdforOrg

                    });
                }
            }

        }

        public int NewSqlUser(string newuser, string password, int roleid)
        {

            int Result = 0;
            string _SqlAddUser = "CREATE LOGIN [" + newuser + "] WITH PASSWORD=N'" + password + "',CHECK_EXPIRATION = OFF,CHECK_POLICY = OFF";
            string _SqlCreateUser = "CREATE USER [" + newuser + " ] FOR LOGIN [" + newuser + "]";
            //string _SqlSchma = "ALTER AUTHORIZATION ON SCHEMA::[db_owner] TO [" + newuser + "]";
            string _SqlSpUser = "EXEC sp_addrolemember N'db_owner', N'" + newuser + "'";
            string _sqlseladmin = "";
            try
            {
                //  _adduser = new SqlCommand(_SqlAddUser, oConn);
                _sqlString = _SqlAddUser;
                ExecuteSqlQuery();
                _sqlString = _SqlCreateUser;
                //_adduser = new SqlCommand(_SqlCreateUser, oConn);
                ExecuteSqlQuery();
                // _adduser = new SqlCommand(_SqlSchma, oConn);
                // _adduser.ExecuteNonQuery();
                _sqlString = _SqlSpUser;
                //_adduser = new SqlCommand(_SqlSpUser, oConn);
                ExecuteSqlQuery();
                if (roleid == 1)
                {
                    _sqlString = _sqlseladmin;
                    //  _adduser = new SqlCommand(_sqlseladmin, oConn);
                    ExecuteSqlQuery();
                }
                Result = 1;
            }
            catch (SqlException es)
            {


            }


            return Result;
        }


        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveUserRole(string roleId)
        {
            try
            {
                int roleIdreturn = 0;
                string customer_name = "";
                int customer_id = 0;
                int roletype = 3;
                if (roleId != "" && roleId != string.Empty)
                {
                    var SuperAdminCheck = _objLs.vw_aspnet_UsersInRoles
                       .Join(_objLs.aspnet_Users, tps => tps.UserId, tpf => tpf.UserId, (tps, tpf) => new { tps, tpf })
                       .Join(_objLs.aspnet_Roles, tcptps => tcptps.tps.RoleId, tcp => tcp.RoleId, (tcptps, tcp) => new { tcptps, tcp })
                       .Where(x => x.tcptps.tpf.UserName == User.Identity.Name).ToList();
                    if (SuperAdminCheck.Any())
                    {
                        var sd = SuperAdminCheck.FirstOrDefault();
                        if (sd.tcp.RoleType == 1)
                        {
                            roletype = 2;
                        }
                        else if (sd.tcp.RoleType == 2)
                        {
                            roletype = 3;

                        }
                    }

                    var customerList = _objLs.Customers.Join(_objLs.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == User.Identity.Name);
                    var custlist = customerList.Select(x => new { x.a.CompanyName, x.a.CustomerId }).ToList();
                    if (custlist.Any())
                    {
                        customer_name = custlist.Select(a => a.CompanyName).FirstOrDefault().Replace(" ", "").Replace("_", "");
                        customer_id = custlist.Select(a => a.CustomerId).FirstOrDefault();
                    }
                    roleId = customer_name + "_" + roleId;
                    var tbroleCheck = _objLs.TB_ROLE.Where(a => a.ROLE_NAME == roleId);
                    if (!tbroleCheck.Any())
                    {
                        var result = _objLs.STP_CATALOGSTUDIO5_AddNewRole(roleId);
                        _objLs.SaveChanges();
                        if (result != null)
                        {
                            int rid = Convert.ToInt32(result);
                            var roleResult = _objLs.TB_ROLE.FirstOrDefault(a => a.ROLE_NAME == roleId);
                            if (roleResult != null)
                            {
                                UserRolesList role = new UserRolesList();
                                var aspnetrolescheck = _objLs.aspnet_Roles.Where(x => x.Role_id == roleResult.ROLE_ID);
                                if (!aspnetrolescheck.Any())
                                {
                                    //var objAspnetRoles = new aspnet_Roles
                                    //{
                                    //    RoleName = roleResult.ROLE_NAME,
                                    //    Role_id = roleResult.ROLE_ID
                                    //};
                                    _objLs.aspnet_Roles_CreateRole("/", roleId);
                                    var aspnetrolescheck1 = _objLs.aspnet_Roles.FirstOrDefault(x => x.RoleName == roleId);
                                    if (aspnetrolescheck1 != null)
                                    {
                                        roleIdreturn = roleResult.ROLE_ID;
                                        aspnetrolescheck1.Role_id = roleResult.ROLE_ID;
                                        aspnetrolescheck1.RoleType = roletype;
                                        aspnetrolescheck1.CustomerId = customer_id;
                                        aspnetrolescheck1.Prefix_CompanyName = customer_name;

                                        var objCustomercatalogs = _objLs.Customer_Catalog.Where(x => x.CustomerId == customer_id);
                                        if (objCustomercatalogs.Any())
                                        {
                                            foreach (var objCustomercatalog in objCustomercatalogs)
                                            {
                                                var rolecatalog = _objLs.Customer_Role_Catalog.Where(x => x.CATALOG_ID == objCustomercatalog.CATALOG_ID && x.RoleId == roleIdreturn && x.CustomerId == customer_id);
                                                if (!rolecatalog.Any())
                                                {

                                                    var objcustomerrolecatalog = new Customer_Role_Catalog
                                                    {
                                                        RoleId = roleIdreturn,
                                                        CustomerId = customer_id,
                                                        CATALOG_ID = objCustomercatalog.CATALOG_ID,
                                                        DateCreated = DateTime.Now,
                                                        DateUpdated = DateTime.Now,
                                                        IsActive = true
                                                    };
                                                    _objLs.Customer_Role_Catalog.Add(objcustomerrolecatalog);

                                                }
                                            }
                                        }

                                        _objLs.SaveChanges();
                                    }


                                }
                            }
                        }
                        _objLs.SaveChanges();
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Role name already exists, please enter a different Role name and continue");
                    }

                    return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted + "~" + roleIdreturn);
                }
                string messages = string.Join("; ", ModelState.Values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
                return Request.CreateResponse(HttpStatusCode.BadRequest, messages);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at UserAdminApiController: SaveUserRole", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveUserWorkFlow(object model)
        {
            try
            {
                //using (var db = new CSEntities())
                // {
                var workFlowitems = ((JArray)model).Select(x => new TB_USER_FLOW
                {
                    TB_USER_ID = (string)x["TB_USER_ID"],
                    NEW = (bool)x["NEW"],
                    UPDATED = (bool)x["UPDATED"],
                    REVIEW_PROCESS = (bool)x["REVIEW_PROCESS"],
                    SUBMIT = (bool)x["SUBMIT"],
                    APPROVE = (bool)x["APPROVE"]
                }).ToList();


                foreach (var dynTuw in workFlowitems)
                {
                    int userworkflowcount = _objLs.TB_USER_WORKFLOW.Count(x => x.TB_USER_ID == dynTuw.TB_USER_ID);
                    if (userworkflowcount != 0)
                    {
                        var objuserworkflow = _objLs.TB_USER_WORKFLOW.Find(dynTuw.TB_USER_ID);
                        objuserworkflow.NEW = dynTuw.NEW;
                        objuserworkflow.UPDATED = dynTuw.UPDATED;
                        objuserworkflow.REVIEW_PROCESS = dynTuw.REVIEW_PROCESS;
                        objuserworkflow.SUBMIT = dynTuw.SUBMIT;
                        objuserworkflow.APPROVE = dynTuw.APPROVE;
                        _objLs.SaveChanges();
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);
                // }

                //if (model.TB_USER_ID != "" && model.TB_USER_ID != string.Empty)
                //    {

                //        TB_USER_WORKFLOW objcatalog = new TB_USER_WORKFLOW();
                //        objcatalog.TB_USER_ID = model.TB_USER_ID;
                //        objcatalog.SUBMIT = model.SUBMIT;
                //        objcatalog.APPROVE = model.APPROVE;
                //        objLS.TB_USER_WORKFLOW.Add(objcatalog);

                //        objLS.SaveChanges();
                //        return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);
                //    }
                //    else
                //    {
                //        string messages = string.Join("; ", ModelState.Values
                //                       .SelectMany(x => x.Errors)
                //                       .Select(x => x.ErrorMessage));
                //        return Request.CreateResponse(HttpStatusCode.BadRequest, messages);
                //    }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: SaveUserWorkFlow", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        public void EmptyRecycleBindetails()
        {
            try
            {
                TB_RECYCLE_FOR_ATTRIBUTES objTbRecycleForAttributes = _objLs.TB_RECYCLE_FOR_ATTRIBUTES.Find();
                _objLs.TB_RECYCLE_FOR_ATTRIBUTES.Remove(objTbRecycleForAttributes);
                _objLs.SaveChanges();

                TB_RECYCLE_TABLE objTbRecycleForTable = _objLs.TB_RECYCLE_TABLE.Find();
                _objLs.TB_RECYCLE_TABLE.Remove(objTbRecycleForTable);
                _objLs.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:EmptyRecycleBindetails", ex);

            }
        }
        //---save Changes---------//
        [System.Web.Http.HttpPost]
        public void SaveproductconfigSpecs(JArray model, int familyId, int catalogId)
        {
            try
            {
                var values = (model).Select(x => x).ToList();
                //  var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(model.ToString());
                int productId = 0;
                // int catalogId = 0;
                // int familyId = 0;
                //bool publish = true;
                //bool publish2Print = true;
                //int workflowstatus = 0;

                if (values[0] != null)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(values[0]["value"])))
                    {
                        productId = Convert.ToInt32(values[0]["value"]);
                    }
                }

                //var objproductpublish = _objLs.TB_PROD_FAMILY.FirstOrDefault(p => p.FAMILY_ID == familyId && p.PRODUCT_ID == productId);
                string categoryId = _objLs.TB_FAMILY.FirstOrDefault(a => a.FAMILY_ID == familyId).CATEGORY_ID;
                foreach (var value in model)
                {
                    if (Convert.ToString(value["title"]).Contains("__"))
                    {
                        var attributeNames = Convert.ToString(value["title"])
                            .Split(new[] { "__" }, StringSplitOptions.None);
                        string attributeName = attributeNames[0];
                        //string imageCaption = Convert.ToString(attributeNames[1]);
                        var attributeId = Convert.ToInt32(attributeNames[2].TrimStart('A'));

                        if (attributeId == 0)
                        {
                            var attributenamedetails = _objLs.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == attributeName);
                            if (attributenamedetails.Any())
                            {
                                var attributenames = attributenamedetails.FirstOrDefault();
                                if (attributenames != null) attributeId = attributenames.ATTRIBUTE_ID;
                            }
                        }
                        if (attributeId != 1)
                        {
                            var attributedetails = _objLs.TB_ATTRIBUTE.Find(attributeId);
                            var attributeType = attributedetails.ATTRIBUTE_TYPE;
                            var attributeDatatype = attributedetails.ATTRIBUTE_DATATYPE;
                            var attributeDataformat = attributedetails.ATTRIBUTE_DATAFORMAT;
                            if (value["value"] != null)
                            {
                                var stringvalue = Convert.ToString(value["value"]);
                                string attributesize = Convert.ToString(attributedetails.ATTRIBUTE_DATATYPE);
                                bool isCalculated = Convert.ToBoolean(attributedetails.IS_CALCULATED);
                                int textLength = 0;
                                if (!string.IsNullOrEmpty(stringvalue))
                                {
                                    textLength = stringvalue.Length;
                                }
                                if (textLength > 0)
                                {
                                    var objproductSpecs =
                                        _objLs.TB_PROD_SPECS.FirstOrDefault(
                                            s => s.PRODUCT_ID == productId && s.ATTRIBUTE_ID == attributeId);
                                    if (objproductSpecs == null)
                                    {
                                        var count =
                                            _objLs.TB_PROD_FAMILY_ATTR_LIST.Count(
                                                x =>
                                                    x.FAMILY_ID == familyId && x.PRODUCT_ID == 0 &&
                                                    x.ATTRIBUTE_ID == attributeId);
                                        if (count == 0)
                                        {
                                            var familyattrcount =
                                                _objLs.TB_PROD_FAMILY_ATTR_LIST.Where(
                                                    x => x.FAMILY_ID == familyId && x.ATTRIBUTE_ID == attributeId);
                                            if (familyattrcount.Any())
                                            {
                                                var tbProdFamilyAttrList = familyattrcount.FirstOrDefault();
                                                if (tbProdFamilyAttrList != null)
                                                {
                                                    var sortOrder = tbProdFamilyAttrList.SORT_ORDER;
                                                    var objTbProdFamilyAttrList = new TB_PROD_FAMILY_ATTR_LIST
                                                    {
                                                        FAMILY_ID = familyId,
                                                        PRODUCT_ID = 0,
                                                        SORT_ORDER = sortOrder,
                                                        ATTRIBUTE_ID = attributeId,
                                                        CREATED_USER = User.Identity.Name,
                                                        MODIFIED_USER = User.Identity.Name,
                                                        MODIFIED_DATE = DateTime.Now,
                                                        CREATED_DATE = DateTime.Now
                                                    };
                                                    _objLs.TB_PROD_FAMILY_ATTR_LIST.Add(objTbProdFamilyAttrList);

                                                }
                                            }
                                            else
                                            {
                                                var familymaxsortorder =
                                                    _objLs.TB_PROD_FAMILY_ATTR_LIST.Where(x => x.FAMILY_ID == familyId);
                                                int sortOrder = 0;
                                                if (familymaxsortorder.Any())
                                                {
                                                    sortOrder = familymaxsortorder.Max(x => x.SORT_ORDER);
                                                }
                                                var objTbProdFamilyAttrList = new TB_PROD_FAMILY_ATTR_LIST
                                                {
                                                    FAMILY_ID = familyId,
                                                    PRODUCT_ID = 0,
                                                    SORT_ORDER = sortOrder + 1,
                                                    ATTRIBUTE_ID = attributeId,
                                                    CREATED_USER = User.Identity.Name,
                                                    MODIFIED_USER = User.Identity.Name,
                                                    MODIFIED_DATE = DateTime.Now,
                                                    CREATED_DATE = DateTime.Now
                                                };
                                                _objLs.TB_PROD_FAMILY_ATTR_LIST.Add(objTbProdFamilyAttrList);
                                            }
                                            _objLs.SaveChanges();
                                        }
                                        if (attributeType != 4)
                                        {
                                            switch (attributeType)
                                            {
                                                case 6:
                                                    {
                                                        var partskeycount =
                                                            _objLs.TB_PARTS_KEY.Count(
                                                                x =>
                                                                    x.PRODUCT_ID == productId &&
                                                                    x.ATTRIBUTE_ID == attributeId && x.FAMILY_ID == familyId &&
                                                                    x.CATALOG_ID == catalogId &&
                                                                    x.CATEGORY_ID == categoryId);
                                                        if (partskeycount == 0)
                                                        {
                                                            var objTbProdspecs = new TB_PARTS_KEY
                                                            {
                                                                PRODUCT_ID = productId,
                                                                FAMILY_ID = familyId,
                                                                CATALOG_ID = catalogId,
                                                                ATTRIBUTE_ID = attributeId,
                                                                ATTRIBUTE_VALUE = stringvalue,
                                                                CATEGORY_ID = categoryId,
                                                                CREATED_USER = User.Identity.Name,
                                                                MODIFIED_USER = User.Identity.Name,
                                                                MODIFIED_DATE = DateTime.Now,
                                                                CREATED_DATE = DateTime.Now
                                                            };

                                                            _objLs.TB_PARTS_KEY.Add(objTbProdspecs);
                                                            _objLs.SaveChanges();
                                                        }
                                                        else
                                                        {
                                                            var objpartskey =
                                                                _objLs.TB_PARTS_KEY.FirstOrDefault(
                                                                    x =>
                                                                        x.PRODUCT_ID == productId &&
                                                                        x.ATTRIBUTE_ID == attributeId &&
                                                                        x.FAMILY_ID == familyId && x.CATALOG_ID == catalogId && x.CATEGORY_ID == categoryId);
                                                            if (objpartskey != null)
                                                            {
                                                                objpartskey.ATTRIBUTE_VALUE = stringvalue;
                                                                _objLs.SaveChanges();

                                                            }
                                                        }
                                                    }
                                                    break;
                                                case 3:
                                                    {
                                                        var objTbProdspecs = new TB_PROD_SPECS
                                                        {
                                                            PRODUCT_ID = productId,
                                                            ATTRIBUTE_ID = attributeId,
                                                            STRING_VALUE = stringvalue,
                                                            CREATED_USER = User.Identity.Name,
                                                            MODIFIED_USER = User.Identity.Name,
                                                            MODIFIED_DATE = DateTime.Now,
                                                            CREATED_DATE = DateTime.Now
                                                        };
                                                        _objLs.TB_PROD_SPECS.Add(objTbProdspecs);
                                                        _objLs.SaveChanges();
                                                    }
                                                    break;
                                                default:
                                                    if (attributeDatatype.Contains("Num"))
                                                    {
                                                        if (string.IsNullOrEmpty(stringvalue))
                                                        {
                                                            var objTbProdspecs = new TB_PROD_SPECS
                                                            {
                                                                PRODUCT_ID = productId,
                                                                ATTRIBUTE_ID = attributeId,
                                                                NUMERIC_VALUE = null,
                                                                CREATED_USER = User.Identity.Name,
                                                                MODIFIED_USER = User.Identity.Name,
                                                                MODIFIED_DATE = DateTime.Now,
                                                                CREATED_DATE = DateTime.Now
                                                            };
                                                            _objLs.TB_PROD_SPECS.Add(objTbProdspecs);
                                                            _objLs.SaveChanges();
                                                        }
                                                        else
                                                        {
                                                            var objTbProdspecs = new TB_PROD_SPECS
                                                            {
                                                                PRODUCT_ID = productId,
                                                                ATTRIBUTE_ID = attributeId,
                                                                NUMERIC_VALUE = Convert.ToDecimal(stringvalue),
                                                                CREATED_USER = User.Identity.Name,
                                                                MODIFIED_USER = User.Identity.Name,
                                                                MODIFIED_DATE = DateTime.Now,
                                                                CREATED_DATE = DateTime.Now
                                                            };
                                                            _objLs.TB_PROD_SPECS.Add(objTbProdspecs);
                                                            _objLs.SaveChanges();
                                                        }

                                                    }
                                                    else
                                                    {
                                                        var objTbProdspecs = new TB_PROD_SPECS
                                                        {
                                                            PRODUCT_ID = productId,
                                                            ATTRIBUTE_ID = attributeId,
                                                            STRING_VALUE = stringvalue,
                                                            CREATED_USER = User.Identity.Name,
                                                            MODIFIED_USER = User.Identity.Name,
                                                            MODIFIED_DATE = DateTime.Now,
                                                            CREATED_DATE = DateTime.Now
                                                        };
                                                        _objLs.TB_PROD_SPECS.Add(objTbProdspecs);
                                                        _objLs.SaveChanges();
                                                    }
                                                    break;
                                            }
                                        }
                                        else
                                        {
                                            if (string.IsNullOrEmpty(stringvalue))
                                            {
                                                var objTbProdspecs = new TB_PROD_SPECS
                                                {
                                                    PRODUCT_ID = productId,
                                                    ATTRIBUTE_ID = attributeId,
                                                    NUMERIC_VALUE = null,
                                                    CREATED_USER = User.Identity.Name,
                                                    MODIFIED_USER = User.Identity.Name,
                                                    MODIFIED_DATE = DateTime.Now,
                                                    CREATED_DATE = DateTime.Now
                                                };
                                                _objLs.TB_PROD_SPECS.Add(objTbProdspecs);
                                                _objLs.SaveChanges();
                                            }
                                            else
                                            {
                                                var objTbProdspecs = new TB_PROD_SPECS
                                                {
                                                    PRODUCT_ID = productId,
                                                    ATTRIBUTE_ID = attributeId,
                                                    NUMERIC_VALUE = Convert.ToDecimal(stringvalue),
                                                    CREATED_USER = User.Identity.Name,
                                                    MODIFIED_USER = User.Identity.Name,
                                                    MODIFIED_DATE = DateTime.Now,
                                                    CREATED_DATE = DateTime.Now
                                                };
                                                _objLs.TB_PROD_SPECS.Add(objTbProdspecs);
                                                _objLs.SaveChanges();
                                            }
                                        }

                                    }
                                    else
                                    {
                                        if (attributeType == 4)
                                        {
                                            if (string.IsNullOrEmpty(stringvalue))
                                            {
                                                objproductSpecs.NUMERIC_VALUE = null;
                                                _objLs.SaveChanges();
                                            }
                                            else
                                            {
                                                decimal numericvalue;
                                                Decimal.TryParse(stringvalue, out numericvalue);
                                                objproductSpecs.NUMERIC_VALUE = numericvalue;
                                                _objLs.SaveChanges();
                                            }
                                        }
                                        else if (attributeType == 3)
                                        {
                                            objproductSpecs.STRING_VALUE = stringvalue;
                                            _objLs.SaveChanges();

                                        }
                                        else
                                        {
                                            if (attributeDatatype.ToLower().Contains("number"))
                                            {
                                                if (string.IsNullOrEmpty(stringvalue))
                                                {
                                                    objproductSpecs.NUMERIC_VALUE = null;
                                                    _objLs.SaveChanges();
                                                }
                                                else
                                                {
                                                    decimal numericvalue;
                                                    Decimal.TryParse(stringvalue, out numericvalue);
                                                    objproductSpecs.NUMERIC_VALUE = numericvalue;
                                                    _objLs.SaveChanges();
                                                }
                                            }
                                            else
                                            {
                                                if (attributesize.Contains('('))
                                                {
                                                    string[] sizearray = attributesize.Split('(');
                                                    string[] size = sizearray[1].Split(')');
                                                    if (textLength <= Convert.ToInt32(size[0]))
                                                    {
                                                        objproductSpecs.STRING_VALUE = stringvalue;
                                                    }
                                                }
                                                else
                                                {
                                                    if (attributeDatatype.Contains("Date"))
                                                    {
                                                        if (attributeDataformat ==
                                                            @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))"
                                                            ||
                                                            attributeDataformat ==
                                                            @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2}\s\w{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))")
                                                        {
                                                            var val = stringvalue.Split('M');
                                                            if (val[1].Trim() == "")
                                                            {
                                                                objproductSpecs.STRING_VALUE = stringvalue;
                                                            }
                                                        }
                                                        else if (attributeDataformat ==
                                                                 @"(?=dd(-|/|.)dd(-|/|.)d{4})(?=.{0}(?:0[1-9]|[12]d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(-|/|.)02(-|/|.)...[13579])(?!29(-|/|.)02(-|/|.)..[13579][048])(?!29(-|/|.)02(-|/|.)..[02468][26])(?!29(-|/|.)02(-|/|.).[13579]00)(?!29(-|/|.)02(-|/|.)[13579][048]00)(?!29(-|/|.)02(-|/|.)[02468][26]00)")
                                                        {
                                                            objproductSpecs.STRING_VALUE = stringvalue;
                                                        }
                                                        else if (attributeDataformat == "System default settings")
                                                        {
                                                            //string sysFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
                                                            //string concat = sysFormat.Substring(3,1);
                                                            //string[] val = stringvalue.Split('/');
                                                            //stringvalue = val[1] + concat + val[0] + concat + val[2];
                                                            objproductSpecs.STRING_VALUE = stringvalue;
                                                        }
                                                        else
                                                        {

                                                            objproductSpecs.STRING_VALUE = stringvalue;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        objproductSpecs.STRING_VALUE = stringvalue;
                                                    }

                                                }
                                            }
                                            _objLs.SaveChanges();
                                        }
                                    }



                                }
                                else
                                {
                                    if (isCalculated)
                                    {
                                        var objproductSpecs =
                                            _objLs.TB_PROD_SPECS.FirstOrDefault(
                                                s => s.PRODUCT_ID == productId && s.ATTRIBUTE_ID == attributeId);
                                        if (objproductSpecs == null)
                                        {
                                            var objTbProdspecs = new TB_PROD_SPECS
                                            {
                                                PRODUCT_ID = productId,
                                                ATTRIBUTE_ID = attributeId,
                                                CREATED_USER = User.Identity.Name,
                                                MODIFIED_USER = User.Identity.Name,
                                                MODIFIED_DATE = DateTime.Now,
                                                CREATED_DATE = DateTime.Now
                                            };
                                            _objLs.TB_PROD_SPECS.Add(objTbProdspecs);
                                            _objLs.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                        //SaveCalculatedProdSpecs(productId, familyId, catalogId);
                    }
                    // }

                }
                // SaveCalculatedProdSpecs(productId, familyId, catalogId);
                _objLs.STP_CATALOGSTUDIO5_ProductAttributeSortOrder(familyId);
                //publish
                //if (objproductpublish != null)
                //{
                //    objproductpublish.PUBLISH = publish;
                //    objproductpublish.PUBLISH2PRINT = publish2Print;
                //    objproductpublish.WORKFLOW_STATUS = workflowstatus;
                //    _objLs.SaveChanges();
                //}
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: SaveproductconfigSpecs", ex);
            }
        }



        [System.Web.Http.HttpPost]
        public string SaveProductSpecs(string category_id, int Parentproduct_Id, bool blurCheck, Object model)
        {
            try
            {
                string message = "";
                var SpaceProvided = _objLs.TB_PLAN
                          .Join(_objLs.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                          .Join(_objLs.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                          .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => x.tcptps.tps.StorageGB).ToList();
                double alotment = (double)SpaceProvided[0];
                var userId = _objLs.Customer_User.Join(_objLs.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();
                double folderSize = 0;
                if (string.IsNullOrEmpty(userId))
                {
                    userId = "Questudio";
                }

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + ""));
                }
                var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "");
                DirectoryInfo dInfo = new DirectoryInfo(path);
                long sizeOfDir = DirectorySize(dInfo, true);
                folderSize = (((double)sizeOfDir) / (double)(1024 * 1024 * 1024));

                double availableSpace = alotment - folderSize;
                if (availableSpace > 0)
                {
                    var usedPercentage = ((folderSize / alotment) * 100);

                    //// Need to work on Save - For Sengottuvel
                    message = "Saved successfully";

                    //***********Need to Get two Object model for Changed attribute  and unchanged attributes*********************
                    string[] getUpdateModel = ((IEnumerable)model).Cast<object>().Select(x => x.ToString()).ToArray();
                    var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(getUpdateModel[0].ToString());
                    var oldChangesAttributes = JsonConvert.DeserializeObject<Dictionary<string, string>>(getUpdateModel[1].ToString());

                    //*****************Remove Html Tag Based on the ckeditor************************************************
                    Dictionary<string, string> removeHtmlTagAttributes = new Dictionary<string, string>();

                    foreach (var removeTagAttribute in values)
                    {
                        var stringvalue23 = HttpUtility.HtmlDecode(removeTagAttribute.Value);


                        if (!string.IsNullOrEmpty(stringvalue23))
                        {
                            if (stringvalue23.ToLower().EndsWith("\n"))
                            {
                                int sd = stringvalue23.LastIndexOf("\n", StringComparison.Ordinal);
                                stringvalue23 = stringvalue23.Remove(sd);
                            }
                            if (stringvalue23.ToLower().EndsWith("\r"))
                            {
                                int sd = stringvalue23.LastIndexOf("\r", StringComparison.Ordinal);
                                stringvalue23 = stringvalue23.Remove(sd);
                            }
                            if (stringvalue23.ToLower().StartsWith("<p>") && stringvalue23.ToLower().EndsWith("</p>"))
                            {
                                int count = 0;
                                int i = 0;
                                while ((i = stringvalue23.IndexOf("<p>", i, System.StringComparison.Ordinal)) != -1)
                                {
                                    i += "<p>".Length;
                                    count++;
                                }
                                if (count == 1)
                                {
                                    int endp = stringvalue23.LastIndexOf("</p>", StringComparison.Ordinal);
                                    stringvalue23 = stringvalue23.Remove(endp);
                                    int p = stringvalue23.IndexOf("<p>", StringComparison.Ordinal);
                                    stringvalue23 = stringvalue23.Remove(p, 3);
                                }
                            }
                            stringvalue23 = stringvalue23.Replace("\r", "");
                            stringvalue23 = stringvalue23.Replace("\n\n", "\n");
                            stringvalue23 = stringvalue23.Replace("\n", "\r\n");
                           // stringvalue23 = stringvalue23.Replace("<br />", "\r\n");
                        }
                        removeHtmlTagAttributes.Add(removeTagAttribute.Key, stringvalue23);

                    }

                    //***************************End***************************/
                    //**********************Get Only Edited Attributes*********/

                    var updatedAttributes = removeHtmlTagAttributes.Except(oldChangesAttributes);

                    //***************************End***************************/

                    //var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(model.ToString());
                    int productId = 0;
                    int catalogId = 0;
                    int familyId = 0;
                    bool publish2Print = true;
                    bool publish2Web = true;
                    bool publish2Export = true;
                    bool publish2PDF = true;
                    bool publish2Portal = true;
                    int workflowstatus = 0;
                    int preVal = 0;
                    int orgVal = 0;
                    string categoryId = string.Empty;

                    if (values.ContainsKey("PRODUCT_ID"))
                    {

                        if (!string.IsNullOrEmpty(values["PRODUCT_ID"]))
                        {
                            productId = Convert.ToInt32(values["PRODUCT_ID"]);
                        }
                    }
                    else if (values.ContainsKey("SUBPRODUCT_ID"))
                    {
                        if (!string.IsNullOrEmpty(values["SUBPRODUCT_ID"]))
                        {
                            productId = Convert.ToInt32(values["SUBPRODUCT_ID"]);
                        }
                    }
                    if (values.ContainsKey("FAMILY_ID"))
                    {
                        if (!string.IsNullOrEmpty(values["FAMILY_ID"]))
                        {
                            familyId = Convert.ToInt32(values["FAMILY_ID"]);
                        }
                    }
                    if (values.ContainsKey("CATALOG_ID"))
                    {
                        if (!string.IsNullOrEmpty(values["CATALOG_ID"]))
                        {
                            catalogId = Convert.ToInt32(values["CATALOG_ID"]);
                        }
                    }
                    if (values.ContainsKey("PUBLISH2WEB"))
                        publish2Web = Convert.ToBoolean(values["PUBLISH2WEB"]);
                    if (values.ContainsKey("PUBLISH2PDF"))
                        publish2PDF = Convert.ToBoolean(values["PUBLISH2PDF"]);
                    if (values.ContainsKey("PUBLISH2PORTAL"))
                        publish2Portal = Convert.ToBoolean(values["PUBLISH2PORTAL"]);
                    if (values.ContainsKey("PUBLISH2EXPORT"))
                        publish2Export = Convert.ToBoolean(values["PUBLISH2EXPORT"]);
                    if (values.ContainsKey("PUBLISH2PRINT"))
                        publish2Print = Convert.ToBoolean(values["PUBLISH2PRINT"]);
                    if (values.ContainsKey("SORT"))
                        preVal = Convert.ToInt32(values["SORT"]);
                    if (category_id.Trim() != "")
                    {
                        var id = category_id.Split('~');
                        categoryId = Convert.ToString(id[0]);
                    }
                    var objproductpublish = _objLs.TB_PROD_FAMILY.FirstOrDefault(p => p.FAMILY_ID == familyId && p.PRODUCT_ID == productId);

                    foreach (var value in updatedAttributes)
                    {

                        if (value.Key == "WORKFLOW STATUS")
                        {
                            var objworkflow = _objLs.TB_WORKFLOW_STATUS.FirstOrDefault(k => k.STATUS_NAME == value.Value);
                            if (objworkflow != null)
                            {
                                workflowstatus = objworkflow.STATUS_CODE;

                            }
                        }
                        if (value.Key.Contains("__"))
                        {
                            var attributeNames = value.Key.Split(new[] { "__" }, StringSplitOptions.None);
                            string attributeName = attributeNames[0];
                            string imageCaption = Convert.ToString(attributeNames[1]);
                            var attributeId = Convert.ToInt32(attributeNames[2]);
                            if (customerId_ == 0)
                            {
                                var customerid = _objLs.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                                if (customerid != null)
                                {
                                    customerId_ = customerid.CustomerId;
                                }
                            }
                            if (attributeId == 0)
                            {
                                var attributenamedetails = _objLs.TB_ATTRIBUTE.Join(_objLs.CUSTOMERATTRIBUTE, ta => ta.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (ta, ca) => new { ta, ca }).Where(x => x.ta.ATTRIBUTE_NAME == attributeName && x.ca.CUSTOMER_ID == customerId_).Select(z => z.ta);
                                if (attributenamedetails.Any())
                                {
                                    var attributenames = attributenamedetails.FirstOrDefault();
                                    if (attributenames != null) attributeId = attributenames.ATTRIBUTE_ID;
                                }
                            }

                            var attributedetails = _objLs.TB_ATTRIBUTE.Find(attributeId);
                            var attributeType = attributedetails.ATTRIBUTE_TYPE;
                            var attributeDatatype = attributedetails.ATTRIBUTE_DATATYPE;
                            var attributeDataformat = attributedetails.ATTRIBUTE_DATAFORMAT;
                            var stringvalue = HttpUtility.HtmlDecode(value.Value);
                            string attributesize = Convert.ToString(attributedetails.ATTRIBUTE_DATATYPE);

                            if (!string.IsNullOrEmpty(stringvalue))
                            {
                                if (stringvalue.ToLower().EndsWith("\n"))
                                {
                                    int sd = stringvalue.LastIndexOf("\n", StringComparison.Ordinal);
                                    stringvalue = stringvalue.Remove(sd);
                                }
                                if (stringvalue.ToLower().EndsWith("\r"))
                                {
                                    int sd = stringvalue.LastIndexOf("\r", StringComparison.Ordinal);
                                    stringvalue = stringvalue.Remove(sd);
                                }
                                if (stringvalue.ToLower().StartsWith("<p>") && stringvalue.ToLower().EndsWith("</p>"))
                                {
                                    int count = 0;
                                    int i = 0;
                                    while ((i = stringvalue.IndexOf("<p>", i, System.StringComparison.Ordinal)) != -1)
                                    {
                                        i += "<p>".Length;
                                        count++;
                                    }
                                    if (count == 1)
                                    {
                                        int endp = stringvalue.LastIndexOf("</p>", StringComparison.Ordinal);
                                        stringvalue = stringvalue.Remove(endp);
                                        int p = stringvalue.IndexOf("<p>", StringComparison.Ordinal);
                                        stringvalue = stringvalue.Remove(p, 3);
                                    }
                                }
                                stringvalue = stringvalue.Replace("\r", "");
                                stringvalue = stringvalue.Replace("\n\n", "\n");
                                stringvalue = stringvalue.Replace("\n", "\r\n");
                                //stringvalue = stringvalue.Replace("<br />", "\r\n");
                            }
                            // int textLength = stringvalue.Length;
                            int textLength = 0;
                            if (!string.IsNullOrEmpty(stringvalue))
                            {
                                textLength = stringvalue.Length;
                            }
                            if (attributeType != 6)
                            {
                                var objproductSpecs = _objLs.TB_PROD_SPECS.FirstOrDefault(s => s.PRODUCT_ID == productId && s.ATTRIBUTE_ID == attributeId);
                                var subproductcheck = _objLs.TB_SUBPRODUCT.Where(a => a.SUBPRODUCT_ID == productId);
                                if (objproductSpecs == null)
                                {
                                    SaveProdSpecforNull(stringvalue, attributeType, attributeDatatype, productId, attributeId, attributesize, textLength, attributeDataformat, imageCaption, string.Empty);
                                }
                                else
                                {
                                    if (attributeType == 1 && attributeId == 1 && !subproductcheck.Any())
                                    {
                                        if (blurCheck)
                                        {
                                            var itemnocheck = _objLs.TB_PROD_SPECS.Join(_objLs.TB_CATALOG_PRODUCT, a => a.PRODUCT_ID, b => b.PRODUCT_ID, (a, b) => new { a, b }).Where(x => x.a.ATTRIBUTE_ID == 1 && x.a.STRING_VALUE == stringvalue && x.b.CATALOG_ID == catalogId);
                                            if (!itemnocheck.Any())
                                            {
                                                objproductSpecs.STRING_VALUE = stringvalue;
                                                _objLs.SaveChanges();
                                            }
                                            else if (objproductSpecs.STRING_VALUE != stringvalue)
                                                message = "" + attributeName + " Already exists";
                                        }
                                        else
                                        {
                                            objproductSpecs.STRING_VALUE = stringvalue;
                                        }
                                    }
                                    else if (attributeType == 1 && attributeId == 1 && subproductcheck.Any())
                                    {
                                        if (blurCheck)
                                        {
                                            var itemnocheck = _objLs.TB_PROD_SPECS.Where(x => x.ATTRIBUTE_ID == 1 && x.STRING_VALUE == stringvalue);
                                            if (!itemnocheck.Any())
                                            {
                                                objproductSpecs.STRING_VALUE = stringvalue;
                                                _objLs.SaveChanges();
                                            }
                                            else if (objproductSpecs.STRING_VALUE != stringvalue)
                                                message = "" + attributeName + " Already exists";
                                        }
                                        else
                                        {
                                            objproductSpecs.STRING_VALUE = stringvalue;
                                        }
                                    }
                                    else if (attributeType == 4)
                                    {
                                        if (string.IsNullOrEmpty(stringvalue))
                                        {
                                            objproductSpecs.NUMERIC_VALUE = null;
                                            _objLs.SaveChanges();
                                        }
                                        else
                                        {
                                            decimal numericvalue;
                                            Decimal.TryParse(stringvalue, out numericvalue);
                                            objproductSpecs.NUMERIC_VALUE = numericvalue;
                                            _objLs.SaveChanges();
                                        }
                                    }
                                    else if (attributeType == 3 && imageCaption == "OBJ")
                                    {
                                        if (!string.IsNullOrEmpty(stringvalue))
                                        {
                                            if (stringvalue.Contains(@"//Content") ||
                                                stringvalue.Contains(@"//ImageandAttFile"))
                                            {
                                                objproductSpecs.STRING_VALUE = objproductSpecs.STRING_VALUE;
                                            }
                                            else
                                            {
                                                string[] aa = stringvalue.Split('\\');
                                                string objectType = string.Empty;
                                                foreach (var img in aa)
                                                {
                                                    if (img.Contains("."))
                                                    {
                                                        objectType = img.Split('.')[1];
                                                    }
                                                }
                                                objproductSpecs.OBJECT_TYPE = objectType;
                                                objproductSpecs.STRING_VALUE = stringvalue;
                                            }
                                        }
                                        objproductSpecs.STRING_VALUE = stringvalue;
                                        _objLs.SaveChanges();

                                    }
                                    else if (attributeType == 3 && imageCaption != "OBJ")
                                    {
                                        var objprodspecsimagecaption = _objLs.TB_PROD_SPECS.FirstOrDefault(x => x.PRODUCT_ID == productId && x.ATTRIBUTE_ID == attributeId);
                                        if (objprodspecsimagecaption == null) continue;
                                        objprodspecsimagecaption.OBJECT_NAME = stringvalue;
                                        _objLs.SaveChanges();
                                    }
                                    else
                                    {
                                        if (attributeDatatype.ToLower().Contains("number"))
                                        {
                                            if (string.IsNullOrEmpty(stringvalue))
                                            {
                                                objproductSpecs.NUMERIC_VALUE = null;
                                                _objLs.SaveChanges();
                                            }
                                            else
                                            {
                                                decimal numericvalue;
                                                Decimal.TryParse(stringvalue, out numericvalue);
                                                objproductSpecs.NUMERIC_VALUE = numericvalue;
                                                _objLs.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            if (attributesize.Contains('('))
                                            {
                                                string[] sizearray = attributesize.Split('(');
                                                string[] size = sizearray[1].Split(')');
                                                if (textLength <= Convert.ToInt32(size[0]))
                                                {
                                                    objproductSpecs.STRING_VALUE = stringvalue;
                                                }
                                            }
                                            else
                                            {
                                                if (attributeDatatype.Contains("Date"))
                                                {
                                                    if (attributeDataformat == @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))"
                                                        || attributeDataformat == @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2}\s\w{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))")
                                                    {
                                                        if (!string.IsNullOrEmpty(stringvalue))
                                                        {
                                                            var val = stringvalue.Split('M');
                                                            if (val[1].Trim() == "")
                                                            {
                                                                objproductSpecs.STRING_VALUE = stringvalue;
                                                            }
                                                        }
                                                    }
                                                    else if (attributeDataformat == @"(?=dd(-|/|.)dd(-|/|.)d{4})(?=.{0}(?:0[1-9]|[12]d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(-|/|.)02(-|/|.)...[13579])(?!29(-|/|.)02(-|/|.)..[13579][048])(?!29(-|/|.)02(-|/|.)..[02468][26])(?!29(-|/|.)02(-|/|.).[13579]00)(?!29(-|/|.)02(-|/|.)[13579][048]00)(?!29(-|/|.)02(-|/|.)[02468][26]00)")
                                                    {
                                                        objproductSpecs.STRING_VALUE = stringvalue;
                                                    }
                                                    else if (attributeDataformat == "System default settings")
                                                    {
                                                        objproductSpecs.STRING_VALUE = stringvalue;
                                                    }
                                                    else
                                                    {

                                                        objproductSpecs.STRING_VALUE = stringvalue;
                                                    }
                                                }
                                                else
                                                {
                                                    objproductSpecs.STRING_VALUE = stringvalue;
                                                }

                                            }
                                        }
                                        objproductSpecs.MODIFIED_USER = User.Identity.Name;
                                        objproductSpecs.MODIFIED_DATE = DateTime.Now;
                                        _objLs.SaveChanges();
                                    }
                                }
                            }

                            else
                            {
                                var subproduct_check = _objLs.TB_SUBPRODUCT.Where(a => a.PRODUCT_ID == Parentproduct_Id && a.SUBPRODUCT_ID == productId);
                                if (!subproduct_check.Any())
                                {
                                    var objpartskey = _objLs.TB_PARTS_KEY.FirstOrDefault(x => x.PRODUCT_ID == productId && x.ATTRIBUTE_ID == attributeId && x.FAMILY_ID == familyId && x.CATALOG_ID == catalogId && x.CATEGORY_ID == categoryId);
                                    if (objpartskey != null)
                                    {
                                        if (!string.IsNullOrEmpty(stringvalue))
                                        {
                                            textLength = stringvalue.Length;
                                        }
                                        if (attributeDatatype.Contains('(') && !attributeDatatype.Contains(","))
                                        {
                                            string[] sizearray = attributeDatatype.Split('(');
                                            string[] size = sizearray[1].Split(')');
                                            if (textLength <= Convert.ToInt32(size[0]))
                                            {
                                                objpartskey.ATTRIBUTE_VALUE = stringvalue;
                                                _objLs.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            objpartskey.ATTRIBUTE_VALUE = stringvalue;
                                            _objLs.SaveChanges();
                                        }
                                        //objpartskey.ATTRIBUTE_VALUE = stringvalue;
                                        //_objLs.SaveChanges();

                                    }
                                    else
                                    {
                                        var objparts = new TB_PARTS_KEY
                                        {
                                            ATTRIBUTE_ID = attributeId,
                                            PRODUCT_ID = productId,
                                            FAMILY_ID = familyId,
                                            ATTRIBUTE_VALUE = stringvalue,
                                            CREATED_USER = User.Identity.Name,
                                            CREATED_DATE = DateTime.Now,
                                            MODIFIED_USER = User.Identity.Name,
                                            MODIFIED_DATE = DateTime.Now,
                                            CATALOG_ID = catalogId,
                                            CATEGORY_ID = categoryId
                                        };
                                        _objLs.TB_PARTS_KEY.Add(objparts);
                                        _objLs.SaveChanges();
                                    }
                                }

                                else
                                {

                                    var objsubprokey_check = _objLs.TB_SUBPRODUCT_KEY.FirstOrDefault(a => a.CATALOG_ID == catalogId && a.CATEGORY_ID == categoryId && a.FAMILY_ID == familyId && a.PRODUCT_ID == Parentproduct_Id && a.SUBPRODUCT_ID == productId && a.ATTRIBUTE_ID == attributeId);
                                    if (objsubprokey_check != null)
                                    {
                                        if (!string.IsNullOrEmpty(stringvalue))
                                        {
                                            textLength = stringvalue.Length;
                                        }
                                        if (attributeDatatype.Contains('(') && !attributeDatatype.Contains(","))
                                        {
                                            string[] sizearray = attributeDatatype.Split('(');
                                            string[] size = sizearray[1].Split(')');
                                            if (textLength <= Convert.ToInt32(size[0]))
                                            {
                                                objsubprokey_check.ATTRIBUTE_VALUE = stringvalue;
                                                _objLs.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            objsubprokey_check.ATTRIBUTE_VALUE = stringvalue;
                                            _objLs.SaveChanges();
                                        }
                                        //objsubprokey_check.ATTRIBUTE_VALUE = stringvalue;
                                        //_objLs.SaveChanges();
                                    }
                                    else
                                    {
                                        var objparts = new TB_SUBPRODUCT_KEY
                                        {
                                            ATTRIBUTE_ID = attributeId,
                                            PRODUCT_ID = Parentproduct_Id,
                                            SUBPRODUCT_ID = productId,
                                            FAMILY_ID = familyId,
                                            ATTRIBUTE_VALUE = stringvalue,
                                            CREATED_USER = User.Identity.Name,
                                            CREATED_DATE = DateTime.Now,
                                            MODIFIED_USER = User.Identity.Name,
                                            MODIFIED_DATE = DateTime.Now,
                                            CATALOG_ID = catalogId,
                                            CATEGORY_ID = categoryId
                                        };
                                        _objLs.TB_SUBPRODUCT_KEY.Add(objparts);
                                        _objLs.SaveChanges();
                                    }
                                }
                            }

                        }

                        // }


                    }
                    //SaveCalculatedProdSpecs(productId, familyId, catalogId);
                    //publish
                    if (objproductpublish != null)
                    {
                        var catalogProd = _objLs.TB_CATALOG_PRODUCT.FirstOrDefault(x => x.CATALOG_ID == catalogId && x.PRODUCT_ID == productId);
                        if (catalogProd == null && (publish2Web || publish2Print))
                        {
                            var catProdParts = new TB_CATALOG_PRODUCT
                            {
                                PRODUCT_ID = productId,
                                CATALOG_ID = catalogId,
                                CREATED_USER = User.Identity.Name,
                                CREATED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now
                            };
                            _objLs.TB_CATALOG_PRODUCT.Add(catProdParts);
                            _objLs.SaveChanges();
                        }
                        objproductpublish.PUBLISH2WEB = publish2Web;
                        objproductpublish.PUBLISH = publish2Web;
                        objproductpublish.PUBLISH2EXPORT = publish2Export;
                        objproductpublish.PUBLISH2PDF = publish2PDF;
                        objproductpublish.PUBLISH2PORTAL = publish2Portal;
                        objproductpublish.PUBLISH2PRINT = publish2Print;
                        objproductpublish.WORKFLOW_STATUS = workflowstatus;
                        objproductpublish.MODIFIED_USER = User.Identity.Name;
                        objproductpublish.MODIFIED_DATE = DateTime.Now;
                        _objLs.SaveChanges();

                        var objfamily = _objLs.TB_FAMILY.FirstOrDefault(p => p.FAMILY_ID == familyId);
                        objfamily.MODIFIED_DATE = DateTime.Now;
                        objfamily.MODIFIED_USER = User.Identity.Name;
                        _objLs.SaveChanges();


                        var objProducts = _objLs.TB_PROD_FAMILY.Where(p => p.FAMILY_ID == familyId).OrderBy(x => x.SORT_ORDER).ToArray();
                        orgVal = objproductpublish.SORT_ORDER;
                        int startVal = 0;
                        int endval = 0;
                        for (int index = 0; index < objProducts.Length; index++)
                        {
                            if (orgVal.ToString() == objProducts[index].SORT_ORDER.ToString())
                            {
                                startVal = index;
                            }
                            if (preVal.ToString() == objProducts[index].SORT_ORDER.ToString())
                            {
                                endval = index;
                            }

                        }
                        if (orgVal < preVal)
                        {

                            for (int i = startVal + 1; i <= endval; i++)
                            {
                                int prodid = objProducts[i].PRODUCT_ID;
                                int famid = objProducts[i].FAMILY_ID;
                                var level1 = _objLs.TB_PROD_FAMILY.FirstOrDefault(a => a.FAMILY_ID == famid && a.PRODUCT_ID == prodid);
                                if (level1 != null)
                                {
                                    level1.SORT_ORDER = level1.SORT_ORDER - 1;
                                    _objLs.SaveChanges();
                                }



                            }
                            var level2 = _objLs.TB_PROD_FAMILY.FirstOrDefault(a => a.FAMILY_ID == familyId && a.PRODUCT_ID == productId);
                            if (level2 != null)
                            {
                                level2.SORT_ORDER = preVal;
                                _objLs.SaveChanges();
                            }

                        }
                        if (orgVal > preVal)
                        {

                            for (int i = endval; i < startVal; i++)
                            {
                                int prodid = objProducts[i].PRODUCT_ID;
                                int famid = objProducts[i].FAMILY_ID;
                                var level3 = _objLs.TB_PROD_FAMILY.FirstOrDefault(a => a.FAMILY_ID == famid && a.PRODUCT_ID == prodid);
                                if (level3 != null)
                                {
                                    level3.SORT_ORDER = objProducts[i + 1].SORT_ORDER;
                                    _objLs.SaveChanges();
                                }
                            }

                            var level4 = _objLs.TB_PROD_FAMILY.FirstOrDefault(a => a.FAMILY_ID == familyId && a.PRODUCT_ID == productId);
                            if (level4 != null)
                            {
                                level4.SORT_ORDER = preVal;
                                _objLs.SaveChanges();
                            }
                        }
                    }
                    else
                    {
                        var objsubProducts =
                            _objLs.TB_SUBPRODUCT.Where(p => p.PRODUCT_ID == Parentproduct_Id && p.SUBPRODUCT_ID == productId && p.CATALOG_ID == catalogId);

                        // 
                        if (objsubProducts.Any())
                        {

                            var objProducts = _objLs.TB_SUBPRODUCT.Where(p => p.PRODUCT_ID == Parentproduct_Id && p.CATALOG_ID == catalogId).OrderBy(x => x.SORT_ORDER).ToArray();
                            var objsubproductpublish = objsubProducts.FirstOrDefault();
                            orgVal = objsubproductpublish.SORT_ORDER;
                            var objsubproductpublishOptions = _objLs.TB_SUBPRODUCT.FirstOrDefault(p => p.PRODUCT_ID == Parentproduct_Id && p.SUBPRODUCT_ID == productId && p.CATALOG_ID == catalogId);
                            objsubproductpublishOptions.PUBLISH2EXPORT = publish2Export;
                            objsubproductpublishOptions.PUBLISH2PDF = publish2PDF;
                            objsubproductpublishOptions.PUBLISH2PORTAL = publish2Portal;
                            objsubproductpublishOptions.PUBLISH2PRINT = publish2Print;
                            objsubproductpublishOptions.PUBLISH2WEB = publish2Web;
                            objsubproductpublishOptions.MODIFIED_DATE = DateTime.Now;
                            _objLs.SaveChanges();
                            // var objProducts = objsubProducts.ToArray();
                            int startVal = 0;
                            int endval = 0;
                            for (int index = 0; index < objProducts.Length; index++)
                            {
                                if (orgVal.ToString() == objProducts[index].SORT_ORDER.ToString())
                                {
                                    startVal = index;
                                }
                                if (preVal.ToString() == objProducts[index].SORT_ORDER.ToString())
                                {
                                    endval = index;
                                }

                            }
                            if (orgVal < preVal)
                            {
                                for (int i = startVal + 1; i <= endval; i++)
                                {
                                    int prodid = objProducts[i].PRODUCT_ID;
                                    int famid = objProducts[i].SUBPRODUCT_ID;
                                    var level1 =
                                        _objLs.TB_SUBPRODUCT.FirstOrDefault(
                                            a => a.SUBPRODUCT_ID == famid && a.PRODUCT_ID == prodid && a.CATALOG_ID == catalogId);
                                    if (level1 != null)
                                    {
                                        level1.SORT_ORDER = level1.SORT_ORDER - 1;
                                        _objLs.SaveChanges();
                                    }

                                }
                                var level2 =
                                    _objLs.TB_SUBPRODUCT.FirstOrDefault(
                                        a => a.PRODUCT_ID == Parentproduct_Id && a.SUBPRODUCT_ID == productId && a.CATALOG_ID == catalogId);
                                if (level2 != null)
                                {
                                    level2.SORT_ORDER = preVal;
                                    _objLs.SaveChanges();
                                }

                            }
                            if (orgVal > preVal)
                            {
                                for (int i = endval; i < startVal; i++)
                                {
                                    int prodid = objProducts[i].PRODUCT_ID;
                                    int famid = objProducts[i].SUBPRODUCT_ID;
                                    var level3 =
                                        _objLs.TB_SUBPRODUCT.FirstOrDefault(
                                            a => a.SUBPRODUCT_ID == famid && a.PRODUCT_ID == prodid && a.CATALOG_ID == catalogId);
                                    if (level3 != null)
                                    {
                                        level3.SORT_ORDER = objProducts[i + 1].SORT_ORDER;
                                        _objLs.SaveChanges();
                                    }

                                }

                                var level4 =
                                    _objLs.TB_SUBPRODUCT.FirstOrDefault(
                                        a => a.SUBPRODUCT_ID == productId && a.PRODUCT_ID == Parentproduct_Id && a.CATALOG_ID == catalogId);
                                if (level4 != null)
                                {
                                    level4.SORT_ORDER = preVal;
                                    _objLs.SaveChanges();
                                }
                            }
                        }
                    }
                    var AssetLimitPercentage = _objLs.Customer_Settings.Where(x => x.CustomerId == 1);
                    if (AssetLimitPercentage.Any())
                    {
                        var assetLimitPercentageno = AssetLimitPercentage.FirstOrDefault();
                        double asset = Convert.ToDouble(assetLimitPercentageno.AssetLimitPercentage);
                        if (usedPercentage >= asset)
                        {
                            message = string.Format(message + ".You are about to reach the maximum memory");
                        }
                    }
                }
                else
                {
                    message = "You have reached the maximum allotted memory, please contact Administrator";
                }

                return message;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: SaveProductSpecs", ex);
                return "Product Specifications are not updated";
            }
        }

        public void SaveProdSpecforNull(string stringvalue, int attributeType, string attributeDatatype, int productId, int attributeId, string attributesize, int textLength, string attributeDataformat, string imageCaption, string value)
        {

            string valuefornew = string.Empty;
            if (attributeType == 1)
            {
                if (attributeDatatype.ToLower().Contains("number"))
                {
                    if (string.IsNullOrEmpty(stringvalue))
                    {
                        var objprodspecs = new TB_PROD_SPECS
                        {
                            STRING_VALUE = null,
                            NUMERIC_VALUE = null,
                            PRODUCT_ID = productId,
                            ATTRIBUTE_ID = attributeId,
                            CREATED_DATE = DateTime.Now,
                            CREATED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name
                        };
                        _objLs.TB_PROD_SPECS.Add(objprodspecs);
                    }
                    else
                    {
                        decimal numericvalue;
                        Decimal.TryParse(stringvalue, out numericvalue);
                        var objprodspecs = new TB_PROD_SPECS
                        {
                            STRING_VALUE = null,
                            NUMERIC_VALUE = numericvalue,
                            PRODUCT_ID = productId,
                            ATTRIBUTE_ID = attributeId,
                            CREATED_DATE = DateTime.Now,
                            CREATED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name
                        };
                        _objLs.TB_PROD_SPECS.Add(objprodspecs);
                    }
                }
                else
                {
                    if (attributesize.Contains('('))
                    {
                        string[] sizearray = attributesize.Split('(');
                        string[] size = sizearray[1].Split(')');
                        if (textLength <= Convert.ToInt32(size[0]))
                        {
                            var objprodspecs = new TB_PROD_SPECS
                            {
                                STRING_VALUE = stringvalue,
                                NUMERIC_VALUE = null,
                                PRODUCT_ID = productId,
                                ATTRIBUTE_ID = attributeId,
                                CREATED_DATE = DateTime.Now,
                                CREATED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name
                            };
                            _objLs.TB_PROD_SPECS.Add(objprodspecs);
                        }
                    }
                    else
                    {
                        if (attributeDatatype.Contains("Date"))
                        {
                            if (attributeDataformat == @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))"
                                || attributeDataformat == @"(?=\d\d(\-|\/|\.)\d\d(\-|\/|\.)\d{4}\s\d{2}(\-|\:)\d\d(\-|\:)\d{2}\s\w{2})(?=.{0}(?:0[1-9]|[12]\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\-|\/|\.)02(\-|\/|\.)...[13579])(?!29(\-|\/|\.)02(\-|\/|\.)..[13579][048])(?!29(\-|\/|\.)02(\-|\/|\.)..[02468][26])(?!29(\-|\/|\.)02(\-|\/|\.).[13579]00)(?!29(\-|\/|\.)02(\-|\/|\.)[13579][048]00)(?!29(\-|\/|\.)02(\-|\/|\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))")
                            {
                                var val = stringvalue.Split('M');
                                if (val[1].Trim() == "")
                                {
                                    var objprodspecs = new TB_PROD_SPECS
                                    {
                                        STRING_VALUE = stringvalue,
                                        NUMERIC_VALUE = null,
                                        PRODUCT_ID = productId,
                                        ATTRIBUTE_ID = attributeId,
                                        CREATED_DATE = DateTime.Now,
                                        CREATED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now,
                                        MODIFIED_USER = User.Identity.Name
                                    };
                                    _objLs.TB_PROD_SPECS.Add(objprodspecs);
                                }
                            }
                            else if (attributeDataformat == @"(?=dd(-|/|.)dd(-|/|.)d{4})(?=.{0}(?:0[1-9]|[12]d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(-|/|.)02(-|/|.)...[13579])(?!29(-|/|.)02(-|/|.)..[13579][048])(?!29(-|/|.)02(-|/|.)..[02468][26])(?!29(-|/|.)02(-|/|.).[13579]00)(?!29(-|/|.)02(-|/|.)[13579][048]00)(?!29(-|/|.)02(-|/|.)[02468][26]00)")
                            {
                                var objprodspecs = new TB_PROD_SPECS
                                {
                                    STRING_VALUE = stringvalue,
                                    NUMERIC_VALUE = null,
                                    PRODUCT_ID = productId,
                                    ATTRIBUTE_ID = attributeId,
                                    CREATED_DATE = DateTime.Now,
                                    CREATED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now,
                                    MODIFIED_USER = User.Identity.Name
                                };
                                _objLs.TB_PROD_SPECS.Add(objprodspecs);
                            }
                            else if (attributeDataformat == "System default settings")
                            {
                                var objprodspecs = new TB_PROD_SPECS
                                {
                                    STRING_VALUE = stringvalue,
                                    NUMERIC_VALUE = null,
                                    PRODUCT_ID = productId,
                                    ATTRIBUTE_ID = attributeId,
                                    CREATED_DATE = DateTime.Now,
                                    CREATED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now,
                                    MODIFIED_USER = User.Identity.Name
                                };
                                _objLs.TB_PROD_SPECS.Add(objprodspecs);
                            }
                            else
                            {

                                var objprodspecs = new TB_PROD_SPECS
                                {
                                    STRING_VALUE = stringvalue,
                                    NUMERIC_VALUE = null,
                                    PRODUCT_ID = productId,
                                    ATTRIBUTE_ID = attributeId,
                                    CREATED_DATE = DateTime.Now,
                                    CREATED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now,
                                    MODIFIED_USER = User.Identity.Name
                                };
                                _objLs.TB_PROD_SPECS.Add(objprodspecs);
                            }
                        }
                        else
                        {
                            var objprodspecs = new TB_PROD_SPECS
                            {
                                STRING_VALUE = stringvalue,
                                NUMERIC_VALUE = null,
                                PRODUCT_ID = productId,
                                ATTRIBUTE_ID = attributeId,
                                CREATED_DATE = DateTime.Now,
                                CREATED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                MODIFIED_USER = User.Identity.Name
                            };
                            _objLs.TB_PROD_SPECS.Add(objprodspecs);
                        }

                    }
                }
            }
            else if (attributeType == 4)
            {
                decimal numericvalue;
                Decimal.TryParse(stringvalue, out numericvalue);
                var objprodspecs = new TB_PROD_SPECS
                {
                    STRING_VALUE = null,
                    NUMERIC_VALUE = numericvalue,
                    PRODUCT_ID = productId,
                    ATTRIBUTE_ID = attributeId,
                    CREATED_DATE = DateTime.Now,
                    CREATED_USER = User.Identity.Name,
                    MODIFIED_DATE = DateTime.Now,
                    MODIFIED_USER = User.Identity.Name
                };
                _objLs.TB_PROD_SPECS.Add(objprodspecs);
            }
            else if (attributeType == 3 && imageCaption == "OBJ")
            {
                if (!string.IsNullOrEmpty(stringvalue))
                {
                    if (stringvalue.Contains(@"//Content") || stringvalue.Contains(@"//ImageandAttFile"))
                    {
                        var objprodspecs = new TB_PROD_SPECS
                        {
                            STRING_VALUE = null,
                            NUMERIC_VALUE = null,
                            PRODUCT_ID = productId,
                            ATTRIBUTE_ID = attributeId,
                            CREATED_DATE = DateTime.Now,
                            CREATED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name
                        };
                        _objLs.TB_PROD_SPECS.Add(objprodspecs);
                    }

                    else
                    {
                        var objprodspecs = new TB_PROD_SPECS
                        {
                            STRING_VALUE = stringvalue,
                            NUMERIC_VALUE = null,
                            PRODUCT_ID = productId,
                            ATTRIBUTE_ID = attributeId,
                            CREATED_DATE = DateTime.Now,
                            CREATED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name
                        };
                        _objLs.TB_PROD_SPECS.Add(objprodspecs);
                    }
                }
                else
                {
                    var objprodspecs = new TB_PROD_SPECS
                    {
                        STRING_VALUE = null,
                        NUMERIC_VALUE = null,
                        PRODUCT_ID = productId,
                        ATTRIBUTE_ID = attributeId,
                        CREATED_DATE = DateTime.Now,
                        CREATED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now,
                        MODIFIED_USER = User.Identity.Name
                    };
                    _objLs.TB_PROD_SPECS.Add(objprodspecs);
                }
            }
            _objLs.SaveChanges();
        }

        [System.Web.Http.HttpPost]
        public void SaveFamilySpecs(int catalogid, string Category_id, object model)
        {
            try
            {
                int textLength = 0;
                if (!string.IsNullOrEmpty(Category_id))
                {
                    var id = Category_id.Split('~');
                    Category_id = Convert.ToString(id[0]);
                }
                else
                {
                    Category_id = "0";
                }





                //***********Need to Get two Object model for Changed attribute  and unchanged attributes*********************
                string[] getUpdateModel = ((IEnumerable)model).Cast<object>().Select(x => x.ToString()).ToArray();
                var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(getUpdateModel[0].ToString());
                var oldChangesFamilyAttributes = JsonConvert.DeserializeObject<Dictionary<string, string>>(getUpdateModel[1].ToString());

                //*****************Remove Html Tag Based on the ckeditor************************************************
                Dictionary<string, string> removeHtmlTagFamilyAttributes = new Dictionary<string, string>();

                foreach (var removeTagAttribute in values)
                {
                    var stringvalue23 = HttpUtility.HtmlDecode(removeTagAttribute.Value);


                    if (!string.IsNullOrEmpty(stringvalue23))
                    {
                        if (stringvalue23.ToLower().EndsWith("\n"))
                        {
                            int sd = stringvalue23.LastIndexOf("\n", StringComparison.Ordinal);
                            stringvalue23 = stringvalue23.Remove(sd);
                        }
                        if (stringvalue23.ToLower().EndsWith("\r"))
                        {
                            int sd = stringvalue23.LastIndexOf("\r", StringComparison.Ordinal);
                            stringvalue23 = stringvalue23.Remove(sd);
                        }
                        if (stringvalue23.ToLower().StartsWith("<p>") && stringvalue23.ToLower().EndsWith("</p>"))
                        {
                            int count = 0;
                            int i = 0;
                            while ((i = stringvalue23.IndexOf("<p>", i, System.StringComparison.Ordinal)) != -1)
                            {
                                i += "<p>".Length;
                                count++;
                            }
                            if (count == 1)
                            {
                                int endp = stringvalue23.LastIndexOf("</p>", StringComparison.Ordinal);
                                stringvalue23 = stringvalue23.Remove(endp);
                                int p = stringvalue23.IndexOf("<p>", StringComparison.Ordinal);
                                stringvalue23 = stringvalue23.Remove(p, 3);
                            }
                        }
                        stringvalue23 = stringvalue23.Replace("\r", "");
                        stringvalue23 = stringvalue23.Replace("\n\n", "\n");
                        stringvalue23 = stringvalue23.Replace("\n", "\r\n");
                        //stringvalue23 = stringvalue23.Replace("<br />", "\r\n");
                    }
                    removeHtmlTagFamilyAttributes.Add(removeTagAttribute.Key, stringvalue23);

                }

                //***************************End***************************/
                //**********************Get Only Edited Attributes*********/

                var updatedFamilyAttributes = removeHtmlTagFamilyAttributes.Except(oldChangesFamilyAttributes);

                //***************************End***************************/







                //  var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(model.ToString());
                int familyId = 0;
                int catalogId;
                if (values.ContainsKey("FAMILY_ID"))
                {
                    if (!string.IsNullOrEmpty(values["FAMILY_ID"]))
                    {
                        familyId = Convert.ToInt32(values["FAMILY_ID"]);
                    }
                }
                //if (values.ContainsKey("CATALOG_ID"))
                //{
                //if (catalogid != null)
                //{
                catalogId = catalogid;
                // }
                //}
                foreach (var value in updatedFamilyAttributes)
                {
                    if (value.Key.Contains("__"))
                    {
                        var attributeNames = value.Key.Split(new[] { "__" }, StringSplitOptions.None);
                        // var attributeNames = value.Key.Split('_');
                        // string attributeName = attributeNames[0];
                        var attributeType = attributeNames[3];
                        var attributeId = Convert.ToInt32(attributeNames[2]);
                        string imageCaption = Convert.ToString(attributeNames[1]);
                        var attributenamedetails = _objLs.TB_ATTRIBUTE.Find(attributeId);

                        //attributeId = attributenames.ATTRIBUTE_ID;


                        var attributeDatatype = attributenamedetails.ATTRIBUTE_DATATYPE;
                        var stringvalue = HttpUtility.HtmlDecode(value.Value);

                        if (!string.IsNullOrEmpty(stringvalue))
                        {
                            if (stringvalue.ToLower().EndsWith("\n"))
                            {
                                int sd = stringvalue.LastIndexOf("\n", StringComparison.Ordinal);
                                stringvalue = stringvalue.Remove(sd);
                            }
                            if (stringvalue.ToLower().EndsWith("\r"))
                            {
                                int sd = stringvalue.LastIndexOf("\r", StringComparison.Ordinal);
                                stringvalue = stringvalue.Remove(sd);
                            }
                            if (stringvalue.ToLower().StartsWith("<p>") && stringvalue.ToLower().EndsWith("</p>"))
                            {
                                int count = 0;
                                int i = 0;
                                while ((i = stringvalue.IndexOf("<p>", i, System.StringComparison.Ordinal)) != -1)
                                {
                                    i += "<p>".Length;
                                    count++;
                                }
                                if (count == 1)
                                {
                                    int endp = stringvalue.LastIndexOf("</p>", StringComparison.Ordinal);
                                    stringvalue = stringvalue.Remove(endp);
                                    int p = stringvalue.IndexOf("<p>", StringComparison.Ordinal);
                                    stringvalue = stringvalue.Remove(p, 3);
                                }
                            }

                            stringvalue = stringvalue.Replace("\r", "");
                            stringvalue = stringvalue.Replace("\n\n", "\n");
                            stringvalue = stringvalue.Replace("\n", "\r\n");
                           // stringvalue = stringvalue.Replace("<br />", "\r\n");

                        }
                        if (!string.IsNullOrEmpty(stringvalue))
                        {
                            textLength = stringvalue.Length;
                        }
                        var objfamilySpecs = _objLs.TB_FAMILY_SPECS.FirstOrDefault(s => s.FAMILY_ID == familyId && s.ATTRIBUTE_ID == attributeId);
                        if (attributeType != "13")
                        {
                            if (objfamilySpecs != null)
                            {
                                if (attributeType == "12")
                                {
                                    if (string.IsNullOrEmpty(stringvalue))
                                    {
                                        objfamilySpecs.NUMERIC_VALUE = null;
                                        _objLs.SaveChanges();
                                    }
                                    else
                                    {
                                        decimal numericvalue;
                                        Decimal.TryParse(stringvalue, out numericvalue);
                                        objfamilySpecs.NUMERIC_VALUE = numericvalue;
                                        _objLs.SaveChanges();
                                    }
                                }
                                else
                                {
                                    if (attributeDatatype.ToLower().Contains("number"))
                                    {
                                        if (string.IsNullOrEmpty(stringvalue))
                                        {
                                            objfamilySpecs.NUMERIC_VALUE = null;
                                            _objLs.SaveChanges();
                                        }
                                        else
                                        {
                                            decimal numericvalue;
                                            Decimal.TryParse(stringvalue, out numericvalue);
                                            objfamilySpecs.NUMERIC_VALUE = numericvalue;
                                            _objLs.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        if (attributeDatatype.Contains('('))
                                        {
                                            string[] sizearray = attributeDatatype.Split('(');
                                            string[] size = sizearray[1].Split(')');
                                            if (textLength <= Convert.ToInt32(size[0]))
                                            {
                                                objfamilySpecs.STRING_VALUE = stringvalue;
                                                _objLs.SaveChanges();
                                            }
                                        }
                                        else if (attributeType == "9" && imageCaption == "OBJ")
                                        {

                                            objfamilySpecs.STRING_VALUE = stringvalue;
                                            _objLs.SaveChanges();

                                        }
                                        else if (attributeType == "9" && imageCaption != "OBJ")
                                        {
                                            objfamilySpecs.OBJECT_NAME = stringvalue;
                                            _objLs.SaveChanges();
                                        }
                                        else
                                        {
                                            objfamilySpecs.STRING_VALUE = stringvalue;
                                            _objLs.SaveChanges();
                                        }
                                    }
                                }

                            }
                        }
                        else
                        {
                            var objfamilykey = _objLs.TB_FAMILY_KEY.FirstOrDefault(x => x.ATTRIBUTE_ID == attributeId && x.FAMILY_ID == familyId && x.CATALOG_ID == catalogId && x.CATEGORY_ID == Category_id);
                            if (objfamilykey == null)
                            {
                                //objfamilykey.ATTRIBUTE_VALUE = stringvalue;
                                //_objLs.SaveChanges();

                                var objFamilypecs = new TB_FAMILY_KEY
                                {
                                    CATALOG_ID = catalogId,
                                    ATTRIBUTE_ID = attributeId,
                                    FAMILY_ID = familyId,
                                    ATTRIBUTE_VALUE = stringvalue,
                                    CREATED_USER = User.Identity.Name,
                                    MODIFIED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now,
                                    CREATED_DATE = DateTime.Now,
                                    CATEGORY_ID = Category_id
                                };

                                _objLs.TB_FAMILY_KEY.Add(objFamilypecs);
                                _objLs.SaveChanges();
                            }
                            else
                            {
                                if (attributeDatatype.Contains('(') && !attributeDatatype.Contains(","))
                                {
                                    string[] sizearray = attributeDatatype.Split('(');
                                    string[] size = sizearray[1].Split(')');
                                    if (textLength <= Convert.ToInt32(size[0]))
                                    {
                                        objfamilykey.ATTRIBUTE_VALUE = stringvalue;
                                        _objLs.SaveChanges();
                                    }
                                }
                                else
                                {
                                    objfamilykey.ATTRIBUTE_VALUE = stringvalue;
                                    _objLs.SaveChanges();
                                }
                            }
                        }
                    }
                }
                // SaveCalculatedFamilySpecs(familyId, catalogId);


            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: SaveFamilySpecs", ex);
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage CreateNewGroupFromMultipleTable(string newGroupName, int familyId, int catalogId)
        {
            try
            {
                if (!String.IsNullOrEmpty(newGroupName))
                {
                    int group = _objLs.TB_ATTRIBUTE_GROUP_SECTIONS.Count(a => a.TB_ATTRIBUTE_GROUP.GROUP_NAME == newGroupName && a.FAMILY_ID == familyId && a.CATALOG_ID == catalogId);
                    if (group == 0)
                    {
                        //var objTbAttributeGroup = new TB_ATTRIBUTE_GROUP
                        //{
                        //    GROUP_NAME = newGroupName,
                        //    MODIFIED_USER = User.Identity.Name,
                        //    CREATED_USER = User.Identity.Name,
                        //    MODIFIED_DATE = DateTime.Now,
                        //    CREATEED_DATE = DateTime.Now,
                        //    LAYOUT = "Horizontal"
                        //};
                        //_objLs.TB_ATTRIBUTE_GROUP.Add(objTbAttributeGroup);
                        //_objLs.SaveChanges();
                        _objLs.STP_CATALOGSTUDIO5_AttributeGroup(newGroupName, catalogId, Convert.ToString(familyId), 0, 0, "", "GROUPINSERT", 0, "Horizontal");
                        var tbmultiplefamstru = _objLs.TB_ATTRIBUTE_GROUP_SECTIONS.FirstOrDefault(a => a.TB_ATTRIBUTE_GROUP.GROUP_NAME == newGroupName && a.FAMILY_ID == familyId && a.CATALOG_ID == catalogId);
                        if (tbmultiplefamstru != null)
                        {
                            int groupId = tbmultiplefamstru.GROUP_ID;
                            var multiplegroupdelete = _objLs.STP_LS_MULTIPLETABLE(newGroupName, groupId, "INSERT");
                        }
                        _objLs.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Duplicate);
                }
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Error);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  CreateNewGroupFromMultipleTable", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        [System.Web.Http.HttpPost]
        public HttpResponseMessage CreateNewGroupFromMultipleTableSections(int groupId, int catalogId, int familyId)
        {
            try
            {
                var objTbAttributeGroupSections = new TB_ATTRIBUTE_GROUP_SECTIONS
                {
                    GROUP_ID = groupId,
                    CATALOG_ID = catalogId,
                    FAMILY_ID = familyId,
                    MODIFIED_USER = User.Identity.Name,
                    CREATED_USER = User.Identity.Name,
                    MODIFIED_DATE = DateTime.Now,
                    CREATED_DATE = DateTime.Now
                };
                _objLs.TB_ATTRIBUTE_GROUP_SECTIONS.Add(objTbAttributeGroupSections);
                _objLs.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);

            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  CreateNewGroupFromMultipleTableSections", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage RemoveGroupFromMultipleTable(int groupId, int catalogId)
        {
            try
            {
                var removegroups = _objLs.TB_ATTRIBUTE_GROUP.Where(x => x.GROUP_ID == groupId);
                if (removegroups.Any())
                {
                    var removegroup = removegroups.FirstOrDefault();
                    _objLs.TB_ATTRIBUTE_GROUP.Remove(removegroup);
                    _objLs.SaveChanges();

                    _objLs.STP_LS_MULTIPLETABLE("", groupId, "DELETE");
                    _objLs.SaveChanges();
                }

                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  RemoveGroupFromMultipleTable", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [System.Web.Http.HttpGet]
        public IList GetGroupTableDetails(string familyId, int catalogId)
        {
            try
            {
                int famID;
                int.TryParse(familyId.Trim('~'), out famID);
                var catalogs = _objLs.TB_ATTRIBUTE_GROUP_SECTIONS.Join(_objLs.TB_ATTRIBUTE_GROUP, a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { a, b })
                    .Where(x => x.a.FAMILY_ID == famID && x.a.CATALOG_ID == catalogId).OrderBy(x => x.b.GROUP_NAME)
                    //.Where(x => x.FAMILY_ID == 36 &&  !x.CATALOG_NAME.ToLower().Contains("master")).OrderBy(x => x.CATALOG_NAME)
                    .Select(x => new
                    {
                        x.a.GROUP_ID,
                        x.b.GROUP_NAME,
                        x.b.LAYOUT
                    }).ToList();
                return catalogs;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at  FamilyApiController:  GetGroupTableDetails", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetGroupLayout()
        {
            try
            {

                // var catalogs =new List<string, string>();
                var catalogs = new List<TB_ATTRIBUTE_GROUP>();
                var attrgroup = new TB_ATTRIBUTE_GROUP();
                attrgroup.LAYOUT = "Horizontal";
                catalogs.Add(attrgroup);
                attrgroup = new TB_ATTRIBUTE_GROUP();
                attrgroup.LAYOUT = "Vertical";
                catalogs.Add(attrgroup);
                return catalogs;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at  FamilyApiController:  GetGroupTableDetails", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetMultipleTableLeftTreeEntities(int catalogId, string familyId, int attributeType, int groupId)
        {
            try
            {
                int famID;
                int.TryParse(familyId.Trim('~'), out famID);
                return GetCategoriesForTree(catalogId, famID, attributeType, groupId);
            }
            catch (Exception objException)
            {
                Logger.Error("Error at  FamilyApiController:  GetMultipleTableLeftTreeEntities", objException);
                return null;
            }
        }

        public IList GetCategoriesForTree(int catalogId, int familyId, int attributeType, int groupId)
        {
            try
            {
                var customerid = _objLs.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                if (customerid != null)
                {
                    customerId_ = customerid.CustomerId;
                }
                var selectedattributes = _objLs.TB_ATTRIBUTE_GROUP
                    .Join(_objLs.TB_ATTRIBUTE_GROUP_SECTIONS, a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { a, b })
                  .Join(_objLs.TB_ATTRIBUTE_GROUP_SPECS, y => y.b.GROUP_ID, z => z.GROUP_ID, (y, z) => new { y, z })
                  .Join(_objLs.TB_ATTRIBUTE, d => d.z.ATTRIBUTE_ID, d => d.ATTRIBUTE_ID, (c, d) => new { c, d })
                  .Join(_objLs.CUSTOMERATTRIBUTE, y => y.d.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (y, ca) => new { y, ca })
                  .Where(
                  x => x.y.c.z.GROUP_ID == groupId
                      && x.y.c.y.b.FAMILY_ID == familyId
                      && x.y.c.y.b.CATALOG_ID == catalogId
                      && x.ca.CUSTOMER_ID == customerId_
                      && !(x.y.d.ATTRIBUTE_TYPE == 7)
                      && !(x.y.d.ATTRIBUTE_TYPE == 9)
                      && !(x.y.d.ATTRIBUTE_TYPE == 11)
                      && !(x.y.d.ATTRIBUTE_TYPE == 12)
                      && !(x.y.d.ATTRIBUTE_TYPE == 13)
                      )
                  .Select(s => new
                  {
                      s.y.d.ATTRIBUTE_TYPE,
                      s.y.d.ATTRIBUTE_ID,
                      s.y.d.ATTRIBUTE_NAME,
                      s.y.c.y.b.CATALOG_ID,
                      s.y.c.y.b.FAMILY_ID,
                      ISAvailable = true,
                      s.y.c.z.SORT_ORDER
                  }).Distinct().ToList();
                //return selectedattributes;

                if (attributeType == 0)
                {
                    var attributes = _objLs.TB_ATTRIBUTE
                        .Join(_objLs.TB_PROD_FAMILY_ATTR_LIST, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a.ATTRIBUTE_TYPE, a.ATTRIBUTE_ID, a.ATTRIBUTE_NAME, b.FAMILY_ID })
                 .Join(_objLs.TB_CATALOG_FAMILY, a => a.FAMILY_ID, b => b.FAMILY_ID, (a, b) => new { a.ATTRIBUTE_ID, a.ATTRIBUTE_NAME, a.ATTRIBUTE_TYPE, a.FAMILY_ID, b.CATALOG_ID, b.CATEGORY_ID })
                 .Join(_objLs.TB_CATALOG_ATTRIBUTES, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a, b }).Join(_objLs.CUSTOMERATTRIBUTE, z => z.b.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (z, ca) => new { z, ca })
                 .Where(
                 x => x.z.a.FAMILY_ID == familyId
                     && x.z.a.CATALOG_ID == catalogId
                     && x.z.b.CATALOG_ID == catalogId
                     && x.ca.CUSTOMER_ID == customerId_
                     && x.z.a.ATTRIBUTE_TYPE != 7
                      && x.z.a.ATTRIBUTE_TYPE != 9
                       && x.z.a.ATTRIBUTE_TYPE != 11
                        && x.z.a.ATTRIBUTE_TYPE != 12
                         && x.z.a.ATTRIBUTE_TYPE != 13
                     )
             .Select(s => new
             {
                 s.z.a.ATTRIBUTE_TYPE,
                 s.z.a.ATTRIBUTE_ID,
                 s.z.a.ATTRIBUTE_NAME,
                 s.z.a.CATALOG_ID,
                 s.z.a.FAMILY_ID,
                 ISAvailable = false
             }).Distinct().ToList();

                    foreach (var items in selectedattributes)
                    {
                        attributes.Remove(attributes.SingleOrDefault(x => x.ATTRIBUTE_ID == items.ATTRIBUTE_ID));
                    }

                    // var res = prodfamilyAttributes.Union(prodspecAttributes);
                    //return res.OrderBy(y => y.SORT_ORDER).ToList();

                    return attributes;
                }
                else
                {
                    var attributes = _objLs.TB_ATTRIBUTE
                        .Join(_objLs.TB_PROD_FAMILY_ATTR_LIST, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a.ATTRIBUTE_TYPE, a.ATTRIBUTE_ID, a.ATTRIBUTE_NAME, b.FAMILY_ID })
                   .Join(_objLs.TB_CATALOG_FAMILY, a => a.FAMILY_ID, b => b.FAMILY_ID, (a, b) => new { a.ATTRIBUTE_ID, a.ATTRIBUTE_NAME, a.ATTRIBUTE_TYPE, a.FAMILY_ID, b.CATALOG_ID, b.CATEGORY_ID })
                   .Join(_objLs.TB_CATALOG_ATTRIBUTES, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a, b }).Join(_objLs.CUSTOMERATTRIBUTE, z => z.b.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (z, ca) => new { z, ca })
                   .Where(
                   x => x.z.a.ATTRIBUTE_TYPE == attributeType
                       && x.z.a.FAMILY_ID == familyId
                       && x.z.a.CATALOG_ID == catalogId
                       && x.z.b.CATALOG_ID == catalogId
                       && x.ca.CUSTOMER_ID == customerId_
                       && x.z.a.ATTRIBUTE_TYPE != 7
                        && x.z.a.ATTRIBUTE_TYPE != 9
                         && x.z.a.ATTRIBUTE_TYPE != 11
                          && x.z.a.ATTRIBUTE_TYPE != 12
                           && x.z.a.ATTRIBUTE_TYPE != 13
                       )
               .Select(s => new
               {
                   s.z.a.ATTRIBUTE_TYPE,
                   s.z.a.ATTRIBUTE_ID,
                   s.z.a.ATTRIBUTE_NAME,
                   s.z.a.CATALOG_ID,
                   s.z.a.FAMILY_ID,
                   ISAvailable = false
               }).Distinct().ToList();

                    foreach (var items in selectedattributes)
                    {
                        attributes.Remove(attributes.SingleOrDefault(x => x.ATTRIBUTE_ID == items.ATTRIBUTE_ID));
                    }
                    return attributes;
                }



                //var Attributes = objLS.TB_ATTRIBUTE.Where(a => a.ATTRIBUTE_TYPE == Attributeid).Select(s => new LS_TB_ATTRIBUTE
                //var Attributes = objLS.TB_ATTRIBUTE.Join(objLS.TB_PROD_FAMILY_ATTR_LIST,a=>a.ATTRIBUTE_ID,b=>b.ATTRIBUTE_ID,(a,b)=>new {a.ATTRIBUTE_TYPE,a.ATTRIBUTE_ID,a.ATTRIBUTE_NAME})


            }
            catch (Exception objException)
            {
                Logger.Error("Error at  FamilyApiController:  GetCategoriesForTree", objException);
                return null;
            }

        }

        [System.Web.Http.HttpGet]
        public IList GetAllattributesMultipleTable(int groupId, int familyId, int catalogId)
        {
            try
            {
                if (customerId_ == 0)
                {
                    var customerid = _objLs.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                    if (customerid != null)
                    {
                        customerId_ = customerid.CustomerId;
                    }
                }

                var attributes = _objLs.TB_ATTRIBUTE_GROUP.Join(_objLs.TB_ATTRIBUTE_GROUP_SECTIONS, a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { a, b })
                        .Join(_objLs.TB_ATTRIBUTE_GROUP_SPECS, y => y.b.GROUP_ID, z => z.GROUP_ID, (y, z) => new { y, z })
                        .Join(_objLs.TB_ATTRIBUTE, d => d.z.ATTRIBUTE_ID, d => d.ATTRIBUTE_ID, (c, d) => new { c, d }).Join(_objLs.CUSTOMERATTRIBUTE, ca => ca.d.ATTRIBUTE_ID, caca => caca.ATTRIBUTE_ID, (ca, caca) => new { ca, caca })
                        .Where(x => x.ca.c.z.GROUP_ID == groupId && x.ca.c.y.b.FAMILY_ID == familyId && x.ca.c.y.b.CATALOG_ID == catalogId && x.caca.CUSTOMER_ID == customerId_)
                        .Select(s => new
                        {
                            s.ca.d.ATTRIBUTE_TYPE,
                            s.ca.d.ATTRIBUTE_ID,
                            s.ca.d.ATTRIBUTE_NAME,
                            s.ca.c.y.b.CATALOG_ID,
                            s.ca.c.y.b.FAMILY_ID,
                            ISAvailable = true,
                            s.ca.c.z.SORT_ORDER
                        });
                return attributes.Distinct().ToList();
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: GetAllattributesMultipleTable", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage UpdateGlobalAttrValueForSelectedFamily(int catalogId, string attrType, string attrId, string attrValue, object model) // 
        {
            try
            {
                string result = string.Empty;
                string[] categoryIds;
                string category_Ids = string.Empty;

                string[] familyIds = Convert.ToString(model).Split(',');
                _gaattrid = Convert.ToInt16(attrId);
                int workingCatalogID1 = catalogId;
                if (attrType != null)
                {
                    string id = string.Empty;
                    for (int i = 0; i < familyIds.Length; i++)
                    {
                        if (Convert.ToString(familyIds[i]).Contains('~'))
                        {
                            id = id + "," + Convert.ToString(familyIds[i].Trim('~'));
                        }
                        else
                        {
                            var cat_det = _objLs.Category_Function(workingCatalogID1, Convert.ToString(familyIds[i]));
                            if (cat_det.Any())
                            {
                                foreach (var item in cat_det)
                                {
                                    string category_id = item.CATEGORY_ID;
                                    if (!category_Ids.Contains(category_id))
                                        category_Ids = category_Ids + category_id + ",";
                                    var family_ids = _objLs.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == category_id && a.CATALOG_ID == workingCatalogID1);
                                    if (family_ids.Any())
                                    {
                                        foreach (var family in family_ids)
                                        {
                                            id = id + "," + Convert.ToString(family.FAMILY_ID);

                                        }
                                    }
                                }
                            }
                        }
                    }
                    result = UpdateAttrValues(workingCatalogID1, id.TrimStart(','), Convert.ToInt32(attrId), attrValue, category_Ids);
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  UpdateGlobalAttrValueForSelectedFamily", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        public string UpdateAttrValues(int workingCatalogID1, string familyIds, int attrId, string attrValue, string category_Ids)
        {
            var dtfamilyID = new DataTable();
            dtfamilyID.Columns.Add("Family_id");
            dtfamilyID.Columns.Add("CATEGORY_ID");
            string[] familyId = familyIds.Split(',');
            for (int ind = 0; ind < familyId.Length; ind++)
            {
                // dtfamilyID.Rows.Add(familyId[ind]);    
                if (familyId[ind] != "")
                {
                    DataSet cat = new DataSet();
                    _sqlString = "SELECT CATEGORY_ID FROM TB_CATALOG_FAMILY WHERE FAMILY_ID = " + familyId[ind] + " and catalog_id = " + workingCatalogID1 + "";
                    cat = CreateDataSet();
                    if (cat.Tables != null && cat.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow rr in cat.Tables[0].Rows)
                        {
                            string ccatid = rr["CATEGORY_ID"].ToString();
                            dtfamilyID.Rows.Add(familyId[ind], ccatid);
                        }

                    }
                }

            }
            //catalog_attributes check
            var catalogAttributes = _objLs.TB_CATALOG_ATTRIBUTES.FirstOrDefault(a => a.ATTRIBUTE_ID == _gaattrid && a.CATALOG_ID == workingCatalogID1);
            if (catalogAttributes == null)
            {
                var objTbcatalogattributes = new TB_CATALOG_ATTRIBUTES
                {
                    CATALOG_ID = workingCatalogID1,
                    ATTRIBUTE_ID = _gaattrid,
                    CREATED_USER = User.Identity.Name,
                    CREATED_DATE = DateTime.Now,
                    MODIFIED_USER = User.Identity.Name,
                    MODIFIED_DATE = DateTime.Now
                };
                _objLs.TB_CATALOG_ATTRIBUTES.Add(objTbcatalogattributes);
                _objLs.SaveChanges();
            }
            _sqlString = " select attribute_type from tb_attribute ta join CUSTOMERATTRIBUTE ca on ca.attribute_id=ta.attribute_id where ta.attribute_id=" + attrId + "";
            DataSet atttyp = CreateDataSet();
            string typecheck = atttyp.Tables[0].Rows[0][0].ToString();
            if (!string.IsNullOrEmpty(attrValue))
            {
                if (typecheck == "6" && typecheck != "7" && typecheck != "11" && typecheck != "12" && typecheck != "13" && typecheck != "4" && typecheck != "21" && typecheck != "23" && typecheck != "25")
                {
                    for (int s = 0; s < dtfamilyID.Rows.Count; s++)
                    {
                        string gafamid = dtfamilyID.Rows[s][0].ToString();
                        string Cate_id = dtfamilyID.Rows[s][1].ToString();
                        _sqlString = " select product_id from TB_PROD_FAMILY where family_id=" + gafamid + " ";
                        DataSet pkch = CreateDataSet();
                        for (int k = 0; k < pkch.Tables[0].Rows.Count; k++)
                        {
                            string productIDGa = pkch.Tables[0].Rows[k][0].ToString();
                            _sqlString = "select product_id  from tb_parts_key where family_id =" + gafamid + " and catalog_id =" + workingCatalogID1 + " and attribute_id =" + _gaattrid + " and product_id=" + productIDGa + " and category_id ='" + Cate_id + "'";
                            DataSet updatekey = CreateDataSet();
                            if (updatekey.Tables[0].Rows.Count > 0)
                            {
                                _sqlString = "update tb_parts_key set attribute_value ='" + attrValue + "' where product_id in (select  product_id from tb_parts_key where family_id=" + gafamid + " and catalog_id =" + workingCatalogID1 + " and attribute_id=" + _gaattrid + "  )and  attribute_id=" + _gaattrid + " and catalog_id =" + workingCatalogID1 + "";
                                ExecuteSqlQuery();
                            }
                            else
                            {
                                _sqlString = " insert into tb_parts_key values(" + _gaattrid + "," + productIDGa + "," + gafamid + ",'" + attrValue + "',SUSER_NAME(),getdate(),SUSER_NAME(),getdate()," + workingCatalogID1 + ",'" + Cate_id + "')";
                                ExecuteSqlQuery();
                            }
                            //// tb_prod_family_attr_list check
                            PublishAttributes(gafamid, productIDGa, _gaattrid);
                        }

                    }

                }


                if (typecheck != "6" && typecheck != "7" && typecheck != "11" && typecheck != "12" && typecheck != "13" && typecheck != "4" && typecheck != "21" && typecheck != "23" && typecheck != "25")
                {
                    for (int s = 0; s < dtfamilyID.Rows.Count; s++)
                    {
                        string gafamid = dtfamilyID.Rows[s][0].ToString();
                        _sqlString = "select product_id from TB_PROD_FAMILY where family_id=" + gafamid + "";
                        DataSet prid1 = CreateDataSet();
                        DataSet Dataformat = new DataSet();
                        string format = string.Empty;
                        _sqlString = "select attribute_datatype from tb_attribute ta join CUSTOMERATTRIBUTE ca on ca.attribute_id=ta.attribute_id where ta.attribute_id = " + _gaattrid + "";
                        Dataformat = CreateDataSet();
                        foreach (DataRow ropid1 in prid1.Tables[0].Rows)
                        {
                            string productIDGa1 = ropid1["product_id"].ToString();
                            _sqlString = "select product_id from tb_prod_specs where product_id =" + productIDGa1 + " and attribute_id =" + _gaattrid + "";
                            DataSet prodnum = CreateDataSet();
                            if (prodnum.Tables[0].Rows.Count == 0)
                            {
                                _sqlString = "INSERT INTO TB_PROD_SPECS VALUES('" + attrValue + "'," + productIDGa1 + "," + _gaattrid + ",NULL,NULL,NULL,SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                ExecuteSqlQuery();
                            }
                            else
                            {
                                if (Dataformat.Tables != null && Dataformat.Tables[0].Rows.Count > 0)
                                {
                                    format = Dataformat.Tables[0].Rows[0][0].ToString();
                                }
                                if (format.Contains("Num"))
                                {

                                    _sqlString = "update tb_prod_specs set Numeric_value = " + attrValue + " where  product_id in (select product_id from TB_PROD_FAMILY where family_id=" + gafamid + ") and attribute_id =" + _gaattrid + "";
                                    ExecuteSqlQuery();
                                }
                                else
                                {
                                    _sqlString = "update tb_prod_specs set string_value ='" + attrValue + "' where  product_id in (select product_id from TB_PROD_FAMILY where family_id=" + gafamid + ") and attribute_id =" + _gaattrid + "";
                                    ExecuteSqlQuery();
                                }
                            }
                            //// tb_prod_family_attr_list check
                            PublishAttributes(gafamid, productIDGa1, _gaattrid);
                        }

                    }
                }


                if (typecheck == "7" || typecheck == "11" || typecheck == "12")
                {
                    for (int s = 0; s < dtfamilyID.Rows.Count; s++)
                    {
                        string gafamid = dtfamilyID.Rows[s][0].ToString();
                        _sqlString = "update tb_family_specs set string_value='" + attrValue + "' where family_id =" + gafamid + " and attribute_id = " + _gaattrid + "";
                        ExecuteSqlQuery();
                    }
                }

                if (typecheck == "13")
                {
                    for (int s = 0; s < dtfamilyID.Rows.Count; s++)
                    {
                        string gafamid = dtfamilyID.Rows[s][0].ToString();
                        _sqlString = "update tb_family_key set attribute_value='" + attrValue + "' where family_id =" + gafamid + " and catalog_id =" + workingCatalogID1 + " and attribute_id =" + _gaattrid + "";
                        ExecuteSqlQuery();
                    }
                }
                if (typecheck == "4")
                {
                    for (int s = 0; s < dtfamilyID.Rows.Count; s++)
                    {
                        string gafamid = dtfamilyID.Rows[s][0].ToString();
                        _sqlString = "select product_id from TB_PROD_FAMILY where family_id=" + gafamid + "";
                        DataSet prid = CreateDataSet();
                        foreach (DataRow ropid in prid.Tables[0].Rows)
                        {
                            string productIDGa = ropid["product_id"].ToString();
                            _sqlString = "select product_id from tb_prod_specs where product_id =" + productIDGa + " and attribute_id =" + _gaattrid + "";
                            DataSet prodnum = CreateDataSet();
                            if (prodnum.Tables[0].Rows.Count == 0)
                            {
                                _sqlString = "INSERT INTO TB_PROD_SPECS VALUES(NULL," + productIDGa + "," + _gaattrid + "," + attrValue + ",NULL,NULL,SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                ExecuteSqlQuery();
                            }
                            else
                            {
                                _sqlString = "update tb_prod_specs set numeric_value ='" + attrValue + "' where  product_id in (select product_id from TB_PROD_FAMILY where family_id=" + gafamid + ") and attribute_id =" + _gaattrid + "";
                                ExecuteSqlQuery();
                            }
                            PublishAttributes(gafamid, productIDGa, _gaattrid);
                        }
                    }
                }

                // Start Category level Attributes

                int textLength = 0;
                if (!string.IsNullOrEmpty(attrValue))
                {
                    textLength = attrValue.Length;
                }

                if (typecheck == "21" || typecheck == "23" || typecheck == "25")
                {

                    category_Ids = category_Ids.Substring(0, category_Ids.LastIndexOf(','));

                    string[] categoryIds = category_Ids.Split(',');
                    foreach (var id in categoryIds)
                    {
                        string categoryId = id;
                        var attributenamedetails = _objLs.TB_ATTRIBUTE.Find(attrId);
                        var attributeDatatype = attributenamedetails.ATTRIBUTE_DATATYPE;
                        var objcategorySpecs = _objLs.TB_CATEGORY_SPECS.FirstOrDefault(s => s.CATEGORY_ID == categoryId && s.ATTRIBUTE_ID == attrId);
                        if (objcategorySpecs != null)
                        {
                            if (attributeDatatype.ToLower().Contains("number"))
                            {
                                if (string.IsNullOrEmpty(attrValue))
                                {
                                    objcategorySpecs.NUMERIC_VALUE = null;
                                    _objLs.SaveChanges();
                                }
                                else
                                {
                                    decimal numericvalue;
                                    Decimal.TryParse(attrValue, out numericvalue);
                                    objcategorySpecs.NUMERIC_VALUE = numericvalue;
                                    _objLs.SaveChanges();
                                }
                            }
                            else
                            {
                                if (attributeDatatype.Contains('('))
                                {
                                    string[] sizearray = attributeDatatype.Split('(');
                                    string[] size = sizearray[1].Split(')');
                                    if (textLength <= Convert.ToInt32(size[0]))
                                    {
                                        objcategorySpecs.STRING_VALUE = attrValue;
                                        _objLs.SaveChanges();
                                    }
                                }
                                else if (typecheck == "23")
                                {
                                    objcategorySpecs.STRING_VALUE = attrValue;
                                    _objLs.SaveChanges();
                                }
                                else
                                {
                                    objcategorySpecs.STRING_VALUE = attrValue;
                                    _objLs.SaveChanges();
                                }
                            }

                        }
                    }
                }
                // End Category level Attributes

                return ResponseMsg.Duplicate;
            }
            return ResponseMsg.Duplicate;
        }
        //sathya//
        [System.Web.Http.HttpPost]
        public HttpResponseMessage globalattribute(int Catalogid, int Attributetype, string option, int sort, object[] model1)
        {
            try
            {
                string Attrid = string.Empty;
                if (option == "PUBLISH")
                {
                    option = "ADD,PUBLISH";
                }
                string model = string.Empty;
                string[] getUpdateModel = ((IEnumerable)model1).Cast<object>().Select(x => x.ToString()).ToArray();
                var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(getUpdateModel[0].ToString());

                foreach (var Testing in values)
                {
                    if (Testing.Key == "Attribute_id")
                    {
                        Attrid = Testing.Value;
                    }
                    else if (Testing.Key == "Categoryid")
                    {
                        model = Testing.Value;
                    }

                }


                int groupId = 0;


                if ((Attrid).Contains('~'))
                {
                    Attrid = Attrid.Trim('~');
                    groupId = Convert.ToInt32(Attrid);
                    var attributeNames = (from packageDetails in _objLs.TB_PACKAGE_DETAILS
                                          join master in _objLs.TB_PACKAGE_MASTER on packageDetails.GROUP_ID equals master.GROUP_ID
                                          join catalogAttributes in _objLs.TB_CATALOG_ATTRIBUTES on packageDetails.ATTRIBUTE_ID equals catalogAttributes.ATTRIBUTE_ID
                                          join attributes in _objLs.TB_ATTRIBUTE on catalogAttributes.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                          where master.CATALOG_ID == Catalogid && attributes.FLAG_RECYCLE == "A" && packageDetails.GROUP_ID == groupId
                                          select new
                                          {
                                              attributes.ATTRIBUTE_ID,
                                              attributes.ATTRIBUTE_NAME,
                                              attributes.ATTRIBUTE_TYPE,
                                          }).Distinct().ToList();

                    Attrid = "";

                    foreach (var item in attributeNames)
                    {
                        Attrid = Attrid + Convert.ToString(item.ATTRIBUTE_ID) + ',';
                    }
                }

                string category_Ids = string.Empty;
                //if (Attributetype == 25 || Attributetype == 23 || Attributetype == 21)
                //{
                //    categoryIds = Convert.ToString(model).Split(',');
                //    foreach (var item in categoryIds)
                //    {
                //        if (item.Contains("CAT"))
                //            category_Ids = category_Ids + Convert.ToString(item) + ',';
                //    }
                //}

                int customer_Id = 0;
                HomeApiController apiObj = new HomeApiController();
                customer_Id = apiObj.GetCustomerId();

                if (!string.IsNullOrEmpty(model.ToString()))
                {
                    var Passtable = new DataTable();
                    Passtable.Columns.Add("CATEGORY_ID");
                    Passtable.Columns.Add("FAMILY_ID");
                    string[] selectedoption = option.Split(',');
                    string[] familyid = Convert.ToString(model).Split(',');
                    string id = string.Empty;
                    for (int i = 0; i < familyid.Length; i++)
                    {
                        if (Convert.ToString(familyid[i]).Contains('~'))
                        {
                            id = id + "," + Convert.ToString(familyid[i].Trim('~'));

                            // insert record into TB_ATTRIBUTE_HIERARCHY table to associate upcoming Product or Family
                            if (groupId != 0)
                            {
                                string familyIdstring = Convert.ToString(familyid[i].Trim('~'));
                                TB_ATTRIBUTE_HIERARCHY attributeHierarchyInsert = new TB_ATTRIBUTE_HIERARCHY();
                                attributeHierarchyInsert = _objLs.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == familyIdstring && s.PACK_ID == groupId && s.TYPE == "Family").FirstOrDefault();
                                if (attributeHierarchyInsert == null)
                                {
                                    string QryString = "insert into TB_ATTRIBUTE_HIERARCHY values(" + groupId + ",'" + "Family" + "','" + familyIdstring + "'," + customer_Id + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                    SqlConnection conn = new SqlConnection(connectionString);
                                    SqlCommand CmdObj = new SqlCommand(QryString, conn);
                                    conn.Open();
                                    CmdObj.ExecuteNonQuery();
                                    conn.Close();
                                }
                            }

                        }
                        else
                        {
                            var cat_det = _objLs.Category_Function(Catalogid, Convert.ToString(familyid[i]));

                            if (cat_det.Any())
                            {
                                foreach (var item in cat_det)
                                {
                                    string category_id = item.CATEGORY_ID;
                                    if (!category_Ids.Contains(category_id))
                                        category_Ids = category_Ids + category_id + ",";
                                    // insert record into TB_ATTRIBUTE_HIERARCHY table to associate upcoming Product or Family
                                    if (groupId != 0)
                                    {
                                        TB_ATTRIBUTE_HIERARCHY attributeHierarchyInsert = new TB_ATTRIBUTE_HIERARCHY();
                                        attributeHierarchyInsert = _objLs.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == category_id && s.PACK_ID == groupId && s.TYPE == "Category").FirstOrDefault();
                                        if (attributeHierarchyInsert == null)
                                        {
                                            string QryString = "insert into TB_ATTRIBUTE_HIERARCHY values(" + groupId + ",'" + "Category" + "','" + category_id + "'," + customer_Id + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                            SqlConnection conn = new SqlConnection(connectionString);
                                            SqlCommand CmdObj = new SqlCommand(QryString, conn);
                                            conn.Open();
                                            CmdObj.ExecuteNonQuery();
                                            conn.Close();
                                        }
                                    }

                                    var family_ids = _objLs.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == category_id && a.CATALOG_ID == Catalogid);
                                    if (family_ids.Any())
                                    {
                                        foreach (var family in family_ids)
                                        {
                                            id = id + "," + Convert.ToString(family.FAMILY_ID);
                                            string familyIdstring = Convert.ToString(family.FAMILY_ID);
                                            // insert record into TB_ATTRIBUTE_HIERARCHY table to associate upcoming Product or Family
                                            if (groupId != 0)
                                            {
                                                TB_ATTRIBUTE_HIERARCHY attributeHierarchyInsert = new TB_ATTRIBUTE_HIERARCHY();
                                                attributeHierarchyInsert = _objLs.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == familyIdstring && s.PACK_ID == groupId && s.TYPE == "Family").FirstOrDefault();
                                                if (attributeHierarchyInsert == null)
                                                {
                                                    string QryString = "insert into TB_ATTRIBUTE_HIERARCHY values(" + groupId + ",'" + "Family" + "','" + familyIdstring + "'," + customer_Id + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                                    SqlConnection conn = new SqlConnection(connectionString);
                                                    SqlCommand CmdObj = new SqlCommand(QryString, conn);
                                                    conn.Open();
                                                    CmdObj.ExecuteNonQuery();
                                                    conn.Close();
                                                }
                                            }

                                            //-------------------New Global Attributes ------------------                                            
                                            DataSet cat = new DataSet();
                                            _sqlString = "SELECT CATEGORY_ID FROM TB_CATALOG_FAMILY WHERE FAMILY_ID = " + Convert.ToString(family.FAMILY_ID) + " and catalog_id = " + Catalogid + "";
                                            cat = CreateDataSet();
                                            if (cat.Tables != null && cat.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow rr in cat.Tables[0].Rows)
                                                {
                                                    string ccatid = rr["CATEGORY_ID"].ToString();
                                                    Passtable.Rows.Add(ccatid, Convert.ToString(family.FAMILY_ID));
                                                }
                                            }
                                            //--------------END------------------
                                        }
                                    }
                                }
                            }
                        }
                    }


                    using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        conn.Open();
                        _sqlString = "IF OBJECT_ID('TEMPDB..##Global') IS NOT NULL   DROP TABLE  ##Global ";
                        ExecuteSqlQuery();
                        string SQLString = null;
                        _sqlString = CreateTable("[##Global]", Passtable);
                        ExecuteSqlQuery();
                        var bulkCopy_cp = new SqlBulkCopy(conn)
                        {
                            DestinationTableName = "[##Global]"
                        };
                        bulkCopy_cp.WriteToServer(Passtable);
                    }
                    int customerId = 0;
                    var cumstomAttr =
                           _objLs.Customer_User.Where(
                               x => x.User_Name == User.Identity.Name).Select(z => z.CustomerId).ToList();
                    if (cumstomAttr.Any())
                    {
                        int.TryParse(cumstomAttr[0].ToString(), out customerId);
                    }
                    UpdateGlobalAttr(selectedoption, Attrid, Catalogid, Attributetype, sort, id.TrimStart(','), customerId, category_Ids);
                    _objLs.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);


            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  globalattribute", ex);
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Error);
            }
        }
        //public HttpResponseMessage globalattribute(string Attrid, int Catalogid, int Attributetype, string option, int sort, object model)
        //{
        //    try
        //    {
        //        int groupId = 0;
        //        if ((Attrid).Contains('~'))
        //        {
        //            Attrid = Attrid.Trim('~');
        //            groupId = Convert.ToInt32(Attrid);
        //            var attributeNames = (from packageDetails in _objLs.TB_PACKAGE_DETAILS
        //                                  join master in _objLs.TB_PACKAGE_MASTER on packageDetails.GROUP_ID equals master.GROUP_ID
        //                                  join catalogAttributes in _objLs.TB_CATALOG_ATTRIBUTES on packageDetails.ATTRIBUTE_ID equals catalogAttributes.ATTRIBUTE_ID
        //                                  join attributes in _objLs.TB_ATTRIBUTE on catalogAttributes.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
        //                                  where master.CATALOG_ID == Catalogid && attributes.FLAG_RECYCLE == "A" && packageDetails.GROUP_ID == groupId
        //                                  select new
        //                                  {
        //                                      attributes.ATTRIBUTE_ID,
        //                                      attributes.ATTRIBUTE_NAME,
        //                                      attributes.ATTRIBUTE_TYPE,
        //                                  }).Distinct().ToList();

        //            Attrid = "";

        //            foreach (var item in attributeNames)
        //            {
        //                Attrid = Attrid + Convert.ToString(item.ATTRIBUTE_ID) + ',';
        //            }
        //        }

        //        string category_Ids = string.Empty;
        //        //if (Attributetype == 25 || Attributetype == 23 || Attributetype == 21)
        //        //{
        //        //    categoryIds = Convert.ToString(model).Split(',');
        //        //    foreach (var item in categoryIds)
        //        //    {
        //        //        if (item.Contains("CAT"))
        //        //            category_Ids = category_Ids + Convert.ToString(item) + ',';
        //        //    }
        //        //}

        //        int customer_Id = 0;
        //        HomeApiController apiObj = new HomeApiController();
        //        customer_Id = apiObj.GetCustomerId();

        //        if (!string.IsNullOrEmpty(model.ToString()))
        //        {
        //            var Passtable = new DataTable();
        //            Passtable.Columns.Add("CATEGORY_ID");
        //            Passtable.Columns.Add("FAMILY_ID");
        //            string[] selectedoption = option.Split(',');
        //            string[] familyid = Convert.ToString(model).Split(',');
        //            string id = string.Empty;
        //            for (int i = 0; i < familyid.Length; i++)
        //            {
        //                if (Convert.ToString(familyid[i]).Contains('~'))
        //                {
        //                    id = id + "," + Convert.ToString(familyid[i].Trim('~'));

        //                    // insert record into TB_ATTRIBUTE_HIERARCHY table to associate upcoming Product or Family
        //                    if (groupId != 0)
        //                    {
        //                        string familyIdstring = Convert.ToString(familyid[i].Trim('~'));
        //                        TB_ATTRIBUTE_HIERARCHY attributeHierarchyInsert = new TB_ATTRIBUTE_HIERARCHY();
        //                        attributeHierarchyInsert = _objLs.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == familyIdstring && s.PACK_ID == groupId && s.TYPE == "Family").FirstOrDefault();
        //                        if (attributeHierarchyInsert == null)
        //                        {
        //                            string QryString = "insert into TB_ATTRIBUTE_HIERARCHY values(" + groupId + ",'" + "Family" + "','" + familyIdstring + "'," + customer_Id + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
        //                            SqlConnection conn = new SqlConnection(connectionString);
        //                            SqlCommand CmdObj = new SqlCommand(QryString, conn);
        //                            conn.Open();
        //                            CmdObj.ExecuteNonQuery();
        //                            conn.Close();
        //                        }
        //                    }

        //                }
        //                else
        //                {
        //                    var cat_det = _objLs.Category_Function(Catalogid, Convert.ToString(familyid[i]));

        //                    if (cat_det.Any())
        //                    {
        //                        foreach (var item in cat_det)
        //                        {
        //                            string category_id = item.CATEGORY_ID;
        //                            if (!category_Ids.Contains(category_id))
        //                                category_Ids = category_Ids + category_id + ",";
        //                            // insert record into TB_ATTRIBUTE_HIERARCHY table to associate upcoming Product or Family
        //                            if (groupId != 0)
        //                            {
        //                                TB_ATTRIBUTE_HIERARCHY attributeHierarchyInsert = new TB_ATTRIBUTE_HIERARCHY();
        //                                attributeHierarchyInsert = _objLs.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == category_id && s.PACK_ID == groupId && s.TYPE == "Category").FirstOrDefault();
        //                                if (attributeHierarchyInsert == null)
        //                                {
        //                                    string QryString = "insert into TB_ATTRIBUTE_HIERARCHY values(" + groupId + ",'" + "Category" + "','" + category_id + "'," + customer_Id + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
        //                                    SqlConnection conn = new SqlConnection(connectionString);
        //                                    SqlCommand CmdObj = new SqlCommand(QryString, conn);
        //                                    conn.Open();
        //                                    CmdObj.ExecuteNonQuery();
        //                                    conn.Close();
        //                                }
        //                            }

        //                            var family_ids = _objLs.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == category_id && a.CATALOG_ID == Catalogid);
        //                            if (family_ids.Any())
        //                            {
        //                                foreach (var family in family_ids)
        //                                {
        //                                    id = id + "," + Convert.ToString(family.FAMILY_ID);
        //                                    string familyIdstring = Convert.ToString(family.FAMILY_ID);
        //                                    // insert record into TB_ATTRIBUTE_HIERARCHY table to associate upcoming Product or Family
        //                                    if (groupId != 0)
        //                                    {
        //                                        TB_ATTRIBUTE_HIERARCHY attributeHierarchyInsert = new TB_ATTRIBUTE_HIERARCHY();
        //                                        attributeHierarchyInsert = _objLs.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == familyIdstring && s.PACK_ID == groupId && s.TYPE == "Family").FirstOrDefault();
        //                                        if (attributeHierarchyInsert == null)
        //                                        {
        //                                            string QryString = "insert into TB_ATTRIBUTE_HIERARCHY values(" + groupId + ",'" + "Family" + "','" + familyIdstring + "'," + customer_Id + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
        //                                            SqlConnection conn = new SqlConnection(connectionString);
        //                                            SqlCommand CmdObj = new SqlCommand(QryString, conn);
        //                                            conn.Open();
        //                                            CmdObj.ExecuteNonQuery();
        //                                            conn.Close();
        //                                        }
        //                                    }

        //                                    //-------------------New Global Attributes ------------------                                            
        //                                    DataSet cat = new DataSet();
        //                                    _sqlString = "SELECT CATEGORY_ID FROM TB_CATALOG_FAMILY WHERE FAMILY_ID = " + Convert.ToString(family.FAMILY_ID) + " and catalog_id = " + Catalogid + "";
        //                                    cat = CreateDataSet();
        //                                    if (cat.Tables != null && cat.Tables[0].Rows.Count > 0)
        //                                    {
        //                                        foreach (DataRow rr in cat.Tables[0].Rows)
        //                                        {
        //                                            string ccatid = rr["CATEGORY_ID"].ToString();
        //                                            Passtable.Rows.Add(ccatid, Convert.ToString(family.FAMILY_ID));
        //                                        }
        //                                    }
        //                                    //--------------END------------------
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }


        //            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
        //            {
        //                conn.Open();
        //                _sqlString = "IF OBJECT_ID('TEMPDB..##Global') IS NOT NULL   DROP TABLE  ##Global ";
        //                ExecuteSqlQuery();
        //                string SQLString = null;
        //                _sqlString = CreateTable("[##Global]", Passtable);
        //                ExecuteSqlQuery();
        //                var bulkCopy_cp = new SqlBulkCopy(conn)
        //                {
        //                    DestinationTableName = "[##Global]"
        //                };
        //                bulkCopy_cp.WriteToServer(Passtable);
        //            }
        //            int customerId = 0;
        //            var cumstomAttr =
        //                   _objLs.Customer_User.Where(
        //                       x => x.User_Name == User.Identity.Name).Select(z => z.CustomerId).ToList();
        //            if (cumstomAttr.Any())
        //            {
        //                int.TryParse(cumstomAttr[0].ToString(), out customerId);
        //            }
        //            UpdateGlobalAttr(selectedoption, Attrid, Catalogid, Attributetype, sort, id.TrimStart(','), customerId, category_Ids);
        //            _objLs.SaveChanges();
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);


        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Error("Error at FamilyApiController:  globalattribute", ex);
        //        return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Error);
        //    }
        //}
        //sathya//
        public static string CreateTable(string tableName, DataTable table)
        {
            string sqlsc = "CREATE TABLE " + tableName + "(";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                if (!table.Columns[i].ColumnName.Contains("["))
                {
                    sqlsc += "\n [" + table.Columns[i].ColumnName + "] ";
                }
                else
                {
                    sqlsc += "\n" + table.Columns[i].ColumnName + "";
                }
                if (table.Columns[i].DataType.ToString().Contains("System.String"))
                    sqlsc += "nvarchar(max) ";
                else
                    sqlsc += "int";
                sqlsc += ",";
            }
            return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
        }
        public void UpdateGlobalAttr(string[] selectedoption, string Attrid, int Catalogid, int Attributetype, int sort, string family_ids, int customerId, string category_Ids)
        {
            string[] fam_ids = family_ids.Split(',');
            List<int> family_Ids = new List<int>();

            if (category_Ids != null && category_Ids != "")
            {
                category_Ids = category_Ids.Substring(0, category_Ids.LastIndexOf(','));
            }
            foreach (var id in fam_ids)
            {
                int Id = Convert.ToInt32(id);
                family_Ids.Add(Id);
            }

            foreach (string opt in selectedoption)
            {
                if (opt.Equals("REMOVE"))
                {
                    Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                }
                if (opt.Equals("UNPUBLISH"))
                {
                    Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                }
                _objLs.Database.CommandTimeout = 0;
                if (Attributetype != 14 && Attributetype != 21 && Attributetype != 23 && Attributetype != 25)
                {
                    var stp = _objLs.STP_CATALOGSTUDIO5_Global_attribute_manager(Catalogid, family_ids, Attributetype, Attrid, opt, sort, customerId, "");
                }
                else if (Attributetype == 21 || Attributetype == 23 || Attributetype == 25)
                {
                    string[] categoryIds = category_Ids.Split(',');
                    foreach (var id in categoryIds)
                    {
                        string categoryId = id;
                        using (
                          var objSqlConnection =
                              new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_Global_attribute_manager";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.CommandTimeout = 0;
                            objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = Catalogid;
                            objSqlCommand.Parameters.Add("@FAMILY_IDS", SqlDbType.NVarChar).Value = "";
                            objSqlCommand.Parameters.Add("@ATTRIBUTETYPE", SqlDbType.Int).Value = Attributetype;
                            objSqlCommand.Parameters.Add("@ATTR_IDS", SqlDbType.NVarChar).Value = Attrid;
                            objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = opt;
                            objSqlCommand.Parameters.Add("@NEWSORT", SqlDbType.Int).Value = sort;
                            objSqlCommand.Parameters.Add("@CustomerID", SqlDbType.NVarChar).Value = customerId;
                            objSqlCommand.Parameters.Add("@CATEGORY_IDS", SqlDbType.NVarChar).Value = categoryId;
                            objSqlConnection.Open();
                            objSqlCommand.ExecuteNonQuery();
                        }
                    }
                }
                else
                {
                    string[] attr_ids = Attrid.Split(',');

                    foreach (var attr_id in attr_ids)
                    {
                        if (attr_id != "")
                        {
                            int attributeId;
                            int.TryParse(attr_id, out attributeId);
                            int attribute_type = _objLs.TB_ATTRIBUTE.Where(s => s.ATTRIBUTE_ID == attributeId).FirstOrDefault().ATTRIBUTE_TYPE;
                            string attr_Ids = attr_id + ',';
                            if (opt.Equals("REMOVE") || opt.Equals("UNPUBLISH"))
                            {
                                attr_Ids = attr_Ids.Substring(0, attr_Ids.LastIndexOf(','));
                            }

                            var stp = _objLs.STP_CATALOGSTUDIO5_Global_attribute_manager(Catalogid, family_ids, attribute_type, attr_Ids, opt, sort, customerId, "");
                        }
                    }
                }
            }
        }

        public void UpdateGlobalAttrsub(string[] selectedoption, string Attrid, int Catalogid, int Attributetype, int sort, string family_ids)
        {
            string option = string.Empty;
            //string ids = string .Empty;

            foreach (string opt in selectedoption)
            {
                int length = Attrid.Length;
                if (opt.Equals("REMOVE"))
                {
                    option = "SUBPRODUCTSREMOVEPRODUCT";
                    if (Attrid.LastIndexOf(',') == length - 1)
                    {
                        Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                    }
                }
                if (opt.Equals("UNPUBLISH"))
                {
                    option = "SUBPRODUCTSUNPUBLISHPRODUCT";
                    if (Attrid.LastIndexOf(',') == length - 1)
                    {
                        Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                    }
                }
                if (opt.Equals("PUBLISH"))
                {
                    option = "SUBPRODUCTSPUBLISHPRODUCT";
                    if (Attrid.LastIndexOf(',') == length - 1)
                    {
                        Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                    }
                }
                if (opt.Equals("ADD"))
                {
                    option = "SUBPRODUCTSADDPRODUCT";
                    if (Attrid.LastIndexOf(',') == length - 1)
                    {
                        Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                    }
                }
                if (opt.Equals("sortSub"))
                {
                    option = "SUBPRODUCTSSORT";
                    family_ids = family_ids.Replace("#", "");
                    if (Attrid.LastIndexOf(',') > 0)
                    {
                        Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                    }
                }
                if (Attributetype != 7 && Attributetype != 9 && Attributetype != 11 && Attributetype != 12 && Attributetype != 13)
                {

                    string[] atids = Attrid.Split(',');
                    for (int k = 0; k < atids.Length; k++)
                    {
                        // Attrid = atids[k].ToString();
                        _objLs.Database.CommandTimeout = 0;

                        if (Attributetype == 14)
                        {
                            int attribute_type;
                            int attributeId;
                            int.TryParse(atids[k].ToString(), out attributeId);
                            if (atids[k].ToString() != "")
                            {
                                attribute_type = _objLs.TB_ATTRIBUTE.Where(s => s.ATTRIBUTE_ID == attributeId).FirstOrDefault().ATTRIBUTE_TYPE;
                                var stp = _objLs.STP_CATALOGSTUDIO5_Global_attribute_manager(Catalogid, family_ids, attribute_type, atids[k].ToString(), option, sort, 0, "");
                            }
                        }
                        else
                        {
                            var stp = _objLs.STP_CATALOGSTUDIO5_Global_attribute_manager(Catalogid, family_ids, Attributetype, atids[k].ToString(), option, sort, 0, "");
                        }
                    }
                }

            }
        }

        private string attribute_ids = string.Empty;
        public void UpdateGlobalAttrsubmain(string[] selectedoption, string Attrid, int Catalogid, int Attributetype, int sort, string family_ids)
        {
            string option = string.Empty;
            //string ids = string .Empty;

            foreach (string opt in selectedoption)
            {
                if (string.IsNullOrEmpty(attribute_ids))
                {
                    attribute_ids = Attrid;
                }
                else
                {
                    Attrid = attribute_ids;
                }
                if (opt.Equals("REMOVE"))
                {
                    option = "SUBPRODUCTSREMOVE";
                    if (Attrid.LastIndexOf(',') > 0)
                    {
                        Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                    }
                }
                if (opt.Equals("UNPUBLISH"))
                {
                    option = "SUBPRODUCTSUNPUBLISH";
                    if (Attrid.LastIndexOf(',') > 0)
                    {
                        Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                    }
                }
                if (opt.Equals("PUBLISH"))
                {
                    //option = "SUBPRODUCTSPUBLISH";
                    //if (Attrid.LastIndexOf(',') > 0)
                    //{
                    //    Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                    //}

                    using (
                        var objSqlConnection =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_LS_Global_attribute_manager";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.CommandTimeout = 0;
                        objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = Catalogid;
                        objSqlCommand.Parameters.Add("@FAMILY_IDS", SqlDbType.NVarChar).Value = family_ids;
                        objSqlCommand.Parameters.Add("@ATTRIBUTETYPE", SqlDbType.Int).Value = Attributetype;
                        objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "SUBPRODUCTSPUBLISH";
                        objSqlCommand.Parameters.Add("@ATTR_IDS", SqlDbType.NVarChar).Value = Attrid;
                        objSqlCommand.Parameters.Add("@NEWSORT", SqlDbType.Int).Value = 0;
                        objSqlCommand.Parameters.Add("@SSID", SqlDbType.NVarChar).Value = "1";
                        objSqlConnection.Open();
                        objSqlCommand.ExecuteNonQuery();
                    }
                }
                if (opt.Equals("ADD"))
                {
                    option = "SUBPRODUCTSADD";
                    //if (Attrid.LastIndexOf(',') > 0)
                    //{
                    //    Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                    //}

                    using (
                        var objSqlConnection =
                            new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_LS_Global_attribute_manager";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;
                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.CommandTimeout = 0;
                        objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = Catalogid;
                        objSqlCommand.Parameters.Add("@FAMILY_IDS", SqlDbType.NVarChar).Value = family_ids;
                        objSqlCommand.Parameters.Add("@ATTRIBUTETYPE", SqlDbType.Int).Value = Attributetype;
                        objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar).Value = "SUBPRODUCTSADD";
                        objSqlCommand.Parameters.Add("@ATTR_IDS", SqlDbType.NVarChar).Value = Attrid;
                        objSqlCommand.Parameters.Add("@NEWSORT", SqlDbType.Int).Value = 0;
                        objSqlCommand.Parameters.Add("@SSID", SqlDbType.NVarChar).Value = "1";
                        objSqlConnection.Open();
                        objSqlCommand.ExecuteNonQuery();
                    }
                }
                DataSet globalSet = new DataSet();
                if (opt.Equals("sortSub"))
                {
                    _sqlString = "IF OBJECT_ID('TEMPDB..##GlobalSort') IS NOT NULL   select * from  ##GlobalSort ";
                    globalSet = CreateDataSet();
                    option = "SUBPRODUCTSSORT";
                    family_ids = family_ids.Replace("#", "");
                    if (Attrid.LastIndexOf(',') > 0)
                    {
                        Attrid = Attrid.Substring(0, Attrid.LastIndexOf(','));
                    }
                }
                if (!opt.Equals("ADD") && !opt.Equals("PUBLISH"))
                {
                    if (Attributetype != 7 && Attributetype != 9 && Attributetype != 11 && Attributetype != 12 &&
                        Attributetype != 13)
                    {

                        //string[] famids = family_ids.Split(',');

                        if (globalSet != null && globalSet.Tables.Count > 0 && globalSet.Tables[0].Rows.Count > 0)
                        {
                            string[] atids1 = Attrid.Split(',');
                            for (int k = 0; k < atids1.Length; k++)
                            {

                                string _Attrid = atids1[k];
                                _objLs.Database.CommandTimeout = 0;
                                var stp = _objLs.STP_CATALOGSTUDIO5_Global_attribute_manager(Catalogid, "0",
                                    Attributetype,
                                    _Attrid, option, sort, 0, "");
                            }
                        }
                        else
                        {
                            string[] famids = family_ids.Split(',');
                            for (int kq = 0; kq < famids.Length; kq++)
                            {
                                string _family_ids = famids[kq];
                                int familyid = Convert.ToInt32(_family_ids);
                                var subproductchk = _objLs.TB_SUBPRODUCT.Join(_objLs.TB_PROD_FAMILY,
                                    tps => tps.PRODUCT_ID,
                                    tpf => tpf.PRODUCT_ID, (tps, tpf) => new { tps, tpf })
                                    .Where(x => x.tpf.FAMILY_ID == familyid && x.tps.CATALOG_ID == Catalogid);
                                if (subproductchk.Any())
                                {
                                    string[] atids = Attrid.Split(',');
                                    for (int k = 0; k < atids.Length; k++)
                                    {
                                        string _Attrid = atids[k].ToString();
                                        _objLs.Database.CommandTimeout = 0;
                                        var stp = _objLs.STP_CATALOGSTUDIO5_Global_attribute_manager(Catalogid,
                                            _family_ids,
                                            Attributetype, _Attrid, option, sort, 0, "");
                                        DataSet subfamcheck = new DataSet();

                                        _sqlString = " select family_id from tb_family where parent_family_id = " +
                                                     _family_ids + "";
                                        subfamcheck = CreateDataSet();
                                        if (subfamcheck.Tables[0].Rows.Count > 0)
                                        {
                                            foreach (DataRow item in subfamcheck.Tables[0].Rows)
                                            {
                                                _family_ids = item["family_id"].ToString();
                                                _objLs.Database.CommandTimeout = 0;
                                                var stp1 = _objLs.STP_CATALOGSTUDIO5_Global_attribute_manager(Catalogid,
                                                    _family_ids, Attributetype, _Attrid, option, sort, 0, "");
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage globalCalculatedAttribute(int Catalogid, string option, object model)
        {
            try
            {
                string[] familyid = Convert.ToString(model).Split(',');
                string id = string.Empty;
                for (int i = 0; i < familyid.Length; i++)
                {
                    if (Convert.ToString(familyid[i]).Contains('~'))
                    {
                        id = id + "," + Convert.ToString(familyid[i].Trim('~'));
                    }
                    else
                    {
                        var cat_det = _objLs.Category_Function(Catalogid, Convert.ToString(familyid[i]));
                        if (cat_det.Any())
                        {
                            foreach (var item in cat_det)
                            {
                                string category_id = item.CATEGORY_ID;
                                var family_ids = _objLs.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == category_id && a.CATALOG_ID == Catalogid);
                                if (family_ids.Any())
                                {
                                    foreach (var family in family_ids)
                                    {
                                        id = id + "," + Convert.ToString(family.FAMILY_ID);
                                    }
                                }
                            }
                        }
                    }
                }
                int family_id = 0;
                var subproductId = _objLs.TB_PROD_FAMILY.Join(_objLs.TB_SUBPRODUCT, x => x.PRODUCT_ID,
                    xx => xx.PRODUCT_ID, (x, xx) => new { tpf = x, tsp = xx });
                string[] familtIDList = id.TrimStart(',').Split(',');
                int count = 0;
                foreach (var s in familtIDList)
                {
                    int.TryParse(s, out family_id);
                    subproductId = _objLs.TB_PROD_FAMILY.Join(_objLs.TB_SUBPRODUCT, x => x.PRODUCT_ID,
                        xx => xx.PRODUCT_ID, (x, xx) => new { tpf = x, tsp = xx })
                        .Where(tt => tt.tpf.FAMILY_ID == family_id && tt.tsp.CATALOG_ID == Catalogid);
                    if (subproductId.Any())
                    {
                        count = count + 1;
                    }
                }
                if (count > 0)
                {
                    UpdateGlobalAttributes(Catalogid, id.TrimStart(','), "CALCSUB");
                    _objLs.SaveChanges();
                }
                else
                {
                    UpdateGlobalAttributes(Catalogid, id.TrimStart(','), option);
                    _objLs.SaveChanges();
                }
                //UpdateGlobalAttributes(Catalogid, id.TrimStart(','), option);
                //_objLs.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);

            }
            catch (Exception ex)
            {
                Logger.Error("Error at WizardController:  globalCalculatedAttribute", ex);
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Error);
            }
        }


        public void UpdateGlobalAttributes(int catalog_id, string family_ids, string option)
        {
            _objLs.Database.CommandTimeout = 0;
            var stp = _objLs.STP_CATALOGSTUDIO5_Global_attribute_manager(catalog_id, family_ids, 0, "0", option, 0, 0, "");
        }
        public void PublishAttributes(string familyid, string productid, int attrid)
        {
            try
            {
                int productID = Convert.ToInt32(productid);
                int familyID = Convert.ToInt32(familyid);
                var prodFamilyAttrlist = _objLs.TB_PROD_FAMILY_ATTR_LIST.Where(a => a.ATTRIBUTE_ID == attrid && a.FAMILY_ID == familyID && a.PRODUCT_ID == 0);
                if (!prodFamilyAttrlist.Any())
                {
                    var sortOrderCheck = _objLs.TB_PROD_FAMILY_ATTR_LIST.FirstOrDefault(a => a.FAMILY_ID == familyID && a.PRODUCT_ID == 0 && a.ATTRIBUTE_ID == attrid);
                    int sortOrder = 1;
                    if (sortOrderCheck != null)
                    {
                        sortOrder = sortOrderCheck.SORT_ORDER;
                    }
                    else
                    {
                        sortOrder = _objLs.TB_PROD_FAMILY_ATTR_LIST.Where(a => a.FAMILY_ID == familyID && a.PRODUCT_ID == 0).Max(x => x.SORT_ORDER) + 1;
                    }

                    var objprodFamilyAttrlist = new TB_PROD_FAMILY_ATTR_LIST
                    {
                        ATTRIBUTE_ID = attrid,
                        SORT_ORDER = sortOrder,
                        FAMILY_ID = familyID,
                        PRODUCT_ID = 0,
                        CREATED_USER = User.Identity.Name,
                        CREATED_DATE = DateTime.Now,
                        MODIFIED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now
                    };
                    _objLs.TB_PROD_FAMILY_ATTR_LIST.Add(objprodFamilyAttrlist);
                    _objLs.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  PublishAttributes", ex);
            }
        }

        public DataSet CreateDataSet()
        {
            try
            {
                var dsReturn = new DataSet();
                using (
                    var objSqlConnection =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    var dbAdapter = new SqlDataAdapter(_sqlString, objSqlConnection) { SelectCommand = { CommandTimeout = 0 } };
                    dbAdapter.Fill(dsReturn);
                    dbAdapter.Dispose();
                    return dsReturn;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  CreateDataSet", ex);
            }
            return null;
        }

        public void ExecuteSqlQuery()
        {
            try
            {
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = _sqlString;
                    objSqlCommand.CommandType = CommandType.Text;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  ExecuteSqlQuery", ex);
            }
        }

        [System.Web.Http.HttpGet]
        public IList GroupTableDataGrid(string familyId, int catalogId)
        {
            try
            {
                int famID;
                int.TryParse(familyId.Trim('~'), out famID);
                return _objLs.TB_ATTRIBUTE_GROUP_SECTIONS.Where(x => x.FAMILY_ID == famID && x.CATALOG_ID == catalogId).Join(_objLs.TB_ATTRIBUTE_GROUP, a => a.GROUP_ID, b => b.GROUP_ID, (a, b) => new { a.GROUP_ID, b.GROUP_NAME }).OrderBy(x => x.GROUP_ID).ToList();
            }
            catch (Exception objException)
            {
                Logger.Error("Error at FamilyApiController:GroupTableDataGrid", objException);
                return null;
            }
        }


        public class TB_USER_FLOW
        {
            public string TB_USER_ID { get; set; }
            public Nullable<bool> NEW { get; set; }
            public Nullable<bool> UPDATED { get; set; }
            public Nullable<bool> REVIEW_PROCESS { get; set; }
            public Nullable<bool> SUBMIT { get; set; }
            public Nullable<bool> APPROVE { get; set; }
        }

        public class MyProfile
        {
            public int CustomerId { get; set; }
            public string CustomerName { get; set; }
            public string RoleName { get; set; }
            public bool Active { get; set; }
        }

        public class TB_USER_FUNCTION_ALLOWED
        {
            public int FUNCTION_ID { get; set; }
            public string FUNCTION_NAME { get; set; }
            public int ROLE_ID { get; set; }
            public Nullable<bool> ACTION_VIEW { get; set; }
            public Nullable<bool> ACTION_MODIFY { get; set; }
            public Nullable<bool> ACTION_ADD { get; set; }
            public Nullable<bool> ACTION_REMOVE { get; set; }
        }

        public class ALL_ATTRIBUTES_LIST
        {
            public int ATTRIBUTE_ID { get; set; }
            public string ATTRIBUTE_NAME { get; set; }
            public int ATTRIBUTE_TYPE { get; set; }
            public int SORT_ORDER { get; set; }
            public string ATTRIBUTE_DATATYPE { get; set; }
        }
        public string MissingImagePath(string imageFile)
        {
            try
            {
                return string.IsNullOrEmpty(imageFile)
                    ? imageFile
                    : (File.Exists(HttpContext.Current.Server.MapPath("~/Content/ProductImages") + imageFile)
                        ? "/Content/ProductImages" + imageFile
                        : "/Content/ProductImages/Images/unsupportedImageformat.jpg");
            }
            catch (Exception objException)
            {
                Logger.Error("Error at FamilyApiController : MissingImagePath", objException);
                return string.Empty;
            }
        }
        //family img Attchment save

        static long DirectorySize(DirectoryInfo dInfo, bool includeSubDir)
        {
            long totalSize = dInfo.EnumerateFiles()
                         .Sum(file => file.Length);
            if (includeSubDir)
            {
                totalSize += dInfo.EnumerateDirectories()
                         .Sum(dir => DirectorySize(dir, true));
            }
            return totalSize;
        }

        public string SaveFamilyImgAttach(FamilyImgAttachment model, int catalogId)
        {
            try
            {
                string alert = "";
                var SpaceProvided = _objLs.TB_PLAN
                          .Join(_objLs.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                          .Join(_objLs.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                          .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => x.tcptps.tps.StorageGB).ToList();
                double alotment = (double)SpaceProvided[0];
                var userId = _objLs.Customer_User.Join(_objLs.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();
                double folderSize = 0;
                if (string.IsNullOrEmpty(userId))
                {
                    userId = "Questudio";
                }

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + ""));
                }
                var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "");
                DirectoryInfo dInfo = new DirectoryInfo(path);
                long sizeOfDir = DirectorySize(dInfo, true);
                folderSize = (((double)sizeOfDir) / (double)(1024 * 1024 * 1024));

                double availableSpace = alotment - folderSize;
                if (availableSpace > 0)
                {
                    var usedPercentage = ((folderSize / alotment) * 100);

                    if (model.FAMILYIMG_NAME != "")
                    {
                        int count = _objLs.TB_FAMILY_SPECS.Count(a => a.FAMILY_ID == model.FAMILY_ID && a.ATTRIBUTE_ID == model.ATTRIBUTE_ID);
                        if (count <= 1)
                        {
                            var familydetails = _objLs.TB_FAMILY_SPECS.FirstOrDefault(a => a.FAMILY_ID == model.FAMILY_ID && a.ATTRIBUTE_ID == model.ATTRIBUTE_ID);
                            if (familydetails != null)
                            {
                                familydetails.STRING_VALUE = model.FAMILYIMG_NAME;
                                familydetails.OBJECT_NAME = model.OBJECT_NAME;
                                string imgExtension = Path.GetExtension(model.FAMILYIMG_NAME);
                                if (imgExtension != null) familydetails.OBJECT_TYPE = imgExtension.Substring(1);
                            }
                            _objLs.SaveChanges();
                        }
                        else
                        {
                            var objFam = new TB_FAMILY_SPECS
                            {
                                STRING_VALUE = model.FAMILYIMG_NAME,
                                FAMILY_ID = model.FAMILY_ID,
                                ATTRIBUTE_ID = model.ATTRIBUTE_ID
                            };
                            string imgExtension = Path.GetExtension(model.FAMILYIMG_NAME);
                            if (imgExtension != null) objFam.OBJECT_TYPE = imgExtension.Substring(1);
                            objFam.OBJECT_NAME = model.OBJECT_NAME;
                            objFam.CREATED_USER = User.Identity.Name;
                            objFam.CREATED_DATE = DateTime.Now;
                            objFam.MODIFIED_USER = User.Identity.Name;
                            objFam.MODIFIED_DATE = DateTime.Now;
                            _objLs.TB_FAMILY_SPECS.Add(objFam);
                            _objLs.SaveChanges();
                        }
                        var AssetLimitPercentage = _objLs.Customer_Settings.Where(x => x.CustomerId == 1);
                        if (AssetLimitPercentage.Any())
                        {
                            var assetLimitPercentageno = AssetLimitPercentage.FirstOrDefault();
                            double asset = Convert.ToDouble(assetLimitPercentageno.AssetLimitPercentage);
                            if (usedPercentage >= asset)
                            {
                                alert = "Saved successfully. You are about to reach the maximum memory";
                            }
                            else
                            {
                                alert = "Saved successfully";
                            }
                        }
                    }
                    else
                    {
                        alert = "Please upload the image";
                    }
                }
                else
                {
                    alert = "You have reached the maximum allotted memory, please contact Administrator";
                }
                return alert;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : SaveFamilyImgAttach", objexception);
                return "Save action not completed";
            }
        }
        //Clear Image Attachment
        public string ClearFamilyAttach(FamilyImgAttachment model, int catalogId)
        {
            try
            {
                int count = _objLs.TB_FAMILY_SPECS.Count(a => a.FAMILY_ID == model.FAMILY_ID && a.ATTRIBUTE_ID == model.ATTRIBUTE_ID);
                if (count >= 1)
                {
                    var familydetails = _objLs.TB_FAMILY_SPECS.FirstOrDefault(a => a.FAMILY_ID == model.FAMILY_ID && a.ATTRIBUTE_ID == model.ATTRIBUTE_ID);
                    if (familydetails != null)
                    {
                        familydetails.STRING_VALUE = "";
                        familydetails.OBJECT_NAME = "";
                        string imgExtension = Path.GetExtension(model.FAMILYIMG_NAME);
                        if (imgExtension != null) familydetails.OBJECT_TYPE = "";
                    }
                    _objLs.SaveChanges();
                }
                else
                {
                    var objFam = new TB_FAMILY_SPECS
                    {
                        STRING_VALUE = "",
                        FAMILY_ID = model.FAMILY_ID,
                        ATTRIBUTE_ID = model.ATTRIBUTE_ID
                    };
                    string imgExtension = Path.GetExtension(model.FAMILYIMG_NAME);
                    if (imgExtension != null) objFam.OBJECT_TYPE = "";
                    objFam.OBJECT_NAME = "";
                    objFam.CREATED_USER = User.Identity.Name;
                    objFam.CREATED_DATE = DateTime.Now;
                    objFam.MODIFIED_USER = User.Identity.Name;
                    objFam.MODIFIED_DATE = DateTime.Now;
                    _objLs.TB_FAMILY_SPECS.Add(objFam);
                    _objLs.SaveChanges();
                }
                return "Cleared successfully";
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : CreateNewCategory", objexception);
                return "Create action not completed";
            }
        }
        public string SaveFamilyDescription(TB_FAMILY_SPECS model, int catalogId)
        {
            try
            {

                //if (!string.IsNullOrEmpty(model.STRING_VALUE))
                //{
                //    model.STRING_VALUE = HttpUtility.HtmlDecode(model.STRING_VALUE);
                //    model.STRING_VALUE = model.STRING_VALUE.Replace("<p>", "");
                //    model.STRING_VALUE = model.STRING_VALUE.Replace("</p>", "");
                //    //model.STRING_VALUE = model.STRING_VALUE.Replace("\n", "\r\n");
                //    //if (model.STRING_VALUE.ToLower().StartsWith("<p>") && model.STRING_VALUE.ToLower().EndsWith("</p>"))
                //    //{
                //    //    model.STRING_VALUE = model.STRING_VALUE.Replace("<p>", "");
                //    //    model.STRING_VALUE = model.STRING_VALUE.Replace("</p>", "");
                //    //}
                //    if (model.STRING_VALUE.ToLower().EndsWith("\n"))
                //    {
                //        int sd = model.STRING_VALUE.LastIndexOf("\n", StringComparison.Ordinal);
                //        model.STRING_VALUE = model.STRING_VALUE.Remove(sd);
                //    }
                //    if (model.STRING_VALUE.ToLower().EndsWith("\r"))
                //    {
                //        int sd = model.STRING_VALUE.LastIndexOf("\r", StringComparison.Ordinal);
                //        model.STRING_VALUE = model.STRING_VALUE.Remove(sd);
                //    }
                //    if (model.STRING_VALUE.ToLower().StartsWith("<p>") && model.STRING_VALUE.ToLower().EndsWith("</p>"))
                //    {
                //        int counta = 0;
                //        int i = 0;
                //        while ((i = model.STRING_VALUE.IndexOf("<p>", i, System.StringComparison.Ordinal)) != -1)
                //        {
                //            i += "<p>".Length;
                //            counta++;
                //        }
                //        if (counta == 1)
                //        {
                //            int endp = model.STRING_VALUE.LastIndexOf("</p>", StringComparison.Ordinal);
                //            model.STRING_VALUE = model.STRING_VALUE.Remove(endp);
                //            int p = model.STRING_VALUE.IndexOf("<p>", StringComparison.Ordinal);
                //            model.STRING_VALUE = model.STRING_VALUE.Remove(p, 3);
                //        }
                //    }
                //    model.STRING_VALUE = model.STRING_VALUE.Replace("\r", "");
                //    model.STRING_VALUE = model.STRING_VALUE.Replace("\n\n", "\n");
                //    model.STRING_VALUE = model.STRING_VALUE.Replace("\n", "\r\n");
                //    model.STRING_VALUE = model.STRING_VALUE.Replace("<br />", "\r\n");
                //    //if (model.STRING_VALUE.ToLower().Contains("<p>") && model.STRING_VALUE.ToLower().Contains("</p>"))
                //    //{
                //    //    model.STRING_VALUE = model.STRING_VALUE.Replace("<p>", "");
                //    //    model.STRING_VALUE = model.STRING_VALUE.Replace("</p>", "");
                //    //}
                //}

                if (!string.IsNullOrEmpty(model.STRING_VALUE))
                {
                    model.STRING_VALUE = HttpUtility.HtmlDecode(model.STRING_VALUE);

                    if (model.STRING_VALUE.ToLower().EndsWith("\n"))
                    {
                        int sd = model.STRING_VALUE.LastIndexOf("\n", StringComparison.Ordinal);
                        model.STRING_VALUE = model.STRING_VALUE.Remove(sd);
                    }
                    if (model.STRING_VALUE.ToLower().EndsWith("\r"))
                    {
                        int sd = model.STRING_VALUE.LastIndexOf("\r", StringComparison.Ordinal);
                        model.STRING_VALUE = model.STRING_VALUE.Remove(sd);
                    }
                    if (model.STRING_VALUE.ToLower().StartsWith("<p>") && model.STRING_VALUE.ToLower().EndsWith("</p>"))
                    {
                        int counta = 0;
                        int i = 0;
                        while ((i = model.STRING_VALUE.IndexOf("<p>", i, System.StringComparison.Ordinal)) != -1)
                        {
                            i += "<p>".Length;
                            counta++;
                        }
                        if (counta == 1)
                        {
                            int endp = model.STRING_VALUE.LastIndexOf("</p>", StringComparison.Ordinal);
                            model.STRING_VALUE = model.STRING_VALUE.Remove(endp);
                            int p = model.STRING_VALUE.IndexOf("<p>", StringComparison.Ordinal);
                            model.STRING_VALUE = model.STRING_VALUE.Remove(p, 3);
                        }
                    }
                    model.STRING_VALUE = model.STRING_VALUE.Replace("\r", "");
                    model.STRING_VALUE = model.STRING_VALUE.Replace("\n\n", "\n");
                    model.STRING_VALUE = model.STRING_VALUE.Replace("\n", "\r\n");
                    //model.STRING_VALUE = model.STRING_VALUE.Replace("<br />", "\r\n");
                    model.STRING_VALUE = model.STRING_VALUE.Replace("<p>", "");
                    model.STRING_VALUE = model.STRING_VALUE.Replace("</p>", "");
                    //model.STRING_VALUE = model.STRING_VALUE.Replace("<br>", "\r\n");
                }
                int count = _objLs.TB_FAMILY_SPECS.Count(a => a.FAMILY_ID == model.FAMILY_ID && a.ATTRIBUTE_ID == model.ATTRIBUTE_ID);
                if (count <= 1)
                {
                    var familydetails = _objLs.TB_FAMILY_SPECS.FirstOrDefault(a => a.FAMILY_ID == model.FAMILY_ID && a.ATTRIBUTE_ID == model.ATTRIBUTE_ID);
                    if (familydetails != null)
                        familydetails.STRING_VALUE = model.STRING_VALUE;
                    _objLs.SaveChanges();
                }
                else
                {
                    var objFam = new TB_FAMILY_SPECS
                    {
                        STRING_VALUE = model.STRING_VALUE,
                        FAMILY_ID = model.FAMILY_ID,
                        ATTRIBUTE_ID = model.ATTRIBUTE_ID,
                        OBJECT_NAME = null,
                        CREATED_USER = User.Identity.Name,
                        CREATED_DATE = DateTime.Now,
                        MODIFIED_USER = User.Identity.Name,
                        MODIFIED_DATE = DateTime.Now
                    };
                    _objLs.TB_FAMILY_SPECS.Add(objFam);
                    _objLs.SaveChanges();
                }
                return "Update completed";
                // }
                // return "Please fill the Description";

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : CreateNewCategory", objexception);
                return "Create action not completed";
            }
        }
        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveTbAttribute(int catalogId, string attributeName, byte attributeType, string styleName, bool publish2PRINT, bool publish2Web, string attributeDataType, string size, string decimalsize, string prefix, string suffix, string condition, string customvalue, string applyto, bool applyForNumericOnly, string attributeDataFormat)
        {
            try
            {
                using (var db = new CSEntities())
                {
                    //ayyappan......
                    string dataFormat;
                    string datarule = AttrDataRuleDS(prefix, suffix, condition, customvalue, applyto, applyForNumericOnly);
                    if (attributeDataType.ToLower().Contains("number"))
                    {
                        if (Convert.ToInt32(size) != 0 && Convert.ToInt32(decimalsize) != 0)
                        {
                            if (Convert.ToInt32(size) <= 13)
                            {
                                attributeDataType = attributeDataType + "(" + size + "," + decimalsize + ")";
                                if (attributeDataFormat.Trim() == "")
                                {
                                    dataFormat = @"^-{0,1}?\d*\.{0,1}\d{0,6}$";
                                }
                                else
                                {
                                    dataFormat = attributeDataFormat;
                                }
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, "The maximum no. of digits that can be entered is 13");
                            }

                        }
                        else if (Convert.ToInt32(size) != 0 && Convert.ToInt32(decimalsize) == 0)
                        {
                            if (Convert.ToInt32(size) <= 13)
                            {
                                attributeDataType = attributeDataType + "(" + size + ")";
                                dataFormat = attributeDataFormat.Trim() == "" ? @"(^-?\d\d*$)" : attributeDataFormat;
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, "The maximum no. of digits that can be entered is 13");
                            }
                        }
                        else
                        {
                            dataFormat = attributeDataFormat.Trim() == "" ? @"^[ 0-9a-zA-Z\r\n\x20-\x7E\u0000-\uFFFF]*$" : attributeDataFormat;
                        }
                    }
                    else if (attributeDataType.ToLower().Contains("text") && Convert.ToInt32(size) != 0)
                    {
                        attributeDataType = attributeDataType + "(" + size + ")";
                        dataFormat = attributeDataFormat.Trim() == "" ? @"^[ 0-9a-zA-Z\r\n\x20-\x7E\u0000-\uFFFF]*$" : attributeDataFormat;
                    }
                    else
                    {
                        dataFormat = attributeDataFormat.Trim() == "" ? @"^[ 0-9a-zA-Z\r\n\x20-\x7E\u0000-\uFFFF]*$" : attributeDataFormat;
                    }
                    var attricount = _objLs.TB_ATTRIBUTE.Join(_objLs.CUSTOMERATTRIBUTE, ta => ta.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (ta, ca) => new { ta, ca }).Count(x => x.ta.ATTRIBUTE_NAME == attributeName);
                    if (attricount == 0)
                    {
                        var objAttr = new TB_ATTRIBUTE
                        {
                            ATTRIBUTE_NAME = attributeName,
                            ATTRIBUTE_TYPE = attributeType,
                            CREATE_BY_DEFAULT = Convert.ToBoolean(0),
                            VALUE_REQUIRED = Convert.ToBoolean(0),
                            STYLE_NAME = styleName,
                            STYLE_FORMAT = "",
                            DEFAULT_VALUE = "",
                            PUBLISH2PRINT = publish2PRINT ? Convert.ToBoolean(1) : Convert.ToBoolean(0),
                            PUBLISH2WEB = publish2Web ? Convert.ToBoolean(1) : Convert.ToBoolean(0),
                            USE_PICKLIST = Convert.ToBoolean(0),
                            ATTRIBUTE_DATATYPE = attributeDataType,
                            ATTRIBUTE_DATAFORMAT = dataFormat,
                            ATTRIBUTE_DATARULE = datarule,
                            UOM = null,
                            IS_CALCULATED = Convert.ToBoolean(0),
                            ATTRIBUTE_CALC_FORMULA = "",
                            PICKLIST_NAME = null,
                            CREATED_USER = User.Identity.Name,
                            CREATED_DATE = DateTime.Now,
                            MODIFIED_USER = User.Identity.Name,
                            MODIFIED_DATE = DateTime.Now,
                            FLAG_RECYCLE = "A"
                        };
                        _objLs.TB_ATTRIBUTE.Add(objAttr);
                        _objLs.SaveChanges();
                        var catalogs = db.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == attributeName && x.ATTRIBUTE_TYPE == attributeType).Select(x => new
                        {
                            x.ATTRIBUTE_ID
                        }).ToList();

                        int customer_id = 0;
                        var customerList =
                            _objLs.Customers.Where(x => x.CustomerName == User.Identity.Name)
                                .Select(z => z.CustomerId)
                                .ToList();

                        if (customerList.Any())
                        {
                            int.TryParse(customerList[0].ToString(), out customer_id);
                        }
                        var cumstomAttr =
                            _objLs.CUSTOMERATTRIBUTE.Where(
                                x => x.ATTRIBUTE_ID == catalogs[0].ATTRIBUTE_ID && x.CUSTOMER_ID == customer_id)
                                .ToList();
                        if (!cumstomAttr.Any())
                        {
                            if (customer_id != 0)
                            {
                                var objCatalogAttributes = new CUSTOMERATTRIBUTE
                                {
                                    CUSTOMER_ID = customer_id,
                                    ATTRIBUTE_ID = catalogs[0].ATTRIBUTE_ID,
                                    CREATED_USER = User.Identity.Name,
                                    CREATED_DATE = DateTime.Now,
                                    MODIFIED_USER = User.Identity.Name,
                                    MODIFIED_DATE = DateTime.Now
                                };
                                _objLs.CUSTOMERATTRIBUTE.Add(objCatalogAttributes);
                                _objLs.SaveChanges();
                            }
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, "Attribute saved successfully");
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, "Attribute name already exists");
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at saveTBAttribute", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }
        private string AttrDataRuleDS(string prefix, string suffix, string condition, string customvalue, string appy, bool applyForNumericOnly)
        {
            string dataString = string.Empty;
            if (prefix != "" || suffix != "" || condition != "" || customvalue != "" || string.IsNullOrEmpty(prefix) || string.IsNullOrEmpty(suffix) || string.IsNullOrEmpty(condition) || string.IsNullOrEmpty(customvalue))
            {
                var dataRuleDS = new DataSet();
                var dTable = new DataTable("DataRule");
                var dCol = new DataColumn("Prefix", typeof(string));
                dTable.Columns.Add(dCol);
                dCol = new DataColumn("Suffix", typeof(string));
                dTable.Columns.Add(dCol);
                dCol = new DataColumn("Condition", typeof(string));
                dTable.Columns.Add(dCol);
                dCol = new DataColumn("CustomValue", typeof(string));
                dTable.Columns.Add(dCol);
                dCol = new DataColumn("ApplyTo", typeof(string));
                dTable.Columns.Add(dCol);
                dCol = new DataColumn("ApplyForNumericOnly", typeof(int));
                dTable.Columns.Add(dCol);
                DataRow dRow = dTable.NewRow();
                dRow[0] = prefix;
                dRow[1] = suffix;
                dRow[2] = condition;
                dRow[3] = customvalue;
                dRow[4] = appy;
                if (applyForNumericOnly)
                {
                    dRow[5] = 1;
                }
                else
                {
                    dRow[5] = 0;
                }
                dTable.Rows.Add(dRow);
                dataRuleDS.Tables.Add(dTable);
                string path = HttpContext.Current.Server.MapPath("~/Content/XML/");
                dataRuleDS.WriteXml(path + "\\DataRulexml.xml");
                var fileStream = new FileStream(path + "\\DataRulexml.xml", FileMode.Open);
                var streamWriter = new StreamReader(fileStream);
                dataString = streamWriter.ReadToEnd();
                streamWriter.Close();
                fileStream.Close();
            }
            return dataString;
        }

        public int attributecount { get; set; }


        [System.Web.Http.HttpPost]
        public HttpResponseMessage UpdateGlobalAttrValueForSelectedFamilysub(int catalogId, string attrType, string attrId, string attrValue, object model) // 
        {
            try
            {
                string result = string.Empty;
                string[] familyIds = Convert.ToString(model).Split(',');
                _gaattrid = Convert.ToInt16(attrId);
                int workingCatalogID1 = catalogId;
                if (attrType != null)
                {
                    string id = string.Empty;
                    string prod_id = string.Empty;
                    for (int i = 0; i < familyIds.Length; i++)
                    {
                        if (Convert.ToString(familyIds[i]).Contains('~'))
                        {
                            id = id + "," + Convert.ToString(familyIds[i].Trim('~'));
                        }
                        if (Convert.ToString(familyIds[i]).Contains('#'))
                        {
                            prod_id = prod_id + "," + Convert.ToString(familyIds[i].Trim('#'));

                        }

                        else
                        {
                            var cat_det = _objLs.Category_Function(workingCatalogID1, Convert.ToString(familyIds[i]));
                            if (cat_det.Any())
                            {
                                foreach (var item in cat_det)
                                {
                                    string category_id = item.CATEGORY_ID;
                                    var family_ids = _objLs.TB_CATALOG_FAMILY.Where(a => a.CATEGORY_ID == category_id && a.CATALOG_ID == workingCatalogID1);
                                    if (family_ids.Any())
                                    {
                                        foreach (var family in family_ids)
                                        {
                                            id = id + "," + Convert.ToString(family.FAMILY_ID);

                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (id != "")
                    {
                        result = UpdateAttrValuesmainsub(workingCatalogID1, id.TrimStart(','), Convert.ToInt32(attrId), attrValue, model);
                    }
                    else
                    {
                        result = UpdateAttrValuessub(workingCatalogID1, prod_id.TrimStart(','), Convert.ToInt32(attrId), attrValue, model);
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  UpdateGlobalAttrValueForSelectedFamily", ex);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        public string UpdateAttrValuessub(int workingCatalogID1, string prod_ids, int attrId, string attrValue, object model)
        {
            var dtfamilyID = new DataTable();
            dtfamilyID.Columns.Add("Family_id");
            dtfamilyID.Columns.Add("CATEGORY_ID");
            string[] pridd = prod_ids.Split(',');
            string family_id = string.Empty;
            string[] iids = Convert.ToString(model).Split(',');
            for (int i = 0; i < iids.Length; i++)
            {
                if (Convert.ToString(iids[i]).Contains('~'))
                {
                    family_id = family_id + "," + Convert.ToString(iids[i].Trim('~'));
                }
            }

            //  for (int ind = 0; ind < pridd.Length; ind++)
            //  {               


            // dtfamilyID.Rows.Add(familyId[ind]);    
            // if (pridd[ind] != "")
            {
                //DataSet cat = new DataSet();
                //_sqlString = "select tpf.family_id,tcf.category_id,tpf.product_id from tb_prod_family tpf join " +
                //" tb_catalog_family tcf on tpf.family_id = tcf.family_id where tpf.product_id in (" +pridd+") and tcf.catalog_id =" + workingCatalogID1 + " ";                   

                //cat = CreateDataSet();
                //if (cat.Tables != null && cat.Tables[0].Rows.Count > 0)
                //{
                //    foreach (DataRow rr in cat.Tables[0].Rows)
                //    {
                //        string ccatid = rr["CATEGORY_ID"].ToString();
                //        dtfamilyID.Rows.Add(familyId[ind], ccatid);
                //    }

                //}
            }

            // }
            //catalog_attributes check
            var catalogAttributes = _objLs.TB_CATALOG_ATTRIBUTES.FirstOrDefault(a => a.ATTRIBUTE_ID == _gaattrid && a.CATALOG_ID == workingCatalogID1);
            if (catalogAttributes == null)
            {
                var objTbcatalogattributes = new TB_CATALOG_ATTRIBUTES
                {
                    CATALOG_ID = workingCatalogID1,
                    ATTRIBUTE_ID = _gaattrid,
                    CREATED_USER = User.Identity.Name,
                    CREATED_DATE = DateTime.Now,
                    MODIFIED_USER = User.Identity.Name,
                    MODIFIED_DATE = DateTime.Now
                };
                _objLs.TB_CATALOG_ATTRIBUTES.Add(objTbcatalogattributes);
                _objLs.SaveChanges();
            }
            var customerid = _objLs.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
            if (customerid != null)
            {
                customerId_ = customerid.CustomerId;
            }
            _sqlString = " select attribute_type from tb_attribute ta join customerAttribute ca on ta.Attribute_id=ca.Attribute_id where ta.attribute_id=" + attrId + " and ca.CUSTOMER_ID=" + customerId_;
            DataSet atttyp = CreateDataSet();
            string typecheck = atttyp.Tables[0].Rows[0][0].ToString();
            if (!string.IsNullOrEmpty(attrValue))
            {
                if (typecheck == "6" && typecheck != "7" && typecheck != "11" && typecheck != "12" && typecheck != "13" && typecheck != "4")
                {
                    for (int s = 0; s < pridd.Length; s++)
                    {
                        string pr_sub_id = pridd[s];

                        DataSet cat = new DataSet();
                        _sqlString = "select tpf.family_id,tcf.category_id,tpf.product_id from tb_prod_family tpf join " +
                        " tb_catalog_family tcf on tpf.family_id = tcf.family_id where tpf.product_id in (" + pr_sub_id + ") and tcf.catalog_id =" + workingCatalogID1 + " ";
                        cat = CreateDataSet();

                        string gafamid = cat.Tables[0].Rows[s][0].ToString();
                        string Cate_id = cat.Tables[0].Rows[s][1].ToString();
                        _sqlString = " select subproduct_id from tb_subproduct where product_id =" + pr_sub_id + " and catalog_id =" + workingCatalogID1 + " ";
                        DataSet pkch = CreateDataSet();
                        for (int k = 0; k < pkch.Tables[0].Rows.Count; k++)
                        {
                            string productIDGa = pkch.Tables[0].Rows[k][0].ToString();
                            _sqlString = "select product_id  from tb_subproduct_key where family_id =" + gafamid + " and catalog_id =" + workingCatalogID1 + " and attribute_id =" + _gaattrid + " and product_id=" + pr_sub_id + " and category_id ='" + Cate_id + "' and subproduct_id =" + productIDGa + "";
                            DataSet updatekey = CreateDataSet();

                            if (updatekey.Tables[0].Rows.Count > 0)
                            {
                                _sqlString = "update tb_subproduct_key set attribute_value ='" + attrValue + "' where subproduct_id in (" + productIDGa + ")and  attribute_id=" + _gaattrid + " and catalog_id =" + workingCatalogID1 + " and category_id = '" + Cate_id + "'";
                                ExecuteSqlQuery();
                            }
                            else
                            {
                                _sqlString = " insert into tb_subproduct_key values(" + _gaattrid + "," + productIDGa + "," + pr_sub_id + "," + gafamid + ",'" + attrValue + "',SUSER_NAME(),getdate(),SUSER_NAME(),getdate()," + workingCatalogID1 + ",'" + Cate_id + "')";
                                ExecuteSqlQuery();
                            }
                            //// tb_prod_family_attr_list check
                            // PublishAttributes(gafamid, productIDGa, _gaattrid);
                        }
                    }
                }

                if (typecheck != "6" && typecheck != "7" && typecheck != "11" && typecheck != "12" && typecheck != "13" && typecheck != "4")
                {
                    for (int s = 0; s < pridd.Length; s++)
                    {
                        string pids = pridd[s];
                        // string gafamid = dtfamilyID.Rows[s][0].ToString();
                        _sqlString = "select subproduct_id  from tb_subproduct where product_id =" + pids + "";
                        DataSet prid1 = CreateDataSet();
                        DataSet Dataformat = new DataSet();
                        string format = string.Empty;
                        customerid = _objLs.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                        if (customerid != null)
                        {
                            customerId_ = customerid.CustomerId;
                        }
                        _sqlString = "select attribute_datatype from tb_attribute  ta join customerAttribute ca on ta.Attribute_id=ca.Attribute_id where ta.attribute_id = " + _gaattrid + " and ca.CUSTOMER_ID=" + customerId_;
                        Dataformat = CreateDataSet();
                        foreach (DataRow ropid1 in prid1.Tables[0].Rows)
                        {
                            string productIDGa1 = ropid1["subproduct_id"].ToString();
                            _sqlString = "select product_id from tb_prod_specs where product_id =" + productIDGa1 + " and attribute_id =" + _gaattrid + "";
                            DataSet prodnum = CreateDataSet();
                            if (prodnum.Tables[0].Rows.Count == 0)
                            {
                                _sqlString = "INSERT INTO TB_PROD_SPECS VALUES('" + attrValue + "'," + productIDGa1 + "," + _gaattrid + ",NULL,NULL,NULL,SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                ExecuteSqlQuery();
                            }
                            else
                            {
                                if (Dataformat.Tables != null && Dataformat.Tables[0].Rows.Count > 0)
                                {
                                    format = Dataformat.Tables[0].Rows[0][0].ToString();
                                }
                                if (format.Contains("Num"))
                                {

                                    _sqlString = "update tb_prod_specs set Numeric_value = " + attrValue + " where  product_id in (" + productIDGa1 + ") and attribute_id =" + _gaattrid + "";
                                    ExecuteSqlQuery();
                                }
                                else
                                {
                                    _sqlString = "update tb_prod_specs set string_value ='" + attrValue + "' where  product_id in (" + productIDGa1 + ") and attribute_id =" + _gaattrid + "";
                                    ExecuteSqlQuery();
                                }
                            }
                            //// tb_prod_family_attr_list check
                            //   PublishAttributes(gafamid, productIDGa1, _gaattrid);
                        }

                    }
                }
                if (typecheck == "4")
                {
                    for (int s = 0; s < pridd.Length; s++)
                    {
                        string pids = pridd[s];
                        //string gafamid = dtfamilyID.Rows[s][0].ToString();
                        _sqlString = "select subproduct_id  from tb_subproduct where product_id =" + pids + "";
                        DataSet prid = CreateDataSet();
                        foreach (DataRow ropid in prid.Tables[0].Rows)
                        {
                            string productIDGa = ropid["subproduct_id"].ToString();
                            _sqlString = "select product_id from tb_prod_specs where product_id =" + productIDGa + " and attribute_id =" + _gaattrid + "";
                            DataSet prodnum = CreateDataSet();
                            if (prodnum.Tables[0].Rows.Count == 0)
                            {
                                _sqlString = "INSERT INTO TB_PROD_SPECS VALUES(NULL," + productIDGa + "," + _gaattrid + "," + attrValue + ",NULL,NULL,SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                ExecuteSqlQuery();
                            }
                            else
                            {
                                _sqlString = "update tb_prod_specs set numeric_value ='" + attrValue + "' where  product_id in (" + productIDGa + ") and attribute_id =" + _gaattrid + "";
                                ExecuteSqlQuery();
                            }
                            // PublishAttributes(gafamid, productIDGa, _gaattrid);
                        }
                    }
                }
                return ResponseMsg.Duplicate;
            }
            return ResponseMsg.Duplicate;
        }

        public string UpdateAttrValuesmainsub(int workingCatalogID1, string prod_ids, int attrId, string attrValue, object model)
        {
            var dtfamilyID = new DataTable();
            dtfamilyID.Columns.Add("Family_id");
            dtfamilyID.Columns.Add("CATEGORY_ID");
            string[] pridd = prod_ids.Split(',');
            string family_id = string.Empty;
            string[] iids = Convert.ToString(model).Split(',');
            for (int i = 0; i < iids.Length; i++)
            {
                if (Convert.ToString(iids[i]).Contains('~'))
                {
                    family_id = family_id + "," + Convert.ToString(iids[i].Trim('~'));
                }
            }


            //catalog_attributes check
            var catalogAttributes = _objLs.TB_CATALOG_ATTRIBUTES.FirstOrDefault(a => a.ATTRIBUTE_ID == _gaattrid && a.CATALOG_ID == workingCatalogID1);
            if (catalogAttributes == null)
            {
                var objTbcatalogattributes = new TB_CATALOG_ATTRIBUTES
                {
                    CATALOG_ID = workingCatalogID1,
                    ATTRIBUTE_ID = _gaattrid,
                    CREATED_USER = User.Identity.Name,
                    CREATED_DATE = DateTime.Now,
                    MODIFIED_USER = User.Identity.Name,
                    MODIFIED_DATE = DateTime.Now
                };
                _objLs.TB_CATALOG_ATTRIBUTES.Add(objTbcatalogattributes);
                _objLs.SaveChanges();
            }
            var customerid = _objLs.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
            if (customerid != null)
            {
                customerId_ = customerid.CustomerId;
            }

            _sqlString = " select attribute_type from tb_attribute ta join customerAttribute ca on ta.Attribute_id=ca.Attribute_id where ta.attribute_id=" + attrId + " and ca.CUSTOMER_ID=" + customerId_;
            DataSet atttyp = CreateDataSet();
            string typecheck = atttyp.Tables[0].Rows[0][0].ToString();
            if (!string.IsNullOrEmpty(attrValue))
            {
                string pr_sub_id = string.Empty;
                if (typecheck == "6" && typecheck != "7" && typecheck != "11" && typecheck != "12" && typecheck != "13" && typecheck != "4")
                {
                    for (int s = 0; s < pridd.Length; s++)
                    {
                        pr_sub_id = pridd[s];
                        DataSet cat = new DataSet();
                        _sqlString = "select family_id,category_id from tb_catalog_family where family_id = " + pr_sub_id + " and catalog_id =" + workingCatalogID1 + " ";
                        cat = CreateDataSet();
                        if (cat.Tables[0].Rows.Count > 0)
                        {
                            string gafamid = cat.Tables[0].Rows[0][0].ToString();
                            string Cate_id = cat.Tables[0].Rows[0][1].ToString();
                            _sqlString = " select product_id from TB_PROD_FAMILY where family_id=" + gafamid + " ";
                            DataSet pkch = CreateDataSet();
                            for (int k = 0; k < pkch.Tables[0].Rows.Count; k++)
                            {
                                string productIDGa = pkch.Tables[0].Rows[k][0].ToString();
                                string subpr_id = string.Empty;
                                DataSet subprod = new DataSet();
                                _sqlString = "select subproduct_id from tb_subproduct where product_id =" + productIDGa + " and catalog_id =" + workingCatalogID1 + " ";
                                subprod = CreateDataSet();
                                if (subprod.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow item in subprod.Tables[0].Rows)
                                    {
                                        subpr_id = item["subproduct_id"].ToString();
                                        _sqlString = "select product_id  from tb_subproduct_key where family_id =" + gafamid + " and catalog_id =" + workingCatalogID1 + " and attribute_id =" + _gaattrid + " and product_id=" + productIDGa + " and category_id ='" + Cate_id + "' and subproduct_id =" + subpr_id + "";
                                        DataSet updatekey = CreateDataSet();
                                        if (updatekey.Tables[0].Rows.Count > 0)
                                        {
                                            _sqlString = "update tb_subproduct_key set attribute_value ='" + attrValue + "' where product_id in (select  product_id from tb_subproduct_key where family_id=" + gafamid + " and catalog_id =" + workingCatalogID1 + " and attribute_id=" + _gaattrid + "  )and  attribute_id=" + _gaattrid + " and catalog_id =" + workingCatalogID1 + " and subproduct_id =" + subpr_id + "";
                                            ExecuteSqlQuery();
                                        }
                                        else
                                        {
                                            _sqlString = " insert into tb_subproduct_key values(" + _gaattrid + "," + subpr_id + "," + productIDGa + "," + gafamid + ",'" + attrValue + "',SUSER_NAME(),getdate(),SUSER_NAME(),getdate()," + workingCatalogID1 + ",'" + Cate_id + "')";
                                            ExecuteSqlQuery();
                                        }
                                    }
                                }
                            }
                        }

                        DataSet sub_family_check = new DataSet();
                        _sqlString = "SELECT FAMILY_ID  FROM TB_FAMILY WHERE PARENT_FAMILY_ID = " + pr_sub_id + "";
                        sub_family_check = CreateDataSet();
                        if (sub_family_check.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in sub_family_check.Tables[0].Rows)
                            {
                                pr_sub_id = dr["FAMILY_ID"].ToString();

                                DataSet cat12 = new DataSet();
                                _sqlString = "select family_id,category_id from tb_catalog_family where family_id = " + pr_sub_id + " and catalog_id =" + workingCatalogID1 + " ";
                                cat12 = CreateDataSet();
                                if (cat12.Tables[0].Rows.Count > 0)
                                {
                                    string gafamid = cat12.Tables[0].Rows[0][0].ToString();
                                    string Cate_id = cat12.Tables[0].Rows[0][1].ToString();
                                    _sqlString = " select product_id from TB_PROD_FAMILY where family_id=" + gafamid + " ";
                                    DataSet pkch = CreateDataSet();
                                    for (int k = 0; k < pkch.Tables[0].Rows.Count; k++)
                                    {
                                        string productIDGa = pkch.Tables[0].Rows[k][0].ToString();
                                        string subpr_id = string.Empty;
                                        DataSet subprod = new DataSet();
                                        _sqlString = "select subproduct_id from tb_subproduct where product_id =" + productIDGa + " and catalog_id =" + workingCatalogID1 + " ";
                                        subprod = CreateDataSet();
                                        if (subprod.Tables[0].Rows.Count > 0)
                                        {
                                            foreach (DataRow item in subprod.Tables[0].Rows)
                                            {
                                                subpr_id = item["subproduct_id"].ToString();
                                                _sqlString = "select product_id  from tb_subproduct_key where family_id =" + gafamid + " and catalog_id =" + workingCatalogID1 + " and attribute_id =" + _gaattrid + " and product_id=" + productIDGa + " and category_id ='" + Cate_id + "' and subproduct_id =" + subpr_id + "";
                                                DataSet updatekey = CreateDataSet();
                                                if (updatekey.Tables[0].Rows.Count > 0)
                                                {
                                                    _sqlString = "update tb_subproduct_key set attribute_value ='" + attrValue + "' where product_id in (select  product_id from tb_subproduct_key where family_id=" + gafamid + " and catalog_id =" + workingCatalogID1 + " and attribute_id=" + _gaattrid + "  )and  attribute_id=" + _gaattrid + " and catalog_id =" + workingCatalogID1 + " and subproduct_id =" + subpr_id + "";
                                                    ExecuteSqlQuery();
                                                }
                                                else
                                                {
                                                    _sqlString = " insert into tb_subproduct_key values(" + _gaattrid + "," + subpr_id + "," + productIDGa + "," + gafamid + ",'" + attrValue + "',SUSER_NAME(),getdate(),SUSER_NAME(),getdate()," + workingCatalogID1 + ",'" + Cate_id + "')";
                                                    ExecuteSqlQuery();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                }

                if (typecheck != "6" && typecheck != "7" && typecheck != "11" && typecheck != "12" && typecheck != "13" && typecheck != "4")
                {
                    for (int s = 0; s < pridd.Length; s++)
                    {
                        string pids = pridd[s];
                        // string gafamid = dtfamilyID.Rows[s][0].ToString();

                        _sqlString = "select subproduct_id from tb_subproduct where product_id in(select product_id  from tb_prod_family where family_id = " + pids + ") and catalog_id = " + workingCatalogID1 + "";
                        DataSet prid1 = CreateDataSet();
                        DataSet Dataformat = new DataSet();
                        string format = string.Empty;
                        customerid = _objLs.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                        if (customerid != null)
                        {
                            customerId_ = customerid.CustomerId;
                        }
                        string[] aa = attrValue.Split('\\');
                        string objectType = string.Empty;
                        foreach (var img in aa)
                        {
                            if (img.Contains("."))
                            {
                                objectType = img.Split('.')[1];
                            }
                        }
                        _sqlString = "select attribute_datatype from tb_attribute ta join customerAttribute ca on ta.Attribute_id=ca.Attribute_id where ta.attribute_id = " + _gaattrid + " and ca.CUSTOMER_ID=" + customerId_;
                        Dataformat = CreateDataSet();
                        foreach (DataRow ropid1 in prid1.Tables[0].Rows)
                        {
                            string productIDGa1 = ropid1["subproduct_id"].ToString();
                            _sqlString = "select product_id from tb_prod_specs where product_id =" + productIDGa1 + " and attribute_id =" + _gaattrid + "";
                            DataSet prodnum = CreateDataSet();
                            if (prodnum.Tables[0].Rows.Count == 0)
                            {
                                _sqlString = "INSERT INTO TB_PROD_SPECS VALUES('" + attrValue + "'," + productIDGa1 + "," + _gaattrid + ",NULL,'" + objectType + "',NULL,SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                ExecuteSqlQuery();
                            }
                            else
                            {
                                if (Dataformat.Tables != null && Dataformat.Tables[0].Rows.Count > 0)
                                {
                                    format = Dataformat.Tables[0].Rows[0][0].ToString();
                                }
                                if (format.Contains("Num"))
                                {

                                    _sqlString = "update tb_prod_specs set Numeric_value = " + attrValue + " where  product_id in (" + productIDGa1 + ") and attribute_id =" + _gaattrid + "";
                                    ExecuteSqlQuery();
                                }
                                else
                                {
                                    _sqlString = "update tb_prod_specs set string_value ='" + attrValue + "' and Object_type='" + objectType + "' where  product_id in (" + productIDGa1 + ") and attribute_id =" + _gaattrid + "";
                                    ExecuteSqlQuery();
                                }
                            }
                            //// tb_prod_family_attr_list check
                            //   PublishAttributes(gafamid, productIDGa1, _gaattrid);
                        }


                        DataSet sub_family_check = new DataSet();
                        _sqlString = "SELECT FAMILY_ID  FROM TB_FAMILY WHERE PARENT_FAMILY_ID = " + pids + "";
                        sub_family_check = CreateDataSet();
                        if (sub_family_check.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in sub_family_check.Tables[0].Rows)
                            {
                                pids = dr["FAMILY_ID"].ToString();
                                _sqlString = "select subproduct_id from tb_subproduct where product_id in(select product_id  from tb_prod_family where family_id = " + pids + ") and catalog_id = " + workingCatalogID1 + "";
                                DataSet prid12 = CreateDataSet();
                                DataSet Dataformat2 = new DataSet();
                                string format2 = string.Empty;
                                customerid = _objLs.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                                if (customerid != null)
                                {
                                    customerId_ = customerid.CustomerId;
                                }
                                _sqlString = "select attribute_datatype from tb_attribute  ta join customerAttribute ca on ta.Attribute_id=ca.Attribute_id where ta.attribute_id = " + _gaattrid + " and ca.CUSTOMER_ID=" + customerId_;
                                Dataformat2 = CreateDataSet();
                                aa = attrValue.Split('\\');
                                objectType = string.Empty;
                                foreach (var img in aa)
                                {
                                    if (img.Contains("."))
                                    {
                                        objectType = img.Split('.')[1];
                                    }
                                }

                                foreach (DataRow ropid1 in prid12.Tables[0].Rows)
                                {
                                    string productIDGa1 = ropid1["subproduct_id"].ToString();
                                    _sqlString = "select product_id from tb_prod_specs where product_id =" + productIDGa1 + " and attribute_id =" + _gaattrid + "";
                                    DataSet prodnum = CreateDataSet();
                                    if (prodnum.Tables[0].Rows.Count == 0)
                                    {
                                        _sqlString = "INSERT INTO TB_PROD_SPECS VALUES('" + attrValue + "'," + productIDGa1 + "," + _gaattrid + ",NULL,'" + objectType + "',NULL,SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                        ExecuteSqlQuery();
                                    }
                                    else
                                    {
                                        if (Dataformat2.Tables != null && Dataformat2.Tables[0].Rows.Count > 0)
                                        {
                                            format = Dataformat2.Tables[0].Rows[0][0].ToString();
                                        }
                                        if (format.Contains("Num"))
                                        {

                                            _sqlString = "update tb_prod_specs set Numeric_value = " + attrValue + " where  product_id in (" + productIDGa1 + ") and attribute_id =" + _gaattrid + "";
                                            ExecuteSqlQuery();
                                        }
                                        else
                                        {
                                            _sqlString = "update tb_prod_specs set string_value ='" + attrValue + "' and Object_type='" + objectType + "' where  product_id in (" + productIDGa1 + ") and attribute_id =" + _gaattrid + "";
                                            ExecuteSqlQuery();
                                        }
                                    }
                                    //// tb_prod_family_attr_list check
                                    //   PublishAttributes(gafamid, productIDGa1, _gaattrid);
                                }
                            }
                        }
                        else
                        {

                        }

                    }
                }
                if (typecheck == "4")
                {
                    for (int s = 0; s < pridd.Length; s++)
                    {
                        string pids = pridd[s];
                        //string gafamid = dtfamilyID.Rows[s][0].ToString();
                        _sqlString = "select subproduct_id from tb_subproduct where product_id in(select product_id  from tb_prod_family where family_id = " + pids + ") and catalog_id = " + workingCatalogID1 + "";
                        DataSet prid = CreateDataSet();
                        foreach (DataRow ropid in prid.Tables[0].Rows)
                        {
                            string productIDGa = ropid["subproduct_id"].ToString();
                            _sqlString = "select product_id from tb_prod_specs where product_id =" + productIDGa + " and attribute_id =" + _gaattrid + "";
                            DataSet prodnum = CreateDataSet();
                            if (prodnum.Tables[0].Rows.Count == 0)
                            {
                                _sqlString = "INSERT INTO TB_PROD_SPECS VALUES(NULL," + productIDGa + "," + _gaattrid + "," + attrValue + ",NULL,NULL,SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                ExecuteSqlQuery();
                            }
                            else
                            {
                                _sqlString = "update tb_prod_specs set numeric_value ='" + attrValue + "' where  product_id in (" + productIDGa + ") and attribute_id =" + _gaattrid + "";
                                ExecuteSqlQuery();
                            }
                            // PublishAttributes(gafamid, productIDGa, _gaattrid);
                        }
                    }
                }
                return ResponseMsg.Duplicate;
            }
            return ResponseMsg.Duplicate;
        }
        [System.Web.Http.HttpPost]
        public HttpResponseMessage globalattributesub(string Attrid, int Catalogid, int Attributetype, string option, int sort, object model)
        {
            try
            {
                int groupId = 0;
                if ((Attrid).Contains('~'))
                {
                    Attrid = Attrid.Trim('~');
                    groupId = Convert.ToInt32(Attrid);
                    var attributeNames = (from packageDetails in _objLs.TB_PACKAGE_DETAILS
                                          join master in _objLs.TB_PACKAGE_MASTER on packageDetails.GROUP_ID equals master.GROUP_ID
                                          join catalogAttributes in _objLs.TB_CATALOG_ATTRIBUTES on packageDetails.ATTRIBUTE_ID equals catalogAttributes.ATTRIBUTE_ID
                                          join attributes in _objLs.TB_ATTRIBUTE on catalogAttributes.ATTRIBUTE_ID equals attributes.ATTRIBUTE_ID
                                          where master.CATALOG_ID == Catalogid && attributes.FLAG_RECYCLE == "A" && packageDetails.GROUP_ID == groupId
                                          select new
                                          {
                                              attributes.ATTRIBUTE_ID,
                                              attributes.ATTRIBUTE_NAME,
                                              attributes.ATTRIBUTE_TYPE,
                                          }).Distinct().ToList();

                    Attrid = "";

                    foreach (var item in attributeNames)
                    {
                        Attrid = Attrid + Convert.ToString(item.ATTRIBUTE_ID) + ',';
                    }
                }

                int customer_Id = 0;
                HomeApiController apiObj = new HomeApiController();
                customer_Id = apiObj.GetCustomerId();

                if (!string.IsNullOrEmpty(model.ToString()))
                {
                    var Passtable = new DataTable();
                    Passtable.Columns.Add("CATEGORY_ID");
                    Passtable.Columns.Add("FAMILY_ID");

                    var subProductSort = new DataTable();
                    subProductSort.Columns.Add("CATEGORY_ID");
                    subProductSort.Columns.Add("FAMILY_ID");
                    subProductSort.Columns.Add("Product_ID");
                    subProductSort.Columns.Add("SUBPRODUCT_ID");
                    string[] selectedoption = option.Split(',');
                    string[] familyid = Convert.ToString(model).Split(',');
                    string id = string.Empty;
                    string prod_id = string.Empty;



                    for (int i = 0; i < familyid.Length; i++)
                    {
                        if (subProductSort.Rows.Count > 0)
                        {
                            subProductSort.Rows.Clear();
                        }
                        if (Convert.ToString(familyid[i]).Contains('~'))
                        {
                            int famid = 0;
                            id = string.Empty;
                            prod_id = string.Empty;
                            int.TryParse(familyid[i].Replace("~", ""), out famid);

                            var family_id = _objLs.TB_FAMILY.Where(a => a.FAMILY_ID == famid || a.PARENT_FAMILY_ID == famid);
                            if (family_id.Any())
                            {
                                foreach (var family in family_id)
                                {
                                    id = id + "," + Convert.ToString(family.FAMILY_ID);
                                    string fam = Convert.ToString(familyid[i]);

                                    // insert record into TB_ATTRIBUTE_HIERARCHY table to associate upcoming Product or Family
                                    if (groupId != 0)
                                    {
                                        string familyIdstring = Convert.ToString(family.FAMILY_ID);
                                        TB_ATTRIBUTE_HIERARCHY attributeHierarchyInsert = new TB_ATTRIBUTE_HIERARCHY();
                                        attributeHierarchyInsert = _objLs.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == familyIdstring && s.PACK_ID == groupId && s.TYPE == "Family").FirstOrDefault();
                                        if (attributeHierarchyInsert == null)
                                        {
                                            string QryString = "insert into TB_ATTRIBUTE_HIERARCHY values(" + groupId + ",'" + "Family" + "','" + familyIdstring + "'," + customer_Id + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                            SqlConnection conn = new SqlConnection(connectionString);
                                            SqlCommand CmdObj = new SqlCommand(QryString, conn);
                                            conn.Open();
                                            CmdObj.ExecuteNonQuery();
                                            conn.Close();
                                        }
                                    }

                                    DataSet subfamily = new DataSet();
                                    _sqlString = "SELECT FAMILY_ID FROM TB_FAMILY WHERE PARENT_FAMILY_ID =  " + fam + "";
                                    subfamily = CreateDataSet();
                                    if (subfamily.Tables[0].Rows.Count > 0)
                                    {
                                        foreach (DataRow item in subfamily.Tables[0].Rows)
                                        {
                                            id = id + "," + item["FAMILY_ID"].ToString();
                                        }
                                    }

                                    //-------------------New Global Attributes ------------------                                            
                                    DataSet cat = new DataSet();
                                    _sqlString =
                                        "SELECT CATEGORY_ID FROM TB_CATALOG_FAMILY WHERE FAMILY_ID = " +
                                        Convert.ToString(family.FAMILY_ID) + " and catalog_id = " + Catalogid +
                                        "";
                                    cat = CreateDataSet();
                                    if (cat.Tables != null && cat.Tables[0].Rows.Count > 0)
                                    {
                                        foreach (DataRow rr in cat.Tables[0].Rows)
                                        {
                                            string ccatid = rr["CATEGORY_ID"].ToString();
                                            Passtable.Rows.Add(ccatid, Convert.ToString(family.FAMILY_ID));
                                            var subproductchk = _objLs.TB_SUBPRODUCT.Join(_objLs.TB_PROD_FAMILY, tps => tps.PRODUCT_ID, tpf => tpf.PRODUCT_ID,
                                                (tps, tpf) => new { tps, tpf }).Where(x => x.tpf.FAMILY_ID == family.FAMILY_ID && x.tps.CATALOG_ID == Catalogid);
                                            foreach (var tbProdFamily in subproductchk)
                                            {
                                                subProductSort.Rows.Add(ccatid,
                                                    Convert.ToString(tbProdFamily.tpf.FAMILY_ID),
                                                    Convert.ToString(tbProdFamily.tpf.PRODUCT_ID),
                                                    tbProdFamily.tps.SUBPRODUCT_ID);
                                            }
                                        }
                                    }
                                    //--------------END------------------
                                }
                            }
                        }
                        else if (Convert.ToString(familyid[i]).Contains('#'))
                        {
                            prod_id = prod_id + "," + Convert.ToString(familyid[i].Trim('#'));
                        }
                        else
                        {
                            var cat_det = _objLs.Category_Function(Catalogid, Convert.ToString(familyid[i]));
                            if (cat_det.Any())
                            {
                                foreach (var item in cat_det)
                                {
                                    string category_id = item.CATEGORY_ID;

                                    // insert record into TB_ATTRIBUTE_HIERARCHY table to associate upcoming Product or Family
                                    if (groupId != 0)
                                    {
                                        TB_ATTRIBUTE_HIERARCHY attributeHierarchyInsert = new TB_ATTRIBUTE_HIERARCHY();
                                        attributeHierarchyInsert = _objLs.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == category_id && s.PACK_ID == groupId && s.TYPE == "Category").FirstOrDefault();
                                        if (attributeHierarchyInsert == null)
                                        {
                                            string QryString = "insert into TB_ATTRIBUTE_HIERARCHY values(" + groupId + ",'" + "Category" + "','" + category_id + "'," + customer_Id + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                            SqlConnection conn = new SqlConnection(connectionString);
                                            SqlCommand CmdObj = new SqlCommand(QryString, conn);
                                            conn.Open();
                                            CmdObj.ExecuteNonQuery();
                                            conn.Close();
                                        }
                                    }

                                    var family_ids =
                                        _objLs.TB_CATALOG_FAMILY.Where(
                                            a => a.CATEGORY_ID == category_id && a.CATALOG_ID == Catalogid);
                                    if (family_ids.Any())
                                    {
                                        foreach (var family in family_ids)
                                        {
                                            id = id + "," + Convert.ToString(family.FAMILY_ID);

                                            // insert record into TB_ATTRIBUTE_HIERARCHY table to associate upcoming Product or Family
                                            if (groupId != 0)
                                            {
                                                string familyIdstring = Convert.ToString(family.FAMILY_ID);
                                                TB_ATTRIBUTE_HIERARCHY attributeHierarchyInsert = new TB_ATTRIBUTE_HIERARCHY();
                                                attributeHierarchyInsert = _objLs.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == familyIdstring && s.PACK_ID == groupId && s.TYPE == "Family").FirstOrDefault();
                                                if (attributeHierarchyInsert == null)
                                                {
                                                    string QryString = "insert into TB_ATTRIBUTE_HIERARCHY values(" + groupId + ",'" + "Family" + "','" + familyIdstring + "'," + customer_Id + "," + "SUSER_NAME(),GETDATE(),SUSER_NAME(),GETDATE())";
                                                    SqlConnection conn = new SqlConnection(connectionString);
                                                    SqlCommand CmdObj = new SqlCommand(QryString, conn);
                                                    conn.Open();
                                                    CmdObj.ExecuteNonQuery();
                                                    conn.Close();
                                                }
                                            }


                                            //-------------------New Global Attributes ------------------                                            
                                            DataSet cat = new DataSet();
                                            _sqlString =
                                                "SELECT CATEGORY_ID FROM TB_CATALOG_FAMILY WHERE FAMILY_ID = " +
                                                Convert.ToString(family.FAMILY_ID) + " and catalog_id = " + Catalogid +
                                                "";
                                            cat = CreateDataSet();
                                            if (cat.Tables != null && cat.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow rr in cat.Tables[0].Rows)
                                                {
                                                    string ccatid = rr["CATEGORY_ID"].ToString();
                                                    Passtable.Rows.Add(ccatid, Convert.ToString(family.FAMILY_ID));
                                                    //var productId =
                                                    //    _objLs.TB_PROD_FAMILY.Where(x => x.FAMILY_ID == family.FAMILY_ID);
                                                    var subproductchk = _objLs.TB_SUBPRODUCT.Join(_objLs.TB_PROD_FAMILY, tps => tps.PRODUCT_ID, tpf => tpf.PRODUCT_ID,
                                                        (tps, tpf) => new { tps, tpf }).Where(x => x.tpf.FAMILY_ID == family.FAMILY_ID && x.tps.CATALOG_ID == Catalogid);

                                                    foreach (var tbProdFamily in subproductchk)
                                                    {
                                                        subProductSort.Rows.Add(ccatid,
                                                            Convert.ToString(tbProdFamily.tpf.FAMILY_ID),
                                                            Convert.ToString(tbProdFamily.tpf.PRODUCT_ID),
                                                            tbProdFamily.tps.SUBPRODUCT_ID);
                                                    }
                                                }
                                            }
                                            //--------------END------------------
                                        }
                                    }
                                }
                            }
                        }
                        //}
                        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            conn.Open();
                            _sqlString = "IF OBJECT_ID('TEMPDB..##Global') IS NOT NULL   DROP TABLE  ##Global ";
                            ExecuteSqlQuery();
                            string SQLString = null;
                            _sqlString = CreateTable("[##Global]", Passtable);
                            ExecuteSqlQuery();
                            var bulkCopy_cp = new SqlBulkCopy(conn)
                            {
                                DestinationTableName = "[##Global]"
                            };
                            bulkCopy_cp.WriteToServer(Passtable);


                        }
                        using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            conn.Open();
                            _sqlString = "IF OBJECT_ID('TEMPDB..##GlobalSort') IS NOT NULL begin  DROP TABLE  ##GlobalSort end ";
                            ExecuteSqlQuery();
                            //  SQLString = null;
                            if (subProductSort != null && subProductSort.Rows.Count > 0)
                            {
                                _sqlString = CreateTable("[##GlobalSort]", subProductSort);
                                ExecuteSqlQuery();
                                var bulkCopy_cpSort = new SqlBulkCopy(conn)
                                {
                                    DestinationTableName = "[##GlobalSort]"
                                };
                                bulkCopy_cpSort.WriteToServer(subProductSort);
                            }
                        }
                        if (prod_id != "")
                        {
                            string[] prodi = prod_id.Split(',');
                            for (int k = 0; k < prodi.Length; k++)
                            {
                                prod_id = prodi[k].ToString();

                                if (prod_id != "," && prod_id != "")
                                {
                                    UpdateGlobalAttrsub(selectedoption, Attrid, Catalogid, Attributetype, sort, prod_id);
                                    _objLs.SaveChanges();
                                }
                            }
                        }
                        else
                        {
                            if (subProductSort != null && subProductSort.Rows.Count > 0)
                            {
                                UpdateGlobalAttrsubmain(selectedoption, Attrid, Catalogid, Attributetype, sort,
                                    id.TrimStart(','));
                                _objLs.SaveChanges();
                            }
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);


            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  globalattributesub", ex);
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Error);
            }
        }

        [System.Web.Http.HttpGet]
        public JsonResult GetFamilySpecfication(int catalogId, string familyIdString, string CategoryId, int packId)
        {
            try
            {
                int familyId;
                string categoryId = string.Empty;
                int.TryParse(familyIdString.Trim('~'), out familyId);
                var dynamicColumns = new Dictionary<string, string>();
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                JsonWriter jsonWriter = new JsonTextWriter(sw);
                if (CategoryId.Trim() != "")
                {
                    var id = CategoryId.Split('~');
                    categoryId = id[0];
                }
                var objDataTable1 = new DataTable();
                var model = new DashBoardModel();
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_FamilyPivotTable_For_Publish";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar, 100).Value = familyId;
                    objSqlCommand.Parameters.Add("@USER_ID", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                    objSqlCommand.Parameters.Add("@GROUP_ID", SqlDbType.NVarChar).Value = packId;
                    objSqlConnection.Open();
                    var objDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objDataAdapter.Fill(objDataTable1);
                    if (objDataTable1 != null)
                    {
                        foreach (DataColumn columns in objDataTable1.Columns)
                        {
                            PTColumns objPTColumns = new PTColumns();
                            objPTColumns.Caption = columns.Caption;
                            objPTColumns.ColumnName = columns.ColumnName;//.Split(new[] {"__"}, StringSplitOptions.None)[0];
                            model.Columns.Add(objPTColumns);
                        }
                    }
                    string JSONString = string.Empty;
                    JSONString = JsonConvert.SerializeObject(objDataTable1);
                    model.Data = JSONString;
                    return new JsonResult() { Data = model };
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : GetProdSpecs", objexception);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public DataSourceResult GetFamilylevelPacks(int catalogId, int familyId, DataSourceRequest request)
        {
            try
            {
                HomeApiController homeObj = new HomeApiController();
                TB_CATALOG_FAMILY catalogFamily = new TB_CATALOG_FAMILY();
                List<string> categoryIds = new List<string>();
                catalogFamily = _objLs.TB_CATALOG_FAMILY.Where(s => s.FAMILY_ID == familyId && s.CATALOG_ID == catalogId).FirstOrDefault();
                string familyIdstring = Convert.ToString(familyId);
                categoryIds.Add(familyIdstring);
                var ds = new DataSet();
                string categoryId = string.Empty;
                if (catalogFamily != null && catalogFamily.CATALOG_ID > 0)
                {
                    categoryId = catalogFamily.CATEGORY_ID;
                    categoryIds.Add(categoryId);
                }

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESIGNEXPORT_IMPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = 0;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETPARENT";
                    objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.Int).Value = 0;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.VarChar).Value = categoryId;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(ds);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtRow in ds.Tables[0].Rows)
                    {
                        if (dtRow["CATEGORY_ID"].ToString() != null)
                        {
                            categoryIds.Add(dtRow["CATEGORY_ID"].ToString());
                        }
                    }
                }

                var familyAttributePack = (from attrHeirarchy in _objLs.TB_ATTRIBUTE_HIERARCHY
                                           join attributePack in _objLs.TB_PACKAGE_MASTER on attrHeirarchy.PACK_ID equals attributePack.GROUP_ID
                                           where attributePack.CATALOG_ID == catalogId && attributePack.IS_FAMILY.ToLower() == "Family" && attrHeirarchy.ASSIGN_TO == familyIdstring && attributePack.FLAG_RECYCLE == "A"
                                           select new
                                           {
                                               attributePack.GROUP_ID,
                                               attributePack.GROUP_NAME,
                                               ISAvailable = true,
                                           }).Distinct().ToList();

                var allfamilyAttributePacks = _objLs.TB_PACKAGE_MASTER.Where(s => s.CATALOG_ID == catalogId && s.IS_FAMILY.ToLower() == "Family" && s.FLAG_RECYCLE == "A").Select(z => new
                {
                    GROUP_ID = z.GROUP_ID,
                    GROUP_NAME = z.GROUP_NAME,
                    ISAvailable = false
                }).Distinct().ToList();


                List<AttributePack> attributePackList = new List<AttributePack>();

                foreach (var allfamilyAttributePack in allfamilyAttributePacks)
                {
                    bool isContain = false;
                    isContain = familyAttributePack.Any(s => s.GROUP_ID == allfamilyAttributePack.GROUP_ID);
                    AttributePack attributePack = new AttributePack();
                    attributePack.GROUP_ID = allfamilyAttributePack.GROUP_ID;
                    attributePack.GROUP_NAME = allfamilyAttributePack.GROUP_NAME;
                    if (isContain)
                        attributePack.ISAvailable = true;
                    else
                        attributePack.ISAvailable = false;
                    attributePackList.Add(attributePack);
                }

                return attributePackList.AsQueryable().ToDataSourceResult(request);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  GetFamilylevelPacks", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public DataSourceResult GetProductlevelPacks(int catalogId, int familyId, DataSourceRequest request)
        {
            try
            {
                HomeApiController homeObj = new HomeApiController();
                TB_CATALOG_FAMILY catalogFamily = new TB_CATALOG_FAMILY();
                List<string> categoryIds = new List<string>();
                catalogFamily = _objLs.TB_CATALOG_FAMILY.Where(s => s.FAMILY_ID == familyId && s.CATALOG_ID == catalogId).FirstOrDefault();
                string familyIdstring = Convert.ToString(familyId);
                categoryIds.Add(familyIdstring);
                var ds = new DataSet();
                string categoryId = string.Empty;
                if (catalogFamily != null && catalogFamily.CATALOG_ID > 0)
                {
                    categoryId = catalogFamily.CATEGORY_ID;
                    categoryIds.Add(categoryId);
                }

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_CATALOGSTUDIO5_INDESIGNEXPORT_IMPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = 0;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar).Value = "GETPARENT";
                    objSqlCommand.Parameters.Add("@PROJECT_ID", SqlDbType.Int).Value = 0;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.VarChar).Value = categoryId;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(ds);
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtRow in ds.Tables[0].Rows)
                    {
                        if (dtRow["CATEGORY_ID"].ToString() != null)
                        {
                            categoryIds.Add(dtRow["CATEGORY_ID"].ToString());
                        }
                    }
                }

                var productAttributePack = (from attrHeirarchy in _objLs.TB_ATTRIBUTE_HIERARCHY
                                            join attributePack in _objLs.TB_PACKAGE_MASTER on attrHeirarchy.PACK_ID equals attributePack.GROUP_ID
                                            where attributePack.CATALOG_ID == catalogId && attributePack.IS_FAMILY.ToLower() == "product" && attrHeirarchy.ASSIGN_TO == familyIdstring && attributePack.FLAG_RECYCLE == "A"
                                            select new
                                            {
                                                attributePack.GROUP_ID,
                                                attributePack.GROUP_NAME,
                                                ISAvailable = true,
                                            }).Distinct().ToList();

                var allproductAttributePacks = _objLs.TB_PACKAGE_MASTER.Where(s => s.CATALOG_ID == catalogId && s.IS_FAMILY.ToLower() == "product" && s.FLAG_RECYCLE == "A").Select(z => new
                {
                    GROUP_ID = z.GROUP_ID,
                    GROUP_NAME = z.GROUP_NAME,
                    ISAvailable = false
                }).Distinct().ToList();


                List<AttributePack> attributePackList = new List<AttributePack>();

                foreach (var allproductAttributePack in allproductAttributePacks)
                {
                    bool isContain = false;
                    isContain = productAttributePack.Any(s => s.GROUP_ID == allproductAttributePack.GROUP_ID);
                    AttributePack attributePack = new AttributePack();
                    attributePack.GROUP_ID = allproductAttributePack.GROUP_ID;
                    attributePack.GROUP_NAME = allproductAttributePack.GROUP_NAME;
                    if (isContain)
                        attributePack.ISAvailable = true;
                    else
                        attributePack.ISAvailable = false;
                    attributePackList.Add(attributePack);
                }

                return attributePackList.AsQueryable().ToDataSourceResult(request);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController:  GetFamilylevelPacks", ex);
                return null;
            }
        }
        [System.Web.Http.HttpPost]
        public bool UndoUpdateFamily(int catalogid, string Category_id, object model)
        {

            try
            {

                //    //if (familyName != "")
                //    //{
                //    //    familyName = AesEncrytDecry.DecryptStringAes(familyName);
                //    //}

                //    if (workflowstatus != "")
                //    {
                //        var objworkflow = _objLs.TB_WORKFLOW_STATUS.FirstOrDefault(k => k.STATUS_NAME == workflowstatus);
                //        if (objworkflow != null)
                //        {
                //            workflowstatuscode = objworkflow.STATUS_CODE;

                //        }
                //    }
                //    var objFamily = _objLs.TB_FAMILY.Single(s => s.FAMILY_ID == familyId);
                //    objFamily.FAMILY_NAME = familyName;
                //    objFamily.CATEGORY_ID = categoryId;
                //    objFamily.STATUS = status;
                //    objFamily.PUBLISH = publishToWeb;
                //    objFamily.PUBLISH2PRINT = publishToPrint;
                //    objFamily.WORKFLOW_STATUS = workflowstatuscode;
                //    objFamily.MODIFIED_USER = User.Identity.Name;
                //    objFamily.PUBLISH2WEB = publishToWeb;
                //    objFamily.PUBLISH2PDF = publishToPdf;
                //    objFamily.PUBLISH2EXPORT = publishToExport;
                //    objFamily.PUBLISH2PORTAL = publishToPortal;
                //    _objLs.SaveChanges();
                //    // return true;
                if (!string.IsNullOrEmpty(Category_id))
                {
                    var id = Category_id.Split('~');
                    Category_id = Convert.ToString(id[0]);
                }
                else
                {
                    Category_id = "0";
                }
                if (model != null)
                {
                    int textLength = 0;
                    var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(model.ToString());
                    int familyId = 0;
                    int catalogId = 0; string status = string.Empty; string family_Name = string.Empty; int workflowstatuscode = 0;
                    bool publishToWeb = false; bool publishToPrint = false; string workflowstatus = string.Empty; bool publishToPdf = false; bool publishToExport = false; bool publishToPortal = false;
                    if (values.ContainsKey("CATALOG_ID"))
                    {
                        if (!string.IsNullOrEmpty(values["CATALOG_ID"]))
                        {
                            catalogId = Convert.ToInt32(values["CATALOG_ID"]);
                        }
                    }
                    //if (values.ContainsKey("CATEGORY_ID"))
                    //{
                    //    if (!string.IsNullOrEmpty(values["CATEGORY_ID"]))
                    //    {
                    //        Category_id = values["CATEGORY_ID"].ToString();
                    //    }
                    //    else
                    //    {
                    //        Category_id = "0";
                    //    }
                    //}
                    if (values.ContainsKey("FAMILY_ID"))
                    {
                        if (!string.IsNullOrEmpty(values["FAMILY_ID"]))
                        {
                            familyId = Convert.ToInt32(values["FAMILY_ID"]);
                        }
                    }
                    if (values.ContainsKey("STATUS"))
                    {
                        if (!string.IsNullOrEmpty(values["STATUS"]))
                        {
                            status = values["STATUS"].ToString();
                        }
                    }
                    if (values.ContainsKey("PUBLISH"))
                    {
                        if (!string.IsNullOrEmpty(values["PUBLISH"]))
                        {
                            publishToWeb = Convert.ToBoolean(values["PUBLISH"]);
                        }
                    }
                    if (values.ContainsKey("PUBLISH2PRINT"))
                    {
                        if (!string.IsNullOrEmpty(values["PUBLISH2PRINT"]))
                        {
                            publishToPrint = Convert.ToBoolean(values["PUBLISH2PRINT"]);
                        }
                    }
                    if (values.ContainsKey("WORKFLOW_STATUS"))
                    {
                        if (!string.IsNullOrEmpty(values["WORKFLOW_STATUS"]))
                        {
                            workflowstatuscode = Convert.ToInt32(values["WORKFLOW_STATUS"]);
                        }
                    }
                    if (values.ContainsKey("PUBLISH2WEB"))
                    {
                        if (!string.IsNullOrEmpty(values["PUBLISH2WEB"]))
                        {
                            publishToWeb = Convert.ToBoolean(values["PUBLISH2WEB"]);
                        }
                    }
                    if (values.ContainsKey("PUBLISH2PDF"))
                    {
                        if (!string.IsNullOrEmpty(values["PUBLISH2PDF"]))
                        {
                            publishToPdf = Convert.ToBoolean(values["PUBLISH2PDF"]);
                        }
                    }
                    if (values.ContainsKey("PUBLISH2EXPORT"))
                    {
                        if (!string.IsNullOrEmpty(values["PUBLISH2EXPORT"]))
                        {
                            publishToExport = Convert.ToBoolean(values["PUBLISH2EXPORT"]);
                        }
                    }
                    if (values.ContainsKey("PUBLISH2PORTAL"))
                    {
                        if (!string.IsNullOrEmpty(values["PUBLISH2PORTAL"]))
                        {
                            publishToPortal = Convert.ToBoolean(values["PUBLISH2PORTAL"]);
                        }
                    }
                    if (values.ContainsKey("FAMILY_NAME"))
                    {
                        if (!string.IsNullOrEmpty(values["FAMILY_NAME"]))
                        {
                            family_Name = values["FAMILY_NAME"].ToString();
                        }
                    }
                    var objFamily = _objLs.TB_FAMILY.Single(s => s.FAMILY_ID == familyId);
                    objFamily.FAMILY_NAME = family_Name;
                    objFamily.CATEGORY_ID = Category_id;
                    objFamily.STATUS = status;
                    objFamily.PUBLISH = publishToWeb;
                    objFamily.PUBLISH2PRINT = publishToPrint;
                    objFamily.WORKFLOW_STATUS = workflowstatuscode;
                    objFamily.MODIFIED_USER = User.Identity.Name;
                    objFamily.PUBLISH2WEB = publishToWeb;
                    objFamily.PUBLISH2PDF = publishToPdf;
                    objFamily.PUBLISH2EXPORT = publishToExport;
                    objFamily.PUBLISH2PORTAL = publishToPortal;
                    _objLs.SaveChanges();
                    foreach (var value in values)
                    {
                        if (value.Key.Contains("__"))
                        {
                            var attributeNames = value.Key.Split(new[] { "__" }, StringSplitOptions.None);
                            // var attributeNames = value.Key.Split('_');
                            // string attributeName = attributeNames[0];
                            var attributeType = attributeNames[3];
                            var attributeId = Convert.ToInt32(attributeNames[2]);
                            string imageCaption = Convert.ToString(attributeNames[1]);
                            var attributenamedetails = _objLs.TB_ATTRIBUTE.Find(attributeId);

                            //attributeId = attributenames.ATTRIBUTE_ID;


                            var attributeDatatype = attributenamedetails.ATTRIBUTE_DATATYPE;
                            var stringvalue = HttpUtility.HtmlDecode(value.Value);

                            if (!string.IsNullOrEmpty(stringvalue))
                            {
                                if (stringvalue.ToLower().EndsWith("\n"))
                                {
                                    int sd = stringvalue.LastIndexOf("\n", StringComparison.Ordinal);
                                    stringvalue = stringvalue.Remove(sd);
                                }
                                if (stringvalue.ToLower().EndsWith("\r"))
                                {
                                    int sd = stringvalue.LastIndexOf("\r", StringComparison.Ordinal);
                                    stringvalue = stringvalue.Remove(sd);
                                }
                                if (stringvalue.ToLower().StartsWith("<p>") && stringvalue.ToLower().EndsWith("</p>"))
                                {
                                    int count = 0;
                                    int i = 0;
                                    while ((i = stringvalue.IndexOf("<p>", i, System.StringComparison.Ordinal)) != -1)
                                    {
                                        i += "<p>".Length;
                                        count++;
                                    }
                                    if (count == 1)
                                    {
                                        int endp = stringvalue.LastIndexOf("</p>", StringComparison.Ordinal);
                                        stringvalue = stringvalue.Remove(endp);
                                        int p = stringvalue.IndexOf("<p>", StringComparison.Ordinal);
                                        stringvalue = stringvalue.Remove(p, 3);
                                    }
                                }

                                stringvalue = stringvalue.Replace("\r", "");
                                stringvalue = stringvalue.Replace("\n\n", "\n");
                                stringvalue = stringvalue.Replace("\n", "\r\n");
                               // stringvalue = stringvalue.Replace("<br />", "\r\n");

                            }
                            if (!string.IsNullOrEmpty(stringvalue))
                            {
                                textLength = stringvalue.Length;
                            }
                            var objfamilySpecs = _objLs.TB_FAMILY_SPECS.FirstOrDefault(s => s.FAMILY_ID == familyId && s.ATTRIBUTE_ID == attributeId);
                            if (attributeType != "13")
                            {
                                if (objfamilySpecs != null)
                                {
                                    if (attributeType == "12")
                                    {
                                        if (string.IsNullOrEmpty(stringvalue))
                                        {
                                            objfamilySpecs.NUMERIC_VALUE = null;
                                            _objLs.SaveChanges();
                                        }
                                        else
                                        {
                                            decimal numericvalue;
                                            Decimal.TryParse(stringvalue, out numericvalue);
                                            objfamilySpecs.NUMERIC_VALUE = numericvalue;
                                            _objLs.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        if (attributeDatatype.ToLower().Contains("number"))
                                        {
                                            if (string.IsNullOrEmpty(stringvalue))
                                            {
                                                objfamilySpecs.NUMERIC_VALUE = null;
                                                _objLs.SaveChanges();
                                            }
                                            else
                                            {
                                                decimal numericvalue;
                                                Decimal.TryParse(stringvalue, out numericvalue);
                                                objfamilySpecs.NUMERIC_VALUE = numericvalue;
                                                _objLs.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            if (attributeDatatype.Contains('('))
                                            {
                                                string[] sizearray = attributeDatatype.Split('(');
                                                string[] size = sizearray[1].Split(')');
                                                if (textLength <= Convert.ToInt32(size[0]))
                                                {
                                                    objfamilySpecs.STRING_VALUE = stringvalue;
                                                    _objLs.SaveChanges();
                                                }
                                            }
                                            else if (attributeType == "9" && imageCaption == "OBJ")
                                            {

                                                objfamilySpecs.STRING_VALUE = stringvalue;
                                                _objLs.SaveChanges();

                                            }
                                            else if (attributeType == "9" && imageCaption != "OBJ")
                                            {
                                                objfamilySpecs.OBJECT_NAME = stringvalue;
                                                _objLs.SaveChanges();
                                            }
                                            else
                                            {
                                                objfamilySpecs.STRING_VALUE = stringvalue;
                                                _objLs.SaveChanges();
                                            }
                                        }
                                    }

                                }
                            }
                            else
                            {
                                var objfamilykey = _objLs.TB_FAMILY_KEY.FirstOrDefault(x => x.ATTRIBUTE_ID == attributeId && x.FAMILY_ID == familyId && x.CATALOG_ID == catalogId && x.CATEGORY_ID == Category_id);
                                if (objfamilykey == null)
                                {
                                    //objfamilykey.ATTRIBUTE_VALUE = stringvalue;
                                    //_objLs.SaveChanges();

                                    var objFamilypecs = new TB_FAMILY_KEY
                                    {
                                        CATALOG_ID = catalogId,
                                        ATTRIBUTE_ID = attributeId,
                                        FAMILY_ID = familyId,
                                        ATTRIBUTE_VALUE = stringvalue,
                                        CREATED_USER = User.Identity.Name,
                                        MODIFIED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now,
                                        CREATED_DATE = DateTime.Now,
                                        CATEGORY_ID = Category_id
                                    };

                                    _objLs.TB_FAMILY_KEY.Add(objFamilypecs);
                                    _objLs.SaveChanges();
                                }
                                else
                                {
                                    if (attributeDatatype.Contains('(') && !attributeDatatype.Contains(","))
                                    {
                                        string[] sizearray = attributeDatatype.Split('(');
                                        string[] size = sizearray[1].Split(')');
                                        if (textLength <= Convert.ToInt32(size[0]))
                                        {
                                            objfamilykey.ATTRIBUTE_VALUE = stringvalue;
                                            _objLs.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        objfamilykey.ATTRIBUTE_VALUE = stringvalue;
                                        _objLs.SaveChanges();
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }

            catch (Exception ex)
            {
                Logger.Error("Error at UndoUpdateFamily", ex);
                return false;
            }
        }
        #region Familly Attribute new design->To get all attributes
        [System.Web.Http.HttpPost]
        public DataTable GetAllFamilyAttributesNew(int catalogId, string familyid)
        {
            try
            {
                //  List<string> categoryAttributes=new List<string>();
                DataTable resultTable = new DataTable();
                //int[] attributeType = { 21, 23, 24, 25 };
                var customAttr =
                      _objLs.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(z => z.CustomerId).ToList();
                int customerID = 0;
                if (customAttr.Any())
                {
                    int.TryParse(customAttr[0].ToString(), out customerID);
                    HttpContext.Current.Session["customerId"] = customerID;
                }
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_GETALLFAMILY_ATTRIBUTES";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                    objSqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.NVarChar).Value = familyid;
                    objSqlCommand.Parameters.Add("@CUSTOMER_ID", SqlDbType.NVarChar, 100).Value = customerID;
                    objSqlConnection.Open();
                    var objDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objDataAdapter.Fill(resultTable);
                }


                return resultTable;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController : GetAllCatalogattributes", objException);
                return null;
            }
        }
        #endregion



        [System.Web.Http.HttpPost]

        public string UpdateValueForAttr(string category_id, int selectedFamilyId, int getSelectedRowProd_id, int Parentproduct_Id, bool blurCheck, Object model)
        {
            try
            {
                string message = "";
                var SpaceProvided = _objLs.TB_PLAN
                          .Join(_objLs.Customers, tps => tps.PLAN_ID, tpf => tpf.PlanId, (tps, tpf) => new { tps, tpf })
                          .Join(_objLs.Customer_User, tcptps => tcptps.tpf.CustomerId, tcp => tcp.CustomerId, (tcptps, tcp) => new { tcptps, tcp })
                          .Where(x => x.tcp.User_Name == User.Identity.Name).Select(x => x.tcptps.tps.StorageGB).ToList();
                double alotment = (double)SpaceProvided[0];
                var userId = _objLs.Customer_User.Join(_objLs.Customers, cu => cu.CustomerId, c => c.CustomerId, (cu, c) => new { cu, c }).Where(x => x.cu.User_Name == User.Identity.Name).Select(y => y.c.Comments).FirstOrDefault().ToString();
                double folderSize = 0;
                if (string.IsNullOrEmpty(userId))
                {
                    userId = "Questudio";
                }

                if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "")))
                {
                    Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + ""));
                }
                var path = System.Web.HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + userId + "");
                DirectoryInfo dInfo = new DirectoryInfo(path);
                long sizeOfDir = DirectorySize(dInfo, true);
                folderSize = (((double)sizeOfDir) / (double)(1024 * 1024 * 1024));

                double availableSpace = alotment - folderSize;
                if (availableSpace > 0)
                {
                    var usedPercentage = ((folderSize / alotment) * 100);

                    //// Need to work on Save - For Sengottuvel
                    message = "Saved successfully";

                    //***********Need to Get two Object model for Changed attribute  and unchanged attributes*********************
                    string[] getUpdateModel = ((IEnumerable)model).Cast<object>().Select(x => x.ToString()).ToArray();
                    var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(getUpdateModel[0].ToString());
                    var oldChangesAttributes = JsonConvert.DeserializeObject<Dictionary<string, string>>(getUpdateModel[1].ToString());

                    //*****************Remove Html Tag Based on the ckeditor************************************************
                    Dictionary<string, string> removeHtmlTagAttributes = new Dictionary<string, string>();

                    foreach (var removeTagAttribute in values)
                    {
                        var newAttrValue = HttpUtility.HtmlDecode(removeTagAttribute.Value);
                        var attributeNames = HttpUtility.HtmlDecode(removeTagAttribute.Key);
                        var newAttrNames = attributeNames.Split(new[] { "__" }, StringSplitOptions.None);
                        var newAttrName = newAttrNames[0];

                        if (newAttrName != "ID" && newAttrName != "PRODUCT_ID")
                        {

                            var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                            objSqlConnection.Open();

                            if (newAttrName == "NUMERIC_VALUE")
                            {
                                if (newAttrValue != null)
                                {
                                    var attribute_Id1 = _objLs.TB_PROD_SPECS.Where(z => z.PRODUCT_ID == getSelectedRowProd_id).Select(z => z.ATTRIBUTE_ID).ToList();

                                    string cmdString = string.Empty;

                                    foreach (int id in attribute_Id1)
                                    {
                                        cmdString = " UPDATE TB_PROD_SPECS SET NUMERIC_VALUE = '" + newAttrValue + "'   WHERE ATTRIBUTE_ID = " + id + " and PRODUCT_ID = " + getSelectedRowProd_id + "";
                                    }
                                    SqlCommand cmd = new SqlCommand(cmdString, objSqlConnection);
                                    cmd.ExecuteNonQuery();
                                }
                            }

                            if (newAttrValue != null)
                            {
                                int attribute_Id = _objLs.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == newAttrName).Select(y => y.ATTRIBUTE_ID).FirstOrDefault();

                                int str = _objLs.TB_PARTS_KEY.Where(y => y.ATTRIBUTE_ID == attribute_Id && y.PRODUCT_ID == getSelectedRowProd_id).Select(y => y.ATTRIBUTE_ID).FirstOrDefault();


                                if (attribute_Id == str)
                                {

                                    string cmdString1 = " UPDATE TB_PARTS_KEY SET ATTRIBUTE_VALUE = '" + newAttrValue + "' WHERE ATTRIBUTE_ID = " + attribute_Id + " and PRODUCT_ID = " + getSelectedRowProd_id + " and FAMILY_ID=" + selectedFamilyId + "";

                                    SqlCommand cmd1 = new SqlCommand(cmdString1, objSqlConnection);
                                    cmd1.ExecuteNonQuery();

                                    //string cmdString2 = " UPDATE TB_PROD_SPECS SET STRING_VALUE = '" + newAttrValue + "' WHERE ATTRIBUTE_ID = " + attribute_Id + " and PRODUCT_ID = " + getSelectedRowProd_id + "";


                                    //SqlCommand cmd1 = new SqlCommand(cmdString2, objSqlConnection);                 
                                    //cmd1.ExecuteNonQuery();
                                }

                                string cmdString3 = " UPDATE TB_PROD_SPECS SET STRING_VALUE = '" + newAttrValue + "'   WHERE ATTRIBUTE_ID = " + attribute_Id + " and PRODUCT_ID = " + getSelectedRowProd_id + "";

                                SqlCommand cmd3 = new SqlCommand(cmdString3, objSqlConnection);
                                cmd3.ExecuteNonQuery();




                                message = string.Format(message + ".You are about to reach the maximum memory");
                            }

                        }
                    }
                }
                return message;
            }

            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: UpdateValueForAttr", ex);
                return "Product Specifications are not updated";
            }
        }

        private class GroupListItemEqualityComparer
        {
            public GroupListItemEqualityComparer()
            {
            }
        }
    }





    public partial class TB_USER1
    {

        public string TB_USER_ID { get; set; }
        public string STATUS { get; set; }
        public string Comments { get; set; }
        public int Customer_User_Id { get; set; }
        public int CustomerId { get; set; }
        public string User_Name { get; set; }
        public string Password { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateUpdated { get; set; }
        public string Address { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Country { get; set; }
        public string Fax { get; set; }
        public string UserRoleName { get; set; }
        public string Active { get; set; }
        public string CATALOG_ID { get; set; }
        public string organisation { get; set; }
        public string UserRoles { get; set; }

        public virtual Customer Customer { get; set; }
    }


}
