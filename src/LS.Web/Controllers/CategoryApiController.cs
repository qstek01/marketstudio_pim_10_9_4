﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using LS.Data;
using log4net;
using System.Web.Http;
using Kendo.DynamicLinq;
using LS.Data.Model.ProductView;
using Newtonsoft.Json;
using LS.Data.Model.CatalogSectionModels;
using LS.Data.Model;
using System.Net.Http;
using System.IO;
using System.Web;
using System.Net;
using LS.Data.Utilities;
using System.Data.Entity.Validation;
using Newtonsoft.Json.Linq;
using LS.Data.Model.CatalogSectionModels;
using System.Text;
using System.Security.Cryptography;

namespace LS.Web.Controllers
{
    public class CategoryApiController : ApiController
    {
        // GET: Family
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CategoryApiController));

        private readonly CSEntities _dbcontext = new CSEntities();



        [HttpGet]
        public IList GetOriginalCategory(string id, string catalogId, string option, string categoryId)
        {

            //if (request.Sort == null || !request.Sort.Any())
            //{
            //    request.Sort = new List<Sort> { new Sort { Field = "CATEGORY_ID", Dir = "asc" } };
            //}
            if (id == null)
            {
                id = "";
            }
            try
            {
                if (option == "ORIGINAL")
                {
                    int familyIdInt;
                    int catalogids;
                    int.TryParse(id.Trim('~'), out familyIdInt);
                    int.TryParse(catalogId.Trim('~'), out catalogids);
                    string categoryIds = string.Empty;
                    using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {

                        SqlCommand sqlCommand = new SqlCommand("STP_CATALOGSTUDIO_GETCATEGORYLIST", con);
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyIdInt;
                        sqlCommand.Parameters.Add("@CATALOGID", SqlDbType.Int).Value = catalogId;
                        sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar, 100).Value = "ORIGINAL";
                        sqlCommand.Parameters.Add("@CATEGORYIDS", SqlDbType.VarChar, 200);
                        sqlCommand.Parameters["@CATEGORYIDS"].Direction = ParameterDirection.Output;
                        sqlCommand.CommandTimeout = 0;
                        con.Open();
                        sqlCommand.ExecuteNonQuery();
                        con.Close();
                        if (familyIdInt != 0)
                            categoryIds = (string)sqlCommand.Parameters["@CATEGORYIDS"].Value;
                    }

                    var objoriginalcategory =
                    _dbcontext.TB_FAMILY.Join(_dbcontext.TB_CATALOG_FAMILY, tf => tf.FAMILY_ID, tcf => tcf.FAMILY_ID,
                        (tf, tcf) => new { tf, tcf })
                        .Join(_dbcontext.TB_CATEGORY, tcatf => tcatf.tcf.CATEGORY_ID, tc => tc.CATEGORY_ID,
                            (tcatf, tc) => new { tcatf, tc }).Where(x =>
                                (x.tcatf.tf.FAMILY_ID == familyIdInt && x.tcatf.tcf.ROOT_CATEGORY == "0" && x.tcatf.tcf.CATALOG_ID != 1 && x.tc.FLAG_RECYCLE == "A" && x.tcatf.tcf.FLAG_RECYCLE == "A"))
                        .Select(z => new
                        {
                            id = categoryIds,
                            z.tcatf.tcf.CATALOG_ID,
                            z.tcatf.tcf.TB_CATALOG.CATALOG_NAME,
                            z.tcatf.tf.FAMILY_ID,
                            z.tcatf.tf.FAMILY_NAME,
                            PARENT_FAMILY_ID =
                                _dbcontext.TB_SUBFAMILY.Any(x => x.SUBFAMILY_ID == familyIdInt)
                                    ? z.tcatf.tf.PARENT_FAMILY_ID
                                    : 0,
                            ISSUBFAMILY = _dbcontext.TB_SUBFAMILY.Any(x => x.SUBFAMILY_ID == familyIdInt),
                            z.tc.CATEGORY_NAME,
                            z.tc.CATEGORY_ID,
                            z.tc.CATEGORY_SHORT,
                            Original = _dbcontext.TB_CATALOG_FAMILY.Any(x => x.FAMILY_ID == familyIdInt && x.CATALOG_ID == catalogids && x.CATEGORY_ID == categoryId && x.ROOT_CATEGORY == "0" && x.FLAG_RECYCLE == "A"),
                            CATEGORY_LEVEL =
                                    z.tc.PARENT_CATEGORY == "0"
                                        ? "Root"
                                        : _dbcontext.TB_CATEGORY.FirstOrDefault(
                                            x => x.CATEGORY_ID == z.tc.PARENT_CATEGORY).CATEGORY_NAME
                        }).Distinct().ToList();

                    return objoriginalcategory;
                }
                else
                {
                    int familyIdInt;
                    int catalogids;
                    int.TryParse(id.Trim('~'), out familyIdInt);
                    int.TryParse(catalogId.Trim('~'), out catalogids);
                    string categoryIds = string.Empty;
                    using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {

                        SqlCommand sqlCommand = new SqlCommand("STP_CATALOGSTUDIO_GETCATEGORYLIST", con);
                        sqlCommand.CommandType = CommandType.StoredProcedure;
                        sqlCommand.Parameters.Add("@FAMILY_ID", SqlDbType.Int).Value = familyIdInt;
                        sqlCommand.Parameters.Add("@CATALOGID", SqlDbType.Int).Value = catalogId;
                        sqlCommand.Parameters.Add("@OPTION", SqlDbType.VarChar, 100).Value = "CLONE";
                        sqlCommand.Parameters.Add("@CATEGORYIDS", SqlDbType.VarChar, 200);
                        sqlCommand.Parameters["@CATEGORYIDS"].Direction = ParameterDirection.Output;
                        sqlCommand.CommandTimeout = 0;
                        con.Open();
                        sqlCommand.ExecuteNonQuery();
                        con.Close();
                        categoryIds = (string)sqlCommand.Parameters["@CATEGORYIDS"].Value;

                    }
                    var objoriginalcategory1 =
                        _dbcontext.TB_FAMILY.Join(_dbcontext.TB_CATALOG_FAMILY, tf => tf.FAMILY_ID, tcf => tcf.FAMILY_ID,
                            (tf, tcf) => new { tf, tcf })
                            .Join(_dbcontext.TB_CATEGORY, tcatf => tcatf.tcf.CATEGORY_ID, tc => tc.CATEGORY_ID,
                                (tcatf, tc) => new { tcatf, tc }).Where(x =>
                                    (x.tcatf.tf.FAMILY_ID == familyIdInt && x.tcatf.tcf.ROOT_CATEGORY != "0" && x.tcatf.tcf.CATALOG_ID != 1 && x.tc.FLAG_RECYCLE == "A" && x.tcatf.tcf.FLAG_RECYCLE == "A")).ToList()
                            .Select(z => new
                            {
                                id = categoryIds,
                                z.tcatf.tcf.CATALOG_ID,
                                z.tcatf.tcf.TB_CATALOG.CATALOG_NAME,
                                z.tcatf.tf.FAMILY_ID,
                                z.tcatf.tf.FAMILY_NAME,
                                PARENT_FAMILY_ID =
                                    _dbcontext.TB_SUBFAMILY.Any(x => x.SUBFAMILY_ID == familyIdInt)
                                        ? z.tcatf.tf.PARENT_FAMILY_ID
                                        : 0,
                                ISSUBFAMILY = _dbcontext.TB_SUBFAMILY.Any(x => x.SUBFAMILY_ID == familyIdInt),
                                z.tc.CATEGORY_NAME,
                                z.tc.CATEGORY_ID,
                                z.tc.CATEGORY_SHORT,
                                CATEGORY_LEVEL =
                                    z.tc.PARENT_CATEGORY == "0" ? "Root" : _dbcontext.STP_LS_CATEGORYLEVEL(z.tcatf.tcf.CATALOG_ID, z.tc.CATEGORY_ID).FirstOrDefault()
                            }).Distinct().ToList();
                    //var wef = _dbcontext.STP_LS_CATEGORYLEVEL(2, "CLONE1-saple ").FirstOrDefault(); 
                    // var fdgd = objoriginalcategory1.ToDataSourceResult(request);
                    return objoriginalcategory1;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CategoryApiController: GetOriginalCategory", ex);
                return null;
            }
        }

        [HttpGet]
        public string RemoveClonedFamily(string id, string catalogId, string categoryId)
        {
            try
            {
                int familyIdInt;
                int catalogids;
                int.TryParse(id.Trim('~'), out familyIdInt);
                int.TryParse(catalogId.Trim('~'), out catalogids);
                const string xml = "<?xml version=\"1.0\" standalone=\"yes\"?><Deleted_Items>";

                var random = new Random();
                int xmlID = random.Next(1, 1000);
                using (
                    var con =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    con.Open();

                    string catalogXML = "<?xml version=\"1.0\" standalone=\"yes\"?><CATALOG_ASSOCIATES>";
                    var cmd =
                        new SqlCommand(
                            "SELECT CATALOG_ID,SORT_ORDER,FAMILY_ID,CATEGORY_ID,CREATED_USER,CREATED_DATE,ROOT_CATEGORY FROM TB_CATALOG_FAMILY WHERE CATALOG_ID!=1 " +
                            "AND FAMILY_ID in (" + familyIdInt + ") AND CATEGORY_ID='" + categoryId +
                            "' for xml path('CATALOG_FAMILY'),type", con);
                    var dafam = new SqlDataAdapter(cmd);
                    var ds = new DataSet();
                    dafam.Fill(ds, "CATALOG_FAMILY");
                    if (ds.Tables["CATALOG_FAMILY"].Rows.Count > 0 &&
                        ds.Tables["CATALOG_FAMILY"].Rows[0][0].ToString() != "" &&
                        ds.Tables["CATALOG_FAMILY"].Rows[0][0] != null)
                    {
                        catalogXML = catalogXML + ds.Tables["CATALOG_FAMILY"].Rows[0][0];
                    }
                    if (catalogXML != "<?xml version=\"1.0\" standalone=\"yes\"?><CATALOG_ASSOCIATES>")
                    {
                        //var firstOrDefault = _dbcontext.TB_FAMILY.FirstOrDefault(x => x.FAMILY_ID == familyIdInt);
                        //if (firstOrDefault != null)
                        //{
                        //    var objRecycleTable = new TB_RECYCLE_TABLE();
                        //    objRecycleTable.XML_ID = xmlID;
                        //    objRecycleTable.XML_DOC = xml;
                        //    objRecycleTable.FAMILY_ID = familyIdInt;
                        //    objRecycleTable.CATALOG_ID = catalogids;
                        //    objRecycleTable.FAMILY_NAME = firstOrDefault.FAMILY_NAME;
                        //    objRecycleTable.CATEGORY_ID = null;
                        //    objRecycleTable.CATEGORY_NAME = null;
                        //    objRecycleTable.DELETED_DATE = DateTime.Now;
                        //    objRecycleTable.DELETED_USER = User.Identity.Name;
                        //    _dbcontext.TB_RECYCLE_TABLE.Add(objRecycleTable);
                        //    _dbcontext.SaveChanges();
                        //}

                        //string sqlstr1 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,FAMILY_ID,FAMILY_NAME,XML_DOC) VALUES"
                        //                     + "('" + xmlID + "'," + catalogId + ","
                        //                     + " '" + familyIdInt + "',(SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" +
                        //                     id + "'),'" + xml + "</Deleted_Items>" + "' ) ";
                        //cmd = new SqlCommand(sqlstr1, con);
                        //cmd.ExecuteNonQuery();
                        //catalogXML = catalogXML + "</CATALOG_ASSOCIATES>";
                        //catalogXML = catalogXML.Replace("'", "''");




                        //var catalogobjRecycleTable = new TB_RECYCLE_TABLE();
                        //catalogobjRecycleTable.XML_ID = xmlID;
                        //catalogobjRecycleTable.XML_DOC = catalogXML;
                        //catalogobjRecycleTable.FAMILY_ID = familyIdInt;
                        //catalogobjRecycleTable.DELETED_DATE = DateTime.Now;
                        //catalogobjRecycleTable.DELETED_USER = User.Identity.Name;
                        //_dbcontext.TB_RECYCLE_TABLE.Add(catalogobjRecycleTable);
                        //_dbcontext.SaveChanges();
                        string sqlstr1 = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,CATALOG_ID,FAMILY_ID,FAMILY_NAME,XML_DOC,DELETED_USER,DELETED_DATE) VALUES"
                                 + "('" + xmlID + "'," + catalogId + ","
                                 + " '" + familyIdInt + "',(SELECT FAMILY_NAME FROM TB_FAMILY WHERE FAMILY_ID='" + familyIdInt + "'),'" + xml + "</Deleted_Items>" + "','" +
                                            User.Identity.Name + "',GETDATE()) ";
                        cmd = new SqlCommand(sqlstr1, con);
                        cmd.ExecuteNonQuery();
                        catalogXML = catalogXML + "</CATALOG_ASSOCIATES>";
                        catalogXML = catalogXML.Replace("'", "''");
                        string sqlstr = "INSERT INTO TB_RECYCLE_TABLE (XML_ID,FAMILY_ID,XML_DOC,DELETED_USER,DELETED_DATE) VALUES('" + xmlID + "','" + familyIdInt + "','" + catalogXML + "','" +
                                            User.Identity.Name + "',GETDATE()) ";
                        cmd = new SqlCommand(sqlstr, con);
                        cmd.ExecuteNonQuery();
                        List<STP_CATALOGSTUDIO5_CategoryLevel_Result> objcategorylevel = _dbcontext.STP_CATALOGSTUDIO5_CategoryLevel(categoryId).ToList();

                        // string categoryLevel = " exec STP_CATALOGSTUDIO5_CategoryLevel '" + categoryId + "'";
                        //  cmd = new SqlCommand(categoryLevel, con);
                        //  var dsCatlevel = new DataSet();
                        // var dafam1 = new SqlDataAdapter(cmd);
                        // dafam1.Fill(dsCatlevel);
                        foreach (var drcat in objcategorylevel)
                        {
                            string sqlstr2 = "INSERT INTO TB_RECYCLE_CATEGORY_REFERENCE (XML_ID,CAT_ID,LEVEL,CREATED_USER,CREATED_DATE) VALUES"
                                             + "('" + xmlID + "','" + drcat.CATEGORY_ID + "','" +
                                             drcat.LEVEL + "','" +
                                            User.Identity.Name + "',GETDATE())";
                            cmd = new SqlCommand(sqlstr2, con);
                            cmd.ExecuteNonQuery();

                            //var objRecycleTablecategoryreference = new TB_RECYCLE_CATEGORY_REFERENCE();
                            //catalogobjRecycleTable.XML_ID = xmlID;
                            //catalogobjRecycleTable.XML_DOC = catalogXML;
                            //catalogobjRecycleTable.FAMILY_ID = familyIdInt;
                            //_dbcontext.TB_RECYCLE_TABLE.Add(catalogobjRecycleTable);
                            //_dbcontext.SaveChanges();
                        }
                    }
                    //var cmdClone =
                    //    new SqlCommand(
                    //        "DELETE FROM TB_CATALOG_FAMILY WHERE FAMILY_ID= " + familyIdInt + " AND CATEGORY_ID='" +
                    //        categoryId + "'", con)
                    //    {
                    //        CommandTimeout = 0
                    //    };

                    //cmdClone.ExecuteNonQuery();
                    //con.Close();

                    var deletefamily =
                        _dbcontext.TB_CATALOG_FAMILY.Where(
                            x => x.FAMILY_ID == familyIdInt && x.CATEGORY_ID == categoryId);
                    foreach (var tbCatalogFamily in deletefamily)
                    {
                        _dbcontext.TB_CATALOG_FAMILY.Remove(tbCatalogFamily);
                    }
                    _dbcontext.SaveChanges();
                }
                return "Delete successful";
            }
            catch (Exception ex)
            {

                Logger.Error("Error at CategoryApiController: RemoveClonedFamily", ex);
                return "";
            }



        }
        [HttpGet]
        public string ChkMasterFamily(string id, string catalogId, string categoryId)
        {
            try
            {
                int familyIdInt;
                int catalogids;
                int.TryParse(id.Trim('~'), out familyIdInt);
                int.TryParse(catalogId.Trim('~'), out catalogids);
                var dsclonedata = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_CATEGORY, tcatf => tcatf.CATEGORY_ID,
                          tc => tc.CATEGORY_ID,
                          (tcatf, tc) => new { tcatf, tc })
                          .Where(x =>
                                  (x.tcatf.FAMILY_ID == familyIdInt && x.tcatf.ROOT_CATEGORY != "0" && x.tcatf.CATALOG_ID != 1 &&
                                   x.tc.CATEGORY_ID == categoryId));
                if (dsclonedata.Any())
                {
                    return "This action will switch the original family to selected cloned family, do you want to continue?";
                }
                return "This action will switch the cloned family to the original family, do you want to continue?";
            }
            catch (Exception ex)
            {

                Logger.Error("Error at CategoryApiController: SwitchMasterFamily", ex);
                return "";
            }



        }
        [HttpGet]
        public string SwitchMasterFamily(string id, string catalogId, string categoryId)
        {
            try
            {
                int familyIdInt;
                int catalogids;
                int.TryParse(id.Trim('~'), out familyIdInt);
                int.TryParse(catalogId.Trim('~'), out catalogids);
                //DialogResult diag = new DialogResult();
                //CategoryID = otherclonecatgrid.Rows[e.Cell.Row.Index].Cells["CATEGORY_ID"].Text;
                //var ocon = new DBConnection();
                //ocon._SQLString = "SELECT C.CATEGORY_ID AS  [CATEGORY ID] ,C.CATEGORY_NAME AS  [CATEGORY NAME] FROM TB_CATALOG_FAMILY CF"
                //                  + " INNER JOIN TB_CATEGORY C ON C.CATEGORY_ID=CF.CATEGORY_ID"
                //                  + " WHERE CF.FAMILY_ID='" + FamilyID +
                //                  "' AND ROOT_CATEGORY='0' AND CF.CATALOG_ID<>1 AND C.CATEGORY_ID='" + CategoryID + "'";

                //DataSet DsCloneData = ocon.CreateDataSet();
                //if (DsCloneData.Tables[0].Rows.Count > 0)
                //{
                //    diag =
                //        MessageBox.Show(
                //            "This action will switch the original family to selected cloned family. Do you want to continue?'",
                //            "", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                //}
                //else
                //{
                //    diag =
                //        MessageBox.Show(
                //            "This action will switch the cloned family to the original family. Do you want to continue?",
                //            "", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                //}

                //var cmd = new SqlCommand
                //{
                //    CommandText = "STP_CATALOGSTUDIO5_SwitchMaster",
                //    CommandType = CommandType.StoredProcedure,
                //    Connection = Ocon.GetInstance()
                //};

                _dbcontext.STP_CATALOGSTUDIO5_SwitchMaster(categoryId, familyIdInt, 0);
                _dbcontext.SaveChanges();
                return "Cloned family marked master successfully";

            }
            catch (Exception ex)
            {

                Logger.Error("Error at CategoryApiController: SwitchMasterFamily", ex);
                return "";
            }



        }

        [HttpGet]
        public string SwitchMasterFamilyNavigatorRemove(string id, string catalogId, string categoryId)
        {
            try
            {
                int familyIdInt;
                int catalogids;
                int.TryParse(id.Trim('~'), out familyIdInt);
                int.TryParse(catalogId.Trim('~'), out catalogids);
                _dbcontext.STP_CATALOGSTUDIO5_SwitchMaster(categoryId, familyIdInt, 1);
                _dbcontext.SaveChanges();
                return "Cloned family successfully changed to master";

            }
            catch (Exception ex)
            {

                Logger.Error("Error at CategoryApiController: SwitchMasterFamilyNavigatorRemove", ex);
                return "";
            }



        }
        [HttpGet]
        public IList GetCategoryLevels(string categoryId, int catalogId)
        {

            //if (request.Sort == null || !request.Sort.Any())
            //{
            //    request.Sort = new List<Sort> { new Sort { Field = "CATEGORY_ID", Dir = "asc" } };
            //}
            if (categoryId == null)
            {
                categoryId = "";
            }
            try
            {
                var categorylevel = _dbcontext.CATEGORY_FUNCTION_TOP(catalogId, categoryId).ToList();
                return categorylevel;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CategoryApiController: GetCategoryLevels", ex);
                return null;
            }
        }

        #region Attribute Pack

        [System.Web.Http.HttpPost]
        public DataSourceResult GetFamilyPacksforCategory(int catalogId, string categoryId, DataSourceRequest request)
        {
            try
            {
                HomeApiController homeObj = new HomeApiController();
                string category_Id = categoryId;
                List<TB_ATTRIBUTE_HIERARCHY> attributeHierarchy = new List<TB_ATTRIBUTE_HIERARCHY>();
                attributeHierarchy = _dbcontext.TB_ATTRIBUTE_HIERARCHY.Where(s => s.ASSIGN_TO == category_Id).ToList();
                foreach (var item in attributeHierarchy)
                {
                    if (item.TYPE.Contains("Pending"))
                    {
                        string[] result;
                        result = item.TYPE.Split(new string[] { "Pending" }, StringSplitOptions.None);
                        if (result[0] == "Category")
                        {
                            string[] Id = result[1].Split(',');
                            foreach (var groupId in Id)
                            {
                                int group_Id = Convert.ToInt32(groupId);
                                homeObj.AssociateAttributePackaging(category_Id, group_Id, catalogId);
                            }
                        }

                        _dbcontext.TB_ATTRIBUTE_HIERARCHY.Remove(item);
                        _dbcontext.SaveChanges();
                    }
                }

                var cat_familyAttributePack = (from attrHeirarchy in _dbcontext.TB_ATTRIBUTE_HIERARCHY
                                               join attributePack in _dbcontext.TB_PACKAGE_MASTER on attrHeirarchy.PACK_ID equals attributePack.GROUP_ID
                                               where attributePack.CATALOG_ID == catalogId && attributePack.IS_FAMILY.ToLower() == "family" && attrHeirarchy.ASSIGN_TO == categoryId && attributePack.FLAG_RECYCLE == "A"
                                               select new
                                               {
                                                   attributePack.GROUP_ID,
                                                   attributePack.GROUP_NAME,
                                                   ISAvailable = true,
                                               }).Distinct().ToList();

                var allfamilyAttributePacks = _dbcontext.TB_PACKAGE_MASTER.Where(s => s.CATALOG_ID == catalogId && s.IS_FAMILY.ToLower() == "family" && s.FLAG_RECYCLE == "A").Select(z => new
                {
                    GROUP_ID = z.GROUP_ID,
                    GROUP_NAME = z.GROUP_NAME,
                    ISAvailable = false
                }).Distinct().ToList();


                List<AttributePack> attributePackList = new List<AttributePack>();

                foreach (var allfamilyAttributePack in allfamilyAttributePacks)
                {
                    bool isContain = false;
                    isContain = cat_familyAttributePack.Any(s => s.GROUP_ID == allfamilyAttributePack.GROUP_ID);
                    AttributePack attributePack = new AttributePack();
                    attributePack.GROUP_ID = allfamilyAttributePack.GROUP_ID;
                    attributePack.GROUP_NAME = allfamilyAttributePack.GROUP_NAME;
                    if (isContain)
                        attributePack.ISAvailable = true;
                    else
                        attributePack.ISAvailable = false;
                    attributePackList.Add(attributePack);
                }


                return attributePackList.AsQueryable().ToDataSourceResult(request);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CategoryApiController : GetFamilyPacksforCategory", ex);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public DataSourceResult GetProductPacksforCategory(int catalogId, string categoryId, DataSourceRequest request)
        {
            try
            {
                HomeApiController homeObj = new HomeApiController();
                var cat_ProductAttributePack = (from attrHeirarchy in _dbcontext.TB_ATTRIBUTE_HIERARCHY
                                                join attributePack in _dbcontext.TB_PACKAGE_MASTER on attrHeirarchy.PACK_ID equals attributePack.GROUP_ID
                                                where attributePack.CATALOG_ID == catalogId && attributePack.IS_FAMILY.ToLower() == "product" && attrHeirarchy.ASSIGN_TO == categoryId && attributePack.FLAG_RECYCLE == "A"
                                                select new
                                                {
                                                    attributePack.GROUP_ID,
                                                    attributePack.GROUP_NAME,
                                                    ISAvailable = true,
                                                }).Distinct().ToList();

                var allProductAttributePacks = _dbcontext.TB_PACKAGE_MASTER.Where(s => s.CATALOG_ID == catalogId && s.IS_FAMILY.ToLower() == "product" && s.FLAG_RECYCLE == "A").Select(z => new
                {
                    GROUP_ID = z.GROUP_ID,
                    GROUP_NAME = z.GROUP_NAME,
                    ISAvailable = false
                }).Distinct().ToList();

                List<AttributePack> attributePackList = new List<AttributePack>();

                foreach (var allProductAttributePack in allProductAttributePacks)
                {
                    bool isContain = false;
                    isContain = cat_ProductAttributePack.Any(s => s.GROUP_ID == allProductAttributePack.GROUP_ID);
                    AttributePack attributePack = new AttributePack();
                    attributePack.GROUP_ID = allProductAttributePack.GROUP_ID;
                    attributePack.GROUP_NAME = allProductAttributePack.GROUP_NAME;
                    if (isContain)
                        attributePack.ISAvailable = true;
                    else
                        attributePack.ISAvailable = false;
                    attributePackList.Add(attributePack);
                }

                return attributePackList.AsQueryable().ToDataSourceResult(request);
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CategoryApiController : GetProductPacksforCategory", ex);
                return null;
            }
        }


        #endregion

        [System.Web.Http.HttpPost]
        public HttpResponseMessage savenewcategoryattribute(NewAttribute model, int catalogId)
        {
            try
            {
                model.ATTRIBUTE_DATARULE = AttrDataRuleDS(model);
                if (model.ATTRIBUTE_DATATYPE.ToLower().Contains("number"))
                {
                    if (model.ATTRIBUTE_SIZE != 0)
                    {
                        if (model.ATTRIBUTE_SIZE == 0 && model.ATTRIBUTE_DECIMAL != 0)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Please enter size and continue");
                        }
                        else if (model.ATTRIBUTE_SIZE != 0 && model.ATTRIBUTE_DECIMAL != 0)
                        {
                            if (model.ATTRIBUTE_SIZE <= 13)
                            {
                                model.ATTRIBUTE_DATATYPE = model.ATTRIBUTE_DATATYPE + "(" + model.ATTRIBUTE_SIZE + "," +
                                                           model.ATTRIBUTE_DECIMAL + ")";
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, "The Maximum Digit You Can Enter Is 13.");
                            }
                        }
                        else
                        {
                            if (model.ATTRIBUTE_SIZE <= 13)
                            {
                                model.ATTRIBUTE_DATATYPE = model.ATTRIBUTE_DATATYPE + "(" + model.ATTRIBUTE_SIZE + "," + 0 + ")";
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, "The Maximum Digit You Can Enter Is 13.");
                            }
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Please enter size and continue");
                    }
                }
                else if (model.ATTRIBUTE_DATATYPE.ToLower().Contains("text"))
                {
                    if (model.ATTRIBUTE_SIZE != 0)
                    {
                        model.ATTRIBUTE_DATATYPE = model.ATTRIBUTE_DATATYPE + "(" + model.ATTRIBUTE_SIZE + ")";
                    }
                }
                else
                {
                    model.ATTRIBUTE_DATATYPE = model.ATTRIBUTE_DATATYPE;
                }
                var customerid = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name);
                if (customerid.Any())
                {
                    var customerIds = customerid.FirstOrDefault();
                    if (customerIds != null)
                    {
                        var attributecount = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                                    tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                    .Count(x => x.tcp.ATTRIBUTE_NAME == model.ATTRIBUTE_NAME || x.tcp.CAPTION == model.ATTRIBUTE_NAME && x.tpf.CUSTOMER_ID == customerIds.CustomerId);
                        var captioncount = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                               tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                               .Count(x => x.tcp.ATTRIBUTE_NAME == model.CAPTION || x.tcp.CAPTION == model.CAPTION && x.tpf.CUSTOMER_ID == customerIds.CustomerId);
                        if (attributecount != 0 && captioncount != 0)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, "Attribute Name and Caption Name already exists, please enter a different Names and create the Attribute");
                        }
                        if (attributecount == 0)
                        {
                            if (captioncount != 0)
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, "Caption Name already exists, please enter a different Name and create the Attribute");
                            }
                            string caption = string.Empty;
                            caption = model.CAPTION;
                            if (string.IsNullOrWhiteSpace(caption))
                            {
                                caption = model.ATTRIBUTE_NAME;
                            }
                            var objattribute = new TB_ATTRIBUTE
                            {
                                ATTRIBUTE_NAME = model.ATTRIBUTE_NAME,
                                ATTRIBUTE_TYPE = model.ATTRIBUTE_TYPE,
                                CREATE_BY_DEFAULT = model.CREATE_BY_DEFAULT,
                                VALUE_REQUIRED = model.VALUE_REQUIRED,
                                STYLE_NAME = model.STYLE_NAME,
                                STYLE_FORMAT = model.STYLE_FORMAT,
                                DEFAULT_VALUE = model.DEFAULT_VALUE,
                                PUBLISH2PRINT = model.PUBLISH2PRINT,
                                PUBLISH2WEB = model.PUBLISH2WEB,
                                PUBLISH2CDROM = false,
                                PUBLISH2ODP = false,
                                USE_PICKLIST = model.USE_PICKLIST,
                                ATTRIBUTE_DATATYPE = model.ATTRIBUTE_DATATYPE,
                                ATTRIBUTE_DATAFORMAT = model.ATTRIBUTE_DATAFORMAT,
                                ATTRIBUTE_DATARULE = model.ATTRIBUTE_DATARULE,
                                UOM = model.UOM,
                                IS_CALCULATED = model.IS_CALCULATED,
                                ATTRIBUTE_CALC_FORMULA = model.ATTRIBUTE_CALC_FORMULA,
                                PICKLIST_NAME = model.PICKLIST_NAME,
                                MODIFIED_USER = User.Identity.Name,
                                CREATED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                CREATED_DATE = DateTime.Now,
                                FLAG_RECYCLE = "A",
                                PUBLISH2EXPORT = model.PUBLISH2EXPORT,
                                PUBLISH2PDF = model.PUBLISH2PDF,
                                PUBLISH2PORTAL = model.PUBLISH2PORTAL,
                                CAPTION = caption
                            };
                            _dbcontext.TB_ATTRIBUTE.Add(objattribute);
                            _dbcontext.SaveChanges();
                            var firstOrDefault = _dbcontext.TB_ATTRIBUTE.Max(x => x.ATTRIBUTE_ID);
                            var lastcreated = _dbcontext.TB_ATTRIBUTE.FirstOrDefault(x => x.ATTRIBUTE_ID == firstOrDefault);
                            if (lastcreated != null)
                            {
                                var attributeId = lastcreated.ATTRIBUTE_ID;
                                if (catalogId != 1)
                                {


                                    var catalogattributecount =
                                        _dbcontext.TB_CATALOG_ATTRIBUTES.Count(
                                            x => x.ATTRIBUTE_ID == attributeId && x.CATALOG_ID == catalogId);
                                    if (catalogattributecount == 0)
                                    {
                                        var objcatalogattribute = new TB_CATALOG_ATTRIBUTES
                                        {
                                            CATALOG_ID = catalogId,
                                            ATTRIBUTE_ID = attributeId,
                                            MODIFIED_USER = User.Identity.Name,
                                            CREATED_USER = User.Identity.Name,
                                            MODIFIED_DATE = DateTime.Now,
                                            CREATED_DATE = DateTime.Now
                                        };
                                        _dbcontext.TB_CATALOG_ATTRIBUTES.Add(objcatalogattribute);
                                        _dbcontext.SaveChanges();
                                    }




                                }
                                var customAttr =
                                    _dbcontext.CUSTOMERATTRIBUTE.Where(
                                        x => x.ATTRIBUTE_ID == attributeId && x.CUSTOMER_ID == customerIds.CustomerId).Select(z => z.ATTRIBUTE_ID)
                                        .ToList();

                                if (customAttr.Count == 0)
                                {
                                    var objcustomerattribute = new CUSTOMERATTRIBUTE
                                    {
                                        CUSTOMER_ID = customerIds.CustomerId,
                                        ATTRIBUTE_ID = attributeId,
                                        MODIFIED_USER = User.Identity.Name,
                                        CREATED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now,
                                        CREATED_DATE = DateTime.Now
                                    };
                                    _dbcontext.CUSTOMERATTRIBUTE.Add(objcustomerattribute);
                                    _dbcontext.SaveChanges();
                                }
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Inserted);
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, "Attribute Name already exists, please enter a different Name and create the Attribute");

                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, "Attribute Name already exists, please enter a different Name and create the Attribute");
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : SaveNewAttribute", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        private string AttrDataRuleDS(NewAttribute model)
        {
            string dataString = string.Empty;
            if (model.Prefix != "" || model.Suffix != "" || model.Condition != "" || model.CustomValue != "")
            {
                var dataRuleDS = new DataSet();
                var dTable = new DataTable("DataRule");
                var dCol = new DataColumn("Prefix", typeof(string));
                dTable.Columns.Add(dCol);
                dCol = new DataColumn("Suffix", typeof(string));
                dTable.Columns.Add(dCol);
                dCol = new DataColumn("Condition", typeof(string));
                dTable.Columns.Add(dCol);
                dCol = new DataColumn("CustomValue", typeof(string));
                dTable.Columns.Add(dCol);
                dCol = new DataColumn("ApplyTo", typeof(string));
                dTable.Columns.Add(dCol);
                dCol = new DataColumn("ApplyForNumericOnly", typeof(int));
                dTable.Columns.Add(dCol);
                DataRow dRow = dTable.NewRow();
                dRow[0] = model.Prefix;
                dRow[1] = model.Suffix;
                //if (comboDatatype.Text.ToString() != "")
                //{
                //    DRow[2] = DecPrecisionFill();
                //}
                //else
                dRow[2] = model.Condition;
                dRow[3] = model.CustomValue;
                dRow[4] = model.ApplyTo;
                if (model.ApplyForNumericOnly == "True")
                {
                    dRow[5] = 1;
                }
                else
                {
                    dRow[5] = 0;
                }
                // dRow[5] = Convert.ToInt32(model.ApplyForNumericOnly);
                dTable.Rows.Add(dRow);
                dataRuleDS.Tables.Add(dTable);
                string path = HttpContext.Current.Server.MapPath("~/Content/XML/");
                dataRuleDS.WriteXml(path + "\\DataRulexml.xml");
                var fileStream = new FileStream(path + "\\DataRulexml.xml", FileMode.Open);
                var streamWriter = new StreamReader(fileStream);
                dataString = streamWriter.ReadToEnd();
                streamWriter.Close();
                fileStream.Close();
            }
            return dataString;
        }
        [System.Web.Http.HttpGet]
        public IList GetAllCatalogCategoryAttributes(int catalogId, string categoryId)
        {
            try
            {

                int[] attributeType = { 21, 23, 24, 25 };
                var customAttr =
                      _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(z => z.CustomerId).ToList();
                int customerID = 0;
                if (customAttr.Any())
                {
                    int.TryParse(customAttr[0].ToString(), out customerID);
                    HttpContext.Current.Session["customerId"] = customerID;
                }
                var attributesCategorySpecs = _dbcontext.TB_CATEGORY_SPECS.Join(_dbcontext.TB_CATALOG_ATTRIBUTES, a => a.ATTRIBUTE_ID, b => b.ATTRIBUTE_ID, (a, b) => new { a, b })
                        .Join(_dbcontext.TB_ATTRIBUTE, x => x.a.ATTRIBUTE_ID, y => y.ATTRIBUTE_ID, (x, y) => new { x, y })
                         .Join(_dbcontext.CUSTOMERATTRIBUTE, za => za.y.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (za, ca) => new { za, ca })
                    //  .Join(_dbcontext.TB_CATEGORY_FAMILY_ATTR_LIST, t => t.y.ATTRIBUTE_ID, z => z.ATTRIBUTE_ID, (t, z) => new { t, z })
                        .Where(f => f.za.x.a.CATEGORY_ID == categoryId && f.za.x.b.CATALOG_ID == catalogId && f.ca.CUSTOMER_ID == customerID)
                        .Select(z => new
                        {
                            z.za.x.b.CATALOG_ID,
                            z.za.x.a.CATEGORY_ID,
                            z.za.x.a.ATTRIBUTE_ID,
                            z.za.y.ATTRIBUTE_NAME,
                            z.za.y.ATTRIBUTE_TYPE,
                            ISAvailable = false
                        }).Distinct();
                var catalogs =
                    _dbcontext.TB_CATALOG_ATTRIBUTES.Join(_dbcontext.CUSTOMERATTRIBUTE, z => z.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (z, ca) => new { z, ca }).Where(
                        x => x.z.CATALOG_ID == catalogId && attributeType.Contains(x.z.TB_ATTRIBUTE.ATTRIBUTE_TYPE) && x.ca.CUSTOMER_ID == customerID)
                        .Select(x => new
                        {
                            x.z.CATALOG_ID,
                            CATEGORY_ID = categoryId,
                            x.z.TB_ATTRIBUTE.ATTRIBUTE_ID,
                            x.z.TB_ATTRIBUTE.ATTRIBUTE_NAME,
                            x.z.TB_ATTRIBUTE.ATTRIBUTE_TYPE,
                            ISAvailable = false
                        }).Distinct().ToList();
                var catalogallAttribtues = catalogs.Except(attributesCategorySpecs);
                return catalogallAttribtues.OrderBy(y => y.ATTRIBUTE_NAME).ToList();
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController : GetAllCatalogattributes", objException);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveCategoryPublishAttributes(string categoryId, int catalogId, JArray model)
        {
            try
            {
                // var arr = (JArray)((JObject.Parse(model.ToString())).SelectToken("data")).SelectToken("models");
                if (!string.IsNullOrEmpty(categoryId))
                {
                    var id = categoryId.Split('~');
                    categoryId = Convert.ToString(id[0]);

                }
                var functionAlloweditems = ((JArray)model).Select(x => new CategoryAttributePublish()
                {
                    CATALOG_ID = (int)x["CATALOG_ID"],
                    CATEGORY_ID = (string)x["CATEGORY_ID"],
                    ATTRIBUTE_ID = (int)x["ATTRIBUTE_ID"],
                    ATTRIBUTE_TYPE = (int)x["ATTRIBUTE_TYPE"],
                    // ATTRIBUTE_NAME = (string)x["ATTRIBUTE_NAME"],
                    ISAvailable = (bool)x["ISAvailable"]
                }).ToList();
                int selectedcategorycount = functionAlloweditems.Count(name => name.ISAvailable == true);
                int sortOrder = 0;
                var countCategory =
                           _dbcontext.TB_CATEGORY_SPECS.Count(
                               x =>
                                   x.CATEGORY_ID == categoryId && x.CATALOG_ID == catalogId);
                if (countCategory <= 12 && selectedcategorycount + countCategory <= 12)
                {
                    foreach (var item in functionAlloweditems)
                    {
                        if (item.ISAvailable)
                        {
                            var sortOrders =
                                _dbcontext.TB_CATEGORY_SPECS.Where(
                                    y =>
                                        y.CATEGORY_ID == item.CATEGORY_ID && y.CATALOG_ID == item.CATALOG_ID &&
                                        y.CATEGORY_ID == categoryId);
                            if (sortOrders.Any())
                            {
                                sortOrder = sortOrders.Max(x => x.SORT_ORDER);
                            }
                            // var tbProdFamily = _dbcontext.TB_PROD_FAMILY.Where(x => x.FAMILY_ID == item.FAMILY_ID).Select(x => x).ToList();
                            sortOrder++;
                            //  foreach (var proditems in tbProdFamily)
                            //  {
                            var count =
                                _dbcontext.TB_CATEGORY_SPECS.Count(
                                    x =>
                                        x.CATEGORY_ID == item.CATEGORY_ID && x.CATALOG_ID == item.CATALOG_ID);
                            var categorySpecCount =
                                _dbcontext.TB_CATEGORY_SPECS.Count(
                                    x => x.CATEGORY_ID == item.CATEGORY_ID && x.ATTRIBUTE_ID == item.ATTRIBUTE_ID);

                            var defaultValue = _dbcontext.TB_ATTRIBUTE.Find(item.ATTRIBUTE_ID);
                            if (categorySpecCount != 0) continue;
                            if (item.ATTRIBUTE_TYPE != 24)
                            {
                                switch (item.ATTRIBUTE_TYPE)
                                {
                                    case 9:
                                        {
                                            var objTbProdspecs = new TB_CATEGORY_SPECS
                                            {
                                                CATALOG_ID = item.CATALOG_ID,
                                                CATEGORY_ID = item.CATEGORY_ID,
                                                ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                                                STRING_VALUE = defaultValue.DEFAULT_VALUE,
                                                SORT_ORDER = sortOrder,
                                                CREATED_USER = User.Identity.Name,
                                                MODIFIED_USER = User.Identity.Name,
                                                MODIFIED_DATE = DateTime.Now,
                                                CREATED_DATE = DateTime.Now,
                                                FLAG_RECYCLE = "A",
                                                PUBLICHCATEGORY_FLAG = true
                                            };
                                            _dbcontext.TB_CATEGORY_SPECS.Add(objTbProdspecs);
                                            _dbcontext.SaveChanges();
                                        }
                                        break;
                                    default:
                                        if (defaultValue.ATTRIBUTE_DATATYPE.Contains("Num"))
                                        {
                                            if (string.IsNullOrEmpty(defaultValue.DEFAULT_VALUE))
                                            {
                                                var objTbProdspecs = new TB_CATEGORY_SPECS
                                                {
                                                    CATALOG_ID = item.CATALOG_ID,
                                                    CATEGORY_ID = item.CATEGORY_ID,
                                                    ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                                                    NUMERIC_VALUE = null,
                                                    SORT_ORDER = sortOrder,
                                                    CREATED_USER = User.Identity.Name,
                                                    MODIFIED_USER = User.Identity.Name,
                                                    MODIFIED_DATE = DateTime.Now,
                                                    CREATED_DATE = DateTime.Now,
                                                    FLAG_RECYCLE = "A",
                                                    PUBLICHCATEGORY_FLAG = true
                                                };
                                                _dbcontext.TB_CATEGORY_SPECS.Add(objTbProdspecs);
                                                _dbcontext.SaveChanges();
                                            }
                                            else
                                            {
                                                var objTbProdspecs = new TB_CATEGORY_SPECS
                                                {

                                                    CATALOG_ID = item.CATALOG_ID,
                                                    CATEGORY_ID = item.CATEGORY_ID,
                                                    ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                                                    NUMERIC_VALUE = Convert.ToDecimal(defaultValue.DEFAULT_VALUE),
                                                    SORT_ORDER = sortOrder,
                                                    CREATED_USER = User.Identity.Name,
                                                    MODIFIED_USER = User.Identity.Name,
                                                    MODIFIED_DATE = DateTime.Now,
                                                    CREATED_DATE = DateTime.Now,
                                                    FLAG_RECYCLE = "A",
                                                    PUBLICHCATEGORY_FLAG = true
                                                };
                                                _dbcontext.TB_CATEGORY_SPECS.Add(objTbProdspecs);
                                                _dbcontext.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            var objTbProdspecs = new TB_CATEGORY_SPECS
                                            {
                                                CATALOG_ID = item.CATALOG_ID,
                                                CATEGORY_ID = item.CATEGORY_ID,
                                                ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                                                STRING_VALUE = defaultValue.DEFAULT_VALUE,
                                                SORT_ORDER = sortOrder,
                                                CREATED_USER = User.Identity.Name,
                                                MODIFIED_USER = User.Identity.Name,
                                                MODIFIED_DATE = DateTime.Now,
                                                CREATED_DATE = DateTime.Now,
                                                FLAG_RECYCLE = "A",
                                                PUBLICHCATEGORY_FLAG = true
                                            };
                                            _dbcontext.TB_CATEGORY_SPECS.Add(objTbProdspecs);
                                            _dbcontext.SaveChanges();
                                        }
                                        break;
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(defaultValue.DEFAULT_VALUE))
                                {
                                    var objTbProdspecs = new TB_CATEGORY_SPECS
                                    {
                                        CATALOG_ID = item.CATALOG_ID,
                                        CATEGORY_ID = item.CATEGORY_ID,
                                        ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                                        NUMERIC_VALUE = null,
                                        SORT_ORDER = sortOrder,
                                        CREATED_USER = User.Identity.Name,
                                        MODIFIED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now,
                                        CREATED_DATE = DateTime.Now,
                                        FLAG_RECYCLE = "A",
                                        PUBLICHCATEGORY_FLAG = true
                                    };
                                    _dbcontext.TB_CATEGORY_SPECS.Add(objTbProdspecs);
                                    _dbcontext.SaveChanges();
                                }
                                else
                                {
                                    var objTbProdspecs = new TB_CATEGORY_SPECS
                                    {

                                        CATALOG_ID = item.CATALOG_ID,
                                        CATEGORY_ID = item.CATEGORY_ID,
                                        ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                                        NUMERIC_VALUE = Convert.ToDecimal(defaultValue.DEFAULT_VALUE),
                                        SORT_ORDER = sortOrder,
                                        CREATED_USER = User.Identity.Name,
                                        MODIFIED_USER = User.Identity.Name,
                                        MODIFIED_DATE = DateTime.Now,
                                        CREATED_DATE = DateTime.Now,
                                        FLAG_RECYCLE = "A",
                                        PUBLICHCATEGORY_FLAG = true
                                    };
                                    _dbcontext.TB_CATEGORY_SPECS.Add(objTbProdspecs);
                                    _dbcontext.SaveChanges();
                                }
                            }
                        }

                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "NotAvailable");
                }
                // }

                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : SaveCategoryPublishAttributes", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }



        [System.Web.Http.HttpGet]
        public IList GetPublishedCategoryAttributes(int catalogId, string categoryId)
        {
            try
            {
                if (!string.IsNullOrEmpty(categoryId))
                {
                    var id = categoryId.Split('~');
                    categoryId = Convert.ToString(id[0]);
                }
                var tblprod = new DataSet();

                string strZero = "0";
                string strAll = "ALL";
                string catfamquery = "STP_CATALOGSTUDIO5_categoryAttributes " + catalogId + ",'" + categoryId + "','" + strZero + "','" + strZero + "','" + strAll + "'";
                var conn =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                var comm = new SqlCommand(catfamquery, conn) { CommandTimeout = 0 };
                var dbAdapter = new SqlDataAdapter(comm);
                dbAdapter.Fill(tblprod);
                var prodspecAttributesPublished = tblprod.Tables[0].AsEnumerable().Select(dr => new CategoryAttributePublish
                {
                    CATALOG_ID = catalogId,
                    CATEGORY_ID = categoryId,
                    ATTRIBUTE_ID = Convert.ToInt32(dr.Field<int>("ATTRIBUTE_ID")),
                    ATTRIBUTE_NAME = dr.Field<string>("ATTRIBUTE_NAME"),
                    SORT_ORDER = Convert.ToInt32(dr.Field<int>("SORT_ORDER")),
                    ATTRIBUTE_TYPE = Convert.ToInt32(dr["ATTRIBUTE_TYPE"]),
                    ISAvailable = true
                }).Distinct().ToList();
                if (tblprod.Tables.Count > 1)
                {
                    var prodspecAttributesUnPublished = tblprod.Tables[2].AsEnumerable().Select(dr => new CategoryAttributePublish
                    {
                        CATALOG_ID = catalogId,
                        CATEGORY_ID = categoryId,
                        ATTRIBUTE_ID = Convert.ToInt32(dr.Field<int>("ATTRIBUTE_ID")),
                        ATTRIBUTE_NAME = dr.Field<string>("ATTRIBUTE_NAME"),
                        SORT_ORDER = 1000,
                        ATTRIBUTE_TYPE = Convert.ToInt32(dr["ATTRIBUTE_TYPE"]),
                        ISAvailable = false
                    }).Distinct().ToList();
                    return prodspecAttributesPublished.Union(prodspecAttributesUnPublished).OrderBy(y => y.SORT_ORDER).Distinct().ToList();
                }
                else
                {
                    return prodspecAttributesPublished.OrderBy(y => y.SORT_ORDER).Distinct().ToList();

                }
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController : GetprodfamilyAttributes", objException);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage DeleteCategoryPublishAttributes(object model, string categoryId)
        {
            try
            {
                var functionAlloweditems = ((JArray)model).Select(x => new CategoryAttributePublish()
                {
                    CATALOG_ID = (int)x["CATALOG_ID"],
                    CATEGORY_ID = (string)x["CATEGORY_ID"],
                    ATTRIBUTE_ID = (int)x["ATTRIBUTE_ID"],
                    ATTRIBUTE_TYPE = (int)x["ATTRIBUTE_TYPE"],
                    // ATTRIBUTE_NAME = (string)x["ATTRIBUTE_NAME"],
                    //  ISAvailable = (bool)x["ISAvailable"]
                }).ToList();
                foreach (var item in functionAlloweditems)
                {
                    if (item != null)
                    {

                        if (!string.IsNullOrEmpty(categoryId))
                        {
                            var id = categoryId.Split('~');
                            categoryId = Convert.ToString(id[0]);
                        }
                        var prodspeccount =
                            _dbcontext.TB_CATEGORY_SPECS.Count(
                                x => x.CATEGORY_ID == item.CATEGORY_ID && x.ATTRIBUTE_ID == item.ATTRIBUTE_ID);

                        if (prodspeccount > 0)
                        {
                            var prodpspecsattrList =
                                _dbcontext.TB_CATEGORY_SPECS.FirstOrDefault(
                                    x => x.CATEGORY_ID == item.CATEGORY_ID && x.ATTRIBUTE_ID == item.ATTRIBUTE_ID);
                            _dbcontext.TB_CATEGORY_SPECS.Remove(prodpspecsattrList);
                            _dbcontext.SaveChanges();
                        }

                    }
                    // }
                    // _dbcontext.STP_CATALOGSTUDIO5_ProductAttributeSortOrder(item.FAMILY_ID);
                }
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : DeleteFamilyPublishAttributes", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage BtnCategoryMoveUpClick(int sortOrder, CategoryAttributePublish item, string categoryId)
        {
            try
            {
                var diff = item.SORT_ORDER - sortOrder;

                var tbProdFamilysortorder =
                    _dbcontext.TB_CATEGORY_SPECS.Count(
                        x =>
                            x.SORT_ORDER == item.SORT_ORDER &&
                            x.CATEGORY_ID == categoryId && x.CATALOG_ID == item.CATALOG_ID);
                if (tbProdFamilysortorder > 0)
                {
                    var prodfamilyattrList =
                        _dbcontext.TB_CATEGORY_SPECS.FirstOrDefault(
                            x =>
                                x.SORT_ORDER == item.SORT_ORDER &&
                                x.CATEGORY_ID == categoryId && x.CATALOG_ID == item.CATALOG_ID);
                    if (prodfamilyattrList != null) prodfamilyattrList.SORT_ORDER = item.SORT_ORDER - diff;
                    _dbcontext.SaveChanges();
                }
                var prodfamilyattrListcount =
                    _dbcontext.TB_CATEGORY_SPECS.Count(
                        x =>
                            x.SORT_ORDER == item.SORT_ORDER - diff &&
                            x.CATEGORY_ID == categoryId && x.CATALOG_ID == item.CATALOG_ID);
                if (prodfamilyattrListcount > 0)
                {
                    var prodfamilyattrListupdate =
                        _dbcontext.TB_CATEGORY_SPECS.FirstOrDefault(
                            x =>
                                x.SORT_ORDER == item.SORT_ORDER - diff &&
                                x.CATEGORY_ID == categoryId && x.CATALOG_ID == item.CATALOG_ID &&
                                x.ATTRIBUTE_ID != item.ATTRIBUTE_ID);
                    if (prodfamilyattrListupdate != null) prodfamilyattrListupdate.SORT_ORDER = item.SORT_ORDER;
                    _dbcontext.SaveChanges();
                }

                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : BtnMoveUpClick", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }


        [System.Web.Http.HttpPost]
        public HttpResponseMessage BtnCategoryMoveDownClick(int sortOrder, CategoryAttributePublish item, string categoryId)
        {
            try
            {
                var diff = sortOrder - item.SORT_ORDER;

                var tbProdFamilysortorder =
                    _dbcontext.TB_CATEGORY_SPECS.Count(
                        x =>
                            x.SORT_ORDER == item.SORT_ORDER &&
                            x.CATEGORY_ID == categoryId && x.CATALOG_ID == item.CATALOG_ID);
                if (tbProdFamilysortorder > 0)
                {
                    // _dbcontext.TB_PROD_FAMILY_ATTR_LIST.Where(y => y.FAMILY_ID == item.FAMILY_ID)
                    int maxsortorder = 0;
                    var maxsortorders =
                        _dbcontext.TB_CATEGORY_SPECS.Where(
                            x =>
                                x.CATEGORY_ID == categoryId &&
                                x.CATALOG_ID == item.CATALOG_ID);
                    if (maxsortorders.Any())
                    {
                        maxsortorder = maxsortorders.Max(x => x.SORT_ORDER);
                    }
                    if (maxsortorder != item.SORT_ORDER)
                    {
                        var prodfamilyattrList =
                            _dbcontext.TB_CATEGORY_SPECS.FirstOrDefault(
                                x =>
                                   x.SORT_ORDER == item.SORT_ORDER &&
                                    x.CATEGORY_ID == categoryId && x.CATALOG_ID == item.CATALOG_ID);
                        if (prodfamilyattrList != null) prodfamilyattrList.SORT_ORDER = item.SORT_ORDER + diff;
                        _dbcontext.SaveChanges();
                        var prodfamilyattrListcount =
                            _dbcontext.TB_CATEGORY_SPECS.Count(
                                x =>
                                    x.SORT_ORDER == item.SORT_ORDER + diff &&
                                    x.CATEGORY_ID == categoryId && x.CATALOG_ID == item.CATALOG_ID);
                        if (prodfamilyattrListcount > 0)
                        {
                            var prodfamilyattrListupdate =
                                _dbcontext.TB_CATEGORY_SPECS.FirstOrDefault(
                                    x =>
                                        x.SORT_ORDER == item.SORT_ORDER + diff
                                        && x.CATEGORY_ID == categoryId && x.CATALOG_ID == item.CATALOG_ID && x.ATTRIBUTE_ID != item.ATTRIBUTE_ID);
                            if (prodfamilyattrListupdate != null)
                                prodfamilyattrListupdate.SORT_ORDER = item.SORT_ORDER;
                            _dbcontext.SaveChanges();
                        }
                    }

                }
                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : BtnMoveDownClick", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        //[System.Web.Http.HttpPost]
        //public HttpResponseMessage UnPublishCategoryAttributes(JArray model, string categoryId)
        //{
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(categoryId))
        //        {
        //            var id = categoryId.Split('~');
        //            categoryId = Convert.ToString(id[0]);
        //        }
        //        var functionAlloweditems = ((JArray)model).Select(x => new CategoryAttributePublish()
        //        {
        //            CATALOG_ID = (int)x["CATALOG_ID"],
        //            CATEGORY_ID = (string)x["CATEGORY_ID"],
        //            ATTRIBUTE_ID = (int)x["ATTRIBUTE_ID"],
        //            ISAvailable = (bool)x["ISAvailable"]
        //        }).ToList();

        //        int sortOrder = 0;
        //        foreach (var item in functionAlloweditems)
        //        {
        //            var count =
        //                _dbcontext.TB_CATEGORY_SPECS.Count(
        //                    x =>
        //                         x.ATTRIBUTE_ID == item.ATTRIBUTE_ID &&
        //                        x.CATEGORY_ID == categoryId && x.CATALOG_ID == item.CATALOG_ID);
        //            if (!item.ISAvailable)
        //            {
        //                if (count <= 0) continue;

        //                _dbcontext.TB_CATEGORY_SPECS.Where(
        //                  x => x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.CATEGORY_ID == categoryId && x.CATALOG_ID == item.CATALOG_ID).ToList().ForEach(x => x.PUBLICHCATEGORY_FLAG = false);
        //                _dbcontext.SaveChanges();
        //            }
        //            else
        //            {
        //                if (count != 0) continue;

        //                var maxsortorders =
        //                    _dbcontext.TB_CATEGORY_SPECS.Where(
        //                        x =>
        //                             x.CATEGORY_ID == categoryId &&
        //                            x.CATALOG_ID == item.CATALOG_ID);
        //                if (maxsortorders.Any())
        //                {
        //                    sortOrder = maxsortorders.Max(x => x.SORT_ORDER);
        //                }
        //                if (!string.IsNullOrEmpty(categoryId))
        //                {
        //                    var objTbProdFamilyAttrList = new TB_CATEGORY_SPECS
        //                    {
        //                        CATALOG_ID = item.CATALOG_ID,
        //                        CATEGORY_ID = item.CATEGORY_ID,
        //                        ATTRIBUTE_ID = item.ATTRIBUTE_ID,
        //                        SORT_ORDER = sortOrder,
        //                        CREATED_USER = User.Identity.Name,
        //                        MODIFIED_USER = User.Identity.Name,
        //                        MODIFIED_DATE = DateTime.Now,
        //                        CREATED_DATE = DateTime.Now,
        //                        FLAG_RECYCLE = "A",
        //                        PUBLICHCATEGORY_FLAG = true

        //                    };
        //                    _dbcontext.TB_CATEGORY_SPECS.Add(objTbProdFamilyAttrList);
        //                }
        //                _dbcontext.SaveChanges();
        //            }
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
        //    }
        //    catch (Exception objexception)
        //    {
        //        Logger.Error("Error at HomeApiController : UnPublishAttributes", objexception);
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError);
        //    }
        //}
        [System.Web.Http.HttpPost]
        public HttpResponseMessage UnPublishCategoryAttributes(string categoryId, string selecetedCatalogId, JArray model)
        {
            try
            {

                int catalogId = Convert.ToInt32(selecetedCatalogId);


                if (!string.IsNullOrEmpty(categoryId))
                {
                    var id = categoryId.Split('~');
                    categoryId = Convert.ToString(id[0]);
                }


                var treenode = ((JArray)model).Select(x => x).ToList();
                var lt = treenode[0].ToList();
                var rt = treenode[1].ToList();




                var functionAlloweditems = ((JArray)model[0]).Select(x => new CategoryAttributePublish()
                {
                    CATALOG_ID = (int)x["CATALOG_ID"],
                    CATEGORY_ID = (string)x["CATEGORY_ID"],
                    ATTRIBUTE_ID = (int)x["ATTRIBUTE_ID"],
                    ISAvailable = (bool)x["ISAvailable"],
                    FLAG = (string)x["FLAG"]
                }).ToList();

                int sortOrder = 0;
                foreach (var item in functionAlloweditems)
                {



                    //----------------------------------------------------------------------------------------------------------------------------------


                    if (item.FLAG == "publish")
                    {

                        var checkCount =
_dbcontext.TB_CATEGORY_SPECS.Count(x => x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.CATALOG_ID == item.CATALOG_ID && x.CATEGORY_ID == item.CATEGORY_ID && x.FLAG_RECYCLE == "A");




                        if (checkCount > 0)
                        {

                            var unpublish = _dbcontext.TB_CATEGORY_SPECS.Where(x => x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.CATALOG_ID == item.CATALOG_ID && x.CATEGORY_ID == item.CATEGORY_ID).Single(x => x.PUBLICHCATEGORY_FLAG == false);
                            unpublish.PUBLICHCATEGORY_FLAG = true;
                            _dbcontext.SaveChanges();
                        }

                    }
                    else if (item.FLAG == "unpublish")
                    {
                        var checkCount =
                      _dbcontext.TB_CATEGORY_SPECS.Count(x => x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.CATALOG_ID == item.CATALOG_ID && x.CATEGORY_ID == item.CATEGORY_ID && x.FLAG_RECYCLE == "A" && x.PUBLICHCATEGORY_FLAG == true);


                        if (checkCount > 0)
                        {

                            var unpublish = _dbcontext.TB_CATEGORY_SPECS.Where(x => x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.CATALOG_ID == item.CATALOG_ID && x.CATEGORY_ID == item.CATEGORY_ID).Single(x => x.PUBLICHCATEGORY_FLAG == true);
                            unpublish.PUBLICHCATEGORY_FLAG = false;
                            _dbcontext.SaveChanges();
                        }



                    }
                    else if (item.FLAG == "Available")
                    {

                        var maxsortorders =
                                            _dbcontext.TB_CATEGORY_SPECS.Where(
                                            x =>
                                            x.CATEGORY_ID == categoryId &&
                                            x.CATALOG_ID == item.CATALOG_ID);
                        if (maxsortorders.Any())
                        {
                            sortOrder = maxsortorders.Max(x => x.SORT_ORDER);
                        }
                        if (!string.IsNullOrEmpty(categoryId))
                        {
                            var objTbProdFamilyAttrList = new TB_CATEGORY_SPECS
                            {

                                CATALOG_ID = item.CATALOG_ID,
                                CATEGORY_ID = item.CATEGORY_ID,
                                ATTRIBUTE_ID = item.ATTRIBUTE_ID,
                                SORT_ORDER = sortOrder + 1,
                                CREATED_USER = User.Identity.Name,
                                MODIFIED_USER = User.Identity.Name,
                                MODIFIED_DATE = DateTime.Now,
                                CREATED_DATE = DateTime.Now,
                                FLAG_RECYCLE = "A",
                                PUBLICHCATEGORY_FLAG = false

                            };
                            _dbcontext.TB_CATEGORY_SPECS.Add(objTbProdFamilyAttrList);
                        }
                        _dbcontext.SaveChanges();


                    }
                    else if (item.FLAG == "UnAvailable")
                    {

                        var UnpublishDatass =
                    _dbcontext.TB_CATEGORY_SPECS.FirstOrDefault(x => x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.CATALOG_ID == item.CATALOG_ID && x.CATEGORY_ID == item.CATEGORY_ID && x.FLAG_RECYCLE == "A");

                        _dbcontext.TB_CATEGORY_SPECS.Remove(UnpublishDatass);
                        _dbcontext.SaveChanges();

                    }



                    //=======================================================================================================================================


                }


                // Sort Functions _ Starts

                var functionAlloweditemsSort = ((JArray)model[1]).Select(x => new CategoryAttributePublish()
                {
                    CATALOG_ID = (int)x["CATALOG_ID"],
                    CATEGORY_ID = (string)x["CATEGORY_ID"],
                    ATTRIBUTE_ID = (int)x["ATTRIBUTE_ID"],
                    ATTRIBUTE_TYPE = (int)x["ATTRIBUTE_TYPE"],
                    // ATTRIBUTE_NAME = (string)x["ATTRIBUTE_NAME"],
                    ISAvailable = (bool)x["PUBLISHED"]
                }).ToList();
                int selectedcategorycount = functionAlloweditemsSort.Count(name => name.ISAvailable == true);
                // int sortOrder = 0;


                int i = 1;
                foreach (var item in functionAlloweditemsSort)
                {

                    // if (item.ISAvailable)
                    // {
                    var objTbCategoryAttrList = _dbcontext.TB_CATEGORY_SPECS.FirstOrDefault(x =>
                    x.CATEGORY_ID == categoryId && x.CATALOG_ID == catalogId && x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.CATEGORY_ID == item.CATEGORY_ID && x.PUBLICHCATEGORY_FLAG == true);
                    if (objTbCategoryAttrList != null)
                    {
                        // var objAttrList = new TB_PROD_FAMILY_ATTR_LIST();
                        objTbCategoryAttrList.SORT_ORDER = i;
                        //  _dbcontext.TB_PROD_FAMILY_ATTR_LIST.Add(objAttrList);
                        _dbcontext.SaveChanges();
                    }
                    // }
                    i++;
                }

                // Sort Functions _ End



                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);

            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : UnPublishAttributes", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        [System.Web.Http.HttpGet]
        public System.Web.Mvc.JsonResult getCategoryspecification(int catalogId, string CategoryId)
        {
            try
            {

                string categoryId = string.Empty;
                var dynamicColumns = new Dictionary<string, string>();
                var sb = new StringBuilder();
                var sw = new StringWriter(sb);
                JsonWriter jsonWriter = new JsonTextWriter(sw);
                if (CategoryId.Trim() != "")
                {
                    var id = CategoryId.Split('~');
                    categoryId = id[0];
                }
                var objDataTable1 = new DataTable();
                var model = new DashBoardModel();
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_CategoryPivotTable_For_Publish";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                    objSqlCommand.Parameters.Add("@USER_ID", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                    objSqlConnection.Open();
                    var objDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objDataAdapter.Fill(objDataTable1);
                    if (objDataTable1 != null & objDataTable1.Columns.Count - 2 <= 12)
                        
                    {
                        foreach (DataColumn columns in objDataTable1.Columns)
                        {
                            PTColumns objPTColumns = new PTColumns();
                            objPTColumns.Caption = columns.Caption;
                            objPTColumns.ColumnName = columns.ColumnName;
                            model.Columns.Add(objPTColumns);
                        }
                    }
                    string JSONString = string.Empty;
                    JSONString = JsonConvert.SerializeObject(objDataTable1);
                    model.Data = JSONString;
                    return new System.Web.Mvc.JsonResult() { Data = model };
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : GetProdSpecs", objexception);
                return null;
            }
        }


        [System.Web.Http.HttpGet]
        public Tuple<IList, string> GetCategoryAttributeDetails(int catalogId, string categoryid)
        {
            try
            {
                string ExternalAssetDriveURL = string.Empty;
                var UseExternalAssetDrive = ConfigurationManager.AppSettings["UseExternalAssetDrive"];
               

                if (UseExternalAssetDrive=="true")
                {
                    ExternalAssetDriveURL = ConfigurationManager.AppSettings["ExternalAssetDriveURL"];
                }
                else
                {
                    ExternalAssetDriveURL = "";
                }

               

                var objcountries = _dbcontext.STP_LS_CategoryPivotTableAttributes(catalogId, categoryid).Select(x => x).ToList();


                Tuple<IList, string> CategoryAttributeList =
                        new Tuple<IList, string>(objcountries, ExternalAssetDriveURL);

                return CategoryAttributeList;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController : GetFamilyAttributeDetails", objException);
                return null;
            }
        }


        [System.Web.Http.HttpPost]
        public void SaveCategorySpecs(int catalogid, string Category_id, object model)
        {
            try
            {
                int textLength = 0;
                if (!string.IsNullOrEmpty(Category_id))
                {
                    var id = Category_id.Split('~');
                    Category_id = Convert.ToString(id[0]);
                }
                else
                {
                    Category_id = "0";
                }
                var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(model.ToString());
                int catalogId;
                catalogId = catalogid;
                foreach (var value in values)
                {
                    if (value.Key.Contains("__"))
                    {
                        var attributeNames = value.Key.Split(new[] { "__" }, StringSplitOptions.None);
                        // var attributeNames = value.Key.Split('_');
                        // string attributeName = attributeNames[0];
                        var attributeType = attributeNames[3];
                        var attributeId = Convert.ToInt32(attributeNames[2]);
                        string imageCaption = Convert.ToString(attributeNames[1]);
                        var attributenamedetails = _dbcontext.TB_ATTRIBUTE.Find(attributeId);

                        //attributeId = attributenames.ATTRIBUTE_ID;


                        var attributeDatatype = attributenamedetails.ATTRIBUTE_DATATYPE;
                        var stringvalue = HttpUtility.HtmlDecode(value.Value);

                        if (!string.IsNullOrEmpty(stringvalue))
                        {
                            if (stringvalue.ToLower().EndsWith("\n"))
                            {
                                int sd = stringvalue.LastIndexOf("\n", StringComparison.Ordinal);
                                stringvalue = stringvalue.Remove(sd);
                            }
                            if (stringvalue.ToLower().EndsWith("\r"))
                            {
                                int sd = stringvalue.LastIndexOf("\r", StringComparison.Ordinal);
                                stringvalue = stringvalue.Remove(sd);
                            }
                            if (stringvalue.ToLower().StartsWith("<p>") && stringvalue.ToLower().EndsWith("</p>"))
                            {
                                int count = 0;
                                int i = 0;
                                while ((i = stringvalue.IndexOf("<p>", i, System.StringComparison.Ordinal)) != -1)
                                {
                                    i += "<p>".Length;
                                    count++;
                                }
                                if (count == 1)
                                {
                                    int endp = stringvalue.LastIndexOf("</p>", StringComparison.Ordinal);
                                    stringvalue = stringvalue.Remove(endp);
                                    int p = stringvalue.IndexOf("<p>", StringComparison.Ordinal);
                                    stringvalue = stringvalue.Remove(p, 3);
                                }
                            }

                            stringvalue = stringvalue.Replace("\r", "");
                            stringvalue = stringvalue.Replace("\n\n", "\n");
                            stringvalue = stringvalue.Replace("\n", "\r\n");
                            //stringvalue = stringvalue.Replace("<br />", "\r\n");

                        }
                        if (!string.IsNullOrEmpty(stringvalue))
                        {
                            textLength = stringvalue.Length;
                        }
                        var objcategorySpecs = _dbcontext.TB_CATEGORY_SPECS.FirstOrDefault(s => s.CATEGORY_ID == Category_id && s.ATTRIBUTE_ID == attributeId);
                        if (objcategorySpecs != null)
                        {
                            if (attributeDatatype.ToLower().Contains("number"))
                            {
                                if (string.IsNullOrEmpty(stringvalue))
                                {
                                    objcategorySpecs.NUMERIC_VALUE = null;
                                    _dbcontext.SaveChanges();
                                }
                                else
                                {
                                    decimal numericvalue;
                                    Decimal.TryParse(stringvalue, out numericvalue);
                                    objcategorySpecs.NUMERIC_VALUE = numericvalue;
                                    _dbcontext.SaveChanges();
                                }
                            }
                            else
                            {
                                if (attributeDatatype.Contains('('))
                                {
                                    string[] sizearray = attributeDatatype.Split('(');
                                    string[] size = sizearray[1].Split(')');
                                    if (textLength <= Convert.ToInt32(size[0]))
                                    {
                                        objcategorySpecs.STRING_VALUE = stringvalue;
                                        _dbcontext.SaveChanges();
                                    }
                                }
                                else if (attributeType == "23" && imageCaption == "OBJ")
                                {

                                    objcategorySpecs.STRING_VALUE = stringvalue;
                                    _dbcontext.SaveChanges();

                                }
                                else if (attributeType == "23" && imageCaption != "OBJ")
                                {
                                    objcategorySpecs.OBJECT_NAME = stringvalue;
                                    _dbcontext.SaveChanges();
                                }
                                else
                                {
                                    objcategorySpecs.STRING_VALUE = stringvalue;
                                    _dbcontext.SaveChanges();
                                }
                            }


                        }


                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at FamilyApiController: SaveFamilySpecs", ex);
            }
        }
        [System.Web.Http.HttpGet]
        public string setPdfXpressType(string  Type , string Category_id , string Family_Name , string Product_id, string Catalog_ID)
        {

            // getValues
            Category_id = DecryptStringAES(Category_id);
            Family_Name = DecryptStringAES(Family_Name);
            HttpContext.Current.Session["PDFTYPE"] = Type;
            if(Type == "ALL")
            {
                HttpContext.Current.Session["PDFCATRGORYID"] = "EMPTY";
                HttpContext.Current.Session["PDFFAMILYID"] = "EMPTY";
                HttpContext.Current.Session["PDFPRODUCTID"] = "EMPTY";
            }
            else if(Type == "CATEGORY")
            {
                string categoryId = _dbcontext.TB_CATEGORY.Join(_dbcontext.TB_CATALOG_SECTIONS, tc => tc.CATEGORY_ID, tcs => tcs.CATEGORY_ID, (tc, tcs) => new { tc, tcs }).Where(a => a.tc.CATEGORY_SHORT == Category_id && a.tc.FLAG_RECYCLE == "A" && a.tcs.CATALOG_ID.ToString() == Catalog_ID).Select(x => x.tc.CATEGORY_ID).SingleOrDefault(); 
                //_dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_SHORT == Category_id && x.FLAG_RECYCLE=="A").Select(x => x.CATEGORY_ID).SingleOrDefault();
                if (Category_id == null)
                {
                    HttpContext.Current.Session["PDFCATRGORYID"] = "EMPTY";
                    HttpContext.Current.Session["PDFFAMILYID"] = "EMPTY";
                    HttpContext.Current.Session["PDFPRODUCTID"] = "EMPTY";
                }
                else
                {
                    HttpContext.Current.Session["PDFCATRGORYID"] = categoryId;
                    HttpContext.Current.Session["PDFFAMILYID"] = "EMPTY";
                    HttpContext.Current.Session["PDFPRODUCTID"] = "EMPTY";
                }
            }
            else if (Type == "FAMILY")
            {
               int Family_Id = Convert.ToInt32(Family_Name);
                string categoryId = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == Category_id).Select(x => x.CATEGORY_ID).SingleOrDefault();
                int familyId = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID == Family_Id).Select(x => x.FAMILY_ID).FirstOrDefault();


                HttpContext.Current.Session["PDFCATRGORYID"] = categoryId;
                HttpContext.Current.Session["PDFFAMILYID"] = familyId;
                HttpContext.Current.Session["PDFPRODUCTID"] = "EMPTY";

            }else if (Type == "PRODUCT")
            {
                int Family_Id = Convert.ToInt32(Family_Name);
                string categoryId = _dbcontext.TB_CATEGORY.Where(x => x.CATEGORY_ID == Category_id).Select(x => x.CATEGORY_ID).SingleOrDefault();
                int familyId = _dbcontext.TB_FAMILY.Where(x => x.FAMILY_ID == Family_Id).Select(x => x.FAMILY_ID).FirstOrDefault();


                HttpContext.Current.Session["PDFCATRGORYID"] = categoryId;
                HttpContext.Current.Session["PDFFAMILYID"] = familyId;
                HttpContext.Current.Session["PDFPRODUCTID"] = Product_id;
            }else
            {

            }
           
           
            return "Success";
        }

        public string DecryptStringAES(string cipherText)
        {
            try
            {
                var keybytes = Encoding.UTF8.GetBytes("8080808080808080");
                var iv = Encoding.UTF8.GetBytes("8080808080808080");

                var encrypted = Convert.FromBase64String(cipherText.Replace(" ", "+"));
                var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
                return string.Format(decriptedFromJavascript);
            }
            catch (Exception ex)
            {
               
                return null;
            }
        }
         
        private string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext = null;

            // Create an RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings  
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.  
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream  
                                // and place them in a string.  
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }


        [System.Web.Http.HttpPost]
        public int FindFamilyIDValue(string findFamilyId, string catalogID, string categoryID)
        {
            try
            {
                int familyIdValue = 0;
                int catalogIDVal =  Convert.ToInt32(catalogID);
                if(findFamilyId != "undefined")
                {                 
                    familyIdValue = _dbcontext.TB_CATALOG_FAMILY.Join(_dbcontext.TB_FAMILY, tcf => tcf.FAMILY_ID, tf => tf.FAMILY_ID, (tcf, tf) => new { tcf, tf })
                        .Where(x => x.tf.FAMILY_NAME == findFamilyId && x.tcf.CATALOG_ID == catalogIDVal).Select(x => x.tf.FAMILY_ID).FirstOrDefault();
                }
                return familyIdValue;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }


        [System.Web.Http.HttpPost]
        public HttpResponseMessage SaveCategoryPublishSortAttributes(string categoryId, int catalogId, JArray model)
        {
            try
            {
                // var arr = (JArray)((JObject.Parse(model.ToString())).SelectToken("data")).SelectToken("models");
                if (!string.IsNullOrEmpty(categoryId))
                {
                    var id = categoryId.Split('~');
                    categoryId = Convert.ToString(id[0]);

                }
                var functionAlloweditems = ((JArray)model).Select(x => new CategoryAttributePublish()
                {
                    CATALOG_ID = (int)x["CATALOG_ID"],
                    CATEGORY_ID = (string)x["CATEGORY_ID"],
                    ATTRIBUTE_ID = (int)x["ATTRIBUTE_ID"],
                    ATTRIBUTE_TYPE = (int)x["ATTRIBUTE_TYPE"],
                    // ATTRIBUTE_NAME = (string)x["ATTRIBUTE_NAME"],
                    ISAvailable = (bool)x["PUBLISHED"]
                }).ToList();
                int selectedcategorycount = functionAlloweditems.Count(name => name.ISAvailable == true);
                // int sortOrder = 0;


                int i = 1;
                foreach (var item in functionAlloweditems)
                {
                    //    var countCategory =
                    //     _dbcontext.TB_CATEGORY_SPECS.Count(
                    //         x =>

                    //         x.CATEGORY_ID == categoryId && x.CATALOG_ID == catalogId && x.ATTRIBUTE_ID ==item.ATTRIBUTE_ID && x.PUBLICHCATEGORY_FLAG==true);
                    //if (countCategory != 0)
                    //{
                    if (item.ISAvailable)
                    {
                        var objTbCategoryAttrList = _dbcontext.TB_CATEGORY_SPECS.FirstOrDefault(x =>

                       x.CATEGORY_ID == categoryId && x.CATALOG_ID == catalogId && x.ATTRIBUTE_ID == item.ATTRIBUTE_ID && x.PUBLICHCATEGORY_FLAG == true);

                        if (objTbCategoryAttrList != null)
                        {
                            // var objAttrList = new TB_PROD_FAMILY_ATTR_LIST();
                            objTbCategoryAttrList.SORT_ORDER = i;

                            //  _dbcontext.TB_PROD_FAMILY_ATTR_LIST.Add(objAttrList);
                            _dbcontext.SaveChanges();
                        }



                        // }

                    }
                    i++;
                }


                return Request.CreateResponse(HttpStatusCode.OK, ResponseMsg.Updated);
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : SaveCategoryPublishAttributes", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        #region Category Attribute new design->To get all attributes
        [System.Web.Http.HttpPost]
        public DataTable GetAllCategoryAttributesNew(int catalogId, string categoryId)
        {
            try
            {
                //  List<string> categoryAttributes=new List<string>();
                DataTable resultTable = new DataTable();
                int[] attributeType = { 21, 23, 24, 25 };
                var customAttr =
                      _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(z => z.CustomerId).ToList();
                int customerID = 0;
                if (customAttr.Any())
                {
                    int.TryParse(customAttr[0].ToString(), out customerID);
                    HttpContext.Current.Session["customerId"] = customerID;
                }
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_GETALLCATEGORY_ATTRIBUTES";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.NVarChar, 100).Value = catalogId;
                    objSqlCommand.Parameters.Add("@CATEGORY_ID", SqlDbType.NVarChar).Value = categoryId;
                    objSqlCommand.Parameters.Add("@CUSTOMER_ID", SqlDbType.NVarChar, 100).Value = customerID;
                    objSqlConnection.Open();
                    var objDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objDataAdapter.Fill(resultTable);
                }


                return resultTable;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController : GetAllCatalogattributes", objException);
                return null;
            }
        }
        #endregion


    }


}
