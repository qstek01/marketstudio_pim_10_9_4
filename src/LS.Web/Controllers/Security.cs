﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Net.Mail;


/// <summary>
/// Summary description for security
/// </summary>
namespace WebCat.CatalogDB
{
    public class Security
    {

        public delegate System.Security.Cryptography.ICryptoTransform GetEncryptengine();

        public static string Encrypt(string Clear_Text)
        {
            try
            {
                GetEncryptengine Encryptor = new GetEncryptengine(Get_Encryption_Engine().CreateEncryptor);
                return Convert.ToBase64String(Transform(System.Text.Encoding.Default.GetBytes(Clear_Text), Encryptor()));
            }
            catch (Exception objException)
            {
                string Error = objException.Message;
                return null;
            }

        }
        public static string Decrypt(string Cipher_Text)
        {
            try
            {
                SymmetricAlgorithm R_Encryption_engine = Get_Encryption_Engine();
                GetEncryptengine Decryptor = new GetEncryptengine(R_Encryption_engine.CreateDecryptor);
                return System.Text.Encoding.Default.GetString(Transform(Convert.FromBase64String(Cipher_Text), Decryptor()));
            }
            catch (Exception objException)
            {
                string Error = objException.Message.ToString();
                return null;
            }
        }
        public static System.Security.Cryptography.SymmetricAlgorithm Get_Encryption_Engine()
        {
            try
            {
                System.Security.Cryptography.SymmetricAlgorithm Encryption_engine;
                Encryption_engine = new System.Security.Cryptography.RijndaelManaged();
                Encryption_engine.Mode = System.Security.Cryptography.CipherMode.CBC;
                Encryption_engine.Key = Convert.FromBase64String("U1fknVDCPQWERTYGZfRqvAYCK7gFpUukYKOqsCuN8XU=");
                Encryption_engine.IV = Convert.FromBase64String("vEQWERTYRMrovjV+NXos5g==");
                return Encryption_engine;
            }
            catch (Exception objException)
            {
                string Error = objException.Message;
                return null;
            }
        }
        public static byte[] Transform(byte[] Source, System.Security.Cryptography.ICryptoTransform Transformer)
        {
            try
            {
                System.IO.MemoryStream stream = new System.IO.MemoryStream();
                System.Security.Cryptography.CryptoStream cryptographic_stream = new System.Security.Cryptography.CryptoStream(stream, Transformer, System.Security.Cryptography.CryptoStreamMode.Write);
                cryptographic_stream.Write(Source, 0, Source.Length);
                cryptographic_stream.FlushFinalBlock();
                cryptographic_stream.Close();
                return stream.ToArray();
            }
            catch (Exception objException)
            {
                string Error = objException.Message;
                return null;
            }
        }



    }
}
