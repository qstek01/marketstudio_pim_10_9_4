﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace LS.Web.Controllers
//{
//    public class RecycleBinController : Controller
//    {
//        // GET: RecycleBin
//        public ActionResult Index()
//        {
//            return View();
//        }

//        // GET: RecycleBin/Details/5
//        public ActionResult Details(int id)
//        {
//            return View();
//        }

//        // GET: RecycleBin/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: RecycleBin/Create
//        [HttpPost]
//        public ActionResult Create(FormCollection collection)
//        {
//            try
//            {
//                // TODO: Add insert logic here

//                return RedirectToAction("Index");
//            }
//            catch
//            {
//                return View();
//            }
//        }

//        // GET: RecycleBin/Edit/5
//        public ActionResult Edit(int id)
//        {
//            return View();
//        }

//        // POST: RecycleBin/Edit/5
//        [HttpPost]
//        public ActionResult Edit(int id, FormCollection collection)
//        {
//            try
//            {
//                // TODO: Add update logic here

//                return RedirectToAction("Index");
//            }
//            catch
//            {
//                return View();
//            }
//        }

//        // GET: RecycleBin/Delete/5
//        public ActionResult Delete(int id)
//        {
//            return View();
//        }

//        // POST: RecycleBin/Delete/5
//        [HttpPost]
//        public ActionResult Delete(int id, FormCollection collection)
//        {
//            try
//            {
//                // TODO: Add delete logic here

//                return RedirectToAction("Index");
//            }
//            catch
//            {
//                return View();
//            }
//        }
//    }
//}


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace LS.Web.Controllers
{
    public class RecycleBinController : Controller
    {
        // GET: RecycleBin
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult RecycleBin()
        {
            if (string.IsNullOrEmpty(User.Identity.Name))
            {
                return RedirectToAction("LogOn", "Accounts");
            }
            return View("~/Views/App/RecycleBin/RecycleBin.cshtml");

        }


    }
}
