﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Data;
using Infragistics.Documents.Excel;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using log4net;
using LS.Data;
using LS.Data.Model;
using LS.Data.Model.CatalogSectionModels;
using LS.Web.Models;
using LS.Web.Utility;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using ClosedXML;
using ClosedXML.Excel;
using System.Text.RegularExpressions;
using LS.Data.Utilities;
using System.Collections;
using iTextSharpPDF = iTextSharp.text.pdf;
using Stimulsoft.Report;
using Stimulsoft.Report.Export;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf;
using System.Reflection;


namespace LS.Web.Controllers
{
    public class PDFIndexApiController : ApiController
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        private readonly CSEntities _dbcontext = new CSEntities();
        private static readonly ILog Logger = LogManager.GetLogger(typeof(HomeApiController));
        private static StiReport Report = new StiReport();

        public class indexData
        {
            public string KEYWORD_NAME { get; set; }
            public string PAGE_NO { get; set; }
        }

        public class sourcePDFdata
        {
            public string FILE_NAME { get; set; }
            public string FILE_PATH { get; set; }
        }

        /// <summary>
        /// creating keywords process with specified template name
        /// </summary>
        /// <param name="keyword">passes the input keyword name</param>
        /// <param name="group">passes the input keyword group name</param>
        /// <returns>"success" or null</returns>
        [System.Web.Http.HttpPost]
        public string addKeywordData(string keyword, string group)
        {
            try
            {
                if (!String.IsNullOrEmpty(keyword))
                {
                    var keywordCheck = _dbcontext.QSWS_KEYWORDS_LIST.Where(x => x.KEYWORD_NAME == keyword && x.KEYWORD_LIST == group).Count();
                    var keywordInExempted = _dbcontext.QSWS_EXCEPT_WORDS.Where(x => x.WORD == keyword).Count();
                    if (keywordCheck == 0 && keywordInExempted == 0)
                    {
                        QSWS_KEYWORDS_LIST objKeyList = new QSWS_KEYWORDS_LIST();
                        objKeyList.KEYWORD_NAME = keyword;
                        objKeyList.KEYWORD_LIST = group;
                        _dbcontext.QSWS_KEYWORDS_LIST.Add(objKeyList);
                        _dbcontext.SaveChanges();
                        return "Saved Successfully.";
                    }
                    else
                    {
                        return "Keyword Already Exist.";
                    }
                }
                else
                {
                    return "Plese enter the Keyword";
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : AddKeywordData", objexception);
                return null;
            }
        }  

        /// <summary>
        /// this function is to save the exempted word into the table
        /// </summary>
        /// <param name="word">passes the input exemptedword</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public string saveExemptedWord(string word)
        {

            try
            {
                if (!String.IsNullOrEmpty(word))
                {
                    var exemptCheck = _dbcontext.QSWS_EXCEPT_WORDS.Where(x => x.WORD.ToLower() == word.ToLower()).Count();
                    var exemptedInKeyword = _dbcontext.QSWS_KEYWORDS_LIST.Where(x => x.KEYWORD_NAME == word).ToList();
                    if (exemptCheck == 0)
                    {
                       // if (Regex.IsMatch(word, @"^[a-zA-Z]+$"))
                       // {
                            foreach (QSWS_KEYWORDS_LIST row in exemptedInKeyword)
                            {
                                _dbcontext.QSWS_KEYWORDS_LIST.Remove(row);
                            }
                            QSWS_EXCEPT_WORDS objExpt = new QSWS_EXCEPT_WORDS();
                            objExpt.WORD = word;
                            _dbcontext.QSWS_EXCEPT_WORDS.Add(objExpt);
                            _dbcontext.SaveChanges();
                            return "Saved Successfully.";
                     //   }
                     //   return "Save Failed.";
                    }
                    else
                    {
                        return "Keyword Already Exist.";
                    }

                }
                else
                {
                    return "Please enter the Keyword";
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : saveExemptedWord", objexception);
                return null;
            }
        }

        /// <summary>
        /// getting the keywords grid data
        /// </summary>
        /// <param name="groupType">passes the keywords template name</param>
        /// <returns>Ilist or null</returns>
        [System.Web.Http.HttpPost]
        public IList getKeywordsList(string groupType)
        {
            try
            {
                var gridList = _dbcontext.QSWS_KEYWORDS_LIST.Where(x => x.KEYWORD_LIST == groupType).ToList();
                return gridList;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : getKeywordsList", objexception);
                return null;
            }
        }

        /// <summary>
        /// getting the content template list value
        /// </summary>
        /// <returns> returning content template list</returns>
        [System.Web.Http.HttpPost]
        public IList getContentTempList()
        {
            try
            {
                var contentTemplateList = _dbcontext.QSWS_PDFCONTENT_TEMPLATE.ToList();
                return contentTemplateList;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : getContentTempList", objexception);
                return null;
            }
        }

        /// <summary>
        /// adding content template details into QSWS_PDFCONTENT_TEMPLATE table
        /// </summary>
        /// <param name="templateName">passes the input template name for saving</param>
        /// <returns>success or null</returns>
        [System.Web.Http.HttpPost]
        public string saveTemplateDetail(string templateName)
        {
            try
            {
                var contentTemplateCheck = _dbcontext.QSWS_PDFCONTENT_TEMPLATE.Where(x => x.TEMPLATE_NAME == templateName).Count();
                if (contentTemplateCheck == 0)
                {
                    QSWS_PDFCONTENT_TEMPLATE objTemp = new QSWS_PDFCONTENT_TEMPLATE();
                    objTemp.TEMPLATE_NAME = templateName;
                    _dbcontext.QSWS_PDFCONTENT_TEMPLATE.Add(objTemp);
                    _dbcontext.SaveChanges();
                }
                return "success";
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : saveTemplateDetail", objexception);
                return null;
            }
        }

        /// <summary>
        /// extracting the source document data and store that in QSWS_PDFCONTENT_DATA table
        /// </summary>
        /// <param name="templateName">passes the template name under which the PDF data will store</param>
        /// <param name="filePath">passes the PDF file path which we need to read</param>
        /// <returns>success or null</returns>
        [System.Web.Http.HttpPost]
        public string readSourceData(string templateName, string filePath)
        {
            try
            {
                string pdfData = "";
                using (iTextSharpPDF.PdfReader reader = new iTextSharpPDF.PdfReader(filePath))
                {
                    for (int i = 1; i <= reader.NumberOfPages; i++)
                    {
                        iTextSharpPDF.parser.ITextExtractionStrategy startegy = new iTextSharpPDF.parser.LocationTextExtractionStrategy();
                        string currentpageContent = iTextSharpPDF.parser.PdfTextExtractor.GetTextFromPage(reader, i, startegy);
                        pdfData = pdfData + " " + currentpageContent;
                        QSWS_PDFCONTENT_DATA objPDFdata = new QSWS_PDFCONTENT_DATA();
                        objPDFdata.PAGE_NO = i;
                        objPDFdata.PAGE_DATA = currentpageContent;
                        objPDFdata.TEMPLATE_NAME = templateName;
                        _dbcontext.QSWS_PDFCONTENT_DATA.Add(objPDFdata);
                    }
                    string[] pdfWords = pdfData.Split(' ');
                    if (pdfData == " ")
                    {
                        return "Failed";
                    }
                    foreach (string word in pdfWords)
                    {
                        if (Regex.IsMatch(word, @"^[a-zA-Z]+$") && word.Trim().Length>1)
                        {
                            var wordCheck = _dbcontext.QSWS_EXCEPT_WORDS.Where(x => x.WORD.ToLower() == word.ToLower()).Count();
                            var keywordCheck = _dbcontext.QSWS_KEYWORDS_LIST.Where(x => x.KEYWORD_NAME == word && x.KEYWORD_LIST == templateName).Count();
                            if (wordCheck == 0 && keywordCheck == 0)
                            {
                                QSWS_KEYWORDS_LIST objKeyList = new QSWS_KEYWORDS_LIST();
                                objKeyList.KEYWORD_NAME = word;
                                objKeyList.KEYWORD_LIST = templateName;
                                _dbcontext.QSWS_KEYWORDS_LIST.Add(objKeyList);
                                _dbcontext.SaveChanges();
                            }
                        }
                    }
                    return "success";
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : readSourceData", objexception);
                return null;
            }
        }

        /// <summary>
        /// obtaining the PDF data of the grid for a particular template
        /// </summary>
        /// <param name="templatename">passes the template name for which we need the PDF data</param>
        /// <returns>returns Grid data as a list</returns>
        [System.Web.Http.HttpPost]
        public IList getContentGridData(string templatename)
        {
            try
            {
                var contentGridData = _dbcontext.QSWS_PDFCONTENT_DATA.Where(x => x.TEMPLATE_NAME == templatename).ToList();
                return contentGridData;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : getContentGridData", objexception);
                return null;
            }
        }

        /// <summary>
        /// getting index PDF data from STP_QSWS_PDFINDEX_GENERATION for the selected template value 
        /// </summary>
        /// <param name="templateName">passes the selected template name for which we generate index for</param>
        /// <param name="layout">layout design which used to generate PDF</param>
        /// <returns>Index data and generated index PDF path</returns>
        [System.Web.Http.HttpPost]
        public Tuple<DataTable, string> indexGenerationProcess(string templateName, string layout)
        {
            try
            {
                string tempOut = "";
                var layoutType = layout;
                string templayoutPath = ConfigurationSettings.AppSettings["layoutPath"].ToString() + layoutType + ".mrt";
                Guid currId = Guid.NewGuid();
                string sessionId = Regex.Replace(currId.ToString(), "[^a-zA-Z0-9_]+", "");
                string indexTemplate = templateName;
                DataTable objPdfDataset = new DataTable();
                using (var objSqlConnection =
                       new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_QSWS_PDFINDEX_GENERATION";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.AddWithValue("@OPTION", "GENERATE_INDEX");
                    objSqlCommand.Parameters.AddWithValue("@TEMPLATE_NAME", templateName);
                    objSqlConnection.Open();
                    SqlDataAdapter objDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objDataAdapter.Fill(objPdfDataset);
                }
                foreach (DataRow dr in objPdfDataset.Rows)
                {
                    var selectedPgno = dr["PAGE_NO"].ToString();
                    if (selectedPgno.Length > 1)
                    {
                        var arrayPageno = selectedPgno.Split(',');
                        if (arrayPageno.Length > 2)
                        {
                            var newPageNo = "";
                            var firstValue = arrayPageno[0];
                            var count = 0;
                            int a = 0;
                            int b = 0;
                            pageLoop:
                            for (int i = a; i < arrayPageno.Length;)
                            {
                                //count = 0;
                                if (((i + 1) < arrayPageno.Length) && (Int32.Parse(arrayPageno[i + 1]) == Int32.Parse(firstValue) + (b + 1)))
                                {
                                    count++;
                                    a++;
                                    b++;
                                    if ((a + 1) < arrayPageno.Length)
                                        goto pageLoop;
                                    else
                                    {
                                        a = a + 1;
                                        newPageNo = newPageNo + firstValue + "-" + arrayPageno[i + 1] + ",";
                                        b = 0;
                                        count = 0;
                                        goto pageLoop;
                                    }
                                }
                                else if (count > 0)
                                {
                                    newPageNo = newPageNo + firstValue + "-" + arrayPageno[i] + ",";
                                    count = 0;
                                    b = 0;
                                    firstValue = arrayPageno[i + 1];
                                    a++;
                                    goto pageLoop;
                                }
                                else
                                {
                                    newPageNo = newPageNo + arrayPageno[i] + ",";
                                    if ((i + 1) < arrayPageno.Length)
                                        firstValue = arrayPageno[i + 1];
                                    a++;
                                    goto pageLoop;
                                }
                            }
                            dr["PAGE_NO"] = newPageNo.Remove(newPageNo.Length - 1);
                        }
                    }
                }
                //tempOut = generatePDFIndex(objPdfDataset, templayoutPath, sessionId);
                Tuple<DataTable, string> odjTuple = new Tuple<DataTable, string>(objPdfDataset, tempOut);
                return odjTuple;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : indexGenerationProcess", objexception);
                return null;
            }
        }        

        /// <summary>
        /// generationg the index PDF for the selected template value
        /// </summary>
        /// <param name="objPdfDataset">passses the data with page numbers which we need to generate as PDF</param>
        /// <param name="filePath">Passes the output file path where the generated PDF file will be stored</param>
        /// <param name="sessionId">passes a unique id which we use as the PDF name</param>
        /// <returns>returns the output file path with file name</returns>
        public string generatePDFIndex(string layout, string filePath, string insertType, object dataTable)
        {
            try
            {
                var dataTableList = ((JArray)dataTable).Select(x => new indexData
                {
                    KEYWORD_NAME = (string)x["KEYWORD_NAME"],
                    PAGE_NO = (string)x["PAGE_NO"]
                }).ToList();
                DataTable objPdfDataset = ToDataTable(dataTableList);
                string dt = "";
                var layoutType = layout;
                string templayoutPath = ConfigurationSettings.AppSettings["layoutPath"].ToString() + layoutType + ".mrt";
                Guid currId = Guid.NewGuid();
                string sessionId = Regex.Replace(currId.ToString(), "[^a-zA-Z0-9_]+", "");
                string outputFilePath = ConfigurationSettings.AppSettings["outputIndex"];
                Exception pdf1 = new Exception();
                Report.Load(templayoutPath);
                int pagesCount = Report.Pages.Count;
                objPdfDataset.TableName = "Index_Table";
                Report.Dictionary.Databases.Clear();
                Report.Dictionary.DataSources.Clear();
                Report.RegData(objPdfDataset);
                Report.Dictionary.Synchronize();
                Report.Render(false);
                string newFilePath = Path.Combine(outputFilePath + @"\PdfStore\" + sessionId);
                if (!Directory.Exists(newFilePath))
                {
                    Directory.CreateDirectory(newFilePath);
                }

                Stream ostream = new FileStream(newFilePath + @"\" + sessionId + ".pdf", FileMode.OpenOrCreate, FileAccess.Write);
                var pdfSettings = new StiPdfExportSettings
                {
                    EmbeddedFonts = true,
                    ImageQuality = 0.9f,
                    ImageResolution = 300,
                    Compressed = true,
                    UseUnicode = true,
                    ImageCompressionMethod = StiPdfImageCompressionMethod.Jpeg
                };
                Report.ExportDocument(StiExportFormat.Pdf, ostream, pdfSettings);
                ostream.Dispose();
                ostream.Close();
                dt = newFilePath + @"\" + sessionId + ".pdf";

                string output = pdfMergeProcess(dt, filePath, insertType);
                return output;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : generatePDFIndex", objexception);
                return null;
            }

        }

        /// <summary>
        /// merging the generated index PDF with the selected source file.
        /// </summary>
        /// <param name="indexFile">pasess the generated index file path</param>
        /// <param name="sourceFile">passes the source document path</param>
        /// <param name="insertType">passes (top or bottom) where we need to insert generated index PDF</param>
        /// <returns>returns sessionId</returns>
        [System.Web.Http.HttpPost]
        public string pdfMergeProcess(string indexFile, string sourceFile, string insertType)
        {
            try
            {
                PdfDocument readdoc;
                var mergedDocument = new PdfDocument();
                Guid currId = Guid.NewGuid();
                string sessionId = Regex.Replace(currId.ToString(), "[^a-zA-Z0-9_]+", "");
                string FilePath = ConfigurationSettings.AppSettings["outputIndex"];

                var inputFiles = new List<string>();

                if (insertType == "top")
                {
                    inputFiles.Add(indexFile);
                    inputFiles.Add(sourceFile);
                    //string[] inputFiles = new string[] { sourceFile, indexFile };
                }
                else
                {
                    inputFiles.Add(sourceFile);
                    inputFiles.Add(indexFile);
                }
                foreach (var file in inputFiles)
                {
                    readdoc = PdfReader.Open(file, PdfDocumentOpenMode.Import);
                    foreach (PdfPage pg in readdoc.Pages)
                    {
                        mergedDocument.AddPage(pg);
                    }
                }
                foreach (var file in inputFiles)
                {
                    File.Delete(file);
                }
                mergedDocument.Save(FilePath + @"\FinalPDF" + sessionId + ".pdf");
                mergedDocument.Dispose();
                return sessionId;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : pdfMergeProcess", objexception);
                return null;
            }
        }

        /// <summary>
        /// getting the exempted words list which we neglect while PDF generation 
        /// </summary>
        /// <returns>returs a list containing exempted words</returns>
        [System.Web.Http.HttpPost]
        public IList getExemptedWords()
        {
            try
            {
                var exemptedList = _dbcontext.QSWS_EXCEPT_WORDS.ToList();
                return exemptedList;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : getExemptedWords", objexception);
                return null;
            }
        }

        /// <summary>
        /// deleted the selected exempted word from the list
        /// </summary>
        /// <param name="deleteId">passes the id value which the data will be deleted</param>
        /// <returns>returns success or null</returns>
        [System.Web.Http.HttpPost]
        public string deleteExemptedWord(int deleteId)
        {
            try
            {
                var deleteRecord = _dbcontext.QSWS_EXCEPT_WORDS.Where(x => x.ID == deleteId).FirstOrDefault();
                _dbcontext.QSWS_EXCEPT_WORDS.Remove(deleteRecord);
                _dbcontext.SaveChanges();
                return "success";
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : deleteExemptedWord", objexception);
                return null;
            }
        }

        /// <summary>
        /// deleting the selected keyword and adding that value into exempted words list
        /// </summary>
        /// <param name="deleteId">passes the id value under which the value willbe deleted</param>
        /// <returns>return success or null</returns>
        [System.Web.Http.HttpPost]
        public string deleteKeyword(int deleteId)
        {
            try
            {
                var deleteRecord = _dbcontext.QSWS_KEYWORDS_LIST.Where(x => x.ID == deleteId).FirstOrDefault();
                var exmptCheck = _dbcontext.QSWS_EXCEPT_WORDS.Where(x => x.WORD.ToUpper() == deleteRecord.KEYWORD_NAME.ToUpper()).Count();
                if (exmptCheck == 0)
                {
                    QSWS_EXCEPT_WORDS objExpt = new QSWS_EXCEPT_WORDS();
                    objExpt.WORD = deleteRecord.KEYWORD_NAME;
                    _dbcontext.QSWS_EXCEPT_WORDS.Add(objExpt);
                }
                _dbcontext.QSWS_KEYWORDS_LIST.Remove(deleteRecord);
                _dbcontext.SaveChanges();
                return "success";
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApi : deleteKeyword", objexception);
                return null;
            }
        }

        /// <summary>
        /// updating the keywords value after editing
        /// </summary>
        /// <param name="data">passes the edited list having new keyword name to save</param>
        [System.Web.Http.HttpPost]
        public string updateKeywords(object modifiedData)
        {
            try
            {
                var kewordsData = ((JArray)modifiedData).Select(x => new QSWS_KEYWORDS_LIST
                {
                    ID = (int)x["ID"],
                    KEYWORD_NAME = (string)x["KEYWORD_NAME"],
                    KEYWORD_LIST = (string)x["KEYWORD_LIST"]
                }).ToList();
                DataTable newKeywordsList = ToDataTable(kewordsData);
                using (var objSqlConnection =
                                       new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_QSWS_PDF_INDEX_MANAGEMENT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.AddWithValue("@KEYWORDS_TABLE", newKeywordsList);
                    objSqlConnection.Open();
                    objSqlCommand.ExecuteNonQuery();
                }
                return "success";
                //foreach (var selectedRow in data)
                //{
                //    var updatedRow = _dbcontext.QSWS_KEYWORDS_LIST.Where(x => x.ID == selectedRow.ID).FirstOrDefault();
                //    updatedRow.KEYWORD_NAME = selectedRow.KEYWORD_NAME;
                //}
                //_dbcontext.SaveChanges();
                //IList<QSWS_KEYWORDS_LIST> keywordsList = data;
                //DataTable newKeywords = ToDataTable(keywordsList);                     
                //foreach (QSWS_KEYWORDS_LIST row in newKeywordsList)
                //{
                //    var selectedRow = _dbcontext.QSWS_KEYWORDS_LIST.Where(x => x.ID == row.ID).FirstOrDefault();
                //    selectedRow.KEYWORD_NAME = row.KEYWORD_NAME;
                //}
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : UpdateKeywords", objexception);
                return null;
            }
        }

        /// <summary>
        /// function to convert list to data table
        /// </summary>
        /// <typeparam name="T">data table type</typeparam>
        /// <param name="items">input list value</param>
        /// <returns>returns datatable for the corresponding list value</returns>
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedTemplateNmae"></param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public string ExportKeywordsList(string selectedTemplateNmae, string exportFormatType)
        {
            try
            {


                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {


                    exportFormatType = exportFormatType.ToLower();
                    var ExportIndex = new DataSet();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "QS_PDFINDEX_EXPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.AddWithValue("@SelectedTemplate", selectedTemplateNmae);
                    objSqlCommand.Parameters.AddWithValue("@SelectedExport", "PDFCONTENT");
                    objSqlConnection.Open();
                    SqlDataAdapter objDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objDataAdapter.Fill(ExportIndex);
                    System.Web.HttpContext.Current.Session["KEYWORDS"] = ExportIndex.Tables[0];
                    System.Web.HttpContext.Current.Session["Exemptwords"] = null;
                    System.Web.HttpContext.Current.Session["ExportTable"] = null;

                    return "ExportKeywordsList" + exportFormatType;

                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApiController : ExportKeywordsList", objexception);
                return null;
            }




        }


        [System.Web.Http.HttpPost]
        public string exceptwords(string exportFormatType)
        {
            try
            {


                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {


                    exportFormatType = exportFormatType.ToLower();
                    var ExportIndex = new DataSet();
                    DataTable ExportDataTableKeyWordList = new DataTable();
                    DataTable ExportPdfContent = new DataTable();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "QS_PDFINDEX_EXPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.AddWithValue("@SelectedTemplate", "");
                    objSqlCommand.Parameters.AddWithValue("@SelectedExport", "Exemptwords");
                    objSqlConnection.Open();
                    SqlDataAdapter objDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objDataAdapter.Fill(ExportIndex);
                    System.Web.HttpContext.Current.Session["PDFCONTENT"] = null;
                    System.Web.HttpContext.Current.Session["KEYWORDS"] = null;
                    System.Web.HttpContext.Current.Session["Exemptwords"] = null;
                    System.Web.HttpContext.Current.Session["ExportTable"] = null;
                    System.Web.HttpContext.Current.Session["Exemptwords"] = ExportIndex.Tables[0];


                }
                return "Exemptwords" + exportFormatType;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApiController : ExportKeywordsList", objexception);
                return null;
            }

        }
        /// <summary>
        /// merging the selected multiple source files to generate the index source PDF file
        /// </summary>
        /// <param name="selectedValues">array that contains selected multiple PDF files</param>
        [System.Web.Http.HttpPost]
        public string mergeSourcePDF(object selectedValues)
        {
            try
            {                
                var dataTableList = ((JArray)selectedValues).Select(x => new sourcePDFdata
                {
                    FILE_NAME = (string)x["FILE_NAME"],
                    FILE_PATH = (string)x["FILE_PATH"]
                }).ToList();

                PdfDocument readdoc;
                var mergedDocument = new PdfDocument();
                Guid currId = Guid.NewGuid();
                string sessionId = Regex.Replace(currId.ToString(), "[^a-zA-Z0-9_]+", "");
                string FilePath = ConfigurationSettings.AppSettings["outputIndex"];

                var inputFiles = new List<string>();
                foreach (var file in dataTableList)
                {
                    readdoc = PdfReader.Open(file.FILE_PATH, PdfDocumentOpenMode.Import);
                    foreach (PdfPage pg in readdoc.Pages)
                    {
                        mergedDocument.AddPage(pg);
                    }
                }
                var outputSourcePDF = FilePath + @"\FinalSourcePDF" + sessionId + ".pdf";
                mergedDocument.Save(FilePath + @"\FinalSourcePDF" + sessionId + ".pdf");
                mergedDocument.Dispose();
                return outputSourcePDF;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at PDFIndexApiController : mergeSourcePDF", objexception);
                return null;
            }
        }
        /// <summary>
        /// process of deleting the details which is stored under he selected template name
        /// </summary>
        /// <param name="templateName">It passes the template name for which we have to delete the details</param>
        /// <returns></returns>
        [System.Web.Http.HttpPost]
        public string DeletePDFIndexValues(string templateName)
        {
            try
            {
                using (var objSqlConnection =
                       new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_QSWS_PDFINDEX_GENERATION";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.AddWithValue("@OPTION", "DELETE_INDEX");
                    objSqlCommand.Parameters.AddWithValue("@TEMPLATE_NAME", templateName);
                    objSqlConnection.Open();
                    objSqlCommand.ExecuteNonQuery();
                }
                return "success";
            }
            catch (Exception e)
            {
                Logger.Error("Error at PDFIndexApiController : DeletePDFIndexValues", e);
                return null;
            }
        }
       
    }
}
