﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LS.Web.Controllers
{
    public class ChartDetails
    {
        public string CATEGORY_NAME {get;set;}
        public string MODIFIED_USER { get; set; }
        public string CREATED_DATE { get; set; }
        public string MODIFIED_DATE { get; set; }

    }
    public class FamilyChartDetails
    {
        public string FAMILY_NAME { get; set; }
        public string MODIFIED_USER { get; set; }
        public string CREATED_DATE { get; set; }
        public string MODIFIED_DATE { get; set; } 
    }
    public class ProductChartDetails
    {
        public string STRING_VALUE { get; set; }
        public string MODIFIED_USER { get; set; }
        public string CREATED_DATE { get; set; }
        public string MODIFIED_DATE { get; set; }
    }
}
