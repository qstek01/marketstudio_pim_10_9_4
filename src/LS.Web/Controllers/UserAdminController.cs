﻿using LS.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using System.Net;
using System.IO;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Dapper;
using System.Configuration;
using LS.Data.Model.Accounts;
using System.Net.Http;

namespace LS.Web.Controllers
{
    public class UserAdminController : Controller
    {
        // GET: UserAdmin
        readonly CSEntities _objLs = new CSEntities();

        static ILog _logger = LogManager.GetLogger(typeof(FamilyApiController));
        public ActionResult Index()
        {
            return View();
        }
        public string DecryptString(string password)
        {
            byte[] b;
            string decrypted;
            try
            {
                b = Convert.FromBase64String(password);
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(b);
            }
            catch (FormatException fe)
            {
                decrypted = "";
            }
            return decrypted;
        }
        public ActionResult UserAdmin()
         {
            //if (string.IsNullOrEmpty(User.Identity.Name))
            //{
            //    return RedirectToAction("LogOn", "Accounts");
            //}
            //TempData.Keep("WelcomeName");


           // return View("~/Views/UserAdmin/UserAdministration.cshtml");
            var username = User.Identity.Name;
            string password = Session["passWord"].ToString();
            byte[] b;
            b = Convert.FromBase64String(password);
            password = System.Text.ASCIIEncoding.ASCII.GetString(b);


            Response.Cookies["userName"].Value = username;
            Response.Cookies["userName"].Expires = DateTime.Now.AddHours(10);
            Response.Cookies["userName"].HttpOnly = false;
            Response.Cookies["userName"].Path = "/";
            if (System.Web.Configuration.WebConfigurationManager.AppSettings["DomainName"].ToString() != "")
            {
                Response.Cookies["userName"].Domain = System.Web.Configuration.WebConfigurationManager.AppSettings["DomainName"].ToString();

            }
            var userData = "username=" + username + "&password=" + password + "&grant_type=password";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(System.Web.Configuration.WebConfigurationManager.AppSettings["ConsoleApiURL"].ToString() + userData);
            request.Accept = "application/json";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            request.AllowAutoRedirect = true;
            byte[] bytes = Encoding.ASCII.GetBytes(userData);


            request.ContentLength = bytes.Length;
            using (Stream os = request.GetRequestStream())
            {
                //Stream os = request.GetRequestStream();
                os.Write(bytes, 0, bytes.Length); //Push it out there
                os.Close();
            }
            try
            {
                HttpWebResponse httpWebResponse = (HttpWebResponse)request.GetResponse();
                if (httpWebResponse.StatusCode.ToString() == "OK")
                {
               
                    //Response.Redirect(System.Web.Configuration.WebConfigurationManager.AppSettings["ConsoleURL"].ToString());

                   return Redirect(System.Web.Configuration.WebConfigurationManager.AppSettings["ConsoleURL"].ToString());
                  
                  
                   
                }

            }
            catch (Exception e)
            {
                return RedirectToAction("Account", "LogOn", e);
            }





            return Redirect("#");
        }

        public ActionResult OCPRedirect()
        {
            //if (string.IsNullOrEmpty(User.Identity.Name))
            //{
            //    return RedirectToAction("LogOn", "Accounts");
            //}
            //TempData.Keep("WelcomeName");


            //return View("~/Views/UserAdmin/UserAdministration.cshtml");
            var username = User.Identity.Name;
            string password = Session["passWord"].ToString();
            byte[] b;
            b = Convert.FromBase64String(password);
            password = System.Text.ASCIIEncoding.ASCII.GetString(b);


            Response.Cookies["userName"].Value = username;
            Response.Cookies["userName"].Expires = DateTime.Now.AddHours(10);
            Response.Cookies["userName"].HttpOnly = false;
            Response.Cookies["userName"].Path = "/";
            if (System.Web.Configuration.WebConfigurationManager.AppSettings["DomainName"].ToString()!="")
            {
                Response.Cookies["userName"].Domain = System.Web.Configuration.WebConfigurationManager.AppSettings["DomainName"].ToString();
               
            }


            var userData = "username=" + username + "&password=" + password + "&grant_type=password";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(System.Web.Configuration.WebConfigurationManager.AppSettings["WebApiURL"].ToString() + userData);
            request.Accept = "application/json";
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            request.AllowAutoRedirect = true;
            byte[] bytes = Encoding.ASCII.GetBytes(userData);

            request.ContentLength = bytes.Length;
            using (Stream os = request.GetRequestStream())
            {
                //Stream os = request.GetRequestStream();
                os.Write(bytes, 0, bytes.Length); //Push it out there
                os.Close();
            }
            try
            {
                HttpWebResponse httpWebResponse = (HttpWebResponse)request.GetResponse();
                if (httpWebResponse.StatusCode.ToString() == "OK")
                {
                    var RoleId = GetRoleId(User.Identity.Name);
                    // Un COMMET These lines in 10.9.5 Version
                //    var result = GetHomePageStatus(RoleId);
                 //   if (result == true)
                    //{
                    //    var redirectUrl= System.Web.Configuration.WebConfigurationManager.AppSettings["WebURL"].ToString() + "dashboard";
                    //    return Redirect(redirectUrl);
                    //}
                    //else
                    //{
                        var redirectUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["WebURL"].ToString() + "category";
                        return Redirect(redirectUrl);
                   // }

                }

            }
            catch (Exception e)
            {
                return RedirectToAction("Account", "LogOn", e);
            }





            return Redirect("#");
        }


        public string GetRoleId(string userName)
        {
            try
            {
                using (IDbConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    var parameter = new { userName = userName };
                    var getRoleIdQuery = "select Role_id from aspnet_Roles as x join aspnet_UsersInRoles as y on x.RoleId = y.RoleId join aspnet_Users as z on y.UserId = z.UserId and z.UserName = @userName ";
                    var roleId = connection.Query(getRoleIdQuery, parameter).FirstOrDefault();
                    roleId = roleId.Role_id;
                    string role_Id = roleId.ToString();
                    return role_Id;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at CategoryApiController: GetRoleId", ex);
                return null;
            }
        }
        public bool GetHomePageStatus(string roleId)

        {
            try
            {
                using (IDbConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    var parameter = new { RoleId = roleId };
                    var getHomePageStatusQuery = "select ACTION_VIEW from TB_ROLE_FUNCTIONS where ROLE_ID = @RoleId and FUNCTION_ID=5000086";
                    var homePageStatus = connection.Query(getHomePageStatusQuery, parameter).FirstOrDefault();
                    homePageStatus = homePageStatus.ACTION_VIEW;
                    return homePageStatus;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error at HomeApiController : GetHomePageStatus", ex);
                return false;
            }
        }
        [HttpGet]
        public IList GetUsers()
        {
            try
            {
                var lx = (from x in new CSEntities().STP_LS_GET_USER_LIST(User.Identity.Name,0) select x).ToList();
                return lx;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at getfamilyspecs", ex);
                return null;
            }
            // return null;
        }

        //Custom Menu
         [HttpGet]
        public IList getHeader()
        {
            try
            {
                var UserId = _objLs.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var result = _objLs.Custom_Menu.Where(x => x.Customer_ID == UserId).Select(y => y.Header).ToList();

                return result;
            }
            catch (Exception ex)
            {
                _logger.Error("Error at Custom ", ex);
                return null;
            }
            //return null;
        }
         public ActionResult Announcement()
         {
             if (string.IsNullOrEmpty(User.Identity.Name))
             {
                 return RedirectToAction("LogOn", "Accounts");
             }
             return View("~/Views/UserAdmin/Announcement.cshtml");
         }
        
    }
}