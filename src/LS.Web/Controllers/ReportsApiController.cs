﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using log4net;
using LS.Data;
using System.Collections;
using LS.Data.Model.Reports;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Web.Configuration;
using System.Web;
using LS.Data.Model.CatalogSectionModels;
using System.Web.Http.Results;
using Newtonsoft.Json;
using System.Web.Mvc;
using LS.Data.Model.ProductView;
using System.Globalization;
using Newtonsoft.Json.Linq;

namespace LS.Web.Controllers
{
    public class ReportsApiController : ApiController
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;


        private static readonly ILog Logger = LogManager.GetLogger(typeof(ReportsApiController));
        private readonly CSEntities _dbcontext = new CSEntities();

        [System.Web.Http.HttpGet]
        public IList GetAllReports(int catalogId, DateTime startDateModify, DateTime endDateModify, DateTime startDatenew, DateTime endDatenew, string option,string fileNames, DateTime start_Date, DateTime end_date
            )
        {
            try
            {
                if (option == "Category Listfrom MasterCatalog")
                {

                    var customArr = new List<STP_REPORT_Category_Listfrom_MasterCatalog_Result>();
                    Reportresults(catalogId, customArr);


                    return customArr;

                }
                if (option == "Image Type Reports")
                {
                    return Reportresultsoptiontwo();
                }
                if (option == "Supplier List Report")
                {

                    var supplier = new List<STP_REPORT_SUPPLIER_LIST_REPORT_Result>();
                    Supplierresults(catalogId, supplier);
                    return supplier;
                }
                if (option == "Virtual Pages Report")
                {

                    var virtuallist = new List<STP_REPORT_VIRTUAL_PAGES_REPORT_Result_DATA>();
                    Virtualresults(catalogId, virtuallist);
                    return virtuallist;
                }
                if (option == "Family Attribute Association with Catalog Report")
                {

                    var familyattrib = new List<STP_REPORT_FAMILY_ATTRIBUTE_ASSOCIATION_WITH_CATALOG_REPORT_Result>();
                    Famattribresults(catalogId, familyattrib);
                    return familyattrib;
                }
                if (option == "Family and Products not selected in Catalog Report")
                {

                    var familyprod = new List<STP_REPORT_FAMILY_AND_PRODUCTS_NOT_SELECTED_IN_CATALOG_REPORT_Result>();
                    Famprodresults(catalogId, familyprod);
                    return familyprod;
                }
                if (option == "Family Created in Last 7 days Report")
                {
                    var famcreated = new List<STP_REPORT_FAMILY_CREATED_IN_LAST_7_DAYS_REPORT_Result>();
                    Famcreatedresults(catalogId, famcreated);
                    return famcreated;
                }
                if (option == "Family Modified in Last 7 days Report")
                {
                    var fammodified = new List<STP_REPORT_FAMILY_MODIFIED_IN_LAST_7_DAYS_REPORT_Result>();
                    Fammodifiedresults(catalogId, fammodified);
                    return fammodified;
                }

                if (option == "No Image Families Report")
                {
                    var imgmodified = new List<STP_REPORT_NO_IMAGE_FAMILIES_REPORT_Result>();
                    Imgresults(catalogId, imgmodified);
                    return imgmodified;
                }
                if (option == "Product Attribute Association with catalog Report")
                {
                    var prodattrib = new List<STP_REPORT_PRODUCT_ATTRIBUTE_ASSOCIATION_WITH_CATALOG_REPORT_Result>();
                    Prodattribresults(catalogId, prodattrib);
                    return prodattrib;
                }
                if (option == "Products with multiple categories and catalog Report")
                {
                    var prodmulticatoptions = new List<STP_REPORT_PRODUCTS_WITH_MULTIPLE_CATEGORIES_AND_CATALOG_REPORT_Result>();
                    Prodmulticatoptionsresults(catalogId, prodmulticatoptions);
                    return prodmulticatoptions;
                }
                if (option == "Products Created in Last 7 days Report")
                {
                    var prodcreated = new List<STP_REPORT_PRODUCT_CREATED_IN_LAST_7_DAYS_REPORT_Result>();
                    Prodcreatedresults(catalogId, prodcreated);
                    return prodcreated;
                }
                if (option == "Products Modified in Last 7 days Report")
                {
                    var prodmodifyoptions = new List<STP_REPORT_PRODUCT_MODIFIED_IN_LAST_7_DAYS_REPORT_Result>();
                    Prodmodifiedresults(catalogId, prodmodifyoptions);
                    return prodmodifyoptions;
                }
                if (option == "Total products Count Report")
                {
                    //STP_REPORT_TOTAL PRODUCTS COUNT REPORT
                    // var prodcountoptions = new List<STP_REPORT_TOTAL_PRODUCTS_COUNT_REPORT__Result>();
                    return Prodcountoptionsresults(catalogId);
                    //return prodcountoptions;
                }
                if (option == "Unpublished product Report")
                {
                    var unpublishedoptions = new List<STP_REPORT_UNPUBLISHED_PRODUCT_REPORT_Result>();
                    Unpublishedoptionsresults(catalogId, unpublishedoptions);
                    return unpublishedoptions;
                }
                if (option == "Product that has Unpublished Columns and Values Report")
                {
                    var unpublishprodoptions = new List<STP_REPORT_PRODUCT_THAT_HAS_UNPUBLISHED_COLUMNS_AND_VALUES_REPORT_Result>();
                    Unpublishprodoptionsresults(catalogId, unpublishprodoptions);
                    return unpublishprodoptions;
                }
                if (option == "All Attributes Report")
                {
                    var unpublishprodoptions = new List<STP_REPORT_ALL_ATTRIBUTES_REPORT_Result>();
                    Allattribresults(catalogId, unpublishprodoptions);
                    return unpublishprodoptions;
                }
                if (option == "All Products with No price Report")
                {
                    var unpublishprodoptions = new List<STP_REPORT_ALL_PRODUCTS_WITH_NO_PRICE_REPORT_Result>();
                    Allprodnopriceresults(catalogId, unpublishprodoptions);
                    return unpublishprodoptions;
                }
                if (option == "All Products with No Attributes Report")
                {
                    var unpublishprodoptions = new List<STP_REPORT_ALL_PRODUCTS_WITH_NO_ATTRIBUTES_REPORT_Result>();
                    Allprodnoattribresults(catalogId, unpublishprodoptions);
                    return unpublishprodoptions;
                }
                if (option == "Catalog Attribute Association Report")
                {
                    var unpublishprodoptions = new List<STP_REPORT_CATALOG_ATTRIBUTE_ASSOCIATION_REPORT_Result>();
                    Catalogattribresults(catalogId, unpublishprodoptions);
                    return unpublishprodoptions;
                }
                if (option == "Duplicate Catalog Item Number Report")
                {
                    var unpublishprodoptions = new List<STP_REPORT_DUPLICATE_CATALOG_ITEM_NUMBER_REPORT_Result>();
                    Duplicateitemresults(catalogId, unpublishprodoptions);
                    return unpublishprodoptions;
                }
                if (option == "Report modified products")
                {
                    var modifiedproductsoption = new List<MODIFIED_LIST>();
                    ReportModifiedproductsresults(catalogId, startDateModify, endDateModify, modifiedproductsoption);
                    return modifiedproductsoption;

                }
                if (option == "Report new products")
                {
                    var newproductsoption = new List<NEW_PRODUCTS>();
                    Newlycreatedproducts(catalogId, startDatenew, endDatenew, newproductsoption);
                    return newproductsoption;

                }
                if (option == "Duplicate image list")
                {
                    var item = new List<duplicateimage>();
                    item = Duplicateimage_lists(fileNames, item);
                    return item;
                }
                if (option == "DisContinued Products")
                {
                    var deleted_products = new List<discontinuedproducts>();
                    deleted_products = DisContinued_Products(start_Date, end_date, catalogId);
                    return deleted_products;

                }


                return null;
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at HomeApiController : GetALLReports", objexception);
                return null;
            }
        }
        public List<StpReportImageTypeReportsResult> Reportresultsoptiontwo()
        {
            try
            {
                //();
                List<string> allobjCategoryResults = _dbcontext.STP_REPORT_IMAGE_TYPE_REPORTS().ToList();
                var objStpReportImageTypeReportsResult = new StpReportImageTypeReportsResult();
                objStpReportImageTypeReportsResult.ObjectType = allobjCategoryResults[0];
                var objreports = new List<StpReportImageTypeReportsResult>();
                objreports.Add(objStpReportImageTypeReportsResult);
                return objreports;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Reportresultsoptiontwo", exception);
                var objrecentlymodfieddetails = new List<StpReportImageTypeReportsResult>();
                return objrecentlymodfieddetails;
            }
        }
        public List<STP_REPORT_DUPLICATE_CATALOG_ITEM_NUMBER_REPORT_Result> Duplicateitemresults(int catalogId, List<STP_REPORT_DUPLICATE_CATALOG_ITEM_NUMBER_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_DUPLICATE_CATALOG_ITEM_NUMBER_REPORT(catalogId);

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Duplicateitemresults", exception);
                var objrecentlymodfieddetails = new List<STP_REPORT_DUPLICATE_CATALOG_ITEM_NUMBER_REPORT_Result>();
                return objrecentlymodfieddetails;
            }
        }
        public List<STP_REPORT_CATALOG_ATTRIBUTE_ASSOCIATION_REPORT_Result> Catalogattribresults(int catalogId, List<STP_REPORT_CATALOG_ATTRIBUTE_ASSOCIATION_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_CATALOG_ATTRIBUTE_ASSOCIATION_REPORT();

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Catalogattribresults", exception);
                var objrecentlymodfieddetails = new List<STP_REPORT_CATALOG_ATTRIBUTE_ASSOCIATION_REPORT_Result>();
                return objrecentlymodfieddetails;
            }
        }
        public List<STP_REPORT_ALL_PRODUCTS_WITH_NO_ATTRIBUTES_REPORT_Result> Allprodnoattribresults(int catalogId, List<STP_REPORT_ALL_PRODUCTS_WITH_NO_ATTRIBUTES_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_ALL_PRODUCTS_WITH_NO_ATTRIBUTES_REPORT();

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Allprodnoattribresults", exception);
                var objrecentlymodfieddetails = new List<STP_REPORT_ALL_PRODUCTS_WITH_NO_ATTRIBUTES_REPORT_Result>();
                return objrecentlymodfieddetails;
            }
        }
        public List<STP_REPORT_ALL_PRODUCTS_WITH_NO_PRICE_REPORT_Result> Allprodnopriceresults(int catalogId, List<STP_REPORT_ALL_PRODUCTS_WITH_NO_PRICE_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_ALL_PRODUCTS_WITH_NO_PRICE_REPORT();

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Allprodnopriceresults", exception);
                var objreports = new List<STP_REPORT_ALL_PRODUCTS_WITH_NO_PRICE_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_ALL_ATTRIBUTES_REPORT_Result> Allattribresults(int catalogId, List<STP_REPORT_ALL_ATTRIBUTES_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_ALL_ATTRIBUTES_REPORT();

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Allattribresults", exception);
                var objreports = new List<STP_REPORT_ALL_ATTRIBUTES_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_PRODUCT_THAT_HAS_UNPUBLISHED_COLUMNS_AND_VALUES_REPORT_Result> Unpublishprodoptionsresults(int catalogId, List<STP_REPORT_PRODUCT_THAT_HAS_UNPUBLISHED_COLUMNS_AND_VALUES_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_PRODUCT_THAT_HAS_UNPUBLISHED_COLUMNS_AND_VALUES_REPORT(catalogId);

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Unpublishprodoptionsresults", exception);
                var objreports = new List<STP_REPORT_PRODUCT_THAT_HAS_UNPUBLISHED_COLUMNS_AND_VALUES_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_UNPUBLISHED_PRODUCT_REPORT_Result> Unpublishedoptionsresults(int catalogId, List<STP_REPORT_UNPUBLISHED_PRODUCT_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_UNPUBLISHED_PRODUCT_REPORT(catalogId);

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Unpublishedoptionsresults", exception);
                var objreports = new List<STP_REPORT_UNPUBLISHED_PRODUCT_REPORT_Result>();
                return objreports;
            }
        }
        public List<StpReportProductCountResult> Prodcountoptionsresults(int catalogId)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_TOTAL_PRODUCTS_COUNT_REPORT(catalogId).ToList();
                var objStpReportImageTypeReportsResult = new StpReportProductCountResult();
                objStpReportImageTypeReportsResult.PRODUCT_COUNT = allobjCategoryResults[0];
                var objreports = new List<StpReportProductCountResult>();
                objreports.Add(objStpReportImageTypeReportsResult);
                return objreports;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Prodcountoptionsresults", exception);
                var objreports = new List<StpReportProductCountResult>();
                return objreports;
            }
        }
        public List<STP_REPORT_PRODUCT_MODIFIED_IN_LAST_7_DAYS_REPORT_Result> Prodmodifiedresults(int catalogId, List<STP_REPORT_PRODUCT_MODIFIED_IN_LAST_7_DAYS_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_PRODUCT_MODIFIED_IN_LAST_7_DAYS_REPORT(catalogId);

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Prodmodifiedresults", exception);
                var objreports = new List<STP_REPORT_PRODUCT_MODIFIED_IN_LAST_7_DAYS_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_PRODUCT_CREATED_IN_LAST_7_DAYS_REPORT_Result> Prodcreatedresults(int catalogId, List<STP_REPORT_PRODUCT_CREATED_IN_LAST_7_DAYS_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_PRODUCT_CREATED_IN_LAST_7_DAYS_REPORT(catalogId);

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Prodcreatedresults", exception);
                var objreports = new List<STP_REPORT_PRODUCT_CREATED_IN_LAST_7_DAYS_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_PRODUCTS_WITH_MULTIPLE_CATEGORIES_AND_CATALOG_REPORT_Result> Prodmulticatoptionsresults(int catalogId, List<STP_REPORT_PRODUCTS_WITH_MULTIPLE_CATEGORIES_AND_CATALOG_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_PRODUCTS_WITH_MULTIPLE_CATEGORIES_AND_CATALOG_REPORT();

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Prodmulticatoptionsresults", exception);
                var objreports = new List<STP_REPORT_PRODUCTS_WITH_MULTIPLE_CATEGORIES_AND_CATALOG_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_PRODUCT_ATTRIBUTE_ASSOCIATION_WITH_CATALOG_REPORT_Result> Prodattribresults(int catalogId, List<STP_REPORT_PRODUCT_ATTRIBUTE_ASSOCIATION_WITH_CATALOG_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_PRODUCT_ATTRIBUTE_ASSOCIATION_WITH_CATALOG_REPORT(catalogId);

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Prodattribresults", exception);
                var objreports = new List<STP_REPORT_PRODUCT_ATTRIBUTE_ASSOCIATION_WITH_CATALOG_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_NO_IMAGE_FAMILIES_REPORT_Result> Imgresults(int catalogId, List<STP_REPORT_NO_IMAGE_FAMILIES_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_NO_IMAGE_FAMILIES_REPORT();

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Imgresults", exception);
                var objreports = new List<STP_REPORT_NO_IMAGE_FAMILIES_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_FAMILY_MODIFIED_IN_LAST_7_DAYS_REPORT_Result> Fammodifiedresults(int catalogId, List<STP_REPORT_FAMILY_MODIFIED_IN_LAST_7_DAYS_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_FAMILY_MODIFIED_IN_LAST_7_DAYS_REPORT(catalogId);

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Fammodifiedresults", exception);
                var objreports = new List<STP_REPORT_FAMILY_MODIFIED_IN_LAST_7_DAYS_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_FAMILY_CREATED_IN_LAST_7_DAYS_REPORT_Result> Famcreatedresults(int catalogId, List<STP_REPORT_FAMILY_CREATED_IN_LAST_7_DAYS_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_FAMILY_CREATED_IN_LAST_7_DAYS_REPORT(catalogId);

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Famcreatedresults", exception);
                var objreports = new List<STP_REPORT_FAMILY_CREATED_IN_LAST_7_DAYS_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_FAMILY_AND_PRODUCTS_NOT_SELECTED_IN_CATALOG_REPORT_Result> Famprodresults(int catalogId, List<STP_REPORT_FAMILY_AND_PRODUCTS_NOT_SELECTED_IN_CATALOG_REPORT_Result> objCategoryResults)
        {
            try
            {
                var customerId = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId.ToString()).FirstOrDefault();
                var allobjCategoryResults = _dbcontext.STP_REPORT_FAMILY_AND_PRODUCTS_NOT_SELECTED_IN_CATALOG_REPORT(catalogId, Convert.ToInt16(customerId));

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Famprodresults", exception);
                var objreports = new List<STP_REPORT_FAMILY_AND_PRODUCTS_NOT_SELECTED_IN_CATALOG_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_FAMILY_ATTRIBUTE_ASSOCIATION_WITH_CATALOG_REPORT_Result> Famattribresults(int catalogId, List<STP_REPORT_FAMILY_ATTRIBUTE_ASSOCIATION_WITH_CATALOG_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_FAMILY_ATTRIBUTE_ASSOCIATION_WITH_CATALOG_REPORT(catalogId);

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Famattribresults", exception);
                var objreports = new List<STP_REPORT_FAMILY_ATTRIBUTE_ASSOCIATION_WITH_CATALOG_REPORT_Result>();
                return objreports;
            }
        }



        public partial class STP_REPORT_VIRTUAL_PAGES_REPORT_Result_DATA
        {
            public int CATALOG_ID { get; set; }
            public string CATEGORY_ID { get; set; }
            public int PROJECT_ID { get; set; }
            public string PROJECT_NAME { get; set; }
            public string COMMENTS { get; set; }
            public int RECORD_ID { get; set; }
            public int FILE_NO { get; set; }
            public int FAMILY_ID { get; set; }
            public Nullable<int> SORT_ORDER { get; set; }
            public string PAGE_FILE_NAME { get; set; }
            public string PAGE_FILE_PATH { get; set; }
            public string PAGE_FILE_TYPE { get; set; }
            public string NOTES { get; set; }
            public string XML_FILE_NAME { get; set; }
        }



        public List<STP_REPORT_VIRTUAL_PAGES_REPORT_Result_DATA> Virtualresults(int catalogId, List<STP_REPORT_VIRTUAL_PAGES_REPORT_Result_DATA> objCategoryResults)
        {
            try
            {
                DataTable dt = new DataTable();


                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_REPORT_VIRTUAL_PAGES_REPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;

                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(dt);
                }

                // var allobjCategoryResults = _dbcontext.STP_REPORT_VIRTUAL_PAGES_REPORT(catalogId);

                IList<STP_REPORT_VIRTUAL_PAGES_REPORT_Result_DATA> items = dt.AsEnumerable().Select(row =>
                    new STP_REPORT_VIRTUAL_PAGES_REPORT_Result_DATA
                    {
                        CATALOG_ID = row.Field<int>("CATALOG_ID"),

                        CATEGORY_ID = row.Field<string>("CATEGORY_ID"),
                        PROJECT_ID = row.Field<int>("PROJECT_ID"),

                        PROJECT_NAME = row.Field<string>("PROJECT_NAME"),
                        COMMENTS = row.Field<string>("COMMENTS"),

                        RECORD_ID = row.Field<int>("RECORD_ID"),
                        FILE_NO = row.Field<int>("FILE_NO"),

                        FAMILY_ID = row.Field<int>("FAMILY_ID"),
                        SORT_ORDER = row.Field<int>("SORT_ORDER"),

                        PAGE_FILE_NAME = row.Field<string>("PAGE_FILE_NAME"),

                        PAGE_FILE_PATH = row.Field<string>("PAGE_FILE_PATH"),
                        PAGE_FILE_TYPE = row.Field<string>("PAGE_FILE_TYPE"),

                        NOTES = row.Field<string>("NOTES"),
                        XML_FILE_NAME = row.Field<string>("XML_FILE_NAME"),

                    }
                    ).ToList();


                objCategoryResults.InsertRange(0, items);



                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Virtualresults", exception);
                var objreports = new List<STP_REPORT_VIRTUAL_PAGES_REPORT_Result_DATA>();
                return objreports;
            }
        }
        public List<STP_REPORT_SUPPLIER_LIST_REPORT_Result> Supplierresults(int catalogId, List<STP_REPORT_SUPPLIER_LIST_REPORT_Result> objCategoryResults)
        {
            try
            {
                var allobjCategoryResults = _dbcontext.STP_REPORT_SUPPLIER_LIST_REPORT(catalogId);

                objCategoryResults.InsertRange(0, allobjCategoryResults);
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Supplierresults", exception);
                var objreports = new List<STP_REPORT_SUPPLIER_LIST_REPORT_Result>();
                return objreports;
            }
        }
        public List<STP_REPORT_Category_Listfrom_MasterCatalog_Result> Reportresults(int catalogId, List<STP_REPORT_Category_Listfrom_MasterCatalog_Result> objCategoryResults)
        {
            try
            {
                var customerId = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId.ToString()).FirstOrDefault();
                var allobjCategoryResults = _dbcontext.STP_REPORT_Category_Listfrom_MasterCatalog(catalogId, Convert.ToInt16(customerId));

                objCategoryResults.InsertRange(0, allobjCategoryResults.Join(_dbcontext.Customer_Catalog, allObj => allObj.CATALOG_ID, cc => cc.CATALOG_ID, (allObj, cc) => new { allObj, cc }).Where(x => x.cc.CustomerId == int.Parse(customerId)).Select(y => y.allObj));
                return objCategoryResults;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Reportresults", exception);
                var objreports = new List<STP_REPORT_Category_Listfrom_MasterCatalog_Result>();
                return objreports;
            }
        }
        public partial class MODIFIED_LIST
        {
            public string ITEM_NUMBER { get; set; }
            public int PRODUCT_ID { get; set; }
            public int FAMILY_ID { get; set; }
            public string FAMILY_NAME { get; set; }
            public string CATEGORY_ID { get; set; }
            public string CATEGORY_NAME { get; set; }
            public DateTime MODIFIED_DATE { get; set; }
            public string MODIFIED_USER { get; set; }
            public string CREATED_DATE { get; set; }



        }

        [System.Web.Http.HttpGet]
        public List<MODIFIED_LIST> ReportModifiedproductsresults(int catalogId, DateTime startDateModify, DateTime endDateModify, List<MODIFIED_LIST> objCategoryResults)
        {


            try
            {
                DataTable dt = new DataTable();
                // IList<MODIFIED_LIST> items = new List<MODIFIED_LIST>();


                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    // public static void ReportModifiedproductscountresults();
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_REPORT_RECENTLY_MODIFIED_PRODUCTS";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    objSqlCommand.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDateModify;
                    objSqlCommand.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDateModify;
                  
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(dt);

                }
                //foreach (DataRow val in dt.Rows)
                //{
                //    if (val["MODIFIED_DATE"].ToString()!=(val["CREATED_DATE"].ToString()))
                //    {
                //items = dt.AsEnumerable().Where(x => x.Field<DateTime>("MODIFIED_DATE")!=x.Field<DateTime>("CREATED_DATE")).Select(row =>
                //           new MODIFIED_LIST
                //           {
                //               ITEM_NUMBER = row.Field<string>("ITEM#"),
                //               MODIFIED_DATE = row.Field<DateTime>("MODIFIED_DATE"),
                //               MODIFIED_USER = row.Field<string>("MODIFIED_USER"),

                //           }
                //           ).ToList();
                IList<MODIFIED_LIST> items = dt.AsEnumerable().Select(row =>
                           new MODIFIED_LIST
                           {
                               ITEM_NUMBER = row.Field<string>("ITEM#"),
                               PRODUCT_ID = row.Field<int>("PRODUCT_ID"),
                               FAMILY_ID = row.Field<int>("FAMILY_ID"),
                               FAMILY_NAME = row.Field<string>("FAMILY_NAME"),
                               CATEGORY_ID = row.Field<string>("CATEGORY_ID"),
                               CATEGORY_NAME = row.Field<string>("CATEGORY_NAME"),
                               MODIFIED_DATE = row.Field<DateTime>("MODIFIED_DATE"),
                               MODIFIED_USER = row.Field<string>("MODIFIED_USER"),


                           }
                           ).ToList();
                //    }
                //}


                objCategoryResults.InsertRange(0, items);
                return objCategoryResults;


            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : ReportModifiedproductsresults", exception);
                var objreports = new List<MODIFIED_LIST>();
                return null;
            }
        }


        public partial class MODIFIED_COUNT
        {
            public int total_product { get; set; }
        }
        [System.Web.Http.HttpGet]
        public int ReportModifiedproductscountresults(int catalogId, DateTime startDateModify, DateTime endDateModify, List<MODIFIED_COUNT> objCategoryResults)
        {

            try
            {
                DataTable dt = new DataTable();


                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {

                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_recently_modifie_count_reports";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    objSqlCommand.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDateModify;
                    objSqlCommand.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDateModify;
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(dt);


                }
                string myNum = dt.Rows[0][0].ToString();
                int count = Convert.ToInt32(myNum);
                return count;


            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : ReportModifiedproductsresults", exception);
                var objreports = new List<MODIFIED_LIST>();
                return 0;
            }
        }

        public partial class NEW_PRODUCTS
        {
            public string ITEM_NUMBER { get; set; }
            public int PRODUCT_ID { get; set; }
            public int FAMILY_ID { get; set; }
            public string FAMILY_NAME { get; set; }
            public string CATEGORY_ID { get; set; }
            public string CATEGORY_NAME { get; set; }
            public DateTime CREATED_DATE { get; set; }
            public string CREATED_USER { get; set; }


        }


        public List<NEW_PRODUCTS> Newlycreatedproducts(int catalogId, DateTime startDatenew, DateTime endDatenew,  List<NEW_PRODUCTS> objCategoryResults)
        {
            try
            {
                DataTable dt = new DataTable();


                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_REPORT_NEWLY_CREATED_PRODUCTS";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    objSqlCommand.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDatenew;
                    objSqlCommand.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDatenew;
                    objSqlConnection.Open();
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(dt);
                }
                IList<NEW_PRODUCTS> items = dt.AsEnumerable().Select(row =>
                   new NEW_PRODUCTS
                   {
                       //CATALOG_ID = row.("CATALOG_ID"),
                       ITEM_NUMBER = row.Field<string>("ITEM#"),
                       PRODUCT_ID = row.Field<int>("PRODUCT_ID"),
                       FAMILY_ID = row.Field<int>("FAMILY_ID"),
                       FAMILY_NAME = row.Field<string>("FAMILY_NAME"),
                       CATEGORY_ID = row.Field<string>("CATEGORY_ID"),
                       CATEGORY_NAME = row.Field<string>("CATEGORY_NAME"),
                       CREATED_DATE = row.Field<DateTime>("CREATED_DATE"),
                       CREATED_USER = row.Field<string>("CREATED_USER"),
                   }
                   ).ToList();


                objCategoryResults.InsertRange(0, items);
                return objCategoryResults;


            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : ReportModifiedproductsresults", exception);
                var objreports = new List<NEW_PRODUCTS>();
                return null;
            }
        }

        //=========new products count============================//
        public partial class NEW_COUNT
        {
            public int total_product { get; set; }
        }
        [System.Web.Http.HttpGet]
        public int Reportnewproductscountresults(int catalogId, DateTime startDatenew, DateTime endDatenew, List<NEW_COUNT> objCategoryResults)
        {

            try
            {
                DataTable dt = new DataTable();


                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {

                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_NEW_PRODUCT_COUNT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    objSqlCommand.Parameters.Add("@startDate", SqlDbType.DateTime).Value = startDatenew;
                    objSqlCommand.Parameters.Add("@endDate", SqlDbType.DateTime).Value = endDatenew;
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(dt);


                }
                string myNum = dt.Rows[0][0].ToString();
                int count = Convert.ToInt32(myNum);
                return count;


            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : ReportModifiedproductsresults", exception);
                var objreports = new List<NEW_COUNT>();
                return 0;
            }
        }
        public partial class duplicateimage
        {
            public string previewPath { get; set; }
            public string fileNames { get; set; }
            public string Name { get; set; }
            public string path { get; set; }
            public string dimension { get; set; }
            public string size { get; set; }

        }
        public List<duplicateimage> Duplicateimage_lists(string fileNames, List<duplicateimage> objCategoryResults)
        {
            try
            {
                int user_id = 0;
                var userdet = _dbcontext.Customer_User.FirstOrDefault(a => a.User_Name.ToLower() == User.Identity.Name.ToLower());
                if (userdet != null)
                {
                    user_id = userdet.CustomerId;
                }

                //get username
                var UserName = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.a.CustomerId == user_id).Select(x => x.b.User_Name).FirstOrDefault();
                if (string.IsNullOrEmpty(UserName))
                {
                    UserName = "questudio";
                }
                else
                {
                    UserName = UserName.Replace("&", "").Replace(" ", "");
                }
                //get customer folder
                var CustomerFolder = _dbcontext.Customers.Join(_dbcontext.Customer_User, a => a.CustomerId, b => b.CustomerId, (a, b) => new { a, b }).Where(z => z.b.User_Name == UserName).Select(x => x.a.Comments).FirstOrDefault();
                if (string.IsNullOrEmpty(CustomerFolder))
                { CustomerFolder = "/STUDIOSOFT"; }
                else
                { CustomerFolder = "/" + CustomerFolder.Replace("&", ""); }


                // To pass dynamic path location from web config   - Start
                string IsServerUpload = WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
                string pathDesignation = WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
                string pathUrl = WebConfigurationManager.AppSettings["ExternalAssetDriveURL"].ToString();
                // string AssetPath = WebConfigurationManager.AppSettings["AssetmanagementHyperLinkEmailPath"].ToString();

                //declare 
                string destinationPath;
                string generatedUrl;
                string RootDirectory;
                string val = string.Empty;


                //checking external asset drive is true or false
                if (IsServerUpload.ToUpper() == "TRUE")
                {
                    destinationPath = pathDesignation + "//Content//ProductImages" + CustomerFolder;
                    pathUrl = pathUrl.Replace("\\", "//");
                    generatedUrl = pathUrl + "Content/ProductImages";
                    RootDirectory = destinationPath;
                    goto GetImage;
                }
                else
                {
                    destinationPath = HttpContext.Current.Server.MapPath("~/Content/ProductImages/" + CustomerFolder);
                    generatedUrl = "..//Content//ProductImages//";
                    RootDirectory = destinationPath;
                    goto GetImage;
                }


                //getting duplicate images from directories
            GetImage:
                List<duplicateimage> items = new List<duplicateimage>();
                DirectoryInfo directories = new DirectoryInfo(RootDirectory);

                string[] Image_Path = Directory.GetFiles(RootDirectory);
                string[] Sub_directories = Directory.GetDirectories(RootDirectory);

                if (Image_Path.Length > 0)
                {
                    foreach (string Images in Image_Path)
                    {
                        string Duplicate_Images = GetFolder_image(Images);

                        if (Duplicate_Images.Contains(".jpg") || Duplicate_Images.Contains(".psd") || Duplicate_Images.Contains(".bmp") ||
                            Duplicate_Images.Contains(".jpeg") || Duplicate_Images.Contains(".svg") || Duplicate_Images.Contains(".png"))
                        {
                            FileInfo[] AllTypeImages = directories.GetFiles(Duplicate_Images, SearchOption.AllDirectories);
                            if (AllTypeImages.Length > 2)
                            {
                                if (Images.Contains(".jpg") || Images.Contains(".psd") || Images.Contains(".bmp") ||
                                        Images.Contains(".jpeg") || Images.Contains(".svg") || Images.Contains(".png"))
                                {

                                    //Image width hieght calculation
                                    System.Drawing.Image img = System.Drawing.Image.FromFile(Images);
                                    int str = img.Width;
                                    int innt = img.Height;

                                    //get size of the image in KB
                                    FileInfo fi = new FileInfo(Images);
                                    double fileSizeKB = fi.Length / 1024;

                                    //splitting the path
                                    String[] spearator = { "Content" };
                                    String[] strlist = Images.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                                    foreach (string pathlist in strlist)
                                    {
                                        if (pathlist == strlist[1])
                                        {
                                            val = pathlist;
                                        }

                                    }


                                    //string folder_path = Images.Replace(Images, "..");
                                    string ImagePath = "//Content" + val.Replace(Duplicate_Images, "..");
                                    string imagePreview = "\\Content" + val;
                                    string imgs = ImagePath + Duplicate_Images;
                                    //adding values to list
                                    items.Add(new duplicateimage()
                                    {
                                        Name = Duplicate_Images,
                                        path = ImagePath,
                                        previewPath = imagePreview,
                                        dimension = str + " X " + innt,
                                        size = fileSizeKB + " KB"
                                    });
                                }
                            }
                        }

                    }
                }
                if (Sub_directories.Length > 0)
                {
                    //getting sub folders 
                    foreach (string sub_folder in Sub_directories)
                    {
                        if (sub_folder.Contains("\\Temp")) { }
                        else
                        {
                            string[] get_image = Directory.GetFiles(sub_folder);
                            string[] Folders = Directory.GetDirectories(sub_folder);

                            if (get_image.Length > 0)
                            {
                                foreach (string imgz in get_image)
                                {
                                    string Imagez = GetFolder_image(imgz);

                                    if (Imagez.Contains(".jpg") || Imagez.Contains(".psd") || Imagez.Contains(".bmp") ||
                                               Imagez.Contains(".jpeg") || Imagez.Contains(".svg") || Imagez.Contains(".png"))
                                    {
                                        FileInfo[] AllTypeImages = directories.GetFiles(Imagez, SearchOption.AllDirectories);
                                        if (AllTypeImages.Length > 2)
                                        {
                                            if (imgz.Contains(".jpg") || imgz.Contains(".psd") || imgz.Contains(".bmp") ||
                                                   imgz.Contains(".jpeg") || imgz.Contains(".svg") || imgz.Contains(".png"))
                                            {
                                                System.Drawing.Image img = System.Drawing.Image.FromFile(imgz);
                                                int str = img.Width;
                                                int innt = img.Height;

                                                FileInfo fi = new FileInfo(imgz);
                                                double fileSizeKB = fi.Length / 1024;

                                                //splitting the path
                                                String[] spearator = { "Content" };
                                                String[] strlist = imgz.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                                                foreach (string pathlist in strlist)
                                                {
                                                    if (pathlist == strlist[1])
                                                    {
                                                        val = pathlist;
                                                    }

                                                }

                                                ///string folder_path = imgz.Replace(imgz, "..");
                                                string ImagePath = "//Content" + val.Replace(Imagez, "..");
                                                string imagePreview = "\\Content" + val;
                                                items.Add(new duplicateimage()
                                                {
                                                    Name = Imagez,
                                                    path = ImagePath,
                                                    previewPath = imagePreview,
                                                    dimension = str + " X " + innt,
                                                    size = fileSizeKB + " KB"

                                                });
                                            }
                                        }
                                    }
                                }
                            }

                            if (Folders.Length > 0)
                            {
                                foreach (string sub_folders in Folders)
                                {
                                    if (sub_folder.Contains("\\Temp")) { }
                                    else
                                    {
                                        string Duplicates = sub_folders;
                                    subCheck:
                                        string[] Get_images = Directory.GetFiles(Duplicates);
                                        string[] Get_directories = Directory.GetDirectories(Duplicates);

                                        if (Get_images.Length > 0)
                                        {
                                            foreach (string Image_directory in Get_images)
                                            {
                                                string Duplicate_Images1 = GetFolder_image(Image_directory);

                                                if (Duplicate_Images1.Contains(".jpg") || Duplicate_Images1.Contains(".psd") || Duplicate_Images1.Contains(".bmp") ||
                                                    Duplicate_Images1.Contains(".jpeg") || Duplicate_Images1.Contains(".svg") || Duplicate_Images1.Contains(".png"))
                                                {
                                                    FileInfo[] AllTypeImages = directories.GetFiles(Duplicate_Images1, SearchOption.AllDirectories);
                                                    if (AllTypeImages.Length > 2)
                                                    {
                                                        if (Image_directory.Contains(".jpg") || Image_directory.Contains(".psd") || Image_directory.Contains(".bmp") ||
                                                        Image_directory.Contains(".jpeg") || Image_directory.Contains(".svg") || Image_directory.Contains(".png"))
                                                        {
                                                            System.Drawing.Image img = System.Drawing.Image.FromFile(Image_directory);
                                                            int str = img.Width;
                                                            int innt = img.Height;

                                                            FileInfo fi = new FileInfo(Image_directory);
                                                            double fileSizeKB = fi.Length / 1024;

                                                            //splitting the path
                                                            String[] spearator = { "Content" };
                                                            String[] strlist = Image_directory.Split(spearator, StringSplitOptions.RemoveEmptyEntries);
                                                            foreach (string pathlist in strlist)
                                                            {
                                                                if (pathlist == strlist[1])
                                                                {
                                                                    val = pathlist;
                                                                }

                                                            }

                                                            //string folder_path = Image_directory.Replace(Image_directory, "..");
                                                            string ImagePath = "//Content" + val.Replace(Duplicate_Images1, "..");
                                                            string imagePreview = "\\Content" + val;
                                                            items.Add(new duplicateimage()
                                                            {
                                                                Name = Duplicate_Images1,
                                                                path = ImagePath,
                                                                previewPath = imagePreview,
                                                                dimension = str + " X " + innt,
                                                                size = fileSizeKB + " KB"

                                                            });
                                                        }
                                                    }
                                                }

                                            }
                                        }
                                        if (Get_directories.Length > 0)
                                        {
                                            foreach (string filez in Get_directories)
                                            {
                                                Duplicates = filez;
                                                goto subCheck;
                                            }
                                        }
                                    }

                                }

                            }
                        }


                    }
                }
                var newList = items.OrderBy(x => x.Name).ToList();
                return newList;

            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : DuplicateImageList", exception);
                return null;
            }
        }
        public string GetFolder_image(string Images)
        {
            string Get_Image = Path.GetFileName(Images);
            return Get_Image;
        }


        public partial class discontinuedproducts
        {

            public int product_id { get; set; }
            public string product_name { get; set; }                       
            public DateTime Deleted_Date { get; set; }
            public string Deleted_User { get; set; }

        }
        public List<discontinuedproducts> DisContinued_Products(DateTime start_Date, DateTime end_Date, int catalogId)
        {
            try
            {
                DataTable dt = new DataTable();
                List<discontinuedproducts> deletedproducts = new List<discontinuedproducts>();

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    SqlCommand _sqlCommand = objSqlConnection.CreateCommand();
                    _sqlCommand.Connection = objSqlConnection;
                    _sqlCommand.CommandText = "STP_DISCONTINUED_PRODUCTS";
                    _sqlCommand.CommandType = CommandType.StoredProcedure;
                    _sqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int).Value = catalogId;
                    _sqlCommand.Parameters.Add("@FROM_DATE", SqlDbType.DateTime).Value = start_Date;
                    _sqlCommand.Parameters.Add("@TO_DATE", SqlDbType.DateTime).Value = end_Date;

                    SqlDataAdapter _sqlDataAdapter = new SqlDataAdapter(_sqlCommand);
                    _sqlDataAdapter.Fill(dt);

                    deletedproducts = new List<discontinuedproducts>();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        discontinuedproducts DisContinued_Products = new discontinuedproducts();
                        DisContinued_Products.product_id = Convert.ToInt32(dt.Rows[i]["product_id"]);                        
                        DisContinued_Products.product_name = dt.Rows[i]["product_name"].ToString();                       
                        DisContinued_Products.Deleted_Date = Convert.ToDateTime(dt.Rows[i]["Deleted_Date"]);
                        DisContinued_Products.Deleted_User = dt.Rows[i]["Deleted_User"].ToString();
                        deletedproducts.Add(DisContinued_Products);
                    }


                }

                return deletedproducts;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : Discontinued Products", exception);
                return null;
            }
        }
        [System.Web.Http.HttpPost]
        public DataTable GetNullvalueAttributes(string selectedFamilyId, int pageno, int ProdCountPerPage)
        {
            try
            {
                DataTable dt = new DataTable();
                var model = new DashBoardModel();

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {

                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_NULLVALUE_ATTRIBUTES";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@family_id", SqlDbType.Int).Value = selectedFamilyId;
                    objSqlCommand.Parameters.Add("@PAGENO", SqlDbType.Int).Value = pageno;
                    objSqlCommand.Parameters.Add("@PERPAGECOUNT", SqlDbType.Int).Value = ProdCountPerPage;

                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(dt);


                    return dt;
                }

            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController :GetNullvalueAttributes", exception);
                return null;
            }
        }

        [System.Web.Http.HttpPost]
        public Dictionary<string, string> GetNullvalueAttributesForSearch(int selectedFamilyId, int getProduct_id, int SelectedCatalogId)
        {
            try
            {
                DataTable dt = new DataTable();
                var model = new DashBoardModel();





                //string created_user = _dbcontext.TB_CATALOG_PRODUCT.Join(_dbcontext.TB_PROD_FAMILY, tcp => tcp.PRODUCT_ID, tpf => tpf.PRODUCT_ID, (tcp, tpf) => new { tcp, tpf })
                //    .Join(_dbcontext.TB_PROD_SPECS, tps => tps.tcp.PRODUCT_ID, tpps => tpps.PRODUCT_ID, (tps, tpps) => new { tps, tpps })
                //    .Where(tps1 => tps1.tps.tcp.CATALOG_ID == SelectedCatalogId && tps1.tps.tpf.FAMILY_ID == selectedFamilyId && tps1.tps.tpf.PRODUCT_ID == getProduct_id)
                //    .Select(a => a.tpps.CREATED_USER).FirstOrDefault();
                string created_user = User.Identity.Name; 
                var id = _dbcontext.aspnet_Users.Where(a => a.UserName == created_user).Select(b => b.UserId).FirstOrDefault();
                var Guid_role_id = _dbcontext.vw_aspnet_UsersInRoles.Where(x => x.UserId == id).Select(y => y.RoleId).FirstOrDefault();
                var Role_id = _dbcontext.aspnet_Roles.Where(z => z.RoleId == Guid_role_id).Select(w => w.Role_id).FirstOrDefault();

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {

                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "[STP_FOR_EDIT_INVERTED_PRODUCTS]";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@family_id", SqlDbType.Int).Value = selectedFamilyId;
                    objSqlCommand.Parameters.Add("@product_Id", SqlDbType.Int).Value = getProduct_id;
                    objSqlCommand.Parameters.Add("@catalog_id", SqlDbType.Int).Value = SelectedCatalogId;
                    objSqlCommand.Parameters.AddWithValue("@created_user", created_user);
                    objSqlCommand.Parameters.AddWithValue("@Role_id", Role_id);
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(dt);

                }

                Dictionary<string, string> result = new Dictionary<string, string>();
                foreach (DataRow row in dt.Rows)
                {
                    result.Add(row["ATTRIBUTE_NAME"].ToString(), row["STRING_VALUE"].ToString());
                }
                if (dt.Rows != null && dt.Rows.Count > 0)
                {
                    result.Add("CATALOG_ID", dt.Rows[0]["CATALOG_ID"].ToString());
                    result.Add("FAMILY_ID", dt.Rows[0]["FAMILY_ID"].ToString());
                    result.Add("PRODUCT_ID", dt.Rows[0]["PRODUCT_ID"].ToString());
                }
                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController :GetNullvalueAttributesForSearch", exception);
                return null;
            }
        }
        [System.Web.Http.HttpPost]
        public int NullValueAttrCount(int selectedFamilyId)
        {
            try
            {
                DataTable dt = new DataTable();
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {

                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_NULLVALUE_ATTRIBUTES_FOR_COUNT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@family_id", SqlDbType.Int).Value = selectedFamilyId;

                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(dt);


                }
                string myNum = dt.Rows[0][0].ToString();
                int count = Convert.ToInt32(myNum);
                return count;

            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController : NullValueAttrCount", exception);
                return 0;
            }
        }





        [System.Web.Http.HttpGet]
        public DataTable Navigationforreports(string itemnumber)
        {
            try
            {
                DataTable dt = new DataTable();
                var model = new DashBoardModel();

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {

                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_REPORTS_NAVIGATION";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.Parameters.Add("@STRING_VALUE", SqlDbType.NVarChar).Value = itemnumber;
                    var objSqlDataAdapter = new SqlDataAdapter(objSqlCommand);
                    objSqlDataAdapter.Fill(dt);
                    return dt;
                }

            }
            catch (Exception exception)
            {
                Logger.Error("Error at ReportsApiController :Navigationforreports", exception);
                return null;
            }
        }


    }
}
