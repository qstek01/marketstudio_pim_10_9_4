﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Http;
using log4net;
using LS.Data;
using System.Web;
using LS.Data.Model;
using LS.Web.Models;
using System.Collections;
using System.Configuration;
using System.Data.OleDb;
using System.Diagnostics;
using System.Data.SqlClient;
using Infragistics.Documents.Excel;
using LS.Data.Model.CatalogSectionModels;
using Newtonsoft.Json;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Net.Http;
using System.Net;


namespace LS.Web.Controllers
{
    public class AttributeApiController : ApiController
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString;
        // GET: Home
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AttributeApiController));
        private readonly CSEntities _dbcontext = new CSEntities();
        AttributeMethods objAttributeMethods = new AttributeMethods();
        private ImportController IC = new ImportController();
        string attributeTemp;

        string SQLString = string.Empty;
        public class AttributeGroup
        {
            public string ATTRIBUTEGROUPNAME { get; set; }
            public int ATTRIBUTETYPE { get; set; }

        }
        [System.Web.Http.HttpGet]
        public List<AttributeGroup> GetAllattributesCount(int catalogid)
        {
            var objAttributeGroups = new List<AttributeGroup>();
            try
            {
                var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                if (customerid != null)
                {
                    var objAttrCount = new AttributeGroup();
                    //  var productSpec = _dbcontext.TB_ATTRIBUTE.Count(x => x.ATTRIBUTE_TYPE == 1);
                    var productSpec = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                        .Count(x => x.tcp.ATTRIBUTE_TYPE == 1 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount.ATTRIBUTETYPE = 1;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Item Specifications (" + productSpec + " items)";
                    objAttributeGroups.Add(objAttrCount);
                    var productimage = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                            .Count(x => x.tcp.ATTRIBUTE_TYPE == 3 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount = new AttributeGroup();
                    objAttrCount.ATTRIBUTETYPE = 3;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Item Image / Attachment (" + productimage + " items)";
                    objAttributeGroups.Add(objAttrCount);
                    var productPrice = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                               .Count(x => x.tcp.ATTRIBUTE_TYPE == 4 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount = new AttributeGroup();
                    objAttrCount.ATTRIBUTETYPE = 4;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Item Price (" + productPrice + " items)";
                    objAttributeGroups.Add(objAttrCount);
                    var productKey = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                               .Count(x => x.tcp.ATTRIBUTE_TYPE == 6 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount = new AttributeGroup();
                    objAttrCount.ATTRIBUTETYPE = 6;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Item Key (" + productKey + " items)";
                    objAttributeGroups.Add(objAttrCount);

                    var familyattribute = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                              .Count(x => x.tcp.ATTRIBUTE_TYPE == 11 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount = new AttributeGroup();
                    objAttrCount.ATTRIBUTETYPE = 11;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Product Specification (" + familyattribute + " items)";
                    objAttributeGroups.Add(objAttrCount);

                    var familyimage = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                               .Count(x => x.tcp.ATTRIBUTE_TYPE == 9 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount = new AttributeGroup();
                    objAttrCount.ATTRIBUTETYPE = 9;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Product Image / Attachment (" + familyimage + " items)";
                    objAttributeGroups.Add(objAttrCount);

                    var familyprice = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                               .Count(x => x.tcp.ATTRIBUTE_TYPE == 12 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount = new AttributeGroup();
                    objAttrCount.ATTRIBUTETYPE = 12;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Product Price (" + familyprice + " items)";
                    objAttributeGroups.Add(objAttrCount);
                    var familykey = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                               .Count(x => x.tcp.ATTRIBUTE_TYPE == 13 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount = new AttributeGroup();
                    objAttrCount.ATTRIBUTETYPE = 13;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Product Key (" + familykey + " items)";
                    objAttributeGroups.Add(objAttrCount);

                    var familydesc = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                           .Count(x => x.tcp.ATTRIBUTE_TYPE == 7 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount = new AttributeGroup();
                    objAttrCount.ATTRIBUTETYPE = 7;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Product Description (" + familydesc + " items)";
                    objAttributeGroups.Add(objAttrCount);

                    var categorySpecs = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                           .Count(x => x.tcp.ATTRIBUTE_TYPE == 21 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount = new AttributeGroup();
                    objAttrCount.ATTRIBUTETYPE = 21;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Category Specification (" + categorySpecs + " items)";
                    objAttributeGroups.Add(objAttrCount);

                    var categoryImg = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                           .Count(x => x.tcp.ATTRIBUTE_TYPE == 23 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount = new AttributeGroup();
                    objAttrCount.ATTRIBUTETYPE = 23;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Category Image / Attachment (" + categoryImg + " items)";
                    objAttributeGroups.Add(objAttrCount);

                    var categorydesc = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID, tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                           .Count(x => x.tcp.ATTRIBUTE_TYPE == 25 && x.tpf.CUSTOMER_ID == customerid.CustomerId);
                    objAttrCount = new AttributeGroup();
                    objAttrCount.ATTRIBUTETYPE = 25;
                    objAttrCount.ATTRIBUTEGROUPNAME = "Category Description (" + categorydesc + " items)";
                    objAttributeGroups.Add(objAttrCount);

                }
                return objAttributeGroups;
            }
            catch (Exception objException)
            {
                Logger.Error("Error at AttributeApiController :  GetAllattributesCount", objException);
                return null;
            }
        }
        [System.Web.Http.HttpGet]
        public IList GetAllattributesManager(int catalogId, int attributetype)
        {
            try
            {


                var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                var roleId = int.Parse(_dbcontext.aspnet_Roles.Join(_dbcontext.vw_aspnet_UsersInRoles, ar => ar.RoleId, aur => aur.RoleId, (ar, aur) => new { ar, aur }).Join(
                   _dbcontext.aspnet_Users, aspRole => aspRole.aur.UserId, au => au.UserId, (aspRole, au) => new { aspRole, au }).Where(x => x.au.UserName == User.Identity.Name).Select(y => y.aspRole.ar.Role_id).FirstOrDefault().ToString());
                var attrList = _dbcontext.VIEW_CATALOGSTUDIO5_ReadonlyAttribute.Join(_dbcontext.CUSTOMERATTRIBUTE, vcra => vcra.attribute_id, ca => ca.ATTRIBUTE_ID, (vcra, ca) => new { vcra, ca }).Join(
      _dbcontext.TB_READONLY_ATTRIBUTES, vrca => vrca.ca.ATTRIBUTE_ID, tra => tra.ATTRIBUTE_ID, (vrca, tra) => new { vrca, tra }).Where(x1 => x1.tra.ROLE_ID == roleId && x1.vrca.vcra.role_id == roleId);

                if (customerid != null)
                {



                    if (attributetype == 0)
                    {
                        var catalogs = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                                                   tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                                   .Where(x => x.tpf.CUSTOMER_ID == customerid.CustomerId && x.tcp.FLAG_RECYCLE == "A")
                                                   //.OrderBy(x => x.tcp.ATTRIBUTE_TYPE)
                                                   .ToList()
                                                   .Select(x => new NewDataSet
                                                   {
                                                       ATTRIBUTE_NAME = x.tcp.ATTRIBUTE_NAME,
                                                       ATTRIBUTE_TYPE = x.tcp.ATTRIBUTE_TYPE,
                                                       ATTRIBUTE_ID = x.tcp.ATTRIBUTE_ID,
                                                       CREATE_BY_DEFAULT = x.tcp.CREATE_BY_DEFAULT,
                                                       VALUE_REQUIRED = x.tcp.VALUE_REQUIRED,
                                                       STYLE_NAME = x.tcp.STYLE_NAME,
                                                       STYLE_FORMAT = x.tcp.STYLE_FORMAT,
                                                       DEFAULT_VALUE = x.tcp.DEFAULT_VALUE,
                                                       PUBLISH2PRINT = x.tcp.PUBLISH2PRINT,
                                                       PUBLISH2WEB = x.tcp.PUBLISH2WEB,
                                                       PUBLISH2CDROM = x.tcp.PUBLISH2CDROM,
                                                       PUBLISH2ODP = x.tcp.PUBLISH2ODP,
                                                       USE_PICKLIST = x.tcp.USE_PICKLIST,
                                                       ATTRIBUTE_DATATYPE = x.tcp.ATTRIBUTE_DATATYPE,
                                                       ATTRIBUTE_DATAFORMAT = objAttributeMethods.DataFormat(x.tcp.ATTRIBUTE_DATAFORMAT),
                                                       ATTRIBUTE_DATARULE = x.tcp.ATTRIBUTE_DATARULE,
                                                       dataRuleList = objAttributeMethods.XmlDeserializefunction(x.tcp.ATTRIBUTE_DATARULE),
                                                       UOM = x.tcp.UOM,
                                                       IS_CALCULATED = x.tcp.IS_CALCULATED,
                                                       ATTRIBUTE_CALC_FORMULA = x.tcp.ATTRIBUTE_CALC_FORMULA,
                                                       PICKLIST_NAME = x.tcp.PICKLIST_NAME,
                                                       NUMERIC = objAttributeMethods.GetNumericValues(x.tcp.ATTRIBUTE_DATATYPE.Length, x.tcp.ATTRIBUTE_DATATYPE, "Num"),
                                                       DECIMAL = objAttributeMethods.GetNumericValues(x.tcp.ATTRIBUTE_DATATYPE.Length, x.tcp.ATTRIBUTE_DATATYPE, "Dec"),
                                                       ALLOWEDIT = attrList.ToList().Count == 0 ? true : _dbcontext.VIEW_CATALOGSTUDIO5_ReadonlyAttribute.Join(_dbcontext.CUSTOMERATTRIBUTE, vcra => vcra.attribute_id, ca => ca.ATTRIBUTE_ID, (vcra, ca) => new { vcra, ca }).Join(
                               _dbcontext.TB_READONLY_ATTRIBUTES, vrca => vrca.ca.ATTRIBUTE_ID, tra => tra.ATTRIBUTE_ID, (vrca, tra) => new { vrca, tra }).Where(x1 => x1.tra.ROLE_ID == roleId && x1.vrca.vcra.role_id == roleId).Select(y => y.vrca.vcra.attribute_name).FirstOrDefault().ToString() == x.tcp.ATTRIBUTE_NAME ? true : false,
                                                       PUBLISH2EXPORT = x.tcp.PUBLISH2EXPORT,
                                                       PUBLISH2PORTAL = x.tcp.PUBLISH2PORTAL,
                                                       PUBLISH2PDF = x.tcp.PUBLISH2PDF,
                                                       CAPTION = x.tcp.CAPTION
                                                   });
                        //ImportApiController importapiController = new ImportApiController();
                        //var attribute_Table = new DataTable();
                        //attribute_Table = importapiController.ToDataTable(catalogs.ToList());

                        //  System.Web.HttpContext.Current.Session["AttributeExport"] = attribute_Table;
                        return catalogs.ToList();

                    }

                    else
                    {
                        var catalogs = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                            tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                            .Where(x => x.tpf.CUSTOMER_ID == customerid.CustomerId && x.tcp.ATTRIBUTE_TYPE == attributetype && x.tcp.FLAG_RECYCLE == "A")
                            .OrderBy(x => x.tcp.ATTRIBUTE_TYPE)
                            .ToList()
                            .Select(x => new NewDataSet
                            {
                                ATTRIBUTE_NAME = x.tcp.ATTRIBUTE_NAME,
                                ATTRIBUTE_TYPE = x.tcp.ATTRIBUTE_TYPE,
                                ATTRIBUTE_ID = x.tcp.ATTRIBUTE_ID,
                                CREATE_BY_DEFAULT = x.tcp.CREATE_BY_DEFAULT,
                                VALUE_REQUIRED = x.tcp.VALUE_REQUIRED,
                                STYLE_NAME = x.tcp.STYLE_NAME,
                                STYLE_FORMAT = x.tcp.STYLE_FORMAT,
                                DEFAULT_VALUE = x.tcp.DEFAULT_VALUE,
                                PUBLISH2PRINT = x.tcp.PUBLISH2PRINT,
                                PUBLISH2WEB = x.tcp.PUBLISH2WEB,
                                PUBLISH2CDROM = x.tcp.PUBLISH2CDROM,
                                PUBLISH2ODP = x.tcp.PUBLISH2ODP,
                                USE_PICKLIST = x.tcp.USE_PICKLIST,
                                ATTRIBUTE_DATATYPE = x.tcp.ATTRIBUTE_DATATYPE,
                                ATTRIBUTE_DATAFORMAT = objAttributeMethods.DataFormat(x.tcp.ATTRIBUTE_DATAFORMAT),
                                ATTRIBUTE_DATARULE = x.tcp.ATTRIBUTE_DATARULE,
                                dataRuleList = objAttributeMethods.XmlDeserializefunction(x.tcp.ATTRIBUTE_DATARULE),
                                UOM = x.tcp.UOM,
                                IS_CALCULATED = x.tcp.IS_CALCULATED,
                                ATTRIBUTE_CALC_FORMULA = x.tcp.ATTRIBUTE_CALC_FORMULA,
                                PICKLIST_NAME = x.tcp.PICKLIST_NAME,
                                NUMERIC = objAttributeMethods.GetNumericValues(x.tcp.ATTRIBUTE_DATATYPE.Length, x.tcp.ATTRIBUTE_DATATYPE, "Num"),
                                DECIMAL = objAttributeMethods.GetNumericValues(x.tcp.ATTRIBUTE_DATATYPE.Length, x.tcp.ATTRIBUTE_DATATYPE, "Dec"),
                                ALLOWEDIT = attrList.ToList().Count == 0 ? true : _dbcontext.VIEW_CATALOGSTUDIO5_ReadonlyAttribute.Join(_dbcontext.CUSTOMERATTRIBUTE, vcra => vcra.attribute_id, ca => ca.ATTRIBUTE_ID, (vcra, ca) => new { vcra, ca }).Join(
        _dbcontext.TB_READONLY_ATTRIBUTES, vrca => vrca.ca.ATTRIBUTE_ID, tra => tra.ATTRIBUTE_ID, (vrca, tra) => new { vrca, tra }).Where(x1 => x1.tra.ROLE_ID == roleId && x1.vrca.vcra.role_id == roleId).Select(y => y.vrca.vcra.attribute_name).FirstOrDefault().ToString() == x.tcp.ATTRIBUTE_NAME ? true : false,
                                PUBLISH2EXPORT = x.tcp.PUBLISH2EXPORT,
                                PUBLISH2PORTAL = x.tcp.PUBLISH2PORTAL,
                                PUBLISH2PDF = x.tcp.PUBLISH2PDF,
                                CAPTION = x.tcp.CAPTION
                            });
                        //ImportApiController importapiController = new ImportApiController();
                        //var attribute_Table = new DataTable();
                        //attribute_Table = importapiController.ToDataTable(catalogs.ToList());

                        //System.Web.HttpContext.Current.Session["AttributeExport"] = attribute_Table;
                        return catalogs.ToList();
                    }




                }
                else
                {
                    return new List<TB_ATTRIBUTE>();
                }
            }
            catch (Exception objException)
            {
                Logger.Error("Error at HomeApiController : GetAllattributesManager", objException);
                return new List<TB_ATTRIBUTE>();
            }

        }

        [System.Web.Http.HttpGet]
        public IList GetAllAttributesExport(int catalogId, int attributetype, bool displayIdcolumns)
        {
            try
            {
                var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                var roleId = int.Parse(_dbcontext.aspnet_Roles.Join(_dbcontext.vw_aspnet_UsersInRoles, ar => ar.RoleId, aur => aur.RoleId, (ar, aur) => new { ar, aur }).Join(
                   _dbcontext.aspnet_Users, aspRole => aspRole.aur.UserId, au => au.UserId, (aspRole, au) => new { aspRole, au }).Where(x => x.au.UserName == User.Identity.Name).Select(y => y.aspRole.ar.Role_id).FirstOrDefault().ToString());
                var attrList = _dbcontext.VIEW_CATALOGSTUDIO5_ReadonlyAttribute.Join(_dbcontext.CUSTOMERATTRIBUTE, vcra => vcra.attribute_id, ca => ca.ATTRIBUTE_ID, (vcra, ca) => new { vcra, ca }).Join(
      _dbcontext.TB_READONLY_ATTRIBUTES, vrca => vrca.ca.ATTRIBUTE_ID, tra => tra.ATTRIBUTE_ID, (vrca, tra) => new { vrca, tra }).Where(x1 => x1.tra.ROLE_ID == roleId && x1.vrca.vcra.role_id == roleId);

                if (customerid != null)
                {
                    if (attributetype == 0)
                    {
                        var catalogs = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                                                 tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                                 .Where(x => x.tpf.CUSTOMER_ID == customerid.CustomerId && x.tcp.ATTRIBUTE_TYPE != 0  && x.tcp.FLAG_RECYCLE == "A")
                                                 .OrderBy(x => x.tcp.ATTRIBUTE_TYPE)
                                                 .ToList()
                                                 .Select(x => new NewDataSet
                                                 {
                                                     ATTRIBUTE_NAME = x.tcp.ATTRIBUTE_NAME,
                                                     CAPTION = x.tcp.CAPTION,
                                                     ATTRIBUTE_TYPE = x.tcp.ATTRIBUTE_TYPE,
                                                     ATTRIBUTE_ID = x.tcp.ATTRIBUTE_ID,
                                                     CREATE_BY_DEFAULT = x.tcp.CREATE_BY_DEFAULT,
                                                     VALUE_REQUIRED = x.tcp.VALUE_REQUIRED,
                                                     STYLE_NAME = x.tcp.STYLE_NAME,
                                                     STYLE_FORMAT = x.tcp.STYLE_FORMAT,
                                                     DEFAULT_VALUE = x.tcp.DEFAULT_VALUE,
                                                     PUBLISH2PRINT = x.tcp.PUBLISH2PRINT,
                                                     PUBLISH2WEB = x.tcp.PUBLISH2WEB,
                                                     PUBLISH2CDROM = x.tcp.PUBLISH2CDROM,
                                                     PUBLISH2ODP = x.tcp.PUBLISH2ODP,
                                                     USE_PICKLIST = x.tcp.USE_PICKLIST,
                                                     ATTRIBUTE_DATATYPE = x.tcp.ATTRIBUTE_DATATYPE,
                                                     ATTRIBUTE_DATAFORMAT = objAttributeMethods.DataFormat(x.tcp.ATTRIBUTE_DATAFORMAT),
                                                     ATTRIBUTE_DATARULE = x.tcp.ATTRIBUTE_DATARULE,
                                                     dataRuleList = objAttributeMethods.XmlDeserializefunction(x.tcp.ATTRIBUTE_DATARULE),
                                                     UOM = x.tcp.UOM,
                                                     IS_CALCULATED = x.tcp.IS_CALCULATED,
                                                     ATTRIBUTE_CALC_FORMULA = x.tcp.ATTRIBUTE_CALC_FORMULA,
                                                     PICKLIST_NAME = x.tcp.PICKLIST_NAME,
                                                     NUMERIC = objAttributeMethods.GetNumericValues(x.tcp.ATTRIBUTE_DATATYPE.Length, x.tcp.ATTRIBUTE_DATATYPE, "Num"),
                                                     DECIMAL = objAttributeMethods.GetNumericValues(x.tcp.ATTRIBUTE_DATATYPE.Length, x.tcp.ATTRIBUTE_DATATYPE, "Dec"),
                                                     ALLOWEDIT = attrList.ToList().Count == 0 ? true : _dbcontext.VIEW_CATALOGSTUDIO5_ReadonlyAttribute.Join(_dbcontext.CUSTOMERATTRIBUTE, vcra => vcra.attribute_id, ca => ca.ATTRIBUTE_ID, (vcra, ca) => new { vcra, ca }).Join(
                             _dbcontext.TB_READONLY_ATTRIBUTES, vrca => vrca.ca.ATTRIBUTE_ID, tra => tra.ATTRIBUTE_ID, (vrca, tra) => new { vrca, tra }).Where(x1 => x1.tra.ROLE_ID == roleId && x1.vrca.vcra.role_id == roleId).Select(y => y.vrca.vcra.attribute_name).FirstOrDefault().ToString() == x.tcp.ATTRIBUTE_NAME ? true : false,
                                                     PUBLISH2EXPORT = x.tcp.PUBLISH2EXPORT,
                                                     PUBLISH2PORTAL = x.tcp.PUBLISH2PORTAL,
                                                     PUBLISH2PDF = x.tcp.PUBLISH2PDF
                                                 });
                        ImportApiController importapiController = new ImportApiController();
                        var attribute_Table = new DataTable();
                        attribute_Table = importapiController.ToDataTable(catalogs.ToList());
                        attribute_Table.Columns.Remove("CREATED_USER");
                        attribute_Table.Columns.Remove("CREATED_DATE");
                        attribute_Table.Columns.Remove("MODIFIED_USER");
                        attribute_Table.Columns.Remove("MODIFIED_DATE");
                        attribute_Table.Columns.Remove("PUBLISH2CDROM");
                        attribute_Table.Columns.Remove("PUBLISH2ODP");
                        attribute_Table.Columns.Remove("ALLOWEDIT");
                        attribute_Table.Columns.Remove("NUMERIC");
                        attribute_Table.Columns.Remove("DECIMAL");
                        attribute_Table.Columns.Remove("UOM");
                        attribute_Table.Columns.Add("ACTION");

                        if (attribute_Table.Columns.Contains("ACTION"))
                        {

                            attribute_Table.Columns["ACTION"].SetOrdinal(0);
                        }



                        // attribute_Table.Columns.RemoveAt(columnIndex);
                        foreach (DataRow dr in attribute_Table.Rows)
                        {
                            if (dr["ATTRIBUTE_TYPE"].ToString() == "1")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Item Specifications";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "3")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Item Image/Attachment";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "4")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Item Price";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "6")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Item Key";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "7")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Product Description";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "9")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Product Image/Attachment";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "11")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Product Specifications";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "12")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Product Price";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "13")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Product Key";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "21")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Category Specifications";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "23")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Category Image/Attachment";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "25")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Category Description";
                            }
                            else
                            {
                                dr["ATTRIBUTE_TYPE"] = "Category Image/Attachment";
                            }
                        }
                        if(!displayIdcolumns)
                        {
                            attribute_Table.Columns.Remove("ATTRIBUTE_ID");
                        }
                        attribute_Table.AcceptChanges();
                        System.Web.HttpContext.Current.Session["AttributeExport"] = attribute_Table;
                        return catalogs.ToList();
                    }
                    else
                    {
                        var catalogs = _dbcontext.TB_ATTRIBUTE.Join(_dbcontext.CUSTOMERATTRIBUTE, tcp => tcp.ATTRIBUTE_ID,
                                                      tpf => tpf.ATTRIBUTE_ID, (tcp, tpf) => new { tcp, tpf })
                                                      .Where(x => x.tpf.CUSTOMER_ID == customerid.CustomerId && x.tcp.ATTRIBUTE_TYPE != 0 && x.tcp.ATTRIBUTE_TYPE == attributetype && x.tcp.FLAG_RECYCLE == "A")
                                                      .OrderBy(x => x.tcp.ATTRIBUTE_TYPE)
                                                      .ToList()
                                                      .Select(x => new NewDataSet
                                                      {
                                                          ATTRIBUTE_NAME = x.tcp.ATTRIBUTE_NAME,
                                                          CAPTION = x.tcp.CAPTION,
                                                          ATTRIBUTE_TYPE = x.tcp.ATTRIBUTE_TYPE,
                                                          ATTRIBUTE_ID = x.tcp.ATTRIBUTE_ID,
                                                          CREATE_BY_DEFAULT = x.tcp.CREATE_BY_DEFAULT,
                                                          VALUE_REQUIRED = x.tcp.VALUE_REQUIRED,
                                                          STYLE_NAME = x.tcp.STYLE_NAME,
                                                          STYLE_FORMAT = x.tcp.STYLE_FORMAT,
                                                          DEFAULT_VALUE = x.tcp.DEFAULT_VALUE,
                                                          PUBLISH2PRINT = x.tcp.PUBLISH2PRINT,
                                                          PUBLISH2WEB = x.tcp.PUBLISH2WEB,
                                                          PUBLISH2CDROM = x.tcp.PUBLISH2CDROM,
                                                          PUBLISH2ODP = x.tcp.PUBLISH2ODP,
                                                          USE_PICKLIST = x.tcp.USE_PICKLIST,
                                                          ATTRIBUTE_DATATYPE = x.tcp.ATTRIBUTE_DATATYPE,
                                                          ATTRIBUTE_DATAFORMAT = objAttributeMethods.DataFormat(x.tcp.ATTRIBUTE_DATAFORMAT),
                                                          ATTRIBUTE_DATARULE = x.tcp.ATTRIBUTE_DATARULE,
                                                          dataRuleList = objAttributeMethods.XmlDeserializefunction(x.tcp.ATTRIBUTE_DATARULE),
                                                          UOM = x.tcp.UOM,
                                                          IS_CALCULATED = x.tcp.IS_CALCULATED,
                                                          ATTRIBUTE_CALC_FORMULA = x.tcp.ATTRIBUTE_CALC_FORMULA,
                                                          PICKLIST_NAME = x.tcp.PICKLIST_NAME,
                                                          NUMERIC = objAttributeMethods.GetNumericValues(x.tcp.ATTRIBUTE_DATATYPE.Length, x.tcp.ATTRIBUTE_DATATYPE, "Num"),
                                                          DECIMAL = objAttributeMethods.GetNumericValues(x.tcp.ATTRIBUTE_DATATYPE.Length, x.tcp.ATTRIBUTE_DATATYPE, "Dec"),
                                                          ALLOWEDIT = attrList.ToList().Count == 0 ? true : _dbcontext.VIEW_CATALOGSTUDIO5_ReadonlyAttribute.Join(_dbcontext.CUSTOMERATTRIBUTE, vcra => vcra.attribute_id, ca => ca.ATTRIBUTE_ID, (vcra, ca) => new { vcra, ca }).Join(
                                  _dbcontext.TB_READONLY_ATTRIBUTES, vrca => vrca.ca.ATTRIBUTE_ID, tra => tra.ATTRIBUTE_ID, (vrca, tra) => new { vrca, tra }).Where(x1 => x1.tra.ROLE_ID == roleId && x1.vrca.vcra.role_id == roleId).Select(y => y.vrca.vcra.attribute_name).FirstOrDefault().ToString() == x.tcp.ATTRIBUTE_NAME ? true : false,
                                                          PUBLISH2EXPORT = x.tcp.PUBLISH2EXPORT,
                                                          PUBLISH2PORTAL = x.tcp.PUBLISH2PORTAL,
                                                          PUBLISH2PDF = x.tcp.PUBLISH2PDF
                                                      });

                        ImportApiController importapiController = new ImportApiController();
                        var attribute_Table = new DataTable();

                        attribute_Table = importapiController.ToDataTable(catalogs.ToList());
                        attribute_Table.Columns.Remove("CREATED_USER");
                        attribute_Table.Columns.Remove("CREATED_DATE");
                        attribute_Table.Columns.Remove("MODIFIED_USER");
                        attribute_Table.Columns.Remove("MODIFIED_DATE");
                        attribute_Table.Columns.Remove("PUBLISH2CDROM");
                        attribute_Table.Columns.Remove("PUBLISH2ODP");
                        attribute_Table.Columns.Remove("ALLOWEDIT");
                        attribute_Table.Columns.Remove("NUMERIC");
                        attribute_Table.Columns.Remove("DECIMAL");
                        attribute_Table.Columns.Remove("UOM");
                        attribute_Table.Columns.Add("ACTION");

                        if (attribute_Table.Columns.Contains("ACTION"))
                        {

                            attribute_Table.Columns["ACTION"].SetOrdinal(0);
                        }


                        // attribute_Table.Columns.RemoveAt(columnIndex);
                        foreach (DataRow dr in attribute_Table.Rows)
                        {
                            if (dr["ATTRIBUTE_TYPE"].ToString() == "1")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Item Specifications";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "3")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Item Image/Attachment";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "4")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Item Price";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "6")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Item Key";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "7")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Product Description";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "9")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Product Image/Attachment";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "11")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Product Specifications";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "12")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Product Price";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "13")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Product Key";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "21")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Category Specifications";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "23")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Category Image/Attachment";
                            }
                            else if (dr["ATTRIBUTE_TYPE"].ToString() == "25")
                            {
                                dr["ATTRIBUTE_TYPE"] = "Category Description";
                            }
                            else
                            {
                                dr["ATTRIBUTE_TYPE"] = "Category Image/Attachment";
                            }
                        }
                        if (!displayIdcolumns)
                        {
                            attribute_Table.Columns.Remove("ATTRIBUTE_ID");
                        }
                        attribute_Table.AcceptChanges();
                        System.Web.HttpContext.Current.Session["AttributeExport"] = attribute_Table;
                        return catalogs.ToList();
                    }
                }

                else
                {
                    return new List<TB_ATTRIBUTE>();
                }

            }



            catch (Exception objException)
            {
                Logger.Error("Error at AttributeApiApiController : GetAllAttributesExport", objException);
                return new List<TB_ATTRIBUTE>();
            }

        }

        #region Attributes List Export


        [System.Web.Http.HttpGet]
        public string ExportAttributesList(int catalogId)
        {

            try
            {

                var objDataSet = new DataSet();
                var ReferenceExport = string.Empty;
                int[] attributetypes = new int[] { 1, 3, 4, 6, 7, 9, 11, 12, 13 };
                List<int> attributeTypes = new List<int>();
                attributeTypes = attributetypes.ToList();
                var customerid = _dbcontext.Customer_User.FirstOrDefault(x => x.User_Name == User.Identity.Name);
                var roleId = int.Parse(_dbcontext.aspnet_Roles.Join(_dbcontext.vw_aspnet_UsersInRoles, ar => ar.RoleId, aur => aur.RoleId, (ar, aur) => new { ar, aur }).Join(
                   _dbcontext.aspnet_Users, aspRole => aspRole.aur.UserId, au => au.UserId, (aspRole, au) => new { aspRole, au }).Where(x => x.au.UserName == User.Identity.Name).Select(y => y.aspRole.ar.Role_id).FirstOrDefault().ToString());
                var attrList = _dbcontext.VIEW_CATALOGSTUDIO5_ReadonlyAttribute.Join(_dbcontext.CUSTOMERATTRIBUTE, vcra => vcra.attribute_id, ca => ca.ATTRIBUTE_ID, (vcra, ca) => new { vcra, ca }).Join(
                _dbcontext.TB_READONLY_ATTRIBUTES, vrca => vrca.ca.ATTRIBUTE_ID, tra => tra.ATTRIBUTE_ID, (vrca, tra) => new { vrca, tra }).Where(x1 => x1.tra.ROLE_ID == roleId && x1.vrca.vcra.role_id == roleId);

                if (customerid != null)
                {
                    var catalogs = _dbcontext.TB_CATALOG_ATTRIBUTES.Join(_dbcontext.TB_ATTRIBUTE, tca => tca.ATTRIBUTE_ID, ta => ta.ATTRIBUTE_ID, (tca, ta) => new { tca, ta })
                                                                   .Join(_dbcontext.CUSTOMERATTRIBUTE, tcaa => tcaa.ta.ATTRIBUTE_ID, ca => ca.ATTRIBUTE_ID, (tcaa, ca) => new { tcaa, ca })
                                                                   .Where(x => x.tcaa.tca.CATALOG_ID == catalogId && x.ca.CUSTOMER_ID == customerid.CustomerId && x.tcaa.ta.FLAG_RECYCLE == "A").OrderBy(x => x.tcaa.ta.ATTRIBUTE_TYPE)
                                                                   .ToList().Select(x => new NewDataSet
                                                                   {
                                                                       ATTRIBUTE_NAME = x.tcaa.ta.ATTRIBUTE_NAME,
                                                                       CAPTION=x.tcaa.ta.CAPTION,
                                                                       ATTRIBUTE_TYPE = x.tcaa.ta.ATTRIBUTE_TYPE,
                                                                       ATTRIBUTE_ID = x.tcaa.ta.ATTRIBUTE_ID,
                                                                       CREATE_BY_DEFAULT = x.tcaa.ta.CREATE_BY_DEFAULT,
                                                                       VALUE_REQUIRED = x.tcaa.ta.VALUE_REQUIRED,
                                                                       STYLE_NAME = x.tcaa.ta.STYLE_NAME,
                                                                       STYLE_FORMAT = x.tcaa.ta.STYLE_FORMAT,
                                                                       DEFAULT_VALUE = x.tcaa.ta.DEFAULT_VALUE,
                                                                       PUBLISH2PRINT = x.tcaa.ta.PUBLISH2PRINT,
                                                                       PUBLISH2WEB = x.tcaa.ta.PUBLISH2WEB,
                                                                       // PUBLISH2CDROM = x.tcp.PUBLISH2CDROM,
                                                                       //PUBLISH2ODP = x.tcp.PUBLISH2ODP,
                                                                       USE_PICKLIST = x.tcaa.ta.USE_PICKLIST,
                                                                       ATTRIBUTE_DATATYPE = x.tcaa.ta.ATTRIBUTE_DATATYPE,
                                                                       ATTRIBUTE_DATAFORMAT = objAttributeMethods.DataFormat(x.tcaa.ta.ATTRIBUTE_DATAFORMAT),
                                                                       ATTRIBUTE_DATARULE = x.tcaa.ta.ATTRIBUTE_DATARULE,
                                                                       dataRuleList = objAttributeMethods.XmlDeserializefunction(x.tcaa.ta.ATTRIBUTE_DATARULE),
                                                                       UOM = x.tcaa.ta.UOM,
                                                                       IS_CALCULATED = x.tcaa.ta.IS_CALCULATED,
                                                                       ATTRIBUTE_CALC_FORMULA = x.tcaa.ta.ATTRIBUTE_CALC_FORMULA,
                                                                       PICKLIST_NAME = x.tcaa.ta.PICKLIST_NAME,
                                                                       //NUMERIC = objAttributeMethods.GetNumericValues(x.tcp.ATTRIBUTE_DATATYPE.Length, x.tcp.ATTRIBUTE_DATATYPE, "Num"),
                                                                       // DECIMAL = objAttributeMethods.GetNumericValues(x.tcp.ATTRIBUTE_DATATYPE.Length, x.tcp.ATTRIBUTE_DATATYPE, "Dec"),
                                                                       ALLOWEDIT = attrList.ToList().Count == 0 ? true : _dbcontext.VIEW_CATALOGSTUDIO5_ReadonlyAttribute.Join(_dbcontext.CUSTOMERATTRIBUTE, vcra => vcra.attribute_id, ca => ca.ATTRIBUTE_ID, (vcra, ca) => new { vcra, ca }).Join(
                                                                      _dbcontext.TB_READONLY_ATTRIBUTES, vrca => vrca.ca.ATTRIBUTE_ID, tra => tra.ATTRIBUTE_ID, (vrca, tra) => new { vrca, tra }).Where(x1 => x1.tra.ROLE_ID == roleId && x1.vrca.vcra.role_id == roleId).Select(y => y.vrca.vcra.attribute_name).FirstOrDefault().ToString() == x.tcaa.ta.ATTRIBUTE_NAME ? true : false,
                                                                       PUBLISH2EXPORT = x.tcaa.ta.PUBLISH2EXPORT,
                                                                       PUBLISH2PORTAL = x.tcaa.ta.PUBLISH2PORTAL,
                                                                       PUBLISH2PDF = x.tcaa.ta.PUBLISH2PDF
                                                                   });
                    ImportApiController importapiController = new ImportApiController();
                    var dt = new DataTable();
                    dt = importapiController.ToDataTable(catalogs.ToList());
                    ExportAttributesListDataSetToExcel(dt);

                    return "AttributesListExport";
                }
                return "AttributesListExport";
            }
            catch (Exception objException)
            {
                Logger.Error("Error at AttributeApiController : ExportAttributesList", objException);
                return "";
            }
        }

        public void ExportAttributesListDataSetToExcel(DataTable attributeList)
        {
            try
            {
                System.Web.HttpContext.Current.Session["ExportTableAttributeList"] = null;
                System.Web.HttpContext.Current.Session["ExportTableAttributeList"] = attributeList;
            }
            catch (Exception ex)
            {

            }
        }

        #endregion

        #region Attribute Import Validation

        [System.Web.Http.HttpPost]
        public string SheetValidation(string excelPath, string importtype)
        {
            try
            {
                DataTable sheetValue = new DataTable();
                DataTable checkSheet = new DataTable();
                importtype = importtype.ToUpper();
                int i = 0;
                //DataTable dtResult = null;
                int totalSheet = 0;
                using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelPath + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();
                    DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    //   string sheetName = string.Empty;
                    if (dt != null)
                    {
                        var tempDataTable = (from dataRow in dt.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();
                        dt = tempDataTable;
                        totalSheet = dt.Rows.Count;

                    }
                    String[] excelSheets = new String[dt.Rows.Count];
                    foreach (DataRow dr in dt.Rows)
                    {
                        string tableName = dr["TABLE_NAME"].ToString();
                        tableName = tableName.Replace(@"$", string.Empty);
                        excelSheets[i] = tableName;
                        i++;
                    }
                    if (excelSheets.Length == 1 && importtype == "ATTRIBUTE")
                    {
                        if (excelSheets[0] != "Attribute")
                        {
                            return "invalid sheet name";
                        }
                        else
                        {
                            sheetValue = ConvertExcelToDataTable(excelPath, "Attribute");
                            if (sheetValue.Rows.Count == 0)
                            {
                                return "empty value";
                            }
                            else
                            {
                                checkSheet = sheetValue.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                                if (checkSheet.Rows.Count == 0)
                                {
                                    return "empty value";
                                }
                            }
                        }
                    }


                    return (excelSheets.Length).ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error at ChangeRoleViewLog ", ex);
                return "false";
            }
        }

        public static DataTable ConvertExcelToDataTable(string FileName, string Sheetname)
        {
            try
            {
                DataTable dtResult = null;
                string tableName = Sheetname;
                int totalSheet = 0; //No of sheets on excel file  
                using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
                {
                    objConn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    OleDbDataAdapter oleda = new OleDbDataAdapter();
                    DataSet ds = new DataSet();
                    DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    string sheetName = string.Empty;
                    if (dt != null)
                    {
                        var tempDataTable = (from dataRow in dt.AsEnumerable()
                                             where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                             select dataRow).CopyToDataTable();
                        dt = tempDataTable;
                        totalSheet = dt.Rows.Count;
                        sheetName = Sheetname + "$";
                    }
                    cmd.Connection = objConn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                    oleda = new OleDbDataAdapter(cmd);
                    oleda.Fill(ds, tableName);
                    dtResult = ds.Tables[tableName];
                    objConn.Close();
                    return dtResult; //Returning Dattable  
                }

            }
            catch (Exception ex)
            {
                Logger.Error("Error at ConvertExcelToDataTable ", ex);
                return null;
            }

        }

        #endregion

        public string CreateTable(string tableName, DataTable objtable)
        {
            try
            {
                string sqlsc = "CREATE TABLE " + tableName + "(";
                for (int i = 0; i < objtable.Columns.Count; i++)
                {
                    if (!objtable.Columns[i].ColumnName.Contains('['))
                    {
                        sqlsc += "\n [" + objtable.Columns[i].ColumnName + "] ";
                    }
                    else
                    {
                        sqlsc += "\n" + objtable.Columns[i].ColumnName + "";
                    }
                    if (objtable.Columns[i].ColumnName.Contains("CATALOG_ID"))
                        sqlsc += "nvarchar(50) ";
                    else if (objtable.Columns[i].DataType.ToString().Contains("System.String") ||
                             objtable.Columns[i].ColumnName.Contains("SUBCATID") ||
                             objtable.Columns[i].ColumnName.Contains("CATALOG_ITEM_NO"))
                        sqlsc += "nvarchar(max) ";
                    else
                        sqlsc += "nvarchar(max) ";
                    sqlsc += ",";
                }
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    var sqlString = "EXEC('delete from QS_IMPORTMAPPING')";
                    SqlCommand sqlCommand = new SqlCommand(sqlString, con);
                    sqlCommand.ExecuteNonQuery();
                    con.Dispose();
                }

                return sqlsc.Substring(0, sqlsc.Length - 1) + ")";
            }
            catch (Exception ex)
            {
                Logger.Error("Error at CreateTable : Program - ", ex);
                throw;
            }
        }

        #region Attribute Import Sheet Validation
        [System.Web.Http.HttpPost]
        public string ValidateAttrImport(string importExcelSheetDDSelectionValue, string excelPath, string importFormat)
        {

            try
            {

                string[] columnName =  {"ACTION", "ATTRIBUTE_NAME","ATTRIBUTE_TYPE",
                                       "CREATE_BY_DEFAULT", "VALUE_REQUIRED", "STYLE_NAME","STYLE_FORMAT",
                                       "DEFAULT_VALUE","PUBLISH2PRINT","PUBLISH2WEB","USE_PICKLIST","ATTRIBUTE_DATATYPE",
                                       "ATTRIBUTE_DATAFORMAT","IS_CALCULATED","ATTRIBUTE_CALC_FORMULA",
                                       "PICKLIST_NAME","ATTRIBUTE_DATARULE","PUBLISH2PDF" };
                //,"PUBLISH2EXPORT","PUBLISH2PORTAL"
                attributeTemp = Guid.NewGuid().ToString();
                // string SheetName = "Attribute$";
                string[] missingColumn = new string[columnName.Length];
                int missingValues = 0, duplicateRows = 0, duplicateId = 0,datatype_Validation = 0;
                string errorMissingColumn = string.Empty;
                DataTable import_Table = ConvertExcelToDataTable(excelPath, importExcelSheetDDSelectionValue);
                DataTable attrImportTable = import_Table.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();
                DataColumnCollection columns = attrImportTable.Columns;
                int attrMissingColumn = 0, attrRefExists = 0;
                if (!attrImportTable.Columns.Contains("ATTRIBUTE_ID"))
                {
                    attrImportTable.Columns.Add("ATTRIBUTE_ID");
                }
                //Check the column is in the  sheet or not
                attrImportTable.Columns.Add("Missing Column", typeof(string));
                for (int i = 0; i < columnName.Length; i++)
                {
                    if (!columns.Contains(columnName[i]))
                    {
                        if (attrMissingColumn == 0)
                        {
                            missingColumn[attrMissingColumn] = columnName[i];
                            errorMissingColumn = errorMissingColumn + columnName[i];
                            attrMissingColumn++;
                        }
                        else
                        {
                            missingColumn[attrMissingColumn] = columnName[i];
                            errorMissingColumn = errorMissingColumn + "~" + columnName[i];
                            attrMissingColumn++;
                        }

                    }
                }
                //Write the missing column in datatable
                foreach (DataRow dr in attrImportTable.Rows)
                {

                    dr["Missing Column"] = errorMissingColumn;
                    break;
                }

                string[] columnValues = { "ATTRIBUTE_NAME", "ATTRIBUTE_TYPE", "CREATE_BY_DEFAULT", "ATTRIBUTE_DATAFORMAT", "ATTRIBUTE_DATATYPE" };

                //Missing Values Validation

                attrImportTable.Columns.Add("Missing Values", typeof(string));
                for (int i = 0; i < columnValues.Length; i++)
                {
                    if (columns.Contains(columnValues[i]))
                    {
                        foreach (DataRow dr in attrImportTable.Select("" + columnValues[i] + " is NULL or " + columnValues[i] + "=''"))
                        {
                            missingValues++;
                            string updateMissingValues = dr["Missing Values"].ToString();
                            if (updateMissingValues != "")
                            {
                                updateMissingValues = updateMissingValues + ".|";
                            }
                            dr["Missing Values"] = updateMissingValues + "" + columnValues[i] + " " + " has no values";
                            updateMissingValues = string.Empty;
                        }
                    }


                }
                attrImportTable.AcceptChanges();


                //Reference Exists
                foreach (DataRow delAttr in attrImportTable.Rows)
                {
                    string action = delAttr["ACTION"].ToString();
                    action = action.ToUpper();
                    if (action.Equals("DELETE"))
                    {
                        DataTable deleteAttrValues = new DataTable();
                        deleteAttrValues = attrImportTable.AsEnumerable().Where(x => x.Field<string>("ACTION") != null && x.Field<string>("ACTION").ToUpper().Contains("DELETE")).CopyToDataTable();


                        foreach (DataRow dr in deleteAttrValues.Rows)
                        {
                            int attribute_Id = 0;

                            if (dr["ATTRIBUTE_ID"] == DBNull.Value)
                            {

                                string attr_Name = dr["ATTRIBUTE_NAME"].ToString();
                                attribute_Id = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_NAME == attr_Name).Select(x => x.ATTRIBUTE_ID).FirstOrDefault();

                            }
                            else
                            {
                                attribute_Id = Convert.ToInt32(dr["ATTRIBUTE_ID"]);
                            }

                            var family_Attr = _dbcontext.TB_CATEGORY_FAMILY_ATTR_LIST.Where(x => x.ATTRIBUTE_ID == attribute_Id);
                            var prod_Attr = _dbcontext.TB_PROD_FAMILY_ATTR_LIST.Where(y => y.ATTRIBUTE_ID == attribute_Id);
                            var family_Specs = _dbcontext.TB_FAMILY_SPECS.Where(z => z.STRING_VALUE != "NULL" || z.STRING_VALUE != "");
                            var prod_Specs = _dbcontext.TB_PROD_SPECS.Where(a => a.STRING_VALUE != "NULL" || a.STRING_VALUE != "");

                            if (family_Attr.Count() > 0 && prod_Attr.Count() > 0 && family_Specs.Count() > 0 && prod_Specs.Count() > 0)
                            {
                                attrRefExists++;
                                string updateMissingValues = dr["Missing Values"].ToString();
                                if (updateMissingValues != "")
                                {
                                    updateMissingValues = updateMissingValues + ".|";
                                }
                                dr["Missing Values"] = updateMissingValues + "Attribute cannot be Deleted";
                            }

                        }
                        attrImportTable.AcceptChanges();
                    }
                }
                //Duplicate Rows Checking


                var dupValues = attrImportTable.AsEnumerable()
                  .GroupBy(dr => dr.Field<string>("ATTRIBUTE_NAME")).Where(g => g.Count() > 1).Select(g => g.First()).ToList();

                //  var dup_Cap_Values = attrImportTable.AsEnumerable()
                //  .GroupBy(dr => dr.Field<string>("CAPTION")).Where(g.Count() > 1 ).Select(g => g.First()).ToList();

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SQLString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        attributeTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + attributeTemp + "] END')";
                    SqlCommand _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                    _DBCommand.ExecuteNonQuery();

                    SQLString = CreateTable("[##IMPORTTEMP" + attributeTemp + "]", attrImportTable);
                    _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                    _DBCommand.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + attributeTemp + "]"
                    };
                    bulkCopy.WriteToServer(attrImportTable);


                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = " SELECT t.caption FROM [##IMPORTTEMP" + attributeTemp + "] t WHERE t.caption is not null GROUP BY t.caption HAVING COUNT(t.caption) > 1";

                    DataTable dup_Cap_Values = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter(objSqlCommand);
                    sda.Fill(dup_Cap_Values);


                    foreach (var dup in dupValues)
                    {
                        string dupAttrValue = dup[1].ToString();
                        foreach (DataRow dr in attrImportTable.Rows)
                        {
                            string checkDuplicate = dr["ATTRIBUTE_NAME"].ToString();
                            if (checkDuplicate.Equals(dupAttrValue))
                            {
                                string updateMissingValues = dr["Missing Values"].ToString();
                                if (updateMissingValues != "")
                                {
                                    updateMissingValues = updateMissingValues + ".|";
                                }
                                dr["Missing Values"] = updateMissingValues + "Duplicate Values";
                            }
                        }
                        duplicateRows++;

                    }

                    foreach (DataRow dupc in dup_Cap_Values.Rows)
                    {
                        string dupCapValue = dupc["CAPTION"].ToString();
                        foreach (DataRow dr in attrImportTable.Rows)
                        {
                            string checkDuplicate = dr["CAPTION"].ToString();
                            if (checkDuplicate.Equals(dupCapValue))
                            {
                                string updateMissingValues = dr["Missing Values"].ToString();
                                if (updateMissingValues != "")
                                {
                                    updateMissingValues = updateMissingValues + ".|";
                                }
                                dr["Missing Values"] = updateMissingValues + "Duplicate Values";
                                duplicateRows++;
                            }
                        }


                    }

                    attrImportTable.AcceptChanges();
                }

                // bool aExists = attrImportTable.Select().ToList().Exists(row => row["ATTRIBUTE_ID"].ToString() == "");
                //   if (aExists)
                //    {
                var customerid = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    objSqlConnection.Open();
                    SQLString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        attributeTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + attributeTemp + "] END')";
                    SqlCommand _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                    _DBCommand.ExecuteNonQuery();

                    SQLString = CreateTable("[##IMPORTTEMP" + attributeTemp + "]", attrImportTable);
                    _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                    _DBCommand.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + attributeTemp + "]"
                    };
                    bulkCopy.WriteToServer(attrImportTable);


                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_ATTRIBUTE_IMPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;

                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Parameters.Add("@ATTRNAME", SqlDbType.NVarChar, 100).Value = "";
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "VALIDATION ATTR_NAMES and caption";
                    objSqlCommand.Parameters.Add("@CUSTOMER_ID", SqlDbType.Int, 100).Value = customerid;
                    objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = attributeTemp;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int, 100).Value = 0;

                    DataSet dup_Attrs_Name = new DataSet();
                    SqlDataAdapter sda = new SqlDataAdapter(objSqlCommand);
                    sda.Fill(dup_Attrs_Name);
                    foreach (DataRow dupc in dup_Attrs_Name.Tables[0].Rows)
                    {
                        string dupCapValue = dupc["ATTRIBUTE_NAME"].ToString();
                        foreach (DataRow dr in attrImportTable.Rows)
                        {
                            string checkDuplicate = dr["ATTRIBUTE_NAME"].ToString();
                            if (checkDuplicate.Equals(dupCapValue))
                            {
                                string updateMissingValues = dr["Missing Values"].ToString();
                                if (updateMissingValues != "")
                                {
                                    updateMissingValues = updateMissingValues + ".|";
                                }
                                dr["Missing Values"] = updateMissingValues + "Duplicate Values";
                                duplicateRows++;
                            }
                        }

                    }
                    foreach (DataRow dupc in dup_Attrs_Name.Tables[1].Rows)
                    {
                        string dupCapValue = dupc["ATTRIBUTE_NAME"].ToString();
                        foreach (DataRow dr in attrImportTable.Rows)
                        {
                            string checkDuplicate = dr["ATTRIBUTE_NAME"].ToString();
                            if (checkDuplicate.Equals(dupCapValue))
                            {
                                string updateMissingValues = dr["Missing Values"].ToString();
                                if (updateMissingValues != "")
                                {
                                    updateMissingValues = updateMissingValues + ".|";
                                }
                                dr["Missing Values"] = updateMissingValues + "Duplicate Values";
                                duplicateRows++;
                            }
                        }

                    }
                    
                }



                bool exists = attrImportTable.Select().ToList().Exists(row => row["ATTRIBUTE_ID"].ToString() == "");
                if (!exists)
                {
                    var customer_id = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();


                    using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                    {
                        objSqlConnection.Open();
                        SQLString =
                            "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                            attributeTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + attributeTemp + "] END')";
                        SqlCommand _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                        _DBCommand.ExecuteNonQuery();

                        SQLString = CreateTable("[##IMPORTTEMP" + attributeTemp + "]", attrImportTable);
                        _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                        _DBCommand.ExecuteNonQuery();
                        var bulkCopy = new SqlBulkCopy(objSqlConnection)
                        {
                            DestinationTableName = "[##IMPORTTEMP" + attributeTemp + "]"
                        };
                        bulkCopy.WriteToServer(attrImportTable);


                        SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                        objSqlCommand.CommandText = "STP_LS_ATTRIBUTE_IMPORT";
                        objSqlCommand.CommandType = CommandType.StoredProcedure;

                        objSqlCommand.Connection = objSqlConnection;
                        objSqlCommand.CommandTimeout = 0;
                        objSqlCommand.Parameters.Add("@ATTRNAME", SqlDbType.NVarChar, 100).Value = "";
                        objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "VALIDATION DUP_ATTR_NAME";
                        objSqlCommand.Parameters.Add("@CUSTOMER_ID", SqlDbType.Int, 100).Value = customer_id;
                        objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                        objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                        objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = attributeTemp;
                        objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int, 100).Value = 0;

                        DataTable dup_Attr_Name = new DataTable();
                        SqlDataAdapter sda = new SqlDataAdapter(objSqlCommand);
                        sda.Fill(dup_Attr_Name);

                        foreach (DataRow _duplicate_AttrNames in attrImportTable.Rows)
                        {
                            int attrId = int.Parse(_duplicate_AttrNames["ATTRIBUTE_ID"].ToString());

                            var AttrNameCheck = _dbcontext.TB_ATTRIBUTE.Where(x => x.ATTRIBUTE_ID == attrId).Select(x => x.ATTRIBUTE_NAME).FirstOrDefault();



                            foreach (DataRow _duplicateAttrNames in dup_Attr_Name.Rows)
                            {

                                String duplicate_AttrNames = _duplicateAttrNames["ATTRIBUTE_NAME"].ToString();
                                var Counts=1;
                                if (AttrNameCheck == duplicate_AttrNames)
                                {
                                     Counts = 0;
                                }

                                if (Counts != 0)
                                {

                                    foreach (DataRow dr in attrImportTable.Rows)
                                    {
                                        string check_DupId = dr["ATTRIBUTE_NAME"].ToString();
                                        if (check_DupId.Equals(duplicate_AttrNames))
                                        {
                                            string updateMissingValues = dr["ATTRIBUTE_NAME"].ToString();
                                            if (updateMissingValues != "")
                                            {
                                                updateMissingValues = updateMissingValues + ".|";
                                            }
                                            dr["Missing Values"] = updateMissingValues + "Attribute Name or Caption Name already exists.";
                                            duplicateRows++;
                                        }
                                    }

                                }
                            }
                        }

                    }

                    attrImportTable.AcceptChanges();
                }



                //DataType Validation
                var obj_SqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString);
                using (obj_SqlConnection)
                {
                    obj_SqlConnection.Open();
                    SQLString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        attributeTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + attributeTemp + "] END')";
                    SqlCommand _DBCommand = new SqlCommand(SQLString, obj_SqlConnection);
                    _DBCommand.ExecuteNonQuery();

                    SQLString = CreateTable("[##IMPORTTEMP" + attributeTemp + "]", attrImportTable);
                    _DBCommand = new SqlCommand(SQLString, obj_SqlConnection);
                    _DBCommand.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(obj_SqlConnection)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + attributeTemp + "]"
                    };
                    bulkCopy.WriteToServer(attrImportTable);

                    SqlCommand objSqlCommand = obj_SqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_ATTRIBUTE_IMPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;

                    objSqlCommand.Connection = obj_SqlConnection;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Parameters.Add("@ATTRNAME", SqlDbType.NVarChar, 100).Value = "";
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "DATATYPE VALIDATION";
                    objSqlCommand.Parameters.Add("@CUSTOMER_ID", SqlDbType.Int, 100).Value = 0;
                    objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = attributeTemp;
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int, 100).Value = 0;

                    DataTable attr_datatype = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter(objSqlCommand);
                    sda.Fill(attr_datatype);

                    foreach (DataRow _misMatchedAttrdatatype in attr_datatype.Rows)
                    {
                        String mis_MatchedDatatype = _misMatchedAttrdatatype["ATTRIBUTE_NAME"].ToString();
                        if (!mis_MatchedDatatype.Contains("__") && !mis_MatchedDatatype.Contains("*") && !mis_MatchedDatatype.Contains("%") && !mis_MatchedDatatype.Contains("+") && !mis_MatchedDatatype.Contains("&"))
                        {


                            foreach (DataRow dr in attrImportTable.Rows)
                            {
                                string check_Dup_Datatype = dr["ATTRIBUTE_NAME"].ToString();
                                if (check_Dup_Datatype.Equals(mis_MatchedDatatype))
                                {

                                    string updateMissingValues = dr["ATTRIBUTE_NAME"].ToString();
                                    if (updateMissingValues != "")
                                    {
                                        updateMissingValues = updateMissingValues + ".|";
                                    }
                                    dr["Missing Values"] = updateMissingValues + "Datatype mismatched";
                                    datatype_Validation++;
                                }
                            }
                        }
                        foreach (DataRow Spc in attr_datatype.Rows)
                        {
                            string SpecialCharcterValue = Spc["ATTRIBUTE_NAME"].ToString();
                            foreach (DataRow dr in attrImportTable.Rows)
                            {
                                string checkSpecialcharacter = dr["ATTRIBUTE_NAME"].ToString();
                                if (SpecialCharcterValue == checkSpecialcharacter)
                                {
                                    if (SpecialCharcterValue.Contains("__") || SpecialCharcterValue.Contains("*") || SpecialCharcterValue.Contains("&") || SpecialCharcterValue.Contains("+"))
                                    {
                                        string updateMissingValues = dr["Missing Values"].ToString();
                                        if (updateMissingValues != "")
                                        {
                                            updateMissingValues = updateMissingValues + ".|";
                                        }
                                        dr["Missing Values"] = updateMissingValues + "Attribute name should not contain special characters like (* & % + __ )";
                                        attrMissingColumn++;
                                    }
                                }
                            }

                        }
                    }
                }

                    ///view log page
                System.Web.HttpContext.Current.Session["attrImportTableLog"] = attrImportTable;
                ///Download view log 
                System.Web.HttpContext.Current.Session["ExportErrorTable"] = attrImportTable;

                return "true" + "~" + attrMissingColumn + "~" + missingValues + "~" + duplicateRows + "~" + attrRefExists + "~" + datatype_Validation;


            }
            catch (Exception ex)
            {
                Logger.Error("Error at validateUserImport ", ex);
                return "false";
            }
        }

        [System.Web.Http.HttpPost]
        public System.Web.Mvc.JsonResult AttributeViewLog()
        {
            DashBoardModel model = new DashBoardModel();
            var attrImportTableLog = (DataTable)System.Web.HttpContext.Current.Session["attrImportTableLog"];
            foreach (DataColumn columns in attrImportTableLog.Columns)
            {
                var objPTColumns = new PTColumns();
                objPTColumns.Caption = columns.Caption;
                objPTColumns.ColumnName = columns.ColumnName;
                model.Columns.Add(objPTColumns);
            }
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(attrImportTableLog);
            model.Data = JSONString;
            return new JsonResult() { Data = model };
        }

        [System.Web.Http.HttpPost]
        public HttpResponseMessage importlogs(string Errorlogoutputformat, string fileName)
        {
            try
            {
                if (Errorlogoutputformat.ToLower() == "csv")
                {

                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".csv" : fileName + ".csv");
                }
                else if (Errorlogoutputformat.ToLower() == "text")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".txt" : fileName + ".txt");
                }
                else if (Errorlogoutputformat.ToLower() == "xml")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".xml" : fileName + ".xml");
                }
                else if (Errorlogoutputformat.ToLower() == "xlsx")
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".xlsx" : fileName + ".xlsx");
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, fileName.Contains(".") ? fileName.Split('.')[0] + ".xls" : fileName + ".xls");
                }
            }
            catch (Exception objexception)
            {
                Logger.Error("Error at importApiController : importlogs", objexception);
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region Attribute Import 

        [System.Web.Http.HttpPost]
        public string AttrImport(string importExcelSheetDDSelectionValue, string excelPath, string importType, string importFormat, int catalog_Id)
        {
            try
            {
                int insertRecords = 0; int updateRecords = 0; int skippedRecords = 0; int deleteRecords = 0;
                var customerid = _dbcontext.Customer_User.Where(x => x.User_Name == User.Identity.Name).Select(y => y.CustomerId).FirstOrDefault();
                var ImportTable = new DataTable();
                ImportTable = ConvertExcelToDataTable(excelPath, importExcelSheetDDSelectionValue);
                DataTable attrImportTable = ImportTable.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field => field is System.DBNull || string.Compare((field as string).Trim(), string.Empty) == 0)).CopyToDataTable();

                Stopwatch sw = new Stopwatch();
                sw.Start();
                foreach (DataRow dr in attrImportTable.Rows)
                {
                    string action = dr["ACTION"].ToString();
                    action = action.ToUpper();
                    if (action.Equals("SKIPPED"))
                    {
                        skippedRecords = skippedRecords + 1;
                        dr.Delete();
                    }

                    if (action.Equals("DELETE"))
                    {
                        string del_AttrName = dr["ATTRIBUTE_NAME"].ToString();
                        deleteRecords = deleteRecords + 1;
                        using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                        {
                            SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                            objSqlCommand.CommandText = "STP_LS_ATTRIBUTE_IMPORT";
                            objSqlCommand.CommandType = CommandType.StoredProcedure;
                            objSqlCommand.Connection = objSqlConnection;
                            objSqlCommand.Parameters.Add("@ATTRNAME", SqlDbType.NVarChar, 100).Value = del_AttrName;
                            objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "DELETE";
                            objSqlCommand.Parameters.Add("@CUSTOMER_ID", SqlDbType.Int, 100).Value = customerid;
                            objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                            objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                            objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = "";
                            objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int, 100).Value = "";
                            objSqlConnection.Open();
                            objSqlCommand.ExecuteNonQuery();
                            objSqlConnection.Close();
                        }
                        dr.Delete();

                    }
                }

                attrImportTable.AcceptChanges();



                //if (!attrImportTable.Columns.Contains("UOM"))
                //{
                //    attrImportTable.Columns.Add("UOM");
                //    attrImportTable.Columns["UOM"].SetOrdinal(21);
                //}


                //if (!attrImportTable.Columns.Contains("CREATED USER"))
                //{
                //    attrImportTable.Columns.Add("CREATED USER");
                //    attrImportTable.Columns["CREATED USER"].SetOrdinal(22);
                //}
                //if (!attrImportTable.Columns.Contains("CREATED DATE"))
                //{
                //    attrImportTable.Columns.Add("CREATED DATE");
                //    attrImportTable.Columns["CREATED DATE"].SetOrdinal(23);
                //}
                //if (!attrImportTable.Columns.Contains("MODIFIED USER"))
                //{
                //    attrImportTable.Columns.Add("MODIFIED USER");
                //    attrImportTable.Columns["MODIFIED USER"].SetOrdinal(24);
                //}

                //if (!attrImportTable.Columns.Contains("MODIFIED DATE"))
                //{
                //    attrImportTable.Columns.Add("MODIFIED DATE");
                //    attrImportTable.Columns["MODIFIED DATE"].SetOrdinal(25);
                //}

                if (!attrImportTable.Columns.Contains("ATTRIBUTE_ID"))
                {
                    attrImportTable.Columns.Add("ATTRIBUTE_ID");
                    //  attrImportTable.Columns["ATTRIBUTE_ID"].SetOrdinal(21);

                    //attrImportTable.Columns.Add("CAPTION");
                    //attrImportTable.Columns["CAPTION"].SetOrdinal(22);
                }
                if (!attrImportTable.Columns.Contains("CAPTION"))
                {
                    attrImportTable.Columns.Add("CAPTION");
                    // attrImportTable.Columns["CAPTION"].SetOrdinal(28);
                }

                foreach (DataRow dr in attrImportTable.Rows)
                {
                    if ((dr["ATTRIBUTE_TYPE"].ToString() == "Product Price" || dr["ATTRIBUTE_TYPE"].ToString() == "Family Price") && dr["ATTRIBUTE_DATATYPE"].ToString() == "Number")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "(^-?\\d\\d*$)";
                    }

                    else if ((dr["ATTRIBUTE_TYPE"].ToString() == "Product Image / Attachment" || dr["ATTRIBUTE_TYPE"].ToString() == "Family Image / Attachment" || dr["ATTRIBUTE_TYPE"].ToString() == "Family Description") && dr["ATTRIBUTE_DATAFORMAT"].ToString() == "All Characters" && dr["ATTRIBUTE_DATATYPE"].ToString() == "Text")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString() == "Date and Time")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-";
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString() == "HyperLink")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "^(https?://)|^(HTTPS?://)|^(Https?://)?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+\\.)*([0-9a-";
                    }
                    else if (dr["ATTRIBUTE_DATAFORMAT"].ToString() == "Integer")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "(^-?\\d\\d*$)";
                    }
                    else if (dr["ATTRIBUTE_DATAFORMAT"].ToString() == "Real Numbers")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "^-{0,1}?\\d*\\.{0,1}\\d{0,6}$";
                    }
                    else if (dr["ATTRIBUTE_DATAFORMAT"].ToString() == "Alpha Numeric")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "^[ 0-9a-zA-Z\\r\\n]*$";
                    }
                    else if (dr["ATTRIBUTE_DATAFORMAT"].ToString() == "Alphabets Only")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "^[ a-zA-Z\\r\\n]*$";
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString() == "Date and Time" && dr["ATTRIBUTE_DATAFORMAT"].ToString() == "Short Format (dd/mm/yyyy)")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-";
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString() == "Date and Time" && dr["ATTRIBUTE_DATAFORMAT"].ToString() == "Short Format (mm/dd/yyyy)")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{3}(?:0[1-9]|[12]\\d|3[01]))(?=.{0}(?:0[1-9]|1[0-2]))(?!.{0}(?:0[2469]|11)-31)(?!.{0}02-30)(?!02(\\-";
                    }
                    else if (dr["ATTRIBUTE_DATATYPE"].ToString() == "Date and Time" && dr["ATTRIBUTE_DATAFORMAT"].ToString() == "Long Format")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4}\\s\\d{2}(\\-|\\:)\\d\\d(\\-|\\:)\\d{2}\\s\\w{2})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-";
                    }
                    else if (dr["ATTRIBUTE_DATAFORMAT"].ToString() == "System default")
                    {
                        dr["ATTRIBUTE_DATAFORMAT"] = "(^-?dd*$)";
                    }


                }

                foreach (DataRow dr in attrImportTable.Rows)
                {
                    string attrType = dr["ATTRIBUTE_TYPE"].ToString();

                    if (attrType.ToUpper() == "ITEM SPECIFICATIONS")
                    {
                        dr["ATTRIBUTE_TYPE"] = "1";
                    }
                    else if (attrType.ToUpper() == "ITEM IMAGE/ATTACHMENT")
                    {
                        dr["ATTRIBUTE_TYPE"] = "3";
                    }
                    else if (attrType.ToUpper() == "ITEM PRICE")
                    {
                        dr["ATTRIBUTE_TYPE"] = "4";
                    }
                    else if (attrType.ToUpper() == "ITEM KEY")
                    {
                        dr["ATTRIBUTE_TYPE"] = "6";
                    }
                    else if (attrType.ToUpper() == "PRODUCT DESCRIPTION")
                    {
                        dr["ATTRIBUTE_TYPE"] = "7";
                    }
                    else if (attrType.ToUpper() == "PRODUCT IMAGE/ATTACHMENT")
                    {
                        dr["ATTRIBUTE_TYPE"] = "9";
                    }
                    else if (attrType.ToUpper() == "PRODUCT SPECIFICATIONS")
                    {
                        dr["ATTRIBUTE_TYPE"] = "11";
                    }
                    else if (attrType.ToUpper() == "PRODUCT PRICE")
                    {
                        dr["ATTRIBUTE_TYPE"] = "12";
                    }
                    else if (attrType.ToUpper() == "PRODUCT KEY")
                    {
                        dr["ATTRIBUTE_TYPE"] = "13";
                    }
                    else if (attrType.ToUpper() == "CATEGORY SPECIFICATIONS")
                    {
                        dr["ATTRIBUTE_TYPE"] = "21";
                    }
                    else if (attrType.ToUpper() == "CATEGORY DESCRIPTION")
                    {
                        dr["ATTRIBUTE_TYPE"] = "23";
                    }

                    else
                    {
                        dr["ATTRIBUTE_TYPE"] = "25";
                    }
                }
                attrImportTable.AcceptChanges();

                using (var objSqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ConnectionString))
                {
                    attributeTemp = Guid.NewGuid().ToString();
                    objSqlConnection.Open();
                    SQLString =
                        "EXEC('if exists(select NAME from TEMPDB.sys.objects where type=''u'' and name=''[##IMPORTTEMP" +
                        attributeTemp + "]'')BEGIN DROP TABLE [##IMPORTTEMP" + attributeTemp + "] END')";
                    SqlCommand _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                    _DBCommand.ExecuteNonQuery();

                    SQLString = CreateTable("[##IMPORTTEMP" + attributeTemp + "]", attrImportTable);
                    _DBCommand = new SqlCommand(SQLString, objSqlConnection);
                    _DBCommand.ExecuteNonQuery();
                    var bulkCopy = new SqlBulkCopy(objSqlConnection)
                    {
                        DestinationTableName = "[##IMPORTTEMP" + attributeTemp + "]"
                    };
                    bulkCopy.WriteToServer(attrImportTable);

                    SqlCommand objSqlCommand = objSqlConnection.CreateCommand();
                    objSqlCommand.CommandText = "STP_LS_ATTRIBUTE_IMPORT";
                    objSqlCommand.CommandType = CommandType.StoredProcedure;
                    objSqlCommand.Connection = objSqlConnection;
                    objSqlCommand.CommandTimeout = 0;
                    objSqlCommand.Parameters.Add("@CREATED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.Parameters.Add("@MODIFIED_USER", SqlDbType.NVarChar, 100).Value = User.Identity.Name;
                    objSqlCommand.Parameters.Add("@SESSION_ID", SqlDbType.NVarChar, 500).Value = attributeTemp;
                    objSqlCommand.Parameters.Add("@CUSTOMER_ID", SqlDbType.Int, 100).Value = customerid;
                    objSqlCommand.Parameters.Add("@OPTION", SqlDbType.NVarChar, 100).Value = "IMPORT";
                    objSqlCommand.Parameters.Add("@ATTRNAME", SqlDbType.NVarChar, 100).Value = "";
                    objSqlCommand.Parameters.Add("@CATALOG_ID", SqlDbType.Int, 100).Value = catalog_Id;
                    SqlDataAdapter sde = new SqlDataAdapter(objSqlCommand);
                    DataSet import_Count = new DataSet();
                    sde.Fill(import_Count);
                    if (import_Count.Tables.Count > 2)
                    {
                        updateRecords = Convert.ToInt32(import_Count.Tables[0].Rows[0]["updated"]) + Convert.ToInt32(import_Count.Tables[1].Rows[0]["updated"]);
                        insertRecords = Convert.ToInt32(import_Count.Tables[2].Rows[0]["inserted"]);
                    }
                    else if (import_Count.Tables.Count == 2)
                    {
                        updateRecords = Convert.ToInt32(import_Count.Tables[0].Rows[0]["updated"]);
                        insertRecords = Convert.ToInt32(import_Count.Tables[1].Rows[0]["inserted"]);
                    }
                    else
                    {
                        updateRecords = Convert.ToInt32(import_Count.Tables[0].Rows[0]["updated"]);

                    }


                }
                sw.Stop();
                string customer_Name = User.Identity.Name;
                string calcualteImportTiminings = string.Format("{0}:{1}:{2}", sw.Elapsed.Hours, sw.Elapsed.Minutes, sw.Elapsed.Seconds);
                return "true" + "~" + insertRecords + "~" + updateRecords + "~" + calcualteImportTiminings + "~" + skippedRecords + "~" + deleteRecords + "~" + customer_Name;
            }
            catch (Exception ex)
            {
                Logger.Error("Error at AttributeApiController : AttrImport ", ex);
                return "false";
            }
        }

        #endregion

    }

}

