﻿using Infragistics.Documents.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace LS.Web
{
    /// <summary>
    /// Summary description for DownloadBackup
    /// </summary>
    public class DownloadBackup : IHttpHandler
    {
        //------------------------------------------Seperated path values initializing------------------------------------------------------//
        public string pathCheckFlag = System.Web.Configuration.WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
        public string pathServerValue = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDriveURL"].ToString();
        public string serverPathShareVal = System.Web.Configuration.WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
        //------------------------------------------Seperated path values initializing------------------------------------------------------//

        public void ProcessRequest(HttpContext context1)
        {
            HttpContext context = System.Web.HttpContext.Current;
            string CustomerFolder = HttpContext.Current.Request.QueryString["CustomerFolder"];
            HttpResponse response = System.Web.HttpContext.Current.Response;
            var folderVal = CustomerFolder.Split('\\');
            var workbook = new Workbook();
            string filename = (string)System.Web.HttpContext.Current.Session["FileName"];
            filename = filename + ".xls";
            var selectedItems = (List<string>)System.Web.HttpContext.Current.Session["Files"];
            if (selectedItems == null)
                selectedItems = new List<string>();



            foreach (string table in selectedItems)
            {
                if (context.Session[table] != null)
                {
                    var distinctdsFam = (DataTable)context.Session[table];
                    if (!distinctdsFam.Columns.Contains("Action"))
                    {
                        distinctdsFam.Columns.Add("Action", typeof(Boolean)).SetOrdinal(0);
                    }
                    var tableFam = distinctdsFam.DefaultView.ToTable(true);
                    context.Session[table] = null;
                    if (tableFam.Rows.Count > 0)
                    {
                        string tableName1 = table;
                        string tableName = table;
                        int rowcntTemp = tableFam.Rows.Count;
                        int j = 0;
                        int runningcnt = 0;
                        do
                        {
                            int rowcnt;
                            if (rowcntTemp <= 65000)
                            {
                                rowcnt = rowcntTemp;
                                rowcntTemp = rowcnt - 65000;
                            }
                            else
                            {
                                rowcnt = 65000;
                                rowcntTemp = tableFam.Rows.Count - (runningcnt + 65000);
                            }
                            j++;
                            if (j != 1)
                            {
                                tableName = tableName1 + (j - 1);
                            }
                            var worksheet = workbook.Worksheets.Add(tableName);
                            workbook.WindowOptions.SelectedWorksheet = worksheet;
                            for (int jj = 1; jj <= rowcnt; jj++)
                                for (int columnIndex = 0; columnIndex < tableFam.Columns.Count; columnIndex++)
                                {
                                    if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "CATALOG_ITEM_NO" || HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "ITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) == "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpContext.Current.Session["CustomerSubItemNo"];
                                    }
                                    else if (HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "CATALOG_ITEM_NO" && HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "ITEM#" && HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName) != "SUBITEM#")
                                    {
                                        worksheet.Rows[1].Cells[columnIndex].Value = HttpUtility.HtmlDecode(tableFam.Columns[columnIndex].ColumnName);
                                    }
                                }
                            int rowIndex = 2;
                            int temprunningcnt = runningcnt;
                            for (int k = runningcnt; k < (temprunningcnt + rowcnt); k++)
                            {
                                var row = worksheet.Rows[rowIndex++];
                                runningcnt++;
                                for (int columnIndex = 0; columnIndex < tableFam.Rows[k].ItemArray.Length; columnIndex++)
                                {
                                    row.Cells[columnIndex].Value = tableFam.Rows[k].ItemArray[columnIndex];
                                }
                            }
                        }
                        while (rowcntTemp > 0);
                    }
                    else
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("No records found.");
                    }
                }
            }

            System.Web.HttpContext.Current.Session["Files"] = null;

            if (workbook.Worksheets[0] != null)
            {
                var worksheet1 = workbook.Worksheets[0];
                workbook.WindowOptions.SelectedWorksheet = worksheet1;
            }
            var filePath = "";
            var fileExt = "";

            filePath = context.Server.MapPath("~/Content/ProductImages/" + folderVal[2] + "/BacupFiles/");
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            workbook.Save(filePath + filename);
            if (pathCheckFlag.ToLower() == "true")
            {
                fileExt = serverPathShareVal + "/Content/ProductImages/" + folderVal[2] + "/BacupFiles/";
                if (!Directory.Exists(fileExt))
                {
                    Directory.CreateDirectory(fileExt);
                }
                workbook.Save(fileExt + filename);
            }
            if (System.IO.File.Exists(filePath + filename))
            {
                response.ClearContent();
                response.Clear();
                response.ContentType = "application/vnd.ms-excel";
                response.AddHeader("Content-Disposition", string.Format("attachment; filename = \"{0}\"", filename));
                response.TransmitFile(filePath + filename);
                response.BufferOutput = true;
                response.Flush();
                if (pathCheckFlag.ToLower() == "true")
                {
                    File.Delete(filePath + filename);
                }
                response.End();
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("File not found!");
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}