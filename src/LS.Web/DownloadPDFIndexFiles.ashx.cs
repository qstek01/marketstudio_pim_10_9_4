﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Configuration;

namespace LS.Web
{
    /// <summary>
    /// Summary description for DownloadPDFIndexFiles
    /// </summary>
    public class DownloadPDFIndexFiles : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            HttpResponse response = context.Response;
            string sessionId = HttpContext.Current.Request.QueryString["SessionId"];
            string filePath = ConfigurationManager.AppSettings["outputIndex"];
            if (Directory.Exists(filePath))
            {
                string fileName = "FinalPDF" + sessionId + ".pdf";
                string file = filePath + @"\FinalPDF" + sessionId + ".pdf";
                response.ClearContent();
                response.Clear();
                response.ContentType = "text/plain";
                response.AddHeader("Content-Disposition",
                                   "attachment; filename=" + fileName + ";");
                response.TransmitFile(file);
                response.Flush();
                response.End();


            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("File not found!");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}