﻿using LS.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;


namespace LS.Web
{
    /// <summary>
    /// Summary description for DownloadExportBatchFile
    /// </summary>
    public class DownloadExportBatchFile : IHttpHandler
    {
        readonly CSEntities _dbcontext = new CSEntities();
        string connectionString = ConfigurationManager.ConnectionStrings["LSAppDBConnection"].ToString();
        //  SqlCommand sqlCommand = new SqlCommand();  
       
        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();



        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            HttpResponse response = context.Response;

            string folderPath = HttpContext.Current.Request.QueryString["Path"];
            string ExportId = HttpContext.Current.Request.QueryString["ExportBatchId"];

            int ExP_Id = int.Parse(ExportId);
            String query = "select Comments from Customers where CustomerId in (select CustomerId from Customer_User where  user_Name in (select customer_name from EXPORTBATCHPROCESS where EXPORTID = '" + ExP_Id + "'))";

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd1 = new SqlCommand(query, con);
            con.Open();
            var user_Name = (string)cmd1.ExecuteScalar();
            con.Close();


            // Separate path --- Start
           


            // To pass dynamic path location from web config   - Start
            string IsServerUpload = WebConfigurationManager.AppSettings["UseExternalAssetDrive"].ToString();
            string pathDesignation = WebConfigurationManager.AppSettings["ExternalAssetDrivePath"].ToString();
            string pathUrl = WebConfigurationManager.AppSettings["ExternalAssetDriveURL"];

            string path = string.Empty;
            string destinationPath;

            string filePath = string.Empty;

            // Seprate Path -- End
            if (IsServerUpload.ToUpper() == "TRUE")
            {
                destinationPath = pathDesignation + "\\Content\\ProductImages\\" + user_Name + "\\Export\\";
                destinationPath = destinationPath.Replace("////", "\\\\");
                destinationPath = destinationPath.Replace("/", "\\");
            }
            else
            {
               
                destinationPath =  context.Server.MapPath("~/Content\\ProductImages\\" + user_Name + "\\Export\\");   // exportFilePath + "\\Content\\ProductImages\\" + user_Name + "\\Export\\";
            }

            string _xmlpath1 = destinationPath;
          
            string filename = System.IO.Path.GetFileName(folderPath);
            _xmlpath1 = string.Format("{0}{1}", _xmlpath1, folderPath);
            if (File.Exists(_xmlpath1))
            {
                //var path = context.Session["Xmlpath"] + filename;
                response.ClearContent();
                response.Clear();
                response.ContentType = "text/plain";
                response.AddHeader("Content-Disposition",
                                   "attachment; filename=" + filename + ";");
                // response.TransmitFile((context.Session["Xmlpath"] + filename));
                response.TransmitFile(_xmlpath1);
                response.Flush();
                response.End();
            }
            else
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("File not found!");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}