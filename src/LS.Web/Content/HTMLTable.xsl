<?xml version="1.0" encoding="utf-16"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" indent="no" />
  <xsl:template match="/">
    <html>
      <h2></h2>
      <h3></h3>
      <body>
        <head>
          <!--<link href="master.css" media="screen" rel="stylesheet" type="text/css"/>-->
        </head>
        
        <table class="table table-condensed table-bordered table-striped">
          <xsl:for-each select="products/table/row">
            <tr >
              <xsl:apply-templates
         select=".|following-sibling::Cell[position()]"/>
           </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="Cell">
      <xsl:variable name="rspan" select="./@crows" />
      <xsl:variable name="cspan" select="./@ccols" />
      <xsl:variable name="imagePath" select="./@imagePath" />
    

    <xsl:choose>
      
        <xsl:when test="./@cellArea='Header' and ./@imagePath=''">
          <th scope="col" colspan="{$cspan}" rowspan="{$rspan}">
            <xsl:value-of select="."/>
          </th>
        </xsl:when>
      
      <xsl:when test="./@cellArea='Header' and ./@imagePath!=''">
        <th scope="col" colspan="{$cspan}" rowspan="{$rspan}">
          <img width="75" height="75" src="{$imagePath}" />
        </th>
      </xsl:when>

      <xsl:when test="./@cellArea='ColumnField' and ./@imagePath=''">
        <th scope="col" colspan="{$cspan}" rowspan="{$rspan}">
          <xsl:value-of select="."/>
         
        </th>
      </xsl:when>
      
      <xsl:when test="./@cellArea='ColumnField' and ./@imagePath!=''">
        <th scope="col" colspan="{$cspan}" rowspan="{$rspan}">
          <img width="75" height="75" src="{$imagePath}" />
        </th>
      </xsl:when>

      <xsl:when test="./@cellArea='RowField' and ./@rowID='0' and ./@imagePath=''">
        <th class="spec" scope="row" colspan="{$cspan}" rowspan="{$rspan}">
          <xsl:value-of select="."/>
         
        </th>
      </xsl:when>
      
      <xsl:when test="./@cellArea='RowField' and ./@rowID='0' and ./@imagePath!=''">
        <th class="spec" scope="row" colspan="{$cspan}" rowspan="{$rspan}">
          <img width="75" height="75" src="{$imagePath}" />
        </th>
      </xsl:when>

      <xsl:when test="./@cellArea='RowField' and ./@rowID!='0' and ./@imagePath=''">
        <th class="specalt" scope="row" colspan="{$cspan}" rowspan="{$rspan}">
          <xsl:value-of select="."/>
        
        </th>
      </xsl:when>
      
      <xsl:when test="./@cellArea='RowField' and ./@rowID!='0' and ./@imagePath!=''">
        <th class="specalt" scope="row" colspan="{$cspan}" rowspan="{$rspan}">
          <img width="75" height="75" src="{$imagePath}" />
        </th>
      </xsl:when>

      <xsl:when test="./@cellArea='SummaryField' and ./@rowID='0' and ./@imagePath=''">
        <td class="reg" colspan="{$cspan}" rowspan="{$rspan}">
          <xsl:value-of select="."/>
         </td>
      </xsl:when>
      
      <xsl:when test="./@cellArea='SummaryField' and ./@rowID='0' and ./@imagePath!=''">
        <td class="reg" colspan="{$cspan}" rowspan="{$rspan}">
          <img width="75" height="75" src="{$imagePath}" />
        </td>
      </xsl:when>

      <xsl:when test="./@cellArea='SummaryField' and ./@rowID!='0' and ./@imagePath=''">
        <td class="alt" colspan="{$cspan}" rowspan="{$rspan}">
          <xsl:value-of select="."/>
        </td>
      </xsl:when>
      
      <xsl:when test="./@cellArea='SummaryField' and ./@rowID!='0' and ./@imagePath!=''">
        <td class="alt" colspan="{$cspan}" rowspan="{$rspan}">
          <img width="75" height="75" src="{$imagePath}" />
        </td>
      </xsl:when>

      <xsl:when test="./@cellArea='SummaryHeader' and ./@imagePath=''">
        <th class="nobg"  colspan="{$cspan}" rowspan="{$rspan}">
          <xsl:value-of select="."/>
        </th>
      </xsl:when>
      
        <xsl:when test="./@cellArea='SummaryHeader' and ./@imagePath!=''">
        <th class="nobg"  colspan="{$cspan}" rowspan="{$rspan}">
          <img width="75" height="75" src="{$imagePath}" />
        </th>
      </xsl:when>
      <xsl:otherwise>
        <td class="alt" valign="middle" align="center" colspan="{$cspan}" rowspan="{$rspan}">
          <xsl:value-of select="."/>
        </td>
      </xsl:otherwise>
    </xsl:choose>
    
    
    </xsl:template>
</xsl:stylesheet>
