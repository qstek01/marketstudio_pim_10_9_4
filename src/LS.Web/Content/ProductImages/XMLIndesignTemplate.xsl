<?xml version="1.0" encoding="utf-16"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:aid="http://ns.adobe.com/AdobeInDesign/4.0/" xmlns:aid5="http://ns.adobe.com/AdobeInDesign/5.0/">
  <xsl:template match="/">
    <PRODUCT_GROUP nrows="{products/@nrows}" ncols="{products/@ncols}" TBGUID="{products/@TBGUID}" Format="{products/table/@FORMAT}">
      <TABLE xmlns:aid="http://ns.adobe.com/AdobeInDesign/4.0/" nrows="{products/@nrows}" ncols="{products/@ncols}" TBGUID="{products/table/@TBGUID}" Format="{products/table/@FORMAT}" Transpose="{products/table/@TRANSPOSE}" aid5:tablestyle="TABLE" aid:trows="{products/table/@trows}" aid:tcols="{products/table/@tcols}"  aid:table="table">
          <xsl:for-each select="products/table/row">
              <xsl:apply-templates
         select=".|following-sibling::Cell[position()]"/>
          </xsl:for-each>
      </TABLE>
    </PRODUCT_GROUP>
    </xsl:template>
  <xsl:template match="Cell">
    <xsl:variable name="CellName" select="./@cellStyle" />
    <xsl:element name="{$CellName}" >
      <xsl:attribute name="aid:ccols">
        <xsl:value-of select="./@ccols"/>
      </xsl:attribute>
      <xsl:attribute name="aid:crows">
        <xsl:value-of select="./@crows"/>
      </xsl:attribute>
      <xsl:attribute name="aid5:cellstyle">
        <xsl:value-of select="./@cellStyle"/>
      </xsl:attribute>
      <xsl:attribute name="TBGUID">
        <xsl:value-of select="./@TBGUID"/>
      </xsl:attribute>
      <xsl:attribute name="product_id">
        <xsl:value-of select="./@product_ID"/>
      </xsl:attribute>
      
      <xsl:attribute name="COTYPE">CELL</xsl:attribute>
      
      <xsl:if test="@theader">
      <xsl:attribute name="aid:theader"></xsl:attribute>
      </xsl:if>
                
      <xsl:if test="@attribute_id">
        <xsl:attribute name="attribute_id">
          <xsl:value-of select="./@attribute_id"/>
        </xsl:attribute>
      </xsl:if>

      <xsl:if test="@Attribute_type">
        <xsl:attribute name="Attribute_type">
          <xsl:value-of select="./@Attribute_type"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:attribute name="aid:table">cell</xsl:attribute>

      <xsl:value-of select="."/>
      <xsl:if test="@EmbedImage">
        <xsl:element name="Image_Name" >

        <xsl:attribute name="TBGUID">
          <xsl:value-of select="Cell/@TBGUID"/>
        </xsl:attribute>

          <xsl:attribute name="product_id">
            <xsl:value-of select="Cell/@product_ID"/>
          </xsl:attribute>

          <xsl:if test="Cell/@Attribute_type">
            <xsl:attribute name="Attribute_type">
              <xsl:value-of select="Cell/@Attribute_type"/>
            </xsl:attribute>
            
          </xsl:if>

          <xsl:if test="Cell/@attribute_id">
            <xsl:attribute name="attribute_id">
              <xsl:value-of select="Cell/@attribute_id"/>
            </xsl:attribute>
          </xsl:if>
          
       <xsl:if test="Cell/@IMAGE_FILE">
         <xsl:attribute name="IMAGE_FILE">
           <xsl:value-of select="Cell/@IMAGE_FILE"/>
         </xsl:attribute>
        </xsl:if>
      </xsl:element>
      </xsl:if>
      
    </xsl:element>
    
    </xsl:template>
  
  <xsl:output method="xml" version="1.0" indent="no" cdata-section-elements="Cell"/>
</xsl:stylesheet>
<!--<![CDATA[]]>-->