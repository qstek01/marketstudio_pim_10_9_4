﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;

namespace LS.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure();
            Stimulsoft.Base.StiLicense.Key = "6vJhGtLLLz2GNviWmUTrhSqnOItdDwjBylQzQcAOiHnGcQgpiN5admIHPSPe55F+Z2bB1CwfxX9AWhXUwJ28M6V6lf" +
"IeHcG34fG5g0qbLN3Nqc6pZNHfAAN//zvalzKc8GRjrSOyhXJjeSqO7+tPMSw2AXiS1iFr6axFLrqpxvhGvCmLO4C2" +
"xpKt0rfd9aweQTEMzXhcm0q+dSEfkG7/2rDJfMNmOdCTeCH9/18EHiWsR33/Sv6lvZgHfm5mldfNCArY8gs31WqM0p" +
"xjyeuLH/rwofOJGDALpTJR9/+2JvPIHfXw8W5NujlA5HOh1yZtCiLWu6PUwGqdVVyqsuaJAzCuC/d8PxvWzvmYyh9H" +
"aVLGy7M+h8QSjWHo4YKklc060Z4XZGZCRvAbWUvTT2/4wFTSJcikAGVpbu49V5ms5z6rbJUlIY8DURhqRqG/Pkh3rJ" +
"tfLIeJcylyCs3Z333aC7KPhszLSTOOGv/h0vmG98hBCdyODMVs3FnJ3StI75qRol1rFN4P+W6BP4RvR3dkRqqpK5PW" +
"IG0p2CQt4UgD5Aeimz55N9DiklaDeljtCH4sTWFbKgI2bN0CkGPDYpND5CGMr/KOL6P4TJWmKzl2/f+0zJbzsJAV2p" +
"JJozCXvR0JJkaJTOU/HIn9NzspHQH1LrBv9e4WlrFkzle7GUOtgRaptrfKCEXF3wYH+mB8DRM2";
        }
        protected void Application_PostAuthorizeRequest()
        {
            HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
        }
        void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError();

            if (exc.GetType() == typeof(HttpUnhandledException))
            {

            }
        }
    }
}
