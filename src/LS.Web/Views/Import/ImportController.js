﻿LSApp.controller('ImportController', ['$scope', 'dataFactory', '$http', '$compile', '$rootScope', '$timeout', 'ngTableParams', 'blockUI', '$localStorage', '$location', function ($scope, dataFactory, $http, $compile, $rootScope, $timeout, ngTableParams, blockUI, $localStorage, $location) {
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    $scope.UploadFile = null;
    $scope.createTemplate = false;
    $scope.choosefileimp = true;
    $scope.Atrmappingimp = true;
    $scope.impvalidation = true;
    $scope.import = true;
    $scope.templateDetails = true;
    $scope.logim = true;
    $scope.NextToTemplate = false;
    $scope.ImportFormat = "XLS";
    $scope.Errorlogoutputformat = "XLS";
    $scope.IsFormSubmitted = false;
    $scope.fileChoose = false;
    $scope.AttributeMappingdiv = false;
    $scope.excelPath = "";
    $scope.passvalidationNext = true;
    $scope.allowDuplication = "0";
    $scope.importErrorWindowdiv = false;
    $scope.importErrorWindowfortabledesignerdiv = false;
    $scope.importpicklistdiv = false;
    $scope.importErrorFileWindowdiv = false;
    $scope.sheetvalidationdiv = false;
    $scope.importSuccessWindowdiv = false;
    $scope.ProcessName = "";
    $scope.ResultDisplay = false;
    $scope.ResultDisplayfortabledesigner = false;
    $scope.missingColumnInValid = false;
    $scope.validationPassed = false;
    $scope.deleteEnable = true;
    $scope.showValidatediv = false;
    $scope.templateFirst = true;
    $scope.templatesecond = false;
    $scope.attrMap = false;
    $scope.nextSheetProcess = false;
    $scope.importCompletedStatus = true;
    $scope.ValidateDiv = false;
    $scope.validationSuccess = true;
    $scope.ImportType = "";
    $scope.sampleEnable = false;
    $scope.EnableBatch = false;
    $scope.BatchImport = false;
    $scope.ItemImport = false;
    $scope.validated = { Column: true, Id: true, Picklist: true, dataType: true, subcatalog: true };
    $scope.displayFileName = "";
    $scope.ValidationBackdiv = true;
    $scope.scheduleImport = false;
    /*********Product input screen changes by Aswin *********///
    $scope.validationSelectionSheet = [];
    $scope.importSelectionSheet = [];
    $scope.mappingAttributes = [];
    $scope.showMappingMessage = false;
    $scope.showImportMessage = false;
    $scope.disable_validateimportimportTypeSelection = true;
    $scope.groupAttributeSelectionSheet = [];
    $scope.ShowGroupAttributesGrid = false;
    $scope.ShowNoGroupAttributes = false;
    $scope.saveGroupNameFlag = "1"
    $scope.selectedAttributeType = "undefined";
    $localStorage.CopymappingAttributes = [];
    /*********Product input screen changes by Aswin *********///

    $scope.radioButtonYes = function () {
        $scope.allowDuplication = "1";
    };
    $scope.radioButtonNo = function () {
        $scope.allowDuplication = "0";
    };

    $scope.allowDuplication1 = "0";
    $scope.radioButtonYes1 = function () {
        $scope.allowDuplication1 = "1";
    };
    $scope.radioButtonNo1 = function () {
        $scope.allowDuplication1 = "0";
    };
    $scope.importLog = {

        CatalogName: "",
        InsertedRecords: 0,
        UpdatedRecords: 0,
        DeletedRecords: 0,
        TimeTaken: "",
        SkippedRecords: 0
    }

    //Get Catalogs of the customers
    $scope.ImportcatalogDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetCurretUserInfo().success(function (response) {
                    $rootScope.currentUser = response;
                    dataFactory.GetCatalogDetailsForCurrentCustomer($scope.SelectedItem, $rootScope.currentUser.CustomerDetails.CustomerId).success(function (cResponse) {
                        options.success(cResponse);
                    }).error(function (error) {
                        options.error(error);
                    });
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $('#drpImportType').kendoDropDownList({
        dataSource: [
            { Format: "XLS", isDisabled: false },
            { Format: "XLSX", isDisabled: false },
            { Format: "CSV", isDisabled: true },
            { Format: "XML", isDisabled: true }
        ],
        dataTextField: "Format",
        dataValueField: "Format",
        select: function (e) {

            if (e.dataItem.isDisabled) {
                e.preventDefault();
            }
            else {
                $scope.ImportFormat = e.sender.text();
            }
        },
        change: function (data) {
            $scope.ImportFormat = data.sender.text();
        },
        template: kendo.template($("#template").html())
    });
    $('#errorLog').kendoDropDownList({
        dataSource: [
            { Format: "XLS", isDisabled: false },
            { Format: "XLSX", isDisabled: false },
            { Format: "CSV", isDisabled: true },
            { Format: "XML", isDisabled: true }
        ],
        dataTextField: "Format",
        dataValueField: "isDisabled",
        select: function (e) {

            if (e.dataItem.isDisabled) {
                e.preventDefault();
            }
        },
        change: function (data) {
            $scope.Errorlogoutputformat = data.sender.text();
        },
    });

    //Get Existing template of the customers
    $scope.GetTemplateDetailsImport = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetTemplateForImport().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.selectFileforUploadRead = function (file) {
        this.value = null;
        $rootScope.fileHeader = XLSX.utils.sheet_to_json(file.Sheets[file.SheetNames[0]], { header: 1 })[0];
        $rootScope.fileData = XLSX.utils.sheet_to_json(file.Sheets[file.SheetNames[0]]);
        $scope.SelectedFileForUpload = file[0];
        $scope.$apply(function () {
            $scope.SelectedFileForUploadnamemainproduct = file[0].name;
            $rootScope.SelectedFileForUploadnamemain = file[0].name;
            if ($rootScope.SelectedFileForUploadnamemain.length <= 40) {
                $scope.displayFileName = $rootScope.SelectedFileForUploadnamemain;
            }
            else {
                $scope.displayFileName = $rootScope.SelectedFileForUploadnamemain.substring(0, 40) + "." + $rootScope.SelectedFileForUploadnamemain.split('.')[1];
            }
            $scope.IsFormSubmitted = true;
            $scope.fileChoose = true;
        });
    };
    $scope.selectFileforUpload = function (file) {
        this.value = null;
        $scope.SelectedFileForUpload = file[0];
        $scope.$apply(function () {
            $scope.SelectedFileForUploadnamemainproduct = file[0].name;
            $rootScope.SelectedFileForUploadnamemain = file[0].name;
            if ($rootScope.SelectedFileForUploadnamemain.length <= 40) {
                $scope.displayFileName = $rootScope.SelectedFileForUploadnamemain;
            }
            else {
                $scope.displayFileName = $rootScope.SelectedFileForUploadnamemain.substring(0, 40) + "." + $rootScope.SelectedFileForUploadnamemain.split('.')[1];
            }
            $scope.IsFormSubmitted = true;
            $scope.fileChoose = true;
        });
    };

    $scope.ResetPage = function () {
        $scope.SelectedFileForUpload = null;
        $scope.TemplateNameImport = "";
        $scope.backbutton = false;
        $scope.fileName = null;
        var $el = $('#importfile1');
        if ($el.val() != "") {
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
        }
        $rootScope.SelectedFileForUploadnamemain = "";
        $scope.SelectedFileForUploadnamemainsam = "";
        var dropdownlist = $("#drpTemplateDetails").data("kendoDropDownList");
        dropdownlist.select(0);
        $scope.ImportcatalogDataSource.read();
        var dropdownlistCatalog = $("#catalogDrpDownLisr").data("kendoDropDownList");
        dropdownlistCatalog.select(0);
        var dropdownlistType = $("#drpImportType").data("kendoDropDownList");
        dropdownlistType.select(0);
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#Atrmappingimp").removeClass("leftMenuactive");
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#choosefileimp").addClass("leftMenuactive");
        $scope.templateDetails = true;
        $scope.AttributeMappingdiv = false;
        $scope.fileChoose = false;
        $scope.showValidatediv = false;
        $scope.templateNamedisabled = false;
        $scope.ImportcatalogIdAttr = "";
        $scope.GetTemplateDetailsImport.read();
        $("#catalogDrpDownLisr").data("kendoDropDownList").enable(true);
        $("#drpImportType").data("kendoDropDownList").enable(true);
        if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
            $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        /**Product input form changes by Aswin *****/
        $scope.showMappingMessage = false;
        $scope.showImportMessage = false;
        $scope.sampleEnable = false;
        /**Product input form changes by Aswin *****/
    };

    $scope.SaveFile = function (templateDetails) {


        blockUI.start();
        $scope.IsFormSubmitted = true;
        $scope.Message = "";
        /*********Product input screen changes by Aswin *********///
        $scope.modifyTemplateFlag = "0";
        if ($scope.fileName != null || $scope.fileName == 'undefined') {
            $scope.modifyTemplateFlag = "1";
            //$rootScope.SelectedFileForUploadnamemain = $scope.fileName;
        }

        if ($scope.SelectedFileForUpload == null || $scope.SelectedFileForUpload == undefined) {

            dataFactory.ModifyTemplateDetails($scope.fileName).success(function (response) {
                if (response.Data == "0") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'The selected import file is not found.',
                    });
                }
                else {
                    $scope.modifyTemplateFlag = "2";
                    $scope.excelPath = response.Data;
                    $rootScope.SelectedFileForUploadnamemain = $scope.fileName;
                    $scope.importExcelSheetSelction.read();

                    $scope.templateDetails = false;
                    $("#importSelectionTable").css("margin-top", "0px");
                    $scope.AttributeMappingdiv = true;
                    $scope.ProcessName = "Map Attributes";
                    $("#choosefileimp").removeClass("leftMenuactive");
                    $("#Atrmappingimp").addClass("leftMenuactive");
                }
                blockUI.stop();
            }).error(function (error) {
                options.error(error);
            });




        }
            /*********Product input screen changes by Aswin *********///
        else if ($scope.SelectedFileForUpload !== null) {
            $scope.ImportFormat = $('#drpImportType').data("kendoDropDownList").text();
            var fileExtension = $scope.SelectedFileForUpload.name.split('.')[1];
            if (fileExtension == 'txt') {
                fileExtension = 'csv';
            }
            if ($scope.ImportFormat.toString().toUpperCase() != fileExtension.toString().toUpperCase()) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose a valid file.',
                });
                blockUI.stop();
                return true;
            }
            $scope.ChechFileValid($scope.SelectedFileForUpload);
            $scope.UploadFile($scope.SelectedFileForUpload);
        }

        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please choose a file.',
            });
            blockUI.stop();
        }
    };


    $scope.Catalogchange = function (e) {
        $scope.sampleEnable = false;
    }
    $scope.SelectedValues = [];
    $scope.ChechFileValid = function (file) {
        var isValid = false;
        $scope.enablevalidation();
        if ($scope.SelectedFileForUpload != null) {
            if (file.name.contains(".xls") || file.name.contains(".xlsx")) {
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
        }
        else {
            $scope.FileInvalidMessage = "File required!";
        }
        $scope.IsFileValid = isValid;
    };

    $scope.UploadFile = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        //We can send more data to server using append         
        $rootScope.SelectedFileForUploadnamemain = file.name;
        $http.post("/Import/SaveFilesFullImport?XMLName=" + $scope.newxmlname1 + "", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {
                $scope.filePath = $scope.excelPath = d.split(',')[0];// to get file path with session id
                if (d.split(',')[1].toUpperCase() == "TRUE") {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Invalid sheet, please download the sample template.',
                        type: "error"
                    });
                    if ($scope.ImportcatalogIdAttr != undefined && $scope.ImportcatalogIdAttr != '')
                        $scope.IsFormSubmitted = false;
                    else
                        $scope.IsFormSubmitted = false;
                    $scope.Message = "";
                    var $el = $('#importfile1');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                    $rootScope.SelectedFileForUploadnamemain = "";
                    blockUI.stop();
                    return;
                }
                else {
                    if ($scope.ImportFormat.toString().toUpperCase().includes('XLS')) {
                        $scope.excelPath = d.split(',')[0];
                        $scope.importExcelSheetSelction.read();

                    }

                    else if ($scope.ImportFormat.toString().toUpperCase().includes('CSV')) {
                        var url = d.split(',')[0].replace($location.$$absUrl, '');
                        $http.get(url).success(function (data) {
                            var allData = data.split(/\r\n|\n/);
                            var headers = data[0].split(',');
                            var row = [];
                            angular.forEach(data, function (values) {
                                var value = values.split(',')
                            });
                        })
                    }
                }

            }).error(function () {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });
            });
        return defer.promise;
    };

    $scope.enablevalidation = function () {
        $scope.passvalidationimportenable = true;
        return;
        dataFactory.enableitemvalidation().success(function (response) {
            $scope.passvalidation = response;
            if (response == "true") {
                $scope.passvalidationimport = false;
                $scope.passvalidation = true;
            }
            else {
                $scope.passvalidationimport = true;
                $scope.passvalidation = false;
            }
            $scope.passvalidationimport = true;
            $scope.passvalidation = false;
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.skipValidateimport = function () {
        $scope.passvalidationimportenable = false;
        $scope.ResultDisplay = false;
        $scope.AttributeMappingdiv = true;
        $scope.ProcessName = "Import Content";
        $scope.passvalidationNext = false;
        $scope.showValidatediv = false;
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#Atrmappingimp").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#imptamp").removeClass("leftMenuactive");
        $("#import").addClass("leftMenuactive");
        $("#importSelectionTable").css("margin-top", "50px");
    };

    if ($scope.excelPath !== "") {
        $scope.tblBasicDashImportBoard = new ngTableParams({ page: 1, count: 5 },
            {
                count: [],
                total: function () { return $scope.basicprodImportData.length; },
                getData: function ($defer, params) {
                    var filteredData = $scope.basicprodImportData;
                    var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });
    }

    /*********Product input screen changes by Aswin *********///
    $scope.changeImportType = function (importType) {
        debugger;
        $scope.ImportType = importType;
        $scope.attrMap = false;

        //Import type change based on Sheet //////////////
        if ($scope.ImportType.toUpperCase().includes('FAMILYPRODUCTS')) {
            $scope.importTypeSelection = "ProductItems";
            $scope.importSheetType = "FamilyProducts";
        }
        else if ($scope.ImportType.toUpperCase().includes('SUBPRODUCT')) {
            $scope.importTypeSelection = "SubProduct";
            $scope.importSheetType = "SubProducts";
        } else if ($scope.ImportType.toUpperCase() == 'PRODUCT') {
            $scope.importTypeSelection = "Item";
            $scope.importSheetType = "Products";

        } else if ($scope.ImportType.toUpperCase().includes('FAMIL')) {
            $scope.importTypeSelection = "Product";
            $scope.importSheetType = "Families";
        }
        else if ($scope.ImportType.toUpperCase().includes('CATEG')) {
            $scope.importTypeSelection = "Category";
            $scope.importSheetType = "Categories";
        } else if ($scope.ImportType == undefined || $scope.ImportType == "") {
            $scope.importTypeSelection = "Item";
            $scope.importSheetType = "Products";
        } else if ($scope.ImportType.toUpperCase().includes('TABLEDESIGNER')) {
            $scope.importTypeSelection = "TableDesigner";
            $scope.importSheetType = "TableDesigner";
        }
        dataFactory.excelImportType($scope.excelPath, $scope.importExcelSheetDDSelectionValue).success(function (response) {

            if (response != null) {
                $scope.checkColumnsArray = [];

                $scope.checkColumnsArray = response.Data;

                $scope.checkProduct = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === $localStorage.CatalogItemNumber || ob["ExcelColumn"] === "ITEM#");

                $scope.checkFamily = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === "FAMILY_NAME");


                $scope.checkCategory = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === "CATEGORY_NAME");

                $scope.checkTabledesigner = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === "FAMILY_TABLE_STRUCTURE");

                if ($scope.checkProduct != undefined && $scope.ImportType == 'FamilyProducts') {
                    //$scope.importTypeSelection = "Product";
                    // $scope.importSheetType = "Products";
                    $scope.importSelectedvalue = $scope.importExcelSheetDDSelectionValue;
                    //$scope.ImportType = "Products";

                }
                else if ($scope.checkProduct != undefined) {
                    //$scope.importTypeSelection = "Product";
                    // $scope.importSheetType = "Products";
                    $scope.importSelectedvalue = $scope.importExcelSheetDDSelectionValue;
                    $scope.ImportType = "Products";

                }
                else if ($scope.checkTabledesigner != undefined) {
                    //$scope.importTypeSelection = "TableDesigner";
                    //$scope.importSheetType = "TableDesigner";
                    $scope.importSelectedvalue = $scope.importExcelSheetDDSelectionValue;
                    $scope.ImportType = "TableDesigner";
                    $scope.attrMappingEnabled = false;

                    $scope.attrMap = false;
                }
                else if ($scope.checkFamily != undefined && $scope.checkTabledesigner == undefined) {
                    // $scope.importTypeSelection = "Family";
                    /// $scope.importSheetType = "Families";
                    $scope.importSelectedvalue = $scope.importExcelSheetDDSelectionValue;
                    $scope.ImportType = "Families";

                }

                else if ($scope.checkCategory != undefined) {
                    // $scope.importTypeSelection = "Category";
                    // $scope.importSheetType = "Categories";
                    $scope.importSelectedvalue = $scope.importExcelSheetDDSelectionValue;
                    $scope.ImportType = "Categories";

                }




            }

            if ($scope.importSheetType == $scope.ImportType) {
                $scope.importGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath, 0);
            }
            else if ($scope.ImportType == 'FamilyProducts') {
                $scope.importGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath, 0);
            }

            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Invalid Import Type & Sheet Name.',
                    type: "error"
                });
                $scope.attrMappingEnabled = false;

            }


        }).error(function (error) {
            options.error(error);
        });




        //$scope.getImportTypeBasedonSheet();


        //Import type change based on Sheet //////////////

        /*********Product input screen changes by Aswin *********///

    };

    /*********Product input screen changes by Aswin *********///


    $scope.btnNextImport = function (templateDetails) {


        $scope.nextSheetProcess = false;
        if (($scope.TemplateNameImport == "" || $scope.selectedTemplate == "") && $scope.catalogIdAttr == "" && $scope.ImportFormat == "") {
            return;
        };
        if ($scope.ImportcatalogIdAttr == undefined || $scope.ImportcatalogIdAttr.toString().toUpperCase() == "ALL" || $scope.ImportcatalogIdAttr == "") {
            $scope.ImportcatalogIdAttr = 0;
        }
        if ($scope.TemplateNameImport == undefined) {
            $scope.TemplateNameImport = "";
        }

        dataFactory.SaveTemplateDetails($scope.TemplateNameImport, $scope.ImportcatalogIdAttr, $scope.ImportFormat, $scope.filePath, $scope.modifyTemplateFlag, $scope.workBookFlag).success(function (response) {
            var result = response.split("_");
            if (result.length > 3) {
                $scope.PicklistValidation = result[3].toString().toUpperCase() == "FALSE" ? false : true;
                $scope.subcatalogItem = result[4] == "0" ? false : true;
                if ($scope.templateId == undefined)
                    $scope.templateId = result.length > 4 ? result[5] : 0;
                    ///***********************Product input Screen changes by Aswin  **********************/
                else {
                    $scope.templateId = result[5];
                    $scope.selectedTemplate = $scope.templateId;
                }

                $scope.templateNameExists = result[6];



            }


            if ($scope.templateNameExists == "True") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Template name already exists.',
                    type: "info"
                });

            }
            else {


                $scope.getImportTypeBasedonSheet();

                //$scope.importGridOptions($scope.importSelectedvalue, $scope.excelPath, 0);

                $scope.templateDetails = false;
                $("#importSelectionTable").css("margin-top", "0px");
                $scope.AttributeMappingdiv = true;
                $scope.ProcessName = "Map Attributes";
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#Atrmappingimp").addClass("leftMenuactive");
                $scope.attrMap = true;
            }

            ///***********************Product input Screen changes by Aswin  **********************/
            blockUI.stop();

        }).error(function (error) {
            options.error(error);
        });
    };


    /*********Product input screen changes by Aswin *********///

    $scope.DeleteTemplatedetails = function (selectedTemplate) {

        if (selectedTemplate == null || selectedTemplate == "" || selectedTemplate == undefined) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Template.',
                type: "info"
            });
        }
        else {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to delete the template?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    var bool = false;
                    if (result === "Yes") {
                        dataFactory.DeleteTemplate(selectedTemplate).success(function (response) {
                            /*********Product input screen changes by Aswin *********///
                            if (response == "Import template deleted Successfully") {
                                $scope.TemplateNameImport = "";
                                $scope.ResetPage();
                                $scope.GetTemplateDetailsImport.read();
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Import template deleted Successfully.',
                                    type: "info"
                                });
                                /*********Product input screen changes by Aswin *********///
                                $scope.selectedTemplate = 0;

                            }
                        });
                    }
                }
            });

        }




    };
    /*********Product input screen changes by Aswin *********///
    $scope.listAllSheet = [];

    $scope.importExcelSheetSelction = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.importExcelSheetSelection($scope.excelPath).success(function (response) {
                    debugger;
                    if (response == null) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'This file is not a valid file.Please check the log for more information.',
                            type: "info"
                        });
                        $scope.IsFormSubmitted = false;

                        $scope.$apply(function () {
                            blockUI.stop();
                        });
                    }
                    else {
                        options.success(response);

                    }

                    //For Name Changes OCT 18
                    if (response != null && response.length != 0 && response.length == 3) {

                        angular.forEach(response, function (value, key) {
                            if (value.TABLE_NAME == "Products") {
                                value.TABLE_NAME = "Families"
                            }
                            if (value.TABLE_NAME == "Items") {
                                value.TABLE_NAME = "Products"
                            }
                        });
                    }
                    else
                    {
                        angular.forEach(response, function (value, key) {
                            if (value.TABLE_NAME == "Products") {
                                value.TABLE_NAME = "Products"
                            }
                            if (value.TABLE_NAME == "Items") {
                                value.TABLE_NAME = "Items"
                            }
                        });

                    }

                    if (response != null && response.length != 0) {

                        angular.forEach(response, function (value, key) {
                            if (value.TABLE_NAME == "TableDesigner") {
                                $scope.enableTableDesignerid = true;
                                $scope.enableTableDesignerid1 = false;
                            }
                            else {
                                $scope.enableTableDesignerid1 = true;
                                $scope.enableTableDesignerid = false;
                            }
                        });
                    }
                    if ($rootScope.EnableSubProduct == false) {
                        var subProdList = [];
                        if (response != null && response.length != 0) {
                            $scope.subproduct = response[0].TABLE_NAME;
                        }
                        angular.forEach($scope.importExcelSheetSelction._data, function (val) {

                            if (val.TABLE_NAME.toString().toUpperCase().includes('SUBPRODUCTS')) {
                                $scope.importExcelSheetSelction._data.remove(val)
                            }

                            //if (val.TABLE_NAME.toString().includes('Products')) {
                            //    val.TABLE_NAME = "Families"
                            //}

                            //if (val.TABLE_NAME.toString().includes('Items')) {
                            //    val.TABLE_NAME = "Products"
                            //}
                        });
                    }
                    if (response != null && response.length != 0) {
                        $scope.currentWorkbookSheets = response;
                        $scope.existingWorkbookSheets = $scope.existingSheetNames;
                        $scope.workBookFlag = "0";
                        //  angular.equals($scope.currentSheetNames,$scope.existingSheetNames);
                        //angular.forEach($scope.currentWorkbookSheets, function (value1, key1) {
                        //    angular.forEach($scope.existingWorkbookSheets, function (value2, key2) {
                        //        if (value1.TABLE_NAME != value2.TABLE_NAME ) {

                        //            $scope.differentWorkBook = "1";

                        //        }

                        //    });

                        //});
                        //  var differentWorkBook = $scope.currentWorkbookSheets.find(o => !$scope.existingWorkbookSheets.find(o2 => o.TABLE_NAME === o2.TABLE_NAME));


                        if ($scope.modifyTemplateFlag == "1" && $scope.existingWorkbookSheets != undefined) {

                            var differentWorkBook = $scope.currentWorkbookSheets.every((val, idx) => val.TABLE_NAME === $scope.existingWorkbookSheets[idx].TABLE_NAME);
                            if (differentWorkBook == false) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: "Selected workbook is different from the existing workbook. Do you want to lose all the changes?",
                                    type: "confirm",
                                    buttons: [{ value: "Yes" }, { value: "No" }],
                                    success: function (result) {

                                        if (result === "Yes") {

                                            $scope.workBookFlag = "1";
                                            $scope.importSelectedvalue = response[0].TABLE_NAME;
                                            $scope.importSheetSelected = response[0].TABLE_NAME;
                                            $('#importSelectionTable').show();
                                            $scope.importExcelSheetDDSelectionValue = $scope.importSelectedvalue;
                                            $scope.btnNextImport(templateDetails);

                                        }
                                        else {

                                            $scope.$apply(function () {
                                                blockUI.stop();
                                            });
                                        }

                                    }

                                });

                            }
                            else {
                                $scope.importSelectedvalue = response[0].TABLE_NAME;
                                $scope.importSheetSelected = response[0].TABLE_NAME;
                                $('#importSelectionTable').show();
                                $scope.importExcelSheetDDSelectionValue = $scope.importSelectedvalue;
                                $scope.btnNextImport(templateDetails);
                            }
                        }
                        else {
                            $scope.importSelectedvalue = response[0].TABLE_NAME;
                            $scope.importSheetSelected = response[0].TABLE_NAME;
                            $('#importSelectionTable').show();
                            $scope.importExcelSheetDDSelectionValue = $scope.importSelectedvalue;
                            $scope.btnNextImport(templateDetails);

                        }

                    }
                    else {


                        $scope.$apply(function () {
                            blockUI.stop();
                        });
                    }

                }).error(function (error) {
                    options.error(error);

                    $scope.$apply(function () {
                        blockUI.stop();
                    });
                });
            }
        }
    });

    /*********Product input screen changes by Aswin *********///
    $scope.importSheetType = "Families";
    $scope.importExcelSheet = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                debugger;
                dataFactory.importExcelSheetSelection($scope.excelPath).success(function (response) {
                    options.success(response);

                    if (response != null && response.length != 0) {

                        $scope.importSheetSelected = response[0].TABLE_NAME;

                        blockUI.stop();
                    }
                    else {
                        blockUI.stop();
                    }
                }).error(function (error) {
                    options.error(error);
                    blockUI.stop();
                });
            }
        }
    });

    $scope.importSheetType = "Products";
    $scope.importSheetChange = function (e) {
    };

    if ($scope.TemplateNameImport != undefined && $scope.ImportcatalogIdAttr != undefined && $scope.ImportFormat != undefined) {
        dataFactory.SaveTemplateDetails($scope.TemplateNameImport, $scope.ImportcatalogIdAttr, $scope.ImportFormat).success(function (response) {
            var result = response.split("_");
            if (result.length > 3) {
                $scope.PicklistValidation = result[3].toString().toUpperCase() == "FALSE" ? false : true;
                $scope.subcatalogItem = result[4] == "0" ? false : true;
                $scope.templateId = result.length > 4 ? result[5] : 0;
            }

            $scope.importGridOptions($scope.importSelectedvalue, $scope.excelPath, 0);
            $scope.templateDetails = false;
            $("#importSelectionTable").css("margin-top", "0px");
            $scope.AttributeMappingdiv = true;
            $scope.ProcessName = "Map Attributes";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
        }).error(function (error) {
            options.error(error);
        });
    }


    $scope.SelectToImportClick = function (row, dd, captionName) {

        var captionName = captionName;
        var selectedToImport = row;

        var captionNameIndex = $scope.mappingAttributes.findIndex((ob => ob["captionName"] === captionName));

        if (captionNameIndex != -1) {

            $scope.mappingAttributes[captionNameIndex]["selectedToImport"] = row;

            //$scope.mappingAttributes.push({
            //    captionName: $scope.captionName, attributeName: $scope.attributeName, selectedToImport: $scope.defaultSelectionToImport,
            //    attributeType: $scope.defaultAttributeType
            //});
        }
        else {
            $scope.mappingAttributes.push({
                captionName: captionName, attributeName: "", selectedToImport: row,
                attributeType: "0"
            });
        }

        if ($('.select').length > 0) {
            angular.forEach($('.select').closest("tr"), function (data) {
                if (data.children[1].children[0].checked == false) {
                    data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header select";
                }
                else if (data.children[1].children[0].checked == true && data.children[3].children[0].children[0].children[0].children[1].value == "") {
                    data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header unselect";
                    $scope.selectedCaptionName = data.cells[0].innerText;
                    $scope.selectedData = data;

                    //dataFactory.selectedImport($scope.captionName, $scope.selectedTemplate, $scope.importTypeSelection).success(function (response) {
                    //    $scope.selectedindex = data.rowIndex - 1;

                    //    for (var i = 0; i < $scope.prodImportData.length; i++)
                    //    {
                    //        if ($scope.prodImportData[i]["CatalogField"] == $localStorage.CatalogItemNumber)
                    //        {
                    //            $scope.selectedindex = $scope.selectedindex + i;
                    //        }
                    //    }

                    //    $scope.prodImportData[$scope.selectedindex]["AttrMapping"] = response.m_Item1;
                    //   // $scope.columnsForImportAtt[1].ATTRIBUTE_NAME = response.m_Item1;
                    //   // $scope.mappingAttribute = response.m_Item1;
                    //    // $scope.mappingAttribute = "(H) Stocking Type2";
                    //    //$scope.Data[column.field] = response.m_Item2;


                    //}).error(function (error) {
                    //    options.error(error);
                    //});

                }
                else {
                    data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header select";
                }


            });

        }
        //  $scope.selectimport();
        $scope.newattribute();

        if ($('.unselect').length > 0 && $('.unselect option:selected').text() === "") {
            $scope.attrMap = true;
        }
        else {
            $scope.attrMap = false;
        }
        if ($('.selectattrList').length > 0) {
            angular.forEach($('.selectattrList').closest("tr"), function (data) {
                if (data.children[1].children[0].checked == false) {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";
                }
                else if (data.children[1].children[0].checked == true && data.children[2].children[0].children[0].children[1].value == "") {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header unselectattrList";
                }
                else {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";
                }
            });
        }
        if ($('.unselectattrList').length > 0 && ($('.unselectattrList option:selected').text() === "" || $('.unselectattrList option:selected').text() === "--- Select Attribute type ---")) {
            $scope.attrMap = true;
        }
        else if ($('.unselect').length > 0 && $('.unselect option:selected').text() === "") {
            $scope.attrMap = true;
        }
        else {
            $scope.attrMap = false;
        }
        $scope.newattribute();
    }

    //$scope.selectimport = function () {
    //    $scope.selectedindex = $scope.selectedData.rowIndex;

    //    if ($scope.importTypeSelection == "Product") {

    //        for (var i = 0; i < $scope.prodImportData.length; i++) {
    //            if ($scope.prodImportData[i]["CatalogField"] == $localStorage.CatalogItemNumber) {
    //                $scope.selectedindex = $scope.selectedindex + i;
    //                break;
    //            }
    //        }
    //    }

    //    else if ($scope.importTypeSelection == "Family") {
    //        for (var i = 0; i < $scope.prodImportData.length; i++) {
    //            if ($scope.prodImportData[i]["CatalogField"] == "SUBFAMILY_NAME") {
    //                $scope.selectedindex = $scope.selectedindex + i;
    //                break;
    //            }
    //        }
    //    }

    //    dataFactory.selectedImport($scope.captionName, $scope.selectedTemplate, $scope.importTypeSelection).success(function (response) {

    //        $scope.prodImportData[$scope.selectedindex]["AttrMapping"] = response.m_Item1;
    //        $scope.prodImportData[$scope.selectedindex]["FieldType"] = response.m_Item2;
    //        // $scope.columnsForImportAtt[1].ATTRIBUTE_NAME = response.m_Item1;
    //        // $scope.mappingAttribute = response.m_Item1;
    //        // $scope.mappingAttribute = "(H) Stocking Type2";
    //        //$scope.Data[column.field] = response.m_Item2;


    //    }).error(function (error) {
    //        options.error(error);
    //    });

    //}

    //$scope.setClickedRow = function(index,data)
    //{
    //    $scope.selectedindex = index;

    //    dataFactory.selectedImport($scope.captionName, $scope.selectedTemplate, $scope.importTypeSelection).success(function (response) {
    //      //  $scope.selectedindex = data.rowIndex - 1;

    //        //for (var i = 0; i < $scope.prodImportData.length; i++) {
    //        //    if ($scope.prodImportData[i]["CatalogField"] == $localStorage.CatalogItemNumber) {
    //        //        $scope.selectedindex = $scope.selectedindex + i;
    //        //    }
    //        //}

    //        $scope.prodImportData[$scope.selectedindex]["AttrMapping"] = response.m_Item1;
    //        $scope.prodImportData[$scope.selectedindex]["FieldType"] = response.m_Item2;
    //        // $scope.columnsForImportAtt[1].ATTRIBUTE_NAME = response.m_Item1;
    //        // $scope.mappingAttribute = response.m_Item1;
    //        // $scope.mappingAttribute = "(H) Stocking Type2";
    //        //$scope.Data[column.field] = response.m_Item2;


    //    }).error(function (error) {
    //        options.error(error);
    //    });
    //}

    $scope.newattribute = function () {
        if ($('.unselect').length > 0) {
            angular.forEach($('.unselect').closest("tr"), function (data) {
                if (data.children[1].children[0].checked == false) {
                    data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header select";
                }
                else if (data.children[1].children[0].checked == true && data.children[3].children[0].children[0].children[0].children[1].value == "") {
                    data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header unselect";
                }
                else {
                    data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header select";
                }
            });
        }
        if ($('.unselectattrList').length > 0) {
            angular.forEach($('.unselectattrList').closest("tr"), function (data) {
                if (data.children[1].children[0].checked == false) {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";
                }
                else if (data.children[1].children[0].checked == true && (data.children[2].children[0].children[0].children[1].value == "" || data.children[2].children[0].children[0].children[1].value == "--- Select Attribute type ---")) {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header unselectattrList";
                }
                else {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";
                }
            });
        }
        if ($('.selectattrList').length > 0) {
            angular.forEach($('.selectattrList').closest("tr"), function (data) {
                if (data.children[1].children[0].checked == false) {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";
                }
                else if (data.children[1].children[0].checked == true && data.children[2].children[0].children[0].children[1].value == "") {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header unselectattrList";
                }
                else {
                    data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";
                }
            });
        }
        if ($('.unselectattrList').length > 0 && ($('.unselectattrList option:selected').text() === "" || $('.unselectattrList option:selected').text() === "--- Select Attribute type ---")) {
            $scope.attrMap = true;
        }
        else if ($('.unselect').length > 0 && $('.unselect option:selected').text() === "") {
            $scope.attrMap = true;
        }
        else {
            $scope.attrMap = false;
        }
    };

    $scope.importGridOptions = function (importExcelSheetDDSelectionValue, excelPath, selectAll) {
        if ($scope.templateId == undefined) {
            $scope.templateId = 0;
            blockUI.stop();
        };
        if ($scope.IsSelectAll == true) {
            selectAll = 1;
        }
        if ($scope.IsSelectAll == false) {
            selectAll = 0;
        }
        if ($scope.importSelectedvalue.toUpperCase().includes("PRODUCTITEMS")) {
            $scope.importTypeSelection = "ProductItems";
        }
        /*********Product input screen changes by Aswin *********///

        //if ($scope.importSelectedvalue.toUpperCase().includes("SUB")) {
        //    $scope.importTypeSelection = "SubProduct";
        //    $scope.importSheetType = "SubProducts";

        //} else if ($scope.importSelectedvalue.toUpperCase().includes("PRODUCT")) {
        //    $scope.importTypeSelection = "Product";
        //    $scope.importSheetType = "Products";
        //} else if ($scope.importSelectedvalue.toUpperCase().includes("FAMIL")) {
        //    $scope.importTypeSelection = "Family";
        //    $scope.importSheetType = "Families";
        //}
        //else if ($scope.importSelectedvalue.toUpperCase().includes("CATEG")) {
        //    $scope.importTypeSelection = "Category";
        //    $scope.importSheetType = "Categories";
        //} else if ($scope.ImportType == undefined || $scope.ImportType === "") {
        //    $scope.importTypeSelection = "Product";
        //    $scope.importSheetType = "Products";
        //} else if ($scope.importSelectedvalue.toUpperCase().includes("TABLEDESIGNER")) {
        //    $scope.importTypeSelection = "TableDesigner";
        //    $scope.importSheetType = "TableDesigner";
        //} else {
        //    $scope.importTypeSelection = "Product";
        //    $scope.importSheetType = "Products";
        //}
        //$scope.ImportType = $scope.importSheetType;

        //$scope.importTypeSelection = "Product";
        //$scope.importSheetType = "Products";
        //$scope.ImportType = $scope.importSheetType;

        $scope.mappingAttributes = [];



        if (!($scope.selectedSheet.includes($scope.importExcelSheetDDSelectionValue))) {
            $scope.selectedSheet.push($scope.importExcelSheetDDSelectionValue);
        }

        /*********Product input screen changes by Aswin *********///

        if ($scope.selectedTemplate != "" && $scope.selectedTemplate != undefined && $scope.templateId == "0") {
            $scope.templateId = $scope.selectedTemplate;
        }
        dataFactory.GetImportSpecs(importExcelSheetDDSelectionValue, excelPath, $scope.templateId, $scope.ImportType, selectAll).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.prodImportData = obj.Data;

            // START
            // When storage value in ITEM#

            for (var i = 0 ; i < $scope.prodImportData.length ; i++) {

                var ItemValue = $scope.prodImportData[i]["ExcelColumn"];

                ///***** Add ITEM# VALUES not IN Mapping table ************************//////////

                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogItemNumber.toUpperCase()) {
                    $scope.prodImportData[i]["ExcelColumn"] = $localStorage.CatalogItemNumber;
                    $scope.prodImportData[i]["CatalogField"] = $localStorage.CatalogItemNumber;
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";

                }
                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogSubItemNumber.toUpperCase()) {
                    $scope.prodImportData[i]["ExcelColumn"] = $localStorage.CatalogSubItemNumber;
                    $scope.prodImportData[i]["CatalogField"] = $localStorage.CatalogSubItemNumber;
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";

                }
                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == "ITEM#") {

                    $scope.prodImportData[i]["CatalogField"] = "ITEM#";
                    $scope.prodImportData[i]["FieldType"] = "";
                    $scope.prodImportData[i]["IsSystemField"] = "";
                    $scope.prodImportData[i]["SelectedToImport"] = "False";
                    $scope.prodImportData[i]["AttrMapping"] = "";
                    $scope.prodImportData[i]["ExcelColumn"] = "ITEM#";


                }
                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == "SUBITEM#") {

                    $scope.prodImportData[i]["CatalogField"] = "SUBITEM#";
                    $scope.prodImportData[i]["FieldType"] = "";
                    $scope.prodImportData[i]["IsSystemField"] = "";
                    $scope.prodImportData[i]["SelectedToImport"] = "False";
                    $scope.prodImportData[i]["AttrMapping"] = "";
                    $scope.prodImportData[i]["ExcelColumn"] = "SUBITEM#";
                }

                ///***** Add ITEM# VALUES not IN Mapping table ************************//////////


                ///***** Add ITEM# VALUES IN Mapping table ************************//////////

                //if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogItemNumber.toUpperCase()) {
                //    $scope.prodImportData[i]["ExcelColumn"] = $localStorage.CatalogItemNumber;
                //    $scope.prodImportData[i]["CatalogField"] = $localStorage.CatalogItemNumber;
                //    $scope.prodImportData[i]["FieldType"] = "0";
                //    $scope.prodImportData[i]["IsSystemField"] = "True";
                //    $scope.prodImportData[i]["SelectedToImport"] = "True";
                //    $scope.prodImportData[i]["AttrMapping"] = $localStorage.CatalogItemNumber;

                //}
                //if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogSubItemNumber.toUpperCase()) {
                //    $scope.prodImportData[i]["ExcelColumn"] = $localStorage.CatalogSubItemNumber;
                //    $scope.prodImportData[i]["CatalogField"] = $localStorage.CatalogSubItemNumber;
                //    $scope.prodImportData[i]["FieldType"] = "0";
                //    $scope.prodImportData[i]["IsSystemField"] = "True";
                //    $scope.prodImportData[i]["SelectedToImport"] = "True";
                //    $scope.prodImportData[i]["AttrMapping"] = $localStorage.CatalogItemNumber;

                //}
                //if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == "ITEM#") {

                //    $scope.prodImportData[i]["CatalogField"] = "ITEM#";
                //    $scope.prodImportData[i]["FieldType"] = "";
                //    $scope.prodImportData[i]["IsSystemField"] = "";
                //    $scope.prodImportData[i]["SelectedToImport"] = "False";
                //    $scope.prodImportData[i]["AttrMapping"] = "ITEM#";
                //    $scope.prodImportData[i]["ExcelColumn"] = "ITEM#";


                //}
                //if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == "SUBITEM#") {

                //    $scope.prodImportData[i]["CatalogField"] = "SUBITEM#";
                //    $scope.prodImportData[i]["FieldType"] = "";
                //    $scope.prodImportData[i]["IsSystemField"] = "";
                //    $scope.prodImportData[i]["SelectedToImport"] = "False";
                //    $scope.prodImportData[i]["AttrMapping"] = "SUBITEM#";
                //    $scope.prodImportData[i]["ExcelColumn"] = "SUBITEM#";
                //}


                ///***** Add ITEM# VALUES IN Mapping table ************************//////////

                $scope.mappingAttributes.push({
                    captionName: $scope.prodImportData[i]["ExcelColumn"], attributeName: $scope.prodImportData[i]["AttrMapping"], selectedToImport: $scope.prodImportData[i]["SelectedToImport"],
                    attributeType: $scope.prodImportData[i]["FieldType"]
                })


            }



            // Pdf Xpress Implementation   - Start

            for (var i = 0 ; i < $scope.prodImportData.length ; i++) {

                if ($scope.prodImportData[i]["ExcelColumn"] == "Family_PdfTemplate") {

                    $scope.prodImportData[i]["CatalogField"] = "Family_PdfTemplate";
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";
                    $scope.prodImportData[i]["AttrMapping"] = "";
                    $scope.prodImportData[i]["ExcelColumn"] = "Family_PdfTemplate";
                }


                if ($scope.prodImportData[i]["ExcelColumn"] == "Catalog_PdfTemplate") {

                    $scope.prodImportData[i]["CatalogField"] = "Catalog_PdfTemplate";
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";
                    $scope.prodImportData[i]["AttrMapping"] = "";
                    $scope.prodImportData[i]["ExcelColumn"] = "Catalog_PdfTemplate";
                }


                if ($scope.prodImportData[i]["ExcelColumn"] == "Category_PdfTemplate") {

                    $scope.prodImportData[i]["CatalogField"] = "Category_PdfTemplate";
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";
                    $scope.prodImportData[i]["AttrMapping"] = "";
                    $scope.prodImportData[i]["ExcelColumn"] = "Category_PdfTemplate";
                }

                if ($scope.prodImportData[i]["ExcelColumn"] == "Product_PdfTemplate") {

                    $scope.prodImportData[i]["CatalogField"] = "Product_PdfTemplate";
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";
                    $scope.prodImportData[i]["AttrMapping"] = "";
                    $scope.prodImportData[i]["ExcelColumn"] = "Product_PdfTemplate";
                }

            }



            // Pdf Xpress Implemebtation - End
            //END




            $scope.columnsForImportAtt = obj.Columns;
            $scope.attrMappingEnabled = false;
            angular.forEach($scope.prodImportData, function (data) {
                if (data.FieldType != "0" && data.ExcelColumn.toString().toUpperCase() != "ITEM#" && data.ExcelColumn.toString().toUpperCase() != "SUBITEM#") {
                    $scope.attrMappingEnabled = true;
                }
            });
            if ($scope.importExcelSheetDDSelectionValue == "Categories")
            {
                $scope.ImportType = "Categories";
            }
            else if ($scope.importExcelSheetDDSelectionValue == "Products")
            {
                $scope.ImportType = "Products";
            }
            else
            {
                $scope.ImportType = "Items";

            }

            //change_1
            if ($scope.ImportType.toUpperCase().includes("FAMILYPRODUCTS")) {
                var prodAttr = $rootScope.AttributeDataSource1;
                $scope.AttributeDataSource1 = [];
                for (var i = 0; i < prodAttr.length; i++) {
                    if (prodAttr[i].importTypeSelection.toUpperCase().includes("PRODUCTS") || prodAttr[i].importTypeSelection.toUpperCase().includes("FAMILIES")) {
                        $scope.AttributeDataSource1.push(prodAttr[i]);
                    }
                }
            }
            else if ($scope.ImportType.toUpperCase().includes("PRODUCT")) {
                var prodAttr = $rootScope.AttributeDataSource1;
                $scope.AttributeDataSource1 = [];
                for (var i = 0; i < prodAttr.length; i++) {
                    if (prodAttr[i].importTypeSelection.toUpperCase().includes("PRODUCT")) {
                        $scope.AttributeDataSource1.push(prodAttr[i]);
                    }
                }
            }
            else if ($scope.ImportType.toUpperCase().includes("FAMI")) {
                var prodAttr = $rootScope.AttributeDataSource1;
                $scope.AttributeDataSource1 = [];
                for (var i = 0; i < prodAttr.length; i++) {
                    if (!prodAttr[i].importTypeSelection.toUpperCase().includes("PRODUCT") && !prodAttr[i].importTypeSelection.toUpperCase().includes("CATEG")) {
                        $scope.AttributeDataSource1.push(prodAttr[i]);
                    }
                }
            }
            else if ($scope.ImportType.toUpperCase().includes("CATEG")) {
                var prodAttr = $rootScope.AttributeDataSource1;
                $scope.AttributeDataSource1 = [];
                for (var i = 0; i < prodAttr.length; i++) {
                    if (!prodAttr[i].importTypeSelection.toUpperCase().includes("PRODUCT") && !prodAttr[i].importTypeSelection.toUpperCase().includes("FAMI")) {
                        $scope.AttributeDataSource1.push(prodAttr[i]);
                    }
                }
            }
            if (selectAll == 1)
                $scope.newmappingname = "<<<New Attribute>>>";




            //if ($('.unselect').length > 0)
            //{
            //    angular.forEach($('.unselect').closest("tr"), function (data)
            //    {
            //        if (data.children[1].children[0].checked == false)
            //        {
            if ($('.unselect').length > 0) {
                angular.forEach($('.unselect').closest("tr"), function (data) {
                    if (data.children[1].children[0].checked == false) {
                        data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header select";
                    }
                    else if (data.children[1].children[0].checked == true && data.children[3].children[0].children[0].children[0].children[1].value == "") {
                        data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header unselect";
                    }
                    else {
                        data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header select";
                    }
                });
            }
            if ($('.unselect').length > 0 && $('.unselect option:selected').text() === "") {
                $scope.attrMap = true;
            }
            else {
                $scope.attrMap = false;
            }
            if ($('.selectattrList').length > 0) {
                angular.forEach($('.selectattrList').closest("tr"), function (data) {
                    if (data.children[1].children[0].checked == false) {
                        data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";

                        if (selectAll == 1) {
                            var captionName = data.cells[0].innerText;

                            var captionNameIndex = $scope.mappingAttributes.findIndex((ob => ob["captionName"] === captionName));

                            if (captionNameIndex != -1) {

                                $scope.mappingAttributes[captionNameIndex]["attributeName"] = "<<<New Attribute>>>";

                            }
                        }
                    }
                    else if (data.children[1].children[0].checked == true && data.children[2].children[0].children[0].children[1].value == "") {
                        data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header unselectattr";
                    }
                    else {
                        data.children[2].children[0].children[0].className = "k-widget k-dropdown k-header selectattrList";
                    }
                });
            }
            if ($('.unselectattrList').length > 0 && $('.unselectattrList option:selected').text() === "") {
                $scope.attrMap = true;
            }
            else if ($('.unselect').length > 0 && $('.unselect option:selected').text() === "") {
                $scope.attrMap = true;
            }
            else {
                $scope.attrMap = false;
            }
            if (obj.Data.length == 0) {
                $scope.attrMap = true;
            }
            if ($scope.attrMappingEnabled == false) {
                $scope.attrMap = false;
                blockUI.stop();
            }
            $timeout(function () {
                if (selectAll == 1) {
                    $('#selectAllChk').prop('checked', true);
                }
            }, 100);
        }).error(function (error) {
            options.error(error);
            blockUI.stop();
        });
        if (importExcelSheetDDSelectionValue == "TableDesigner" || importExcelSheetDDSelectionValue == "TABLEDESIGNER") {
            $("#importSelectionTable").hide();

        }
        else {
            $("#importSelectionTable").show();
        }
        //$("#importSelectionTable").show();

        if ($scope.IsSelectAll == true) {
            $scope.IsSelectAll = false;
        }



    };

    $('#selectAllChk').prop('checked', false);

    /***Product input screen changes by Aswin ****/
    $scope.Templatechange = function (value) {
        $scope.TemplateNameImport = value.sender.text();
        $scope.showMappingMessage = false;
        $scope.showImportMessage = false;

        if ($scope.TemplateNameImport == "----- New -----") {
            $scope.SelectedFileForUploadnamemainsam = "";
            $rootScope.SelectedFileForUploadnamemain = "";
            $scope.templateNamedisabled = false;
            $scope.deleteEnable = true;
            $scope.sampleEnable = false;
            $("#catalogDrpDownLisr").data("kendoDropDownList").enable(true);
            $("#drpImportType").data("kendoDropDownList").enable(true);
            $scope.fileName = null;
            $scope.ResetPage();
            return;
        }
            //else if (value.sender.value() == "1") {
            //    $scope.IsFormSubmitted = false;
            //    $scope.deleteEnable = true;
            //    $scope.SelectedFileForUploadnamemainsam = "Download Sample.xls";
            //    $scope.sampleEnable = true;
            //    $("#catalogDrpDownLisr").data("kendoDropDownList").enable(true);
            //    $("#drpImportType").data("kendoDropDownList").enable(true);
            //    $scope.sampleSheetDownload = "../../Content/Excel/Sample Sheet.xls";
            //}
            //else if (value.sender.value() == "2") {
            //    $scope.IsFormSubmitted = false;
            //    $scope.sampleEnable = true;
            //    $scope.sampleSheetDownload = "../../Content/Excel/Sample Sheet2.xls";
            //    $("#catalogDrpDownLisr").data("kendoDropDownList").enable(true);
            //    $("#drpImportType").data("kendoDropDownList").enable(true);
            //    $scope.SelectedFileForUploadnamemainsam = "Download Sample2.xls";
            //}
        else {
            var templateId = value.sender.value();
            $scope.templateId = templateId;
            $scope.templateNamedisabled = true;
            //$scope.deleteEnable = false;
            //$scope.showValidatediv = false;
            //$scope.sampleEnable = false;
            $scope.IsFormSubmitted = false;
            $scope.deleteEnable = false;
            $scope.sampleEnable = false;
            $scope.IsFormSubmitted = true;
            $scope.fileChoose = true;
            /*********Product input screen changes by Aswin *********///

            dataFactory.getTemplateDetails(templateId).success(function (response) {
                if (response != null) {


                    var data = response.m_Item1[0];
                    $scope.TemplateNameImport = data.TEMPLATE_NAME;
                    $scope.ImportcatalogIdAttr = data.CATALOG_ID;
                    // var dropdownlistCatalog = $("#catalogDrpDownLisr").data("kendoDropDownList");
                    var dropdownlistCatalog = $("#catalogDrpDownLisr").data('kendoDropDownList').value($scope.ImportcatalogIdAttr);
                    //  dropdownlistCatalog.select($scope.ImportcatalogIdAttr);
                    $scope.ImportFormat = data.EXCEL_FORMAT;
                    var dropdownlistImportFormat = $("#drpImportType").data("kendoDropDownList");
                    dropdownlistImportFormat.text($scope.ImportFormat);
                    $scope.MappingID = data.ATTRIBUTE_MAPPING_ID;
                    $scope.importFormatLowercase = $scope.ImportFormat.toLowerCase();
                    $scope.fileName = $scope.TemplateNameImport + "." + $scope.importFormatLowercase;
                    $scope.displayFileName = $scope.fileName;
                    //   $rootScope.SelectedFileForUploadnamemain= $scope.fileName;
                    if (response.m_Item2 == true) {
                        // $scope.fileName = $scope.TemplateNameImport + "." + "xls";
                        $scope.sampleEnable = true;
                        $scope.sampleSheetDownload = "../../Content/ImportTemplate/" + $scope.fileName;
                        $scope.SelectedFileForUploadnamemainsam = $scope.fileName;
                    }



                    $("#catalogDrpDownLisr").data("kendoDropDownList").enable(false);
                    $("#drpImportType").data("kendoDropDownList").enable(false);

                    $scope.loadMappingSheetNames();
                    /*********Product input screen changes by Aswin *********///
                }
            });
        }
    };
    /***Product input screen changes by Aswin ****/

    $scope.importExcelSheetDDSelection = function (e) {
        if ($scope.Attrmapping._filter.filters[1]) {
            $scope.Attrmapping._filter.filters.splice(1, 1);
        }
     
        debugger;
        //if ($scope.importSelectedvalue.toUpperCase().includes("PRODUCTS") || $scope.importSelectedvalue.toUpperCase().includes("SUB") || $scope.importSelectedvalue.toUpperCase().includes("FAMILIES") || $scope.importSelectedvalue.toUpperCase().includes("CATEGORIES") || $scope.importSelectedvalue.toUpperCase().includes("TABLEDESIGNER")) {
        if (true) {
            $scope.attrMap = true;
            blockUI.start();
            $scope.importExcelSheetDDSelectionValue = e.sender.value();
            if ($scope.importExcelSheetDDSelectionValue !== "") {
                $scope.getImportTypeBasedonSheet();
                // $scope.importGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath, 0);
            }
            dataFactory.checkCatalogDetails($scope.excelPath, $scope.importExcelSheetDDSelectionValue, $scope.ImportcatalogIdAttr).success(function (data) {
                if (data == null || data === "") {
                    data = "Catalog Valid_Yes_Product";
                }
                var result = data.split("_");
                $scope.ImportType = result[2];
                //if ($scope.ImportType.toUpperCase().includes('SUBPRODUCT') || $scope.importSelectedvalue.toUpperCase().includes("SUB")) {
                //    $scope.importTypeSelection = "SubProduct";
                //    $scope.importSheetType = "SubProducts";
                //}
                //else if ($scope.ImportType.toUpperCase() === 'PRODUCT' && $scope.importSelectedvalue.toUpperCase().includes("PRODUCT")) {
                //    $scope.importTypeSelection = "Product";
                //    $scope.importSheetType = "Products";
                //}
                //else if ($scope.ImportType.toUpperCase().includes('FAMIL') || $scope.importSelectedvalue.toUpperCase().includes("FAMIL")) {
                //    $scope.importTypeSelection = "Family";
                //    $scope.importSheetType = "Families";
                //}

                //else if ($scope.ImportType.toUpperCase().includes('CATEG') || $scope.importSelectedvalue.toUpperCase().includes("CATEG")) {
                //    $scope.importTypeSelection = "Category";
                //    $scope.importSheetType = "Categories";
                //}
                //else if ($scope.ImportType.toUpperCase().includes('TABLEDESIGNER') || $scope.importSelectedvalue.toUpperCase().includes("TABLEDESIGNER")) {
                //    $scope.importTypeSelection = "TableDesigner";
                //    $scope.importSheetType = "TableDesigner";

                //}
                //else if ($scope.ImportType == undefined || $scope.ImportType === "") {
                //    $scope.importTypeSelection = "Product";
                //    $scope.importSheetType = "Products";
                //} else if ($scope.ImportType.toUpperCase().includes('TABLEDESIGNER') || $scope.importSelectedvalue.toUpperCase().includes("TABLEDESIGNER")) {
                //    $scope.importTypeSelection = "TableDesigner";
                //    $scope.importSheetType = "TableDesigner";
                //}
                //else {
                //    $scope.importTypeSelection = "Product";
                //    $scope.importSheetType = "Products";
                //}
                //$scope.ImportType = $scope.importSheetType;
                $scope.selectedSheet.push($scope.importExcelSheetDDSelectionValue);
                $timeout(function () {
                    blockUI.stop();
                }, 100);
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Invalid Sheet Chosen.',
                type: "error"
            });
            $scope.attrMap = true;
            return false;
        }
    };
    /*********Product input screen changes by Aswin *********///


    $scope.importTypeSelect = function () {
        if ($scope.ImportType.toUpperCase().includes('SUBPRODUCT') || $scope.importSelectedvalue.toUpperCase().includes("SUB")) {
            $scope.importTypeSelection = "SubItem";
            $scope.importSheetType = "SubProducts";

        } else if ($scope.ImportType.toUpperCase().includes('PRODUCT') || $scope.importSelectedvalue.toUpperCase().includes("PRODUCT")) {
            $scope.importTypeSelection = "Item";
            $scope.importSheetType = "Products";
        } else if ($scope.ImportType.toUpperCase().includes('FAMIL') || $scope.importSelectedvalue.toUpperCase().includes("FAMIL")) {
            $scope.importTypeSelection = "Product";
            $scope.importSheetType = "Families";

        }
        else if ($scope.ImportType.toUpperCase().includes('CATEG') || $scope.importSelectedvalue.toUpperCase().includes("CATEG")) {
            $scope.importTypeSelection = "Category";
            $scope.importSheetType = "Categories";

        } else if ($scope.ImportType.toUpperCase().includes('TABLEDESIGNER') || $scope.importSelectedvalue.toUpperCase().includes("TABLEDESIGNER")) {
            $scope.importTypeSelection = "TableDesigner";
            $scope.importSheetType = "TableDesigner";

        } else if ($scope.ImportType == undefined || $scope.ImportType === "") {
            $scope.importTypeSelection = "Item";
            $scope.importSheetType = "Products";
        } else {
            $scope.importTypeSelection = "Item";
            $scope.importSheetType = "Products";
        }
        $scope.ImportType = $scope.importSheetType;
    };
    $rootScope.AttributeDataSource1 = [
        {
            Text: "Alpha-Numeric [Item Level]", value: "Alpha-Numeric",
            value: 1,
            importTypeSelection: "Products"
        },
        {
            Text: "Price / Numbers [Item Level]", value: "Price/Numbers",
            value: 4,
            importTypeSelection: "Products"
        },
        {
            Text: "Image / Attachment Path [Item Level]", value: "Image/AttachmentPath",
            value: 3,
            importTypeSelection: "Products"
        },
        {
            Text: "Date-Time(MM/DD/YYYY) [Item Level]", value: "Date-Time",
            value: 10,
            importTypeSelection: "Products"
        },
        {
            Text: "Item Key Alpha-Numeric [Item Level]", value: "ProductKeyAlpha-Numeric",
            value: 6,
            importTypeSelection: "Products"
        },
        {
            Text: "Alpha-Numeric [Product Level]", value: "Alpha-Numeric",
            value: 7,
            importTypeSelection: "Families"
        },
        {
            Text: "Image / Attachment Path [Product Level]", value: "Image/AttachmentPath",
            value: 9,
            importTypeSelection: "Families"
        },
        {
            Text: "Product-Attribute [Product Level]", value: "Family-Attribute",
            value: 11,
            importTypeSelection: "Families"
        },
        {
            Text: "Product-Price [Product Level]", value: "Family-Price",
            value: 12,
            importTypeSelection: "Families"
        },
        {
            Text: "Product-Key [Product Level]", value: "Family-Key",
            value: 13,
            importTypeSelection: "Families"
        },
          {
              Text: "Alpha-Numeric [Category Level]", value: "Alpha-Numeric",
              value: 21,
              importTypeSelection: "Categories"
          },
            {
                Text: "Image / Attachment Path [Category Level]", value: "Image/AttachmentPath",
                value: 23,
                importTypeSelection: "Categories"
            },
            {
                Text: "Category Description[Category Level]", value: "Category Description",
                value: 25,
                importTypeSelection: "Categories"
            },
    ];

    $("#chkselectall").change(function () {  //"select all" change 
        $(".chk").prop('checked', $(this).prop("checked")); //change all ".checkbox" checked status
        $("#validateImport").attr("disabled", "disabled");
        $(".chk").each(function () {
            if ($(this).is(":checked") && $(this)[0].id != "chkselectall") {

                $("#validateImport").removeAttr("disabled");
            }
        });
    });
    //".checkbox" change 
    $('.chk').change(function () {
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if (false == $(this).prop("checked")) { //if this item is unchecked
            $("#chkselectall").prop('checked', false); //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if ($('.chk:checked').length == $('.chk').length) {
            $("#chkselectall").prop('checked', true);
        }
        $("#validateImport").attr("disabled", "disabled");
        $(".chk").each(function () {
            if ($(this).prop("checked") && $(this)[0].id != "chkselectall") {

                $("#validateImport").removeAttr("disabled");
            }
        });
    });



    $scope.Attrmapping = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        filter: "contains",
        transport: {
            read: function (options) {

                $localStorage.CopymappingAttributes = angular.copy($scope.mappingAttributes);
                if (options.data.filter.filters.length == 1) {
                    var filter = ""
                
                
                dataFactory.attributeimpmapping($scope.ImportType, $scope.selectedAttributeType).success(function (response) {
                    //angular.forEach(response, function (data) {
                    //});

                    if ($scope.mappingAttributes.length == 0) {
                        $scope.mappingAttributes = $localStorage.CopymappingAttributes;
                    }

                    if ($scope.ImportType == "Products") {
                        for (var i = 0; response.length > i; i++) {
                            if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                            }

                        }
                    }

                    if ($scope.ImportType == "SubProducts") {
                        for (var i = 0; response.length > i; i++) {
                            if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                            }


                        }
                    }


                    $scope.attributeList = response;
                    //$scope.DS1.data = response;
                    //$scope.DS1.options.schema = response;
                    options.success(response);

                    //$scope.checksearch();
                    //$scope.checksearch();


                    //var dropdownlist = $("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList");
                    //dropdownlist.setDataSource(response);
                    //$("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList").select(0);
                    //var dropdownlist = $("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList");
                    //dropdownlist.setDataSource(response);
                    // dropdownlist.dataSource.read();

                    //  $("#id_mappingattribute_" + $scope.attributeRowIndex).options.success(response);


                    //$("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList").options.success = $scope.attributeList;

                    // var dropdownlist = $("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList");

                    // dropdownlist.options.success(response);



                    //$scope.attrMap = true;  // disable mapping based on import type
                    $timeout(function () {
                        blockUI.stop();
                    }, 200)

                }).error(function (error) {
                    options.error(error);
                });
                
                }
                else {
                    var result = [];
                    var filter = options.data.filter.filters[1].value;
                    if (filter != "") {
                        for (var i = 0; i < $scope.attributeList.length; i++) {

                            if ($scope.attributeList[i].ATTRIBUTE_NAME.toLowerCase().includes(filter.toLowerCase())) {
                                result.push($scope.attributeList[i]);
                            }
                        }
                        if (result.length == 0) {
                            if ($scope.Attrmapping._filter.filters[1]) {
                                $scope.Attrmapping._filter.filters.splice(1, 1);
                            }
                  
                            options.success($scope.attributeList);
                        } else {
                            options.success(result);
                        }
                
                        $timeout(function () {
                            blockUI.stop();
                        }, 200)

                    } else if (filter == "") {
                        options.success($scope.attributeList);
                        $timeout(function () {
                            blockUI.stop();
                        }, 200)
                    }
                }
            }
        },
        sort: {
            field: 'ATTRIBUTE_NAME',
            dir: 'asc'
        }




    });

    //$scope.checksearch = function () {
    //    $("#products").kendoDropDownList({
    //        filter: "startswith",
    //        dataValueField: "ATTRIBUTE_ID",
    //        dataTextField: "ATTRIBUTE_NAME",
    //        dataSource: $scope.DS1
    //    });
    //}


    //$scope.DS1 = new kendo.data.DataSource({
    //    type: "json",
    //    autobind: false,
    //    transport: {
    //        read: function (options) {
    //            $localStorage.CopymappingAttributes = angular.copy($scope.mappingAttributes);
    //            dataFactory.attributeimpmapping($scope.ImportType, $scope.selectedAttributeType).success(function (response) {
    //                debugger;
    //                if ($scope.mappingAttributes.length == 0) {
    //                    $scope.mappingAttributes = $localStorage.CopymappingAttributes;
    //                }

    //                if ($scope.ImportType == "Products") {
    //                    for (var i = 0 ; response.length > i ; i++) {
    //                        if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
    //                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
    //                        }
    //                    }
    //                }

    //                if ($scope.ImportType == "SubProducts") {
    //                    for (var i = 0 ; response.length > i ; i++) {
    //                        if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
    //                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
    //                        }


    //                    }
    //                }


    //                $scope.attributeList = response;

    //                options.success(response);


    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //    }

    //});






    $scope.AttrmappingBasedOnTypeChange = function () {

        dataFactory.attributeimpmapping($scope.ImportType, $scope.selectedAttributeType).success(function (response) {
            //angular.forEach(response, function (data) {
            //});

            if ($scope.ImportType == "Products") {
                for (var i = 0 ; response.length > i ; i++) {
                    if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                        response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                    }

                }
            }

            if ($scope.ImportType == "SubProducts") {
                for (var i = 0 ; response.length > i ; i++) {
                    if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                        response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                    }


                }
            }


            $scope.attributeList = response;

            //var kendoMultiSelect = $("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList");
            //kendoMultiSelect.dataSource.options.value = response;

            //  options.success(response);


            var dropdownlist = $("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList");
            //dropdownlist.setDataSource(response);
            dropdownlist.setDataSource(response);
            $("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList").select(0);


            //var dropdownlist = $("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList");
            //dropdownlist.setDataSource(response);
            // dropdownlist.dataSource.read();

            //  $("#id_mappingattribute_" + $scope.attributeRowIndex).options.success(response);


            //$("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList").options.success = $scope.attributeList;

            // var dropdownlist = $("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList");

            // dropdownlist.options.success(response);



            //$scope.attrMap = true;  // disable mapping based on import type


        }).error(function (error) {
            options.error(error);
        });
    }


    $scope.AttrmappingNew = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        filter: "startswith",
        transport: {
            read: function (options) {
                dataFactory.attributeimpmapping().success(function (response) {
                    angular.forEach(response, function (data) {
                    });
                    $scope.attributeList = response;
                    options.success(response);
                    $timeout(function () {
                        blockUI.stop();
                    }, 200)

                }).error(function (error) {
                    options.error(error);
                });
                // }
            }
        },
        sort: {
            field: 'ATTRIBUTE_NAME',
            dir: 'asc'
        }
    });

    //$scope.AttrmappingNew = new kendo.data.DataSource({
    //    type: "json",
    //    serverFiltering: true,
    //    filter: "startswith",
    //    transport: {
    //        read: function (options) {
    //            dataFactory.attributeimpmapping().success(function (response) {
    //                $scope.attributeList = response;
    //                options.success(response);
    //                $timeout(function () {
    //                    blockUI.stop();
    //                }, 200)

    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //            // }
    //        }
    //    },
    //    sort: {
    //        field: 'ATTRIBUTE_NAME',
    //        dir: 'asc'
    //    }
    //});

    $scope.Attrmappingexp = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.attributeimpmappingexp().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.AttrtypeChange1 = function (e, captionName, rowIndex) {
        var attributeType = e.sender._old;
        var captionName = captionName;

        var captionNameIndex = $scope.mappingAttributes.findIndex((ob => ob["captionName"] === captionName));

        $scope.selectedAttributeType = attributeType;

        $scope.attributeRowIndex = rowIndex;

        if (captionNameIndex != -1) {

            $scope.mappingAttributes[captionNameIndex]["attributeType"] = attributeType;

            //  var attributeName = $scope.prodImportData[captionNameIndex]["AttrMapping"];

            //$scope.mappingAttributes[captionNameIndex]["attributeName"] = attributeName;


            //$scope.mappingAttributes.push({
            //    captionName: $scope.captionName, attributeName: $scope.attributeName, selectedToImport: $scope.defaultSelectionToImport,
            //    attributeType: $scope.defaultAttributeType
            //});
        }
        else {
            var prodCaptionNameIndex = $scope.prodImportData.findIndex((ob => ob["ExcelColumn"] === captionName));

            var prodSelectedtoImport = $scope.prodImportData[prodCaptionNameIndex]["SelectedToImport"];

            //  var attributeName = $scope.prodImportData[prodCaptionNameIndex]["AttrMapping"];

            $scope.mappingAttributes.push({
                captionName: captionName, attributeName: "", selectedToImport: prodSelectedtoImport,
                attributeType: attributeType
            });
        }

        $scope.AttrmappingBasedOnTypeChange();

        if (e.sender._old != "") {
            var unselect = 0;


            angular.forEach($('.unselectattrList').find('select'), function (data) {
                if (data.selectedIndex > 0) {
                    unselect = 1;
                    data.parentElement.className = data.parentElement.className.replace("unselectattrList", "selectattrList");
                }
            });

            if (unselect == 0) {

                $scope.attrMap = false;
            }
            else {
                $scope.attrMap = true;
            }
            $scope.newattribute();
            return true;
        }
        else {
            var unselect = 0;
            angular.forEach($('.selectattrList').find('select'), function (data) {
                if (data.selectedIndex == 0) {
                    data.parentElement.className = data.parentElement.className.replace("selectattrList", "unselectattrList");
                    unselect = 1;
                }
            });
            $scope.newattribute();
            if (unselect == 0) {
                $scope.attrMap = false;
            }
            else {

                $scope.attrMap = true;
            }
            return true;
        }




    };

    $scope.attrmapping = function (event, data, rowIndex) {
        if ($scope.Attrmapping._filter.filters[1]) {
            $scope.Attrmapping._filter.filters.splice(1, 1);
          
        }
   
   
        $scope.newmappingname = event.sender._old;
        $scope.oldmaapingattribute = data;

        $scope.attributeName = $scope.newmappingname;
        $scope.captionName = $scope.oldmaapingattribute;
        //$scope.defaultSelectionToImport = "False";
        //$scope.defaultAttributeType = "0";

        if ($scope.newmappingname == "ITEM#") {
            $scope.newmappingname = "ITEM_NO";
        }
        if ($scope.oldmaapingattribute == "ITEM#") {
            $scope.oldmaapingattribute = "ITEM_NO";
        }
        if ($scope.newmappingname.trim() == "<<<New Attribute>>>") {
            var unselect = 0;


            var captionNameIndex = $scope.mappingAttributes.findIndex((ob => ob["captionName"] === $scope.captionName));


            if (captionNameIndex != -1) {

                $scope.mappingAttributes[captionNameIndex]["attributeName"] = $scope.attributeName;

                //$scope.mappingAttributes.push({
                //    captionName: $scope.captionName, attributeName: $scope.attributeName, selectedToImport: $scope.defaultSelectionToImport,
                //    attributeType: $scope.defaultAttributeType
                //});
            }
            else {

                $scope.mappingAttributes.push({
                    captionName: $scope.captionName, attributeName: $scope.attributeName, selectedToImport: "False",
                    attributeType: "0"
                });
            }




            //   $scope.prodImportData[captionNameIndex]["FieldType"].className = "k-widget k-dropdown k-header unselectattrList";

            angular.forEach($('.select').find('select'), function (data) {
                if (data.selectedIndex == 0) {
                    data.parentElement.className = data.parentElement.className.replace("select", "unselect");
                    unselect = 1;
                }
            });
            angular.forEach($('.unselectattrList').find('select'), function (data) {
                if (data.selectedIndex == 0) {
                    unselect = 1;
                    data.parentElement.className = data.parentElement.className.replace("selectattrList", "unselectattrList");
                }
            });
            angular.forEach($('.unselect').find('select'), function (data) {
                if (data.selectedIndex == 0) {
                    unselect = 1;
                }
                else {
                    data.parentElement.className = data.parentElement.className.replace("unselect", "select");
                }
            });
            angular.forEach($('.unselectattrList').find('select'), function (data) {
                if (data.selectedIndex > 0) {
                    unselect = 1;
                    data.parentElement.className = data.parentElement.className.replace("unselectattrList", "selectattrList");
                }
            });

            if (unselect == 0) {
                $scope.attrMap = false;
            }
            else {
                $scope.attrMap = true;
            }
            $scope.newattribute();


            if ($scope.mappingAttributes[captionNameIndex]["attributeType"] == "") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select the Attribute Type.',
                    type: "error"
                });

                // $scope.attrMap = true;

                //$scope.prodImportData[captionNameIndex]["AttrMapping"] = "";


                //var ddl = $("#id_mappingattribute").data("kendoDropDownList");
                //ddl.optionLabel.text("--- Select Attribute ---")
                //ddl.options.optionLabel = "--- Select Attribute ---";
                //ddl.refresh();
                //ddl.select(0);

                $("#id_mappingattribute_" + rowIndex).data("kendoDropDownList").select(0);


            }

            return true;
        }
        else if ($scope.newmappingname == "") {
            var unselect = 0;
            angular.forEach($('.select').find('select'), function (data) {
                if (data.selectedIndex == 0) {
                    data.parentElement.className = data.parentElement.className.replace("select", "unselect");
                    unselect = 1;
                }
            });
            angular.forEach($('.unselectattrList').find('select'), function (data) {
                if (data.selectedIndex == 0) {
                    unselect = 1;
                    data.parentElement.className = data.parentElement.className.replace("selectattrList", "unselectattrList");
                }
            });
            angular.forEach($('.unselect').find('select'), function (data) {
                if (data.selectedIndex == 0) {
                    unselect = 1;
                }
                else {
                    data.parentElement.className = data.parentElement.className.replace("unselect", "select");
                }
            });
            angular.forEach($('.unselectattrList').find('select'), function (data) {
                if (data.selectedIndex > 0) {
                    unselect = 1;
                    data.parentElement.className = data.parentElement.className.replace("unselectattrList", "selectattrList");
                }
            });
            $scope.newattribute();
            if (unselect == 0) {
                $scope.attrMap = false;
            }
            else {

                $scope.attrMap = true;
            }

            //var captionNameIndex = $scope.mappingAttributes.findIndex((ob => ob["captionName"] === $scope.captionName));


            //if (captionNameIndex != -1) {

            //    $scope.mappingAttributes[captionNameIndex]["attributeName"] = $scope.attributeName;

            //    //$scope.mappingAttributes.push({
            //    //    captionName: $scope.captionName, attributeName: $scope.attributeName, selectedToImport: $scope.defaultSelectionToImport,
            //    //    attributeType: $scope.defaultAttributeType
            //    //});
            //}
            //else {

            //    $scope.mappingAttributes.push({
            //        captionName: $scope.captionName, attributeName: $scope.attributeName, selectedToImport: "False",
            //        attributeType: "0"
            //    });
            //}




            //if ($scope.mappingAttributes[captionNameIndex]["attributeName"] == "") {
            //    $scope.mappingAttributes[captionNameIndex]["attributeType"] = "";

            //    $("#id_attributetype_" + rowIndex).data("kendoDropDownList").select(0);
            //}

            return true;
        }
        if ($scope.selectedTemplate == "" || $scope.selectedTemplate == undefined) {
            $scope.selectedTemplate = $scope.templateId;
        }




        //var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        //var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        //$scope.captionName = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.newmappingname), key, {
        //    keySize: 128 / 8,
        //    iv: iv,
        //    mode: CryptoJS.mode.CBC,
        //    padding: CryptoJS.pad.Pkcs7
        //});
        //$scope.attributeName = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.oldmaapingattribute), key, {
        //    keySize: 128 / 8,
        //    iv: iv,
        //    mode: CryptoJS.mode.CBC,
        //    padding: CryptoJS.pad.Pkcs7
        //});
        /*********Product input screen changes by Aswin *********///






        //$scope.checkChangeMappingAttributenames = $scope.mappingAttributes.find(ob => ob["captionName"] === $scope.captionName);

        //if ($scope.checkChangeMappingAttributenames != undefined) {
        //    $scope.mappingAttributes.pop({
        //        captionName: $scope.captionName, attributeName: $scope.attributeName, selectedToImport: $scope.defaultSelectionToImport,
        //        attributeType: $scope.defaultAttributeType
        //    });

        //    $scope.mappingAttributes.push({
        //        captionName: $scope.captionName, attributeName: $scope.attributeName, selectedToImport: $scope.defaultSelectionToImport,
        //        attributeType: $scope.defaultAttributeType
        //    });
        //}
        //else {
        //    $scope.mappingAttributes.push({
        //        captionName: $scope.captionName, attributeName: $scope.attributeName, selectedToImport: $scope.defaultSelectionToImport,
        //        attributeType: $scope.defaultAttributeType
        //    });
        //}

        //var captionNameIndex = $scope.prodImportData.findIndex((ob => ob["ExcelColumn"] === $scope.captionName));

        //$scope.checkExistingAttributeName = $scope.prodImportData.find(ob => ob["ExcelColumn"] === $scope.attributeName || ob["AttrMapping"] === $scope.attributeName || ob["MappedAttributeName"] === $scope.attributeName);

        //if ($scope.checkExistingAttributeName != undefined) {
        //    $.msgBox({
        //        title: $localStorage.ProdcutTitle,
        //        content: 'The selected Map Attribute is already mapped for the caption.',
        //        type: "error"
        //    });

        //    $scope.prodImportData[captionNameIndex]["AttrMapping"] = "";
        //}

        //else
        //{

        var captionNameIndex = $scope.mappingAttributes.findIndex((ob => ob["captionName"] === $scope.captionName));

        var existAttributeType = $scope.mappingAttributes.findIndex((ob => ob["captionName"] === $scope.captionName))

        if (captionNameIndex != -1) {

            $scope.mappingAttributes[captionNameIndex]["attributeName"] = $scope.attributeName;

            //$scope.mappingAttributes.push({
            //    captionName: $scope.captionName, attributeName: $scope.attributeName, selectedToImport: $scope.defaultSelectionToImport,
            //    attributeType: $scope.defaultAttributeType
            //});
        }
        else {

            $scope.mappingAttributes.push({
                captionName: $scope.captionName, attributeName: $scope.attributeName, selectedToImport: "False",
                attributeType: "0"
            });
        }

        //  }





        /*********Product input screen changes by Aswin *********///

        //dataFactory.mappingattribute($scope.newmappingname, $scope.oldmaapingattribute, $scope.selectedTemplate, $scope.importTypeSelection, $scope.importSelectedvalue).success(function (importresult) {
        //    var unselect = 0;
        //    if ($('.unselect').length > 0) {
        //        angular.forEach($('.unselect').closest("tr"), function (data) {
        //            if (data.children[1].children[0].checked == true) {
        //                data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header select";
        //            }
        //            if (data.children[1].children[0].checked == false) {
        //                data.children[3].children[0].children[0].children[0].className = "k-widget k-dropdown k-header unselect";
        //            }
        //            else if ($('.unselect').length > 0 && $('.unselect option:selected').text() === "") {
        //                $scope.attrMap = true;
        //            }
        //            else {
        //                $scope.attrMap = false;
        //            }
        //        });
        //    }
        //    angular.forEach($('.unselect').find('select'), function (data) {
        //        if (data.selectedIndex == 0) {
        //            unselect = 1;
        //        }
        //    });
        //    angular.forEach($('.unselectattrList').find('select'), function (data) {
        //        if (data.selectedIndex == 0) {
        //            unselect = 1;
        //        }
        //    });
        //    if (unselect == 0) {
        //        $scope.attrMap = false;
        //    }
        //    else {
        //        $scope.attrMap = true;
        //    }
        //}).error(function (error) {
        //    $.msgBox({
        //        title: $localStorage.ProdcutTitle,
        //        content: 'Import failed, please try again.',
        //        type: "error"
        //    });
        //    $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        //    options.error(error);
        //});   
        $scope.Attrmapping = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            filter: "contains",
            transport: {
                read: function (options) {

                    $localStorage.CopymappingAttributes = angular.copy($scope.mappingAttributes);
                    if (options.data.filter.filters.length == 1) {
                        var filter = ""


                        dataFactory.attributeimpmapping($scope.ImportType, $scope.selectedAttributeType).success(function (response) {
                            //angular.forEach(response, function (data) {
                            //});

                            if ($scope.mappingAttributes.length == 0) {
                                $scope.mappingAttributes = $localStorage.CopymappingAttributes;
                            }

                            if ($scope.ImportType == "Products") {
                                for (var i = 0; response.length > i; i++) {
                                    if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                        response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                                    }

                                }
                            }

                            if ($scope.ImportType == "SubProducts") {
                                for (var i = 0; response.length > i; i++) {
                                    if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                        response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                                    }


                                }
                            }


                            $scope.attributeList = response;
                            //$scope.DS1.data = response;
                            //$scope.DS1.options.schema = response;
                            options.success(response);

                            //$scope.checksearch();
                            //$scope.checksearch();


                            //var dropdownlist = $("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList");
                            //dropdownlist.setDataSource(response);
                            //$("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList").select(0);
                            //var dropdownlist = $("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList");
                            //dropdownlist.setDataSource(response);
                            // dropdownlist.dataSource.read();

                            //  $("#id_mappingattribute_" + $scope.attributeRowIndex).options.success(response);


                            //$("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList").options.success = $scope.attributeList;

                            // var dropdownlist = $("#id_mappingattribute_" + $scope.attributeRowIndex).data("kendoDropDownList");

                            // dropdownlist.options.success(response);



                            //$scope.attrMap = true;  // disable mapping based on import type
                            $timeout(function () {
                                blockUI.stop();
                            }, 200)

                        }).error(function (error) {
                            options.error(error);
                        });

                    }
                    else {
                        var result = [];
                        var filter = options.data.filter.filters[1].value;
                        if (filter != "") {
                            for (var i = 0; i < $scope.attributeList.length; i++) {

                                if ($scope.attributeList[i].ATTRIBUTE_NAME.toLowerCase().includes(filter.toLowerCase())) {
                                    result.push($scope.attributeList[i]);
                                }
                            }

                            if (result.length == 0) {
                                if ($scope.Attrmapping._filter.filters[1]) {
                                    $scope.Attrmapping._filter.filters.splice(1, 1);
                                }
                                options.success($scope.attributeList);
                            } else {
                                options.success(result);
                            }
                            $timeout(function () {
                                blockUI.stop();
                            }, 200)

                        } else if (filter == "") {
                            options.success($scope.attributeList);
                            $timeout(function () {
                                blockUI.stop();
                            }, 200)
                        }
                    }
                }
            },
            sort: {
                field: 'ATTRIBUTE_NAME',
                dir: 'asc'
            }




        });
    };


    $scope.attrmappingexpp = function (event) {
        $scope.atrributename = event.sender._old;
        if ($scope.atrributename == "ITEM#") {
            $scope.atrributename = "ITEM_NO";
        }
    };

    $scope.importResultDatasource = new kendo.data.DataSource({
        type: "json", autobind: false,
        sort: { field: "ErrorMessage", dir: "asc" },
        transport: {
            read: function (options) {
                if ($scope.ItemImport == false) {
                    dataFactory.getFinishImportResults($scope.importSessionID).success(function (response) {
                        $('#myModal').hide();
                        $scope.progressComplete = true;
                        /*********Product input screen changes by Aswin *********///
                        if ($scope.getImportType == "Products") {
                            $scope.getImportType = "Items"
                        }

                        if ($scope.getImportType == "Families") {
                            $scope.getImportType = "Products"
                        }

                        if ($scope.getImportType == "Familyproducts") {
                            $scope.getImportType = "ProductItems"
                        }

                        $("#completed").text($scope.getImportType + " Import Failed");
                        /*********Product input screen changes by Aswin *********///
                        $scope.showOkButton = false;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    dataFactory.GetItemImportErrorlist().success(function (errorList) {
                        $('#myModal').hide();
                        $scope.progressComplete = true;
                        if ($scope.getImportType == "Products") {
                            $scope.getImportType = "Items"
                        }

                        if ($scope.getImportType == "Families") {
                            $scope.getImportType = "Products"
                        }

                        if ($scope.getImportType == "Familyproducts") {
                            $scope.getImportType = "ProductItems"
                        }

                        $("#completed").text($scope.getImportType + " Import Failed");

                        $scope.showOkButton = false;
                        options.success(errorList);
                    }).error(function (error) {
                        options.error(error);
                    });

                }
            }
        }, schema: {
            model: {
                id: "ErrorMessage",
                fields: {
                    ErrorMessage: { type: "string" },
                    ErrorProcedure: { type: "string" },
                    ErrorSeverity: { type: "string" },
                    ErrorState: { type: "string" },
                    ErrorNumber: { type: "string" },
                    ErrorLine: { type: "string" }
                }
            }
        }

    });


    $scope.validatetResultDatasource = new kendo.data.DataSource({
        type: "json", autobind: false,
        transport: {
            read: function (options) {
                dataFactory.validateImportResults($scope.importSessionID, $scope.ImportType).success(function (response) {
                    options.success(response);
                    if (response != null) {
                        var obj = jQuery.parseJSON(response.Data.Data);

                        for (var i = 0 ; obj.length > i ; i++) {
                            // Attribut_name
                            if (obj[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                obj[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                                obj[i].VALIDATION_TYPE = obj[i].VALIDATION_TYPE.replace("CATALOG_ITEM_NO", $localStorage.CatalogItemNumber);
                            }

                            if (obj[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                obj[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;

                                obj[i].VALIDATION_TYPE = obj[i].VALIDATION_TYPE.replace("SUBCATALOG_ITEM_NO", $localStorage.CatalogSubItemNumber);
                            }
                            //Attribute_value
                            if (obj[i]["ATTRIBUTE_VALUES"] == "ITEM#" || obj[i]["ATTRIBUTE_VALUES"] == "CATALOG_ITEM_NO") {
                                obj[i]["ATTRIBUTE_VALUES"] = $localStorage.CatalogItemNumber;
                                obj[i].VALIDATION_TYPE = obj[i].VALIDATION_TYPE.replace("CATALOG_ITEM_NO", $localStorage.CatalogItemNumber);
                            }

                            if (obj[i]["ATTRIBUTE_VALUES"] == "SUBITEM#" || obj[i]["ATTRIBUTE_VALUES"] == "SUBCATALOG_ITEM_NO") {
                                obj[i]["ATTRIBUTE_VALUES"] = $localStorage.CatalogSubItemNumber;
                                obj[i].VALIDATION_TYPE = obj[i].VALIDATION_TYPE.replace("SUBCATALOG_ITEM_NO", $localStorage.CatalogSubItemNumber);
                            }
                        }

                        $scope.validateLogprod = obj;
                        $scope.validateLogcolumn = response.Data.Columns;
                        $scope.importErrorFileWindowdiv = true;
                    }

                }).error(function (error) {
                    options.error(error);
                });
            }
        }

    });

    $scope.GetValidationResult = function () {
        dataFactory.validateImportResults($scope.importSessionID[$scope.importSessionID.length - 1], $scope.ImportType).success(function (response) {
            if (response != null) {
                var obj = jQuery.parseJSON(response.Data.Data);


                for (var i = 0 ; obj.length > i ; i++) {
                    // Attribut_name
                    if (obj[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                        obj[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                        obj[i].VALIDATION_TYPE = obj[i].VALIDATION_TYPE.replace("CATALOG_ITEM_NO", $localStorage.CatalogItemNumber);
                    }

                    if (obj[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                        obj[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;

                        obj[i].VALIDATION_TYPE = obj[i].VALIDATION_TYPE.replace("SUBCATALOG_ITEM_NO", $localStorage.CatalogSubItemNumber);
                    }
                    //Attribute_value
                    if (obj[i]["ATTRIBUTE_VALUES"] == "ITEM#" || obj[i]["ATTRIBUTE_VALUES"] == "CATALOG_ITEM_NO") {
                        obj[i]["ATTRIBUTE_VALUES"] = $localStorage.CatalogItemNumber;
                        obj[i].VALIDATION_TYPE = obj[i].VALIDATION_TYPE.replace("CATALOG_ITEM_NO", $localStorage.CatalogItemNumber);
                    }

                    if (obj[i]["ATTRIBUTE_VALUES"] == "SUBITEM#" || obj[i]["ATTRIBUTE_VALUES"] == "SUBCATALOG_ITEM_NO") {
                        obj[i]["ATTRIBUTE_VALUES"] = $localStorage.CatalogSubItemNumber;
                        obj[i].VALIDATION_TYPE = obj[i].VALIDATION_TYPE.replace("SUBCATALOG_ITEM_NO", $localStorage.CatalogSubItemNumber);
                    }



                }


                $scope.validateLogprod = obj;
                $scope.validateLogcolumn = response.Data.Columns;
                $scope.importErrorFileWindowdiv = true;
                $scope.importErrorWindowfortabledesignerdiv = false;
            }
        });
    };

    $scope.importResultGridOptionsvalidation = {
        dataSource: $scope.validatetResultDatasource,
        //sortable: true,
        scrollable: true, resizable: true,
        //filterable: true,
        //pageable: true,
        autoBind: true,

    };

    $scope.importResultGridOptions = {
        dataSource: $scope.importResultDatasource,
        //sortable: true,
        scrollable: true, resizable: true,
        //filterable: true,
        //pageable: true,
        autoBind: false,
        columns: [
            { field: "ErrorMessage", title: "Error Message", width: "200px" },
            { field: "ErrorProcedure", title: "Error Procedure", width: "200px" },
            { field: "ErrorSeverity", title: "Error Severity", width: "200px" },
            { field: "ErrorState", title: "Error State", width: "200px" },
            { field: "ErrorNumber", title: "Error Number", width: "200px" },
            { field: "ErrorLine", title: "Error Line" }
        ]
    };
    $scope.closePopup = function () {
        $scope.showOkButton = false;
        $('#myModal').hide();
        $("#importCompleteness").kendoProgressBar({
            type: "chunk",
            chunkCount: 10,
            min: 0,
            max: 10,
            value: 1
        }).data("kendoProgressBar");
        $("#completed").text("10 %");
    };
    $scope.importSuccessResultDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autobind: false,
        scrollable: true,
        sort: { field: "CATALOG_NAME", dir: "asc" },
        transport: {
            read: function (options) {

                dataFactory.getFinishImportSuccessResults($scope.importSessionID, $scope.getImportType).success(function (response) {
                    options.success(response);
                    var importProgress = $("#importCompleteness").kendoProgressBar({
                        type: "chunk",
                        chunkCount: 10,
                        min: 0,
                        max: 10,
                        value: 1
                    }).data("kendoProgressBar");
                    importProgress.value(10);
                    $scope.importCompletedStatus = false;

                    if ($scope.getImportType == "Products") {
                        $scope.getImportType = "Items"
                    }

                    if ($scope.getImportType == "Families") {
                        $scope.getImportType = "Products"
                    }

                    if ($scope.getImportType == "Familyproducts") {
                        $scope.getImportType = "ProductItems"
                    }


                    /*********Product input screen changes by Aswin *********///
                    $("#completed").text($scope.getImportType + " Import Successful");
                    /*********Product input screen changes by Aswin *********///
                    $scope.showOkButton = true;
                    //$scope.UpdateImportFlagFunctionCall();
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    CATALOG_NAME: { type: "string" },
                    CATEGORY_DETAILS: { type: "string" },
                    FAMILY_DETAILS: { type: "string" },
                    SUBFAMILY_DETAILS: { type: "string" },
                    PRODUCT_DETAILS: { type: "string" },
                    STATUS: { type: "string" }
                }
            }
        }

    });

    /*********Product input screen changes by Aswin *********///

    $scope.selectedSheet = [];

    $scope.mappingValidation = [];

    $scope.BtnNextMapping = function () {

        debugger;

        if ($scope.mappingAttributes.length == 0) {
            $scope.mappingAttributes = $localStorage.CopymappingAttributes;

        }

        var excelSheets = $scope.importExcelSheetSelction._data;

        if ($scope.selectedSheet.length == 0) {
            $scope.selectedSheet.push($scope.importExcelSheetDDSelectionValue);
        }


        //  if ($scope.selectedSheet.length > 0 && !($scope.selectedSheet.includes("Categories")) && $scope.importSelectedvalue=="Categories")
        //  {
        //      $scope.selectedSheet.push($scope.importSelectedvalue);
        //  }
        //  else if ($scope.selectedSheet.length > 0 && !($scope.selectedSheet.includes("Families")) && $scope.importSelectedvalue == "Families") {
        //      $scope.selectedSheet.push($scope.importSelectedvalue);
        //  }
        //else  if ($scope.selectedSheet.length > 0 && !($scope.selectedSheet.includes("Products")) && $scope.importSelectedvalue == "Products") {
        //      $scope.selectedSheet.push($scope.importSelectedvalue);
        //  }
        //else  if ($scope.selectedSheet.length > 0 && !($scope.selectedSheet.includes("TableDesigner")) && $scope.importSelectedvalue == "TableDesigner") {
        //      $scope.selectedSheet.push($scope.importSelectedvalue);
        //  }

        ////if ($scope.mappingAttributes.length > 0) {

        $scope.duplicateAttributeNames = [];
        var hasDuplicate = false;
        $scope.mappingAttributes.map(v => v.attributeName).sort().sort((a, b) => {
            if (a.trim() != "<<<New Attribute>>>" && a != "" && a == b) {
                var duplicateAttributeNamesIndex = $scope.duplicateAttributeNames.findIndex((ob => ob === a));

                if (duplicateAttributeNamesIndex == -1) {

                    $scope.duplicateAttributeNames.push(a);
                }
                hasDuplicate = true;
            }
        });
        for (var i = 0; i < $scope.mappingAttributes.length; i++) {
            if ($scope.mappingAttributes[i].captionName.contains("__") || $scope.mappingAttributes[i].captionName.contains("*") || $scope.mappingAttributes[i].captionName.contains("&") || $scope.mappingAttributes[i].captionName.contains("%") || $scope.mappingAttributes[i].captionName.contains("+")) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Attribute name should not contain special characters like (* & % + __ ).',
                    type: "info"
                });
                return true;
            }
        }

        if (hasDuplicate == true) {
            //$.msgBox({
            //    title: $localStorage.ProdcutTitle,
            //    content: 'More than one captions mapped with same attribute name.The following attribute names are duplicate:' + "<br />" + "<br />"
            //        + $scope.duplicateAttributeNames,
            //    type: "error"
            //});


            $('#id_DuplicateAttributesModal').show();

            $scope.BindDuplicateAttributes = $scope.duplicateAttributeNames;
        }
        else {
            $scope.saveAllMAppingAttributes();
        }

        //   }	
        //if ($scope.importTypeSelection == "TableDesigner") {	
        //    $scope.saveAllMAppingAttributes();	
        //}

        if ($scope.selectedSheet.length != $scope.mappingValidation.length) {
            if ($scope.selectedTemplate == "" || $scope.selectedTemplate == undefined) {
                $scope.selectedTemplate = $scope.templateId;
            }

            $scope.updateMappingImportTypeFlag();
        }

        // if ($scope.selectedSheet.length == excelSheets.length && $scope.selectedSheet.length != $scope.mappingValidation.length)
        //{
        //    $scope.selectedSheet = [];
        //}

        var continueLoop = true;

        angular.forEach(excelSheets, function (sheet) {
            if (continueLoop) {
                if (!($scope.selectedSheet.includes(sheet.TABLE_NAME))) {

                    ////Newly added for Name Change Oct 19 2022
                    //if (sheet.TABLE_NAME == "Categories") {
                    //    sheet.TABLE_NAME = "Categories"
                    //    $scope.importSelectedvalue = "Categories"
                    //    $scope.importExcelSheetDDSelectionValue = "Categories"
                    //    $scope.importSheetSelected = "Categories"
                    //}
                    //if (sheet.TABLE_NAME == "Products") {
                    //    sheet.TABLE_NAME = "Families"
                    //    $scope.importSelectedvalue = "Products"
                    //    $scope.importExcelSheetDDSelectionValue = "Products"
                    //    $scope.importSheetSelected = "Products"
                    //}
                    //if (sheet.TABLE_NAME == "Items") {
                    //    sheet.TABLE_NAME = "Products"
                    //    $scope.importSelectedvalue = "Items"
                    //    $scope.importExcelSheetDDSelectionValue = "Items"
                    //    $scope.importSheetSelected = "Items"
                    //}

                    $scope.importSelectedvalue = sheet.TABLE_NAME;
                    $scope.importSheetSelected = sheet.TABLE_NAME;
                    $scope.importExcelSheetDDSelectionValue = sheet.TABLE_NAME;
                    $scope.selectedSheet.push(sheet.TABLE_NAME);
                    continueLoop = false;


                }
                //else if (sheet.TABLE_NAME == "Families" && !($scope.selectedSheet.includes("Families"))) {
                //    $scope.importSelectedvalue = "Families";
                //    $scope.importSheetSelected = "Families";
                //    $scope.importExcelSheetDDSelectionValue = "Families";
                //    $scope.selectedSheet.push($scope.importSelectedvalue);
                //    continueLoop = false;
                //}
                //else if (sheet.TABLE_NAME == "Products" && !($scope.selectedSheet.includes("Products"))) {
                //    $scope.importSelectedvalue = "Products";
                //    $scope.importSheetSelected = "Products";
                //    $scope.importExcelSheetDDSelectionValue = "Products";
                //    $scope.selectedSheet.push($scope.importSelectedvalue);
                //    continueLoop = false;
                //}

                //else if (sheet.TABLE_NAME == "TableDesigner" && !($scope.selectedSheet.includes("TableDesigner"))) {
                //    $scope.importSelectedvalue = "TableDesigner";
                //    $scope.importSheetSelected = "TableDesigner";
                //    $scope.importExcelSheetDDSelectionValue = "TableDesigner";
                //    $scope.selectedSheet.push($scope.importSelectedvalue);
                //    continueLoop = false;
                //}


                //else if ($scope.importSelectedvalue == "TableDesigner") {
                //    continueLoop = false;
                //}


            }
        });



        if (continueLoop == false && hasDuplicate == false) {

            blockUI.start();
            $scope.getImportTypeBasedonSheet();

            blockUI.stop();
        }



        ///Validation page
        //if (continueLoop == true) {


        //    $scope.passvalidationNext = false;
        //    //if ($scope.importExcelSheetDDSelectionValue == "TableDesigner") {
        //    //    $scope.ValidateDiv = false;
        //    //    $scope.ValidatetableDesignerDiv = true;
        //    //    $scope.tbldesigner = true;
        //    //}
        //    // else {
        //    $scope.ValidateDiv = true;
        //    $scope.tbldesigner = false;
        //    //$scope.validationSheetnames.read();
        //    //  }


        //    $scope.showValidatediv = false;
        //    $scope.showexpressiondiv = true;
        //    $scope.AttributeMappingdiv = false;


        //    $scope.ProcessName = "Validate Content";
        //    $("#imptamp").removeClass("leftMenuactive");
        //    $("#logim").removeClass("leftMenuactive");
        //    $("#impvalidation").removeClass("leftMenuactive");
        //    $("#Atrmappingimp").removeClass("leftMenuactive");

        //    $("#sheetselectimp").removeClass("leftMenuactive");
        //    $("#impvalidation").addClass("leftMenuactive");
        //    $("#import").removeClass("leftMenuactive");

        //}

        //group attributes
        if (continueLoop == true && hasDuplicate == false) {

            // $scope.passvalidationNext = false;
            //if ($scope.importExcelSheetDDSelectionValue == "TableDesigner") {
            //    $scope.ValidateDiv = false;
            //    $scope.ValidatetableDesignerDiv = true;
            //    $scope.tbldesigner = true;
            //}
            // else {

            //  $scope.GroupAttributeDiv = true;
            //   $scope.ValidateDiv = true;
            //  $scope.tbldesigner = false;
            $scope.GroupAttributeSheetnamesDataSource.read();
            //  }


            //$scope.showValidatediv = false;
            //$scope.showexpressiondiv = true;
            //$scope.AttributeMappingdiv = false;

            //$scope.ProcessName = "Group Attributes";
            //// $scope.ProcessName = "Validate Content";
            //$("#imptamp").removeClass("leftMenuactive");
            //$("#logim").removeClass("leftMenuactive");
            //$("#impvalidation").removeClass("leftMenuactive");
            //$("#Atrmappingimp").removeClass("leftMenuactive");

            //$("#sheetselectimp").removeClass("leftMenuactive");
            ////   $("#impvalidation").addClass("leftMenuactive");
            //$("#Groupattribute").addClass("leftMenuactive");
            //$("#import").removeClass("leftMenuactive");
            //$scope.AttributeGroupDataSource.read();

        }



        //----------------------- uncomment this after checking ---------------
        //$scope.passvalidationNext = false;
        //if ($scope.importExcelSheetDDSelectionValue == "TableDesigner") {
        //    $scope.ValidateDiv = false;
        //    $scope.ValidatetableDesignerDiv = true;
        //    $scope.tbldesigner = true;
        //}
        //else {
        //    $scope.ValidateDiv = true;
        //    $scope.tbldesigner = false;
        //}

        // $scope.showValidatediv = false;
        //$scope.showexpressiondiv = true;
        //$scope.AttributeMappingdiv = false;


        //$scope.ProcessName = "Validate Content";
        //$("#imptamp").removeClass("leftMenuactive");
        //$("#logim").removeClass("leftMenuactive");
        //$("#impvalidation").removeClass("leftMenuactive");
        //$("#Atrmappingimp").removeClass("leftMenuactive");

        //$("#sheetselectimp").removeClass("leftMenuactive");
        //$("#impvalidation").addClass("leftMenuactive");
        //$("#import").removeClass("leftMenuactive");

        //----------------------- uncomment this after checking ---------------

        //if ($scope.passvalidation == true) {

        //$('#attrMappBtn').hide();



        // For mapping attribute



        //  for (var i = 0 ; i < $scope.prodImportData.length ; i++) {
        //if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogItemNumber.toUpperCase()) {
        //    $scope.prodImportData[i]["ExcelColumn"] = "ITEM#";
        //    $scope.prodImportData[i]["CatalogField"] = "ITEM#";
        //    $scope.prodImportData[i]["FieldType"] = "0";
        //    $scope.prodImportData[i]["IsSystemField"] = "True";
        // $scope.prodImportData.push ="MappedAttributeName";
        //}
        //if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogSubItemNumber.toUpperCase()) {
        //    $scope.prodImportData[i]["ExcelColumn"] = "SUBITEM#"
        //    $scope.prodImportData[i]["CatalogField"] = "SUBITEM#"
        //    $scope.prodImportData[i]["FieldType"] = "0";
        //    $scope.prodImportData[i]["IsSystemField"] = "True";
        //    $scope.prodImportData[i]["SelectedToImport"] = "True";

        //}
        //    }


        // END
        //}
        //else {
        //   // $('#attrMappBtn').hide();
        //    $scope.ProcessName = "Import Content";
        //    $scope.showexpressiondiv = true;
        //    $("#expressionupdate").hide();
        //    $("#impfirst").show();
        //    $("#newsheetselection").show();
        //    $("#buttoncheck").show();
        //    $("#imptamp").removeClass("leftMenuactive");
        //    $("#logim").removeClass("leftMenuactive");
        //    $("#impvalidation").removeClass("leftMenuactive");
        //    $("#Atrmappingimp").removeClass("leftMenuactive");
        //    $("#impvalidation").removeClass("leftMenuactive");
        //    $("#sheetselectimp").removeClass("leftMenuactive");
        //    $("#imptamp").removeClass("leftMenuactive");
        //    $("#import").addClass("leftMenuactive");
        //}
    };
    /*********Product input screen changes by Aswin *********///

    $scope.GoToSetup = function () {
        $("#import").removeClass("leftMenuactive");
        $scope.BatchImport = false;
        $scope.ImportCompeted();
        $scope.ResetPage();
        // $scope.GetBatchLogList();

    };
    $scope.ImportCompeted = function () {

        $scope.validationPassed = false;
        $scope.ResultDisplay = false;
        $scope.showValidatediv = false;

        $scope.listAllSheet = [];
        $scope.templateDetails = false;
        $scope.ResultDisplay = false;
        $scope.AttributeMappingdiv = false;
        $scope.ProcessName = "Validate Content";
        $scope.Validateback();
        /***************** Product input screen ******************/
        $scope.importAllResults = [];
        $scope.validationSelectionSheet = [];
        $scope.importSelectionSheet = [];
        $scope.mappingAttributes = [];
        $scope.templateName = "";
        /***************** Product input screen ******************/
    };
    $scope.progressComplete = false;
    $("#importCompleteness").kendoProgressBar({
        type: "chunk",
        chunkCount: 10,
        min: 0,
        max: 10,
        value: 1
    }).data("kendoProgressBar");
    $scope.GetImportStatus = function (percentage, rowCount) {
        //dataFactory.GetImportStatus('').success(function (response) {
        //    
        //    if (response == "" || response == null) {
        //        response = 10;
        //    }
        //    alert(response);

        $timeout(function () {
            if (percentage <= 6) {
                if ($scope.progressComplete != true) {
                    var importProgress = $("#importCompleteness").kendoProgressBar({
                        type: "chunk",
                        chunkCount: 10,
                        min: 0,
                        max: 10,
                        value: 0.5
                    }).data("kendoProgressBar");

                    importProgress.value((percentage));
                    $("#completed").text(((percentage) * 10) + "%");
                    $scope.GetImportStatus(percentage + 1, rowCount);
                }
            }
            else {
                if ($scope.progressComplete != true) {
                    var importProgress = $("#importCompleteness").kendoProgressBar({
                        type: "chunk",
                        chunkCount: 10,
                        min: 0,
                        max: 10,
                        value: 1
                    }).data("kendoProgressBar");

                    importProgress.value(6);
                    $("#completed").text("60%");
                }
                $timeout(function () {
                    if ($scope.progressComplete != true) {
                        var importProgress = $("#importCompleteness").kendoProgressBar({
                            type: "chunk",
                            chunkCount: 10,
                            min: 0,
                            max: 10,
                            value: 1
                        }).data("kendoProgressBar");

                        importProgress.value(7);
                        $("#completed").text("70%");
                    }
                }, 5000);
                $timeout(function () {
                    if ($scope.progressComplete != true) {
                        var importProgress = $("#importCompleteness").kendoProgressBar({
                            type: "chunk",
                            chunkCount: 10,
                            min: 0,
                            max: 10,
                            value: 1
                        }).data("kendoProgressBar");

                        importProgress.value(8);
                        $("#completed").text("80%");
                    }
                }, 10000);
                $timeout(function () {
                    if ($scope.progressComplete != true) {
                        var importProgress = $("#importCompleteness").kendoProgressBar({
                            type: "chunk",
                            chunkCount: 10,
                            min: 0,
                            max: 10,
                            value: 1
                        }).data("kendoProgressBar");
                        importProgress.value(9);
                        $("#completed").text("90%");
                    }
                }, 15000);
            }
        }, 4000);

        //  $scope.GetImportStatus(sheetName, excelPath);
        //if (response == "60") {
        //    $timeout(function() {
        //        var elem = document.getElementById("myBar");
        //        elem.innerHTML = response.toString() + '%';
        //        var elem = document.getElementById("myBar");
        //        var width = response / 10;
        //        var id = setInterval(frame, 8);

        //        function frame() {


        //            if (width >= 100) {
        //                clearInterval(id);
        //                $scope.progressComplete = true;
        //            } else {
        //                width++;
        //                elem.style.width = width + '%';
        //                elem.innerHTML = width * 1;
        //                elem.innerHTML = elem.innerHTML.toString().split('.')[0] + '%';
        //            }
        //        }

        //        if ($scope.progressComplete == false)
        //            $scope.GetImportStatus(sheetName, excelPath);
        //    }, 200);
        //} else {
        //    var elem = document.getElementById("myBar");
        //    elem.innerHTML = response.toString() + '%';
        //    var elem = document.getElementById("myBar");
        //    var width = response / 10;
        //    var id = setInterval(frame, 8);

        //    function frame() {


        //        if (width >= 100) {
        //            clearInterval(id);
        //            $scope.progressComplete = true;
        //        } else {
        //            width++;
        //            elem.style.width = width + '%';
        //            elem.innerHTML = width * 1;
        //            elem.innerHTML = elem.innerHTML.toString().split('.')[0] + '%';
        //        }
        //    }

        //    if ($scope.progressComplete == false)
        //        $scope.GetImportStatus(sheetName, excelPath);
        //}
        //    var elem = document.getElementById("myBar");
        //    var width = 3;
        //    var id = setInterval(frame, 8);
        //    function frame() {


        //        if (width >= 100) {
        //            clearInterval(id);
        //            $scope.progressComplete = true;
        //        } else {
        //            width++;
        //            elem.style.width = width + '%';
        //            elem.innerHTML = width * 1;
        //            elem.innerHTML = elem.innerHTML.toString().split('.')[0] + '%';
        //        }


        //    }
        //    $scope.GetImportStatus(sessionId);
        //} else {
        //    alert(response);
        //    var elem = document.getElementById("myBar");
        //    var width = response / 100;
        //    var id = setInterval(frame, 10);


        //    if (progressComplete == false)
        //        $scope.GetImportStatus(sessionId);

        //    function frame() {


        //        if (width >= 100) {
        //            clearInterval(id);
        //            $scope.progressComplete = true;
        //        } else {
        //            width++;
        //            elem.style.width = width + '%';
        //            elem.innerHTML = width * 1;
        //            elem.innerHTML = elem.innerHTML.toString().split('.')[0] + '%';
        //        }


        //    }
        //}
        // });
    };

    /*********Product input screen changes by Aswin *********///

    $scope.importAllResults = [];

    $scope.finishImport = function () {
        if ($scope.templateId == undefined) {
            $scope.templateId = 0;
            blockUI.stop();
        };
        if ($scope.IsSelectAll == true) {
            selectAll = 1;
        }
        if ($scope.IsSelectAll == false) {
            selectAll = 0;
        }
        if ($scope.selectedTemplate != "" && $scope.selectedTemplate != undefined && $scope.templateId == "0") {
            $scope.templateId = $scope.selectedTemplate;
        }
        dataFactory.GetImportSpecs($scope.validationimportSelectedvalue, $scope.excelPath, $scope.templateId, $scope.ImportType, selectAll).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.prodImportData = obj.Data;


            // START
            // When storage value in ITEM#

            for (var i = 0 ; i < $scope.prodImportData.length ; i++) {

                var ItemValue = $scope.prodImportData[i]["ExcelColumn"];

                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogItemNumber.toUpperCase()) {
                    $scope.prodImportData[i]["ExcelColumn"] = $localStorage.CatalogItemNumber;
                    $scope.prodImportData[i]["CatalogField"] = $localStorage.CatalogItemNumber;
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";

                }
                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogSubItemNumber.toUpperCase()) {
                    $scope.prodImportData[i]["ExcelColumn"] = $localStorage.CatalogSubItemNumber;
                    $scope.prodImportData[i]["CatalogField"] = $localStorage.CatalogSubItemNumber;
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";

                }
                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == "ITEM#") {

                    //$scope.prodImportData[i]["CatalogField"] = "ITEM#";
                    //$scope.prodImportData[i]["FieldType"] = "";
                    //$scope.prodImportData[i]["IsSystemField"] = "";
                    //$scope.prodImportData[i]["SelectedToImport"] = "False";
                    //$scope.prodImportData[i]["AttrMapping"] = "";
                    //$scope.prodImportData[i]["ExcelColumn"] = "ITEM#";

                    $scope.prodImportData[i]["ExcelColumn"] = "ITEM#";
                    $scope.prodImportData[i]["CatalogField"] = "ITEM#";
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";


                }
                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == "SUBITEM#") {

                    //$scope.prodImportData[i]["CatalogField"] = "SUBITEM#";
                    //$scope.prodImportData[i]["FieldType"] = "";
                    //$scope.prodImportData[i]["IsSystemField"] = "";
                    //$scope.prodImportData[i]["SelectedToImport"] = "False";
                    //$scope.prodImportData[i]["AttrMapping"] = "";
                    //$scope.prodImportData[i]["ExcelColumn"] = "SUBITEM#";

                    $scope.prodImportData[i]["ExcelColumn"] = "SUBITEM#"
                    $scope.prodImportData[i]["CatalogField"] = "SUBITEM#"
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";
                }


            }

            $scope.StartfinishImport();


        }).error(function (error) {
            options.error(error);
            blockUI.stop();
        });
    }

    /*********Product input screen changes by Aswin *********///

    $scope.StartfinishImport = function () {
        blockUI.stop();
        $scope.importCompletedStatus = true;
        $scope.progressComplete = false;
        $scope.scheduleImport = true;
        $scope.ItemImport = false;
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#Atrmappingimp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");

        $("#createTemplate").removeClass("leftMenuactive");

        $("#imptamp").removeClass("leftMenuactive");

        $("#impvalidation").removeClass("leftMenuactive");
        $("#import").addClass("leftMenuactive");

        $('.imageloader').hide();

        $scope.importStatusFlag = "0;"

        var DateTime = new Date($('#txtDataTime').val());
        var hour = DateTime.getHours() == '0' ? '00' : DateTime.getHours().toString().length == 1 ? '0' + DateTime.getHours() : DateTime.getHours();
        var min = DateTime.getMinutes() == '0' ? '00' : DateTime.getMinutes().toString().length == 1 ? '0' + DateTime.getMinutes() : DateTime.getMinutes();
        var scheduleDate = DateTime.getDate() + "/" + (DateTime.getMonth() + 1) + "/" + DateTime.getFullYear() + " " + hour + ":" + min;
        //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
        //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
        //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
        //$('#impvalidation')[0].className = 'k-item';
        //$('#logim')[0].className = 'k-item k-state-disabled';
        //$('#imptamp')[0].className = 'k-item k-state-disabled';
        $scope.getImportType = $scope.ImportType;
        if ($scope.EnableBatch == false) {
            scheduleDate = "";
        }
        $scope.impimp = true;

        if ($scope.EnableBatch == true) {
            dataFactory.finishImport($rootScope.SelectedFileForUploadnamemain, $scope.importExcelSheetDDSelectionValue, $scope.allowDuplication, $scope.ImportType, $scope.ImportcatalogIdAttr, $scope.excelPath, $scope.EnableBatch, scheduleDate, $scope.templateId, $scope.prodImportData).success(function (importresult) {
                //    $scope.loadingimg.center().close();
                $scope.importSessionID = importresult.split("~", 2); // kendo data source ;
                if (importresult == "Batch in Queue") {
                    $scope.scheduleImport = false;
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Added to queue.Please check the Batch log.',
                        type: "info"
                    });

                    $scope.BatchImport = true;
                    $scope.scheduleImport = false;
                    return true;
                }
            });
            return true;
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Import may take some time to complete.",
                type: "confirm",
                buttons: [{ value: "Ok" },
                    { value: "Cancel" }
                ],
                success: function (result) {
                    if (result === "Ok") {
                        if ($scope.EnableBatch != true) {
                            $('#myModal').show();

                            var importProgress = $("#importCompleteness").kendoProgressBar({
                                type: "chunk",
                                chunkCount: 10,
                                min: 0,
                                max: 10,
                                value: 1
                            }).data("kendoProgressBar");
                            importProgress.value(1);
                            $("#completed").text("10%");
                            var modal = document.getElementById('myModal');

                            modal.style.display = "block";
                        }


                        ///***************** Product input screen changes by Aswin  ***********************///////


                        $scope.validationImportSheetName = $scope.validationimportSelectedvalue;

                        dataFactory.GetImportStatus($scope.excelPath, $scope.validationImportSheetName).success(function (rowCount) {

                            $scope.GetImportStatus(1, rowCount);
                            if ($scope.listAllSheet.length > 0) {
                                var lastVal = $scope.listAllSheet[$scope.listAllSheet.length - 1];
                                if (lastVal != $scope.importSelectedvalue)
                                    $scope.listAllSheet.push($scope.importSelectedvalue);
                            }
                            else
                                $scope.listAllSheet.push($scope.importSelectedvalue);
                            var continueLoop = true;
                            var excelSheets = $scope.importExcelSheetSelction._data;
                            var nextValue = 0;
                            if (excelSheets.length == 1) {
                                $scope.sheetCompleted = true;
                            }
                            else if (excelSheets.length >= 1 && $scope.listAllSheet.length < excelSheets.length) {

                                $scope.sheetCompleted = false;
                            } else {
                                $scope.sheetCompleted = true;
                            }

                            $scope.validationImportSheetName = $scope.validationimportSelectedvalue;


                            $scope.importMappingAtttributeErrorWindow = false;

                            ///***************** Product input screen changes by Aswin  ***********************///////

                            dataFactory.finishImport($rootScope.SelectedFileForUploadnamemain, $scope.validationimportSelectedvalue, $scope.allowDuplication, $scope.ImportType, $scope.ImportcatalogIdAttr, $scope.excelPath, $scope.EnableBatch, "", $scope.templateId, $scope.prodImportData).success(function (importresult) {
                                //    $scope.loadingimg.center().close();
                                $scope.scheduleImport = false;
                                if ($scope.EnableBatch != true) {
                                    var importProgress = $("#importCompleteness").kendoProgressBar({
                                        type: "chunk",
                                        chunkCount: 10,
                                        min: 0,
                                        max: 10,
                                        value: 1
                                    }).data("kendoProgressBar");
                                    importProgress.value(9);
                                    $("#completed").text("90 %");
                                }
                                $scope.progressComplete = true;

                                $scope.importSessionID = importresult.split("~", 2); // kendo data source ;
                                if (importresult == "Batch in Queue") {

                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'Added to queue.Please check the Batch log.',
                                        type: "info"
                                    });
                                    return true;
                                }


                                // $scope.importAllResults.push(importresult);

                                if (importresult.includes('ITEM#') && !importresult.includes('Import Failed')) {

                                    $scope.ImportSheetName = $scope.validationimportSelectedvalue;

                                    if ($scope.importSelectionSheet.length == 0) {
                                        $scope.importSelectionSheet.push($scope.ImportSheetName);
                                    }
                                    //else {
                                    //    $scope.importSelectionSheet.push($scope.ImportSheetName);
                                    //}

                                    $scope.importStatusFlag = "1";
                                    $scope.UpdateImportFlagFunctionCall();


                                    // ///Loop the excel sheet import types ///////////////////////////////

                                    var excelSheets = $scope.importSheetNames;

                                    var continueLoop = true;

                                    $scope.viewLogImportType = $scope.ImportType;

                                    //Import success result Import type name changes

                                    //if ($scope.viewLogImportType == "Products") {
                                    //    $scope.viewLogImportType = "Items"
                                    //}

                                    //else if ($scope.viewLogImportType == "Families") {
                                    //    $scope.viewLogImportType = "Products"
                                    //}
                             
                                    //else if ($scope.viewLogImportType == "Familyproducts") {
                                    //    $scope.viewLogImportType = "ProductItems"
                                    //}

                                    if ($scope.ImportSheetName == "Products") {
                                        $scope.viewLogImportType = "Products"
                                    }
                                    else if ($scope.ImportSheetName == "Items") {
                                        $scope.viewLogImportType = "Items"
                                    }

                                    else if ($scope.ImportSheetName == "ProductItems") {
                                        $scope.viewLogImportType = "ProductItems"
                                    }


                                    angular.forEach(excelSheets, function (sheet) {
                                        if (continueLoop) {
                                            if (sheet.IMPORT_TYPE.trim().toUpperCase() == "CATEGORY" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.ImportType = "Categories";
                                                $scope.validateimportTypeSelection = "Category";
                                                $scope.importSheetType = "Categories";
                                                $scope.importSelectedvalue = "Categories";
                                                $scope.importSheetSelected = "Categories";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importExcelSheetDDSelectionValue = "Categories";
                                                $scope.validateimportTypeSelection = "Category";
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;


                                            }
                                            else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "PRODUCT" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.ImportType = "Families";
                                                $scope.validateimportTypeSelection = "Product";
                                                $scope.importSheetType = "Products";
                                                $scope.importSelectedvalue = "Products";
                                                $scope.importSheetSelected = "Families";
                                                $scope.importExcelSheetDDSelectionValue = "Families";
                                                $scope.validateimportTypeSelection = "Product";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;
                                            }
                                            else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "ITEM" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.ImportType = "Products";
                                                $scope.validateimportTypeSelection = "Item";
                                                $scope.importSheetType = "Items";
                                                $scope.importSelectedvalue = "Items";
                                                $scope.importSheetSelected = "Products";
                                                $scope.importExcelSheetDDSelectionValue = "Products";
                                                $scope.validateimportTypeSelection = "Item";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;
                                            }

                                            else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "TABLEDESIGNER" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.importSelectedvalue = "TableDesigner";
                                                $scope.ImportType = "TableDesigner";
                                                $scope.importSheetSelected = "TableDesigner";
                                                $scope.importSheetType = "TableDesigner";
                                                $scope.importExcelSheetDDSelectionValue = "TableDesigner";
                                                $scope.validateimportTypeSelection = "TableDesigner";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;
                                            }

                                        }
                                    });

                                    // ///Loop the excel sheet import types ///////////////////////////////


                                    if (continueLoop == false) {


                                        if ($scope.importSheetType == "TableDesigner") {
                                            $scope.ValidateDiv = false;
                                            $scope.showValidatediv = false;
                                            $scope.tbldesigner = true;
                                            $scope.showValidatetabledesignerdiv = true;

                                        }


                                        angular.forEach(importresult.split("~"), function (value) {
                                            if (value.includes('InsertRecords')) {
                                                var insertRecordCount = value.split(':')[1];
                                                $scope.importLog.InsertedRecords = insertRecordCount;

                                            }
                                            else if (value.includes('UpdateRecords')) {
                                                var updateRecordCount = value.split(':')[1];
                                                $scope.importLog.UpdatedRecords = updateRecordCount;
                                                // $scope.importAllResults.push({ "UpdatedRecords": $scope.importLog.UpdatedRecords });
                                            }
                                            else if (value.includes('DeletedRecords')) {
                                                var deletedRecordCount = value.split(':')[1];
                                                $scope.importLog.DeletedRecords = deletedRecordCount;
                                                //$scope.importAllResults.push({ "DeletedRecords": $scope.importLog.DeletedRecords });
                                            }
                                            else if (value.includes('TimeElapsed')) {
                                                var timeElapsed = value.split('-')[1];
                                                $scope.importLog.TimeTaken = timeElapsed;
                                                // $scope.importAllResults.push({ "TimeTaken": $scope.importLog.TimeTaken });
                                            }
                                            else if (value.includes('SkippedRecords')) {
                                                var skippedRecords = value.split(':')[1];
                                                $scope.importLog.SkippedRecords = skippedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }
                                            else if (value.includes('DetachedRecords')) {
                                                var DetachedRecords = value.split(':')[1];
                                                $scope.importLog.DetachedRecords = DetachedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }
                                        })
                                        $scope.importLog.CatalogName = $("#catalogDrpDownLisr").data("kendoDropDownList").text();
                                        // $scope.importAllResults.push({ "CatalogName": $scope.importLog.CatalogName });
                                        /// $scope.importAllResults.push({ "importSheetType": $scope.importSheetType });



                                        $scope.importAllResults.push({
                                            "InsertRecords": $scope.importLog.InsertedRecords, "UpdatedRecords": $scope.importLog.UpdatedRecords, "DeletedRecords": $scope.importLog.DeletedRecords
                                            , "DetachedRecords": $scope.importLog.DetachedRecords
                                        , "TimeTaken": $scope.importLog.TimeTaken, "SkippedRecords": $scope.importLog.SkippedRecords, "CatalogName": $scope.importLog.CatalogName,
                                            "importSheetType": $scope.viewLogImportType, "SheetName": $scope.ImportSheetName
                                        });
                                        $scope.importSuccessResultDatasource.read();
                                    }

                                    if (continueLoop == true) {
                                        $scope.ProcessName = "View Log";
                                        $("#import").removeClass("leftMenuactive");
                                        $("#impvalidation").removeClass("leftMenuactive");
                                        $("#logim").addClass("leftMenuactive");
                                        $scope.importErrorWindowdiv = false;
                                        $scope.importSuccessWindowdiv = true;
                                        $scope.importErrorFileWindowdiv = false;
                                        $scope.showValidatediv = false;
                                        $("#importErrorWindow").hide();

                                        //angular.forEach(importresult.split("~"), function (value) {
                                        //    if (value.includes('InsertRecords')) {
                                        //        var insertRecordCount = value.split(':')[1];
                                        //        $scope.importLog.InsertedRecords = insertRecordCount;
                                        //    }
                                        //    else if (value.includes('UpdateRecords')) {
                                        //        var updateRecordCount = value.split(':')[1];
                                        //        $scope.importLog.UpdatedRecords = updateRecordCount;
                                        //    }
                                        //    else if (value.includes('DeletedRecords')) {
                                        //        var deletedRecordCount = value.split(':')[1];
                                        //        $scope.importLog.DeletedRecords = deletedRecordCount;
                                        //    }
                                        //    else if (value.includes('TimeElapsed')) {
                                        //        var timeElapsed = value.split('-')[1];
                                        //        $scope.importLog.TimeTaken = timeElapsed;
                                        //    }
                                        //    else if (value.includes('SkippedRecords')) {
                                        //        var skippedRecords = value.split(':')[1];
                                        //        $scope.importLog.SkippedRecords = skippedRecords;
                                        //    }
                                        //});
                                        angular.forEach(importresult.split("~"), function (value) {
                                            if (value.includes('InsertRecords')) {
                                                var insertRecordCount = value.split(':')[1];
                                                $scope.importLog.InsertedRecords = insertRecordCount;

                                            }
                                            else if (value.includes('UpdateRecords')) {
                                                var updateRecordCount = value.split(':')[1];
                                                $scope.importLog.UpdatedRecords = updateRecordCount;
                                                // $scope.importAllResults.push({ "UpdatedRecords": $scope.importLog.UpdatedRecords });
                                            }
                                            else if (value.includes('DeletedRecords')) {
                                                var deletedRecordCount = value.split(':')[1];
                                                $scope.importLog.DeletedRecords = deletedRecordCount;
                                                //$scope.importAllResults.push({ "DeletedRecords": $scope.importLog.DeletedRecords });
                                            }
                                            else if (value.includes('TimeElapsed')) {
                                                var timeElapsed = value.split('-')[1];
                                                $scope.importLog.TimeTaken = timeElapsed;
                                                // $scope.importAllResults.push({ "TimeTaken": $scope.importLog.TimeTaken });
                                            }
                                            else if (value.includes('SkippedRecords')) {
                                                var skippedRecords = value.split(':')[1];
                                                $scope.importLog.SkippedRecords = skippedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }
                                            else if (value.includes('DetachedRecords')) {
                                                var DetachedRecords = value.split(':')[1];
                                                $scope.importLog.DetachedRecords = DetachedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }

                                        })
                                        $scope.importLog.CatalogName = $("#catalogDrpDownLisr").data("kendoDropDownList").text();
                                        // $scope.importAllResults.push({ "CatalogName": $scope.importLog.CatalogName });
                                        /// $scope.importAllResults.push({ "importSheetType": $scope.importSheetType });



                                        $scope.importAllResults.push({
                                            "InsertRecords": $scope.importLog.InsertedRecords, "UpdatedRecords": $scope.importLog.UpdatedRecords, "DeletedRecords": $scope.importLog.DeletedRecords
                                            , "DetachedRecords": $scope.importLog.DetachedRecords
                                        , "TimeTaken": $scope.importLog.TimeTaken, "SkippedRecords": $scope.importLog.SkippedRecords, "CatalogName": $scope.importLog.CatalogName,
                                            "importSheetType": $scope.viewLogImportType, "SheetName": $scope.ImportSheetName
                                        });

                                        // $scope.importLog.CatalogName = $("#catalogDrpDownLisr").data("kendoDropDownList").text();
                                        $scope.nextSheetProcess = true;
                                        $scope.validationPassed = true;
                                        $scope.ResultDisplay = true;
                                        $scope.AttributeMappingdiv = false;
                                        $("#importSuccessWindow").show();
                                        // $("#importSuccessWindow").hide();
                                        $("#bulkimportSuccessWindow").show();

                                        $scope.importSuccessResultDatasource.read();

                                    }
                                    //$scope.importResultDatasource.read();
                                    return;
                                    /*********Product input screen changes by Aswin *********///
                                }
                                else if (importresult.includes('ITEMEMPTY')) {
                                    $scope.ItemImport = true;
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + importresult.split("~", 1) + '.',
                                        type: "info"
                                    });
                                    $("#choosefileimp").removeClass("leftMenuactive");
                                    $("#sheetselectimp").removeClass("leftMenuactive");
                                    $("#Atrmappingimp").removeClass("leftMenuactive");
                                    $("#logim").removeClass("leftMenuactive");

                                    $("#createTemplate").removeClass("leftMenuactive");
                                    $("#impvalidation").removeClass("leftMenuactive");

                                    $("#imptamp").removeClass("leftMenuactive");
                                    $("#logim").addClass("leftMenuactive");
                                    $scope.AttributeMappingdiv = false;
                                    $scope.ResultDisplay = true;

                                    $scope.showValidatediv = false;
                                    //$("#impvalidation").removeClass("leftMenuactive");
                                    //$("#logim").addClass("leftMenuactive");
                                    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                                    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                                    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                                    //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                                    //$('#logim')[0].className = 'k-item';
                                    //$('#imptamp')[0].className = 'k-item k-state-disabled';
                                    $scope.importErrorWindowdiv = true;
                                    $("#importErrorWindow").show();
                                    $("#bulkimportErrorWindow").show();
                                    $scope.importSuccessWindowdiv = false;

                                    $("#importSuccessWindow").hide();
                                    $("#bulkimportSuccessWindow").hide();
                                    $('#myModal').hide();

                                    $scope.importResultDatasource.read();
                                    return;
                                }
                                else if (importresult.includes('ITEM#') && importresult.includes('Import Failed')) {
                                    $scope.ItemImport = true;
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + importresult.split("~", 1) + '.',
                                        type: "info"
                                    });
                                    $("#choosefileimp").removeClass("leftMenuactive");
                                    $("#sheetselectimp").removeClass("leftMenuactive");
                                    $("#Atrmappingimp").removeClass("leftMenuactive");
                                    $("#logim").removeClass("leftMenuactive");

                                    $("#createTemplate").removeClass("leftMenuactive");
                                    $("#impvalidation").removeClass("leftMenuactive");

                                    $("#imptamp").removeClass("leftMenuactive");
                                    $("#logim").addClass("leftMenuactive");
                                    $scope.AttributeMappingdiv = false;
                                    $scope.ResultDisplay = true;

                                    $scope.showValidatediv = false;
                                    //$("#impvalidation").removeClass("leftMenuactive");
                                    //$("#logim").addClass("leftMenuactive");
                                    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                                    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                                    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                                    //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                                    //$('#logim')[0].className = 'k-item';
                                    //$('#imptamp')[0].className = 'k-item k-state-disabled';
                                    $scope.importErrorWindowdiv = true;
                                    $("#importErrorWindow").show();
                                    $("#bulkimportErrorWindow").show();
                                    $scope.importSuccessWindowdiv = false;

                                    $("#importSuccessWindow").hide();
                                    $("#bulkimportSuccessWindow").hide();
                                    $('#myModal').hide();


                                    $scope.importResultDatasource.read();
                                    return;
                                }
                                else if (importresult.includes('MappingAttribute') && importresult.includes('Import Failed')) {
                                    $scope.ItemImport = true;
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + importresult.split("~", 1) + '.',
                                        type: "info"
                                    });
                                    $("#choosefileimp").removeClass("leftMenuactive");
                                    $("#sheetselectimp").removeClass("leftMenuactive");
                                    $("#Atrmappingimp").removeClass("leftMenuactive");
                                    $("#logim").removeClass("leftMenuactive");

                                    $("#createTemplate").removeClass("leftMenuactive");
                                    $("#impvalidation").removeClass("leftMenuactive");

                                    $("#imptamp").removeClass("leftMenuactive");
                                    $("#logim").addClass("leftMenuactive");
                                    $scope.AttributeMappingdiv = false;
                                    $scope.ResultDisplay = true;

                                    $scope.showValidatediv = false;
                                    //$("#impvalidation").removeClass("leftMenuactive");
                                    //$("#logim").addClass("leftMenuactive");
                                    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                                    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                                    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                                    //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                                    //$('#logim')[0].className = 'k-item';
                                    //$('#imptamp')[0].className = 'k-item k-state-disabled';

                                    $scope.importMappingAtttributeErrorWindow = true;
                                    $("#importErrorWindow").show();
                                    $("#bulkimportErrorWindow").show();
                                    $scope.importSuccessWindowdiv = false;
                                    $scope.importErrorWindowdiv = false;
                                    $("#importSuccessWindow").hide();
                                    $("#bulkimportSuccessWindow").hide();
                                    $('#myModal').hide();
                                    $scope.showErrorLog = false;
                                    // $scope.mappingAttribute = importresult.split('~')[2];
                                    $scope.fun_ImportMissingMappingAttributeResult();
                                    $("#import").removeClass("leftMenuactive");
                                    return;
                                }
                                if (importresult.split("~", 1) == "Import Failed") {

                                    if ($scope.getImportType == "Products") {
                                        $scope.getImportType = "Items"
                                    }

                                    if ($scope.getImportType == "Families") {
                                        $scope.getImportType = "Products"
                                    }

                                    if ($scope.getImportType == "Familyproducts") {
                                        $scope.getImportType = "ProductItems"
                                    }

                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: $scope.getImportType + ' ' + importresult.split("~", 1) + '.',
                                        type: "info"
                                    });
                                    $("#choosefileimp").removeClass("leftMenuactive");
                                    $("#sheetselectimp").removeClass("leftMenuactive");
                                    $("#Atrmappingimp").removeClass("leftMenuactive");
                                    $("#logim").removeClass("leftMenuactive");

                                    $("#createTemplate").removeClass("leftMenuactive");
                                    $("#impvalidation").removeClass("leftMenuactive");

                                    $("#imptamp").removeClass("leftMenuactive");
                                    $("#logim").addClass("leftMenuactive");
                                    $scope.AttributeMappingdiv = false;
                                    $scope.ResultDisplay = true;

                                    $scope.showValidatediv = false;
                                    //$("#impvalidation").removeClass("leftMenuactive");
                                    //$("#logim").addClass("leftMenuactive");
                                    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                                    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                                    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                                    //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                                    //$('#logim')[0].className = 'k-item';
                                    //$('#imptamp')[0].className = 'k-item k-state-disabled';
                                    $scope.importErrorWindowdiv = true;
                                    $("#importErrorWindow").show();
                                    $("#bulkimportErrorWindow").show();
                                    $scope.importSuccessWindowdiv = false;

                                    $("#importSuccessWindow").hide();
                                    $("#bulkimportSuccessWindow").hide();
                                    $('#myModal').hide();

                                    $scope.importResultDatasource.read();

                                    $("#import").removeClass("leftMenuactive");
                                } else if (importresult.split("~", 1) == "SKU Exceed") {

                                    $scope.ResultDisplay = true;
                                    $scope.AttributeMappingdiv = false;
                                    $scope.importErrorWindowdiv = false;
                                    $scope.showValidatediv = false;
                                    $("#importErrorWindow").hide();
                                    $scope.importSuccessWindowdiv = false;
                                    $("#importSuccessWindow").hide();
                                    $("#importSuccessWindow").hide();
                                    $("#bulkimportSuccessWindow").hide();
                                    $scope.importResultDatasource.read();

                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'You have exceeded the maximum No. of SKUs as per your plan.',
                                        type: "error"
                                    });
                                    $("#choosefileimp").removeClass("leftMenuactive");
                                    $("#sheetselectimp").removeClass("leftMenuactive");
                                    $("#Atrmappingimp").removeClass("leftMenuactive");
                                    $("#logim").removeClass("leftMenuactive");

                                    $("#createTemplate").removeClass("leftMenuactive");
                                    $("#impvalidation").removeClass("leftMenuactive");

                                    $("#imptamp").removeClass("leftMenuactive");
                                    $("#logim").addClass("leftMenuactive");


                                    //$("#impvalidation").removeClass("leftMenuactive");
                                    //$("#logim").addClass("leftMenuactive");
                                    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                                    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                                    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                                    //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                                    //$('#logim')[0].className = 'k-item';
                                    //$('#imptamp')[0].className = 'k-item k-state-disabled';
                                    $('#myModal').hide();
                                } else {

                                    /*********Product input screen changes by Aswin *********///

                                    //$.msgBox({
                                    //    title: $localStorage.ProdcutTitle,
                                    //    content: 'Import successful',
                                    //    //content: ''+importresult.split("~", 1)+'',
                                    //    type: "info"
                                    //});

                                    $scope.ImportSheetName = $scope.validationimportSelectedvalue;

                                    $scope.importStatusFlag = "1";
                                    $scope.UpdateImportFlagFunctionCall();


                                    // ///Loop the excel sheet import types ///////////////////////////////

                                    var excelSheets = $scope.importSheetNames;

                                    var continueLoop = true;

                                    if ($scope.importSelectionSheet.length == 0) {
                                        $scope.importSelectionSheet.push($scope.ImportSheetName);
                                    }
                                    //else {
                                    //    $scope.importSelectionSheet.push($scope.ImportSheetName);
                                    //}

                                    $scope.viewLogImportType = $scope.ImportType;

                                    //Import success result Import type name changes

                                    //if ($scope.viewLogImportType == "Products") {
                                    //    $scope.viewLogImportType = "Items"
                                    //}

                                    //if ($scope.viewLogImportType == "Families") {
                                    //    $scope.viewLogImportType = "Products"
                                    //}

                                    //if ($scope.viewLogImportType == "Familyproducts") {
                                    //    $scope.viewLogImportType = "ProductItems"
                                    //}
                                    if ($scope.ImportSheetName == "Products") {
                                        $scope.viewLogImportType = "Products"
                                    }
                                    else if ($scope.ImportSheetName == "Items") {
                                        $scope.viewLogImportType = "Items"
                                    }

                                    else if ($scope.ImportSheetName == "ProductItems") {
                                        $scope.viewLogImportType = "ProductItems"
                                    }

                                    angular.forEach(excelSheets, function (sheet) {
                                        if (continueLoop) {
                                            if (sheet.IMPORT_TYPE.trim().toUpperCase() == "CATEGORY" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.ImportType = "Categories";
                                                $scope.validateimportTypeSelection = "Category";
                                                $scope.importSheetType = "Categories";
                                                $scope.importSelectedvalue = "Categories";
                                                $scope.importSheetSelected = "Categories";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importExcelSheetDDSelectionValue = "Categories";
                                                $scope.validateimportTypeSelection = "Category";
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;


                                            }
                                            else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "PRODUCT" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.ImportType = "Families";
                                                $scope.validateimportTypeSelection = "Product";
                                                $scope.importSheetType = "Products";
                                                $scope.importSelectedvalue = "Products";
                                                $scope.importSheetSelected = "Families";
                                                $scope.importExcelSheetDDSelectionValue = "Families";
                                                $scope.validateimportTypeSelection = "Product";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;
                                            }
                                            else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "ITEM" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.ImportType = "Products";
                                                $scope.validateimportTypeSelection = "Item";
                                                $scope.importSheetType = "Items";
                                                $scope.importSelectedvalue = "Items";
                                                $scope.importSheetSelected = "Products";
                                                $scope.importExcelSheetDDSelectionValue = "Products";
                                                $scope.validateimportTypeSelection = "Item";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;
                                            }

                                            else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "TABLEDESIGNER" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.importSelectedvalue = "TableDesigner";
                                                $scope.ImportType = "TableDesigner";
                                                $scope.importSheetSelected = "TableDesigner";
                                                $scope.importSheetType = "TableDesigner";
                                                $scope.importExcelSheetDDSelectionValue = "TableDesigner";
                                                $scope.validateimportTypeSelection = "TableDesigner";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;
                                            }

                                        }
                                    });


                                    // ///Loop the excel sheet import types ///////////////////////////////


                                    if (continueLoop == false) {
                                        //    angular.forEach(importresult.split("~"), function (value) {
                                        //        if (value.includes('InsertRecords')) {
                                        //            var insertRecordCount = value.split(':')[1];
                                        //            $scope.importAllResults.push({"InsertRecords":$scope.importLog.InsertedRecords = insertRecordCount});
                                        //        }
                                        //        else if (value.includes('UpdateRecords')) {
                                        //            var updateRecordCount = value.split(':')[1];
                                        //            $scope.importLog.UpdatedRecords = updateRecordCount;
                                        //        }
                                        //        else if (value.includes('DeletedRecords')) {
                                        //            var deletedRecordCount = value.split(':')[1];
                                        //            $scope.importLog.DeletedRecords = deletedRecordCount;
                                        //        }
                                        //        else if (value.includes('TimeElapsed')) {
                                        //            var timeElapsed = value.split('-')[1];
                                        //            $scope.importLog.TimeTaken = timeElapsed;
                                        //        }
                                        //        else if (value.includes('SkippedRecords')) {
                                        //            var skippedRecords = value.split(':')[1];
                                        //            $scope.importLog.SkippedRecords = skippedRecords;
                                        //        }
                                        //    })

                                        if ($scope.importSheetType == "TableDesigner") {
                                            $scope.ValidateDiv = false;
                                            $scope.showValidatediv = false;
                                            $scope.tbldesigner = true;
                                            $scope.showValidatetabledesignerdiv = true;

                                        }


                                        angular.forEach(importresult.split("~"), function (value) {
                                            if (value.includes('InsertRecords')) {
                                                var insertRecordCount = value.split(':')[1];
                                                $scope.importLog.InsertedRecords = insertRecordCount;

                                            }
                                            else if (value.includes('UpdateRecords')) {
                                                var updateRecordCount = value.split(':')[1];
                                                $scope.importLog.UpdatedRecords = updateRecordCount;
                                                // $scope.importAllResults.push({ "UpdatedRecords": $scope.importLog.UpdatedRecords });
                                            }
                                            else if (value.includes('DeletedRecords')) {
                                                var deletedRecordCount = value.split(':')[1];
                                                $scope.importLog.DeletedRecords = deletedRecordCount;
                                                //$scope.importAllResults.push({ "DeletedRecords": $scope.importLog.DeletedRecords });
                                            }
                                            else if (value.includes('TimeElapsed')) {
                                                var timeElapsed = value.split('-')[1];
                                                $scope.importLog.TimeTaken = timeElapsed;
                                                // $scope.importAllResults.push({ "TimeTaken": $scope.importLog.TimeTaken });
                                            }
                                            else if (value.includes('SkippedRecords')) {
                                                var skippedRecords = value.split(':')[1];
                                                $scope.importLog.SkippedRecords = skippedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }
                                            else if (value.includes('DetachedRecords')) {
                                                var DetachedRecords = value.split(':')[1];
                                                $scope.importLog.DetachedRecords = DetachedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }

                                        })
                                        $scope.importLog.CatalogName = $("#catalogDrpDownLisr").data("kendoDropDownList").text();
                                        // $scope.importAllResults.push({ "CatalogName": $scope.importLog.CatalogName });
                                        /// $scope.importAllResults.push({ "importSheetType": $scope.importSheetType });



                                        $scope.importAllResults.push({
                                            "InsertRecords": $scope.importLog.InsertedRecords, "UpdatedRecords": $scope.importLog.UpdatedRecords, "DeletedRecords": $scope.importLog.DeletedRecords
                                        , "DetachedRecords": $scope.importLog.DetachedRecords, "TimeTaken": $scope.importLog.TimeTaken, "SkippedRecords": $scope.importLog.SkippedRecords, "CatalogName": $scope.importLog.CatalogName,
                                            "importSheetType": $scope.viewLogImportType, "SheetName": $scope.ImportSheetName
                                        });
                                        $scope.importSuccessResultDatasource.read();
                                    }
                                    else if (continueLoop == true) {

                                        $scope.ProcessName = "View Log";
                                        $("#import").removeClass("leftMenuactive");
                                        $("#impvalidation").removeClass("leftMenuactive");
                                        $("#logim").addClass("leftMenuactive");
                                        //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                                        //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                                        //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                                        //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                                        //$('#logim')[0].className = 'k-item';
                                        //$('#imptamp')[0].className = 'k-item k-state-disabled';
                                        $scope.importErrorWindowdiv = false;
                                        $scope.importSuccessWindowdiv = true;
                                        $scope.importErrorFileWindowdiv = false;
                                        $scope.showValidatediv = false;
                                        $("#importErrorWindow").hide();

                                        //if (importresult.split("~", 3) >=2) {
                                        //    $scope.nextSheetProcess = true;
                                        //}
                                        // angular.forEach($scope.importAllResults.split("~"), function (value) {
                                        angular.forEach(importresult.split("~"), function (value) {
                                            if (value.includes('InsertRecords')) {
                                                var insertRecordCount = value.split(':')[1];
                                                $scope.importLog.InsertedRecords = insertRecordCount;
                                                // $scope.importAllResults.push({ "InsertRecords": $scope.importLog.InsertedRecords });
                                            }
                                            else if (value.includes('UpdateRecords')) {
                                                var updateRecordCount = value.split(':')[1];
                                                $scope.importLog.UpdatedRecords = updateRecordCount;
                                                // $scope.importAllResults.push({ "UpdatedRecords": $scope.importLog.UpdatedRecords });
                                            }
                                            else if (value.includes('DeletedRecords')) {
                                                var deletedRecordCount = value.split(':')[1];
                                                $scope.importLog.DeletedRecords = deletedRecordCount;
                                                // $scope.importAllResults.push({ "DeletedRecords": $scope.importLog.DeletedRecords });
                                            }
                                            else if (value.includes('TimeElapsed')) {
                                                var timeElapsed = value.split('-')[1];
                                                $scope.importLog.TimeTaken = timeElapsed;
                                                /// $scope.importAllResults.push({ "TimeTaken": $scope.importLog.TimeTaken });
                                            }
                                            else if (value.includes('SkippedRecords')) {
                                                var skippedRecords = value.split(':')[1];
                                                $scope.importLog.SkippedRecords = skippedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }
                                            else if (value.includes('DetachedRecords')) {
                                                var DetachedRecords = value.split(':')[1];
                                                $scope.importLog.DetachedRecords = DetachedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }
                                        })
                                        $scope.importLog.CatalogName = $("#catalogDrpDownLisr").data("kendoDropDownList").text();
                                        // $scope.importAllResults.push({ "CatalogName": $scope.importLog.CatalogName });
                                        // $scope.importAllResults.push({ "importSheetType": $scope.importSheetType });
                                        $scope.importAllResults.push({
                                            "InsertRecords": $scope.importLog.InsertedRecords, "UpdatedRecords": $scope.importLog.UpdatedRecords, "DeletedRecords": $scope.importLog.DeletedRecords
                                       , "DetachedRecords": $scope.importLog.DetachedRecords, "TimeTaken": $scope.importLog.TimeTaken, "SkippedRecords": $scope.importLog.SkippedRecords, "CatalogName": $scope.importLog.CatalogName,
                                            "importSheetType": $scope.viewLogImportType, "SheetName": $scope.ImportSheetName
                                        });
                                        $scope.nextSheetProcess = true;
                                        $scope.validationPassed = true;
                                        $scope.ResultDisplay = true;
                                        $scope.AttributeMappingdiv = false;
                                        $("#importSuccessWindow").show();
                                        // $("#importSuccessWindow").hide();
                                        $("#bulkimportSuccessWindow").show();

                                        $scope.importSuccessResultDatasource.read();
                                        $('.imageloader').show();



                                    }

                                }
                                /*********Product input screen changes by Aswin *********///
                                //$http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
                            }).error(function (error) {
                                // $scope.loadingimg.center().close();  
                                if ($scope.getImportType == "Products") {
                                    $scope.getImportType = "Items"
                                }

                                if ($scope.getImportType == "Families") {
                                    $scope.getImportType = "Products"
                                }

                                if ($scope.getImportType == "Familyproducts") {
                                    $scope.getImportType = "ProductItems"
                                }

                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: $scope.getImportType + ' Import failed, please try again.',
                                    type: "error"
                                });
                                // $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
                                options.error(error);
                            });
                        });
                    }

                    else {
                        $('.imageloader').show();
                        return false;
                    }
                }

            });
        }


        //  }
        //  $scope.loadingimg.refresh({ url: "../views/app/partials/ImgLoading.html" });
        // $scope.loadingimg.center().open();
        // $http.post("/Import/finishImport?SheetName=" + $scope.importExcelSheetDDSelectionValue + "&allowDuplicate=" + $scope.allowDuplication + "&excelPath=" + $scope.excelPath)
        // .success(function (importresult) {

        //     $scope.loadingimg.center().close();
        //     alert(importresult.split("~", 1));
        //     $scope.importSessionID = importresult.split("~", 2);   // kendo data source ;
        //     if (importresult.split("~", 1) == "Import Failed") {
        //         $("#importErrorWindow").show();
        //         $("#importSuccessWindow").hide();
        //         $scope.importResultDatasource.read();

        //         //alert("issue");
        //     }
        //     else {
        //         //alert("success");
        //         $("#importErrorWindow").hide();
        //         $("#importSuccessWindow").show();
        //         $scope.importSuccessResultDatasource.read();
        //     }

        //     $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        // })
        //.error(function () {
        //    $scope.loadingimg.center().close();
        //    alert("Import Failed. Please Try Again.");
        //    $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);


        //});
    };
    $scope.importSuccessResultGridOptions = {
        dataSource: $scope.importSuccessResultDatasource,
        autoBind: false, resizable: true,
        sortable: true,
        pageable: false,
        serverPaging: false,
        serverSorting: false,
        scrollable: true,
        columns: [
            { field: "CATALOG_NAME", title: "CATALOG NAME", width: "200px" },
            { field: "CATEGORY_DETAILS", title: "CATEGORY DETAILS", width: "200px" },
            { field: "FAMILY_DETAILS", title: "FAMILY DETAILS", width: "200px" },
            { field: "SUBFAMILY_DETAILS", title: "SUBFAMILY DETAILS", width: "200px" },
            { field: "PRODUCT_DETAILS", title: "PRODUCT DETAILS", width: "200px" },
            { field: "STATUS", title: "STATUS" }
        ]
    };
    $scope.importSuccessResultGridOptionsSub = {
        dataSource: $scope.importSuccessResultDatasource,
        autoBind: false, resizable: true,
        sortable: true,
        pageable: false,
        serverPaging: false,
        serverSorting: false,
        scrollable: true,
        columns: [
            { field: "CATALOG_NAME", title: "CATALOG NAME", width: "200px" },
            { field: "CATEGORY_DETAILS", title: "CATEGORY DETAILS", width: "200px" },
            { field: "FAMILY_DETAILS", title: "FAMILY DETAILS", width: "200px" },
            { field: "SUBFAMILY_DETAILS", title: "SUBFAMILY DETAILS", width: "200px" },
            { field: "PRODUCT_DETAILS", title: "PRODUCT DETAILS", width: "200px" },
            { field: "SUB_PRODUCT_DETAILS", title: "SUBPRODUCT DETAILS", width: "200px" },
            { field: "STATUS", title: "STATUS" }
        ]
    };
    $scope.importSuccessResultGridOptionsFam = {
        dataSource: $scope.importSuccessResultDatasource,
        autoBind: false, resizable: true,
        sortable: true,
        pageable: false,
        serverPaging: false,
        serverSorting: false,
        scrollable: true,
        columns: [
            { field: "CATALOG_NAME", title: "CATALOG NAME", width: "200px" },
            { field: "CATEGORY_DETAILS", title: "CATEGORY DETAILS", width: "200px" },
            { field: "FAMILY_DETAILS", title: "FAMILY DETAILS", width: "200px" },
            { field: "STATUS", title: "STATUS" }
        ]
    };
    $scope.validatefile = function () {

        dataFactory.basicimportExcelSheetSelection($scope.allowDuplication, $scope.basicImportExcelSheetDDSelectionvalue, $scope.excelPath, $scope.basicprodImportData).success(function (response) {
            $scope.successREsultBasic = jQuery.parseJSON(response.Data);
            $scope.tblImportBasic.reload();
            $scope.tblImportBasic.$params.page = 1;
            $("#basicImportSuccessWindow").show();
            // $("#basicImportSuccessWindowTemplate").show();
        }).error(function (error) {
            options.error(error);
        });;


    };
    $scope.backexpression = function () {
        if ($scope.passvalidationNext == false) {
            //$scope.ProcessName = "Attribute Mapping";
            $scope.ProcessName = "Map Attributes";
            $scope.passvalidationNext = true;
            ("#imptamp").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#imptamp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
        }
        else if ($scope.passvalidation == true) {

            $scope.templateDetails = true;
            $scope.AttributeMappingdiv = false;
            $scope.showValidatediv = false;
            //  $scope.ProcessName = "Attribute Mapping";
            $("#choosefileimp").addClass("leftMenuactive");
            $("#Atrmappingimp").removeClass("leftMenuactive");
        }
    }
    $("#validationResultForAll").kendoGrid({
        autoBind: false,
        dataSource: $scope.validatetResultDatasource
    });

    /*********Product input screen changes by Aswin *********///
    $scope.Validateimport = function () {
        if ($scope.templateId == undefined) {
            $scope.templateId = 0;
            blockUI.stop();
        };
        if ($scope.IsSelectAll == true) {
            selectAll = 1;
        }
        if ($scope.IsSelectAll == false) {
            selectAll = 0;
        }
        if ($scope.selectedTemplate != "" && $scope.selectedTemplate != undefined && $scope.templateId == "0") {
            $scope.templateId = $scope.selectedTemplate;
        }

        //if ($scope.validationimportSelectedvalue == "Products") {
        //$scope.validationimportSelectedvalue = "Families"
        //}
        //if ($scope.validationimportSelectedvalue == "Items") {
        //    $scope.validationimportSelectedvalue = "Products"
        //}



        if ($scope.ImportType == "Products" && $scope.validationimportSelectedvalue == "Items")
        {
            $scope.ImportType = "Products"
        }
        else
        {
            if ($scope.ImportType == "Items") {
                $scope.ImportType = "Products"
            }
            if ($scope.ImportType == "Products") {
                $scope.ImportType = "Families"
            }
        }

        dataFactory.GetImportSpecs($scope.validationimportSelectedvalue, $scope.excelPath, $scope.templateId, $scope.ImportType, selectAll).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.prodImportData = obj.Data;


            // START
            // When storage value in ITEM#

            for (var i = 0 ; i < $scope.prodImportData.length ; i++) {

                var ItemValue = $scope.prodImportData[i]["ExcelColumn"];

                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogItemNumber.toUpperCase()) {
                    $scope.prodImportData[i]["ExcelColumn"] = $localStorage.CatalogItemNumber;
                    $scope.prodImportData[i]["CatalogField"] = $localStorage.CatalogItemNumber;
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";

                }
                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogSubItemNumber.toUpperCase()) {
                    $scope.prodImportData[i]["ExcelColumn"] = $localStorage.CatalogSubItemNumber;
                    $scope.prodImportData[i]["CatalogField"] = $localStorage.CatalogSubItemNumber;
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";

                }
                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == "ITEM#") {

                    //$scope.prodImportData[i]["CatalogField"] = "ITEM#";
                    //$scope.prodImportData[i]["FieldType"] = "";
                    //$scope.prodImportData[i]["IsSystemField"] = "";
                    //$scope.prodImportData[i]["SelectedToImport"] = "False";
                    //$scope.prodImportData[i]["AttrMapping"] = "";
                    //$scope.prodImportData[i]["ExcelColumn"] = "ITEM#";


                    $scope.prodImportData[i]["ExcelColumn"] = "ITEM#";
                    $scope.prodImportData[i]["CatalogField"] = "ITEM#";
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";



                }
                if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == "SUBITEM#") {

                    //$scope.prodImportData[i]["CatalogField"] = "SUBITEM#";
                    //$scope.prodImportData[i]["FieldType"] = "";
                    //$scope.prodImportData[i]["IsSystemField"] = "";
                    //$scope.prodImportData[i]["SelectedToImport"] = "False";
                    //$scope.prodImportData[i]["AttrMapping"] = "";
                    //$scope.prodImportData[i]["ExcelColumn"] = "SUBITEM#";

                    $scope.prodImportData[i]["ExcelColumn"] = "SUBITEM#"
                    $scope.prodImportData[i]["CatalogField"] = "SUBITEM#"
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";
                }


            }


            $scope.StartValidateimport();


        }).error(function (error) {
            options.error(error);
            blockUI.stop();
        });
    }

    /*********Product input screen changes by Aswin *********///

    $scope.StartValidateimport = function () {
        $scope.importSuccessWindowdiv = false;
        $scope.importErrorWindowdiv = false;
        $scope.validationStatus = "0";



        $.msgBox({
            title: $localStorage.ProdcutTitle,
            //content: "Validation may take some time to complete",
            content: "Proceed to validate import file.",
            type: "confirm",
            buttons: [{ value: "Ok" },
            { value: "Cancel" }],
            success: function (result) {
                if (result === "Ok") {
                    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                    //$('#impvalidation')[0].className = 'k-item';
                    //$('#logim')[0].className = 'k-item k-state-disabled';
                    //$('#imptamp')[0].className = 'k-item k-state-disabled';

                    //Dynamicaly Changed ctalog)item values _ START.

                    for (var i = 0 ; i < $scope.prodImportData.length ; i++) {
                        if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogItemNumber.toUpperCase()) {
                            $scope.prodImportData[i]["ExcelColumn"] = "ITEM#";
                            $scope.prodImportData[i]["CatalogField"] = "ITEM#";
                            $scope.prodImportData[i]["FieldType"] = "0";
                            $scope.prodImportData[i]["IsSystemField"] = "True";
                            $scope.prodImportData[i]["SelectedToImport"] = "True";
                        }
                        if ($scope.prodImportData[i]["ExcelColumn"].toUpperCase() == $localStorage.CatalogSubItemNumber.toUpperCase()) {
                            $scope.prodImportData[i]["ExcelColumn"] = "SUBITEM#"
                            $scope.prodImportData[i]["CatalogField"] = "SUBITEM#"
                            $scope.prodImportData[i]["FieldType"] = "0";
                            $scope.prodImportData[i]["IsSystemField"] = "True";
                            $scope.prodImportData[i]["SelectedToImport"] = "True";

                        }
                    }


                    //END


                    $scope.validationImportSheetName = $scope.validationimportSelectedvalue;


                    //if ($scope.validationimportSelectedvalue == "Products") {
                    //    $scope.validationimportSelectedvalue = "Families"
                    //}
                    //if ($scope.validationimportSelectedvalue == "Items") {
                    //    $scope.validationimportSelectedvalue = "Products"
                    //}

                    if ($scope.ImportType == "Products" && $scope.validationimportSelectedvalue == "Items") {
                        $scope.ImportType = "Products"
                    }
                    else {
                        if ($scope.ImportType == "Items") {
                            $scope.ImportType = "Products"
                        }
                        if ($scope.ImportType == "Products") {
                            $scope.ImportType = "Families"
                        }
                    }

                    dataFactory.Validateimport($scope.validationimportSelectedvalue, $scope.allowDuplication, $scope.excelPath, $scope.ImportType, $scope.ImportcatalogIdAttr, $scope.ImportFormat, $scope.validated, $scope.templateId, $scope.prodImportData).success(function (importresult) {
                        $scope.importSessionID = importresult.split("~", importresult.split("~").length);   // kendo data source ;
                        var validation = "";
                        $scope.subcatalogItemerror = false;
                        $scope.missingColumnError = false;
                        $scope.PicklistValidationerror = false;
                        $scope.duplicateItemError = false;
                        $scope.dataTypError = false;
                        ///****************** Product input screen changes ******************//////////////
                        $scope.duplicateMappingAttributes = false;
                        ///****************** Product input screen changes ******************//////////////
                        $scope.pdfXpressValidation = false;


                        var validation = "";
                        angular.forEach(importresult.split('~'), function (data) {
                            if (data == "Import Failed") {

                                $scope.ResultDisplay = false;
                                return;
                            }

                            if (data.includes("pdf")) {

                                $scope.ResultDisplay = true;
                                $scope.pdfXpressValidation = true;
                            }


                            if (data.toString().toUpperCase() == "MISSING_COLUMNS" || data.toString().toUpperCase() == "MISSINGCOLUMNS") {
                                //  $scope.ProcessName = "View Log";
                                $scope.ResultDisplay = true;
                                // $scope.missingColumnInValid = true;
                                //$scope.showValidatediv = false;
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");
                                $scope.missingColumnError = true;
                                //  $scope.MissingColumns.read();
                            }
                            else if (data == "NEW_CATALOG_ITEM_NO") {
                                //  $scope.ProcessName = "View Log";
                                //$scope.importErrorFileWindowdiv = true;

                                $scope.ResultDisplay = true;
                                //$scope.showValidatediv = false;
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");
                                $scope.subcatalogItemerror = true;
                                // $scope.validatetResultDatasource.read();
                            }
                            else if (data == "NEW_FAMILY_NAME") {
                                $scope.ResultDisplay = true;
                                $scope.subcatalogItemerror = true;

                            }
                            else if (data == "PICKLIST_VALUE" || data == "PRODUCT_KEY" || data == "DATATYPE_VALIDATION" || data == "FAMILY_KEY") {
                                //$scope.ProcessName = "View Log";
                                //$scope.importpicklistdiv = true;
                                $scope.ResultDisplay = true;
                                $scope.showValidatediv = false;
                                //$scope.ProcessName = "View Log";
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");
                                if (data == "PICKLIST_VALUE" || data == "PRODUCT_KEY") {
                                    $scope.PicklistValidationerror = true;
                                }
                                if (data == "PRODUCT_KEY") {
                                    $scope.PartsKeyValidationerror = true;
                                }
                                if (data == "DATATYPE_VALIDATION") {
                                    $scope.dataTypError = true;
                                }
                                //  $scope.getFinishImportFailedpicklistResults.read();
                            }
                            else if (data == "DUPLICATE_ITEM_NO" || data == "DUPLICATE_RECORDS") {
                                //$scope.ProcessName = "View Log";
                                //$scope.importpicklistdiv = true;
                                $scope.ResultDisplay = true;
                                $scope.showValidatediv = false;
                                //$scope.ProcessName = "View Log";
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");
                                $scope.duplicateItemError = true;
                                //  $scope.getFinishImportFailedpicklistResults.read();
                            }
                            else if (data == "DUPLICATE_FAMILY_NAME" || data == "DUPLICATE_RECORDS") {
                                //$scope.ProcessName = "View Log";
                                //$scope.importpicklistdiv = true;
                                $scope.ResultDisplay = true;
                                $scope.showValidatediv = false;
                                //$scope.ProcessName = "View Log";
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");
                                $scope.duplicateItemError = true;
                                //  $scope.getFinishImportFailedpicklistResults.read();
                            }
                                ///****************** Product input screen changes ******************//////////////
                            else if (data == "DUPLICATE_MAPPING_ATTRIBUTES") {
                                //$scope.ProcessName = "View Log";
                                //$scope.importpicklistdiv = true;
                                $scope.ResultDisplay = true;
                                $scope.showValidatediv = false;
                                //$scope.ProcessName = "View Log";
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");
                                $scope.duplicateMappingAttributes = true;
                                //  $scope.getFinishImportFailedpicklistResults.read();
                            }
                                ///****************** Product input screen changes ******************//////////////
                            else if (data == "DUPLICATE_CATEGORY" || data == "DUPLICATE_RECORDS") {
                                //$scope.ProcessName = "View Log";
                                //$scope.importpicklistdiv = true;
                                $scope.ResultDisplay = true;
                                $scope.showValidatediv = false;
                                //$scope.ProcessName = "View Log";
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");
                                $scope.duplicateItemError = true;
                                //  $scope.getFinishImportFailedpicklistResults.read();
                            }
                            else if (data.toUpperCase().includes("PASSED") || data.toUpperCase().includes('SUCCESS')) {
                                //$scope.passvalidation = true;
                                //$scope.passvalidationimport = true;
                                //$scope.passvalidation = false;
                                validation = "Passed";

                            }
                        });

                        if ($scope.ResultDisplay == false && validation == "Passed") {

                            // IN this variable get the product count from importing sheet
                            var P_Count_Value = $scope.importSessionID[2];
                            //alert($scope.importSessionID[2]);
                            //alert($scope.importSessionID[3]);
                            /*********Product input screen changes by Aswin *********///

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: $scope.validateimportTypeSelection + ' validation completed successfully.',
                                type: "success"
                            });

                            $scope.validationStatus = "1";
                            $scope.updateValidationImportTypeFlag();

                            var excelSheets = $scope.validationSheetname;

                            var continueLoop = true;

                            if ($scope.validationSelectionSheet.length == 0) {
                                $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                            }
                            //else
                            //{
                            //    $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                            //}
                            angular.forEach(excelSheets, function (sheet) {
                                if (continueLoop) {
                                    if (sheet.IMPORT_TYPE.trim().toUpperCase() == "CATEGORY" && (!$scope.validationSelectionSheet.includes(sheet.SHEET_NAME))) {
                                        $scope.ImportType = "Categories";
                                        $scope.validateimportTypeSelection = "Category";
                                        $scope.importSheetType = "Categories";
                                        $scope.importSelectedvalue = "Categories";
                                        $scope.importSheetSelected = "Categories";
                                        $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                        $scope.importExcelSheetDDSelectionValue = sheet.SHEET_NAME;
                                        $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                                        continueLoop = false;


                                    }
                                    else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "PRODUCT" && (!$scope.validationSelectionSheet.includes(sheet.SHEET_NAME))) {
                                        $scope.ImportType = "Families";
                                        $scope.validateimportTypeSelection = "Product";
                                        $scope.importSheetType = "Products";
                                        $scope.importSelectedvalue = "Products";
                                        $scope.importSheetSelected = "Families";
                                        $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                        $scope.importExcelSheetDDSelectionValue = sheet.SHEET_NAME;
                                        $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                                        continueLoop = false;
                                    }
                                    else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "ITEM" && (!$scope.validationSelectionSheet.includes(sheet.SHEET_NAME))) {
                                        $scope.ImportType = "Products";
                                        $scope.validateimportTypeSelection = "Item";
                                        $scope.importSheetType = "Items";
                                        $scope.importSelectedvalue = "Items";
                                        $scope.importSheetSelected = "Products";
                                        $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                        $scope.importExcelSheetDDSelectionValue = sheet.SHEET_NAME;
                                        $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                                        continueLoop = false;
                                    }

                                    else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "TABLEDESIGNER" && (!$scope.validationSelectionSheet.includes(sheet.SHEET_NAME))) {
                                        $scope.importSelectedvalue = "TableDesigner";
                                        $scope.validateimportTypeSelection = "TableDesigner";
                                        $scope.importSheetSelected = "TableDesigner";
                                        $scope.importSheetType = "TableDesigner";
                                        $scope.ImportType = "TableDesigner";
                                        $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                        $scope.importExcelSheetDDSelectionValue = sheet.SHEET_NAME;
                                        $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                                        continueLoop = false;
                                    }

                                }
                            });
                            //In this Condition check the products values greater than 1000 by importing the sheet.


                            var userBatchImportValue = $scope.importSessionID[3];

                            if (parseInt(P_Count_Value) > parseInt(userBatchImportValue)) {

                                $scope.ChecktheProdCount = true;
                            } else {
                                $scope.ChecktheProdCount = false;
                            }

                            if (continueLoop == false) {

                                if ($scope.importSheetType != "TableDesigner") {

                                    $scope.validationPassed = true;
                                    $scope.passvalidationimportenable = false;
                                    $scope.passvalidationNext = false;
                                    $scope.validationCompleted = true;
                                    //$scope.importpicklistdiv = true;
                                    $scope.ResultDisplay = false;
                                    $scope.AttributeMappingdiv = false;
                                    $scope.ValidateDiv = false;
                                    $scope.showValidatediv = true;
                                    $scope.validationSuccess = true;
                                    $scope.importErrorFileWindowdiv = false;
                                    $scope.showValidationforNextSheet = false;
                                }

                                if ($scope.importSheetType == "TableDesigner") {
                                    $scope.ValidateDiv = false;
                                    $scope.ValidatetableDesignerDiv = true;
                                    $scope.tbldesigner = true;
                                    $scope.showValidatediv = false;

                                }

                            }

                            if (continueLoop == true && $scope.importSheetType != "TableDesigner") {


                                $scope.validationPassed = true;
                                $scope.passvalidationimportenable = false;
                                $scope.passvalidationNext = false;
                                $scope.ProcessName = "View Log";
                                $scope.validationCompleted = true;
                                //$scope.importpicklistdiv = true;
                                $scope.ResultDisplay = false;
                                $scope.AttributeMappingdiv = false;
                                $scope.ValidateDiv = false;
                                $scope.showValidatediv = true;
                                $scope.validationSuccess = true;
                                $scope.importErrorFileWindowdiv = false;
                                $scope.ProcessName = "Import Content";
                                $("#import").addClass("leftMenuactive");
                                $("#impvalidation").removeClass("leftMenuactive");

                                $("#Atrmappingimp").removeClass("leftMenuactive");
                                $("#choosefileimp").removeClass("leftMenuactive");
                                $("#sheetselectimp").removeClass("leftMenuactive");

                                $("#createTemplate").removeClass("leftMenuactive");

                                $("#logim").removeClass("leftMenuactive");
                                $scope.showValidationforNextSheet = true;
                                $scope.loadImportSheetNames();
                            }
                            /*********Product input screen changes by Aswin *********///
                        }
                        else if ($scope.ResultDisplay == false) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: $scope.validateimportTypeSelection + ' validation unsuccessful.',
                                type: "error"
                            });
                            $scope.ValidateDiv = true;
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: $scope.validateimportTypeSelection + ' validation unsuccessful.',
                                type: "error"
                            });

                            $scope.AttributeMappingdiv = false;
                            $scope.ValidateDiv = false;
                            $scope.validationCompleted = false;
                            $scope.importErrorFileWindowdiv = false;
                            $scope.showValidatediv = true;
                            $scope.validationPassed = true;
                            $scope.validationSuccess = false;
                            $scope.ResultDisplay = false;

                            // $scope.validatetResultDatasource.read();
                        }
                        //if (importresult.split("~", 1)[0].toLowerCase() == "validation failed") {
                        //    $.msgBox({
                        //        title: $localStorage.ProdcutTitle,
                        //        content: 'Validation failed. Please check log for details',
                        //        type: "error"
                        //    });
                        //    $scope.ProcessName = "Log Details";
                        //    $("#imptamp").removeClass("leftMenuactive");
                        //    $("#impvalidation").removeClass("leftMenuactive");

                        //    $("#Atrmappingimp").removeClass("leftMenuactive");
                        //    $("#choosefileimp").removeClass("leftMenuactive");
                        //    $("#sheetselectimp").removeClass("leftMenuactive");

                        //    $("#createTemplate").removeClass("leftMenuactive");
                        //    $("#logim").addClass("leftMenuactive");
                        //    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                        //    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                        //    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                        //    //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                        //    //$('#logim')[0].className = 'k-item';
                        //    //$('#imptamp')[0].className = 'k-item k-state-disabled';

                        //    $("#importSelectionTable").hide();
                        //    $scope.displayexportbutton = true;
                        //    $("#importErrorWindow1").show();

                        //    $scope.passvalidation = false;
                        //    $scope.validatetResultDatasource.read();
                        //    $scope.importErrorFileWindowdiv = true;
                        //    $scope.AttributeMappingdiv = false;
                        //    $scope.backbutton = true;
                        //    $scope.importErrorWindowdiv = false;
                        //    $("#importErrorWindow").hide();
                        //    $scope.importErrorWindowdiv = false;
                        //    $("#bulkimportErrorWindow").hide();
                        //    $scope.importSuccessWindowdiv = false;
                        //    $("#importSuccessWindow").hide();
                        //    $("#bulkimportSuccessWindow").hide();
                        //}
                        //else if (importresult.split("~", 1)[0].toLowerCase() == "validation passed") {
                        //    //$scope.passvalidation = true;
                        //    $scope.passvalidationimport = true;
                        //    $scope.passvalidation = false;

                        //    $.msgBox({
                        //        title: $localStorage.ProdcutTitle,
                        //        content: 'Validation passed.',
                        //        type: "error"
                        //    });
                        //}

                    }).error(function (error) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: $scope.validateimportTypeSelection + 'validation failed, please try again.',
                            type: "error"
                        });
                        $scope.validationPassed = true;
                        options.error(error);
                    });
                }
            }
        });

    };
    $scope.ViewLog = function () {
        $scope.showValidatediv = false;
        $scope.importErrorFileWindowdiv = true;
        $scope.ResultDisplay = true;
        $scope.ProcessName = "View Log";
        $("#imptamp").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#Atrmappingimp").removeClass("leftMenuactive");
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#createTemplate").removeClass("leftMenuactive");
        $("#logim").addClass("leftMenuactive");
        $scope.GetValidationResult();
    }

    $scope.ViewLogfortabledesigner = function () {
        $scope.showValidatediv = false;
        $scope.showValidatetabledesignerdiv = false;
        $scope.importErrorWindowfortabledesignerdiv = true;
        $scope.ResultDisplayfortabledesigner = true;
        $scope.tabledesignerhide = true;
        $scope.tabledesignershow = true;
        $scope.importErrorFileWindowdiv = false;
        $scope.ProcessName = "View Log";
        $("#imptamp").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#Atrmappingimp").removeClass("leftMenuactive");
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#createTemplate").removeClass("leftMenuactive");
        $("#logim").addClass("leftMenuactive");
        $scope.GetValidationResultfortabledesigner();
    }



    $scope.gridColumns = [
        { field: "STATUS", title: "STATUS", width: "200px" },
        { field: "ITEM_NO", title: "ITEM_NO", width: "200px" },
        { field: "ATTRIBUTE_NAME", title: "ATTRIBUTE_NAME", width: "200px" },
        { field: "PICKLIST_VALUE", title: "PICKLIST_VALUE", width: "200px" }

    ];
    $scope.getFinishImportFailedpicklistResults = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autoBind: false,
        scrollable: true,
        sort: { field: "STATUS", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportFailedpicklistResults($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    STATUS: { type: "string" },
                    ITEM_NO: { type: "string" },
                    ATTRIBUTE_NAME: { type: "string" },
                    PICKLIST_VALUE: { type: "string" }
                }
            }
        }

    });

    $scope.NextToValidate = function () {
        $scope.validationPassed = false;
        $scope.skipValidateimport();

    };
    $scope.nextSheet = false;
    $scope.Validateback = function () {

        $scope.BatchImport = false;
        $scope.EnableBatch = false;
        ///**************************** Product Input screen changes by Aswin  *************************/
        $scope.selectedSheet = [];
        $scope.validationSelectionSheet = [];
        $scope.importAllResults = [];
        ///**************************** Product Input screen changes by Aswin  *************************/
        if ($scope.ProcessName == "Validate Content" && $scope.ValidateDiv == true) {
            $scope.passvalidationimportenable = false;
            $scope.templateDetails = false;
            //$("#importSelectionTable").css("margin-top", "0px");
            $scope.AttributeMappingdiv = true;
            $scope.passvalidationNext = true;
            $scope.validationPassed = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = false;
            $scope.showValidatediv = false;
            $scope.ProcessName = "Map Attributes";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
            //$scope.ProcessName = "Attribute Mapping";

            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");

            $("#Atrmappingimp").removeClass("leftMenuactive");

            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
            $scope.getImportTypeBasedonSheet();
            // $scope.importGridOptions($scope.importSelectedvalue, $scope.excelPath, 0);
            return;
        }
        else if ($scope.ProcessName == "Validate Content" && $scope.AttributeMappingdiv == true) {
            $scope.passvalidationimportenable = false;
            $scope.templateDetails = false;
            //$("#importSelectionTable").css("margin-top", "0px");
            $scope.AttributeMappingdiv = true;
            $scope.passvalidationNext = true;
            $scope.validationPassed = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = false;
            $scope.showValidatediv = false;
            $scope.ProcessName = "Map Attributes";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
            //$scope.ProcessName = "Attribute Mapping";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#Atrmappingimp").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
            return;
        }
        else if ($scope.ProcessName == "Validate Content" && $scope.showValidatediv == true) {
            $scope.passvalidationimportenable = false;
            $scope.templateDetails = false;
            //$("#importSelectionTable").css("margin-top", "0px");
            $scope.AttributeMappingdiv = false;
            $scope.passvalidationNext = false;
            $scope.validationPassed = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = true;
            $scope.showValidatediv = false;
            $scope.ProcessName = "Validate Content";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#Atrmappingimp").removeClass("leftMenuactive");
            //$scope.ProcessName = "Attribute Mapping";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").addClass("leftMenuactive");
            $("#Atrmappingimp").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            return;
        }
        else if ($scope.ProcessName == "Import Content" && $scope.AttributeMappingdiv == false) {
            $scope.validationPassed = false;
            $scope.ResultDisplay = false;
            $scope.showValidatediv = false;
            $scope.ValidateDiv = true;
            $scope.templateDetails = false;
            $scope.ResultDisplay = false;
            $scope.AttributeMappingdiv = true;
            $scope.progressComplete = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = true;
            $scope.ResultDisplay = false;
            $scope.ResultDisplayfortabledesigner = false;
            $scope.AttributeMappingdiv = false;
            $scope.ProcessName = "Validate Content";

        }

        else if ($scope.ProcessName == "View Log" && $scope.ResultDisplay == true) {
            if ($scope.validationCompleted == true) {
                $scope.ValidateDiv = false;
                $scope.validationPassed = true;
                $scope.passvalidationimportenable = false;
                $scope.passvalidationNext = false;
                $scope.validationSuccess = true;
                $scope.validationCompleted = true;
                //$scope.importpicklistdiv = true;
                $scope.ResultDisplay = false;
                $scope.ResultDisplayfortabledesigner = false;
                $scope.AttributeMappingdiv = false;
                $scope.showValidatediv = true;
                $scope.importErrorFileWindowdiv = false;
                $scope.ProcessName = "Import Content";
                $("#import").addClass("leftMenuactive");
                $("#impvalidation").removeClass("leftMenuactive");

                $("#Atrmappingimp").removeClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");

                $("#createTemplate").removeClass("leftMenuactive");

                $("#logim").removeClass("leftMenuactive");
                return;
            }
            else if ($scope.validationSuccess == false) {
                $scope.ValidateDiv = false;
                $scope.validationPassed = true;
                $scope.passvalidationimportenable = false;
                $scope.passvalidationNext = false;
                $scope.validationCompleted = false;
                //$scope.importpicklistdiv = true;
                $scope.ResultDisplay = false;
                $scope.ResultDisplayfortabledesigner = false;
                $scope.AttributeMappingdiv = false;
                $scope.showValidatediv = true;
                $scope.importErrorFileWindowdiv = false;
                $scope.ProcessName = "Validate Content";
                $("#import").removeClass("leftMenuactive");
                $("#impvalidation").addClass("leftMenuactive");
                $("#Atrmappingimp").removeClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");

                $("#createTemplate").removeClass("leftMenuactive");

                $("#logim").removeClass("leftMenuactive");
                return;
            }

            $scope.ProcessName = "Validate Content";
            $scope.progressComplete = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = true;
            $scope.ResultDisplay = false;
            $scope.ResultDisplayfortabledesigner = false;
            $scope.AttributeMappingdiv = false;
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").addClass("leftMenuactive");
            //$scope.ProcessName = "Attribute Mapping";

            $("#choosefileimp").removeClass("leftMenuactive");

            $("#Atrmappingimp").removeClass("leftMenuactive");

            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
        }


        else if ($scope.ProcessName == "View Log" && $scope.ResultDisplay == true) {
            if ($scope.validationCompleted == true) {
                $scope.ValidateDiv = false;
                $scope.validationPassed = true;
                $scope.passvalidationimportenable = false;
                $scope.passvalidationNext = false;
                $scope.validationSuccess = true;
                $scope.validationCompleted = true;
                //$scope.importpicklistdiv = true;
                $scope.ResultDisplay = false;
                $scope.ResultDisplayfortabledesigner = false;
                $scope.AttributeMappingdiv = false;
                $scope.showValidatediv = true;
                $scope.importErrorFileWindowdiv = false;
                $scope.ProcessName = "Import Content";
                $("#import").addClass("leftMenuactive");
                $("#impvalidation").removeClass("leftMenuactive");

                $("#Atrmappingimp").removeClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");

                $("#createTemplate").removeClass("leftMenuactive");

                $("#logim").removeClass("leftMenuactive");
                return;
            }


            else if ($scope.validationSuccess == false) {
                $scope.ValidateDiv = false;
                $scope.validationPassed = true;
                $scope.passvalidationimportenable = false;
                $scope.passvalidationNext = false;
                $scope.validationCompleted = false;
                //$scope.importpicklistdiv = true;
                $scope.ResultDisplay = false;
                $scope.ResultDisplayfortabledesigner = false;
                $scope.AttributeMappingdiv = false;
                $scope.showValidatediv = true;
                $scope.importErrorFileWindowdiv = false;
                $scope.ProcessName = "Validate Content";
                $("#import").removeClass("leftMenuactive");
                $("#impvalidation").addClass("leftMenuactive");
                $("#Atrmappingimp").removeClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");

                $("#createTemplate").removeClass("leftMenuactive");

                $("#logim").removeClass("leftMenuactive");
                return;
            }

            $scope.ProcessName = "Validate Content";
            $scope.progressComplete = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = true;
            $scope.ResultDisplay = false;
            $scope.ResultDisplayfortabledesigner = false;

            $scope.AttributeMappingdiv = false;
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").addClass("leftMenuactive");
            //$scope.ProcessName = "Attribute Mapping";

            $("#choosefileimp").removeClass("leftMenuactive");

            $("#Atrmappingimp").removeClass("leftMenuactive");

            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
        }
        else {
            $scope.templateDetails = true;
            $scope.AttributeMappingdiv = false;
            $scope.IsFormSubmitted = true;
            $scope.passvalidationNext = true;
            $scope.ValidateDiv = false;
            $scope.attrMap = false;
            $scope.showValidatetabledesignerdiv = false;
            $scope.ValidatetableDesignerDiv = false;
            $scope.importSuccessWindowdiv = false;
            $scope.ResultDisplayfortabledesigner = false;
            //   $("#imptamp").removeClass("leftMenuactive");
            //$("#logim").removeClass("leftMenuactive");
            //$("#impvalidation").removeClass("leftMenuactive");
            //$("#Atrmappingimp").removeClass("leftMenuactive");
            //$("#choosefileimp").addClass("leftMenuactive");
            //$("#choosefileimp").removeClass("leftMenuactive");
            //$('.mappingattribute').css('height', '420px');
            //$('#leftMenu').css('height', '426px');
            //$("#basicimportTableSheetSlectectionWindow").hide();
            //$("#importTableSheetSlectectionWindow").hide();
            //$("#buttoncheck").hide();
            //$("#importErrorWindow1").hide();
            //$("#importErrorWindow").hide();
            //$("#expressionupdate").show();
            //$("#newsheetselection").hide();
            //$("#sheetvalidation").hide();
            //$("#importpicklist").hide();
            //$("#importTableSheetWindow").hide();
            //$("#importErrorWindow").hide();
            //$("#importpicklist").hide();
            //$("#importErrorWindow1").hide();
            //$scope.SelectedFileForUpload = null;
            //$scope.TemplateNameImport = "";
            //$scope.backbutton = false;
            //if ($scope.sheetCompleted == true) {
            //    $scope.ResetPage();
            //}
            //var $el = $('#importfile1');
            //$el.wrap('<form>').closest('form').get(0).reset();
            //$el.unwrap();
            //$rootScope.SelectedFileForUploadnamemain = "";
            //var dropdownlistCatalog = $("#catalogDrpDownLisr").data("kendoDropDownList");
            //$scope.ImportcatalogIdAttr = dropdownlistCatalog.element.val(0)[0][0].text;

            $("#imptamp").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#Atrmappingimp").removeClass("leftMenuactive");
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#choosefileimp").addClass("leftMenuactive");
            $scope.nextSheet = false;
            if ($scope.sheetCompleted == true) {
                $scope.ResetPage();
                $scope.sheetCompleted = false;
            }
            //$scope.templateDetails = true;
            //$scope.AttributeMappingdiv = false;
            //$scope.fileChoose = false;
            //$scope.IsFormSubmitted = false;

            //$("#drpTemplateDetails").data("kendoDropDownList").value(-1);
            if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
                $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        }
        //$('#choosefileimp')[0].className = 'k-item';

        //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
        //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
        //$('#impvalidation')[0].className = 'k-item k-state-disabled';
        //$('#logim')[0].className = 'k-item k-state-disabled';
        //$('#imptamp')[0].className = 'k-item k-state-disabled'


    };
    $scope.Validateback1 = function () {
        if ($scope.BatchImport == true)
            $scope.BatchImport = false;
        else
            $scope.EnableBatch = false;

        //$scope.Validateback();



    }

    $scope.Validatetabledesignerback = function () {
        if ($scope.ProcessName == "Validate Content" && $scope.ValidatetableDesignerDiv == true) {
            $scope.passvalidationimportenable = false;
            $scope.templateDetails = false;
            //$("#importSelectionTable").css("margin-top", "0px");
            $scope.AttributeMappingdiv = true;
            $scope.passvalidationNext = true;
            $scope.validationPassed = false;
            $scope.templateDetails = false;
            $scope.ValidatetableDesignerDiv = false;
            $scope.showValidatediv = false;
            $scope.ProcessName = "Map Attributes";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
            //$scope.ProcessName = "Attribute Mapping";

            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");

            $("#Atrmappingimp").removeClass("leftMenuactive");

            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
            return;
        }
        else if ($scope.ProcessName == "Validate Content" && $scope.AttributeMappingdiv == true) {
            $scope.passvalidationimportenable = false;
            $scope.templateDetails = false;
            //$("#importSelectionTable").css("margin-top", "0px");
            $scope.AttributeMappingdiv = true;
            $scope.passvalidationNext = true;
            $scope.validationPassed = false;
            $scope.templateDetails = false;
            $scope.ValidatetableDesignerDiv = false;
            $scope.showValidatediv = false;
            $scope.ProcessName = "Map Attributes";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
            //$scope.ProcessName = "Attribute Mapping";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#Atrmappingimp").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
            return;
        }
        else if ($scope.ProcessName == "Validate Content" && $scope.showValidatetabledesignerdiv == true) {
            $scope.passvalidationimportenable = false;
            $scope.templateDetails = false;
            //$("#importSelectionTable").css("margin-top", "0px");
            $scope.AttributeMappingdiv = false;
            $scope.passvalidationNext = false;
            $scope.validationPassed = false;
            $scope.templateDetails = false;
            $scope.showValidatediv = false;
            $scope.ValidatetableDesignerDiv = true;
            $scope.showValidatetabledesignerdiv = false;
            $scope.ProcessName = "Validate Content";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#Atrmappingimp").removeClass("leftMenuactive");
            //$scope.ProcessName = "Attribute Mapping";
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").addClass("leftMenuactive");
            $("#Atrmappingimp").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            return;
        }
        else if ($scope.ProcessName == "Import Content" && $scope.AttributeMappingdiv == false) {
            $scope.validationPassed = false;
            $scope.ResultDisplay = false;
            $scope.showValidatediv = false;
            $scope.ValidateDiv = false;
            $scope.templateDetails = false;
            $scope.ResultDisplay = false;
            $scope.AttributeMappingdiv = true;
            $scope.progressComplete = false;
            $scope.templateDetails = false;
            $scope.ValidatetableDesignerDiv = true;
            $scope.showValidatetabledesignerdiv = false;
            $scope.ResultDisplay = false;
            $scope.AttributeMappingdiv = false;
            $scope.ProcessName = "Validate Content";
        }

        else if ($scope.ProcessName == "View Log" && $scope.ResultDisplayfortabledesigner == true) {
            if ($scope.validationCompleted == true) {
                $scope.ValidateDiv = false;
                $scope.validationPassed = true;
                $scope.passvalidationimportenable = false;
                $scope.passvalidationNext = false;
                $scope.validationSuccess = true;
                $scope.validationCompleted = true;
                //$scope.importpicklistdiv = true;
                $scope.showValidatediv = false;
                $scope.showValidatetabledesignerdiv = true;
                $scope.ResultDisplayfortabledesigner = false;
                $scope.ResultDisplay = false;
                $scope.AttributeMappingdiv = false;
                $scope.showValidatediv = false;
                $scope.importErrorWindowfortabledesignerdiv = false;
                $scope.ProcessName = "Import Content";
                $("#import").addClass("leftMenuactive");
                $("#impvalidation").removeClass("leftMenuactive");

                $("#Atrmappingimp").removeClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");

                $("#createTemplate").removeClass("leftMenuactive");

                $("#logim").removeClass("leftMenuactive");
                return;
            }
            else if ($scope.validationSuccess == false) {
                $scope.ValidateDiv = false;
                $scope.validationPassed = true;
                $scope.passvalidationimportenable = false;
                $scope.passvalidationNext = false;
                $scope.validationCompleted = false;
                //$scope.importpicklistdiv = true;
                $scope.ResultDisplayfortabledesigner = false;
                $scope.AttributeMappingdiv = false;
                $scope.showValidatediv = false;
                $scope.ValidatetableDesignerDiv = false;
                $scope.importErrorWindowfortabledesignerdiv = false;
                $scope.showValidatetabledesignerdiv = true;
                $scope.ProcessName = "Validate Content";
                $("#import").removeClass("leftMenuactive");
                $("#impvalidation").addClass("leftMenuactive");
                $("#Atrmappingimp").removeClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");

                $("#createTemplate").removeClass("leftMenuactive");

                $("#logim").removeClass("leftMenuactive");
                return;
            }

            $scope.ProcessName = "Validate Content";
            $scope.progressComplete = false;
            $scope.templateDetails = false;
            $scope.ValidateDiv = true;
            $scope.ResultDisplay = false;
            $scope.ResultDisplayfortabledesigner = false;
            $scope.AttributeMappingdiv = false;
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#impvalidation").addClass("leftMenuactive");
            //$scope.ProcessName = "Attribute Mapping";

            $("#choosefileimp").removeClass("leftMenuactive");

            $("#Atrmappingimp").removeClass("leftMenuactive");

            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#import").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#Atrmappingimp").addClass("leftMenuactive");
        }
        else {
            $scope.templateDetails = true;
            $scope.AttributeMappingdiv = false;
            $scope.IsFormSubmitted = true;
            $scope.passvalidationNext = true;
            $scope.ValidateDiv = false;
            $scope.showValidatetabledesignerdiv = false;
            $scope.ValidatetableDesignerDiv = false;
            $scope.attrMap = false;
            $scope.importErrorFileWindowdiv = false;
            $scope.ResultDisplay = false;
            //   $("#imptamp").removeClass("leftMenuactive");
            //$("#logim").removeClass("leftMenuactive");
            //$("#impvalidation").removeClass("leftMenuactive");
            //$("#Atrmappingimp").removeClass("leftMenuactive");
            //$("#choosefileimp").addClass("leftMenuactive");
            //$("#choosefileimp").removeClass("leftMenuactive");
            //$('.mappingattribute').css('height', '420px');
            //$('#leftMenu').css('height', '426px');
            //$("#basicimportTableSheetSlectectionWindow").hide();
            //$("#importTableSheetSlectectionWindow").hide();
            //$("#buttoncheck").hide();
            //$("#importErrorWindow1").hide();
            //$("#importErrorWindow").hide();
            //$("#expressionupdate").show();
            //$("#newsheetselection").hide();
            //$("#sheetvalidation").hide();
            //$("#importpicklist").hide();
            //$("#importTableSheetWindow").hide();
            //$("#importErrorWindow").hide();
            //$("#importpicklist").hide();
            //$("#importErrorWindow1").hide();
            //$scope.SelectedFileForUpload = null;
            //$scope.TemplateNameImport = "";
            //$scope.backbutton = false;
            //if ($scope.sheetCompleted == true) {
            //    $scope.ResetPage();
            //}
            //var $el = $('#importfile1');
            //$el.wrap('<form>').closest('form').get(0).reset();
            //$el.unwrap();
            //$rootScope.SelectedFileForUploadnamemain = "";
            //var dropdownlistCatalog = $("#catalogDrpDownLisr").data("kendoDropDownList");
            //$scope.ImportcatalogIdAttr = dropdownlistCatalog.element.val(0)[0][0].text;

            $("#imptamp").removeClass("leftMenuactive");
            $("#logim").removeClass("leftMenuactive");
            $("#impvalidation").removeClass("leftMenuactive");
            $("#Atrmappingimp").removeClass("leftMenuactive");
            $("#choosefileimp").removeClass("leftMenuactive");
            $("#sheetselectimp").removeClass("leftMenuactive");
            $("#choosefileimp").addClass("leftMenuactive");
            $scope.nextSheet = false;
            if ($scope.sheetCompleted == true) {
                $scope.ResetPage();
                $scope.sheetCompleted = false;
            }
            //$scope.templateDetails = true;
            //$scope.AttributeMappingdiv = false;
            //$scope.fileChoose = false;
            //$scope.IsFormSubmitted = false;

            //$("#drpTemplateDetails").data("kendoDropDownList").value(-1);
            if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
                $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        }
        //$('#choosefileimp')[0].className = 'k-item';

        //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
        //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
        //$('#impvalidation')[0].className = 'k-item k-state-disabled';
        //$('#logim')[0].className = 'k-item k-state-disabled';
        //$('#imptamp')[0].className = 'k-item k-state-disabled'

    }

    $scope.export_log = function () {

        // alert("dssds");

        dataFactory.validationresult($scope.Errorlogoutputformat, $rootScope.SelectedFileForUploadnamemain).success(function (response) {
            //$scope.passvalidation = response;
            window.open("DownloadFile.ashx?Path=" + response + "&importType=" + $scope.ImportType);


        }).error(function (error) {

            options.error(error);
        });
    };

    $scope.tableDesignerError_log = function () {

        // alert("dssds");

        dataFactory.validationresult($scope.Errorlogoutputformat, $rootScope.SelectedFileForUploadnamemain).success(function (response) {
            //$scope.passvalidation = response;
            window.open("DownloadTableDesignerErrorLog.ashx?Path=" + response + "&importType=" + $scope.ImportType);


        }).error(function (error) {

            options.error(error);
        });
    };

    $scope.DownLoadSourceFile = function () {

        // alert($scope.excelPath);
        //  $location.path($scope.excelPath);

        ///****************** Product input screen changes ******************//////////////
        if ($scope.importMappingAtttributeErrorWindow == true) {
            dataFactory.validationresult($scope.Errorlogoutputformat, $rootScope.SelectedFileForUploadnamemain).success(function (response) {
                //$scope.passvalidation = response;
                window.open("DownloadTableDesignerErrorLog.ashx?Path=" + response + "&importType=" + $scope.ImportType);


            }).error(function (error) {

                options.error(error);
            });
        }

        else if ($scope.duplicateMappingAttributes == true) {
            dataFactory.validationresult($scope.Errorlogoutputformat, $rootScope.SelectedFileForUploadnamemain).success(function (response) {
                //$scope.passvalidation = response;
                window.open("DownloadTableDesignerErrorLog.ashx?Path=" + response + "&importType=" + $scope.ImportType);


            }).error(function (error) {

                options.error(error);
            });
        }
            ///****************** Product input screen changes ******************//////////////
        else {
            window.open("DowloadImageManagementFiles.ashx?Path=" + $scope.importSessionID[$scope.importSessionID.length - 1] + "&Type=." + $scope.ImportFormat.toString().toLowerCase() + "&DownloadType=" + $scope.SelectedFileForUploadnamemain);
            return false;
            //$window.open($scope.excelPath);
        }

    };

    $scope.sheetdownload = function () {

        // alert($scope.excelPath);
        //  $location.path($scope.excelPath);
        window.open("DowloadImageManagementFiles.ashx?Path=" + $scope.excelPath + "&Type=.xls");
        return false;
        //$window.open($scope.excelPath);
    };
    $scope.MissingColumns = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autoBind: false,
        scrollable: true,
        sort: { field: "CATALOG_NAME", dir: "asc" },
        transport: {
            read: function (options) {

                dataFactory.getmissingcolumns($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    CATALOG_NAME: { type: "string" },
                    CATEGORY_NAME: { type: "string" },
                    FAMILY_NAME: { type: "string" },
                    CATALOG_ITEM_NO: { type: "string" },
                    Missing_Columns: { type: "string" }
                }
            }
        }

    });
    $scope.missingcolumnsheet = [
        { field: "CATALOG_NAME", title: "CATALOG_NAME", width: "200px" },
        { field: "CATEGORY_NAME", title: "CATEGORY_NAME", width: "200px" },
        { field: "FAMILY_NAME", title: "FAMILY_NAME", width: "200px" },
        { field: "CATALOG_ITEM_NO", title: "ITEM_NO", width: "200px" },
        { field: "Missing_Columns", title: "MISSING_COLUMNS", width: "200px" }

    ];
    $scope.sheetCompleted = false;
    $scope.nextSheetSelection = function () {
        $scope.nextSheet = true;
        $scope.importSuccessWindowdiv = false;
        $scope.ResultDisplay = false;
        $scope.ResultDisplayfortabledesigner = false;
        $scope.AttributeMappingdiv = true;
        $scope.progressComplete = false;
        $scope.importCompletedStatus = true;
        $scope.passvalidationNext = true;
        //$scope.listAllSheet.push($scope.importSelectedvalue);
        var continueLoop = true;
        var excelSheets = $scope.importExcelSheetSelction._data;
        var nextValue = 0;
        if ($scope.nextSheetProcess == true && excelSheets.length >= 2 && $scope.listAllSheet.length <= excelSheets.length) {
            $scope.nextSheetProcess = false;
            angular.forEach(excelSheets, function (sheet) {
                if (continueLoop) {
                    if ($scope.importSelectedvalue == "Categories") {

                        $scope.importSelectedvalue = "Families";
                        $scope.importSheetSelected = "Families";
                        $scope.importExcelSheetDDSelectionValue = "Families";
                        continueLoop = false;


                    }
                    else if ($scope.importSelectedvalue == "Families") {
                        $scope.importSelectedvalue = "Products";
                        $scope.importSheetSelected = "Products";
                        $scope.importExcelSheetDDSelectionValue = "Products";
                        continueLoop = false;
                    }
                    else if ($scope.importSelectedvalue == "Products") {
                        $scope.importSelectedvalue = "TableDesigner";
                        $scope.importSheetSelected = "TableDesigner";
                        $scope.importExcelSheetDDSelectionValue = "TableDesigner";
                        continueLoop = false;
                    }

                    else if ($scope.importSelectedvalue == "TableDesigner") {
                        continueLoop = false;
                    }

                    //if (nextValue == 0) {
                    //    nextValue = 1;
                    //} else {
                    //    if (!$scope.listAllSheet.includes(sheet.TABLE_NAME)) {
                    //        $scope.importSelectedvalue = sheet.TABLE_NAME;
                    //        $scope.importSheetSelected = sheet.TABLE_NAME;
                    //        $scope.importExcelSheetDDSelectionValue = sheet.TABLE_NAME;
                    //        //$scope.listAllSheet.push(sheet);
                    //        continueLoop = false;
                    //    }
                    //}
                }
            });
        }

        dataFactory.checkCatalogDetails($scope.excelPath, $scope.importSelectedvalue, $scope.ImportcatalogIdAttr).success(function (data) {
            if (data == null || data == "") {
                data = "Catalog Valid_Yes_Product";
            }
            var result = data.split("_");

            if (result[0] == "Catalog Invalid") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Selected catalog is not available in sheet.Kindly check it.',
                    type: "info"
                });
            } else {
                if ($scope.importSelectedvalue.toUpperCase().includes('PRODUCT')) {
                    $scope.ImportType = "Product";
                } else if ($scope.importSelectedvalue.toUpperCase().includes("FAMIL")) {
                    $scope.ImportType = "Family";
                }
                else if ($scope.importSelectedvalue.toUpperCase().includes("CATEG")) {
                    $scope.ImportType = "Category";

                } else if ($scope.importSelectedvalue.toUpperCase().includes("TABLEDESIGNER")) {
                    $scope.ImportType = "TableDesigner";
                }
                else
                    $scope.ImportType = result[2];
                $scope.importTypeSelect();

            }
        });
        $scope.importGridOptions($scope.importSelectedvalue, $scope.excelPath, 0);
        $scope.ProcessName = "Validate Content"
        $scope.Validateback();
    };


    //$scope.importSelectedvalue = response[1].TABLE_NAME;
    //$scope.importSheetSelected = response[1].TABLE_NAME;

    $scope.btnNextSheet = function () {
        $scope.templateDetails = false;
        $("#importSelectionTable").css("margin-top", "0px");
        $scope.AttributeMappingdiv = true;
        //$scope.ProcessName = "Attribute Mapping";
        $scope.ProcessName = "Map Attributes";
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#Atrmappingimp").addClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
    };

    $scope.btnBackSheet = function () {
        //  $("#basicimportTableSheetSlectectionWindow").hide();
        //  $("#importTableSheetSlectectionWindow").hide();
        //  $("#buttoncheck").hide();
        ////  $("#importErrorWindow1").hide();
        //  $("#importErrorWindow").hide();
        //  $("#expressionupdate").show();
        //  $("#newsheetselection").hide();
        //  $("#sheetvalidation").hide();
        //  $("#importpicklist").hide();
        //  $("#importTableSheetWindow").hide();
        //  $("#importErrorWindow").hide();
        //  $("#importpicklist").hide();
        // // $("#importErrorWindow1").hide();
        $scope.SelectedFileForUpload = null;
        $scope.TemplateNameImport = "";
        $scope.backbutton = false;
        var $el = $('#importfile1');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        $rootScope.SelectedFileForUploadnamemain = "";

        var dropdownlistCatalog = $("#catalogDrpDownLisr").data("kendoDropDownList");
        $scope.ImportcatalogIdAttr = dropdownlistCatalog.element.val(0)[0][0].text;
        $("#imptamp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");
        $("#Atrmappingimp").removeClass("leftMenuactive");
        $("#choosefileimp").addClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $scope.templateFirst = true;
        $scope.templatesecond = false;
        $scope.templateDetails = true;
        $scope.AttributeMappingdiv = false;
        $scope.fileChoose = false;
        $scope.IsFormSubmitted = false;

        $("#drpTemplateDetails").data("kendoDropDownList").value(-1);
    };
    $scope.ViewBachLog = function () {

        $scope.ImportViewLogGridOptionsDatasource.read();
        $scope.winBatchLog.refresh({ url: "../views/app/partials/BatchLog.html" });
        $scope.winBatchLog.center().open();
    }

    //$scope.GetBatchLogList = function () {

    //    dataFactory.GetBatchLogList().success(function (response) {
    //        $scope.BatchLogList = response;
    //        // $scope.ImportViewLogGridOptionsDatasource.read();
    //    });
    //};
    //   $scope.GetBatchLogList();

    //Pause Import process

    $scope.PauseDeatils = function (data) {
        // $scope.GetBatchLogList();
        // $scope.ImportViewLogGridOptionsDatasource.read();
        dataFactory.PauseImportProcess(data.dataItem.BatchId).success(function (response) {
            if (response == "Import is already running.Please wait for few minutes") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Import is already running.Please wait for few minutes.',
                    type: "info"
                });
            }
            else
                if (response == "Import Paused") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Import Paused.',
                        type: "info"
                    });
                }
            //$scope.BatchLogList = response;
            // $timeout(function () { $scope.GetBatchLogList(); }, 500)
            $scope.ImportViewLogGridOptionsDatasource.read();
            // $scope.GetBatchLogList();

        });
    }

    //Run import process
    $scope.RunImportProcess = function (data) {

        // $scope.GetBatchLogList();

        dataFactory.RunImportProcess(data.dataItem.BatchId).success(function (response) {
            if (response == "Import is already running.Please wait for few minutes") {
                // $scope.ImportViewLogGridOptionsDatasource.read();
                //  $scope.GetBatchLogList();EnableBachfun
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Import is already running.Please wait for few minutes.',
                    type: "info"
                });

            }
            //$scope.BatchLogList = response;
            $scope.ImportViewLogGridOptionsDatasource.read();


        });
        //$scope.ImportViewLogGridOptionsDatasource.read();
    }

    $scope.EnableBachfun = function () {
        $scope.EnableBatch = true;
        //  $scope.scheduleImport = false;
        //  $scope.BatchImport = false;
        $scope.scheduleTime = new Date();
        var currentdate = new Date();
        $('#txtDataTime').kendoDateTimePicker({
            value: $scope.scheduleTime,

            parseFormats: ['MM/dd/yyyy'],
            min: $scope.scheduleTime,

        });

    }
    $scope.EnableBachfun1 = function () {
        $scope.EnableBatch = true;
    }
    $scope.RefreshBatchLog = function (batchId) {
        // $scope.RunImportProcess(batchId);
        //$scope.GetBatchLogList();
        $scope.ImportViewLogGridOptionsDatasource.read();
    };

    //$scope.ClearLog = function () {
    //    dataFactory.ClearLog().success(function (response) {
    //        $scope.GetBatchLogList();
    //    })
    //}
    $scope.DownLoadErrorLog = function (batchId) {
        dataFactory.DownLoadErrorLog(batchId).success(function (response) {
            window.open("DownloadFile.ashx?Path=" + response);
        });
    }

    $scope.scheduleTime = new Date();

    $('#txtDataTime').kendoDateTimePicker({
        value: $scope.scheduleTime,

        parseFormats: ['MM/dd/yyyy'],
        min: $scope.scheduleTime,

    });


    // --------To get EnableSubProduct value from preference page-------
    $rootScope.getEnableSubProduct = function () {
        //$scope.CustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
        dataFactory.getEnableSubProduct().success(function (response) {
            $rootScope.EnableSubProduct = response;
            $localStorage.EnableSubProductLocal = response;
        });
    };
    var clearedBatchValues = [];
    //Delete the value by id
    $scope.ClearLogByid = function (data) {
        clearedBatchValues = [];
        clearedBatchValues.push(data.dataItem.BatchId);
        dataFactory.ClearLog(clearedBatchValues).success(function (response) {
            $scope.ImportViewLogGridOptionsDatasource.read();
            // $scope.GetBatchLogList();
            clearedBatchValues = [];
        })
    }

    //New_WithOut value or parameter.
    $scope.ClearLog = function () {
        var getselectedValues = clearedBatchValues;

        if (clearedBatchValues.length != 0) {
            dataFactory.ClearLog(getselectedValues).success(function (response) {
                if (response == "Error") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Multiple delete having error.',
                        type: "info"
                    });
                }
                $scope.ImportViewLogGridOptionsDatasource.read();
                // $scope.GetBatchLogList();
                clearedBatchValues = [];
            })
        } else {
            return;
        }



    }

    //When click the view dtails 
    $scope.ViewDeatils = function (data) {
        $rootScope.Batchviewdetails = data.dataItem;
        //     $scope.GetBatchLogList();
        $scope.ImportViewLogGridOptionsDatasource.read();
        $scope.winBatchLogvalue.refresh({ url: "../views/app/partials/BatchViewDetails.html" });
        $scope.winBatchLogvalue.center().open();
    }

    $scope.selectAlllogSelection = function (data, e) {
        clearedBatchValues = [];
        var CurrentCheck = e.currentTarget.checked;
        if (CurrentCheck == true) {
            for (var i = 0 ; data.length > i ; i++) {
                if (data[i].Status == "UPDATE" || data[i].Status == "ERROR") {
                    clearedBatchValues.push(data[i].BatchId);
                }
            }
        }
        if (CurrentCheck == false) {
            clearedBatchValues = [];
        }
    }
    //---------------To Start ImportViewLogGridOptions   -------------------

    //---------------------------------------------------Product Price------------------------------------------------------------------
    //  $scope.ImportViewLogGridOptionsDatasource.read();

    $scope.ImportViewLogGridOptions =
          {
              dataSource: $scope.ImportViewLogGridOptionsDatasource,
              sortable: true, scrollable: true, editable: false, pageable: { buttonCount: 5 }, autoBind: false,
              toolbar: [
   { name: "save", text: "", template: '<button id="refreshLog" class="btn btn-primary btn-xs" title="Refresh" ng-click="RefreshBatchLog()">Refresh</button>' },
   { name: "cancel", text: "", template: '<button id="clearLog" class="btn btn-primary btn-xs" style="float: right;margin-right: 0px;" title="Delete Multiple Records" ng-click="ClearLog()">Delete</button>' }
              ],
              columns: [
              { field: "CatalogName", title: "Catalog name", width: "100px", headerTemplate: '<span title="Catalog name">Catalog name</span>' },
              { field: "FileName", title: "File name", width: "100px", headerTemplate: '<span title="File name">File name</span>' },
              { field: "NoOfRecords", title: "No of records", width: "120px", headerTemplate: '<span title="No of records">No of records</span>' },
              { field: "ImportType", title: "Import type", width: "100px", headerTemplate: '<span title="Import type">Import type</span>' },
              { field: "CustomStatus", title: "Status", width: "100px", headerTemplate: '<span title="Status">Status</span>' },
              { field: "ElapsedTime", title: "Elapsed time", width: "100px", headerTemplate: '<span title="Elapsed time">Elapsed time</span>' },
              { field: "scheduleTime", title: "Schedule time", width: "100px", headerTemplate: '<span title="Schedule time">Schedule time</span>' },
              {
                  field: "CustomStatus",
                  title: "Action",
                  template: '<button #= (CustomStatus=="Completed" || CustomStatus=="Error") ? "" : "disabled=true" # ng-click="ViewDeatils(this)" title="View Log" class="btn btn-info cus"><i class="fa fa-eye"></i> </button>  &nbsp <button #= CustomStatus=="Pending" ? "" : "disabled=true" # ng-click="RunImportProcess(this)" title="Run"  class="btn btn-info cus"><i class="icon-play"></i> </button> &nbsp <button #= CustomStatus=="Inprogress" ? "disabled=true" : "" # ng-click="ClearLogByid(this)" title="Delete"  class="btn btn-info cus"><i class="fa fa-remove"></i> </button>',
                  width: "110px",
                  sortable: false,
                  headerTemplate: '<span title="Action">Action</span>',
                  filterable: false
              },
             {
                 field: "ISAvailable",
                 title: "Select",
                 sortable: false,
                 // template: '<input type="checkbox"   ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>',#= ISAvailable ? "disabled" : ""
                 template: '<input type="checkbox"  #= CustomStatus=="Inprogress" ? checked="disabled" : !ISAvailable && CustomStatus!="Inprogress" ? checked="" : "checked" #  ng-click="updateSelection($event, this)"></input>',
                 // template: '<input type="checkbox"   #= CustomStatus=="Inprogress"?"(#=ISAvailable ? "checked=checked " : "") ":"disabled=true " #  ng-click="updateSelection($event, this)"></input>',
                 //  template: '<input type="checkbox"   #= CustomStatus=="Inprogress"? "disabled=true " : "checked=checked" #  ng-click="updateSelection($event, this)"></input>',
                 headerTemplate: "<input type='checkbox' id='Updateselect'  title='select all'  ng-click='selectAll($event,this)'/>",
                 width: "40px",
                 filterable: false
             },
              ],
              filterable: true,
              dataBound: function (e) {
              },
          };


    $scope.ImportViewLogGridOptionsDatasource = new kendo.data.DataSource({
        pageSize: 10,
        serverPaging: true,
        serverSorting: true,
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetBatchLogList().success(function (response) {

                    for (var i = 0 ; response.length > i ; i++) {
                        if (response[i]["ImportType"] == "ITEM#" || response[i].ImportType == "CATALOG_ITEM_NO") {
                            response[i]["ImportType"] = $localStorage.CatalogItemNumber;
                        }
                        if (response[i]["ImportType"] == "SUBITEM#" || response[i].ImportType == "SUBCATALOG_ITEM_NO") {
                            response[i]["ImportType"] = $localStorage.CatalogSubItemNumber;
                        }
                    }

                    options.success(response);

                    angular.forEach($('.k-i-filter'), function (value) {
                        value.className = "k-icon k-filter";
                    });
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        },
        schema: {
            model: {
                // id: "BATCHID",
                fields: {
                    // CatalogName: { editable: false, nullable: true },
                    ISAvailable: { editable: true, type: "boolean", sortable: false },

                    CatalogName: { editable: false, nullable: true },
                    FileName: { editable: false, nullable: true },
                    ImportType: { editable: false, nullable: true },
                    NoOfRecords: { editable: false, nullable: true },
                    FAMILY_NAME: { editable: false, nullable: true },
                    CustomStatus: { editable: false, nullable: true },
                    ElapsedTime: { editable: false, nullable: true },
                    scheduleTime: { editable: false, nullable: true },
                }
            }
        }
    });


    var DeleteValues1 = [];


    $scope.selectAll = function (ev, th) {

        var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
        var items = grid.dataItems();
        //Start

        var CurrentCheck = ev.currentTarget.checked;




        if (CurrentCheck == true) {
            items.forEach(function (item) {
                item.set("ISAvailable", false);
            });
        }

        if (CurrentCheck == true) {

            DeleteValues1 = [];

            for (var i = 0; i < items.length; i++) {
                DeleteValues1.push(items[i].BatchId);
            }
        } else {
            DeleteValues1 = [];
        }



        if (CurrentCheck == true) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].CustomStatus != "Inprogress") {
                    clearedBatchValues.push(items[i].BatchId);
                }
            }
            // clearedBatchValues.push(batchId);
        }
        if (CurrentCheck == false) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].CustomStatus != "Inprogress") {
                    clearedBatchValues.pop(items[i].BatchId);
                }
            }
        }
        //End
        items.forEach(function (item) {
            if (item.ISAvailable == false) {

                if (item.CustomStatus == "Inprogress") {
                    item.set("ISAvailable", false);
                } else {
                    {
                        item.set("ISAvailable", ev.target.checked);
                    }
                }
            }
            else if (item.ISAvailable == true) {
                if (item.CustomStatus == "Inprogress") {
                    item.set("ISAvailable", false);
                } else {
                    item.set("ISAvailable", ev.target.checked);
                }
            }
        });
    };


    // To clear the selected value in import viewlog

    $scope.updateSelection = function (e, id) {

        var grid = $(e.target).closest('[kendo-grid]').data("kendoGrid");
        var items = grid.dataItems();


        var batchId = id.dataItem.BatchId;
        var CurrentCheck = e.currentTarget.checked;



        if (CurrentCheck == true) {
            clearedBatchValues.push(batchId);
            DeleteValues1.push(batchId);

            if (items.length == DeleteValues1.length) {
                $("#Updateselect").prop("checked", true);
            }
        }



        if (CurrentCheck == false) {
            for (var i = 0 ; clearedBatchValues.length > i ; i++) {
                if (batchId == clearedBatchValues[i]) {
                    clearedBatchValues.pop(clearedBatchValues[i]);
                }
            }
            DeleteValues1.pop();
            $("#Updateselect").prop("checked", false);
        }

        if (DeleteValues1.length == 0) {
            $("#Updateselect").prop("checked", false);
        }

        // alert(batchId);
    }


    // To clear the selected value in import viewlog BATCH_ID // ***********************************************$$$$$$$$$$$$$$$$$$$$


    $scope.IsSelectAll = false;
    // Select All option for Mapping Atributes
    $scope.selectAllAttributes = function () {
        /*****Product input screen changes  ******/
        $scope.selectedAttributeType = "undefined";
        /*****Product input screen changes  ******/
        if ($("#selectAllChk").prop("checked") == true) {
            //alert("Checkbox is checked.");
            $scope.IsSelectAll = true;
            $scope.importGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath, 1);
        }
        else if ($("#selectAllChk").prop("checked") == false) {
            $scope.IsSelectAll = false;
            $scope.importGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath, 0);
        }

    };

    // Select All option for Mapping Atributes


    $scope.GetValidationResultfortabledesigner = function () {
        dataFactory.validatetabledesignerImportResults($scope.importSessionID, $scope.ImportType).success(function (response) {
            if (response != null) {
                var obj = jQuery.parseJSON(response.Data.Data);
                $scope.validateLogprod = obj;
                $scope.validateLogcolumn = response.Data.Columns;
                $scope.importErrorWindowfortabledesignerdiv = true;
                $scope.showTabledesignerErrorLog = true;
                $scope.showErrorLog = false;
            }
        });
    };


    $scope.Validatetabledesignerimport = function () {
        $scope.importSuccessWindowdiv = false;
        $scope.importErrorWindowdiv = false;
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            //content: "Validation may take some time to complete",
            content: "Proceed to validate import file.",
            type: "confirm",
            buttons: [{ value: "Ok" },
            { value: "Cancel" }],
            success: function (result) {
                if (result === "Ok") {

                    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                    //$('#impvalidation')[0].className = 'k-item';
                    //$('#logim')[0].className = 'k-item k-state-disabled';
                    //$('#imptamp')[0].className = 'k-item k-state-disabled';

                    //Dynamicaly Changed ctalog)item values _ START.


                    //END
                    /*********Product input screen changes by Aswin *********///
                    $scope.validationImportSheetName = $scope.validationimportSelectedvalue;


                    dataFactory.Validatetabledesignerimport($scope.importExcelSheetDDSelectionValue, $scope.allowDuplication, $scope.excelPath, $scope.ImportType, $scope.ImportcatalogIdAttr, $scope.ImportFormat, $scope.validated, $scope.prodImportData).success(function (importresult) {
                        $scope.importSessionID = importresult.split("~", importresult.split("~").length);   // kendo data source ;
                        var validation = "";

                        $scope.CategoryNameValidationerror = false;
                        $scope.FamilyNameValidationerror = false;
                        $scope.CategoryNameValidation = false;
                        $scope.FamilyNameValidation = false;
                        $scope.missingtabledesignerValidationColumnError = false;

                        //$scope.data = importresult.split("~", importresult.split("~").length);
                        angular.forEach(importresult.split('~'), function (data) {
                            if (data == "Import Failed") {

                                $scope.ResultDisplayfortabledesigner = false;

                                return;
                            }
                            if (data.toString().toUpperCase() == "FAMILYNAMEMISSING" || data.toString().toUpperCase() == "FAMILYNAMEMISSING") {
                                //  $scope.ProcessName = "View Log";
                                $scope.ResultDisplayfortabledesigner = true;
                                // $scope.missingColumnInValid = true;
                                //$scope.showValidatediv = false;
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");
                                $scope.FamilyNameValidationerror = true;
                                //  $scope.MissingColumns.read();
                            }
                            else if (data.toString().toUpperCase() == "CATEGORYNAMEMISSING" || data.toString().toUpperCase() == "CATEGORYNAMEMISSING") {
                                //  $scope.ProcessName = "View Log";
                                //$scope.importErrorFileWindowdiv = true;

                                $scope.ResultDisplayfortabledesigner = true;
                                //$scope.showValidatediv = false;
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");
                                $scope.CategoryNameValidationerror = true;
                                // $scope.validatetResultDatasource.read();
                            }

                            else if (data.toString().toUpperCase() == "MISSINGCOLUMNS" || data.toString().toUpperCase() == "MISSINGCOLUMNS") {
                                //$scope.ProcessName = "View Log";
                                //$scope.importpicklistdiv = true;
                                $scope.ResultDisplayfortabledesigner = true;
                                $scope.showValidatediv = false;
                                $scope.missingtabledesignerValidationColumnError = true;
                                $scope.CategoryNameValidationskipped = false;
                                $scope.FamilyNameValidationskipped = false;

                                //$scope.ProcessName = "View Log";
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");

                                //  $scope.getFinishImportFailedpicklistResults.read();
                            }
                            else if (data == "DUPLICATE_ITEM_NO" || data == "DUPLICATE_RECORDS") {
                                //$scope.ProcessName = "View Log";
                                //$scope.importpicklistdiv = true;
                                $scope.ResultDisplay = true;
                                $scope.showValidatediv = false;
                                //$scope.ProcessName = "View Log";
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");
                                $scope.duplicateItemError = true;
                                //  $scope.getFinishImportFailedpicklistResults.read();
                            }
                            else if (data == "DUPLICATE_FAMILY_NAME" || data == "DUPLICATE_RECORDS") {
                                //$scope.ProcessName = "View Log";
                                //$scope.importpicklistdiv = true;
                                $scope.ResultDisplay = true;
                                $scope.showValidatediv = false;
                                //$scope.ProcessName = "View Log";
                                //$("#imptamp").removeClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");
                                //$("#logim").addClass("leftMenuactive");
                                $scope.duplicateItemError = true;
                                //  $scope.getFinishImportFailedpicklistResults.read();
                            }

                            else if (data.toUpperCase().includes("PASSED") || data.toUpperCase().includes('SUCCESS')) {
                                //$scope.passvalidation = true;
                                //$scope.passvalidationimport = true;
                                //$scope.passvalidation = false;
                                validation = "Passed";

                            }

                            if ($scope.missingtabledesignerValidationColumnError == true) {
                                $scope.CategoryNameValidation = false;
                            }

                            if ($scope.missingtabledesignerValidationColumnError == true) {
                                $scope.FamilyNameValidation = false;
                            }

                            if ($scope.FamilyNameValidationerror == true && $scope.missingtabledesignerValidationColumnError == false) {
                                $scope.FamilyNameValidationskipped = true;
                            }
                            if ($scope.FamilyNameValidationerror == false && $scope.missingtabledesignerValidationColumnError == false) {
                                $scope.FamilyNameValidation = true;
                            }
                            if ($scope.CategoryNameValidationerror == true && $scope.missingtabledesignerValidationColumnError == false) {
                                $scope.CategoryNameValidationskipped = true;
                            }
                            if ($scope.CategoryNameValidationerror == false && $scope.missingtabledesignerValidationColumnError == false) {
                                $scope.CategoryNameValidation = true;
                            }

                        });
                        if ($scope.ResultDisplayfortabledesigner == false && validation == "Passed") {

                            // IN this variable get the product count from importing sheet
                            var P_Count_Value = $scope.importSessionID[2];
                            //alert($scope.importSessionID[2]);
                            //alert($scope.importSessionID[3]);


                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: $scope.ImportType + ' validation completed successfully.',
                                type: "success"
                            });

                            $scope.validationStatus = "1";
                            $scope.updateValidationImportTypeFlag();

                            var excelSheets = $scope.validationSheetname;

                            var continueLoop = true;

                            if ($scope.validationSelectionSheet.length == 0) {
                                $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                            }
                            //else
                            //{
                            //    $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                            //}
                            angular.forEach(excelSheets, function (sheet) {
                                if (continueLoop) {
                                    if (sheet.IMPORT_TYPE.trim().toUpperCase() == "CATEGORY" && (!$scope.validationSelectionSheet.includes(sheet.SHEET_NAME))) {
                                        $scope.ImportType = "Categories";
                                        $scope.validateimportTypeSelection = "Category";
                                        $scope.importSheetType = "Categories";
                                        $scope.importSelectedvalue = "Categories";
                                        $scope.importSheetSelected = "Categories";
                                        $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                        $scope.importExcelSheetDDSelectionValue = sheet.SHEET_NAME;
                                        $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                                        continueLoop = false;


                                    }
                                    else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "PRODUCT" && (!$scope.validationSelectionSheet.includes(sheet.SHEET_NAME))) {
                                        $scope.ImportType = "Families";
                                        $scope.validateimportTypeSelection = "Product";
                                        $scope.importSheetType = "Products";
                                        $scope.importSelectedvalue = "Products";
                                        $scope.importSheetSelected = "Families";
                                        $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                        $scope.importExcelSheetDDSelectionValue = sheet.SHEET_NAME;
                                        $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                                        continueLoop = false;
                                    }
                                    else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "ITEM" && (!$scope.validationSelectionSheet.includes(sheet.SHEET_NAME))) {
                                        $scope.ImportType = "Products";
                                        $scope.validateimportTypeSelection = "Item";
                                        $scope.importSheetType = "Items";
                                        $scope.importSelectedvalue = "Items";
                                        $scope.importSheetSelected = "Products";
                                        $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                        $scope.importExcelSheetDDSelectionValue = sheet.SHEET_NAME;
                                        $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                                        continueLoop = false;
                                    }

                                    else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "TABLEDESIGNER" && (!$scope.validationSelectionSheet.includes(sheet.SHEET_NAME))) {
                                        $scope.importSelectedvalue = "TableDesigner";
                                        $scope.validateimportTypeSelection = "TableDesigner";
                                        $scope.importSheetSelected = "TableDesigner";
                                        $scope.importSheetType = "TableDesigner";
                                        $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                        $scope.importExcelSheetDDSelectionValue = sheet.SHEET_NAME;
                                        $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                                        continueLoop = false;
                                    }

                                }
                            });
                            //In this Condition check the products values greater than 1000 by importing the sheet.

                            var userBatchImportValue = $scope.importSessionID[3];

                            if (parseInt(P_Count_Value) > parseInt(userBatchImportValue)) {

                                $scope.ChecktheProdCount = true;
                            } else {
                                $scope.ChecktheProdCount = false;
                            }

                            $scope.ValidateDiv = true;
                            $scope.ValidatetableDesignerDiv = false;
                            $scope.showValidatetabledesignerdiv = false;


                            if (continueLoop == true) {


                                //$scope.CategoryNameValidation = true;
                                //$scope.FamilyNameValidation = true;
                                //$scope.CategoryNameValidationskipped = true;
                                //$scope.FamilyNameValidationskipped = true;
                                //$scope.missingtabledesignerColumnValidation = true;
                                //$scope.validationPassed = true;
                                //$scope.passvalidationimportenable = false;
                                //$scope.passvalidationNext = false;
                                //$scope.ProcessName = "View Log";
                                //$scope.validationCompleted = true;
                                ////$scope.importpicklistdiv = true;
                                //$scope.ResultDisplayfortabledesigner = false;
                                //$scope.AttributeMappingdiv = false;
                                //$scope.ValidateDiv = false;
                                //$scope.ValidatetableDesignerDiv = false;
                                //$scope.showValidatediv = false;
                                //$scope.showValidatetabledesignerdiv = true;
                                //$scope.validationSuccess = true;
                                //$scope.importErrorWindowfortabledesignerdiv = false;
                                //$scope.ProcessName = "Import Content";
                                //$("#import").addClass("leftMenuactive");
                                //$("#impvalidation").removeClass("leftMenuactive");

                                //$("#Atrmappingimp").removeClass("leftMenuactive");
                                //$("#choosefileimp").removeClass("leftMenuactive");
                                //$("#sheetselectimp").removeClass("leftMenuactive");

                                //$("#createTemplate").removeClass("leftMenuactive");

                                //$("#logim").removeClass("leftMenuactive");
                                $scope.loadImportSheetNames();
                            }
                            /*********Product input screen changes by Aswin *********///
                        }
                        else if ($scope.ResultDisplayfortabledesigner == false) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: $scope.ImportType + ' validation unsuccessful.',
                                type: "error"
                            });
                            $scope.ValidatetableDesignerDiv = true;
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: $scope.ImportType + ' validation unsuccessful.',
                                type: "error"
                            });
                            $scope.AttributeMappingdiv = false;
                            $scope.ValidateDiv = false;
                            $scope.showValidatetabledesignerdiv = true;
                            $scope.validationCompleted = false;
                            $scope.importErrorWindowfortabledesignerdiv = false;
                            $scope.showValidatediv = false;
                            $scope.ValidatetableDesignerDiv = false;
                            $scope.validationPassed = true;
                            $scope.validationSuccess = false;
                            $scope.ResultDisplayfortabledesigner = false;

                            // $scope.validatetResultDatasource.read();
                        }
                        //if (importresult.split("~", 1)[0].toLowerCase() == "validation failed") {
                        //    $.msgBox({
                        //        title: $localStorage.ProdcutTitle,
                        //        content: 'Validation failed. Please check log for details',
                        //        type: "error"
                        //    });
                        //    $scope.ProcessName = "Log Details";
                        //    $("#imptamp").removeClass("leftMenuactive");
                        //    $("#impvalidation").removeClass("leftMenuactive");

                        //    $("#Atrmappingimp").removeClass("leftMenuactive");
                        //    $("#choosefileimp").removeClass("leftMenuactive");
                        //    $("#sheetselectimp").removeClass("leftMenuactive");

                        //    $("#createTemplate").removeClass("leftMenuactive");
                        //    $("#logim").addClass("leftMenuactive");
                        //    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                        //    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                        //    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                        //    //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                        //    //$('#logim')[0].className = 'k-item';
                        //    //$('#imptamp')[0].className = 'k-item k-state-disabled';

                        //    $("#importSelectionTable").hide();
                        //    $scope.displayexportbutton = true;
                        //    $("#importErrorWindow1").show();

                        //    $scope.passvalidation = false;
                        //    $scope.validatetResultDatasource.read();
                        //    $scope.importErrorFileWindowdiv = true;
                        //    $scope.AttributeMappingdiv = false;
                        //    $scope.backbutton = true;
                        //    $scope.importErrorWindowdiv = false;
                        //    $("#importErrorWindow").hide();
                        //    $scope.importErrorWindowdiv = false;
                        //    $("#bulkimportErrorWindow").hide();
                        //    $scope.importSuccessWindowdiv = false;
                        //    $("#importSuccessWindow").hide();
                        //    $("#bulkimportSuccessWindow").hide();
                        //}
                        //else if (importresult.split("~", 1)[0].toLowerCase() == "validation passed") {
                        //    //$scope.passvalidation = true;
                        //    $scope.passvalidationimport = true;
                        //    $scope.passvalidation = false;

                        //    $.msgBox({
                        //        title: $localStorage.ProdcutTitle,
                        //        content: 'Validation passed.',
                        //        type: "error"
                        //    });
                        //}

                    }).error(function (error) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: $scope.ImportType + 'validation failed, please try again.',
                            type: "error"
                        });
                        $scope.validationPassed = true;
                        options.error(error);
                    });
                }
            }
        });

    };

    $scope.GoToSetupfortabledesigner = function () {
        $("#import").removeClass("leftMenuactive");
        $scope.BatchImport = false;
        $scope.ImportCompetedfortabledesigner();
        $scope.ResetPage();
        // $scope.GetBatchLogList();

    };
    $scope.ImportCompetedfortabledesigner = function () {
        $scope.validationPassed = false;
        $scope.ResultDisplay = false;
        $scope.showValidatediv = false;
        $scope.showValidatetabledesignerdiv = false;
        $scope.ResultDisplayfortabledesigner = false;
        $scope.listAllSheet = [];
        $scope.templateDetails = false;
        $scope.ResultDisplay = false;
        $scope.AttributeMappingdiv = false;
        $scope.ProcessName = "Validate Content";
        $scope.Validatetabledesignerback();


    };

    $scope.finishtabledesignerImport = function () {

        blockUI.stop();
        $scope.importCompletedStatus = true;
        $scope.progressComplete = false;
        $scope.scheduleImport = true;
        $scope.ItemImport = false;
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#Atrmappingimp").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");

        $("#createTemplate").removeClass("leftMenuactive");

        $("#imptamp").removeClass("leftMenuactive");

        $("#impvalidation").removeClass("leftMenuactive");
        $("#import").addClass("leftMenuactive");

        $('.imageloader').hide();

        var DateTime = new Date($('#txtDataTime').val());
        var hour = DateTime.getHours() == '0' ? '00' : DateTime.getHours().toString().length == 1 ? '0' + DateTime.getHours() : DateTime.getHours();
        var min = DateTime.getMinutes() == '0' ? '00' : DateTime.getMinutes().toString().length == 1 ? '0' + DateTime.getMinutes() : DateTime.getMinutes();
        var scheduleDate = DateTime.getDate() + "/" + (DateTime.getMonth() + 1) + "/" + DateTime.getFullYear() + " " + hour + ":" + min;
        $scope.getImportType = $scope.ImportType;
        //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
        //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
        //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
        //$('#impvalidation')[0].className = 'k-item';
        //$('#logim')[0].className = 'k-item k-state-disabled';
        //$('#imptamp')[0].className = 'k-item k-state-disabled';
        if ($scope.EnableBatch == false) {
            scheduleDate = "";
        }
        $scope.impimp = true;

        if ($scope.EnableBatch == true) {

            dataFactory.finishImport($rootScope.SelectedFileForUploadnamemain, $scope.importExcelSheetDDSelectionValue, $scope.allowDuplication, $scope.ImportType, $scope.ImportcatalogIdAttr, $scope.excelPath, $scope.EnableBatch, scheduleDate, $scope.templateId, $scope.prodImportData).success(function (importresult) {
                //    $scope.loadingimg.center().close();
                $scope.importSessionID = importresult.split("~", 2); // kendo data source ;
                if (importresult == "Batch in Queue") {
                    $scope.scheduleImport = false;
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Added to queue.Please check the Batch log.',
                        type: "info"
                    });

                    $scope.BatchImport = true;
                    $scope.scheduleImport = false;
                    return true;
                }
            });
            return true;
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Import may take some time to complete.",
                type: "confirm",
                buttons: [{ value: "Ok" },
                    { value: "Cancel" }
                ],
                success: function (result) {
                    if (result === "Ok") {
                        if ($scope.EnableBatch != true) {
                            $('#myModal').show();

                            var importProgress = $("#importCompleteness").kendoProgressBar({
                                type: "chunk",
                                chunkCount: 10,
                                min: 0,
                                max: 10,
                                value: 1
                            }).data("kendoProgressBar");
                            importProgress.value(1);
                            $("#completed").text("10%");
                            var modal = document.getElementById('myModal');

                            modal.style.display = "block";
                        }
                        /*********Product input screen changes by Aswin *********///
                        $scope.validationImportSheetName = $scope.validationimportSelectedvalue;

                        dataFactory.GetImportStatus($scope.excelPath, $scope.validationimportSelectedvalue).success(function (rowCount) {
                            $scope.GetImportStatus(1, rowCount);
                            if ($scope.listAllSheet.length > 0) {
                                var lastVal = $scope.listAllSheet[$scope.listAllSheet.length - 1];
                                if (lastVal != $scope.importSelectedvalue)
                                    $scope.listAllSheet.push($scope.importSelectedvalue);
                            }
                            else
                                $scope.listAllSheet.push($scope.importSelectedvalue);
                            var continueLoop = true;
                            var excelSheets = $scope.importExcelSheetSelction._data;
                            var nextValue = 0;
                            //if (excelSheets.length == 1) {
                            //    $scope.sheetCompleted = true;
                            //}
                            //else if (excelSheets.length >= 1 && $scope.listAllSheet.length < excelSheets.length) {

                            //    $scope.sheetCompleted = false;
                            //} else {
                            //    $scope.sheetCompleted = true;
                            //}
                            if (lastVal == "TableDesigner" || lastVal == "Products") {
                                $scope.sheetCompleted = true;
                            }


                            $scope.validationImportSheetName = $scope.validationimportSelectedvalue;


                            dataFactory.finishImport($rootScope.SelectedFileForUploadnamemain, $scope.validationimportSelectedvalue, $scope.allowDuplication, $scope.ImportType, $scope.ImportcatalogIdAttr, $scope.excelPath, $scope.EnableBatch, "", $scope.templateId, $scope.prodImportData).success(function (importresult) {
                                //    $scope.loadingimg.center().close();
                                $scope.scheduleImport = false;
                                if ($scope.EnableBatch != true) {
                                    var importProgress = $("#importCompleteness").kendoProgressBar({
                                        type: "chunk",
                                        chunkCount: 10,
                                        min: 0,
                                        max: 10,
                                        value: 1
                                    }).data("kendoProgressBar");
                                    importProgress.value(9);
                                    $("#completed").text("90 %");
                                }
                                $scope.progressComplete = true;

                                $scope.importSessionID = importresult.split("~", 2); // kendo data source ;
                                if (importresult == "Batch in Queue") {

                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'Added to queue.Please check the Batch log.',
                                        type: "info"
                                    });
                                    return true;
                                }

                                if (importresult.includes('ITEM#') && !importresult.includes('Import Failed')) {

                                    $scope.ProcessName = "View Log";
                                    $("#import").removeClass("leftMenuactive");
                                    $("#impvalidation").removeClass("leftMenuactive");
                                    $("#logim").addClass("leftMenuactive");
                                    $scope.importErrorWindowdiv = false;
                                    $scope.importSuccessWindowdiv = true;
                                    $scope.importErrorFileWindowdiv = false;
                                    $scope.showValidatediv = false;
                                    $scope.showValidatetabledesignerdiv = false;
                                    $("#importErrorWindow").hide();
                                    angular.forEach(importresult.split("~"), function (value) {
                                        if (value.includes('InsertRecords')) {
                                            var insertRecordCount = value.split(':')[1];
                                            $scope.importLog.InsertedRecords = insertRecordCount;
                                        }
                                        else if (value.includes('UpdateRecords')) {
                                            var updateRecordCount = value.split(':')[1];
                                            $scope.importLog.UpdatedRecords = updateRecordCount;
                                        }
                                        else if (value.includes('DeletedRecords')) {
                                            var deletedRecordCount = value.split(':')[1];
                                            $scope.importLog.DeletedRecords = deletedRecordCount;
                                        }
                                        else if (value.includes('TimeElapsed')) {
                                            var timeElapsed = value.split('-')[1];
                                            $scope.importLog.TimeTaken = timeElapsed;
                                        }
                                        else if (value.includes('SkippedRecords')) {
                                            var skippedRecords = value.split(':')[1];
                                            $scope.importLog.SkippedRecords = skippedRecords;
                                        }
                                    });
                                    $scope.importLog.CatalogName = $("#catalogDrpDownLisr").data("kendoDropDownList").text();
                                    $scope.nextSheetProcess = true;
                                    $scope.validationPassed = true;
                                    $scope.ResultDisplayfortabledesigner = true;
                                    $scope.AttributeMappingdiv = false;
                                    $("#importSuccessWindow").show();
                                    // $("#importSuccessWindow").hide();
                                    $("#bulkimportSuccessWindow").show();

                                    $scope.importSuccessResultDatasource.read();
                                    //$scope.importResultDatasource.read();
                                    return;
                                }
                                else if (importresult.includes('ITEMEMPTY')) {
                                    $scope.ItemImport = true;
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + importresult.split("~", 1) + '.',
                                        type: "info"
                                    });
                                    $("#choosefileimp").removeClass("leftMenuactive");
                                    $("#sheetselectimp").removeClass("leftMenuactive");
                                    $("#Atrmappingimp").removeClass("leftMenuactive");
                                    $("#logim").removeClass("leftMenuactive");

                                    $("#createTemplate").removeClass("leftMenuactive");
                                    $("#impvalidation").removeClass("leftMenuactive");

                                    $("#imptamp").removeClass("leftMenuactive");
                                    $("#logim").addClass("leftMenuactive");
                                    $scope.AttributeMappingdiv = false;
                                    $scope.ResultDisplay = true;

                                    $scope.showValidatediv = false;
                                    //$("#impvalidation").removeClass("leftMenuactive");
                                    //$("#logim").addClass("leftMenuactive");
                                    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                                    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                                    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                                    //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                                    //$('#logim')[0].className = 'k-item';
                                    //$('#imptamp')[0].className = 'k-item k-state-disabled';
                                    $scope.importErrorWindowdiv = true;
                                    $("#importErrorWindow").show();
                                    $("#bulkimportErrorWindow").show();
                                    $scope.importSuccessWindowdiv = false;

                                    $("#importSuccessWindow").hide();
                                    $("#bulkimportSuccessWindow").hide();
                                    $('#myModal').hide();

                                    $scope.importResultDatasource.read();
                                    return;
                                }
                                else if (importresult.includes('ITEM#') && importresult.includes('Import Failed')) {
                                    $scope.ItemImport = true;
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + importresult.split("~", 1) + '.',
                                        type: "info"
                                    });
                                    $("#choosefileimp").removeClass("leftMenuactive");
                                    $("#sheetselectimp").removeClass("leftMenuactive");
                                    $("#Atrmappingimp").removeClass("leftMenuactive");
                                    $("#logim").removeClass("leftMenuactive");

                                    $("#createTemplate").removeClass("leftMenuactive");
                                    $("#impvalidation").removeClass("leftMenuactive");

                                    $("#imptamp").removeClass("leftMenuactive");
                                    $("#logim").addClass("leftMenuactive");
                                    $scope.AttributeMappingdiv = false;
                                    $scope.ResultDisplay = true;

                                    $scope.showValidatediv = false;
                                    //$("#impvalidation").removeClass("leftMenuactive");
                                    //$("#logim").addClass("leftMenuactive");
                                    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                                    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                                    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                                    //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                                    //$('#logim')[0].className = 'k-item';
                                    //$('#imptamp')[0].className = 'k-item k-state-disabled';
                                    $scope.importErrorWindowdiv = true;
                                    $("#importErrorWindow").show();
                                    $("#bulkimportErrorWindow").show();
                                    $scope.importSuccessWindowdiv = false;

                                    $("#importSuccessWindow").hide();
                                    $("#bulkimportSuccessWindow").hide();
                                    $('#myModal').hide();

                                    $scope.importResultDatasource.read();
                                    return;
                                }
                                if (importresult.split("~", 1) == "Import Failed") {

                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + importresult.split("~", 1) + '.',
                                        type: "info"
                                    });
                                    $("#choosefileimp").removeClass("leftMenuactive");
                                    $("#sheetselectimp").removeClass("leftMenuactive");
                                    $("#Atrmappingimp").removeClass("leftMenuactive");
                                    $("#logim").removeClass("leftMenuactive");

                                    $("#createTemplate").removeClass("leftMenuactive");
                                    $("#impvalidation").removeClass("leftMenuactive");

                                    $("#imptamp").removeClass("leftMenuactive");
                                    $("#logim").addClass("leftMenuactive");
                                    $scope.AttributeMappingdiv = false;
                                    $scope.ResultDisplay = true;

                                    $scope.showValidatediv = false;
                                    //$("#impvalidation").removeClass("leftMenuactive");
                                    //$("#logim").addClass("leftMenuactive");
                                    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                                    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                                    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                                    //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                                    //$('#logim')[0].className = 'k-item';
                                    //$('#imptamp')[0].className = 'k-item k-state-disabled';
                                    $scope.importErrorWindowdiv = true;
                                    $("#importErrorWindow").show();
                                    $("#bulkimportErrorWindow").show();
                                    $scope.importSuccessWindowdiv = false;

                                    $("#importSuccessWindow").hide();
                                    $("#bulkimportSuccessWindow").hide();
                                    $('#myModal').hide();

                                    $scope.importResultDatasource.read();


                                } else if (importresult.split("~", 1) == "SKU Exceed") {

                                    $scope.ResultDisplay = true;
                                    $scope.AttributeMappingdiv = false;
                                    $scope.importErrorWindowdiv = false;
                                    $scope.showValidatediv = false;
                                    $("#importErrorWindow").hide();
                                    $scope.importSuccessWindowdiv = false;
                                    $("#importSuccessWindow").hide();
                                    $("#importSuccessWindow").hide();
                                    $("#bulkimportSuccessWindow").hide();
                                    $scope.importResultDatasource.read();

                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'You have exceeded the maximum No. of SKUs as per your plan.',
                                        type: "error"
                                    });
                                    $("#choosefileimp").removeClass("leftMenuactive");
                                    $("#sheetselectimp").removeClass("leftMenuactive");
                                    $("#Atrmappingimp").removeClass("leftMenuactive");
                                    $("#logim").removeClass("leftMenuactive");

                                    $("#createTemplate").removeClass("leftMenuactive");
                                    $("#impvalidation").removeClass("leftMenuactive");

                                    $("#imptamp").removeClass("leftMenuactive");
                                    $("#logim").addClass("leftMenuactive");


                                    //$("#impvalidation").removeClass("leftMenuactive");
                                    //$("#logim").addClass("leftMenuactive");
                                    //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                                    //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                                    //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                                    //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                                    //$('#logim')[0].className = 'k-item';
                                    //$('#imptamp')[0].className = 'k-item k-state-disabled';
                                    $('#myModal').hide();
                                } else {

                                    //$.msgBox({
                                    //    title: $localStorage.ProdcutTitle,
                                    //    content: $scope.ImportType + ' Import successful',
                                    //    //content: ''+importresult.split("~", 1)+'',
                                    //    type: "info"
                                    //});

                                    $scope.ImportSheetName = $scope.validationimportSelectedvalue;


                                    if ($scope.importSelectionSheet.length == 0) {
                                        $scope.importSelectionSheet.push($scope.ImportSheetName);

                                    }
                                    //else
                                    //{
                                    //    $scope.importSelectionSheet.push($scope.ImportSheetName);
                                    //}
                                    $scope.importStatusFlag = "1";
                                    $scope.UpdateImportFlagFunctionCall();


                                    // ///Loop the excel sheet import types ///////////////////////////////

                                    var excelSheets = $scope.importSheetNames;

                                    var continueLoop = true;



                                    $scope.viewLogImportType = $scope.ImportType;

                                    //Import success result Import type name changes

                                    //if ($scope.viewLogImportType == "Products") {
                                    //    $scope.viewLogImportType = "Items"
                                    //}

                                    //if ($scope.viewLogImportType == "Families") {
                                    //    $scope.viewLogImportType = "Products"
                                    //}

                                    //if ($scope.viewLogImportType == "Familyproducts") {
                                    //    $scope.viewLogImportType = "ProductItems"
                                    //}
                                    if ($scope.ImportSheetName == "Products") {
                                        $scope.viewLogImportType = "Products"
                                    }
                                    else if ($scope.ImportSheetName == "Items") {
                                        $scope.viewLogImportType = "Items"
                                    }

                                    else if ($scope.ImportSheetName == "ProductItems") {
                                        $scope.viewLogImportType = "ProductItems"
                                    }

                                    angular.forEach(excelSheets, function (sheet) {
                                        if (continueLoop) {
                                            if (sheet.IMPORT_TYPE.trim().toUpperCase() == "CATEGORY" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.ImportType = "Categories";
                                                $scope.validateimportTypeSelection = "Category";
                                                $scope.importSheetType = "Categories";
                                                $scope.importSelectedvalue = "Categories";
                                                $scope.importSheetSelected = "Categories";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importExcelSheetDDSelectionValue = "Categories";
                                                $scope.validateimportTypeSelection = "Category";
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;


                                            }
                                            else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "PRODUCT" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.ImportType = "Families";
                                                $scope.validateimportTypeSelection = "Product";
                                                $scope.importSheetType = "Products";
                                                $scope.importSelectedvalue = "Products";
                                                $scope.importSheetSelected = "Families";
                                                $scope.importExcelSheetDDSelectionValue = "Families";
                                                $scope.validateimportTypeSelection = "Product";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;
                                            }
                                            else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "ITEM" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.ImportType = "Products";
                                                $scope.validateimportTypeSelection = "Item";
                                                $scope.importSheetType = "Items";
                                                $scope.importSelectedvalue = "Items";
                                                $scope.importSheetSelected = "Products";
                                                $scope.importExcelSheetDDSelectionValue = "Products";
                                                $scope.validateimportTypeSelection = "Item";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;
                                            }

                                            else if (sheet.IMPORT_TYPE.trim().toUpperCase() == "TABLEDESIGNER" && (!$scope.importSelectionSheet.includes(sheet.SHEET_NAME))) {
                                                $scope.importSelectedvalue = "TableDesigner";
                                                $scope.ImportType = "TableDesigner";
                                                $scope.importSheetSelected = "TableDesigner";
                                                $scope.importSheetType = "TableDesigner";
                                                $scope.importExcelSheetDDSelectionValue = "TableDesigner";
                                                $scope.validateimportTypeSelection = "TableDesigner";
                                                $scope.validationimportSelectedvalue = sheet.SHEET_NAME;
                                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                                                continueLoop = false;
                                            }

                                        }
                                    });

                                    // ///Loop the excel sheet import types ///////////////////////////////


                                    if (continueLoop == false && $scope.importSheetType != "TableDesigner") {
                                        $scope.passvalidationimportenable = false;
                                        $scope.passvalidationNext = false;
                                        $scope.ProcessName = "View Log";
                                        $scope.validationCompleted = true;
                                        //$scope.importpicklistdiv = true;
                                        $scope.ResultDisplay = false;
                                        $scope.AttributeMappingdiv = false;
                                        $scope.ValidateDiv = false;
                                        $scope.showValidatediv = true;
                                        $scope.validationSuccess = true;
                                        $scope.importErrorFileWindowdiv = false;
                                        $scope.ProcessName = "Import Content";
                                        $("#import").addClass("leftMenuactive");
                                        $("#impvalidation").removeClass("leftMenuactive");

                                        $("#Atrmappingimp").removeClass("leftMenuactive");
                                        $("#choosefileimp").removeClass("leftMenuactive");
                                        $("#sheetselectimp").removeClass("leftMenuactive");

                                        $("#createTemplate").removeClass("leftMenuactive");

                                        $("#logim").removeClass("leftMenuactive");
                                        $scope.showValidationforNextSheet = true;
                                        $scope.ResultDisplayfortabledesigner = false;
                                        $scope.AttributeMappingdiv = false;
                                        $scope.showValidatetabledesignerdiv = false;
                                        $scope.tabledesignerhide = false;
                                        $scope.tabledesignershow = false;

                                        $scope.tbldesigner = false;

                                        angular.forEach(importresult.split("~"), function (value) {
                                            if (value.includes('InsertRecords')) {
                                                var insertRecordCount = value.split(':')[1];
                                                $scope.importLog.InsertedRecords = insertRecordCount;

                                            }
                                            else if (value.includes('UpdateRecords')) {
                                                var updateRecordCount = value.split(':')[1];
                                                $scope.importLog.UpdatedRecords = updateRecordCount;
                                                // $scope.importAllResults.push({ "UpdatedRecords": $scope.importLog.UpdatedRecords });
                                            }
                                            else if (value.includes('DeletedRecords')) {
                                                var deletedRecordCount = value.split(':')[1];
                                                $scope.importLog.DeletedRecords = deletedRecordCount;
                                                //$scope.importAllResults.push({ "DeletedRecords": $scope.importLog.DeletedRecords });
                                            }
                                            else if (value.includes('TimeElapsed')) {
                                                var timeElapsed = value.split('-')[1];
                                                $scope.importLog.TimeTaken = timeElapsed;
                                                // $scope.importAllResults.push({ "TimeTaken": $scope.importLog.TimeTaken });
                                            }
                                            else if (value.includes('SkippedRecords')) {
                                                var skippedRecords = value.split(':')[1];
                                                $scope.importLog.SkippedRecords = skippedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }
                                            else if (value.includes('DetachedRecords')) {
                                                var DetachedRecords = value.split(':')[1];
                                                $scope.importLog.DetachedRecords = DetachedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }
                                        })
                                        $scope.importLog.CatalogName = $("#catalogDrpDownLisr").data("kendoDropDownList").text();
                                        // $scope.importAllResults.push({ "CatalogName": $scope.importLog.CatalogName });
                                        /// $scope.importAllResults.push({ "importSheetType": $scope.importSheetType });



                                        $scope.importAllResults.push({
                                            "InsertRecords": $scope.importLog.InsertedRecords, "UpdatedRecords": $scope.importLog.UpdatedRecords, "DeletedRecords": $scope.importLog.DeletedRecords
                                       , "DetachedRecords": $scope.importLog.DetachedRecords, "TimeTaken": $scope.importLog.TimeTaken, "SkippedRecords": $scope.importLog.SkippedRecords, "CatalogName": $scope.importLog.CatalogName,
                                            "importSheetType": $scope.viewLogImportType, "SheetName": $scope.ImportSheetName
                                        });
                                        $scope.importSuccessResultDatasource.read();
                                    }



                                    if (continueLoop == true) {



                                        $scope.ProcessName = "View Log";
                                        $("#import").removeClass("leftMenuactive");
                                        $("#impvalidation").removeClass("leftMenuactive");
                                        $("#logim").addClass("leftMenuactive");
                                        //$('#choosefileimp')[0].className = 'k-item k-state-disabled';
                                        //$('#sheetselectimp')[0].className = 'k-item k-state-disabled';
                                        //$('#Atrmappingimp')[0].className = 'k-item k-state-disabled';
                                        //$('#impvalidation')[0].className = 'k-item k-state-disabled';
                                        //$('#logim')[0].className = 'k-item';
                                        //$('#imptamp')[0].className = 'k-item k-state-disabled';
                                        $scope.importErrorWindowdiv = false;
                                        $scope.importSuccessWindowdiv = true;
                                        $scope.importErrorFileWindowdiv = false;
                                        $scope.showValidatediv = false;

                                        $("#importErrorWindow").hide();

                                        angular.forEach(importresult.split("~"), function (value) {
                                            if (value.includes('InsertRecords')) {
                                                var insertRecordCount = value.split(':')[1];
                                                $scope.importLog.InsertedRecords = insertRecordCount;

                                            }
                                            else if (value.includes('UpdateRecords')) {
                                                var updateRecordCount = value.split(':')[1];
                                                $scope.importLog.UpdatedRecords = updateRecordCount;
                                                // $scope.importAllResults.push({ "UpdatedRecords": $scope.importLog.UpdatedRecords });
                                            }
                                            else if (value.includes('DeletedRecords')) {
                                                var deletedRecordCount = value.split(':')[1];
                                                $scope.importLog.DeletedRecords = deletedRecordCount;
                                                //$scope.importAllResults.push({ "DeletedRecords": $scope.importLog.DeletedRecords });
                                            }
                                            else if (value.includes('TimeElapsed')) {
                                                var timeElapsed = value.split('-')[1];
                                                $scope.importLog.TimeTaken = timeElapsed;
                                                // $scope.importAllResults.push({ "TimeTaken": $scope.importLog.TimeTaken });
                                            }
                                            else if (value.includes('SkippedRecords')) {
                                                var skippedRecords = value.split(':')[1];
                                                $scope.importLog.SkippedRecords = skippedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }
                                            else if (value.includes('DetachedRecords')) {
                                                var DetachedRecords = value.split(':')[1];
                                                $scope.importLog.DetachedRecords = DetachedRecords;
                                                // $scope.importAllResults.push({ "SkippedRecords": $scope.importLog.SkippedRecords });
                                            }
                                        })
                                        $scope.importLog.CatalogName = $("#catalogDrpDownLisr").data("kendoDropDownList").text();
                                        // $scope.importAllResults.push({ "CatalogName": $scope.importLog.CatalogName });
                                        /// $scope.importAllResults.push({ "importSheetType": $scope.importSheetType });



                                        $scope.importAllResults.push({
                                            "InsertRecords": $scope.importLog.InsertedRecords, "UpdatedRecords": $scope.importLog.UpdatedRecords, "DeletedRecords": $scope.importLog.DeletedRecords
                                        , "DetachedRecords": $scope.importLog.DetachedRecords, "TimeTaken": $scope.importLog.TimeTaken, "SkippedRecords": $scope.importLog.SkippedRecords, "CatalogName": $scope.importLog.CatalogName,
                                            "importSheetType": $scope.viewLogImportType, "SheetName": $scope.ImportSheetName
                                        });
                                        //if (importresult.split("~", 3) >=2) {
                                        //    $scope.nextSheetProcess = true;
                                        //}
                                        //angular.forEach(importresult.split("~"), function (value) {
                                        //    if (value.includes('InsertRecords')) {
                                        //        var insertRecordCount = value.split(':')[1];
                                        //        $scope.importLog.InsertedRecords = insertRecordCount;
                                        //    }
                                        //    else if (value.includes('UpdateRecords')) {
                                        //        var updateRecordCount = value.split(':')[1];
                                        //        $scope.importLog.UpdatedRecords = updateRecordCount;
                                        //    }
                                        //    else if (value.includes('DeletedRecords')) {
                                        //        var deletedRecordCount = value.split(':')[1];
                                        //        $scope.importLog.DeletedRecords = deletedRecordCount;
                                        //    }
                                        //    else if (value.includes('TimeElapsed')) {
                                        //        var timeElapsed = value.split('-')[1];
                                        //        $scope.importLog.TimeTaken = timeElapsed;
                                        //    }
                                        //    else if (value.includes('SkippedRecords')) {
                                        //        var skippedRecords = value.split(':')[1];
                                        //        $scope.importLog.SkippedRecords = skippedRecords;
                                        //    }
                                        //})

                                        // $scope.importLog.CatalogName = $("#catalogDrpDownLisr").data("kendoDropDownList").text();
                                        $scope.nextSheetProcess = true;
                                        $scope.validationPassed = true;
                                        $scope.ResultDisplayfortabledesigner = true;
                                        $scope.AttributeMappingdiv = false;
                                        $scope.showValidatetabledesignerdiv = false;
                                        $scope.tabledesignerhide = true;
                                        $scope.tabledesignershow = true;
                                        $("#importSuccessWindow").show();
                                        // $("#importSuccessWindow").hide();
                                        $("#bulkimportSuccessWindow").show();

                                        $scope.importSuccessResultDatasource.read();
                                        $('.imageloader').show();
                                        /*********Product input screen changes by Aswin *********///
                                    }
                                }
                                //$http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
                            }).error(function (error) {
                                // $scope.loadingimg.center().close();         
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Import failed, please try again.',
                                    type: "error"
                                });
                                // $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
                                options.error(error);
                            });
                        });
                    }

                    else {
                        $('.imageloader').show();
                        return false;
                    }
                }

            });
        }


        //  }
        //  $scope.loadingimg.refresh({ url: "../views/app/partials/ImgLoading.html" });
        // $scope.loadingimg.center().open();
        // $http.post("/Import/finishImport?SheetName=" + $scope.importExcelSheetDDSelectionValue + "&allowDuplicate=" + $scope.allowDuplication + "&excelPath=" + $scope.excelPath)
        // .success(function (importresult) {

        //     $scope.loadingimg.center().close();
        //     alert(importresult.split("~", 1));
        //     $scope.importSessionID = importresult.split("~", 2);   // kendo data source ;
        //     if (importresult.split("~", 1) == "Import Failed") {
        //         $("#importErrorWindow").show();
        //         $("#importSuccessWindow").hide();
        //         $scope.importResultDatasource.read();

        //         //alert("issue");
        //     }
        //     else {
        //         //alert("success");
        //         $("#importErrorWindow").hide();
        //         $("#importSuccessWindow").show();
        //         $scope.importSuccessResultDatasource.read();
        //     }

        //     $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        // })
        //.error(function () {
        //    $scope.loadingimg.center().close();
        //    alert("Import Failed. Please Try Again.");
        //    $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);


        //});
    };


    /*********Product input screen changes by Aswin *********///

    //$scope.importSheetNames = [];

    //Update validation flag  and Mapping flag based on import types //////////

    $scope.updateValidationImportTypeFlag = function () {
        dataFactory.updateImportValidationFlag($scope.selectedTemplate, $scope.validateimportTypeSelection, $scope.validationStatus, $scope.validationImportSheetName).success(function (response) {

            if (response != null) {
                $scope.importSheetNames = "";
                $scope.importSheetNames = response.Data;

                //$scope.loadImportSheetNames();

            }


        }).error(function (error) {
            options.error(error);
        });

    }
    $scope.importFlag = "0";

    $scope.loadImportSheetNames = function () {
        if ($scope.validationSelectionSheet.length == $scope.validationSheetname.length) {
            //$("#id_validationSheetnames").data("kendoDropDownList").setOptions({ dataTextField: "SHEET_NAME", dataValueField: "SHEET_NAME" });
            //$("#id_validationSheetnames").data("kendoDropDownList").setDataSource($scope.importSheetNames);
            //var ddlData = $("#id_validationSheetnames").data("kendoDropDownList");
            //ddlData.setDataSource($scope.importSheetNames);
            //ddlData.dataSource.read();
            //$scope.validationimportSelectedvalue = $scope.importSheetNames[0].SHEET_NAME;
            //$scope.importSheetSelected = $scope.importSheetNames[0].SHEET_NAME;
            //$scope.validateimportTypeSelection = $scope.importSheetNames[0].IMPORT_TYPE.trim();

            //// $scope.importExcelSheetDDSelectionValue = $scope.validationimportSelectedvalue;
            //$scope.getimportTypechange($scope.validateimportTypeSelection);
            $scope.importFlag = "1";
            $scope.validationSheetnames.read();
            if ($scope.validateimportTypeSelection != "TableDesigner") {
                $scope.passvalidationimportenable = false;
                $scope.passvalidationNext = false;
                $scope.ProcessName = "View Log";
                $scope.validationCompleted = true;
                //$scope.importpicklistdiv = true;
                $scope.ResultDisplay = false;
                $scope.AttributeMappingdiv = false;
                $scope.ValidateDiv = false;
                $scope.showValidatediv = true;
                $scope.validationSuccess = true;
                $scope.importErrorFileWindowdiv = false;
                $scope.ProcessName = "Import Content";
                $("#import").addClass("leftMenuactive");
                $("#impvalidation").removeClass("leftMenuactive");

                $("#Atrmappingimp").removeClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");

                $("#createTemplate").removeClass("leftMenuactive");

                $("#logim").removeClass("leftMenuactive");
                $scope.showValidationforNextSheet = true;


                $scope.ValidatetableDesignerDiv = false;
                $scope.tbldesigner = false;


            }
            else {

                $scope.validationPassed = true;
                $scope.passvalidationimportenable = false;
                $scope.passvalidationNext = false;
                $scope.ProcessName = "View Log";
                $scope.validationCompleted = true;
                //$scope.importpicklistdiv = true;
                $scope.ResultDisplay = false;
                $scope.AttributeMappingdiv = false;
                $scope.ValidateDiv = false;
                $scope.showValidatediv = true;
                $scope.validationSuccess = true;
                $scope.importErrorFileWindowdiv = false;
                $scope.ProcessName = "Import Content";
                $("#import").addClass("leftMenuactive");
                $("#impvalidation").removeClass("leftMenuactive");

                $("#Atrmappingimp").removeClass("leftMenuactive");
                $("#choosefileimp").removeClass("leftMenuactive");
                $("#sheetselectimp").removeClass("leftMenuactive");

                $("#createTemplate").removeClass("leftMenuactive");

                $("#logim").removeClass("leftMenuactive");
                $scope.showValidationforNextSheet = true;
            }
        }
    }


    $scope.updateMappingImportTypeFlag = function () {
        dataFactory.updateImportMappingFlag($scope.selectedTemplate, $scope.importTypeSelection, $scope.importExcelSheetDDSelectionValue).success(function (response) {

            if (response != null && response != "" && response != undefined) {
                if (!($scope.mappingValidation.includes("Categories"))) {
                    $scope.mappingValidation.push(response);
                }
                else if (!($scope.mappingValidation.includes("Families"))) {
                    $scope.mappingValidation.push(response);
                }
                else if (!($scope.mappingValidation.includes("Products"))) {
                    $scope.mappingValidation.push(response);
                }
            }

        }).error(function (error) {
            options.error(error);
        });

    }


    //Update validation flag  and Mapping flag based on import types //////////


    ////Save All Mapping Attributes///////////


    $scope.saveAllMAppingAttributes = function () {

        //for (var j = 0; j < $scope.prodImportData.length; j++) {
        //    if ($scope.prodImportData[j]["FieldType"] != "" && $scope.prodImportData[j]["FieldType"] != "0" && $scope.prodImportData[j]["SelectedToImport"] == "True")

        //        $scope.checkMappingAttributenames = $scope.mappingAttributes.find(ob => ob["captionName"] === $scope.prodImportData[j]["ExcelColumn"]);

        //       if ($scope.checkMappingAttributenames==undefined) {


        //            if ($scope.prodImportData[j]["AttrMapping"] != "") {
        //                $scope.mapppedAttributeName = $scope.prodImportData[j]["AttrMapping"];

        //                $scope.mappingAttributes.push({
        //                    captionName: $scope.prodImportData[j]["ExcelColumn"], attributeName: $scope.mapppedAttributeName
        //                });
        //            }

        //            else if ($scope.prodImportData[j]["MapAttributes"] != "") {
        //                $scope.mapppedAttributeName = $scope.prodImportData[j]["MapAttributes"];
        //                $scope.mappingAttributes.push({
        //                    captionName: $scope.prodImportData[j]["ExcelColumn"], attributeName: $scope.mapppedAttributeName
        //                });
        //            }
        //            else {
        //                $scope.mapppedAttributeName = $scope.prodImportData[j]["AttributeNames"];
        //                $scope.mappingAttributes.push({
        //                    captionName: $scope.prodImportData[j]["ExcelColumn"], attributeName: $scope.mapppedAttributeName
        //                });
        //            }
        //        }
        //}
        dataFactory.saveAllMappingAttributes($scope.templateId, $scope.importTypeSelection, $scope.importExcelSheetDDSelectionValue, $scope.excelPath, $scope.mappingAttributes).success(function (response) {
            if (response != null) {
                $scope.mappingAttributes = [];
                $scope.validationSheetnames.read();
            }

        }).error(function (error) {
            options.error(error);
        });
    }

    //*** Import type based on sheet column name Eg: Category_name,Family_name,Item#  *****//

    $scope.getImportTypeBasedonSheet = function () {
        dataFactory.excelImportType($scope.excelPath, $scope.importExcelSheetDDSelectionValue).success(function (response) {

            if (response != null) {
                $scope.checkColumnsArray = [];

                $scope.checkColumnsArray = response.Data;





                $scope.checkProduct = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === $localStorage.CatalogItemNumber || ob["ExcelColumn"] === "ITEM#" || ob["ExcelColumn"] === "Item#");

                $scope.checkFamily = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === "FAMILY_NAME");


                $scope.checkCategory = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === "CATEGORY_NAME");

                $scope.checkTabledesigner = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === "FAMILY_TABLE_STRUCTURE");

                if ($scope.checkProduct != undefined) {
                    $scope.importTypeSelection = "Item";
                    $scope.importSheetType = "Items";
                    //Change_2
                    $scope.importSelectedvalue = $scope.importExcelSheetDDSelectionValue;
                    if ($scope.importExcelSheetDDSelectionValue == "ProductItems") {
                        $scope.ImportType = "Familyproducts";
                    }
                    else {
                        $scope.ImportType = "Products";
                        $scope.importTypeSelection = "Product";
                    }

                    $scope.importGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath, 0);
                }
                else if ($scope.checkTabledesigner != undefined) {
                    $scope.importTypeSelection = "TableDesigner";
                    $scope.importSheetType = "TableDesigner";
                    $scope.importSelectedvalue = $scope.importExcelSheetDDSelectionValue;
                    $scope.ImportType = "TableDesigner";
                    $scope.attrMappingEnabled = false;
                    $scope.attrMap = false;
                }
                else if ($scope.checkFamily != undefined) {
                    $scope.importTypeSelection = "Product";
                    $scope.importSheetType = "Products";
                    $scope.importSelectedvalue = $scope.importExcelSheetDDSelectionValue;
                    $scope.ImportType = "Families";
                    $scope.importGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath, 0);
                }

                else if ($scope.checkCategory != undefined) {
                    $scope.importTypeSelection = "Category";
                    $scope.importSheetType = "Categories";
                    $scope.importSelectedvalue = $scope.importExcelSheetDDSelectionValue;
                    $scope.ImportType = "Categories";
                    $scope.importGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath, 0);
                }


                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Invalid Sheet Chosen.',
                        type: "error"
                    });
                    $scope.attrMappingEnabled = false;

                }

            }



        }).error(function (error) {
            options.error(error);
        });

    }


    //*************************Get validation sheet names from the datatabse  *******************************************///

    $scope.validationSheetnames = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {


                if ($scope.selectedTemplate == undefined || $scope.selectedTemplate == "") {
                    $scope.selectedTemplate = 0;
                }
                else {
                    $scope.selectedTemplate = $scope.templateId;
                }

                if ($scope.importFlag == "1") {
                    dataFactory.updateImportValidationFlag($scope.selectedTemplate, $scope.validateimportTypeSelection, $scope.validationStatus, $scope.validationImportSheetName).success(function (response) {
                        options.success(response.Data);
                        if (response.Data.length > 0) {
                            $scope.validationSheetname = response.Data;
                            $scope.validationimportSelectedvalue = response.Data[0].SHEET_NAME;
                            $scope.importSheetSelected = response.Data[0].SHEET_NAME;
                            $scope.validateimportTypeSelection = response.Data[0].IMPORT_TYPE.trim();
                            $scope.importFlag = "0";

                            if ($scope.importSelectionSheet.length == 0) {
                                $scope.importSelectionSheet.push($scope.validationimportSelectedvalue);
                            }
                            // $scope.importExcelSheetDDSelectionValue = $scope.validationimportSelectedvalue;
                            $scope.getimportTypechange($scope.validateimportTypeSelection);
                        }


                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {


                    dataFactory.validationSheetnames($scope.selectedTemplate).success(function (response) {
                        options.success(response.Data);
                        if (response.Data.length > 0) {
                            $scope.validationSheetname = response.Data;
                            $scope.validationimportSelectedvalue = response.Data[0].SHEET_NAME;
                            $scope.importSheetSelected = response.Data[0].SHEET_NAME;
                            $scope.validateimportTypeSelection = response.Data[0].IMPORT_TYPE.trim();
                            if ($scope.validationSelectionSheet.length == 0) {
                                $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                            }
                            // $scope.importExcelSheetDDSelectionValue = $scope.validationimportSelectedvalue;
                            $scope.getimportTypechange($scope.validateimportTypeSelection);
                        }






                    }).error(function (error) {
                        options.error(error);
                        blockUI.stop();
                    });


                }
            }
        }
    });

    //****Change validation sheet names to get import type  **************************//

    $scope.changevalidationSheetnames = function (e) {

        debugger;
        $scope.sheetName = e.sender.value();

        //if ($scope.sheetName == "Products") {
        //    $scope.sheetName = "Families"
        //}
        //if ($scope.sheetName == "Items") {
        //    $scope.sheetName = "Products"
        //}

        if ($scope.validationSelectionSheet.length > 0 && (!$scope.validationSelectionSheet.includes($scope.sheetName))) {
            $scope.validationSelectionSheet.push($scope.sheetName);
        }

        if ($scope.importSelectionSheet.length > 0 && (!$scope.importSelectionSheet.includes($scope.sheetName))) {
            $scope.importSelectionSheet.push($scope.sheetName);

        }

        dataFactory.excelImportType($scope.excelPath, $scope.sheetName).success(function (response) {

            if (response != null) {
                $scope.checkColumnsArray = [];

                $scope.checkColumnsArray = response.Data;

                $scope.checkProduct = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === $localStorage.CatalogItemNumber || ob["ExcelColumn"] === "ITEM#");

                $scope.checkFamily = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === "FAMILY_NAME");


                $scope.checkCategory = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === "CATEGORY_NAME");

                $scope.checkTabledesigner = $scope.checkColumnsArray.find(ob => ob["ExcelColumn"] === "FAMILY_TABLE_STRUCTURE");

                if ($scope.checkProduct != undefined) {
                    $scope.validateimportTypeSelection = "Item";
                    $scope.validationimportSelectedvalue = $scope.sheetName;
                    $scope.ImportType = "Products";

                }
                else if ($scope.checkTabledesigner != undefined) {
                    $scope.validateimportTypeSelection = "TableDesigner";
                    $scope.validationimportSelectedvalue = $scope.sheetName;
                    $scope.ImportType = "TableDesigner";

                }
                else if ($scope.checkFamily != undefined) {
                    $scope.validateimportTypeSelection = "Product";
                    $scope.validationimportSelectedvalue = $scope.sheetName;
                    $scope.ImportType = "Families";
                }

                else if ($scope.checkCategory != undefined) {
                    $scope.validateimportTypeSelection = "Category";
                    $scope.validationimportSelectedvalue = $scope.sheetName;
                    $scope.ImportType = "Categories";
                }

                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Invalid Sheet Chosen.',
                        type: "error"
                    });


                }

            }



        }).error(function (error) {
            options.error(error);
        });



    }

    ////**** Get validation import type based on sheet names *********/////

    $scope.validationgetimportTypechange = function (getImportSelectionType) {
        if (getImportSelectionType.toUpperCase().includes('SUBPRODUCT')) {

            $scope.validateimportTypeSelection = "SubProduct";
        } else if (getImportSelectionType.toUpperCase().includes('PRO')) {

            $scope.validateimportTypeSelection = "Item";

        } else if (getImportSelectionType.toUpperCase().includes('FAMIL')) {

            $scope.validateimportTypeSelection = "Product";
        }
        else if (getImportSelectionType.toUpperCase().includes('CATEG')) {

            $scope.validateimportTypeSelection = "Category";
        } else if (getImportSelectionType == undefined || getImportSelectionType == "") {

            $scope.validateimportTypeSelection = "Items";
        } else if (getImportSelectionType.toUpperCase().includes('TABLEDESIGNER')) {

            $scope.validateimportTypeSelection = "TableDesigner";
        }




    }


    ////*** Get import type change values ***************////

    $scope.getimportTypechange = function (getImportSelectionType) {
        if (getImportSelectionType.toUpperCase().includes('SUBPRODUCT')) {

            $scope.ImportType = "SubProducts";
        } else if (getImportSelectionType.toUpperCase().includes('PRO')) {

            $scope.ImportType = "Products";

        } else if (getImportSelectionType.toUpperCase().includes('FAMIL')) {

            $scope.ImportType = "Families";
        }
        else if (getImportSelectionType.toUpperCase().includes('CATEG')) {

            $scope.ImportType = "Categories";
        } else if (getImportSelectionType == undefined || getImportSelectionType == "") {

            $scope.ImportType = "Products";
        } else if (getImportSelectionType.toUpperCase().includes('TABLEDESIGNER') && $scope.showValidatetabledesignerdiv == false) {

            $scope.ImportType = "TableDesigner";
            $scope.ValidateDiv = false;
            $scope.ValidatetableDesignerDiv = true;
            $scope.tbldesigner = true;
        }
    }

    /////***********************Update import flag when import gets success  ****************************/

    $scope.UpdateImportFlagFunctionCall = function () {
        dataFactory.UpdateImportFlag($scope.selectedTemplate, $scope.validateimportTypeSelection, $scope.importStatusFlag, $scope.ImportSheetName).success(function (response) {



        }).error(function (error) {
            options.error(error);
        });

    }

    ///***************************  Load Mapping and Import sheet names ***************************////
    $scope.loadMappingSheetNames = function () {
        ///****** Check selected template ID is greater than 0 or not *************/////
        if ($scope.selectedTemplate > 0) {

            dataFactory.getMappingSheetNames($scope.selectedTemplate).success(function (response) {

                if (response.Data.length > 0) {
                    $scope.getImportMappingCountDetails = response.Data;
                    $scope.excelPath = "/Content/ImportTemplate/" + $scope.fileName;
                    $scope.loadExcelSheetNames();

                }

            }).error(function (error) {
                options.error(error);
                blockUI.stop();
            });
        }
    }

    /////****** Load excel file sheet names *******************/

    $scope.loadExcelSheetNames = function () {
        dataFactory.importExcelSheetSelection($scope.excelPath).success(function (response) {


            if (response != null && response.length != 0 && $scope.selectedTemplate > 0) {

                $scope.existingSheetNames = response;

                if ($scope.getImportMappingCountDetails.length > 0) {

                    if ($scope.getImportMappingCountDetails[0].length > 0 && response.length == $scope.getImportMappingCountDetails[0].length) {
                        $scope.showImportMessage = true;
                        $scope.IsFormSubmitted = false;
                    }
                    else if ($scope.getImportMappingCountDetails[1].length > 0) {
                        $scope.dataExists = response.find(ob => ob["DATA_EXISTS"] === true);
                        // $scope.dataExists= Object.keys($scope.dataExists);
                        if ($scope.dataExists != undefined) {
                            $scope.IsFormSubmitted = true;
                        }
                        else {

                            $scope.showMappingMessage = true;
                            $scope.IsFormSubmitted = false;
                        }
                    }
                }


            }

        }).error(function (error) {
            options.error(error);
            blockUI.stop();
        });
    }

    $scope.fun_ImportMissingMappingAttributeResult = function () {
        dataFactory.getImportMissingMappingAttributeResult().success(function (response) {

            if (response != null) {

                $scope.attributeNames = response;
            }

        }).error(function (error) {
            options.error(error);
            blockUI.stop();
        });
    }
    /*********Product input screen changes by Aswin *********///


    //////********************* Group Attribute mapping changes  *************************///////////////////

    $scope.AttributeGroupDataSource = new kendo.data.DataSource({
        type: "json",
        sort: {
            field: "SORT_ORDER", dir: "asc"
        },
        serverFiltering: true,
        serverPaging: true,

        transport: {
            read: function (options) {
                var selectAllCprodPack;
                if ($("#selectAllCprodPack").prop("checked") == true) {
                    selectAllCprodPack = 1;
                }
                else if ($("#selectAllCprodPack").prop("checked") == false) {
                    selectAllCprodPack = 0;
                }
                dataFactory.GetCurrentCatalogGroupNames($scope.ImportcatalogIdAttr, $scope.groupAttributeImportTypeSelection, $scope.templateId, options.data).success(function (response) {
                    options.success(response);
                    if (response.length > 0) {

                        $scope.ShowGroupAttributesGrid = true;
                        $scope.ShowNoGroupAttributes = false;

                        if ($scope.saveGroupNameFlag == "1") {
                            $scope.saveGroupNameFlag = "0";
                            $scope.getGroupId = response[0].GROUP_ID;
                            $scope.getGroupName = response[0].GROUP_NAME;
                            $scope.currentCatalogGroupNames = response;
                            $scope.SaveGroupNamesByTemplateId();
                            $timeout(function () {
                                $scope.AttributeGroupDataSource.read();
                            }, 50);

                        }

                        else {
                            $scope.getGroupId = $scope.getGroupId;
                            $scope.getGroupName = $scope.getGroupName;
                        }

                    }
                    else {
                        $scope.ShowGroupAttributesGrid = false;
                        $scope.ShowNoGroupAttributes = true;
                    }


                    $scope.CatprodPackCount = response.Total;

                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        }, batch: true,
        schema: {
            total: "Total",
            model: {
                id: "GROUP_ID",
                fields: {
                    GROUP_NAME: { editable: false, sortable: false },
                    IsAvailable: { editable: true, type: "boolean", sortable: false },
                    SORT_ORDER: { editable: false }
                }
            }
        }
    });


    $scope.AttributeGroupOptions = {
        dataSource: $scope.AttributeGroupDataSource,
        autoBind: false,
        //toolbar: [
        // { name: "save", text: "", template: '<a ng-click="SaveSelectedProdAttributePack()" class="red k-button k-button-icontext k-grid-upload">Save</a>' },
        // { name: "cancel", text: "", template: '<a ng-click="CancelProductAttributePackforCategory()" class="red k-button k-button-icontext k-grid-upload">Cancel</a>' }],
        filterable: true,

        columns: [
            { field: "IsAvailable", title: "Select", width: 10, sortable: false, template: '<input type="checkbox"  class="chkbxforallattributes" #= IsAvailable ? "checked=checked" : "" #  ng-click="GroupNameupdateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllCprodPack'  class='mc-checkbox'  style='width:135px;' ng-click='GroupNameSelectAll($event,this)'/>" },
            { field: "GROUP_NAME", title: "Group Name", width: 20, attributes: { class: "text-left" }, }
        ],
        selectable: 'row',
        change: function () {
            var selectedRows = this.select();
            var rowData = this.dataItem(selectedRows[0]);
            $scope.getGroupId = rowData.GROUP_ID;
            $scope.getGroupName = rowData.GROUP_NAME;
            $scope.isAvaliable = rowData.IsAvailable;
            $scope.GroupAttributesdataSource.read();
        },
        dataBound: function (e) {
        },
        remove: function () {
            // e.model.ATTRIBUTE_ID
        }
    };

    $scope.GroupAttributeSheetnamesDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                debugger;
                if ($scope.selectedTemplate == undefined || $scope.selectedTemplate == "") {
                    $scope.selectedTemplate = 0;
                }
                else {
                    $scope.selectedTemplate = $scope.templateId;
                }

                dataFactory.groupAttributeSheetnames($scope.selectedTemplate).success(function (response) {
                    options.success(response.Data);

                    if (response.Data.length > 0) {

                        $scope.groupAttributeSheetnames = response.Data;
                        $scope.groupAttributeImportSelectedvalue = response.Data[0].SHEET_NAME;
                        $scope.importSheetSelected = response.Data[0].SHEET_NAME;
                        $scope.groupAttributeImportTypeSelection = response.Data[0].IMPORT_TYPE.trim();

                        if ($scope.groupAttributeSheetnames.length > 0) {
                            // $scope.validationSelectionSheet.push($scope.validationimportSelectedvalue);
                            /////************ Show Group attribute page **************////

                            $scope.GroupAttributeDiv = true;
                            $scope.tbldesigner = false;
                            $scope.showValidatediv = false;
                            $scope.showexpressiondiv = true;
                            $scope.AttributeMappingdiv = false;

                            $scope.ProcessName = "Group Attributes";
                            // $scope.ProcessName = "Validate Content";
                            $("#imptamp").removeClass("leftMenuactive");
                            $("#logim").removeClass("leftMenuactive");
                            $("#impvalidation").removeClass("leftMenuactive");
                            $("#Atrmappingimp").removeClass("leftMenuactive");

                            $("#sheetselectimp").removeClass("leftMenuactive");
                            //   $("#impvalidation").addClass("leftMenuactive");
                            $("#Groupattribute").addClass("leftMenuactive");
                            $("#import").removeClass("leftMenuactive");
                            $scope.groupAttributeSelectionSheet.push($scope.groupAttributeImportSelectedvalue);
                            $scope.groupNameIsAvailable = true;
                            $scope.AttributeGroupDataSource.read();
                            $scope.getimportTypechange($scope.groupAttributeImportTypeSelection);
                        }


                    }

                    else {
                        ///**************** Show Validation page *****************//////

                        $scope.passvalidationNext = false;

                        if ($scope.importExcelSheetDDSelectionValue == "TableDesigner") {
                            $scope.ValidateDiv = false;
                            $scope.ValidatetableDesignerDiv = true;
                            $scope.tbldesigner = true;
                        }
                        else {

                            $scope.ValidateDiv = true;
                            $scope.tbldesigner = false;
                            $scope.validationSheetnames.read();
                        }
                        $scope.showValidatediv = false;
                        $scope.showexpressiondiv = true;
                        $scope.AttributeMappingdiv = false;

                        $scope.ProcessName = "Validate Content";
                        $("#imptamp").removeClass("leftMenuactive");
                        $("#logim").removeClass("leftMenuactive");
                        $("#impvalidation").removeClass("leftMenuactive");
                        $("#Atrmappingimp").removeClass("leftMenuactive");

                        $("#sheetselectimp").removeClass("leftMenuactive");
                        $("#impvalidation").addClass("leftMenuactive");
                        $("#import").removeClass("leftMenuactive");


                    }



                }).error(function (error) {
                    options.error(error);
                    blockUI.stop();
                });



            }
        }
    });

    ///******************** Group name selection ************////
    $scope.GroupNameupdateSelection = function (e, id) {
        id.dataItem.set("IsAvailable", e.currentTarget.checked);
        $scope.groupNameIsAvailable = id.dataItem.IsAvailable;
        $scope.getGroupId = id.dataItem.GROUP_ID;
        $scope.getGroupName = id.dataItem.GROUP_NAME;
        $scope.getCatalog_ID = id.dataItem.CATALOG_ID;
        if ($scope.groupNameIsAvailable == false) {
            $scope.getSelectedGroupNameDetails = [];
            $scope.getSelectedGroupNameDetails.push({
                CATALOG_ID: $scope.getCatalog_ID, GROUP_ID: $scope.getGroupId, GROUP_NAME: $scope.getGroupName, IsAvailable: $scope.groupNameIsAvailable
            });

            $scope.currentCatalogGroupNames = $scope.getSelectedGroupNameDetails;
            $scope.DeleteGroupNamesByTemplateId();
        }
        else {
            $scope.getSelectedGroupNameDetails = [];
            $scope.getSelectedGroupNameDetails.push({
                CATALOG_ID: $scope.getCatalog_ID, GROUP_ID: $scope.getGroupId, GROUP_NAME: $scope.getGroupName, IsAvailable: $scope.groupNameIsAvailable
            });

            $scope.currentCatalogGroupNames = $scope.getSelectedGroupNameDetails;
            $scope.SaveGroupNamesByTemplateId();
        }

    };




    $scope.GroupAttributeNextBtn = function () {
        var continueLoop = true;

        angular.forEach($scope.groupAttributeSheetnames, function (sheet) {
            if (continueLoop) {
                if (!($scope.groupAttributeSelectionSheet.includes(sheet.SHEET_NAME))) {

                    $scope.groupAttributeImportSelectedvalue = sheet.SHEET_NAME;
                    $scope.groupAttributeSelectionSheet.push(sheet.SHEET_NAME);
                    $scope.groupAttributeImportTypeSelection = sheet.IMPORT_TYPE.trim();
                    $scope.saveGroupNameFlag = "1";
                    continueLoop = false;

                }
            }


        });

        $timeout(function () {

            if (continueLoop == false) {
                $scope.AttributeGroupDataSource.read();

            }

            else {
                ///**************** Show Validation page *****************//////

                $scope.passvalidationNext = false;
                $scope.GroupAttributeDiv = false;
                $("#Groupattribute").removeClass("leftMenuactive");


                $scope.ValidateDiv = true;
                $scope.tbldesigner = false;
                $scope.validationSheetnames.read();

                $scope.showValidatediv = false;
                $scope.showexpressiondiv = true;
                $scope.AttributeMappingdiv = false;

                $scope.ProcessName = "Validate Content";
                $("#imptamp").removeClass("leftMenuactive");
                $("#logim").removeClass("leftMenuactive");
                $("#impvalidation").removeClass("leftMenuactive");
                $("#Atrmappingimp").removeClass("leftMenuactive");

                $("#sheetselectimp").removeClass("leftMenuactive");
                $("#impvalidation").addClass("leftMenuactive");
                $("#import").removeClass("leftMenuactive");

            }
        }, 100);


    }


    $scope.GroupAtributeBackBtn = function () {
        $scope.passvalidationimportenable = false;
        $scope.templateDetails = false;
        //$("#importSelectionTable").css("margin-top", "0px");
        $scope.AttributeMappingdiv = true;
        $scope.passvalidationNext = true;
        $scope.GroupAttributeDiv = false;
        $scope.ProcessName = "Map Attributes";
        $("#choosefileimp").removeClass("leftMenuactive");
        $("#Atrmappingimp").addClass("leftMenuactive");
        //$scope.ProcessName = "Attribute Mapping";

        $("#choosefileimp").removeClass("leftMenuactive");
        $("#impvalidation").removeClass("leftMenuactive");

        $("#Atrmappingimp").removeClass("leftMenuactive");

        $("#sheetselectimp").removeClass("leftMenuactive");
        $("#import").removeClass("leftMenuactive");
        $("#logim").removeClass("leftMenuactive");
        $("#Atrmappingimp").addClass("leftMenuactive");
        $("#Groupattribute").removeClass("leftMenuactive");
        $scope.getImportTypeBasedonSheet();
    }

    //////***********************  Group Attribute name display grid ************************/////////////
    $scope.GroupAttributesdataSource = new kendo.data.DataSource({
        type: "json",
        autoBind: false,

        sort: {
            field: "SORT_ORDER", dir: "asc"
        },
        transport: {
            read: function (options) {
                dataFactory.GetAllGroupAttributesByGroupId($scope.ImportcatalogIdAttr, $scope.getGroupId, $scope.groupAttributeImportTypeSelection, $scope.templateId, $scope.groupNameIsAvailable).success(function (response) {
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    IsAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    SORT_ORDER: { editable: false }
                }
            }
            //},
            //group: {
            //    field: "ATTRIBUTE_TYPE", aggregates: [
            //       { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            //    ]
        }

    });


    $scope.GroupAttributesGridOptions = {
        dataSource: $scope.GroupAttributesdataSource,
        autoBind: false,
        filterable: true,
        selectable: 'row',
        //filterable: { mode: "row" },
        columns: [
            { field: "IsAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= IsAvailable ? "checked=checked" : "" #  ng-click="AttributeNameupdateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllforAttributeName'  class='mc-checkbox'  style='width:20px;' ng-click='GroupAttributeNameSelectAll($event)'/>" },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name" },
            { field: "CAPTION", title: "Caption" },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                //aggregates: ["count"],
                //groupHeaderTemplate: groupHeaderName
            }],
        change: function () {
            $scope.selectedRows = this.select();
            var rowData = this.dataItem($scope.selectedRows[0]);
            $scope.getSelectedAttributeId = rowData.ATTRIBUTE_ID;
        },
        dataBound: function (e) {
        },
    };

    $scope.AttributeNameupdateSelection = function (e, id) {
        id.dataItem.set("IsAvailable", e.currentTarget.checked);
        $scope.getAttributeNameIsAvailable = id.dataItem.IsAvailable;
        $scope.getAttributeId = id.dataItem.ATTRIBUTE_ID;
        $scope.getAttributeName = id.dataItem.ATTRIBUTE_NAME;

        $scope.UpdateAttributeNamesByTemplateId();


    }

    //$scope.btnGroupAttributesMoveUp = function()
    //{
    ////    var selectedItem = $scope.selectedRows;
    ////    var selectedUid = selectedItem.attr("data-uid");
    ////    var itemIndex = selectedItem.index();
    ////    var dataItem = $("#id_GroupAttributeNameGrid").data("kendoGrid").dataItem(selectedItem);

    ////    var newIndex = itemIndex - 1;
    ////    var content = $(".k-grid-content");

    ////    var offset = selectedItem.offset().top;

    ////    if (newIndex <= 0) {
    ////        newIndex = 0;
    ////    }
    ////    $('#id_GroupAttributeNameGrid').data('kendoGrid').dataSource.remove(dataItem);
    ////    $('#id_GroupAttributeNameGrid').data('kendoGrid').insert(newIndex, dataItem);
    //////    $("#id_GroupAttributeNameGrid").dataSource.remove(dataItem);
    ////   // $("#id_GroupAttributeNameGrid").dataSource.insert(newIndex, dataItem);

    ////     $("#id_GroupAttributeNameGrid").select("[data-uid=" + selectedUid + "]");


    //    //var grid = $("#id_GroupAttributeNameGrid").data("kendoGrid");
    //    //var selectedItem = $scope.selectedRows;
    //    //var selectedUid = selectedItem.attr("data-uid");
    //    //var dataItem = grid.dataSource.getByUid(selectedUid);
    //    //var index = grid.dataSource.indexOf(dataItem);
    //    //var newIndex = Math.max(0, index - 1);

    //    //if (newIndex != index) {
    //    //    grid.dataSource.remove(dataItem);
    //    //    grid.dataSource.insert(newIndex, dataItem);
    //    //    grid.select("[data-uid=" + selectedUid + "]");

    //    //}

    //    //var selectedItem = $scope.selectedRows;
    //    //var selectedUid = selectedItem.attr("data-uid");
    //    //var grid = $("#id_GroupAttributeNameGrid").data("kendoGrid");
    //    //var dataItem = grid.dataSource.getByUid(selectedUid);
    //    //var skip = grid.dataSource.skip();
    //    //var oldIndex = 1;
    //    //var newIndex = 0;
    //    //grid.dataSource.remove(dataItem);
    //    //grid.dataSource.insert(newIndex, dataItem);
    //   // grid.saveChanges();
    //    //var index = grid.dataSource.indexOf(dataItem);
    //    //var newIndex = Math.max(0, index - 1);

    //    //if (newIndex != index) {
    //    //    grid.dataSource.remove(dataItem);
    //    //    grid.dataSource.insert(newIndex, { "ATTRIBUTE_ID": 2624,
    //    //        "ATTRIBUTE_NAME": "GTG",
    //    //    "ATTRIBUTE_TYPE": 3,
    //    //    "CAPTION": "Webinar_Link",
    //    //    "CATALOG_ID": 25,
    //    //    "FAMILY_ID": undefined,
    //    //    "ISAvailable": true });
    //    //}
    //}

    $scope.btnGroupAttributesMoveUp = function () {
        if ($scope.getSelectedAttributeId > 0) {
            dataFactory.BtnAttributeNameMoveUpClick($scope.getSelectedAttributeId, $scope.getGroupId, $scope.templateId).success(function (response) {
                $scope.GroupAttributesdataSource.read();
            }).error(function (error) {
                options.error(error);
            });

        }

        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                type: "error"
            });
        }
    }

    $scope.btnGroupAttributesMoveDown = function () {
        if ($scope.getSelectedAttributeId > 0) {
            dataFactory.BtnAttributeNameMoveDownClick($scope.getSelectedAttributeId, $scope.getGroupId, $scope.templateId).success(function (response) {
                $scope.GroupAttributesdataSource.read();
            }).error(function (error) {
                options.error(error);
            });

        }

        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                type: "error"
            });
        }
    }


    $scope.btnGroupNameMoveUp = function () {
        if ($scope.getGroupId > 0) {
            dataFactory.BtnGroupNameMoveUp($scope.templateId, $scope.getGroupId, $scope.groupAttributeImportTypeSelection, $scope.ImportcatalogIdAttr).success(function (response) {
                $scope.AttributeGroupDataSource.read();
            }).error(function (error) {
                options.error(error);
            });

        }

        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                type: "error"
            });
        }
    }

    $scope.btnGroupNameMoveDown = function () {
        if ($scope.getGroupId > 0) {
            dataFactory.BtnGroupNameMoveDown($scope.templateId, $scope.getGroupId, $scope.groupAttributeImportTypeSelection, $scope.ImportcatalogIdAttr).success(function (response) {
                $scope.AttributeGroupDataSource.read();
            }).error(function (error) {
                options.error(error);
            });

        }

        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Group.',
                type: "error"
            });
        }
    }



    //function groupHeaderName(e) {

    //    if (e.value === 1) {
    //        return "Product Technical Specification (" + e.count + " items)";
    //    }
    //    else if (e.value === 3) {
    //        return "Product Image / Attachment (" + e.count + " items)";
    //    }
    //    else if (e.value === 4) {
    //        return "Product Price (" + e.count + " items)";
    //    } else if (e.value === 6) {
    //        return "Product Key (" + e.count + " items)";
    //    }
    //    else if (e.value === 7) {
    //        return "Family Description (" + e.count + " items)";
    //    }
    //    else if (e.value === 9) {
    //        return "Family Image / Attachment (" + e.count + " items)";
    //    } else if (e.value === 11) {
    //        return "Family Specifications (" + e.count + " items)";
    //    }
    //    else if (e.value === 12) {
    //        return "Family Price (" + e.count + " items)";
    //    }
    //    else if (e.value === 13) {
    //        return "Family Key (" + e.count + " items)";
    //    } else {
    //        return "Product Technical Specification (" + e.count + " items)";
    //    }
    //}


    ///////////////////////************************  Save Group and attribute names in the Group table *******************/////////////////


    $scope.SaveGroupNamesByTemplateId = function () {
        dataFactory.SaveGroupNamesByTemplateId($scope.ImportcatalogIdAttr, $scope.groupAttributeImportTypeSelection, $scope.templateId, $scope.currentCatalogGroupNames).success(function (response) {
            if (response != null) {
                $scope.AttributeGroupDataSource.read();
                $scope.GroupAttributesdataSource.read();
            }
        }).error(function (error) {
            options.error(error);

        });
    }


    $scope.DeleteGroupNamesByTemplateId = function () {
        dataFactory.DeleteGroupNamesByTemplateId($scope.ImportcatalogIdAttr, $scope.groupAttributeImportTypeSelection, $scope.templateId, $scope.currentCatalogGroupNames).success(function (response) {
            $scope.AttributeGroupDataSource.read();
            $scope.GroupAttributesdataSource.read();
        }).error(function (error) {
            options.error(error);

        });
    }

    $scope.UpdateAttributeNamesByTemplateId = function () {
        dataFactory.UpdateAttributeNamesByTemplateId($scope.ImportcatalogIdAttr, $scope.groupAttributeImportTypeSelection, $scope.templateId, $scope.getGroupId, $scope.getAttributeNameIsAvailable, $scope.getAttributeId).success(function (response) {
            $scope.AttributeGroupDataSource.read();
            $scope.GroupAttributesdataSource.read();
        }).error(function (error) {
            options.error(error);

        });
    }


    $scope.GroupNameSelectAll = function (ev) {

        var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
        var items = grid.dataItems();


        $scope.groupNameHeaderCheckbox = ev.target.checked;
        $scope.getSelectedGroupNameDetails = [];
        if ($scope.groupNameHeaderCheckbox == true) {
            items.forEach(function (item) {

                if (item.IsAvailable == false) {

                    $scope.getGroupId = item.GROUP_ID;
                    $scope.getGroupName = item.GROUP_NAME;
                    $scope.getCatalog_ID = item.CATALOG_ID;

                    $scope.getSelectedGroupNameDetails.push({
                        CATALOG_ID: $scope.getCatalog_ID, GROUP_ID: $scope.getGroupId, GROUP_NAME: $scope.getGroupName, IsAvailable: $scope.groupNameIsAvailable
                    });

                    $scope.currentCatalogGroupNames = $scope.getSelectedGroupNameDetails;

                }


            });
            $scope.SaveGroupNamesByTemplateId();
        }
        else {
            items.forEach(function (item) {

                if (item.IsAvailable == true) {
                    $scope.getGroupId = item.GROUP_ID;
                    $scope.getGroupName = item.GROUP_NAME;
                    $scope.getCatalog_ID = item.CATALOG_ID;


                    $scope.getSelectedGroupNameDetails.push({
                        CATALOG_ID: $scope.getCatalog_ID, GROUP_ID: $scope.getGroupId, GROUP_NAME: $scope.getGroupName, IsAvailable: $scope.groupNameIsAvailable
                    });

                    $scope.currentCatalogGroupNames = $scope.getSelectedGroupNameDetails;

                }

            });
            $scope.DeleteGroupNamesByTemplateId();

        }


    }

    $scope.GroupAttributeNameSelectAll = function (ev) {
        var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
        var items = grid.dataItems();


        $scope.groupNameHeaderCheckbox = ev.target.checked;
        $scope.getSelectedAttributeNameDetails = [];
        $scope.currentCatalogGroupAttributeNames = [];
        if ($scope.groupNameHeaderCheckbox == true) {
            items.forEach(function (item) {

                if (item.IsAvailable == false) {

                    $scope.getAttributeId = item.ATTRIBUTE_ID;
                    $scope.getSortOrder = item.SORT_ORDER;


                    $scope.getSelectedAttributeNameDetails.push({
                        ATTRIBUTE_ID: $scope.getAttributeId, SORT_ORDER: $scope.getSortOrder, GROUP_ID: $scope.getGroupId
                    });

                    $scope.currentCatalogGroupAttributeNames = $scope.getSelectedAttributeNameDetails;

                }


            });

            if ($scope.getSelectedAttributeNameDetails.length > 0) {
                $scope.SaveAttributeNamesByTemplateId();
            }

        }
        else {
            items.forEach(function (item) {

                if (item.IsAvailable == true) {

                    $scope.getAttributeId = item.ATTRIBUTE_ID;
                    $scope.getSortOrder = item.SORT_ORDER;


                    $scope.getSelectedAttributeNameDetails.push({
                        ATTRIBUTE_ID: $scope.getAttributeId, SORT_ORDER: $scope.getSortOrder, GROUP_ID: $scope.getGroupId
                    });

                    $scope.currentCatalogGroupAttributeNames = $scope.getSelectedAttributeNameDetails;

                }

            });

            if ($scope.getSelectedAttributeNameDetails.length > 0) {
                $scope.DeleteAttributeNamesByTemplateId();
            }


        }
    }

    $scope.SaveAttributeNamesByTemplateId = function () {
        dataFactory.SaveAttributeNamesByTemplateId($scope.templateId, $scope.getGroupId, $scope.currentCatalogGroupAttributeNames).success(function (response) {
            if (response != null) {
                //$scope.AttributeGroupDataSource.read();
                $scope.GroupAttributesdataSource.read();
            }
        }).error(function (error) {
            options.error(error);

        });
    }

    $scope.DeleteAttributeNamesByTemplateId = function () {
        dataFactory.DeleteAttributeNamesByTemplateId($scope.templateId, $scope.currentCatalogGroupAttributeNames).success(function (response) {
            if (response != null) {
                // $scope.AttributeGroupDataSource.read();
                $scope.GroupAttributesdataSource.read();
            }
        }).error(function (error) {
            options.error(error);

        });
    }


    //////********************* Group Attribute mapping changes  *************************///////////////////

    $scope.fun_ImportMissingMappingAttributeResult = function () {
        dataFactory.getImportMissingMappingAttributeResult().success(function (response) {

            if (response != null) {

                $scope.attributeNames = response;
            }

        }).error(function (error) {
            options.error(error);
            blockUI.stop();
        });
    }

    ////********************Save empty template name after import ********************////////////////


    $scope.btnSaveTemplate = function () {
        $('#templateSavePopup').dialog({
            modal: true,
            autoOpen: true,
            title: $localStorage.ProdcutTitle,
            buttons: {
                "Ok": function () {

                    $scope.getTemplateName = $scope.templateName;
                    if ($scope.getTemplateName == "" || $scope.getTemplateName == undefined) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "Template name is empty in the textbox.",
                            type: "info"

                        });

                    }
                    else {
                        dataFactory.UpdateTemplateDetails($scope.getTemplateName, $scope.ImportcatalogIdAttr, $scope.ImportFormat, $scope.filePath, $scope.templateId).success(function (response) {
                            var result = response.split("_");
                            if (result.length > 0) {

                                $scope.templateId = result[0];
                                $scope.selectedTemplate = $scope.templateId;
                                $scope.templateNameExists = result[1];


                            }

                            if ($scope.templateNameExists == "True") {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Template name already exists.',
                                    type: "info"
                                });

                            }
                            else {
                                $scope.TemplateNameImport = $scope.getTemplateName;
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: "Template name saved successfully.",
                                    type: "info"

                                });
                                $scope.btnSaveImportTemplate = true;
                                $('#templateSavePopup').dialog("close");


                            }
                            $scope.templateName = "";
                            ///***********************Product input Screen changes by Aswin  **********************/


                        }).error(function (error) {
                            options.error(error);
                        });


                    }

                },
                "Cancel": function () {
                    $('#templateSavePopup').dialog("close");
                }

            }

        });
    }

    /////////////***************** To validate special characters in import template name textbox ********************///////

    $("#txtTemplateNameImport").keypress(function (e) {
        var key = e.keyCode || e.which;
        // $("#error_msg").html("");
        //Regular Expression
        var reg_exp = /^[A-Za-z0-9!$%'()+,-.;=@^_`{}~\[\]  ]+$/;
        //Validate Text Field value against the Regex.
        var is_valid = reg_exp.test(String.fromCharCode(key));

        return is_valid;
    });


    /////////////***************** To validate special characters in view log pop up template name textbox ********************///////

    $("#templateSavePopup").keypress(function (e) {
        var key = e.keyCode || e.which;
        // $("#error_msg").html("");
        //Regular Expression
        var reg_exp = /^[A-Za-z0-9!$%'()+,-.;=@^_`{}~\[\]  ]+$/;
        //Validate Text Field value against the Regex.
        var is_valid = reg_exp.test(String.fromCharCode(key));

        return is_valid;
    });

    $scope.closeDuplicateAttributesPopup = function () {
        $scope.showOkButton = false;
        $('#id_DuplicateAttributesModal').hide();

    };
    /*********Product input screen changes by Aswin *********///


    $("#products").kendoDropDownList({

        filter: "startswith",
        filtering: function (e) {
            //get filter descriptor
            var filter = e.filter;

            // handle the event
        }
    });

    $scope.init = function () {
        $scope.getEnableSubProduct();

    };
    $scope.init();



    return $scope.UploadFile;

}]);
