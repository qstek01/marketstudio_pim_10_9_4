﻿
LSApp.controller('familyController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', '$rootScope', '$localStorage', '$sce',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, $rootScope, $localStorage, $sce) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        $scope.getCurrentUrl = $location.$$absUrl.split("App")[1];
        if ($scope.getCurrentUrl == '/AdminDashboard') {
            $rootScope.familyMainEditor = true;
            $rootScope.invertedproductsshow = false;
            $rootScope.invertedproductsbutton1 = false;
        } else if ($scope.getCurrentUrl == '/Inverted') {
            $("#wrapper").toggleClass("toggled-2");
            $('#menu').css("display", "none");
            $('#sidebar-wrapper').css("border", "#fff solid 1px");
            $('#showName').css("color", "#091f3f");
            $('#sidebar-wrapper').css("display", "none");
            $('#page-content-wrapper');
            $rootScope.familyMainEditor = false;
            $rootScope.invertedproductsshow = true;
            $rootScope.invertedproductsbutton = false;
            $rootScope.invertedproductsbutton1 = false;

        }

        $scope.back = function () {

            $rootScope.familyMainEditor = false;
            $rootScope.invertedproductsshow = true;
            $rootScope.LoadInvertedData();

            //$rootScope.productassociatedfamilyGridDatasource.read();
            //   $rootScope.tblInvertedGrid.reload();
            $rootScope.invertedproductsbutton = false;
            $rootScope.invertedproductsbutton1 = false;
            $rootScope.btnSubProdInvert = false;
            $rootScope.SubProdInvert = false;
            $rootScope.invertedBackToProduct = true;
        };
        $scope.SubProductTabSelection = function () {

            $rootScope.GetInvertedSearchSubProductDetails();
        }
        $scope.backSub = function () {
            $rootScope.familyMainEditor = false;
            $rootScope.invertedproductsshow = true;
            $rootScope.btnSubProdInvert = true;
            $rootScope.SubProdInvert = false;
            $rootScope.invertedBackTosubProduct = true;
            $('#dynamictablesubproducts').show();
            $('#invertedSubProductsMain').show();
            $("#familyEditor").hide();
            $('#invertedProductsMain').hide();
            $('#subassociatedgrid').hide();
        };


        $scope.renderfamilyspecsgrid = false;
        $scope.renderfamilydescriptions = false;
        $scope.renderfamilyimages = false;
        $scope.renderrelatedfamily = false;
        $scope.renderclonedcategory = false;
        $scope.rendermultipletable = false;
        $scope.rendertabledesigner = false;
        $scope.actiondisable = true;
        $("#Familyimport").hide();
        //importTableSheetSlectectionWindow

        $scope.renderAttributeGroup = false;
        $scope.renderTableDesignermultiple = false;
        $scope.renderRemoveGroup = false;
        $scope.renderEdit = false;
        $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
        $scope.isnotNumber = function (string) {
            $scope.result = isNaN(string);
            return $scope.result;
        };
        $scope.trustAsHtml = function (string) {

            return $sce.trustAsHtml(string);
        };
        $scope.Family = {
            FAMILY_ID: '',
            FAMILY_NAME: '',
            CATEGORY_ID: '',
            STATUS: 'CREATED',
            PUBLISH: true,
            PUBLISH2PRINT: true,
            PARENT_FAMILY_ID: 0,
            FAMILYIMG_NAME: '',
            FAMILYIMG_ATTRIBUTE: '',
            ATTRIBUTE_ID: '', OBJECT_NAME: '', CATEGORY_NAME: ''
        };

        // $("#attributeNameValue").hide();
        $scope.fmlyDescChangeDisable = false;
        $scope.fmlyImgAttrChangeDisable = false;


        $scope.SelectedDataItem = { VERSION: '1', DESCRIPTION: '', CATALOG_NAME: '', CATALOG_ID: 0, CATEGORY_ID: 0, ID: 0, SORT_ORDER: 0 };

        //$rootScope.selecetedCatalogId = $rootScope.selecetedCatalogId;

        $scope.StatusDataSource = [{ status: "CREATED", status_text: "Created" },
       { status: "IN PROGRESS", status_text: "In Progress" },
       { status: "TO REVIEW", status_text: "To Review" },
       { status: "APPROVED", status_text: "Approved" },
       { status: "VERIFIED", status_text: "Verified" }];

        $scope.StatusChange = function (e) {
            if (e.sender.value() !== "") {
                $scope.Family.STATUS = e.sender.value();
            }
        };
        $scope.fmlyDescChangeData = {
            FAMILY_ID: '',
            ATTRIBUTE_NAME: '',
            STRING_VALUE: '',
            ATTRIBUTE_ID: ''
        };

        $scope.fmlyImgAttrChangeData = {
            FAMILY_ID: '',
            ATTRIBUTE_NAME: '',
            ATTR_VALUE: '',
            ATTR_ID: '',
            IMG_PATH: ''
        };
        $scope.fmlyImgAttrChangeData.ATTR_ID = '';
        $scope.leftRowAttributeListDatasource = {
            ATTRIBUTE_ID: 999,
            ATTRIBUTE_NAME: 'Left'
        };
        $scope.rightRowAttributeListDatasource = {
            ATTRIBUTE_ID: '987',
            ATTRIBUTE_NAME: 'right'
        };
        $scope.columnAttributeListDatasource = {
            ATTRIBUTE_ID: '977',
            ATTRIBUTE_NAME: 'column'
        };
        $scope.summaryAttributeListDatasource = {
            ATTRIBUTE_ID: '967',
            ATTRIBUTE_NAME: 'summary'
        };
        $scope.ActiveCategoryId = '';
        $scope.tableGroupHeaderValue = false;
        $scope.IsNewFamily = '1';

        $scope.$on("GetFamily", function (event, selectedCategoryId, categoryId) {
            //  $scope.IsNewFamily = '0';
            $http.get("../Family/GetFamily?FamilyId=" + selectedCategoryId + "&categoryId=" + categoryId + "&catalogId=" + $rootScope.selecetedCatalogId).
                then(function (familyDetails) {
                    $("#accordions > div").accordion({ active: 0 });
                    
                    $scope.Family = familyDetails.data;
                    $scope.LoadClonedCategory();
                });
        });


        $rootScope.valueofcheckbox = false;
        $rootScope.valueofcheckall = false;
        $scope.$on("Active_Family_Id", function (event, selectedCategoryId, selectedCategoryDetails) {
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;
            $("#Familyimport").hide();
            $rootScope.selecetedParentCategoryId = selectedCategoryId;
            $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
            $scope.fmlyImgAttrChangeData.IMG_PATH = '';
            $scope.IsNewFamily = '0';
            $scope.ActiveCategoryId = selectedCategoryId;
            $("#divNewFmlyAttriEntry").hide();
            $("#columnsetupfamily").hide();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#divFmlyTab").show();
            $("#gridfamily").show();
            $("#EditMultipletable").hide();
            //  $scope.fmlyImgAttrChangeempty();
            $scope.$broadcast("Active_FamilyId", selectedCategoryId);
            $scope.$broadcast("TABLEDESIGNER");
            //  $scope.familySpecsImgAttrDatasource = [];
            //   $scope.familySpecsImgAttrDatasource.refresh();

            var parentnode = selectedCategoryDetails;
            if (selectedCategoryDetails.id.contains("~")) {
                parentnode = parentnode.parentNode();
                if (parentnode.id.contains("~")) {
                    parentnode = parentnode.parentNode().parentNode();
                }
            }

            $scope.$broadcast("GetFamily", selectedCategoryId, parentnode.CATEGORY_ID);
            $rootScope.FamilyCATEGORY_ID = parentnode.id;

            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();

            $scope.OpenProductDefault();



            var tabstrip = $("#tabstripfamily").data("kendoTabStrip");
            $scope.LoadFamilySpecsDesc();
            var myTab = tabstrip.tabGroup.children("li").eq(1);
            $scope.renderfamilydescriptions = true;
            $rootScope.familySpecsDatasource.read();
            tabstrip.select(myTab);
            $("#productgridtabstrip").show();
            $("#divProductGrid").show();
            if ($rootScope.Mainprodcount > 0) {
                $("#productpaging1").show();
            }
            $("#gridproduct").show();
            $("#productpreview").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();

            //var subproductsgrid = $("#subproductsmaingrid").data("kendoPivotGrid");
            var subproductsgrid = $("#subproductsmaingrid").data("kendoGrid");
            subproductsgrid.dataSource.page(1);
        });

        $scope.$on("Active_Family_Ids", function (event, selectedCategoryId, selectedCategoryDetails) {
            $scope.valueofcheckbox = false;
            $scope.valueofcheckall = false;
            $rootScope.selecetedParentCategoryId = selectedCategoryId;
            //familySpecsImgAttrDatasource = '';
            $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
            $scope.fmlyImgAttrChangeData.IMG_PATH = '';
            $scope.IsNewFamily = '0';
            $scope.ActiveCategoryId = selectedCategoryId;
            $("#divNewFmlyAttriEntry").hide();
            $("#divFmlyTab").show();
            $scope.$broadcast("Active_FamilyId", selectedCategoryId);
            $rootScope.familySpecsDatasource.read();
            $scope.familySpecsImgAttrDatasource.read();
            
            $scope.$broadcast("GetFamily", selectedCategoryId, selectedCategoryDetails.CATEGORY_ID);
           
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();

            $scope.OpenProductDefault();


            var tabstrip = $("#tabstripfamily").data("kendoTabStrip");
            $scope.LoadFamilySpecsDesc();
            var myTab = tabstrip.tabGroup.children("li").eq(1);
            $scope.renderfamilydescriptions = true;
            $rootScope.familySpecsDatasource.read();
            tabstrip.select(myTab);
            $("#productgridtabstrip").show();
            $("#divProductGrid").show();
            if ($rootScope.Mainprodcount > 0) {
                $("#productpaging1").show();
            }
            $("#gridproduct").show();
            $("#productpreview").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();
            //var subproductsgrid = $("#subproductsmaingrid").data("kendoPivotGrid");
            var subproductsgrid = $("#subproductsmaingrid").data("kendoGrid");
            subproductsgrid.dataSource.page(1);
        });

        $rootScope.familySpecsDatasource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.getfamilyspecs($scope.ActiveCategoryId, $rootScope.selecetedCatalogId).success(function (response) {
                        options.success(response);
                        if (response.length !== 0) {
                            
                            $scope.renderfamilydescriptions = true;
                            $scope.fmlyDescChangeData.STRING_VALUE = response[0].ATTR_VALUE;
                            $scope.fmlyDescChangeData.ATTRIBUTE_ID = response[0].ATTR_ID;
                            $scope.fmlyDescChangeData.FAMILY_ID = $scope.Family.FAMILY_ID;
                          
                        } else {
                            
                            $scope.fmlyDescChangeData.STRING_VALUE = "";
                            $scope.renderfamilydescriptions = false;
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.workflowDataSource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.getWorkflow().success(function (response) {

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.workflowitemChange = function (e) {
            $scope.Family.STATUS_NAME = e.sender.value();
        };

        $scope.familySpecsImgAttrDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true, autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.getfamilyspecsImgAttr($scope.ActiveCategoryId, $rootScope.selecetedCatalogId).success(function (response) {
                        options.success(response);
                        if (response.length > 0) {
                            $scope.Family.FAMILY_ID = response[0].FAMILY_ID;
                            $scope.Family.ATTRIBUTE_ID = response[0].ATTR_ID;
                            $scope.fmlyImgAttrChangeData.ATTR_ID = response[0].ATTR_ID;
                            $scope.fmlyImgAttrChangeempty();


                        } else {
                            $scope.Family.ATTRIBUTE_ID = "";
                            $scope.fmlyImgAttrChangeempty();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        $scope.ddlCategoryDataSource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.getRelatedfamily($scope.SelectedDataItem.ID, $scope.SelectedDataItem.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.changefamily = [];

        $scope.exchangeFamilyRows = function (dataItem, e) {
            var grid = e.sender;
            var selectedData = grid.dataItem(grid.select());
            $scope.selectedSort = selectedData;
            $scope.subfamilyid = $scope.selectedSort.FAMILY_ID;
            $scope.changefamily = dataItem;

        };

        $scope.categoryfamilyDropDownEditor = function (container, options) {
            $scope.ddlCategoryDataSource.read();
            var editor = $('<input kendo-drop-down-list required k-data-text-field="\'SORT_ORDER\'" k-data-value-field="\'SORT_ORDER\'" k-on-change=\"familysortChange(kendoEvent)\" k-data-source="ddlCategoryDataSource" data-bind="value:' + options.field + '"/>')
            .appendTo(container);
        };

        $scope.familysortChange = function (e) {
            //dataFactory.savefamilysort($scope.changefamily, $scope.SelectedDataItem.CATALOG_ID, $scope.changefamily.PARENT_FAMILY_ID).success(function (response) {
            dataFactory.savesubfamilysort($scope.changefamily, $scope.SelectedDataItem.CATALOG_ID, $scope.changefamily.PARENT_FAMILY_ID, $scope.subfamilyid, $scope.changefamily.SORT_ORDER, $scope.changefamily.FAMILY_NAME).success(function (response) {
                $rootScope.treeData.read();
                //$scope.treeData.read();
                $scope.familyRelatedFamilyDatasource.read();
            }).error(function (error) {
                options.error(error);
            });

        };

        $scope.familyRelatedFamilyDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true, sort: { field: "SORT_ORDER", dir: "asc" },
            pageable: true,
            pageSize: 5,
            serverPaging: true,
            serverSorting: true, autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.GetRelatedfamilyDetails($scope.SelectedDataItem.ID, $scope.SelectedDataItem.CATALOG_ID, $localStorage.CategoryID, options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "FAMILY_ID",
                    fields: {
                        FAMILY_ID: { type: "number", editable: false },
                        SUBFAMILY_ID: { type: "number", editable: false },
                        FAMILY_NAME: { editable: false },
                        SORT_ORDER: { type: "number", validation: { required: true, min: 1 } }
                    }
                }
            }
        });



        $scope.relatedFamilyGridOptions = {
            dataSource: $scope.familyRelatedFamilyDatasource,
            autoBind: false,
            sortable: true,
            filterable: true,
            pageable: { buttonCount: 5 }, selectable: true,
            columns: [
                         { field: "SORT_ORDER", title: "Sort Order", width: "180px", editor: $scope.categoryfamilyDropDownEditor, template: "#=SORT_ORDER#" },
                         { field: "FAMILY_NAME", title: "Family Name", width: "180px", template: "<a title='FamilyName'  class='k-link' href='javascript:void(0);'   ng-click='ngclkfamilyname(this)'>#=FAMILY_NAME#</a>" }
            ],
            editable: true
        };
        $scope.relatedmainFamilyGridOptions = {
            dataSource: $scope.familyRelatedFamilyDatasource,
            autoBind: false,
            sortable: true,
            filterable: true,
            pageable: { buttonCount: 5 }, selectable: true,
            columns: [{ field: "FAMILY_NAME", title: "Family Name", width: "180px", template: "<a title='FamilyName'  class='k-link' href='javascript:void(0);'   ng-click='ngclkfamilyname(this)'>#=FAMILY_NAME#</a>" }
            ],
            editable: true
        };

        //$scope.leftNavselectNode = function (name) {
        //    // var selectedfamily = $("#leftNavTreeViewKendo").data("kendoTreeView").findByText(name);
        //    // $("#leftNavTreeViewKendo").data("kendoTreeView").select(selectedfamily);
        //};

        $scope.ngclkfamilyname = function (e) {

            if ($scope.userRoleAddRelatedFamily) {
                var familyid = e.dataItem.FAMILY_ID;
                $rootScope.LoadProdData($rootScope.selecetedCatalogId, familyid, $localStorage.CategoryID);
                $scope.$broadcast("GetFamily", familyid, e.dataItem.CATEGORY_ID);
                //   $http.get("../Family/GetFamily?FamilyId=" + familyid + "&categoryId=" + e.dataItem.CATEGORY_ID + "&catalogId=" + $rootScope.selecetedCatalogId).then(function (familyDetails) { $scope.Family = familyDetails.data; });
                $scope.SelectedDataItem.ID = familyid;
                $scope.ActiveCategoryId = e.dataItem.id;
                $scope.familyRelatedFamilyDatasource.read();
                $rootScope.familySpecsDatasource.read();
                $scope.LoadFamilySpecsData();
                $scope.familySpecsImgAttrDatasource.read();
                $scope.LoadAttributeGroupType();
                // Node Selection in Related Family
                //var treeview = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView");
                //var getitem = treeview.dataSource.get("~" + familyid);
                //var selectitem = treeview.findByUid(getitem.uid);
                //treeview.select(selectitem);
                //var node = treeview.findByUid(getitem.parentNode().uid);
                //treeview.expand(node);
                $scope.tabstrip.select(2);
                //var tbs = $("#tabstrip").kendoTabStrip({


                //}).data("kendoTabStrip");
                //tbs.select(2);
                // Node Selection in Related Family -----------------
            }
            else {
                // alert("View rights disabled by administrator.");
            }
        };



        $scope.$on("Active_Category_Selection", function (event, selectedDataItem) {
            $scope.ActiveCategoryId = selectedDataItem.ID;
            $scope.SelectedDataItem = selectedDataItem;
            //if ($scope.SelectedDataItem.ID !== 0 && $scope.SelectedDataItem.CATALOG_ID !== 0) {
            //    $scope.familyRelatedFamilyDatasource.read();
            //}
        });

        $scope.fmlyDescChange = function (e) {
            $scope.fmlyDescChangeData.ATTRIBUTE_ID = e.sender._old;
            $http.get("../Family/GetFamilyAttr?familyId=" + $scope.Family.FAMILY_ID + "&attrId=" + $scope.fmlyDescChangeData.ATTRIBUTE_ID).
            then(function (familyattr) {
                $scope.fmlyDescChangeData.STRING_VALUE = familyattr.data.STRING_VALUE;
                $scope.fmlyDescChangeDisable = familyattr.data.ATTRIBUTE_DISABLE;
                $scope.fmlyDescChangeData.FAMILY_ID = $scope.Family.FAMILY_ID;
            });
            if (e.sender._old != "") {
                $("#attributeNameValue").show();
            }
        };
        $scope.fmlyImgAttrChange = function (e) {
            if (e.sender._old !== "") {
                $scope.fmlyImgAttrChangeData.ATTR_ID = e.sender._old;
                $http.get("../Family/GetFamilyImgAttr?familyId=" + $scope.Family.FAMILY_ID + "&&attrId=" + $scope.fmlyImgAttrChangeData.ATTR_ID).
                then(function (familyimgattr) {

                    if (familyimgattr.data.STRING_VALUE !== '' && familyimgattr.data.STRING_VALUE !== null) {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = "/content/Productimages" + familyimgattr.data.STRING_VALUE;

                        //dataFactory.missingPath(familyimgattr.data.STRING_VALUE).success(function (response) {
                        //    alert(response);
                        //    $scope.fmlyImgAttrChangeData.ATTR_VALUE = response;
                        //}).error(function (error) {
                        //    options.error(error);
                        //});
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        if (familyimgattr.data.PREVIEW_PATH.trim() !== "")
                            $scope.fmlyImgAttrChangeData.IMG_PATH = familyimgattr.data.PREVIEW_PATH;
                        else
                            $scope.fmlyImgAttrChangeData.IMG_PATH = "";

                        $scope.fmlyImgAttrChangeDisable = familyimgattr.data.ATTRIBUTE_DISABLE;
                        $scope.Family.FAMILYIMG_ATTRIBUTE = familyimgattr.data.ATTRIBUTE_NAME;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = familyimgattr.data.STRING_VALUE;
                        $scope.FAMILYIMG_NAME_TEMP = '\\Content\\ProductImages\\' + $scope.getCustomerCompanyName + familyimgattr.data.STRING_VALUE;
                    }
                    else if (familyimgattr.data.STRING_VALUE === undefined) {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '/Empty/';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                        $scope.FAMILYIMG_NAME_TEMP = '';
                    }
                    else if (familyimgattr.data.STRING_VALUE == '') {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '/Empty/';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                        $scope.FAMILYIMG_NAME_TEMP = '';
                    }
                    else {

                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                        $scope.FAMILYIMG_NAME_TEMP = '';
                    }
                });
            }
            else {
                $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
                $scope.fmlyImgAttrChangeData.IMG_PATH = '';
            }
        };

        $scope.fmlyImgAttrChangeempty = function () {

            if ($scope.Family.ATTRIBUTE_ID === undefined || $scope.Family.ATTRIBUTE_ID === "") {
                $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
                $scope.fmlyImgAttrChangeData.IMG_PATH = '';
                $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                $scope.fmlyImgAttrChangeData.ATTR_ID = '';
                $scope.Family.ATTRIBUTE_ID = '';
                $scope.Family.OBJECT_NAME = '';
                $scope.Family.FAMILYIMG_NAME = '';
            }
            else {

                //   $scope.fmlyImgAttrChangeData.ATTR_ID = $scope.Family.ATTRIBUTE_ID;
                $http.get("../Family/GetFamilyImgAttr?familyId=" + $scope.Family.FAMILY_ID + "&&attrId=" + $scope.Family.ATTRIBUTE_ID).
                then(function (familyimgattr) {
                    if (familyimgattr.data.STRING_VALUE !== '' && familyimgattr.data.STRING_VALUE !== null) {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = familyimgattr.data.STRING_VALUE;
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.fmlyImgAttrChangeData.IMG_PATH = familyimgattr.data.PREVIEW_PATH;
                        $scope.Family.FAMILYIMG_ATTRIBUTE = familyimgattr.data.ATTRIBUTE_NAME;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = familyimgattr.data.STRING_VALUE;
                    }
                    else if (familyimgattr.data.STRING_VALUE === undefined) {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '/Empty/';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '/Empty/';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                    }
                    else if (familyimgattr.data.STRING_VALUE == '') {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '/Empty/';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '/Empty/';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                    }
                    else {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                    }
                });
            }
        };

        $scope.uploadFile = function (files) {
            var fd = new FormData();
            fd.append("file", files[0]);
            $scope.fmlyImgAttrChangeData.ATTR_VALUE = "\\Content\\" + files[0].name;
        };

        $scope.uploadImgFile = function (files) {
            $scope.fmlyImgAttrChangeData.ATTR_VALUE = $scope.fmlyImgAttrChangeData.ATTR_VALUE;
        };
        $scope.catListGridOptions = {
            dataSource: $scope.catListDatasource,
            autoBind: false,
            sortable: false,
            pageable: { buttonCount: 5 }
        };
        $scope.catListprodDatasource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.Getprodspecs($rootScope.selecetedCatalogId).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            serverPaging: false,
            serverSorting: false,
            pageSize: 5
        });
        $scope.catProdListGridOptions = {
            dataSource: $scope.catListprodDatasource,
            autoBind: false,
            sortable: true,
            pageable: { buttonCount: 5 }
        };
        $rootScope.Productnew = false;
        $scope.SetFlag = function () {
            
            $rootScope.Productnew = true;
            $scope.IsNewFamily = "1";
            $('#user-toolbar-preview').attr('disabled', 'disabled');
            $('#user-toolbar-trash-family').attr('disabled', 'disabled');
            // $scope.$apply(function () {
            $scope.Family.FAMILY_ID = "";
            $scope.Family.FAMILY_NAME = "";
            $scope.Family.CATEGORY_ID = "";
            $scope.Family.STATUS = "CREATED";
            $scope.Family.STATUS_NAME = "NEW";
            //  });
            $rootScope.LoadProdData($rootScope.selecetedCatalogId, 0, $localStorage.CategoryID);
            $scope.SelectedDataItem.ID = "";
            $scope.originalCategoryDatasource.read();
            $scope.clonedCategoryDatasource.read();
            $("#divFmlyTab").show();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
            //$("#ProductTab").addClass("in active");
            //$("#FamilySpecsTab").removeClass("in active");
            //$("#FamilyDescriTab").removeClass("in active");
            //$("#ImagesTab").removeClass("in active");
            //$("#RelatedFamilyTab").removeClass("in active");
            //$("#MultipleTablesTab").removeClass("in active");


            //$("#lnkProductTab").addClass("active");
            //$("#lnkFamilySpecsTab").removeClass("active");
            //$("#lnkFamilyDescriTab").removeClass("active");
            //$("#lnkImagesTab").removeClass("active");
            //$("#lnkRelatedFamilyTab").removeClass("active");
            //$("#lnkMultipleTablesTab").removeClass("active");

            var tabstrip = $("#tabstripfamily").data("kendoTabStrip");
            $scope.LoadFamilySpecsDesc();
            var myTab = tabstrip.tabGroup.children("li").eq(1);
            
            $scope.renderfamilydescriptions = true;
            $rootScope.familySpecsDatasource.read();
            tabstrip.select(myTab);

        };

        $scope.showNewFmlyAttrAdd = function () {
            $("#divNewFmlyAttriEntry").show();
            $("#divFmlyTab").hide();
        };
        $scope.showNewFmlyAttrCancel = function () {
            $("#divNewFmlyAttriEntry").hide();
            $("#divFmlyTab").show();
        };
        $scope.catalogID = $rootScope.selecetedCatalogId;
        $scope.getAttributeList = "";

        $scope.getCatalogFilter = [];
        $scope.previewFamily = function () {
            $.msgBox({
                title: "Select an Action",
                content: "Family Preview may take some time to process.",
                type: "confirm",
                buttons: [{ value: "Ok" },
                    { value: "Cancel" }
                ],
                success: function (result) {
                    if (result === "Ok") {

                        $scope.PreviewText = '';
                        if (!$rootScope.FamilyCATEGORY_ID.length) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: 'Please select a Family',
                                type: "error"
                            });
                        } else {

                            $("#tabledesigner").hide();
                            $("#EditMultipletable").hide();
                            $scope.previewFamilySpecs();
                        }
                    }
                    else {
                        return false;
                    }
                }
            });
        };
        $scope.showFamily = function () {
            $("#divFmlyTab").show();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
        };
        $scope.showFamily2 = function () {
            $("#divFmlyTab").show();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
        };

        $scope.printToCart = function (printSectionId) {
            var innerContents = document.getElementById(printSectionId).innerHTML;
            var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWinindow.document.open();
            popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
            popupWinindow.document.close();
        };
        $scope.Print = function () {
            $scope.printToCart("print_tab");
        };
        $scope.ProductPrint = function () {
            $scope.printToCart("ProductPreviewprint_tab");
        };
        $scope.SubProductPrint = function () {
            $scope.printToCart("SubProductPreviewprint_tab");
        };

        $scope.ModifyResults = function () {
            $("#familyEditor").hide();
            //  $("#DivSearch").show();
            $("#resultbtntab").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
        };

        /*---------------------------------------------------------- Start New Family Creation --------------------------------------------------------------------------------------*/
        $scope.pubCHK = function () {
            if (document.getElementById('publishCHK').checked === true) {
                $scope.Family.PUBLISH = true;
            } else {
                $scope.Family.PUBLISH = false;
            }
        };
        $scope.pub2PrintCHK = function () {
            if (document.getElementById('publish2PrintCHK').checked === true) {
                $scope.Family.PUBLISH2PRINT = true;
            } else {
                $scope.Family.PUBLISH2PRINT = false;
            }
        };


        $scope.loadNewFamilyDetails = function (familyid, categoryId) {
            $rootScope.LoadProdData($rootScope.selecetedCatalogId, familyid, $localStorage.CategoryID);
            $rootScope.selecetedFamilyId = familyid;
            $scope.$broadcast("GetFamily", familyid, categoryId);
            // $http.get("../Family/GetFamily?FamilyId=" + familyid + "&categoryId=" + categoryId + "&catalogId=" + $rootScope.selecetedCatalogId).then(function (familyDetails) { $scope.Family = familyDetails.data; });
            $scope.SelectedDataItem.ID = familyid;
            $scope.ActiveCategoryId = familyid;
            $scope.familyRelatedFamilyDatasource.read();
            $rootScope.familySpecsDatasource.read();
            $scope.LoadFamilySpecsData();
            $scope.familySpecsImgAttrDatasource.read();
            //  $scope.LoadAttributeGroupType();
        };

        $scope.Create = function () {
            $scope.cloneCheck = "";
            //if ($scope.IsNewFamily != '0' && $scope.IsNewFamily != '1' && $scope.IsNewFamily != '2') {

            ///////----------------Clone Lock check----------------------///////////////////////
            $rootScope.SelectedNodeIDPresist = 1;

            $http.get("../Family/CloneCheck?category_id=" + $rootScope.FamilyCATEGORY_ID + "&catalog_id=" + $rootScope.selecetedCatalogId + "&option=" + $scope.IsNewFamily + "&PranetFamily=" + $scope.Family.PARENT_FAMILY_ID).then(function (data) {
                if (data.data.trim() !== "") {
                    $scope.cloneCheck = data.data;
                    var result = confirm($scope.cloneCheck);
                    if (result === true) {
                        $scope.option = "Clone";
                    }
                    else {
                        $scope.option = "NotClone";
                    }
                }
                else {
                    $scope.option = "NotClone";
                }
                if ($scope.Family.CATEGORY_ID !== '') {

                    if ($rootScope.FamilyCATEGORY_ID != $scope.Family.CATEGORY_ID) {
                        $scope.IsNewFamily = '3';
                    }
                }
                //}
                if ($scope.IsNewFamily !== '2')
                    $scope.Family.CATEGORY_ID = $rootScope.FamilyCATEGORY_ID;
                else
                    $scope.Family.CATEGORY_ID = $scope.selecetedCategoryId;

                var charArray = $scope.Family.FAMILY_NAME;
                charArray = charArray.replace(/^\s+|\s+$/g, "");
                $scope.Family.FAMILY_NAME = charArray;

                if ($scope.Family.CATEGORY_ID !== '') {
                    if ($scope.Family.FAMILY_NAME != '' && $scope.Family.FAMILY_NAME != undefined) {
                        var encryptfamily = $scope.Family.FAMILY_NAME;
                        var encryptedfamilyname = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(encryptfamily), key,
                { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
                        if ($scope.IsNewFamily == '1') {

                            $http.get("../Family/SaveFamily?familyName=" + encryptedfamilyname + "&categoryId=" + $scope.Family.CATEGORY_ID + "&status=" + $scope.Family.STATUS + "&publishToWeb=" + $scope.Family.PUBLISH + "&publishToPrint=" + $scope.Family.PUBLISH2PRINT + "&catalogId=" + $rootScope.selecetedCatalogId + "&workflowstatus=" + $scope.Family.STATUS_NAME + "&option=" + $scope.option).
                                then(function (data) {
                                    if (data.data != 0) {
                                        $scope.$broadcast("GetFamily", data.data, $scope.Family.CATEGORY_ID);
                                        //$http.get("../Family/GetFamily?FamilyId=" + data.data + "&categoryId=" + $scope.Family.CATEGORY_ID + "&catalogId=" + $rootScope.selecetedCatalogId).then(function (familyDetails) { $scope.Family = familyDetails.data; });
                                        $.msgBox({
                                            title: "CatalogStudio",
                                            content: 'Family save completed',
                                            type: "info"
                                        });
                                        $scope.loadNewFamilyDetails(data.data, $scope.Family.CATEGORY_ID);
                                        $('#user-toolbar-preview').removeAttr('disabled');
                                        $('#user-toolbar-trash-family').removeAttr('disabled');
                                        if ($scope.userRoleDeleteFamily) {
                                            $('#user-toolbar-association-removal-family').removeAttr('disabled');
                                            $('#user-toolbar-delete-family').removeAttr('disabled');
                                        }
                                        else {
                                            $('#user-toolbar-association-removal-family').attr('disabled', 'disabled');
                                            $('#user-toolbar-delete-family').attr('disabled', 'disabled');
                                        }
                                        $scope.IsNewFamily = '0';
                                    }
                                    else {
                                        $.msgBox({
                                            title: "CatalogStudio",
                                            content: 'Family save not completed',
                                            type: "error"
                                        });
                                        $scope.IsNewFamily = '1';
                                    }
                                    // $scope.CURRENT_FAMILY_ID = data.data;
                                    $rootScope.treeData.read();

                                });
                        } else if ($scope.IsNewFamily == '0') {

                            $http.get("../Family/UpdateFamily?familyId=" + $scope.Family.FAMILY_ID + "&familyName=" + encryptedfamilyname + "&categoryId=" + $scope.Family.CATEGORY_ID + "&status=" + $scope.Family.STATUS + "&publishToWeb=" + $scope.Family.PUBLISH + "&publishToPrint=" + $scope.Family.PUBLISH2PRINT + "&workflowstatus=" + $scope.Family.STATUS_NAME).
                                then(function (data) {
                                    $.msgBox({
                                        title: "CatalogStudio",
                                        content: 'Family details updated',
                                        type: "info"
                                    });
                                    $rootScope.treeData.read();
                                });
                        }
                        else if ($scope.IsNewFamily == '2') {
                            if ($scope.Family.CATEGORY_ID !== '' && $scope.Family.PARENT_FAMILY_ID !== '') {
                                $http.get("../Family/SaveSubFamily?familyName=" + encryptedfamilyname + "&categoryId=" + $scope.Family.CATEGORY_ID + "&status=" + $scope.Family.STATUS + "&publishToWeb=" + $scope.Family.PUBLISH + "&publishToPrint=" + $scope.Family.PUBLISH2PRINT + "&catalogId=" + $rootScope.selecetedCatalogId + "&parentFamilyId=" + $scope.Family.PARENT_FAMILY_ID + "&workflowstatus=" + $scope.Family.STATUS_NAME + "&Option=" + $scope.option).
                                    then(function (data) {
                                        if (data.data != 0) {
                                            $.msgBox({
                                                title: "CatalogStudio",
                                                content: 'Sub Family details updated',
                                                type: "info"
                                            });
                                            $scope.loadNewFamilyDetails(data.data, $scope.Family.CATEGORY_ID);
                                            $scope.IsNewFamily = '0';

                                        }
                                        else {
                                            $.msgBox({
                                                title: "CatalogStudio",
                                                content: 'Sub Family not saved, please try again',
                                                type: "error"
                                            });
                                            $scope.IsNewFamily = '2';
                                        }
                                        $rootScope.treeData.read();

                                    });
                            }
                        }
                        else if ($scope.IsNewFamily == '3') {
                            if ($scope.Family.CATEGORY_ID != '') {
                                $http.get('../Family/CutPasteFamily?CopiedId=' + $scope.Family.FAMILY_ID + '&RightClickedCategoryId=' + $scope.Family.CATEGORY_ID + '&catalogID=' + $rootScope.selecetedCatalogId + '&previousCatalogID=' + $rootScope.selecetedCatalogId)
                              .then(function (data) {
                                  $rootScope.treeData.read();
                              });
                            }
                        }
                    }
                    else {
                        $.msgBox({
                            title: "CatalogStudio",
                            content: 'Please enter a valid family name.',
                            type: "info"
                        });
                    }
                }
                else {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: 'Select at least one Category',
                        type: "info"
                    });
                }
              //  $rootScope.treeData.read();

                //$scope.treeData.read();
            });



        };
        /*---------------------------------------------------------- End New Family Creation --------------------------------------------------------------------------------------*/

        /*---------------------------------------------------------- Start SubFamily Creation --------------------------------------------------------------------------------------*/
        $scope.btnSubFamilyCreate = function () {
            if ($scope.Family.PARENT_FAMILY_ID != 0) {
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Cannot create a Family under a Sub Family',
                    type: "info"
                });
            }
            else {

                $scope.Family.PARENT_FAMILY_ID = $scope.Family.FAMILY_ID;
                $scope.IsNewFamily = "2";
                // $scope.$apply(function () {
                $scope.Family.FAMILY_NAME = "";
                $scope.Family.CATEGORY_ID = "";
                $scope.Family.STATUS = "CREATED";
                // });
                $rootScope.LoadProdData($rootScope.selecetedCatalogId, 0, $localStorage.CategoryID);
                $scope.familyRelatedFamilyDatasource.read();
                $("#divFmlyTab").show();
                $("#familyPreview").hide();
                $("#tabledesigner").hide();
                $("#EditMultipletable").hide();
                $scope.SelectedDataItem.ID = "";
                $scope.originalCategoryDatasource.read();
                $scope.clonedCategoryDatasource.read();
                var tabstrip = $("#tabstripfamily").data("kendoTabStrip");
                
                var myTab = tabstrip.tabGroup.children("li").eq(1);
                $scope.renderfamilydescriptions = true;
                $scope.LoadFamilySpecsDesc();
                tabstrip.select(myTab);
            }
        };

        /*---------------------------------------------------------- End SubFamily Creation --------------------------------------------------------------------------------------*/

        /*---------------------------------------------------------- Start Export --------------------------------------------------------------------------------------*/
        /*---------------------------------------------------------- Start Export --------------------------------------------------------------------------------------*/
        $scope.GetProdSpecsExport = function () {
            $.msgBox({
                title: "Select an Action",
                content: "Family Export may take some time to process.",
                type: "confirm",
                buttons: [{ value: "Ok" },
                    { value: "Cancel" }
                ],
                success: function (result) {
                    if (result === "Ok") {
                        if ($scope.Family.FAMILY_ID === 0) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: 'Please select at least one Family',
                                type: "info"
                            });
                        } else {

                            $http.get("../Catalog/GetProdSpecsExport?catalogId=" + $rootScope.selecetedCatalogId + "&familyId=" + $scope.Family.FAMILY_ID + "&categoryId=" + $scope.Family.CATEGORY_ID + "&displayIdColumns=" + $rootScope.DisplayIdcolumns).
                       then(function (response) {
                           if (response.data === "-1") {
                               $.msgBox({
                                   title: "CatalogStudio",
                                   content: 'Please try again as Export failed',
                                   type: "error"
                               });
                           } else {
                               window.open("DownloadFile.ashx?Path=" + response.data);
                           }
                       });
                        }
                    }
                    else {

                        return false;
                    }
                }
            });
        };

        /*---------------------------------------------------------- End Export --------------------------------------------------------------------------------------*/



        // Region for Family Specs 
        $scope.saveFamilyAttribute = function (famSpec) {
            $http.get("../Family/SaveFamilySpecs?data=" + JSON.stringify(famSpec.sender._data) + "&&editeddataref=" + JSON.stringify($scope.FamilyAttributeChangeList)).
            then(function () {
            });
        };


        $scope.handleChange = function (dataitem) {
            var isAttributePresent = false;
            for (var i = 0; i < $scope.FamilyAttributeChangeList.length; i++) {
                if ($scope.FamilyAttributeChangeList[i].ATTR_ID == dataitem.ATTR_ID) {
                    isAttributePresent = true;
                }
            }
            if (isAttributePresent == false) {
                $scope.FamilyAttributeChangeList.push({ ATTR_ID: dataitem.ATTR_ID, ATTRIBUTE_NAME: dataitem.ATTRIBUTE_NAME, FAMILY_ID: dataitem.FAMILY_ID, VALUE: dataitem.VALUE });
            }
        };

        $scope.gridColumns = [
            { field: "ATTR_ID", hidden: true },
            { field: "FAMILY_ID", hidden: false },
            { field: "VALUE", hidden: false }
        ];

        $scope.myData1 = [{ name: "Moroni", language: 'fr' },
                                      { name: "Tiancum", language: 'en' },
                                      { name: "Enos", language: 'nl' }];
        $scope.gridOptionsgg = {
            data: 'myData1',
            enableCellSelection: true,
            enableRowSelection: false,
            enableCellEditOnFocus: true,

            columnDefs: [
                {
                    field: 'language', enableFocusedCellEdit: true,
                    editableCellTemplate: '../../language_edit.html',
                    cellFilter: 'LanguageMap'
                },
                { field: 'name', enableFocusedCellEdit: true }
            ],
            canSelectRows: false
        };
        $scope.familySpecsData = [];
        $scope.tblFamilySpecs = new ngTableParams(
        {
            page: 1,
            count: 1000
        },
        {
            counts: [],
            total: function () { return $scope.familySpecsData.length; },
            getData: function ($defer, params) {
                var filteredData = $scope.familySpecsData;
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    filteredData;
                params.total(orderedData.length);
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });
        $scope.LoadFamilySpecsData = function () {       
            $scope.renderfamilyspecsgrid = true;
            $scope.renderfamilydescriptions = false;
            $scope.renderfamilyimages = false;
            $scope.renderrelatedfamily = false;
            $scope.renderclonedcategory = false;
            $scope.rendermultipletable = false;
            $rootScope.Family_Data = "Data";
            dataFactory.GetDynamicFamilySpecs($scope.Family.FAMILY_ID, $localStorage.CategoryID, $rootScope.selecetedCatalogId).success(function (response) {
                if (response != null) {
                    var obj = jQuery.parseJSON(response.Data.Data);
                    if (obj.Columns.length > 0) {
                        $scope.renderfamilydescriptions = true;
                        $scope.familySpecsData = obj.Data;
                        $scope.columnsForFamilSpecs = obj.Columns;
                        $scope.actiondisable = true;
                    } else {
                        $scope.familySpecsData = [];
                        $scope.columnsForFamilSpecs = [];
                        $scope.actiondisable = false;
                    }
                }
            }).error(function (error) {
                options.error(error);
            });
        };


        $scope.LoadFamilySpecsDesc = function () {            
            $scope.renderfamilyspecsgrid = false;
            $scope.renderfamilydescriptions = true;
            $scope.renderfamilyimages = false;
            $scope.renderrelatedfamily = false;
            $scope.renderclonedcategory = false;
            $scope.rendermultipletable = false;
            $rootScope.Family_Data = "Desc";            
            $rootScope.familySpecsDatasource.read();
        };


        $scope.LoadFamilySpecsImage = function () {
            $scope.renderfamilyspecsgrid = false;
            $scope.renderfamilydescriptions = false;
            $scope.renderfamilyimages = true;
            $scope.renderrelatedfamily = false;
            $scope.renderclonedcategory = false;
            $scope.rendermultipletable = false;
            $scope.Family.ATTRIBUTE_ID = "";
            $rootScope.Family_Data = "Image";
            // $scope.fmlyImgAttrChangeempty();
            $scope.familySpecsImgAttrDatasource.read();
        };


        $scope.PreviewText = '';

        $scope.previewFamilySpecs = function () {
            $scope.Family.CATEGORY_ID = $rootScope.FamilyCATEGORY_ID;
            if (!$scope.Family.CATEGORY_ID.length || $scope.Family.FAMILY_ID == "") {
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please select a Family',
                    type: "error"
                });
            } else {
                //dataFactory.GetCsFamilyPreviewListCheck($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $rootScope.FamilyLevelMultipletablePreview).success(function (response) {
                //    if (response != null) {
                //        if (response == 'No Preview Available') {
                //            var result = confirm("Product table preview not available. Are you sure want to continue?");
                //            if (result === true) {
                //                $("#divFmlyTab").hide();
                //                $("#familyPreview").show();
                //                dataFactory.GetCsFamilyPreviewList($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $rootScope.FamilyLevelMultipletablePreview).success(function (response) {
                //                    if (response != null) {
                //                        $scope.PreviewText = response;
                //                    }
                //                }).error(function (error) {
                //                    options.error(error);
                //                });
                //            }
                //            else {
                //                $("#divFmlyTab").show();
                //                $("#familyPreview").hide();
                //                $("#tabledesigner").hide();
                //                $("#EditMultipletable").hide();
                //            }
                //        }
                //        else {
                $("#divFmlyTab").hide();
                $("#familyPreview").show();
                dataFactory.GetCsFamilyPreviewList($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $rootScope.FamilyLevelMultipletablePreview).success(function (response) {
                    if (response != null) {
                        $scope.PreviewText = response;
                    }
                }).error(function (error) {
                    options.error(error);
                });
                //        }
                //    }
                //}).error(function (error) {
                //    options.error(error);
                //});
            }
        };


        $scope.LoadRelatedFamilyData = function () {
            if ($scope.SelectedDataItem.ID !== 0 && $scope.SelectedDataItem.CATALOG_ID !== 0) {
                $scope.renderfamilyspecsgrid = false;
                $scope.renderfamilydescriptions = false;
                $scope.renderfamilyimages = false;
                $scope.renderrelatedfamily = true;
                $scope.renderclonedcategory = false;
                $scope.rendermultipletable = false;
                $rootScope.Family_Data = "FamilyData";
                $scope.familyRelatedFamilyDatasource.read();
            }
        };



        $scope.selectedFamilyRowDynamic = [];
        $scope.GetFamilyPickListData = function (attrName, picklistdata) {
            dataFactory.getPickListData(picklistdata).success(function (response) {
                $scope.selectedFamilyRowDynamic[attrName] = response;
                return response;
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.UIMask = function (decimal, number) {
            var uimask = "";
            var uimask1 = "";
            for (var i = 0; i < decimal; i++) {
                uimask += "x";
            }
            for (var i1 = 0; i1 < number; i1++) {
                uimask1 += "x";
            }
            if (number != 0) {
                return uimask + "." + uimask1;
            } else {
                return uimask;
            }
        };
        $scope.selectedFamilyRowAttributes = [];
        $scope.selectedFamilyRowDynamicAttributes = [];
        $scope.attributePattern = {};
        $scope.uimask = {};
        $scope.UpdateFamilySpecs = function (data) {
            //$scope.selectedRow = angular.copy(data.Data);
            //$scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridItemEditPopup.html" });
            //$scope.winAddOrEditFamilySpecs.center().open();
            $scope.selectedRow = data.Data;
            $scope.attributes = Object.keys(data.Data);
            if ($rootScope.selecetedCatalogId !== 0 && $scope.Family.FAMILY_ID !== 0) {
                dataFactory.GetFamilyAttributeDetails($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID).success(function (response) {
                    if (response != null) {
                        $scope.selectedFamilyRowAttributes = response;
                        angular.forEach($scope.selectedFamilyRowAttributes, function (value, key) {
                            var theString = value.ATTRIBUTE_ID;
                            $scope.selectedFamilyRowDynamicAttributes[theString] = [];
                            $scope.selectedFamilyRowDynamicAttributes[theString] = value;

                            if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Num')) {

                                var numeric = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[0];
                                var decimal = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[1].split(')')[0];
                                var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                pattern = pattern.replace("numeric", numeric);
                                pattern = pattern.replace("decimal", decimal);
                                var reg = new RegExp(pattern);
                                var uimask = $scope.UIMask(numeric, decimal);
                                $scope.uimask[theString] = uimask;
                                $scope.attributePattern[theString] = reg;
                            } else if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                var maxlength = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(')')[0];
                                $scope.attributePattern[theString] = maxlength;
                                $scope.uimask[theString] = maxlength;
                            } else {
                                $scope.attributePattern[theString] = 524288;
                                $scope.uimask[theString] = 524288;
                            }

                        });
                        angular.forEach($scope.selectedRow, function (value, key) {
                            if (!key.contains("FAMILY_ID")) {
                                var sa = $scope.selectedFamilyRowDynamicAttributes[key.split('__')[2]];
                                if (sa !== undefined) {
                                    if (sa.USE_PICKLIST) {
                                        var theString = sa.ATTRIBUTE_ID;
                                        $scope.GetFamilyPickListData(theString, sa.PICKLIST_NAME);
                                    }
                                }
                            }
                        });
                        //  $scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridFamilySpecsEditPopup.html" });
                        //   $scope.winAddOrEditFamilySpecs.center().open();
                    }
                }).error(function (error) {
                    options.error(error);
                });
                $scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridFamilySpecsEditPopup.html" });
                $scope.winAddOrEditFamilySpecs.title("Family Specification");
                $scope.winAddOrEditFamilySpecs.center().open();
            }
        };
        $scope.onDateSelected = function (e, th) {
            var dateReg = /^\d{2}[./-]\d{2}[./-]\d{4}\s\d{2}:\d{2}\s[AP]M$/;
            if (!e.match(dateReg)) {
                $('#datetime' + th.$$watchers[0].last).val("");
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please enter valid format',
                    type: "error"
                });
            }
        };
        $scope.val_type = "true";
        $scope.TextBoxChanged = function (e, id, attribute) {

            var url = e.currentTarget.value;
            var ida = e.currentTarget.id;
            var pattern = /((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)/;
            if (pattern.test(url)) {
            } else {
                //$("#" + ida).focus();
                $scope.selectedRow[attribute] = "";
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please enter a valid URL',
                    type: "info"
                });

            }
        };

        //$scope.DataTypeCheck = function (e, id) {
        //    var value = e.currentTarget.value;
        //    var textboxid = e.currentTarget.id;
        //    dataFactory.GetAttributeDataFormat(id).success(function (response) {
        //        //        var details = response;
        //        var format1 = /^[0-9a-zA-Z]+$/;
        //        var format2 = /^[a-zA-Z]+$/;
        //        var format3 = /^[0-9a-zA-Z\r\n\x20-\x7E\u0000-\uFFFF]+$/
        //        var FormatValue = "/" + details[0].ATTRIBUTE_DATAFORMAT.replace(" ", "") + "/";
        //        FormatValue = FormatValue.replace('*','+')
        //        //        if (FormatValue.contains('u0000')) {
        //            var textValue = e.currentTarget.value;
        //            if (!format3.test(textValue)) {
        //                alert("Please Enter Data in a Valid Format.");
        //                $("#" + textboxid).focus();
        //            }
        //        }

        //        else if (FormatValue.contains("0-9")) {
        //            var textValue = e.currentTarget.value;
        //            if (!format1.test(textValue)) {
        //                alert("Please Enter Data in a Valid Format.");
        //                $("#" + textboxid).focus();
        //            }

        //        }
        //        else if (FormatValue.contains("a-zA-Z")) {
        //            var textValue = e.currentTarget.value;
        //            if (!format2.test(textValue)) {
        //                alert("Please Enter Data in a Valid Format.");
        //                $("#" + textboxid).focus();
        //            }
        //        }


        //    }).error(function (error) {
        //        options.error(error);
        //    });
        //};

        $scope.SaveOrUpdateFamilSpecs = function (row) {

            var displayAttribures = "";
            if ($scope.val_type === "true") {

                angular.forEach(row, function (value, key) {
                    if (key.contains("__")) {
                        if (key.split('__')[3] == '1' && (value == '' || value == null)) {
                            if (displayAttribures == '') {
                                displayAttribures = "The following attribute values are required,\n";
                            }
                            displayAttribures = displayAttribures + "     \n \t" + key.split('__')[0];
                        }
                    }
                });

                if (displayAttribures == '') {
                    dataFactory.SaveFamilySpecs($rootScope.selecetedCatalogId, $localStorage.CategoryID, row).success(function (response) {
                        if (response != null) {
                            $scope.winAddOrEditFamilySpecs.close();
                            $scope.LoadFamilySpecsData();
                            $.msgBox({
                                title: "CatalogStudio",
                                content: 'Update completed',
                                type: "info"
                            });
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: '' + displayAttribures + '',
                        type: "info"
                    });
                }

            }
            else {
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please enter Data using a valid format',
                    type: "info"
                });
            }

        };

        $scope.DeleteFamilSpecs = function (selectedRow) {
        };


        $scope.datatypecheck = function (event, type, attribute) {
            var value = event.currentTarget.value;
            var datatype = type;
            var pattern = "";

            if (type !== "All Characters" && !type.contains("0-9") && !type.contains("uFFFF")) {
                pattern = /^[a-zA-Z ]+$/;
            }
            else {
                if (type.contains("uFFFF")) {
                    //pattern = /^[ A-Za-z0-9_@./#&@$%^*!~*?+-]*$/;
                }
                else {
                    pattern = /^[0-9a-zA-Z ]*$/;
                }
            }
            if (pattern !== "" && value.trim() !== "") {
                var id = event.currentTarget.id;
                if (!pattern.test(value)) {
                    $scope.selectedRow[attribute] = "";
                    $.msgBox({
                        title: "CatalogStudio",
                        content: 'Please enter valid Data',
                        type: "info"
                    });

                }
            }

        };


        $scope.CancelFamilSpecs = function () {
            $scope.winAddOrEditFamilySpecs.close();
            $scope.LoadFamilySpecsData();
        };
        $scope.tableDesignerClick = function (v) {
            //alert("sdfsdf");
            $scope.rendertabledesigner = true;
            dataFactory.GetStructureNameDataSourceDetails($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId, "FamilyTable", 0).success(function (response) {
                if (response.length > 0) {
                    // $scope.winTableDesigner.refresh({ url: "../Views/App/Partials/tableDesignerPopup.html" });
                    $scope.tableGroupHeaderValue = true;
                    $("#divFmlyTab").hide();
                    $("#familyPreview").hide();
                    //  $scope.winTableDesigner.refresh({ url: "../Views/App/Partials/tableDesignerPopup.html" });
                    //  $scope.winTableDesigner.center().open();

                    $scope.$broadcast("TABLEDESIGNER");
                    $("#tabledesigner").show();
                    $("#EditMultipletable").hide();
                }
                else {
                    $scope.createLayoutClick();
                }

            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.VerticalTable_ChkBox = {
            value1: false
        };
        $scope.StructureType = {
            dataSource: {
                data: [
                    { Text: "Horizontal", value: "Horizontal" },
                    { Text: "Vertical", value: "Vertical" },
                    { Text: "Super Table", value: "SuperTable" }
                ]
            },
            dataTextField: "Text",
            dataValueField: "value"
        };

        $scope.StructureTypeChange = function (e) {
            $scope.Structure.tabletype = e.sender.value();
            if ($scope.Structure.tabletype == 'Vertical') {
                $scope.VerticalTable_ChkBox = {
                    value1: true
                };

            } else {
                $scope.VerticalTable_ChkBox = {
                    value1: false
                };
            }
        };

        $scope.createLayoutClick = function () {
            // $scope.winCreateNewLayout.refresh();
            $scope.winCreateNewLayout.center().open();
        };
        $scope.Structure = {
            structureName: "Sample",
            defaultlayout: true,
            tabletype: "Horizontal"
        };
        $scope.CreateStructure = function () {
            $scope.winCreateNewLayout.close();
            $scope.rendertabledesigner = false;
            dataFactory.CreateStructure($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.Structure.defaultlayout, $scope.Structure.structureName, $scope.Structure.tabletype).success(function (response) {

                if (response != null && response !== "Table Layout already exists, please create a different Layout") {
                    $scope.rendertabledesigner = true;
                    $scope.tableGroupHeaderValue = true;
                    $("#divFmlyTab").hide();
                    $("#familyPreview").hide();
                    $scope.$broadcast("TABLEDESIGNER");
                    $("#tabledesigner").show();
                    $("#EditMultipletable").hide();
                } else {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: 'Table Layout already exists, please create a different Layout',
                        type: "info"
                    });
                }
            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.CancelStructure = function () {
            $scope.winCreateNewLayout.close();
        };

        $scope.defaultLayoutClick = function () {
            $scope.winDefaultLayout.refresh({ url: "../Views/App/Partials/setDefaultLayout.html" });
            $scope.winDefaultLayout.center().open();

        };
        $scope.deleteLayoutClick = function () {
            $scope.winDeleteLayout.refresh({ url: "../Views/App/Partials/deleteFamilyLayout.html" });
            $scope.winDeleteLayout.center().open();

        };
        $scope.familyAttributeSetupClick = function () {
            //$scope.winFamilyAttributeSetup.refresh({ url: "../Views/App/Partials/familyAttributeSetup.html" });
            //$scope.winFamilyAttributeSetup.center().open();
            $scope.familyGetAllCatalogattributesdataSource.read();
            $scope.familyprodfamilyattrdataSource.read();
            // $scope.LoadFamilySpecsData();
            $rootScope.familySpecsDatasource.read();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#divFmlyTab").show();
            $("#gridfamily").hide();
            $("#columnsetupfamily").show();
            $("#EditMultipletable").hide();
        };
        $scope.familyNewAttributeSetupClick = function () {
            $scope.winFamilyNewAttributeSetup.refresh({ url: "../Views/App/Partials/familyNewAttributeSetup.html" });
            $scope.winFamilyNewAttributeSetup.center().open();
            // $scope.tableGroupHeaderValue = true;
        };

        //$scope.NewAttrSave = function (attribute) {
        //    if (attribute.ATTRIBUTE_NAME.trim() == "") {
        //        alert("Please enter valid name");
        //    }
        //    else {
        //        dataFactory.savenewattribute(attribute, $scope.CATALOG_ID).success(function (response) {
        //            alert(response);
        //            $scope.Attribute.ATTRIBUTE_SIZE = 0;
        //        }).error(function (error) {
        //            options.error(error);
        //        });
        //    }
        //    $scope.RefreshFamilyAttributes();
        //    $("#customvaluenew").val("");
        //    $("#suffixnew").val("");
        //    $("#prefixnew").val("");
        //    $("#attrapplytofirstnew").val("");
        //    $("#attrapplytoallnew").val("");
        //    $("#txt_attributedatasize").val("");
        //    $("#txt_size").val("");
        //    $("#txt_stylename").val("");
        //    $("#txt_attr_name").val("");
        //    //$("#attrmaingrid").show();
        //    //$("#addnewattribute").hide();
        //};

        $scope.selectedcatalog1 = new kendo.data.HierarchicalDataSource({
            type: "json",
            transport: {
                read: function (options) {
                    // alert("1_TEST");
                    dataFactory.getCategories($rootScope.selecetedCatalogId, options.data.id).success(function (response) {
                        options.success(response);
                        //$("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
                        //$("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
                    }).error(function (response) {
                        options.success(response);
                    });
                },
            },
            schema: {
                model: {
                    id: "id",
                    hasChildren: "hasChildren"
                }
            }
        });


        $scope.treeOptionsCatalog = {

            checkboxes: {
                checkChildren: true,
            },
            loadOnDemand: true,
            dataBound: function (e) {
                //console.log(e);
                if (!e.node) {
                    $scope.attachChangeEvent();
                }
            }
        };

        //For New Product Creation
        $scope.createNewProduct = function () {
            $scope.winNewProductCreation.refresh({ url: "../views/app/partials/newProduct.html" });
            $scope.winNewProductCreation.center().open();
        };

        $scope.$on("NewProductCreation", function (event, selectedCategoryId, categoryId) {
            $scope.IsNewFamily = '0';
            $scope.ActiveCategoryId = selectedCategoryId;
            $http.get("../Family/GetFamily?FamilyId=" + selectedCategoryId + "&categoryId=" + categoryId + "&catalogId=" + $rootScope.selecetedCatalogId).
              then(function (familyDetails) {
                  $scope.Family = familyDetails.data;
                  if ($scope.winNewProductCreation !== undefined) {
                      $scope.winNewProductCreation.refresh({ url: "../views/app/partials/newProduct.html" });
                      $scope.winNewProductCreation.center().open();
                  }
              });
        });

        $scope.$on("NewFamilyCreation", function (event, selectedCategoryId, dataItem) {


            $scope.IsNewFamily = '1';
            $scope.ActiveCategoryId = selectedCategoryId;
            $("#divNewFmlyAttriEntry").hide();
            $("#divFmlyTab").show();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
            //$rootScope.dropDownTreeViewDatasource.read();
            var parentnode = dataItem;
            if (dataItem.CATEGORY_ID.contains("~")) {
                $scope.Family.PARENT_FAMILY_ID = parentnode.CATEGORY_ID.substring(1, parentnode.CATEGORY_ID.length);
                parentnode = dataItem.parentNode();
                if (parentnode.CATEGORY_ID.contains("~")) {
                    //$scope.Family.PARENT_FAMILY_ID = parentnode.id.substring(1, parentnode.id.length);;
                    //$scope.IsNewFamily = "2";
                    //$scope.Family.FAMILY_NAME = "";
                    //$scope.Family.CATEGORY_ID = "";
                    //$scope.Family.STATUS = "";
                    $.msgBox({
                        title: "CatalogStudio",
                        content: 'Cannot create a Family under a Sub Family',
                        type: "info"
                    });
                    parentnode = dataItem.parentNode().parentNode();
                } else {
                    $scope.IsNewFamily = "2";
                    $scope.Family.FAMILY_NAME = "";
                    $scope.Family.CATEGORY_ID = "";
                    $scope.Family.STATUS = "CREATED";
                    $scope.Family.STATUS_NAME = "NEW";
                    $rootScope.LoadProdData($rootScope.selecetedCatalogId, 0, $localStorage.CategoryID);
                    $scope.SelectedDataItem.ID = "";
                    $scope.originalCategoryDatasource.read();
                    $scope.clonedCategoryDatasource.read();
                    var tabstrip = $("#tabstripfamily").data("kendoTabStrip");

                    var myTab = tabstrip.tabGroup.children("li").eq(1);
                    $scope.renderfamilydescriptions = true;
                    $scope.LoadFamilySpecsDesc();
                    tabstrip.select(myTab);
                }
            } else {

                $scope.Family.FAMILY_ID = "";
                $scope.Family.FAMILY_NAME = "";
                $scope.Family.CATEGORY_ID = "";
                $scope.Family.STATUS = "CREATED";
                $scope.Family.STATUS_NAME = "NEW";
                $rootScope.LoadProdData($rootScope.selecetedCatalogId, 0, $localStorage.CategoryID);
                $scope.SelectedDataItem.ID = "";
                $scope.originalCategoryDatasource.read();
                $scope.clonedCategoryDatasource.read();
                $("#divFmlyTab").show();
                $("#familyPreview").hide();
                $("#tabledesigner").hide();
                $("#EditMultipletable").hide();
                var tabstrips = $("#tabstripfamily").data("kendoTabStrip");

                var myTabs = tabstrips.tabGroup.children("li").eq(1);
                $scope.renderfamilydescriptions = true;
                $scope.LoadFamilySpecsDesc();
                tabstrips.select(myTabs);
            }
            //var selectedparentcategory1 = $rootScope.dropDownTreeView._treeview.findByText(parentnode.CATEGORY_NAME);
            //$rootScope.dropDownTreeView._treeview.select(selectedparentcategory1);
            //if (selectedparentcategory1.length > 0) {
            //    $("#dropDownTreeView .k-input").text(parentnode.CATEGORY_NAME);
            $rootScope.FamilyCATEGORY_ID = parentnode.id;
            //}
        });

        $scope.$on("FamilyPreview", function (event, selectedCategoryId, categoryId) {
            //$("#divFmlyTab").hide();
            //$("#familyPreview").show();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
            $scope.IsNewFamily = '0';
            $scope.ActiveCategoryId = selectedCategoryId;
            $http.get("../Family/GetFamily?FamilyId=" + selectedCategoryId + "&categoryId=" + categoryId + "&catalogId=" + $rootScope.selecetedCatalogId).
              then(function (familyDetails) {
                  $scope.Family = familyDetails.data;
                  $rootScope.FamilyCATEGORY_ID = $scope.Family.CATEGORY_ID;
                  $scope.previewFamilySpecs();
              });
        });

        //$scope.tableGroupHeaderChange = function (e) {
        //    $scope.tableDesignerFields.Combo_GroupByColumn = e.sender._old;
        //};

        //$scope.tableGroupSummaryChange = function (e) {
        //    $scope.tableDesignerFields.ComboSummaryGroup = e.sender._old;
        //};

        //To Remove the Family Association
        $scope.removeFamilyPermanently = function () {
            if ($scope.selectedFamilyID !== "") {

                $.msgBox({
                    title: "Remove Family Confirmation",
                    content: "Are you sure you want to remove the Family Association?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {
                        var bool = false;
                        if (result === "Yes") {
                            bool = true;
                        }

                        if (bool === true) {
                            dataFactory.DeleteFamilyPermanent($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                                $.msgBox({
                                    title: "CatalogStudio",
                                    content: '' + response + '',
                                    type: "info"
                                });
                                $rootScope.treeData.read();
                                //$scope.treeData.read();
                                //  $scope.catalogDataSource.read();
                            }).error(function (error) {
                                options.error(error);
                            });
                        }

                    }
                });


            }

        };
        //To Remove the Family Permanently
        $scope.removeFamilyPermanentlyFromMaster = function () {

            if ($scope.selectedFamilyID !== "") {
                $scope.EnableShowMasterFamily = true;
                $http.get('../Category/GetClonedFamilyDet?Catalog_Id=' + $rootScope.selecetedCatalogId + '&Family_Id=' + $scope.Family.FAMILY_ID + '&Category_Id=' + $rootScope.selecetedCategoryId).then(function (e) {
                    $scope.Flag = e.data;
                    if ($scope.Flag !== "1" && $scope.Flag !== "2") {

                        $.msgBox({
                            title: "Select an Action",
                            content: "Are you sure you want to delete the Family?",
                            type: "confirm",
                            buttons: [{ value: "Yes" }, { value: "No" }],
                            success: function (result) {
                                var bool = false;
                                if (result === "Yes") {
                                    bool = true;
                                }

                                if (bool === true) {
                                    dataFactory.DeleteFamilyPermanentFromMaster($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                                        $.msgBox({
                                            title: "CatalogStudio",
                                            content: '' + response + '',
                                            type: "info"
                                        });
                                        $rootScope.treeData.read();
                                        //$scope.treeData.read();
                                    }).error(function (error) {
                                        options.error(error);
                                    });
                                }
                            }
                        });

                    }
                    else {
                        if ($scope.Flag === "1") {
                            $scope.EnableShowMasterFamily = true;
                        }
                        else {
                            $scope.EnableShowMasterFamily = false;
                        }
                        $scope.FamilyClone.refresh({ url: "../views/app/partials/FamilyCloneCheckOption.html" });
                        $scope.FamilyClone.center().open();

                    }

                });


            }
        };
        //To move to Recycle Bin the Family Permanently
        $scope.trashFamilyPermanentlyFromMaster = function () {

            if ($scope.selectedFamilyID !== "" && $scope.selectedFamilyID !== undefined) {
                $rootScope.Productnew = true;
                $scope.EnableShowMasterFamily = true;
                $http.get('../Category/GetClonedFamilyDet?Catalog_Id=' + $rootScope.selecetedCatalogId + '&Family_Id=' + $scope.Family.FAMILY_ID + '&Category_Id=' + $rootScope.selecetedCategoryId).then(function (e) {
                    $scope.Flag = e.data;
                    if ($scope.Flag !== "1" && $scope.Flag !== "2") {

                        $.msgBox({
                            title: "Select an Action",
                            content: "Are you sure you want to delete the Family?",
                            type: "confirm",
                            buttons: [{ value: "Yes" }, { value: "No" }],
                            success: function (result) {
                                var bool = false;
                                if (result === "Yes") {
                                    bool = true;
                                }

                                if (bool === true) {

                                    dataFactory.trashFamilyPermanentFromMaster($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                                        $.msgBox({
                                            title: "CatalogStudio",
                                            content: '' + response + '',
                                            type: "info"
                                        });
                                        $rootScope.treeData.read();
                                        //$scope.treeData.read();
                                    }).error(function (error) {
                                        options.error(error);
                                    });
                                    $scope.SetFlag();

                                }
                            }
                        });

                    }
                    else {
                        if ($scope.Flag === "1") {
                            $scope.EnableShowMasterFamily = true;
                        }
                        else {
                            $scope.EnableShowMasterFamily = false;
                        }
                        $scope.FamilyClone.refresh({ url: "../views/app/partials/FamilyCloneCheckOption.html" });
                        $scope.FamilyClone.center().open();

                    }

                });


            }
        };

        //Family Attachment
        $scope.onSuccess = function (e) {
            if (e.operation === "remove") {
                $scope.Family.FAMILYIMG_NAME = "";
            } else {
                var message = $.map(e.files, function (file) {
                    return file.name;
                }).join(", ");
                var imgext = message.substring(message.length, message.length - 4);
                message = message.replace(/&amp;/g, '&');
                if (message.indexOf('&') == -1 && message.indexOf('#') == -1 && message.indexOf('\'') == -1) {
                    $scope.$apply(function () {
                        $scope.FAMILYIMG_NAME_TEMP = '\\Content\\ProductImages\\' + $scope.getCustomerCompanyName + '\\Images\\' + message;
                        //$scope.fmlyImgAttrChangeData.ATTR_VALUE = '\\Content\\ProductImages\\' + $scope.getCustomerCompanyName + '\\Images\\' + message;
                        //$scope.fmlyImgAttrChangeData.IMG_PATH = '\\Content\\ProductImages\\' + $scope.getCustomerCompanyName + '\\Images\\' + message;

                        $scope.Family.FAMILYIMG_NAME = '\\Images\\' + message;
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '\\Images\\' + message;
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '\\Images\\' + message;

                    });
                } else {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: 'Image name contains invalid special characters( # & \' )',
                        type: "error"
                    });
                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();
                }

            }
            $(".k-upload-files").remove();
            $(".k-upload-status").remove();
        };
        $scope.onSelect = function (e) {
            var message = $.map(e.files, function (file) { return file.name; }).join(", ");
            angular.forEach($scope.selectedRow, function (value, key) {
                if (key == e.sender.element[0].id) {
                    $scope.selectedRow[key] = '\\Images\\' + e.files[0].name;
                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();
                    $scope.$apply();
                }
            });

        };
        function onUpload(e) {
            if (e.files.length > 1) {
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please Upload single image',
                    type: "error"
                });
            }
        }

        $scope.SaveFamilyAttach = function () {
            if ($scope.Family.FAMILY_ID !== "") {
                $scope.Family.FAMILYIMG_NAME = $scope.Family.FAMILYIMG_NAME.replace(/&amp;/g, '&');
                dataFactory.SaveFamilyImgAttach($scope.Family, $rootScope.selecetedCatalogId).success(function (response) {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: '' + response + '',
                        type: "info"
                    });
                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();
                    $scope.familySpecsImgAttrDatasource.read();
                }).error(function (error) {
                    options.error(error);
                });
            }

        };

        $scope.ClearFamilyAttach = function () {
            if ($scope.Family.FAMILY_ID !== "") {
                dataFactory.ClearFamilyAttach($scope.Family, $rootScope.selecetedCatalogId).success(function (response) {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: '' + response + '',
                        type: "info"
                    });
                    $(".k-upload-files").remove();
                    $scope.fmlyImgAttrChangeempty();
                }).error(function (error) {
                    options.error(error);
                });
            }

        };

        $scope.SaveFamilyDescription = function () {
            if ($scope.Family.FAMILY_ID !== "" && $scope.fmlyDescChangeData.ATTRIBUTE_ID != "") {
                $scope.fmlyDescChangeData.STRING_VALUE = $("#__ckd_1")[0].nextElementSibling.innerHTML;
                dataFactory.saveFamilyDescription($scope.fmlyDescChangeData, $rootScope.selecetedCatalogId).success(function (response) {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: '' + response + '',
                        type: "info"
                    });
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please select an Attribute',
                    type: "error"
                });
            }
        };
        $scope.ShowFamilyMTPreview = function () {
            $scope.winFamilyMTPreview.refresh({ url: "../views/app/partials/MultipleTablePreview.html" });
            $scope.winFamilyMTPreview.center().open();
        };

        $scope.init = function () {
            if ($localStorage.getCatalogID === undefined) {
                $rootScope.selecetedCatalogId = 0;
            }
            else {
                $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
            }

            $("#familyEditor").hide();
            // $("#familyPreview").show();
            $("#resultbtntab").hide();
            $("#tabledesigner").hide();
            $("#columnsetupfamily").hide();
            $("#EditMultipletable").hide();
        };

        $scope.init();


        //------ Multiple Table-------------------------------------------------------------------------------------------------------------------------------//

        $scope.tableName = "";
        $scope.btnAddTableNames = function () {
            // for (var i = 0; i < counter; i++) {
            //  var newTableName = $scope.data.fields[i].values;
            $("#multipletabledesigner").hide();
            $("#multipleattributegroup").show();
            if ($scope.tableName.trim() != "") {
                dataFactory.createNewGroupFromMultipleTable($scope.tableName, $scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                    if (response != null) {
                        $.msgBox({
                            title: "CatalogStudio",
                            content: '' + response + '',
                            type: "info"
                        });
                        $scope.GetAllmultipleGroupdataSource.read();
                        $scope.tableName = "";
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please enter a Table Name',
                    type: "error"
                });
            }
            // }
        };
        $scope.RefreshGroup = function () {
            $scope.tableName = '';
            $("#multipletabledesigner").hide();
            $("#multipleattributegroup").show();
            $scope.GetAllmultipleGroupdataSource.read();
            $scope.showmultipleTableSave();
        };
        $scope.RemoveGroup = function () {
            $("#multipletabledesigner").hide();
            $("#multipleattributegroup").show();
            $scope.renderAttributeGroup = false;
            $scope.renderTableDesignermultiple = false;
            $scope.renderRemoveGroup = true;
            $scope.renderEdit = false;
            if ($scope.Group.GROUP_ID === 0) {
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please select a Group that needs to be deleted',
                    type: "error"
                });

            } else {
                $.msgBox({
                    title: "Select an Action",
                    content: "Please Confirm. Do you want to delete this Attribute group?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {
                        var bool = false;
                        if (result === "Yes") {
                            bool = true;
                        }
                        if (bool == true) {
                            dataFactory.RemoveGroupFromMultipleTable($scope.Group.GROUP_ID, $rootScope.selecetedCatalogId).success(function (response) {
                                if (response != null) {
                                    $.msgBox({
                                        title: "CatalogStudio",
                                        content: 'Remove successful',
                                        type: "info"
                                    });
                                    $scope.GetAllmultipleGroupdataSource.read();
                                    $scope.tableName = "";
                                }
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }
                });
            }
        };
        $scope.AttributeGroup = function () {
            $("#multipletabledesigner").hide();
            $("#multipleattributegroup").show();
            if ($scope.Group.GROUP_ID === 0) {

            } else {
                $scope.renderAttributeGroup = true;
                $scope.renderTableDesignermultiple = false;
                $scope.renderRemoveGroup = false;
                $scope.renderEdit = false;
                $scope.winNewAttributeGorup.refresh({ url: "../views/app/partials/AttributeGroupingPopUp.html" });
                $scope.winNewAttributeGorup.center().open();
            }
        };

        $scope.TableDesigner = function () {
            $scope.renderAttributeGroup = false;
            $scope.renderTableDesignermultiple = true;
            $scope.renderRemoveGroup = false;
            $scope.renderEdit = false;
            $("#multipletabledesigner").show();
            $("#multipleattributegroup").hide();
            $scope.$broadcast("MULTIPLETABLEDESIGNER");

        };
        $scope.Previewlayout = function () {
            $scope.winPreviewlayout.refresh({ url: "../views/app/partials/MultipletablepreviewPopUp.html" });
            $scope.winPreviewlayout.center().open();
        };

        $scope.Group = {
            GROUP_NAME: '',
            GROUP_ID: 0,
            LAYOUT: 'Horizontal'
        };


        $scope.LoadAttributeGroupType = function () {
            $scope.renderfamilyspecsgrid = false;
            $scope.renderfamilydescriptions = false;
            $scope.renderfamilyimages = false;
            $scope.renderrelatedfamily = false;
            $scope.renderclonedcategory = false;
            $scope.rendermultipletable = true;
            $scope.renderEdit = true;
            $rootScope.Family_Data == "GroupType"
            $("#multipletabledesigner").hide();
            $("#multipleattributegroup").show();
            $scope.GetAllmultipleGroupdataSource.read();
        };

        $scope.GetAllmultipleGroupdataSource = new kendo.data.DataSource({
            pageSize: 5,
            type: "json",
            serverFiltering: true, editable: true,
            serverPaging: true, autoBind: false,
            serverSorting: true, pageable: true,
            transport: {
                read: function (options) {
                    dataFactory.GetAllMultiplegroups($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }, update: function (options) {
                    dataFactory.UpdateMultiplegroupName(options.data).success(function (response) {
                        $.msgBox({
                            title: "CatalogStudio",
                            content: '' + response + '',
                            type: "info"
                        });
                        $scope.GetAllmultipleGroupdataSource.read();
                        options.success();
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                    return kendo.stringify(options);

                }
            }, schema: {
                model: {
                    id: "GROUP_ID",
                    fields: {
                        GROUP_ID: { editable: false, nullable: true },
                        GROUP_NAME: { editable: true, validation: { required: true } },
                        CATALOG_ID: { type: "number", editable: false },
                        FAMILY_ID: { editable: false },
                        LAYOUT: { editable: false }
                    }
                }
            }
        });


        $scope.mainGridOptionssd = {
            dataSource: $scope.GetAllmultipleGroupdataSource,
            sortable: true, scrollable: true, pageable: { buttonCount: 5 }, autoBind: false,
            selectable: "row", editable: "inline",
            columns: [{ field: "GROUP_NAME", title: "Group Name" },
            { command: ["edit"], title: "&nbsp;" }],
            detailTemplate: kendo.template($("#templates").html()),
            dataBound: function (e) {
                if (e.sender._data.length > 0) {
                    var row = this.tbody.find('tr:first');
                    this.select(row);
                    $scope.Group.GROUP_ID = e.sender._data[0].GROUP_ID;
                    $scope.Group.LAYOUT = e.sender._data[0].LAYOUT;
                } else {
                    $scope.Group.GROUP_ID = 0;
                }
            }
        };
        $scope.handleChange = function (dataItem) {
            if (dataItem != undefined) {
                $scope.Group.GROUP_ID = dataItem.GROUP_ID;
                $scope.Group.LAYOUT = dataItem.LAYOUT;
            }
        };

        $scope.detailGridmultipleGroupOptions = function (dataItem) {
            return {
                dataSource: {
                    type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.GetAllMultiplegroupsattributes(dataItem.GROUP_ID, $rootScope.selecetedCatalogId).success(function (response) {
                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }, schema: {
                        model: {
                            id: "GROUP_ID",
                            fields: {
                                ATTRIBUTE_ID: { editable: false, nullable: true },
                                ATTRIBUTE_NAME: { editable: false, validation: { required: true } },
                                SORT_ORDER: { type: "number" },
                                ATTRIBUTE_TYPE: { editable: false, nullable: true }
                            }
                        }
                    },
                    serverFiltering: true,
                    serverPaging: true,
                    serverSorting: true,
                    pageable: { buttonCount: 5 },
                },
                scrollable: false,
                sortable: true, autobind: false,
                selectable: "row",
                columns: [{ field: "ATTRIBUTE_NAME", title: "Attribute Name" },
                { field: "ATTRIBUTE_TYPE", title: "ATTRIBUTE TYPE", template: "<span>{{attrtypenames(dataItem.ATTRIBUTE_TYPE) }}</span>", }],
                dataBound: function () {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#gridcategoryfamily").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    }
                    else {
                        this.expandRow(this.tbody.find("tr.k-master-row"));
                        //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                    }
                }
            };
        };

        $scope.attrtypenames = function (e) {
            if (e === 1) {
                return "Product Specifications";
            }
            else if (e === 3) {
                return "Product Image / Attachment";
            }
            else if (e === 4) {
                return "Product Price";
            } else if (e === 7) {
                return "Family Description";
            } else if (e === 6) {
                return "Product Key";
            } else if (e === 9) {
                return "Family Image / Attachment";
            } else if (e === 11) {
                return "Family Attribute";
            }
            else if (e === 12) {
                return "Family Price";
            }
            else if (e === 13) {
                return "Family Key";
            } else {
                return "Product Specifications";
            }
        };


        $scope.multipletableData = [];
        $scope.tblDashBoardMultiple = new ngTableParams({ page: 1, count: 5 },
            {
                //counts: [5, 10, 25, 50, 100], 
                sorting: {
                    SORT: 'asc'     // initial sorting
                },
                total: function () { return $scope.multipletableData.length; },
                $scope: { $data: {} },
                getData: function ($defer, params) {
                    var filteredData = $scope.multipletableData;
                    var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) :
                      filteredData;
                    params.total(orderedData.length);
                    return $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });
        $scope.multipleDatasource = function (data) {
            if (data.sender._old !== "") {
                $scope.group_id = data.sender._old;
                $rootScope.LoadMultipletableData($scope.group_id);
            }
            else {
                $scope.multipletableData = [];
                $scope.columnsMultipletable = [];
                $scope.tblDashBoardMultiple.reload();
            }
        };

        $rootScope.LoadMultipletableData = function (groupid) {
            if (groupid === 0) {
                $scope.multipletableData = [];
                $scope.columnsMultipletable = [];
                $scope.tblDashBoardMultiple.reload();
            } else {
                dataFactory.multipletablespecs($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $rootScope.selecetedCategoryId, groupid).success(function (response) {
                    //var obj = jQuery.parseJSON(response.Data.Data);
                    var obj = jQuery.parseJSON(response.Data.Data);
                    //  $scope.prodData = obj;
                    //  $scope.columnsForAtt = response.Data.Columns;
                    $scope.multipletableData = obj;
                    $scope.columnsMultipletable = response.Data.Columns;
                    $scope.tblDashBoardMultiple.reload();
                }).error(function (error) {
                    options.error(error);
                });
            }
        };
        $scope.blurCheck = false;
        $scope.datatypecheckMultiple = function (event, type, e) {
            $scope.blurCheck = true;
            var value = event.currentTarget.value;
            var datatype = type;
            var pattern = "";
            if (type !== "All Characters" && !type.contains("0-9") && !type.contains("uFFFF")) {
                pattern = /^[a-zA-Z ]+$/;
            }
            else {
                if (type.contains("uFFFF")) {
                    //pattern = /^[ A-Za-z0-9_@./#&@$%"'^*!~*?+-]*$/;
                }
                else {
                    pattern = /^[0-9a-zA-Z ]*$/;
                }
            }
            if (pattern !== "" && value.trim() !== "") {
                var id = event.currentTarget.id;
                if (!pattern.test(value)) {
                    $scope.selectedRow[e] = "";
                    $.msgBox({
                        title: "CatalogStudio",
                        content: 'Please enter valid Data',
                        type: "error"
                    });

                }
            }

        };
        $scope.UpdateMultipletable = function (row) {
            var displayAttribures = "";
            if ($scope.val_type === "true") {
                angular.forEach(row, function (value, key) {
                    if (key.contains("~")) {
                        var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];
                        if (sa != undefined) {
                            if (sa.VALUE_REQUIRED == true && value == '' || sa.ATTRIBUTE_ID == 1 && value == '') {
                                if (displayAttribures == '') {
                                    displayAttribures = "The following attribute values are required,\n";
                                }
                                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                            }
                        }
                    }
                });

                if (displayAttribures == '') {
                    dataFactory.SaveProductSpecs(row, $localStorage.CategoryID, 0, $scope.blurCheck).success(function (response) {
                        if (response != null) {
                            $scope.blurCheck = false;
                            $scope.MultipleTableEditPopupWindow.refresh({ url: "../Views/App/Partials/MultipleTableEditPopup.html" });
                            $scope.MultipleTableEditPopupWindow.center().close();
                            $scope.LoadMultipletableData($scope.group_id);
                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);
                            $.msgBox({
                                title: "CatalogStudio",
                                content: '' + response + '',
                                type: "info"
                            });
                        }

                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: '' + displayAttribures + '',
                        type: "info"
                    });
                }
            }

            else {
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please enter Data using a valid format',
                    type: "error"
                });
            }
        };

        $scope.selectedRowDynamicAttributes = [];
        $scope.selectedRow = {};
        $scope.UpdateMultipletableProductItem = function (data) {
            // $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);
            $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
            //$scope.selectedRow = data.Data;
           // $scope.selectedRow = [{}];
            $scope.updateproductspecs = data.Data;
           
            attributeWithout = [];
            $scope.attributes = Object.keys(data.Data);
            $scope.MultipleTableEditPopupWindow.refresh({ url: "../Views/App/Partials/MultipleTableEditPopup.html" });
            $scope.MultipleTableEditPopupWindow.title("Multiple Table Specification");
          
            if ($scope.catalogids !== 0 && $scope.familyids !== 0) {
                dataFactory.GetAttributeDetails($scope.updateproductspecs.CATALOG_ID, $scope.updateproductspecs.FAMILY_ID).success(function (response) {
                    if (response != null) {
                        $scope.selectedRowAttributes = response;
                        var calcAttributestemp = [];
                        angular.forEach($scope.attributes, function (value, key) {
                            if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT')
                                attributeWithout.push(value);
                        });
                        $scope.attributescalc = attributeWithout;
                        $scope.fromPageNo = 0;
                        $scope.toPageNo = 15;
                        if (attributeWithout.length < 15) {
                            $scope.toPageNo = attributeWithout.length;
                        }
                        var attributestemp = [];
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            attributestemp.push(attributeWithout[i]);
                        }

                        angular.forEach($scope.selectedRowAttributes, function (value, key) {
                            var theString = value.ATTRIBUTE_ID;
                            var uimask = "";
                            value.ATTRIBUTE_READONLY = (value.ATTRIBUTE_READONLY === "true");
                            if (value.ATTRIBUTE_NAME === 'Supplier') {
                                theString = 0;
                            }
                            if (value.ATTRIBUTE_DATATYPE.contains('Num')) {
                                var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                pattern = pattern.replace("numeric", value.NUMERICPLACE);
                                pattern = pattern.replace("decimal", value.DECIMAL);
                                var reg = new RegExp(pattern);
                                uimask = $scope.UIMask(value.NUMERICPLACE, value.DECIMAL);
                                value.attributePattern = reg;
                            } else if (value.ATTRIBUTE_DATATYPE.contains('Text(')) {
                                value.attributePattern = value.DECIMAL;
                                uimask = value.DECIMAL;
                            } else {
                                value.attributePattern = 524288;
                                uimask = 524288;
                            }
                            value.uimask = uimask;
                            $scope.selectedRowDynamicAttributes[theString] = [];
                            $scope.selectedRowDynamicAttributes[theString] = value;
                        });
                      
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            if (attributeWithout[i].contains("OBJ")) {
                                var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];

                                if (sa.USE_PICKLIST) {
                                    var theString = sa.ATTRIBUTE_ID;
                                    $scope.
                                        GetPickListData(theString, sa.PICKLIST_NAME);
                                }
                            }
                        }

                        $timeout(function () {
                            angular.copy($scope.updateproductspecs, $scope.selectedRow);
                            $scope.MultipleTableEditPopupWindow.center().open();
                        },500);
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
        };

        $scope.CancelMultipleTable = function () {
            $scope.MultipleTableEditPopupWindow.refresh({ url: "../Views/App/Partials/MultipleTableEditPopup.html" });
            $scope.MultipleTableEditPopupWindow.center().close();
        };
        //----------------------------------------------------------------------------------------------------------------------------------------------------//

        function groupHeaderName(e) {

            if (e.value === 1) {
                return "Product Specifications (" + e.count + " items)";
            }
            else if (e.value === 3) {
                return "Product Image / Attachment (" + e.count + " items)";
            }
            else if (e.value === 4) {
                return "Product Price (" + e.count + " items)";
            } else if (e.value === 7) {
                return "Family Description (" + e.count + " items)";
            } else if (e.value === 6) {
                return "Product Key (" + e.count + " items)";
            } else if (e.value === 9) {
                return "Family Image / Attachment (" + e.count + " items)";
            } else if (e.value === 11) {
                return "Family Attribute (" + e.count + " items)";
            }
            else if (e.value === 12) {
                return "Family Price (" + e.count + " items)";
            }
            else if (e.value === 13) {
                return "Family Key (" + e.count + " items)";
            } else {
                return "Product Specifications (" + e.count + " items)";
            }
        }
        $scope.familyGetAllCatalogattributesdataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true, pageable: true, pageSize: 5, autoBind: false,
            sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogFamilyAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ISAvailable: { type: "boolean" },
                        CATALOG_ID: { editable: false },
                        FAMILY_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false }
                    }
                }
            },
            group: {
                field: "ATTRIBUTE_TYPE", aggregates: [
                   { field: "ATTRIBUTE_TYPE", aggregate: "count" }
                ]
            }

        });

        $scope.familymainGridOptions = {
            dataSource: $scope.familyGetAllCatalogattributesdataSource,
            pageable: { buttonCount: 5 }, autoBind: false,
            columns: [
                { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>' },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" },
                {
                    field: "ATTRIBUTE_TYPE",
                    title: "Attribute Type",
                    hidden: true,
                    aggregates: ["count"],
                    groupHeaderTemplate: groupHeaderName
                }]
        };

        $scope.updateSelection = function (e, id) {
            id.dataItem.set("ISAvailable", e.currentTarget.checked);
        };
        $scope.familyprodfamilyattrdataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            pageable: true, autoBind: false,
            pageSize: 5, sort: { field: "SORT_ORDER", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.getPublishedfamilyAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $localStorage.CategoryID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ISAvailable: {
                            type: "boolean"
                        },
                        CATALOG_ID: { editable: false },
                        FAMILY_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        SORT_ORDER: { editable: false }
                    }
                }
            }

        });

        $scope.familyselectedGridOptions = {

            dataSource: $scope.familyprodfamilyattrdataSource,
            pageable: { buttonCount: 5 }, autoBind: false,
            selectable: "multiple",
            dataBound: onDataBound,
            columns: [
                { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforprodfamily" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>' },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px" }
            ]
        };

        var updownIndex = null;

        function onDataBound(e) {
            if (updownIndex != null) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {

                    if (updownIndex == i) {
                        var isChecked = this.tbody.find("tr[data-uid='" + view[i].uid + "']").find(".chkbxforprodfamily").attr("checked");
                        if (isChecked) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected");
                        }

                    }
                }

                updownIndex = null;
            }
        }

        $scope.updateSelection1 = function (e, id) {
            id.dataItem.set("ISAvailable", e.currentTarget.checked);
        };

        $scope.UnPublishFamilyAttributes = function () {
            dataFactory.UnPublishFamilyAttributes($scope.familyprodfamilyattrdataSource._data, $localStorage.CategoryID).success(function (response) {
                $scope.familyGetAllCatalogattributesdataSource.read();
                $scope.familyprodfamilyattrdataSource.read();
                //$scope.LoadFamilySpecsData();
                $rootScope.familySpecsDatasource.read();
                $scope.familySpecsImgAttrDatasource.read();
                if ($rootScope.Family_Data == "Image") {
                    $scope.LoadFamilySpecsImage();
                }
                else if ($rootScope.Family_Data == "Desc") {
                    $scope.LoadFamilySpecsDesc();
                }
                else if ($rootScope.Family_Data == "Data") {
                    $scope.LoadFamilySpecsData();
                }
                else if ($rootScope.Family_Data == "FamilyData") {
                    $scope.LoadRelatedFamilyData();
                }
                else if ($rootScope.Family_Data == "GroupType") {
                    $scope.LoadAttributeGroupType();
                }
            }).error(function (error) {
                options.error(error);
            });
            // $scope.LoadFamilySpecsData();
            $scope.winFamilyAttributeSetup.close();
            $("#columnsetupfamily").hide();
            $("#gridfamily").show();
            $rootScope.familySpecsDatasource.read();
            $scope.renderfamilydescriptions = true;
        };
        $scope.RefreshFamilyAttributes = function () {
            $scope.familyGetAllCatalogattributesdataSource.read();
            $scope.familyprodfamilyattrdataSource.read();
            $scope.LoadFamilySpecsData();
            $rootScope.familySpecsDatasource.read();
        };
        $scope.CancelFamilyAttributes = function () {
            $scope.LoadFamilySpecsData();
            $scope.winFamilyAttributeSetup.close();
            $("#columnsetupfamily").hide();
            $("#gridfamily").show();
            $rootScope.familySpecsDatasource.read();
            $scope.renderfamilydescriptions = true;
        };
        //$scope.PublishAttributes = function () {
        //    dataFactory.PublishAttributes($scope.prodfamilyattrdataSource._data).success(function (response) {
        //        $scope.GetAllCatalogattributesdataSource.read();
        //        $scope.prodfamilyattrdataSource.read();
        //    }).error(function (error) {
        //        options.error(error);
        //    });
        //};
        $scope.SavePublishAttributes = function () {

            dataFactory.SaveFamilyPublishAttributes($scope.familyGetAllCatalogattributesdataSource._data, $localStorage.CategoryID, $localStorage.CategoryID).success(function (response) {

                $scope.familyGetAllCatalogattributesdataSource.read();
                $scope.familyprodfamilyattrdataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.DeletePublishAttributes = function () {
            //var g = $scope.prodselectedgrid.select();
            //g.each(function (index, row) {
            //    var selectedItem = $scope.prodselectedgrid.dataItem(row);
            //    dataFactory.DeleteFamilyPublishAttributes(selectedItem, $localStorage.CategoryID).success(function (response) {
            //        $scope.familyGetAllCatalogattributesdataSource.read();
            //        $scope.familyprodfamilyattrdataSource.read();
            //    }).error(function (error) {
            //        options.error(error);
            //    });
            //});


            var g = $scope.prodselectedgrid.select();
            var data = [];
            for (var i = 0; i < g.length; i++) {
                var selectedItem = $scope.prodselectedgrid.dataItem(g[i]);
                data.push(selectedItem);
            }

            dataFactory.DeleteFamilyPublishAttributes(data, $localStorage.CategoryID).success(function (response) {

                $scope.familyGetAllCatalogattributesdataSource.read();
                $scope.familyprodfamilyattrdataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.BtnFamilyMoveUpClick = function () {
            var g = $scope.prodselectedgrid.select();
            if (g.length === 1) {
                g.each(function (index, row) {
                    var selectedItem = $scope.prodselectedgrid.dataItem(row);
                    var data = $scope.prodselectedgrid.dataSource.data();
                    var dataRows = $scope.prodselectedgrid.items();
                    var selectedRowIndex = dataRows.index(g);
                    //var sortOrder = data[selectedRowIndex - 1].SORT_ORDER;
                    dataFactory.BtnFamilyMoveUpClick(selectedItem.SORT_ORDER - 1, selectedItem, $rootScope.selecetedCategoryId).success(function (response) {
                        $scope.familyGetAllCatalogattributesdataSource.read();
                        $scope.familyprodfamilyattrdataSource.read();
                        updownIndex = selectedRowIndex - 1;
                    }).error(function (error) {
                        options.error(error);
                    });
                });
            } else {
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please select at least one Attribute',
                    type: "error"
                });
            }
        };


        $scope.BtnFamilyMoveDownClick = function () {

            var g = $scope.prodselectedgrid.select();
            if (g.length === 1) {
                g.each(function (index, row) {
                    var selectedItem = $scope.prodselectedgrid.dataItem(row);
                    var data = $scope.prodselectedgrid.dataSource.data();
                    var dataRows = $scope.prodselectedgrid.items();
                    var selectedRowIndex = dataRows.index(g);
                    //  var sortOrder = data[selectedRowIndex + 1].SORT_ORDER;
                    dataFactory.BtnFamilyMoveDownClick(selectedItem.SORT_ORDER + 1, selectedItem, $rootScope.selecetedCategoryId).success(function (response) {
                        $scope.familyGetAllCatalogattributesdataSource.read();
                        $scope.familyprodfamilyattrdataSource.read();
                        updownIndex = selectedRowIndex + 1;
                    }).error(function (error) {
                        options.error(error);
                    });
                });
            } else {
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please select at least one Attribute',
                    type: "error"
                });


            }
        };





        ///////////////////// Table Designer /////////////////////////////////////////////////////////
        $scope.userRoleAddRelatedFamily = false;
        $scope.userRoleModifyRelatedFamily = false;
        $scope.userRoleDeleteRelatedFamily = false;
        $scope.userRoleViewRelatedFamily = false;

        $scope.getUserRoleRightsRelatedFamily = function () {
            var id = 4020302;
            dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddRelatedFamily = response[0].ACTION_ADD;
                    $scope.userRoleModifyRelatedFamily = response[0].ACTION_MODIFY;
                    $scope.userRoleDeleteRelatedFamily = response[0].ACTION_REMOVE;
                    $scope.userRoleViewRelatedFamily = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.init = function () {
            $scope.getUserRoleRightsRelatedFamily();
        };

        $scope.init();


        //--------------------------------Start Cloned Category-------------------------------------

        $scope.LoadClonedCategory = function () {
            if ($scope.SelectedDataItem.ID !== 0 && $scope.SelectedDataItem.CATALOG_ID !== 0) {
                
                $scope.renderfamilyspecsgrid = false;
               // $scope.renderfamilydescriptions = false;
                $scope.renderfamilyimages = false;
                $scope.renderrelatedfamily = false;
                $scope.renderclonedcategory = true;
                $scope.rendermultipletable = false;
                $scope.originalCategoryDatasource.read();
                $scope.clonedCategoryDatasource.read();
            }

        };

        $scope.ngclkdelete = function (e) {
            var familyid = e.dataItem.FAMILY_ID;
            $.msgBox({
                title: "Are You Sure",
                content: "Do You want to remove clone?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }, { value: "Cancel" }],
                success: function (result) {
                    if (result == "Yes") {
                        dataFactory.DeletecloneFamily(familyid, $rootScope.selecetedCatalogId, e.dataItem.CATEGORY_ID).success(function (response) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: '' + response + '',
                                type: "info"
                            });
                            $rootScope.treeData.read();
                            //$scope.treeData.read();
                            var tabstrips = $("#tabstrip").data("kendoTabStrip");
                            var myTabs = tabstrips.tabGroup.children("li").eq(0);
                            tabstrips.select(myTabs);
                            var myTabsfamily = tabstrips.tabGroup.children("li").eq(1);
                            tabstrips.disable(myTabsfamily);
                            var myTabsproducts = tabstrips.tabGroup.children("li").eq(2);
                            tabstrips.disable(myTabsproducts);
                        }).error(function (error) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: '' + error + '',
                                type: "info"
                            });
                        });
                    }
                }
            });

        };

        $scope.ngclkswitchmaster = function (e) {
            var familyid = e.dataItem.FAMILY_ID;
            var catalogid = e.dataItem.CATALOG_ID;
            dataFactory.CheckmasterFamily(familyid, $rootScope.selecetedCatalogId, e.dataItem.CATEGORY_ID).success(function (response) {
                if (response !== "") {
                    $.msgBox({
                        title: "Are You Sure",
                        content: response,
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }, { value: "Cancel" }],
                        success: function (result) {
                            if (result == "Yes") {
                                dataFactory.SwitchMasterFamily(familyid, catalogid, e.dataItem.CATEGORY_ID).success(function (response1) {
                                    $.msgBox({
                                        title: "CatalogStudio",
                                        content: '' + response1 + '',
                                        type: "info"
                                    });
                                    $rootScope.treeData.read();
                                    //$scope.treeData.read();
                                    var tabstrips = $("#tabstrip").data("kendoTabStrip");
                                    var myTabs = tabstrips.tabGroup.children("li").eq(0);
                                    tabstrips.select(myTabs);

                                    var myTabsfamily = tabstrips.tabGroup.children("li").eq(1);
                                    tabstrips.disable(myTabsfamily);

                                    var myTabsproducts = tabstrips.tabGroup.children("li").eq(2);
                                    tabstrips.disable(myTabsproducts);
                                }).error(function (error) {
                                    $.msgBox({
                                        title: "CatalogStudio",
                                        content: '' + error + '',
                                        type: "error"
                                    });
                                });
                            }
                        }
                    });
                }
            }).error(function (error) {
                $.msgBox({
                    title: "CatalogStudio",
                    content: '' + error + '',
                    type: "error"
                });
            });

        };
        $scope.originalCategoryDatasource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.GetOriginalCategory($scope.SelectedDataItem.ID, $scope.SelectedDataItem.CATALOG_ID, 'ORIGINAL', options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, schema: {
                model: {
                    fields: {
                        CATEGORY_ID: { editable: false }, CATALOG_NAME: { editable: false },
                        CATEGORY_NAME: { editable: false },
                        CATEGORY_LEVEL: { editable: false }

                        //CATEGORY_SHORT: { editable: false }

                    }
                }
            }
        });



        $scope.orginalCategoryGridOptions = {
            dataSource: $scope.originalCategoryDatasource,
            autoBind: false, selectable: true,
            columns: [//{ field: "FAMILY_NAME", title: " ", width: "80px", template: "<a title='FamilyName'  class='k-link' href='javascript:void(0);'   ng-click='ngclkfamilyname(this)'>Show</a>" },
                { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
                { field: "CATEGORY_SHORT", title: "Category ID", width: "180px" },
                { field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
                { field: "CATEGORY_LEVEL", title: "Category Level", width: "180px" }],
            editable: false
        };

        $scope.clonedCategoryDatasource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.GetOriginalCategory($scope.SelectedDataItem.ID, $scope.SelectedDataItem.CATALOG_ID, 'CLONE', options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, schema: {
                model: {
                    fields: {
                        CATEGORY_ID: { editable: false },
                        CATALOG_NAME: { editable: false },
                        CATEGORY_NAME: { editable: false },
                        CATEGORY_LEVEL: { editable: false }

                    }
                }
            }
        });


        $scope.clonedcategoryshow = false;
        $scope.clonedCategoryGridOptions = {
            dataSource: $scope.clonedCategoryDatasource,
            autoBind: false, selectable: true,
            columns: [//{ field: "FAMILY_NAME", title: " ", width: "80px", template: "<a title='FamilyName'  class='k-link' href='javascript:void(0);'   ng-click='ngclkfamilyname(this)'>Show</a>" },
                { field: "FAMILY_NAME", title: " ", width: "80px", template: "<a title='FamilyName'  class='k-link' href='javascript:void(0);'  ng-click='ngclkdelete(this)'>Delete</a>" },
                { field: "FAMILY_NAME", title: "Switch Master", width: "90px", template: "<button class='btn btn-primary' ng-disabled=#=ISSUBFAMILY#  ng-click='ngclkswitchmaster(this)'>Master</button>" },
                { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
                { field: "CATEGORY_SHORT", title: "Category ID", width: "180px" },
                { field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
                { field: "CATEGORY_LEVEL", title: "Category Level", width: "180px" }
            ],
            editable: false,
            dataBound: function (e) {
                if (e.sender._data.length > 0) {
                    $scope.clonedcategoryshow = true;
                } else {
                    $scope.clonedcategoryshow = false;
                }
            }
        };
        $scope.OpenFamilyDefaultTab = function () {
            $rootScope.Dashboards = false;
        }

        $scope.OpenProductDefaultTab = function () {

            $rootScope.Dashboards = true;
            $rootScope.inverted = '';
            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1270px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1270px !important;");
                //$('#sampleProdgrid').css('max-width', '1220px', '!important');
                //$('#sampleProdgrid')[0].style.maxWidth='1220px !important';
                //$('#sampleProdgrid tbody').css('max-width', '1220px', '!important');
                //$('#sampleProdgrid tbody')[0].style.maxWidth = '1220px !important';
            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");
            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1520px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1520px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1590) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1210px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1210px !important;");

            }

            if (window.screen.availWidth >= 1600 && window.screen.availWidth <= 1800) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1420px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1420px !important;");

            }

            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
                $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

            }

            /*Sub Product*/

            if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
                $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1270px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1270px !important;");

            }
            if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
                $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");
            }

            if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
                $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1810px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1810px !important;");
            }

            if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
                $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");

            }
            if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
                $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
                $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

            }
            var tabstrips = $("#productgridtabstrip").data("kendoTabStrip");
            var myTabs = tabstrips.tabGroup.children("li").eq(0);
            tabstrips.select(myTabs);
            $scope.OpenProductDefault();
        };
        $scope.OpenProductDefault = function () {
            $("#productgridtabstrip").show();
            $("#divProductGrid").show();
            if ($rootScope.Mainprodcount > 0) {
                $("#productpaging1").show();
            }
            $("#gridproduct").show();
            $("#productpreview").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();
            $("#cmnvalupdate").hide();
            $('#mainProductsMenu').show();
            $('#subProductsMenu').hide();
            $('#InvertedProductsTab').hide();
            $("#Familyimport").hide();
        };
        $scope.loadInvertedProducts = function () {
            $scope.gridCurrentPage = "10";
            $('#InvertedProductsTab').show();
        };
        //--------------------------------End Cloned Category---------------------------------
        //-------------------------------Start Multiple table edit---------------------------
        $scope.Edit = function () {
            $scope.renderAttributeGroup = false;
            $scope.renderTableDesignermultiple = false;
            $scope.renderRemoveGroup = false;
            $scope.renderEdit = true;
            $scope.GetAllmultipleGroupdataSource.read();
            $rootScope.LoadMultipletableData(0);
            $('#multipletabledrp').data('kendoDropDownList').value(-1);
            $("#EditMultipletable").show();
            $("#divFmlyTab").hide();
        };
        $scope.BackMultipleTable = function () {
            $("#EditMultipletable").hide();
            $("#divFmlyTab").show();
        };
        $scope.attrid = 0;
        $scope.picklistname = "";
        //$scope.selectedRowDynamic = [];

        //$scope.GetPickListData = function (attrName, picklistdata) {
        //    dataFactory.getPickListData(picklistdata).success(function (response) {
        //            $scope.selectedRowDynamic[attrName] = response;
        //            $scope.selectedFamilyRowDynamic[attrName] = response;
        //    }).error(function (error) {
        //        options.error(error);
        //    });
        //};

         $scope.selectedRowDynamic = [];
         $scope.GetPickListDatas = function (attrName, picklistdata) {
            
             dataFactory.getPickListData(picklistdata).success(function (response) {
                
                $scope.selectedRowDynamic[attrName] = response;
                return response;
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.PicklistCreate = function (attrids, picklistname) {
           
            $scope.attrid = attrids;
            $scope.picklistname = picklistname;
            $scope.SelectedPickList = picklistname;
           
            $scope.GetPickListDatas($scope.attrid, $scope.picklistname);
           
            $("#productattrpicklist").show();
            $scope.productattrpicklist.center().open();
            $scope.picklistLoad(picklistname);

        };
        $scope.Closepopup = function () {
            $scope.GetPickListDatas($scope.attrid, $scope.picklistname);
        };

        $scope.PickListDataType = {
            PICKLIST_DATA_TYPE: ''
        };

        $scope.NewPickListName = {
            PICKLIST_NAME: ''
        };
        $("#PickListValues").hide();
        $("#PickListDateValues").hide();
        $scope.picklistChange = function (pkName) {
            $scope.SelectedPickList = pkName.sender._old;
            $scope.picklistLoad($scope.SelectedPickList);
        };
        $scope.picklistLoad = function (pkName) {

            $http.get("../PickList/LoadSelectedPickList?name=" + pkName).
               then(function (details) {
                   $scope.PickListDataType.PICKLIST_DATA_TYPE = details.data.PICKLIST_DATA_TYPE;
                   $scope.PicklistData.PICKLIST_DATA = details.data.PICKLIST_DATA;
                   $scope.selecetedPLXMLData = details.data.PICKLIST_DATA;
                   if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'datetime') {
                       var grid = $("#PickListDateValuesgrid").data("kendoGrid");
                       grid.dataSource.page(1);
                       $scope.GetPickListDateValuesDatasource.read();
                       $("#PickListValues").hide();
                       $("#PickListDateValues").show();
                       $("#PickListNumberValues").hide();
                   } else if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'numeric') {
                       var grid2 = $("#PickListNumberValuesgrid").data("kendoGrid");
                       grid2.dataSource.page(1);
                       $("#PickListValues").hide();
                       $("#PickListDateValues").hide();
                       $("#PickListNumberValues").show();
                       $scope.GetPickListNumericValuesDatasource.read();
                   } else {
                       var grid3 = $("#PickListValuesgrid").data("kendoGrid");
                       grid3.dataSource.page(1);
                       $("#PickListValues").show();
                       $("#PickListDateValues").hide();
                       $("#PickListNumberValues").hide();
                       $scope.GetPickListValuesDatasource.read();
                   }
               });
        };
        $scope.PickListEntries = new kendo.data.ObservableArray([]);
        $scope.PickListGroup = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.getpicklistall('').success(function (response) {

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        $scope.addPickListValue = function () {

            $scope.PickListEntries.push({ Value: $scope.PicklistValue });
            $scope.PicklistValue = '';
        };
        $scope.gridColumns = [
        { field: "Value", title: "Value" }
        ];
        $scope.addNewPicklist = function () {
            if ($scope.NewPickListName.PICKLIST_NAME.trim() == "") {
                $.msgBox({
                    title: "CatalogStudio",
                    content: 'Please enter a valid Name',
                    //type: "info"
                });

            }
            else {
                if ($scope.SelectedPickListDataType !== "") {
                    $http.get("../PickList/SaveNewPickList?Name=" + $scope.NewPickListName.PICKLIST_NAME.trim() + "&&Datatype=" + $scope.SelectedPickListDataType).
                        then(function (request) {
                            $scope.PickListGroup.read();
                            if (request.data != "") {
                                $.msgBox({
                                    title: "CatalogStudio",
                                    content: 'Save successful',
                                    type: "info"
                                });
                            }
                            else {
                                $.msgBox({
                                    title: "CatalogStudio",
                                    content: 'Name already exists, please enter a different name for the Picklist',
                                    type: "error"
                                });

                                $scope.NewPickListName.PICKLIST_NAME = "";
                            }
                        });
                } else {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: 'Please select datatype.',
                        type: "error"
                    });
                }
            }
        };
        $scope.PickListDefaultDatatype = [{
            id: 1,
            DATA_TYPE: "Text"
        },
        {
            id: 2,
            DATA_TYPE: "Numeric"
        },
        {
            id: 3,
            DATA_TYPE: "Hyperlink"
        },
        {
            id: 3,
            DATA_TYPE: "Date and Time"
        }];

        $scope.SelectedPickListDataType = "Text";
        $scope.SelectedPickList = "";
        $scope.DataTypeChanged = function (pkDatatype) {

            if (pkDatatype.sender._old === "Date and Time") {
                $scope.SelectedPickListDataType = "DATETIME";
            } else {
                $scope.SelectedPickListDataType = pkDatatype.sender._old;
            }
        };
        $scope.PicklistData = {
            PICKLIST_DATA: ''
        };
        $("#PickListValues").hide();
        $("#PickListDateValues").hide();
        $("#PickListNumberValues").hide();
        $scope.picklistChange = function (pkName) {
            $scope.SelectedPickList = pkName.sender._old;
            if ($scope.SelectedPickList !== "") {
                $http.get("../PickList/LoadSelectedPickList?name=" + $scope.SelectedPickList).
                    then(function (details) {
                        $scope.PickListDataType.PICKLIST_DATA_TYPE = details.data.PICKLIST_DATA_TYPE;
                        $scope.PicklistData.PICKLIST_DATA = details.data.PICKLIST_DATA;
                        $scope.selecetedPLXMLData = details.data.PICKLIST_DATA;
                        if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'datetime') {
                            $scope.GetPickListDateValuesDatasource.read();
                            $("#PickListValues").hide();
                            $("#PickListDateValues").show();
                            $("#PickListNumberValues").hide();
                        } else if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'numeric') {
                            $("#PickListValues").hide();
                            $("#PickListDateValues").hide();
                            $("#PickListNumberValues").show();
                            $scope.GetPickListNumericValuesDatasource.read();
                        } else {
                            $("#PickListValues").show();
                            $("#PickListDateValues").hide();
                            $("#PickListNumberValues").hide();
                            $scope.GetPickListValuesDatasource.read();
                        }
                    });
            } else {
                $("#PickListValues").hide();
                $("#PickListDateValues").hide();
                $("#PickListNumberValues").hide();
            }
        };

        $scope.selecetedPLXMLData = "";
        $scope.GetPickListValuesDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true, editable: true,
            serverPaging: false, autoBind: false,

            serverSorting: true, pageable: false, sort: { field: "ListItem", dir: "asc" },
            transport: {
                read: function (options) {

                    dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: 'Duplicate Picklist values are not allowed',
                                type: "error"
                            });
                            $scope.GetPickListValuesDatasource.read();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }, destroy: function (options) {
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: 'Duplicate Picklist values are not allowed',
                                type: "error"
                            });
                            $scope.GetPickListValuesDatasource.read();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                create: function (options) {
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: 'Duplicate Picklist values are not allowed',
                                type: "error"
                            });
                            $scope.GetPickListValuesDatasource.read();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            }, batch: true, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ListItem",
                    fields: {
                        ListItem: { editable: true }
                    }
                }
            }

        });
        $scope.GetPickListDateValuesDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            editable: true,
            serverPaging: false,
            serverSorting: true, autoBind: false,
            pageable: false, sort: { field: "ListItem", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: 'Duplicate Picklist values are not allowed',
                                type: "error"
                            });
                            $scope.GetPickListDateValuesDatasource.read();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });

                },
                create: function (options) {
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: 'Duplicate Picklist values are not allowed',
                                type: "error"
                            });
                            $scope.GetPickListDateValuesDatasource.read();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });

                }, destroy: function (options) {
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: 'Duplicate Picklist values are not allowed',
                                type: "error"
                            });
                            $scope.GetPickListDateValuesDatasource.read();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            }, batch: true, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ListItem",
                    fields: {
                        ListItem: { editable: true, type: "date" }
                    }
                }
            }

        });
        $scope.GetPickListNumericValuesDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            editable: true,
            serverPaging: false,
            serverSorting: true, autoBind: false,
            pageable: false, sort: { field: "ListItem", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {
                        options.success(response);

                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: 'Duplicate Picklist values are not allowed',
                                type: "error"
                            });
                            $scope.GetPickListNumericValuesDatasource.read();

                        }
                    }).error(function (error) {
                        options.error(error);
                    });

                },
                create: function (options) {
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: "CatalogStudio",
                                content: 'Duplicate Picklist values are not allowed',
                                type: "error"
                            });
                            $scope.GetPickListNumericValuesDatasource.read();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });

                }, destroy: function (options) {
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            }, batch: true, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ListItem",
                    fields: {
                        ListItem: { editable: true, type: "number" }
                    }
                }
            }

        });
        $scope.GetPickListNumberValues = {
            dataSource: $scope.GetPickListNumericValuesDatasource,
            sortable: true,
            scrollable: true, filterable: true, autoBind: false,
            pageable: {
                numeric: false,
                previousNext: false,
                messages: {
                    display: "Total PickList Values : {2}"
                }
            }, editable: {
                mode: "inline",
                createAt: "bottom"
            },
            toolbar: [{ name: "create", text: "", template: "<button class=\'k-button k-grid-add k-item \'>Add</button>" },
            ],
            //   selectable: "row",
            columns: [{
                field: "ListItem",
                title: "Pick List Value",
                editor: function (container, options) {
                    $scope.name = '';
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'numeric') {
                        var input = $('<input name="' + options.field + '" required="required"/>');
                        input.appendTo(container);
                        input.kendoNumericTextBox();
                    }
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'text') {
                        var input = $('<input name="' + options.field + '" type="text" required="required"/>');
                        input.appendTo(container);
                    }
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'hyperlink') {
                        var input = $('<input name="' + options.field + '" type="text"  onchange="TextBoxChanged(this)" id="link" required="required"/>');
                        input.appendTo(container);
                    }
                    //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                    var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                    tooltipElement.appendTo(container);
                }
            }, {
                //   command: [
                //    { name: "save", text: "", width: "10px", template: "<a ng-click='updateNumeric(this)' class=\'k-grid-save-changes k-item girdicons\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>" },
                //{ name: "cancel", text: "", width: "10px", template: "<a ng-click='cancelNumeric(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
                //{ name: "destroy", text: "", width: "10px", template: "<a  class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove  blue btn-xs-icon\'></div></a>" }], title: "Actions"
                command: ["edit", "destroy"], width: "250px"
                , title: "Actions"
            }],
            dataBound: function (e) {
                if (e.sender._data.length > 0) {
                    var sort = e.sender._data[0].Sort;
                    if (sort != undefined) {
                        if (sort != "asc") {
                            if (e.sender.dataSource.sort()[0].dir != "desc") {
                                e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                            }
                        }
                    }
                }
            }
        };
        $scope.GetPickListDateValues = {
            dataSource: $scope.GetPickListDateValuesDatasource,
            sortable: true,
            scrollable: true, filterable: true, autoBind: false,
            pageable: {
                numeric: false,
                previousNext: false,
                messages: {
                    display: "Total PickList Values : {2}"
                }
            }, editable: {
                mode: "inline",
                createAt: "bottom"
            },
            toolbar: [{ name: "create", text: "", template: "<button class=\'k-button k-grid-add k-item \'>Add</button>" }
            ],
            // selectable: "row",
            columns: [{
                field: "ListItem", title: "Pick List Value", format: "{0:MM/dd/yyyy hh:mm tt}", editor: function (container, options) {
                    //create input element and add the validation attribute
                    $scope.name = '';
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'datetime') {
                        var input = $('<input kendo-date-time-picker name="' + options.field + '" k-interval=10 required="required" />');
                        input.appendTo(container);
                    }
                    //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                    var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                    tooltipElement.appendTo(container);
                }, filterable: {
                    ui: function (element) {
                        element.kendoDatePicker({
                            format: "{0:MM/dd/yyyy}"
                        });
                    }
                }
            }, {
                command: ["edit", "destroy"], width: "250px"
                , title: "Actions"
                //   command: [
                //    { name: "save", text: "", width: "10px", template: "<a ng-click='updateDate(this)' class=\'k-grid-save-changes k-item girdicons\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>" },
                //{ name: "cancel", text: "", width: "10px", template: "<a ng-click='cancelDate(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
                //{ name: "destroy", text: "", width: "10px", template: "<a  class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove  blue btn-xs-icon\'></div></a>" }], title: "Actions"
            }],
            dataBound: function (e) {
                if (e.sender._data.length > 0) {
                    var sort = e.sender._data[0].Sort;
                    if (sort != undefined) {
                        if (sort != "asc") {
                            if (e.sender.dataSource.sort()[0].dir != "desc") {
                                e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                            }
                        }
                    }
                }
            }
        };
        $scope.DeletePickList = function () {
            dataFactory.DeletePickList($scope.SelectedPickList).success(function (response) {
                $.msgBox({
                    title: "CatalogStudio",
                    content: '' + response + '',
                    type: "info"
                });
                $scope.selecetedPLXMLData = "";
                $scope.SelectedPickList = "";
                $scope.PickListDataType.PICKLIST_DATA_TYPE = "";
                $("#PickListValues").hide();
                $("#PickListDateValues").hide();
                $("#PickListNumberValues").hide();
                $scope.PickListGroup.read();
            }).error(function (error) {
                $.msgBox({
                    title: "CatalogStudio",
                    content: '' + error + '',
                    type: "info"
                });
            });
        };




        $scope.GetPickListValues = {
            dataSource: $scope.GetPickListValuesDatasource,
            sortable: true,
            scrollable: true, filterable: true, autoBind: false,
            editable: "inline",

            pageable: {
                numeric: false,
                previousNext: false,
                messages: {
                    display: "Total PickList Values : {2}"
                }
            },
            editable:
                {
                    mode: "inline",

                    createAt: "bottom"
                },
            toolbar: [{ name: "create", text: "", template: "<button class=\'k-button k-grid-add k-item \'>Add</button>" }

            ],
            //  selectable: "row",
            columns: [{

                field: "ListItem", title: "Pick List Value", editor: function (container, options) {
                    //create input element and add the validation attribute
                    $scope.name = '';
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'numeric') {
                        var input = $('<input name="' + options.field + '" data-required-msg="Picklist value is mandatory." required="required"/>');
                        input.appendTo(container);
                        input.kendoNumericTextBox();
                    }
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'text') {
                        var input = $('<input name="' + options.field + '" data-required-msg="Picklist value is mandatory." type="text" required="required"/>');
                        input.appendTo(container);
                    }
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'hyperlink') {
                        var input = $('<input name="' + options.field + '" type="text" data-required-msg="Picklist value is mandatory." onchange="TextBoxChanged(this)" id="link" required="required"/>');
                        input.appendTo(container);
                    }
                    //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                    var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                    tooltipElement.appendTo(container);
                }
            }, {
                //   command: [
                //    { name: "save", text: "", width: "10px", template: "<a  ng-click='update(this)' class=\'k-grid-save-changes k-item girdicons k-state-disabled\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>", hide: false },
                //{ name: "cancel", text: "", width: "10px", template: "<a style='display:none' ng-click='cancel(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
                //{
                //    name: "destroy", text: "", width: "10px", template: "<a  class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove  blue btn-xs-icon\'></div></a>"
                //}]
                command: ["edit", "destroy"], width: "250px"
                , title: "Actions"
            }],
            dataBound: function (e) {
                if (e.sender._data.length > 0) {
                    var sort = e.sender._data[0].Sort;
                    if (sort != undefined) {
                        if (sort != "asc") {
                            if (e.sender.dataSource.sort()[0].dir != "desc") {
                                e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                            }
                        }
                    }
                }
            },
        };

        $scope.update = function (e) {
            dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data).success(function (response) {
                // options.success(response);
                if (response == false) {

                    $.msgBox({
                        title: "CatalogStudio",
                        content: 'Duplicate Picklist values are not allowed',
                        type: "error"
                    });

                    $scope.GetPickListValuesDatasource.read();
                }
            }).error(function (error) {
                // options.error(error);
            });
        }
        $scope.updateNumeric = function (e) {

            dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                // options.success(response);
                if (response == false) {

                    $.msgBox({
                        title: "CatalogStudio",
                        content: 'Duplicate Picklist values are not allowed',
                        type: "error"
                    });

                    $scope.GetPickListValuesDatasource.read();
                }
            }).error(function (error) {
                // options.error(error);
            });
        }

        $scope.updateDate = function (e) {

            dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                // options.success(response);
                if (response == false) {

                    $.msgBox({
                        title: "CatalogStudio",
                        content: 'Duplicate Picklist values are not allowed',
                        type: "error"
                    });

                    $scope.GetPickListValuesDatasource.read();
                }
            }).error(function (error) {
                // options.error(error);
            });
        }

        $scope.cancel = function (e) {

            $scope.GetPickListValuesDatasource.read();
        }
        $scope.cancelNumeric = function (e) {

            $scope.GetPickListNumericValuesDatasource.read();
        }
        $scope.cancelDate = function (e) {
            $scope.GetPickListDateValuesDatasource.read();
        }
        $scope.userRoleAddPickList = false;
        $scope.userRoleModifyPickList = false;
        $scope.userRoleDeletePickList = false;
        $scope.userRoleViewPickList = false;
        $scope.getUserRoleRightsPickList = function () {
            var id = 40205;

            dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddPickList = response[0].ACTION_ADD;
                    $scope.userRoleModifyPickList = response[0].ACTION_MODIFY;
                    $scope.userRoleDeletePickList = response[0].ACTION_REMOVE;
                    $scope.userRoleViewPickList = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };

    }]);