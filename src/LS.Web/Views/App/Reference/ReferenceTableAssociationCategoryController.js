﻿
LSApp.controller("ReferenceTableAssociationCategoryController", ["$scope", "$window", "$location", "$routeParams", "dataFactory", '$http', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $http,$rootScope, $localStorage) {
    // --------------------------------------------------- Start Common Function --------------------------------------------------------------------------------
    
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;


    $scope.ReferenceavailabletabledataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        sort: { field: "TABLE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetAllReferenceTableAssociationCategory($scope.SelectedCatalogId, $scope.RightClickedCategoryId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "TABLE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    TABLE_ID: { editable: false },
                    TABLE_NAME: { editable: false }
                }
            }
        }

    });

    $scope.referenceavailabletableGridOptions = {
        dataSource: $scope.ReferenceavailabletabledataSource,
        columns: [
            { field: "ISAvailable", width: "50px", title: "<input type='checkbox' id='chkSelectAll' ng-click='checkAll($event, this)'/>", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>' },
            { field: "TABLE_NAME", title: "Table Name", width: "250px" }]
    };

    $scope.updateSelection = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };


    $scope.ReferenceselectedtabledataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        sort: { field: "TABLE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetselectedReferenceTableAssociationCategory($scope.SelectedCatalogId, $scope.RightClickedCategoryId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "TABLE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    TABLE_ID: { editable: false },
                    TABLE_NAME: { editable: false }
                }
            }
        }

    });

    $scope.referenceselectedtableOptions = {
        dataSource: $scope.ReferenceselectedtabledataSource,
        selectable: "multiple",
        columns: [
            { field: "TABLE_NAME", title: "Table Name", width: "250px" }]
    };
    $scope.updateSelection1 = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };
    $scope.SavePublishAttributes = function () {
        dataFactory.SavePublishReferenceTableCategory($scope.ReferenceavailabletabledataSource._data, $scope.SelectedCatalogId, $scope.RightClickedCategoryId).success(function (response) {
            $scope.ReferenceavailabletabledataSource.read();
            $scope.ReferenceselectedtabledataSource.read();
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.DeletePublishAttributes = function () {
        var g = $scope.referenceselectedtable.select();
        g.each(function (index, row) {
            var selectedItem = $scope.referenceselectedtable.dataItem(row);
            dataFactory.DeletePublishedReferenceTableCategory(selectedItem, $scope.SelectedCatalogId, $scope.RightClickedCategoryId).success(function (response) {
                $scope.ReferenceavailabletabledataSource.read();
                $scope.ReferenceselectedtabledataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        });

    };

    $scope.checkAll = function (e) {
        var state = e.currentTarget.checked;
        //  var grid = $scope.referenceavailabletable;
        $.each($scope.referenceavailabletable.dataSource._data, function () {
            if (this['ISAvailable'] != state)
                this.dirty = true;
            this['ISAvailable'] = state;
        });
        //$(".chkbxforallattributes").each(function () {
        //    $(this).prop("checked", true);
        //});
        $scope.referenceavailabletable.refresh();
    };

    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $scope.SelectedItem = 0;
            $scope.SelectedCatalogId = 0;
        }
        else {
            $scope.SelectedItem = $localStorage.getCatalogID;
            $scope.SelectedCatalogId = $localStorage.getCatalogID;
        }
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerID = 0;

        }
        else {
            $scope.getCustomerID = $localStorage.getCustomerID;
        }
    };
    $scope.init();
}]);
