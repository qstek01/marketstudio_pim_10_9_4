﻿
LSApp.controller('ReferencetableController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$filter', 'NgTableParams', '$compile', 'productService', '$localStorage', 'blockUI', '$rootScope',
function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $filter, NgTableParams, $compile, productService, $localStorage, blockUI, $rootScope) {

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

    $scope.IsDisabled = true;
    $scope.UploadFile = null;
    $scope.enableReferenceImport = false;
    $scope.clearImage = false;

    //------ Multiple Table-------------------------------------------------------------------------------------------------------------------------------//

    $scope.formSubmit = false;
    $scope.Table = {
        TABLE_NAME: '',
        TABLE_ID: 0,
        ImagePreview: ""
    };
    if ($localStorage.getCustomerCompanyName == undefined) {
        dataFactory.GetCurretUserInfo().success(function (response) {
            $rootScope.currentUser = response;
            $localStorage.getCustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
            $localStorage.getUserName = $rootScope.currentUser.Username;
            $localStorage.getCustomerCompanyName = $rootScope.currentUser.CustomerDetails.Comments;
            $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
        });
    }
    else {
        $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
    }
    $scope.tableName = "";
    $scope.cellId = '';
    $scope.btnAddTableName = function () {
        $scope.formSubmit = true;
        // for (var i = 0; i < counter; i++) {
        //  var newTableName = $scope.data.fields[i].values;
        if ($scope.tableName.trim() != "")
        {
            $scope.formSubmit = false;
            dataFactory.createNewReferencetable($scope.tableName, $localStorage.getCustomerID).success(function (response) {
                if (response != null) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        //type: "info"
                    });
                    $scope.GetAllreferencetabledataSource.read();
                    $scope.ReferenceTableDataSource.read();
                    $scope.tableName = "";
                }

            }).error(function (error) {
                options.error(error);
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a Table Name.',
                //type: "info"
            });


        }
        // }
    };
    //$scope.RefreshGorup = function () {
    //    $scope.GetAllreferencetabledataSource.read();
    //};
    //$scope.RemoveGorup = function () {
    //    if ($scope.Table.TABLE_ID === 0) {
    //        alert("Please select one Group to delete");
    //    } else {
    //        var r = confirm("Please Confirm. Do you want to delete this Attribute group?");
    //        if (r == true) {
    //            dataFactory.RemoveReferencetable($scope.Table.TABLE_ID).success(function (response) {
    //                if (response != null) {
    //                    alert("Removed Successfully");
    //                    $scope.GetAllreferencetabledataSource.read();
    //                    $scope.tableName = "";
    //                }
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //    }
    //};

    $scope.GetAllreferencetabledataSource = new kendo.data.DataSource({
        pageSize: 10,
        type: "json",
        serverFiltering: true, editable: true,
        serverPaging: true,
        serverSorting: true, pageable: true,
        autoBind: false,
        loadOnDemand: true,
        transport: {
            read: function (options) {
                dataFactory.GetAllReferenceTableGrid($rootScope.selecetedCatalogId, $localStorage.getCustomerID, options.data).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }, update: function (options) {
                debugger;
                dataFactory.UpdateReferenceTableName(options.data).success(function (response) {
                    if (response != null) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Table Name updated successfully.',
                            type: "info"
                        });



                    } else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Table Name should be Unique.',
                            // type: "info"
                        });
                        $scope.GetAllreferencetabledataSource.read();
                    }

                    $scope.ReferenceTableDataSource.read();
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });

            }, destroy: function (options) {
                dataFactory.RemoveReferencetable(options.data.TABLE_ID).success(function (response) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
                return kendo.stringify(options);

            }
        }, schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "TABLE_ID",
                fields: {
                    TABLE_ID: { editable: false,type: "number", nullable: true },
                    TABLE_NAME: { editable: true, validation: { required: true } }
                }
            }
        }
    });


    $scope.referencetableGridOptionsWithoutEdit = {
        dataSource: $scope.GetAllreferencetabledataSource,
        sortable: true,
        scrollable: true, filterable: true,
        pageable: { buttonCount: 5 },
        autoBind: true,
        selectable: "row",
        editable: "inline",
        columns: [{ field: "TABLE_ID", title: "Table Id" }, { field: "TABLE_NAME", title: "Table Name" }],
        detailTemplate: kendo.template($("#templates").html()),
        dataBound: function (e) {
            if (e.sender._data.length > 0) {
                var row = this.tbody.find('tr:first');
                this.select(row);
                $scope.Table.TABLE_ID = e.sender._data[0].TABLE_ID;
                $scope.Table.TABLE_NAME = e.sender._data[0].TABLE_NAME;
                // $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            } else {
                $scope.Table.TABLE_ID = 0;
                $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            }
            var count = 0;
            var referenceData = $('#gridcategoryfamily').data('kendo-grid')._data;
            angular.forEach(referenceData, function (data) {
                if ($('#gridcategoryfamily').data('kendo-grid')._data[count].hasChildren == false) {
                    var uid = $('#gridcategoryfamily').data('kendo-grid').tbody.find("tr.k-master-row")[count].dataset.uid
                    $('#gridcategoryfamily').data('kendo-grid').expandRow($('#gridcategoryfamily').data('kendo-grid').tbody.find("tr[data-uid='" + uid + "']"));
                }
                count = count + 1;
            });

        }
    };
    $scope.referencetableGridOptions = {
        dataSource: $scope.GetAllreferencetabledataSource,
        sortable: true,
        scrollable: true, filterable: true,
        pageable: { buttonCount: 5 },
        autoBind: true,
        selectable: "row",
        editable: "inline",
        columns: [{ field: "TABLE_ID", title: "Table Id" }, { field: "TABLE_NAME", title: "Table Name" }, {
            command: [{ name: 'edit', text: "", width: "10px", template: "<a class=\'k-grid-edit k-item girdicons\' ng-if=\'userRoleModifyReferenceTable\'><div title=\'Edit\' class=\'glyphicon glyphicon-edit blue btn-xs-icon\'></div></a>" },
                { name: "destroy", text: "", width: "10px", template: "<a class=\'k-item girdicons\' ng-if=\'userRoleRemoveReferenceTable\' ng-click=\'DeleteRemoveReferencetable($event,this)\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" },
                //{ template: '<a class=\'k-grid-detach k-item girdicons\' ng-show="userRoleDeleteAttributeManagement" ng-if=\'userRoleDetachReferenceTable\'  ng-click=\'RemoveAssociation($event,this)\'><div title=\'Detach\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></<a>' }
            ], title: "Actions", width: "100px",
        }],
        detailTemplate: kendo.template($("#templates").html()),
        dataBound: function (e) {
            if (e.sender._data.length > 0) {
                var row = this.tbody.find('tr:first');
                this.select(row);
                $scope.Table.TABLE_ID = e.sender._data[0].TABLE_ID;
                $scope.Table.TABLE_NAME = e.sender._data[0].TABLE_NAME;
                // $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            } else {
                $scope.Table.TABLE_ID = 0;
                $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            }
            var count = 0;
            var referenceData = $('#gridcategoryfamily').data('kendo-grid')._data;
            angular.forEach(referenceData, function (data) {
                if ($('#gridcategoryfamily').data('kendo-grid')._data[count].hasChildren == false) {
                    var uid = $('#gridcategoryfamily').data('kendo-grid').tbody.find("tr.k-master-row")[count].dataset.uid
                    $('#gridcategoryfamily').data('kendo-grid').expandRow($('#gridcategoryfamily').data('kendo-grid').tbody.find("tr[data-uid='" + uid + "']"));
                }
                count = count + 1;
            });

        }
    };
    $scope.handleChange = function (dataItem) {
        if (dataItem != undefined) {
            $scope.Table.TABLE_ID = dataItem.TABLE_ID;
            $scope.Table.TABLE_NAME = dataItem.TABLE_NAME;
            $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            
        }
    };

    $scope.detailGridReferencetableOptions = function (dataItem) {
        return {
            dataSource: {
                type: "json",
                transport: {
                    read: function (options) {
                        dataFactory.GetAllReferenceTableFamily(dataItem.TABLE_ID, $rootScope.selecetedCatalogId).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }, schema: {
                    model: {
                        id: "FAMILY_ID",
                        fields: {
                            CATALOG_NAME: { editable: false },
                            FAMILY_NAME: { editable: false },
                        }
                    }
                },
                serverFiltering: true,
                serverPaging: true,
                serverSorting: true,
                pageable: { buttonCount: 5 },
            },
            scrollable: false,
            sortable: true, autobind: false,
            selectable: "row",
            columns: [{ field: "CATALOG_NAME", title: "Catalog Name" },
                { field: "FAMILY_NAME", title: "Product Name" }],
            dataBound: function () {
                if (this.dataSource.data().length === 0) {
                    var masterRow = this.element.closest("tr.k-detail-row").prev();
                    $("#gridcategoryfamily").data("kendoGrid").collapseRow(masterRow);
                    masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                }
                else {
                    this.expandRow(this.tbody.find("tr.k-master-row"));
                    //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                }
            }
        };
    };

    //-----------------------------------------------------------------------------------Start Reference table values --------------------------------------------------------

    $scope.RefreshGorup = function () {
        $scope.GetAllreferencetabledataSource.read();
        $scope.formSubmit = false;
        $scope.tableName = "";
    };
    $scope.ReferenceTableDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetAllReferenceTable($scope.selecetedCatalogId, $localStorage.getCustomerID).success(function (response) {
                    if (response.length == 0) {
                        $scope.disableEmptyReferenceValues = true;
                    }
                    else {
                        $scope.disableEmptyReferenceValues = false;
                    }
                    options.success(response);
                   
                   
                }).error(function (error) {
                    options.error(error);
                });

            }

        }
    });


    $scope.rowAdd = function (object) {
        var obj = jQuery.parseJSON(object);
        if (obj.Columns.length > 0) {
            for (var i = $scope.referenceTableData.length; i < obj.Data.length; i++) {
                $scope.referenceTableData.push(obj.Data[i]);
            }
            $scope.columnsForReferenceTable = obj.Columns;
        } else {
            $scope.referenceTableData = [];
            $scope.columnsForReferenceTable = [];
        }
    };
    $scope.columnAdd = function (object) {
        var obj = jQuery.parseJSON(object);
        if (obj.Columns.length > 0) {
            for (var i = $scope.columnsForReferenceTable.length; i < obj.Columns.length; i++) {
                $scope.columnsForReferenceTable.push(obj.Columns[i]);
            }
            //$scope.columnsForReferenceTable = obj.Columns;
        } else {
            $scope.referenceTableData = [];
            $scope.columnsForReferenceTable = [];
        }
    };
    $scope.rowColumnInsert = function (tableid, insert) {
        dataFactory.GetDynamicReferenceTable(tableid, $rootScope.selecetedCatalogId).success(function (response) {
            if (response != null) {
                if (insert == "Row") {
                    $scope.rowAdd(response.Data.Data);
                }
                if (insert == "Column") {
                    $scope.columnAdd(response.Data.Data);
                }
                //var obj = jQuery.parseJSON(response.Data.Data);
                //if (obj.Columns.length > 0) {
                //    for (var i = $scope.referenceTableData.length ; i < obj.Data.length; i++) {
                //        $scope.referenceTableData.push(obj.Data[i]);
                //    }
                //    $scope.columnsForReferenceTable = obj.Columns;
                //} else {
                //    $scope.referenceTableData = [];
                //    $scope.columnsForReferenceTable = [];
                //}
            }
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.ReferenceTableChange = function (e) {
        if (e.sender.value() !== 0) {
            $scope.ReferenceTableChangeValue(e.sender.value());
            //$scope.setimagepreview(e);
            $scope.Table.ImagePreview = '';
        }
    };
    $scope.referenceTableData = [];

    $scope.tblReferenceTable = new NgTableParams(
    {
        page: 1,
        count: 1000
    },
    {
        counts: [],
        total: function () { return $scope.referenceTableData.length; },
        getData: function ($defer, params) {
            var filteredData = $scope.referenceTableData;
            var orderedData = params.sorting() ?
                $filter('orderBy')(filteredData, params.orderBy()) :
                filteredData;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            
        }
    });
    $scope.ReferenceTableChangeValue = function (tableid) {
        $scope.Table.TABLE_ID = tableid;
        dataFactory.GetDynamicReferenceTable(tableid, $rootScope.selecetedCatalogId).success(function (response) {
            if (response != null) {
                var obj = jQuery.parseJSON(response.Data.Data);
                if (obj.Columns.length > 0) {
                    $scope.referenceTableData = obj.Data;
                    $scope.columnsForReferenceTable = obj.Columns;
                    $scope.ReferenceTableDataSource.read();
                    $("#refTblExport").removeAttr('disabled');
                } else {
                    $scope.referenceTableData = [];
                    $scope.columnsForReferenceTable = [];
                    $("#refTblExport").attr('disabled', 'disabled');
                }
            }
        }).error(function (error) {
            options.error(error);
        });
    };




    //==============================================
    $scope.RefreshTable = function () {
        if ($scope.Table.TABLE_ID !== 0) {
            $scope.ReferenceTableDataSource.read();
            $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
        }
    };
    $scope.InsertRow = function () {
        if ($scope.Table.TABLE_ID !== 0) {
            dataFactory.StpCall($scope.Table.TABLE_ID, "", "Text", 0, 0, 0, 0, "Row", "Insert").success(function (response) {
                //$.msgBox({
                //    title: $localStorage.ProdcutTitle,
                //    content: 'Row inserted successful',
                //    type: "info"
                //});
                $scope.rowColumnInsert($scope.Table.TABLE_ID, "Row");
            }).error(function (error) {
                options.error(error);
            });
        }
        if ($scope.currentIds == "" || $scope.selectedRow == null) {
            $scope.IsView = false;
            $scope.IsDisabled = true;
        }
    };
    $scope.InsertColumn = function () {
        if ($scope.Table.TABLE_ID !== 0) {
            dataFactory.StpCall($scope.Table.TABLE_ID, "", "Text", 0, 0, 0, 0, "Column", "Insert").success(function (response) {
                //$.msgBox({
                //    title: $localStorage.ProdcutTitle,
                //    content: 'Column inserted successful',
                //    type: "info"
                //});
                $scope.rowColumnInsert($scope.Table.TABLE_ID, "Column");
                //$scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            }).error(function (error) {
                options.error(error);
            });
        }
    };
    $scope.RemoveRow = function () {
        if ($scope.selectedRow !== null) {
            dataFactory.StpCall($scope.Table.TABLE_ID, "", "Text", 0, $scope.selectedColumn - 1, $scope.selectedRow, 0, "Row", "Delete").success(function (response) {
                //$.msgBox({
                //    title: $localStorage.ProdcutTitle,
                //    content: '' + response + '',
                //    type: "info"
                //});
                $scope.selectedRow = null;
                $scope.selectedColumn = null;
                $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
                $scope.IsView = false;
                $scope.IsDisabled = true;
            }).error(function (error) {
                options.error(error);
            });

            $scope.IsView = false;
            $scope.IsDisabled = true;
            $scope.Table.ImagePreview = "";

        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select a Row and continue.',
                //type: "info"
            });
            // $scope.IsView = false;
        }
    };
    $scope.RemoveColumn = function () {
        if ($scope.selectedColumn !== null) {
            dataFactory.StpCall($scope.Table.TABLE_ID, "", "Text", 0, $scope.selectedColumn - 1, $scope.selectedRow, 0, "Column", "Delete").success(function (response) {
                //$.msgBox({
                //    title: $localStorage.ProdcutTitle,
                //    content: '' + response + '',
                //    type: "info"
                //});
                $scope.selectedRow = null;
                $scope.selectedColumn = null;
                $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
                $scope.IsView = false;
                $scope.IsDisabled = true;
                $scope.Table.ImagePreview = "";
            }).error(function (error) {
                options.error(error);
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select a Column and continue.',
                type: "error"
            });
        }
    };

    $scope.selectedRow = null;
    $scope.selectedColumn = null;
    $scope.setClickedRow = function (index) {
        $scope.selectedRow = index;
    };
    $scope.setClickedColumn = function (index) {
        $scope.selectedColumn = index;
    };
    $scope.currentIds = "";


    if ($scope.currentIds == "" || $scope.selectedColumn == null || $scope.selectedRow == null) {
        $scope.IsView = false;
        $scope.IsDisabled = true;
    }
    $scope.setimagepreview = function (e) {
        //
        $scope.IsView = true;
        $scope.IsDisabled = false;
        $scope.currentIds = e.currentTarget.id;
        $scope.cellId = $scope.currentIds;
        var message = e.currentTarget.value;
        //
        // var imagePath = "http://" + window.location.host + "/Content/ProductImages/" + $scope.getCustomerCompanyName + e.currentTarget.value;

        if ($scope.isLocalOrServer == "true")
        {
            var imagePath = $scope.getPathFromWebConfig + "ProductImages/" + $scope.getCustomerCompanyName + e.currentTarget.value;

            var request = new XMLHttpRequest();
            request.open('HEAD', imagePath, false);

            if (message.toLowerCase().endsWith('.gif') || message.toLowerCase().endsWith('.jpg') || message.toLowerCase().endsWith('.jpeg') || message.toLowerCase().endsWith('.png') || message.toLowerCase().endsWith(".bmp")) {
                //
                $scope.Table.ImagePreview = message;
                if ($scope.getCustomerCompanyName === undefined || $scope.getCustomerCompanyName === "") {
                    $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
                } else {
                    $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
                }

            } else if (message.toLowerCase().endsWith(".eps") || message.toLowerCase().endsWith(".tif") || message.toLowerCase().endsWith(".tiff") || message.toLowerCase().endsWith(".psd") ||
                      message.toLowerCase().endsWith(".tga") || message.toLowerCase().endsWith(".pcx")) {
                //            
                dataFactory.Getimagevaluevalues(message).success(function (response) {
                    //
                    $scope.Table.ImagePreview = response.replace($scope.getCustomerCompanyName, '');


                    //if ($scope.getCustomerCompanyName === undefined) {
                    // $scope.getCustomerCompanyName = "";
                    //}
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {
                $scope.Table.ImagePreview = "";
            }



        } else
        {
            var imagePath = "http://" + window.location.host + "/Content/ProductImages/" + $scope.getCustomerCompanyName + e.currentTarget.value;

            var request = new XMLHttpRequest();
            request.open('HEAD', imagePath, false);
            request.send();

            if (request.status == 200) {
                if (message.toLowerCase().endsWith('.gif') || message.toLowerCase().endsWith('.jpg') || message.toLowerCase().endsWith('.jpeg') || message.toLowerCase().endsWith('.png') || message.toLowerCase().endsWith(".bmp")) {
                    //
                    $scope.Table.ImagePreview = message;
                    if ($scope.getCustomerCompanyName === undefined || $scope.getCustomerCompanyName === "") {
                        $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
                    } else {
                        $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
                    }

                } else if (message.toLowerCase().endsWith(".eps") || message.toLowerCase().endsWith(".tif") || message.toLowerCase().endsWith(".tiff") || message.toLowerCase().endsWith(".psd") ||
                          message.toLowerCase().endsWith(".tga") || message.toLowerCase().endsWith(".pcx")) {
                    //            
                    dataFactory.Getimagevaluevalues(message).success(function (response) {
                        //
                        $scope.Table.ImagePreview = response.replace($scope.getCustomerCompanyName, '');


                        //if ($scope.getCustomerCompanyName === undefined) {
                        // $scope.getCustomerCompanyName = "";
                        //}
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    $scope.Table.ImagePreview = "";
                }

            }
            else {
                if (message.toLowerCase().endsWith('.gif') || message.toLowerCase().endsWith('.jpg') || message.toLowerCase().endsWith('.jpeg') || message.toLowerCase().endsWith('.png') || message.toLowerCase().endsWith(".eps") || message.toLowerCase().endsWith(".tif") || message.toLowerCase().endsWith(".tiff") || message.toLowerCase().endsWith(".psd") ||
                          message.toLowerCase().endsWith(".tga") || message.toLowerCase().endsWith(".pcx") || message.toLowerCase().endsWith(".bmp")) {
                    $scope.Table.ImagePreview = "No";
                    $scope.Table.ImagePreview1 = "\\images\\filenotfound1.jpg";
                }
                else {
                    $scope.Table.ImagePreview = "";
                    $scope.Table.ImagePreview1 = "";
                }
            }
        }
       
    };





    $scope.SaveAllValues = function (e) {
        if ($scope.Table.TABLE_ID !== 0) {
            if (e.referenceTableData.length > 0)
                {
            dataFactory.Savereferencetablevalues($scope.Table.TABLE_ID, e.referenceTableData).success(function (response) {
                if (response != "Please reload the page and continue" ) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: response,
                        type: "info"
                    });
                }
                if (response == "Please reload the page and continue")
                {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'please upload a valid file',
                        type: "info"
                    });
                }
              
                $scope.ReferenceTableChangeValue($scope.Table.TABLE_ID);
            }).error(function (error) {
                options.error(error);
            });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'please upload a valid file',
                    type: "info"
                });
            }

        }
    };
    //===================================================

    $scope.onSuccess1 = function (e) {

        if ($scope.currentIds == "" || $scope.selectedColumn == null) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Table Cell.',
                type: "error"
            });
            $(".k-upload-files").remove();
            $(".k-upload-status").remove();
            $scope.Table.ImagePreview = "";
        }
        else {
            if (e.operation === "remove") {
                $scope.Table.ImagePreview = "";
            } else {
                var message = $.map(e.files, function (file) { return file.name; }).join(", ");
                if (message.indexOf('&') == -1 && message.indexOf('#') == -1 && message.indexOf('\'') == -1) {
                    $scope.$apply(function () {
                        $scope.Table.ImagePreview = '\\Images\\' + message;
                    });
                    if ($scope.currentIds != "") {
                        // document.getElementById($scope.currentIds).value = $scope.Table.ImagePreview;
                        $("#" + $scope.currentIds).val($scope.Table.ImagePreview);
                        $scope.referenceTableData[$scope.selectedRow]['Col_' + ($scope.selectedColumn + 1)] = $scope.Table.ImagePreview;
                    }
                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();

                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Image name contains invalid special characters( # & \' ).',
                        type: "error"
                    });

                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();
                }
                //$scope.Category.IMAGE_FILE2 = '\\' + $localStorage.getCustomerCompanyName + '\\Images\\' + message;
            }
        }
        //  alert(someInfo);
    };

    $scope.onSuccess = function (e) {
        $scope.Table.ImagePreview = '\\Content\\ProductImages\\' + $scope.getCustomerCompanyName + '\\Images';
        if ($scope.selectedColumn !== null && $scope.selectedRow !== null) {
            if (e.operation === "remove") {
                $scope.Table.ImagePreview = "";
            } else {
                var message = $.map(e.files, function (file) {
                    return file.name;
                }).join(", ");
                if (message.indexOf('&') == -1 && message.indexOf('#') == -1 && message.indexOf('\'') == -1) {
                    var imgext = message.substring(message.length, message.length - 4);
                    //$scope.Table.ImagePreview = '\\' + $scope.getCustomerCompanyName + '\\Images\\' + message;
                    $scope.Table.ImagePreview = '\\Images\\' + message;
                    if ($scope.currentIds != "") {
                        // document.getElementById($scope.currentIds).value = $scope.Table.ImagePreview;
                        $("#" + $scope.currentIds).val($scope.Table.ImagePreview);
                        $scope.referenceTableData[$scope.selectedRow]['Col_' + ($scope.selectedColumn + 1)] = $scope.Table.ImagePreview;
                    }
                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Image name contains invalid special characters( # & \' ).',
                        type: "error"
                    });
                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();
                }

            }
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Table Cell.',
                type: "error"
            });
            $(".k-upload-files").remove();
            $(".k-upload-status").remove();
            $scope.Table.ImagePreview = "";
        }
    };
    $scope.onSelect = function (e) {
        var message = $.map(e.files, function (file) { return file.name; }).join(", ");
    };

    $scope.clearReferenceTBImgAttach = function (imageValue) {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure want to Clear the Reference Table Image?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {

                if (result === "Yes") {


                    if ($scope.currentIds != "") {

                        // document.getElementById($scope.currentIds).value = $scope.Table.ImagePreview;

                     //   $("#" + $scope.currentIds).val("");
                        $scope.referenceTableData[$scope.selectedRow]['Col_' + ($scope.selectedColumn + 1)] = "";
                        $scope.Table.ImagePreview = "";
                        $scope.Table.ImagePreview1 = "";
                        $scope.$apply();
                        //  $scope.GetAllreferencetabledataSource.read();
                    }
                }

            }
        });




    };

    //$scope.clearReferenceTBImgAttach = function() {

    //    //dataFactory.clearReferenceTBImgAttach().success(function(response) {
    //    //    alert(response);
    //    //    $(".k-upload-files").remove();

    //    //}).error(function(error) {
    //    //    options.error(error);
    //    //});
    //};
    $scope.DeleteRemoveReferencetable = function (e, id) {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure you want to delete the selected Table Name?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {

                    dataFactory.RemoveReferencetable(id.dataItem.TABLE_ID).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });
                        $('#gridcategoryfamily').data('kendoGrid').dataSource.read();
                        $('#gridcategoryfamily').data('kendoGrid').refresh();
                        $scope.ReferenceTableDataSource.read();
                        // options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                };
            }
        });

    }

    //Asset management implementation//
    $scope.OpenPopupWindow = function (e) {

        $rootScope.paramValue = e.value;
        $scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
        $scope.winManageDrive.title("Asset Management");
        $scope.winManageDrive.center().open();
        $scope.driveMahementIsVisible = true;

    }

    $rootScope.SaveReferenceTableFileSelection = function (fileName, path) {

        var extn = fileName.split(".");
        // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
        var FileFormat = ["rar", "zip"];
        if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
            $rootScope.categoryAttachPath = path;
            if ($rootScope.paramValue == "ReferenceTableImage") {
                $scope.Table.ImagePreview = path;
                if (path.toLowerCase().endsWith('.gif') || path.toLowerCase().endsWith('.jpg') || path.toLowerCase().endsWith('.jpeg') || path.toLowerCase().endsWith('.png') || path.toLowerCase().endsWith('.bmp')) {
                    //
                    $scope.Table.ImagePreview = path;
                }
                else if (path.toLowerCase().endsWith(".eps") || path.toLowerCase().endsWith(".tif") || path.toLowerCase().endsWith(".tiff") || path.toLowerCase().endsWith(".psd") ||
                      path.toLowerCase().endsWith(".tga") || path.toLowerCase().endsWith(".pcx")) {
                    dataFactory.Getimagevaluevalues(path).success(function (response) {
                        //
                        var companyName = '/' + $scope.getCustomerCompanyName;
                        var convertedPath = response.replace(companyName, '');
                        $scope.Table.ImagePreview = convertedPath;
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
            if ($scope.currentIds != "") {
                // document.getElementById($scope.currentIds).value = $scope.Table.ImagePreview;
               // $("#" + $scope.currentIds).val($scope.Table.ImagePreview);
                $scope.referenceTableData[$scope.selectedRow]['Col_' + ($scope.selectedColumn + 1)] = $scope.Table.ImagePreview;
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Image name contains invalid special characters( # & \' ).',
                    type: "error"
                });

                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            }

        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'This file type is not supported',
                type: "info"
            });
        }
        $scope.winManageDrive.center().close();
        $scope.driveMahementIsVisible = false;

    }
    $scope.GetFilePathSavedLocation = function()
    {
        dataFactory.GetServerFilePath().success(function (response) {
            $scope.getPathFromWebConfig = response[0];
            $scope.isLocalOrServer = response[1];
          
        }).error(function (error) {
            options.error(error);
        });
    }
    //-----------------------------------------------------------------------------------End Reference table values --------------------------------------------------------
    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        }
        else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
        }
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerID = 0;

        }
        else {
            $scope.getCustomerID = $localStorage.getCustomerID;
        }

        $scope.GetFilePathSavedLocation();

        $scope.GetAllreferencetabledataSource.read();
        
    };

    $scope.init();

    $scope.Export = function () {
        tableid = $scope.Table.TABLE_ID;
        if (tableid != null && tableid != 0) {
            dataFactory.ReferencetableExport(tableid, $rootScope.selecetedCatalogId).success(function (response) {
                if (response == "No Records") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'No Records in the Table.',
                    });
                } else if (response == "Records") {
                    var ExportSheetName = "ReferenceExport.xls";
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Export Successfully.',
                    });
                    window.open("DownloadFile.ashx?Path=" + ExportSheetName);
                }
                else if (response == null) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Error during the Export.',
                    });
                }

            }).error(function (error) {
                options.error(error);
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select the reference Table.',
            });
        }

    };


    //------------------------------------------------------------------------------------------------//

    //Form Validation

    $scope.selectFileforUpload = function (file) {
        $scope.enableReferenceImport = true;
        $scope.FileInvalidMessage = "";
        $rootScope.SelectedFileForUploadReferenceTable = file[0].name;
        $scope.SelectedFileForUpload = file[0];
        $scope.$apply(function () {
            $rootScope.SelectedFileForUploadReferenceTable = file[0].name;
        });
    };

    $scope.ChechFileValid = function (file) {
        
        var isValid = false;

        if ($scope.SelectedFileForUpload != null) {
            if (file.name.toLowerCase().endsWith('.xls') || file.name.toLowerCase().endsWith(".xlsx")) {// && file.size <= (512 * 2048)
                $scope.FileInvalidMessage = "";
                $scope.excelname = file.name;
                isValid = true;
            }
            else {
                $scope.FileInvalidMessage = "Selected file is Invalid. (only file type xls and xlsx is allowed)";
                return false;
            }
        }
        else {
            $scope.FileInvalidMessage = "File required!";
            return false;
        }
        $scope.IsFileValid = isValid;
    };


    //Clear form 
    $scope.ClearForm = function () {
        $scope.FileDescription = "";
        angular.forEach(angular.element("input[type='file']"), function (inputElem) {
            angular.element(inputElem).val(null);
        });
        $scope.f1.$setPristine();
        $scope.IsFormSubmitted = false;
    }

    //Save File
    $scope.SaveFile = function () {
        $scope.IsFormSubmitted = true;
        $scope.Message = "";
        $scope.FileInvalidMessage = "";
        var selectedRefTable = $("#drpreferencetable").val();
        if (selectedRefTable != null) {
            if ($scope.SelectedFileForUpload !== null) {
                $scope.ChechFileValid($scope.SelectedFileForUpload);
                if ($scope.IsFileValid) {
                    $scope.UploadFile($scope.SelectedFileForUpload);
                }
                else {
                    return false;
                }
            }
            else {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose a File.',
                    type: "info"
                });
            }
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please choose Table to import.',
                type: "info"
            });
        }
    };


    $scope.ResetReferenceImport = function () {
        $rootScope.SelectedFileForUploadReferenceTable = null;
        $rootScope.SelectedFileForUpload = null;
        $scope.enableReferenceImport = false;
        var $el = $('#importfile1reference');
        if ($el.val() != "") {
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
        }

        if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
            $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
    };



    $scope.newxmlname1 = "";
    $scope.excelPath = "";
    $scope.UploadFile = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        //We can send more data to server using append         
        //var defer = $q.defer();
        $http.post("/Catalog/SaveFilesReferenceTable?XMLName=" + $scope.newxmlname1 + "", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: d[0] +".",
                    //type: "error"
                });
                //   $scope.ClearForm();
                if (d[0] != "Please upload valid file") {
                    $scope.enableReferenceImport = false;
                    $("#importfile1reference").val(null);
                    $(".uploadfilename1").val(null);
                    $scope.ResetReferenceImport();
                    $scope.ReferenceTableDataSource.read();
                    $scope.ReferenceTableChangeValue(parseInt(d[1]));
                }
                else if (d == "Please upload valid file") {
                    // $(".uploadfilename1").text('');
                    // $scope.SelectedFileForUpload = null;
                }
            })
            .error(function () {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });
                //$scope.ClearForm();
                $("#importfile1reference").val('');
                $scope.UploadFile = null;
                $scope.SelectedFileForUpload = null;
                $(".uploadfilename1").val('');
                $(".uploadfilename1").text('');
                $scope.ReferenceTableDataSource.read();
                $scope.ReferenceTableChangeValue(1);
            });
        return defer.promise;
    };




}]);
