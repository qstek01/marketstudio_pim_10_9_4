﻿LSApp.controller('EpaymentController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $rootScope,$localStorage) {
    $scope.PaymentPrice = {
        price: "2.0"
    };

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;


    $scope.EpaymentClick = function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        var date = dd.toString() + mm.toString() + yyyy.toString();
        var amounts = $scope.PaymentPrice.price;
        var webOrderNo = "1000";
        var currentDate = date;
        var userid = 7;
        var checksum;
        var msg;
        var msg1 = "HMACUAT" + "|" + userid + "|" + "NA" + "|" + amounts + "|" + "NA" + "|" + "NA" + "|" + "NA" + "|" + "INR" + "|" + "NA" + "|" + "R" + "|" + "hmacuat" + "|" + "NA" + "|" + "NA" + "|" + "F" + "|" + webOrderNo + "|" + currentDate + "|" + "NA" + "|" + "NA" + "|" + "NA" + "|" + "NA" + "|" + "NA" + "|" + "http://Litestudio.Questudio.com/espares/ePayreturn.aspx";
        dataFactory.EpaymentClick(msg1, "uIZ2iayX70hc").success(function (response) {
            if (response != null) {
                checksum = response;
                msg = msg1 + "|" + checksum;
                var form = $('<form/></form>');
                form.attr("action", "https://uat.billdesk.com/pgidsk/PGIMerchantPayment");
                form.attr("method", "POST");
                form.attr("style", "display:none;");
                $scope.addFormFields(form, msg);
                $("body").append(form);
                form.submit();
                form.remove();
            }
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.paypalpaymentClick = function () {
        // global data
        var data = {
            cmd: "_cart",
            business: "sengottuvelm@tradingbell.com",
            upload: "1",
            bn:"Questudio_SP",
            rm: "2",
            return: "http://localhost:57000/App/Success",
            charset: "utf-8"
        };
        data["item_number_1"] = "CatalogStudio";
        data["item_name_1"] = "CatalogStudio Web Version";
        data["quantity_1"] = "2";
        data["amount_1"] = "20.00";
        data["item_number_2"] = "CatalogStudio1";
        data["item_name_2"] = "CatalogStudio1 Web Version";
        data["quantity_2"] = "2";
        data["amount_2"] = "20.00";
        var form = $('<form/></form>');
        form.attr("action", "https://www.sandbox.paypal.com/cgi-bin/webscr");
        form.attr("method", "POST");
        form.attr("style", "display:none;");
        this.addFormField(form, data);
        $("body").append(form);
        // submit form
        form.submit();
        form.remove();
    };

    $scope.addFormField = function (form, data) {
        if (data != null) {
            $.each(data, function (name, value) {
                if (value != null) {
                    var input = $("<input></input>").attr("type", "hidden").attr("name", name).val(value);
                    form.append(input);
                }
            });
        }
    };

    $scope.addFormFields = function (form, msg) {

        var input = $("<input></input>").attr("type", "hidden").attr("name", "msg").val(msg);
        form.append(input);

    };

}]);