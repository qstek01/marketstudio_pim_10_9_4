﻿LSApp.controller('CommonvalueSupplierController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'NgTableParams', '$filter', '$rootScope', '$localStorage',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, NgTableParams, $filter, $rootScope, $localStorage) {

        $scope.GetAllCommonvalueSupplierdataSource = new kendo.data.DataSource({
            pageSize: 10,
            type: "json",
            serverFiltering: true, editable: false,
            serverPaging: true,
            serverSorting: true, pageable: true,
            transport: {
                read: function (options) {
                    dataFactory.GetAllSupplier(options.data,$localStorage.getCustomerID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "SUPPLIER_NAME",
                    fields: {
                        SUPPLIER_NAME: { type: "string" },
                        SUPPLIER_COMPANY_NAME: { type: "string" }
                    }
                }
            }
        });

        if ($("#dropDownCommonvalueSupplierGrid").val() !== undefined) {
            var dropDownGrid = $("#dropDownCommonvalueSupplierGrid").kendoExtDropDownGrid({
                dataTextField: "SUPPLIER_NAME",
                dropDownWidth: "175px",
                gridWidth: "400px",
                grid: {
                    dataSource: $scope.GetAllCommonvalueSupplierdataSource,
                    columns: [
                        {
                            field: "SUPPLIER_NAME",
                            title: "Supplier Name",
                            width: 200
                        },
                        {
                            field: "SUPPLIER_COMPANY_NAME",
                            title: "Company Name",
                            width: 150
                        }],
                    pageable: { buttonCount: 5 },
                    selectable: true,
                    filterable: true
                }
            }).data("kendoExtDropDownGrid");

            dropDownGrid.bind("change", function () {
                var selectedRows = this.grid().select();
                var dataItem = this.grid().dataItem(selectedRows[0]);
                //  $("#userSelection").prepend(kendo.format("<div>You Selected: {0} {1}, {2}</div>", dataItem.SUPPLIER_NAME, dataItem.SUPPLIER_COMPANY_NAME));
                //angular.forEach($scope.selectedRow, function (value, key) {
               //    if (key.contains("Supplier")) {
                //        $scope.selectedRow[key] = dataItem.SUPPLIER_NAME;
                //    }
                //});
                $scope.commonvaluedata.STRING_VALUE = dataItem.SUPPLIER_NAME;
                $("#dropDownCommonvalueSupplierGrid .k-input").text(dataItem.SUPPLIER_NAME);
                // $scope.selectedRow["Supplier__~__0__false_true"] = dataItem.SUPPLIER_NAME;
            });

        };
        //angular.forEach($scope.selectedRow, function (value, key) {
        //    if (key.contains("Supplier")) {
      
        //    }
        //});

    }]);

