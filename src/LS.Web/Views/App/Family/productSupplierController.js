﻿LSApp.controller('productSupplierController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'NgTableParams', '$filter', '$rootScope', '$localStorage',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, NgTableParams, $filter, $rootScope, $localStorage) {

        $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
        

        $scope.getCustomerIDs = $localStorage.getCustomerID;

        //alert($scope.getCustomerIDs);

        

        $scope.GetAllSupplierdataSource = new kendo.data.DataSource({
            pageSize: 10,
            type: "json",
            serverFiltering: true, editable: false,
            serverPaging: true,
            serverSorting: true, pageable: true,
            transport: {
                read: function (options) {
                    

                  

                    dataFactory.GetAllSupplier(options.data, $scope.getCustomerIDs).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "SUPPLIER_NAME",
                    fields: {
                        SUPPLIER_NAME: { type: "string" },
                        SUPPLIER_COMPANY_NAME: { type: "string" }
                    }
                }
            }
        });
        
        if (($("#dropDownSupplierGrid").val() !== undefined) && ($("#dropDownSupplierGrid").val() !== null))  {
            var dropDownGrid = $("#dropDownSupplierGrid").kendoExtDropDownGrid({
                dataTextField: "SUPPLIER_NAME",
                optionLabel: "Select Supplier",
                  dropDownWidth: "175px",
               
               gridWidth: "300px",
                grid: {
                    dataSource: $scope.GetAllSupplierdataSource,
                    columns: [
                        {
                            field: "SUPPLIER_NAME",
                            title: "Supplier Name",
                            width: 200
                        },
                        {
                            field: "SUPPLIER_COMPANY_NAME",
                            title: "Company Name",
                            width: 150
                        }],
                    pageable: { buttonCount: 5 },
                    selectable: true,
                    filterable: true
                }
            }).data("kendoExtDropDownGrid");

            dropDownGrid.bind("change", function () {
                var selectedRows = this.grid().select();
                var dataItem = this.grid().dataItem(selectedRows[0]);
                //  $("#userSelection").prepend(kendo.format("<div>You Selected: {0} {1}, {2}</div>", dataItem.SUPPLIER_NAME, dataItem.SUPPLIER_COMPANY_NAME));
                angular.forEach($scope.selectedRow, function (value, key) {
                    if (key.contains("Supplier__OBJ__0")) {
                        $scope.selectedRow[key] = dataItem.SUPPLIER_NAME;
                    }
                    else
                    {
                       // $scope.selectedRow[key] = "Select Supplier";

                    }
                });
                // $scope.selectedRow["Supplier__~__0__false_true"] = dataItem.SUPPLIER_NAME;
            });
           
        };
        angular.forEach($scope.selectedRow, function (value, key) {
            if (key.contains("Supplier__OBJ__0")) {
               
                if ($scope.selectedRow[key] !== null) {
                    $("#dropDownSupplierGrid .k-input").text($scope.selectedRow[key]);
                }
                else
                {
                    $("#dropDownSupplierGrid .k-input").text("Select Supplier");
                }
            }
        });
       
    }]);

