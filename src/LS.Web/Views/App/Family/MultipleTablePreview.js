﻿LSApp.controller('multipleTablePreview', ['$scope', 'dataFactory', '$http', '$compile', 'NgTableParams', '$rootScope', '$localStorage', function ($scope, dataFactory, $http, $compile, NgTableParams, $rootScope, $localStorage) {
    $scope.family_ID = 0;
    $scope.$on("Active_FamilyId", function (event, selectedCategoryId) {
        $scope.family_ID = selectedCategoryId;
        $scope.MultipleTableDataSource.read();
    });
    $scope.MultipleTableDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GroupTableDataGrid($scope.family_ID, $rootScope.selecetedCatalogId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.previewEntities = [];
    $scope.MultipleTableTabChange = function (e) {
        $scope.selectAttributeData(e.sender.value());

    };
    
    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        }
        else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
        }
        $("#showMultipleTablePreviewTab").hide();

    };
    
    $scope.init();
    $scope.selectAttributeData = function (groupId) {
        $("#showMultipleTablePreviewTab").show();
        $http.get("../MultipleTable/multipletableAttributes?groupID=" + groupId).
            then(function (multipleAttribuetDetails) {
                $scope.previewEntities = [];
                $scope.previewEntities = multipleAttribuetDetails.data;
                //$scope.previewSelected = [];
                //$scope.previewSelectedValue();
            });
    };
    
    //$scope.multipleTableProdSpec = [];
    //$scope.previewSelectedValue = function () {
    //    for (var i = 0; i < $scope.previewEntities.length; i++) {
    //        var entity = $scope.previewEntities[i];
    //        if (entity.ISAvailable > 0) {
    //            $scope.previewSelected.push(entity.ATTRIBUTE_ID);
    //        }
    //        if (entity.ATTRIBUTE_TYPE === 1) {
    //            $scope.attrcount++;
    //            $scope.multipleTableProdSpec.push(entity);
    //        }
    //    }
    //    if ($scope.multipleTableProdSpec.length > 0) {
    //        $scope.tableParams = new NgTableParams({
    //            page: 1,
    //            count: 10,
    //            sorting: {
    //                ISAvailable: 'desc',
    //                ATTRIBUTE_NAME: 'asc'
    //            }
    //        }, {
    //            total: $scope.multipleTableProdSpec.length,
    //            getData: function ($defer, params) {
    //                var orderedData = params.sorting() ?
    //                        $filter('orderBy')($scope.multipleTableProdSpec, params.orderBy()) :
    //                        $scope.multipleTableProdSpec;
    //                orderedData = params.filter() ?
    //                        $filter('filter')(orderedData, params.filter()) :
    //                        orderedData;
    //                alert(orderedData.length);
    //                params.total(orderedData.length);
    //                $defer.resolve($scope.previewAttributes = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    //            }
    //        });
    //    }
    //};
}]);