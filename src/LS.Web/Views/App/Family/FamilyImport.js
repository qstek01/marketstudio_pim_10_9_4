﻿LSApp.controller('FamilyImport', ['$scope', 'dataFactory', '$http', '$compile', '$rootScope', 'ngTableParams', '$localStorage', function ($scope, dataFactory, $http, $compile, $rootScope, ngTableParams, $localStorage) {

    $scope.tabledesignerimporttype = "Product";
    $scope.sheetselection = false;
    $("#importSelectionTable").show();
    $scope.importresultdisable = false;
    $scope.Message = "";
    $scope.FileInvalidMessage = "";
    $scope.SelectedFileForUpload = null;
    $scope.IsFormSubmitted = false;
    $scope.IsFileValid = false;
    $scope.IsFormValid = false;
    $scope.importSessionID = '';
    $("#importTableDesingnerFamilyLevelWindow").hide();
    $("#importTableSheetSlectectionWindow").hide();
    $("#importErrorWindow").hide();
    $("#importSuccessWindow").hide();
    $("#importSuccessWindow1").hide();
    $("#importpicklist").hide();
    $("#importerror").hide();
    $("#importSuccessWindowsubproduct").hide();
    $("#importerrorWindowsubproduct").hide();
    $scope.sheetselection = false;
    $scope.btnimport = true;
    $scope.Deleteproducts = false;
    $("#importerrorDeletelog").hide();
    $rootScope.familyimportstatus = false;
    // $("#importSelectionTable").hide();
    //Form Validation

    $scope.$watch("f1.$valid", function (isValid) {
        $scope.IsFormValid = isValid;
    });
    $scope.SelectedValues = [];
    $scope.excelname = "";
    $scope.ChechFileValid = function (file) {
        var isValid = false;

        if ($scope.SelectedFileForUpload != null) {
            if (file.name.toLowerCase().contains(".xls") || file.name.toLowerCase().contains(".xlsx")) {// && file.size <= (512 * 2048)
                $scope.FileInvalidMessage = "";
                $scope.excelname = file.name;
                isValid = true;
            }
            else {
                $scope.FileInvalidMessage = "Selected file is Invalid. (only file type xls and xlsx is allowed)";
            }
        }
        else {
            $scope.FileInvalidMessage = "File required!";
        }
        $scope.IsFileValid = isValid;
    };
    //File Select event 
    $rootScope.SelectedFileForUploadnamefamilyproduct = "";

    $scope.selectFileforUpload = function (file) {

        $rootScope.SelectedFileForUploadnamefamilyproduct = file[0].name;
        $scope.SelectedFileForUpload = file[0];
        $scope.$apply(function () {
            $rootScope.SelectedFileForUploadnamefamilyproduct = file[0].name;
        });
    };
    //----------------------------------------------------------------------------------------
    //Save File
    $scope.SaveFile = function () {
        $("#importSelectionTable").show();
        $scope.IsFormSubmitted = true;
        $scope.Message = "";
        if ($scope.tabledesignerimporttype = "Product")
        {
            $scope.tabledesignerimporttype = "Product";
        }
        if ($scope.SelectedFileForUpload !== null && $scope.SelectedFileForUpload !== undefined) {
            $scope.ChechFileValid($scope.SelectedFileForUpload);
            $scope.UploadFile($scope.SelectedFileForUpload);
            // clearForm();
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please choose a File.',
                type: "info"
            });
        }
     
    };
    //Clear form 
    function clearForm() {
        $scope.FileDescription = "";
        angular.forEach(angular.element("input[type='file']"), function (inputElem) {
            angular.element(inputElem).val(null);
        });
        $scope.f1.$setPristine();
        $scope.IsFormSubmitted = false;
    }
    //var fac = {};
    //  fac.UploadFile = function (file) {
    $scope.newxmlname1 = "";
    $scope.excelPath = "";
    $scope.UploadFile = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        $http.post("/Import/SaveFiles?XMLName=" + $scope.newxmlname1 + "", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {
                $scope.excelPath = d;
                $scope.ImportExcelSheetSelction.read();
                $("#importTableSheetSlectectionWindow").show();
                $("#importTableSheetWindow").hide();
                $("#family-import").removeClass("disabledbutton1");
            })
            .error(function () {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });

            });
        // return defer.promise;
        $("#family-import").removeClass("disabledbutton1");
    };

    //$scope.ImportExcelSheetSelction = new kendo.data.DataSource({
    //    type: "json",
    //    serverFiltering: true,
    //    transport: {
    //        read: function (options) {

    //            dataFactory.attributemapping($scope.excelPath).success(function (response) {
    //                $scope.autoChangeTableDesignerValue = response;
    //                options.success(response);


    //                if (response.length > 0) {
    //                    if ($rootScope.EnableSubProduct == false) {

    //                        var subProdList = [];
    //                        $scope.subproduct = response[0].TABLE_NAME;
    //                        angular.forEach($scope.ImportExcelSheetSelction._data, function (val) {
    //                            if (val.TABLE_NAME.toString().toUpperCase().includes("SUB")) {
    //                                $scope.ImportExcelSheetSelction._data.pop(val)
    //                            }
    //                        });
    //                    }
    //                    var dropdownlist = $("#familyimportsheetselect").data("kendoDropDownList");
    //                    dropdownlist.select(function (dataItem) {
    //                        dataItem.TABLE_NAME === response[0].TABLE_NAME;
    //                        //$scope.ProductDD = dataItem.TABLE_NAME;
    //                        return dataItem.TABLE_NAME;
    //                    });

    //                    $scope.importExcelSheetDDSelectionValue = response[0].TABLE_NAME;
    //                    if ($scope.importExcelSheetDDSelectionValue !== "") {
    //                        $scope.sheetselection = true;

    //                        $scope.SubproductsimportGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath);
    //                    }

    //                    else {
    //                        $("#importSelectionTable").hide();
    //                    }
    //                }
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //    }
    //});
    $scope.ImportExcelSheetSelction = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {

                dataFactory.attributemapping($scope.excelPath).success(function (response) {
                    $scope.autoChangeTableDesignerValue = response;
                    options.success(response);


                    if (response.length > 0) {
                        if ($rootScope.EnableSubProduct == false) {

                            var subProdList = [];
                            $scope.subproduct = response[0].TABLE_NAME;
                            angular.forEach($scope.ImportExcelSheetSelction._data, function (val) {
                                if (val.TABLE_NAME.toString().toUpperCase().includes("SUB")) {
                                    $scope.ImportExcelSheetSelction._data.pop(val)
                                }
                            });
                        }
                        var dropdownlist = $("#familyimportsheetselect").data("kendoDropDownList");
                        dropdownlist.select(function (dataItem) {
                            dataItem.TABLE_NAME === response[0].TABLE_NAME;
                            //$scope.ProductDD = dataItem.TABLE_NAME;
                            return dataItem.TABLE_NAME;
                        });

                        $scope.importExcelSheetDDSelectionValue = response[0].TABLE_NAME;
                        if ($scope.importExcelSheetDDSelectionValue !== "") {
                            $scope.sheetselection = true;

                            $scope.SubproductsimportGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath);
                        }

                        else {
                            $("#importSelectionTable").hide();
                        }
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.importExcelSheetDDSelectionValue = "";

    //$scope.importExcelSheetDDSelection = function (e, tabledesignerimporttype) {
    //    debugger;
    //    $scope.tableDesignerImportTypeValue = tabledesignerimporttype.toUpperCase();
    //    $scope.importExcelSheetDDSelectionValue = e.sender.value();
    //    var checkImportTypeValue = $scope.importExcelSheetDDSelectionValue;
    //    $scope.checkImportTypeValue = checkImportTypeValue.toUpperCase();
    //    if ($scope.checkImportTypeValue.contains("TABLEDESIGNER")) {

    //        $("#importSelectionTable").hide();
    //        $scope.tabledesignerimporttype = "Table Designer";
    //    }
    //    else if ($scope.checkImportTypeValue.contains("PRODUCTS")) {

    //        $("#importSelectionTable").show();
    //        $scope.tabledesignerimporttype = "Product";
    //    }
    //    if ($scope.importExcelSheetDDSelectionValue !== "TABLEDESIGNER" && $scope.importExcelSheetDDSelectionValue != "") {
    //        $scope.SubproductsimportGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath);
    //    }
    //    else {
    //        $("#importSelectionTable").hide();
    //    }
    //};
    $scope.importExcelSheetDDSelection = function (e, tabledesignerimporttype) {
       

        $scope.tableDesignerImportTypeValue = tabledesignerimporttype.toUpperCase();
        $scope.importExcelSheetDDSelectionValue = e.sender.value();
        var checkImportTypeValue = $scope.importExcelSheetDDSelectionValue;
        $scope.checkImportTypeValue = checkImportTypeValue.toUpperCase();
        if ($scope.checkImportTypeValue.contains("TABLEDESIGNER")) {

            $("#importSelectionTable").hide();
            $scope.tabledesignerimporttype = "Table Designer";
        }
        else if ($scope.checkImportTypeValue.contains("ITEMS")) {

            $("#importSelectionTable").show();
            $scope.tabledesignerimporttype = "Item";
        }
        else if ($scope.checkImportTypeValue.contains("PRODUCT")) {

            $("#importSelectionTable").show();
            $scope.tabledesignerimporttype = "Product";
        }
        if ($scope.importExcelSheetDDSelectionValue !== "TABLEDESIGNER" && $scope.importExcelSheetDDSelectionValue != "") {
            $scope.SubproductsimportGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath);
        }
        else {
            $("#importSelectionTable").hide();
        }
    };
    //$scope.importExcelAttributeDDSelection = function (e) {
    //    $scope.importExcelAttributeDDSelectionValue = e.sender.value();
    //};
    //$scope.btnFamilyfinishImport = function () {

    //    $scope.importDatas = []
    //    $scope.importDatas = $scope.prodImportData;


    //    $.msgBox({
    //        title: $localStorage.ProdcutTitle,
    //        content: "Family import may take some time to process.",
    //        type: "confirm",
    //        buttons: [{ value: "Ok" },
    //            { value: "Cancel" }
    //        ],
    //        success: function (result) {
    //            if (result === "Ok") {

    //                // $scope.loadingimg.refresh({ url: "../views/app/partials/ImgLoading.html" });
    //                //$scope.loadingimg.center().open();    
    //                //Its for an dynamical changed item and sun item#





    //                for (var i = 0 ; i < $scope.importDatas.length ; i++) {


    //                    if ($scope.importDatas[i]["ExcelColumn"].toUpperCase() == "ITEM#" && $localStorage.CatalogItemNumber.toUpperCase() != "ITEM#") {
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: 'Import failed due to errors, please Enter Valid CatalogItemNo.',
    //                            type: "error"
    //                        });
    //                        $("#importerror").show();
    //                        $("#importSelectionTable").hide();
    //                        return false;
    //                    }
    //                    //if ($scope.importDatas[i]["ExcelColumn"] == $localStorage.CatalogItemNumber) {
    //                    //    $scope.importDatas[i]["ExcelColumn"] = "ITEM#";
    //                    //    $scope.importDatas[i]["CatalogField"] = "ITEM#";
    //                    //    $scope.importDatas[i]["FieldType"] = "0";
    //                    //    $scope.importDatas[i]["IsSystemField"] = "True";
    //                    //    $scope.importDatas[i]["SelectedToImport"] = "True";
    //                    //}
    //                    //if ($scope.importDatas[i]["ExcelColumn"] == $localStorage.CatalogSubItemNumber)
    //                    //{
    //                    //    $scope.importDatas[i]["ExcelColumn"] = "SUBITEM#"
    //                    //    $scope.importDatas[i]["CatalogField"] = "SUBITEM#"
    //                    //    $scope.importDatas[i]["FieldType"] = "0";
    //                    //    $scope.importDatas[i]["IsSystemField"] = "True";
    //                    //    $scope.importDatas[i]["SelectedToImport"] = "True";
    //                    //}
    //                }
    //                dataFactory.BtnFamilyfinishImport($scope.importExcelSheetDDSelectionValue, $scope.allowDuplication, $scope.excelPath, $scope.Deleteproducts, $scope.importDatas).success(function (importresult) {
    //                    //  $scope.loadingimg.center().close();
    //                    $("#importSelectionTable").hide();                        
    //                    $scope.importresultdisable = true;
    //                    if (importresult == "Productid_Missing") {
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: 'Please select the valid sheet format',
    //                            type: "error"
    //                        });
    //                        return;
    //                    }
    //                    $scope.importSessionID = importresult.split("~", 2);   // kendo data source ;
    //                    if (importresult.split("~", 1) == "Import Failed") {
    //                        $scope.familypageimportsatatus = "Failed";
    //                        //$.msgBox({
    //                        //    title: "",
    //                        //    content: "Import Failed",
    //                        //    type: "error",
    //                        //    autoClose: false
    //                        //});
    //                        //$.confirm({
    //                        //    title: 'CatalogStudio',
    //                        //    content: 'Import Failed',
    //                        //    type: 'red',
    //                        //    typeAnimated: true,
    //                        //    buttons: {
    //                        //        tryAgain: {
    //                        //            text: 'OK',
    //                        //            btnClass: 'btn-red',
    //                        //            action: function () {
    //                        //            }
    //                        //        },

    //                        //    }
    //                        //});
    //                        //  $scope.SelectedFileForUploadnamefamilyproduct = "";
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: 'Import failed due to errors, please check the error log for details.',
    //                            type: "error"
    //                        });
    //                        $scope.btnimport = false;
    //                        if ($scope.HideEmptyErrorDiv == 0) {
    //                            $("#importErrorWindow").hide();
    //                        }
    //                        else {
    //                            $("#importErrorWindow").show();
    //                        }


    //                        $("#importSuccessWindow").hide();
    //                        $("#importSuccessWindow1").hide();
    //                        $scope.importResultDatasource.read();
    //                    }
    //                    else if (importresult.split("~", 1) == "SKU Exceed") {
    //                        $("#importErrorWindow").hide();
    //                        $("#importSuccessWindow1").hide();
    //                        $("#importSuccessWindow").hide();
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: 'You have exceeded the Maximum No. of SKUs as per your Plan.',
    //                            type: "error"
    //                        });
    //                        $scope.btnimport = false;
    //                        // $scope.SelectedFileForUploadnamefamilyproduct = "";
    //                    }
    //                    else if (importresult.contains("Import failed due to invalid Pick List value")) {
    //                        $scope.familypageimportsatatus = "Failed";
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: 'Import failed due to invalid Pick List value, please check the error log for details.',
    //                            type: "error"
    //                        });
    //                        // $scope.SelectedFileForUploadnamefamilyproduct = "";
    //                        $("#importpicklist").show();
    //                        $scope.getFinishImportFailedpicklistResults1.read();
    //                        $scope.btnimport = false;
    //                    }
    //                    else if (importresult.contains("Import failed due to errors")) {
    //                        $scope.familypageimportsatatus = "Failed";
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: 'Import failed due to errors, please check the error log for details.',
    //                            type: "error"
    //                        });
    //                        $("#importerror").show();
    //                        $scope.getFinishImportFailedpicklistResults12.read();
    //                        //  $scope.SelectedFileForUploadnamefamilyproduct = "";
    //                        $scope.btnimport = false;
    //                    }
    //                    else if (importresult.contains("Import Success Sub products")) {
    //                        $scope.familypageimportsatatus = "Success";
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: 'Import completed successful, please check log for details.',
    //                            type: "info"
    //                        });
    //                        $scope.getFinishImportFailedpicklistResults1subproducts.read();
    //                        $("#importSuccessWindowsubproduct").show();
    //                        //  $scope.SelectedFileForUploadnamefamilyproduct = "";
    //                        $scope.btnimport = false;
    //                        var $el = $('#importfamily');
    //                        $el.wrap('<form>').closest('form').get(0).reset();
    //                        $el.unwrap();
    //                        // $scope.SelectedFileForUpload = "";
    //                        $scope.excelPath = "";
    //                        $scope.ImportExcelSheetSelction.read();
    //                        $scope.excelname = "";
    //                    }
    //                    else if (importresult.contains("Import failed for sub products")) {
    //                        $scope.familypageimportsatatus = "Success";
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: 'Import failed, please check log for details.',
    //                            type: "info"
    //                        });
    //                        $("#importerrorWindowsubproduct").show();
    //                        //  $scope.SelectedFileForUploadnamefamilyproduct = "";
    //                        $scope.getFinishImporterrorlogsubprod.read();
    //                        $scope.btnimport = false;
    //                    }
    //                    else if (importresult.contains("Import failed [Cat #] cannot be empty.")) {
    //                        $scope.familypageimportsatatus = "Failed";
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: '' + importresult + '.',
    //                            type: "info"
    //                        });
    //                        //  $scope.SelectedFileForUploadnamefamilyproduct = "";
    //                        $scope.btnimport = false;
    //                    }
    //                    else if (importresult.contains("Cannot import subproducts.")) {
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: '' + importresult + '.',
    //                            type: "info"
    //                        });
    //                        $scope.btnimport = false;
    //                        // $scope.SelectedFileForUploadnamefamilyproduct = "";
    //                    }
    //                    else if (importresult.split("~", 1) == "Delete Success") {
    //                        $("#importSuccessWindow").hide();
    //                        $("#importerrorDeletelog").show();
    //                        $scope.getFinishImporterrorlogdelete.read();
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: 'Items deleted successfully.',
    //                            type: "error"
    //                        });
    //                    }
    //                    else if (importresult.split("~", 1) == "CATALOG_ID") {
    //                        $("#importSuccessWindow").hide();
    //                        $("#importSuccessWindow1").hide();
    //                        $("#importerrorDeletelog").hide();
    //                        //$scope.getFinishImporterrorlogdelete.read();
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: 'CATALOG_ID is required to delete items.',
    //                            type: "error"
    //                        });
    //                    }
    //                    else {
    //                        //$.msgBox({
    //                        //    title: "",
    //                        //    content: "Import Success",
    //                        //    type: "info",
    //                        //    autoClose: false
    //                        //});
    //                        //$.confirm({
    //                        //    title: 'CatalogStudio',
    //                        //    content: 'Import Success',
    //                        //    type: 'green',
    //                        //    typeAnimated: true,
    //                        //    buttons: {
    //                        //        tryAgain: {
    //                        //            text: 'OK',
    //                        //            btnClass: 'btn-green',
    //                        //            action: function () {
    //                        //            }
    //                        //        },

    //                        //    }
    //                        //});
    //                        $scope.familypageimportsatatus = "Success";
    //                        $("#importSelectionTable").hide();
    //                        $.msgBox({
    //                            title: $localStorage.ProdcutTitle,
    //                            content: 'Import completed successfully, please check log for details.',
    //                            type: "info"
    //                        });
    //                        if ($scope.importExcelSheetDDSelectionValue == "Products") {
    //                            $("#importSuccessWindow").show();
    //                            $("#importErrorWindow").hide();
    //                            $("#importSuccessWindow1").hide();
    //                        }
    //                        else {
    //                            $("#importSuccessWindow1").show();
    //                            $("#importErrorWindow").hide();
    //                            $("#importErrorWindow").hide();
    //                        }
    //                        $scope.importSuccessResultDatasource.read();
    //                        $scope.btnimport = false;
    //                        var $el = $('#importfamily');
    //                        $el.wrap('<form>').closest('form').get(0).reset();
    //                        $el.unwrap();
    //                        // $scope.SelectedFileForUpload = "";
    //                        $scope.excelPath = "";
    //                      //  $scope.ImportExcelSheetSelction.read();
    //                        // $scope.SelectedFileForUploadnamefamilyproduct = "";
    //                        $scope.excelname = "";
    //                    }
    //                    $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
    //                }).error(function (error) {
    //                    // $scope.loadingimg.center().close();
    //                    $scope.btnimport = false;
    //                    $scope.excelname = "";
    //                    $scope.familypageimportsatatus = "Failed";
    //                    $.msgBox({
    //                        title: $localStorage.ProdcutTitle,
    //                        content: 'Import failed, please check log for details.',
    //                        type: "error"
    //                    });
    //                    $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
    //                    // $scope.SelectedFileForUploadnamefamilyproduct = "";
    //                    options.error(error);
    //                });



    //            }
    //            else {

    //                return false;
    //            }
    //        }

    //    });




    //    //});
    //};
    $scope.allowDuplication = "0";
    //$scope.radioButtonYes = function () {
    //    $scope.allowDuplication = "1";
    //};
    $("#importSelectionTable").hide();
    if ($scope.excelPath != "") {
        $scope.tblDashImportBoard = new ngTableParams({ page: 1, count: 5 },
            {
                counts: [],
                total: function () { return $scope.prodImportData.length; },
                getData: function ($defer, params) {
                    var filteredData = $scope.prodImportData;
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(filteredData, params.orderBy()) :
                        filteredData;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });
    }
    $scope.SubproductsimportGridOptions = function (importExcelSheetDdSelectionValue, excelPath) {

        dataFactory.attributemappingGetImportSpecs(importExcelSheetDdSelectionValue, excelPath).success(function (response) {

            var obj = jQuery.parseJSON(response.Data.Data);

            $scope.prodImportData = obj.Data;


            // Pdf Xpress set as system fieds   - Start:

            for (var i = 0 ; i < $scope.prodImportData.length ; i++) {



                if ($scope.prodImportData[i]["ExcelColumn"] == 'Catalog_PdfTemplate' || $scope.prodImportData[i]["ExcelColumn"] == 'Category_PdfTemplate' || $scope.prodImportData[i]["ExcelColumn"] == 'Family_PdfTemplate' || $scope.prodImportData[i]["ExcelColumn"] == 'Product_PdfTemplate') {
                    $scope.prodImportData[i]["FieldType"] = "0";
                    $scope.prodImportData[i]["IsSystemField"] = "True";
                    $scope.prodImportData[i]["SelectedToImport"] = "True";
                }

            }

            // Pdf Xpress System fields - End 


            // To change the values for Item# into customer Item no





            if ($localStorage.CatalogItemNumber == "ITEM#" || $localStorage.CatalogItemNumber == "Item#" || $localStorage.CatalogItemNumber == "item#") {

                for (var i = 0 ; i < $scope.prodImportData.length ; i++) {

                    var ItemValue = $scope.prodImportData[i]["ExcelColumn"];

                    if ($scope.prodImportData[i]["ExcelColumn"].includes('PUBLISH2')) {
                        $scope.prodImportData[i]["FieldType"] = "0";
                        $scope.prodImportData[i]["IsSystemField"] = "True";
                        $scope.prodImportData[i]["SelectedToImport"] = "True";
                    }

                }

            } else {
                for (var i = 0 ; i < $scope.prodImportData.length ; i++) {

                    var ItemValue = $scope.prodImportData[i]["ExcelColumn"];

                    if ($scope.prodImportData[i]["ExcelColumn"] == $localStorage.CatalogItemNumber) {
                        $scope.prodImportData[i]["ExcelColumn"] = $localStorage.CatalogItemNumber;
                        $scope.prodImportData[i]["CatalogField"] = $localStorage.CatalogItemNumber;
                        $scope.prodImportData[i]["FieldType"] = "0";
                        $scope.prodImportData[i]["IsSystemField"] = "True";
                        $scope.prodImportData[i]["SelectedToImport"] = "True";

                    }
                    if ($scope.prodImportData[i]["ExcelColumn"].includes('PUBLISH2')) {
                        $scope.prodImportData[i]["FieldType"] = "0";
                        $scope.prodImportData[i]["IsSystemField"] = "True";
                        $scope.prodImportData[i]["SelectedToImport"] = "True";
                    }
                    if ($scope.prodImportData[i]["ExcelColumn"] == $localStorage.CatalogSubItemNumber) {
                        $scope.prodImportData[i]["ExcelColumn"] = $localStorage.CatalogSubItemNumber;
                        $scope.prodImportData[i]["CatalogField"] = $localStorage.CatalogSubItemNumber;
                        $scope.prodImportData[i]["FieldType"] = "0";
                        $scope.prodImportData[i]["IsSystemField"] = "True";
                        $scope.prodImportData[i]["SelectedToImport"] = "True";

                    }
                    if ($scope.prodImportData[i]["ExcelColumn"] == "ITEM#" || $scope.prodImportData[i]["ExcelColumn"] == "Item#" || $scope.prodImportData[i]["ExcelColumn"] == "item#") {

                        $scope.prodImportData[i]["CatalogField"] = "";
                        $scope.prodImportData[i]["FieldType"] = "";
                        $scope.prodImportData[i]["IsSystemField"] = "";
                        $scope.prodImportData[i]["SelectedToImport"] = "False";
                    }
                    if ($scope.prodImportData[i]["ExcelColumn"] == "SUBITEM#" || $scope.prodImportData[i]["ExcelColumn"] == "SubItem#" || $scope.prodImportData[i]["ExcelColumn"] == "Subitem#" || $scope.prodImportData[i]["ExcelColumn"] == "subitem#") {

                        $scope.prodImportData[i]["CatalogField"] = "";
                        $scope.prodImportData[i]["FieldType"] = "";
                        $scope.prodImportData[i]["IsSystemField"] = "";
                        $scope.prodImportData[i]["SelectedToImport"] = "False";
                    }

                }
            }
            $scope.columnsForImportAtt = obj.Columns;

        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.RadioCreateNew = function () {
        if ($scope.SelectedFileForUpload != null) {
            $scope.winNewxmlcreation.refresh({ url: "../views/app/partials/xmlcreation.html" });
            $scope.winNewxmlcreation.center().open();
        }
    };
    $scope.createXML = function () {
        $scope.newxmlname1 = $scope.newxmlname;
        $scope.winNewxmlcreation.center().close();
    };
    $scope.RadioUseSavedSettings = function () {
        if ($scope.SelectedFileForUpload != null) {
            $scope.winxmlupdation.refresh({ url: "../views/app/partials/xmlupdation.html" });
            $scope.winxmlupdation.center().open();
        }
    };
    $scope.AttributeDataSource1 = [
                 {
                     Text: "Alpha-Numeric [Family Level]",
                     value: 7
                },
                {
                    Text: "Image / Attachment Path [Family Level]", 
                    value: 9
                },
                {
                    Text: "Family-Attribute [Family Level]", 
                    value: 11
                },
                {
                    Text: "Family-Price [Family Level]", 
                    value: 12
                },
                {
                    Text: "Family-Key [Family Level]", 
                    value: 13
                },
                {
                     Text: "Alpha-Numeric [Product Level]", 
                     value: 1
                },
                {
                    Text: "Price / Numbers [Product Level]",
                    value: 4
                },
                {
                    Text: "Image / Attachment Path [Product Level]", 
                    value: 3
                },
                {
                    Text: "Date-Time(MM/DD/YYYY) [Product Level]", 
                    value: 10
                },
                {
                    Text: "Product Key Alpha-Numeric [Product Level]", 
                    value: 6
                },
                { Text: "-SYSTEM FIELD-", value: "SYSTEMFIELD" }
    ];

    $scope.AttrType = "";
    $scope.selectedRow = null;
    $scope.LastIndex = "";

    $scope.importResultDatasource = new kendo.data.DataSource({
        type: "json", autoBind: false,
        //serverFiltering: true, pageable: true, pageSize: 5, serverPaging: true,
        //serverSorting: true,
        sort: { field: "ErrorMessage", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishFamilyImportResults($scope.importSessionID).success(function (response) {
                    if (response.length != 0) {
                        options.success(response);
                        $scope.HideEmptyErrorDiv = response.length;
                        $("#importErrorWindow").show();
                    }
                    else {
                        $scope.HideEmptyErrorDiv = response.length;
                        $("#importErrorWindow").hide();
                    }

                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ErrorMessage",
                fields: {
                    ErrorMessage: { type: "string" },
                    CATALOG_NAME: { type: "string" },
                    CATEGORY_NAME: { type: "string" },
                    PRODUCT_ID: { type: "string" },
                    FAMILY_NAME: { type: "string" },
                    STATUS: { type: "string" },
                    ATTRIBUTE_NAME: { type: "string" },
                    ATTRIBUTE_VALUE: { type: "string" }
                }
            }
        }

    });

    $scope.importResultGridOptions = {
        dataSource: $scope.importResultDatasource, autoBind: false, resizable: true,
        scrollable: true,
        columns: [
        //{ field: "ErrorMessage", title: "Error Message", width: "200px" },
        //{ field: "ErrorProcedure", title: "Error Procedure", width: "200px" },
        //{ field: "ErrorSeverity", title: "Error Severity", width: "200px" },
        //{ field: "ErrorState", title: "Error State", width: "200px" },
        //{ field: "ErrorNumber", title: "Error Number", width: "200px" },
        //{ field: "ErrorLine", title: "Error Line" }
        { field: "CATALOG_NAME", title: "CATALOG", width: "200px" },
             { field: "CATEGORY_NAME", title: "CATEGORY", width: "200px" },
        { field: "PRODUCT_ID", title: "PRODUCT ID", width: "200px" },
            { field: "FAMILY_NAME", title: "FAMILY", width: "200px" },
        { field: "STATUS", title: "STATUS" },
        { field: "ATTRIBUTE_NAME", title: "ATTRIBUTE NAME" },
        { field: "ATTRIBUTE_VALUE", title: "ATTRIBUTE VALUE" }
        ]
    };


    $scope.importPicklistGrid = {
        dataSource: $scope.getFinishImportFailedpicklistResults1,
        autoBind: false, resizable: true,
        sortable: true,
        pageable: false,
        serverPaging: false,
        serverSorting: false,
        scrollable: true,
        columns: [
        { field: "STATUS", title: "STATUS", width: "200px" },
             { field: "ITEM_NO", title: "ITEM_NO", width: "200px" },
        { field: "ATTRIBUTE_NAME", title: "ATTRIBUTE_NAME", width: "200px" },
            { field: "PICKLIST_VALUE", title: "PICKLIST_VALUE", width: "200px" }

        ]
    };

    //$scope.getFinishImportFailedpicklistResults1 = new kendo.data.DataSource({
    //    type: "json",
    //    serverFiltering: false, pageable: false,
    //    scrollable: true,
    //    sort: { field: "STATUS", dir: "asc" },
    //    transport: {
    //        read: function (options) {
    //            //alert($scope.importSessionID);
    //            dataFactory.getFinishImportFailedpicklistResults($scope.importSessionID).success(function (response) {
    //                options.success(response);
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //    }, schema: {
    //        model: {
    //            //   id: "ATTRIBUTE_ID",
    //            fields: {
    //                STATUS: { type: "string" },
    //                ITEM_NO: { type: "string" },
    //                ATTRIBUTE_NAME: { type: "string" },
    //                PICKLIST_VALUE: { type: "string" }


    //            }
    //        }
    //    }
    //});

    $scope.gridColumns = [
    { field: "STATUS", title: "STATUS", width: "200px" },
             { field: "ITEM_NO", title: "ITEM_NO", width: "200px" },
        { field: "ATTRIBUTE_NAME", title: "ATTRIBUTE_NAME", width: "200px" },
            { field: "PICKLIST_VALUE", title: "PICKLIST_VALUE", width: "200px" }

    ];
    $scope.getFinishImportFailedpicklistResults1 = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autoBind: false,
        scrollable: true,
        sort: { field: "STATUS", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportFailedpicklistResults($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    STATUS: { type: "string" },
                    ITEM_NO: { type: "string" },
                    ATTRIBUTE_NAME: { type: "string" },
                    PICKLIST_VALUE: { type: "string" }


                }
            }
        }

    });

    $scope.getFinishImportFailedpicklistResults1subproducts = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autoBind: false,
        scrollable: true,
        sort: { field: "STATUS", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportFailedpicklistResultssubproducts($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    CATALOG_NAME: { type: "string" },
                    PRODUCT_DETATILS: { type: "string" },
                    SUBPRODUCT_DETATILS: { type: "string" },
                    STATUS: { type: "string" }


                }
            }
        }

    });


    $scope.getFinishImporterrorlogsubprod = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autoBind: false,
        scrollable: true,
        sort: { field: "STATUS", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportFailedpicklistResultssubproductserror($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    CATALOG_NAME: { type: "string" },
                    PRODUCT_DETATILS: { type: "string" },
                    SUBPRODUCT_DETATILS: { type: "string" },
                    STATUS: { type: "string" }


                }
            }
        }

    });


    $scope.getFinishImporterrorlogdelete = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autoBind: false,
        scrollable: true,
        sort: { field: "STATUS", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportFailedpicklistResultssubproductserrordelete($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    CATALOG_NAME: { type: "string" },
                    PRODUCT_DETATILS: { type: "string" },
                    SUBPRODUCT_DETATILS: { type: "string" },
                    STATUS: { type: "string" }


                }
            }
        }

    });
    $scope.importSuccessResultDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autoBind: false,
        scrollable: true,
        sort: { field: "CATALOG_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportSuccessResults($scope.importSessionID, $scope.importExcelSheetDDSelectionValue).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    CATALOG_NAME: { type: "string" },
                    CATEGORY_DETAILS: { type: "string" },
                    FAMILY_DETAILS: { type: "string" },
                    PRODUCT_DETAILS: { type: "string" },
                    STATUS: { type: "string" }
                }
            }
        }

    });
    $scope.importSuccessResultGridOptions = {
        dataSource: $scope.importSuccessResultDatasource,
        autoBind: false, resizable: true, autoBind: false,
        sortable: true,
        pageable: false,
        serverPaging: false,
        serverSorting: false,
        scrollable: true,
        columns: [
        { field: "CATALOG_NAME", title: "CATALOG", width: "200px" },
             { field: "CATEGORY_DETAILS", title: "CATEGORY", width: "200px" },
        { field: "PRODUCT_DETAILS", title: "PRODUCT", width: "200px" },
            { field: "FAMILY_DETAILS", title: "FAMILY", width: "200px" },
        { field: "STATUS", title: "STATUS" }
        ]
    };

    $scope.importSuccessResultGridOptions1 = {
        dataSource: $scope.importSuccessResultDatasource,
        autoBind: false, resizable: true, autoBind: false,
        sortable: true,
        pageable: false,
        serverPaging: false,
        serverSorting: false,
        scrollable: true,
        columns: [
        { field: "CATALOG_NAME", title: "CATALOG", width: "200px" },
             { field: "CATEGORY_DETAILS", title: "CATEGORY", width: "200px" },
               { field: "FAMILY_DETAILS", title: "FAMILY", width: "200px" },
        { field: "PRODUCT_DETAILS", title: "PRODUCT", width: "200px" },
         { field: "SUB_PRODUCT_DETAILS", title: "SUBPRODUCT", width: "200px" },

        { field: "STATUS", title: "STATUS" }
        ]
    };
    $scope.getFinishImportFailedpicklistResults12 = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false,
        scrollable: true,
        sort: { field: "STATUS", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportFailedpicklistResults12($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    STATUS: { type: "string" },
                    ITEM_NO: { type: "string" },
                    ATTRIBUTE_NAME: { type: "string" },
                    PICKLIST_VALUE: { type: "string" }


                }
            }
        }

    });

    $scope.btnback = function () {
        $scope.tabledesignerimporttype = "Product";
        $scope.importresultdisable = false;
       
        $("#importTableDesingnerFamilyLevelWindow").hide();
        $("#importTableSheetSlectectionWindow").hide();
        $("#importTableSheetWindow").show();
        $("#importerrorDeletelog").hide();
        // window.location.reload();
        //  var $el = $('#importfamily');
        //  $el.wrap('<form>').closest('form').get(0).reset();
        //  $el.unwrap(); $scope.sheetselection = false;
        $scope.btnimport = true;
        $("#importSuccessWindow").hide();
        $("#importSuccessWindow1").hide();
        $("#importerror").hide();
        $("#importpicklist").hide();
        $("#importErrorWindow").hide();
        $("#importSuccessWindowsubproduct").hide();
        $("#importerrorWindowsubproduct").hide();
        //  $scope.SelectedFileForUpload = "";
        //  $scope.excelPath = "";
        // $scope.ImportExcelSheetSelction.read();
        $("#importSelectionTable").hide();
        //$scope.excelname = "";
        var dropdownlist = $("#familyimportsheetselect").data("kendoDropDownList");
        dropdownlist.text(dropdownlist.options.optionLabel);
        dropdownlist.element.val(-1);
        dropdownlist.selectedIndex = -1;
    };

    

    $scope.itemdelete = function (e) {
        $scope.Deleteproducts = e.Deleteproducts;
    }
    $scope.btnback12 = function () {
        // $scope.SelectedFileForUpload = null;
        // $scope.ChechFileValid();
        $scope.btnimport = true;
        $scope.sheetselection = false;
        //$scope.UploadFile = "";
        $("#productgridtabstrip").show();
        $(".productgridtabstripclass").show();
        $("#divProductGrid").show();
        $("#productpaging1").show();
        $("#productpreview").hide();
        $("#configuratorproducts").hide();
        $("#columnsetupproduct").hide();
        $('#ColumnSetupSubproducts').hide();
        $("#Familyimport").hide();
        $("#importSuccessWindow").hide();
        $("#importSuccessWindow1").hide();
        $("#importerror").hide();
        $("#importpicklist").hide();
        $("#importErrorWindow").hide();
        $("#importSuccessWindowsubproduct").hide();
        $("#importerrorWindowsubproduct").hide();
        $scope.SelectedFileForUpload = "";
        $scope.excelPath = "";
        $scope.ImportExcelSheetSelction.read();
        $scope.excelname = "";
    };
    $scope.gridColumnsnew = [
    { field: "STATUS", title: "STATUS", width: "200px" },
     { field: "PRODUCT_ID", title: "PRODUCT_ID", width: "200px" },
         { field: "ITEM_NO", title: "ITEM_NO", width: "200px" }

    ];

    $scope.gridColumnssub = [
 { field: "CATALOG_NAME", title: "CATALOG_NAME", width: "200px" },
  { field: "PRODUCT_DETAILS", title: "PRODUCT_DETAILS", width: "200px" },
      { field: "SUBPRODUCT_DETAILS", title: "SUBPRODUCT_DETAILS", width: "200px" },
         { field: "STATUS", title: "STATUS", width: "200px" }

    ];

    $scope.gridColumnssuberror = [
{ field: "CATALOG_NAME", title: "CATALOG_NAME", width: "200px" },
{ field: "CATEGORY_NAME", title: "CATEGORY_NAME", width: "200px" },
{ field: "PRODUCT_ID", title: "PRODUCT_ID", width: "200px" },
    { field: "STATUS", title: "STATUS", width: "200px" },
  { field: "ATTRIBUTE_NAME", title: "ATTRIBUTE_NAME", width: "200px" },
    { field: "ATTRIBUTE_VALUE", title: "ATTRIBUTE_VALUE", width: "200px" }

    ];

    $scope.gridColumnsdelerror = [
{ field: "CATALOG_ITEM_NO", title: "CATALOG_ITEM_NO", width: "200px" },
{ field: "STATUS", title: "STATUS", width: "200px" }

    ];

    $rootScope.Reset = function () {
        $scope.SelectedFileForUpload = null;
        $rootScope.SelectedFileForUploadnamefamilyproduct = '';
        $scope.familyimport = true;
        $scope.tabledesignerimporttype = "Item";
        // $scope.IsFileValid = false;
        var $el = $('#importfamily');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        $("#Familyimport").show();
        $("#importSuccessWindow").hide();
        $("#importSuccessWindow1").hide();
        $("#importerror").hide();



    }

    //--------To get EnableSubProduct value from preference page-------
    $rootScope.getEnableSubProduct = function () {
        //$scope.CustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
        dataFactory.getEnableSubProduct().success(function (response) {

            $rootScope.EnableSubProduct = response;
            $localStorage.EnableSubProductLocal = response;

        })
    };


    //$scope.btnFamilyPagefinishImport = function (tabledesignerimporttype, btnFamilyPagefinishImport) {


    //    $rootScope.familyimportstatus = true;
    //    var validItem = true;
    //    $scope.tableimportworksheetdata = $("#familyimportsheetselect").val();
    //    if (($scope.tableimportworksheetdata == "Products" & $scope.tabledesignerimporttype == "Product") || ($scope.tableimportworksheetdata == "TableDesigner" & $scope.tabledesignerimporttype == "Table Designer")) {
    //        validItem = false;
    //    }


    //    if (validItem) {

    //        $.msgBox({
    //            title: $localStorage.ProdcutTitle,
    //            content: 'Invalid Import Type & Sheet Name.',
    //            type: "info"
    //        });
    //        return;
    //    }





    //    $scope.btnimport = false;
    //    $("#importTableDesingnerFamilyLevelWindow").hide();
    //    tabledesignerimporttype = tabledesignerimporttype.toUpperCase();
    //    if (tabledesignerimporttype == "PRODUCT") {
    //        $scope.btnFamilyfinishImport();
    //    }
    //    if (tabledesignerimporttype == "TABLE DESIGNER") {

    //        $.msgBox({
    //            title: $localStorage.ProdcutTitle,
    //            content: "Family import may take some time to process.",
    //            type: "confirm",
    //            buttons: [{ value: "Ok" },
    //                { value: "Cancel" }
    //            ],

    //            success: function (result) {
    //                if (result === "Ok") {
    //                    dataFactory.FinishTableDesignerImport($scope.importExcelSheetDDSelectionValue, $scope.excelPath).success(function (response) {
    //                        $scope.importresultdisable = true;
    //                        var tableDesignerFamilyLevel = [];
    //                        tableDesignerFamilyLevel = response.split("~");
    //                        if (tableDesignerFamilyLevel[0] == "Import Success") {
    //                            $scope.familypageimportsatatus = "Success";
    //                            $scope.familyLevelTableDesignerViewLog("importDatatable");
    //                            $.msgBox({
    //                                title: $localStorage.ProdcutTitle,
    //                                content: 'Import completed successful.Please check log for details.',
    //                                type: "info"
    //                            });
    //                        }
    //                        else if (tableDesignerFamilyLevel[0] == "Validation failed") {
    //                            $scope.familypageimportsatatus = "Failed";
    //                            $scope.familyLevelTableDesignerViewLog("ValidateTableDesignerTable");
    //                        }
    //                        else if (tableDesignerFamilyLevel[0] == "false") {
    //                            $scope.familypageimportsatatus = "Failed";
    //                            $.msgBox({
    //                                title: $localStorage.ProdcutTitle,
    //                                content: 'Family level table designer import failed,please try again later',
    //                                type: "info"
    //                            });
    //                        }
    //                    });
    //                }
    //            }
    //        });
    //    }

    //}
    //Siranjeevi Family Level Import//
    $scope.btnFamilyPagefinishImport = function (tabledesignerimporttype, btnFamilyPagefinishImport) {
        $rootScope.familyimportstatus = true;
        var validItem = true;
        $scope.tableimportworksheetdata = $("#familyimportsheetselect").val();
      
        if (($scope.tableimportworksheetdata == "Items" & $scope.tabledesignerimporttype == "Item") || ($scope.tableimportworksheetdata == "TableDesigner" & $scope.tabledesignerimporttype == "Table Designer") || ($scope.tableimportworksheetdata == "Products" & $scope.tabledesignerimporttype == "Product")) {
            validItem = false;
        }

        if (validItem) {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Invalid Import Type & Sheet Name.',
                type: "info"
            });
            return;
        }

        $scope.btnimport = false;
        $("#importTableDesingnerFamilyLevelWindow").hide();
        tabledesignerimporttype = tabledesignerimporttype.toUpperCase();
        if (tabledesignerimporttype == "PRODUCT") {
            $scope.btnFamilyImport();
        }
        if (tabledesignerimporttype == "ITEM") {
            $scope.btnFamilyfinishImport();
        }
        if (tabledesignerimporttype == "TABLE DESIGNER") {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Product import may take some time to process.",
                type: "confirm",
                buttons: [{ value: "Ok" },
                    { value: "Cancel" }
                ],

                success: function (result) {
                    if (result === "Ok") {
                        dataFactory.FinishTableDesignerImport($scope.importExcelSheetDDSelectionValue, $scope.excelPath).success(function (response) {
                            $scope.importresultdisable = true;
                            var tableDesignerFamilyLevel = [];
                            tableDesignerFamilyLevel = response.split("~");
                            if (tableDesignerFamilyLevel[0] == "Import Success") {
                                $scope.familypageimportsatatus = "Success";
                                $scope.familyLevelTableDesignerViewLog("importDatatable");
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Import completed successful.Please check log for details.',
                                    type: "info"
                                });
                               
                          /*      $("#family-import").addClass("disabledbutton");*/
                            }
                            else if (tableDesignerFamilyLevel[0] == "Validation failed") {
                                $scope.familypageimportsatatus = "Failed";
                                $scope.familyLevelTableDesignerViewLog("ValidateTableDesignerTable");
                            }
                            else if (tableDesignerFamilyLevel[0] == "false") {
                                $scope.familypageimportsatatus = "Failed";
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Product level table designer import failed,please try again later',
                                    type: "info"
                                });
                            }
                        });
                    }
                }
            });
        }

    }


    $scope.btnFamilyImport = function () {

        $scope.importDatas = []
        $scope.importDatas = $scope.prodImportData;


        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Product import may take some time to process.",
            type: "confirm",
            buttons: [{ value: "Ok" },
                { value: "Cancel" }
            ],
            success: function (result) {
                if (result === "Ok") {



                    dataFactory.BtnFamilyfinishImport($scope.importExcelSheetDDSelectionValue, $scope.allowDuplication, $scope.excelPath, $scope.Deleteproducts, $scope.importDatas).success(function (importresult) {
                        if (importresult != null) {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Product level Import completed successful.',
                                type: "info"
                            });
                            $scope.btnimport = true;
                            $scope.tableimportworksheetdata = "Items";
                            $scope.tableDesignerImportTypeValue = "PRODUCT";
                            $scope.importExcelSheetDDSelectionValue = "Items";
                            var checkImportTypeValue = "Items";
                            $scope.checkImportTypeValue = "ITEMS";
                            if ($scope.checkImportTypeValue.contains("TABLEDESIGNER")) {

                                $("#importSelectionTable").hide();
                                $scope.tabledesignerimporttype = "Table Designer";
                            }
                            else if ($scope.checkImportTypeValue.contains("ITEMS")) {

                                $("#importSelectionTable").show();
                                $scope.tabledesignerimporttype = "Item";
                            }
                            else if ($scope.checkImportTypeValue.contains("FAM")) {

                                $("#importSelectionTable").show();
                                $scope.tabledesignerimporttype = "Product";
                            }
                            if ($scope.importExcelSheetDDSelectionValue !== "TABLEDESIGNER" && $scope.importExcelSheetDDSelectionValue != "") {
                                $scope.SubproductsimportGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath);
                            }
                            else {
                                $("#importSelectionTable").hide();
                                $("#family-import").addClass("disabledbutton1");
                            }
                        }

                    });
                }
            }

        });
    };

    $scope.btnFamilyfinishImport = function () {
        
        $scope.importDatas = []
        $scope.importDatas = $scope.prodImportData;


        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Product import may take some time to process.",
            type: "confirm",
            buttons: [{ value: "Ok" },
                { value: "Cancel" }
            ],
            success: function (result) {
                if (result === "Ok") {

                    // $scope.loadingimg.refresh({ url: "../views/app/partials/ImgLoading.html" });
                    //$scope.loadingimg.center().open();    
                    //Its for an dynamical changed item and sun item#





                    for (var i = 0 ; i < $scope.importDatas.length ; i++) {


                        if ($scope.importDatas[i]["ExcelColumn"].toUpperCase() == "ITEM#" && $localStorage.CatalogItemNumber.toUpperCase() != "ITEM#") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Import failed due to errors, please Enter Valid CatalogItemNo.',
                                type: "error"
                            });
                            $("#importerror").show();
                            $("#importSelectionTable").hide();
                            return false;
                        }
                        //if ($scope.importDatas[i]["ExcelColumn"] == $localStorage.CatalogItemNumber) {
                        //    $scope.importDatas[i]["ExcelColumn"] = "ITEM#";
                        //    $scope.importDatas[i]["CatalogField"] = "ITEM#";
                        //    $scope.importDatas[i]["FieldType"] = "0";
                        //    $scope.importDatas[i]["IsSystemField"] = "True";
                        //    $scope.importDatas[i]["SelectedToImport"] = "True";
                        //}
                        //if ($scope.importDatas[i]["ExcelColumn"] == $localStorage.CatalogSubItemNumber)
                        //{
                        //    $scope.importDatas[i]["ExcelColumn"] = "SUBITEM#"
                        //    $scope.importDatas[i]["CatalogField"] = "SUBITEM#"
                        //    $scope.importDatas[i]["FieldType"] = "0";
                        //    $scope.importDatas[i]["IsSystemField"] = "True";
                        //    $scope.importDatas[i]["SelectedToImport"] = "True";
                        //}
                    }
                    dataFactory.BtnFamilyfinishImport($scope.importExcelSheetDDSelectionValue, $scope.allowDuplication, $scope.excelPath, $scope.Deleteproducts, $scope.importDatas).success(function (importresult) {
                        //  $scope.loadingimg.center().close();
                        $("#importSelectionTable").hide();
                        $scope.importresultdisable = true;
                        if (importresult == "Productid_Missing") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Please select the valid sheet format',
                                type: "error"
                            });
                            return;
                        }
                        $scope.importSessionID = importresult.split("~", 2);   // kendo data source ;
                        if (importresult.split("~", 1) == "Import Failed") {
                            $scope.familypageimportsatatus = "Failed";
                            //$.msgBox({
                            //    title: "",
                            //    content: "Import Failed",
                            //    type: "error",
                            //    autoClose: false
                            //});
                            //$.confirm({
                            //    title: 'CatalogStudio',
                            //    content: 'Import Failed',
                            //    type: 'red',
                            //    typeAnimated: true,
                            //    buttons: {
                            //        tryAgain: {
                            //            text: 'OK',
                            //            btnClass: 'btn-red',
                            //            action: function () {
                            //            }
                            //        },

                            //    }
                            //});
                            //  $scope.SelectedFileForUploadnamefamilyproduct = "";
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Import failed due to errors, please check the error log for details.',
                                type: "error"
                            });
                            $scope.btnimport = false;
                            if ($scope.HideEmptyErrorDiv == 0) {
                                $("#importErrorWindow").hide();
                            }
                            else {
                                $("#importErrorWindow").show();
                            }


                            $("#importSuccessWindow").hide();
                            $("#importSuccessWindow1").hide();
                            $scope.importResultDatasource.read();
                        }
                        else if (importresult.split("~", 1) == "SKU Exceed") {
                            $("#importErrorWindow").hide();
                            $("#importSuccessWindow1").hide();
                            $("#importSuccessWindow").hide();
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'You have exceeded the Maximum No. of SKUs as per your Plan.',
                                type: "error"
                            });
                            $scope.btnimport = false;
                            // $scope.SelectedFileForUploadnamefamilyproduct = "";
                        }
                        else if (importresult.contains("Import failed due to invalid Pick List value")) {
                            $scope.familypageimportsatatus = "Failed";
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Import failed due to invalid Pick List value, please check the error log for details.',
                                type: "error"
                            });
                            // $scope.SelectedFileForUploadnamefamilyproduct = "";
                            $("#importpicklist").show();
                            $scope.getFinishImportFailedpicklistResults1.read();
                            $scope.btnimport = false;
                        }
                        else if (importresult.contains("Import failed due to errors")) {
                            $scope.familypageimportsatatus = "Failed";
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Import failed due to errors, please check the error log for details.',
                                type: "error"
                            });
                            $("#importerror").show();
                            $scope.getFinishImportFailedpicklistResults12.read();
                            //  $scope.SelectedFileForUploadnamefamilyproduct = "";
                            $scope.btnimport = false;
                        }
                        else if (importresult.contains("Import Success Sub products")) {
                            $scope.familypageimportsatatus = "Success";
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Import completed successful, please check log for details.',
                                type: "info"
                            });
                            $("#family-import").addClass("disabledbutton1");
                            $scope.getFinishImportFailedpicklistResults1subproducts.read();
                            $("#importSuccessWindowsubproduct").show();
                            //  $scope.SelectedFileForUploadnamefamilyproduct = "";
                            $scope.btnimport = false;
                            var $el = $('#importfamily');
                            $el.wrap('<form>').closest('form').get(0).reset();
                            $el.unwrap();
                            // $scope.SelectedFileForUpload = "";
                            $scope.excelPath = "";
                            $scope.ImportExcelSheetSelction.read();
                            $scope.excelname = "";
                        }
                        else if (importresult.contains("Import failed for sub products")) {
                            $scope.familypageimportsatatus = "Success";
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Import failed, please check log for details.',
                                type: "info"
                            });
                            $("#importerrorWindowsubproduct").show();
                            //  $scope.SelectedFileForUploadnamefamilyproduct = "";
                            $scope.getFinishImporterrorlogsubprod.read();
                            $scope.btnimport = false;
                        }
                        else if (importresult.contains("Import failed [Cat #] cannot be empty.")) {
                            $scope.familypageimportsatatus = "Failed";
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + importresult + '.',
                                type: "info"
                            });
                            //  $scope.SelectedFileForUploadnamefamilyproduct = "";
                            $scope.btnimport = false;
                        }
                        else if (importresult.contains("Cannot import subproducts.")) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + importresult + '.',
                                type: "info"
                            });
                            $scope.btnimport = false;
                            // $scope.SelectedFileForUploadnamefamilyproduct = "";
                        }
                        else if (importresult.split("~", 1) == "Delete Successfully") {
                            $("#importSuccessWindow").hide();
                            $("#importerrorDeletelog").show();
                            $scope.getFinishImporterrorlogdelete.read();
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Items deleted successfully.',
                                type: "error"
                            });
                        }
                        else if (importresult.split("~", 1) == "CATALOG_ID") {
                            $("#importSuccessWindow").hide();
                            $("#importSuccessWindow1").hide();
                            $("#importerrorDeletelog").hide();
                            //$scope.getFinishImporterrorlogdelete.read();
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'CATALOG_ID is required to delete items.',
                                type: "error"
                            });
                        }
                        else {
                            //$.msgBox({
                            //    title: "",
                            //    content: "Import Success",
                            //    type: "info",
                            //    autoClose: false
                            //});
                            //$.confirm({
                            //    title: 'CatalogStudio',
                            //    content: 'Import Success',
                            //    type: 'green',
                            //    typeAnimated: true,
                            //    buttons: {
                            //        tryAgain: {
                            //            text: 'OK',
                            //            btnClass: 'btn-green',
                            //            action: function () {
                            //            }
                            //        },

                            //    }
                            //});
                            $scope.familypageimportsatatus = "Success";
                            $("#importSelectionTable").hide();
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Import completed successfully, please check log for details.',
                                type: "info"
                            });
                            $("#family-import").addClass("disabledbutton1");
                            if ($scope.importExcelSheetDDSelectionValue == "Items") {
                                $("#importSuccessWindow").show();
                                $("#importErrorWindow").hide();
                                $("#importSuccessWindow1").hide();
                            }
                            else {
                                $("#importSuccessWindow1").show();
                                $("#importErrorWindow").hide();
                                $("#importErrorWindow").hide();
                            }
                            $scope.importSuccessResultDatasource.read();
                            $scope.btnimport = false;
                            var $el = $('#importfamily');
                            $el.wrap('<form>').closest('form').get(0).reset();
                            $el.unwrap();
                            // $scope.SelectedFileForUpload = "";
                            $scope.excelPath = "";
                            //  $scope.ImportExcelSheetSelction.read();
                            // $scope.SelectedFileForUploadnamefamilyproduct = "";
                            $scope.excelname = "";
                        }
                        $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
                    }).error(function (error) {
                        // $scope.loadingimg.center().close();
                        $scope.btnimport = false;
                        $scope.excelname = "";
                        $scope.familypageimportsatatus = "Failed";
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Import failed, please check log for details.',
                            type: "error"
                        });
                        $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
                        // $scope.SelectedFileForUploadnamefamilyproduct = "";
                        options.error(error);
                    });



                }
                else {

                    return false;
                }
            }

        });




        //});
    };
    //Siranjeevi Family Level Import//

    $scope.familyLevelTableDesignerViewLog = function (tableName) {
        dataFactory.FamilyLevelTableDesignerViewLog(tableName).success(function (response) {
            if (response != null) {
                var obj = jQuery.parseJSON(response.Data.Data);
                $scope.validateLogprod = obj;
                $scope.validateLogcolumn = response.Data.Columns;
                $("#importTableDesingnerFamilyLevelWindow").show();
            }
        });
    }

    $scope.tableDesignerImportchangeevent = function (tabledesignerimporttype) {
        tabledesignerimporttype = tabledesignerimporttype.toUpperCase();

        var Tempitem;

        if (tabledesignerimporttype == "TABLE DESIGNER") {
            Tempitem = "TABLEDESIGNER"
        }

        if (tabledesignerimporttype == "ITEM") {
            Tempitem = "ITEMS";
        }
        if (tabledesignerimporttype == "PRODUCTS") {
            Tempitem = "Product";
        }


        if ($scope.importExcelSheetDDSelectionValue.toUpperCase() != Tempitem) {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Invalid Import Type & Sheet Name.',
                type: "error"
            });

            return;
        }


        if (tabledesignerimporttype == "TABLE DESIGNER") {
            $("#importSelectionTable").hide();
        }
        else if (tabledesignerimporttype == "ITEM") {
            $("#importSelectionTable").show();
        }

    }

    $scope.export_log = function () {

        window.open("DownloadFamilyPageLogFile.ashx?ImportStatus=" + $scope.familypageimportsatatus);
    };

    return $scope.UploadFile;



}]);
