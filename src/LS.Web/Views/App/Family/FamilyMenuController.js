﻿LSApp.controller('MenuController', ['$scope', function ($scope) {
    function onFamilyClick() {
        $scope.SetFlag();
    }
    function create() {
        $scope.Create();
    }
    function showNewFmlyAttrAdd() {
        $scope.familyAttributeSetupClick();
    }
    function removeFamily() {
        $scope.removeFamilyPermanently();
    }
    function removeFamilyFromMaster() {
        $scope.removeFamilyPermanentlyFromMaster();
    }
    function trashFamilyFromMaster() {
        $scope.trashFamilyPermanentlyFromMaster();
    }
    function emptyFunction() {

    }
    function deleteLayoutClick() {
        $scope.deleteLayoutClick();
    }
    function createLayoutClick() {
        $scope.createLayoutClick();
    }
    function defaultLayoutClick() {
        $scope.defaultLayoutClick();
    }
    function previewFamily() {
        $scope.previewFamily();
    }
    function tabledddDesignerClick() {
        $scope.tableDesignerClick();
    }
    function addSuperColumn() {
        // $scope.addSuperColumn();
    }
    function onSubFamilyClick() {
        $scope.btnSubFamilyCreate();
    }
    function onExportClick() {
        $scope.GetProdSpecsExport();
    }
    $scope.toolbarOptions = {
        items: [
            {
                type: "splitButton",
                text: "New", id: 'user-toolbar-create-new-family', attributes: { "class": "red" }, click: function () {
                    onFamilyClick();
                },
                menuButtons: [
                    //{
                    //    text: "New Family", attributes: { "class": "red" }, click: function () {
                    //        onFamilyClick();
                    //    }
                    //},
                    {
                        text: "New SubFamily", attributes: { "class": "red" }, click: function () {
                            onSubFamilyClick();
                        }
                    }
                    //, {
                    //    text: "Clone Family", attributes: { "class": "red" }, click: function (e) {
                    //        alert(e.target.text() + " is clicked");
                    //    }
                    //}
                ]
            },
             {
                 id: 'user-toolbar-preview',
                 type: "button", text: "Preview", attributes: { "class": "red" }, click: previewFamily
             },
             {
                 id: 'user-toolbar-save-family',
                 type: "button", text: "Save", attributes: { "class": "red" }, click: create
             },
            {
                id: 'user-toolbar-association-removal-family',
                type: "button", text: "Detach", attributes: { "class": "red" }, click: removeFamily
            },
            {
                id: 'user-toolbar-trash-family',
                type: "button", text: "Delete", attributes: { "class": "red" }, click: trashFamilyFromMaster
            },
            //Move to Recycle Bin
            //{
            //    id: 'user-toolbar-delete-family',
            //    type: "button", text: "Delete", attributes: { "class": "red" }, click: removeFamilyFromMaster
            //},

              {
                  id: 'user-toolbar-family-attribute-setup',
                  type: "button", text: "Attribute Setup", attributes: { "class": "red" }, click: showNewFmlyAttrAdd
              },

                  {
                      id: 'user-toolbar-default-layout',
                      type: "splitButton",
                      text: "Default Layout", attributes: { "class": "red" }, click: function () {
                          emptyFunction();
                      },
                      menuButtons: [
                          {
                              id: 'user-toolbar-create-new-layout',
                              text: "Create New Layout", attributes: { "class": "red" }, click: function () {
                                  createLayoutClick();
                              }

                          },
                          {
                              id: 'user-toolbar-delete-layout',
                              text: "Delete Layout", attributes: { "class": "red" }, click: function () {
                                  deleteLayoutClick();
                              }

                          },
                          {
                              id: 'user-toolbar-set-table-layout',
                              text: "Set Default Layout", attributes: { "class": "red" }, click: function () {
                                  defaultLayoutClick();
                              }

                          }
                      ]
                  },
                  {
                      id: 'user-toolbar-table-designer', type: "button", text: "Table Designer", attributes: { "class": "red" }, click: tabledddDesignerClick
                  },

              {
                  id: 'user-toolbar-export', type: "button", text: "Export", attributes: { "class": "red" }, click: onExportClick
              }

        ]
    };
}]);