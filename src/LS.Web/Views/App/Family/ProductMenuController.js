﻿LSApp.controller('ProductMenuController', ['$scope', function ($scope) {
    //function preview() {
    //    $scope.btnProductPreview();
    //}
    function create() {
        $scope.createNewProduct();
    }
    function cut()
    {
    $scope.cut();
    }
    function copy() {
        $scope.copy();
    }
    function paste() {
        $scope.paste();
    }
    //function onOpenLayoutClick() {
    //    $scope.winopenlayout.refresh({ url: "../Views/App/Partials/OpenProductLayout.html" });
    //    $scope.winopenlayout.center().open();
    //}
    //function onDeleteLayoutClick() {
    //    $scope.winRemovelayout.refresh({ url: "../Views/App/Partials/DeleteProductLayout.html" });
    //    $scope.winRemovelayout.center().open();
    //}
    //function onCreateNewLayoutClick() {
    //    $scope.winCreationnewlayout.refresh({ url: "../Views/App/Partials/ProductLayout.html" });
    //    $scope.winCreationnewlayout.center().open();
    //}
    function onColumnSetupClick() {
        //$scope.winColumnSetuplayout.refresh({ url: "../Views/App/Partials/ColumnSetup.html" });
        //$scope.winColumnSetuplayout.center().open();
        //$scope.winColumnSetuplayout.center().close();
        //$("#dynamictable").hide();
        //$("#productpreview").hide();
        //$("#copy").hide();
        //$("#cut").hide();
        //$("#paste").hide();
        //$("#configuratorproducts").hide();
        //$("#newProductBtn").hide();
        //$("#newProductBtn1").hide();
        $("#gridproduct").hide();
        $('#productpreview').hide();
        $('#configuratorproducts').hide();
        $scope.RefreshProductAttributes();
        $("#columnsetupproduct").show();
       
    }

    function AddproductConfigurator() {       
        $scope.Addtoprodctconfigurator();
    }
    function ConfiguratorList() {
        $scope.ConfiguratorList();
    }

    $scope.producttoolbarOptions =  {
        
        items: [
            {
                type: "button", text: "New Product", attributes: { "class": "red" }, click: create
            }
            //{

            //    type: "button", text: "Paste", attributes: { "class": "red" }, click: paste
            //}

             //{
             //    type: "button", text: "Preview", attributes: { "class": "red" }, click: preview
             //},
            //{
            //    type: "splitButton",
            //    text: "Open Layout", attributes: { "class": "red" }, click: function () {
            //        onOpenLayoutClick();
            //    },
            //    menuButtons: [
            //        {
            //            text: "Open Layout", attributes: { "class": "red" }, click: function () {
            //                onOpenLayoutClick();
            //            }
            //        },
            //        {
            //            text: "Create New Layout", attributes: { "class": "red" }, click: function () {
            //                onCreateNewLayoutClick();
            //            }
            //        },
            //        {
            //            text: "Delete Layout", attributes: { "class": "red" }, click: function (e) {
            //                onDeleteLayoutClick();
            //            }
            //        }
            //    ]
            //},
          

             

        ]
        
    };
    
    $scope.producttoolbarOptions1 = {

        items: [
            {
                id: 'product-toolbar-new-product', type: "button", text: "New Product", attributes: { "class": "red" }, click: create
            },
             {

                 id: 'product-toolbar-column-setup', type: "button", text: "Column Setup", attributes: { "class": "red" }, click: onColumnSetupClick
             },
             {

                     id: 'product-toolbar-addtocutpaste', type: "button", text: "Cut", attributes: { "class": "red" }, click: cut
             },
             {

                 id: 'product-toolbar-addtocopypaste', type: "button", text: "Copy", attributes: { "class": "red" }, click: copy
             },
             {

                id: 'product-toolbar-addtopaste', type: "button", text: "Paste", attributes: { "class": "red" }, click: paste
             }

             ,

              {
                  id: 'product-toolbar-addtoproductcongig', type: "button", text: "Add to Product Configurator", attributes: { "class": "red" }, click: AddproductConfigurator
              }
              ,
              {
                  id: 'product-toolbar-productcongig', type: "button", text: "Product Configurator", attributes: { "class": "red" }, click: ConfiguratorList
              }

        ]

    };


   
}]);