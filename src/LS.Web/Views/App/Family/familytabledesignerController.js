﻿LSApp.controller('familytabledesignerController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, $rootScope, $localStorage) {
    $scope.layoutType = 'Yes';
    $scope.structureType = '';
    $scope.DispalyRowHeaderCheckEditor = {
        value1: true
    };
    $scope.DisplayColumnHeaderCheckBox = {
        value1: false
    };
    $scope.DispalySummaryHeaderCheckEditor = {
        value1: false
    };
    $rootScope.familytableStructurechanges = '';

    //////get structure name is default or not

    $scope.IsDefaultStructureName = false;
    $scope.disableApplyBtn = true;
    $scope.disableClearBtn = true;

     ////////////////////////////////////////



    //        $scope.VerticalTable_ChkBox = {
    //            value1: false
    //        };
    //$scope.StructureType = {
    //    dataSource: {
    //        data: [
    //            { Text: "Horizontal", value: "Horizontal" },
    //            { Text: "Vertical", value: "Vertical" },
    //            { Text: "Super Table", value: "SuperTable" }
    //        ]
    //    },
    //    dataTextField: "Text",
    //    dataValueField: "value"
    //};
    //$scope.Structure = {
    //    structureName: "Sample",
    //    defaultlayout: true,
    //    tabletype: ""
    //};
    //$scope.StructureTypeChange = function (e) {
    //    $scope.Structure.tabletype = e.sender.value();
    //    if ($scope.Structure.tabletype == 'Vertical') {
    //        $scope.VerticalTable_ChkBox = {
    //            value1: true
    //        };

    //    } else {
    //        $scope.VerticalTable_ChkBox = {
    //            value1: false
    //        };
    //    }
    //};                                  
    $scope.tableDesignerFields = {
        ComboGroupByColumn: 0,
        ComboSummaryGroup: 0,
        EmptyCellTextBox: '',
        PivotHeaderTextBox: ''
    };
    $scope.tableDesignerFields.PivotHeaderTextBox = '';
    $scope.tableDesignerFields.EmptyCellTextBox = '';

   


    $scope.onDragEnd = function () {
        //var draggable = $("#treeviewallAttributeList");
        //if (!draggable.data("kendoDraggable").dropped) {
        //    alert("sdf");
        //}
    };
    $scope.init();
    $scope.StructureName = 'Default Layout';
    $scope.familyTableStructure = "<?xml version=\"1.0\" encoding=\"utf-8\"?><TradingBell TableType=\"Horizontal\"><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>";
    $scope.Flag = "FamilyTable";
    $scope.multiplegroupid = 0;



    $rootScope.leftTreeNewDataSource = new kendo.data.HierarchicalDataSource({

        type: "json",

        loadOnDemand: false,
        transport: {
            read: function (options) {
                //$scope.getStructureValues();
                if ($rootScope.familytableStructurechanges != 'undefined' && $rootScope.familytableStructurechanges != undefined && $rootScope.familytableStructurechanges != "") {
                    $scope.familyTableStructure = $rootScope.familytableStructurechanges;
                }
                dataFactory.DecodeLayoutXmlLeftTree($scope.StructureName, $scope.familyTableStructure, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, options.data.ATTRIBUTE_NAME, $scope.Flag, $scope.multiplegroupid, $scope.SelectedProductPackId).success(function (response) {

                    for (var i = 0 ; response.length > i ; i++) {
                        if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                        }
                        if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                        }
                    }
                    $rootScope.allAttributeListDatasource = new kendo.data.HierarchicalDataSource({
                        type: "json",
                        loadOnDemand: false,
                        transport: {
                            read: function (options) {
                                if ($rootScope.familytableStructurechanges != 'undefined' && $rootScope.familytableStructurechanges != undefined && $rootScope.familytableStructurechanges != "") {
                                    $scope.familyTableStructure = $rootScope.familytableStructurechanges;
                                }
                                dataFactory.getAllAttributeDataSource($scope.StructureName, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.familyTableStructure, $scope.Flag, $scope.multiplegroupid, $scope.SelectedProductPackId).success(function (response) {
                                 

                                    for (var i = 0 ; response.length > i ; i++) {
                                        if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                                        }
                                        if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                                        }
                                    }

                                    options.success(response);
                                }).error(function (error) {
                                    options.error(error);
                                });

                            }
                        },
                        schema: {
                            model: {
                                id: "ATTRIBUTE_NAME",
                                hasChildren: "hasChildren"
                            }
                        }
                    });

                    $rootScope.rightTreeNewDataSource.read();

                    $rootScope.summaryTreeNewDataSource.read();

                    $rootScope.columnTreeNewDataSource.read();

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_NAME",
                hasChildren: "hasChildren"
            }
        }
    });
   
    $rootScope.rightTreeNewDataSource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                if ($rootScope.familytableStructurechanges != 'undefined' && $rootScope.familytableStructurechanges != undefined && $rootScope.familytableStructurechanges != "") {
                    $scope.familyTableStructure = $rootScope.familytableStructurechanges;
                }
                dataFactory.DecodeLayoutXmlRightTree($scope.StructureName, $scope.familyTableStructure, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, options.data.ATTRIBUTE_NAME, $scope.Flag, $scope.multiplegroupid, $scope.SelectedProductPackId).success(function (response) {
                    for (var i = 0 ; response.length > i ; i++) {
                        if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                        }
                        if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                        }
                    }

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_NAME",
                hasChildren: "hasChildren"
            }
        }
    });

    $rootScope.summaryTreeNewDataSource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                if ($rootScope.familytableStructurechanges != 'undefined' && $rootScope.familytableStructurechanges != undefined && $rootScope.familytableStructurechanges != "") {
                    $scope.familyTableStructure = $rootScope.familytableStructurechanges;
                }
                dataFactory.DecodeLayoutXmlSummaryTree($scope.StructureName, $scope.familyTableStructure, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, options.data.ATTRIBUTE_NAME, $scope.Flag, $scope.multiplegroupid, $scope.SelectedProductPackId).success(function (response) {

                    for (var i = 0 ; response.length > i ; i++) {
                        if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                        }
                        if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                        }
                    }


                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_NAME",
                hasChildren: "hasChildren"
            }
        }
    });

    $rootScope.columnTreeNewDataSource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                if ($rootScope.familytableStructurechanges != 'undefined' && $rootScope.familytableStructurechanges != undefined && $rootScope.familytableStructurechanges != "") {
                    $scope.familyTableStructure = $rootScope.familytableStructurechanges;
                }
                dataFactory.DecodeLayoutXmlColumnTree($scope.StructureName, $scope.familyTableStructure, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, options.data.ATTRIBUTE_NAME, $scope.Flag, $scope.multiplegroupid, $scope.SelectedProductPackId).success(function (response) {

                    for (var i = 0 ; response.length > i ; i++) {
                        if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                        }
                        if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                        }
                    }

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_NAME",
                hasChildren: "hasChildren"
            }
        }
    });



    $rootScope.tableGroupSummaryDataSource = new kendo.data.DataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                //$scope.getStructureValues();
                if ($rootScope.familytableStructurechanges != 'undefined' && $rootScope.familytableStructurechanges != undefined && $rootScope.familytableStructurechanges != "") {
                    $scope.familyTableStructure = $rootScope.familytableStructurechanges;
                }
                dataFactory.tableGroupSummaryDataSource($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.familyTableStructure, $scope.Flag, $scope.multiplegroupid, $scope.SelectedProductPackId, $scope.StructureName).success(function (response) {
                    options.success(response);
                    //$scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                }).error(function (error) {
                    options.error(error);
                });

            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_ID"
            }
        }
    });

    $rootScope.tableGroupHeaderDataSource = new kendo.data.DataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                //$scope.getStructureValues();
                if ($rootScope.familytableStructurechanges != 'undefined' && $rootScope.familytableStructurechanges != undefined && $rootScope.familytableStructurechanges != "") {
                    $scope.familyTableStructure = $rootScope.familytableStructurechanges;
                }
                dataFactory.GettableGroupHeaderDataSource($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.familyTableStructure, $scope.Flag, $scope.multiplegroupid, $scope.SelectedProductPackId, $scope.StructureName).success(function (response) {
                    options.success(response);
                    //$scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                }).error(function (error) {
                    options.error(error);
                });

            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_ID"
            }
        }
    });


    $scope.StructureNameDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {

                var curFamilyId = $scope.Family.FAMILY_ID;
                if (curFamilyId == undefined) {
                    curFamilyId = $rootScope.currentFamilyId;
                }
                //if (curFamilyId.includes('~')) {
                //    var Family_ID = curFamilyId.split('~');
                //    curFamilyId = Family_ID[1];
                //}
                dataFactory.GetStructureNameDataSourceDetails(curFamilyId, $rootScope.selecetedCatalogId, $scope.Flag, $scope.multiplegroupid).success(function (response) {
             
                    if (response.length > 0) {
                        $scope.StructureNameinitial = response[0].STRUCTURE_NAME;
                        $scope.familyTableStructure = response[0].FAMILY_TABLE_STRUCTURE;
                        for (var i = 0; i < response.length; i++) {
                            if (response[i].IS_DEFAULT) {
                                $scope.StructureName = response[i].STRUCTURE_NAME;
                                $scope.StructureNameinitial = response[i].STRUCTURE_NAME;
                                $scope.familyTableStructure = response[i].FAMILY_TABLE_STRUCTURE;

                                $rootScope.leftTreeNewDataSource.read();
                                //$rootScope.rightTreeNewDataSource.read();
                                //$rootScope.summaryTreeNewDataSource.read();
                                //$rootScope.columnTreeNewDataSource.read();
                                //$rootScope.allAttributeListDatasource.read();
                                //$rootScope.tableGroupSummaryDataSource.read();
                                //$rootScope.tableGroupHeaderDataSource.read();
                            }
                        }

                    }
                    $scope.getSelectedStructureLoad();
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.StructureNameChange = function (e) {
        $scope.StructureName = e.sender.value();
        if ($scope.StructureName !== "") {
            dataFactory.GetTableStructure($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId, $scope.StructureName, $scope.Flag, $scope.multiplegroupid).success(function (response) {
                debugger
                if (response != null) {
                    $scope.familyTableStructure = response[0].FAMILY_TABLE_STRUCTURE;
                    if ($scope.StructureName != "" && $scope.familyTableStructure != "") {
                        $rootScope.familytableStructurechanges = $scope.familyTableStructure;
                        $rootScope.leftTreeNewDataSource.read();
                        $rootScope.rightTreeNewDataSource.read();
                        $rootScope.summaryTreeNewDataSource.read();
                        $rootScope.columnTreeNewDataSource.read();
                        $rootScope.allAttributeListDatasource.read();
                        $rootScope.tableGroupSummaryDataSource.read();
                        $rootScope.tableGroupHeaderDataSource.read();
                        $scope.getSelectedStructureLoad();
                        var result = $scope.familyTableStructure.match(/<VerticalTable>(.*?)<\/VerticalTable>/g).map(function (val) {
                            return val.replace(/<\/?VerticalTable>/g, '');
                        });
                        if (result == 'True' || result == 'true') {
                            $scope.VerticalTable_ChkBox.value1 = true;
                        }
                        else {
                            $scope.VerticalTable_ChkBox.value1 = false;
                        }

                    }

                }
            }).error(function (error) {

                options.error(error);
            });
        }
        else {
            $scope.StructureName = $scope.StructureNameinitial;
        }
        $rootScope.ProductGroupNameForFamily.read();
        $scope.SelectedProductPackId = 0;
    };
    $scope.getSelectedStructureLoad = function () {
       
        if ($scope.StructureNameinitial != "" && $scope.familyTableStructure != "") {
            var result = $scope.familyTableStructure.match(/<VerticalTable>(.*?)<\/VerticalTable>/g).map(function (val) {
                return val.replace(/<\/?VerticalTable>/g, '');
            });
            if (result == 'True' || result == 'true') {
                $scope.VerticalTable_ChkBox.value1 = true;
            }
            else {
                $scope.VerticalTable_ChkBox.value1 = false;
            }
           // alert($scope.StructureName);
            dataFactory.DecodeLayoutXmls($scope.StructureName, $scope.familyTableStructure, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.Flag, $scope.multiplegroupid, $scope.SelectedProductPackId).success(function (response1) {
                $scope.tableDesignerFields.ComboSummaryGroup = response1.SummaryGroupFieldValue;
                $scope.tableDesignerFields.ComboGroupByColumn = response1.TableGroupFieldValue;
                $scope.tableDesignerFields.EmptyCellTextBox = response1.PlaceHolderText;
                $scope.DispalyRowHeaderCheckEditor.value1 = response1.DisplayRowHeader;
                $scope.DisplayColumnHeaderCheckBox.value1 = response1.DisplayColumnHeader;
                $scope.DispalySummaryHeaderCheckEditor.value1 = response1.DisplaySummaryHeader;
                $scope.tableDesignerFields.PivotHeaderTextBox = response1.PivotHeaderText;
                $scope.VerticalTable_ChkBox.value1 = response1.VerticalTable;
                //$scope.StructureNameDataSource.datasource.read();
                // $("#dfsdf").data("kendoDropDownList").dataSource.read();
               // $scope.StructureName = $scope.StructureNameinitial;
            }).error(function (error) {

                options.error(error);
            });
        }

    };
  

    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        } else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
        }
      //  $scope.getSelectedStructureLoad();
      //  $scope.StructureNameDataSource.read();
    };
    // function that gathers IDs of checked nodes
    function checkedNodeIds(nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].id);
            }
            if (nodes[i].hasChildren) {
                checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    }

    $scope.onDropall = function (e) {
       
        var data = $('#treeviewallAttributeList').data('kendoTreeView').dataItem(e.sourceNode);
        var allData = $('#treeviewallAttributeList').data('kendoTreeView').dataItem(e.destinationNode);
        var leftData = $('#leftTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var columnData = $('#columnNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var rightData = $('#rightNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var summaryData = $('#summaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var leftnav;
        if($('#leftNavTreeViewKendoNavigator').data('kendoTreeView')!=undefined)
        {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        }
        else
        {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView');
        }
        var lefttree = true;
        if (e.dropTarget.attributes.length >= 4) {
            if (e.dropTarget.attributes[3].nodeName === "ng-dblclick" && e.dropTarget.attributes[4].nodeName === "ng-right-click") {
                lefttree = false;
            }
            if (e.dropTarget.attributes[1].nodeName === "ng-true-value" && e.dropTarget.attributes[2].nodeName === "ng-false-value") {
                lefttree = false;
            }
        }
        if (lefttree) {
            if (e.destinationNode !== undefined && leftnav === undefined && e.destinationNode.id !== "leftNavTreeViewKendoNavigator") {
                var valid = true;
                // if (e.destinationNode.id.contains("treeviewallAttributeList") || e.destinationNode.id === "leftTreeViewKendo" || e.destinationNode.id === "columnNavTreeViewKendo" || e.destinationNode.id === "rightNavTreeViewKendo" || e.destinationNode.id === "summaryNavTreeViewKendo") {
                if (allData != undefined) {
                    if (!allData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                    if (!allData.hasChildren && e.dropPosition == "over" && allData.ATTRIBUTE_ID == -1) {
                        e.setValid(true);
                        valid = false;
                    }
                }
                if (leftData != undefined) {
                    if (!leftData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (columnData != undefined) {
                    if (!columnData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (rightData != undefined) {
                    if (!rightData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (summaryData != undefined) {
                    if (!summaryData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "columnNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "columnNavTreeViewKendo") {
                        e.setValid(false);
                     
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "leftTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "rightNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "summaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "summaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                      
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });
                    }
                }
                // $scope.removeUnPublishAttr();

                if (valid) {
                    try {
                        var products = $rootScope.tableGroupHeaderDataSource.data();
                        // var node1 = products.get(data.ATTRIBUTE_ID);
                        var fulldataitemlength = products.length;
                        for (var i = 0; i < fulldataitemlength; i++) {
                            //var nodes = products.get(data.items[i].ATTRIBUTE_ID);
                            if (products[i].ATTRIBUTE_ID === data.ATTRIBUTE_ID) {
                                $rootScope.tableGroupHeaderDataSource.remove(products[i]);
                            }
                            if (data.hasChildren) {
                                var dataitemlength = data.items.length;
                                for (var j = 0; j < dataitemlength; j++) {
                                    //var nodes = products.get(data.items[j].ATTRIBUTE_ID);
                                    if (products[i].ATTRIBUTE_ID === data.items[j].ATTRIBUTE_ID) {
                                        $rootScope.tableGroupHeaderDataSource.remove(products[i]);
                                    }
                                }
                            }
                        }

                    } catch (e) {

                    }
                }
                if (e.destinationNode.id === "summaryNavTreeViewKendo") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var summaryAttributeListview = $("#summaryAttributeListview").data("kendoDropDownList");
                                summaryAttributeListview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));
                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var summaryAttributeListview1 = $("#summaryAttributeListview").data("kendoDropDownList");
                                        summaryAttributeListview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));
                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }

                //$scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                //} else {
                //    e.setValid(false);
                //}
            } else {
                e.setValid(false);
            }
        } else {
            e.setValid(false);
        }
    };
    $scope.onDropLeft = function (e) {
       
        var data = $('#leftTreeViewKendo').data('kendoTreeView').dataItem(e.sourceNode);
        var leftData = $('#leftTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var allData = $('#treeviewallAttributeList').data('kendoTreeView').dataItem(e.destinationNode);
        var columnData = $('#columnNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var rightData = $('#rightNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var summaryData = $('#summaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        
        //var leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        var leftnav;
        if ($('#leftNavTreeViewKendoNavigator').data('kendoTreeView') != undefined) {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        }
        else {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView');
        }
        var lefttree = true;
        if (e.dropTarget.attributes.length >= 4) {
            if (e.dropTarget.attributes[3].nodeName === "ng-dblclick" && e.dropTarget.attributes[4].nodeName === "ng-right-click") {
                lefttree = false;
            } if (e.dropTarget.attributes[1].nodeName === "ng-true-value" && e.dropTarget.attributes[2].nodeName === "ng-false-value") {
                lefttree = false;
            }
        }
        if (lefttree) {
            if (e.destinationNode !== undefined && leftnav === undefined && e.destinationNode.id !== "leftNavTreeViewKendoNavigator") {
                //if (e.destinationNode.id === "treeviewallAttributeList" || e.destinationNode.id === "leftTreeViewKendo" || e.destinationNode.id === "columnNavTreeViewKendo" || e.destinationNode.id === "rightNavTreeViewKendo" || e.destinationNode.id === "summaryNavTreeViewKendo") {
                var valid = true;
                if (allData != undefined) {
                    if (!allData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (leftData != undefined) {
                    if (!leftData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false); valid = false;
                    }
                }
                if (columnData != undefined) {
                    if (!columnData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (rightData != undefined) {
                    if (!rightData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (summaryData != undefined) {
                    if (!summaryData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "columnNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "columnNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                      
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "leftTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "rightNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "summaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "summaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                      
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });
                    }
                }


                // $scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                if (e.destinationNode.id === "treeviewallAttributeList") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var allAttributeListtreeview = $("#allAttributeListview").data("kendoDropDownList");
                                allAttributeListtreeview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));

                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var allAttributeListtreeview1 = $("#allAttributeListview").data("kendoDropDownList");
                                        allAttributeListtreeview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));

                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }
                if (e.destinationNode.id === "summaryNavTreeViewKendo") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                //  $rootScope.tableGroupSummaryDataSource.add(data);
                                var summaryAttributeListview = $("#summaryAttributeListview").data("kendoDropDownList");
                                summaryAttributeListview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));
                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var summaryAttributeListview1 = $("#summaryAttributeListview").data("kendoDropDownList");
                                        summaryAttributeListview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));
                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }

                //} else {
                //    e.setValid(false);
                //}
            } else {
                e.setValid(false);
            }
        } else {
            e.setValid(false);
        }
        // $scope.removeUnPublishAttr();
    };
    $scope.onDropColumn = function (e) {
       
        var data = $('#columnNavTreeViewKendo').data('kendoTreeView').dataItem(e.sourceNode);
        var summaryData = $('#summaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var leftData = $('#leftTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var columnData = $('#columnNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var rightData = $('#rightNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var allData = $('#treeviewallAttributeList').data('kendoTreeView').dataItem(e.destinationNode);
        //var leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        var leftnav;
        if ($('#leftNavTreeViewKendoNavigator').data('kendoTreeView') != undefined) {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        }
        else {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView');
        }
        var lefttree = true;
        if (e.dropTarget.attributes.length >= 4) {
            if (e.dropTarget.attributes[3].nodeName === "ng-dblclick" && e.dropTarget.attributes[4].nodeName === "ng-right-click") {
                lefttree = false;
            } if (e.dropTarget.attributes[1].nodeName === "ng-true-value" && e.dropTarget.attributes[2].nodeName === "ng-false-value") {
                lefttree = false;
            }
        }
        if (lefttree) {
            if (e.destinationNode !== undefined && leftnav === undefined && e.destinationNode.id !== "leftNavTreeViewKendoNavigator") {
                var valid = true;
                if (allData != undefined) {
                    if (!allData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (leftData != undefined) {
                    if (!leftData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (columnData != undefined) {
                    if (!columnData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (rightData != undefined) {
                    if (!rightData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (summaryData != undefined) {
                    if (!summaryData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                } //$scope.removeUnPublishAttr();
                if (e.destinationNode.id === "treeviewallAttributeList") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var allAttributeListtreeview = $("#allAttributeListview").data("kendoDropDownList");
                                allAttributeListtreeview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));

                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var allAttributeListtreeview1 = $("#allAttributeListview").data("kendoDropDownList");
                                        allAttributeListtreeview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));

                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }
                if (e.destinationNode.id === "summaryNavTreeViewKendo") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var summaryAttributeListview = $("#summaryAttributeListview").data("kendoDropDownList");
                                summaryAttributeListview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));
                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var summaryAttributeListview1 = $("#summaryAttributeListview").data("kendoDropDownList");
                                        summaryAttributeListview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));
                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }
                // $scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                //} else {
                //    e.setValid(false);
                //}
            } else {
                e.setValid(false);
            }
        } else {
            e.setValid(false);
        }
    };
    $scope.onDropSummary = function (e) {
       
        var data = $('#summaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.sourceNode);
        var summaryData = $('#summaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var leftData = $('#leftTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var columnData = $('#columnNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var rightData = $('#rightNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var allData = $('#treeviewallAttributeList').data('kendoTreeView').dataItem(e.destinationNode);
        //var leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        var leftnav;
        if ($('#leftNavTreeViewKendoNavigator').data('kendoTreeView') != undefined) {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        }
        else {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView');
        }
        var lefttree = true;
        if (e.dropTarget.attributes.length >= 4) {
            if (e.dropTarget.attributes[3].nodeName === "ng-dblclick" && e.dropTarget.attributes[4].nodeName === "ng-right-click") {
                lefttree = false;
            } if (e.dropTarget.attributes[1].nodeName === "ng-true-value" && e.dropTarget.attributes[2].nodeName === "ng-false-value") {
                lefttree = false;
            }
        }
        if (lefttree) {
            if (e.destinationNode !== undefined && leftnav === undefined && e.destinationNode.id !== "leftNavTreeViewKendoNavigator") {
                var valid = true;
                if (allData != undefined) {
                    if (!allData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (leftData != undefined) {
                    if (!leftData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (columnData != undefined) {
                    if (!columnData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (rightData != undefined) {
                    if (!rightData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (summaryData != undefined) {
                    if (!summaryData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                // $scope.removeUnPublishAttr();
                if (e.destinationNode.id === "treeviewallAttributeList") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var allAttributeListtreeview = $("#allAttributeListview").data("kendoDropDownList");
                                allAttributeListtreeview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));

                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var allAttributeListtreeview1 = $("#allAttributeListview").data("kendoDropDownList");
                                        allAttributeListtreeview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));

                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }
                if (valid) {
                    try {
                        var products = $rootScope.tableGroupSummaryDataSource.data();
                        // var node1 = products.get(data.ATTRIBUTE_ID);
                        var fulldataitemlength = products.length;
                        for (var i = 0; i < fulldataitemlength; i++) {
                            //var nodes = products.get(data.items[i].ATTRIBUTE_ID);
                            if (products[i].ATTRIBUTE_ID === data.ATTRIBUTE_ID) {
                                $rootScope.tableGroupSummaryDataSource.remove(products[i]);
                            }
                            if (data.hasChildren) {
                                var dataitemlength1 = data.items.length;
                                for (var j1 = 0; j1 < dataitemlength1; j1++) {
                                    //var nodes = products.get(data.items[j].ATTRIBUTE_ID);
                                    if (products[i].ATTRIBUTE_ID === data.items[j1].ATTRIBUTE_ID) {
                                        $rootScope.tableGroupSummaryDataSource.remove(products[i]);
                                    }
                                }
                            }
                        }
                    } catch (e) {

                    }
                }
                //  $scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                //} else {
                //    e.setValid(false);
                //}
            } else {
                e.setValid(false);
            }
        } else {
            e.setValid(false);
        }
    };
    $scope.onDropRight = function (e) {
   
        var data = $('#rightNavTreeViewKendo').data('kendoTreeView').dataItem(e.sourceNode);
        var rightData = $('#rightNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var allData = $('#treeviewallAttributeList').data('kendoTreeView').dataItem(e.destinationNode);
        var columnData = $('#columnNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var leftData = $('#leftTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var summaryData = $('#summaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        //var leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        var leftnav;
        if ($('#leftNavTreeViewKendoNavigator').data('kendoTreeView') != undefined) {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        }
        else {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView');
        }
        var lefttree = true;
        if (e.dropTarget.attributes.length >= 4) {
            if (e.dropTarget.attributes[3].nodeName === "ng-dblclick" && e.dropTarget.attributes[4].nodeName === "ng-right-click") {
                lefttree = false;
            } if (e.dropTarget.attributes[1].nodeName === "ng-true-value" && e.dropTarget.attributes[2].nodeName === "ng-false-value") {
                lefttree = false;
            }
        }
        if (lefttree) {
            if (e.destinationNode !== undefined && leftnav === undefined && e.destinationNode.id !== "leftNavTreeViewKendoNavigator") {
                var valid = true;
                if (rightData != undefined) {
                    if (!rightData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (leftData != undefined) {
                    if (!leftData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (columnData != undefined) {
                    if (!columnData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (allData != undefined) {
                    if (!allData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (summaryData != undefined) {
                    if (!summaryData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "columnNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "columnNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                       
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "leftTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "rightNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "summaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "summaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                       
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });
                    }
                }

                if (e.destinationNode.id === "treeviewallAttributeList") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var allAttributeListtreeview = $("#allAttributeListview").data("kendoDropDownList");
                                allAttributeListtreeview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));

                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var allAttributeListtreeview1 = $("#allAttributeListview").data("kendoDropDownList");
                                        allAttributeListtreeview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));

                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }

                if (e.destinationNode.id === "summaryNavTreeViewKendo") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var summaryAttributeListview = $("#summaryAttributeListview").data("kendoDropDownList");
                                summaryAttributeListview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));
                                //$rootScope.tableGroupSummaryDataSource.add(data);
                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var summaryAttributeListview1 = $("#summaryAttributeListview").data("kendoDropDownList");
                                        summaryAttributeListview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));
                                    }
                                    // value.SORT = parseFloat(value.SORT);
                                });
                            }
                        } catch (e) {

                        }
                    }
                }

            } else {
                e.setValid(false);
            }
        } else {
            e.setValid(false);
        }
    };

    $scope.showTablePreview = function () {
        $rootScope.familytableStructurechanges = "";
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "It may take some time to process.",
                type: "confirm",
                buttons: [{ value: "Ok" },
                    { value: "Cancel" }
                ],
                success: function (result) {
                    if (result === "Ok") {
                       
        if ($rootScope.columnTreeNewDataSource._data.length > 0) {
            if ($rootScope.summaryTreeNewDataSource._data.length > 0) {
                var new_Flag = $scope.Flag + '@' + "Preview";
                dataFactory.generateTableHtml($rootScope.leftTreeNewDataSource._data, $rootScope.rightTreeNewDataSource._data, $rootScope.columnTreeNewDataSource._data, $rootScope.summaryTreeNewDataSource._data, $scope.tableDesignerFields.ComboSummaryGroup, $scope.tableDesignerFields.ComboGroupByColumn, $scope.tableDesignerFields.EmptyCellTextBox, $scope.DispalyRowHeaderCheckEditor.value1, $scope.DisplayColumnHeaderCheckBox.value1, $scope.DispalySummaryHeaderCheckEditor.value1, $scope.VerticalTable_ChkBox.value1, $scope.tableDesignerFields.PivotHeaderTextBox, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.StructureName, $localStorage.CategoryID, new_Flag, $scope.multiplegroupid, $scope.SelectedProductPackId).success(function (response) {
                    $scope.winTableDesignerPreview.refresh({ url: "../Views/App/Partials/SuperColumnPreview.html" });
                    $scope.winTableDesignerPreview.center().open();
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {
              
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'To construct Pivot Table both the Column Attribute and Summary Attribute fields needs to be populated.',
                    //type: "error"
                });

            }
        } else if ($rootScope.columnTreeNewDataSource._data.length === 0 && $rootScope.summaryTreeNewDataSource._data.length > 0) {      
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'To construct Pivot Table both the Column Attribute and Summary Attribute fields needs to be populated.',
                //type: "error"
            });
        }

        else {
            var new_Flag = $scope.Flag + '@' + "Preview";
            dataFactory.generateTableHtml($rootScope.leftTreeNewDataSource._data, $rootScope.rightTreeNewDataSource._data, $rootScope.columnTreeNewDataSource._data, $rootScope.summaryTreeNewDataSource._data, $scope.tableDesignerFields.ComboSummaryGroup, $scope.tableDesignerFields.ComboGroupByColumn, $scope.tableDesignerFields.EmptyCellTextBox, $scope.DispalyRowHeaderCheckEditor.value1, $scope.DisplayColumnHeaderCheckBox.value1, $scope.DispalySummaryHeaderCheckEditor.value1, $scope.VerticalTable_ChkBox.value1, $scope.tableDesignerFields.PivotHeaderTextBox, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.StructureName, $localStorage.CategoryID, new_Flag, $scope.multiplegroupid, $scope.SelectedProductPackId).success(function (response) {
                $scope.winTableDesignerPreview.refresh({ url: "../Views/App/Partials/SuperColumnPreview.html" });
                $scope.winTableDesignerPreview.center().open();
            }).error(function (error) {
                options.error(error);
            });
        }

                    }
                    else {

                        return false;
                    }
                }

            });

       


    };

    $scope.showTableSave = function () {


            //    $.msgBox({
            //    title: "Select an Action",
            //    content: "Family export may take some time to process.",
            //    type: "confirm",
            //    buttons: [{ value: "Ok" }
            //       // { value: "Cancel" }
            //    ],
            //    success: function (result) {
            //        //if (result === "Ok") {

            //        //}
                  
            //    }

            //});
        $rootScope.familytableStructurechanges = "";
          
        if ($rootScope.columnTreeNewDataSource._data.length > 0) {
            if ($rootScope.summaryTreeNewDataSource._data.length > 0) {
                dataFactory.generateTableHtml($rootScope.leftTreeNewDataSource._data, $rootScope.rightTreeNewDataSource._data, $rootScope.columnTreeNewDataSource._data, $rootScope.summaryTreeNewDataSource._data, $scope.tableDesignerFields.ComboSummaryGroup, $scope.tableDesignerFields.ComboGroupByColumn, $scope.tableDesignerFields.EmptyCellTextBox, $scope.DispalyRowHeaderCheckEditor.value1, $scope.DisplayColumnHeaderCheckBox.value1, $scope.DispalySummaryHeaderCheckEditor.value1, $scope.VerticalTable_ChkBox.value1, $scope.tableDesignerFields.PivotHeaderTextBox, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.StructureName, $localStorage.CategoryID, $scope.Flag, $scope.multiplegroupid, $scope.SelectedProductPackId).success(function (response) {
                    if (response != null) {                     
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Saved successfully.',
                            type: "info"
                        });
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {          
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'To construct Pivot Table both the Column Attribute and Summary Attribute fields needs to be populated.',
                    //type: "error"
                });
            }
        } else if ($rootScope.columnTreeNewDataSource._data.length === 0 && $rootScope.summaryTreeNewDataSource._data.length > 0) {
           
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'To construct Pivot Table both the Column Attribute and Summary Attribute fields needs to be populated.',
                //type: "error"
            });
        }

        else {
            dataFactory.generateTableHtml($rootScope.leftTreeNewDataSource._data, $rootScope.rightTreeNewDataSource._data, $rootScope.columnTreeNewDataSource._data, $rootScope.summaryTreeNewDataSource._data, $scope.tableDesignerFields.ComboSummaryGroup, $scope.tableDesignerFields.ComboGroupByColumn, $scope.tableDesignerFields.EmptyCellTextBox, $scope.DispalyRowHeaderCheckEditor.value1, $scope.DisplayColumnHeaderCheckBox.value1, $scope.DispalySummaryHeaderCheckEditor.value1, $scope.VerticalTable_ChkBox.value1, $scope.tableDesignerFields.PivotHeaderTextBox, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.StructureName, $localStorage.CategoryID, $scope.Flag, $scope.multiplegroupid, $scope.SelectedProductPackId).success(function (response) {
                if (response != null) {
                   
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Saved successfully.',
                        type: "info"
                    });
                }
            }).error(function (error) {
                options.error(error);
            });
        }



    };

    $scope.addSuperColumn = function () {
        var flagLeft = false;
        var flagRight = false;
        var flagSummary = false;
        var flagColumn = false;

        var allAttributeListtreeview = $("#treeviewallAttributeList").data("kendoTreeView");
        var selectedNode = allAttributeListtreeview.select();
        if (selectedNode.length == 0 || selectedNode.length == 1) {
            selectedNode = null;
        }
        var lefttreeView = $("#leftTreeViewKendo").data("kendoTreeView");
        var lefttreelength = lefttreeView.dataSource._data.length;
        for (var i = 0; i < lefttreelength; i++) {
            if ($("#txtSuperColumn").val().toLowerCase() == lefttreeView.dataSource._data[i].ATTRIBUTE_NAME.toLowerCase()) {
                flagLeft = true;
            }
        }

        var righttreeView = $("#rightNavTreeViewKendo").data("kendoTreeView");
        var righttreelength = righttreeView.dataSource._data.length;
        for (var i = 0; i < righttreelength; i++) {
            if ($("#txtSuperColumn").val().toLowerCase() == righttreeView.dataSource._data[i].ATTRIBUTE_NAME.toLowerCase()) {
                flagRight = true;
            }
        }

        var summarytreeView = $("#summaryNavTreeViewKendo").data("kendoTreeView");
        var Summarytreelength = summarytreeView.dataSource._data.length;
        for (var i = 0; i < Summarytreelength; i++) {
            if ($("#txtSuperColumn").val().toLowerCase() == summarytreeView.dataSource._data[i].ATTRIBUTE_NAME.toLowerCase()) {
                flagSummary = true;
            }
        }
        var columntreeView = $("#columnNavTreeViewKendo").data("kendoTreeView");
        var columntreelength = columntreeView.dataSource._data.length;
        for (var i = 0; i < columntreelength; i++) {
            if ($("#txtSuperColumn").val().toLowerCase() == columntreeView.dataSource._data[i].ATTRIBUTE_NAME.toLowerCase()) {
                flagColumn = true;
            }
        }
        if ($("#txtSuperColumn").val().length > 0) {
            var checkExistingAttrInAll = allAttributeListtreeview.findByText($("#txtSuperColumn").val());
            if (checkExistingAttrInAll.length == 0 && flagLeft == false && flagRight == false && flagSummary == false && flagColumn == false) {
                var datasourcedata = $rootScope.allAttributeListDatasource.data();
                var exists = false;
                for (var i1 = 0; i1 < datasourcedata.length; i1++) {
                    var dataitem = datasourcedata[i1].ATTRIBUTE_NAME;
                    if (dataitem.toLowerCase() === $("#txtSuperColumn").val().toLowerCase()) {
                        exists = true;
                        
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Attribute already exists.',
                            type: "error"
                        });
                        $("#txtSuperColumn").val("");
                    }
                }
                if (!exists) {
                    allAttributeListtreeview.append(new kendo.data.Node({ 'ATTRIBUTE_NAME': $("#txtSuperColumn").val(), 'ATTRIBUTE_ID': '-1', 'COLOR': 'BLACK', 'id': 'Super', hasChildren: false }), selectedNode);
                    $("#txtSuperColumn").val('');
                }
            }
            else {
               
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Attribute already exists.',
                    type: "error"
                });
                $("#txtSuperColumn").val('');
            }
        }
        else {

         
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Enter required Value.',
                type: "error"
            });
        }






    };

    $scope.tableGroupHeaderChange = function (e) {
        $scope.tableDesignerFields.ComboGroupByColumn = e.sender._old;
    };

    $scope.tableGroupSummaryChange = function (e) {
        $scope.tableDesignerFields.ComboSummaryGroup = e.sender._old;
    };
    
    //$scope.CreateStructure = function () {
    //    $scope.winCreateNewLayout.close();
    //    $scope.rendertabledesigner = false;
    //    dataFactory.CreateStructure($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.Structure.defaultlayout, $scope.Structure.structureName, $scope.Structure.tabletype).success(function (response) {
           
    //        if (response != null && response !== "Already Exists") {
    //            $scope.rendertabledesigner = true;
    //            $scope.tableGroupHeaderValue = true;
    //            $("#divFmlyTab").hide();
    //            $("#familyPreview").hide();
    //            $scope.$broadcast("TABLEDESIGNER");
    //            $("#tabledesigner").show();
    //            $("#EditMultipletable").hide();
    //        } else {
    //            alert("Table Layout already exists. Create a different Layout");
    //        }
    //    }).error(function (error) {
    //        options.error(error);
    //    });
    //};
    //$scope.CancelStructure = function () {
    //    $scope.winCreateNewLayout.close();
    //};



    

    $scope.DeleteStructure = function ()
    {



        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure want to delete the Family Layout?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {

                if (result === "Yes") {

                    $scope.winDeleteLayout.close();
                    if ($scope.StructureName !== "") {
                        dataFactory.DeleteStructure($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId, $scope.StructureName).success(function (response) {
                            if (response != null) {

                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                            }
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
                else {

                    return;
                }


            }
        });


    };

    $scope.DelCancelStructure = function () {
        $scope.winDeleteLayout.close();
    };
    $scope.setCancelStructure = function () {
        $scope.winDefaultLayout.close();

    };
    $scope.DefaultStructure = function () {

        if ($scope.StructureName != '') {
            $scope.winDefaultLayout.close();
            dataFactory.setDefaultLayout($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId, $scope.StructureName).success(function (response) {
                if (response != null) {
                 
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                }
            }).error(function (error) {
                options.error(error);
            });
        }
    };

    $scope.ProductPackNameChange = function (e) {
        $scope.SelectedProductPackId = e.sender.value();
        if ($scope.SelectedProductPackId == "" || $scope.SelectedProductPackId == '') {
            $scope.StructureNameDataSource.read();
        }

        $timeout(function () {
            $rootScope.leftTreeNewDataSource.read();
            $rootScope.rightTreeNewDataSource.read();
            $rootScope.summaryTreeNewDataSource.read();
            $rootScope.columnTreeNewDataSource.read();
            $rootScope.allAttributeListDatasource.read();
            $rootScope.tableGroupSummaryDataSource.read();
            $rootScope.tableGroupHeaderDataSource.read();
            $scope.getSelectedStructureLoad();
        }, 500);
    };

    $scope.$on("TABLEDESIGNER", function (event) {
        // alert("1");
        $scope.StructureNameDataSource.read();
      //  $scope.getSelectedStructureLoad();
       
    });

    //$scope.StructureNameDataSource.read();


    ///////////////////////////////////// Start - Multiple table designer select families - Aswin kumar /////////////////////////////////////////////// 

    $scope.multipleTabledesignerClick = function () {
        $scope.MultipletabledesignerCreation.center().open();
        $scope.IsDefaultStructureName = false;

        $scope.treeData = new kendo.data.HierarchicalDataSource({

            type: "json",
            loadOnDemand: true,
            transport: {
                read: function (options) {

                    dataFactory.getTabledesignerAllCategories($scope.SelectedCatalogId, options.data.id, $scope.SelectedCatalogId, $scope.Templatename).success(function (response) {

                        options.success(response);
                        var treeview = $("#treeviewKendo1").data("kendoTreeView");
                        $('#treeviewKendo1 ul li').each(function (i) {
                            var dataItem = treeview.dataItem(this);
                            if (dataItem.checked && dataItem.check) {
                                //$(this).find(':nth-child(2)').addClass("first");
                                $(this).find('.k-checkbox-wrapper').addClass("first");
                            }
                        });

                    }).error(function (response) {
                        options.success(response);
                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    hasChildren: "hasChildren"
                }
            },

        });

        $scope.treeDataoptions = {
            checkboxes: {
                checkChildren: true,
            }, check: onCheck,
            dataSource: $scope.treeData,
        };
    }


    // function that gathers IDs of checked nodes
    function checkedNodeIds(nodes, checkedNodes) {

        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                if (nodes[i].CATEGORY_ID.contains("~")) {
                    checkedNodes.push(nodes[i].id + "!");
                    // checkedNodes.push(nodes[i].parent().parent().CATEGORY_ID + "!" + nodes[i].CATEGORY_ID);
                } else if (nodes[i].CATEGORY_ID && !nodes[i].check) {
                    checkedNodes.push(nodes[i].CATEGORY_ID);
                }
                else {
                    checkedNodes.push(nodes[i].id);
                }
            }

            if (nodes[i].hasChildren) {
                checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    }

    var checkedNodes = [], message;
    // show checked node IDs on datasource change
    function onCheck() {

        //  var currentId = $(this).attr('id');
        // currentId = $(this).find('.k-item ng-binding').attr('id');

        // alert(currentId);
        checkedNodes = [];
        checkedNodeIds($scope.treeData._data, checkedNodes);

        if (checkedNodes.length > 0) {
            message = "IDs of checked nodes: " + checkedNodes.join(",");


            $scope.checkedNodes = checkedNodes;

            if ($scope.checkedNodes.length > 0) {
                $scope.disableApplyBtn = false;
                $("#user-toolbar-association-apply-family").removeAttr('disabled');
                $scope.disableClearBtn = false;
                $("#user-toolbar-association-clear-family").removeAttr('disabled');

            }

        } else {
            message = "No nodes checked.";
            $scope.disableApplyBtn = true;
            $("#user-toolbar-association-apply-family").prop('disabled', true);
            $scope.disableClearBtn = true;
            $("#user-toolbar-association-clear-family").prop('disabled', true);
        }
        //alert(message);
        //$("#result").html(message);
    }

    $scope.onClose = function (e) {
        $scope.treeData.read();
        $scope.IsDefaultStructureName = false;
        $scope.IsUpdateMasterCatalog = false;
    }



    $scope.multipleTabledesignerApplyClick = function () {


        $scope.currentFamilyId = $scope.Family.FAMILY_ID;
        $scope.getStructureName = $scope.StructureName;
        $scope.getCurrentCatalogId = $scope.SelectedCatalogId;
        $scope.getExistingCategoryFamilyID = $scope.checkedNodes;
        $scope.array = $scope.getExistingCategoryFamilyID;
        $scope.getIsdefaultStructurename = $scope.IsDefaultStructureName;
        $scope.IsUpdateMasterCatalog = $scope.IsUpdateMasterCatalog;

        if ($scope.array.length > 0) {

            dataFactory.multipleTableDesignerSelectionFamilyIds($scope.currentFamilyId, $scope.getStructureName, $scope.getCurrentCatalogId, $scope.getIsdefaultStructurename, $scope.IsUpdateMasterCatalog, $scope.array).success(function (response) {

                if (response == "1") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Table designer applied to the selected families succesfully.",
                        type: "info"
                    });
                    $scope.treeData.read();
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Table designer applied to the selected families failed.",
                        type: "info"
                    });
                }

                $scope.treeData.read();


            }).error(function (error) {
                options.error(error);
            });
            $scope.IsDefaultStructureName = false;
            $scope.IsUpdateMasterCatalog = false;
            $scope.disableApplyBtn = true;
            $("#user-toolbar-association-apply-family").prop('disabled', true);
            $scope.disableClearBtn = true;
            $("#user-toolbar-association-clear-family").prop('disabled', true);
        }

    }

    $scope.multipleTabledesignerClearClick = function () {
        $("#treeviewKendo1 .k-checkbox-wrapper input").prop("checked", false).trigger("change");
        $scope.IsDefaultStructureName = false;
        $scope.IsUpdateMasterCatalog = false;
    }

    $scope.multipleTabledesignerCancelClick = function () {
        $scope.MultipletabledesignerCreation.center().close();
        $("#treeviewKendo1 .k-checkbox-wrapper input").prop("checked", false).trigger("change");
        $scope.IsDefaultStructureName = false;
        $scope.IsUpdateMasterCatalog = false;
    }




    ///////////////////////////////////// End - Multiple table designer select families - Aswin kumar /////////////////////////////////////////////// 

}]);

