﻿LSApp.controller('familyProductController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'NgTableParams', '$filter', '$rootScope', '$localStorage', 'blockUI',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, NgTableParams, $filter, $rootScope, $localStorage, blockUI) {
       
        $rootScope.LoadFamilyData = function (catalogId, id, cat_id) {
                dataFactory.getFamilyspecs(catalogId, FamilyId, cat_id).success(function (response) {
                    var obj = jQuery.parseJSON(response.Data.Data);
                    $scope.prodData = obj;
            });

            blockUI.stop();
        };
        $scope.Init = function () {
            $scope.FAMILY_ID = $scope.Family.FAMILY_ID;
        }
    }]);