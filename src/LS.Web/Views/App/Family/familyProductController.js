﻿LSApp.controller('familyProductController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'NgTableParams', '$filter', '$rootScope', '$localStorage', 'blockUI',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, NgTableParams, $filter, $rootScope, $localStorage, blockUI) {
        $scope.ProdEditPageCount = 10;
        $('#importshow').hide();
        $rootScope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
        menuDisplayed = true;
        $('#selectedattributenoresultfound').hide();
        $scope.SelectedFileForProduct = '';
        $scope.SelectedFileForProductTooltip = 'No file chosen';
        $rootScope.undoProductFlag = "0";
        $scope.MenuClick == false;
        $scope.val_type = "true";
        $("#custom-menuEdit").hide();
        $("#custom-menusubEdit").hide();
        var _this = this;
        this.isWorks = true;
        this.x = 0;
        this.y = 0;
        this.status = 'it works!';
        this.fields = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'];
        this.stuff = mockData();
        $scope.partno = "";
        $scope.selectedItem = "";
        $scope.columnsForAtt1 = [];
        $scope.prodData1 = [];
        $scope.validPattern = /^\d{0,2}(\.\d{0,2})?$/;
        $('#subproductdiv').hide();
        $('#cmnvalupdateforsubproduct').hide();
        $('#subproductattrib').hide();
        $scope.familyimport = false;
        $scope.passselecetedCategoryId = "";
        $scope.rendercommonvalueupdate = false;
        $scope.rendercommonvalueupdatesubproduct = false;
        $scope.rendersubproducts = false;
        $scope.switchsubprod = false;
        $scope.displaypaste = "";
        $scope.currentPage = '0';
        $scope.searchText = '';
        $scope.searchTextSP = '';
        var attributeWithout = [];
        var attributeWithoutSP = [];
        $scope.valueofShowSubProducts = false;
        $scope.valuesubproduct = false;
        $scope.IsNewFamily = '1';
        $scope.product_ids = "";
        $scope.subproduct_ids = "";
        $scope.Family_ID = 0;
        $scope.Falg = "empty";
        $rootScope.valueofcheckbox = false;
        $scope.BulkIds = "";
        $scope.ConfigBulkIds = "";
        $rootScope.valueofcheckall = false;
        $scope.fromPageNo = 0;
        $scope.toPageNo = 10;
        $scope.ExistingCatcheck = false;
        $scope.copycat = true;
        $scope.ProductPreviewText = "";
        $scope.selectedRowAttributes = [];
        $scope.selectedRowDynamicAttributes = [];
        $scope.copyEnable = false;
        $scope.productGridData = "All";
        $rootScope.items = [];
        $scope.detachProductId = [];

        /////   MV
        $('.hideonenter').hide();
        $scope.searchTypeValue = "";
        $scope.SearchType = "0";
        $scope.ProdSpecsValues = true;
        $scope.ProdImageValues = true;
        $scope.ProdKeyValues = true;
        $scope.ProdPriceValues = true;

        $("#selectedattributeEdit").hide();
        /////   MV

        this.empty = function () {
            this.stuff = [];
            this.refresh();
        };

        this.random = function () {
            $log.debug('random new stuff..');
            var total = Math.floor(Math.random() * 30) + 1;
            this.stuff = mockData(total);
            this.refresh();
            $log.debug('total', this.stuff.length);
        };

        this.refresh = function () {
            $log.debug('refresh..');
            $scope.$broadcast('target.resize');
            $scope.$broadcast('target.reset');
        };

        function mockData(total) {
            total = total || 20;
            var data = [];
            for (var i = 0; i < total; ++i) {
                var item = {
                    name: 'Item ' + i
                };
                for (var j = 0; j < _this.fields.length; j++) {
                    item[j] = 'column ' + _this.fields[j] + i;
                }
                data.push(item);
            }
            return data;
        }


        $scope.GetAllProductAttributesNew = new kendo.data.DataSource({
            type: "json",
            //serverFiltering: true,
            //sortable: true,
            filterable: true,
            serverFiltering: false,

            // pageable: true,
            autoBind: false,
            //serverSorting: true,
            //pageSize: 5,
            sort: { field: "SORT_ORDER", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetAllProdFamilyAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId).success(function (response) {

                        // To chcek item#,style name is null [Mariya]

                        for (var i = 0; i < response.length; i++) {
                            if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                            }
                            if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                            }
                            if (response[i]["STYLE_NAME"] == null) {
                                response[i]["STYLE_NAME"] = "";
                            }
                        }



                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ISAvailable: {
                            type: "boolean"
                        },
                        CATALOG_ID: { editable: false },
                        CATEGORY_ID: { editable: false },
                        FAMILY_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        SORT_ORDER: { editable: false },
                        PUBLISH2PRINT: { editable: false },
                        PUBLISH2WEB: { editable: false },
                        PUBLISH2PDF: { editable: false },
                        PUBLISH2EXPORT: { editable: false },
                        PUBLISH2PORTAL: { editable: false },
                        PUBLISH: { editable: false },
                        AVALIABLE: { editable: false },
                        STYLE_NAME: { editable: false },
                        ATTRIBUTE_DATATYPE: { editable: false }
                    }
                }
            }
        });

        //$("#newProductAttributeDesign .k-grid-content").css({
        //    "overflow-y": "scroll"
        //});

        $scope.newProductAttributeDesign = {
            dataSource: $scope.GetAllProductAttributesNew,
            filterable: { mode: "row" },
            //pageable: {
            //    alwaysVisible: false,
            //    pageSizes: [5, 10, 20, 100]
            //},
            //pageable: { buttonCount: 5 },
            scrollable: true,
            //editable: true,
            // height: 200,
            autoBind: false,


            // selectable: "multiple",
            dataBound: onDataBound, serverSorting: true, sortable: true,

            columns: [
                { field: "AVAILABLE", title: "Select", width: "5px", template: '# if(AVAILABLE=="1") {#<input type="checkbox"  id="chk1_#=ATTRIBUTE_ID#" checked="checked"  ng-click="checkAvailable($event, this)"></input>#} else if(AVAILABLE=="0") {#<input type="checkbox" id="chk1_#=ATTRIBUTE_ID#"  ng-click="checkAvailable($event, this)"></input>#}#', filterable: false },
                { field: "PUBLISH", title: "Publish", width: "5px", template: '# if(PUBLISH=="1") {#<input type="checkbox" id="chk_#=ATTRIBUTE_ID#" checked="checked"  ng-click="checkPublish($event, this)"></input>#} else if(PUBLISH=="0") {#<input type="checkbox" id="chk_#=ATTRIBUTE_ID#" ng-click="checkPublish($event, this)"></input>#}#', filterable: false },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "20px", filterable: true, template: "<a title='#=ATTRIBUTE_NAME#' >#=ATTRIBUTE_NAME#</a>" },
                { field: "ATTRIBUTE_TYPE", title: "Type", width: "5px", template: '# if( ATTRIBUTE_TYPE=="1") {# <span title="Specification"><i class="fa fa-tags"></i><span># } else if(ATTRIBUTE_TYPE=="3") {#<span title="Image"><i class="fa fa-image"></i><span>#} else if(ATTRIBUTE_TYPE=="6") {#<span title="Product Key"><i class="fa fa-key"></i><span>#}else if(ATTRIBUTE_TYPE=="4") {#<span title="Price"><i class="fa fa-dollar"></i><span>#} #', filterable: false },
                { field: "ATTRIBUTE_DATATYPE", title: "Data Type", width: "7px" },
                { field: "STYLE_NAME", title: "Style Name", width: "10px", template: "<a title='#=STYLE_NAME#' >#=STYLE_NAME#</a>", filterable: false }
                //{ field: "PUBLISH2PRINT", title: "Print", width: "7px", template: '# if( PUBLISH2PRINT==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PRINT == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
                //{ field: "PUBLISH2WEB", title: "Web", width: "7px", template: '# if( PUBLISH2WEB==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2WEB == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
                //{ field: "PUBLISH2PDF", title: "PDF", width: "7px", template: '# if( PUBLISH2PDF==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PDF == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
                //{ field: "PUBLISH2EXPORT", title: "Export", width: "7px", template: '# if( PUBLISH2EXPORT==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2EXPORT == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
                //{ field: "PUBLISH2PORTAL", title: "Portal", width: "7px", template: '# if( PUBLISH2PORTAL==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PORTAL == false) {#<input type="checkbox" disabled="disabled"></input>#}#' }

            ],
            dataBound: function (e) {
                var columns = e.sender.columns;
                var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "PUBLISH" + "]").index();
                var rows = e.sender.tbody.children();
                for (var j = 0; j < rows.length; j++) {
                    var row = $(rows[j]);
                    var dataItem = e.sender.dataItem(row);
                    var units = dataItem.get("PUBLISH");
                    var publish = dataItem.get("PUBLISH");
                    if (publish == 1 || publish == 0) {
                        row.addClass("selectable");
                    }
                }
            }
        };
        $scope.checkPublish = function (e, id) {

            if (id.dataItem.PUBLISH == 1) {
                id.dataItem.PUBLISH = 0;
                //Jo Nov-06-2021 for item# disable
                if (id.dataItem.ATTRIBUTE_ID == 1) {
                    $("#chk_" + id.dataItem.ATTRIBUTE_ID).prop("checked", true);
                }
                id.dataItem.set("PUBLISH", e.target.checked);
                $scope.items.push({
                    ISAvailable: true,
                    CATALOG_ID: $rootScope.selecetedCatalogId,
                    CATEGORY_ID: $scope.ActiveCategoryId,
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    FLAG: "unpublish"
                });
            }

            else {
                if (id.dataItem.AVAILABLE == 0) {
                    id.dataItem.PUBLISH = 0;
                    //  $("#chk_" + id.dataItem.ATTRIBUTE_ID).prop("checked", false);
                    //Jo Nov-06-2021 for item# disable
                    if (id.dataItem.ATTRIBUTE_ID == 1) {
                        $("#chk_" + id.dataItem.ATTRIBUTE_ID).prop("checked", true);
                    }

                    id.dataItem.set("PUBLISH", e.target.checked);
                    $scope.items.push({
                        ISAvailable: true,
                        CATALOG_ID: $rootScope.selecetedCatalogId,
                        CATEGORY_ID: $scope.ActiveCategoryId,
                        ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                        FLAG: "unpublish"
                    });

                    $("#chk_" + id.dataItem.ATTRIBUTE_ID).prop("checked", false);
                } else {


                    id.dataItem.PUBLISH = 1;
                    //Jo Nov-06-2021 for item# disable
                    if (id.dataItem.ATTRIBUTE_ID == 1) {
                        $("#chk_" + id.dataItem.ATTRIBUTE_ID).prop("checked", true);
                    }
                    id.dataItem.set("PUBLISH", e.target.checked);
                    $scope.items.push({
                        ISAvailable: true,
                        CATALOG_ID: $rootScope.selecetedCatalogId,
                        CATEGORY_ID: $scope.ActiveCategoryId,
                        ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                        FLAG: "publish"
                    });

                }
            }

        };



        $scope.checkAvailable = function (e, id) {
            debugger
            //  id.dataItem.set("PUBLISH", 1);
            if (id.dataItem.AVAILABLE == 1) {
                if (id.dataItem.ATTRIBUTE_ID != 1) {
                    id.dataItem.AVAILABLE = 0;
                    id.dataItem.PUBLISH = 0;
                    $("#chk_" + id.dataItem.ATTRIBUTE_ID).prop("checked", false);
                } else {
                    $("#chk1_" + id.dataItem.ATTRIBUTE_ID).prop("checked", true);
                }

            }
            else {

                id.dataItem.AVAILABLE = 1;
            }
        };


        $scope.sortableOptions = {
            filter: ".k-grid tr[data-uid]",
            hint: $.noop,
            cursor: "move",
            placeholder: function (element) {
                return element
                    .clone()
                    .removeAttr("uid")
                    .addClass("k-state-hover")
                    .css("opacity", 0.65);
            },
            container: ".k-grid tbody",
            change: function (e) {
              
                 
                  //  var grid = $("#id_newProductAttributeDesign").data("kendoGrid");
                    var grid = $scope.newProductAttributeDesign;
           
                dataItem = grid.dataSource.getByUid(e.item.data("uid"));
                grid.dataSource.remove(dataItem);
                grid.dataSource.insert(e.newIndex, dataItem);


                $scope.UnPublishAttributesSorting();
              }
             
        };

        //$scope.sortableOptions = {
        //    filter: ".k-grid tr[data-uid]",
        //    hint: $.noop,
        //    cursor: "move",
        //    placeholder: function (element) {
        //        return element
        //            .clone()
        //            .removeAttr("uid")
        //            .addClass("k-state-hover")
        //            .css("opacity", 0.65);
        //    },
        //    container: ".k-grid tbody",

        //    change: function (e) {
        //        console.log('325');
        //        console.log(e.newIndex);
        //        var grid = $("#newProductAttributeDesign").data("kendoGrid");
        //        console.log(grid);
        //        console.log(e.item.data("uid"));


        //        // var grid = $scope.myGrid
        //        var getItemuid = e.item.data("uid");
        //        console.log('332');
        //        console.log(grid.dataSource);
        //        var datasource = grid.dataSource;
        //        console.log(datasource);
        //        dataItem = datasource.getByUid(getItemuid);
        //        console.log('dataItem');
        //        console.log(dataItem);
        //        console.log('329');
        //        grid.dataSource.remove(dataItem);
        //        console.log('330');
        //        grid.dataSource.insert(e.newIndex, dataItem);
        //        console.log('332');
        //        $scope.UnPublishAttributesSorting();
        //    },
        //};




        $scope.valueofShowSubProductsval = function () {
            dataFactory.GetAllcommonvaluesubproductsCheck($localStorage.getCatalogID, $scope.Family.FAMILY_ID, $localStorage.CategoryID).success(function (response) {
                $scope.valueofShowSubProducts = response;
                // $scope.switchsubprod= =response;
            }).error(function (response) {
                options.success(response);
            });

        };

        $scope.tblDashBoard1 = new NgTableParams({ page: 1, count: 1000 },
            {
                counts: [],
                total: function () { return $scope.prodData1.length; },
                getData: function ($defer, params) {
                    var filteredData = $scope.prodData1;
                    var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

                }
            });

        $scope.SelectProductItem = function (data) {
            //hide the no result found
            $("div#noresultfound").hide();
            $scope.ResultEmpty = false;
            //hide the no result found
            $scope.ExistingCatcheck = true;
            $scope.selecteddRow = data.Data;
            $scope.selectedItem = $scope.selecteddRow.CatalogItemNo;
            $("#dynamicproducttable").hide();
            $("#selectedPartNo").show();
            $scope.SaveProduct();
        };

        $scope.SubSelectProductItem = function (data) {
            $scope.ExistingCatcheck = true;
            $scope.selecteddRow = data.Data;
            $scope.selectedItem = $scope.selecteddRow.CatalogItemNo;
            $("#dynamicproducttable").hide();
            $("#selectedPartNo").show();
            $scope.SaveSubProduct();
        };

        $scope.copycatclick = function (e) {
            $scope.copycat = e.currentTarget.checked;
        };

        $scope.SaveProduct = function () {
            if ($scope.clickEvent == 'buttonclick') {
                $rootScope.sortOrder = 0;
            }
            if ($rootScope.SelectedNodeID != 0)
                $rootScope.selectedFAmilyList = $rootScope.SelectedNodeID;
            $rootScope.SelectedNodeIDPresist = 1;
            if (this.selectedItem.trim() !== "") {
                if ($scope.ExistingCatcheck) {
                    $scope.winNewProductCreation.close();
                    $http.get('../Family/SubproductCheck?family_id=' + $scope.selectedItem + '&multipleFamilyids=' + $scope.checkeditems + '&multipleFamilyidsForAnotherCatalog=' + $scope.IdsForAnotherCatalog + '&catalog_id=' + $scope.selecteddRow.Catalogid + '&option=' + "Product")
                        .then(function (response) {

                            if (response.data === "True" && $rootScope.selecetedCatalogId != $scope.selecteddRow.Catalogid) {
                                $.msgBox({
                                    title: " SubProduct Confirmation",
                                    content: "Do you want to paste SubProducts?",
                                    type: "info",
                                    buttons: [{ value: "Yes" }, { value: "No" }],
                                    success: function (result) {
                                        var bool = false;
                                        if (result === "Yes") {
                                            bool = true;
                                        }
                                        dataFactory.SaveProduct($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.selectedItem, $scope.copycat, $scope.getCustomerIDs, bool, $rootScope.sortOrder, $scope.selecteddRow).success(function (response) {

                                            if (response != null) {
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: '' + response + '.',
                                                    type: "info"
                                                });
                                                $rootScope.treeData.read();
                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                $scope.selecteddRow = null;
                                                $rootScope.ProductCount = $rootScope.ProductCount + 1;
                                            }
                                        }).error(function (error) {
                                            options.error(error);
                                        });

                                    }
                                });
                            } else {
                                dataFactory.SaveProduct($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.selectedItem, $scope.copycat, $scope.getCustomerIDs, false, $rootScope.sortOrder, $scope.selecteddRow).success(function (response) {
                                    if (response != null) {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + response + '.',
                                            type: "info"
                                        });
                                        $rootScope.treeData.read();
                                        $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                        $scope.selecteddRow = null;
                                        $rootScope.ProductCount = $rootScope.ProductCount + 1;
                                        $scope.ExistingCatcheck = false;
                                    }
                                }).error(function (error) {
                                    options.error(error);
                                });

                            }
                        });
                } else {
                    // $scope.winNewProductCreation.close();
                    dataFactory.SaveProduct($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, this.selectedItem, $scope.copycat, $scope.getCustomerIDs, false, $rootScope.sortOrder, $scope.selecteddRow).success(function (response) {

                        if (response != null) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                            $rootScope.treeData.read();
                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                            $scope.selecteddRow = null;
                            $rootScope.ProductCount = $rootScope.ProductCount + 1;
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'ITEM# Cannot be empty.',
                    type: "info"
                });
            }

        };
        //$scope.myHTML = "<div><code>HTML</code>string with <a href=\"#\">links!</a> and other <em>stuff</em><br/> hi</div>";

        // Pdf Xpress Mariyavijayan   -Start

        $scope.btnProductPreview = function (data) {

            var Cat_Id = $rootScope.selecetedCatalogId;
            var CatalogID = $rootScope.selecetedCatalogId;
            var TYPE = 'PRODUCT';
            var Id = $scope.Family.FAMILY_ID;

            //  dataFactory.getPdfXpressdefaultTemplate(Cat_Id).success(function (response) {
            dataFactory.getPdfXpressdefaultTemplate_Product('Product', Id, CatalogID).success(function (response) {

                if (response == "" || response == null) {
                    $scope.btnProductPreview1(data);
                }
                else {
                    $scope.PreviewProductPdf(data);
                }

            });

        }

        // Pdf Xpress - End

        $scope.btnProductPreview1 = function (data) {

            $("#productpreview").show();
            $("#columnsetupproduct").hide();
            $("#divProductGrid").hide();
            $("#productpaging1").hide();
            $scope.productpreview = true;
            $("#myDIV").hide();
            $("#productgridtabstrip").hide();
            $(".productgridtabstripclass").hide();


            dataFactory.ProductPreview(data.Data.CATALOG_ID, data.Data.FAMILY_ID, data.Data.PRODUCT_ID, $rootScope.ProductLevelMultipletablePreview, $scope.selecetedCategoryId).success(function (response) {
                $scope.ProductPreviewText = response.replace("<b>ITEM#</b>", "<b>" + $localStorage.CatalogItemNumber + "</b>");
            }).error(function (error) {
                options.error(error);
            });
        };

        // $scope.gridCurrentPage = "5";
        $rootScope.customerId = 0;

        $scope.callpaging = function (pageno, row) {
            //

            var displayAttribures = "";
            var tt = $('#textboxvalue4');
            if ($scope.val_type === "true") {
                angular.forEach(row, function (value, key) {
                    if (key.contains("OBJ")) {
                        var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];
                        if (value != null && $("#textBoxValue" + key.split('_')[4]).length != 0) {
                            var id = sa.ATTRIBUTE_ID;

                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {

                                row[key] = $("#textBoxValue" + id)[0].nextElementSibling.innerHTML;
                            }
                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
                                row[key] = "";
                            }
                        }
                        else if ($("#textBoxValue" + key.split('_')[4]).length != 0 && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {


                            row[key] = $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
                        }
                        //To check if the attribute is a value required field
                        if (sa != undefined) {
                            if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {
                                if (displayAttribures == '') {
                                    displayAttribures = "The following attribute values are required,\n";
                                }
                                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                            }
                        }
                    }

                });
                angular.copy(row, $scope.updateproductspecs);
            }

            //  $scope.SaveAutomatically(row);

            //$("#overlay").fadeIn();
            showPageLoadingSpinner();
            $rootScope.ajaxLoaderDivShow = true;
            $rootScope.customerId = $scope.getCustomerIDs;
            if (pageno != null) {
                if (pageno == "NEXT") {
                    pageno = ($scope.ProdSpecsCurrentPage < $scope.totalSpecsProdPageCount) ? parseInt($scope.ProdSpecsCurrentPage) + 1 : $scope.totalSpecsProdPageCount;
                    $scope.ProdSpecsCurrentPage = pageno;
                    // $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }
                else if (pageno == "PREV") {
                    pageno = ($scope.ProdSpecsCurrentPage > 1) ? parseInt($scope.ProdSpecsCurrentPage) - 1 : 1;
                    $scope.ProdSpecsCurrentPage = pageno;
                    //  $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }
                else if (pageno == $scope.totalSpecsProdPageCount) {
                    $scope.ProdSpecsCurrentPage = $scope.totalSpecsProdPageCount;
                    // $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }
                else {
                    $scope.ProdSpecsCurrentPage = pageno;
                    // $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }

            }

            //  $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdSpecsCurrentPage;
            var pagingdropdown = $('#cboProdPageCountDown').data("kendoDropDownList")
            pagingdropdown.text($scope.ProdSpecsCurrentPage.toString());
            var pageData = $scope.loopCount[pageno - 1];
            var lpcnt = $scope.loopCount;
            // $("#gridpaging").data("kendoComboBox").value(pageno);
            $scope.fromPageNo = pageData.FROM_PAGE_NO;
            $scope.toPageNo = pageData.TO_PAGE_NO;
            $scope.currentPage = pageData.PAGE_NO - 1;

            if (attributeWithout.length < 15) {
                $scope.toPageNo = attributeWithout.length;
            }
            var attributestemp = [];
            for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
                if (attributeWithout[i] != undefined)
                    attributestemp.push(attributeWithout[i]);
            }
            $scope.attributes = attributestemp;
            $scope.attrLength = $scope.attributes.length;
            for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {

                if (attributeWithout[i] != undefined) {
                    var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });

                    if (tempattrvals.length != 0) {
                        var uimask = "";
                        var theString = tempattrvals[0].ATTRIBUTE_ID;
                        tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                        if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                            theString = 0;
                        }
                        if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                            var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                            pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                            pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                            var reg = new RegExp(pattern);
                            uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                            tempattrvals[0].attributePattern = reg;
                        } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                            tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                            uimask = tempattrvals[0].DECIMAL;
                        } else {
                            tempattrvals[0].attributePattern = 524288;
                            uimask = 524288;
                        }
                        tempattrvals[0].uimask = uimask;
                        $scope.selectedRowDynamicAttributes[theString] = [];
                        $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                    }
                }
            }
            $scope.isUIReleased = 0;
            $scope.iterationCount = 0;
            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                if (attributeWithout[i].contains("OBJ")) {
                    if (attributeWithout[i].split('__')[2] !== "0") {
                        var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                        if (sa.USE_PICKLIST) {
                            $scope.iterationCount++;
                        }
                    }
                }
            }
            $scope.iterationPicklistCount = 0;
            //for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
            //    if (attributeWithout[i] != undefined) {
            //        if (attributeWithout[i].contains("OBJ")) {
            //            if (attributeWithout[i].split('__')[2] !== "0") {
            //                var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
            //                if (sa.USE_PICKLIST) {
            //                    var theString = sa.ATTRIBUTE_ID;
            //                    $scope.GetPickListData(theString, sa.PICKLIST_NAME);
            //                }
            //            }
            //        }
            //    }
            //}
            if ($scope.isUIReleased == 0) {

                angular.copy($scope.updateproductspecs, $scope.selectedRow);
                blockUI.stop();

            }
            $timeout(function () {
                $rootScope.ajaxLoaderDivShow = false;
            }, 50);

        };


        function showPageLoadingSpinner() {
            $('.block-ui-overlay').hide();
            $('.imageloader').hide();
            //   $("#overlay").fadeOut();
        }





        $scope.UpdateProductItem = function (data) {

            blockUI.start();
            //$('#dynamicsort').data("kendoDropDownList").span[0].innerHTML = '';
            $q.all([$scope.UpdateProductItem1(data)]).then(function (result) {
                //hidePageLoadingSpinner();
                return;
            });
        };

        $scope.selectedRow = {};
        $scope.selectedRowDynamic = [];
        $scope.UpdateProductItem1 = function (data) {

            $scope.currentProductId = data.Data.PRODUCT_ID;
            blockUI.start();
            $scope.updateproductspecsEdit = {};
            angular.copy(data.Data, $scope.updateproductspecsEdit);
            $rootScope.ajaxLoaderDivShow = true;
            $scope.selectedRow = {};
            $scope.selectedRowEdit = {};
            $scope.attributes = [];
            showPageLoadingSpinner();
            $('#dynamicsort').data = '';
            $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
            $scope.winAddOrEditProduct.refresh({ url: "../Views/App/Partials/dynamicGridItemEditPopup.html" });
            $scope.winAddOrEditProduct.title("Product Specifications");
            $rootScope.windows_Open();
            $scope.ProductSortOrder.read();
            //  $scope.ProductSortOrder.read();
            // $scope.sorting = 0;


            $scope.hideClick();
            $scope.currentPage = '0';

            // $scope.selectedRowDynamic = $rootScope.selectedRowDynamicPickList;

            //$scope.sorting = $scope.updateproductspecs["SORT"];
            //$scope.selectedRow = {};
            // angular.copy($scope.updateproductspecs, $scope.selectedRow);
            attributeWithout = [];
            $scope.attributesOLD = Object.keys(data.Data);
            if ($scope.catalogids !== 0 && $scope.familyids !== 0) {
                dataFactory.GetAttributeDetails(data.Data.CATALOG_ID, data.Data.FAMILY_ID).success(function (response) {
                    if (response != null) {
                        $scope.selectedRowAttributes = response;
                        var calcAttributestemp = [];
                        angular.forEach($scope.attributesOLD, function (value, key) {
                            if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2') || value == 'WORKFLOW STATUS')
                                attributeWithout.push(value);
                        });
                        $scope.attributescalc = attributeWithout;
                        $scope.fromPageNo = 0;
                        $scope.toPageNo = 150;
                        if (attributeWithout.length < 150) {
                            $scope.toPageNo = attributeWithout.length;
                        }
                        var attributestemp = [];
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            attributestemp.push(attributeWithout[i]);
                        }
                        $scope.attributes = attributestemp;
                        $scope.attrLength = $scope.attributes.length;
                        $scope.ProdSpecsCountPerPage = "150";
                        $scope.ProdSpecsCurrentPage = "1";

                        $scope.loopCount = [];
                        $scope.loopEditProdsCount = [];
                        var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                        var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                        if ((attributeWithout.length % parseInt($scope.ProdSpecsCountPerPage)) > 0) {
                            loopcnt = loopcnt + 1;
                            loopEditProdsCnt = loopEditProdsCnt + 1;
                        }
                        $scope.ProdSpecsPageCount = $scope.loopCount;
                        $scope.totalSpecsProdPageCount = loopEditProdsCnt;


                        for (var i = 0; i < loopcnt; i++) {
                            if (((i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)) < attributeWithout.length) {
                                $scope.loopCount.push({
                                    PAGE_NO: (i + 1),
                                    FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                                    TO_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)
                                });
                            } else {
                                $scope.loopCount.push({
                                    PAGE_NO: (i + 1),
                                    FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                                    TO_PAGE_NO: attributeWithout.length
                                });
                            }
                        }


                        var theString;
                        $scope.selectedRowEdit = {};
                        angular.copy(data.Data, $scope.selectedRowEdit);
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            if (attributeWithout[i].contains('__')) {
                                var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
                                if (tempattrvals.length != 0) {
                                    var uimask = "";
                                    theString = tempattrvals[0].ATTRIBUTE_ID;
                                    tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                                    if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                                        theString = 0;
                                    }
                                    var itemval = attributeWithout[i];
                                    if (tempattrvals[0].ATTRIBUTE_ID === 1) {
                                        $scope.item_ = $scope.selectedRowEdit[itemval];
                                    }
                                    if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                                        var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                        pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                                        pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                                        var reg = new RegExp(pattern);
                                        uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                                        tempattrvals[0].attributePattern = reg;
                                    } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                        tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                                        uimask = tempattrvals[0].DECIMAL;
                                    } else {
                                        tempattrvals[0].attributePattern = 524288;
                                        uimask = 524288;
                                    }
                                    tempattrvals[0].uimask = uimask;
                                    $scope.selectedRowDynamicAttributes[theString] = [];
                                    $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                                }
                            }
                        }
                        $scope.groups = [];
                        var picklistNAme = "";
                        var attrId = "";
                        $scope.iterationCount = 0;
                        $scope.isUIReleased = 0;
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            if (attributeWithout[i].contains("OBJ")) {
                                if (attributeWithout[i].split('__')[2] !== "0") {
                                    var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                                    if (sa.USE_PICKLIST) {
                                        $scope.iterationCount++;
                                    }
                                }
                            }
                        }
                        // $rootScope.selectedRowDynamic = [];

                        $scope.iterationPicklistCount = 0;
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            if (attributeWithout[i].contains("OBJ")) {
                                if (attributeWithout[i].split('__')[2] !== "0") {
                                    var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                                    if (sa.USE_PICKLIST) {
                                        $scope.GetPickListData(sa.ATTRIBUTE_ID, sa.PICKLIST_NAME);
                                    }
                                }
                            }
                        }


                        $scope.updateproductspecs = data.Data;
                        $("#result").hide();

                        if ($scope.isUIReleased == 0) {

                            $scope.selectedRow = {};


                            angular.copy($scope.updateproductspecs, $scope.selectedRow);
                            // $localStorage.selectedRow1 = $scope.selectedRow;
                            $scope.selectedRow["WORKFLOW STATUS"] = e.sender.value();
                            $scope.ProductSortOrder.read();

                            angular.copy($scope.updateproductspecs, $scope.selectedRow);

                            //var ddl = $('#dynamicsort').data('kendoDropDownList');
                            //ddl.dataSource.data({}); // clears dataSource
                            //ddl.text(""); // clears visible text
                            //ddl.value("");

                            $rootScope.windows_Open();

                            blockUI.stop();




                            $rootScope.ajaxLoaderDivShow = false;

                            //$scope.selectedRow["SORT"] = data.$index + 1;
                            //$timeout(function () {
                            //    $scope.selectedRow["SORT"] = data.$index + 1;

                            //}, 50);

                            $timeout(function () {

                                blockUI.stop();
                            }, 100);
                            // $rootScope.ajaxLoaderDivShow = false;

                        }
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
        };

        $scope.GetPickListData = function (attrName, picklistdata) {
            dataFactory.getPickListData(picklistdata).success(function (response) {
                $scope.selectedRowDynamic[attrName] = response;
                $scope.iterationPicklistCount++;
                if ($scope.iterationPicklistCount == $scope.iterationCount) {
                    //$rootScope.windows_Open();
                    $timeout(function () {
                        $scope.selectedRow = {};
                        angular.copy($scope.updateproductspecs, $scope.selectedRow);
                        blockUI.stop();
                        $scope.isUIReleased = 1;
                    }, 200);


                }
            }).error(function (error) {
                options.error(error);
            });
        };

        $rootScope.windows_Open = function () {
            $scope.winAddOrEditProduct.center().open();
        }

        $scope.EntercallAttributeNameSearch = function (e) {

            if (e.keyCode == 13) {
                $scope.callAttributeNameSearch();
            }
        };

        $scope.callAttributeNameSearch = function () {
            $scope.fromPageNo = 1;
            $scope.toPageNo = 1;
            $scope.currentPage = -1;
            var attributeWithout = [];
            var attributesearch = [];

            angular.forEach($scope.attributesOLD, function (value, key) {
                if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2') || value == 'WORKFLOW STATUS')
                    attributeWithout.push(value);

            });
            angular.forEach(attributeWithout, function (value, key) {
                if (value.toLowerCase().indexOf($scope.searchText.toLowerCase()) > -1 || value.toUpperCase() == "PUBLISH2WEB" || value.toUpperCase() == "PUBLISH2PRINT" || value.toUpperCase() == "PUBLISH2PDF" || value.toUpperCase() == "PUBLISH2EXPORT" || value.toUpperCase() == "PUBLISH2PORTAL") {
                    attributesearch.push(value);
                }
            });

            attributeWithout = attributesearch;

            if (attributeWithout == 0) {
                $("#result").show();
            }
            else {
                $("#result").hide();
            }


            $scope.toPageNo = attributeWithout.length;

            var attributestemp = [];
            for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
                attributestemp.push(attributeWithout[i]);
            }
            $scope.attributes = attributestemp;
            if ($scope.attributes.length === 5) {
                $('#selectedattributenoresultfound').show();
            }
            else {
                $('#selectedattributenoresultfound').hide();
            }
            for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {

                var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
                if (tempattrvals.length > 0) {
                    var theString = tempattrvals[0].ATTRIBUTE_ID;
                    var uimask = "";
                    tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                    if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                        theString = 0;
                    }
                    if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                        var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                        pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                        pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                        var reg = new RegExp(pattern);
                        uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                        tempattrvals[0].attributePattern = reg;
                    } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                        tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                        uimask = tempattrvals[0].DECIMAL;
                    } else {
                        tempattrvals[0].attributePattern = 524288;
                        uimask = 524288;
                    }
                    //$scope.attributePattern[name] = reg;
                    tempattrvals[0].uimask = uimask;
                    $scope.selectedRowDynamicAttributes[theString] = [];
                    $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                    // $scope.selectedRowDynamicAttributes[theString] = uimask;
                }
            }
            for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {

                if (attributeWithout[i].contains("OBJ")) {
                    if (attributeWithout[i].split('__')[2] !== "0") {
                        var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                        if (sa.USE_PICKLIST) {
                            var theString = sa.ATTRIBUTE_ID;
                            $scope.GetPickListData(theString, sa.PICKLIST_NAME);
                        }
                    }
                }
            }
            $scope.searchText = '';
        };

        $scope.EntercallAttributeNameSubSearch = function (e) {
            if (e.keyCode == 13) {
                $scope.callAttributeNameSearchSP();
            }
        };

        $scope.callAttributeNameSearchSP = function () {

            $scope.fromPageNoSP = 1;
            $scope.toPageNoSP = 1;
            $scope.currentPageSP = -1;
            var attributeWithoutSP = [];
            var attributesearchSP = [];

            angular.forEach($scope.attributesOLDSP, function (value, key) {
                if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2') || value == 'WORKFLOW STATUS')
                    attributeWithoutSP.push(value);
            });
            angular.forEach(attributeWithoutSP, function (value, key) {
                if (value.toLowerCase().indexOf($scope.searchTextSP.toLowerCase()) > -1) {
                    attributesearchSP.push(value);
                }
            });

            attributeWithoutSP = attributesearchSP;
            $scope.toPageNoSP = attributeWithoutSP.length;

            var attributestempSP = [];
            for (var i = $scope.fromPageNoSP - 1; i < $scope.toPageNoSP; i++) {
                attributestempSP.push(attributeWithoutSP[i]);
            }

            attributeWithout = attributestempSP;
            if (attributeWithout == 0) {
                $("#resultsub").show();
            }
            else {
                $("#resultsub").hide();
            }
            $scope.attributes = attributestempSP;

            for (var i = $scope.fromPageNoSP - 1; i < $scope.toPageNoSP; i++) {
                var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithoutSP[i].split('__')[2] });
                if (tempattrvals.length > 0) {
                    var theString = tempattrvals[0].ATTRIBUTE_ID;
                    var uimask = "";
                    tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                    if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                        theString = 0;
                    }
                    if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                        var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                        pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                        pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                        var reg = new RegExp(pattern);
                        uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                        tempattrvals[0].attributePattern = reg;
                    } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                        tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                        uimask = tempattrvals[0].DECIMAL;
                    } else {
                        tempattrvals[0].attributePattern = 524288;
                        uimask = 524288;
                    }
                    //$scope.attributePattern[name] = reg;
                    tempattrvals[0].uimask = uimask;
                    $scope.selectedRowDynamicAttributes[theString] = [];
                    $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                    // $scope.selectedRowDynamicAttributes[theString] = uimask;
                }
            }
            for (var i = $scope.fromPageNoSP - 1; i < $scope.toPageNoSP; i++) {
                if (attributeWithoutSP[i].contains("OBJ")) {
                    if (attributeWithoutSP[i].split('__')[2] !== "0") {
                        var sa = $scope.selectedRowDynamicAttributes[attributeWithoutSP[i].split('__')[2]];
                        if (sa.USE_PICKLIST) {
                            var theString = sa.ATTRIBUTE_ID;
                            $scope.GetPickListData(theString, sa.PICKLIST_NAME);
                        }
                    }
                }
            }
            $scope.searchTextSP = '';
        };



        $scope.TextBoxChanged = function (e, id, attribute) {

            var url = e.currentTarget.value;
            var ids = e.currentTarget.id;
            var pattern = /((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)/;
            if (pattern.test(url)) {
                $scope.val_type = "true";
            } else {
                $scope.selectedRow[attribute] = "";
                if ($scope.selectedRowSub != undefined)
                    $scope.selectedRowSub[attribute] = "";
                //$("#" + ids).focus();
                // alert("Please Enter a Valid URL");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter a valid URL.',
                    type: "info"
                });

            }

        };

        $scope.blurCheck = false;

        $scope.datatypecheck = function (event, type, e) {
            $scope.blurCheck = true;
            var value = event.currentTarget.value;
            var datatype = type;
            var pattern = "";

            if (type !== "All Characters" && !type.contains("0-9") && !type.contains("uFFFF")) {
                pattern = /^[a-zA-Z ]+$/;
            } else {
                if (type.contains("uFFFF")) {
                    //pattern = /^[ A-Za-z0-9_@./#&@$%"'^*!~*?+-]*$/;
                } else {
                    pattern = /^[0-9a-zA-Z ]*$/;
                }
            }
            if (pattern !== "" && value.trim() !== "") {
                var id = event.currentTarget.id;
                if (!pattern.test(value)) {
                    $scope.selectedRow[e] = "";
                    if ($scope.selectedRowSub != undefined)
                        $scope.selectedRowSub[e] = "";
                    // alert("Please Enter Valid Data.");

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please enter valid Data.',
                        type: "error"
                    });


                }
            }

        };
        $scope.SubProductSortOrder = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.getsubProductSortorder($scope.ParentProductId, $rootScope.selecetedCatalogId).success(function (response) {
                        options.success(response);


                        $scope.subsorting = $scope.selectedRowSub["SORT"];


                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        $scope.onDateSelected = function (e, th) {

            var dateReg = /^\d{2}[./-]\d{2}[./-]\d{4}\s\d{2}:\d{2}\s[AP]M$/;
            if (!e.match(dateReg)) {
                $('#datetime' + th.$$watchers[1].last).val("");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter valid format.',
                    type: "error"
                });
            }
        };



        var Productdetails = [];

        $rootScope.SaveOrUpdateProductItem = function (row) {
            //$(".ckeditorUpdate").mouseout()

            $scope.productGridSearchValue = "";
            $scope.oldAttributeValues = $scope.updateproductspecs;

            var displayAttribures = "";
            var tt = $('#textboxvalue4');
            if ($scope.val_type === "true") {

                angular.forEach(row, function (value, key) {
                    if (key.contains("OBJ")) {
                        var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];



                        if (value != null && $("#textBoxValue" + key.split('_')[4]).length != 0 && sa != undefined && sa.ATTRIBUTE_TYPE != 3) {
                            var id = sa.ATTRIBUTE_ID;

                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {

                                row[key] = $("#textBoxValue" + id)[0].nextElementSibling.innerHTML;
                            }
                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
                                row[key] = "";
                            }
                        }
                        else if (sa != undefined && sa.ATTRIBUTE_TYPE != 3 && $("#textBoxValue" + key.split('_')[4]).length != 0 && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {


                            row[key] = $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
                        }
                        //To check if the attribute is a value required field
                        if (sa != undefined) {
                            if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {
                                if (displayAttribures == '') {
                                    displayAttribures = "The following attribute values are required,\n";
                                }
                                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                            }
                        }
                    }

                });

                if (displayAttribures == '') {
                    $timeout(function () {
                        dataFactory.SaveProductSpecs(row, $localStorage.CategoryID, $scope.ParentProductId, $scope.blurCheck, $scope.oldAttributeValues).success(function (response) {

                            if (response != null) {
                                $scope.blurCheck = false;
                                // $scope.winAddOrEditProduct.destroy();
                                $scope.winAddOrEditProduct.close();
                                $scope.winAddOrEditSubProduct.close();
                                var familyId = '~' + $scope.Family.FAMILY_ID;
                                //$scope.winAddOrEditProductVisible = false;
                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, familyId, $localStorage.CategoryID);
                                $rootScope.rowselectedEvent();
                                if ($scope.ParentProductId !== 0) {

                                    $scope.LoadSubproductData();
                                }
                                // alert(response);
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                            }

                        }).error(function (error) {
                            options.error(error);
                        });
                    }, 200);
                } else {

                    // alert(displayAttribures);

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + displayAttribures + '.',
                        type: "info"
                    });

                }
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter Data using a valid format.',
                    type: "error"
                });
            }

            /// --------------------------------Undo family and Product Tab------------------------------------------------------
            $scope.productSpec = $rootScope.selectedProductAttributeRow;
            $rootScope.ProductDetailsValue = $rootScope.selectedProductAttributeRow;
            $rootScope.baseProductRowAttributes = $scope.selectedRowAttributes;
            $scope.$emit("SendUndoProductGridDetails", $rootScope.ProductDetailsValue);

            /// --------------------------------Undo family and Product Tab------------------------------------------------------


            /// -----------------------------Only Undo Product Tab---------------------------------------------------------

            if ($rootScope.undoProductFlag == "0") {
                $scope.productSpec = angular.copy(row);
                //  angular.extend($scope.productSpec, $scope.familyValues)
                Productdetails.push($scope.productSpec);
                $scope.detailslength = Productdetails.length;
                if (Productdetails.length > 10) {
                    Productdetails.splice(0, 1);
                }

                $rootScope.UndoProductDetailsValueOnly = {};

                $rootScope.UndoProductDetailsValueOnly = $scope.productSpec;

                $localStorage.ProductDetailsValueOnly = Productdetails;

                $scope.$emit("SendOnlyUndoProductGridDetails", $rootScope.UndoProductDetailsValueOnly);

            }

            else {
                $rootScope.undoProductFlag = "0";
            }

            /// -----------------------------Only Undo Product Tab---------------------------------------------------------


            $('.block-ui-overlay').show();
            $('.imageloader').show();
            $scope.workflowProductDataSource.read();
        };
        var Productdetails = [];

        $rootScope.SaveOrUpdatecategoryItem = function (row) {
            //$(".ckeditorUpdate").mouseout()

            $scope.productGridSearchValue = "";
            $scope.oldAttributeValues = $rootScope.updateproductspecs;

            var displayAttribures = "";
            var tt = $('#textboxvalue4');
            if ($scope.val_type === "true") {

                angular.forEach(row, function (value, key) {
                    if (key.contains("OBJ")) {
                        var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];



                        if (value != null && $("#textBoxValue" + key.split('_')[4]).length != 0 && sa != undefined && sa.ATTRIBUTE_TYPE != 3) {
                            var id = sa.ATTRIBUTE_ID;

                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {

                                row[key] = $("#textBoxValue" + id)[0].nextElementSibling.innerHTML;
                            }
                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
                                row[key] = "";
                            }
                        }
                        else if (sa != undefined && sa.ATTRIBUTE_TYPE != 3 && $("#textBoxValue" + key.split('_')[4]).length != 0 && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {


                            row[key] = $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
                        }
                        //To check if the attribute is a value required field
                        if (sa != undefined) {
                            if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {
                                if (displayAttribures == '') {
                                    displayAttribures = "The following attribute values are required,\n";
                                }
                                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                            }
                        }
                    }

                });
                dataFactory.getCategoryFamilyId($rootScope.selecetedCatalogId, $localStorage.CategoryID).success(function (response) {

                    if (response == '0') {
                        $scope.Category_IDs = response.m_Item1;
                        $scope.Family_IDs = response.m_Item2;
                    } else {
                        $scope.Category_IDs = response.m_Item1;
                        if (response.m_Item2 == "")
                        {
                            $scope.Family_IDs = 0;
                        }
                        else {
                            $scope.Family_IDs = response.m_Item2;
                        }
                        
                    }
                });
                if (displayAttribures == '') {
                    $timeout(function () {
                        dataFactory.SaveProductSpecs(row, $localStorage.CategoryID, $scope.ParentProductId, $scope.blurCheck, $scope.oldAttributeValues).success(function (response) {

                            if (response != null) {
                                $scope.blurCheck = false;
                                // $scope.winAddOrEditProduct.destroy();
                                $scope.winAddOrEditProduct.close();
                                $scope.winAddOrEditSubProduct.close();
                                var familyId = '~' + $scope.Family.FAMILY_ID;
                                //$scope.winAddOrEditProductVisible = false;
                                $rootScope.LoadProdItemData($rootScope.selecetedCatalogId, $scope.Family_IDs, $localStorage.CategoryID);
                                $rootScope.rowselectedEvent();
                                if ($scope.ParentProductId !== 0) {

                                    $scope.LoadSubproductData();
                                }
                                // alert(response);
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                            }

                        }).error(function (error) {
                            options.error(error);
                        });
                    }, 200);

                } else {

                    // alert(displayAttribures);

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + displayAttribures + '.',
                        type: "info"
                    });

                }
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter Data using a valid format.',
                    type: "error"
            });

            }

            /// --------------------------------Undo family and Product Tab------------------------------------------------------
            $scope.productSpec = $rootScope.selectedProductAttributeRow;
            $rootScope.ProductDetailsValue = $rootScope.selectedProductAttributeRow;
            $rootScope.baseProductRowAttributes = $scope.selectedRowAttributes;
            $scope.$emit("SendUndoProductGridDetails", $rootScope.ProductDetailsValue);

            /// --------------------------------Undo family and Product Tab------------------------------------------------------


            /// -----------------------------Only Undo Product Tab---------------------------------------------------------

            if ($rootScope.undoProductFlag == "0") {
                $scope.productSpec = angular.copy(row);
                //  angular.extend($scope.productSpec, $scope.familyValues)
                Productdetails.push($scope.productSpec);
                $scope.detailslength = Productdetails.length;
                if (Productdetails.length > 10) {
                    Productdetails.splice(0, 1);
                }

                $rootScope.UndoProductDetailsValueOnly = {};

                $rootScope.UndoProductDetailsValueOnly = $scope.productSpec;

                $localStorage.ProductDetailsValueOnly = Productdetails;

                $scope.$emit("SendOnlyUndoProductGridDetails", $rootScope.UndoProductDetailsValueOnly);

            }

            else {
                $rootScope.undoProductFlag = "0";
            }

            /// -----------------------------Only Undo Product Tab---------------------------------------------------------


            $('.block-ui-overlay').show();
            $('.imageloader').show();
            $scope.workflowProductDataSource.read();
        };
        //var Productdetails = [];

        //$rootScope.SaveOrUpdateProductItem = function (row) {

        //    //$(".ckeditorUpdate").mouseout()

        //    $scope.productGridSearchValue = "";
        //    $scope.oldAttributeValues = $scope.updateproductspecs;

        //    var displayAttribures = "";
        //    var tt = $('#textboxvalue4');
        //    if ($scope.val_type === "true") {

        //        angular.forEach(row, function (value, key) {
        //            if (key.contains("OBJ")) {
        //                var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];



        //                if (value != null && $("#textBoxValue" + key.split('_')[4]).length != 0 && sa.ATTRIBUTE_TYPE!=3 ) {
        //                    var id = sa.ATTRIBUTE_ID;

        //                    if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {

        //                        row[key] = $("#textBoxValue" + id)[0].nextElementSibling.innerHTML;
        //                    }
        //                    if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
        //                        row[key] = "";
        //                    }
        //                }
        //                else if (sa.ATTRIBUTE_TYPE != 3 && $("#textBoxValue" + key.split('_')[4]).length != 0 && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {


        //                    row[key] = $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
        //                }
        //                //To check if the attribute is a value required field
        //                if (sa != undefined) {
        //                    if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {
        //                        if (displayAttribures == '') {
        //                            displayAttribures = "The following attribute values are required,\n";
        //                        }
        //                        displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
        //                    }
        //                }
        //            }

        //        });

        //        if (displayAttribures == '') {

        //            dataFactory.SaveProductSpecs(row, $localStorage.CategoryID, $scope.ParentProductId, $scope.blurCheck, $scope.oldAttributeValues).success(function (response) {

        //                if (response != null) {
        //                    $scope.blurCheck = false;
        //                    // $scope.winAddOrEditProduct.destroy();
        //                    $scope.winAddOrEditProduct.close();
        //                    $scope.winAddOrEditSubProduct.close();
        //                    var familyId = '~' + $scope.Family.FAMILY_ID;
        //                    //$scope.winAddOrEditProductVisible = false;
        //                    $rootScope.LoadProdData($rootScope.selecetedCatalogId, familyId, $localStorage.CategoryID);
        //                    if ($scope.ParentProductId !== 0) {

        //                        $scope.LoadSubproductData();
        //                    }
        //                    // alert(response);
        //                    $.msgBox({
        //                        title: $localStorage.ProdcutTitle,
        //                        content: '' + response + '.',
        //                        type: "info"
        //                    });
        //                }

        //            }).error(function (error) {
        //                options.error(error);
        //            });
        //        } else {

        //            // alert(displayAttribures);

        //            $.msgBox({
        //                title: $localStorage.ProdcutTitle,
        //                content: '' + displayAttribures + '.',
        //                type: "info"
        //            });

        //        }
        //    } else {
        //        $.msgBox({
        //            title: $localStorage.ProdcutTitle,
        //            content: 'Please enter Data using a valid format.',
        //            type: "error"
        //        });
        //    }

        //    /// --------------------------------Undo family and Product Tab------------------------------------------------------
        //    $scope.productSpec = $rootScope.selectedProductAttributeRow;
        //    $rootScope.ProductDetailsValue = $rootScope.selectedProductAttributeRow;
        //    $rootScope.baseProductRowAttributes = $scope.selectedRowAttributes;
        //    $scope.$emit("SendUndoProductGridDetails", $rootScope.ProductDetailsValue);

        //    /// --------------------------------Undo family and Product Tab------------------------------------------------------


        //    /// -----------------------------Only Undo Product Tab---------------------------------------------------------

        //    if ($rootScope.undoProductFlag == "0") {
        //        $scope.productSpec = angular.copy(row);
        //        //  angular.extend($scope.productSpec, $scope.familyValues)
        //        Productdetails.push($scope.productSpec);
        //        $scope.detailslength = Productdetails.length;
        //        if (Productdetails.length > 10) {
        //            Productdetails.splice(0, 1);
        //        }

        //        $rootScope.UndoProductDetailsValueOnly = {};

        //        $rootScope.UndoProductDetailsValueOnly = $scope.productSpec;

        //        $localStorage.ProductDetailsValueOnly = Productdetails;

        //        $scope.$emit("SendOnlyUndoProductGridDetails", $rootScope.UndoProductDetailsValueOnly);

        //    }

        //    else {
        //        $rootScope.undoProductFlag = "0";
        //    }

        //    /// -----------------------------Only Undo Product Tab---------------------------------------------------------


        //    $('.block-ui-overlay').show();
        //    $('.imageloader').show();
        //    $scope.workflowProductDataSource.read();
        //};



        $rootScope.SaveOrUpdateProductItemForUndo = function (row) {

            $scope.productGridSearchValue = "";

            var displayAttribures = "";
            var tt = $('#textboxvalue4');
            if ($scope.val_type === "true") {
                //
                angular.forEach(row, function (value, key) {
                    if (key.contains("OBJ")) {
                        //
                        var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];


                        //
                        if (value != null && $("#textBoxValue" + key.split('_')[4]).length != 0) {
                            //
                            var id = sa.ATTRIBUTE_ID;

                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {
                                if (!$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Value is too long") && !value.includes("Value is too long")) {

                                    row[key] = $("#textBoxValue" + id)[0].nextElementSibling.innerHTML;
                                }
                                else {
                                    row[key] = value;
                                }

                                if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
                                    row[key] = "";
                                }
                            }
                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
                                row[key] = "";
                            }
                        }
                        else if ($("#textBoxValue" + key.split('_')[4]).length != 0 && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {


                            row[key] = $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
                        }
                        if (sa != undefined) {
                            //To check if the attribute is a value required field
                            if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {

                                if (displayAttribures == '') {
                                    displayAttribures = "The following attribute values are required,\n";
                                }
                                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                            }
                        }

                    }

                });

                if (displayAttribures == '') {

                    dataFactory.SaveProductSpecs(row, $localStorage.CategoryID, $scope.ParentProductId, $scope.blurCheck).success(function (response) {

                        if (response != null) {
                            $scope.blurCheck = false;
                            // $scope.winAddOrEditProduct.destroy();
                            $scope.winAddOrEditProduct.close();
                            $scope.winAddOrEditSubProduct.close();

                            //$scope.winAddOrEditProductVisible = false;
                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                            if ($scope.ParentProductId !== 0) {

                                $scope.LoadSubproductData();
                            }
                            // alert(response);
                            //$.msgBox({
                            //    title: $localStorage.ProdcutTitle,
                            //    content: '' + response + '.',
                            //    type: "info"
                            //});
                        }

                    }).error(function (error) {
                        options.error(error);
                    });
                } else {

                    // alert(displayAttribures);

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + displayAttribures + '.',
                        type: "info"
                    });

                }
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter Data using a valid format.',
                    type: "error"
                });
            }



            $('.block-ui-overlay').show();
            $('.imageloader').show();
        };


        //Edit Value Save Automatically
        $scope.SaveAutomatically = function (row) {
            //
            var displayAttribures = "";
            var tt = $('#textboxvalue4');
            if ($scope.val_type === "true") {
                angular.forEach(row, function (value, key) {
                    if (key.contains("OBJ")) {
                        var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];



                        if (value != null && $("#textBoxValue" + key.split('_')[4]).length != 0) {
                            var id = sa.ATTRIBUTE_ID;

                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {

                                row[key] = $("#textBoxValue" + id)[0].nextElementSibling.innerHTML;
                            }
                        }
                        else if ($("#textBoxValue" + key.split('_')[4]).length != 0 && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {


                            row[key] = $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
                        }
                        //To check if the attribute is a value required field
                        if (sa != undefined) {
                            if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {
                                if (displayAttribures == '') {
                                    displayAttribures = "The following attribute values are required,\n";
                                }
                                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                            }
                        }
                    }

                });
            }
            if (displayAttribures == '') {
                dataFactory.SaveProductSpecs(row, $localStorage.CategoryID, $scope.ParentProductId, $scope.blurCheck).success(function (response) {
                    if (response != null) {
                        //$scope.blurCheck = false;
                        // $scope.winAddOrEditProduct.destroy();
                        // $scope.winAddOrEditProduct.close();
                        //$scope.winAddOrEditSubProduct.close();

                        //$scope.winAddOrEditProductVisible = false;
                        //   $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                        if ($scope.ParentProductId !== 0) {

                            $scope.LoadSubproductData();
                        }
                        // alert(response);
                        //$.msgBox({
                        //    title: $localStorage.ProdcutTitle,
                        //    content: '' + response + '',
                        //    type: "info"
                        //});
                    }

                }).error(function (error) {
                    options.error(error);
                });
            }
        }

        //END

        $scope.CancelProductItem = function () {
            $("#gridproduct").css("display", "block");
            $(".check").prop("checked", false);
            if ($scope.ResultEmpty == true) {
                $('#productpaging').attr("style", "display: none !important");
            }
            else {
                $('#productpaging').attr("style", "display: block !important");
            }
            $('#selectedattributeitem').css('display', 'none');
            $('#selectedattributenoresultfound').hide();
            $('#filterAttributePack').show();
            $scope.selectedattributeEdit = false;
            $('#productpaging').attr("style", "display: none !important");
            $('#selectedattributeEdit').hide();
            $('.pull-right').show();
            $("div#noresultfound").show();
            $('#selectedattribute').hide();
            $("#divProductGrid").removeClass("col-sm-3");
            $("#divProductGrid").addClass("col-sm-12");


            //New UI changes For 10.9.3
            $("#mainProductsMenu").show();
            $("#filterAttributePack").show();
            $("#filterAttributePackLeft").show();
            $("#productpaging1").show();
            $("#Productcountid").show();
            $("#sampleProdgrid").show();
            $("#productpaging").show();

            //New UI 11july2022
            //$("#myDIV").hide();

            if ($rootScope.undoProductFlag == "1") {
                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
            }

            $scope.CheckedCheckbox = false;
            $rootScope.valueofcheckall = false;
            var val = $scope.product_ids;
            var res = val.split(",");
            var product_Id = res[1];
            var Product_IDs = $scope.product_ids;
            if (res.length > 0) {
                for (var i = 0; i < res.length; i++) {
                    if (res[i] === product_Id) {
                        Product_IDs = $scope.product_ids.replace(product_Id, "");
                    }
                }
            }
            Product_IDs = Product_IDs.replace(",,", ",");
            $scope.product_ids = Product_IDs.replace(",", "");
            var c = $scope.product_ids.split(",");
            if (c.length == 2) {
                $scope.flag = 0;
                if ($scope.flag == 0) {
                    $('#divProductGrid1').hide();
                    $('#panel').hide();
                    $('#selectedattribute').show();
                    $("#selectedattributeEdit").hide();
                    $("#divProductGrid").addClass("col-sm-3");
                    $("#divProductGrid").removeClass("col-sm-12");
                    $scope.flag = 1;
                }
            }
            else if (c.length == 1) {
                $('#divProductGrid1').hide();
                $('#panel').hide();
                $('#selectedattribute').hide();
                $("#divProductGrid").removeClass("col-sm-2");
                $("#divProductGrid").addClass("col-sm-12");
            }
            else {
                $scope.flag = 0;
                if ($scope.flag == 0) {
                    $('#divProductGrid2').show();
                    $('#selectedattribute').hide();
                    $('#panel').show();
                    //$("#divProductGrid").removeClass("col-sm-2");
                    //$("#divProductGrid").addClass("col-sm-12");
                    $scope.flag = 1;
                }
            }



            //    $rootScope.valueofcheckbox = false;
            //    $scope.CommonFuncationProductGrid();
        };


        $scope.keyPress = function (keyCode) {

            $scope.selectedRow.attribute = $scope.selectedRow.attribute + 1;
        };
        $scope.DeleteProduct = function (selectedRow) {
            if (selectedRow == "deleteDetach") {
                $scope.hideClick();
                $rootScope.SelectedNodeIDPresist = 1;
                $rootScope.selectedFAmilyList = $rootScope.SelectedNodeID;
                $scope.selectedFamilyID = $scope.Family_ID;
                $scope.menuFamilyID = $scope.Family_ID;
                $scope.menuProductID = $scope.menuProductIds;

            }
            else {
                $scope.hideClick();
                $rootScope.SelectedNodeIDPresist = 1;
                $rootScope.selectedFAmilyList = $rootScope.SelectedNodeID;
                $scope.selectedFamilyID = selectedRow.Data.FAMILY_ID;
                $scope.menuFamilyID = selectedRow.Data.FAMILY_ID;;
                $scope.menuProductID = selectedRow.Data.PRODUCT_ID;

            }
            if ($scope.selectedFamilyID !== "") {

                $(".menuProduct").hide();
                $http.get("../Catalog/GetCatalog?catalogId=" + selectedRow.Data.CATALOG_ID).then(function (objcategoryDetails) {
                    $scope.catalogDetails = objcategoryDetails.data[0];
                    $http.get("../Catalog/GetProductExists?productId=" + selectedRow.Data.PRODUCT_ID + '&catalogId=' + selectedRow.Data.CATALOG_ID).then(function (objcategoryDetails) {
                        $scope.ProductCount = objcategoryDetails.data[0];
                        if ($scope.ProductCount == "1") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Do you like to move this Item to Default Product?",
                                type: "confirm",
                                buttons: [{ value: "Yes" }, { value: "No" }],
                                success: function (result) {
                                    var bool = false;
                                    if (result === "Yes") {
                                        bool = true;
                                    }
                                    if (bool == true) {
                                        dataFactory.moveProductDefaultFamily(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID, $scope.catalogDetails.DEFAULT_FAMILY).success(function (response) {
                                            if (response == 'Item Already Exist In Default Product.') {
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: "Item already exists in Default Product. Would you like to delete it from the source you copied it from?",
                                                    type: "confirm",
                                                    buttons: [{ value: "Yes" }, { value: "No" }],
                                                    success: function (result2) {
                                                        bool = false;
                                                        if (result2 === "Yes") {
                                                            bool = true;
                                                        }
                                                        if (bool === true) {
                                                            dataFactory.DeleteProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                                                $.msgBox({
                                                                    title: $localStorage.ProdcutTitle,
                                                                    content: '' + response + '.',
                                                                    type: "info"
                                                                });
                                                                $rootScope.treeData.read();
                                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                            }).error(function (error) {
                                                                options.error(error);
                                                            });
                                                        }
                                                    }
                                                });
                                            } else {
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: '' + response + '.',
                                                    type: "info"
                                                });
                                                $rootScope.treeData.read();
                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID);
                                            }
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    } else {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: "Are you sure you want to delete Item?",
                                            type: "confirm",
                                            buttons: [{ value: "Yes" }, { value: "No" }],
                                            success: function (result1) {
                                                bool = false;
                                                if (result1 === "Yes") {
                                                    bool = true;
                                                }
                                                if (bool === true) {
                                                    dataFactory.trashProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                                        $.msgBox({
                                                            title: $localStorage.ProdcutTitle,
                                                            content: '' + response + '.',
                                                            type: "info"
                                                        });
                                                        $rootScope.treeData.read();
                                                        $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                    }).error(function (error) {
                                                        options.error(error);
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Are you sure you want to remove the Item Association?",
                                type: "confirm",
                                buttons: [{ value: "Yes" }, { value: "No" }],
                                success: function (result) {
                                    var bool = false;
                                    if (result === "Yes") {
                                        bool = true;
                                    }
                                    if (bool === true) {
                                        dataFactory.DeleteProductPermanent($scope.menuFamilyID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.menuProductID).success(function (response) {
                                            //alert(response);
                                            $rootScope.deleteProduct = true;
                                            $.msgBox({
                                                title: $localStorage.ProdcutTitle,
                                                content: '' + response + '.',
                                                type: "info"
                                            });
                                            $rootScope.treeData.read();
                                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    }

                                }

                            });
                        }
                    });
                });
            }
        };
        $scope.DeleteProductItem = function (selectedRow) {
            if (selectedRow == "deleteDetach") {
                $scope.hideClick();
                $rootScope.SelectedNodeIDPresist = 1;
                $rootScope.selectedFAmilyList = $rootScope.SelectedNodeID;
                $scope.selectedFamilyID = $scope.Family_ID;
                $scope.menuFamilyID = $scope.Family_ID;
                $scope.menuProductID = $scope.menuProductIds;

            }
            else {
                $scope.hideClick();
                $rootScope.SelectedNodeIDPresist = 1;
                $rootScope.selectedFAmilyList = $rootScope.SelectedNodeID;
                $scope.selectedFamilyID = selectedRow.Data.FAMILY_ID;
                $scope.menuFamilyID = selectedRow.Data.FAMILY_ID;
                $scope.menuProductID = selectedRow.Data.PRODUCT_ID;

            }

            dataFactory.getdefaultfamilyexists($localStorage.getCatalogID).success(function (response) {
                $scope.defaultfamily = response;
                if (response == 0) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Kindly Select the default Product.',
                        type: "info"
                    });
                }

                if ($scope.selectedFamilyID !== "" && $scope.defaultfamily != "") {

                $(".menuProduct").hide();
                $http.get("../Catalog/GetCatalog?catalogId=" + selectedRow.Data.CATALOG_ID).then(function (objcategoryDetails) {
                    $scope.catalogDetails = objcategoryDetails.data[0];
                    
                        
                        
                        $http.get("../Catalog/GetProductExists?productId=" + selectedRow.Data.PRODUCT_ID + '&catalogId=' + selectedRow.Data.CATALOG_ID).then(function (objcategoryDetails) {
                            $scope.ProductCount = objcategoryDetails.data[0];
                            if ($scope.ProductCount == "1") {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: "Do you like to move this Item to Default Product?",
                                    type: "confirm",
                                    buttons: [{ value: "Yes" }, { value: "No" }],
                                    success: function (result) {
                                        var bool = false;
                                        if (result === "Yes") {
                                            bool = true;
                                        }
                                        if (bool == true) {
                                            dataFactory.moveProductDefaultFamily(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID, $scope.catalogDetails.DEFAULT_FAMILY).success(function (response) {
                                                if (response == 'Item Already Exist In Default Product.') {
                                                    $.msgBox({
                                                        title: $localStorage.ProdcutTitle,
                                                        content: "Item already exists in Default Product. Would you like to delete it from the source you copied it from?",
                                                        type: "confirm",
                                                        buttons: [{ value: "Yes" }, { value: "No" }],
                                                        success: function (result2) {
                                                            bool = false;
                                                            if (result2 === "Yes") {
                                                                bool = true;
                                                            }
                                                            if (bool === true) {
                                                                dataFactory.DeleteProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                                                    $.msgBox({
                                                                        title: $localStorage.ProdcutTitle,
                                                                        content: '' + response + '.',
                                                                        type: "info"
                                                                    });
                                                                    $rootScope.treeData.read();
                                                                    $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                                }).error(function (error) {
                                                                    options.error(error);
                                                                });
                                                            }
                                                        }
                                                    });
                                                } else {
                                                    $.msgBox({
                                                        title: $localStorage.ProdcutTitle,
                                                        content: '' + response + '.',
                                                        type: "info"
                                                    });
                                                    $rootScope.treeData.read();
                                                    $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID);
                                                }
                                            }).error(function (error) {
                                                options.error(error);
                                            });
                                        } else {
                                            $.msgBox({
                                                title: $localStorage.ProdcutTitle,
                                                content: "Are you sure you want to delete Item?",
                                                type: "confirm",
                                                buttons: [{ value: "Yes" }, { value: "No" }],
                                                success: function (result1) {
                                                    bool = false;
                                                    if (result1 === "Yes") {
                                                        bool = true;
                                                    }
                                                    if (bool === true) {
                                                        dataFactory.trashProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                                            $.msgBox({
                                                                title: $localStorage.ProdcutTitle,
                                                                content: '' + response + '.',
                                                                type: "info"
                                                            });
                                                            $rootScope.treeData.read();
                                                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                        }).error(function (error) {
                                                            options.error(error);
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                            else {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: "Are you sure you want to remove the Item Association?",
                                    type: "confirm",
                                    buttons: [{ value: "Yes" }, { value: "No" }],
                                    success: function (result) {
                                        var bool = false;
                                        if (result === "Yes") {
                                            bool = true;
                                        }
                                        if (bool === true) {
                                            dataFactory.DeleteProductPermanent($scope.menuFamilyID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.menuProductID).success(function (response) {
                                                //alert(response);
                                                $rootScope.deleteProduct = true;
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: '' + response + '.',
                                                    type: "info"
                                                });
                                                $rootScope.treeData.read();
                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                            }).error(function (error) {
                                                options.error(error);
                                            });
                                        }

                                    }

                                });
                            }

                        });

                });

            }

        });
        
        };


       


        $scope.catalogDetails = {
            CATALOG_NAME: '',
            CATALOG_ID: 0,
            VERSION: '1.0',
            DESCRIPTION: 'Sample Catalog',
            FAMILY_FILTERS: '',
            PRODUCT_FILTERS: '',
            familyfilter: [],
            productfilter: [],
            DEFAULT_FAMILY: ''
        };
        $scope.DeleteProductItemFromMaster = function (selectedRow) {
            $rootScope.deleteProduct = true;
            $scope.hideClick();
            $scope.selectedFamilyID = selectedRow.Data.FAMILY_ID;
            $scope.selectedCatalogID = selectedRow.Data.CATALOG_ID;
            if ($scope.selectedFamilyID !== "") {
                $http.get("../Catalog/GetCatalog?catalogId=" + $scope.selectedCatalogID).then(function (objcategoryDetails) {
                    $scope.catalogDetails = objcategoryDetails.data[0];
                        if ($scope.catalogDetails.DEFAULT_FAMILY != "" && $scope.catalogDetails.DEFAULT_FAMILY != selectedRow.Data.FAMILY_ID) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Do you like to move this Item to Default Product?",
                                type: "confirm",
                                buttons: [{ value: "Yes" }, { value: "No" }],
                                success: function (result) {
                                    var bool = false;
                                    if (result === "Yes") {
                                        bool = true;
                                    }
                                    if (bool == true) {
                                        dataFactory.moveProductDefaultFamily(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID, $scope.catalogDetails.DEFAULT_FAMILY).success(function (response) {
                                            if (response == 'Item Already Exist In Default Product.') {
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: "Item already exists in Default Product. Would you like to delete it from the source you copied it from?",
                                                    type: "confirm",
                                                    buttons: [{ value: "Yes" }, { value: "No" }],
                                                    success: function (result2) {
                                                        bool = false;
                                                        if (result2 === "Yes") {
                                                            bool = true;
                                                        }
                                                        if (bool === true) {
                                                            dataFactory.DeleteProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                                                //  alert(response);
                                                                $.msgBox({
                                                                    title: $localStorage.ProdcutTitle,
                                                                    content: '' + response + '.',
                                                                    type: "info"
                                                                });
                                                                $rootScope.treeData.read();
                                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                            }).error(function (error) {
                                                                options.error(error);
                                                            });
                                                        }
                                                    }
                                                });
                                            } else{
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: '' + response + '.',
                                                    type: "info"
                                                });
                                                $rootScope.treeData.read();
                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID);
                                            }
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    } else {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: "Are you sure you want to delete Item?",
                                            type: "confirm",
                                            buttons: [{ value: "Yes" }, { value: "No" }],
                                            success: function (result1) {
                                                bool = false;
                                                if (result1 === "Yes") {
                                                    bool = true;
                                                }
                                                if (bool === true) {
                                                    dataFactory.DeleteProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                                        // alert(response);
                                                        $.msgBox({
                                                            title: $localStorage.ProdcutTitle,
                                                            content: '' + response + '.',
                                                            type: "info"
                                                        });
                                                        //var count = parseInt($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[1].replace(')', '').trim()) - 1;
                                                        //var selectedValue = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.replace($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[1].replace(')', '').trim(), count);
                                                        //$("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget.find($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[0].trim()).context.innerHTML = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget.find($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[0].trim()).context.innerHTML.replace($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText, selectedValue)

                                                        $rootScope.treeData.read();
                                                        $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                    }).error(function (error) {
                                                        options.error(error);
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        } else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Are you sure you want to delete Item?",
                                type: "confirm",
                                buttons: [{ value: "Yes" }, { value: "No" }],
                                success: function (result) {
                                    var bool = false;
                                    if (result === "Yes") {
                                        bool = true;
                                    }

                                    if (bool === true) {
                                        dataFactory.DeleteProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                            // alert(response);
                                            $.msgBox({
                                                title: $localStorage.ProdcutTitle,
                                                content: '' + response + '.',
                                                type: "info"
                                            });
                                            //var count = parseInt($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[1].replace(')', '').trim()) - 1;
                                            //var selectedValue = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.replace($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[1].replace(')', '').trim(), count);
                                            //$("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget.find($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[0].trim()).context.innerHTML = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget.find($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[0].trim()).context.innerHTML.replace($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText, selectedValue)

                                            $rootScope.treeData.read();
                                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                            // $scope.LoadMainProducts();
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    }
                                }
                           
                            });
                        }
                    });

            }
        };
        $scope.trashProductFromMaster = function (selectedRow) {
            $rootScope.deleteProduct = true;
            $rootScope.SelectedNodeIDPresist = 1;
            $rootScope.selectedFAmilyList = $rootScope.SelectedNodeID;
            $scope.hideClick();
            $scope.selectedFamilyID = selectedRow.Data.FAMILY_ID;
            $scope.selectedCatalogID = selectedRow.Data.CATALOG_ID;
            if ($scope.selectedFamilyID !== "") {
                $http.get("../Catalog/GetCatalog?catalogId=" + $scope.selectedCatalogID).
                    then(function (objcategoryDetails) {
                        $scope.catalogDetails = objcategoryDetails.data[0];

                        if ($scope.catalogDetails.DEFAULT_FAMILY != "" && $scope.catalogDetails.DEFAULT_FAMILY != selectedRow.Data.FAMILY_ID) {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Do you like to move this Item to Default Product?",
                                type: "confirm",
                                buttons: [{ value: "Yes" }, { value: "No" }],
                                success: function (result) {
                                    var bool = false;
                                    if (result === "Yes") {
                                        bool = true;
                                    }
                                    if (bool == true) {

                                        dataFactory.moveProductDefaultFamily(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID, $scope.catalogDetails.DEFAULT_FAMILY).success(function (response) {
                                            if (response == 'Item Already Exist In Default Product.') {
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: "Item already exists in Default Product. Would you like to delete it from the source you copied it from?",
                                                    type: "confirm",
                                                    buttons: [{ value: "Yes" }, { value: "No" }],
                                                    success: function (result2) {
                                                        bool = false;
                                                        if (result2 === "Yes") {
                                                            bool = true;
                                                        }
                                                        if (bool === true) {
                                                            dataFactory.trashProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                                                //  alert(response);
                                                                $.msgBox({
                                                                    title: $localStorage.ProdcutTitle,
                                                                    content: '' + response + '.',
                                                                    type: "info"
                                                                });
                                                                $rootScope.treeData.read();
                                                                $scope.attributes = [];
                                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                            }).error(function (error) {
                                                                options.error(error);
                                                            });
                                                        }
                                                    }
                                                });
                                            } else {
                                                // alert(response);
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: '' + response + '.',
                                                    type: "info"
                                                });
                                                $rootScope.treeData.read();
                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID);
                                            }
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    } else {

                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: "Do you like to delete this Item?", // move to Recycle Bin
                                            type: "confirm",
                                            buttons: [{ value: "Yes" }, { value: "No" }],
                                            success: function (result1) {
                                                bool = false;
                                                if (result1 === "Yes") {
                                                    bool = true;
                                                }
                                                if (bool === true) {

                                                    dataFactory.trashProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                                        // alert(response);
                                                        $.msgBox({
                                                            title: $localStorage.ProdcutTitle,
                                                            content: '' + response + '.',
                                                            type: "info"
                                                        });
                                                        $rootScope.treeData.read();
                                                        $scope.attributes = [];
                                                        $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                    }).error(function (error) {
                                                        options.error(error);
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        } else {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Do you like to delete this Item?", // move to Recycle Bin
                                type: "confirm",
                                buttons: [{ value: "Yes" }, { value: "No" }],
                                success: function (result) {
                                    var bool = false;
                                    if (result === "Yes") {
                                        bool = true;
                                    }

                                    if (bool === true) {

                                        dataFactory.trashProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                            // alert(response);

                                            $.msgBox({
                                                title: $localStorage.ProdcutTitle,
                                                content: '' + response + '.',
                                                type: "info"
                                            });
                                            $rootScope.treeData.read();
                                            $scope.attributes = [];
                                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                            // $scope.LoadMainProducts();
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    }
                                }
                            });
                        }
                    });


            };
        };

        $scope.trashProductItemFromMaster = function (selectedRow) {
            $rootScope.deleteProduct = true;
            $rootScope.SelectedNodeIDPresist = 1;
            $rootScope.selectedFAmilyList = $rootScope.SelectedNodeID;
            $scope.hideClick();
            $scope.selectedFamilyID = selectedRow.Data.FAMILY_ID;
            $scope.selectedCatalogID = selectedRow.Data.CATALOG_ID;
            if ($scope.selectedFamilyID !== "") {
                $http.get("../Catalog/GetCatalog?catalogId=" + $scope.selectedCatalogID).
                    then(function (objcategoryDetails) {
                        $scope.catalogDetails = objcategoryDetails.data[0];

                        if ($scope.catalogDetails.DEFAULT_FAMILY != "" && $scope.catalogDetails.DEFAULT_FAMILY != selectedRow.Data.FAMILY_ID) {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Do you like to move this Item to Default Product?",
                                type: "confirm",
                                buttons: [{ value: "Yes" }, { value: "No" }],
                                success: function (result) {
                                    var bool = false;
                                    if (result === "Yes") {
                                        bool = true;
                                    }
                                    if (bool == true) {

                                        dataFactory.moveProductDefaultFamily(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID, $scope.catalogDetails.DEFAULT_FAMILY).success(function (response) {
                                            if (response == 'Item Already Exist In Default Product.') {
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: "Item already exists in Default Product. Would you like to delete it from the source you copied it from?",
                                                    type: "confirm",
                                                    buttons: [{ value: "Yes" }, { value: "No" }],
                                                    success: function (result2) {
                                                        bool = false;
                                                        if (result2 === "Yes") {
                                                            bool = true;
                                                        }
                                                        if (bool === true) {
                                                            dataFactory.trashProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                                                //  alert(response);
                                                                $.msgBox({
                                                                    title: $localStorage.ProdcutTitle,
                                                                    content: '' + response + '.',
                                                                    type: "info"
                                                                });
                                                                $rootScope.treeData.read();
                                                                $scope.attributes = [];
                                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                            }).error(function (error) {
                                                                options.error(error);
                                                            });
                                                        }
                                                    }
                                                });
                                            } else {
                                                // alert(response);
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: '' + response + '.',
                                                    type: "info"
                                                });
                                                $rootScope.treeData.read();
                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID);
                                            }
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    } else {

                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: "Do you like to delete this Item?", // move to Recycle Bin
                                            type: "confirm",
                                            buttons: [{ value: "Yes" }, { value: "No" }],
                                            success: function (result1) {
                                                bool = false;
                                                if (result1 === "Yes") {
                                                    bool = true;
                                                }
                                                if (bool === true) {

                                                    dataFactory.trashProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                                        // alert(response);
                                                        $.msgBox({
                                                            title: $localStorage.ProdcutTitle,
                                                            content: '' + response + '.',
                                                            type: "info"
                                                        });
                                                        $rootScope.treeData.read();
                                                        $scope.attributes = [];
                                                        $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                    }).error(function (error) {
                                                        options.error(error);
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        } else {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Do you like to delete this Item?", // move to Recycle Bin
                                type: "confirm",
                                buttons: [{ value: "Yes" }, { value: "No" }],
                                success: function (result) {
                                    var bool = false;
                                    if (result === "Yes") {
                                        bool = true;
                                    }

                                    if (bool === true) {

                                        dataFactory.trashProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.PRODUCT_ID).success(function (response) {
                                            // alert(response);

                                            $.msgBox({
                                                title: $localStorage.ProdcutTitle,
                                                content: '' + response + '.',
                                                type: "info"
                                            });
                                            $rootScope.treeData.read();
                                            $scope.attributes = [];
                                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                            // $scope.LoadMainProducts();
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    }
                                }
                            });
                        }
                    });


            };
        };



        $scope.DeleteSubProductItemFromMaster = function (selectedRow) {
            $rootScope.deleteProduct = true;
            $scope.selectedFamilyID = selectedRow.Data.FAMILY_ID;
            $scope.selectedCatalogID = selectedRow.Data.CATALOG_ID;
            if ($scope.selectedFamilyID !== "") {
                $http.get("../Catalog/GetCatalog?catalogId=" + $scope.selectedCatalogID).
                    then(function (objcategoryDetails) {
                        $scope.catalogDetails = objcategoryDetails.data[0];
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "Do you want to delete Sub Product?",
                            type: "confirm",
                            buttons: [{ value: "Yes" }, { value: "No" }],
                            success: function (result) {
                                var bool = false;
                                if (result === "Yes") {
                                    bool = true;
                                }

                                if (bool === true) {
                                    dataFactory.DeleteSubProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.SUBPRODUCT_ID).success(function (response) {
                                        // alert(response);
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + response + '.',
                                            type: "info"
                                        });
                                        $rootScope.treeData.read();
                                        $scope.LoadMainProducts();

                                        dataFactory.Getsubproductspecs($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId, $scope.ProdCurrentPageSP, $scope.ProdCountPerPageSP).success(function (response) {
                                            var obj = jQuery.parseJSON(response.Data.Data);
                                            $rootScope.SubprodData = obj;



                                            $rootScope.columnsSubAtt = response.Data.Columns;
                                            angular.forEach($rootScope.SubprodData, function (value) {
                                                value.SORT = parseFloat(value.SORT);
                                            });
                                            if ($rootScope.SubprodData.length == 0) {
                                                if ($scope.ProdCurrentPageSP != null && $scope.ProdCurrentPageSP != "") {
                                                    var pgno = parseInt($scope.ProdCurrentPageSP);
                                                    $rootScope.callProductGridPagingSP('PREV');

                                                }

                                            }
                                            // $rootScope.tblDashBoardss.reload();
                                            $('#ColumnSetupSubproducts').hide();
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    }).error(function (error) {
                                        options.error(error);
                                    });
                                }
                            }
                        });
                    });


            }


        };

        $scope.trashSubProductItemFromMaster = function (selectedRow) {
            $rootScope.deleteProduct = true;
            $scope.selectedFamilyID = selectedRow.Data.FAMILY_ID;
            $scope.selectedCatalogID = selectedRow.Data.CATALOG_ID;
            $rootScope.ParentProductId = $scope.ParentProductId;
            if ($scope.selectedFamilyID !== "") {
                $http.get("../Catalog/GetCatalog?catalogId=" + $scope.selectedCatalogID).
                    then(function (objcategoryDetails) {
                        $scope.catalogDetails = objcategoryDetails.data[0];
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "Do you like to delete this SubProduct?",// move to Recycle Bin
                            type: "confirm",
                            buttons: [{ value: "Yes" }, { value: "No" }],
                            success: function (result) {
                                var bool = false;
                                if (result === "Yes") {
                                    bool = true;
                                }

                                if (bool === true) {
                                    dataFactory.trashSubProductPermanentFromMaster(selectedRow.Data.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.Data.SUBPRODUCT_ID).success(function (response) {
                                        // alert(response);
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + response + '.',
                                            type: "info"
                                        });
                                        //$rootScope.treeData.read();
                                        //$scope.LoadMainProducts();
                                        dataFactory.Getsubproductspecs($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId, $scope.ProdCurrentPageSP, $scope.ProdCountPerPageSP).success(function (response) {

                                            var obj = jQuery.parseJSON(response.Data.Data);
                                            $rootScope.SubprodData = obj;

                                            $rootScope.columnsSubAtt = response.Data.Columns;
                                            angular.forEach($rootScope.SubprodData, function (value) {
                                                value.SORT = parseFloat(value.SORT);
                                            });
                                            // $rootScope.tblDashBoardss.reload();
                                            $rootScope.navigateprodProductTab = false;

                                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, selectedRow.Data.FAMILY_ID, $rootScope.selecetedCatalogId);
                                            //$rootScope.treeData.read();
                                            $scope.LoadMainProducts();
                                            $('#ColumnSetupSubproducts').hide();
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    }).error(function (error) {
                                        options.error(error);
                                    });
                                }
                            }
                        });
                    });


            };

        };


        $scope.CheckAll = function (e, f) {


            if (e.currentTarget.checked === true) {
                $rootScope.valueofcheckbox = true;
                $scope.BulkIds = f;
                angular.forEach($scope.BulkIds, function (key) {
                    $scope.product_ids = $scope.product_ids + "," + key.PRODUCT_ID;
                    $scope.Family_ID = key.FAMILY_ID;
                });




                var res = $scope.product_ids.split(",");

                for (var i = 0; i < res.length; i++) {
                    if (res[i] == "") {
                        res.splice(i, 1)
                    }
                }

                for (var i = 0; i < res.length; i++) {
                    DeleteValues.push(res[i]);
                }

                $('#divProductGrid2').hide();
                $('#divProductGrid').show();
                //$("#divProductGrid").addClass("col-sm-3");
                //$("#divProductGrid").removeClass("col-sm-12");
                $('#selectedattribute').hide();
                $('#panel').show();
                $("#attributePack").hide();



                $('.check').prop("checked", true);

            } else {
                DeleteValues = [];
                $scope.product_ids = "";
                $rootScope.valueofcheckbox = false;
                $rootScope.valueofcheckall = false;
                $scope.CancelMulipleAttributeProducts();
                $('.check').prop("checked", false);
                $("#divProductGrid").removeClass("display-none");
                $("#divProductGrid").addClass("display-block");
                //  $("#check").attr("checked", "checked");
            }

            /*----------multiple product_ids-------------*/

            for (var i in res) {

                $scope.detachProductId.push(parseInt(res[i]));
            }

            if (e.currentTarget.checked === false) {
                if ($scope.detachProductId.lastIndexOf(res)) {
                    $scope.detachProductId = $scope.detachProductId.splice(res);
                }
            }
        };

        $scope.selectedId = function (product_Id, e, family_Id) {
            $scope.CheckedProductids = true;
            if ($scope.CheckedCheckbox == true) {
                $scope.product_ids = "";
            }
            if (e.currentTarget.checked === true) {
                $scope.CheckedCheckbox = false;
                $("#copy").show();
                $("#cut").show();
                $("#paste").show();
                $scope.Family_ID = family_Id;
                $scope.product_ids = $scope.product_ids + "," + product_Id;



            } else if (e.currentTarget.checked === false) {
                // document.getElementById("checkall").checked = e.currentTarget.checked;
                $scope.CheckedCheckbox = false;
                $rootScope.valueofcheckall = false;
                var val = $scope.product_ids;
                var res = val.split(",");
                var Product_IDs = $scope.product_ids;
                if (res.length > 0) {
                    for (var i = 0; i < res.length; i++) {
                        if (res[i] === product_Id) {
                            Product_IDs = $scope.product_ids.replace(product_Id, "");
                        }
                    }
                }
                Product_IDs = Product_IDs.replace(",,", ",");
                $scope.product_ids = Product_IDs.replace(",", "");

            } else {
                $("#copy").hide();
                $("#cut").hide();
                $("#paste").hide();
            }
        };
        // Check box  -- Start


        var DeleteValues = [];
        $scope.CheckButtonFunction = function (value, product_Id,f) {

            if (DeleteValues == "") {
                DeleteValues = [];
            }
            if (f != undefined)
            {
                $scope.BulkIds = f.length;
            }
            

            if (value) {
                DeleteValues.push(product_Id);


                if ($scope.BulkIds == DeleteValues.length) {
                    $('.checkall').prop("checked", true);
                }



            } else {
                DeleteValues.pop();
                $('.checkall').prop("checked", false);
            }
        }



        // Check box -- End

        //mv
        ////////////////========================================================================================================================================================================================
        $scope.selectedId1 = function (product_Id, e, family_Id, clickedData,f) {

            $scope.CheckButtonFunction(e.currentTarget.checked, product_Id,f);

            if (e.currentTarget.checked === true) {
                $scope.detachProductId.push(product_Id);
            }

            if (e.currentTarget.checked === false) {
                if ($scope.detachProductId.lastIndexOf(product_Id)) {
                    var index = $scope.detachProductId.indexOf(product_Id);
                    $scope.detachProductId = $scope.detachProductId.splice(index, 1);
                    //  $scope.detachProductId = $scope.detachProductId.splice(product_Id,1);
                }
                // $scope.productids = [];
                // $scope.productids = $scope.product_ids.split(",");
                ////  $scope.productids.trim();
                // var position = $scope.productids.indexOf(product_Id);
                // $scope.productids = $scope.productids.splice(position, 1);
                // $scope.product_ids = "";

            }



            $("#selectedattributeEdit").hide();
            $('.pull-right').hide();

            $scope.flag = 0;
            var GetData = 0;



            if (e.currentTarget.checked === true) {

                $scope.Family_ID = family_Id;
                $scope.product_ids = $scope.product_ids + "," + product_Id;

                var index = 0;
                var c = $scope.product_ids.split(",");

                if (c.length > 0) {
                    for (var i = 0; i < c.length; i++) {
                        // if (c[i] == "") {

                        var index = c.indexOf("");
                        if (index > -1) {
                            c.splice(index, 1);
                            //  }


                        }
                    }
                }
                selectedCount = c.length;
                if (c.length == 1) {
                    $('#divProductGrid').addClass("col-sm-3");
                    $('#divProductGrid').removeClass("col-sm-12");
                    $('#selectedattribute').addClass("col-sm-8");
                    $('#selectedattribute').removeClass("col-sm-7");

                    $('#selectedattribute').addClass("ml-35");

                    $('#divProductGrid1').hide();
                    $('#panel').hide();
                    $('#panel1').hide();
                    $('#selectedattribute').show();
                    $("#selectedattributeEdit").hide();
                    $scope.panel = false;
                    //$("#divProductGrid").addClass("display-none");
                    //$("#divProductGrid").removeClass("display-block");
                } else {
                    $scope.panel = true;
                    $('#divProductGrid2').show();
                    $('#selectedattribute').hide();
                    $('#panel').show();
                    $('#panel1').show();
                    $("#attributePack").hide();
                    $("#panel").addClass("ml-35");
                }
                $("#attributePack").hide();
                $("#copy").show();
                $("#cut").show();
                $("#paste").show();

            } else
                if (e.currentTarget.checked === false) {
                    $("#divProductGrid").removeClass("display-none");
                    $("#divProductGrid").addClass("display-block");
                    // document.getElementById("checkall").checked = e.currentTarget.checked;
                    $scope.CheckedCheckbox = false;
                    $rootScope.valueofcheckall = false;
                    var val = $scope.product_ids;
                    var res = val.split(",");
                    var Product_IDs = $scope.product_ids;

                    if (res.length > 0) {
                        for (var i = 0; i < res.length; i++) {
                            if (res[i] == product_Id) {
                                Product_IDs = $scope.product_ids.replace(product_Id, "");
                            }
                            if (res[i].length > 8) {
                                Product_IDs = $scope.product_ids.replace(res[i], "");
                            }
                        }
                    }
                    Product_IDs = Product_IDs.replace(",,", ",");
                    $scope.product_ids = Product_IDs;//.replace(",", "");

                    if ($scope.product_ids != "") {
                        var index = 0;
                        var c = $scope.product_ids.split(",");
                        if (c.length > 0) {
                            for (var i = 0; i < c.length; i++) {

                                index = c.indexOf("");


                                if (index > -1) {
                                    c.splice(index, 1);
                                }
                                if (c == "") {
                                    c = [];
                                }

                            }
                        }
                    } else {
                        c = [];
                    }
                    selectedCount = c.length;

                    if (c.length == 0) {
                        $('#selectedattribute').hide();
                        $('#panel').hide();
                        //$("#divProductGrid").removeClass("col-sm-2");
                        //$("#divProductGrid").addClass("col-sm-12");

                        $('.pull-right').show();

                    } else if (c.length == 1) {
                        product_Id = c[0];
                        $('#divProductGrid1').show();
                        $('#panel').hide();
                        $('#selectedattribute').show();
                        $("#selectedattributeEdit").hide();
                        $("#panel").addClass("ml-35");
                        $("#divProductGrid").addClass("col-sm-12");
                        $("#divProductGrid").removeClass("col-sm-3");
                    } else if (c.length == 2 || c.length > 2) {
                        $('#divProductGrid2').show();
                        $('#selectedattribute').hide();
                        $('#panel').show();
                        $("#divProductGrid").addClass("col-sm-3");
                        $("#divProductGrid").removeClass("col-sm-12");
                    }

                }





            //product_Id = GetData;




            // Load
            //MV
            if ($scope.prodData.length > 0) {
                for (var i = 0; i < $scope.prodData.length; i++) {
                    if ($scope.prodData[i].PRODUCT_ID == product_Id) {
                        data = i;
                    }
                }
            } else {
                data = 0;
            }

            attributeWithout = [];
            $scope.attributesOLD = Object.keys($scope.prodData[data]);
            dataFactory.GetAttributeDetails($scope.prodData[data].CATALOG_ID, $scope.prodData[data].FAMILY_ID).success(function (response) {

                if (response != null) {
                    $scope.selectedRowAttributes = response;
                    var calcAttributestemp = [];
                    angular.forEach($scope.attributesOLD, function (value, key) {
                        if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2') || value == 'WORKFLOW STATUS')
                            attributeWithout.push(value);
                    });
                    $scope.attributescalc = attributeWithout;
                    $scope.fromPageNo = 0;
                    $scope.toPageNo = 150;
                    if (attributeWithout.length < 150) {
                        $scope.toPageNo = attributeWithout.length;
                    }
                    var attributestemp = [];
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        attributestemp.push(attributeWithout[i]);
                    }
                    $scope.attributes = attributestemp;
                    $scope.attrLength = $scope.attributes.length;
                    $scope.ProdSpecsCountPerPage = "150";
                    $scope.ProdSpecsCurrentPage = "1";

                    $scope.loopCount = [];
                    $scope.loopEditProdsCount = [];
                    var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                    var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                    if ((attributeWithout.length % parseInt($scope.ProdSpecsCountPerPage)) > 0) {
                        loopcnt = loopcnt + 1;
                        loopEditProdsCnt = loopEditProdsCnt + 1;
                    }
                    $scope.ProdSpecsPageCount = $scope.loopCount;
                    $scope.totalSpecsProdPageCount = loopEditProdsCnt;
                    for (var i = 0; i < loopcnt; i++) {
                        if (((i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)) < attributeWithout.length) {
                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)
                            });
                        } else {

                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: attributeWithout.length
                            });
                        }
                    }
                    var theString;
                    $scope.selectedRowEdit = {};
                    angular.copy($scope.prodData[data], $scope.selectedRowEdit);
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].contains('__')) {
                            var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
                            if (tempattrvals.length != 0) {
                                var uimask = "";
                                theString = tempattrvals[0].ATTRIBUTE_ID;
                                tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                                if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                                    theString = 0;
                                }
                                var itemval = attributeWithout[i];
                                if (tempattrvals[0].ATTRIBUTE_ID === 1) {
                                    $scope.item_ = $scope.selectedRowEdit[itemval];
                                }
                                if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                                    var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                    pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                                    pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                                    var reg = new RegExp(pattern);
                                    uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                                    tempattrvals[0].attributePattern = reg;
                                } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                    tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                                    uimask = tempattrvals[0].DECIMAL;
                                } else {
                                    tempattrvals[0].attributePattern = 524288;
                                    uimask = 524288;
                                }
                                tempattrvals[0].uimask = uimask;
                                $scope.selectedRowDynamicAttributes[theString] = [];
                                $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                            }
                        }
                    }
                    $scope.groups = [];
                    var picklistNAme = "";
                    var attrId = "";
                    $scope.iterationCount = 0;
                    $scope.isUIReleased = 0;
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].contains("OBJ")) {
                            if (attributeWithout[i].split('__')[2] !== "0") {
                                var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                                if (sa.USE_PICKLIST) {
                                    $scope.iterationCount++;
                                }
                            }
                        }
                    }
                    // $rootScope.selectedRowDynamic = [];

                    $scope.iterationPicklistCount = 0;
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].contains("OBJ")) {
                            if (attributeWithout[i].split('__')[2] !== "0") {
                                var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                                if (sa.USE_PICKLIST) {
                                    $scope.GetPickListData(sa.ATTRIBUTE_ID, sa.PICKLIST_NAME);
                                }
                            }
                        }
                    }


                    $scope.updateproductspecs = $scope.prodData[data]
                    $("#result").hide();

                    if ($scope.isUIReleased == 0) {

                        $scope.selectedRow = {};


                        angular.copy($scope.updateproductspecs, $scope.selectedRow);
                        // $localStorage.selectedRow1 = $scope.selectedRow;
                        $scope.ProductSortOrder.read();

                        angular.copy($scope.updateproductspecs, $scope.selectedRow);

                        //var ddl = $('#dynamicsort').data('kendoDropDownList');
                        //ddl.dataSource.data({}); // clears dataSource
                        //ddl.text(""); // clears visible text
                        //ddl.value("");

                        //  $rootScope.windows_Open();

                        blockUI.stop();




                        $rootScope.ajaxLoaderDivShow = false;

                        //$scope.selectedRow["SORT"] = data.$index + 1;
                        //$timeout(function () {
                        //    $scope.selectedRow["SORT"] = data.$index + 1;

                        //}, 50);

                        $timeout(function () {

                            blockUI.stop();
                        }, 100);
                        // $rootScope.ajaxLoaderDivShow = false;

                    }
                }
            }).error(function (error) {
                options.error(error);
            });



            //MV




            $scope.selectedRowValue = product_Id;
            $rootScope.selectedRowValue = product_Id;






            // End Load




            return true;


        };


        $scope.baseUndoProductFlag = 0;

        ////////////////========================================================================================================================================================================================


        ////////////////========================================================================================================================================================================================
        $rootScope.rowselectedEvent = function (product_Id, e, family_Id, clickedData) {

            $rootScope.getProduct_id = product_Id;
            $scope.PRODUCT_ID = family_Id;
            $rootScope.getEventValue = e;
            $rootScope.getFamily_id = family_Id;
            $rootScope.getclickedData = clickedData;
            $scope.workflowProductDataSource.read();
            $('#filterAttributePack').show();
            $scope.CheckedProductids = true;
            if ($scope.CheckedCheckbox == true) {
                $scope.product_ids = "";
            }
            $rootScope.undoProductFlag = "0";
            $("#copy").hide();
            $("#cut").hide();
            $("#paste").hide();
            $('#divProductGrid1').hide();

            //New UI Changes For 10.9.3
            $("#mainProductsMenu").hide();
            $("#filterAttributePack").hide();
            $("#filterAttributePackLeft").hide();
            $("#productpaging1").hide();
            $("#productpaging").hide();
            $("#divProductGrid").addClass("col-sm-3");
            $("#divProductGrid").removeClass("col-sm-12");
            $("#selectedattribute").addClass("col-sm-8");
            $("#selectedattribute").addClass("ml-35");
            $("#selectedattribute").removeClass("col-sm-7");
           // $("#selectedattribute").addClass("block");

            if ($scope.product_ids != '') {
                var c = $scope.product_ids.split(",");

                for (var i = 0; i < c.length; i++) {
                    if (c[i] == "") {

                        var index = c.indexOf(c[i]);
                        if (index > -1) {
                            c.splice(index, 1);
                        }

                    }
                }

                if (c.length > 2) {
                    $('#panel').show();
                    $('#selectedattribute').hide();
                    $('#selectedattributeitem').hide();
                }

            }
            else {
                $scope.selectedattribute = true;
                $('#panel').hide();
                $("#selectedattribute").show();
                $("#selectedattributeitem").show();
                $("#selectedattributeEdit").hide();
                $("#selectedattributenoresultfound").hide();
            }





            if ($scope.prodData.length > 0) {
                for (var i = 0; i < $scope.prodData.length; i++) {
                    if ($scope.prodData[i].PRODUCT_ID == product_Id) {
                        data = i;
                    }
                }
            } else {
                data = 0;
            }

            attributeWithout = [];
            var pack_Id = 0;
            if ($rootScope.ProdPage == "1") {
                $scope.ProdCurrentPage = "1";
            }
            else {
                $scope.ProdCurrentPage = $rootScope.ProdPage;
            }
            var Type = null;
            var Attributes = null;
            var productGridSearchValue = null;
            var searchType = null;
            var ProdSpecsValues = true;
            var ProdPriceValues = true;
            var ProdKeyValues = true;
            var ProdImageValues = true;
            dataFactory.getprodspecsattributes($scope.prodData[data].CATALOG_ID, $scope.prodData[data].FAMILY_ID, true, $rootScope.cat_id, $scope.ProdCurrentPage, $scope.ProdCountPerPage, $scope.sub_id, pack_Id, Type, Attributes, productGridSearchValue, searchType, ProdSpecsValues, ProdPriceValues, ProdKeyValues, ProdImageValues, $rootScope.val).success(function (response) {
                if (response != null) {
                    var productDetails = jQuery.parseJSON(response.Data.Data);
                    var obj = productDetails.Table;
                    $scope.prodData1 = obj;
                    $scope.attributesOLD = Object.keys($scope.prodData1[data]);
                    dataFactory.GetAttributeDetails($scope.prodData[data].CATALOG_ID, $scope.prodData[data].FAMILY_ID).success(function (response) {
                        if (response != null) {
                            $scope.selectedRowAttributes = response;

                            var calcAttributestemp = [];
                            angular.forEach($scope.attributesOLD, function (value, key) {
                                if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2') || value == 'WORKFLOW STATUS')
                                    attributeWithout.push(value);
                            });

                            $scope.attributescalc = attributeWithout;
                            $scope.fromPageNo = 0;
                            $scope.toPageNo = 150;
                            if (attributeWithout.length < 150) {
                                $scope.toPageNo = attributeWithout.length;
                            }
                            var attributestemp = [];
                            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                                attributestemp.push(attributeWithout[i]);
                            }


                            $scope.attributes = attributestemp;

                            $scope.AllAttributeValues = $scope.attributes;
                            // To bind top10 vaules in the Grid 
                            $scope.attributesValuesForEdit = [];
                            $scope.ProdEditPageCount = 10;

                            for (var i = 0; i < $scope.ProdEditPageCount; i++) {
                                if ($scope.attributes[i] != undefined)
                                    $scope.attributesValuesForEdit.push($scope.attributes[i])
                            }
                            $scope.attributes = $scope.attributesValuesForEdit;

                            // End


                            $scope.attrLength = $scope.attributes.length;
                            $scope.ProdSpecsCountPerPage = "150";
                            $scope.ProdSpecsCurrentPage = "1";

                            $scope.loopCount = [];
                            $scope.loopEditProdsCount = [];
                            var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                            var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                            if ((attributeWithout.length % parseInt($scope.ProdSpecsCountPerPage)) > 0) {
                                loopcnt = loopcnt + 1;
                                loopEditProdsCnt = loopEditProdsCnt + 1;
                            }
                            $scope.ProdSpecsPageCount = $scope.loopCount;
                            $scope.totalSpecsProdPageCount = loopEditProdsCnt;
                            for (var i = 0; i < loopcnt; i++) {
                                if (((i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)) < attributeWithout.length) {
                                    $scope.loopCount.push({
                                        PAGE_NO: (i + 1),
                                        FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                                        TO_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)
                                    });
                                } else {

                                    $scope.loopCount.push({
                                        PAGE_NO: (i + 1),
                                        FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                                        TO_PAGE_NO: attributeWithout.length
                                    });
                                }
                            }
                            var theString;
                            $scope.selectedRowEdit = {};
                            angular.copy($scope.prodData1[data], $scope.selectedRowEdit);
                            $rootScope.selectedProductAttributeRow = $scope.selectedRowEdit;

                            //////////////getting base undo product value ///////////////////////////////////////////////////////////////////
                            if ($localStorage.baseUndoOnlyProductDetails.length == 0) {
                                $scope.copyselectedRowEdit = angular.copy($scope.selectedRowEdit);
                                $localStorage.baseUndoOnlyProductDetails.push($scope.copyselectedRowEdit);
                            }
                            else {
                                if ($localStorage.baseUndoOnlyProductDetails.PRODUCT_ID != $scope.selectedRowEdit.PRODUCT_ID) {
                                    $scope.copyselectedRowEdit = angular.copy($scope.selectedRowEdit);
                                    $localStorage.baseUndoOnlyProductDetails.push($scope.copyselectedRowEdit);
                                }
                            }

                            //////////////getting base undo product value ///////////////////////////////////////////////////////////////////s

                            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                                if (attributeWithout[i].contains('__')) {
                                    var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
                                    if (tempattrvals.length != 0) {
                                        var uimask = "";
                                        theString = tempattrvals[0].ATTRIBUTE_ID;
                                        tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                                        if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                                            theString = 0;
                                        }
                                        var itemval = attributeWithout[i];
                                        if (tempattrvals[0].ATTRIBUTE_ID === 1) {
                                            $scope.item_ = $scope.selectedRowEdit[itemval];
                                        }
                                        if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                                            var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                            pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                                            pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                                            var reg = new RegExp(pattern);
                                            uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                                            tempattrvals[0].attributePattern = reg;
                                        } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                            tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                                            uimask = tempattrvals[0].DECIMAL;
                                        } else {
                                            tempattrvals[0].attributePattern = 524288;
                                            uimask = 524288;
                                        }
                                        tempattrvals[0].uimask = uimask;
                                        $scope.selectedRowDynamicAttributes[theString] = [];
                                        $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                                    }
                                }
                            }
                            $scope.groups = [];
                            var picklistNAme = "";
                            var attrId = "";
                            $scope.iterationCount = 0;
                            $scope.isUIReleased = 0;
                            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                                if (attributeWithout[i].contains("OBJ")) {
                                    if (attributeWithout[i].split('__')[2] !== "0") {
                                        var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                                        if (sa.USE_PICKLIST) {
                                            $scope.iterationCount++;
                                        }
                                    }
                                }
                            }
                            // $rootScope.selectedRowDynamic = [];

                            $scope.iterationPicklistCount = 0;
                            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                                if (attributeWithout[i].contains("OBJ")) {
                                    if (attributeWithout[i].split('__')[2] !== "0") {
                                        var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                                        if (sa.USE_PICKLIST) {
                                            $scope.GetPickListData(sa.ATTRIBUTE_ID, sa.PICKLIST_NAME);
                                        }
                                    }
                                }
                            }


                            $scope.updateproductspecs = $scope.prodData1[data]
                            $("#result").hide();
                            if ($rootScope.val == false) {
                                document.getElementById('myButton1').innerText = "Show More"


                            }
                            else {

                                document.getElementById('myButton1').innerText = "Show Less"

                            }
                            //  if ($scope.isUIReleased == 0) {

                            $scope.selectedRow = {};

                            angular.copy($scope.updateproductspecs, $scope.selectedRow);
                            blockUI.stop();

                            angular.copy($scope.updateproductspecs, $scope.selectedRow);
                            blockUI.stop();
                            $rootScope.ajaxLoaderDivShow = false;
                            
                            
                        }
                    });
                }

            }).error(function (error) {
                options.error(error);
            });

            $scope.selectedRowValue = product_Id;
            $rootScope.selectedRowValue = product_Id;


        };

        //Undo product function
        $rootScope.rowselectedEventForUndo = function (product_Id, family_Id, clickedData) {
            $('.pull-right').hide();

            $scope.CheckedProductids = true;
            if ($scope.CheckedCheckbox == true) {
                $scope.product_ids = "";
            }

            $("#copy").hide();
            $("#cut").hide();
            $("#paste").hide();

            $('#divProductGrid1').hide();


            //Mariya
            if ($scope.product_ids != '') {
                var c = $scope.product_ids.split(",");

                for (var i = 0; i < c.length; i++) {
                    if (c[i] == "") {

                        var index = c.indexOf(c[i]);
                        if (index > -1) {
                            c.splice(index, 1);
                        }

                    }
                }

                if (c.length > 2) {
                    $('#panel').show();
                    $('#selectedattribute').hide();
                }
            } else {


                $('#panel').hide();
                $('#selectedattribute').show();
                $("#selectedattributeEdit").hide();
            }





            $("#divProductGrid").addClass("col-sm-2");
            $("#divProductGrid").removeClass("col-sm-12");


            if ($scope.prodData.length > 0) {
                for (var i = 0; i < $scope.prodData.length; i++) {
                    if ($scope.prodData[i].PRODUCT_ID == product_Id) {
                        data = i;
                    }
                }
            } else {
                data = 0;
            }

            attributeWithout = [];
            $scope.prodData[data] = clickedData.productAttributes;
            $scope.attributesOLD = Object.keys($scope.prodData[data]);

            $scope.selectedRowAttributes = clickedData.baseProductRowAttributes;
            var calcAttributestemp = [];
            angular.forEach($scope.attributesOLD, function (value, key) {
                if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2') || value == 'WORKFLOW STATUS')
                    attributeWithout.push(value);
            });
            $scope.attributescalc = attributeWithout;
            $scope.fromPageNo = 0;
            $scope.toPageNo = 150;
            if (attributeWithout.length < 150) {
                $scope.toPageNo = attributeWithout.length;
            }
            var attributestemp = [];
            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                attributestemp.push(attributeWithout[i]);
            }


            $scope.attributes = attributestemp;

            $scope.AllAttributeValues = $scope.attributes;
            // To bind top10 vaules in the Grid 
            $scope.attributesValuesForEdit = [];
            $scope.ProdEditPageCount = 50;

            for (var i = 0; i < $scope.ProdEditPageCount; i++) {
                if ($scope.attributes[i] != undefined)
                    $scope.attributesValuesForEdit.push($scope.attributes[i])
            }
            $scope.attributes = $scope.attributesValuesForEdit;

            // End


            $scope.attrLength = $scope.attributes.length;
            $scope.ProdSpecsCountPerPage = "150";
            $scope.ProdSpecsCurrentPage = "1";

            $scope.loopCount = [];
            $scope.loopEditProdsCount = [];
            var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
            var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
            if ((attributeWithout.length % parseInt($scope.ProdSpecsCountPerPage)) > 0) {
                loopcnt = loopcnt + 1;
                loopEditProdsCnt = loopEditProdsCnt + 1;
            }
            $scope.ProdSpecsPageCount = $scope.loopCount;
            $scope.totalSpecsProdPageCount = loopEditProdsCnt;
            for (var i = 0; i < loopcnt; i++) {
                if (((i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)) < attributeWithout.length) {
                    $scope.loopCount.push({
                        PAGE_NO: (i + 1),
                        FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                        TO_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)
                    });
                } else {

                    $scope.loopCount.push({
                        PAGE_NO: (i + 1),
                        FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                        TO_PAGE_NO: attributeWithout.length
                    });
                }
            }
            var theString;
            $scope.selectedRowEdit = {};
            angular.copy($scope.prodData[data], $scope.selectedRowEdit);
            //   $rootScope.selectedProductAttributeRow = $scope.selectedRowEdit;
            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                if (attributeWithout[i].contains('__')) {
                    var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
                    if (tempattrvals.length != 0) {
                        var uimask = "";
                        theString = tempattrvals[0].ATTRIBUTE_ID;
                        tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                        if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                            theString = 0;
                        }
                        var itemval = attributeWithout[i];
                        if (tempattrvals[0].ATTRIBUTE_ID === 1) {
                            $scope.item_ = $scope.selectedRowEdit[itemval];
                        }
                        if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                            var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                            pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                            pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                            var reg = new RegExp(pattern);
                            uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                            tempattrvals[0].attributePattern = reg;
                        } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                            tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                            uimask = tempattrvals[0].DECIMAL;
                        } else {
                            tempattrvals[0].attributePattern = 524288;
                            uimask = 524288;
                        }
                        tempattrvals[0].uimask = uimask;
                        $scope.selectedRowDynamicAttributes[theString] = [];
                        $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                    }
                }
            }
            $scope.groups = [];
            var picklistNAme = "";
            var attrId = "";
            $scope.iterationCount = 0;
            $scope.isUIReleased = 0;
            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                if (attributeWithout[i].contains("OBJ")) {
                    if (attributeWithout[i].split('__')[2] !== "0") {
                        var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                        if (sa.USE_PICKLIST) {
                            $scope.iterationCount++;
                        }
                    }
                }
            }
            // $rootScope.selectedRowDynamic = [];

            $scope.iterationPicklistCount = 0;
            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                if (attributeWithout[i].contains("OBJ")) {
                    if (attributeWithout[i].split('__')[2] !== "0") {
                        var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                        if (sa.USE_PICKLIST) {
                            $scope.GetPickListData(sa.ATTRIBUTE_ID, sa.PICKLIST_NAME);
                        }
                    }
                }
            }


            $scope.updateproductspecs = $scope.prodData[data]
            $("#result").hide();

            if ($scope.isUIReleased == 0) {

                $scope.selectedRow = {};


                angular.copy($scope.updateproductspecs, $scope.selectedRow);
                // $localStorage.selectedRow1 = $scope.selectedRow;
                $scope.ProductSortOrder.read();

                angular.copy($scope.updateproductspecs, $scope.selectedRow);

                //var ddl = $('#dynamicsort').data('kendoDropDownList');
                //ddl.dataSource.data({}); // clears dataSource
                //ddl.text(""); // clears visible text
                //ddl.value("");

                //  $rootScope.windows_Open();

                blockUI.stop();




                $rootScope.ajaxLoaderDivShow = false;

                //$scope.selectedRow["SORT"] = data.$index + 1;
                //$timeout(function () {
                //    $scope.selectedRow["SORT"] = data.$index + 1;

                //}, 50);

                $timeout(function () {
                    // $scope.ResetEditClick();
                    blockUI.stop();
                }, 100);
                // $rootScope.ajaxLoaderDivShow = false;

            }



            $scope.selectedRowValue = product_Id;
            $rootScope.selectedRowValue = product_Id;


        };
        //mv

        $rootScope.rowselectedEventForProductUndoOnly = function (product_Id, family_Id, clickedData) {

            $('.pull-right').hide();

            $scope.CheckedProductids = true;
            if ($scope.CheckedCheckbox == true) {
                $scope.product_ids = "";
            }

            $("#copy").hide();
            $("#cut").hide();
            $("#paste").hide();

            $('#divProductGrid1').hide();


            //Mariya
            if ($scope.product_ids != '') {
                var c = $scope.product_ids.split(",");

                for (var i = 0; i < c.length; i++) {
                    if (c[i] == "") {

                        var index = c.indexOf(c[i]);
                        if (index > -1) {
                            c.splice(index, 1);
                        }

                    }
                }

                if (c.length > 2) {
                    $('#panel').show();
                    $('#selectedattribute').hide();
                }
            } else {


                $('#panel').hide();
                $('#selectedattribute').show();
                $("#selectedattributeEdit").hide();
            }





            $("#divProductGrid").addClass("col-sm-2");
            $("#divProductGrid").removeClass("col-sm-12");


            if ($scope.prodData.length > 0) {
                for (var i = 0; i < $scope.prodData.length; i++) {
                    if ($scope.prodData[i].PRODUCT_ID == product_Id) {
                        data = i;
                    }
                }
            } else {
                data = 0;
            }

            attributeWithout = [];
            $scope.prodData[data] = clickedData;
            $scope.attributesOLD = Object.keys($scope.prodData[data]);

            $scope.selectedRowAttributes = $scope.prodData;
            var calcAttributestemp = [];
            angular.forEach($scope.attributesOLD, function (value, key) {
                if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2') || value == 'WORKFLOW STATUS')
                    attributeWithout.push(value);
            });
            $scope.attributescalc = attributeWithout;
            $scope.fromPageNo = 0;
            $scope.toPageNo = 150;
            if (attributeWithout.length < 150) {
                $scope.toPageNo = attributeWithout.length;
            }
            var attributestemp = [];
            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                attributestemp.push(attributeWithout[i]);
            }


            $scope.attributes = attributestemp;

            $scope.AllAttributeValues = $scope.attributes;
            // To bind top10 vaules in the Grid 
            $scope.attributesValuesForEdit = [];
            $scope.ProdEditPageCount = 50;

            for (var i = 0; i < $scope.ProdEditPageCount; i++) {
                if ($scope.attributes[i] != undefined)
                    $scope.attributesValuesForEdit.push($scope.attributes[i])
            }
            $scope.attributes = $scope.attributesValuesForEdit;

            // End


            $scope.attrLength = $scope.attributes.length;
            $scope.ProdSpecsCountPerPage = "150";
            $scope.ProdSpecsCurrentPage = "1";

            $scope.loopCount = [];
            $scope.loopEditProdsCount = [];
            var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
            var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
            if ((attributeWithout.length % parseInt($scope.ProdSpecsCountPerPage)) > 0) {
                loopcnt = loopcnt + 1;
                loopEditProdsCnt = loopEditProdsCnt + 1;
            }
            $scope.ProdSpecsPageCount = $scope.loopCount;
            $scope.totalSpecsProdPageCount = loopEditProdsCnt;
            for (var i = 0; i < loopcnt; i++) {
                if (((i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)) < attributeWithout.length) {
                    $scope.loopCount.push({
                        PAGE_NO: (i + 1),
                        FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                        TO_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)
                    });
                } else {

                    $scope.loopCount.push({
                        PAGE_NO: (i + 1),
                        FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                        TO_PAGE_NO: attributeWithout.length
                    });
                }
            }
            var theString;
            $scope.selectedRowEdit = {};
            angular.copy($scope.prodData[data], $scope.selectedRowEdit);
            //   $rootScope.selectedProductAttributeRow = $scope.selectedRowEdit;
            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                if (attributeWithout[i].contains('__')) {
                    var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
                    if (tempattrvals.length != 0) {
                        var uimask = "";
                        theString = tempattrvals[0].ATTRIBUTE_ID;
                        tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                        if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                            theString = 0;
                        }
                        var itemval = attributeWithout[i];
                        if (tempattrvals[0].ATTRIBUTE_ID === 1) {
                            $scope.item_ = $scope.selectedRowEdit[itemval];
                        }
                        if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                            var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                            pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                            pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                            var reg = new RegExp(pattern);
                            uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                            tempattrvals[0].attributePattern = reg;
                        } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                            tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                            uimask = tempattrvals[0].DECIMAL;
                        } else {
                            tempattrvals[0].attributePattern = 524288;
                            uimask = 524288;
                        }
                        tempattrvals[0].uimask = uimask;
                        $scope.selectedRowDynamicAttributes[theString] = [];
                        $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                    }
                }
            }
            $scope.groups = [];
            var picklistNAme = "";
            var attrId = "";
            $scope.iterationCount = 0;
            $scope.isUIReleased = 0;
            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                if (attributeWithout[i].contains("OBJ")) {
                    if (attributeWithout[i].split('__')[2] !== "0") {
                        var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                        if (sa.USE_PICKLIST) {
                            $scope.iterationCount++;
                        }
                    }
                }
            }
            // $rootScope.selectedRowDynamic = [];

            $scope.iterationPicklistCount = 0;
            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                if (attributeWithout[i].contains("OBJ")) {
                    if (attributeWithout[i].split('__')[2] !== "0") {
                        var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                        if (sa.USE_PICKLIST) {
                            $scope.GetPickListData(sa.ATTRIBUTE_ID, sa.PICKLIST_NAME);
                        }
                    }
                }
            }


            $scope.updateproductspecs = $scope.prodData[data]
            $("#result").hide();

            if ($scope.isUIReleased == 0) {

                $scope.selectedRow = {};


                angular.copy($scope.updateproductspecs, $scope.selectedRow);
                // $localStorage.selectedRow1 = $scope.selectedRow;
                $scope.ProductSortOrder.read();

                angular.copy($scope.updateproductspecs, $scope.selectedRow);

                //var ddl = $('#dynamicsort').data('kendoDropDownList');
                //ddl.dataSource.data({}); // clears dataSource
                //ddl.text(""); // clears visible text
                //ddl.value("");

                //  $rootScope.windows_Open();

                blockUI.stop();




                $rootScope.ajaxLoaderDivShow = false;

                //$scope.selectedRow["SORT"] = data.$index + 1;
                //$timeout(function () {
                //    $scope.selectedRow["SORT"] = data.$index + 1;

                //}, 50);

                $timeout(function () {
                    // $scope.ResetEditClick();
                    blockUI.stop();
                }, 100);
                // $rootScope.ajaxLoaderDivShow = false;

            }

            $scope.$apply();
            $scope.$digest();

            $scope.selectedRowValue = product_Id;
            $rootScope.selectedRowValue = product_Id;


        };


        $scope.$on("oldCatids", function (event) {

            $scope.passcategoryid = $rootScope.familycategory;
            $scope.oldcatalogiddump = $rootScope.WORKINGCATALOGID;
        });
        $scope.cut = function (e) {
            $(".menuProduct").hide();
            $scope.displaypaste = true;
            $rootScope.pasteProductNav = true;
            if ($scope.Family_ID != 0 && $scope.product_ids.trim() != "") {
                $scope.Falg = "Cut";
                $scope.oldcatalogid = $scope.oldcatalogiddump;
                $scope.oldFamily_Id = $scope.Family_ID;
                $scope.passoldcategoryid = $scope.passcategoryid;
                $scope.productids = $scope.product_ids;
                $rootScope.SelectedNodeIDPresist = 1;

            } else {
                $scope.displaypaste = false;
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Select Items and Continue.',
                    type: "info"
                });
            }
            $scope.CancelProductItem();
            $('#divProductGrid').removeClass("display-none");
        };
        $scope.copy = function (e) {
            $(".menuProduct").hide();
            $('#divProductGrid').removeClass("display-none");

            if ($scope.copyEnable == false) {
                $scope.displaypaste = true;
                $rootScope.pasteProductNav = true;
                if ($rootScope.ProductCount >= ($rootScope.ProductSKUCount * ($rootScope.SKUAlertPercentage / 100))) {

                    if ($rootScope.ProductCount > 0 && $rootScope.ProductSKUCount > 0) {
                        $rootScope.SKUAlertPercentageCalculated = Math.round(($rootScope.ProductCount / $rootScope.ProductSKUCount) * 100);
                    }
                    $scope.skuAlertOptions.refresh({ url: "../views/app/partials/skuAlert.html" });
                    $scope.skuAlertOptions.center().open();
                }
                if ($scope.Family_ID != 0 && $scope.product_ids.trim() != "") {
                    $scope.Falg = "Copy";
                    $scope.oldcatalogid = $scope.oldcatalogiddump;
                    $scope.oldFamily_Id = $scope.Family_ID;
                    $scope.passoldcategoryid = $scope.passcategoryid;
                    $rootScope.SelectedNodeIDPresist = 1;
                    $scope.productids = $scope.product_ids;
                    $scope.copyEnable = true;
                } else {
                    $scope.displaypaste = false;
                    // alert("");
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please Select Items and Continue.',
                        type: "info"
                    });

                }
            }
            else {
                $scope.productids = $scope.product_ids
            }
            $scope.CancelProductItem();
        };

        $scope.detachProducts = function (e) {
            $(".menuProduct").hide();
            if ($scope.Family_ID != 0 && $scope.product_ids.trim() != "") {

                $scope.PRODUCT_ID = $scope.product_ids.replace(",", "")
                if ($scope.PRODUCT_ID != "")
                {

                    $http.get("../Catalog/GetProductExists?productId=" + $scope.PRODUCT_ID + '&catalogId=' + $rootScope.selecetedCatalogId).then(function (ProductExists) {
                        let Count = ProductExists.data;
                        $http.get("../Catalog/GetCatalog?catalogId=" + $rootScope.selecetedCatalogId).then(function (objcategoryDetails) {

                            $scope.catalogDetails = objcategoryDetails.data[0];

                            if ($scope.Family_ID != 0 && $scope.product_ids.trim() != "") {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: "Do you want to move this to the default product?",
                                    type: "confirm",
                                    buttons: [{ value: "Yes" }, { value: "No" }],
                                    success: function (result) {
                                        var bool = false;
                                        if (result === "Yes") {

                                            bool = true;
                                        }
                                        else {
                                            bool = false;
                                        }

                                        if (bool === true) {
                                            dataFactory.move_ProductsDefaultFamily($scope.Family_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.detachProductId, $scope.catalogDetails.DEFAULT_FAMILY).success(function (response) {
                                                $rootScope.deleteProduct = true;
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: '' + response + '.',
                                                    type: "info"
                                                });


                                                $rootScope.treeData.read();
                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                $scope.detachProductId = [];
                                                $scope.CancelProductItem();
                                                $scope.CancelMulipleAttributeProducts();
                                            }).error(function (error) {
                                                options.error(error);
                                            });
                                        }
                                        else {
                                            $.msgBox({
                                                title: $localStorage.ProdcutTitle,
                                                content: "Do you like to detach this Item?", // move to Recycle Bin
                                                type: "confirm",
                                                buttons: [{ value: "Yes" }, { value: "No" }],
                                                success: function (result) {
                                                    var bool = false;
                                                    if (result === "Yes") {
                                                        bool = true;
                                                    }

                                                    if (bool === true) {


                                                        dataFactory.trashProductPermanentFromMaster($scope.Family_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.detachProductId).success(function (response) {

                                                            $.msgBox({
                                                                title: $localStorage.ProdcutTitle,
                                                                content: '' + response + '.',
                                                                type: "info"
                                                            });
                                                            $rootScope.treeData.read();
                                                            $scope.attributes = [];
                                                            $scope.detachProductId = [];
                                                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                            $scope.CancelProductItem();
                                                            $scope.CancelMulipleAttributeProducts();
                                                        }).error(function (error) {
                                                            options.error(error);
                                                        });
                                                    }
                                                }
                                            });


                                        }

                                    }

                                });
                            }

                        });
                    });

                }
            }
            else
            {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Select Items and Continue.',
                    type: "info"
                });
            }
        };

        $scope.multipleDeleteItems = function (e) {
            $(".menuProduct").hide();
            if ($scope.Family_ID != 0 && $scope.product_ids.trim() != "") {
                $http.get("../Catalog/GetCatalog?catalogId=" + $rootScope.selecetedCatalogId).
                    then(function (objcategoryDetails) {
                        $scope.catalogDetails = objcategoryDetails.data[0];

                        if ($scope.catalogDetails.DEFAULT_FAMILY != "" && $scope.catalogDetails.DEFAULT_FAMILY != $scope.Family_ID) {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Do you like to move this Item to Default Product?",
                                type: "confirm",
                                buttons: [{ value: "Yes" }, { value: "No" }],
                                success: function (result) {
                                    var bool = false;
                                    if (result === "Yes") {
                                        bool = true;
                                    }
                                    if (bool == true) {

                                        dataFactory.move_ProductsDefaultFamily($scope.Family_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.detachProductId, $scope.catalogDetails.DEFAULT_FAMILY).success(function (response) {
                                            if (response == 'Item Already Exist In Default Product.') {
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: "Item already exists in Default Product. Would you like to delete it from the source you copied it from?",
                                                    type: "confirm",
                                                    buttons: [{ value: "Yes" }, { value: "No" }],
                                                    success: function (result2) {
                                                        bool = false;
                                                        if (result2 === "Yes") {
                                                            bool = true;
                                                        }
                                                        if (bool === true) {
                                                            dataFactory.delete_ProductsPermanentFromMaster($scope.Family_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.detachProductId).success(function (response) {
                                                                //  alert(response);
                                                                $.msgBox({
                                                                    title: $localStorage.ProdcutTitle,
                                                                    content: '' + response + '.',
                                                                    type: "info"
                                                                });
                                                                $rootScope.treeData.read();
                                                                $scope.attributes = [];
                                                                $scope.detachProductId = [];
                                                                $scope.CancelProductItem();
                                                                $scope.CancelMulipleAttributeProducts();
                                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                            }).error(function (error) {
                                                                options.error(error);
                                                            });
                                                        }
                                                    }
                                                });
                                            } else {
                                                // alert(response);
                                                $.msgBox({
                                                    title: $localStorage.ProdcutTitle,
                                                    content: '' + response + '.',
                                                    type: "info"
                                                });
                                                $rootScope.treeData.read();
                                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID);
                                            }
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    } else {

                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: "Do you like to delete this Item?", // move to Recycle Bin
                                            type: "confirm",
                                            buttons: [{ value: "Yes" }, { value: "No" }],
                                            success: function (result1) {
                                                bool = false;
                                                if (result1 === "Yes") {
                                                    bool = true;
                                                }
                                                if (bool === true) {
                                                    // $scope.CancelProductItem();
                                                    //  $scope.CancelMulipleAttributeProducts();

                                                    dataFactory.delete_ProductsPermanentFromMaster($scope.Family_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.detachProductId).success(function (response) {
                                                        // alert(response);
                                                        $.msgBox({
                                                            title: $localStorage.ProdcutTitle,
                                                            content: '' + response + '.',
                                                            type: "info"
                                                        });
                                                        $rootScope.treeData.read();
                                                        $scope.attributes = [];
                                                        $scope.detachProductId = [];
                                                        $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                                        $scope.CancelProductItem();
                                                        $scope.CancelMulipleAttributeProducts();
                                                    }).error(function (error) {
                                                        options.error(error);
                                                    });
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        } else {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Do you like to delete this Item?", // move to Recycle Bin
                                type: "confirm",
                                buttons: [{ value: "Yes" }, { value: "No" }],
                                success: function (result) {
                                    var bool = false;
                                    if (result === "Yes") {
                                        bool = true;
                                    }

                                    if (bool === true) {


                                        dataFactory.delete_ProductsPermanentFromMaster($scope.Family_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.detachProductId).success(function (response) {
                                            // alert(response);

                                            $.msgBox({
                                                title: $localStorage.ProdcutTitle,
                                                content: '' + response + '.',
                                                type: "info"
                                            });
                                            $rootScope.treeData.read();
                                            $scope.attributes = [];
                                            $scope.detachProductId = [];
                                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                            $scope.CancelProductItem();
                                            $scope.CancelMulipleAttributeProducts();

                                            // $scope.LoadMainProducts();
                                        }).error(function (error) {
                                            options.error(error);
                                        });
                                    }
                                }
                            });
                        }
                    });




            }

            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Select Items and Continue.',
                    type: "info"
                });
            }

        };

        $scope.paste = function (e) {

            $(".menuProduct").hide();
            $('#panel').hide();
            $('#divProductGrid').removeClass("col-sm-3");
            if (e == 'buttonclick') {
                $rootScope.sortOrder = 0;
            }
            if ($rootScope.SelectedNodeID.includes('~!')) {
                $rootScope.selectedFAmilyList = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget.context.childNodes[1].childNodes[6].id;
                if ($rootScope.selectedFAmilyList == undefined) {
                    $rootScope.selectedFAmilyList = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget.context.childNodes[1].children[0].id
                }
                $rootScope.familyDeleteNav = $rootScope.selectedFAmilyList.split('~!')[0];
                if ($rootScope.familyDeleteNav.includes('~')) {
                    $rootScope.familyDeleteNav = $rootScope.familyDeleteNav.split('~')[$rootScope.familyDeleteNav.split('~').length - 1];
                }
            }
            else if ($rootScope.SelectedNodeID.includes('~')) {
                $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
            }
            if ($scope.oldFamily_Id != 0 && $scope.productids.trim() != "") {
                var selProductCnt = $scope.productids.split(',').length - 1;
                if ($rootScope.AllowDuplicateItem_PartNum) {
                    $rootScope.ProductCount = $rootScope.ProductCount + selProductCnt;
                }

                if (($rootScope.ProductCount) < $rootScope.ProductSKUCount) {
                    $rootScope.ProductCount = $rootScope.ProductCount + selProductCnt;
                    dataFactory.PasteProduct($scope.oldFamily_Id, $scope.productids, $scope.Falg, $scope.Family.FAMILY_ID, $scope.getCustomerIDs, $rootScope.selecetedCatalogId, $scope.oldcatalogid, $scope.passoldcategoryid, $scope.passcategoryid, $rootScope.sortOrder).success(function (response) {
                        // alert(response.data);
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });

                        //var count = parseInt($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[1].replace(')', '').trim()) + ($scope.product_ids.split(',').length - 1);
                        //var selectedValue = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.replace($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[1].replace(')', '').trim(), count);
                        //$("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget.find($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[0].trim()).context.innerHTML = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget.find($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[0].trim()).context.innerHTML.replace($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText, selectedValue)
                        $scope.ResultEmpty = false;

                        //$scope.Family_ID = 0;
                        //$scope.productids = "";
                        //$scope.product_ids = "";
                        $scope.BulkIds = '';
                        $scope.displaypaste = false;
                        $rootScope.SelectedNodeIDPresist = 1;
                        $rootScope.treeData.read();
                        $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                        $rootScope.valueofcheckall = false;
                        $rootScope.valueofcheckbox = false;
                        $scope.copyEnable = false;
                    }).error(function (error) {
                        options.error(error);
                    });
                } else {
                    if ($scope.Falg === "Copy")
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'You have exceeded the Maximum No. of SKUs as per your Plan.',
                            type: "info"
                        });

                    else {
                        dataFactory.PasteProduct($scope.oldFamily_Id, $scope.productids, $scope.Falg, $scope.Family.FAMILY_ID, $scope.getCustomerIDs, $rootScope.selecetedCatalogId, $scope.oldcatalogid, $scope.passoldcategoryid, $scope.passcategoryid).success(function (response) {
                            // alert(response);
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });


                            $scope.ResultEmpty = false;
                            $scope.Family_ID = 0;
                            $scope.product_ids = "";
                            $scope.BulkIds = '';
                            $rootScope.treeData.read();
                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                            $rootScope.valueofcheckall = false;
                            $rootScope.valueofcheckbox = false;

                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            } else {
                // alert("Please Select Products and Continue.");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Select Items and Continue.',
                    type: "error"
                });


            }
        };


        //---------------------------------------------product configurator------------------------------------------------------------

        $scope.Addtoprodctconfigurator = function (e) {

            if ($scope.Family_ID != 0 && $scope.product_ids.trim() != "") {

                dataFactory.AddProductconfigurator($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.product_ids, $scope.selecteddRow).success(function (response) {
                    if (response != null) {
                        //alert(response);

                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });

                    }
                });

            } else {
                // alert("Please select the products");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select the Items.',
                    type: "info"
                });

            }
        };

        $scope.referenceTableData = [];
        $scope.tblReferenceTable = new NgTableParams({
            page: 1,
            count: 1000
        },
            {
                counts: [],
                total: function () { return $scope.referenceTableData.length; },
                getData: function ($defer, params) {
                    var filteredData = $scope.referenceTableData;
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(filteredData, params.orderBy()) :
                        filteredData;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });


        $scope.ConfiguratorList = function () {
            $("#cmnvalupdate").hide();
            $("#configuratorproducts").show();
            $scope.productconfig_ids = "";
            dataFactory.getprodconfigdata($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID).success(function (response) {
                if (response != null) {
                    var obj = jQuery.parseJSON(response.Data.Data);
                    if (obj.Columns.length > 0) {
                        $scope.referenceTableData = obj.Data;
                        $scope.columnsForReferenceTable = obj.Columns;
                    } else {
                        $scope.referenceTableData = [];
                        $scope.columnsForReferenceTable = [];
                    }
                }
            }).error(function (error) {
                options.error(error);
            });
            if ($scope.Family.FAMILY_ID != 0) {
                $scope.GetAllproductcofiguratordataSource.read();
            }
        };

        $scope.checkall_Prd_Config = function (e, f) {
            if (e.currentTarget.checked === true) {
                $scope.valueofcheckboxconfig = true;
                $scope.ConfigBulkIds = f;
                angular.forEach($scope.ConfigBulkIds, function (key) {
                    $scope.productconfig_ids = $scope.productconfig_ids + "," + key.product_id;
                    // $scope.Family_ID = key.FAMILY_ID;
                });
            } else {
                $scope.valueofcheckboxconfig = false;
                $scope.productconfig_ids = "";
            }
        };
        $scope.prodconfigselectedId = function (product_id, e) {
            if (e.currentTarget.checked === true) {
                //  $scope.Family_ID = family_Id;
                $scope.productconfig_ids = $scope.productconfig_ids + "," + product_id;
                $scope.productconfig_ids = $scope.productconfig_ids.replace(",,", ",");
            } else {
                var val = $scope.productconfig_ids;
                var res = val.split(",");
                var Product_IDs = $scope.productconfig_ids;
                if (res.length > 0) {
                    for (var i = 0; i < res.length; i++) {
                        if (res[i] === product_id) {
                            Product_IDs = $scope.productconfig_ids.replace(product_id, "");
                        }
                    }
                }
                Product_IDs = Product_IDs.replace(",,", ",");
                $scope.productconfig_ids = Product_IDs;
            }

        };

        $scope.ProdconfigData = [];
        $scope.tblprdconfigTable = new NgTableParams({
            page: 1,
            count: 1000
        },
            {
                counts: [],
                total: function () { return $scope.ProdconfigData.length; },
                getData: function ($defer, params) {
                    var filteredData = $scope.ProdconfigData;
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(filteredData, params.orderBy()) :
                        filteredData;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });

        $scope.Create_prodconfig = function () {
            if ($scope.productconfig_ids !== "") {
                var configIds = $scope.productconfig_ids;
                var splitIds = configIds.split(',');
                if (splitIds.length <= 4 && splitIds.length !== 2) {
                    dataFactory.createprodconfigdata($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $scope.productconfig_ids).success(function (response) {
                        if (response != null) {
                            var obj = jQuery.parseJSON(response.Data.Data);
                            if (obj.Columns.length > 0) {
                                $('#btnsaveclose').prop('disabled', false);
                                $scope.ProdconfigData = obj.Data;
                                $scope.columnsForProdconfigTable = obj.Columns;
                                //$scope.winProductConfig.refresh({ url: "../Views/App/Partials/ProductConfigurator.html" });
                                //$scope.winProductConfig.title("Product Configuraror");
                                //$scope.winProductConfig.center().open();
                            } else {
                                $scope.ProdconfigData = [];
                                $scope.columnsForProdconfigTable = [];
                            }
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                } else {
                    if (splitIds.length == 2) {
                        // alert("Please Select less than 2 Products and Continue.");
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please Select less than 2 Items and Continue.',
                            type: "error"
                        });

                    }
                    {
                        //alert("Please Select less than 3 Products and Continue.");
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please Select less than 3 Items and Continue.',
                            type: "error"
                        });

                    }
                }
            } else {
                //alert("Please select the products");

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select the Items.',
                    type: "error"
                });
            }

        };
        $scope.selectedRowDynamics = [];
        $scope.referenceTableData1 = [];
        $scope.columnsForReferenceTable1 = [];
        $scope.attributePattern = {};
        $scope.UpdateProductConfig = function (data) {
            $scope.configId = data;
            dataFactory.ProdconfigSpecs($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.configId).success(function (response) {
                if (response != "" && response != null) {
                    // $scope.prodconfigselectedRow = $.parseJSON(response.Data.Data);
                    //  $scope.prodconfigselectedRow = response.Data.Data;
                    var obj = jQuery.parseJSON(response.Data.Data);
                    if (obj.Columns.length > 0) {
                        $scope.referenceTableData1 = obj.Data;
                        $scope.columnsForReferenceTable1 = obj.Columns;


                        dataFactory.GetAllattributesProdConfig($rootScope.selecetedCatalogId).success(function (response) {
                            for (var i = 0; i < obj.Columns.length; i++) {
                                for (var j = 0; j < response.length; j++) {
                                    if (obj.Columns[i].title.split('__')[0] == response[j].ATTRIBUTE_NAME) {
                                        var name = response[j].ATTRIBUTE_NAME;
                                        var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                        pattern = pattern.replace("numeric", response[j].NUMERIC);
                                        pattern = pattern.replace("decimal", response[j].DECIMAL);
                                        var reg = new RegExp(pattern);
                                        $scope.attributePattern[name] = reg;
                                    }
                                }
                            }


                        });


                    } else {
                        $scope.referenceTableData1 = [];
                        $scope.columnsForReferenceTable1 = [];
                    }
                    $scope.winProductConfigEdit.refresh({ url: "../Views/App/Partials/dynamicprodconfigEditPopup.html" });
                    $scope.winProductConfigEdit.title("Product Configurator");
                    if ($scope.winProductConfigEdit) {
                        $scope.winProductConfigEdit.center().open();
                    }
                }
            });
        };
        $scope.Remove_prodconfig = function () {
            if ($scope.productconfig_ids !== "") {
                dataFactory.RemoveProductconfigurator($scope.productconfig_ids).success(function (response) {
                    if (response != "" && response != null) {
                        // alert(response);
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "error"
                        });
                        $scope.ConfiguratorList();
                    }
                });

            } else {
                // alert("Please select the products");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select the Items.',
                    type: "error"
                });
            }

        };
        $scope.LoadConfigData = function () {
            if ($scope.Family.FAMILY_ID != 0) {
                $scope.GetAllproductcofiguratordataSource.read();
            }
        };
        $scope.GetAllproductcofiguratordataSource = new kendo.data.DataSource({
            pageSize: 10,
            type: "json",
            autoBind: false,
            serverFiltering: true,
            editable: true,
            serverPaging: true,
            serverSorting: true,
            pageable: { buttonCount: 5 },
            transport: {
                read: function (options) {
                    dataFactory.GetAllConfigTreeData($scope.Family.FAMILY_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                    return kendo.stringify(options);

                }
            },
            schema: {
                model: {
                    id: "CONFIG_PRODUCT_ID",
                    fields: {
                        CONFIG_PRODUCT_ID: { editable: false, nullable: true },
                        ITEM_ID: {
                            editable: true,
                            validation: { required: true }
                        },
                        CATEGORY_ID: {
                            editable: false,
                            validation: { required: true }
                        },
                        CATEGORY_SHORT: {
                            editable: true,
                            validation: { required: true }
                        },
                        FAMILY_ID: {
                            editable: true,
                            validation: { required: true }
                        },
                        FAMILY_NAME: {
                            editable: true,
                            validation: { required: true }
                        }
                    }
                }
            }
        });

        $scope.productconfiguratorGridOptions = {
            dataSource: $scope.GetAllproductcofiguratordataSource,
            sortable: true,
            scrollable: true,
            pageable: { buttonCount: 5 },
            autoBind: false,
            selectable: "row",
            editable: "inline",
            columns: [{ field: "CONFIG_PRODUCT_ID", title: "Config Product ID" }, { field: "ITEM_ID", title: "ITEM#" },
            { field: "CATEGORY_SHORT", title: "Category ID" }, { field: "FAMILY_ID", title: "Family ID" }, { field: "FAMILY_NAME", title: "Family Name" }
            ],
            detailTemplate: kendo.template($("#prodconfigtemplates").html()),
            dataBound: function (e) {
                if (e.sender._data.length > 0) {
                    var row = this.tbody.find('tr:first');
                    this.select(row);

                } else {

                }
            }
        };
        $scope.handleChanges = function (dataItem) {
            if (dataItem != undefined) {

            }
        };


        $scope.detailGridproductconfiguratorOptions = function (dataItem) {
            return {
                dataSource: {
                    type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.GetAllTreeDataDetails($scope.Family.FAMILY_ID, dataItem.CONFIG_PRODUCT_ID).success(function (response) {
                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    },
                    schema: {
                        model: {
                            id: "PRODUCT_ID",
                            fields: {
                                PRODUCT_ID: { editable: false },
                                ITEM_ID: { editable: false },
                                CATEGORY_ID: { editable: false },
                                CATEGORY_SHORT: { editable: false },
                                CATEGORY_NAME: { editable: false },
                                FAMILY_ID: { editable: false },
                                FAMILY_NAME: { editable: false },
                                CONFIG_PRODUCT_ID: { editable: false },
                            }
                        }
                    },
                    serverFiltering: true,
                    serverPaging: true,
                    serverSorting: true,
                    pageable: { buttonCount: 5 },
                },
                scrollable: false,
                sortable: true,
                autobind: false,
                selectable: "row",
                columns: [{ field: "PRODUCT_ID", title: "Product ID" }, { field: "ITEM_ID", title: "ITEM#" },
                { field: "CATEGORY_SHORT", title: "Category ID" }, { field: "FAMILY_ID", title: "Family ID" }, { field: "FAMILY_NAME", title: "Family Name" }, { field: "CONFIG_PRODUCT_ID", title: "Config Product ID" }],
                dataBound: function () {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#ConfiguratorGrid").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    } else {
                        this.expandRow(this.tbody.find("tr.k-master-row"));
                        //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                    }
                }
            };
        };


        $scope.SaveOrUpdateProductConfigItem = function (row) {
            dataFactory.SaveproductconfigSpecs(row, $scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                debugger
                if (response != null) {
                    $scope.winProductConfigEdit.close();
                    $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                    // alert("Success.");
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Save successful.',
                        type: "error"
                    });
                    $scope.ProdconfigData = [];
                    $scope.columnsForProdconfigTable = [];
                    $('#btnsaveclose').prop('disabled', true);
                    $("#configuratorproducts").hide();
                }
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.CancelProductConfigItem = function () {
            $scope.winProductConfigEdit.close();
            //$("#configuratorproducts").hide();
        };
        $scope.Save_prodconfig = function () {
            $scope.ProdconfigData = [];
            $scope.columnsForProdconfigTable = [];
            $('#btnsaveclose').prop('disabled', true);
            $("#configuratorproducts").hide();
        };
        //----------------------------------------------------------------------------------------------------------


        $scope.hideClick = function (e) {
            $scope.productpreview = false;
            $("#productpreview").hide();
            $("#columnsetupproduct").hide();
            $("#myDIV").show();
            $("#divProductGrid").show();
            // $("#productpaging1").show();
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();

        };
        $scope.subhideClick = function (e) {
            $("#subproductpreview").hide();
            $("#divsubProductGrid").show();
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();
            $("#subproductpaging1").show();
        };

        $scope.onProdSelect = function (e) {

            var message = $.map(e.files, function (file) { return file.name; }).join(", ");

            // $scope.CategoryImage = '~/Images/' + message;
            // alert(message);
            //kendoConsole.log("event :: select (" + message + ")");
        };
        $scope.onProdSuccess = function (e) {

            var message = $.map(e.files, function (file) { return file.name; }).join(", ");
            angular.forEach($scope.selectedRow, function (value, key) {
                if (key == e.sender.element[0].id) {
                    $scope.selectedRow[key] = '\\Images\\' + e.files[0].name;
                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();
                    $scope.$apply();
                }
            });
            angular.forEach($scope.selectedRowSub, function (value, key) {
                if (key == e.sender.element[0].id) {
                    $scope.selectedRowSub[key] = '\\Images\\' + e.files[0].name;
                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();
                    $scope.$apply();
                }
            });
            $(".k-upload-files").remove();
            $(".k-upload-status").remove();
        };

        $scope.datetimecheck = function (e, name) {
        };

        $scope.datecheck = function (e, name) {
        };
        $scope.myDecimal = 0;
        $scope.setTwoNumberDecimal = function (event) {
            this.value = parseFloat(this.value).toFixed(2);
        };
        $scope.AllowDublicatePartNoss = false;


        function groupHeaderName(e) {


            if (e.value === 1) {
                return "Item Specifications (" + e.count + " items)";
            } else if (e.value === 3) {
                return "Item Image / Attachment (" + e.count + " items)";
            } else if (e.value === 4) {
                return "Item Price (" + e.count + " items)";
            } else if (e.value === 6) {
                return "Item Key (" + e.count + " items)";
            } else if (e.value === 7) {
                return "Product Description (" + e.count + " items)";
            } else if (e.value === 9) {
                return "Product Image / Attachment (" + e.count + " items)";
            } else if (e.value === 11) {
                return "Product Specifications (" + e.count + " items)";
            } else if (e.value === 12) {
                return "Product Price (" + e.count + " items)";
            } else if (e.value === 13) {
                return "Product Key (" + e.count + " items)";
            } else {
                return "Item Specifications (" + e.count + " items)";
            }
        }


        $scope.GetAllCatalogattributesdataSource = new kendo.data.DataSource({
            type: "json",
            pageable: { buttonCount: 5 },
            pageSize: 12,
            batch: true,
            autoBind: false,
            sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogattributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ISAvailable: { type: "boolean" },
                        CATALOG_ID: { editable: false },
                        FAMILY_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false }
                    }
                }
            },
            group: {
                field: "ATTRIBUTE_TYPE",
                aggregates: [
                    { field: "ATTRIBUTE_TYPE", aggregate: "count" }
                ]
            }
        });

        $scope.mainGridOptions = {
            dataSource: $scope.GetAllCatalogattributesdataSource,
            pageable: { alwaysVisible: false, buttonCount: 5 },
            filterable: { mode: "row" },
            navigatable: true,

            autoBind: false,
            columns: [
                { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', filterable: false },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px", filterable: true },
                {
                    field: "ATTRIBUTE_TYPE",
                    title: "Attribute Type",
                    hidden: true,
                    aggregates: ["count"],
                    groupHeaderTemplate: groupHeaderName
                }]
        };

        $scope.updateSelection = function (e, id) {
            id.dataItem.set("ISAvailable", e.currentTarget.checked);
        };
        $scope.prodfamilyattrdataSource = new kendo.data.DataSource({
            type: "json",
            autoBind: false,
            serverFiltering: true,
            sort: { field: "SORT_ORDER", dir: "asc" },
            pageable: { buttonCount: 5 },
            pageSize: 12,
            transport: {
                read: function (options) {
                    // alert($rootScope.selecetedFamilyId);
                    // if ($rootScope.selecetedFamilyId !== "0") {
                    dataFactory.GetprodfamilyAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId).success(function (response) {

                        for (var i = 0; response.length > i; i++) {
                            if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                            }
                            if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                            }
                        }
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                    // }
                }
            },
            schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ISAvailable: {
                            type: "boolean"
                        },
                        CATALOG_ID: { editable: false },
                        FAMILY_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        SORT_ORDER: { editable: false }
                    }
                }
            }
        });

        $scope.selectedGridOptions = {
            dataSource: $scope.prodfamilyattrdataSource,
            pageable: { buttonCount: 5 },
            autoBind: false,
            selectable: "multiple",
            dataBound: onDataBound,
            columns: [
                { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforprodfamily" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>' },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px" }
            ]
        };

        var updownIndex = null;

        function onDataBound(e) {
            if (updownIndex != null) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {

                    if (updownIndex == i) {
                        var isChecked = this.tbody.find("tr[data-uid='" + view[i].uid + "']").find(".chkbxforprodfamily").attr("checked");
                        if (isChecked) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .addClass("k-state-selected");
                        }

                    }
                }

                updownIndex = null;
            }
        }

        $scope.updateSelection1 = function (e, id) {
            id.dataItem.set("ISAvailable", e.currentTarget.checked);
        };
        $scope.productNewAttributeSetupClick = function () {
            $scope.winProductNewAttributeSetup.refresh({ url: "../Views/App/Partials/productNewAttributeSetup.html" });
            $scope.winProductNewAttributeSetup.center().open();
            // $scope.tableGroupHeaderValue = true;
        };
        $scope.subProductNewAttributeSetupClick = function () {
            $scope.winSubProductNewAttributeSetup.refresh({ url: "../Views/App/Partials/subProductNewAttributeSetup.html" });
            $scope.winSubProductNewAttributeSetup.center().open();
            // $scope.tableGroupHeaderValue = true;
        };
        $scope.UnPublishAttributes = function () {
            // $scope.DeletePublishAttributes();
            //   $scope.SavePublishAttributes();
            $scope.myDIV = true;
            $("#ItemGrid").removeClass('ng-hide');
            dataFactory.UnPublishAttributes($localStorage.CategoryID, $scope.GetAllProductAttributesNew._data).success(function (response) {

                // $scope.GetAllCatalogattributesdataSource.read();
                //$scope.GetAllProductAttributesNew.read();

                $scope.ResetPage();
                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $localStorage.CategoryID);
                $scope.OpenProductDefaultTab();
                $("#columnsetupproduct").hide();
                $("#columnsetup").hide();
                $scope.myDIV = true;
                $(".column_setup").hide();
                $("#newProductBtn").hide();
                $("#newProductBtn1").show();
                $("#gridproduct").show();
                $("#productgridtabstrip").show();
                $(".productgridtabstripclass").show();
            }).error(function (error) {
                options.error(error);
            });
            $scope.winColumnSetuplayout.close();

            $scope.GetChangesAvailableData();
        };


        $scope.UnPublishAttributesSorting = function () {
            dataFactory.UnPublishAttributes($localStorage.CategoryID, $scope.GetAllProductAttributesNew._data).success(function (response) {
                $scope.GetAllProductAttributesNew.read();
            }).error(function (error) {
                options.error(error);
            });
        };

        $("#columnsetup").hide();
        $(".column_setup").hide();

        //$scope.RefreshProductAttributes = function () {

        //    // $("#productgridtabstrip").hide();
        //    // $("#divProductGrid").hide();
        //    //$("#newProductBtn1").hide();
        //    $("#gridproduct").hide();
        //    $("#attributePack").hide();
        //    $("#cmnvalupdate").hide();
        //    $("#columnsetup").show();
        //    $(".column_setup").show();
        //    $('#productpreview').hide();
        //    $('#configuratorproducts').hide();
        //    $("#columnsetupproduct").show();
        //    //$("#importTableSheetWindow").show();
        //    $("#importTableSheetWindow").hide();
        //    $scope.ResetPage();
        //    $scope.GetAllCatalogattributesdataSource.read();
        //    $scope.prodfamilyattrdataSource.read();
        //};

        $scope.RefreshProductAttributes = function () {
            // $("#productgridtabstrip").hide();
            // $("#divProductGrid").hide();
            //$("#newProductBtn1").hide();
            // $("#gridproduct").hide();
            $('#myDIV').css('display', 'none');
            $("#ItemGrid").addClass('ng-hide');
            // $('#gridproduct1').show();
            $("#columnsetup").show();
            $(".column_setup").show();
            $("#attributePack").hide();
            $("#cmnvalupdate").hide();
            $('#productpreview').hide();
            $('#configuratorproducts').hide();
            $("#attributePack").hide();
            $("#filterAttributePack").hide();
            $("#productgridtabstrip").hide();
            $(".productgridtabstripclass").hide();
            $("#productpreview").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();
            $("#importSuccessWindow").hide();
            $("#importerror").hide();
            $("#importpicklist").hide();
            $("#importErrorWindow").hide();
            $("#importTableSheetSlectectionWindow").hide();
            $("#cmnvalupdate").hide();
            $scope.ShowProductGridValues = true;
            $("#columnsetupproduct").show();
            $scope.AttributeInProductGrid = true;
            $("#importTableSheetWindow").hide();
            $scope.ResetPage();
            $scope.GetAllCatalogattributesdataSource.read();
            $scope.prodfamilyattrdataSource.read();
            $scope.GetAllProductAttributesNew.read();
            $("#productpaging1").hide();
            $("#productpaging").hide();
        };

        $scope.CancelProductAttributes = function () {
            $("#ItemGrid").removeClass('ng-hide');
            $scope.ResetPage();
            $("#columnsetupproduct").hide();
            $scope.OpenProductDefaultTab();
            $("#columnsetupproduct").hide();
            $("#columnsetup").hide();
            $(".column_setup").hide();
            $("#cmnvalupdate").hide();
            $("#newProductBtn").hide();
            $("#newProductBtn1").show();
            $("#gridproduct").show();
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();
            if ($rootScope.Mainprodcount > 0) {
                $("#productpaging1").show();
                $("#Productcountid").show();
            }
            else {
                $("#Productcountid").hide();
            }
            $scope.winColumnSetuplayout.close();
            $('#sampleProdgrid').show();
        };
        $scope.ResetPage = function () {
            $scope.prodfamilyattrdataSource._currentRangeStart = 0;
            $scope.prodfamilyattrdataSource._page = 1;
            $scope.prodfamilyattrdataSource._skip = 0;
            $scope.GetAllCatalogattributesdataSource._currentRangeStart = 0;
            $scope.GetAllCatalogattributesdataSource._page = 1;
            $scope.GetAllCatalogattributesdataSource._skip = 0;
        };
        $scope.SavePublishAttributes = function () {

            dataFactory.SavePublishAttributes($localStorage.CategoryID, $scope.GetAllProductAttributesNew._data).success(function (response) {



                // $scope.GetAllCatalogattributesdataSource.read();
                //  $scope.GetAllProductAttributesNew.read();
                // })
                //.error(function (error) {
                //  options.error(error);
            });
        };
        $scope.DeletePublishAttributes = function () {
            var entityGrid = $("#newProductAttributeDesign").data("kendoGrid");
            var g = entityGrid.dataSource._data;
            // var g = $scope.prodselectgrid.select();
            var data = [];
            for (var i = 0; i < g.length; i++) {
                if (g[i].AVAILABLE == 0) {
                    var selectedItem = g[i];
                    //entityGrid.dataItem(g[i]);
                    data.push(selectedItem);
                }
            }
            //if (g != undefined && g != null && g != '' && g != "") {
            //    data.push(g);
            //}

            dataFactory.DeletePublishAttributes(data).success(function (response) {
                //   $scope.GetAllCatalogattributesdataSource.read();
                // $scope.prodfamilyattrdataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.BtnMoveUpClicksub = function () {
            var g = $scope.prodselectgridsubproducts.select();
            if (g.length === 1) {
                g.each(function (index, row) {
                    var selectedItem = $scope.prodselectgridsubproducts.dataItem(row);
                    var data = $scope.prodselectgridsubproducts.dataSource.data();
                    var dataRows = $scope.prodselectgridsubproducts.items();
                    var selectedRowIndex = dataRows.index(g);
                    // var sortOrder = data[selectedRowIndex - 1].SORT_ORDER;
                    if (selectedItem.SORT_ORDER !== 1000) {
                        dataFactory.BtnMoveUpClicksub(selectedItem.SORT_ORDER - 1, selectedItem).success(function (response) {
                            $scope.GetAllCatalogattributesdataSourceforSubproducts.read();
                            $scope.prodfamilyattrdataSourcesubproducts.read();
                            updownIndex = selectedRowIndex - 1;
                        }).error(function (error) {
                            options.error(error);
                        });
                    } else {
                        // alert("Selected Attribute Is Not Part of the Product Table Display.");
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Selected Attribute cannot be moved as it is not part of the Item Table Display.',
                            type: "info"
                        });
                    }
                });
            } else {
                //alert("Please Select At Least One Attribute.");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one Attribute.',
                    type: "info"
                });

            }
        };


        $scope.BtnMoveDownClicksub = function () {
            var g = $scope.prodselectgridsubproducts.select();

            if (g.length === 1) {

                g.each(function (index, row) {
                    var selectedItem = $scope.prodselectgridsubproducts.dataItem(row);
                    var data = $scope.prodselectgridsubproducts.dataSource.data();
                    var dataRows = $scope.prodselectgridsubproducts.items();
                    var selectedRowIndex = dataRows.index(g);
                    // var sortOrder = data[selectedRowIndex + 1].SORT_ORDER;
                    if (selectedItem.SORT_ORDER !== 1000) {
                        dataFactory.BtnMoveDownClicksub(selectedItem.SORT_ORDER + 1, selectedItem).success(function (response) {
                            $scope.GetAllCatalogattributesdataSourceforSubproducts.read();
                            $scope.prodfamilyattrdataSourcesubproducts.read();
                            updownIndex = selectedRowIndex + 1;
                        }).error(function (error) {
                            options.error(error);
                        });
                    } else {
                        // alert("Selected Attribute Is Not Part of the Product Table Display.");
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Selected Attribute cannot be moved as it is not part of the Item Table Display.',
                            type: "info"
                        });
                    }
                });
            } else {
                //alert("Please Select At Least One Attribute.");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one Attribute.',
                    type: "error"
                });
            }
        };

        $scope.MainProducts = new kendo.data.DataSource({
            type: "json",
            batch: true,
            autoBind: false,
            pageSize: 5,
            transport: {
                read: function (options) {
                    dataFactory.getProducts("", $rootScope.selecetedFamilyId, $rootScope.selecetedCatalogId).success(function (response) {
                        $rootScope.MainProductsAll = response;
                        if ($scope.valueofShowSubProducts) {
                            options.success(response);
                        } else {
                            if ($scope.valuesubproduct) {

                                $('#subproductdiv').show();
                                $scope.valuesubproduct = false;
                            } else {
                                $('#subproductdiv').hide();
                            }
                            var tempresponse = response.filter(function (attrVals) { return attrVals.SubProdCnt > 0; });
                            options.success(tempresponse);
                        }
                        if (response.length > 0) {

                            if ($rootScope.navigateprodProductTab != false) {
                                if ($scope.clicksubProductTab == true || $scope.clicksubProductTab == undefined) {
                                    var continueLoad = true;
                                    angular.forEach(response, function (Maindata) {
                                        if (Maindata.PRODUCT_ID == $scope.ParentProductId) {
                                            if (continueLoad == true) {

                                                if ((Maindata.STRING_VALUE.split('(')[1].replace(')', '').trim()) != "0") {
                                                    $("#subproductsmaingrid tbody").find("tr")[0].className = "ng-scope k-state-selected";
                                                    $scope.itemClick(Maindata, null);
                                                    continueLoad = false;
                                                }
                                            }
                                        }
                                    });

                                }
                            }
                            if ($scope.ParentProductId == undefined);
                            {
                                // $scope.ParentProductId = response[0]["PRODUCT_ID"];
                            }
                            $('#ColumnSetupSubproducts').hide();
                            $('#cmnvalupdateforsubproduct').hide();
                            $('#subproductattrib').hide();

                        } else {
                            $('#subproductdiv').hide();
                            $('#ColumnSetupSubproducts').hide();
                            $('#cmnvalupdateforsubproduct').hide();
                            //$scope.navigateprodProductTab = false;
                        }

                    }).error(function (response) {
                        options.success(response);
                    });

                }

            },
            schema: {
                model: {
                    id: "PRODUCT_ID"
                }
            },
            error: function (e) {
                // alert(e.xhr.responseText);
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });
            }
        });

        $scope.MainProductsfordiv = new kendo.data.DataSource({
            type: "json",
            batch: true,
            autoBind: false,
            pageSize: 5,
            transport: {
                read: function (options) {

                    dataFactory.getProducts("", $rootScope.selecetedFamilyId, $rootScope.selecetedCatalogId).success(function (response) {
                        // alert(response.length);
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response.length + '.',
                            type: "info"
                        });
                        if ($scope.valueofShowSubProducts) {
                            options.success(response);
                        } else {
                            $('#subproductdiv').hide();
                            var tempresponse = response.filter(function (attrVals) { return attrVals.SubProdCnt > 0; });
                            options.success(tempresponse);
                        }
                        if (response.length > 0) {
                            if ($scope.ParentProductId == undefined);
                            {
                                // $scope.ParentProductId = response[0]["PRODUCT_ID"];
                            }
                            $('#ColumnSetupSubproducts').hide();
                            $('#cmnvalupdateforsubproduct').hide();
                            $('#subproductattrib').hide();
                        } else {
                            $('#subproductdiv').hide();
                            $('#ColumnSetupSubproducts').hide();
                            $('#cmnvalupdateforsubproduct').hide();
                        }
                    }).error(function (response) {
                        options.success(response);
                    });

                }
            },
            schema: {
                model: {
                    id: "PRODUCT_ID"
                }
            },
            error: function (e) {
                // alert(e.xhr.responseText);
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });
            }
        });
        $rootScope.LoadMainProductssub = function () {

            $scope.ParentProductId = $rootScope.ParentProductId;
            $scope.MainProducts.read();

            $scope.rendersubproducts = true;
            $("#cmnvalupdate").hide();
            $("#subproductpreview").hide();
            $scope.valueofShowSubProducts = false;
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();
            $('#mainProductsMenu').hide();
            $('#subProductsMenu').show();
        };

        $scope.subproducts = {
            dataSource: $scope.MainProducts,
            autoBind: false,
            sortable: true,

            pageSize: 5,
            pageable: {


            },
            selectable: true,

            columns: [
                {
                    field: "STRING_VALUE",
                    title: $localStorage.CatalogItemNumber
                },
                {
                    field: "Description",
                    title: "DESCRIPTION"
                }
            ],
            dataBound: function (e) {
                if (e.sender._data.length > 0) {
                    if ($scope.ParentProductId === 'undefined' || $scope.ParentProductId === '0' || $scope.ParentProductId == 0) {
                        $scope.ParentProductId = e.sender._data[0].PRODUCT_ID;
                    }
                    if ($scope.ParentProductId == undefined || $scope.ParentProductId == 0) {
                        var row = this.tbody.find('tr:first');
                        this.select(row);
                    } else {
                        var rowSub = this.tbody.find('tr');

                        for (var i = 0; i < e.sender._data.length; i++) {
                            if ($rootScope.ParentProductId == e.sender._data[i].PRODUCT_ID) {
                                $scope.ParentProductId = e.sender._data[i].PRODUCT_ID;
                                var row1 = this.tbody.find(rowSub[i]);
                                this.select(row1);

                            }
                        }
                    }

                    //  $scope.ParentProductId = $rootScope.ParentProductId == 0 ? e.sender._data[0].PRODUCT_ID : $rootScope.ParentProductId;



                } else {
                    $scope.ParentProductId = 0;
                }

            }
        };

        $scope.LoadMainProducts = function () {

            //if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            //    $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1051px !important;");
            //    $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1051px !important;");

            //}

            //if (window.screen.availWidth >= 1490 && window.screen.availWidth <= 1600) {
            //    $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1480px !important;");
            //    $("#samplegridss tbody").css("cssText", "margin-left: -2px;width: 1180px !important;");

            //}

            $scope.valueofShowSubProducts = false;
            if ($scope.ParentProductId == undefined || $scope.ParentProductId == 0) {
                $scope.ParentProductId = 0;
            }
            $scope.rendersubproducts = true;
            $("#cmnvalupdate").hide();
            $scope.valueofShowSubProducts = false;
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();
            $('#mainProductsMenu').hide();
            $('#subProductsMenu').show();
            if ($rootScope.navigateprodProductTab != false) {

                if ($location.$$absUrl.contains("App") && $location.$$absUrl.split("App")[1] != '/Inverted' && $location.$$absUrl.split("App")[1] != '/Search') {
                    $scope.clicksubProductTab = true;

                }
            }
            var tbs = $(".productgridtabstripclass").kendoTabStrip({

                change: function (e) {
                    //some code
                }
            }).data("kendoTabStrip");

            tbs.select(1);

            $scope.MainProducts.read();
            var grid = $("#subproductsmaingrid").data("kendoGrid");
            if ($rootScope.pageCountMain != undefined && $rootScope.pageCountMain.toString() != "" && $rootScope.pageCountMain != null) {
                // grid.dataSource.page($rootScope.pageCountMain);
            }
            else {
                grid.dataSource.page(1);
            }

        };
        $scope.showAllClick = function () {
            $scope.rendersubproducts = true;
            $scope.clicksubProductTab = true;
            if ($scope.ParentProductId == undefined || $scope.ParentProductId == 0) {
                $scope.ParentProductId = 0;
            }
            $scope.MainProducts.read();
        };
        function onChange(e) {

            if ($rootScope.invertPro != "SubProduct") {

                $scope.callProductGridPagePerSP(e.sender.text().trim());
            }
            else {

            }
        };
        $(".subproditemSelect").kendoDropDownList({
            change: onChange,
            dataSource: [
                { Value: "5" },
                { Value: "10" },
                { Value: "25" },
                { Value: "50" },
                { Value: "100" }
            ],
            dataTextField: "Value",
            dataValueField: "Value",

        });

        $scope.ParentProductId = 0;
        $rootScope.columnsSubAtt = [];
        $rootScope.SubprodData = [];
        $scope.ProdCurrentPageSP = 1;
        $scope.ProdCountPerPageSP = "10";
        $scope.gridCurrentPageSP = "10";
        $scope.callProductGridPagePerSP = function (pageper) {
            //  alert('11')

            if (pageper != null && pageper != "0") {
                //document.getElementById("gridpagingSP" + $scope.ProdCountPerPageSP).className = "gridpaging";
                $scope.ProdCountPerPageSP = pageper;
                $scope.ProdCurrentPageSP = 1;
                var ProdCountSP = [];

                dataFactory.GetsubproductspecsWithoutAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId).success(function (response) {
                    //        alert('1')
                    var objSP = jQuery.parseJSON(response.Data.Data);
                    var ProdEditPageloopcntSP = Math.floor(objSP.length / parseInt($scope.ProdCountPerPageSP));
                    if ((objSP.length % parseInt($scope.ProdCountPerPageSP)) > 0) {
                        ProdEditPageloopcntSP = ProdEditPageloopcntSP + 1;
                    }
                    for (var i = 1; i <= ProdEditPageloopcntSP; i++) {
                        ProdCountSP.push(i);
                    }
                    if (objSP.length == 0) {
                        ProdCountSP[0] = 1
                        $scope.ProdPageCountSP = ProdCountSP;
                        $scope.ProdCurrentPageSP = 1;
                        $scope.totalProdPageCountSP = 1;

                    }
                    else {
                        ProdCountSP = [];
                        for (var i = 1; i <= ProdEditPageloopcntSP; i++) {
                            ProdCountSP.push(i);
                        }


                        $scope.ProdPageCountSP = ProdCountSP;
                        $scope.ProdCurrentPageSP = 1;
                        $scope.totalProdPageCountSP = ProdEditPageloopcntSP;

                        $scope.ProdCurrentPageSP = 1;
                    }

                });
                dataFactory.Getsubproductspecs($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId, $scope.ProdCurrentPageSP, $scope.ProdCountPerPageSP).success(function (response) {
                    var obj = jQuery.parseJSON(response.Data.Data);
                    $rootScope.SubprodData = obj;
                    $rootScope.columnsSubAtt = response.Data.Columns;
                    angular.forEach($rootScope.SubprodData, function (value) {
                        value.SORT = parseFloat(value.SORT);
                    });

                }).error(function (error) {
                    options.error(error);
                });
            }
        };

        $rootScope.callProductGridPagingSP = function (pageno) {
            if (pageno != null && pageno <= $scope.totalProdPageCountSP && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {
                if (pageno == 'PREV' && $scope.ProdCurrentPageSP != 1) {
                    $scope.ProdCurrentPageSP = parseInt($scope.ProdCurrentPageSP) - 1;
                } else if (pageno == 'PREV' && $scope.ProdCurrentPageSP == 1) {
                    $scope.ProdCurrentPageSP = 1;
                } else if (pageno == 'NEXT') {
                    $scope.ProdCurrentPageSP = parseInt($scope.ProdCurrentPageSP) + 1;
                    if ($scope.ProdCurrentPageSP > $scope.totalProdPageCountSP) {
                        $scope.ProdCurrentPageSP = $scope.totalProdPageCountSP;
                    }
                } else {
                    $scope.ProdCurrentPageSP = pageno;
                }
                dataFactory.Getsubproductspecs($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId, $scope.ProdCurrentPageSP, $scope.ProdCountPerPageSP).success(function (response) {
                    var obj = jQuery.parseJSON(response.Data.Data);
                    $rootScope.SubprodData = obj;

                    $rootScope.columnsSubAtt = response.Data.Columns;
                    angular.forEach($rootScope.SubprodData, function (value) {
                        value.SORT = parseFloat(value.SORT);
                    });
                    //  $rootScope.tblDashBoardss.reload();
                }).error(function (error) {
                    options.error(error);
                });
            }
        };
        $scope.itemClick = function (data, e) {
            //   alert('121')
            $scope.ParentProductId = data.PRODUCT_ID;
            $rootScope.dataItemClick = data;
            $rootScope.getProductId = data.PRODUCT_ID;
            //$rootScope.eItemClick=e;
            // $scope.ProdCurrentPageSP = "1";
            //var dd = $('#cboProdPageCountSPup').data('kendoDropDownList');
            //dd.selectedIndex = 0;
            var ProdCountSP = [];
            dataFactory.GetsubproductspecsWithoutAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId).success(function (response) {
                var objSP = jQuery.parseJSON(response.Data.Data);
                var ProdEditPageloopcntSP = Math.floor(objSP.length / parseInt($scope.ProdCountPerPageSP));
                if ((objSP.length % parseInt($scope.ProdCountPerPageSP)) > 0) {
                    ProdEditPageloopcntSP = ProdEditPageloopcntSP + 1;
                }

                for (var i = 1; i <= ProdEditPageloopcntSP; i++) {
                    ProdCountSP.push(i);
                }

                if (objSP.length == 0) {
                    ProdCountSP[0] = 1
                    $scope.ProdPageCountSP = ProdCountSP;
                    $scope.ProdCurrentPageSP = 1;
                    $scope.totalProdPageCountSP = 1;

                }
                else {
                    ProdCountSP = [];
                    for (var i = 1; i <= ProdEditPageloopcntSP; i++) {
                        ProdCountSP.push(i);
                    }


                    $scope.ProdPageCountSP = ProdCountSP;
                    $scope.ProdCurrentPageSP = 1;
                    $scope.totalProdPageCountSP = ProdEditPageloopcntSP;

                    $scope.ProdCurrentPageSP = 1;
                }

                //$scope.ProdCurrentPageSP = 1;
                dataFactory.Getsubproductspecs($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId, $scope.ProdCurrentPageSP, $scope.ProdCountPerPageSP).success(function (response) {
                    var obj = jQuery.parseJSON(response.Data.Data);
                    $rootScope.SubprodData = obj;

                    for (var i = 0; i < response.Data.Columns.length; i++) {
                        if (response.Data.Columns[i]["Caption"].includes("SUBITEM#")) {
                            response.Data.Columns[i]["Caption"] = response.Data.Columns[i]["Caption"].replace("SUBITEM#", $localStorage.CatalogSubItemNumber);
                        }
                    }


                    $rootScope.columnsSubAtt = response.Data.Columns;
                    angular.forEach($rootScope.SubprodData, function (value) {
                        value.SORT = parseFloat(value.SORT);
                    });
                    $scope.clicksubProductTab = false;
                    if ($rootScope.invertProdId == undefined) {
                        return true;
                    }

                    dataFactory.GetPagenumberInverted($scope.SelectedDataItem.CATALOG_ID, $scope.SelectedDataItem.CATEGORY_ID, $scope.Family.FAMILY_ID, $scope.invertProdId, "SubProduct").success(function (response1) {
                        var obj = response1.split('~');

                        var ProdEditPageloopcnt1 = Math.floor(obj[0] / parseInt($scope.ProdCountPerPage));
                        if ((obj[0] % parseInt($scope.ProdCountPerPage)) > 0) {
                            ProdEditPageloopcnt1 = ProdEditPageloopcnt1 + 1;
                        }

                        var ProdEditPageloopcntMain = Math.floor(obj[0] / parseInt($scope.ProdCountPerPage));
                        if ((obj[0] % parseInt($scope.ProdCountPerPage)) > 0) {
                            ProdEditPageloopcntMain = ProdEditPageloopcntMain + 1;
                        }

                        $rootScope.pageCountMain = ProdEditPageloopcntMain;
                        if ($scope.invertOption == "SubProduct") {
                            if (ProdEditPageloopcnt1 == 0) {
                                ProdEditPageloopcnt1 = 1;
                            }
                            $rootScope.SP(ProdEditPageloopcnt1);
                            if (obj[2] != "") {
                                var ProdEditPageloopcntMain = Math.floor(obj[2] / parseInt($scope.ProdCountPerPage));
                                if ((obj[2] % parseInt($scope.ProdCountPerPage)) > 0) {
                                    ProdEditPageloopcntMain = ProdEditPageloopcntMain + 1;
                                }

                                $rootScope.pageCountMain = ProdEditPageloopcntMain;

                                var grid = $("#subproductsmaingrid").data("kendoGrid");
                                if ($rootScope.pageCountMain != undefined && $rootScope.pageCountMain != "" && $rootScope.pageCountMain != null) {
                                    //grid.dataSource.page($rootScope.pageCountMain);
                                    $rootScope.callProductGridPagingSP(ProdEditPageloopcnt1);
                                }
                                $('#cboProdPageCountSPup').data("kendoDropDownList").span[0].innerHTML = ProdEditPageloopcnt1;
                                $('#cboProdPageCountSPdown').data("kendoDropDownList").span[0].innerHTML = ProdEditPageloopcnt1;
                            }

                        }
                        else {
                            $scope.callProductGridPaging(ProdEditPageloopcnt1);
                        }
                    });
                    //$rootScope.tblDashBoardss.reload();
                }).error(function (error) {
                    options.error(error);
                });
            });
            $scope.GetAllCatalogattributesdataSourceforSubproducts.read();
            $scope.prodfamilyattrdataSourcesubproducts.read();
            //var dataUId = $("#subproductsmaingrid tbody").find("tr[aria-selected='true']");
            //var data_uid = dataUId[0].innerText;
            //$rootScope.fetchDatauid = data_uid;
            // $rootScope.commonvalueupdatesubproducttreeData.read();
            $('#subproductdiv').show();
            $('#ColumnSetupSubproducts').hide();
            $('#cmnvalupdateforsubproduct').hide();
            $('#subproductattrib').hide();
        };


        $rootScope.commonvalueupdatesubproducttreeData = new kendo.data.HierarchicalDataSource({
            type: "json",
            loadOnDemand: false,
            autoBind: false,
            transport: {
                read: function (options) {
                    if ($scope.Family.FAMILY_ID == "") {
                        $scope.familyids = 0;
                    } else {
                        $scope.familyids = $scope.Family.FAMILY_ID;
                    }
                    dataFactory.GetAllcommonvaluesubproducts($localStorage.getCatalogID, $scope.familyids, $localStorage.CategoryID, options.data.id, $scope.ParentProductId).success(function (response) {
                        options.success(response);
                    }).error(function (response) {
                        options.success(response);
                    });

                }
            },
            schema: {
                model: {
                    id: "id",
                    hasChildren: "hasChildren"
                }
            }
        });

        $scope.SaveSubProduct = function () {
            $scope.winNewSubProductCreation.center().close();
            if ($scope.ParentProductId !== 0 && $scope.selecteddRow !== null || this.selectedItem !== "") {
                dataFactory.SaveSubProduct($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, this.selectedItem, $scope.copycat, $scope.getCustomerIDs, $scope.ParentProductId, $rootScope.sortOrder, $scope.selecteddRow).success(function (response) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                    $rootScope.ParentProductId = $scope.ParentProductId;
                    $scope.LoadMainProducts();
                    $scope.LoadSubproductData();
                }).error(function (error) {
                    options.error(error);
                });
            } else {
                if ($scope.selecteddRow !== null) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select the Main Item and continue.',
                        type: "info"
                    });

                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'SUBITEM# Cannot be empty.',
                        type: "error"
                    });
                }

            }
        };

        $scope.GetAllCatalogattributesdataSourceforSubproducts = new kendo.data.DataSource({
            type: "json",
            pageable: { buttonCount: 5 },
            pageSize: 12,
            autoBind: false,
            sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogattributesSubproducts($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId, $scope.ParentProductId, $localStorage.CategoryID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields:
                    {
                        ISAvailable: { type: "boolean" },
                        CATALOG_ID: { editable: false },
                        FAMILY_ID: { editable: false },
                        SUBPRODUCT_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false }
                    }
                }
            },
            group: {
                field: "ATTRIBUTE_TYPE",
                aggregates: [
                    { field: "ATTRIBUTE_TYPE", aggregate: "count" }
                ]
            }
        });

        $scope.mainGridOptionsSubproducts = {
            dataSource: $scope.GetAllCatalogattributesdataSourceforSubproducts,
            pageable: { buttonCount: 5 },
            autoBind: false,
            filterable: { mode: "row" },
            columns: [
                { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionsubproduct($event, this)"></input>', filterable: false },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px", filterable: true },
                {
                    field: "ATTRIBUTE_TYPE",
                    title: "Attribute Type",
                    hidden: true,
                    aggregates: ["count"],
                    groupHeaderTemplate: groupHeaderName
                }]
        };

        $scope.updateSelectionsubproduct = function (e, id) {
            id.dataItem.set("ISAvailable", e.currentTarget.checked);
        };

        $scope.subproductsColumnsetup = function () {

            $scope.GetAllCatalogattributesdataSourceforSubproducts.read();
            $scope.prodfamilyattrdataSourcesubproducts.read();
            $('#subproductdiv').hide();
            $('#columnsetupproduct').hide();
            $('#ColumnSetupSubproducts').show();
            $('#cmnvalupdateforsubproduct').hide();
            $('#subproductattrib').hide();
        };


        $scope.prodfamilyattrdataSourcesubproducts = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            sort: { field: "SORT_ORDER", dir: "asc" },
            pageable: true,
            autoBind: false,
            pageSize: 12,
            transport: {
                read: function (options) {
                    dataFactory.GetprodfamilyAttributessubproducts($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $scope.ParentProductId, $localStorage.CategoryID).success(function (response) {

                        for (var i = 0; response.length > i; i++) {
                            if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                            }



                        }
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ISAvailable: {
                            type: "boolean"
                        },
                        CATALOG_ID: { editable: false },
                        FAMILY_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        SORT_ORDER: { editable: false }
                    }
                }
            }
        });
        $scope.selectedGridOptionssubproducts = {
            dataSource: $scope.prodfamilyattrdataSourcesubproducts,
            pageable: { buttonCount: 5 },
            autoBind: false,
            selectable: "multiple",
            dataBound: onDataBound,
            columns: [
                { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforprodfamily" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionSubproducts1($event, this)"></input>' },
                {
                    field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px",
                    template: "#if(ATTRIBUTE_ID == 1) {##:ATTRIBUTE_NAME##} else{##:ATTRIBUTE_NAME##}#"
                }
            ]
        };
        var updownIndex = null;

        function onDataBound(e) {
            if (updownIndex != null) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {

                    if (updownIndex == i) {
                        var isChecked = this.tbody.find("tr[data-uid='" + view[i].uid + "']").find(".chkbxforprodfamily").attr("checked");
                        if (isChecked) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                                .addClass("k-state-selected");
                        }

                    }
                }

                updownIndex = null;
            }
        }

        $scope.updateSelectionSubproducts1 = function (e, id) {
            id.dataItem.set("ISAvailable", e.currentTarget.checked);
        };

        $scope.SavePublishAttributesforsubproducts = function () {
            dataFactory.SavePublishAttributesforsubproducts($localStorage.CategoryID, $scope.ParentProductId, $scope.GetAllCatalogattributesdataSourceforSubproducts._data).success(function (response) {
                $scope.GetAllCatalogattributesdataSourceforSubproducts.read();
                $scope.prodfamilyattrdataSourcesubproducts.read();
            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.UnPublishAttributesSubproducts = function () {
            $('#ColumnSetupSubproducts').hide();
            dataFactory.UnPublishAttributessubproducts($scope.ParentProductId, $scope.prodfamilyattrdataSourcesubproducts._data).success(function (response) {
                $scope.GetAllCatalogattributesdataSourceforSubproducts.read();
                $scope.prodfamilyattrdataSourcesubproducts.read();
                $scope.LoadSubproductData();
                $('#subproductdiv').show();
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.LoadSubproductData = function () {
            if ($scope.ProdCurrentPageSP == null) {
                $scope.ProdCurrentPageSP = 1;
            }
            dataFactory.Getsubproductspecs($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId, $scope.ProdCurrentPageSP, $scope.ProdCountPerPageSP).success(function (response) {
                var obj = jQuery.parseJSON(response.Data.Data);
                $rootScope.SubprodData = obj;

                $rootScope.columnsSubAtt = response.Data.Columns;
                angular.forEach($rootScope.SubprodData, function (value) {
                    value.SORT = parseFloat(value.SORT);
                });
                if ($rootScope.invertProdId == undefined) {
                    if ($rootScope.SubprodData.length == 1) {
                        $('#cboProdPageCountSPup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageSP == null ? 1 : $scope.ProdCurrentPageSP == '' ? 1 : parseInt($scope.ProdCurrentPageSP);
                        $('#cboProdPageCountSPdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageSP == null ? 1 : $scope.ProdCurrentPageSP == '' ? 1 : parseInt($scope.ProdCurrentPageSP);;
                    }
                    return true;
                }
                dataFactory.GetPagenumberInverted($rootScope.selecetedCatalogId, $localStorage.CategoryID, $rootScope.selecetedFamilyId, $rootScope.invertProdId, "SubProduct").success(function (response1) {

                    if (response1 != null && response1 != "null" && response1 != "~") {

                        var obj = response1.split('~');
                        var ProdEditPageloopcnt1 = Math.floor(obj[0] / parseInt($scope.ProdCountPerPage));
                        if ((obj[0] % parseInt($scope.ProdCountPerPage)) > 0) {
                            ProdEditPageloopcnt1 = ProdEditPageloopcnt1 + 1;
                        }
                        if (ProdEditPageloopcnt1 == 1) {
                            var ProdEditPageloopcntMain = Math.floor(obj[2] / parseInt($scope.ProdCountPerPage));
                            if ((obj[2] % parseInt($scope.ProdCountPerPage)) > 0) {
                                ProdEditPageloopcntMain = ProdEditPageloopcntMain + 1;
                            }
                            $('#subproductdiv').show();
                            $('#ColumnSetupSubproducts').hide();
                            if (ProdEditPageloopcntMain == 1) {
                                return true;
                            }
                            $rootScope.pageCountMain = ProdEditPageloopcntMain;
                            var grid = $("#subproductsmaingrid").data("kendoGrid");
                            grid.dataSource.page($rootScope.pageCountMain);
                            return true;
                        }
                        if (ProdEditPageloopcnt1 == 0) {
                            ProdEditPageloopcnt1 = 1;
                        }
                        $rootScope.callProductGridPagingSP(ProdEditPageloopcnt1);
                        if (obj[2] != "") {
                            var ProdEditPageloopcntMain = Math.floor(obj[2] / parseInt($scope.ProdCountPerPage));
                            if ((obj[2] % parseInt($scope.ProdCountPerPage)) > 0) {
                                ProdEditPageloopcntMain = ProdEditPageloopcntMain + 1;
                            }
                            $rootScope.totalProdPageCountSP = ProdEditPageloopcntMain;
                            $rootScope.pageCountMain = ProdEditPageloopcntMain;

                            var grid = $("#subproductsmaingrid").data("kendoGrid");
                            if ($rootScope.pageCountMain != undefined && $rootScope.pageCountMain != "" && $rootScope.pageCountMain != null) {
                                grid.dataSource.page($rootScope.pageCountMain);
                                $rootScope.callProductGridPagingSP(ProdEditPageloopcnt1);
                            }
                        }
                        $('#cboProdPageCountSPup').data("kendoDropDownList").span[0].innerHTML = ProdEditPageloopcnt1;
                        $('#cboProdPageCountSPdown').data("kendoDropDownList").span[0].innerHTML = ProdEditPageloopcnt1;
                    }
                });
                ////$rootScope.tblDashBoardss.reload();

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.DeletePublishAttributessubproducts = function () {
            var g = $scope.prodselectgridsubproducts.select();
            var data = [];
            for (var i = 0; i < g.length; i++) {
                var selectedItem = $scope.prodselectgridsubproducts.dataItem(g[i]);
                data.push(selectedItem);
            }
            dataFactory.DeletePublishAttributessubproducts($scope.ParentProductId, $localStorage.CategoryID, data).success(function (response) {
                $scope.GetAllCatalogattributesdataSourceforSubproducts.read();
                $scope.prodfamilyattrdataSourcesubproducts.read();
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.RefreshProductAttributessubproducts = function () {
            $scope.GetAllCatalogattributesdataSourceforSubproducts.read();
            $scope.prodfamilyattrdataSourcesubproducts.read();
        };
        $scope.CancelProductAttributessubproducts = function () {
            $('#ColumnSetupSubproducts').hide();
            $('#subproductdiv').show();
        };
        $scope.callpagingSP = function (pageno, row) {
            //
            var displayAttribures = "";
            var tt = $('#textboxvalue4');
            if ($scope.val_type === "true") {
                angular.forEach(row, function (value, key) {
                    if (key.contains("OBJ")) {
                        var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];



                        if (value != null && $("#textBoxValue" + key.split('_')[4]).length != 0) {
                            var id = sa.ATTRIBUTE_ID;

                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {

                                row[key] = $("#textBoxValue" + id)[0].nextElementSibling.innerHTML;
                            }
                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
                                row[key] = "";
                            }
                        }
                        else if ($("#textBoxValue" + key.split('_')[4]).length != 0 && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {


                            row[key] = $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
                        }
                        //To check if the attribute is a value required field
                        if (sa != undefined) {
                            if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {
                                if (displayAttribures == '') {
                                    displayAttribures = "The following attribute values are required,\n";
                                }
                                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                            }
                        }
                    }

                });
                angular.copy(row, $scope.updateproductspecs);
            }

            showPageLoadingSpinner();
            $rootScope.ajaxLoaderDivShowSub = true;
            if (pageno != null) {
                if (pageno == "NEXT") {
                    pageno = ($scope.SubProdSpecsCurrentPage < $scope.totalSpecsSubProdPageCount) ? parseInt($scope.SubProdSpecsCurrentPage) + 1 : $scope.totalSpecsSubProdPageCount;
                    $scope.SubProdSpecsCurrentPage = pageno;
                    //$('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }
                else if (pageno == "PREV") {
                    pageno = ($scope.SubProdSpecsCurrentPage > 1) ? parseInt($scope.SubProdSpecsCurrentPage) - 1 : 1;
                    $scope.SubProdSpecsCurrentPage = pageno;
                    //$('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }
                else if (pageno == $scope.totalSpecsSubProdPageCount) {
                    $scope.SubProdSpecsCurrentPage = $scope.totalSpecsSubProdPageCount;
                    //$('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }
                else {
                    $scope.SubProdSpecsCurrentPage = pageno;
                    //$('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }
                //  $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = $scope.SubProdSpecsCurrentPage;

            }
            var pagingdropdown = $('#cboProdPageCountDown').data("kendoDropDownList")
            pagingdropdown.text($scope.SubProdSpecsCurrentPage.toString());
            var pageData = $scope.loopCountSP[pageno - 1];
            $scope.fromPageNoSP = pageData.FROM_PAGE_NO;
            $scope.toPageNoSP = pageData.TO_PAGE_NO;
            $scope.currentPageSP = pageData.PAGE_NO - 1;

            // var attributeWithoutSP = [];
            //var calcAttributestempSP = [];
            //angular.forEach($scope.attributesOLDSP, function (value, key) {

            //    if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT')
            //        attributeWithoutSP.push(value);
            //});
            if (attributeWithoutSP.length < 10) {

                $scope.toPageNo = attributeWithoutSP.length;
            }
            var attributestempSP = [];
            for (var i = $scope.fromPageNoSP - 1; i < $scope.toPageNoSP; i++) {
                if (attributeWithoutSP[i] != undefined)
                    attributestempSP.push(attributeWithoutSP[i]);
            }
            $scope.attributes = attributestempSP;
            $scope.attrSubLength = $scope.attributes.length;

            for (var i = $scope.fromPageNoSP - 1; i < $scope.toPageNoSP; i++) {
                if (attributeWithoutSP[i] != undefined) {
                    var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithoutSP[i].split('__')[2] });
                    if (tempattrvals.length != 0) {
                        var theString = tempattrvals[0].ATTRIBUTE_ID;
                        var uimask = "";
                        tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                        if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                            theString = 0;
                        }
                        if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                            var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                            pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                            pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                            var reg = new RegExp(pattern);
                            uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                            tempattrvals[0].attributePattern = reg;
                        } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                            tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                            uimask = tempattrvals[0].DECIMAL;
                        } else {
                            tempattrvals[0].attributePattern = 524288;
                            uimask = 524288;
                        }
                        //$scope.attributePattern[name] = reg;
                        tempattrvals[0].uimask = uimask;
                        $scope.selectedRowDynamicAttributes[theString] = [];
                        $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                        //  $scope.selectedRowDynamicAttributes[theString] = uimask;
                    }
                }
            }

            $scope.iterationCount = 0;
            $scope.isUIReleased = 0;
            for (var i = $scope.fromPageNoSP - 1; i < $scope.toPageNoSP; i++) {
                if (attributeWithoutSP[i] != undefined && attributeWithoutSP[i].contains("OBJ")) {
                    if (attributeWithoutSP[i].split('__')[2] !== "0") {
                        var sa = $scope.selectedRowDynamicAttributes[attributeWithoutSP[i].split('__')[2]];
                        if (sa.USE_PICKLIST) {
                            $scope.iterationCount++;
                        }
                    }
                }
            }
            // $rootScope.selectedRowDynamic = [];
            //$scope.iterationPicklistCount = 0;
            //for (var i = $scope.fromPageNoSP; i < $scope.toPageNoSP; i++) {
            //    if (attributeWithoutSP[i].contains("OBJ")) {
            //        if (attributeWithoutSP[i].split('__')[2] !== "0") {
            //            var sa = $scope.selectedRowDynamicAttributes[attributeWithoutSP[i].split('__')[2]];
            //            if (sa.USE_PICKLIST) {
            //                $scope.GetPickListData(sa.ATTRIBUTE_ID, sa.PICKLIST_NAME);
            //            }
            //        }
            //    }
            //}
            if ($scope.isUIReleased == 0) {

                angular.copy($scope.updateproductspecs, $scope.selectedRow);
                blockUI.stop();
            }
            $route.reload();

            $timeout(function () {
                $rootScope.ajaxLoaderDivShowSub = false;
            }, 500)
        };
        $scope.selectedRow = {};
        $scope.selectedRowSub = {};
        $scope.selectedRowDynamic = [];
        $scope.UpdateProductItemSubProduct = function (data) {

            // blockUI.start();
            $scope.selectedRowSub = {};
            $scope.selectedRowEdit = {};

            $q.all([$scope.UpdateProductItemSubProductStart(data)]).then(function (response) {


                return;
            });
        }

        $scope.UpdateProductItemSubProductStart = function (data) {


            $scope.attributes = [];
            $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
            $scope.winAddOrEditProduct.refresh({ url: "../Views/App/Partials/dynamicGridItemEditPopupforSubproduct.html" });
            $scope.winAddOrEditProduct.title("SubProduct Specifications");
            $rootScope.windows_Open();

            $scope.SubProductSortOrder.read();
            //  blockUI.start();

            blockUI.start();

            $rootScope.ajaxLoaderDivShow = true;
            $scope.currentPageSP = '0';
            $scope.selectedRowSub = {};
            $scope.selectedRowEdit = {};

            showPageLoadingSpinner();
            $('#dynamicsubsort').data = '';
            // $scope.selectedRowDynamic = $rootScope.selectedRowDynamicPickList;
            $scope.updateproductspecsEdit = {};
            angular.copy(data.Data, $scope.updateproductspecsEdit);
            //$scope.subsorting = $scope.updateproductspecs["SORT"];
            // angular.copy($scope.updateproductspecs, $scope.selectedRow);
            // $scope.attributes = Object.keys(data.Data);
            $scope.attributesOLDSP = Object.keys(data.Data);
            attributeWithoutSP = [];
            if ($scope.catalogids !== 0 && $scope.familyids !== 0) {
                dataFactory.GetAttributeDetailsSubproduct(data.Data.CATALOG_ID, data.Data.FAMILY_ID, $scope.ParentProductId).success(function (response) {
                    if (response != null) {
                        $scope.selectedRowAttributes = response;
                        var calcAttributestempSP = [];
                        angular.forEach($scope.attributesOLDSP, function (value, key) {
                            if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2') || value == 'WORKFLOW STATUS')
                                attributeWithoutSP.push(value);
                        });
                        $scope.attributescalcSP = attributeWithoutSP;
                        angular.forEach($scope.selectedRowAttributes, function (value, key) {
                            if (value.IS_CALCULATED)
                                calcAttributestempSP.push(value);
                        });
                        $scope.fromPageNoSP = 0;
                        $scope.toPageNoSP = 150;
                        if (attributeWithoutSP.length < 150) {
                            $scope.toPageNoSP = attributeWithoutSP.length;
                        }
                        var attributestempSP = [];
                        for (var i = $scope.fromPageNoSP; i < $scope.toPageNoSP; i++) {
                            attributestempSP.push(attributeWithoutSP[i]);
                        }
                        $scope.attributes1 = attributestempSP;
                        $scope.attrSubLength = $scope.attributes1.length;
                        // $scope.SubProductSortOrder.read();
                        $scope.SubProdSpecsCountPerPage = "150";
                        $scope.SubProdSpecsCurrentPage = "1";
                        $scope.loopCountSP = [];
                        $scope.loopEditSubProdsCount = [];
                        var loopcntSP = Math.floor(attributeWithoutSP.length / parseInt($scope.SubProdSpecsCountPerPage));
                        var loopEditCntSP = Math.floor(attributeWithoutSP.length / parseInt($scope.SubProdSpecsCountPerPage));
                        if ((attributeWithoutSP.length % parseInt($scope.SubProdSpecsCountPerPage)) > 0) {
                            loopcntSP = loopcntSP + 1;
                            loopEditCntSP = loopEditCntSP + 1;
                        }
                        $scope.SubProdSpecsPageCount = $scope.loopCountSP;
                        $scope.totalSpecsSubProdPageCount = loopEditCntSP;
                        for (var i = 0; i < loopcntSP; i++) {
                            // $scope.loopCount.push(i);
                            if (((i * 10) + 10) < attributeWithoutSP.length) {
                                $scope.loopCountSP.push({
                                    PAGE_NO: (i + 1),
                                    FROM_PAGE_NO: (i * parseInt($scope.SubProdSpecsCountPerPage)) + 1,
                                    TO_PAGE_NO: (i * parseInt($scope.SubProdSpecsCountPerPage)) + parseInt($scope.SubProdSpecsCountPerPage)
                                });
                            } else {

                                $scope.loopCountSP.push({
                                    PAGE_NO: (i + 1),
                                    FROM_PAGE_NO: (i * parseInt($scope.SubProdSpecsCountPerPage)) + 1,
                                    TO_PAGE_NO: attributeWithoutSP.length
                                });
                            }
                        }
                        $scope.selectedRowEdit = {};
                        angular.copy(data.Data, $scope.selectedRowEdit);
                        for (var i = $scope.fromPageNoSP; i < $scope.toPageNoSP; i++) {
                            if (attributeWithoutSP[i].contains('__')) {
                                var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithoutSP[i].split('__')[2] });
                                if (tempattrvals.length != 0) {
                                    var uimask = "";
                                    var theString = tempattrvals[0].ATTRIBUTE_ID;
                                    tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                                    if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                                        theString = 0;
                                    }
                                    var subval = attributeWithoutSP[i];
                                    if (tempattrvals[0].ATTRIBUTE_ID === 1) {
                                        $scope.Subitem = $scope.selectedRowEdit[subval];
                                    }
                                    if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                                        var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                        pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                                        pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                                        var reg = new RegExp(pattern);
                                        uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                                        tempattrvals[0].attributePattern = reg;
                                    } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                        tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                                        uimask = tempattrvals[0].DECIMAL;
                                    } else {
                                        tempattrvals[0].attributePattern = 524288;
                                        uimask = 524288;
                                    }
                                    //$scope.attributePattern[name] = reg;
                                    tempattrvals[0].uimask = uimask;
                                    $scope.selectedRowDynamicAttributes[theString] = [];
                                    $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                                    // $scope.selectedRowDynamicAttributes[theString] = uimask;
                                }
                            }
                        }

                        $scope.iterationCount = 0;
                        $scope.isUIReleased = 0;
                        for (var i = $scope.fromPageNoSP; i < $scope.toPageNoSP; i++) {
                            if (attributeWithoutSP[i].contains("OBJ")) {
                                if (attributeWithoutSP[i].split('__')[2] !== "0") {
                                    var sa = $scope.selectedRowDynamicAttributes[attributeWithoutSP[i].split('__')[2]];
                                    if (sa.USE_PICKLIST) {
                                        $scope.iterationCount++;
                                    }
                                }
                            }
                        }
                        $rootScope.selectedRowDynamic = [];
                        $scope.iterationPicklistCount = 0;
                        for (var i = $scope.fromPageNoSP; i < $scope.toPageNoSP; i++) {
                            if (attributeWithoutSP[i].contains("OBJ")) {
                                if (attributeWithoutSP[i].split('__')[2] !== "0") {
                                    var sa = $scope.selectedRowDynamicAttributes[attributeWithoutSP[i].split('__')[2]];
                                    if (sa.USE_PICKLIST) {
                                        $scope.GetPickListData(sa.ATTRIBUTE_ID, sa.PICKLIST_NAME);
                                    }
                                }

                            }
                        }

                        if ($scope.isUIReleased == 0) {

                            $scope.selectedRowSub = {};

                            $scope.updateproductspecs = data.Data;
                            angular.copy($scope.updateproductspecs, $scope.selectedRowSub);
                            $scope.IsSortval = true;

                            $scope.SubProductSortOrder.read();

                            $scope.attributes = $scope.attributes1;
                            //  $scope.ProductSortOrder.read();

                            $("#resultsub").hide();

                            blockUI.stop();
                            $timeout(function () {
                                blockUI.stop();
                            }, 100);
                        }

                    }

                }).error(function (error) {
                    options.error(error);
                });
            }

        };

        $scope.btnSubProductPreview = function (data) {
            $("#subproductpreview").show();
            $("#divsubProductGrid").hide();
            $("#subproductpaging1").hide();
            dataFactory.SubProductPreview(data.Data.CATALOG_ID, data.Data.FAMILY_ID, data.Data.SUBPRODUCT_ID, true, $scope.selecetedCategoryId).success(function (response) {
                if (response != null) {
                    $scope.subProductPreviewText = response.replace("SUBITEM#", $localStorage.CatalogSubItemNumber);
                }
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.selectedsubproductId = function (product_Id, e, family_Id) {
            $scope.CheckedProductids = true;
            if ($scope.CheckedCheckbox == true) {

                $scope.subproduct_ids = "";
            }
            if (e.currentTarget.checked === true) {
                $scope.Family_ID = family_Id;
                $scope.subproduct_ids = $scope.subproduct_ids + "," + product_Id;

            } else if (e.currentTarget.checked === false) {
                $rootScope.valueofcheckall = false;
                var val = $scope.subproduct_ids;
                var res = val.split(",");
                var Product_IDs = $scope.subproduct_ids;
                if (res.length > 0) {
                    for (var i = 0; i < res.length; i++) {
                        if (res[i] === product_Id) {
                            Product_IDs = $scope.subproduct_ids.replace(product_Id, "");
                        }
                    }
                }
                Product_IDs = Product_IDs.replace(",,", ",");
                $scope.subproduct_ids = Product_IDs.replace(",", "");
                $scope.CheckedCheckbox = false;
            }
        };


        $scope.CheckAllsubproducts = function (e, f) {
            if (e.currentTarget.checked === true) {
                $rootScope.valueofcheckbox = true;
                $scope.BulkIds = f;
                angular.forEach($scope.BulkIds, function (key) {
                    $scope.subproduct_ids = $scope.subproduct_ids + "," + key.PRODUCT_ID;
                    $scope.Family_ID = key.FAMILY_ID;
                });
            } else {
                $scope.subproduct_ids = "";
                $scope.LoadSubproductData();
                $rootScope.valueofcheckbox = false;
                $rootScope.valueofcheckall = false;
            }
        };
        $scope.cutcopyFlag = "empty";
        $scope.subproductscopy = function (e) {
            $(".subProductsContext").hide();

            if ($rootScope.ProductCount >= ($rootScope.ProductSKUCount * ($rootScope.SKUAlertPercentage / 100))) {

                if ($rootScope.ProductCount > 0 && $rootScope.ProductSKUCount > 0) {
                    $rootScope.SKUAlertPercentageCalculated = Math.round(($rootScope.ProductCount / $rootScope.ProductSKUCount) * 100);
                }
                $scope.skuAlertOptions.refresh({ url: "../views/app/partials/skuAlert.html" });
                $scope.skuAlertOptions.center().open();
            }
            if ($scope.Family_ID != 0 && $scope.subproduct_ids.trim() != "") {
                $scope.cutcopyFlag = "Copy";
                $scope.OldParentProductId = $scope.ParentProductId;
                $scope.oldFamily_Id = $scope.Family_ID;
                $scope.passselecetedCategoryId = $scope.selecetedCategoryId;
                if ($rootScope.Dashboards == true) {
                    $scope.oldcatalogiddump = $rootScope.selecetedCatalogId;
                }
                $scope.oldcatloggid = $scope.oldcatalogiddump;
                $scope.subproductids = scope.subproduct_ids;
                // alert("Products Copied.");
            } else {

                //alert("Please Select Subproducts and Continue.");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select Sub Items and continue.',
                    type: "info"
                });

            }
        };
        $scope.subproductspaste = function (e) {

            $(".subProductsContext").hide();

            if (e == 'buttonclick') {
                $rootScope.sortOrder = 0;
            }
            if ($scope.ParentProductId != 0) {
                var selProductCnt = $scope.subproduct_ids.split(',').length - 1;
                if ($rootScope.AllowDuplicateItem_PartNum) {
                    $rootScope.ProductCount = $rootScope.ProductCount + selProductCnt;
                }

                if (($rootScope.ProductCount) < $rootScope.ProductSKUCount && $scope.cutcopyFlag === "Copy") {
                    $rootScope.ProductCount = $rootScope.ProductCount + selProductCnt;
                    //if ($scope.ParentProductId != $scope.OldParentProductId) {

                    dataFactory.PasteSubProduct($scope.oldFamily_Id, $scope.Family.FAMILY_ID, $scope.subproductids, $scope.cutcopyFlag, $scope.ParentProductId, $scope.getCustomerIDs, $rootScope.selecetedCatalogId, $scope.OldParentProductId, $localStorage.CategoryID, $scope.passselecetedCategoryId, $scope.oldcatloggid, $rootScope.sortOrder).success(function (response) {
                        //  alert(response);
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });

                        $scope.Family_ID = 0;
                        $scope.subproduct_ids = "";
                        $scope.passselecetedCategoryId = "";
                        $scope.OldParentProductId = 0;
                        $rootScope.ParentProductId = $scope.ParentProductId;
                        $scope.LoadMainProducts();
                        //$scope.MainProducts.read();
                        $scope.LoadSubproductData();
                        $rootScope.valueofcheckall = false;
                        $rootScope.valueofcheckbox = false;
                        $scope.cutcopyFlag = "empty";
                    }).error(function (error) {
                        options.error(error);
                    });
                    //} else {
                    //    //alert("Please Select a Different Product and Continue.");
                    //    $.msgBox({
                    //        title: $localStorage.ProdcutTitle,
                    //        content: 'You have selected the same Product, please select a different Product and continue.',
                    //        type: "info"
                    //    });

                    //}
                } else {
                    if ($scope.cutcopyFlag === "Copy")

                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'You have exceeded the Maximum No. of SKUs as per your Plan.',
                            type: "error"
                        });

                    else {
                        //if ($scope.ParentProductId != $scope.OldParentProductId) {
                        dataFactory.PasteSubProduct($scope.oldFamily_Id, $scope.Family.FAMILY_ID, $scope.subproductids, $scope.cutcopyFlag, $scope.ParentProductId, $scope.getCustomerIDs, $rootScope.selecetedCatalogId, $scope.OldParentProductId, $localStorage.CategoryID, $scope.passselecetedCategoryId, $scope.oldcatloggid, $rootScope.sortOrder).success(function (response) {
                            //alert(response);

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "error"
                            });
                            $scope.Family_ID = 0;
                            $scope.subproduct_ids = "";
                            $scope.OldParentProductId = 0;
                            $scope.LoadMainProducts();
                            $scope.LoadSubproductData();
                            $rootScope.valueofcheckall = false;
                            $rootScope.valueofcheckbox = false;
                            $scope.cutcopyFlag = "empty";
                        }).error(function (error) {
                            options.error(error);
                        });
                        //} else {

                        //    // alert("Please Select a Different Product and Continue.");
                        //    $.msgBox({
                        //        title: $localStorage.ProdcutTitle,
                        //        content: 'You have selected the same Product, please select a different Product and continue.',
                        //        type: "error"
                        //    });
                        //}
                    }
                }
            } else {
                // alert("Please Select Products and Continue.");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Select Items and Continue.',
                    type: "info"
                });

            }
        };

        $scope.mainproductsclick = function () {

            $('#subproductdiv').hide();
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();
            $("#divProductGrid").show();
            if ($rootScope.Mainprodcount > 0) {
                $("#productpaging1").show();
            }
            $("#gridproduct").show();
            $("#productpreview").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
            $("#cmnvalupdate").hide();
            $('#mainProductsMenu').show();
            $('#subProductsMenu').hide();

            // $scope.callProductGridPagePer($scope.gridCurrentPage);
            var tbs = $(".productgridtabstripclass").kendoTabStrip({

                change: function (e) {
                    //some code
                }
            }).data("kendoTabStrip");
            tbs.select(0);
        };
        $scope.subproductscut = function (e) {
            $(".subProductsContext").hide();
            if ($scope.Family_ID != 0 && $scope.subproduct_ids.trim() != "") {
                $scope.cutcopyFlag = "Cut";
                $scope.OldParentProductId = $scope.ParentProductId;
                $scope.oldFamily_Id = $scope.Family_ID;
                $scope.passselecetedCategoryId = $scope.selecetedCategoryId;
                $scope.oldcatloggid = $scope.oldcatalogiddump;
                $scope.subproductids = $scope.subproduct_ids;
            } else {

                // alert("Please Select Subproducts and Continue.");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select Sub Items and continue.',
                    type: "info"
                });
                cr

            }
        };

        $scope.DeleteSubProductItem = function (selectedRow) {
            if (selectedRow == "deleteDetach") {

                $scope.selectedSubProduct_Id = $scope.menusubProductIds;
            }
            else {
                $scope.selectedSubProduct_Id = selectedRow.Data.SUBPRODUCT_ID;
            }

            if ($scope.ParentProductId !== 0) {

                $(".subProductsContext").hide();

                //if ($scope.selectedFamilyID !== "") {
                //selectedRow.FAMILY_ID, selectedRow.CATEGORY_ID, $rootScope.selecetedCatalogId, selectedRow.PRODUCT_ID
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Are you sure you want to delete Sub Item Association?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {
                        var bool = false;
                        if (result === "Yes") {
                            bool = true;
                        }
                        if (bool === true)

                            dataFactory.DeleteSubProductPermanent($scope.ParentProductId, $scope.selectedSubProduct_Id, $rootScope.selecetedCatalogId).success(function (response) {
                                // alert(response);

                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                                $scope.LoadMainProducts();
                                $('#subproductdiv').show();
                                $scope.LoadSubproductData();

                            }).error(function (error) {
                                options.error(error);
                            });
                    }
                });
            }
        };
        $scope.BtnMoveUpClick = function () {
            var g = $scope.prodselectgrid.select();
            if (g.length === 1) {
                g.each(function (index, row) {
                    var selectedItem = $scope.prodselectgrid.dataItem(row);
                    var data = $scope.prodselectgrid.dataSource.data();
                    var dataRows = $scope.prodselectgrid.items();
                    var selectedRowIndex = dataRows.index(g);
                    // var sortOrder = data[selectedRowIndex - 1].SORT_ORDER;
                    if (selectedItem.SORT_ORDER !== 1000) {
                        dataFactory.BtnMoveUpClick(selectedItem.SORT_ORDER - 1, selectedItem).success(function (response) {
                            $scope.GetAllCatalogattributesdataSource.read();
                            $scope.prodfamilyattrdataSource.read();
                            updownIndex = selectedRowIndex - 1;
                        }).error(function (error) {
                            options.error(error);
                        });
                    }

                    else {
                        //alert("Selected Attribute Is Not Part of the Product Table Display.");

                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Selected Attribute cannot be moved as it is not part of the Item Table Display.',
                            type: "info"
                        });
                    }

                });
            } else {
                // alert("Please Select At Least One Attribute.");

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one Attribute.',
                    type: "info"
                });

            }
        };


        $scope.BtnMoveDownClick = function () {

            var g = $scope.prodselectgrid.select();
            if (g.length === 1) {
                g.each(function (index, row) {
                    var selectedItem = $scope.prodselectgrid.dataItem(row);
                    var data = $scope.prodselectgrid.dataSource.data();
                    var dataRows = $scope.prodselectgrid.items();
                    var selectedRowIndex = dataRows.index(g);
                    //   var sortOrder = data[selectedRowIndex + 1].SORT_ORDER;
                    if (selectedItem.SORT_ORDER !== 1000) {
                        dataFactory.BtnMoveDownClick(selectedItem.SORT_ORDER + 1, selectedItem).success(function (response) {
                            $scope.GetAllCatalogattributesdataSource.read();
                            $scope.prodfamilyattrdataSource.read();
                            updownIndex = selectedRowIndex + 1;
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                    else {
                        //alert("Selected Attribute Is Not Part of the Product Table Display.");
                        $.msgBox(
                            {
                                title: $localStorage.ProdcutTitle,
                                content: 'Selected Attribute cannot be moved as it is not part of the Item Table Display.',
                                type: "info"
                            }
                        );
                    }
                });


            } else {
                // alert("Please Select At Least One Attribute.");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one Attribute.',
                    type: "info"
                });
            }
        };

        $scope.$on("Active_SubProduct", function (event, selectedCategoryId, categoryId, Mainproduct) {
            if (Mainproduct !== 0) {
                $scope.rendersubproducts = true;
                $scope.ParentProductId = Mainproduct;
                $scope.LoadSubproductData();
            }
            else {
                $scope.LoadMainProducts();
            }
        });

        $scope.setClickedRowsubproduct = function (index) {
            $scope.selectedRowValuesubproduct = index;
        };
        //--------To get EnableSubProduct value from preference page-------
        $rootScope.getEnableSubProduct = function () {
            //$scope.CustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
            dataFactory.getEnableSubProduct().success(function (response) {
                $rootScope.EnableSubProduct = response;
                $localStorage.EnableSubProductLocal = response;
            })
        };

        // Work with separate path - start

        $scope.GetFilePathSavedLocation = function () {
            dataFactory.GetServerFilePath().success(function (response) {
                $scope.getPathFromWebConfig = response[0];
                $scope.isLocalOrServer = response[1];

            }).error(function (error) {
                options.error(error);
            });
        }

        // End


        $scope.init = function () {
            $('#panel').hide();
            $rootScope.getEnableSubProduct();
            $('#btnsaveclose').prop('disabled', true);
            if ($localStorage.getCatalogID === undefined) {
                $rootScope.selecetedCatalogId = 0;
            } else {
                $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
            }
            if ($localStorage.getCustomerID == undefined) {
                $scope.getCustomerIDs = 0;
            }
            else {
                $scope.getCustomerIDs = $localStorage.getCustomerID;
            }
            dataFactory.GetCurretUserInfo().success(function (response) {
                $scope.AllowDublicatePartNoss = response.CustomerDetails.AllowDublicatePartNo;
            }).error(function (error) {
                options.error(error);
            });
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();
            $("#gridproduct").show();
            $("#productpreview").hide();
            $("#copy").hide();
            $("#cut").hide();
            $("#paste").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();
            $("#Familyimport").hide();
            $("#filterAttributePack").show();
            $("#attributePack").hide();
            $scope.valueofShowSubProducts = false;
            // To Work with separate path  
            $scope.GetFilePathSavedLocation();
            // End
            $timeout(function () {
                $rootScope.ProductGroupNameDataSource.read();
            }, 200);
        };

        $scope.LoadCommonValueProducts = function () {
            $("#myDIV").hide();
            $("#productgridtabstrip").hide();
            $(".productgridtabstripclass").hide();
            $("#attributePack").hide();
            $('#productpreview').hide();
            $('#configuratorproducts').hide();
            $("#columnsetupproduct").hide();
            $("#cmnvalupdate").show();
            $("#newProductBtn1").show();
            $("#Familyimport").hide();
            $("#productpaging1").hide();
            $("#columnsetup").hide();
            $(".column_setup").hide();
            $("#columnsetupproduct").hide();
            $scope.rendercommonvalueupdate = true;
            $rootScope.commonitemupdater = true;
            $rootScope.usercommon = true;
            $rootScope.commonvalueupdatetreeData.read();
            $rootScope.productAttrDatasource.read();
           // $rootScope.commonitemupdater = true;



        };
        $scope.LoadCommonValueSubProducts = function () {
            $('edit_mycommonForm').hide();
            $scope.showsubproduct_grid = false;
            $('#cmnvalupdateforsubproduct').show();
            $('#subproductattrib').show();
            $('#subproductdiv').hide();
            $('#ColumnSetupSubproducts').hide();
            $("#Familyimport").hide();

            $scope.$broadcast("CommonUpdateClick");
            $scope.rendercommonvalueupdatesubproduct = true;
        };

        $scope.ProductSortOrder = new kendo.data.DataSource({
            type: "json", autoBind: false,
            serverFiltering: false,
            transport: {
                read: function (options) {

                    dataFactory.getProductSortorderForItem($scope.FAMILY_ID).success(function (response) {
                        options.success(response);
                        
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.SortingChanges = function (e) {
            if (e.sender.value() !== "") {

                $scope.selectedRow["SORT"] = e.sender.value();
            }
        };

        //------------------Family Import ---------------
        $scope.sheetselection = false;
        $scope.Family_import = function () {
            $("#productpaging1").hide();
            $scope.familyimport = true;
            $scope.sheetselection = false;
            $scope.myDIV = false
            $('#myDIV').css('display', 'none');
            $("#columnsetup").hide();
            $(".column_setup").hide();
            $("#productgridtabstrip").hide();
            $("#filterAttributePack").hide();
            $(".productgridtabstripclass").hide();
            $("#productpreview").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();
            $("#Familyimport").show();
            $("#importSuccessWindow").hide();
            $("#importerror").hide();
            $("#importpicklist").hide();
            $("#importErrorWindow").hide();
            $("#importTableSheetWindow").show();
            $("#importTableSheetSlectectionWindow").hide();
            $("#attributePack").hide();
            $("#cmnvalupdate").hide();
            $scope.Reset();
        };
        $scope.btnback11 = function () {
            $("#myDIV").show();
            $("#productpaging").show();
            $('#filterAttributePack').show();
            $("#associatedgrid").hide();
            $scope.UploadFile = "";
            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $localStorage.CategoryID);
            $rootScope.SelectedFileForUploadnamefamilyproduct = "";
            $scope.sheetselection = false;
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();
            $("#divProductGrid").show();
            $("#productpaging1").show();
            $("#productpreview").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();
            $("#Familyimport").hide();
            $("#importSuccessWindow").hide();
            $("#importSuccessWindow1").hide();
            $("#importerror").hide();
            $("#importpicklist").hide();
            $("#importErrorWindow").hide();
            $("#importSuccessWindowsubproduct").hide();
            $("#importerrorWindowsubproduct").hide();
            $scope.myDIV = true;
            // $scope.IsFileValid = false;
            var $el = $('#importfamily');
            $el.wrap('<form>').closest('form').get(0).reset();
            $el.unwrap();
            $scope.btnimport = true;
            $scope.SelectedFileForUpload = "";
            $scope.excelPath = "";
            $scope.ImportExcelSheetSelction.read();

            $scope.excelname = "";
        };
        $scope.OpenSubProductDefaultTab = function () {

            $('#subproductdiv').show();
            $('#ColumnSetupSubproducts').hide();
            $('#cmnvalupdateforsubproduct').hide();
            $scope.LoadSubproductData();
            // $rootScope.tblDashBoardss.reload();
        };
        //------------END-----------

        //$scope.prjDataBound = function (e) {
        //    if (e.sender._data().length > 0) {
        //     }
        //};


        /// -- Create new Product & New SubProduct-----------------
        //For New Product Creation
        $scope.newitemcli = true;
        $scope.newitemclisub = true;

        $scope.NewProductscreate = function () {
            $scope.currentPage = '0';
            dataFactory.getprodattr($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, true, $scope.Family.CATEGORY_ID).success(function (response) {
                $scope.selectedRow = {};
                attributeWithout = [];
                $scope.attributesOLD = response.Data.Columns;
                angular.forEach($scope.attributesOLD, function (value, key) {
                    if (value.ColumnName.includes("__") || value.ColumnName.includes("PUBLISH2")) {
                        attributeWithout.push(value);
                    }
                });
                //$scope.attributes = response.Data.Columns;

                dataFactory.GetAddProductAttributeDetails($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID).success(function (response1) {

                    if (response1 != null) {
                        $scope.selectedRowAttributes = response1;
                        $scope.fromPageNo = 0;
                        $scope.toPageNo = 50;
                        if (attributeWithout.length < 50) {

                            $scope.toPageNo = attributeWithout.length;
                        }
                        var attributestemp = [];
                        for (var i1 = $scope.fromPageNo; i1 < $scope.toPageNo; i1++) {
                            if (attributeWithout[i1] != undefined)
                                attributestemp.push(attributeWithout[i1]);
                        }

                        $scope.attributes = attributestemp;
                        $scope.attrNewProdLength = $scope.attributes.length;
                        $scope.newSubprodCurrentPage = "1";
                        $scope.newproductloopCount = [];
                        var loopcnt = Math.floor(attributeWithout.length / 50);
                        var loopNewProdCnt = Math.floor(attributeWithout.length / 50);
                        if ((attributeWithout.length % 50) > 0) {

                            loopcnt = loopcnt + 1;
                            loopNewProdCnt = loopNewProdCnt + 1;
                        }
                        for (var i2 = 0; i2 < loopcnt; i2++) {
                            if (((i2 * 10) + 10) < attributeWithout.length) {
                                $scope.newproductloopCount.push({
                                    PAGE_NO: (i2 + 1),
                                    FROM_PAGE_NO: (i2 * 50) + 1,
                                    TO_PAGE_NO: (i2 * 50) + 50
                                });
                            } else {

                                $scope.newproductloopCount.push({
                                    PAGE_NO: (i2 + 1),
                                    FROM_PAGE_NO: (i2 * 50) + 1,
                                    TO_PAGE_NO: attributeWithout.length
                                });
                            }
                        }

                        $scope.newSubProdPageCount = $scope.newproductloopCount;
                        $scope.totalSubProdPageCount = loopNewProdCnt;
                        var theString;
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            if (attributeWithout[i].ColumnName.contains('__')) {

                                var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].ColumnName.split('__')[2] });
                                if (tempattrvals.length != 0) {
                                    var uimask = "";
                                    theString = tempattrvals[0].ATTRIBUTE_ID;
                                    tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                                    if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                                        theString = 0;
                                    }
                                    if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                                        var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                        pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                                        pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                                        var reg = new RegExp(pattern);
                                        uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                                        tempattrvals[0].attributePattern = reg;
                                    } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                        tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                                        uimask = tempattrvals[0].DECIMAL;
                                    } else {
                                        tempattrvals[0].attributePattern = 524288;
                                        uimask = 524288;
                                    }
                                    tempattrvals[0].uimask = uimask;
                                    $scope.selectedRowDynamicAttributes[theString] = [];
                                    $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                                }
                            }
                        }

                        for (var i3 = $scope.fromPageNo; i3 < $scope.toPageNo; i3++) {
                            if (attributeWithout[i3].ColumnName.contains("OBJ")) {
                                if (attributeWithout[i3].ColumnName.split('__')[2] !== "0") {
                                    var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i3].ColumnName.split('__')[2]];
                                    if (sa.USE_PICKLIST) {
                                        theString = sa.ATTRIBUTE_ID;
                                        $scope.GetPickListData(theString, sa.PICKLIST_NAME);
                                    }
                                }
                            }
                        }

                        $("#divProductGrid").addClass("col-sm-2");
                        $("#divProductGrid").removeClass("col-sm-12");
                        $("#selectedattributeEdit").show();
                        $('.pull-right').hide();


                        // $scope.winNewProductCreation.refresh({ url: "../views/app/partials/newProduct.html" });
                        // $scope.winNewProductCreation.center().open();
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.callnewproductpaging = function (pageno) {

            if (pageno != null) {
                if (pageno == "NEXT") {
                    pageno = ($scope.newSubprodCurrentPage < $scope.totalSubProdPageCount) ? parseInt($scope.newSubprodCurrentPage) + 1 : $scope.totalSubProdPageCount;
                    $scope.newSubprodCurrentPage = pageno;
                }
                else if (pageno == "PREV") {
                    pageno = ($scope.newSubprodCurrentPage > 1) ? parseInt($scope.newSubprodCurrentPage) - 1 : 1;
                    $scope.newSubprodCurrentPage = pageno;
                }
                else if (pageno == $scope.totalSubProdPageCount) {
                    $scope.newSubprodCurrentPage = $scope.totalSubProdPageCount;
                }
                else {
                    $scope.newSubprodCurrentPage = pageno;
                }
            }

            var pageData = $scope.newproductloopCount[pageno - 1];
            //$("#gridpaging").data("kendoComboBox").value(pageno);
            $scope.fromPageNo = pageData.FROM_PAGE_NO;
            $scope.toPageNo = pageData.TO_PAGE_NO;
            $scope.currentPage = pageData.PAGE_NO - 1;

            if (attributeWithout.length < 15) {
                $scope.toPageNo = attributeWithout.length;
            }
            var attributestemp = [];
            for (var i1 = $scope.fromPageNo - 1; i1 < $scope.toPageNo; i1++) {
                if (attributeWithout[i1] != undefined)
                    attributestemp.push(attributeWithout[i1]);
            }




            $scope.attributes = attributestemp;
            $scope.attrNewProdLength = $scope.attributes.length;
            for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {

                var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].ColumnName.split('__')[2] });

                if (tempattrvals.length != 0) {
                    var uimask = "";
                    var theString = tempattrvals[0].ATTRIBUTE_ID;
                    tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                    if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                        theString = 0;
                    }
                    if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                        var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                        pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                        pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                        var reg = new RegExp(pattern);
                        uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                        tempattrvals[0].attributePattern = reg;
                    } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                        tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                        uimask = tempattrvals[0].DECIMAL;
                    } else {
                        tempattrvals[0].attributePattern = 524288;
                        uimask = 524288;
                    }
                    //$scope.attributePattern[name] = reg;
                    tempattrvals[0].uimask = uimask;
                    $scope.selectedRowDynamicAttributes[theString] = [];
                    $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                    // $scope.selectedRowDynamicAttributes[theString] = uimask;
                }
            }
            for (var i2 = $scope.fromPageNo - 1; i2 < $scope.toPageNo; i2++) {
                if (attributeWithout[i2].ColumnName.contains("OBJ")) {
                    if (attributeWithout[i2].ColumnName.split('__')[2] !== "0") {
                        var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i2].ColumnName.split('__')[2]];
                        if (sa.USE_PICKLIST) {
                            var theString = sa.ATTRIBUTE_ID;
                            $scope.GetPickListData(theString, sa.PICKLIST_NAME);
                        }
                    }
                }
            }
        };
        $scope.createNewProduct = function (e) {

            //To Bring the Product Create Popup Down
            $('html, body').animate({
                scrollTop: $("#selectedattributeEdit").offset().top
            }, 5000);

            //$("#ui-id-7").css("display", "none");
            //$("#ui-id-6 > span").removeClass("ui-icon-triangle-1-s");
            //$("#ui-id-6 > span").addClass("ui-icon-triangle-1-e");

            //$("#ui-id-6").removeClass("ui-state-active");
            //$("#ui-id-6 > span").removeClass("ui-icon-triangle-1-s");
            //$("#ui-id-6 > span").addClass("ui-icon-triangle-1-e");
            //$("#ui-id-6").addClass("ui-state-default");
            //$("#divFmlyTab").css("display", "none");

            $rootScope.pagination = e;
            //$("#productpaging").hide();
            //$('#gridfamily').css('display', 'none');
            //$('#gridfamily').css('display', 'block');
            //$(".familyTab").accordion({ collapsible: true });
            // $(".familyTab").accordion("option", "active", 0);
            //$(".familyTab").addClass('display-block');
            //$(".familyTab").addClass('display-none');
            //$(".familyTab").css('display', 'none');
            //$('#ui-id-6').css('display', 'none');
            //$('#ui-id-6').css('background-position', '-32px');

            $("div#noresultfound").hide();
            $('#filterAttributePack').hide();
            $("#panel").hide();
            $(".menuProduct").hide();
            $scope.clickEvent = e;
            $scope.partno = "";
            $('#cathashSearchValue').val('');
            $scope.selectedItem = "";
            $scope.cathashSearch();
            $scope.selectedRow = {};
            attributeWithout = [];
            $scope.selectedattributeEdit = true;
            $("#selectedattributeEdit").removeClass('ng-hide');
            $scope.NewProductscreate();
            $scope.CancelProductAttributes();
            $('#productpaging').attr("style", "display: none !important");
            $('#selectedattributeEdit').attr("style", "display: block !important");

            //New UI Changes
            $("#mainProductsMenu").hide();
            //$("#productgridtabstrip").hide();
            $("#filterAttributePack").hide();
            $("#filterAttributePackLeft").hide();
            $("#productpaging1").hide();
            $("#Productcountid").hide();
            $("#sampleProdgrid").hide();
            $("#productpaging").hide();
        };

        $scope.$on("NewProductCreation", function (event, selectedCategoryId, categoryId) {
            $scope.IsNewFamily = '0';
            $http.get("../Family/GetFamily?FamilyId=" + selectedCategoryId + "&categoryId=" + categoryId + "&catalogId=" + $rootScope.selecetedCatalogId).
                then(function (familyDetails) {
                    $scope.Family = familyDetails.data;
                    if ($scope.winNewProductCreation !== undefined) {
                        $scope.partno = "";
                        $scope.selectedItem = "";
                        $scope.cathashSearch();
                        $scope.selectedRow = {};
                        $("#myDIV").show();
                        attributeWithout = [];
                        $scope.NewProductscreate();
                    }
                });
        });


        $scope.SaveProductItem = function (row) {
            //Hide No result Found
            $("div#noresultfound").hide();
            $scope.ResultEmpty = false;
            //Hide No result Found
            if ($scope.clickEvent == 'buttonclick') {
                $rootScope.sortOrder = 0;
            }
            var itemcheck = false;
            var item_value = "";
            var displayAttribures = "";
            var tt = $('#textboxvalue4');
            if ($scope.val_type === "true") {

                angular.forEach(row, function (value, key) {
                    //
                    if (key.contains("OBJ")) {
                        itemcheck = true;
                        var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];
                        if (sa != undefined) {
                            if (sa.ATTRIBUTE_ID == 1) {
                                item_value = value;
                            }
                            //To check if the attribute is a value required field
                            if (sa.VALUE_REQUIRED == true && (value == '' || value == null) || sa.ATTRIBUTE_ID == 1 && (value == '' || value == null)) {

                                if (displayAttribures == '') {
                                    displayAttribures = "The following attribute values are required,\n";
                                }
                                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                            }
                        }
                    }
                });
                if (displayAttribures == '' && itemcheck == true && item_value !== '') {
                    row.FAMILY_ID = $scope.Family.FAMILY_ID;
                    row.CATEGORY_ID = $localStorage.CategoryID;
                    row.CATALOG_ID = $rootScope.selecetedCatalogId;
                    $timeout(function () {
                        dataFactory.SaveNewProduct($scope.getCustomerIDs, $rootScope.sortOrder, row).success(function (response) {
                            if (response != null) {
                                $scope.blurCheck = false;
                                $rootScope.SelectedNodeIDPresist = 1;
                                $rootScope.selectedFAmilyList = $rootScope.SelectedNodeID;
                                $rootScope.treeData.read();
                                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                                if ($scope.ParentProductId !== 0) {
                                    $scope.LoadSubproductData();
                                }
                                $('#selectedattributenoresultfound').hide();
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: response,
                                    type: "info"
                                });
                                // $scope.winNewProductCreation.close();
                                $('#selectedattributenoresultfound').hide();
                                $('#filterAttributePack').show();

                                //New UI changes For 10.9.3
                                $("#mainProductsMenu").show();
                                $("#filterAttributePack").show();
                                $("#filterAttributePackLeft").show();
                                $("#productpaging1").show();
                                $("#Productcountid").show();
                                $("#sampleProdgrid").show();
                                $("#productpaging").show();
                                $scope.selectedattributeEdit = false;
                                
                                $('.pull-right').show();
                                $("div#noresultfound").show();
                                $('#selectedattribute').hide();
                                $("#divProductGrid").removeClass("col-sm-2");
                                $("#divProductGrid").addClass("col-sm-12");
                                //var treeView = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView');

                                //if (treeView != null && CurrentlyActiveGroupTreeNode != null) {
                                //    var selectedDataItem = treeView.dataItem(CurrentlyActiveGroupTreeNode);
                                //    selectedDataItem.haschildren = true;
                                //    treeView.collapse(CurrentlyActiveGroupTreeNode);
                                //    selectedDataItem.loaded(false);
                                //    selectedDataItem.load();
                                //    treeView.expand(CurrentlyActiveGroupTreeNode);
                                //}
                                //var count = parseInt($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[1].replace(')', '').trim()) + 1;
                                //var selectedValue = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.replace($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[1].replace(')', '').trim(), count);
                                //$("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget.find($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[0].trim()).context.innerHTML = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget.find($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText.split('(')[0].trim()).context.innerHTML.replace($("#leftNavTreeViewKendoNavigator").data("kendoTreeView")._clickTarget[0].innerText, selectedValue)

                            }

                        }).error(function (error) {
                            options.error(error);
                        });
                    }, 200);

                }
                else {
                    if (item_value == '') {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'ITEM# Cannot be empty.',
                            type: "error"
                        });
                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + displayAttribures + '.',
                            type: "info"
                        });
                    }
                }
            }
            else {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter Data using a valid format.',
                    type: "error"
                });
            }
            $scope.product_ids = "";

        };


        $scope.cathashSearch = function () {
            //if (this.partno.trim() !== "") {

            $("#dynamicproducttable").show();
            if (this.partno.trim() !== "") {
                this.partno = document.getElementById("cathashSearchValue").value;
                if (this.partno.trim() !== "") {
                    dataFactory.cathashSearch(this.partno).success(function (response) {
                        var obj = jQuery.parseJSON(response.Data.Data);
                        $scope.prodData1 = obj.Data;
                        $scope.columnsForAtt1 = obj.Columns;
                        if (obj.Data.length === 0) {
                            //alert("No Results Found. Please Try Again Using a Valid Value.");
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'No Results found, Please try again using a valid value.',
                                type: "info"
                            });
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            } else {

                $scope.columnsForAtt1 = [];
                $scope.prodData1 = [];
            }
        };

        $scope.cathashSearchsub = function () {
            $("#dynamicproducttable").show();
            if (this.partno.trim() !== "") {
                dataFactory.cathashSearchsub(this.partno).success(function (response) {
                    var obj = jQuery.parseJSON(response.Data.Data);
                    $scope.prodData1 = obj.Data;
                    $scope.columnsForAtt1 = obj.Columns;
                    if (obj.Data.length === 0) {
                        //alert("No Results Found. Please Try Again Using a Valid Value.");
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'No Results found, Please try again using a valid value.',
                            type: "info"
                        });
                    }
                }).error(function (error) {
                    options.error(error);
                });
            } else {
                $("#newProductAttributeDesign").hide();
                $scope.columnsForAtt1 = [];
                $scope.prodData1 = [];
            }
        };

        $scope.CreateSubProduct = function (e) {

            $(".subProductsContext").hide();
            $scope.clickEvent = e;
            if ($scope.ParentProductId !== 0) {

                $scope.currentPage = '0';
                $scope.selecteddRow = null;
                // = "";
                $scope.selectedItem = "";
                $scope.cathashSearchsub();
                $scope.selectedRow = {};
                attributeWithout = [];
                dataFactory.getsubprodattr($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, true, $scope.Family.CATEGORY_ID, $scope.ParentProductId).success(function (response) {

                    $scope.selectedRow = {};
                    attributeWithout = [];
                    $scope.attributesOLD = response.Data.Columns;
                    angular.forEach($scope.attributesOLD, function (value, key) {
                        if (value.ColumnName.includes("__")) {
                            attributeWithout.push(value);
                        }
                    });
                    dataFactory.GetAddSubProductAttributeDetails($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.ParentProductId).success(function (response1) {
                        if (response1 != null) {

                            $scope.selectedRowAttributes = response1;
                            $scope.fromPageNo = 0;
                            $scope.toPageNo = 50;
                            if (attributeWithout.length < 15) {

                                $scope.toPageNo = attributeWithout.length;
                            }
                            var attributestemp = [];
                            for (var i1 = $scope.fromPageNo; i1 < $scope.toPageNo; i1++) {
                                if (attributeWithout[i1] != undefined)
                                    attributestemp.push(attributeWithout[i1]);
                            }
                            $scope.attributes = attributestemp;
                            $scope.attrNewProdLength = $scope.attributes.length;
                            $scope.newSubprodCountPerPage = "10";
                            $scope.newSubprodCurrentPage = "1";
                            $scope.newproductloopCount = [];

                            var loopcnt = Math.floor(attributeWithout.length / 10);

                            var loopNewSubProdCnt = Math.floor(attributeWithout.length / 10);

                            if ((attributeWithout.length % 10) > 0) {
                                loopcnt = loopcnt + 1;
                                loopNewSubProdCnt = loopNewSubProdCnt + 1;
                            }

                            for (var i2 = 0; i2 < loopcnt; i2++) {
                                if (((i2 * 10) + 10) < attributeWithout.length) {
                                    $scope.newproductloopCount.push({
                                        PAGE_NO: (i2 + 1),
                                        FROM_PAGE_NO: (i2 * 10) + 1,
                                        TO_PAGE_NO: (i2 * 10) + 10
                                    });
                                } else {

                                    $scope.newproductloopCount.push({
                                        PAGE_NO: (i2 + 1),
                                        FROM_PAGE_NO: (i2 * 10) + 1,
                                        TO_PAGE_NO: attributeWithout.length
                                    });
                                }
                            }

                            $scope.newSubProdPageCount = $scope.newproductloopCount;
                            $scope.totalSubProdPageCount = loopNewSubProdCnt;
                            var theString;
                            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                                if (attributeWithout[i].ColumnName.contains('__')) {
                                    var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].ColumnName.split('__')[2] });
                                    if (tempattrvals.length != 0) {
                                        var uimask = "";
                                        theString = tempattrvals[0].ATTRIBUTE_ID;
                                        tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                                        if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                                            theString = 0;
                                        }
                                        if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                                            var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                            pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                                            pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                                            var reg = new RegExp(pattern);
                                            uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                                            tempattrvals[0].attributePattern = reg;
                                        } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                            tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                                            uimask = tempattrvals[0].DECIMAL;
                                        } else {
                                            tempattrvals[0].attributePattern = 524288;
                                            uimask = 524288;
                                        }
                                        tempattrvals[0].uimask = uimask;
                                        $scope.selectedRowDynamicAttributes[theString] = [];
                                        $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                                    }
                                }
                            }

                            for (var i3 = $scope.fromPageNo; i3 < $scope.toPageNo; i3++) {
                                if (attributeWithout[i3].ColumnName.contains("OBJ")) {
                                    if (attributeWithout[i3].ColumnName.split('__')[2] !== "0") {
                                        var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i3].ColumnName.split('__')[2]];
                                        if (sa.USE_PICKLIST) {
                                            theString = sa.ATTRIBUTE_ID;
                                            $scope.GetPickListData(theString, sa.PICKLIST_NAME);
                                        }
                                    }
                                }
                            }

                            $scope.winNewSubProductCreation.refresh({ url: "../views/app/partials/NewSubProduct.html" });
                            $scope.winNewSubProductCreation.center().open();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }).error(function (error) {
                    options.error(error);
                });

            }
            else {
                // alert("Please Select The Parent Product and Continue.");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select the Main Item and continue.',
                    type: "info"
                });
            }
        };
        $scope.SaveNewSubProduct = function (row) {
            //  alert('131')
            //
            if ($scope.clickEvent == 'buttonclick') {
                $rootScope.sortOrder = 0;
            }
            var displayAttribures = "";
            if ($scope.val_type === "true") {
                angular.forEach(row, function (value, key) {
                    if (key.contains("OBJ")) {
                        var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];
                        //To check if the attribute is a value required field
                        if (sa != undefined) {
                            if (sa.VALUE_REQUIRED == true && (value == '' || value == null) || sa.ATTRIBUTE_ID == 1 && (value == '' || value == null)) {
                                if (displayAttribures == '') {
                                    displayAttribures = "The following attribute values are required,\n";
                                }
                                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                            }
                        }
                    }
                });
                if (displayAttribures == '') {
                    row.FAMILY_ID = $scope.Family.FAMILY_ID;
                    row.CATEGORY_ID = $localStorage.CategoryID;
                    row.CATALOG_ID = $rootScope.selecetedCatalogId;
                    // row.PRODUCT_ID = $scope.ParentProductId;
                    dataFactory.SaveNewSubProduct($scope.getCustomerIDs, row, $scope.ParentProductId, $rootScope.sortOrder).success(function (response) {
                        if (response != null) {
                            $scope.blurCheck = false;
                            $scope.ProdCurrentPageSP = 1;
                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID, $scope.ParentProductId);
                            if ($scope.ParentProductId !== 0) {
                                $scope.LoadSubproductData();
                                $scope.MainProducts.read();
                                $('#subproductdiv').show();
                                var prodCountSP = [];
                                dataFactory.GetsubproductspecsWithoutAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId).success(function (response1) {
                                    var objSP = jQuery.parseJSON(response1.Data.Data);
                                    var prodEditPageloopcntSP = Math.floor(objSP.length / parseInt($scope.ProdCountPerPageSP));
                                    if ((objSP.length % parseInt($scope.ProdCountPerPageSP)) > 0) {
                                        prodEditPageloopcntSP = prodEditPageloopcntSP + 1;
                                    }
                                    for (var i = 1; i <= prodEditPageloopcntSP; i++) {
                                        prodCountSP.push(i);
                                    }
                                    //  alert('13')
                                    if (objSP.length == 0) {
                                        prodCountSP[0] = 1
                                        $scope.ProdPageCountSP = prodCountSP;
                                        $scope.ProdCurrentPageSP = 1;
                                        $scope.totalProdPageCountSP = 1;
                                    }
                                    else {
                                        ProdCountSP = [];
                                        for (var i = 1; i <= prodEditPageloopcntSP; i++) {
                                            prodCountSP.push(i);
                                        }
                                        $scope.ProdPageCountSP = prodCountSP;
                                        $scope.ProdCurrentPageSP = 1;
                                        $scope.totalProdPageCountSP = prodEditPageloopcntSP;

                                        $scope.ProdCurrentPageSP = 1;
                                    }
                                    $('#cboProdPageCountSPup').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageSP;
                                    $('#cboProdPageCountSPdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageSP;
                                    //var dataUId = $("#subproductsmaingrid tbody").find("tr[aria-selected='true']");
                                    //var className = dataUId[0].className;
                                    //$rootScope.fetchDatauid;
                                    //     $("#subproductsmaingrid tbody").find("tr[" + $rootScope.fetchDatauid + "]").addClass(className);
                                    var dataItem;
                                    if ($rootScope.dataItemClick != undefined) {
                                        angular.forEach($scope.MainProducts._data, function (value) {
                                            if ($rootScope.dataItemClick.PRODUCT_ID == value.PRODUCT_ID) {
                                                dataItem = value;
                                            }
                                        })
                                    }
                                    $("#subproductsmaingrid tbody").find("tr[data-uid='" + dataItem.uid + "']")[0].className = "ng-scope k-state-selected";
                                    $scope.itemClick(dataItem, $rootScope.eItemClick);
                                });
                            }
                            // alert(response);
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                    $scope.winNewSubProductCreation.close();
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + displayAttribures + '.',
                        type: "info"
                    });
                }
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter Data using a valid format.',
                    type: "error"
                });
            }
        };
        $scope.onNewProductSelect = function (e) {
            var message = $.map(e.files, function (file) { return file.name; }).join(", ");
            $scope.selectedRow[e.sender.element[0].id] = '\\Images\\' + e.files[0].name;
            $scope.$apply();
            $(".k-upload-files").remove();
            $(".k-upload-status").remove();
        };
        $scope.navigateprod = function (data) {
            $rootScope.navigateprodProductTab = false;
            $rootScope.ParentProductId = data.PRODUCT_ID;
            $scope.ParentProductId = data.PRODUCT_ID;
            $scope.switchsubprod = true;
            $scope.valuesubproduct = true;
            $scope.LoadMainProducts();
            $scope.ParentProductId = data.PRODUCT_ID;
            $rootScope.selecetedFamilyId = data.FAMILY_ID;
            //   $("#subproductsmaingrid tbody").find("tr")[0].className = "none";
            //if ($scope.ProdCurrentPage == "1") {
            //    return true;
            //}
            //if ($rootScope.invertProdId != undefined) {
            //    return true;
            //}
            dataFactory.GetPagenumberInverted($rootScope.selecetedCatalogId, "", 0, $scope.ParentProductId, "Product").success(function (response1) {
                if (response1 != null && response1 != "null" && response1 != "~") {
                    var obj = response1.split('~');
                    var ProdEditPageloopcnt1 = Math.floor(obj[0] / parseInt($scope.ProdCountPerPage));
                    if ((obj[0] % parseInt($scope.ProdCountPerPage)) > 0) {
                        ProdEditPageloopcnt1 = ProdEditPageloopcnt1 + 1;
                    }
                    //if (ProdEditPageloopcnt1 == 1) {
                    //    return true;
                    //}
                    var countSubproduct = 0;
                    var subProductNotEqual = true;
                    angular.forEach($rootScope.MainProductsAll, function (Maindata) {
                        if ((Maindata.STRING_VALUE.split('(')[1].replace(')', '').trim()) != "0") {
                            if (subProductNotEqual) {
                                if (Maindata.PRODUCT_ID != $scope.ParentProductId) {
                                    countSubproduct = countSubproduct + 1;
                                }
                                else {
                                    subProductNotEqual = false;
                                }
                            }
                        }
                    });
                    var ProdEditPageloopcntMain = Math.floor((countSubproduct + 1) / 5);
                    if (((countSubproduct + 1) % 5) > 0) {
                        ProdEditPageloopcntMain = ProdEditPageloopcntMain + 1;
                    }
                    $rootScope.pageCountMain = ProdEditPageloopcntMain;
                    var grid = $("#subproductsmaingrid").data("kendoGrid");
                    if ($rootScope.pageCountMain != undefined && $rootScope.pageCountMain != "" && $rootScope.pageCountMain != null) {
                        grid.dataSource.page($rootScope.pageCountMain);
                    }

                }
            });
            // document.getElementById("gridpagingSP" + $scope.ProdCountPerPageSP).className = "gridpaging";
            // $scope.ProdCountPerPageSP = 5;

            //var ProdCountSP = [];
            //dataFactory.GetsubproductspecsWithoutAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId).success(function(response) {

            //    var objSP = jQuery.parseJSON(response.Data.Data);
            //    var ProdEditPageloopcntSP = Math.floor(objSP.length / parseInt($scope.ProdCountPerPageSP));
            //    if ((objSP.length % parseInt($scope.ProdCountPerPageSP)) > 0) {
            //        ProdEditPageloopcntSP = ProdEditPageloopcntSP + 1;
            //    }


            //    for (var i = 1; i <= ProdEditPageloopcntSP; i++) {
            //        ProdCountSP.push(i);
            //    }
            //    $scope.ProdPageCountSP = ProdCountSP;
            //    $scope.totalProdPageCountSP = ProdEditPageloopcntSP;
            //});
            //$scope.ProdCurrentPageSP = 1;
            //dataFactory.Getsubproductspecs($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $localStorage.CategoryID, $scope.ParentProductId, $scope.ProdCurrentPageSP, $scope.ProdCountPerPageSP).success(function(response) {
            //    var obj = jQuery.parseJSON(response.Data.Data);
            //    $rootScope.SubprodData = obj;
            //    $rootScope.columnsSubAtt = response.Data.Columns;
            //    angular.forEach($rootScope.SubprodData, function(value) {
            //        value.SORT = parseFloat(value.SORT);
            //    });
            //    $rootScope.tblDashBoardss.reload();
            //}).error(function(error) {
            //    options.error(error);
            //});
            //$scope.GetAllCatalogattributesdataSourceforSubproducts.read();
            //$scope.prodfamilyattrdataSourcesubproducts.read();
            //// $rootScope.commonvalueupdatesubproducttreeData.read();
            //$('#subproductdiv').show();
            //$('#ColumnSetupSubproducts').hide();
            //$('#cmnvalupdateforsubproduct').hide();
            //$('#subproductattrib').hide();

        };
        $scope.Reset = function () {
            $rootScope.Reset();
            // $rootScope.SelectedFileForUploadnamefamilyproduct = '';
            $scope.familyimport = true;
            $scope.sheetselection = false;
            $("#productgridtabstrip").hide();
            $(".productgridtabstripclass").hide();
            $("#productpreview").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();
            $("#Familyimport").show();
            $("#importSuccessWindow").hide();
            $("#importerror").hide();
            $("#importpicklist").hide();
            $("#importErrorWindow").hide();
            $("#importTableSheetWindow").show();
            $("#importTableSheetSlectectionWindow").hide();
            $("#cmnvalupdate").hide();
        }




        $scope.onClose = function () {
            $('.block-ui-overlay').show();
            $('.imageloader').show();


        }

        //---- End New Product------------------------------------


        //---------------Reference Table clear Image attachment----------

        //----End -------------------------------
        //--------------Product attachment through asset management-----------------
        $scope.OpenPopupWindow = function (id) {
            //
            //  $window.open("../Views/App/Partials/ImageManagementPopup.html", "popup", "width=300,height=200,left=10,top=150");
            //$scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
            //$scope.winManageDrive.center().open();
            $scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
            $scope.winManageDrive.title("Asset Management");
            $scope.winManageDrive.center().open();
            $scope.driveMahementIsVisible = true;

            if (id.Name == "" || id.Name == undefined) {
                $rootScope.paramValue = id;
                $scope.attribute = id;
            }
            else {

                $rootScope.paramValue = id.Name;
                $scope.attribute = id.Value;
            }
        };
        $scope.toggleClass = function () {

            if (document.getElementById('myButton1').innerText == "Show More") {
                document.getElementById('myButton1').innerText = "Show Less"
                $rootScope.val = true;
                $rootScope.LoadData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);

            }
            else {

                document.getElementById('myButton1').innerText = "Show More"
                $rootScope.val = false;
                $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);
            }

        };
        $rootScope.SaveNewProdFileSelection = function (fileName, path) {
            //
            var extn = fileName.split(".");
            // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
            var FileFormat = ["rar", "zip"];
            if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
                document.getElementById($scope.attribute).value = path;
                $scope.selectedRow[$scope.attribute] = path;
                //$scope.attribute = path;
                $rootScope.productAttachPath = path;
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'This file type is not supported',
                    type: "info"
                });
            }
            $scope.winManageDrive.center().close();
            $scope.driveMahementIsVisible = false;
        };
        $rootScope.SaveNewSubFileSelection = function (fileName, path) {
            //
            var extn = fileName.split(".");
            // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
            var FileFormat = ["rar", "zip"];
            if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
                document.getElementById($scope.attribute).value = path;
                $scope.selectedRow[$scope.attribute] = path;
                //$scope.attribute = path;
                $rootScope.productAttachPath = path;
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'This file type is not supported',
                    type: "info"
                });
            }
            $scope.winManageDrive.center().close();
            $scope.driveMahementIsVisible = false;

        };
        $rootScope.SaveProductFileSelection = function (fileName, path) {


            var extn = fileName.split(".");
            // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
            var FileFormat = ["rar", "zip"];
            if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
                angular.forEach($scope.selectedRow, function (value, key) {
                    if (key == $rootScope.paramValue) {
                        $scope.selectedRow[key] = path;
                        // $scope.$apply();
                    }
                });
                angular.forEach($scope.selectedRowSub, function (value, key) {
                    if (key == $rootScope.paramValue) {
                        $scope.selectedRowSub[key] = path;
                        // $scope.$apply();
                    }
                });
                $rootScope.productAttachPath = path;
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'This file type is not supported',
                    type: "info"
                });
            }
            $scope.winManageDrive.center().close();
            $scope.driveMahementIsVisible = false;

        };

        $scope.clearImagePath = function (e) {
            //
            document.getElementById(e).value = "";
            $scope.selectedRowSub[e] = "";
            $scope.selectedRow[e] = "";
        };


        //$scope.setWidth = function (event) {   
        //    var resizeheader = document.getElementById("sampleProdgrid thead tr th a" + event);

        //    var resizebody = document.getElementById("sampleProdgrid tbody tr td a" + event);
        //   
        //    style = window.getComputedStyle(resizeheader);
        //    wdt = style.getPropertyValue('width');
        //    resizebody.style.width = wdt;
        //}
        //$scope.GetScrollId = function (e) {
        //    $scope.scrollId = e;
        //}

        //$scope.ClearScrollId = function () {
        //    $scope.scrollId = "";
        //}

        //$scope.setWidth = function (e) {

        //    var resizeheader = document.getElementById("resize_" + $scope.scrollId);
        //    var resizebody = document.getElementById("resize1_" + $scope.scrollId);
        //    style = window.getComputedStyle(resizeheader);
        //    wdt = style.getPropertyValue('width');
        //    resizebody.style.width = wdt;
        //}


        //$scope.subsetWidth = function () {
        //    var resizeheader = document.getElementById("subresize_" + $scope.scrollId);
        //    var resizebody = document.getElementById("subresize1_" + $scope.scrollId);
        //    style = window.getComputedStyle(resizeheader);
        //    wdt = style.getPropertyValue('width');
        //    resizebody.style.width = wdt;
        //}



        //To get the Default Item# and SubItem# values:

        $scope.getUserDefaultCatalogItemNumber = function () {

            dataFactory.GetDefaultCatalogItemNumber().success(function (response) {

                if (response != null) {
                    $localStorage.CatalogItemNumber = response[0];
                    $localStorage.CatalogSubItemNumber = response[1];
                    $rootScope.CatalogItemNumber = response[0];
                    $rootScope.CatalogSubItemNumber = response[1];
                }
            }).error(function (error) {
                options.error(error);
            });
        };


        //To get the Default Item# and SubItem# values:

        $scope.getUserDefaultCatalogItemNumber = function () {

            dataFactory.GetDefaultCatalogItemNumber().success(function (response) {

                if (response != null) {
                    $localStorage.CatalogItemNumber = response[0];
                    $localStorage.CatalogSubItemNumber = response[1];

                    $rootScope.CatalogItemNumber = response[0];
                    $rootScope.CatalogSubItemNumber = response[1];
                }



            }).error(function (error) {
                options.error(error);
            });
        };
        $rootScope.sortOrder = 0;
        // $scope.selectedRowValue = 0;

        $scope.setClickedColumn = function (index) {
            $scope.selectedColumn = index;
        };


        $scope.EmptyNavigatorRightClick1 = function (e, product_Id, family_Id, sort_order, Mainproductdata, MainproductcolumnsAtt) {
            if ($scope.CheckedProductids == true) {
                $scope.product_ids = "";
            }

            if ($scope.OldFamilyId == undefined || family_Id == $scope.OldFamilyId) {
                $scope.product_ids = "";
            }
            $rootScope.sortOrder = sort_order;
            $scope.selectedRowValue = product_Id;
            $("#copy").show();
            $("#cut").show();
            $("#paste").show();
            $scope.Family_ID = family_Id;
            $scope.OldFamilyId = family_Id;
            $scope.product_ids = $scope.product_ids + "," + product_Id;
            $scope.menuProductIds = product_Id;
            $scope.CheckedCheckbox = true;
            $scope.CheckedProductids = false;
            $('input:checkbox').removeAttr('checked');
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;
            var menuBox = null;
            var left = arguments[0].clientX;
            var top = arguments[0].clientY;
            menuBox = window.document.querySelector(".menuProduct");
            menuBox.style.left = left + "px";
            menuBox.style.top = top + "px";
            menuBox.style.display = "block";
            arguments[0].preventDefault();
            menuBox = window.document.querySelector(".menuProduct");
            menuBox.style.left = left + "px";
            menuBox.style.top = top + "px";
            menuBox.style.display = "block";
            arguments[0].preventDefault();
            var index = $scope.selectedColumn;
            $scope.Copyitemtextdropdown = false;
            $scope.searchforitemtextdropdown = false;
            $scope.Copycolumntextdropdown = false;
            $scope.Searchforattributevaluedropdown = false;

            if (index > 9) {

                $scope.Copyitemtextdropdown = true;
                $scope.searchforitemtextdropdown = true;

                $scope.itemtext = Mainproductdata[$scope.item];
                $scope.itemcolumnheader = MainproductcolumnsAtt[10].Caption;
                $scope.copyitemtextvalue = $scope.itemcolumnheader.split('__')[0];
                $scope.binditemtext = $scope.itemtext;

                document.getElementById("Copyitemtext").title = $scope.itemtext;
                $('[data-toggle="tooltip"]').tooltip();


                for (var i = 0; i < $scope.copyitemtextvalue.length; i++) {
                    if (i > 9) {
                        $scope.copyitemtextvalue = $scope.copyitemtextvalue.slice(0, 10);
                        $scope.copyitemtextvalue = $scope.copyitemtextvalue + ("...".replace($scope.copyitemtextvalue));
                    }
                }

                document.getElementById("searchforitemtext").title = $scope.itemtext;
                $('[data-toggle="tooltip"]').tooltip();


                for (var i = 0; i < $scope.binditemtext.length; i++) {
                    if (i > 9) {
                        $scope.binditemtext = $scope.binditemtext.slice(0, 10);
                        $scope.binditemtextvalue = $scope.binditemtext + ("...".replace($scope.binditemtext));
                        $scope.binditemtext = $scope.binditemtextvalue;
                    }
                }
            }

            if (index > 10) {

                $scope.Copycolumntextdropdown = true;
                $scope.Searchforattributevaluedropdown = true;

                $scope.columnheader = MainproductcolumnsAtt[index].Caption;
                $scope.copycolumnheader = $scope.columnheader.split('__')[0];
                $scope.bindcolumntext = $scope.copycolumnheader;
                $scope.bindattributeproductvalue = Mainproductdata[$scope.columnheader];

                if ($scope.columnheader == "Part#__OBJ__1__1__false__true") {
                    $scope.columnheader = ("ITEM#__OBJ__1__1__false__true".replace("Part#__OBJ__1__1__false__true"));
                }

                document.getElementById("Copycolumntext").title = $scope.bindattributeproductvalue;
                $('[data-toggle="tooltip"]').tooltip();

                $scope.Copycolumndatavalue = $scope.bindcolumntext;

                for (var i = 0; i < $scope.bindcolumntext.length; i++) {
                    if (i > 9) {
                        $scope.bindcolumntext = $scope.bindcolumntext.slice(0, 10);
                        $scope.bindcolumntext = $scope.bindcolumntext + ("...".replace($scope.bindcolumntext));
                    }
                }

                //Searchforattributevalue
                $scope.bindattributevalue = $scope.bindattributeproductvalue;

                if ($scope.bindattributevalue == null) {
                    $scope.Copycolumntextdropdown = false;
                    $scope.Searchforattributevaluedropdown = false;
                }
                else {
                    document.getElementById("Searchforattributevalue").title = $scope.bindattributevalue;
                    $('[data-toggle="tooltip"]').tooltip();

                    for (var i = 0; i < $scope.bindattributevalue.length; i++) {
                        if (i > 9) {
                            $scope.bindattributevalue = $scope.bindattributevalue.slice(0, 10);
                            $scope.bindattributevalue = $scope.bindattributevalue + ("...".replace($scope.bindattributevalue));
                        }
                    }
                }

            }
        };
        $scope.CopyItemtext = function (selectedRow) {
            $scope.hidemenu();
            if (selectedRow == "itemtext") {
                $scope.hideClick();
                $scope.Itemtextvalue = $scope.itemtext;
                var cellText = $scope.Itemtextvalue;
                if (selectedRow === 'itemtext') {
                    new Clipboard('#Copyitemtext', {
                        text: function (itemtest) {
                            return cellText;
                        }
                    });
                };
            }
        }


        $scope.Searchfor = function (selectedRow) {

            if (selectedRow == "Searchfor") {
                $scope.hideClick();
                $scope.Itemtextvalue = $scope.itemtext;
                document.getElementById('txt_search').value = $scope.Itemtextvalue;
                sessionStorage.setItem("searchText", $scope.Itemtextvalue);
                Item = "Products";
                sessionStorage.setItem("Dropdownvalue", Item);

                if ($scope.Itemtextvalue != "") {
                    window.document.location = "/App/Search";
                    return false;
                } else {
                    window.document.location = "/App/Search";
                    return false;
                }
            }
        }

        $scope.Copycolumntext = function (selectedRow) {
            $scope.hidemenu();
            if (selectedRow == "Columntext") {
                $scope.hideClick();
                $scope.copycolumnheader = $scope.bindattributeproductvalue;
                var cellTextforcopycolumnheader = $scope.copycolumnheader;
                if (selectedRow === 'Columntext') {
                    new Clipboard('#Copycolumntext', {
                        text: function (itemtest) {
                            return cellTextforcopycolumnheader;
                        }
                    });
                };
            }
        }

        $scope.Searchforattribute = function (selectedRow) {
            if (selectedRow == "Searchforattributevalue") {
                $scope.hideClick();
                $scope.attributevalue = $scope.bindattributeproductvalue;
                document.getElementById('txt_search').value = $scope.attributevalue;
                sessionStorage.setItem("searchText", $scope.attributevalue);
                $localStorage.productgrid = "True";
                Item = "Products";
                sessionStorage.setItem("Dropdownvalue", Item);
                sessionStorage.setItem("Dropdownvalueforproductselectedattribute", $scope.copycolumnheader);
                if ($scope.attributevalue != "") {
                    window.document.location = "/App/Search";
                    return false;
                } else {
                    window.document.location = "/App/Search";
                    return false;
                }
            }
        }







        $scope.EmptyNavigatorSubRightClick1 = function (e, subProduct_Id, family_Id, sort_order) {
            $scope.product_Id = $rootScope.getProductId;
            if ($scope.product_Id == undefined || $scope.product_Id == $scope.OldProductID) {
                $scope.subproduct_ids = "";
            }
            $rootScope.sortOrder = sort_order;
            $scope.selectedRowValuesubproduct = subProduct_Id;
            $("#copy").show();
            $("#cut").show();
            $("#paste").show();
            $scope.Family_ID = family_Id;
            $scope.OldProductID = $scope.product_Id;
            $scope.subproduct_ids = $scope.subproduct_ids + "," + subProduct_Id;
            $scope.menusubProductIds = subProduct_Id;
            $scope.CheckedCheckbox = true;
            $scope.CheckedProductids = false;
            $('input:checkbox').removeAttr('checked');
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;

            var menuDisplayed = false;
            var menuBox = null;


            var left = arguments[0].clientX;
            var top = arguments[0].clientY;

            menuBox = window.document.querySelector(".menu");
            menuBox.style.left = left + "px";
            menuBox.style.top = top + "px";
            menuBox.style.display = "block";

            arguments[0].preventDefault();

            menuDisplayed = true;



        };



        $rootScope.sortOrder = 0;
        //  $scope.selectedRowValue = 0;

        $scope.hidemenu = function () {
            $(".menuProduct").hide();
        }
        $scope.hidesubmenu = function () {
            $(".menu").hide();
        }


        // Attribute Grouping Start 

        $rootScope.productAttributePackdataSource = new kendo.data.DataSource({

            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
            { field: "GROUP_ID", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    //

                    var selectAllPprodpack;
                    if ($("#selectAllPprodpack").prop("checked") == true) {
                        selectAllPprodpack = 1;
                    }
                    else if ($("#selectAllPprodpack").prop("checked") == false) {
                        selectAllPprodpack = 0;
                    }
                    //  if ($scope.Family.FAMILY_ID.contains('~')) {
                    //  var Family_ID = $scope.Family.FAMILY_ID.split('~');
                    //   $scope.Family.FAMILY_ID = Family_ID[1];
                    //  }
                    dataFactory.GetProductlevelPacks($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, options.data).success(function (response) {
                        if (selectAllPprodpack == 1) {
                            $('#selectAllPprodpack').prop('checked', false);
                        }
                        $scope.prodPackCount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });
                    } else {
                        $rootScope.items;
                        dataFactory.savecatalogallattrdetails($rootScope.selecetedCatalogId, $rootScope.items).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info",
                            });
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "GROUP_ID",
                    fields: {
                        GROUP_NAME: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });


        $scope.productAttributePackOptions = {
            dataSource: $rootScope.productAttributePackdataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false, height: 200,
            toolbar: [
                { name: "save", text: "", template: '<a ng-click="SaveProductAttributePack()" class="red k-button k-button-icontext k-grid-upload">Save</a>' },
                { name: "cancel", text: "", template: '<a ng-click="CancelProductAttributePack()" class="red k-button k-button-icontext k-grid-upload">Cancel</a>' }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllPprodpack' class='mc-checkbox' ng-click='selectAll($event)'/>" },
                { field: "GROUP_NAME", title: "Group Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };

        //$scope.ProductAttributePacks = function () {
        //    $rootScope.productAttributePackdataSource.read();
        //    $timeout(function () {
        //        $("#attributePack").show();
        //        $("#filterAttributePack").hide();
        //        $("#productgridtabstrip").hide();
        //        $(".productgridtabstripclass").hide();
        //        $("#productpreview").hide();
        //        $("#configuratorproducts").hide();
        //        $("#columnsetupproduct").hide();
        //        $('#ColumnSetupSubproducts').hide();
        //        // $("#Familyimport").show();
        //        $("#importSuccessWindow").hide();
        //        $("#importerror").hide();
        //        $("#importpicklist").hide();
        //        $("#importErrorWindow").hide();
        //        //$("#importTableSheetWindow").show();
        //        $("#importTableSheetSlectectionWindow").hide();
        //        $("#cmnvalupdate").hide();
        //    }, 200);
        //};

        $scope.ProductAttributePacks = function () {
            $rootScope.productAttributePackdataSource.read();
            $timeout(function () {
                $scope.attributePack = true;
                $scope.dynamictable = false;

                dynamictable
                $('#accordion1').show();
                $("#productpaging1").hide();
                $("#attributePack").show();
                $("#myDIV").hide();
                $("#columnsetup").hide();
                $(".column_setup").hide();
                $("#filterAttributePack").hide();
                $("#productgridtabstrip").hide();
                $(".productgridtabstripclass").hide();
                $("#productpreview").hide();
                $("#Familyimport").hide();
                $("#importTableSheetWindow").hide();
                $("#configuratorproducts").hide();
                $("#columnsetupproduct").hide();
                $('#ColumnSetupSubproducts').hide();
                // $("#Familyimport").show();
                $("#importSuccessWindow").hide();
                $("#importerror").hide();
                $("#importpicklist").hide();
                $("#importErrorWindow").hide();
                //$("#importTableSheetWindow").show();
                $("#importTableSheetSlectectionWindow").hide();
                $("#cmnvalupdate").hide();
            }, 200);
        };


        $rootScope.ProductGroupNameDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetdropdownGroupName($rootScope.selecetedCatalogId, "Product").success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $rootScope.ProductGroupNameForFamily = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    if ($rootScope.ClickFamilyIDValue != null && $rootScope.ClickFamilyIDValue != undefined && $rootScope.ClickFamilyIDValue != "undefined") {
                        $scope.Family.FAMILY_ID = $rootScope.ClickFamilyIDValue;
                    }
                    dataFactory.GetdropdownGroupNameforFamily($rootScope.selecetedCatalogId, "Product", $scope.Family.FAMILY_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        //new changes for family associate

        //$rootScope.FamilyGroupNameforFamilyAssociate = new kendo.data.DataSource({
        //    type: "json",
        //    serverFiltering: true,
        //    transport: {
        //        read: function (options) {
        //            dataFactory.GetdropdownGroupName($rootScope.selecetedCatalogId, "Family", $scope.Family.FAMILY_ID).success(function (response) {s                    
        //                options.success(response);
        //            }).error(function (error) {
        //                options.error(error);
        //            });
        //        }
        //    }
        //});

        $scope.ProductGroupNameChange = function (e) {

            $scope.SelectedProductGroupId = e.sender.value();
            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID, $scope.SelectedProductGroupId, $scope.searchTypeValue, $scope.AttrValues, $scope.productGridSearchValue, $scope.searchType, $scope.ProdSpecsValues, $scope.ProdPriceValues, $scope.ProdKeyValues, $scope.ProdImageValues);

            $scope.selecteddRow = null;
            $rootScope.ProductCount = $rootScope.ProductCount + 1;

        }

        $scope.updateSelection = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);

                $rootScope.items.push({
                    GROUP_ID: id.dataItem.GROUP_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    GROUP_NAME: id.dataItem.GROUP_NAME,
                    CATALOG_ID: $rootScope.selecetedCatalogId,
                    FAMILY_ID: $scope.Family.FAMILY_ID,
                    CATEGORY_ID: $scope.ActiveCategoryId,
                });

            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };


        $scope.selectAll = function (ev) {

            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };


        $scope.SaveProductAttributePack = function () {
            if ($rootScope.selecetedCatalogId === 1) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Master Catalog update is not permitted.',
                    type: "info"
                });
            } else {
                if ($rootScope.items != undefined && $rootScope.items.length != 0) {
                    dataFactory.SaveProductAttributePack($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $rootScope.items).success(function (response) {
                        $rootScope.items = [];
                        $('#selectAllPprodpack').prop('checked', false);
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Process completed.',
                            type: "info",
                        });
                        $rootScope.productAttributePackdataSource.read();

                        $rootScope.ProductGroupNameForFamily.read();
                        $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                        // options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {

                }
            }
        };


        //$scope.CancelProductAttributePack = function () {

        //    $rootScope.productAttributePackdataSource.read();
        //};

        $scope.CancelProductAttributePack = function () {

            $('#accordion1').hide();
            $scope.OpenProductDefaultTab();
            $scope.dynamictable = true;
            $("#newProductBtn1").show();
            $("#gridproduct").show();
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();
            if ($rootScope.Mainprodcount > 0) {
                $("#productpaging1").show();
            }
            $scope.winColumnSetuplayout.close();
            $('#sampleProdgrid').show();
            $rootScope.productAttributePackdataSource.read();
        };

        // Attribute Grouping end 

        $scope.init();

        // Pdf Express

        //$scope.PreviewProductPdf = function () {
        //    $scope.winPdoductPdf.refresh({ url: "../views/app/partials/ProdcutPdfPage.cshtml" });
        //    $scope.winPdoductPdf.center().open();

        //}
        //----------------------------------------------------------------------------------------------------------------------------------


        //Upload mrt files for Open Template
        $scope.ChechFileValid = function (file) {
            var isValid = false;
            if ($scope.SelectedFileForUpload != null) {
                if (file.name.contains(".mrt")) {
                    $scope.FileInvalidMessage = "";
                    isValid = true;
                }
                else {
                    $scope.FileInvalidMessage = "Selected file is Invalid. (only file type .mrt allowed)";
                }
            }
            else {
                $scope.FileInvalidMessage = "MRT Files required!";
            }
            $scope.IsFileValid = isValid;
        };

        $scope.openPDFPath = "";

        $scope.UploadFile = function (file, saveData) {
            var formData = new FormData();
            formData.append("CatalogId", $localStorage.getCatalogID)
            formData.append("file", file);
            formData.append("TYPE", "Product");

            formData.append("ID", $scope.currentFamilyIdPdf)

            $http.post("/XpressCatalog/SaveFiles", formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function (d) {
                    $scope.openPDFPath = d;
                    //$.msgBox({
                    //    title: $localStorage.ProdcutTitle,
                    //    content: 'File upload successful.',
                    //    // type: "info"
                    //});

                    dataFactory.FindProjectType().success(function (response) {
                        if (response !== "Error") {
                            if (response.toLocaleLowerCase().contains("simple")) {
                                //   $("#opendesignerProd").show();
                            } else {
                                //    $("#opendesignerProd").show();
                            }
                        } else {
                            //   $("#opendesignerProd").show();
                        }
                    }).error(function (error) {
                        //     $("#opendesignerProd").show();
                    });
                })
                .error(function () {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'File upload failed, please try again.',
                        type: "error"
                    });

                });

            //  return defer.promise;
        };
        //JO
        $rootScope.UploadFile = function (file, saveData) {
            var formData = new FormData();
            formData.append("CatalogId", $localStorage.getCatalogID)
            formData.append("file", file);
            formData.append("TYPE", "Product");

            formData.append("ID", $scope.currentFamilyIdPdf)

            $http.post("/XpressCatalog/SaveFiles", formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function (d) {
                    $scope.openPDFPath = d;
                    //$.msgBox({
                    //    title: $localStorage.ProdcutTitle,
                    //    content: 'File upload successful.',
                    //    // type: "info"
                    //});

                    dataFactory.FindProjectType().success(function (response) {
                        if (response !== "Error") {
                            if (response.toLocaleLowerCase().contains("simple")) {
                                //   $("#opendesignerProd").show();
                            } else {
                                //    $("#opendesignerProd").show();
                            }
                        } else {
                            //   $("#opendesignerProd").show();
                        }
                    }).error(function (error) {
                        //     $("#opendesignerProd").show();
                    });
                })
                .error(function () {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'File upload failed, please try again.',
                        type: "error"
                    });

                });

            //  return defer.promise;
        };

        $scope.closeOpentemplate = function () {
            $("#opendesignerProd").hide();
        };


        $scope.selectFileforUploadRun_Product = function (file) {
            if ($scope.currentProductId == null || $scope.currentProductId == '') {
                $scope.currentProductId = 0;
            }


            $scope.SelectedFileForUpload = file[0];
            $rootScope.SelectedFileForUpload = file[0];


            $scope.SelectedFileForProduct = file[0].name;
            $scope.SelectedFileForProduct = $scope.SelectedFileForProduct.substring(0, 12);
            $scope.SelectedFileForProductTooltip = $scope.SelectedFileForProduct.substring(0, 12);
            var category_Id = $('#txt_CategoryID').val();
            //changes
            var category_Name = $rootScope.selectedCloneDetails;
            var Family_Name = $rootScope.currentFamilyId;

            var countList = 0;
            var listSelecteddata = category_Name.split('~');
            var listSelected = category_Name.split('~');

            angular.forEach(listSelected, function (value) {
                angular.forEach(listSelecteddata, function (value1) {
                    if (value == value1) {
                        countList = countList + 1;
                    }
                });
            });

            var Catalog_ID = $localStorage.getCatalogID;
            dataFactory.setPdfXpressType("PRODUCT", listSelected[0], Family_Name, $scope.currentProductId, Catalog_ID).success(function (response) {
                dataFactory.getCatalogID($scope.SelectedCatalogId).success(function (response) {

                });
            });

        };

        //Jothipriya NOV-19-2021 Product list preview start
        $scope.PreviewProductPdf = function (data) {
            var TYPE = 'PRODUCT';
            var Id = data.Data.FAMILY_ID;;
            var Catalog_id = $localStorage.getCatalogID;

            dataFactory.getPdfXpressdefaultType(TYPE, Id, Catalog_id).success(function (response) {

                if (response != "") {
                    $scope.SelectedFileForUpload = response;
                    $scope.UploadFile($scope.SelectedFileForUpload, false);
                }
            });
            $scope.currentProductId = data.Data.PRODUCT_ID;
            $scope.currentFamilyIdPdf = data.Data.FAMILY_ID;
            $scope.saveProductSessionForPdfXpress(data);
            $scope.Message = "";
            if ($rootScope.SelectedFileForUpload != undefined)
                $timeout(function () {
                    $rootScope.UploadFile($rootScope.SelectedFileForUpload, false);
                }, 1000);
        };

        $scope.saveProductSessionForPdfXpress = function (data) {
            if ($scope.currentProductId == null || $scope.currentProductId == '') {
                $scope.currentProductId = 0;
            }
            var Id = data.Data.FAMILY_ID;

            var category_Name = $localStorage.CategoryID;
            var Family_Name = data.Data.FAMILY_ID;
            var countList = 0;
            var listSelecteddata = category_Name.split('~');
            var listSelected = category_Name.split('~');
            //var category_Id = data.Data.CATEGORY_ID;
            var category_Id = listSelecteddata[0];

            angular.forEach(listSelected, function (value) {
                angular.forEach(listSelecteddata, function (value1) {
                    if (value == value1) {
                        countList = countList + 1;
                    }
                });
            });
            let Catalog_id = $localStorage.getCatalogID;
            dataFactory.CheckTemplatePath_ProductItem("PRODUCT", Family_Name, Catalog_id).success(function (response) {
                debugger;
                if (response == "Template found") {
                    var Catalog_ID = $localStorage.getCatalogID;
                    dataFactory.setPdfXpressType("PRODUCT", listSelected[0], Family_Name, $scope.currentProductId, Catalog_ID).success(function (response) {
                        window.open("../Category/PdfPreviewProduct?countList=" + countList, '_blank');
                    });
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'PDFxPress Template is missing in the path. Kindly check it.',
                        type: "error"
                    });
                }

            });
            
        }
        //Jothipriya NOV-19-2021 Product list end 


        $rootScope.PreviewProductPdf = function (data) {
            var TYPE = 'PRODUCT';
            var ID = data.Data.PRODUCT_ID;
            var Id = data.Data.FAMILY_ID;
            var Catalog_id = $localStorage.getCatalogID;

            dataFactory.getPdfXpressdefaultType(TYPE, Id, Catalog_id).success(function (response) {

                if (response != "") {
                    $scope.SelectedFileForUpload = response;
                    $scope.UploadFile($scope.SelectedFileForUpload, false);
                }


            });
            $scope.currentProductId = data.Data.PRODUCT_ID;
            $scope.currentFamilyIdPdf = data.Data.FAMILY_ID;
            $scope.saveProductSessionForPdfXpress();

            // $('#opendesignerProd').hide();
            // $scope.IsFormSubmitted = true;
            $scope.Message = "";
            //  $scope.ChechFileValid($scope.SelectedFileForUpload);


            if ($scope.SelectedFileForUpload != undefined)
                $scope.UploadFile($scope.SelectedFileForUpload, false);

        }
        function clearForm() {
            $scope.FileDescription = "";
            angular.forEach(angular.element("input[type='file']"), function (inputElem) {
                angular.element(inputElem).val(null);
            });
            $scope.f1.$setPristine();
            $scope.IsFormSubmitted = false;
        }


        $scope.SaveRunFileProductPdf = function () {

            $scope.exportFileFormat = "pdf";

            if ($scope.exportFileFormat != undefined) {
                $http.post("../XpressCatalog/ExportResult?format=" + $scope.exportFileFormat).then(function (response) {
                    var windowlocation = window.location.origin;
                    var url = response.data;
                    $.ajax({
                        url: windowlocation,
                        success: function () {
                            window.open(url);
                        },
                        error: function () {
                            alert('does not exist in server location');
                        },
                    });
                });
            }
        }



        //Clear pdf express in default.


        $scope.deleteProductPdf = function () {
            debugger;
            var id = $scope.currentProductId;
            var catId = $localStorage.getCatalogID;


            dataFactory.clearPdfXpressType("Product", id, catId).success(function (response) {
                if (response == "Success") {
                    $scope.SelectedFileForProduct = response.substring(0, 12);
                    $scope.SelectedFileForProductTooltip = response.substring(0, 12);
                }
            });


        }

        // End
        //mv


        $scope.ResultEmpty = false;
        $('#divProductGrid12').show();




        $scope.searchProduct = function () {
            $scope.ResultEmpty = false;
            $('#selectedattribute').hide();
            $('.pull-right').show();
            $rootScope.TypeOfProduct = "searchProduct";



            // Chdeck Box UnCommand
            var checks = $('input[class="check"]');
            for (var i = 0; i < checks.length; i++) {
                var check = checks[i];
                if (!check.disabled) {
                    if (check.checked) {
                        check.checked = false;
                    }
                }
            }


            $scope.product_ids = "";
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;

            // Chdeck Box UnCommand




            $("#divProductGrid").removeClass("col-sm-2");
            $("#divProductGrid").addClass("col-sm-12");
            //if (selectedCount != undefined && selectedCount!= null && selectedCount > 0) {
            //    $("#panel").hide();
            //}Mariya



            if ($scope.SelectedProductGroupId == null || $scope.SelectedProductGroupId == "" || $scope.SelectedProductGroupId == undefined) {
                $scope.SelectedProductGroupId = undefined;
            }
            if ($scope.AttrValues == null || $scope.AttrValues == "" || $scope.AttrValues == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Please select a Attribute.",
                    type: "info"
                });
            }

            else if ($scope.productGridSearchValue == null || $scope.productGridSearchValue == "" || $scope.productGridSearchValue == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Please Enter a Search Value.",
                    type: "info"
                });
            }

            if ($scope.productGridSearchValue != "" && $scope.AttrValues != "" && $scope.productGridSearchValue != undefined && $scope.AttrValues != undefined) {
                $scope.searchTypeValue = 'product';
            }

            // mv22

            if ($scope.productGridSearchValue == "" || $scope.productGridSearchValue == "" || $scope.productGridSearchValue == "" || $scope.productGridSearchValue == undefined) {
                $scope.searchTypeValue = 'product';
            }

            // mv22

            if ($scope.AttrValues != "" && $scope.AttrValues != undefined) {
                $("#cboProdPageCountup").data("kendoDropDownList").text("");
                $("#cboProdPageCountdown").data("kendoDropDownList").text("");
                //$scope.ProdCurrentPage = parseInt($scope.ProdCurrentPage) + 1;

                $scope.ProdCurrentPage = "1";

                if ($scope.Family.FAMILY_ID == "")
                {
                    $rootScope.LoadProdItemData($rootScope.selecetedCatalogId, $scope.Family_IDs, $localStorage.CategoryID, $scope.SelectedProductGroupId, $scope.searchTypeValue, $scope.AttrValues, $scope.productGridSearchValue, $scope.searchType, $scope.ProdSpecsValues, $scope.ProdPriceValues, $scope.ProdKeyValues, $scope.ProdImageValues);
                }
                else
                {
                    $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID, $scope.SelectedProductGroupId, $scope.searchTypeValue, $scope.AttrValues, $scope.productGridSearchValue, $scope.searchType, $scope.ProdSpecsValues, $scope.ProdPriceValues, $scope.ProdKeyValues, $scope.ProdImageValues);
                }
                
                $scope.selecteddRow = null;
                $rootScope.ProductCount = $rootScope.ProductCount + 1;
            }
            // else {
            //    $.msgBox({
            //        title: $localStorage.ProdcutTitle,
            //        content: 'Please Select Attribute',
            //        type: "info"
            //    });
            //}



            $("#divProductGrid").removeClass("col-sm-2");
            $("#divProductGrid").addClass("col-sm-12");


        }

        $scope.myFunction = function () {
            if ($scope.flag == 0) {
                $("#divProductGrid").removeClass("col-sm-2");
                $("#divProductGrid").addClass("col-sm-12");
                $('#selectedattribute').hide();
                $('#myDIV').show();
                $('#sampleProdgrid').hide();
                $('#productpaging1').hide();
                $('#productpaging').hide();
                $('divProductGrid').show();
                $('divProductGridItemOnly').hide();
                $scope.flag = 1;
            }
            else {
                $('#myDIV').hide();
                $('#sampleProdgrid').show();
                // $('#productpaging1').show();
                //$('#productpaging').show();

                $scope.flag = 0;
            }


            //MV

            var data = $rootScope.selectedRowValue;
            if (data == undefined) {
                data = 0;

            }
            attributeWithout = [];
            $scope.attributesOLD = Object.keys($scope.prodData[data]);
            dataFactory.getprodspecs($scope.prodData[data].CATALOG_ID, $scope.prodData[data].FAMILY_ID, true, cat_id, $scope.ProdCurrentPage, $scope.ProdCountPerPage, $scope.sub_id, pack_Id, Type, Attributes, productGridSearchValue, searchType, ProdSpecsValues, ProdPriceValues, ProdKeyValues, ProdImageValues, $rootScope.val).success(function (response) {
                if (response != null) {
                    $scope.selectedRowAttributes = response;
                    var calcAttributestemp = [];
                    angular.forEach($scope.attributesOLD, function (value, key) {
                        if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2') || value == 'WORKFLOW STATUS')
                            attributeWithout.push(value);
                    });
                    $scope.attributescalc = attributeWithout;
                    $scope.fromPageNo = 0;
                    $scope.toPageNo = 150;
                    if (attributeWithout.length < 150) {
                        $scope.toPageNo = attributeWithout.length;
                    }
                    var attributestemp = [];
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        attributestemp.push(attributeWithout[i]);
                    }
                    $scope.attributes = attributestemp;
                    $scope.attrLength = $scope.attributes.length;
                    $scope.ProdSpecsCountPerPage = "150";
                    $scope.ProdSpecsCurrentPage = "1";

                    $scope.loopCount = [];
                    $scope.loopEditProdsCount = [];
                    var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                    var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.ProdSpecsCountPerPage));
                    if ((attributeWithout.length % parseInt($scope.ProdSpecsCountPerPage)) > 0) {
                        loopcnt = loopcnt + 1;
                        loopEditProdsCnt = loopEditProdsCnt + 1;
                    }
                    $scope.ProdSpecsPageCount = $scope.loopCount;
                    $scope.totalSpecsProdPageCount = loopEditProdsCnt;
                    for (var i = 0; i < loopcnt; i++) {
                        if (((i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)) < attributeWithout.length) {
                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + parseInt($scope.ProdSpecsCountPerPage)
                            });
                        } else {

                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.ProdSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: attributeWithout.length
                            });
                        }
                    }
                    var theString;
                    $scope.selectedRowEdit = {};
                    angular.copy($scope.prodData[data], $scope.selectedRowEdit);
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].contains('__')) {
                            var tempattrvals = $scope.selectedRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });
                            if (tempattrvals.length != 0) {
                                var uimask = "";
                                theString = tempattrvals[0].ATTRIBUTE_ID;
                                tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                                if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                                    theString = 0;
                                }
                                var itemval = attributeWithout[i];
                                if (tempattrvals[0].ATTRIBUTE_ID === 1) {
                                    $scope.item_ = $scope.selectedRowEdit[itemval];
                                }
                                if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                                    var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                    pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                                    pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                                    var reg = new RegExp(pattern);
                                    uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                                    tempattrvals[0].attributePattern = reg;
                                } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                    tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                                    uimask = tempattrvals[0].DECIMAL;
                                } else {
                                    tempattrvals[0].attributePattern = 524288;
                                    uimask = 524288;
                                }
                                tempattrvals[0].uimask = uimask;
                                $scope.selectedRowDynamicAttributes[theString] = [];
                                $scope.selectedRowDynamicAttributes[theString] = tempattrvals[0];
                            }
                        }
                    }
                    $scope.groups = [];
                    var picklistNAme = "";
                    var attrId = "";
                    $scope.iterationCount = 0;
                    $scope.isUIReleased = 0;
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].contains("OBJ")) {
                            if (attributeWithout[i].split('__')[2] !== "0") {
                                var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                                if (sa.USE_PICKLIST) {
                                    $scope.iterationCount++;
                                }
                            }
                        }
                    }
                    // $rootScope.selectedRowDynamic = [];

                    $scope.iterationPicklistCount = 0;
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].contains("OBJ")) {
                            if (attributeWithout[i].split('__')[2] !== "0") {
                                var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                                if (sa.USE_PICKLIST) {
                                    $scope.GetPickListData(sa.ATTRIBUTE_ID, sa.PICKLIST_NAME);
                                }
                            }
                        }
                    }


                    $scope.updateproductspecs = $scope.prodData[data]
                    $("#result").hide();

                    if ($scope.isUIReleased == 0) {

                        $scope.selectedRow = {};


                        angular.copy($scope.updateproductspecs, $scope.selectedRow);
                        // $localStorage.selectedRow1 = $scope.selectedRow;
                        $scope.ProductSortOrder.read();

                        angular.copy($scope.updateproductspecs, $scope.selectedRow);

                        //var ddl = $('#dynamicsort').data('kendoDropDownList');
                        //ddl.dataSource.data({}); // clears dataSource
                        //ddl.text(""); // clears visible text
                        //ddl.value("");

                        //  $rootScope.windows_Open();

                        blockUI.stop();




                        $rootScope.ajaxLoaderDivShow = false;

                        //$scope.selectedRow["SORT"] = data.$index + 1;
                        //$timeout(function () {
                        //    $scope.selectedRow["SORT"] = data.$index + 1;

                        //}, 50);

                        $timeout(function () {

                            blockUI.stop();
                        }, 100);
                        // $rootScope.ajaxLoaderDivShow = false;

                    }
                }
            }).error(function (error) {
                options.error(error);
            });

            //MV
        }

        var selectedCount = 0;
        $scope.resizeattr = function () {

            if ($scope.flag == 0) {

                //change product grid
                $('#divProductGrid').show();
                $('#divProductGridItemOnly').hide();
                $('sampleProdgrid3').show();
                //change product grid

                $('#divProductGrid1').show();
                $('#selectedattribute').hide();
                $("#divProductGrid").removeClass("col-sm-2");
                $("#divProductGrid").addClass("col-sm-12");
                $scope.flag = 1;
            }
            else {

                //change product grid

                $('#divProductGridItemOnly').show();
                //change product grid
                $('sampleProdgrid3').show();

                $('#divProductGrid1').show();
                if (selectedCount != undefined && selectedCount > 1) {
                    $("#panel").show();
                } else {
                    $('#selectedattribute').show();
                    $("#selectedattributeEdit").hide();
                }
                $("#divProductGrid").removeClass("col-sm-12");
                $("#divProductGrid").addClass("col-sm-2");
                $scope.flag = 0;
            }
        }


        $scope.myFunction2 = function () {
            if ($scope.flag == 0) {
                $('#divProductGrid2').show();
                $('#selectedattribute').hide();
                $('#panel').show();
                //$("#divProductGrid").removeClass("col-sm-2");
                //$("#divProductGrid").addClass("col-sm-12");
                $scope.flag = 1;
            }
            else {
                $('#divProductGrid2').show();
                $('#selectedattribute').show();
                $("#selectedattributeEdit").hide();
                $('#panel').hide();
                //$("#divProductGrid").removeClass("col-sm-12");
                //$("#divProductGrid").addClass("col-sm-3");
                $scope.flag = 0;
            }
        }
        ;
        $scope.newItemNo = 1;
        $scope.addNewChoice = function () {
            $scope.newItemNo = $scope.newItemNo + 1;
            if ($scope.newItemNo == 2) {
                $scope.choices = [{ CUSTOM_TEXT_FIELD: 1, CUSTOM_NUM_FIELD: 1 }
                ];
                $scope.newItemNo = $scope.newItemNo + 1;
            } else {
                $scope.choices = [{ CUSTOM_TEXT_FIELD: 1, CUSTOM_NUM_FIELD: 1 },
                { CUSTOM_TEXT_FIELD: 2, CUSTOM_NUM_FIELD: 2 }];
            }

            $scope.dynamicValue = 0;
            $scope.dynamicValue = $scope.dynamicValue + 1;
            $scope.assignIdAssignValue = "DynamicAttributeId" + $scope.dynamicValue;
            ////  $scope.choices.push({ 'id': 'choice' + newItemNo });
            //if (newItemNo == 2) {
            //    $scope.choices = [{ CUSTOM_TEXT_FIELD1: $scope.Category.CUSTOM_TEXT_FIELD1, CUSTOM_NUM_FIELD1: $scope.Category.CUSTOM_NUM_FIELD1 },
            //        { CUSTOM_TEXT_FIELD2: $scope.Category.CUSTOM_TEXT_FIELD2, CUSTOM_NUM_FIELD2: $scope.Category.CUSTOM_NUM_FIELD2 }];
            //} else {
            //    $scope.choices = [{ CUSTOM_TEXT_FIELD1: $scope.Category.CUSTOM_TEXT_FIELD1, CUSTOM_NUM_FIELD1: $scope.Category.CUSTOM_NUM_FIELD1 },
            //        { CUSTOM_TEXT_FIELD2: $scope.Category.CUSTOM_TEXT_FIELD2, CUSTOM_NUM_FIELD2: $scope.Category.CUSTOM_NUM_FIELD2 },
            //        { CUSTOM_TEXT_FIELD3: $scope.Category.CUSTOM_TEXT_FIELD3, CUSTOM_NUM_FIELD3: $scope.Category.CUSTOM_NUM_FIELD3 }];
            //}
        };

        $scope.removeChoice = function () {

            var lastItem = 2 - 1;

            if (lastItem !== 0) {
                $scope.categoryCustom.CUSTOM_TEXT_FIELD[lastItem + 1] = "";
                $scope.categoryCustom.CUSTOM_NUM_FIELD[lastItem + 1] = "";
                $scope.choices.splice(lastItem);
            }

        };
        $('#firstName').on('click', function () {

            $('.hideonenter').show();
            //    alert("testing");
        });

        $('#firstName').on('blur', function () {
            var x = document.getElementById("firstName").value;
            if (x == "") {
                $('.hideonenter').hide();
            }

        });

        $scope.MultipleProductGridValueChange = function () {

            //var s = $scope.columnsForAtt;

        };

        $scope.UpdateMulipleAttributeProducts = function (MulipleAttributename, UserUpadtedAttributeValue, AttributeTypeVlauesBySelect) {

            if (MulipleAttributename != null && MulipleAttributename != undefined && MulipleAttributename != '' && MulipleAttributename != "" && UserUpadtedAttributeValue != null && UserUpadtedAttributeValue != undefined && UserUpadtedAttributeValue != '' && UserUpadtedAttributeValue != "" && AttributeTypeVlauesBySelect != null && AttributeTypeVlauesBySelect != undefined && AttributeTypeVlauesBySelect != '' && AttributeTypeVlauesBySelect != "") {
                dataFactory.updateMulipleAttributeProducts(MulipleAttributename, UserUpadtedAttributeValue, AttributeTypeVlauesBySelect, $scope.product_ids, $scope.Family.FAMILY_ID, $localStorage.CategoryID, $rootScope.selecetedCatalogId).success(function (response) {
                    if (response == "Success") {
                        // alert('Update Success');
                        $scope.CommonFuncationProductGrid();
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "Updated Successfully.",
                            type: "info"
                        });
                    }

                });
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Please choose attribute type and value.",
                    type: "error"
                });
            }
        }
        //MV
        $scope.callSearchType = function (e) {

            $scope.searchType = e;
        }

        $rootScope.getProductGridDetails = function (data) {

            $scope.selectedRow = {};

            angular.copy(data, $scope.selectedRow);
            // $localStorage.selectedRow1 = $scope.selectedRow;
            $scope.ProductSortOrder.read();

            angular.copy(data, $scope.selectedRow);

        }

        $scope.tagBtnClick = function () {

            $("#selectedattribute").hide();
            // Chdeck Box UnCommand
            var checks = $('input[class="check"]');
            for (var i = 0; i < checks.length; i++) {
                var check = checks[i];
                if (!check.disabled) {
                    if (check.checked) {
                        check.checked = false;
                    }
                }
            }

            $scope.product_ids = "";
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;

            // Chdeck Box UnCommand
            //  $('#check').prop('checked', false);

            $("#divProductGrid").removeClass("col-sm-2");
            $("#divProductGrid").addClass("col-sm-12");
            var classVal = $("#tagBtnItem").attr("class");
            if (classVal.includes("active2")) {
                $("#tagBtnItem").removeClass("active2");


            }


            else {
                $("#tagBtnItem").addClass("active2");
                $("#moneyBtnItem").removeClass("active2");
                $("#imageBtnItem").removeClass("active2");
                $("#keyBtnItem").removeClass("active2");
            }


            if ($scope.ProdSpecsValues == true) {
                $scope.ProdSpecsValues = false;
                $scope.ProdPriceValues = true;
                $scope.ProdImageValues = true;
                $scope.ProdKeyValues = true;
            } else {
                $scope.ProdPriceValues = true;
                $scope.ProdImageValues = true;
                $scope.ProdKeyValues = true;
                $scope.ProdSpecsValues = true;

            }
            $scope.CommonFuncationProductGrid();
        }

        $scope.moneyBtnClick = function () {
            $("#selectedattribute").hide();
            // Chdeck Box UnCommand
            var checks = $('input[class="check"]');
            for (var i = 0; i < checks.length; i++) {
                var check = checks[i];
                if (!check.disabled) {
                    if (check.checked) {
                        check.checked = false;
                    }
                }
            }

            $scope.product_ids = "";
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;

            // Chdeck Box UnCommand

            $("#divProductGrid").removeClass("col-sm-2");
            $("#divProductGrid").addClass("col-sm-12");
            var classVal = $("#moneyBtnItem").attr("class");
            if (classVal.includes("active2")) {
                $("#moneyBtnItem").removeClass("active2");
            }

            else {
                $("#tagBtnItem").removeClass("active2");
                $("#moneyBtnItem").addClass("active2");
                $("#imageBtnItem").removeClass("active2");
                $("#keyBtnItem").removeClass("active2");
            }

            if ($scope.ProdPriceValues == true) {
                $scope.ProdSpecsValues = true;
                $scope.ProdImageValues = true;
                $scope.ProdKeyValues = true;
                $scope.ProdPriceValues = false;
            } else {
                $scope.ProdPriceValues = true;
                $scope.ProdImageValues = true;
                $scope.ProdKeyValues = true;
                $scope.ProdSpecsValues = true;
            }

            $scope.CommonFuncationProductGrid();

        }

        $scope.keyBtnClick = function () {
            // Chdeck Box UnCommand
            $("#selectedattribute").hide();
            var checks = $('input[class="check"]');
            for (var i = 0; i < checks.length; i++) {
                var check = checks[i];
                if (!check.disabled) {
                    if (check.checked) {
                        check.checked = false;
                    }
                }
            }

            $scope.product_ids = "";
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;

            // Chdeck Box UnCommand

            $("#divProductGrid").removeClass("col-sm-2");
            $("#divProductGrid").addClass("col-sm-12");

            var classVal = $("#keyBtnItem").attr("class");
            if (classVal.includes("active2")) {
                $("#keyBtnItem").removeClass("active2");
            }
            else {
                $("#tagBtnItem").removeClass("active2");
                $("#moneyBtnItem").removeClass("active2");
                $("#imageBtnItem").removeClass("active2");
                $("#keyBtnItem").addClass("active2");
            }


            if ($scope.ProdKeyValues == true) {
                $scope.ProdSpecsValues = true;
                $scope.ProdImageValues = true;
                $scope.ProdPriceValues = true;
                $scope.ProdKeyValues = false;
            } else {
                $scope.ProdKeyValues = true;
                $scope.ProdSpecsValues = true;
                $scope.ProdImageValues = true;
                $scope.ProdPriceValues = true;
            }
            $scope.CommonFuncationProductGrid();
        }

        $scope.imageBtnClick = function () {
            $("#selectedattribute").hide();
            // Chdeck Box UnCommand
            var checks = $('input[class="check"]');
            for (var i = 0; i < checks.length; i++) {
                var check = checks[i];
                if (!check.disabled) {
                    if (check.checked) {
                        check.checked = false;
                    }
                }
            }


            $scope.product_ids = "";
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;

            // Chdeck Box UnCommand

            $("#divProductGrid").removeClass("col-sm-2");
            $("#divProductGrid").addClass("col-sm-12");

            var classVal = $("#imageBtnItem").attr("class");
            if (classVal.includes("active2")) {
                $("#imageBtnItem").removeClass("active2");
            }

            else {
                $("#tagBtnItem").removeClass("active2");
                $("#moneyBtnItem").removeClass("active2");
                $("#imageBtnItem").addClass("active2");
                $("#keyBtnItem").removeClass("active2");

            }


            if ($scope.ProdImageValues == true) {
                $scope.ProdSpecsValues = true;
                $scope.ProdPriceValues = true;
                $scope.ProdKeyValues = true;
                $scope.ProdImageValues = false;
            } else {
                $scope.ProdImageValues = true;
                $scope.ProdSpecsValues = true;
                $scope.ProdPriceValues = true;
                $scope.ProdKeyValues = true;
            }
            $scope.CommonFuncationProductGrid();
        }

        $scope.CommonFuncationProductGrid = function () {
            $scope.searchTypeValue = "";
            $rootScope.LoadData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID, $scope.SelectedProductGroupId, $scope.searchTypeValue, $scope.AttrValues, $scope.productGridSearchValue, $scope.searchType, $scope.ProdSpecsValues, $scope.ProdPriceValues, $scope.ProdKeyValues, $scope.ProdImageValues);
            $scope.selecteddRow = null;
            $rootScope.ProductCount = $rootScope.ProductCount + 1;
        }

        $scope.MulipleAttributenameResultsChanges = function (values) {
            $scope.TypeAttributeValues = [];

            if (values != undefined || values != "" || values != null) {


                var count = 1;

                var defaultdata = $localStorage.GetAllAttributesasSelected;


                dataFactory.getAttributeDetailsbyType(values).success(function (response) {

                    var GetValues = response;

                    for (var i = 0; GetValues.length > i; i++) {
                        for (var j = 0; defaultdata.length > j; j++) {
                            if (defaultdata[j]["Name"] == GetValues[i]) {
                                $scope.TypeAttributeValues.push({ "Value": count, "Name": GetValues[i] });

                                count = count + 1;
                            }
                        }
                    }
                }).error(function (response) {
                });
            }


            // mv22
        }
        //mv


        $scope.GetChangesAvailableData = function () {
            dataFactory.viewByHideProducts($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $localStorage.CategoryID).success(function (response) {

                var viewByChangesButton = response.split("~");
                if (viewByChangesButton[0] == "true") {
                    if (viewByChangesButton[1] == 0) {

                        $scope.SpecificationButton = true;

                        //mm22
                        var classVal = $("#tagBtn").attr("class");
                        $("#tagBtn").removeClass("active");
                        ////mm22
                    }
                    else {
                        $("#tagBtn").addClass("active");
                        $scope.SpecificationButton = false;
                    }

                    if (viewByChangesButton[2] == 0) {

                        $scope.ImageButton = true;
                        //mm22
                        var classVal = $("#imageBtn").attr("class");
                        $("#imageBtn").removeClass("active");
                        ////mm22
                    }
                    else {
                        $("#imageBtn").addClass("active");
                        $scope.ImageButton = false;

                    }
                    if (viewByChangesButton[3] == 0) {

                        $scope.PriceButton = true;
                        //mm22
                        var classVal = $("#moneyBtn").attr("class");
                        $("#moneyBtn").removeClass("active");
                        ////mm22
                    }
                    else {
                        $("#moneyBtn").addClass("active");
                        $scope.PriceButton = false;

                    }
                    if (viewByChangesButton[4] == 0) {

                        $scope.KeyButton = true;
                        //mm22
                        var classVal = $("#keyBtn").attr("class");
                        $("#keyBtn").removeClass("active");
                        ////mm22
                    }
                    else {
                        $("#keyBtn").addClass("active");
                        $scope.KeyButton = false;

                    }
                }

            });
        }

        //handle SendDown event


        $scope.$on("SendDown", function () {
            $scope.productGridSearchValue = "";
            $scope.product_ids = "";
            $scope.MulipleAttributename = "";
        });


        $scope.$on("ForResultEmpty", function () {
            $scope.ResultEmpty = false;
        });

        $scope.$on("ToHideAllTheProductGridFunctions", function () {

            $('.pull-right').show();
            $('#selectedattribute').hide();
            $('#panel').hide();
            $('#divProductGrid12').hide();
            $scope.ResultEmpty = true;
            //hide "view by" option in productgrid if no result found
            $scope.SpecificationButton = true;
            $scope.PriceButton = true;
            $scope.KeyButton = true;
            $scope.ImageButton = true;
            //hide "view by" option in productgrid if no result found
        });



        $scope.ResetClick = function () {
            $('#selectedattribute').hide();
            $('.check').prop("checked", false);
            DeleteValues = [];
            $scope.product_ids = "";
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;
            $('#panel').hide();
            $("#productpreview").hide();
            $scope.ProdSpecsValues = true;
            $scope.ProdPriceValues = true;
            $scope.ProdKeyValues = true;
            $scope.ProdImageValues = true;
            $scope.productGridSearchValue = "";
            $('.pull-right').show();
            $scope.ResultEmpty = false;
            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID, $scope.SelectedProductGroupId, "", $scope.AttrValues, $scope.productGridSearchValue, $scope.searchType, $scope.ProdSpecsValues, $scope.ProdPriceValues, $scope.ProdKeyValues, $scope.ProdImageValues);
            $scope.selecteddRow = null;
            $rootScope.ProductCount = $rootScope.ProductCount + 1;
            $("#divProductGrid").removeClass("col-sm-2");
            $("#divProductGrid").addClass("col-sm-12");
            $scope.productGridSearchValue = "";
            $scope.MulipleAttributename = "";
            $scope.GetChangesAvailableData();
        }
        $scope.ResetClickItem = function () {
            $('#selectedattribute').hide();
            $('.check').prop("checked", false);
            DeleteValues = [];
            $scope.product_ids = "";
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;
            $('#panel').hide();
            $("#productpreview").hide();
            $scope.ProdSpecsValues = true;
            $scope.ProdPriceValues = true;
            $scope.ProdKeyValues = true;
            $scope.ProdImageValues = true;
            $scope.productGridSearchValue = "";
            $('.pull-right').show();
            $scope.ResultEmpty = false;
            dataFactory.getCategoryFamilyId($rootScope.selecetedCatalogId, $rootScope.selectedcategory_id).success(function (response) {

                if (response == '0') {
                    $scope.Category_IDs = response.m_Item1;
                    $scope.Family_IDs = response.m_Item2;
                } else {
                    $scope.Category_IDs = response.m_Item1;
                    if (response.m_Item2 == "") {
                        $scope.Family_IDs = 0;
                    }
                    else {
                        $scope.Family_IDs = response.m_Item2;
                    }

                }
                $rootScope.LoadProdItemData($rootScope.selecetedCatalogId, $scope.Family_IDs, $localStorage.CategoryID, $scope.SelectedProductGroupId, "", $scope.AttrValues, $scope.productGridSearchValue, $scope.searchType, $scope.ProdSpecsValues, $scope.ProdPriceValues, $scope.ProdKeyValues, $scope.ProdImageValues);
                $scope.selecteddRow = null;
                $rootScope.ProductCount = $rootScope.ProductCount + 1;
                $("#divProductGrid").removeClass("col-sm-2");
                $("#divProductGrid").addClass("col-sm-12");
                $scope.productGridSearchValue = "";
                $scope.MulipleAttributename = "";
                $scope.GetChangesAvailableData();
            });
        }
        window.addEventListener('load', function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('#sampleProdgrid tbody').scroll(function (e) {
                /*
                Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
                of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain
                it's relative position at the left of the table.
                */
                $('#sampleProdgrid thead').css("left", -$("#sampleProdgrid tbody").scrollLeft()); //fix the thead relative to the body scrolling
                $('#sampleProdgrid thead th:nth-child(-n+9)').css("left", $("#sampleProdgrid tbody").scrollLeft()); //fix the first cell of the header
                $('#sampleProdgrid tbody td:nth-child(-n+9)').css("left", $("#sampleProdgrid tbody").scrollLeft()); //fix the first column of tdbody
            });

        });
        //window.addEventListener('load', function () {
        //    $('[data-toggle="tooltip"]').tooltip();
        //    $('#sampleProdgrid tbody').scroll(function (e) {
            
        //        $('#sampleProdgrid5 thead').css("left", -$("#sampleProdgrid5 tbody").scrollLeft()); //fix the thead relative to the body scrolling
        //        $('#sampleProdgrid5 thead th:nth-child(-n+9)').css("left", $("#sampleProdgrid5 tbody").scrollLeft()); //fix the first cell of the header
        //        $('#sampleProdgrid5 tbody td:nth-child(-n+9)').css("left", $("#sampleProdgrid5 tbody").scrollLeft()); //fix the first column of tdbody
        //    });

        //});

        $scope.ResetEditClick = function () {
            $scope.searchText = '';
            $scope.callAttributeNameSearch();
        }

        $scope.LoadMoreData = function () {
            if ($scope.ProdEditPageCount != 0) {

                $scope.attributesValuesForEdit = [];
                $scope.ProdEditPageCount = ($scope.ProdEditPageCount + 10);

                for (var i = 0; i < $scope.ProdEditPageCount; i++) {
                    if ($scope.AllAttributeValues[i] != undefined) {
                        $scope.attributesValuesForEdit.push($scope.AllAttributeValues[i])
                    }
                }
                $scope.attributes = $scope.attributesValuesForEdit;
            }
        }

        //$scope.displayID = "true";
        //// $scope.ProdSpecsCurrentPage = 1;
        //$scope.ProdAttributesCountPerPage = "10";
        //$scope.editCurrentPage = "10";
        //$scope.callProductEditAttributesPerPage = function (pageper) {
        //    if (pageper != null && pageper != "0") {

        //        $scope.ProdAttributesCountPerPage = pageper;
        //        $scope.ProdSpecsCurrentPage = "1";
        //        //$scope.productGridSearchValue = "";

        //    }


        //};


        //$scope.callProductEditPaging = function (pageno) {
        //    if (pageno != null && pageno <= $scope.totalSpecsProdPageCount && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {

        //        if (pageno == 'PREV' && $scope.ProdSpecsCurrentPage != 1) {
        //            $scope.ProdSpecsCurrentPage = parseInt($scope.ProdSpecsCurrentPage) - 1;
        //        } else if (pageno == 'PREV' && $scope.ProdSpecsCurrentPage == 1) {
        //            $scope.ProdSpecsCurrentPage = "1";
        //            //new
        //            $('#cboProdEditPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdSpecsCurrentPage;
        //            return;
        //        } else if (pageno == 'NEXT' && $scope.totalSpecsProdPageCount != 1) {
        //            $("#cboProdEditPageCountdown").data("kendoDropDownList").text("");
        //            $scope.ProdSpecsCurrentPage = parseInt($scope.ProdSpecsCurrentPage) + 1;
        //            if ($scope.ProdSpecsCurrentPage > $scope.totalSpecsProdPageCount) {
        //                $scope.ProdSpecsCurrentPage = $scope.totalSpecsProdPageCount;
        //            }
        //        }
        //            //new
        //        else if (pageno == 'NEXT' && $scope.totalSpecsProdPageCount == 1) {
        //            $scope.ProdSpecsCurrentPage = "1";
        //            $('#cboProdEditPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdSpecsCurrentPage;
        //            return;
        //        }

        //        else if ($scope.totalSpecsProdPageCount == 1) {
        //            $scope.ProdSpecsCurrentPage = "1";
        //            $scope.ProdSpecsCurrentPage = "1";
        //            $('#cboProdEditPageCountdown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdSpecsCurrentPage;
        //            return;
        //        }
        //        else {

        //            $("#cboProdEditPageCountdown").data("kendoDropDownList").text("");
        //            //$scope.ProdSpecsCurrentPage = parseInt($scope.ProdSpecsCurrentPage) + 1;
        //            $timeout(($scope.ProdSpecsCurrentPage = pageno), 500);


        //            $scope.ProdSpecsCurrentPage = "1";
        //            $scope.ProdSpecsCurrentPage = pageno;
        //        }
        //        $rootScope.rowselectedEvent();
        //    }
        //};

        $scope.$on("LoadProductGroupAssociate", function () {
            $rootScope.ProductGroupNameForFamily.read();
        });

        $scope.searchcathashKeypress = function (e) {
            $scope.partno = $('#cathashSearchValue').val();
        }

        $scope.CancelMulipleAttributeProducts = function () {
            $scope.detachProductId = [];
            $("#panel").hide();
            $("#panel1").hide();
            $('#selectedattributenoresultfound').hide();
            $('#filterAttributePack').show();
            $('#selectedattributeEdit').hide();
            $('.pull-right').show();
            $("div#noresultfound").show();
            $('#selectedattribute').hide();
            $("#myDIV").show();
            $("#divProductGrid").show();
            $("#divProductGrid").removeClass("col-sm-2");
            $("#divProductGrid").removeClass("col-sm-3");
            $("#divProductGrid").addClass("col-sm-12");
            $("#check").prop("checked", false);
            $("#checkall").prop("checked", false);
            $('.check').prop("checked", false);
            $scope.product_ids = "";
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;
        }


        $rootScope.LoaAttributeDataProduct = function () {
            $scope.GetAllProductAttributesNew.read();
        };

        // To add a concept fot getting a selected data;

        $rootScope.GetAllLoadDataBasedonViews = function () {
            $scope.searchTypeValue = "";
            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID, $scope.SelectedProductGroupId, $scope.searchTypeValue, $scope.AttrValues, $scope.productGridSearchValue, $scope.searchType, $scope.ProdSpecsValues, $scope.ProdPriceValues, $scope.ProdKeyValues, $scope.ProdImageValues);
            $scope.selecteddRow = null;
            $rootScope.ProductCount = $rootScope.ProductCount + 1;
        };


        $scope.displayHtmlPreview = function (t) {
            var gettextareaName = "textBoxValue" + t;
            $scope.gettextareaValue = $("textarea[name=" + gettextareaName + "]").val();
        };

        $scope.workflowitemProductChange = function (e) {
            //$scope.Product.STATUS_NAME = e.sender.value();
            $scope.selectedRow["WORKFLOW STATUS"] = e.sender.value();
        };

        $scope.workflowProductDataSource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            serverFiltering: true,
            transport: {
                read: function (options) {
                    //dataFactory.getWorkflow().success(function (response) {

                    dataFactory.getworkflowforproduct($rootScope.getProduct_id, $scope.Family.FAMILY_ID).success(function (response) {
                        options.success(response);

                    }).error(function (error) {
                        options.error(error);
                    });


                }
            }
        });

        $scope.workflowProductDataSourceProductList = new kendo.data.DataSource({
            type: "json", autoBind: false,
            serverFiltering: true,
            transport: {
                read: function (options) {
                    //dataFactory.getWorkflow().success(function (response) {

                    dataFactory.getworkflowforproduct($rootScope.getProduct_id, $scope.Family.FAMILY_ID).success(function (response) {
                        options.success(response);

                    }).error(function (error) {
                        options.error(error);
                    });


                }
            }
        });

        $scope.workflowProductfordropdown = new kendo.data.DataSource({
            type: "json", autoBind: false,
            serverFiltering: true,
            transport: {
                read: function (options) {

                    dataFactory.getworkflowforproductdropdown($rootScope.getProduct_id).success(function (response) {
                        options.success(response);


                    }).error(function (error) {
                        options.error(error);
                    });


                }
            }
        });


        /* Drag and drop functionality start */


        //$scope.beforeDrop = function(event) {
        //    var deferred = $q.defer();
        //    if (confirm('Are you sure???')) {
        //        deferred.resolve();
        //    } else {
        //        deferred.reject();
        //    }
        //    return deferred.promise;
        //};

        $scope.beforeDrop = function (event) {
            var deferred = $q.defer();
            if (confirm) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Are you sure you want to move the product?',
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {
                        if (result === "Yes") {
                            deferred.resolve();
                        }
                        else if (result === "No") {
                            deferred.reject();
                        }
                    }

                });

            }

            //else {
            //    deferred.reject();
            //}
            return deferred.promise;
        };

        $scope.isDragged = false;

        $scope.startDrag = function (ui, event, item) {
            $scope.dragSortOrder = item.item.SORT;
            $scope.isDragged = true;
            $rootScope.ProductDrag = [];
            $rootScope.ProductDrag = item.item;
            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };


        $scope.startDrop = function (ui, event, item) {
            $scope.dropSortOrder = item.item.SORT;
            $scope.isDragged = true;
            $rootScope.ProductDrop = [];
            $rootScope.ProductDrop = item.item;

            $rootScope.ProductDrag["SORT"] = $scope.dropSortOrder;
            $rootScope.ProductDrop["SORT"] = $scope.dragSortOrder;

            $scope.updateSortOrderFromDragDrop();

            if (!$scope.$$phase) {
                $scope.$apply();
            }
        };


        $scope.updateSortOrderFromDragDrop = function () {
            $scope.blurCheck = false;
            dataFactory.SaveProductSpecs($rootScope.ProductDrag, $localStorage.CategoryID, 0, $scope.blurCheck, $rootScope.ProductDrop).success(function (response) {
                if (response != null) {

                    $scope.blurCheck = false;
                    $scope.isDragged = false;
                    $scope.LoadMultipletableData($scope.group_id);
                    $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                }

            }).error(function (error) {
                options.error(error);
            });
        }


        /* Drag and drop functionality End */




       

    }]);


