﻿LSApp.controller('AttributeGrouping', ['$scope', 'dataFactory', '$http', '$compile', '$rootScope', '$localStorage', function ($scope, dataFactory, $http, $compile, $rootScope, $localStorage) {
    // $scope.family_ID = 0;
    // $scope.seelectCatalogID = 0;
    $scope.Attr_Type = 0;
    $scope.AttributeDataSource = [
        { id: 0, DATA_TYPE: "All" },
        { id: 1, DATA_TYPE: "Item Specifications" },
        { id: 3, DATA_TYPE: "Item Image / Attachment" },
        { id: 4, DATA_TYPE: "Item Price" },
        { id: 6, DATA_TYPE: "Item key" }];

    $scope.layoutType = [
        { id: "Horizontal", DATA_TYPE: "Horizontal" },
        { id: "Vertical", DATA_TYPE: "Vertical" }];

    //Declartions
    // $scope.selectedAttributetype = 0;
    // $scope.selectedLayoutType = "Horizontal";



    $scope.AttrtypeChangetab = function (e) {
        if ($scope.Group.GROUP_ID !== 0) {
            $scope.Attr_Type = e.sender.value();
            $scope.GetAllCatalogfamilyattributesdataSource.read();
        }
    };

    $scope.layoutTypeChange = function (e) {
        if ($scope.Group.GROUP_ID !== 0) {
            $scope.Group.LAYOUT = e.sender.value();
            dataFactory.SaveMultipleLayout($scope.Group.GROUP_ID, $scope.Group.LAYOUT).success(function (response) {
                // $scope.GetAllmultipleGroupdataSource.read();
              //  options.success(response);
            }).error(function (error) {
               // options.error(error);
            });
        }
    };
    $scope.catalogtypeChange = function (e) {
        $scope.selectedcatalog1(e.sender.value());
    };

    $scope.selectedcatalog1 = function (catalogId) {
        if (catalogId !== "") {
            $scope.Group.GROUP_ID = catalogId;
            if ($scope.Group.GROUP_ID !== 0 && $scope.Group.GROUP_ID !== "") {
                $scope.GetAllCatalogfamilyattributesdataSource.read();
                $scope.prodavailablefamilyattrdataSource.read();
                //$scope.selectcatalogattr(0);
            } else {
                $scope.Group.GROUP_ID = 0;
                $scope.GetAllCatalogfamilyattributesdataSource.read();
                $scope.prodavailablefamilyattrdataSource.read();
            }
        } else {
            $scope.Group.GROUP_ID = 0;
            $scope.GetAllCatalogfamilyattributesdataSource.read();
            $scope.prodavailablefamilyattrdataSource.read();
        }
    };

    $scope.attributeGroupDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getGroupTableDetails($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.LayoutGroupDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetGroupLayout().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.GetAllCatalogfamilyattributesdataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, pageable: true, pageSize: 10,
        sort: { field: "ATTRIBUTE_ID", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getMultipleTableLeftTreeEntities($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.Attr_Type, $scope.Group.GROUP_ID).success(function (response) {

                   
                        for (var i = 0 ; response.length > i ; i++) {
                            if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                            }

                            if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                            }
                        }
                                                      

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        }

    });

    $scope.mainFamilyGridOptions = {
        dataSource: $scope.GetAllCatalogfamilyattributesdataSource,
        pageable: { buttonCount: 10 },
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px" }]
    };

    $scope.updateSelection = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };
    $scope.prodavailablefamilyattrdataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, sort: { field: "SORT_ORDER", dir: "asc" },
        pageable: { buttonCount: 10 },
        pageSize: 10,
        transport: {
            read: function (options) {
                dataFactory.GetAllattributesMultipleTable($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.Group.GROUP_ID).success(function (response) {


                    for (var i = 0 ; response.length > i ; i++) {
                        if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                        }

                        if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                        }
                    }

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: {
                        type: "boolean"
                    },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    SORT_ORDER: { editable: false }
                }
            }
        }

    });

    $scope.selectedfamilyGridOptions = {
        dataSource: $scope.prodavailablefamilyattrdataSource,
        pageable: { buttonCount: 10 },
        selectable: "multiple", dataBound: onDataBound,
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforprodfamily" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px" }
        ]
    };
    $scope.updateSelection1 = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };

    $scope.MultipleSavePublishAttributes = function () {
        if ($scope.Group.GROUP_ID !== 0 && $scope.Group.GROUP_ID !== "") {
            dataFactory.MultipleSavePublishAttributes($scope.GetAllCatalogfamilyattributesdataSource._data, $scope.Group.GROUP_ID).success(function (response) {
                $scope.GetAllCatalogfamilyattributesdataSource.read();
                $scope.prodavailablefamilyattrdataSource.read();
                //$scope.GetAllmultipleGroupdataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        }
    };
    $scope.MultipleDeletePublishAttributes = function () {
        if ($scope.Group.GROUP_ID !== 0 && $scope.Group.GROUP_ID !== "") {
            var g = $scope.multipleselectgrid.select();
            g.each(function (index, row) {
                var selectedItem = $scope.multipleselectgrid.dataItem(row);
                dataFactory.MultipleDeletePublishAttributes(selectedItem, $scope.Group.GROUP_ID).success(function (response) {
                    $scope.GetAllCatalogfamilyattributesdataSource.read();
                    $scope.prodavailablefamilyattrdataSource.read();
                    //  $scope.GetAllmultipleGroupdataSource.read();
                }).error(function (error) {
                    options.error(error);
                });
            });
        }
    };

    //$scope.GetAttributeGrouping = function () {
    //    dataFactory.GetAttributeGroupings($scope.Group.GROUP_ID).success(function (response) {
    //        options.success(response);
    //    }).error(function (error) {
    //        options.error(error);
    //    });
    //};
    
    var updownIndex = null;

    function onDataBound(e) {
        if (updownIndex != null) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {

                if (updownIndex == i) {
                    var isChecked = this.tbody.find("tr[data-uid='" + view[i].uid + "']").find(".chkbxforprodfamily").attr("checked");
                    if (isChecked) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .addClass("k-state-selected");
                    }

                }
            }

            updownIndex = null;
        }
    }
   
    $scope.BtnMoveUpClick = function () {

        var g = $scope.multipleselectgrid.select();

        if (g.length === 1) {

            g.each(function (index, row) {
                var selectedItem = $scope.multipleselectgrid.dataItem(row);
                var data = $scope.multipleselectgrid.dataSource.data();
                var dataRows = $scope.multipleselectgrid.items();
                var selectedRowIndex = dataRows.index(g);
                var sortOrder = data[selectedRowIndex - 1].SORT_ORDER;
                dataFactory.BtnMoveUpAttrGroupClick(sortOrder, selectedItem, $scope.Group.GROUP_ID).success(function (response) {
                    $scope.GetAllCatalogfamilyattributesdataSource.read();
                    $scope.prodavailablefamilyattrdataSource.read();
                    updownIndex = selectedRowIndex - 1;
                }).error(function (error) {
                    options.error(error);
                });
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                //type: "info"
            });

        }
    };


    $scope.BtnMoveDownClick = function () {

        var g = $scope.multipleselectgrid.select();
        if (g.length === 1) {
            g.each(function (index, row) {
                var selectedItem = $scope.multipleselectgrid.dataItem(row);
                var data = $scope.multipleselectgrid.dataSource.data();
                var dataRows = $scope.multipleselectgrid.items();
                var selectedRowIndex = dataRows.index(g);
                var sortOrder = data[selectedRowIndex + 1].SORT_ORDER;
                dataFactory.BtnMoveDownAttrGroupClick(sortOrder, selectedItem,$scope.Group.GROUP_ID).success(function (response) {
                    $scope.GetAllCatalogfamilyattributesdataSource.read();
                    $scope.prodavailablefamilyattrdataSource.read();
                    updownIndex = selectedRowIndex + 1;
                }).error(function (error) {
                    options.error(error);
                });
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                //type: "info"
            });

        }
    };

    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        }
        else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
            //  $scope.GetAttributeGrouping();
        }
    };


    $scope.init();
}]);