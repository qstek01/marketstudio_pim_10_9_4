﻿
LSApp.controller("familyAttributeSetupController", ["$scope", "$window", "$location", "$routeParams", "dataFactory", '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $rootScope, $localStorage) {

    function groupHeaderName(e) {

        if (e.value === 1) {
            return "Product Technical Specification (" + e.count + " items)";
        }
        else if (e.value === 3) {
            return "Product Image / Attachment (" + e.count + " items)";
        }
        else if (e.value === 4) {
            return "Product Price (" + e.count + " items)";
        } else if (e.value === 6) {
            return "Product Key (" + e.count + " items)";
        } else if (e.value === 7) {
            return "Family Description (" + e.count + " items)";
        } else if (e.value === 9) {
            return "Family Image / Attachment (" + e.count + " items)";
        } else if (e.value === 11) {
            return "Family Specifications (" + e.count + " items)";
        }
        else if (e.value === 12) {
            return "Family Price (" + e.count + " items)";
        }
        else if (e.value === 13) {
            return "Family Key (" + e.count + " items)";
        } else {
            return "Product Technical Specification (" + e.count + " items)";
        }
    }
    $scope.familyGetAllCatalogattributesdataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, pageable: true, pageSize: 5,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetAllCatalogFamilyAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
               { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });

    $scope.familymainGridOptions = {
        dataSource: $scope.familyGetAllCatalogattributesdataSource,
        pageable: { buttonCount: 5 },
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name" },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderName
            }]
    };

    $scope.updateSelection = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };
    $scope.familyprodfamilyattrdataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        pageable: true,
        pageSize: 5, sort: { field: "SORT_ORDER", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getPublishedfamilyAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: {
                        type: "boolean"
                    },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    SORT_ORDER: { editable: false }
                }
            }
        }

    });

    $scope.familyselectedGridOptions = {

        dataSource: $scope.familyprodfamilyattrdataSource,
        pageable: { buttonCount: 5 },
        selectable: "multiple",
        dataBound: onDataBound,
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforprodfamily" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px" }
        ]
    };

    var updownIndex = null;

    function onDataBound(e) {
        if (updownIndex != null) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {

                if (updownIndex == i) {
                    var isChecked = this.tbody.find("tr[data-uid='" + view[i].uid + "']").find(".chkbxforprodfamily").attr("checked");
                    if (isChecked) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .addClass("k-state-selected");
                    }

                }
            }

            updownIndex = null;
        }
    }

    $scope.updateSelection1 = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };

    $scope.UnPublishFamilyAttributes = function () {
        dataFactory.UnPublishFamilyAttributes($scope.familyprodfamilyattrdataSource._data, $rootScope.selecetedCategoryId).success(function (response) {

            $rootScope.LoaAttributeDataFamily();


            $scope.familyGetAllCatalogattributesdataSource.read();
            $scope.familyprodfamilyattrdataSource.read();

        }).error(function (error) {
            options.error(error);
        });
        $rootScope.familySpecsDatasource.read();
        $scope.LoadFamilySpecsData();
        $scope.winFamilyAttributeSetup.close();
        $('#FamilySpecsTab').css("display", "block");

    };
    $scope.RefreshFamilyAttributes = function () {
        $scope.familyGetAllCatalogattributesdataSource.read();
        $scope.familyprodfamilyattrdataSource.read();
        $scope.LoadFamilySpecsData();
        $rootScope.familySpecsDatasource.read();
    };
    //$scope.PublishAttributes = function () {
    //    dataFactory.PublishAttributes($scope.prodfamilyattrdataSource._data).success(function (response) {
    //        $scope.GetAllCatalogattributesdataSource.read();
    //        $scope.prodfamilyattrdataSource.read();
    //    }).error(function (error) {
    //        options.error(error);
    //    });
    //};
    $scope.SavePublishAttributes = function () {
        dataFactory.SaveFamilyPublishAttributes($scope.familyGetAllCatalogattributesdataSource._data, $rootScope.selecetedCategoryId).success(function (response) {
            $scope.familyGetAllCatalogattributesdataSource.read();
            $scope.familyprodfamilyattrdataSource.read();
        }).error(function (error) {
            options.error(error);
        });
    };
    $scope.DeletePublishAttributes = function () {
        var g = $scope.prodselectedgrid.select();
        g.each(function (index, row) {
            var selectedItem = $scope.prodselectedgrid.dataItem(row);
            dataFactory.DeleteFamilyPublishAttributes(selectedItem, $rootScope.selecetedCategoryId).success(function (response) {
                $scope.familyGetAllCatalogattributesdataSource.read();
                $scope.familyprodfamilyattrdataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        });
    };

    $scope.BtnFamilyMoveUpClick = function () {
        var g = $scope.prodselectedgrid.select();
        if (g.length === 1) {
            g.each(function (index, row) {
                var selectedItem = $scope.prodselectedgrid.dataItem(row);
                var data = $scope.prodselectedgrid.dataSource.data();
                var dataRows = $scope.prodselectedgrid.items();
                var selectedRowIndex = dataRows.index(g);
                var sortOrder = data[selectedRowIndex - 1].SORT_ORDER;
                dataFactory.BtnFamilyMoveUpClick(sortOrder, selectedItem, $rootScope.selecetedCategoryId).success(function (response) {
                    $scope.familyGetAllCatalogattributesdataSource.read();
                    $scope.familyprodfamilyattrdataSource.read();
                    updownIndex = selectedRowIndex - 1;
                }).error(function (error) {
                    options.error(error);
                });
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                type: "error"
            });

        }
    };


    $scope.BtnFamilyMoveDownClick = function () {

        var g = $scope.prodselectedgrid.select();
        if (g.length === 1) {
            g.each(function (index, row) {
                var selectedItem = $scope.prodselectedgrid.dataItem(row);
                var data = $scope.prodselectedgrid.dataSource.data();
                var dataRows = $scope.prodselectedgrid.items();
                var selectedRowIndex = dataRows.index(g);
                var sortOrder = data[selectedRowIndex + 1].SORT_ORDER;
                dataFactory.BtnFamilyMoveDownClick(sortOrder, selectedItem, $rootScope.selecetedCategoryId).success(function (response) {
                    $scope.familyGetAllCatalogattributesdataSource.read();
                    $scope.familyprodfamilyattrdataSource.read();
                    updownIndex = selectedRowIndex + 1;
                }).error(function (error) {
                    options.error(error);
                });
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                type: "error"
            });

        }
    };



    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        } else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
        }

    };
    $scope.init();
}]);
