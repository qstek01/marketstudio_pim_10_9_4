﻿LSApp.controller('familymultipletabledesignerController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, $rootScope, $localStorage) {
    $scope.multiplelayoutType = 'Yes';
    $scope.multiplestructureType = '';
    $scope.multipleDispalyRowHeaderCheckEditor = {
        value1: true
    };
    $scope.multipleDisplayColumnHeaderCheckBox = {
        value1: false
    };
    $scope.multipleDispalySummaryHeaderCheckEditor = {
        value1: false
    };
    $scope.multipleVerticalTable_ChkBox = {
        value1: false
    };
    $scope.multipleStructureType = {
        dataSource: {
            data: [
                { Text: "Horizontal", value: "Horizontal" },
                { Text: "Vertical", value: "Vertical" },
                { Text: "Super Table", value: "SuperTable" }
            ]
        },
        dataTextField: "Text",
        dataValueField: "value"
    };
    $scope.multipleStructure = {
        structureName: "Sample",
        defaultlayout: true,
        tabletype: ""
    };
    $scope.multipleStructureTypeChange = function (e) {
        $scope.multipleStructure.tabletype = e.sender.value();
        if ($scope.multipleStructure.tabletype == 'Vertical') {
            $scope.multipleVerticalTable_ChkBox = {
                value1: true
            };

        } else {
            $scope.multipleVerticalTable_ChkBox = {
                value1: false
            };
        }
    };
    $scope.multipletableDesignerFields = {
        ComboGroupByColumn: 0,
        ComboSummaryGroup: 0,
        EmptyCellTextBox: '',
        PivotHeaderTextBox: ''
    };
    $scope.multipletableDesignerFields.PivotHeaderTextBox = '';
    $scope.multipletableDesignerFields.EmptyCellTextBox = '';



    $scope.multipleonDragEnd = function () {
        //var draggable = $("#treeviewallAttributeList");
        //if (!draggable.data("kendoDraggable").dropped) {
        //    alert("sdf");
        //}
    };
    $scope.init();
    $scope.multipleStructureName = 'Default Layout';
    $scope.multiplefamilyTableStructure = "<?xml version=\"1.0\" encoding=\"utf-8\"?><TradingBell TableType=\"Horizontal\"><SummaryGroupField></SummaryGroupField><TableGroupField></TableGroupField><PlaceHolderText><![CDATA[]]></PlaceHolderText><DisplayRowHeader>True</DisplayRowHeader><DisplayColumnHeader>False</DisplayColumnHeader><DisplaySummaryHeader>False</DisplaySummaryHeader><VerticalTable>False</VerticalTable><PivotHeaderText><![CDATA[]]></PivotHeaderText><MergeRowHeader></MergeRowHeader><MergeSummaryFields></MergeSummaryFields></TradingBell>";
    $scope.Flag = "MultipleTable";
    $rootScope.multipleallAttributeListDatasource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                dataFactory.getAllAttributeDataSource($scope.StructureName,$rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.multiplefamilyTableStructure, $scope.Flag, $scope.Group.GROUP_ID,0).success(function (response) {



                    if (response.length != 0) {
                        for (var i = 0 ; response.length > i ; i++) {
                            if (response[i]["ATTRIBUTE_ID"] == 1) {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                            }

                            if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                            }
                        }

                    }


                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });

            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_NAME",
                hasChildren: "hasChildren"
            }
        }
    });
    $rootScope.multipleleftTreeNewDataSource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                //$scope.getStructureValues();
                dataFactory.DecodeLayoutXmlLeftTree($scope.multipleStructureName, $scope.multiplefamilyTableStructure, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, options.data.ATTRIBUTE_NAME, $scope.Flag, $scope.Group.GROUP_ID,0).success(function (response) {


                    if (response.length != 0) {
                        for (var i = 0 ; response.length > i ; i++) {
                            if (response[i]["ATTRIBUTE_ID"] == 1) {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                            }

                            if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                            }
                        }

                    }

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });

            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_NAME",
                hasChildren: "hasChildren"
            }
        }
    });

    $rootScope.multiplerightTreeNewDataSource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                dataFactory.DecodeLayoutXmlRightTree($scope.multipleStructureName, $scope.multiplefamilyTableStructure, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, options.data.ATTRIBUTE_NAME, $scope.Flag, $scope.Group.GROUP_ID,0).success(function (response) {

                    if (response.length != 0)
                    {
                        for (var i = 0 ; response.length > i ; i++) {
                            if (response[i]["ATTRIBUTE_ID"] == 1) {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                            }

                            if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                            }
                        }

                    }



                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_NAME",
                hasChildren: "hasChildren"
            }
        }
    });

    $rootScope.multiplesummaryTreeNewDataSource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                dataFactory.DecodeLayoutXmlSummaryTree($scope.multipleStructureName, $scope.multiplefamilyTableStructure, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, options.data.ATTRIBUTE_NAME, $scope.Flag, $scope.Group.GROUP_ID,0).success(function (response) {
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_NAME",
                hasChildren: "hasChildren"
            }
        }
    });

    $rootScope.multiplecolumnTreeNewDataSource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                dataFactory.DecodeLayoutXmlColumnTree($scope.multipleStructureName, $scope.multiplefamilyTableStructure, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, options.data.ATTRIBUTE_NAME, $scope.Flag, $scope.Group.GROUP_ID,0).success(function (response) {
                    if (response.length != 0) {
                        for (var i = 0 ; response.length > i ; i++) {
                            if (response[i]["ATTRIBUTE_ID"] == 1) {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                            }

                            if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                            }
                        }

                    }
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_NAME",
                hasChildren: "hasChildren"
            }
        }
    });



    $rootScope.multipletableGroupSummaryDataSource = new kendo.data.DataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                //$scope.getStructureValues();
                dataFactory.tableGroupSummaryDataSource($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.multiplefamilyTableStructure, $scope.Flag, $scope.Group.GROUP_ID,0).success(function (response) {
                    options.success(response);
                    //$scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                }).error(function (error) {
                    options.error(error);
                });

            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_ID"
            }
        }
    });

    $rootScope.multipletableGroupHeaderDataSource = new kendo.data.DataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                //$scope.getStructureValues();
                dataFactory.GettableGroupHeaderDataSource($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.multiplefamilyTableStructure, $scope.Flag, $scope.Group.GROUP_ID,0).success(function (response) {
                    options.success(response);
                    //$scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                }).error(function (error) {
                    options.error(error);
                });

            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_ID"
            }
        }
    });


    $scope.multipleStructureNameDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {

                 $scope.Family.FAMILY_ID = '' + $scope.Family.FAMILY_ID + '';
                //adding double quotes for family id to satisfy contains condition//

                if ($scope.Family.FAMILY_ID.contains('~')) {
                    var Family_ID = $scope.Family.FAMILY_ID.split('~');
                    $scope.Family.FAMILY_ID = Family_ID[1];
                }
                dataFactory.GetStructureNameDataSourceDetails($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId, $scope.Flag, $scope.Group.GROUP_ID).success(function (response) {
                    debugger
                    if (response.length > 0) {
                        $scope.multipleStructureNameinitial = response[0].STRUCTURE_NAME;
                        $scope.multiplefamilyTableStructure = response[0].MULTIPLE_TABLE_STRUCTURE;
                        for (var i = 0; i < response.length; i++) {
                            if (response[i].IS_DEFAULT) {
                                $scope.multipleStructureName = response[i].STRUCTURE_NAME;
                                $scope.multipleStructureNameinitial = response[i].STRUCTURE_NAME;
                                $scope.multiplefamilyTableStructure = response[i].MULTIPLE_TABLE_STRUCTURE;
                                $rootScope.multipleleftTreeNewDataSource.read();
                                $rootScope.multiplerightTreeNewDataSource.read();
                                $rootScope.multiplesummaryTreeNewDataSource.read();
                                $rootScope.multiplecolumnTreeNewDataSource.read();
                                $rootScope.multipleallAttributeListDatasource.read();
                                $rootScope.multipletableGroupSummaryDataSource.read();
                                $rootScope.multipletableGroupHeaderDataSource.read();
                            }
                        }
                        $scope.multiplegetSelectedStructureLoad();
                    }
                    options.success(response);
                    if (response.length > 0) {
                        $scope.multipleStructureName = response[0].STRUCTURE_NAME;
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.multipleStructureNameChange = function (e) {
        $scope.multipleStructureName = e.sender.value();
        if ($scope.multipleStructureName !== "") {
            dataFactory.GetTableStructure($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId, $scope.multipleStructureName, $scope.Flag, $scope.Group.GROUP_ID).success(function (response) {
                if (response != null) {
                    $scope.multiplefamilyTableStructure = response[0].MULTIPLE_TABLE_STRUCTURE;
                    if ($scope.multipleStructureName != "" && $scope.multiplefamilyTableStructure != "") {
                        $rootScope.multipleleftTreeNewDataSource.read();
                        $rootScope.multiplerightTreeNewDataSource.read();
                        $rootScope.multiplesummaryTreeNewDataSource.read();
                        $rootScope.multiplecolumnTreeNewDataSource.read();
                        $rootScope.multipleallAttributeListDatasource.read();
                        $rootScope.multipletableGroupSummaryDataSource.read();
                        $rootScope.multipletableGroupHeaderDataSource.read();
                        var result = $scope.multiplefamilyTableStructure.match(/<VerticalTable>(.*?)<\/VerticalTable>/g).map(function (val) {
                            return val.replace(/<\/?VerticalTable>/g, '');
                        });
                        if (result == 'True' || result == 'true') {
                            $scope.multipleVerticalTable_ChkBox.value1 = true;
                        }
                        else {
                            $scope.multipleVerticalTable_ChkBox.value1 = false;
                        }

                    }
                    $scope.multiplegetSelectedStructureLoad();
                }
            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + error + '.',
                    type: "info"
                });
            });
        }
        else {
            $scope.multipleStructureName = $scope.multipleStructureNameinitial;
        }

    };
    $scope.multiplegetSelectedStructureLoad = function () {
        if ($scope.multipleStructureNameinitial != "" && $scope.multiplefamilyTableStructure != "" && $scope.multiplefamilyTableStructure !== undefined) {
            var result = $scope.multiplefamilyTableStructure.match(/<VerticalTable>(.*?)<\/VerticalTable>/g).map(function (val) {
                return val.replace(/<\/?VerticalTable>/g, '');
            });
            if (result == 'True' || result == 'true') {
                $scope.multipleVerticalTable_ChkBox.value1 = true;
            }
            else {
                $scope.multipleVerticalTable_ChkBox.value1 = false;
            }
            dataFactory.DecodeLayoutXmls($scope.multipleStructureName, $scope.multiplefamilyTableStructure, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.Flag, $scope.Group.GROUP_ID, 0).success(function (response1) {
                $scope.multipletableDesignerFields.ComboSummaryGroup = response1.SummaryGroupFieldValue;
                $scope.multipletableDesignerFields.ComboGroupByColumn = response1.TableGroupFieldValue;
                $scope.multipletableDesignerFields.EmptyCellTextBox = response1.PlaceHolderText;
                $scope.multipleDispalyRowHeaderCheckEditor.value1 = response1.DisplayRowHeader;
                $scope.multipleDisplayColumnHeaderCheckBox.value1 = response1.DisplayColumnHeader;
                $scope.multipleDispalySummaryHeaderCheckEditor.value1 = response1.DisplaySummaryHeader;
                $scope.multipletableDesignerFields.PivotHeaderTextBox = response1.PivotHeaderText;
                $scope.multipleVerticalTable_ChkBox.value1 = response1.VerticalTable;


            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + errot + '.',
                    type: "info"
                });
            });
        }

    };

    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        } else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
        }
        //  $scope.multiplegetSelectedStructureLoad();
        $scope.multipleStructureNameDataSource.read();
    };
    // function that gathers IDs of checked nodes
    function checkedNodeIds(nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].id);
            }
            if (nodes[i].hasChildren) {
                checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    }

    $scope.multipleonDropall = function (e) {
        
        var data = $('#multipletreeviewallAttributeList').data('kendoTreeView').dataItem(e.sourceNode);
        var allData = $('#multipletreeviewallAttributeList').data('kendoTreeView').dataItem(e.destinationNode);
        var leftData = $('#multipleleftTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var columnData = $('#multiplecolumnNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var rightData = $('#multiplerightNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var summaryData = $('#multiplesummaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var leftnav = undefined;
        if ($('#leftNavTreeViewKendoNavigator').data('kendoTreeView') != undefined) {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        }
        var lefttree = true;
        
        if (e.dropTarget.attributes.length >= 4) {
            if (e.dropTarget.attributes[3].nodeName === "ng-dblclick" && e.dropTarget.attributes[4].nodeName === "ng-right-click") {
                lefttree = false;
            }
            if (e.dropTarget.attributes[1].nodeName === "ng-true-value" && e.dropTarget.attributes[2].nodeName === "ng-false-value") {
                lefttree = false;
            }
        }
        if (lefttree) {
            
            if (e.destinationNode !== undefined && leftnav === undefined && e.destinationNode.id !== "leftNavTreeViewKendoNavigator") {
                var valid = true;
                // if (e.destinationNode.id.contains("treeviewallAttributeList") || e.destinationNode.id === "leftTreeViewKendo" || e.destinationNode.id === "columnNavTreeViewKendo" || e.destinationNode.id === "rightNavTreeViewKendo" || e.destinationNode.id === "summaryNavTreeViewKendo") {
                if (allData != undefined) {
                    if (!allData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                    if (!allData.hasChildren && e.dropPosition == "over" && allData.ATTRIBUTE_ID == -1) {
                        e.setValid(true);
                        valid = false;
                    }
                }
                if (leftData != undefined) {
                    if (!leftData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (columnData != undefined) {
                    if (!columnData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (rightData != undefined) {
                    if (!rightData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (summaryData != undefined) {
                    if (!summaryData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multiplecolumnNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "multiplecolumnNavTreeViewKendo") {
                        e.setValid(false);
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multipleleftTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multiplerightNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multiplesummaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "multiplesummaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });

                    }
                }
                // $scope.removeUnPublishAttr();

                if (valid) {
                    try {
                        var products = $rootScope.multipletableGroupHeaderDataSource.data();
                        // var node1 = products.get(data.ATTRIBUTE_ID);
                        var fulldataitemlength = products.length;
                        for (var i = 0; i < fulldataitemlength; i++) {
                            //var nodes = products.get(data.items[i].ATTRIBUTE_ID);
                            if (products[i].ATTRIBUTE_ID === data.ATTRIBUTE_ID) {
                                $rootScope.multipletableGroupHeaderDataSource.remove(products[i]);
                            }
                            if (data.hasChildren) {
                                var dataitemlength = data.items.length;
                                for (var j = 0; j < dataitemlength; j++) {
                                    //var nodes = products.get(data.items[j].ATTRIBUTE_ID);
                                    if (products[i].ATTRIBUTE_ID === data.items[j].ATTRIBUTE_ID) {
                                        $rootScope.multipletableGroupHeaderDataSource.remove(products[i]);
                                    }
                                }
                            }
                        }

                    } catch (e) {

                    }
                }
                if (e.destinationNode.id === "multiplesummaryNavTreeViewKendo") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var summaryAttributeListview = $("#multiplesummaryAttributeListview").data("kendoDropDownList");
                                summaryAttributeListview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));
                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var summaryAttributeListview1 = $("#multiplesummaryAttributeListview").data("kendoDropDownList");
                                        summaryAttributeListview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));
                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }

                //$scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                //} else {
                //    e.setValid(false);
                //}
            } else {
                e.setValid(false);
            }
        } else {
            e.setValid(false);
        }
    };
    $scope.multipleonDropLeft = function (e) {
        var data = $('#multipleleftTreeViewKendo').data('kendoTreeView').dataItem(e.sourceNode);
        var leftData = $('#multipleleftTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var allData = $('#multipletreeviewallAttributeList').data('kendoTreeView').dataItem(e.destinationNode);
        var columnData = $('#multiplecolumnNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var rightData = $('#multiplerightNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var summaryData = $('#multiplesummaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var leftnav = undefined;
        if ($('#leftNavTreeViewKendoNavigator').data('kendoTreeView') != undefined) {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        }
        var lefttree = true;
        if (e.dropTarget.attributes.length >= 4) {
            if (e.dropTarget.attributes[3].nodeName === "ng-dblclick" && e.dropTarget.attributes[4].nodeName === "ng-right-click") {
                lefttree = false;
            } if (e.dropTarget.attributes[1].nodeName === "ng-true-value" && e.dropTarget.attributes[2].nodeName === "ng-false-value") {
                lefttree = false;
            }
        }
        if (lefttree) {
            if (e.destinationNode !== undefined && leftnav === undefined && e.destinationNode.id !== "leftNavTreeViewKendoNavigator") {
                //if (e.destinationNode.id === "treeviewallAttributeList" || e.destinationNode.id === "leftTreeViewKendo" || e.destinationNode.id === "columnNavTreeViewKendo" || e.destinationNode.id === "rightNavTreeViewKendo" || e.destinationNode.id === "summaryNavTreeViewKendo") {
                var valid = true;
                if (allData != undefined) {
                    if (!allData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (leftData != undefined) {
                    if (!leftData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false); valid = false;
                    }
                }
                if (columnData != undefined) {
                    if (!columnData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (rightData != undefined) {
                    if (!rightData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (summaryData != undefined) {
                    if (!summaryData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multiplecolumnNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "multiplecolumnNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multipleleftTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multiplerightNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multiplesummaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "multiplesummaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });
                    }
                }


                // $scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                if (e.destinationNode.id === "multipletreeviewallAttributeList") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var allAttributeListtreeview = $("#multipleallAttributeListview").data("kendoDropDownList");
                                allAttributeListtreeview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));

                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var allAttributeListtreeview1 = $("#multipleallAttributeListview").data("kendoDropDownList");
                                        allAttributeListtreeview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));

                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }
                if (e.destinationNode.id === "multiplesummaryNavTreeViewKendo") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                //  $rootScope.tableGroupSummaryDataSource.add(data);
                                var summaryAttributeListview = $("#multiplesummaryAttributeListview").data("kendoDropDownList");
                                summaryAttributeListview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));
                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var summaryAttributeListview1 = $("#multiplesummaryAttributeListview").data("kendoDropDownList");
                                        summaryAttributeListview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));
                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }

                //} else {
                //    e.setValid(false);
                //}
            } else {
                e.setValid(false);
            }
        } else {
            e.setValid(false);
        }
        // $scope.removeUnPublishAttr();
    };
    $scope.multipleonDropColumn = function (e) {

        var data = $('#multiplecolumnNavTreeViewKendo').data('kendoTreeView').dataItem(e.sourceNode);
        var summaryData = $('#multiplesummaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var leftData = $('#multipleleftTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var columnData = $('#multiplecolumnNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var rightData = $('#multiplerightNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var allData = $('#multipletreeviewallAttributeList').data('kendoTreeView').dataItem(e.destinationNode);
        var leftnav = undefined;
        if ($('#leftNavTreeViewKendoNavigator').data('kendoTreeView') != undefined) {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        }
        var lefttree = true;
        if (e.dropTarget.attributes.length >= 4) {
            if (e.dropTarget.attributes[3].nodeName === "ng-dblclick" && e.dropTarget.attributes[4].nodeName === "ng-right-click") {
                lefttree = false;
            } if (e.dropTarget.attributes[1].nodeName === "ng-true-value" && e.dropTarget.attributes[2].nodeName === "ng-false-value") {
                lefttree = false;
            }
        }
        if (lefttree) {
            if (e.destinationNode !== undefined && leftnav === undefined && e.destinationNode.id !== "leftNavTreeViewKendoNavigator") {
                var valid = true;
                if (allData != undefined) {
                    if (!allData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (leftData != undefined) {
                    if (!leftData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (columnData != undefined) {
                    if (!columnData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (rightData != undefined) {
                    if (!rightData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (summaryData != undefined) {
                    if (!summaryData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                } //$scope.removeUnPublishAttr();
                if (e.destinationNode.id === "multipletreeviewallAttributeList") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var allAttributeListtreeview = $("#multipleallAttributeListview").data("kendoDropDownList");
                                allAttributeListtreeview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));

                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var allAttributeListtreeview1 = $("#multipleallAttributeListview").data("kendoDropDownList");
                                        allAttributeListtreeview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));

                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }
                if (e.destinationNode.id === "multiplesummaryNavTreeViewKendo") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var summaryAttributeListview = $("#multiplesummaryAttributeListview").data("kendoDropDownList");
                                summaryAttributeListview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));
                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var summaryAttributeListview1 = $("#multiplesummaryAttributeListview").data("kendoDropDownList");
                                        summaryAttributeListview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));
                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }
                // $scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                //} else {
                //    e.setValid(false);
                //}
            } else {
                e.setValid(false);
            }
        } else {
            e.setValid(false);
        }
    };
    $scope.multipleonDropSummary = function (e) {
        var data = $('#multiplesummaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.sourceNode);
        var summaryData = $('#multiplesummaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var leftData = $('#multipleleftTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var columnData = $('#multiplecolumnNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var rightData = $('#multiplerightNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var allData = $('#multipletreeviewallAttributeList').data('kendoTreeView').dataItem(e.destinationNode);
        var leftnav = undefined;
        if ($('#leftNavTreeViewKendoNavigator').data('kendoTreeView') != undefined) {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        }
        var lefttree = true;
        if (e.dropTarget.attributes.length >= 4) {
            if (e.dropTarget.attributes[3].nodeName === "ng-dblclick" && e.dropTarget.attributes[4].nodeName === "ng-right-click") {
                lefttree = false;
            } if (e.dropTarget.attributes[1].nodeName === "ng-true-value" && e.dropTarget.attributes[2].nodeName === "ng-false-value") {
                lefttree = false;
            }
        }
        if (lefttree) {
            if (e.destinationNode !== undefined && leftnav === undefined && e.destinationNode.id !== "leftNavTreeViewKendoNavigator") {
                var valid = true;
                if (allData != undefined) {
                    if (!allData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (leftData != undefined) {
                    if (!leftData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (columnData != undefined) {
                    if (!columnData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (rightData != undefined) {
                    if (!rightData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (summaryData != undefined) {
                    if (!summaryData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                // $scope.removeUnPublishAttr();
                if (e.destinationNode.id === "multipletreeviewallAttributeList") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var allAttributeListtreeview = $("#multipleallAttributeListview").data("kendoDropDownList");
                                allAttributeListtreeview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));

                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var allAttributeListtreeview1 = $("#multipleallAttributeListview").data("kendoDropDownList");
                                        allAttributeListtreeview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));

                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }
                if (valid) {
                    try {
                        var products = $rootScope.multipletableGroupSummaryDataSource.data();
                        // var node1 = products.get(data.ATTRIBUTE_ID);
                        var fulldataitemlength = products.length;
                        for (var i = 0; i < fulldataitemlength; i++) {
                            //var nodes = products.get(data.items[i].ATTRIBUTE_ID);
                            if (products[i].ATTRIBUTE_ID === data.ATTRIBUTE_ID) {
                                $rootScope.multipletableGroupSummaryDataSource.remove(products[i]);
                            }
                            if (data.hasChildren) {
                                var dataitemlength1 = data.items.length;
                                for (var j1 = 0; j1 < dataitemlength1; j1++) {
                                    //var nodes = products.get(data.items[j].ATTRIBUTE_ID);
                                    if (products[i].ATTRIBUTE_ID === data.items[j1].ATTRIBUTE_ID) {
                                        $rootScope.multipletableGroupSummaryDataSource.remove(products[i]);
                                    }
                                }
                            }
                        }
                    } catch (e) {

                    }
                }
                //  $scope.tableGroupHeaderDataSource = $rootScope.allAttributeListDatasource;
                //} else {
                //    e.setValid(false);
                //}
            } else {
                e.setValid(false);
            }
        } else {
            e.setValid(false);
        }
    };
    $scope.multipleonDropRight = function (e) {
        var data = $('#multiplerightNavTreeViewKendo').data('kendoTreeView').dataItem(e.sourceNode);
        var rightData = $('#multiplerightNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var allData = $('#multipletreeviewallAttributeList').data('kendoTreeView').dataItem(e.destinationNode);
        var columnData = $('#multiplecolumnNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var leftData = $('#multipleleftTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var summaryData = $('#multiplesummaryNavTreeViewKendo').data('kendoTreeView').dataItem(e.destinationNode);
        var leftnav = undefined;
        if ($('#leftNavTreeViewKendoNavigator').data('kendoTreeView') != undefined) {
            leftnav = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView').dataItem(e.destinationNode);
        }
        var lefttree = true;
        if (e.dropTarget.attributes.length >= 4) {
            if (e.dropTarget.attributes[3].nodeName === "ng-dblclick" && e.dropTarget.attributes[4].nodeName === "ng-right-click") {
                lefttree = false;
            } if (e.dropTarget.attributes[1].nodeName === "ng-true-value" && e.dropTarget.attributes[2].nodeName === "ng-false-value") {
                lefttree = false;
            }
        }
        if (lefttree) {
            if (e.destinationNode !== undefined && leftnav === undefined && e.destinationNode.id !== "leftNavTreeViewKendoNavigator") {
                var valid = true;
                if (rightData != undefined) {
                    if (!rightData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (leftData != undefined) {
                    if (!leftData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (columnData != undefined) {
                    if (!columnData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (allData != undefined) {
                    if (!allData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (summaryData != undefined) {
                    if (!summaryData.hasChildren && e.dropPosition == "over") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multiplecolumnNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "multiplecolumnNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multipleleftTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multiplerightNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.items === undefined && data.ATTRIBUTE_ID == -1) {
                    if (e.destinationNode.id === "multiplesummaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                    }
                }
                if (data.COLOR == "RED") {
                    if (e.destinationNode.id === "multiplesummaryNavTreeViewKendo") {
                        e.setValid(false);
                        valid = false;
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Unpublished Attributes can only be used as a Row Field.',
                            type: "error"
                        });
                    }
                }

                if (e.destinationNode.id === "multipletreeviewallAttributeList") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var allAttributeListtreeview = $("#multipleallAttributeListview").data("kendoDropDownList");
                                allAttributeListtreeview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));

                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var allAttributeListtreeview1 = $("#multipleallAttributeListview").data("kendoDropDownList");
                                        allAttributeListtreeview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));

                                    }
                                });
                            }
                        } catch (e) {

                        }
                    }
                }

                if (e.destinationNode.id === "multiplesummaryNavTreeViewKendo") {
                    if (valid) {
                        try {
                            if (data.ATTRIBUTE_ID !== "-1") {
                                var summaryAttributeListview = $("#multiplesummaryAttributeListview").data("kendoDropDownList");
                                summaryAttributeListview.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': data.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': data.ATTRIBUTE_ID }));
                                //$rootScope.tableGroupSummaryDataSource.add(data);
                            }
                            if (data.hasChildren) {
                                angular.forEach(data.items, function (value) {
                                    if (value.ATTRIBUTE_ID !== "-1") {
                                        var summaryAttributeListview1 = $("#multiplesummaryAttributeListview").data("kendoDropDownList");
                                        summaryAttributeListview1.dataSource.add(new kendo.data.Node({ 'ATTRIBUTE_NAME': value.ATTRIBUTE_NAME, 'ATTRIBUTE_ID': value.ATTRIBUTE_ID }));
                                    }
                                    // value.SORT = parseFloat(value.SORT);
                                });
                            }
                        } catch (e) {

                        }
                    }
                }

            } else {
                e.setValid(false);
            }
        } else {
            e.setValid(false);
        }
    };

    $scope.showmultipleTablePreview = function () {
        if ($rootScope.multiplecolumnTreeNewDataSource._data.length > 0) {
            if ($rootScope.multiplesummaryTreeNewDataSource._data.length > 0) {
                dataFactory.generateTableHtml($rootScope.multipleleftTreeNewDataSource._data, $rootScope.multiplerightTreeNewDataSource._data, $rootScope.multiplecolumnTreeNewDataSource._data, $rootScope.multiplesummaryTreeNewDataSource._data, $scope.multipletableDesignerFields.ComboSummaryGroup, $scope.multipletableDesignerFields.ComboGroupByColumn, $scope.multipletableDesignerFields.EmptyCellTextBox, $scope.multipleDispalyRowHeaderCheckEditor.value1, $scope.multipleDisplayColumnHeaderCheckBox.value1, $scope.multipleDispalySummaryHeaderCheckEditor.value1, $scope.multipleVerticalTable_ChkBox.value1, $scope.multipletableDesignerFields.PivotHeaderTextBox, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.multipleStructureName, $localStorage.CategoryID, $scope.Flag, $scope.Group.GROUP_ID,0).success(function (response) {
                    $scope.winTableDesignerPreview.refresh({ url: "../Views/App/Partials/SuperColumnPreview.html" });
                    $scope.winTableDesignerPreview.center().open();
                }).error(function (error) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + error + '.',
                        type: "error"
                    });
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'To construct Pivot Table both the Column Attribute and Summary Attribute fields needs to be populated.',
                    //type: "error"
                });
            }
        } else if ($rootScope.multiplecolumnTreeNewDataSource._data.length === 0 && $rootScope.multiplesummaryTreeNewDataSource._data.length > 0) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'To construct Pivot Table both the Column Attribute and Summary Attribute fields needs to be populated.',
                //type: "error"
            });
        }

        else {
            dataFactory.generateTableHtml($rootScope.multipleleftTreeNewDataSource._data, $rootScope.multiplerightTreeNewDataSource._data, $rootScope.multiplecolumnTreeNewDataSource._data, $rootScope.multiplesummaryTreeNewDataSource._data, $scope.multipletableDesignerFields.ComboSummaryGroup, $scope.multipletableDesignerFields.ComboGroupByColumn, $scope.multipletableDesignerFields.EmptyCellTextBox, $scope.multipleDispalyRowHeaderCheckEditor.value1, $scope.multipleDisplayColumnHeaderCheckBox.value1, $scope.multipleDispalySummaryHeaderCheckEditor.value1, $scope.multipleVerticalTable_ChkBox.value1, $scope.multipletableDesignerFields.PivotHeaderTextBox, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.multipleStructureName, $localStorage.CategoryID, $scope.Flag, $scope.Group.GROUP_ID,0).success(function (response) {
                $scope.winTableDesignerPreview.refresh({ url: "../Views/App/Partials/SuperColumnPreview.html" });
                $scope.winTableDesignerPreview.center().open();
            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + error + '.',
                    type: "error"
                });
            });
        }


    };

    $scope.showmultipleTableSave = function () {
        if ($rootScope.multiplecolumnTreeNewDataSource._data.length > 0) {
            if ($rootScope.multiplesummaryTreeNewDataSource._data.length > 0) {
                dataFactory.generateTableHtml($rootScope.multipleleftTreeNewDataSource._data, $rootScope.multiplerightTreeNewDataSource._data, $rootScope.multiplecolumnTreeNewDataSource._data, $rootScope.multiplesummaryTreeNewDataSource._data, $scope.multipletableDesignerFields.ComboSummaryGroup, $scope.multipletableDesignerFields.ComboGroupByColumn, $scope.multipletableDesignerFields.EmptyCellTextBox, $scope.multipleDispalyRowHeaderCheckEditor.value1, $scope.multipleDisplayColumnHeaderCheckBox.value1, $scope.multipleDispalySummaryHeaderCheckEditor.value1, $scope.multipleVerticalTable_ChkBox.value1, $scope.multipletableDesignerFields.PivotHeaderTextBox, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.multipleStructureName, $localStorage.CategoryID, $scope.Flag, $scope.Group.GROUP_ID,0).success(function (response) {
                    if (response != null) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Saved successfully.',
                            type: "info"
                        });
                    }
                }).error(function (error) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + error + '.',
                        type: "error"
                    });
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'To construct Pivot Table both the Column Attribute and Summary Attribute fields needs to be populated.',
                    //type: "error"
                });
            }
        } else if ($rootScope.multiplecolumnTreeNewDataSource._data.length === 0 && $rootScope.multiplesummaryTreeNewDataSource._data.length > 0) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'To construct Pivot Table both the Column Attribute and Summary Attribute fields needs to be populated.',
                //type: "error"
            });
        }

        else {
            dataFactory.generateTableHtml($rootScope.multipleleftTreeNewDataSource._data, $rootScope.multiplerightTreeNewDataSource._data, $rootScope.multiplecolumnTreeNewDataSource._data, $rootScope.multiplesummaryTreeNewDataSource._data, $scope.multipletableDesignerFields.ComboSummaryGroup, $scope.multipletableDesignerFields.ComboGroupByColumn, $scope.multipletableDesignerFields.EmptyCellTextBox, $scope.multipleDispalyRowHeaderCheckEditor.value1, $scope.multipleDisplayColumnHeaderCheckBox.value1, $scope.multipleDispalySummaryHeaderCheckEditor.value1, $scope.multipleVerticalTable_ChkBox.value1, $scope.multipletableDesignerFields.PivotHeaderTextBox, $rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.multipleStructureName, $localStorage.CategoryID, $scope.Flag, $scope.Group.GROUP_ID,0).success(function (response) {
                if (response != null) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Saved successfully.',
                        type: "info"
                    });
                }
            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + error + '.',
                    type: "error"
                });
            });
        }



    };

    $scope.multipleaddSuperColumn = function () {
        var flagLeft = false;
        var flagRight = false;
        var flagSummary = false;
        var flagColumn = false;

        var allAttributeListtreeview = $("#multipletreeviewallAttributeList").data("kendoTreeView");
        var selectedNode = allAttributeListtreeview.select();
        if (selectedNode.length == 0 || selectedNode.length == 1) {
            selectedNode = null;
        }
        var lefttreeView = $("#multipleleftTreeViewKendo").data("kendoTreeView");
        var lefttreelength = lefttreeView.dataSource._data.length;
        for (var i = 0; i < lefttreelength; i++) {
            if ($("#txtmultipleSuperColumn").val().toLowerCase() == lefttreeView.dataSource._data[i].ATTRIBUTE_NAME.toLowerCase()) {
                flagLeft = true;
            }
        }

        var righttreeView = $("#multiplerightNavTreeViewKendo").data("kendoTreeView");
        var righttreelength = righttreeView.dataSource._data.length;
        for (var i = 0; i < righttreelength; i++) {
            if ($("#txtmultipleSuperColumn").val().toLowerCase() == righttreeView.dataSource._data[i].ATTRIBUTE_NAME.toLowerCase()) {
                flagRight = true;
            }
        }

        var summarytreeView = $("#multiplesummaryNavTreeViewKendo").data("kendoTreeView");
        var summarytreelength = summarytreeView.dataSource._data.length;
        for (var i = 0; i < summarytreelength; i++) {
            if ($("#txtmultipleSuperColumn").val().toLowerCase() == summarytreeView.dataSource._data[i].ATTRIBUTE_NAME.toLowerCase()) {
                flagSummary = true;
            }
        }
        var columntreeView = $("#multiplecolumnNavTreeViewKendo").data("kendoTreeView");
        var columntreelength = columntreeView.dataSource._data.length;
        for (var i = 0; i < columntreelength; i++) {
            if ($("#txtmultipleSuperColumn").val().toLowerCase() == columntreeView.dataSource._data[i].ATTRIBUTE_NAME.toLowerCase()) {
                flagColumn = true;
            }
        }
        if ($("#txtmultipleSuperColumn").val().length > 0) {
            var checkExistingAttrInAll = allAttributeListtreeview.findByText($("#txtmultipleSuperColumn").val());
            if (checkExistingAttrInAll.length == 0 && flagLeft == false && flagRight == false && flagSummary == false && flagColumn == false) {
                var datasourcedata = $rootScope.multipleallAttributeListDatasource.data();
                var exists = false;
                for (var i1 = 0; i1 < datasourcedata.length; i1++) {
                    var dataitem = datasourcedata[i1].ATTRIBUTE_NAME;
                    if (dataitem.toLowerCase() === $("#txtmultipleSuperColumn").val().toLowerCase()) {
                        exists = true;
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Attribute already exists.',
                            // type: "info"
                        });
                        $("#txtmultipleSuperColumn").val('');
                    }
                }
                if (!exists) {
                    allAttributeListtreeview.append(new kendo.data.Node({ 'ATTRIBUTE_NAME': $("#txtmultipleSuperColumn").val(), 'ATTRIBUTE_ID': '-1', 'COLOR': 'BLACK', 'id': 'Super', hasChildren: false }), selectedNode);
                    $("#txtmultipleSuperColumn").val('');
                }
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Attribute already exists.',
                    // type: "info"
                });
                $("#txtmultipleSuperColumn").val('');
            }
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Enter required Value.',
                // type: "info"
            });
        }






    };

    $scope.tableGroupHeaderChange = function (e) {
        $scope.multipletableDesignerFields.ComboGroupByColumn = e.sender._old;
    };

    $scope.tableGroupSummaryChange = function (e) {
        $scope.multipletableDesignerFields.ComboSummaryGroup = e.sender._old;
    };

    $scope.$on("MULTIPLETABLEDESIGNER", function (event) {
        $("#multipletabledesigner").show();
        $("#multipleattributegroup").hide();
        $scope.multipleStructureNameDataSource.read();

    });

    $scope.Backtomultipletable = function () {
        $("#multipletabledesigner").hide();
        $("#multipleattributegroup").show();
    };
    //$scope.StructureNameDataSource.read();


}]);

