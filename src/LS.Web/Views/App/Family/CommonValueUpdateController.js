﻿LSApp.controller('CommonValueUpdateController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', 'ngDialog', '$rootScope', 'productService', '$localStorage', 'blockUI',
function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, ngDialog, $rootScope, productService, $localStorage, blockUI) {

    $scope.familyids = 0;
    $scope.CommonValuePicklistDatasource = [];
    $scope.attribute = {
        ATTRIBUTE_NAME: '',
        ATTRIBUTE_TYPE: 0,
        ATTRIBUTE_ID: 0,
        CREATE_BY_DEFAULT: false,
        VALUE_REQUIRED: '',
        STYLE_NAME: '',
        STYLE_FORMAT: '',
        DEFAULT_VALUE: '',
        PUBLISH2PRINT: false,
        PUBLISH2WEB: false,
        PUBLISH2CDROM: false,
        PUBLISH2ODP: false,
        USE_PICKLIST: false,
        ATTRIBUTE_DATATYPE: '',
        ATTRIBUTE_DATAFORMAT: '',
        ATTRIBUTE_DATARULE: '',
        UOM: '',
        IS_CALCULATED: false,
        ATTRIBUTE_CALC_FORMULA: '',
        PICKLIST_NAME: '',
        NUMERIC: 13,
        DECIMAL: 2,
        attributePattern: '',
        uimask: 524288

    };

    $rootScope.commonvalueupdatetreeData = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false, autoBind: false,
        transport: {
            read: function (options) {
                if ($scope.Family.FAMILY_ID == "") {
                    $scope.familyids = 0;
                } else {
                    $scope.familyids = $scope.Family.FAMILY_ID;
                }
                dataFactory.GetAllcommonvalueproducts($localStorage.getCatalogID, $scope.familyids, $localStorage.CategoryID, options.data.id).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });

            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        }
    });


    $scope.commonvalueupdatetreeOptions = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            if (!e.node) {
                $scope.commonvalueChangeEvent();
            }
        }
    };

    $scope.CloseCommonValuTab = function () {
        $("#cmnvalupdate").hide();
        $rootScope.commonitemupdater = false;
        $rootScope.usercommon = false;
        $("#myDIV").css('display','block');
        $(".productgridtabstripclass").css('display', 'block'); 
        
    };


    $scope.commonvalueChangeEvent = function () {

        var dataSource = $rootScope.commonvalueupdatetreeData;
        $scope.checkeditems = '';
        // $scope.uncheckeditems = '';
        dataSource.bind("change", function (e) {
            var selectedNodes = 0;
            var checkedNodes = [];
            $scope.checkedNodeIds(dataSource.view(), checkedNodes);;
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {

                    selectedNodes++;
                }
            }
            $scope.checkeditems = checkedNodes.join(",");
        });
    };
    $scope.checkedNodeIds = function (nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked && !nodes[i].hasChildren) {
                checkedNodes.push(nodes[i].FAMILY_ID);
            }
            if (nodes[i].hasChildren) {
                $scope.checkedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    };

    $scope.UncheckedNodeIds = function (nodes, UncheckedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (!nodes[i].checked) {
                UncheckedNodes.push(nodes[i].FAMILY_ID);
            }
            if (nodes[i].hasChildren) {
                $scope.UncheckedNodeIds(nodes[i].children.view(), UncheckedNodes);
            }
        }
    };

    $rootScope.productAttrDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                if ($scope.Family.FAMILY_ID == "") {
                    $scope.familyids = 0;
                } else {
                    $scope.familyids = $scope.Family.FAMILY_ID;
                }
                $scope.attribute = [];
                $scope.commonvaluedata.STRING_VALUE = "";
                $scope.commonvaluedata.IMAGE_CAPTION = "";
                $scope.commonvaluedata.IMAGE_CAPTION_BOX = "";
                $scope.commonvaluedata.ATTRIBUTE_ID = 0;
                dataFactory.Getprodattributes($localStorage.getCatalogID, $scope.familyids, $localStorage.CategoryID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.commonvaluedata = {
        PRODUCT_ID: '',
        ATTRIBUTE_NAME: '',
        STRING_VALUE: '',
        ATTRIBUTE_ID: '',
        ATTRIBUTE_TYPE: '',
        IMAGE_CAPTION: '',
        IMAGE_CAPTION_BOX: ''
    };

    $scope.commonvalueattrChange = function (e) {
        if (e.sender._oldIndex !== 0) {
            $scope.commonvaluedata.ATTRIBUTE_ID = e.sender._old;
            $scope.attribute = e.sender.dataSource.view()[e.sender._oldIndex - 1];
            $scope.commonvaluedata.STRING_VALUE = "";
            $scope.commonvaluedata.ATTRIBUTE_TYPE = 1;
            var uimask = "";
            if ($scope.attribute.ATTRIBUTE_DATATYPE.contains("Num")) {
                if ($scope.attribute.ATTRIBUTE_DATATYPE.length == 12) {
                    $scope.attribute.NUMERIC = $scope.attribute.ATTRIBUTE_DATATYPE.substring(9, 7);
                } else {
                    $scope.attribute.NUMERIC = $scope.attribute.ATTRIBUTE_DATATYPE.substring(8, 7);
                }

                if ($scope.attribute.ATTRIBUTE_DATATYPE.length == 12) {
                    $scope.attribute.Decimal = $scope.attribute.ATTRIBUTE_DATATYPE.substring(10).split(')')[0];
                } else {
                    $scope.attribute.Decimal = $scope.attribute.ATTRIBUTE_DATATYPE.substring(9).split(')')[0];
                }
                var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                uimask = $scope.UIMask($scope.attribute.NUMERIC, $scope.attribute.Decimal);
                pattern = pattern.replace("numeric", $scope.attribute.NUMERIC);
                pattern = pattern.replace("decimal", $scope.attribute.Decimal);
                $scope.attribute.uimask = uimask;
                $scope.attribute.attributePattern = new RegExp(pattern);
            } if ($scope.attribute.USE_PICKLIST) {
                $scope.CommonGetPickListData($scope.attribute.ATTRIBUTE_ID, $scope.attribute.PICKLIST_NAME);
            }
        }
    };

    $scope.CommonGetPickListData = function (attrName, picklistdata) {
        dataFactory.getPickListData(picklistdata).success(function (response) {
            $scope.CommonValuePicklistDatasource = response;
            return response;
        }).error(function (error) {
            options.error(error);
        });
    };
    $scope.SaveCommonValue = function () {

        blockUI.start();
        $q.all([$scope.SaveCommonValue1()]).then(function (response) {

            return;
        });

    }
    $scope.SaveCommonValue1 = function () {
        if ($scope.checkeditems !== '') {
            if ($scope.attribute.ATTRIBUTE_TYPE == 4)
            {
                var textval = $("#textBoxValue")[0].value;
                if (textval != undefined && textval != null)
                {
                    $scope.commonvaluedata.STRING_VALUE = textval;
                }
            }
            else
            {
            var textval = $("#textBoxValue")[0];
            var getval = "";

            if (textval != undefined && textval != null)
            {
                getval = textval.nextElementSibling.innerText;
                $scope.commonvaluedata.STRING_VALUE = getval;
            }

            }
            if ($scope.attribute.ATTRIBUTE_TYPE == 3) {
                var imageval =  $scope.commonvaluedata.IMAGE_CAPTION_BOX;
               
            }
               
            if ($scope.attribute.ATTRIBUTE_ID !== 0) {
                if ($scope.commonvaluedata.STRING_VALUE !== '' || $scope.commonvaluedata.IMAGE_CAPTION_BOX !== '') {
                    //blockUI.start();
                    $q.all([$scope.SaveCommonValueLoad(), $rootScope.LoadProdData($localStorage.getCatalogID, $scope.Family.FAMILY_ID, $localStorage.CategoryID)]).then(function (response) {

                        return;
                    });

                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Specification cannot be empty, please enter Value and try again.',
                        //type: "info"
                    });

                }
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Attribute to update Product Specifications.',
                    //type: "info"
                });
            }
        } else {
            {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Product to update Specifications.',
                    //type: "info"
                });
            }
        }
        //$timeout(function () {
        //    blockUI.stop();
        //}, 200);
        blockUI.stop();
    };

    $scope.SaveCommonValueLoad = function () {
        blockUI.stop();
        dataFactory.SaveCommonProductSpecs($localStorage.getCatalogID, $scope.Family.FAMILY_ID, $localStorage.CategoryID, $scope.checkeditems, $scope.commonvaluedata.STRING_VALUE, $scope.attribute.ATTRIBUTE_ID, $scope.commonvaluedata.IMAGE_CAPTION_BOX, $scope.commonvaluedata).success(function (response) {
            if (response != null) {

                blockUI.stop();
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Product Attribute Values updated successfully.',
                    type: "info"
                });

            }
        }).error(function (error) {
            options.error(error);
        });
    }
    $scope.onCommonValueSelect = function (e) {
        var message = $.map(e.files, function (file) { return file.name; }).join(", ");
        $scope.commonvaluedata.STRING_VALUE = '\\Images\\' + e.files[0].name;
        $scope.$apply();
    };
    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        }
        else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
        }
        $scope.CommonGetPickListData("1", "");
    };

    //Asset Management

    $scope.OpenPopupWindow = function (e) {
        $rootScope.paramValue = e.value;

        $scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
        $scope.winManageDrive.title("Asset Management");
        $scope.winManageDrive.center().open();
        $scope.driveMahementIsVisible = true;
    }
    $rootScope.saveUpdation = function (fileName, path) {
        var extn = fileName.split(".");
        // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
        var FileFormat = ["rar", "zip"];
        if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
            $rootScope.AttachPath = path;
            if ($rootScope.paramValue == "CommonMain") {
                $scope.commonvaluedata.IMAGE_CAPTION_BOX = path;
                if (path.toLowerCase().endsWith('.gif') || path.toLowerCase().endsWith('.jpg') || path.toLowerCase().endsWith('.jpeg') || path.toLowerCase().endsWith('.png') || path.toLowerCase().endsWith('.bmp')) {
                    //
                    $scope.commonvaluedata.IMAGE_CAPTION = path.replace(/\\/g, "/");
                }
                else if (path.toLowerCase().endsWith(".eps") || path.toLowerCase().endsWith(".tif") || path.toLowerCase().endsWith(".tiff") || path.toLowerCase().endsWith(".psd") ||
                      path.toLowerCase().endsWith(".tga") || path.toLowerCase().endsWith(".pcx")) {
                    dataFactory.Getimagevaluevalues(path).success(function (response) {
                        //
                        var companyName = '/' + $scope.getCustomerCompanyName;
                        var convertedPath = response.replace(companyName, '');
                        $scope.commonvaluedata.IMAGE_CAPTION = convertedPath.replace(/\\/g, "/");
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                //$scope.commonvaluedata.IMAGE_CAPTION = path.replace(/\\/g, "/");
            }
            
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'This file type is not supported',
                type: "info"
            });
        }
        $scope.winManageDrive.center().close();
        $scope.driveMahementIsVisible = false;


    };
        
    $scope.RemoveImage = function () {

        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Do you want to remove the attachment?", 
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result1) {
                if (result1 === "Yes") {
                    $scope.commonvaluedata.IMAGE_CAPTION = '';
                    $scope.commonvaluedata.IMAGE_CAPTION_BOX = '';
                    $scope.commonvalueupdatetreeData.read();
                }
            }
        });

    };
    $scope.init();
}]);

