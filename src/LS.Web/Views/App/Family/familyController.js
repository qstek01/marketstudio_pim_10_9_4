﻿LSApp.controller('familyController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', '$rootScope', '$localStorage', '$sce', 'blockUI',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, $rootScope, $localStorage, $sce, blockUI) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
        $('#hideFamilyAttributeGroup').show;
        $rootScope.familyCreated = false;
        $scope.SelectedFileForFamily = '';
        $scope.ProdCheckAssociate = '';
        $scope.SelectedFileForFamilyTooltip = 'No file chosen';
        $("#tableDesignerImport").hide();
        $('#tableDesignerImportss').hide();
        $scope.getCurrentUrl = $location.$$absUrl.split("App")[1];
        $scope.attributes = false;
        $scope.attributes1 = false;
       // $scope.renderrelatedfamily = true;

        if ($scope.getCurrentUrl == '/AdminDashboard') {
            $rootScope.familyMainEditor = true;
            $rootScope.invertedproductsshow = false;
            $rootScope.invertedproductsbutton1 = false;
        } else if ($scope.getCurrentUrl == '/Inverted') {
            $("#wrapper").toggleClass("toggled-2");
            $('#menu').css("display", "none");
            $('#sidebar-wrapper').css("border", "#fff solid 1px");
            $('#showName').css("color", "#091f3f");
            $('#sidebar-wrapper2').css("display", "none");
            $('.gutter').css("display", "none");
            $('#page-content-wrapper1');
            $('#page-content-wrapper1').css("min-width", "100%");
            $rootScope.familyMainEditor = false;

            $rootScope.invertedproductsshow = true;
            $rootScope.invertedproductsbutton = false;
            $rootScope.invertedproductsbutton1 = false;

        }
        $scope.ProdCurrentPageTest = "1";
        $scope.ProdCountPerPageTest = "10";
        $scope.gridCurrentPageTest = "10";
        $scope.back = function () {
            $rootScope.familyMainEditor = false;
            $rootScope.invertedproductsshow = true;
            $rootScope.LoadInvertedData();
            $rootScope.LoadSubProductGrid = false;
            //$rootScope.productassociatedfamilyGridDatasource.read();
            //   $rootScope.tblInvertedGrid.reload();
            $rootScope.invertedproductsbutton = false;
            $rootScope.invertedproductsbutton1 = false;
            $rootScope.btnSubProdInvert = false;
            $rootScope.SubProdInvert = false;
            $rootScope.invertedBackToProduct = true;
        };
        $scope.SubProductTabSelection = function () {

            $rootScope.GetInvertedSearchSubProductDetails();
        }
        $scope.backSub = function () {
            $rootScope.familyMainEditor = false;
            $rootScope.invertedproductsshow = true;
            $rootScope.btnSubProdInvert = true;
            $rootScope.SubProdInvert = false;
            $rootScope.invertedBackTosubProduct = true;
            $('#dynamictablesubproducts').show();
            $('#invertedSubProductsMain').show();
            $("#familyEditor").hide();
            $('#invertedProductsMain').hide();
            $('#subassociatedgrid').hide();
        };

        $scope.renderfamilyspecsgrid = false;
        $scope.renderfamilydescriptions = false;
        $scope.renderfamilyimages = false;
        $scope.renderrelatedfamily = false;
        $scope.renderclonedcategory = false;
        $scope.rendermultipletable = false;
        $scope.rendertabledesigner = false;
        $scope.actiondisable = true;
        $("#Familyimport").hide();
        $rootScope.selectedFamilyDetails = [];
        //$("#undoClickId").addClass("undoclick");
        //$("#Undooption").addClass("undolist");
        //importTableSheetSlectectionWindow
        $scope.renderAttributeGroup = false;
        $scope.renderTableDesignermultiple = false;
        $scope.renderRemoveGroup = false;
        $scope.renderEdit = false;
        $rootScope.clonedDisplay = [];
        $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
        $rootScope.familySpecCopy = [];
        $scope.isnotNumber = function (string) {
            $scope.result = isNaN(string);
            return $scope.result;
        };
        $scope.trustAsHtml = function (string) {

            return $sce.trustAsHtml(string);
        };
        $scope.Family = {
            FAMILY_ID: '',
            FAMILY_NAME: '',
            CATEGORY_ID: '',
            STATUS: 'CREATED',
            PUBLISH: true,
            PUBLISH2PRINT: true,
            PUBLISH2WEB: true,
            PUBLISH2PDF: true,
            PUBLISH2EXPORT: true,
            PUBLISH2PORTAL: true,
            PARENT_FAMILY_ID: 0,
            FAMILYIMG_NAME: '',
            FAMILYIMG_ATTRIBUTE: '',
            ATTRIBUTE_ID: '', OBJECT_NAME: '', CATEGORY_NAME: ''
        };

        // $("#attributeNameValue").hide();
        $scope.fmlyDescChangeDisable = false;
        $scope.fmlyImgAttrChangeDisable = false;

        $scope.SelectedDataItem = { VERSION: '1', DESCRIPTION: '', CATALOG_NAME: '', CATALOG_ID: 0, CATEGORY_ID: 0, ID: 0, SORT_ORDER: 0 };

        //$rootScope.selecetedCatalogId = $rootScope.selecetedCatalogId;

        $scope.StatusDataSource = [{ status: "CREATED", status_text: "Created" },
       { status: "IN PROGRESS", status_text: "In Progress" },
       { status: "TO REVIEW", status_text: "To Review" },
       { status: "APPROVED", status_text: "Approved" },
       { status: "VERIFIED", status_text: "Verified" }];

        $scope.StatusChange = function (e) {
            if (e.sender.value() !== "") {
                $scope.Family.STATUS = e.sender.value();
            }
        };

        $scope.fmlyDescChangeData = {

            FAMILY_ID: '',
            ATTRIBUTE_NAME: '',
            STRING_VALUE: '',
            ATTRIBUTE_ID: ''
        };

        $scope.fmlyImgAttrChangeData = {
            FAMILY_ID: '',
            ATTRIBUTE_NAME: '',
            ATTR_VALUE: '',
            ATTR_ID: '',
            IMG_PATH: ''
        };
        $scope.fmlyImgAttrChangeData.ATTR_ID = '';
        $scope.leftRowAttributeListDatasource = {
            ATTRIBUTE_ID: 999,
            ATTRIBUTE_NAME: 'Left'
        };
        $scope.rightRowAttributeListDatasource = {
            ATTRIBUTE_ID: '987',
            ATTRIBUTE_NAME: 'right'
        };
        $scope.columnAttributeListDatasource = {
            ATTRIBUTE_ID: '977',
            ATTRIBUTE_NAME: 'column'
        };
        $scope.summaryAttributeListDatasource = {
            ATTRIBUTE_ID: '967',
            ATTRIBUTE_NAME: 'summary'
        };
        $scope.ActiveCategoryId = '';
        $scope.tableGroupHeaderValue = false;
        $scope.IsNewFamily = '1';

        $scope.$on("GetFamily", function (event, selectedCategoryId, categoryId) {
            //  $scope.IsNewFamily = '0';
            $http.get("../Family/GetFamily?FamilyId=" + selectedCategoryId + "&categoryId=" + categoryId + "&catalogId=" + $rootScope.selecetedCatalogId).
                then(function (familyDetails) {
                    $("#accordions > div").accordion({ active: 1 });

                    //  $scope.Family = familyDetails.data;
                    $scope.LoadClonedCategory();
                    $scope.familyValues = familyDetails.data;
                    $scope.Family = {
                        FAMILY_ID: familyDetails.data.FAMILY_ID,
                        FAMILY_NAME: familyDetails.data.FAMILY_NAME,
                        CATEGORY_ID: familyDetails.data.CATEGORY_ID,
                        STATUS: familyDetails.data.STATUS,
                        PUBLISH: familyDetails.data.PUBLISH,
                        PUBLISH2PRINT: familyDetails.data.PUBLISH2PRINT,
                        PUBLISH2WEB: familyDetails.data.PUBLISH2WEB,
                        PUBLISH2PDF: familyDetails.data.PUBLISH2PDF,
                        PUBLISH2EXPORT: familyDetails.data.PUBLISH2EXPORT,
                        PUBLISH2PORTAL: familyDetails.data.PUBLISH2PORTAL,
                        PARENT_FAMILY_ID: familyDetails.data.PARENT_FAMILY_ID,
                        FAMILYIMG_NAME: familyDetails.data.FAMILYIMG_NAME,
                        FAMILYIMG_ATTRIBUTE: familyDetails.data.FAMILYIMG_ATTRIBUTE,
                        ATTRIBUTE_ID: familyDetails.data.ATTRIBUTE_ID, OBJECT_NAME: familyDetails.data.OBJECT_NAME, CATEGORY_NAME: familyDetails.data.CATEGORY_NAME
                    };
                });
        });


        $rootScope.valueofcheckbox = false;
        $rootScope.valueofcheckall = false;
        $scope.$on("Active_Family_Id", function (event, selectedCategoryId, selectedCategoryDetails) {
            $rootScope.valueofcheckbox = false;
            $rootScope.valueofcheckall = false;
            $("#Familyimport").hide();
            $rootScope.selecetedParentCategoryId = selectedCategoryId;
            $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
            $scope.fmlyImgAttrChangeData.IMG_PATH = '';
            $scope.IsNewFamily = '0';
            $scope.SelectedFileForFamily = '';
            $scope.SelectedFileForProduct = '';
            $scope.ActiveCategoryId = selectedCategoryId;
            $("#divNewFmlyAttriEntry").hide();
            $("#columnsetupfamily").hide();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#divFmlyTab").show();
            $("#gridfamily").show();
            $("#EditMultipletable").hide();
            $("#attributePack").hide();
            //  $scope.fmlyImgAttrChangeempty();
            $scope.$broadcast("Active_FamilyId", selectedCategoryId);
            $scope.$broadcast("TABLEDESIGNER");
            //  $scope.familySpecsImgAttrDatasource = [];
            //   $scope.familySpecsImgAttrDatasource.refresh();
            
            var parentnode = selectedCategoryDetails;
            if (selectedCategoryDetails.id.contains("~")) {
                parentnode = parentnode.parentNode();
                if (parentnode.id.contains("~")) {
                    parentnode = parentnode.parentNode().parentNode();
                }
            }

            $timeout(function () {
                $scope.familyAttributePackdataSource.read();
                $scope.productAttributePackdataSource.read();
                $rootScope.ProductGroupNameForFamily.read();
                $rootScope.FamilyGroupNameforFamilyAssociate.read();
                $rootScope.LoadFamilyData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                dataFactory.GetPdfXpressdefaultType_Family("Family", $scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                    if (response != null && response != '0') {
                        $scope.SelectedFileForUpload = response;
                        $scope.UploadFile($scope.SelectedFileForUpload, false);
                        debugger;
                        $rootScope.FamilyMrtFile = response;
                        $scope.SelectedFileForFamily = $rootScope.FamilyMrtFile;
                    }
                });
                dataFactory.getPdfXpressdefaultTemplate_Product('Product', $scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                    if (response != null && response != '0') {
                        $scope.SelectedFileForProduct = response;
                        debugger;
                        $rootScope.ProductMrtFile = $scope.SelectedFileForProduct;
                        $scope.SelectedFileForProduct = $rootScope.ProductMrtFile;
                    }
                });
            }, 300);

            $scope.$broadcast("GetFamily", selectedCategoryId, parentnode.CATEGORY_ID);
            $rootScope.FamilyCATEGORY_ID = parentnode.id;

            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();

            $scope.OpenProductDefault();


            var tabstrip = $("#tabstripfamily").data("kendoTabStrip");
            $scope.LoadFamilySpecsDesc();
            var myTab = tabstrip.tabGroup.children("li").eq(0);
            $scope.renderfamilydescriptions = true;
            $rootScope.familySpecsDatasource.read();
            tabstrip.select(myTab);
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();
            $("#divProductGrid").show();
            if ($rootScope.Mainprodcount > 0) {
                $("#productpaging1").show();
            }
            $("#gridproduct").show();

            $("#divProductGrid").removeClass("col-sm-2");
            $("#divProductGrid").addClass("col-sm-12");
            $('#selectedattribute').hide();
            $('#myDIV').show();
            $('#sampleProdgrid').show();
            $('#productpaging1').hide();
            $('#productpaging').show();
            $('divProductGrid').show();
            $('divProductGridItemOnly').hide();
            $('subproductsmaingrid').hide();
            $('productpaging1').hide();
            $('sampleProdgrid3').hide();
            $('subproductsgrid').hide();



            $("#productpreview").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();

            // mm22
            var classVal = $("#tagBtn").attr("class");
            $("#tagBtn").addClass("active");

            var classVal = $("#moneyBtn").attr("class");
            $("#moneyBtn").addClass("active");

            var classVal = $("#keyBtn").attr("class");
            $("#keyBtn").addClass("active");

            var classVal = $("#imageBtn").attr("class");
            $("#imageBtn").addClass("active");

            //mm22

            var w = $scope.ViewByHideProducts();
            //var subproductsgrid = $("#subproductsmaingrid").data("kendoPivotGrid");
            //    var subproductsgrid = $("#subproductsmaingrid").data("kendoGrid");
            //    subproductsgrid.dataSource.page(1);


        });

        $scope.$on("Active_Family_Ids", function (event, selectedCategoryId, selectedCategoryDetails) {
            $scope.valueofcheckbox = false;
            $scope.valueofcheckall = false;
            $rootScope.selecetedParentCategoryId = selectedCategoryId;
            //familySpecsImgAttrDatasource = '';
            $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
            $scope.fmlyImgAttrChangeData.IMG_PATH = '';
            $scope.IsNewFamily = '0';
            $scope.ActiveCategoryId = selectedCategoryId;
            $("#divNewFmlyAttriEntry").hide();
            $("#divFmlyTab").show();
            $scope.$broadcast("Active_FamilyId", selectedCategoryId);
            $rootScope.familySpecsDatasource.read();
            $scope.familySpecsImgAttrDatasource.read();

            $scope.$broadcast("GetFamily", selectedCategoryId, selectedCategoryDetails.CATEGORY_ID);

            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();

            $scope.OpenProductDefault();

            var tabstrip = $("#tabstripfamily").data("kendoTabStrip");
            $scope.LoadFamilySpecsDesc();
            var myTab = tabstrip.tabGroup.children("li").eq(0);
            $scope.renderfamilydescriptions = true;
            $rootScope.familySpecsDatasource.read();
            tabstrip.select(myTab);
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();
            $("#divProductGrid").show();
            if ($rootScope.Mainprodcount > 0) {
                $("#productpaging1").show();
            }
            $("#gridproduct").show();
            $("#productpreview").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();
            //var subproductsgrid = $("#subproductsmaingrid").data("kendoPivotGrid");
            var subproductsgrid = $("#subproductsmaingrid").data("kendoGrid");
            subproductsgrid.dataSource.page(1);
        });

        $rootScope.familySpecsDatasource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            serverFiltering: true,
            transport: {
                read: function (options) {

                    dataFactory.getfamilyspecs($scope.ActiveCategoryId, $rootScope.selecetedCatalogId).success(function (response) {
                        options.success(response);
                        if (response.length !== 0) {

                            $scope.renderfamilydescriptions = true;
                            $scope.fmlyDescChangeData.STRING_VALUE = response[0].ATTR_VALUE;
                            $scope.fmlyDescChangeData.ATTRIBUTE_ID = response[0].ATTR_ID;
                            $scope.fmlyDescChangeData.FAMILY_ID = response[0].FAMILY_ID;


                        } else {

                            $scope.fmlyDescChangeData.STRING_VALUE = "";
                            $scope.renderfamilydescriptions = false;
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        //$scope.getworkflowDataSource = new kendo.data.DataSource({
        //    type: "json", autoBind: false,
        //    serverFiltering: true,
        //    transport: {
        //        read: function (options) {
        //            //dataFactory.getWorkflow().success(function (response) {

        //            dataFactory.bindworkflowforfamily($scope.Family.FAMILY_ID).success(function (response) {
        //                options.success(response);

        //                //$scope.Family.STATUS_NAME = response;

        //            }).error(function (error) {
        //                options.error(error);
        //            });


        //        }
        //    }
        //});


        //$scope.workflowitemChange = function (e) {
        //    $scope.Family.STATUS_NAME = e.sender.value();

        //};


        $scope.workflowitemChange = function (e) {
            $scope.Family.STATUS_NAME = e.sender.value();

        };

        $rootScope.workflowDataSource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            serverFiltering: true,
            transport: {
                read: function (options) {
                    //dataFactory.getWorkflow().success(function (response) {

                    dataFactory.getWorkflowforfamily($rootScope.currentFamilyId).success(function (response) {
                        options.success(response);
                        debugger;
                        $scope.SelectedFileForFamily = $rootScope.FamilyMrtFile;
                        $scope.SelectedFileForProduct = $rootScope.ProductMrtFile;
                        $scope.Family.STATUS_NAME = response[0];
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.familySpecsImgAttrDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true, autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.getfamilyspecsImgAttr($scope.ActiveCategoryId, $rootScope.selecetedCatalogId).success(function (response) {
                        options.success(response);
                        if (response.length > 0) {
                            $scope.Family.FAMILY_ID = response[0].FAMILY_ID;
                            $scope.Family.ATTRIBUTE_ID = response[0].ATTR_ID;
                            $scope.fmlyImgAttrChangeData.ATTR_ID = response[0].ATTR_ID;
                            $scope.fmlyImgAttrChangeempty();
                        } else {
                            $scope.Family.ATTRIBUTE_ID = "";
                            $scope.fmlyImgAttrChangeempty();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.ddlCategoryDataSource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.getRelatedfamily($scope.SelectedDataItem.ID, $scope.SelectedDataItem.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.changefamily = [];

        $scope.exchangeFamilyRows = function (dataItem, e) {
            var grid = e.sender;
            var selectedData = grid.dataItem(grid.select());
            $scope.selectedSort = selectedData;
            $scope.subfamilyid = $scope.selectedSort.FAMILY_ID;
            $scope.changefamily = dataItem;
        };

        $scope.categoryfamilyDropDownEditor = function (container, options) {
            $scope.ddlCategoryDataSource.read();
            var editor = $('<input kendo-drop-down-list required k-data-text-field="\'SORT_ORDER\'" k-data-value-field="\'SORT_ORDER\'" k-on-change=\"familysortChange(kendoEvent)\" k-data-source="ddlCategoryDataSource" data-bind="value:' + options.field + '"/>')
            .appendTo(container);
        };

        $scope.familysortChange = function (e) {
            //dataFactory.savefamilysort($scope.changefamily, $scope.SelectedDataItem.CATALOG_ID, $scope.changefamily.PARENT_FAMILY_ID).success(function (response) {
            dataFactory.savesubfamilysort($scope.changefamily, $scope.SelectedDataItem.CATALOG_ID, $scope.changefamily.PARENT_FAMILY_ID, $scope.subfamilyid, $scope.changefamily.SORT_ORDER, $scope.changefamily.FAMILY_NAME).success(function (response) {
                $rootScope.treeData.read();
                //$scope.treeData.read();
                $scope.familyRelatedFamilyDatasource.read();
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.familyRelatedFamilyDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true, sort: { field: "SORT_ORDER", dir: "asc" },
            pageable: true,
            pageSize: 5,
            serverPaging: true,
            serverSorting: true, autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.GetRelatedfamilyDetails($scope.SelectedDataItem.ID, $scope.SelectedDataItem.CATALOG_ID, $localStorage.CategoryID, options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "FAMILY_ID",
                    fields: {
                        FAMILY_ID: { type: "number", editable: false },
                        SUBFAMILY_ID: { type: "number", editable: false },
                        FAMILY_NAME: { editable: false },
                        SORT_ORDER: { type: "number", validation: { required: true, min: 1 } }
                    }
                }
            }
        });


        $scope.relatedFamilyGridOptions = {
            dataSource: $scope.familyRelatedFamilyDatasource,
            autoBind: false,
            sortable: true,
            filterable: true,
            pageable: { buttonCount: 5 }, selectable: true,
            columns: [
                         { field: "SORT_ORDER", title: "Sort Order", width: "180px", editor: $scope.categoryfamilyDropDownEditor, template: "#=SORT_ORDER#" },
                         { field: "FAMILY_NAME", title: "Product Name", width: "180px", template: "<a title='FamilyName'  class='k-link' href='javascript:void(0);'   ng-click='ngclkfamilyname(this,2)'>#=FAMILY_NAME#</a>" }
            ],
            editable: true
        };
        $scope.relatedmainFamilyGridOptions = {
            dataSource: $scope.familyRelatedFamilyDatasource,
            autoBind: false,
            sortable: true,
            filterable: true,
            pageable: { buttonCount: 5 }, selectable: true,
            columns: [{ field: "FAMILY_NAME", title: "Product Name", width: "180px", template: "<a title='FamilyName'  class='k-link' href='javascript:void(0);'   ng-click='ngclkfamilyname(this,2)'>#=FAMILY_NAME#</a>" }
            ],
            editable: true
        };

        //$scope.leftNavselectNode = function (name) {
        //    // var selectedfamily = $("#leftNavTreeViewKendo").data("kendoTreeView").findByText(name);
        //    // $("#leftNavTreeViewKendo").data("kendoTreeView").select(selectedfamily);
        //};

        $rootScope.ngclkfamilyname = function (e, options) {
            if ($scope.userRoleAddRelatedFamily) {
                if (options == 0 || options == 1) {
                    $rootScope.cloneOptions = options;
                    var familyid;
                    if (options == 0) {
                        if ((e == "" || e == undefined || e == null) && $rootScope.clonedDisplay != undefined && $rootScope.clonedDisplay != null && $rootScope.clonedDisplay != "")
                            familyid = $rootScope.clonedDisplay;
                        else if (e != undefined && e != null) {
                            familyid = e.dataItem.id.split(',');
                            $rootScope.clonedDisplay = familyid;
                            if ($scope.orginalNodeId == undefined) {
                                $rootScope.orginalNodeId = $rootScope.SelectedNodeID;
                                $scope.orginalNodeId = $rootScope.SelectedNodeID;
                            }
                        }
                    }
                    if (options == 1) {
                        if ((e == "" || e == undefined || e == null) && $rootScope.clonedDisplay != undefined && $rootScope.clonedDisplay != null && $rootScope.clonedDisplay != "")
                            familyid = $rootScope.clonedDisplay;
                        else if (e != undefined && e != null) {
                            familyid = (e.dataItem.id + ',').split(',');
                            $rootScope.clonedDisplay = familyid;
                            if ($scope.orginalNodeId == undefined) {
                                $rootScope.orginalNodeId = $rootScope.SelectedNodeID;
                                $scope.orginalNodeId = $rootScope.SelectedNodeID;
                            }
                        }
                    }

                    $rootScope.displayCloneFamilies = true;
                    var treeView = $('#leftNavTreeViewKendoNavigator').data('kendoTreeView');
                    var status = true;

                    angular.forEach($rootScope.treeData._data, function (nodes) {
                        for (var i = 0; i < familyid.length; i++) {
                            if (treeView != null && nodes != null && (nodes.id == familyid[i] || nodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(nodes.id))) {
                                var node = treeView.findByUid(nodes.uid);
                                if (nodes.id == familyid[i])
                                    treeView.expand(node);
                                angular.forEach(nodes.children._data, function (firstChildnodes) {
                                    for (var i = 0; i < familyid.length; i++) {
                                        if (treeView != null && firstChildnodes != null && (firstChildnodes.id == familyid[i] || firstChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(firstChildnodes.id))) {
                                            var node = treeView.findByUid(firstChildnodes.uid);
                                            if (firstChildnodes.id == familyid[i])
                                                treeView.expand(node);
                                        }
                                        else if (treeView != null && firstChildnodes != null && (firstChildnodes.CATEGORY_ID.replace('~', '') == familyid[i] || firstChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(firstChildnodes.id))) {
                                            var node = treeView.findByUid(firstChildnodes.uid);
                                            if (firstChildnodes.CATEGORY_ID.replace('~', '') == familyid[i])
                                                treeView.expand(node);
                                        }
                                    }
                                    if (treeView != null && firstChildnodes != null && firstChildnodes.id.includes($scope.Family.FAMILY_ID) && (!firstChildnodes.id.includes($rootScope.orginalNodeId) || !$rootScope.orginalNodeId.includes(firstChildnodes.id))) {
                                        var firstChildnode = treeView.findByUid(firstChildnodes.uid);
                                        firstChildnode.children().children()[firstChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                        $rootScope.SelectedNodeID = firstChildnodes.id.replace($scope.Family.FAMILY_ID, '');
                                        var j = 0;
                                        for (var i = 0; i < familyid.length; i++) {
                                            if ($rootScope.SelectedNodeID.includes(familyid[i]))
                                                j = j + 1;
                                        }
                                        if (j == i)
                                            $rootScope.displayCloneFamilies = false;

                                    }
                                    //if (treeView != null && firstChildnodes != null && (firstChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(firstChildnodes.id))) {
                                    //    var firstChildnode = treeView.findByUid(firstChildnodes.uid);
                                    //    firstChildnode.children().children()[firstChildnode.children().children().length - 1].className = "k-in";
                                    //}
                                    angular.forEach(firstChildnodes.children._data, function (secondChildnodes) {
                                        for (var i = 0; i < familyid.length; i++) {
                                            if (treeView != null && secondChildnodes != null && (secondChildnodes.id == familyid[i] || secondChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(secondChildnodes.id))) {
                                                var node = treeView.findByUid(secondChildnodes.uid);
                                                treeView.expand(node);
                                            }
                                            else if (treeView != null && secondChildnodes != null && (secondChildnodes.CATEGORY_ID.replace('~', '') == familyid[i] || secondChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(secondChildnodes.id))) {
                                                var node = treeView.findByUid(secondChildnodes.uid);
                                                treeView.expand(node);
                                            }
                                        }
                                        if (treeView != null && secondChildnodes != null && (secondChildnodes.id.includes($scope.Family.FAMILY_ID) && (!$rootScope.orginalNodeId.includes(secondChildnodes.id) || !secondChildnodes.id.includes($rootScope.orginalNodeId)))) {
                                            var secondChildnode = treeView.findByUid(secondChildnodes.uid);
                                            secondChildnode.children().children()[secondChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                            status = false;
                                            for (var i = 0; i < familyid.length; i++) {
                                                if ($rootScope.SelectedNodeID.includes(familyid[i]))
                                                    j = j + 1;
                                            }
                                            if (j == i)
                                                $rootScope.displayCloneFamilies = false;
                                        }

                                        angular.forEach(secondChildnodes.children._data, function (thirdChildnodes) {
                                            for (var i = 0; i < familyid.length; i++) {
                                                if (treeView != null && thirdChildnodes != null && (thirdChildnodes.id == familyid[i] || thirdChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(thirdChildnodes.id))) {
                                                    var node = treeView.findByUid(thirdChildnodes.uid);
                                                    treeView.expand(node);
                                                }
                                                else if (treeView != null && thirdChildnodes != null && (thirdChildnodes.CATEGORY_ID.replace('~', '') == familyid[i] || thirdChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(thirdChildnodes.id))) {
                                                    var node = treeView.findByUid(thirdChildnodes.uid);
                                                    treeView.expand(node);
                                                }
                                            }
                                            if (treeView != null && thirdChildnodes != null && (thirdChildnodes.id.includes($scope.Family.FAMILY_ID) && (!$rootScope.orginalNodeId.includes(thirdChildnodes.id) || !thirdChildnodes.id.includes($rootScope.orginalNodeId)))) {
                                                var thirdChildnode = treeView.findByUid(thirdChildnodes.uid);
                                                thirdChildnode.children().children()[thirdChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                                status = false;
                                                $rootScope.displayCloneFamilies = false;
                                            }

                                            angular.forEach(thirdChildnodes.children._data, function (forthChildnodes) {
                                                for (var i = 0; i < familyid.length; i++) {
                                                    if (treeView != null && forthChildnodes != null && (forthChildnodes.id == familyid[i] || forthChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(forthChildnodes.id))) {
                                                        var node = treeView.findByUid(forthChildnodes.uid);
                                                        treeView.expand(node);
                                                    }
                                                    else if (treeView != null && forthChildnodes != null && (forthChildnodes.CATEGORY_ID.replace('~', '') == familyid[i] || forthChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(forthChildnodes.id))) {
                                                        var node = treeView.findByUid(forthChildnodes.uid);
                                                        treeView.expand(node);
                                                    }
                                                }
                                                if (treeView != null && forthChildnodes != null && forthChildnodes.id.includes($scope.Family.FAMILY_ID) && (!$rootScope.orginalNodeId.includes(forthChildnodes.id) || !forthChildnodes.id.includes($rootScope.orginalNodeId))) {
                                                    var forthChildnode = treeView.findByUid(forthChildnodes.uid);
                                                    forthChildnode.children().children()[forthChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                                    status = false;
                                                    $rootScope.displayCloneFamilies = false;
                                                }

                                                angular.forEach(forthChildnodes.children._data, function (famChildnodes) {
                                                    for (var i = 0; i < familyid.length; i++) {
                                                        if (treeView != null && famChildnodes != null && (famChildnodes.id == familyid[i] || famChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(famChildnodes.id))) {
                                                            var node = treeView.findByUid(famChildnodes.uid);
                                                            treeView.expand(node);
                                                        }
                                                        else if (treeView != null && famChildnodes != null && (famChildnodes.CATEGORY_ID.replace('~', '') == familyid[i] || famChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(famChildnodes.id))) {
                                                            var node = treeView.findByUid(famChildnodes.uid);
                                                            treeView.expand(node);
                                                        }
                                                    }
                                                    if (treeView != null && famChildnodes != null && famChildnodes.id.includes($scope.Family.FAMILY_ID) && (!$rootScope.orginalNodeId.includes(famChildnodes.id) || !famChildnodes.id.includes($rootScope.orginalNodeId))) {
                                                        var famChildnode = treeView.findByUid(famChildnodes.uid);
                                                        famChildnode.children().children()[famChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                                        status = false;
                                                        $rootScope.displayCloneFamilies = false;
                                                    }

                                                    angular.forEach(famChildnodes.children._data, function (subfamChildnodes) {
                                                        for (var i = 0; i < familyid.length; i++) {
                                                            if (treeView != null && subfamChildnodes != null && (subfamChildnodes.id == familyid[i] || subfamChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(subfamChildnodes.id))) {
                                                                var node = treeView.findByUid(subfamChildnodes.uid);
                                                                treeView.expand(node);
                                                            }
                                                            else if (treeView != null && subfamChildnodes != null && (subfamChildnodes.CATEGORY_ID.replace('~', '') == familyid[i] || subfamChildnodes.id.includes($rootScope.orginalNodeId) || $rootScope.orginalNodeId.includes(subfamChildnodes.id))) {
                                                                var node = treeView.findByUid(subfamChildnodes.uid);
                                                                treeView.expand(node);
                                                            }
                                                        }
                                                        if (treeView != null && subfamChildnodes != null && subfamChildnodes.id.includes($scope.Family.FAMILY_ID) && (!$rootScope.orginalNodeId.includes(subfamChildnodes.id) || !subfamChildnodes.id.includes($rootScope.orginalNodeId))) {
                                                            var subfamChildnode = treeView.findByUid(subfamChildnodes.uid);
                                                            subfamChildnode.children().children()[subfamChildnode.children().children().length - 1].className = "k-in k-state-selected";
                                                            status = false;
                                                            $rootScope.displayCloneFamilies = false;
                                                        }

                                                    });
                                                });

                                            });
                                        });
                                    });
                                });
                            }
                        }
                    });
                } else {
                    var familyid = e.dataItem.FAMILY_ID;
                    $rootScope.LoadProdData($rootScope.selecetedCatalogId, familyid, $localStorage.CategoryID);
                    $scope.$broadcast("GetFamily", familyid, e.dataItem.CATEGORY_ID);
                    //   $http.get("../Family/GetFamily?FamilyId=" + familyid + "&categoryId=" + e.dataItem.CATEGORY_ID + "&catalogId=" + $rootScope.selecetedCatalogId).then(function (familyDetails) { $scope.Family = familyDetails.data; });
                    $scope.SelectedDataItem.ID = familyid;
                    $scope.ActiveCategoryId = e.dataItem.id;
                    $scope.familyRelatedFamilyDatasource.read();
                    $rootScope.familySpecsDatasource.read();
                    $scope.LoadFamilySpecsData();
                    $scope.familySpecsImgAttrDatasource.read();
                    $scope.LoadAttributeGroupType();
                    // Node Selection in Related Family
                    //var treeview = $("#leftNavTreeViewKendoNavigator").data("kendoTreeView");
                    //var getitem = treeview.dataSource.get("~" + familyid);
                    //var selectitem = treeview.findByUid(getitem.uid);
                    //treeview.select(selectitem);
                    //var node = treeview.findByUid(getitem.parentNode().uid);
                    //treeview.expand(node);
                    $scope.tabstrip.select(2);
                    //var tbs = $("#tabstrip").kendoTabStrip({


                    //}).data("kendoTabStrip");
                    //tbs.select(2);
                    // Node Selection in Related Family -----------------


                }
            }
            else {
                // alert("View rights disabled by administrator.");
            }
        };



        $scope.$on("Active_Category_Selection", function (event, selectedDataItem) {
            $scope.ActiveCategoryId = selectedDataItem.ID;
            $scope.SelectedDataItem = selectedDataItem;
            //if ($scope.SelectedDataItem.ID !== 0 && $scope.SelectedDataItem.CATALOG_ID !== 0) {
            //    $scope.familyRelatedFamilyDatasource.read();
            //}
        });

        $scope.fmlyDescChange = function (e) {

            $scope.fmlyDescChangeData.ATTRIBUTE_ID = e.sender._old;
            $http.get("../Family/GetFamilyAttr?familyId=" + $scope.Family.FAMILY_ID + "&attrId=" + $scope.fmlyDescChangeData.ATTRIBUTE_ID).
            then(function (familyattr) {

                $scope.fmlyDescChangeData.STRING_VALUE = familyattr.data.STRING_VALUE;
                $scope.fmlyDescChangeDisable = familyattr.data.ATTRIBUTE_DISABLE;
                $scope.fmlyDescChangeData.FAMILY_ID = $scope.Family.FAMILY_ID;
            });
            if (e.sender._old != "") {
                $("#attributeNameValue").show();
            }
        };
        $scope.fmlyImgAttrChange = function (e) {
            if (e.sender._old !== "") {
                $scope.fmlyImgAttrChangeData.ATTR_ID = e.sender._old;
                $http.get("../Family/GetFamilyImgAttr?familyId=" + $scope.Family.FAMILY_ID + "&&attrId=" + $scope.fmlyImgAttrChangeData.ATTR_ID).
                then(function (familyimgattr) {
                    if (familyimgattr.data.STRING_VALUE !== '' && familyimgattr.data.STRING_VALUE !== null) {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = "/content/Productimages" + familyimgattr.data.STRING_VALUE;

                        //dataFactory.missingPath(familyimgattr.data.STRING_VALUE).success(function (response) {
                        //    alert(response);
                        //    $scope.fmlyImgAttrChangeData.ATTR_VALUE = response;
                        //}).error(function (error) {
                        //    options.error(error);
                        //});
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        if (familyimgattr.data.PREVIEW_PATH.trim() !== "") {

                            $scope.fmlyImgAttrChangeData.IMG_PATH = familyimgattr.data.PREVIEW_PATH;
                        }
                        else
                            $scope.fmlyImgAttrChangeData.IMG_PATH = "";

                        $scope.fmlyImgAttrChangeDisable = familyimgattr.data.ATTRIBUTE_DISABLE;
                        $scope.Family.FAMILYIMG_ATTRIBUTE = familyimgattr.data.ATTRIBUTE_NAME;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = familyimgattr.data.STRING_VALUE + '?' + new Date().getTime();
                        $scope.FAMILYIMG_NAME_TEMP = '\\Content\\ProductImages\\' + $scope.getCustomerCompanyName + familyimgattr.data.STRING_VALUE;
                    }
                    else if (familyimgattr.data.STRING_VALUE === undefined) {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '/Empty/';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                        $scope.FAMILYIMG_NAME_TEMP = '';
                    }
                    else if (familyimgattr.data.STRING_VALUE == '') {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '/Empty/';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                        $scope.FAMILYIMG_NAME_TEMP = '';
                    }
                    else {

                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                        $scope.FAMILYIMG_NAME_TEMP = '';
                    }
                });
            }
            else {
                $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
                $scope.fmlyImgAttrChangeData.IMG_PATH = '';
            }
        };

        $scope.fmlyImgAttrChangeempty = function () {
            if ($scope.Family.ATTRIBUTE_ID === undefined || $scope.Family.ATTRIBUTE_ID === "") {
                $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
                $scope.fmlyImgAttrChangeData.IMG_PATH = '';
                $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                $scope.fmlyImgAttrChangeData.ATTR_ID = '';
                $scope.Family.ATTRIBUTE_ID = '';
                $scope.Family.OBJECT_NAME = '';
                $scope.Family.FAMILYIMG_NAME = '';
            }
            else {

                //   $scope.fmlyImgAttrChangeData.ATTR_ID = $scope.Family.ATTRIBUTE_ID;
                $http.get("../Family/GetFamilyImgAttr?familyId=" + $scope.Family.FAMILY_ID + "&&attrId=" + $scope.Family.ATTRIBUTE_ID).
                then(function (familyimgattr) {
                    if (familyimgattr.data.STRING_VALUE !== '' && familyimgattr.data.STRING_VALUE !== null) {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = familyimgattr.data.STRING_VALUE;
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.fmlyImgAttrChangeData.IMG_PATH = familyimgattr.data.PREVIEW_PATH;
                        $scope.Family.FAMILYIMG_ATTRIBUTE = familyimgattr.data.ATTRIBUTE_NAME;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = familyimgattr.data.STRING_VALUE + '?' + new Date().getTime();;
                    }
                    else if (familyimgattr.data.STRING_VALUE === undefined) {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '/Empty/';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '/Empty/';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                    }
                    else if (familyimgattr.data.STRING_VALUE == '') {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '/Empty/';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '/Empty/';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                    }
                    else {
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '';
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '';
                        $scope.Family.FAMILYIMG_ATTRIBUTE = '';
                        $scope.fmlyImgAttrChangeData.ATTR_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.ATTRIBUTE_ID = familyimgattr.data.ATTRIBUTE_ID;
                        $scope.Family.OBJECT_NAME = familyimgattr.data.OBJECT_NAME;
                        $scope.Family.FAMILYIMG_NAME = '';
                    }
                });
            }
        };

        $scope.uploadFile = function (files) {

            var fd = new FormData();
            fd.append("file", files[0]);
            $scope.fmlyImgAttrChangeData.ATTR_VALUE = "\\Content\\" + files[0].name;
        };

        $scope.uploadImgFile = function (files) {
            $scope.fmlyImgAttrChangeData.ATTR_VALUE = $scope.fmlyImgAttrChangeData.ATTR_VALUE;
        };
        $scope.catListGridOptions = {
            dataSource: $scope.catListDatasource,
            autoBind: false,
            sortable: false,
            pageable: { buttonCount: 5 }
        };
        $scope.catListprodDatasource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.Getprodspecs($rootScope.selecetedCatalogId).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            serverPaging: false,
            serverSorting: false,
            pageSize: 5
        });
        $scope.catProdListGridOptions = {
            dataSource: $scope.catListprodDatasource,
            autoBind: false,
            sortable: true,
            pageable: { buttonCount: 5 }
        };
        $rootScope.Productnew = false;
        $scope.SetFlag = function () {
            $('#hideFamilyAttributeGroup').hide();
            $rootScope.Productnew = true;
            $rootScope.familyCreated = true;
            $scope.IsNewFamily = "1";
            $('#user-toolbar-preview').attr('disabled', 'disabled');
            $('#user-toolbar-trash-family').attr('disabled', 'disabled');
            // $scope.$apply(function () {
            $scope.Family.FAMILY_ID = "";
            $scope.Family.FAMILY_NAME = "";
            $scope.Family.CATEGORY_ID = "";
            $scope.Family.STATUS = "CREATED";
            $scope.Family.STATUS_NAME = "NEW";
            $scope.$broadcast("ToClearProdFamData", "some data");

            $("#FamilySpecsTab").hide();
            var tabstrips = $("#tabstrip").data("kendoTabStrip");
            var myTabsproducts = tabstrips.tabGroup.children("li").eq(2);
            //tabstrips.disable(myTabsproducts);
            //  });
            $rootScope.LoadProdData($rootScope.selecetedCatalogId, 0, $localStorage.CategoryID);
            $scope.SelectedDataItem.ID = "";
            $scope.originalCategoryDatasource.read();
            $scope.clonedCategoryDatasource.read();
            $("#divFmlyTab").show();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
            //$("#ProductTab").addClass("in active");
            //$("#FamilySpecsTab").removeClass("in active");
            //$("#FamilyDescriTab").removeClass("in active");
            //$("#ImagesTab").removeClass("in active");
            //$("#RelatedFamilyTab").removeClass("in active");
            //$("#MultipleTablesTab").removeClass("in active");


            //$("#lnkProductTab").addClass("active");
            //$("#lnkFamilySpecsTab").removeClass("active");
            //$("#lnkFamilyDescriTab").removeClass("active");
            //$("#lnkImagesTab").removeClass("active");
            //$("#lnkRelatedFamilyTab").removeClass("active");
            //$("#lnkMultipleTablesTab").removeClass("active");
            var tabstrip = $("#tabstripfamily").data("kendoTabStrip");
            $scope.LoadFamilySpecsDesc();
          //  var myTab = tabstrip.tabGroup.children("li").eq(0);
            $scope.renderfamilydescriptions = true;
            $rootScope.familySpecsDatasource.read();
            //tabstrip.select(myTab);

            $rootScope.workflowDataSource.read();

        };

        $scope.showNewFmlyAttrAdd = function () {
            $("#divNewFmlyAttriEntry").show();
            $("#divFmlyTab").hide();
        };
        $scope.showNewFmlyAttrCancel = function () {
            $("#divNewFmlyAttriEntry").hide();
            $("#divFmlyTab").show();
        };
        $scope.catalogID = $rootScope.selecetedCatalogId;
        $scope.getAttributeList = "";

        $scope.getCatalogFilter = [];



        $scope.previewFamily = function () {
           
            var Cat_Id = $rootScope.selecetedCatalogId;
            var CatalogID = $rootScope.selecetedCatalogId;
            var id = $scope.Family.FAMILY_ID; 
            dataFactory.getPdfXpressdefaultTemplate_Family('Family', id, CatalogID).success(function (response) {

                if (response == null) {
                    
                    $scope.FamilyDefaultPreview();
                }
                else {
                    $scope.FamilyPdfPreview();
                }

            });



        }



        $scope.FamilyDefaultPreview = function () {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Product Preview may take some time to process.",
                type: "confirm",
                buttons: [{ value: "Ok" },
                    { value: "Cancel" }
                ],
                success: function (result) {
                    if (result === "Ok") {

                        $scope.PreviewText = '';
                        if (!$rootScope.FamilyCATEGORY_ID.length) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Please select a Product.',
                                type: "error"
                            });
                        } else {


                            $("#tabledesigner").hide();
                            $("#EditMultipletable").hide();
                            //debugger;
                            $scope.previewFamilySpecs();
                        }
                    }
                    else {
                        return false;
                    }
                }
            });

        };

        $scope.selectfamily = function () {
            $("#familyselect").removeClass('display-none');
            $("#familyselect").addClass('display-block');
            $("#sampleProdgridxyz").addClass('display-none');
            $("#sampleProdgridxyz").removeClass('display-block');
            $(".main-menu-copy").removeClass('display-none');
            $(".main-menu-copy").addClass('display-block');
            $("#family-top-sec").removeClass('display-none');
            $("#family-top-sec").addClass('display-block');

        }

        $scope.showfamilygrid = function () {

            //$("#ui-id-7").css("display", "block");
            //$('#ui-id-7').attr("style", "display: block !important");
           // $('#ui-id-6').attr("style", "display: block !important");

            //To Create New Item And To Hide the Product
            //$("#divFmlyTab").removeClass('display-none');
            //$("#divFmlyTab").addClass('display-block');
            //$("#divFmlyTab").show();
            //$("#divFmlyTab").css("display", "block");
            //$("#ui-id-6 > span").addClass("ui-icon-triangle-1-s");
            //$("#ui-id-6 > span").removeClass("ui-icon-triangle-1-e");
            //$("#ui-id-6").removeClass("ui-state-default");
            //$("#ui-id-6").addClass("ui-state-active");
            //$("#divFmlyTab").css("display", "block");
            $("#sampleProdgridxyz").addClass('display-block');
            $("#sampleProdgridxyz").removeClass('display-none');
            $("#familyselect").addClass('display-block');
            $("#familyselect").removeClass('display-none');
            $(".main-menu-copy").removeClass('display-none');
            $(".main-menu-copy").addClass('display-block');
            $("#family-top-sec").removeClass('display-none');
            $("#family-top-sec").addClass('display-block');
//$(".testfield").css("display", "block");
            debugger;
            let FamilyID = $rootScope.currentFamilyId;
            $rootScope.workflowDataSource.read();
            

        }

        $scope.showFamily = function () {
            $("#divFmlyTab").show();
            $(".btn-primary").css({ "color": "#091f3f", "background-color": "#efefef","border":"#efefef"});
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
        };
        $scope.showFamily2 = function () {
            $("#divFmlyTab").show();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
            $("#gridfamily").show();
        };

        $scope.printToCart = function (printSectionId) {
            var innerContents = document.getElementById(printSectionId).innerHTML;
            var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWinindow.document.open();
            popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
            popupWinindow.document.close();
        };
        $scope.Print = function () {
            $scope.printToCart("print_tab");
        };
        $scope.ProductPrint = function () {
            $scope.printToCart("ProductPreviewprint_tab");
        };
        $scope.SubProductPrint = function () {
            $scope.printToCart("SubProductPreviewprint_tab");
        };

        $scope.ModifyResults = function () {
            $("#familyEditor").hide();
            //  $("#DivSearch").show();
            $("#recentmodifiedtab").show();
            $("#resultbtntab").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
            $("#searchtab").show();
            $("#dynamictable").hide();
        };

        /*---------------------------------------------------------- Start New Family Creation --------------------------------------------------------------------------------------*/
        $scope.pubCHK = function () {
            if (document.getElementById('publishCHK').checked === true) {
                $scope.Family.PUBLISH2WEB = true;
                $scope.Family.PUBLISH = true;
            } else {
                $scope.Family.PUBLISH2WEB = false;
                $scope.Family.PUBLISH = false;
            }
        };
        $scope.pub2PrintCHK = function () {
            if (document.getElementById('publish2PrintCHK').checked === true) {
                $scope.Family.PUBLISH2PRINT = true;
            } else {
                $scope.Family.PUBLISH2PRINT = false;
            }
        };
        $scope.publish2Pdf = function () {
            if (document.getElementById('publish2Pdf').checked === true) {
                $scope.Family.PUBLISH2PDF = true;
            } else {
                $scope.Family.PUBLISH2PDF = false;
            }
        };
        $scope.publish2Export = function () {
            if (document.getElementById('publish2Export').checked === true) {
                $scope.Family.PUBLISH2EXPORT = true;
            } else {
                $scope.Family.PUBLISH2EXPORT = false;
            }
        };
        $scope.publish2Portal = function () {
            if (document.getElementById('publish2Portal').checked === true) {
                $scope.Family.PUBLISH2PORTAL = true;
            } else {
                $scope.Family.PUBLISH2PORTAL = false;
            }
        };

        $scope.loadNewFamilyDetails = function (familyid, categoryId) {
            //
            $localStorage.CategoryID = categoryId + '~' + familyid + '~!~~';
            $rootScope.LoadProdData($rootScope.selecetedCatalogId, familyid, $localStorage.CategoryID);
            $rootScope.selecetedFamilyId = '~' + familyid;

            $scope.$broadcast("GetFamily", familyid, categoryId);
            // $http.get("../Family/GetFamily?FamilyId=" + familyid + "&categoryId=" + categoryId + "&catalogId=" + $rootScope.selecetedCatalogId).then(function (familyDetails) { $scope.Family = familyDetails.data; });
            $scope.SelectedDataItem.ID = familyid;
            $scope.ActiveCategoryId = familyid;
            $scope.familyRelatedFamilyDatasource.read();
            $rootScope.familySpecsDatasource.read();
            $scope.LoadFamilySpecsData();
            $scope.familySpecsImgAttrDatasource.read();
            //  $scope.LoadAttributeGroupType();
        };

        $scope.Create = function () {
            $scope.cloneCheck = "";
            //if ($scope.IsNewFamily != '0' && $scope.IsNewFamily != '1' && $scope.IsNewFamily != '2') {

            ///////----------------Clone Lock check----------------------///////////////////////
            $rootScope.SelectedNodeIDPresist = 1;
            $http.get("../Family/CloneCheck?category_id=" + $rootScope.FamilyCATEGORY_ID + "&catalog_id=" + $rootScope.selecetedCatalogId + "&option=" + $scope.IsNewFamily + "&PranetFamily=" + $scope.Family.PARENT_FAMILY_ID).then(function (data) {
                if (data.data.trim() !== "") {
                    $scope.cloneCheck = data.data;
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: $scope.cloneCheck + ".",
                        type: "confirm",
                        buttons: [{ value: "Ok" },
                            { value: "Cancel" }
                        ],
                        success: function (result) {
                            if (result === "Ok") {
                                $scope.SaveFamilies("Clone");
                            }
                            else {
                                //$scope.SaveFamilies("NotClone");
                                return;
                            }
                        }
                    });
                }
                else if (data.data.trim() === "") {
                    $scope.SaveFamilies("NotClone");
                }
            });
            // $rootScope.workflowDataSource.read();
        };
        // Save Family Details
        $scope.SaveFamilies = function (status) {
            $scope.option = status;
            if ($scope.Family.CATEGORY_ID !== '') {

                if ($rootScope.FamilyCATEGORY_ID != $scope.Family.CATEGORY_ID) {
                    $scope.IsNewFamily = '3';
                }
            }
            //}
            if ($scope.IsNewFamily !== '2')
                $scope.Family.CATEGORY_ID = $rootScope.FamilyCATEGORY_ID;
            else
                $scope.Family.CATEGORY_ID = $rootScope.FamilyCATEGORY_ID;

            var charArray = $scope.Family.FAMILY_NAME;
            charArray = charArray.replace(/^\s+|\s+$/g, "");
            $scope.Family.FAMILY_NAME = charArray;

            if ($scope.Family.CATEGORY_ID !== '') {
                if ($scope.Family.FAMILY_NAME != '' && $scope.Family.FAMILY_NAME != undefined) {
                    var encryptfamily = $scope.Family.FAMILY_NAME;
                    var encryptedfamilyname = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(encryptfamily), key,
            { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
                    if ($scope.IsNewFamily == '1') {

                        $http.get("../Family/SaveFamily?familyName=" + encryptedfamilyname + "&categoryId=" + $scope.Family.CATEGORY_ID + "&status=" + $scope.Family.STATUS + "&publishToWeb=" + $scope.Family.PUBLISH2WEB + "&publishToPrint=" + $scope.Family.PUBLISH2PRINT + "&catalogId=" + $rootScope.selecetedCatalogId + "&workflowstatus=" + $scope.Family.STATUS_NAME + "&option=" + $scope.option + "&publishToPdf=" + $scope.Family.PUBLISH2PDF + "&publishToExport=" + $scope.Family.PUBLISH2EXPORT + "&publishToPortal=" + $scope.Family.PUBLISH2PORTAL).
                            then(function (data) {

                                if (data.data != 0) {



                                    $rootScope.currentFamilyId = data.data;
                                    $scope.tempfamId = data.data;


                                    if ($scope.SelectedFileForUploadProduct != undefined & $scope.SelectedFileForUploadProduct != "") {
                                        $scope.UploadFileProduct($scope.SelectedFileForUploadProduct, true);
                                    }

                                    if ($scope.SelectedFileForUpload != undefined & $scope.SelectedFileForUpload != "") {
                                        $scope.UploadFileFamily($scope.SelectedFileForUpload, true);
                                    }

                                    $scope.SelectedFileForUpload = '';
                                    $scope.SelectedFileForUploadProduct = '';









                                    $scope.$broadcast("GetFamily", data.data, $scope.Family.CATEGORY_ID);
                                    //$http.get("../Family/GetFamily?FamilyId=" + data.data + "&categoryId=" + $scope.Family.CATEGORY_ID + "&catalogId=" + $rootScope.selecetedCatalogId).then(function (familyDetails) { $scope.Family = familyDetails.data; });
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'Product save completed.',
                                        type: "info"
                                    });

                                    $rootScope.familyCreated = true;
                                    $rootScope.workflowDataSource.read();
                                    //$('#CategoryTab').removeClass('k-state-active');
                                    //$('#FamilyTab').addClass('k-state-active');
                                    //$scope.CategoryTab.show = false;
                                    $scope.loadNewFamilyDetails(data.data, $scope.Family.CATEGORY_ID);
                                    $('#user-toolbar-preview').removeAttr('disabled');
                                    $('#user-toolbar-trash-family').removeAttr('disabled');
                                    if ($scope.userRoleDeleteFamily) {

                                        $('#user-toolbar-association-removal-family').removeAttr('disabled');
                                        $('#user-toolbar-delete-family').removeAttr('disabled');
                                    }
                                    else {
                                        $('#user-toolbar-association-removal-family').attr('disabled', 'disabled');
                                        $('#user-toolbar-delete-family').attr('disabled', 'disabled');
                                    }
                                    $scope.IsNewFamily = '0';
                                }
                                else {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'Product save not completed.',
                                        type: "error"
                                    });
                                    $scope.IsNewFamily = '1';
                                }
                                // $scope.CURRENT_FAMILY_ID = data.data;
                                if ($rootScope.Dashboards != true)
                                    $rootScope.treeData.read();
                                // $rootScope.workflowDataSource.read();
                            });
                        //  $rootScope.workflowDataSource.read();
                    } else if ($scope.IsNewFamily == '0') {


                        if ($scope.SelectedFileForUploadProduct != undefined & $scope.SelectedFileForUploadProduct != "") {
                            $scope.UploadFileProduct($scope.SelectedFileForUploadProduct, true);
                        }

                        if ($scope.SelectedFileForUpload != undefined & $scope.SelectedFileForUpload != "") {
                            $scope.UploadFileFamily($scope.SelectedFileForUpload, true);
                        }

                        $scope.SelectedFileForUpload = '';
                        $scope.SelectedFileForUploadProduct = '';



                        $scope.specFamily = true;
                        $scope.checkFamilyIdFlag = "0";

                        //if ($scope.undoClickFlag == "1")
                        //{
                        //    $scope.removeUndoListValue();
                        //    $scope.undoClickFlag ="0";

                        //}
                        var addBaseFamilyFlag = "0";
                        if ($scope.baseCopyFamilyFlag == "0") {

                            if ($localStorage.savebaseFamilyAttributeRow.length == 0) {
                                // angular.extend($scope.selectedAttributeRow[0], $scope.familyValues);
                                $scope.basecopyfamilyWithAttributes = angular.copy($scope.selectedAttributeRow[0]);
                                $localStorage.savebaseFamilyAttributeRow.push({ familyWithoutAttributes: $scope.familyValues, familyWithAttributes: $scope.basecopyfamilyWithAttributes, selectedFamilyRowAttributes: $scope.selectedFamilyRowAttributes });
                                // $localStorage.savebaseFamilyAttributeRow.push($scope.selectedAttributeRow);
                                $scope.baseCopyFamilyFlag = "1";
                            }

                            else if ($localStorage.savebaseFamilyAttributeRow.length > 1) {

                                for (var k = 0; k < $localStorage.savebaseFamilyAttributeRow.length; k++) {
                                    if ($scope.selectedAttributeRow[0].FAMILY_ID == $localStorage.savebaseFamilyAttributeRow[k].familyWithoutAttributes.FAMILY_ID) {
                                        addBaseFamilyFlag = "1";
                                    }
                                    if (addBaseFamilyFlag == "1") {
                                        $scope.basecopyfamilyWithAttributes = angular.copy($scope.selectedAttributeRow[0]);
                                        $localStorage.savebaseFamilyAttributeRow[k].familyWithoutAttributes = $scope.familyValues;
                                        $localStorage.savebaseFamilyAttributeRow[k].familyWithAttributes = $scope.basecopyfamilyWithAttributes;
                                        $localStorage.savebaseFamilyAttributeRow[k].selectedFamilyRowAttributes = $scope.selectedFamilyRowAttributes;

                                        // $localStorage.savebaseFamilyAttributeRow.push($scope.selectedAttributeRow);
                                        $scope.baseCopyFamilyFlag = "1";
                                        break;
                                    }


                                }

                            }

                            if (addBaseFamilyFlag == "0") {
                                for (var i = 0; i < $localStorage.savebaseFamilyAttributeRow.length; i++) {
                                    if ($scope.selectedAttributeRow[0].FAMILY_ID == $localStorage.savebaseFamilyAttributeRow[i].familyWithoutAttributes.FAMILY_ID) {
                                        $scope.basecopyfamilyWithAttributes = angular.copy($scope.selectedAttributeRow[0]);
                                        $localStorage.savebaseFamilyAttributeRow[i].familyWithoutAttributes = $scope.familyValues;
                                        $localStorage.savebaseFamilyAttributeRow[i].familyWithAttributes = $scope.basecopyfamilyWithAttributes;
                                        $localStorage.savebaseFamilyAttributeRow[i].selectedFamilyRowAttributes = $scope.selectedFamilyRowAttributes;
                                        //  $localStorage.savebaseFamilyAttributeRow.splice(i, 1);
                                        //  $scope.basecopyfamilyWithAttributes = angular.copy($scope.selectedAttributeRow[0]);
                                        //  $localStorage.savebaseFamilyAttributeRow.push({ familyWithoutAttributes: $scope.familyValues, familyWithAttributes: $scope.basecopyfamilyWithAttributes, selectedFamilyRowAttributes: $scope.selectedFamilyRowAttributes });
                                        //  angular.extend($scope.selectedAttributeRow[0], $scope.familyValues);
                                        //  $localStorage.savebaseFamilyAttributeRow.push($scope.selectedAttributeRow);
                                        $scope.baseCopyFamilyFlag = "1";
                                    }
                                }

                                if ($scope.baseCopyFamilyFlag != "1") {
                                    // angular.extend($scope.selectedAttributeRow[0], $scope.familyValues);
                                    //   $localStorage.savebaseFamilyAttributeRow.push($scope.selectedAttributeRow);
                                    $scope.basecopyfamilyWithAttributes = angular.copy($scope.selectedAttributeRow[0]);
                                    $localStorage.savebaseFamilyAttributeRow.push({ familyWithoutAttributes: $scope.familyValues, familyWithAttributes: $scope.basecopyfamilyWithAttributes, selectedFamilyRowAttributes: $scope.selectedFamilyRowAttributes });
                                    $scope.baseCopyFamilyFlag = "1";
                                }
                            }

                        }



                        if ($localStorage.savebaseFamilyAttributeRow.length == 1) {
                            //details.push($localStorage.savebaseFamilyAttributeRow);
                            // $localStorage.saveDetails.push($scope.copyUndoFamilyValues);
                            $localStorage.saveSelectedFamilyDetails = $localStorage.savebaseFamilyAttributeRow;
                            // $rootScope.selectedFamilyDetails = $localStorage.saveSelectedFamilyDetails;
                            $rootScope.selectedFamilyDetails = $localStorage.savebaseFamilyAttributeRow;
                        }
                        else {
                            for (var i = 0; i < $localStorage.savebaseFamilyAttributeRow.length; i++) {
                                if ($scope.Family.FAMILY_ID != $localStorage.savebaseFamilyAttributeRow[i].familyWithoutAttributes.FAMILY_ID) {

                                    //details.push($localStorage.savebaseFamilyAttributeRow);
                                    // $localStorage.saveDetails.push($scope.copyUndoFamilyValues);
                                    $scope.detailslength = $localStorage.savebaseFamilyAttributeRow.length;
                                    if ($scope.detailslength > 5) {
                                        $localStorage.savebaseFamilyAttributeRow.splice(0, 1);
                                    }

                                    $localStorage.saveSelectedFamilyDetails = $localStorage.savebaseFamilyAttributeRow;
                                    // $rootScope.selectedFamilyDetails = $localStorage.saveSelectedFamilyDetails;
                                    $rootScope.selectedFamilyDetails = $localStorage.savebaseFamilyAttributeRow;
                                }
                            }
                        }


                        $rootScope.undoFamilyFlag = "0";


                        //if (details.length == 0)
                        //{
                        //    $scope.familyspec = $rootScope.selectedAttributeRow[0];
                        //    angular.extend($scope.familyspec, $scope.familyValues)
                        //    details.push($scope.familyspec);
                        //    $scope.detailslength = details.length;
                        //    if (details.length > 5) {
                        //        details.splice(0, 1);
                        //    }

                        //    $scope.selectedFamilyDetails = details;
                        //}

                        //else

                        //{
                        //  //  $rootScope.LoadFamilyData($scope.familyspec.CATEGORY_ID, $scope.familyspec.FAMILY_NAME);


                        //    //for (var i = 0; i < details.length; i++) {
                        //    //    if (details[i].FAMILY_ID == $rootScope.selectedAttributeRow[0].FAMILY_ID || details[i].FAMILY_ID == $rootScope.selectedAttributeRow.FAMILY_ID) {
                        //    //        // details.pop(details.pop[i]);
                        //    //        details.splice(i, 1);

                        //    //        //$scope.checkFamilyIdFlag="1";
                        //    //    }

                        //    //}


                        //    $scope.copyUndoFamilyValues = angular.copy($scope.familySpecCopy);
                        //    $scope.copyUndoFamilyValues = $scope.familySpecCopy;
                        //    $rootScope.copyselectedAttributeRow.push($scope.copyUndoFamilyValues);
                        //    $rootScope.selectedAttributeRow = $rootScope.copyselectedAttributeRow;

                        //    $scope.familyspec = $rootScope.copyselectedAttributeRow;

                        //    details.push($scope.familyspec);
                        //    //details = $scope.familyspec;
                        //    $scope.detailslength = details.length;
                        //    if (details.length > 5) {
                        //        details.splice(0, 1);
                        //    }

                        //    $scope.selectedFamilyDetails = details;

                        //    if ($scope.checkFamilyIdFlag == "0") {
                        //        $scope.familyspec = $rootScope.selectedAttributeRow[0];
                        //        angular.extend($scope.familyspec, $scope.familyValues)
                        //        details.push($scope.familyspec);
                        //        $scope.detailslength = details.length;
                        //        if (details.length > 5) {
                        //            details.splice(0, 1);
                        //        }

                        //        $scope.selectedFamilyDetails = details;
                        //    }
                        //}

                        $http.get("../Family/UpdateFamily?familyId=" + $scope.Family.FAMILY_ID + "&familyName=" + encryptedfamilyname + "&categoryId=" + $scope.Family.CATEGORY_ID + "&status=" + $scope.Family.STATUS + "&publishToWeb=" + $scope.Family.PUBLISH + "&publishToPrint=" + $scope.Family.PUBLISH2PRINT + "&workflowstatus=" + $scope.Family.STATUS_NAME + "&publishToPdf=" + $scope.Family.PUBLISH2PDF + "&publishToExport=" + $scope.Family.PUBLISH2EXPORT + "&publishToPortal=" + $scope.Family.PUBLISH2PORTAL).
                            then(function (data) {
                                $rootScope.workflowDataSource.read();
                                //$scope.CopyFamily = {
                                //    FAMILY_ID: $scope.Family.FAMILY_ID,
                                //    FAMILY_NAME: $scope.Family.FAMILY_NAME,
                                //    CATEGORY_ID: $scope.Family.CATEGORY_ID,
                                //    STATUS: $scope.Family.STATUS,
                                //    PUBLISH: $scope.Family.PUBLISH,
                                //    PUBLISH2PRINT: $scope.Family.PUBLISH2PRINT,
                                //    PUBLISH2WEB: $scope.Family.PUBLISH2WEB,
                                //    PUBLISH2PDF: $scope.Family.PUBLISH2PDF,
                                //    PUBLISH2EXPORT: $scope.Family.PUBLISH2EXPORT,
                                //    PUBLISH2PORTAL: $scope.Family.PUBLISH2PORTAL,
                                //    CATEGORY_NAME: $scope.Family.CATEGORY_NAME,
                                //    PARENT_FAMILY_ID: 0
                                //};

                                //    $rootScope.familySpecCopy.push($scope.selectedRow);

                                //angular.extend($scope.familySpecCopy, $scope.CopyFamily);
                                //  $scope.copyUndoFamilyValues = $scope.CopyFamily;

                                //for (var i = 0; i < details.length; i++)
                                //{
                                //    if($scope.Family.FAMILY_NAME==details[i].FAMILY_NAME && $scope.Family.FAMILY_ID==details[i].FAMILY_ID)
                                //    {
                                //        details.splice(i, 1);
                                //    }
                                //}
                                // $scope.copyfamilyWithAttributes = angular.copy($scope.selectedRow);
                                // $localStorage.saveDetails.push({ familyWithoutAttributes: $scope.CopyFamily, familyWithAttributes: $scope.copyfamilyWithAttributes, selectedFamilyRowAttributes: $scope.selectedFamilyRowAttributes });
                                // details.push($scope.copyUndoFamilyValues);
                                //// $localStorage.saveDetails.push($scope.copyUndoFamilyValues);
                                // $scope.detailslength = $localStorage.saveDetails.length;
                                // if ($localStorage.saveDetails.length > 10) {
                                //     $localStorage.saveDetails.splice(0, 1);
                                // }

                                // if ($localStorage.saveDetails.length > 0)
                                // {
                                //     $("#undoClickId").removeClass("undoclick");
                                //     $("#Undooption").removeClass("undolist");
                                // }

                                // $localStorage.saveSelectedFamilyDetails = $localStorage.saveDetails;
                                //// $rootScope.selectedFamilyDetails = $localStorage.saveSelectedFamilyDetails;
                                // $scope.selectedFamilyDetails = $localStorage.saveSelectedFamilyDetails;

                                $scope.SaveOrUpdateFamilSpecs($scope.selectedRow);

                                /*  $.msgBox({
                                      title: $localStorage.ProdcutTitle,
                                      content: 'Family details updated.',
                                      type: "info"
                                  });*/
                                $scope.LoadFamilySpecsData();
                                $rootScope.LoadFamilyData($scope.undoBeforeChangeAttributeRow.CATALOG_ID, $scope.Family.FAMILY_ID, $scope.undoBeforeChangeAttributeRow.CATEGORY_ID);

                                if ($scope.clickedCategoryId != $scope.Family.CATEGORY_ID && $scope.undoListFlag == "1") {
                                    $("#leftNavTreeViewKendoNavigator").data("kendoTreeView").collapse(".k-item");
                                    $scope.undoListFlag = "0";
                                }

                                else if ($rootScope.Dashboards != true)
                                    $rootScope.treeData.read();
                                //--------------------------------FOR WORKFLOW-------------------------------------------------------------//
                                $scope.OpenFamilyDefaultTab();

                                //--------------------------------FOR WORKFLOW-------------------------------------------------------------//
                            });
                    }
                    else if ($scope.IsNewFamily == '2') {

                        if ($scope.Family.CATEGORY_ID !== '' && $scope.Family.PARENT_FAMILY_ID !== '') {
                            $http.get("../Family/SaveSubFamily?familyName=" + encryptedfamilyname + "&categoryId=" + $scope.Family.CATEGORY_ID + "&status=" + $scope.Family.STATUS + "&publishToWeb=" + $scope.Family.PUBLISH + "&publishToPrint=" + $scope.Family.PUBLISH2PRINT + "&catalogId=" + $rootScope.selecetedCatalogId + "&parentFamilyId=" + $scope.Family.PARENT_FAMILY_ID + "&workflowstatus=" + $scope.Family.STATUS_NAME + "&Option=" + $scope.option + "&publishToPdf=" + $scope.Family.PUBLISH2PDF + "&publishToExport=" + $scope.Family.PUBLISH2EXPORT + "&publishToPortal=" + $scope.Family.PUBLISH2PORTAL).
                                then(function (data) {
                                    if (data.data != 0) {

                                        $rootScope.currentFamilyId = data.data;
                                        $rootScope.familyCreated = true;

                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: 'Sub Product save completed.',
                                            type: "info"
                                        });

                                        $scope.loadNewFamilyDetails(data.data, $scope.Family.CATEGORY_ID);
                                        $scope.IsNewFamily = '0';

                                    }
                                    else {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: 'Sub Product not saved, please try again.',
                                            type: "error"
                                        });
                                        $scope.IsNewFamily = '2';
                                    }
                                    if ($rootScope.Dashboards != true)
                                        $rootScope.treeData.read();

                                });
                        }
                    }
                    else if ($scope.IsNewFamily == '3') {
                        if ($scope.Family.CATEGORY_ID != '') {
                            $http.get('../Family/CutPasteFamily?CopiedId=' + $scope.Family.FAMILY_ID + '&RightClickedCategoryId=' + $scope.Family.CATEGORY_ID + '&catalogID=' + $rootScope.selecetedCatalogId + '&previousCatalogID=' + $rootScope.selecetedCatalogId)
                          .then(function (data) {
                              if ($rootScope.Dashboards != true)
                                  $rootScope.treeData.read();
                          });
                        }
                    }
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please enter a valid Item name.',
                        type: "info"
                    });
                }
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Select at least one Category.',
                    type: "info"
                });
            }
            //  $rootScope.treeData.read();
            //$rootScope.workflowDataSource.read();

        }
        /*---------------------------------------------------------- End New Family Creation --------------------------------------------------------------------------------------*/

        /*---------------------------------------------------------- Start SubFamily Creation --------------------------------------------------------------------------------------*/
        $scope.btnSubFamilyCreate = function () {

            if ($scope.Family.PARENT_FAMILY_ID != 0) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Cannot create a Item under a Sub Product.',
                    type: "info"
                });
            }
            else {
                $('#hideFamilyAttributeGroup').hide();
                $("#divFmlyTab").show();
                $("#familyPreview").hide();
                $("#tabledesigner").hide();
                $("#EditMultipletable").hide();
                //$("#FamilySpecsTab").hide();
                $('#FamilySpecsTab').css("display", "none");
                $scope.Family.PARENT_FAMILY_ID = $scope.Family.FAMILY_ID;
                $scope.IsNewFamily = "2";
                $scope.Family.FAMILY_ID = 0;
                $('#user-toolbar-preview').attr('disabled', 'disabled');
                $('#user-toolbar-trash-family').attr('disabled', 'disabled');
                $rootScope.workflowDataSource.read();
                // $scope.$apply(function () {
                $scope.Family.FAMILY_NAME = "";
                $scope.Family.CATEGORY_ID = "";
                $scope.fmlyDescChangeData.STRING_VALUE = "";
                $scope.Family.STATUS = "CREATED";
                $scope.Family.FAMILY_ID = "";
                $scope.$broadcast("ToClearProdFamData", "some data");
                $scope.familyattr.data.STRING_VALUE = "";
                //$scope.renderfamilydescriptions = true;

                // });
                $rootScope.LoadProdData($rootScope.selecetedCatalogId, 0, $localStorage.CategoryID);
                $scope.familyRelatedFamilyDatasource.read();
                $("#divFmlyTab").show();
                $("#familyPreview").hide();
                $("#tabledesigner").hide();
                $("#EditMultipletable").hide();

                $("#FamilySpecsTab").hide();
                var tabstrips = $("#tabstrip").data("kendoTabStrip");
                var myTabsproducts = tabstrips.tabGroup.children("li").eq(2);
                tabstrips.disable(myTabsproducts);

                //$("#FamilyDescriTab").hide();
                //$("#RelatedFamilyTab").hide();
                //$("#clonedCategoryTab").hide();
                //$("#ImagesTab").hide();
                //$("#MultipleTablesTab").hide();

                // $("#FamilyDescriTab").hide();


                $scope.SelectedDataItem.ID = "";
                $scope.originalCategoryDatasource.read();
                $scope.clonedCategoryDatasource.read();
                var tabstrip = $("#tabstripfamily").data("kendoTabStrip");
                var myTab = tabstrip.tabGroup.children("li").eq(0);


                $scope.renderfamilydescriptions = true;
                $scope.LoadFamilySpecsDesc();

                $scope.renderfamilydescriptions = false;

                //$scope.LoadFamilySpecsDesc();


                tabstrip.select(myTab);
            }
        };

        /*---------------------------------------------------------- End SubFamily Creation --------------------------------------------------------------------------------------*/

        /*---------------------------------------------------------- Start Export --------------------------------------------------------------------------------------*/
        /*---------------------------------------------------------- Start Export --------------------------------------------------------------------------------------*/

        $scope.GetProdSpecsExport = function () {
            //var result = $scope.tableDesignerExport($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId, $scope.Family.CATEGORY_ID);

            $("#attributePack").hide();
            if ($scope.columnsForAtt.length > 256) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Maximum 256 Attributes are allowed.',
                    type: "info"
                });
                return;
            }
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Product Export may take some time to process.",
                type: "confirm",
                buttons: [{ value: "Ok" },
                    { value: "Cancel" }
                ],
                success: function (result) {
                    if (result === "Ok") {
                        if ($scope.Family.FAMILY_ID === 0) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Please select at least one Product.',
                                type: "info"
                            });
                        } else {

                            $http.get("../Catalog/GetProdSpecsExport?catalogId=" + $rootScope.selecetedCatalogId + "&familyId=" + $scope.Family.FAMILY_ID + "&categoryId=" + $scope.Family.CATEGORY_ID + "&displayIdColumns=" + $rootScope.DisplayIdcolumns + "&EnableSubProduct=" + $rootScope.EnableSubProduct).
                       then(function (response) {
                           if (response.data === "-1") {
                               $.msgBox({
                                   title: $localStorage.ProdcutTitle,
                                   content: 'Please try again as Export failed.',
                                   type: "error"
                               });
                           } else {

                               window.open("DownloadFullExport.ashx?Path=" + response.data + "&EnableSubProduct=" + $rootScope.EnableSubProduct);
                           }
                       });
                        }
                    }
                    else {

                        return false;
                    }
                }
            });
        };

        /*---------------------------------------------------------- End Export --------------------------------------------------------------------------------------*/



        // Region for Family Specs 
        $scope.saveFamilyAttribute = function (famSpec) {
            $http.get("../Family/SaveFamilySpecs?data=" + JSON.stringify(famSpec.sender._data) + "&&editeddataref=" + JSON.stringify($scope.FamilyAttributeChangeList)).
            then(function () {
            });
        };


        $scope.handleChange = function (dataitem) {
            var isAttributePresent = false;
            for (var i = 0; i < $scope.FamilyAttributeChangeList.length; i++) {
                if ($scope.FamilyAttributeChangeList[i].ATTR_ID == dataitem.ATTR_ID) {
                    isAttributePresent = true;
                }
            }
            if (isAttributePresent == false) {
                $scope.FamilyAttributeChangeList.push({ ATTR_ID: dataitem.ATTR_ID, ATTRIBUTE_NAME: dataitem.ATTRIBUTE_NAME, FAMILY_ID: dataitem.FAMILY_ID, VALUE: dataitem.VALUE });
            }
        };

        $scope.gridColumns = [
            { field: "ATTR_ID", hidden: true },
            { field: "FAMILY_ID", hidden: false },
            { field: "VALUE", hidden: false }
        ];

        $scope.myData1 = [{ name: "Moroni", language: 'fr' },
                                      { name: "Tiancum", language: 'en' },
                                      { name: "Enos", language: 'nl' }];
        $scope.gridOptionsgg = {
            data: 'myData1',
            enableCellSelection: true,
            enableRowSelection: false,
            enableCellEditOnFocus: true,
            columnDefs: [
                {
                    field: 'language', enableFocusedCellEdit: true,
                    editableCellTemplate: '../../language_edit.html',
                    cellFilter: 'LanguageMap'
                },
                { field: 'name', enableFocusedCellEdit: true }

            ],
            canSelectRows: false
        };

        $scope.familySpecsData = [];

        $scope.tblFamilySpecs = new ngTableParams(
        {
            page: 1,
            count: 1000
        },
        {
            counts: [],
            total: function () { return $scope.familySpecsData.length; },
            getData: function ($defer, params) {
                var filteredData = $scope.familySpecsData;
                var orderedData = params.sorting() ?
                    $filter('orderBy')(filteredData, params.orderBy()) :
                    filteredData;
                params.total(orderedData.length);
                $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
            }
        });

        $scope.LoadFamilySpecsData = function () {
            if ($scope.SelectedDataItem.ID !== "") {
                $scope.renderfamilyspecsgrid = true;
                $scope.renderfamilydescriptions = false;
                $scope.renderfamilyimages = false;
                $scope.renderrelatedfamily = false;
                $scope.renderclonedcategory = false;
                $scope.rendermultipletable = false;
                $rootScope.Family_Data = "Data";
                dataFactory.GetDynamicFamilySpecs($scope.Family.FAMILY_ID, $localStorage.CategoryID, $rootScope.selecetedCatalogId).success(function (response) {
                    if (response != null) {
                        var obj = jQuery.parseJSON(response.Data.Data);
                        if (obj.Columns.length > 0) {
                            $scope.familySpecsData = obj.Data;
                            $scope.columnsForFamilSpecs = obj.Columns;
                            $scope.actiondisable = true;
                        } else {
                            $scope.familySpecsData = [];
                            $scope.columnsForFamilSpecs = [];
                            $scope.actiondisable = false;
                        }

                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {
                $scope.renderfamilyspecsgrid = false;
            }
        };

        $rootScope.FamilyEdit = function () {
            $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;



            if ($scope.Family.FAMILY_ID == undefined) {
                $scope.Family.FAMILY_ID = $scope.searchedFamilyId;
            }
            if ($scope.Family.CATEGORY_ID == undefined) {
                $scope.Family.CATEGORY_ID = $scope.searchedCategoryId;
            }




            dataFactory.GetFamilyAttributeDetails($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID).success(function (response) {
                if (response != null) {
                    //var data = response;                    
                    attributeWithout = [];
                    var data = $scope.familyData[0];
                    $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
                    $scope.selectedRow = data;
                    $scope.attributes = Object.keys(data);
                    $scope.selectedFamilyRowAttributes = response;
                    $scope.updateFamilyspecification = data;

                    angular.forEach($scope.attributes, function (value, key) {
                        if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2'))
                            attributeWithout.push(value);
                    });


                    $scope.fromPageNo = 0;
                    $scope.toPageNo = 500;

                    if (attributeWithout.length < 500) {
                        $scope.toPageNo = attributeWithout.length;
                    }


                    //if (attributeWithout.length < $scope.toPageNo) {
                    //    $scope.toPageNo = attributeWithout.length;
                    //}
                    var attributestemp = [];
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        attributestemp.push(attributeWithout[i]);
                    }

                    $scope.attributes = attributestemp;
                    $scope.AllAttributeValuesFamily = $scope.attributes;
                    // To bind top10 vaules in the Grid 
                    $scope.attributesValuesForFamilyEdit = [];
                    $scope.FamilyEditPageCount = 10;

                    for (var i = 0 ; i < $scope.FamilyEditPageCount ; i++) {
                        if ($scope.attributes[i] != undefined)
                            $scope.attributesValuesForFamilyEdit.push($scope.attributes[i])
                    }
                    $scope.attributes = $scope.attributesValuesForFamilyEdit;

                    // End
                    $scope.attrLength = $scope.attributes.length;
                    $scope.FamSpecsCountPerPage = "100";
                    $scope.FamSpecsCurrentPage = "1";
                    $scope.loopCount = [];
                    $scope.loopEditProdsCount = [];

                    var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                    var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                    if ((attributeWithout.length % parseInt($scope.FamSpecsCountPerPage)) > 0) {
                        loopcnt = loopcnt + 1;
                        loopEditProdsCnt = loopEditProdsCnt + 1;
                    }
                    $scope.FamSpecsPageCount = $scope.loopCount;
                    $scope.totalSpecsFamPageCount = loopEditProdsCnt;

                    for (var i = 0; i < loopcnt; i++) {
                        if (((i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)) < attributeWithout.length) {
                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)
                            });
                        } else {
                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: attributeWithout.length
                            });
                        }
                    }
                    var theString;
                    $scope.selectedRowEdit = {};
                    angular.copy(data, $scope.selectedRowEdit);
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].contains('__')) {
                            angular.forEach($scope.selectedFamilyRowAttributes, function (value, key) {
                                var theString = value.ATTRIBUTE_ID;
                                $scope.selectedFamilyRowDynamicAttributes[theString] = [];
                                $scope.selectedFamilyRowDynamicAttributes[theString] = value;

                                if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Num')) {

                                    var numeric = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[0];
                                    var decimal = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[1].split(')')[0];
                                    var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                    pattern = pattern.replace("numeric", numeric);
                                    pattern = pattern.replace("decimal", decimal);
                                    var reg = new RegExp(pattern);
                                    var uimask = $scope.UIMask(numeric, decimal);
                                    $scope.uimask[theString] = uimask;
                                    $scope.attributePattern[theString] = reg;
                                } else if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                    var maxlength = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(')')[0];
                                    $scope.attributePattern[theString] = maxlength;
                                    $scope.uimask[theString] = maxlength;
                                } else {
                                    $scope.attributePattern[theString] = 524288;
                                    $scope.uimask[theString] = 524288;
                                }

                            });
                        }
                    }

                    angular.forEach($scope.selectedRow, function (value, key) {
                        if (!key.contains("FAMILY_ID")) {
                            var sa = $scope.selectedFamilyRowDynamicAttributes[key.split('__')[2]];
                            if (sa !== undefined) {
                                if (sa.USE_PICKLIST) {
                                    var theString = sa.ATTRIBUTE_ID;
                                    $scope.GetFamilyPickListData(theString, sa.PICKLIST_NAME);
                                }
                            }
                        }
                    });



                    //$scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridFamilySpecsEditPopup.html" });
                    //   $scope.winAddOrEditFamilySpecs.center().open();
                }
            }).error(function (error) {
                options.error(error);
            });



        }

        $scope.LoadMoreFamilyData = function () {
            if ($scope.FamilyEditPageCount != 0) {

                $scope.attributesValuesForFamilyEdit = [];
                $scope.FamilyEditPageCount = ($scope.FamilyEditPageCount + 10);

                for (var i = 0 ; i < $scope.FamilyEditPageCount ; i++) {
                    if ($scope.AllAttributeValuesFamily[i] != undefined) {
                        $scope.attributesValuesForFamilyEdit.push($scope.AllAttributeValuesFamily[i])
                    }
                }
                $scope.attributes = $scope.attributesValuesForFamilyEdit;
            }
        }

        $scope.LoadFamilySpecsDesc = function () {



            $scope.renderfamilyspecsgrid = false;
            $scope.renderfamilydescriptions = true;
            $scope.renderfamilyimages = false;
            $scope.renderrelatedfamily = false;
            $scope.renderclonedcategory = false;
            $scope.rendermultipletable = false;
            $rootScope.Family_Data = "Desc";
            //if ($scope.Family.PARENT_FAMILY_ID != $scope.Family.FAMILY_ID) {
            if ($scope.Family.PARENT_FAMILY_ID != 0) {


                if ($scope.SelectedDataItem.ID !== "") {
                    $scope.renderfamilyspecsgrid = false;
                    $scope.renderfamilydescriptions = true;
                    $scope.renderfamilyimages = false;
                    $scope.renderrelatedfamily = false;
                    $scope.renderclonedcategory = false;
                    $scope.rendermultipletable = false;
                    $rootScope.Family_Data = "Desc";


                    $rootScope.familySpecsDatasource.read();
                }
            }
        };
        $scope.LoadFamilySpecsImage = function () {
            if ($scope.SelectedDataItem.ID !== "") {
                $scope.renderfamilyspecsgrid = false;
                $scope.renderfamilydescriptions = false;
                $scope.renderfamilyimages = true;
                $scope.renderrelatedfamily = false;
                $scope.renderclonedcategory = false;
                $scope.rendermultipletable = false;
                $scope.Family.ATTRIBUTE_ID = "";
                $rootScope.Family_Data = "Image";

                // $scope.fmlyImgAttrChangeempty();
                $scope.familySpecsImgAttrDatasource.read();
            }
            else {
                $scope.renderfamilyimages = false;
            }
        };
        $scope.PreviewText = '';
        $scope.previewFamilySpecs = function () {

            $scope.Family.CATEGORY_ID = $rootScope.FamilyCATEGORY_ID;
            if (!$scope.Family.CATEGORY_ID.length || $scope.Family.FAMILY_ID == "") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Product.',
                    type: "error"
                });
            } else {
                //dataFactory.GetCsFamilyPreviewListCheck($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $rootScope.FamilyLevelMultipletablePreview).success(function (response) {
                //    if (response != null) {
                //        if (response == 'No Preview Available') {
                //            var result = confirm("Product table preview not available. Are you sure want to continue?");
                //            if (result === true) {
                //                $("#divFmlyTab").hide();
                //                $("#familyPreview").show();
                //                dataFactory.GetCsFamilyPreviewList($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $rootScope.FamilyLevelMultipletablePreview).success(function (response) {
                //                    if (response != null) {
                //                        $scope.PreviewText = response;
                //                    }
                //                }).error(function (error) {
                //                    options.error(error);
                //                });
                //            }
                //            else {
                //                $("#divFmlyTab").show();
                //                $("#familyPreview").hide();
                //                $("#tabledesigner").hide();
                //                $("#EditMultipletable").hide();
                //            }
                //        }
                //        else {

                $("#divFmlyTab").hide();
                $("#familyPreview").show();

                dataFactory.GetCsFamilyPreviewList($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $rootScope.FamilyLevelMultipletablePreview, $rootScope.EnableSubProduct).success(function (response) {
                    if (response != null) {
                        //debugger;

                        $scope.PreviewText = response.replace(/ITEM#/g, $localStorage.CatalogItemNumber);

                        $timeout(function () {
                            var divText = document.getElementById("dynamictableFmly").outerHTML;
                            var myWindow = window.open('', '_blank', 'width=1600,height=700');
                            var doc = myWindow.document;
                            doc.open();
                            doc.write(divText);
                            doc.close();
                        }, 100);
                        $timeout(function () {
                            $scope.showFamily();
                        }, 150);

                        //$scope.PreviewText = response;
                    }
                }).error(function (error) {
                    options.error(error);
                });
                //        }

                //    }
                //}).error(function (error) {
                //    options.error(error);
                //});


            }

        };


        $scope.LoadRelatedFamilyData = function () {


            if ($scope.SelectedDataItem.ID !== 0 && $scope.SelectedDataItem.CATALOG_ID !== 0 && $scope.SelectedDataItem.ID != "") {

                $scope.renderfamilyspecsgrid = false;
                $scope.renderfamilydescriptions = false;
                $scope.renderfamilyimages = false;
                $scope.renderrelatedfamily = true;
                $scope.renderclonedcategory = false;
                $scope.rendermultipletable = false;
                $rootScope.Family_Data = "FamilyData";
                $scope.familyRelatedFamilyDatasource.read();
            }
            else {
                $scope.renderrelatedfamily = false;
            }

        };
        $scope.LoadCloneFamilyData = function () {
            if ($scope.Family.FAMILY_ID !== 0 && $rootScope.selecetedCatalogId !== 0 && $scope.Family.FAMILY_ID != "") {

                $scope.renderfamilyspecsgrid = false;
                $scope.renderfamilydescriptions = false;
                $scope.renderfamilyimages = false;
                $scope.renderrelatedfamily = false;
                $scope.renderclonedcategory = true;
                $scope.rendermultipletable = false;

                $scope.originalCategoryDatasource.read();
                $scope.clonedCategoryDatasource.read();
            }
            else {
                $scope.renderclonedcategory = false;
            }

        };

        $scope.selectedFamilyRowDynamic = [];
        $scope.GetFamilyPickListData = function (attrName, picklistdata) {
            dataFactory.getPickListData(picklistdata).success(function (response) {
                $scope.selectedFamilyRowDynamic[attrName] = response;
                return response;
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.UIMask = function (decimal, number) {
            var uimask = "";
            var uimask1 = "";
            for (var i = 0; i < decimal; i++) {
                uimask += "x";
            }
            for (var i1 = 0; i1 < number; i1++) {
                uimask1 += "x";
            }
            if (number != 0) {
                return uimask + "." + uimask1;
            } else {
                return uimask;
            }
        };
        $scope.selectedFamilyRowAttributes = [];
        $scope.selectedFamilyRowDynamicAttributes = [];
        $scope.attributePattern = {};
        $scope.uimask = {};
        $scope.UpdateFamilySpecs = function (data) {
            //$scope.selectedRow = angular.copy(data.Data);
            //$scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridItemEditPopup.html" });
            //$scope.winAddOrEditFamilySpecs.center().open();
            attributeWithout = [];
            if ($rootScope.selecetedCatalogId !== 0 && $scope.Family.FAMILY_ID !== 0) {
                dataFactory.GetFamilyAttributeDetails($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID).success(function (response) {
                    if (response != null) {

                        $scope.selectedRow = data.Data;
                        $scope.attributes = Object.keys(data.Data);
                        $scope.selectedFamilyRowAttributes = response;
                        $scope.updateFamilyspecification = data.Data;
                        $scope.fromPageNo = 0;
                        $scope.toPageNo = 15;
                        angular.forEach($scope.attributes, function (value, key) {
                            if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2'))
                                attributeWithout.push(value);
                        });
                        if (attributeWithout.length < $scope.toPageNo) {
                            $scope.toPageNo = attributeWithout.length;
                        }
                        var attributestemp = [];
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            attributestemp.push(attributeWithout[i]);
                        }
                        $scope.attributes = attributestemp;
                        $scope.attrLength = $scope.attributes.length;
                        $scope.FamSpecsCountPerPage = "15";
                        $scope.FamSpecsCurrentPage = "1";
                        $scope.loopCount = [];
                        $scope.loopEditProdsCount = [];

                        var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                        var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                        if ((attributeWithout.length % parseInt($scope.FamSpecsCountPerPage)) > 0) {
                            loopcnt = loopcnt + 1;
                            loopEditProdsCnt = loopEditProdsCnt + 1;
                        }
                        $scope.FamSpecsPageCount = $scope.loopCount;
                        $scope.totalSpecsFamPageCount = loopEditProdsCnt;

                        for (var i = 0; i < loopcnt; i++) {
                            if (((i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)) < attributeWithout.length) {
                                $scope.loopCount.push({
                                    PAGE_NO: (i + 1),
                                    FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                                    TO_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)
                                });
                            } else {
                                $scope.loopCount.push({
                                    PAGE_NO: (i + 1),
                                    FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                                    TO_PAGE_NO: attributeWithout.length
                                });
                            }
                        }
                        var theString;
                        $scope.selectedRowEdit = {};
                        angular.copy(data.Data, $scope.selectedRowEdit);
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            if (attributeWithout[i].contains('__')) {
                                angular.forEach($scope.selectedFamilyRowAttributes, function (value, key) {
                                    var theString = value.ATTRIBUTE_ID;
                                    $scope.selectedFamilyRowDynamicAttributes[theString] = [];
                                    $scope.selectedFamilyRowDynamicAttributes[theString] = value;

                                    if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Num')) {

                                        var numeric = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[0];
                                        var decimal = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[1].split(')')[0];
                                        var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                        pattern = pattern.replace("numeric", numeric);
                                        pattern = pattern.replace("decimal", decimal);
                                        var reg = new RegExp(pattern);
                                        var uimask = $scope.UIMask(numeric, decimal);
                                        $scope.uimask[theString] = uimask;
                                        $scope.attributePattern[theString] = reg;
                                    } else if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                        var maxlength = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(')')[0];
                                        $scope.attributePattern[theString] = maxlength;
                                        $scope.uimask[theString] = maxlength;
                                    } else {
                                        $scope.attributePattern[theString] = 524288;
                                        $scope.uimask[theString] = 524288;
                                    }

                                });
                            }
                        }



                        //  $scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridFamilySpecsEditPopup.html" });
                        //   $scope.winAddOrEditFamilySpecs.center().open();
                    }
                }).error(function (error) {
                    options.error(error);
                });
                $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
                $scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridFamilySpecsEditPopup.html" });
                $scope.winAddOrEditFamilySpecs.title("Product Specification");
                $scope.winAddOrEditFamilySpecs.center().open();
            }
        };
        $scope.onDateSelected = function (e, th) {
            var dateReg = /^\d{2}[./-]\d{2}[./-]\d{4}\s\d{2}:\d{2}\s[AP]M$/;
            if (!e.match(dateReg)) {
                $('#datetime' + th.$$watchers[0].last).val("");
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter valid format.',
                    type: "error"
                });
            }
        };
        $scope.val_type = "true";
        $scope.TextBoxChanged = function (e, id, attribute) {
            var url = e.currentTarget.value;
            var ida = e.currentTarget.id;
            var pattern = /((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)/;
            if (pattern.test(url)) {
            } else {
                //$("#" + ida).focus();
                $scope.selectedRow[attribute] = "";
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter a valid URL.',
                    type: "info"
                });

            }
        };

        //$scope.DataTypeCheck = function (e, id) {
        //    var value = e.currentTarget.value;
        //    var textboxid = e.currentTarget.id;
        //    dataFactory.GetAttributeDataFormat(id).success(function (response) {
        //        //        var details = response;
        //        var format1 = /^[0-9a-zA-Z]+$/;
        //        var format2 = /^[a-zA-Z]+$/;
        //        var format3 = /^[0-9a-zA-Z\r\n\x20-\x7E\u0000-\uFFFF]+$/
        //        var FormatValue = "/" + details[0].ATTRIBUTE_DATAFORMAT.replace(" ", "") + "/";
        //        FormatValue = FormatValue.replace('*','+')
        //        //        if (FormatValue.contains('u0000')) {
        //            var textValue = e.currentTarget.value;
        //            if (!format3.test(textValue)) {
        //                alert("Please Enter Data in a Valid Format.");
        //                $("#" + textboxid).focus();
        //            }
        //        }

        //        else if (FormatValue.contains("0-9")) {
        //            var textValue = e.currentTarget.value;
        //            if (!format1.test(textValue)) {
        //                alert("Please Enter Data in a Valid Format.");
        //                $("#" + textboxid).focus();
        //            }

        //        }
        //        else if (FormatValue.contains("a-zA-Z")) {
        //            var textValue = e.currentTarget.value;
        //            if (!format2.test(textValue)) {
        //                alert("Please Enter Data in a Valid Format.");
        //                $("#" + textboxid).focus();
        //            }
        //        }


        //    }).error(function (error) {
        //        options.error(error);
        //    });
        //};
        $scope.callpaging = function (pageno, row) {
            var displayAttribures = "";
            var tt = $('#textboxvalue4');
            ////if ($scope.val_type === "true") {
            ////    angular.forEach(row, function (value, key) {
            ////        if (key.contains("OBJ")) {
            ////            var sa = $scope.selectedFamilyRowDynamicAttributes[key.split('__')[2]];
            ////            if (value != null && $("#textBoxValue" + key.split('_')[4]).length != 0) {
            ////                var id = sa.ATTRIBUTE_ID;

            ////                if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {

            ////                    row[key] = $("#textBoxValue" + id)[0].nextElementSibling.innerHTML;
            ////                }
            ////                if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
            ////                    row[key] = "";
            ////                }
            ////            }
            ////            else if ($("#textBoxValue" + key.split('_')[4]).length != 0 && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {


            ////                row[key] = $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
            ////            }
            ////            //To check if the attribute is a value required field
            ////            if (sa != undefined) {
            ////                if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {
            ////                    if (displayAttribures == '') {
            ////                        displayAttribures = "The following attribute values are required,\n";
            ////                    }
            ////                    displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
            ////                }
            ////            }
            ////        }

            ////    });
            ////    angular.copy(row, $scope.updateFamilyspecification);
            ////}

            $rootScope.ajaxLoaderDivShow = true;
            $rootScope.customerId = $scope.getCustomerIDs;
            if (pageno != null) {
                if (pageno == "NEXT") {
                    pageno = ($scope.FamSpecsCurrentPage < $scope.totalSpecsFamPageCount) ? parseInt($scope.FamSpecsCurrentPage) + 1 : $scope.totalSpecsFamPageCount;
                    $scope.FamSpecsCurrentPage = pageno;
                    // $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }
                else if (pageno == "PREV") {
                    pageno = ($scope.FamSpecsCurrentPage > 1) ? parseInt($scope.FamSpecsCurrentPage) - 1 : 1;
                    $scope.FamSpecsCurrentPage = pageno;
                    //  $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }
                else if (pageno == $scope.totalSpecsFamPageCount) {
                    $scope.FamSpecsCurrentPage = $scope.totalSpecsFamPageCount;
                    // $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }
                else {
                    $scope.FamSpecsCurrentPage = pageno;
                    // $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = "";
                }

            }

            //  $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = $scope.FamSpecsCurrentPage;

            var pageData = $scope.loopCount[pageno - 1];
            var lpcnt = $scope.loopCount;
            // $("#gridpaging").data("kendoComboBox").value(pageno);
            $scope.fromPageNo = pageData.FROM_PAGE_NO;
            $scope.toPageNo = pageData.TO_PAGE_NO;
            $scope.currentPage = pageData.PAGE_NO - 1;

            if (attributeWithout.length < 15) {
                $scope.toPageNo = attributeWithout.length;
            }
            var attributestemp = [];
            for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
                if (attributeWithout[i] != undefined)
                    attributestemp.push(attributeWithout[i]);
            }
            $scope.attributes = attributestemp;
            $scope.attrLength = $scope.attributes.length;
            for (var i = $scope.fromPageNo - 1; i < $scope.toPageNo; i++) {
                if (attributeWithout[i] != undefined) {
                    var tempattrvals = $scope.selectedFamilyRowAttributes.filter(function (attrVals) { return attrVals.ATTRIBUTE_ID == attributeWithout[i].split('__')[2] });

                    if (tempattrvals.length != 0) {
                        var uimask = "";
                        var theString = tempattrvals[0].ATTRIBUTE_ID;
                        tempattrvals[0].ATTRIBUTE_READONLY = (tempattrvals[0].ATTRIBUTE_READONLY === "true");
                        if (tempattrvals[0].ATTRIBUTE_NAME === 'Supplier') {
                            theString = 0;
                        }
                        if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Num')) {
                            var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                            pattern = pattern.replace("numeric", tempattrvals[0].NUMERICPLACE);
                            pattern = pattern.replace("decimal", tempattrvals[0].DECIMAL);
                            var reg = new RegExp(pattern);
                            uimask = $scope.UIMask(tempattrvals[0].NUMERICPLACE, tempattrvals[0].DECIMAL);
                            tempattrvals[0].attributePattern = reg;
                        } else if (tempattrvals[0].ATTRIBUTE_DATATYPE.contains('Text(')) {
                            tempattrvals[0].attributePattern = tempattrvals[0].DECIMAL;
                            uimask = tempattrvals[0].DECIMAL;
                        } else {
                            tempattrvals[0].attributePattern = 524288;
                            uimask = 524288;
                        }
                        tempattrvals[0].uimask = uimask;
                        $scope.selectedFamilyRowDynamicAttributes[theString] = [];
                        $scope.selectedFamilyRowDynamicAttributes[theString] = tempattrvals[0];
                    }
                }
            }
            $scope.isUIReleased = 0;
            $scope.iterationCount = 0;
            for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                if (attributeWithout[i].contains("OBJ")) {
                    if (attributeWithout[i].split('__')[2] !== "0") {
                        var sa = $scope.selectedFamilyRowDynamicAttributes[attributeWithout[i].split('__')[2]];
                        if (sa.USE_PICKLIST) {
                            $scope.iterationCount++;
                        }
                    }
                }
            }
            $scope.iterationPicklistCount = 0;
            if ($scope.isUIReleased == 0) {
                $scope.selectedRow = {};
                angular.copy($scope.updateFamilyspecification, $scope.selectedRow);
                blockUI.stop();
            }
            $timeout(function () {
                $rootScope.ajaxLoaderDivShow = false;
            }, 50);
        };

        var details = [];

        $scope.SaveOrUpdateFamilSpecs = function (row) {
            $scope.oldFamilySpecs = $scope.selectedRowEdit;

            var displayAttribures = "";
            if ($scope.val_type === "true") {
                angular.forEach(row, function (value, key) {
                    if (key.contains("OBJ")) {
                        var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];



                        if (value != null && $("#textBoxValue" + key.split('_')[4]).length != 0 && sa != undefined && sa.ATTRIBUTE_TYPE != 3) {
                            var id = sa.ATTRIBUTE_ID;

                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {

                                row[key] = $("#textBoxValue" + id)[0].nextElementSibling.innerHTML;
                            }
                            if ($("#textBoxValue" + id)[0].nextElementSibling.innerHTML != value && !$("#textBoxValue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
                                row[key] = "";
                            }
                        }
                        else if (sa != undefined && sa.ATTRIBUTE_TYPE != 3 && $("#textBoxValue" + key.split('_')[4]).length != 0 && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {


                            row[key] = $("#textBoxValue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
                        }
                        //To check if the attribute is a value required field
                        if (sa != undefined) {
                            if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {
                                if (displayAttribures == '') {
                                    displayAttribures = "The following attribute values are required,\n";
                                }
                                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                            }
                        }
                    }

                });
                //$scope.selectedRow = '';
                if (displayAttribures == '') {
                    $timeout(function () {
                        dataFactory.SaveFamilySpecs($rootScope.selecetedCatalogId, $localStorage.CategoryID, row, $scope.oldFamilySpecs).success(function (response) {
                            if (response != null) {
                                $scope.OpenFamilyDefaultTab();
                                $scope.winAddOrEditFamilySpecs.close();
                                $scope.LoadFamilySpecsData();

                                if ($rootScope.undoFamilyFlag == "0") {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'Update completed.',
                                        type: "info"
                                    });
                                }


                            }
                        }).error(function (error) {
                            options.error(error);
                        });
                    }, 150);
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + displayAttribures + '.',
                        type: "info"
                    });
                }

            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter Data using a valid format.',
                    type: "info"
                });
            }
            $scope.LoadFamilySpecsData();
            $rootScope.LoadFamilyData($scope.undoBeforeChangeAttributeRow.CATALOG_ID, $scope.Family.FAMILY_ID, $scope.undoBeforeChangeAttributeRow.CATEGORY_ID);

        };

        $scope.DeleteFamilSpecs = function (selectedRow) {
        };


        $scope.datatypecheck = function (event, type, attribute) {
            var value = event.currentTarget.value;
            var datatype = type;
            var pattern = "";

            if (type !== "All Characters" && !type.contains("0-9") && !type.contains("uFFFF")) {
                pattern = /^[a-zA-Z ]+$/;
            }
            else {
                if (type.contains("uFFFF")) {
                    //pattern = /^[ A-Za-z0-9_@./#&@$%^*!~*?+-]*$/;
                }
                else {
                    pattern = /^[0-9a-zA-Z ]*$/;
                }
            }
            if (pattern !== "" && value.trim() !== "") {
                var id = event.currentTarget.id;
                if (!pattern.test(value)) {
                    $scope.selectedRow[attribute] = "";
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please enter valid Data.',
                        type: "info"
                    });

                }
            }

        };


        $scope.CancelFamilySpecs = function () {
            $scope.winAddOrEditFamilySpecs.close();
            $scope.OpenFamilyDefaultTab();
        };

        $scope.CancelProdSpecs = function () {
            $scope.winAddOrEditFamilySpecs.close();
            $scope.rowselectedEvent($rootScope.getProduct_id, $rootScope.getEventValue, $rootScope.getFamily_id, $rootScope.getclickedData);
        };
    

        $scope.tableDesignerClick = function (v) {
            $scope.rendertabledesigner = true;
            $scope.Family.FAMILY_ID = '' + $scope.Family.FAMILY_ID + '';
            //adding double quotes for family id to satisfy contains condition//
            if ($scope.Family.FAMILY_ID.contains('~')) {
                var Family_ID = $scope.Family.FAMILY_ID.split('~');
                $scope.Family.FAMILY_ID = Family_ID[1];
            }
            dataFactory.GetStructureNameDataSourceDetails($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId, "FamilyTable", 0).success(function (response) {
                if (response.length > 0) {
                    // $scope.winTableDesigner.refresh({ url: "../Views/App/Partials/tableDesignerPopup.html" });
                    $scope.tableGroupHeaderValue = true;
                    $("#divFmlyTab").hide();
                    $("#familyPreview").hide();
                    //$scope.winTableDesigner.refresh({ url: "../Views/App/Partials/tableDesignerPopup.html" });
                    //  $scope.winTableDesigner.center().open();

                    $scope.$broadcast("TABLEDESIGNER");
                    $("#tabledesigner").show();
                    $("#EditMultipletable").hide();
                    $('.k-i-arrow-s').removeClass('k-loading');
                }
                else {
                    $scope.createLayoutClick();
                }

            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.VerticalTable_ChkBox = {
            value1: false
        };
        $scope.StructureType = {
            dataSource: {
                data: [
                    { Text: "Horizontal", value: "Horizontal" },
                    { Text: "Vertical", value: "Vertical" },
                    { Text: "Super Table", value: "SuperTable" }
                ]
            },
            dataTextField: "Text",
            dataValueField: "value"
        };

        $scope.StructureTypeChange = function (e) {
            $scope.Structure.tabletype = e.sender.value();
            if ($scope.Structure.tabletype == 'Vertical') {
                $scope.VerticalTable_ChkBox = {
                    value1: true
                };

            } else {
                $scope.VerticalTable_ChkBox = {
                    value1: false
                };
            }
        };

        $scope.createLayoutClick = function () {
            // $scope.winCreateNewLayout.refresh();
            $scope.winCreateNewLayout.center().open();
            $scope.Structure = {
                structureName: "Sample",
                defaultlayout: true,
                tabletype: "Horizontal"
            };
        };
        $scope.Structure = {
            structureName: "Sample",
            defaultlayout: true,
            tabletype: "Horizontal"
        };
        $scope.CreateStructure = function () {
            $scope.winCreateNewLayout.close();
            $scope.rendertabledesigner = false;
            dataFactory.CreateStructure($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.Structure.defaultlayout, $scope.Structure.structureName, $scope.Structure.tabletype).success(function (response) {

                if (response != null && response !== "Table Layout already exists, please create a different Layout") {
                    $scope.rendertabledesigner = true;
                    $scope.tableGroupHeaderValue = true;
                    $("#divFmlyTab").hide();
                    $("#familyPreview").hide();
                    $scope.$broadcast("TABLEDESIGNER");
                    $("#tabledesigner").show();
                    $("#EditMultipletable").hide();
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Table Layout already exists, please create a different Layout.',
                        type: "info"
                    });
                }
            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.CancelStructure = function () {
            $scope.winCreateNewLayout.close();
        };

        $scope.defaultLayoutClick = function () {
            $scope.winDefaultLayout.refresh({ url: "../Views/App/Partials/setDefaultLayout.html" });
            $scope.winDefaultLayout.center().open();

        };
        $scope.deleteLayoutClick = function () {
            $scope.winDeleteLayout.refresh({ url: "../Views/App/Partials/deleteFamilyLayout.html" });
            $scope.winDeleteLayout.center().open();

        };
        $scope.familyAttributeSetupClick = function () {
            $scope.GetAllFamilyAttributesNew.read();
            //$scope.winFamilyAttributeSetup.refresh({ url: "../Views/App/Partials/familyAttributeSetup.html" });
            //$scope.winFamilyAttributeSetup.center().open();
            $scope.familyGetAllCatalogattributesdataSource.read();
            $scope.familyprodfamilyattrdataSource.read();
            // $scope.LoadFamilySpecsData();
            $rootScope.familySpecsDatasource.read();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#divFmlyTab").show();
            $("#gridfamily").hide();
            $("#columnsetupfamily").show();
            $("#EditMultipletable").hide();
            $scope.items = [];
        };
        $scope.familyNewAttributeSetupClick = function () {
            $scope.winFamilyNewAttributeSetup.refresh({ url: "../Views/App/Partials/familyNewAttributeSetup.html" });
            $scope.winFamilyNewAttributeSetup.center().open();
            // $scope.tableGroupHeaderValue = true;
        };

        //$scope.NewAttrSave = function (attribute) {
        //    if (attribute.ATTRIBUTE_NAME.trim() == "") {
        //        alert("Please enter valid name");
        //    }
        //    else {
        //        dataFactory.savenewattribute(attribute, $scope.CATALOG_ID).success(function (response) {
        //            alert(response);
        //            $scope.Attribute.ATTRIBUTE_SIZE = 0;
        //        }).error(function (error) {
        //            options.error(error);
        //        });
        //    }
        //    $scope.RefreshFamilyAttributes();
        //    $("#customvaluenew").val("");
        //    $("#suffixnew").val("");
        //    $("#prefixnew").val("");
        //    $("#attrapplytofirstnew").val("");
        //    $("#attrapplytoallnew").val("");
        //    $("#txt_attributedatasize").val("");
        //    $("#txt_size").val("");
        //    $("#txt_stylename").val("");
        //    $("#txt_attr_name").val("");
        //    //$("#attrmaingrid").show();
        //    //$("#addnewattribute").hide();
        //};

        $scope.selectedcatalog1 = new kendo.data.HierarchicalDataSource({
            type: "json",
            transport: {
                read: function (options) {
                    // alert("1_TEST");
                    dataFactory.getCategories($rootScope.selecetedCatalogId, options.data.id).success(function (response) {
                        options.success(response);
                        //$("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
                        //$("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
                    }).error(function (response) {
                        options.success(response);
                    });
                },
            },
            schema: {
                model: {
                    id: "id",
                    hasChildren: "hasChildren"
                }
            }
        });


        $scope.treeOptionsCatalog = {

            checkboxes: {
                checkChildren: true,
            },
            loadOnDemand: true,
            dataBound: function (e) {
                //console.log(e);
                if (!e.node) {
                    $scope.attachChangeEvent();
                }
            }
        };

        //For New Product Creation
        $scope.createNewProduct = function () {
            $scope.winNewProductCreation.refresh({ url: "../views/app/partials/newProduct.html" });
            $scope.winNewProductCreation.center().open();
        };

        $scope.$on("NewProductCreation", function (event, selectedCategoryId, categoryId) {
            $scope.IsNewFamily = '0';
            $scope.ActiveCategoryId = selectedCategoryId;
            $http.get("../Family/GetFamily?FamilyId=" + selectedCategoryId + "&categoryId=" + categoryId + "&catalogId=" + $rootScope.selecetedCatalogId).
              then(function (familyDetails) {
                  $scope.Family = familyDetails.data;
                  if ($scope.winNewProductCreation !== undefined) {
                      $scope.winNewProductCreation.refresh({ url: "../views/app/partials/newProduct.html" });
                      $scope.winNewProductCreation.center().open();
                  }
              });
        });

        $scope.$on("NewFamilyCreation", function (event, selectedCategoryId, dataItem) {
            $('#hideFamilyAttributeGroup').hide();
            $scope.IsNewFamily = '1';
            $scope.ActiveCategoryId = selectedCategoryId;
            $("#divNewFmlyAttriEntry").hide();
            $("#divFmlyTab").show();
            $("#familyPreview").hide();
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
            //$rootScope.dropDownTreeViewDatasource.read();
            var parentnode = dataItem;
            if (dataItem.CATEGORY_ID.contains("~")) {
                $scope.Family.PARENT_FAMILY_ID = parentnode.CATEGORY_ID.substring(1, parentnode.CATEGORY_ID.length);
                parentnode = dataItem.parentNode();
                if (parentnode.CATEGORY_ID.contains("~")) {
                    //$scope.Family.PARENT_FAMILY_ID = parentnode.id.substring(1, parentnode.id.length);;
                    //$scope.IsNewFamily = "2";
                    //$scope.Family.FAMILY_NAME = "";
                    //$scope.Family.CATEGORY_ID = "";
                    //$scope.Family.STATUS = "";
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Cannot create a Product under a Sub Family.',
                        type: "info"
                    });
                    parentnode = dataItem.parentNode().parentNode();
                } else {
                    $scope.IsNewFamily = "2";
                    $scope.Family.FAMILY_ID = "";
                    $scope.Family.FAMILY_NAME = "";
                    $scope.Family.CATEGORY_ID = "";
                    $scope.Family.STATUS = "CREATED";
                    $scope.Family.STATUS_NAME = "NEW";
                    $rootScope.LoadProdData($rootScope.selecetedCatalogId, 0, $localStorage.CategoryID);
                    $scope.SelectedDataItem.ID = "";
                    $scope.originalCategoryDatasource.read();
                    $scope.clonedCategoryDatasource.read();
                    var tabstrip = $("#tabstripfamily").data("kendoTabStrip");
                    var myTab = tabstrip.tabGroup.children("li").eq(0);
                    $scope.renderfamilydescriptions = true;
                    $scope.LoadFamilySpecsDesc();
                    tabstrip.select(myTab);
                }
            } else {

                $scope.Family.FAMILY_ID = "";
                $scope.Family.FAMILY_NAME = "";
                $scope.Family.CATEGORY_ID = "";
                $scope.Family.STATUS = "CREATED";
                $scope.Family.STATUS_NAME = "NEW";
                $rootScope.LoadProdData($rootScope.selecetedCatalogId, 0, $localStorage.CategoryID);
                $scope.SelectedDataItem.ID = "";
                $scope.originalCategoryDatasource.read();
                $scope.clonedCategoryDatasource.read();
                $("#divFmlyTab").show();
                $("#familyPreview").hide();
                $("#tabledesigner").hide();
                $("#EditMultipletable").hide();
                var tabstrips = $("#tabstripfamily").data("kendoTabStrip");
                var myTabs = tabstrips.tabGroup.children("li").eq(0);
                $scope.renderfamilydescriptions = true;
                $scope.LoadFamilySpecsDesc();
                tabstrips.select(myTabs);
            }
            $rootScope.FamilyCATEGORY_ID = parentnode.id;
        });

        $scope.$on("FamilyPreview", function (event, selectedCategoryId, categoryId) {
            $("#tabledesigner").hide();
            $("#EditMultipletable").hide();
            $scope.IsNewFamily = '0';
            $scope.ActiveCategoryId = selectedCategoryId;
            $http.get("../Family/GetFamily?FamilyId=" + selectedCategoryId + "&categoryId=" + categoryId + "&catalogId=" + $rootScope.selecetedCatalogId).
              then(function (familyDetails) {
                  $scope.Family = familyDetails.data;
                  $rootScope.FamilyCATEGORY_ID = $scope.Family.CATEGORY_ID;
                  $scope.previewFamilySpecs();
                  $rootScope.LoadFamilyData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $rootScope.FamilyCATEGORY_ID);
              });
        });

        //To Remove the Family Association
        $scope.removeFamilyPermanently = function () {
            if ($scope.selectedFamilyID !== "") {
                $.msgBox({
                    title: "CatalogStudio",
                    content: "Are you sure you want to remove the Product Association?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {
                        var bool = false;
                        if (result === "Yes") {
                            bool = true;
                        }
                        if (bool === true) {
                            dataFactory.DeleteFamilyPermanent($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                                $rootScope.treeData.read();
                                $scope.attributes = [];
                                $("#txtFAMILY_NAME").val(''); $("#familycategoryname").val('');
                                $rootScope.clickSelectedItem($rootScope.treeData._data[0]);
                                //$scope.treeData.read();
                                //  $scope.catalogDataSource.read();
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }
                });
            }
        };
        //To Remove the Family Permanently
        $scope.removeFamilyPermanentlyFromMaster = function () {
            if ($scope.selectedFamilyID !== "") {
                $scope.EnableShowMasterFamily = true;
                $http.get('../Category/GetClonedFamilyDet?Catalog_Id=' + $rootScope.selecetedCatalogId + '&Family_Id=' + $scope.Family.FAMILY_ID + '&Category_Id=' + $rootScope.selecetedCategoryId).then(function (e) {
                    $scope.Flag = e.data;
                    if ($scope.Flag !== "1" && $scope.Flag !== "2") {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "Are you sure you want to delete the Product?",
                            type: "confirm",
                            buttons: [{ value: "Yes" }, { value: "No" }],
                            success: function (result) {
                                var bool = false;
                                if (result === "Yes") {
                                    bool = true;
                                }
                                if (bool === true) {
                                    dataFactory.DeleteFamilyPermanentFromMaster($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + response + '.',
                                            type: "info"
                                        });
                                        $rootScope.treeData.read();
                                        $scope.attributes = [];
                                        //$scope.treeData.read();
                                    }).error(function (error) {
                                        options.error(error);
                                    });
                                }
                            }
                        });
                    }
                    else {
                        if ($scope.Flag === "1") {
                            $scope.EnableShowMasterFamily = true;
                        }
                        else {
                            $scope.EnableShowMasterFamily = false;
                        }
                        $scope.FamilyClone.refresh({ url: "../views/app/partials/FamilyCloneCheckOption.html" });
                        $scope.FamilyClone.center().open();
                    }
                });
            }
        };
        //To move to Recycle Bin the Family Permanently
        $scope.trashFamilyPermanentlyFromMaster = function () {

            dataFactory.checkproducts($rootScope.selecetedCatalogId, $scope.RightClickedCategoryId, $scope.deleteFamilyList).success(function (response) {

                if (response == "Associated") {
                    $scope.ProdCheckAssociate = 1;
                    $scope.FamilyDeletion();
                }
                else if (response == "Defualt_Family") {
                    $scope.ProdCheckAssociate = 1;
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Do you want to Permanent delete the Items under this Default Product?",
                        type: "confirm",
                        buttons: [{ value: "Yes" },
                            { value: "No" }
                        ],
                        success: function (result) {
                            if (result === "Yes") {
                                $scope.FamilyDeletion();
                            }
                        }
                    });

                }

                else if (response == "Not Associated") {
                    $scope.ProdCheckAssociate = 0;
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Do you want to move Default Product?",
                        type: "confirm",
                        buttons: [{ value: "Ok" },
                            { value: "Cancel" }
                        ],
                        success: function (result) {
                            if (result === "Ok") {
                                $scope.FamilyDeletion();
                            }
                        }
                    });
                }
            });
        }
        //if ($scope.selectedFamilyID !== "" && $scope.selectedFamilyID !== undefined)
        //    {

        $scope.FamilyDeletion = function () {
            $rootScope.Productnew = true;
            $scope.EnableShowMasterFamily = true;
            $http.get('../Category/GetClonedFamilyDet?Catalog_Id=' + $rootScope.selecetedCatalogId + '&Family_Id=' + $scope.Family.FAMILY_ID + '&Category_Id=' + $rootScope.selecetedCategoryId).then(function (e) {
                $scope.Flag = e.data;
                if ($scope.Flag !== "1" && $scope.Flag !== "2") {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Are you sure you want to delete the Product?",
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }],
                        success: function (result) {
                            var bool = false;
                            if (result === "Yes") {
                                bool = true;
                            }

                            if (bool === true) {

                                dataFactory.trashFamilyPermanentFromMaster($scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID, $rootScope.selecetedCatalogId, $scope.ProdCheckAssociate).success(function (response) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + response + '.',
                                        type: "info"
                                    });

                                    $rootScope.treeData.read();
                                    $scope.attributes = [];
                                    //$scope.treeData.read();
                                }).error(function (error) {
                                    options.error(error);
                                });
                                $scope.SetFlag();

                            }
                        }
                    });

                }
                else {
                    if ($scope.Flag === "1") {
                        $scope.EnableShowMasterFamily = true;
                    }
                    else {
                        $scope.EnableShowMasterFamily = false;
                    }
                    $scope.FamilyClone.refresh({ url: "../views/app/partials/FamilyCloneCheckOption.html" });
                    $scope.FamilyClone.center().open();

                }

            });


            //}
        };

        //Family Attachment
        $scope.onSuccess = function (e) {
            if (e.operation === "remove") {
                $scope.Family.FAMILYIMG_NAME = "";
            } else {
                var message = $.map(e.files, function (file) {
                    return file.name;
                }).join(", ");
                var imgext = message.substring(message.length, message.length - 4);
                message = message.replace(/&amp;/g, '&');
                if (message.indexOf('&') == -1 && message.indexOf('#') == -1 && message.indexOf('\'') == -1) {
                    $scope.$apply(function () {
                        $scope.FAMILYIMG_NAME_TEMP = '\\Content\\ProductImages\\' + $scope.getCustomerCompanyName + '\\Images\\' + message;
                        //$scope.fmlyImgAttrChangeData.ATTR_VALUE = '\\Content\\ProductImages\\' + $scope.getCustomerCompanyName + '\\Images\\' + message;
                        //$scope.fmlyImgAttrChangeData.IMG_PATH = '\\Content\\ProductImages\\' + $scope.getCustomerCompanyName + '\\Images\\' + message;

                        $scope.Family.FAMILYIMG_NAME = '\\Images\\' + message;
                        $scope.fmlyImgAttrChangeData.ATTR_VALUE = '\\Images\\' + message;
                        $scope.fmlyImgAttrChangeData.IMG_PATH = '\\Images\\' + message;

                    });
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Image name contains invalid special characters( # & \' ).',
                        type: "error"
                    });
                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();
                }

            }
            $(".k-upload-files").remove();
            $(".k-upload-status").remove();
        };
        $scope.onSelect = function (e) {
            var message = $.map(e.files, function (file) { return file.name; }).join(", ");
            angular.forEach($scope.selectedRow, function (value, key) {
                if (key == e.sender.element[0].id) {
                    $scope.selectedRow[key] = '\\Images\\' + e.files[0].name;
                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();
                    $scope.$apply();
                }
            });

        };
        function onUpload(e) {
            if (e.files.length > 1) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Upload single image.',
                    type: "error"
                });
            }
        }

        $scope.SaveFamilyAttach = function () {
            if ($scope.Family.FAMILY_ID !== "") {
                $scope.Family.FAMILYIMG_NAME = $scope.Family.FAMILYIMG_NAME.replace(/&amp;/g, '&');
                dataFactory.SaveFamilyImgAttach($scope.Family, $rootScope.selecetedCatalogId).success(function (response) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                    $(".k-upload-files").remove();
                    $(".k-upload-status").remove();
                    $scope.familySpecsImgAttrDatasource.read();
                }).error(function (error) {
                    options.error(error);
                });
            }

        };

        $scope.ClearFamilyAttach = function () {
            if ($scope.Family.FAMILY_ID !== "") {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Are you sure want to delete the Product Image?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {

                        if (result === "Yes") {
                            dataFactory.ClearFamilyAttach($scope.Family, $rootScope.selecetedCatalogId).success(function (response) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                                $(".k-upload-files").remove();
                                $scope.fmlyImgAttrChangeempty();
                            }).error(function (error) {
                                options.error(error);
                            });


                        }
                        else {
                            return;
                            // $scope.catalogDataSource.read();
                        }


                    }
                });




            }

        };

        $scope.SaveFamilyDescription = function () {

            if ($scope.Family.FAMILY_ID !== "" && $scope.fmlyDescChangeData.ATTRIBUTE_ID != "") {
                $scope.fmlyDescChangeData.STRING_VALUE = $("#__ckd_1")[0].nextElementSibling.innerHTML;
                dataFactory.saveFamilyDescription($scope.fmlyDescChangeData, $rootScope.selecetedCatalogId).success(function (response) {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Attribute.',
                    type: "error"
                });
            }
        };
        $scope.ShowFamilyMTPreview = function () {
            $scope.winFamilyMTPreview.refresh({ url: "../views/app/partials/MultipleTablePreview.html" });
            $scope.winFamilyMTPreview.center().open();
        };
        //--------To get EnableSubProduct value from preference page-------
        $rootScope.getEnableSubProduct = function () {
            //$scope.CustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
            dataFactory.getEnableSubProduct().success(function (response) {

                $rootScope.EnableSubProduct = response;
            })
        };


        // 
        //Filter based on Attribute pack start
        $rootScope.FamilyGroupNameDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetdropdownGroupNameforFamily($rootScope.selecetedCatalogId, "Family", $scope.Family.FAMILY_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });


        //old
        //$rootScope.FamilyGroupNameforFamily = new kendo.data.DataSource({
        //    type: "json",
        //    serverFiltering: true,
        //    transport: {
        //        read: function (options) {
        //            dataFactory.GetdropdownGroupName($rootScope.selecetedCatalogId, "Family").success(function (response) {
        //                options.success(response);
        //            }).error(function (error) {
        //                options.error(error);
        //            });
        //        }
        //    }
        //});


        //new changes

        $rootScope.FamilyGroupNameforFamilyAssociate = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetdropdownGroupName($rootScope.selecetedCatalogId, "Family", $scope.Family.FAMILY_ID).success(function (response) {
                        options.success(response);
                        
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        //Filter based on Attribute pack end

        // Work with separate path - start

        $scope.GetFilePathSavedLocation = function () {
            dataFactory.GetServerFilePath().success(function (response) {
                $scope.getFamilPathFromWebConfig = response[0];
                $scope.isLocalOrServer = response[1];

            }).error(function (error) {
                options.error(error);
            });
        }

        // End

        // To Work with separate path  
        $scope.GetFilePathSavedLocation();
        // End



        $scope.init = function () {
            if ($localStorage.getCatalogID === undefined) {
                $rootScope.selecetedCatalogId = 0;
            }
            else {
                $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
            }

            $("#familyEditor").hide();
            // $("#familyPreview").show();
            $("#resultbtntab").hide();
            $("#tabledesigner").hide();
            $("#columnsetupfamily").hide();
            $("#EditMultipletable").hide();

            $rootScope.FamilyGroupNameforFamilyAssociate.read();
            //Filter based on Attribute pack start
            //$timeout(function () {
            //    $rootScope.FamilyGroupNameforFamily.read();
            //}, 200);

            //Filter based on Attribute pack end


            $scope.getEnableSubProduct();

            if ($rootScope.selectedFamilyDetails.length == 0 || $rootScope.selectedFamilyDetails == undefined) {
                if ($localStorage.savebaseFamilyAttributeRow != undefined) {
                    if ($localStorage.savebaseFamilyAttributeRow.length > 0) {
                        $rootScope.selectedFamilyDetails = $localStorage.savebaseFamilyAttributeRow;
                    }
                }
            }
        };

        $scope.init();


        //------ Multiple Table-------------------------------------------------------------------------------------------------------------------------------//
       
        //$scope.rendermultipletable = true;
        //$scope.renderEdit = true;
        $scope.tableName = "";
        $scope.btnAddTableNames = function () {
            // for (var i = 0; i < counter; i++) {
            //  var newTableName = $scope.data.fields[i].values;
            $("#multipletabledesigner").hide();
            $("#multipleattributegroup").show();
            //$scope.GetAllmultipleGroupdataSource.read();
            if ($scope.tableName.trim() != "") {
                dataFactory.createNewGroupFromMultipleTable($scope.tableName, $scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                    if (response != null) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });
                        $scope.GetAllmultipleGroupdataSource.read();
                        $scope.tableName = "";
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter a Group Name.',
                    type: "error"
                });
            }
            // }
        };
        $scope.RefreshGroup = function () {
            $scope.tableName = '';
            $("#multipletabledesigner").hide();
            $("#multipleattributegroup").show();
            $scope.GetAllmultipleGroupdataSource.read();
            $scope.showmultipleTableSave();
        };
        $scope.RemoveGroup = function () {
            $("#multipletabledesigner").hide();
            $("#multipleattributegroup").show();
            $scope.renderAttributeGroup = false;
            $scope.renderTableDesignermultiple = false;
            $scope.renderRemoveGroup = true;
            $scope.renderEdit = false;
            if ($scope.Group.GROUP_ID === 0) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Group that needs to be deleted.',
                    type: "error"
                });

            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Please Confirm. Do you want to delete this Attribute group?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {
                        var bool = false;
                        if (result === "Yes") {
                            bool = true;
                        }
                        if (bool == true) {
                            dataFactory.RemoveGroupFromMultipleTable($scope.Group.GROUP_ID, $rootScope.selecetedCatalogId).success(function (response) {
                                if (response != null) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'Remove successful.',
                                        type: "info"
                                    });
                                    $scope.GetAllmultipleGroupdataSource.read();
                                    $scope.tableName = "";
                                }
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }
                });
            }
        };
        $scope.AttributeGroup = function () {
            $("#multipletabledesigner").hide();
            $("#multipleattributegroup").show();
            if ($scope.Group.GROUP_ID === 0) {

            } else {
                $scope.renderAttributeGroup = true;
                $scope.renderTableDesignermultiple = false;
                $scope.renderRemoveGroup = false;
                $scope.renderEdit = false;
                $scope.winNewAttributeGorup.refresh({ url: "../views/app/partials/AttributeGroupingPopUp.html" });
                $scope.winNewAttributeGorup.center().open();
            }
        };

        $scope.TableDesigner = function () {
            $scope.renderAttributeGroup = false;
            $scope.renderTableDesignermultiple = true;
            $scope.renderRemoveGroup = false;
            $scope.renderEdit = false;
            $scope.rendermultipletable = true;
            $("#multipletabledesigner").show();
            $("#multipleattributegroup").hide();
            $scope.$broadcast("MULTIPLETABLEDESIGNER");

        };
        $scope.Previewlayout = function () {
            $scope.winPreviewlayout.refresh({ url: "../views/app/partials/MultipletablepreviewPopUp.html" });
            $scope.winPreviewlayout.center().open();
        };

        $scope.Group = {
            GROUP_NAME: '',
            GROUP_ID: 0,
            LAYOUT: 'Horizontal'
        };


        $scope.LoadAttributeGroupType = function () {


            if ($scope.SelectedDataItem.ID.toString() == "") {

                $scope.GetAllmultipleGroupdataSource.read();
                $scope.rendermultipletable = false;
                // $scope.rendermultipletable = false;


            }
            else {
                $scope.renderfamilyspecsgrid = false;
                $scope.renderfamilydescriptions = false;
                $scope.renderfamilyimages = false;
                $scope.renderrelatedfamily = false;
                $scope.renderclonedcategory = false;
                $scope.rendermultipletable = true;
                $scope.renderEdit = true;
                $rootScope.Family_Data == "GroupType"
                $("#multipletabledesigner").hide();
                $("#multipleattributegroup").show();

                $scope.GetAllmultipleGroupdataSource.read();
            }
        };

        $scope.GetAllmultipleGroupdataSource = new kendo.data.DataSource({
            pageSize: 5,
            type: "json",
            serverFiltering: true, editable: true,
            serverPaging: true, autoBind: false,
            serverSorting: true, pageable: true,
            transport: {
                read: function (options) {
                    dataFactory.GetAllMultiplegroups($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }, update: function (options) {
                    dataFactory.UpdateMultiplegroupName(options.data).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });
                        $scope.GetAllmultipleGroupdataSource.read();
                        options.success();
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                    return kendo.stringify(options);

                }
            }, schema: {
                model: {
                    id: "GROUP_ID",
                    fields: {
                        GROUP_ID: { editable: false, nullable: true },
                        GROUP_NAME: { editable: true, validation: { required: true } },
                        CATALOG_ID: { type: "number", editable: false },
                        FAMILY_ID: { editable: false },
                        LAYOUT: { editable: false }
                    }
                }
            }
        });


        $scope.mainGridOptionssd = {
            dataSource: $scope.GetAllmultipleGroupdataSource,
            sortable: true, scrollable: true, pageable: { buttonCount: 5 }, autoBind: false,
            selectable: "row", editable: "inline",
            columns: [{ field: "GROUP_NAME", title: "Table Name" },
            { command: ["edit"], title: "&nbsp;", template: '<div ng-if="userMultiplenableModify"></div>' }],
            detailTemplate: kendo.template($("#templates").html()),
            dataBound: function (e) {
                if (e.sender._data.length > 0) {
                    var row = this.tbody.find('tr:first');
                    this.select(row);
                    $scope.Group.GROUP_ID = e.sender._data[0].GROUP_ID;
                    $scope.Group.LAYOUT = e.sender._data[0].LAYOUT;
                } else {
                    $scope.Group.GROUP_ID = 0;
                }
            }
        };
        $scope.handleChange = function (dataItem) {
            if (dataItem != undefined) {
                $scope.Group.GROUP_ID = dataItem.GROUP_ID;
                $scope.Group.LAYOUT = dataItem.LAYOUT;
            }
        };

        $scope.detailGridmultipleGroupOptions = function (dataItem) {
            return {
                dataSource: {
                    type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.GetAllMultiplegroupsattributes(dataItem.GROUP_ID, $rootScope.selecetedCatalogId).success(function (response) {



                                for (var i = 0 ; response.length > i ; i++) {
                                    if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                        response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                                    }

                                    if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                        response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                                    }
                                }



                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }, schema: {
                        model: {
                            id: "GROUP_ID",
                            fields: {
                                ATTRIBUTE_ID: { editable: false, nullable: true },
                                ATTRIBUTE_NAME: { editable: false, validation: { required: true } },
                                SORT_ORDER: { type: "number" },
                                ATTRIBUTE_TYPE: { editable: false, nullable: true }
                            }
                        }
                    },
                    serverFiltering: true,
                    serverPaging: true,
                    serverSorting: true,
                    pageable: { buttonCount: 5 },
                },
                scrollable: false,
                sortable: true, autobind: false,
                selectable: "row",
                columns: [{ field: "ATTRIBUTE_NAME", title: "Attribute Name" },
                { field: "ATTRIBUTE_TYPE", title: "ATTRIBUTE TYPE", template: "<span>{{attrtypenames(dataItem.ATTRIBUTE_TYPE) }}</span>", }],
                dataBound: function () {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#gridcategoryfamily").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    }
                    else {
                        this.expandRow(this.tbody.find("tr.k-master-row"));
                        //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                    }
                }
            };
        };

        $scope.attrtypenames = function (e) {
            if (e === 1) {
                return "Item Specifications";
            }
            else if (e === 3) {
                return "Item Image / Attachment";
            }
            else if (e === 4) {
                return "Item Price";
            } else if (e === 6) {
                return "Item Key";
            } else if (e === 7) {
                return "Product Description";
            } else if (e === 9) {
                return "Product Image / Attachment";
            } else if (e === 11) {
                return "Product Specifications";
            }
            else if (e === 12) {
                return "Product Price";
            }
            else if (e === 13) {
                return "Product Key";
            } else {
                return "Item Specifications";
            }
        };



        $scope.multipleDatasource = function (data) {

            $scope.grid = true;
            $scope.option = data.sender.text();
            if (data.sender._old !== "") {
                $scope.group_id = data.sender._old;
                $rootScope.LoadProdData2($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId, $scope.group_id);
            }
            else {
                $scope.multipletableData = [];
                $scope.columnsMultipletable = [];
                $scope.tblDashBoardMultiple.reload();
            }
        };

        $rootScope.LoadMultipletableData = function (groupid) {

            if (groupid === 0) {
                $scope.multipletableData = [];
                $scope.columnsMultipletable = [];
                $scope.tblDashBoardMultiple.reload();
            } else {
                dataFactory.multipletablespecs($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, true, $rootScope.selecetedCategoryId, groupid).success(function (response) {

                    //var obj = jQuery.parseJSON(response.Data.Data);
                    var obj = jQuery.parseJSON(response.Data.Data);
                    //  $scope.prodData = obj;
                    //  $scope.columnsForAtt = response.Data.Columns;
                    $scope.multipletableData = obj;
                    $scope.columnsMultipletable = response.Data.Columns;
                    $scope.tblDashBoardMultiple.reload();
                }).error(function (error) {
                    options.error(error);
                });
            }
        };
        $scope.blurCheck = false;
        $scope.datatypecheckMultiple = function (event, type, e) {
            $scope.blurCheck = true;
            var value = event.currentTarget.value;
            var datatype = type;
            var pattern = "";
            if (type !== "All Characters" && !type.contains("0-9") && !type.contains("uFFFF")) {
                pattern = /^[a-zA-Z ]+$/;
            }
            else {
                if (type.contains("uFFFF")) {
                    //pattern = /^[ A-Za-z0-9_@./#&@$%"'^*!~*?+-]*$/;
                }
                else {
                    pattern = /^[0-9a-zA-Z ]*$/;
                }
            }
            if (pattern !== "" && value.trim() !== "") {
                var id = event.currentTarget.id;
                if (!pattern.test(value)) {
                    $scope.selectedRow[e] = "";
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please enter valid Data.',
                        type: "error"
                    });

                }
            }

        };
        $scope.UpdateMultipletable = function (row) {
            var displayAttribures = "";
            if ($scope.val_type === "true") {

                angular.forEach(row, function (value, key) {
                    if (key.contains("OBJ") && !key.contains("Supplier")) {

                        var sa = $scope.selectedRowDynamicAttributes[key.split('__')[2]];
                        if (sa != undefined) {
                            if (sa.VALUE_REQUIRED == true && value == '' || sa.ATTRIBUTE_ID == 1 && value == '') {
                                if (displayAttribures == '') {
                                    displayAttribures = "The following attribute values are required,\n";
                                }
                                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
                            }

                            else if (sa.ATTRIBUTE_ID != 1 && $("#textBoxValue" + sa.ATTRIBUTE_ID) != undefined
                            && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0] != undefined && value != $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.nextSibling.innerHTML
                                && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.nextSibling.innerHTML != ""
                                && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.nextSibling.innerHTML != undefined
                                && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.nextSibling.innerHTML != "<p><br></p>") {
                                if (sa.ATTRIBUTE_TYPE == 4) {
                                    row[key] = value;
                                } else {
                                    row[key] = $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.nextSibling.innerHTML;
                                }


                            }

                            else if (sa.ATTRIBUTE_ID != 1 && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0] != undefined && value != $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.innerHTML && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.innerHTML != "" && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.innerHTML != undefined && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.innerHTML != "<p><br></p>") {
                                row[key] = $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.innerHTML;
                            }
                            else if (sa.ATTRIBUTE_ID != 1 && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0] != undefined && value != $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.innerHTML && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.innerHTML != "" && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.innerHTML != undefined && $("#textBoxValue" + sa.ATTRIBUTE_ID)[0].nextSibling.innerHTML == "<p><br></p>") {
                                row[key] = "";
                            }
                        }
                    }
                });

                if (displayAttribures == '') {

                    dataFactory.SaveProductSpecs(row, $localStorage.CategoryID, 0, $scope.blurCheck, $rootScope.oldMultipleTableEditAttributes).success(function (response) {
                        if (response != null) {

                            $scope.blurCheck = false;
                            $scope.MultipleTableEditPopupWindow.refresh({ url: "../Views/App/Partials/MultipleTableEditPopup.html" });
                            $scope.MultipleTableEditPopupWindow.center().close();
                            $scope.LoadMultipletableData($scope.group_id);
                            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                        }

                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + displayAttribures + '.',
                        type: "info"
                    });
                }
            }

            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter Data using a valid format.',
                    type: "error"
                });
            }
        };

        $scope.selectedRowDynamicAttributes = [];
        $scope.selectedRow = {};
        $scope.UpdateMultipletableProductItem = function (data) {
            // $rootScope.LoadProdData($rootScope.selecetedCatalogId, $scope.SelectedItem, $localStorage.CategoryID);
            $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
            //$scope.selectedRow = data.Data;
            // $scope.selectedRow = [{}];
            $scope.updateproductspecs = data.Data;

            attributeWithout = [];
            $scope.attributes = Object.keys(data.Data);

            $scope.MultipleTableEditPopupWindow.refresh({ url: "../Views/App/Partials/MultipleTableEditPopup.html" });
            $scope.MultipleTableEditPopupWindow.title("Multiple Table Specification");

            if ($scope.catalogids !== 0 && $scope.familyids !== 0) {
                dataFactory.GetAttributeDetails($scope.updateproductspecs.CATALOG_ID, $scope.updateproductspecs.FAMILY_ID).success(function (response) {
                    if (response != null) {
                        $scope.selectedRowAttributes = response;
                        var calcAttributestemp = [];
                        angular.forEach($scope.attributes, function (value, key) {
                            if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT')
                                attributeWithout.push(value);
                        });
                        $scope.attributescalc = attributeWithout;
                        $scope.fromPageNo = 0;
                        $scope.toPageNo = 15;
                        if (attributeWithout.length < 15) {
                            $scope.toPageNo = attributeWithout.length;
                        }
                        var attributestemp = [];
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            attributestemp.push(attributeWithout[i]);
                        }

                        angular.forEach($scope.selectedRowAttributes, function (value, key) {
                            var theString = value.ATTRIBUTE_ID;
                            var uimask = "";
                            value.ATTRIBUTE_READONLY = (value.ATTRIBUTE_READONLY === "true");
                            if (value.ATTRIBUTE_NAME === 'Supplier') {
                                theString = 0;
                            }
                            if (value.ATTRIBUTE_DATATYPE.contains('Num')) {
                                var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                pattern = pattern.replace("numeric", value.NUMERICPLACE);
                                pattern = pattern.replace("decimal", value.DECIMAL);
                                var reg = new RegExp(pattern);
                                uimask = $scope.UIMask(value.NUMERICPLACE, value.DECIMAL);
                                value.attributePattern = reg;
                            } else if (value.ATTRIBUTE_DATATYPE.contains('Text(')) {
                                value.attributePattern = value.DECIMAL;
                                uimask = value.DECIMAL;
                            } else {
                                value.attributePattern = 524288;
                                uimask = 524288;
                            }
                            value.uimask = uimask;
                            $scope.selectedRowDynamicAttributes[theString] = [];
                            $scope.selectedRowDynamicAttributes[theString] = value;
                        });

                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            if (attributeWithout[i].contains("OBJ")) {
                                var sa = $scope.selectedRowDynamicAttributes[attributeWithout[i].split('__')[2]];

                                if (sa.USE_PICKLIST) {
                                    var theString = sa.ATTRIBUTE_ID;
                                    $scope.GetPickListDatas(theString, sa.PICKLIST_NAME);
                                }
                            }
                        }

                        $timeout(function () {
                            angular.copy($scope.updateproductspecs, $scope.selectedRow);

                            $rootScope.oldMultipleTableEditAttributes = $scope.selectedRow;
                            $scope.MultipleTableEditPopupWindow.center().open();
                        }, 500);
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
        };

        $scope.CancelMultipleTable = function () {
            $scope.MultipleTableEditPopupWindow.refresh({ url: "../Views/App/Partials/MultipleTableEditPopup.html" });
            $scope.MultipleTableEditPopupWindow.center().close();
        };
        //----------------------------------------------------------------------------------------------------------------------------------------------------//

        function groupHeaderName(e) {

            if (e.value === 1) {
                return "Item Specifications (" + e.count + " items)";
            }
            else if (e.value === 3) {
                return "Item Image / Attachment (" + e.count + " items)";
            }
            else if (e.value === 4) {
                return "Item Price (" + e.count + " items)";
            } else if (e.value === 7) {
                return "Product Description (" + e.count + " items)";
            } else if (e.value === 6) {
                return "Item Key (" + e.count + " items)";
            } else if (e.value === 9) {
                return "Product Image / Attachment (" + e.count + " items)";
            } else if (e.value === 11) {
                return "Product Specifications (" + e.count + " items)";
            }
            else if (e.value === 12) {
                return "Product Price (" + e.count + " items)";
            }
            else if (e.value === 13) {
                return "Product Key (" + e.count + " items)";
            } else {
                return "Item Specifications (" + e.count + " items)";
            }
        }



        $scope.familyGetAllCatalogattributesdataSource = new kendo.data.DataSource({
            type: "json",
            pageable: true, pageSize: 5, autoBind: false,
            sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetAllCatalogFamilyAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ISAvailable: { type: "boolean" },
                        CATALOG_ID: { editable: false },
                        FAMILY_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false }
                    }
                }
            },
            group: {
                field: "ATTRIBUTE_TYPE", aggregates: [
                   { field: "ATTRIBUTE_TYPE", aggregate: "count" }
                ]
            }

        });

        $scope.familymainGridOptions = {
            dataSource: $scope.familyGetAllCatalogattributesdataSource,
            pageable: { buttonCount: 5 }, autoBind: false,
            filterable: { mode: "row" },
            columns: [
                { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', filterable: false },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name", filterable: true },
                {
                    field: "ATTRIBUTE_TYPE",
                    title: "Attribute Type",
                    hidden: true,
                    aggregates: ["count"],
                    groupHeaderTemplate: groupHeaderName
                }]
        };

        $scope.updateSelection = function (e, id) {
            id.dataItem.set("ISAvailable", e.currentTarget.checked);
        };
        $scope.familyprodfamilyattrdataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            pageable: true, autoBind: false,
            pageSize: 5, sort: { field: "SORT_ORDER", dir: "asc" },
            transport: {
                read: function (options) {
                    //
                    dataFactory.getPublishedfamilyAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $localStorage.CategoryID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ISAvailable: {
                            type: "boolean"
                        },
                        CATALOG_ID: { editable: false },
                        FAMILY_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        SORT_ORDER: { editable: false }
                    }
                }
            }

        });

        $scope.familyselectedGridOptions = {

            dataSource: $scope.familyprodfamilyattrdataSource,
            pageable: { buttonCount: 5 }, autoBind: false,
            selectable: "multiple",
            dataBound: onDataBound,
            columns: [
                { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforprodfamily" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>' },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px" }
            ]
        };

        var updownIndex = null;

        function onDataBound(e) {
            if (updownIndex != null) {
                var view = this.dataSource.view();
                for (var i = 0; i < view.length; i++) {

                    if (updownIndex == i) {
                        var isChecked = this.tbody.find("tr[data-uid='" + view[i].uid + "']").find(".chkbxforprodfamily").attr("checked");
                        if (isChecked) {
                            this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                            .addClass("k-state-selected");
                        }

                    }
                }

                updownIndex = null;
            }
        }

        $scope.updateSelection1 = function (e, id) {
            id.dataItem.set("ISAvailable", e.currentTarget.checked);
        };

        $scope.UnPublishFamilyAttributes = function () {
           
            dataFactory.UnPublishFamilyAttributes($scope.items, $localStorage.CategoryID, $scope.GetAllFamilyAttributesNew.data()).success(function (response) {

                $scope.GetAllFamilyAttributesNew.read();


                $scope.familyGetAllCatalogattributesdataSource.read();
                $scope.familyprodfamilyattrdataSource.read();
                //$scope.LoadFamilySpecsData();
                $rootScope.familySpecsDatasource.read();
                $scope.familySpecsImgAttrDatasource.read();

                //$scope.deleteFamilyPdf();
                //$scope.deleteProductPdf();

                if ($rootScope.Family_Data == "Image") {
                    $scope.LoadFamilySpecsImage();
                }
                else if ($rootScope.Family_Data == "Desc") {
                    $scope.LoadFamilySpecsDesc();
                }
                else if ($rootScope.Family_Data == "Data") {
                    $scope.LoadFamilySpecsData();
                }
                else if ($rootScope.Family_Data == "FamilyData") {

                    $scope.LoadRelatedFamilyData();
                }
                else if ($rootScope.Family_Data == "GroupType") {
                    $scope.LoadAttributeGroupType();
                }
                $scope.OpenFamilyDefaultTab();
            }).error(function (error) {
                options.error(error);
            });
            // $scope.LoadFamilySpecsData();
            var CatalogID = $localStorage.getCatalogID;
            var Id = $rootScope.selecetedFamilyId;
            dataFactory.GetPdfXpressdefaultType_Family("Family", Id, CatalogID).success(function (response) {
                if (response != "") {
                    $scope.SelectedFileForUpload = response;
                    $scope.UploadFile($scope.SelectedFileForUpload, false);
                    debugger;
                    $rootScope.FamilyMrtFile = response;
                }
            });
            dataFactory.getPdfXpressdefaultTemplate_Product('Product', Id, CatalogID).success(function (response) {
                if (response != null && response != '0') {
                    $scope.SelectedFileForProduct = response;
                    debugger;
                    $rootScope.ProductMrtFile = $scope.SelectedFileForProduct;
                }
            });
            $scope.winFamilyAttributeSetup.close();
            $("#columnsetupfamily").hide();
            $("#gridfamily").show();
            $('#FamilySpecsTab').css("display", "block");
            $rootScope.familySpecsDatasource.read();
            $scope.renderfamilydescriptions = true;
        };
        $scope.UnPublishFamilyAttributesSorting = function () {
            dataFactory.UnPublishFamilyAttributes($scope.items, $localStorage.CategoryID, $scope.GetAllFamilyAttributesNew.data()).success(function (response) {
                $scope.GetAllFamilyAttributesNew.read();
            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.RefreshFamilyAttributes = function () {

            $scope.GetAllFamilyAttributesNew.read();

            $scope.familyGetAllCatalogattributesdataSource.read();
            $scope.familyprodfamilyattrdataSource.read();
            $scope.LoadFamilySpecsData();
            $rootScope.familySpecsDatasource.read();
        };
        $scope.CancelFamilyAttributes = function () {
            $scope.LoadFamilySpecsData();
            $scope.winFamilyAttributeSetup.close();
            $("#columnsetupfamily").hide();
            $("#gridfamily").show();
            $rootScope.familySpecsDatasource.read();
            $scope.renderfamilydescriptions = true;
            $scope.OpenFamilyDefaultTab();
        };
        //$scope.PublishAttributes = function () {
        //    dataFactory.PublishAttributes($scope.prodfamilyattrdataSource._data).success(function (response) {
        //        $scope.GetAllCatalogattributesdataSource.read();
        //        $scope.prodfamilyattrdataSource.read();
        //    }).error(function (error) {
        //        options.error(error);
        //    });
        //};
        $scope.SavePublishAttributes = function () {
            //
            var familyId = $localStorage.CategoryID + "~" + $rootScope.selecetedFamilyId;
            dataFactory.SaveFamilyPublishAttributes($scope.familyGetAllCatalogattributesdataSource._data, $localStorage.CategoryID, familyId).success(function (response) {

                $scope.familyGetAllCatalogattributesdataSource.read();
                $scope.familyprodfamilyattrdataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.DeletePublishAttributes = function () {
            //var g = $scope.prodselectedgrid.select();
            //g.each(function (index, row) {
            //    var selectedItem = $scope.prodselectedgrid.dataItem(row);
            //    dataFactory.DeleteFamilyPublishAttributes(selectedItem, $localStorage.CategoryID).success(function (response) {
            //        $scope.familyGetAllCatalogattributesdataSource.read();
            //        $scope.familyprodfamilyattrdataSource.read();
            //    }).error(function (error) {
            //        options.error(error);
            //    });
            //});


            var g = $scope.prodselectedgrid.select();
            var data = [];
            for (var i = 0; i < g.length; i++) {
                var selectedItem = $scope.prodselectedgrid.dataItem(g[i]);
                data.push(selectedItem);
            }

            dataFactory.DeleteFamilyPublishAttributes(data, $localStorage.CategoryID).success(function (response) {

                $scope.familyGetAllCatalogattributesdataSource.read();
                $scope.familyprodfamilyattrdataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.BtnFamilyMoveUpClick = function () {
            var g = $scope.prodselectedgrid.select();
            if (g.length === 1) {
                g.each(function (index, row) {
                    var selectedItem = $scope.prodselectedgrid.dataItem(row);
                    var data = $scope.prodselectedgrid.dataSource.data();
                    var dataRows = $scope.prodselectedgrid.items();
                    var selectedRowIndex = dataRows.index(g);
                    //var sortOrder = data[selectedRowIndex - 1].SORT_ORDER;
                    dataFactory.BtnFamilyMoveUpClick(selectedItem.SORT_ORDER - 1, selectedItem, $rootScope.selecetedCategoryId).success(function (response) {
                        $scope.familyGetAllCatalogattributesdataSource.read();
                        $scope.familyprodfamilyattrdataSource.read();
                        updownIndex = selectedRowIndex - 1;
                    }).error(function (error) {
                        options.error(error);
                    });
                });
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one Attribute.',
                    type: "error"
                });
            }
        };


        $scope.BtnFamilyMoveDownClick = function () {

            var g = $scope.prodselectedgrid.select();
            if (g.length === 1) {
                g.each(function (index, row) {
                    var selectedItem = $scope.prodselectedgrid.dataItem(row);
                    var data = $scope.prodselectedgrid.dataSource.data();
                    var dataRows = $scope.prodselectedgrid.items();
                    var selectedRowIndex = dataRows.index(g);
                    //  var sortOrder = data[selectedRowIndex + 1].SORT_ORDER;
                    dataFactory.BtnFamilyMoveDownClick(selectedItem.SORT_ORDER + 1, selectedItem, $rootScope.selecetedCategoryId).success(function (response) {
                        $scope.familyGetAllCatalogattributesdataSource.read();
                        $scope.familyprodfamilyattrdataSource.read();
                        updownIndex = selectedRowIndex + 1;
                    }).error(function (error) {
                        options.error(error);
                    });
                });
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one Attribute.',
                    type: "error"
                });


            }
        };





        ///////////////////// Table Designer /////////////////////////////////////////////////////////
        $scope.userRoleAddRelatedFamily = false;
        $scope.userRoleModifyRelatedFamily = false;
        $scope.userRoleDeleteRelatedFamily = false;
        $scope.userRoleViewRelatedFamily = false;

        $scope.getUserRoleRightsRelatedFamily = function () {
            var id = 4020302;
            dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddRelatedFamily = response[0].ACTION_ADD;
                    $scope.userRoleModifyRelatedFamily = response[0].ACTION_MODIFY;
                    $scope.userRoleDeleteRelatedFamily = response[0].ACTION_REMOVE;
                    $scope.userRoleViewRelatedFamily = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.init = function () {
            $scope.getUserRoleRightsRelatedFamily();
        };

        $scope.init();


        //--------------------------------Start Cloned Category-------------------------------------

        $scope.LoadClonedCategory = function () {
            if ($scope.SelectedDataItem.ID !== 0 && $scope.SelectedDataItem.CATALOG_ID !== 0) {
                $scope.renderfamilyspecsgrid = false;
                //  $scope.renderfamilydescriptions = false;
                $scope.renderfamilyimages = false;
                $scope.renderrelatedfamily = false;
                $scope.renderclonedcategory = true;
                $scope.rendermultipletable = false;
                $scope.originalCategoryDatasource.read();
                $scope.clonedCategoryDatasource.read();
            }

        };

        $scope.ngclkdelete = function (e) {
            var familyid = e.dataItem.FAMILY_ID;
            $.msgBox({
                title: "CatalogStudio",
                content: "Do You want to remove clone?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }, { value: "Cancel" }],
                success: function (result) {
                    if (result == "Yes") {
                        dataFactory.DeletecloneFamily(familyid, $rootScope.selecetedCatalogId, e.dataItem.CATEGORY_ID).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                            $rootScope.treeData.read();
                            //$scope.treeData.read();
                            var tabstrips = $("#tabstrip").data("kendoTabStrip");
                            var myTabs = tabstrips.tabGroup.children("li").eq(0);
                            tabstrips.select(myTabs);
                            var myTabsfamily = tabstrips.tabGroup.children("li").eq(1);
                            tabstrips.disable(myTabsfamily);
                            var myTabsproducts = tabstrips.tabGroup.children("li").eq(2);
                            tabstrips.disable(myTabsproducts);
                        }).error(function (error) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + error + '.',
                                type: "info"
                            });
                        });
                    }
                }
            });

        };

        $scope.ngclkswitchmaster = function (e) {
            var familyid = e.dataItem.FAMILY_ID;
            var catalogid = e.dataItem.CATALOG_ID;
            if ($rootScope.SelectedNodeID.includes('~!')) {
                $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
            }
            else if ($rootScope.SelectedNodeID.includes('~')) {
                $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
            }
            $rootScope.familyDeleteNavStatus = true;
            $rootScope.SelectedNodeIDPresist = 1;
            dataFactory.CheckmasterFamily(familyid, $rootScope.selecetedCatalogId, e.dataItem.CATEGORY_ID).success(function (response) {
                if (response !== "") {
                    $.msgBox({
                        title: "CatalogStudio",
                        content: response,
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }, { value: "Cancel" }],
                        success: function (result) {
                            if (result == "Yes") {
                                dataFactory.SwitchMasterFamily(familyid, catalogid, e.dataItem.CATEGORY_ID).success(function (response1) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + response1 + '.',
                                        type: "info"
                                    });
                                    $rootScope.treeData.read();
                                    //$scope.treeData.read();
                                    //var tabstrips = $("#tabstrip").data("kendoTabStrip");
                                    //var myTabs = tabstrips.tabGroup.children("li").eq(0);
                                    //tabstrips.select(myTabs);

                                    //var myTabsfamily = tabstrips.tabGroup.children("li").eq(1);
                                    //tabstrips.disable(myTabsfamily);

                                    //var myTabsproducts = tabstrips.tabGroup.children("li").eq(2);
                                    //tabstrips.disable(myTabsproducts);
                                    $scope.LoadCloneFamilyData();
                                }).error(function (error) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + error + '.',
                                        type: "error"
                                    });
                                });
                            }
                        }
                    });
                }
            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + error + '.',
                    type: "error"
                });
            });

        };
        $scope.originalCategoryDatasource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.GetOriginalCategory($rootScope.FAMILY_ID, $rootScope.selecetedCatalogId, 'ORIGINAL', $rootScope.Category_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, schema: {
                model: {
                    fields: {
                        CATEGORY_ID: { editable: false }, CATALOG_NAME: { editable: false },
                        CATEGORY_NAME: { editable: false },
                        CATEGORY_LEVEL: { editable: false }

                        //CATEGORY_SHORT: { editable: false }

                    }
                }
            }
        });

        $scope.orginalCategoryGridOptions = {
            dataSource: $scope.originalCategoryDatasource,
            autoBind: false, selectable: true,
            columns: [{ field: "FAMILY_NAME", title: "Action", width: "80px", template: "<a title='Show'  class='k-link' href='javascript:void(0);'ng-click='ngclkfamilyname(this,0)'>Show</a>" },
                { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
                { field: "CATEGORY_SHORT", title: "Category ID", width: "180px" },
                { field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
                { field: "CATEGORY_LEVEL", title: "Category Level", width: "180px" }],
            editable: false
        };

        $scope.clonedCategoryDatasource = new kendo.data.DataSource({
            type: "json", autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.GetOriginalCategory($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId, 'CLONE', $rootScope.Clone_Category_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, schema: {
                model: {
                    fields: {
                        CATEGORY_ID: { editable: false },
                        CATALOG_NAME: { editable: false },
                        CATEGORY_NAME: { editable: false },
                        CATEGORY_LEVEL: { editable: false }

                    }
                }
            }
        });


        $scope.clonedcategoryshow = false;
        $scope.clonedCategoryGridOptions = {
            dataSource: $scope.clonedCategoryDatasource,
            autoBind: false, selectable: true,
            columns: [{ field: "FAMILY_NAME", title: "Actions", width: "80px", template: "<a title='Show' class='k-link' href='javascript:void(0);' ng-click='ngclkfamilyname(this,1)'>Show</a> , <a title='Delete' class='k-link' href='javascript:void(0);' ng-click='ngclkdelete(this)'>Delete</a>" },
              //  { field: "FAMILY_NAME", title: " ", width: "80px", template: "<a title='FamilyName' class='k-link' href='javascript:void(0);' ng-click='ngclkdelete(this)'>Delete</a>" },
                { field: "FAMILY_NAME", title: "Switch Master", width: "180px", template: "<button class='btn btn-primary' ng-disabled=#=ISSUBFAMILY#  ng-click='ngclkswitchmaster(this)'>Master</button>" },
                { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
                { field: "CATEGORY_SHORT", title: "Category ID", width: "180px" },
                { field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
                { field: "CATEGORY_LEVEL", title: "Category Level", width: "180px" }
            ],
            editable: false,
            dataBound: function (e) {
                if (e.sender._data.length > 0) {
                    $scope.clonedcategoryshow = true;
                } else {
                    $scope.clonedcategoryshow = false;
                }
            }
        };

        $("#attributePack").hide();

        $scope.OpenProductDefaultTab = function () {
            // $rootScope.Dashboards = true;
            $rootScope.LoadProdData($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $localStorage.CategoryID);
            if ($rootScope.familyimportstatus == true) {
                $rootScope.treeData.read();
                $rootScope.familyimportstatus == false;
            }

            $("#attributePack").hide();
            $("#panel").hide();
            $("#filterAttributePack").show();
            $('#opendesignerProd').hide();
            $("#columnsetup").hide();
            var tabstrips = $("#productgridtabstrip").data("kendoTabStrip");
            var myTabs = tabstrips.tabGroup.children("li").eq(0);
            tabstrips.select(myTabs);
            $rootScope.categoryUndo = false;
            $rootScope.familyUndo = false;
            $rootScope.productUndo = true;

            if ($rootScope.selectedProductDetails.length == 0) {
                $rootScope.selectedProductDetails = $localStorage.selectedListOnlyProductDetails;
            }
            $scope.OpenProductDefault();
        };
        $scope.OpenProductDefault = function () {
            $("#productgridtabstrip").show();
            $(".productgridtabstripclass").show();
            $("#divProductGrid").show();
            if ($rootScope.Mainprodcount > 0) {
                $("#productpaging1").show();
                //  $("#Productcountid").show();	
                $("#divProductGrid").show();
            }
            else {
                // $("#Productcountid").hide();	
                $("#divProductGrid").hide();
            }
            $("#gridproduct").show();
            $("#divProductGrid").removeClass("col-sm-2");
            $("#divProductGrid").removeClass("col-sm-3");
            $("#divProductGrid").addClass("col-sm-12");
            $('#selectedattribute').hide();
            $('#myDIV').show();
            $('#sampleProdgrid').show();
            $('#productpaging1').hide();
            $('#productpaging').show();

            $('divProductGrid').show();
            $('divProductGridItemOnly').hide();
            $('subproductsmaingrid').hide();
            $('productpaging1').hide();
            $('sampleProdgrid3').hide();
            $('#subproductsgrid').hide();
            $('selectedattribute').hide();

            $("#productpreview").hide();
            $("#configuratorproducts").hide();
            $("#columnsetupproduct").hide();
            $('#ColumnSetupSubproducts').hide();
            $("#cmnvalupdate").hide();
            $('#mainProductsMenu').show();
            $('#subProductsMenu').hide();
            $('#InvertedProductsTab').hide();
            $("#Familyimport").hide();
            var w = $scope.ViewByHideProducts();
            $scope.showUndoAllbutton = true;



        };
        $scope.loadInvertedProducts = function () {
            $scope.gridCurrentPage = "10";
            $('#InvertedProductsTab').show();
        };
        //--------------------------------End Cloned Category---------------------------------
        //-------------------------------Start Multiple table edit---------------------------
        $scope.Edit = function () {
            $scope.renderAttributeGroup = false;
            $scope.renderTableDesignermultiple = false;
            $scope.renderRemoveGroup = false;
            $scope.renderEdit = true;
            $scope.GetAllmultipleGroupdataSource.read();
            $rootScope.LoadMultipletableData(0);
            $('#multipletabledrp').data('kendoDropDownList').value(-1);
            $("#EditMultipletable").show();
            $("#divFmlyTab").hide();
        };
        $scope.BackMultipleTable = function () {

            $scope.OpenFamilyDefaultTab();
            $("#EditMultipletable").hide();
            $("#divFmlyTab").show();
        };
        $scope.attrid = 0;
        $scope.picklistname = "";
        //$scope.selectedRowDynamic = [];

        //$scope.GetPickListData = function (attrName, picklistdata) {
        //    dataFactory.getPickListData(picklistdata).success(function (response) {
        //            $scope.selectedRowDynamic[attrName] = response;
        //            $scope.selectedFamilyRowDynamic[attrName] = response;
        //    }).error(function (error) {
        //        options.error(error);
        //    });
        //};

        $scope.selectedRowDynamic = [];
        $scope.GetPickListDatas = function (attrName, picklistdata) {

            dataFactory.getPickListData(picklistdata).success(function (response) {

                $scope.selectedRowDynamic[attrName] = response;
                return response;
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.PicklistCreate = function (attrids, picklistname) {
            $scope.attrid = attrids;
            $scope.picklistname = picklistname;
            $scope.SelectedPickList = picklistname;

            $scope.GetPickListDatas($scope.attrid, $scope.picklistname);

            $("#productattrpicklist").show();
            $scope.productattrpicklist.center().open();
            $scope.picklistLoad(picklistname);

        };

        $scope.familyPicklistCreate = function (attrids, picklistname) {

            $scope.attrid = attrids;
            $scope.picklistname = picklistname;
            $scope.SelectedPickList = picklistname;

            $scope.GetPickListDatas($scope.attrid, $scope.picklistname);

            $("#Familyattrpicklist").show();
            $scope.Familyattrpicklist.center().open();
            $scope.picklistLoad(picklistname);

        };



        $scope.Closepopup = function () {
            $scope.GetPickListDatas($scope.attrid, $scope.picklistname);
        };

        $scope.PickListDataType = {
            PICKLIST_DATA_TYPE: ''
        };

        $scope.NewPickListName = {
            PICKLIST_NAME: ''
        };
        $("#PickListValues").hide();
        $("#PickListDateValues").hide();
        $scope.picklistChange = function (pkName) {
            $scope.SelectedPickList = pkName.sender._old;
            $scope.picklistLoad($scope.SelectedPickList);
        };
        $scope.picklistLoad = function (pkName) {
            $http.get("../PickList/LoadSelectedPickList?name=" + pkName).
               then(function (details) {
                   $scope.PickListDataType.PICKLIST_DATA_TYPE = details.data.PICKLIST_DATA_TYPE;
                   $scope.PicklistData.PICKLIST_DATA = details.data.PICKLIST_DATA;
                   $scope.selecetedPLXMLData = details.data.PICKLIST_DATA;
                   if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'datetime') {
                       var grid = $("#PickListDateValuesgrid").data("kendoGrid");
                       grid.dataSource.page(1);
                       $scope.GetPickListDateValuesDatasource.read();
                       $("#PickListValues").hide();
                       $("#PickListDateValues").show();
                       $("#PickListNumberValues").hide();
                   } else if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'numeric') {
                       var grid2 = $("#PickListNumberValuesgrid").data("kendoGrid");
                       grid2.dataSource.page(1);
                       $("#PickListValues").hide();
                       $("#PickListDateValues").hide();
                       $("#PickListNumberValues").show();
                       $scope.GetPickListNumericValuesDatasource.read();
                   } else {
                       var grid3 = $("#PickListValuesgrid").data("kendoGrid");
                       grid3.dataSource.page(1);
                       $("#PickListValues").show();
                       $("#PickListDateValues").hide();
                       $("#PickListNumberValues").hide();
                       $scope.GetPickListValuesDatasource.read();
                   }
               });
        };
        $scope.PickListEntries = new kendo.data.ObservableArray([]);
        $scope.PickListGroup = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.getpicklistall('').success(function (response) {

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        $scope.addPickListValue = function () {

            $scope.PickListEntries.push({ Value: $scope.PicklistValue });
            $scope.PicklistValue = '';
        };
        $scope.gridColumns = [
        { field: "Value", title: "Value" }
        ];
        $scope.addNewPicklist = function () {
            if ($scope.NewPickListName.PICKLIST_NAME.trim() == "") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter a valid Name.',
                    //type: "info"
                });

            }
            else {
                if ($scope.SelectedPickListDataType !== "") {
                    $http.get("../PickList/SaveNewPickList?Name=" + $scope.NewPickListName.PICKLIST_NAME.trim() + "&Datatype=" + $scope.SelectedPickListDataType).
                        then(function (request) {
                            $scope.PickListGroup.read();
                            if (request.data != "") {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Save successful.',
                                    type: "info"
                                });
                            }
                            else {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Name already exists, please enter a different name for the Picklist.',
                                    type: "error"
                                });

                                $scope.NewPickListName.PICKLIST_NAME = "";
                            }
                        });
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select datatype.',
                        type: "error"
                    });
                }
            }
        };
        $scope.PickListDefaultDatatype = [{
            id: 1,
            DATA_TYPE: "Text"
        },
        {
            id: 2,
            DATA_TYPE: "Numeric"
        },
        {
            id: 3,
            DATA_TYPE: "Hyperlink"
        },
        {
            id: 3,
            DATA_TYPE: "Date and Time"
        }];

        $scope.SelectedPickListDataType = "Text";
        $scope.SelectedPickList = "";
        $scope.DataTypeChanged = function (pkDatatype) {

            if (pkDatatype.sender._old.toLowerCase() === "date and time") {
                $scope.SelectedPickListDataType = "Date and Time";
            } else {
                $scope.SelectedPickListDataType = pkDatatype.sender._old;
            }
        };
        $scope.PicklistData = {
            PICKLIST_DATA: ''
        };
        $("#PickListValues").hide();
        $("#PickListDateValues").hide();
        $("#PickListNumberValues").hide();
        $scope.picklistChange = function (pkName) {
            $scope.SelectedPickList = pkName.sender._old;
            if ($scope.SelectedPickList !== "") {
                $http.get("../PickList/LoadSelectedPickList?name=" + $scope.SelectedPickList).
                    then(function (details) {
                        $scope.PickListDataType.PICKLIST_DATA_TYPE = details.data.PICKLIST_DATA_TYPE;
                        $scope.PicklistData.PICKLIST_DATA = details.data.PICKLIST_DATA;
                        $scope.selecetedPLXMLData = details.data.PICKLIST_DATA;
                        if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'date and time') {
                            $scope.GetPickListDateValuesDatasource.read();
                            $("#PickListValues").hide();
                            $("#PickListDateValues").show();
                            $("#PickListNumberValues").hide();
                        } else if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'numeric') {
                            $("#PickListValues").hide();
                            $("#PickListDateValues").hide();
                            $("#PickListNumberValues").show();
                            $scope.GetPickListNumericValuesDatasource.read();
                        } else {
                            $("#PickListValues").show();
                            $("#PickListDateValues").hide();
                            $("#PickListNumberValues").hide();
                            $scope.GetPickListValuesDatasource.read();
                        }
                    });
            } else {
                $("#PickListValues").hide();
                $("#PickListDateValues").hide();
                $("#PickListNumberValues").hide();
            }
        };

        $scope.selecetedPLXMLData = "";
        $scope.GetPickListValuesDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true, editable: true,
            serverPaging: false, autoBind: false,

            serverSorting: true, pageable: false, sort: { field: "ListItem", dir: "asc" },
            transport: {
                read: function (options) {

                    dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {

                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data[$scope.getRowIndex].id, $scope.getRowIndex, "Update", $scope.GetPickListValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Duplicate Picklist values are not allowed.',
                                type: "error"
                            });
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Update successful.',
                                type: "error"
                            });
                        }
                        $scope.GetPickListValuesDatasource.read();
                    }).error(function (error) {
                        options.error(error);
                    });
                }, destroy: function (options) {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Are you sure want to delete the PickList Value?",
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }],
                        success: function (result) {

                            if (result === "Yes") {

                                var withoutSelectedVal = $scope.GetPickListValuesDatasource._data;
                                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data).success(function (response) {
                                    options.success(response);
                                    if (response == false) {

                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: 'The Picklist value is used in another place, Are you want to delete the PickList Value Everywhere?',
                                            //type: "error"
                                            type: "confirm",
                                            buttons: [{ value: "Yes" }, { value: "No" }],
                                            success: function (result) {

                                                if (result === "Yes") {

                                                    dataFactory.removeFromAll($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, withoutSelectedVal).success(function (response) {
                                                        options.success(response);
                                                        if (response == false) {

                                                            $.msgBox({
                                                                title: $localStorage.ProdcutTitle,
                                                                content: 'The Picklist value is deleted successfully.',

                                                                //type: "error"
                                                            });
                                                            $scope.GetPickListValuesDatasource.read();

                                                        }
                                                    });
                                                }
                                            }
                                        });

                                    }
                                    else {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: 'The Picklist value is deleted successfully.',
                                            //type: "error"
                                        });
                                    }

                                }).error(function (error) {
                                    options.error(error);
                                });
                            }
                            //$scope.GetPickListValuesDatasource.read();
                        }
                    });

                },
                create: function (options) {
                    if ($scope.GetPickListValuesDatasource._data[0].id == undefined) {
                        $scope.GetPickListValuesDatasource._data[0].id = "";
                    }
                    else {
                        $scope.GetPickListValuesDatasource._data[0].id = "";
                    }

                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data[0].id, "0", "Insert", $scope.GetPickListValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Duplicate Picklist values are not allowed.',
                                type: "error"
                            });
                            $scope.GetPickListValuesDatasource.read();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            }, batch: true, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ListItem",
                    fields: {
                        ListItem: { editable: true }
                    }
                }
            }

        });
        $scope.GetPickListDateValuesDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            editable: true,
            serverPaging: false,
            serverSorting: true, autoBind: false,
            pageable: false, sort: { field: "ListItem", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    var month, day, year, hours, minutes, seconds;
                    var date = new Date($scope.GetPickListDateValuesDatasource._data[$scope.getRowIndex].id),
                        month = ("0" + (date.getMonth() + 1)).slice(-2),
                        day = ("0" + date.getDate()).slice(-2);
                    hours = ("0" + date.getHours()).slice(-2);
                    minutes = ("0" + date.getMinutes()).slice(-2);
                    seconds = ("0" + date.getSeconds()).slice(-2);

                    var getDate = [month, day, date.getFullYear()].join("/");
                    var getTime = [hours, minutes, seconds].join(":");
                    var getUpdatedID = [getDate, getTime].join(" ");
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, getUpdatedID, $scope.getRowIndex, "Update", $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Duplicate Picklist values are not allowed.',
                                type: "error"
                            });
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Update successful.',
                                type: "error"
                            });
                        }
                        $scope.GetPickListDateValuesDatasource.read();
                    }).error(function (error) {
                        options.error(error);
                    });

                },
                create: function (options) {
                    if ($scope.GetPickListDateValuesDatasource._data[0].id == undefined) {
                        $scope.GetPickListDateValuesDatasource._data[0].id = "";
                    }
                    else {
                        $scope.GetPickListDateValuesDatasource._data[0].id = "";
                    }
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListDateValuesDatasource._data[0].id, "0", "Insert", $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Duplicate Picklist values are not allowed.',
                                type: "error"
                            });
                            $scope.GetPickListDateValuesDatasource.read();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });

                }, destroy: function (options) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Are you sure want to delete the PickList Value?",
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }],
                        success: function (result) {

                            if (result === "Yes") {
                                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                                    options.success(response);
                                    if (response == false) {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: 'The Picklist value is used in another place, please make sure before deleting.',
                                            //type: "error"
                                        });

                                    }
                                    else {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: 'The Picklist value is deleted successfully.',
                                            //type: "error"
                                        });
                                    }

                                }).error(function (error) {
                                    options.error(error);
                                });
                            }
                            $scope.GetPickListDateValuesDatasource.read();
                        }
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            }, batch: true, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ListItem",
                    fields: {
                        ListItem: { editable: true, type: "date" }
                    }
                }
            }

        });
        $scope.GetPickListNumericValuesDatasource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            editable: true,
            serverPaging: false,
            serverSorting: true, autoBind: false,
            pageable: false, sort: { field: "ListItem", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {
                        options.success(response);

                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data[$scope.getRowIndex].id, $scope.getRowIndex, "Update", $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Duplicate Picklist values are not allowed.',
                                type: "error"
                            });
                        }
                        else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Update successful.',
                                type: "error"
                            });
                        }
                        $scope.GetPickListNumericValuesDatasource.read();
                    }).error(function (error) {
                        options.error(error);
                    });

                },
                create: function (options) {
                    if ($scope.GetPickListNumericValuesDatasource._data[0].id == undefined) {
                        $scope.GetPickListNumericValuesDatasource._data[0].id = "";
                    }
                    else {
                        $scope.GetPickListNumericValuesDatasource._data[0].id = "";
                    }
                    dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data[0].id, "0", "Insert", $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response == false) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Duplicate Picklist values are not allowed.',
                                type: "error"
                            });
                            $scope.GetPickListNumericValuesDatasource.read();
                        }
                    }).error(function (error) {
                        options.error(error);
                    });

                }, destroy: function (options) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Are you sure want to delete the PickList Value?",
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }],
                        success: function (result) {

                            if (result === "Yes") {
                                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data).success(function (response) {
                                    options.success(response);
                                    if (response == false) {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: 'The Picklist value is used in another place, please make sure before deleting.',
                                            //type: "error"
                                        });

                                    }
                                    else {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: 'The Picklist value is deleted successfully.',
                                            //type: "error"
                                        });
                                    }

                                }).error(function (error) {
                                    options.error(error);
                                });
                            }
                            $scope.GetPickListNumericValuesDatasource.read();
                        }
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                }
            }, batch: true, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ListItem",
                    fields: {
                        ListItem: { editable: true, type: "number" }
                    }
                }
            }

        });
        $scope.GetPickListNumberValues = {
            dataSource: $scope.GetPickListNumericValuesDatasource,
            sortable: true,
            scrollable: true, filterable: true, autoBind: false,
            pageable: {
                numeric: false,
                previousNext: false,
                messages: {
                    display: "Total PickList Values : {2}"
                }
            }, editable: {
                mode: "inline",
                createAt: "bottom"
            },
            cancel: function (e) {
                setTimeout(function () {
                    var grid = $("#PickListNumberValuesgrid").data("kendoGrid");
                    var gridData = grid.dataSource.view();

                    for (var i = 0; i < gridData.length; i++) {
                        var currentUid = gridData[i].uid;
                        if ($rootScope.userRoleModifyPickListManagement == false) {
                            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var editButton = $(currenRow).find(".k-grid-edit");
                            editButton.hide();
                        }
                        if ($rootScope.userRoleDeletePickListManagement == false) {

                            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var deleteButton = $(currenRow).find(".k-grid-delete");
                            deleteButton.hide();
                        }

                    }
                });
            },
            toolbar: [{ name: "create", text: "", template: "<button title=\'Add\' class=\'k-button k-grid-add k-item \' ng-show=\'userRoleAddPickListManagement\'>Add</button>" },
            ],
            //   selectable: "row",
            columns: [{
                field: "ListItem",
                title: "Pick List Value",
                editor: function (container, options) {
                    $scope.name = '';
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'numeric') {
                        var input = $('<input name="' + options.field + '" required="required"/>');
                        input.appendTo(container);
                        input.kendoNumericTextBox();
                    }
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'text') {
                        var input = $('<input name="' + options.field + '" type="text" required="required"/>');
                        input.appendTo(container);
                    }
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'hyperlink') {
                        var input = $('<input name="' + options.field + '" type="text"  onchange="TextBoxChanged(this)" id="link" required="required"/>');
                        input.appendTo(container);
                    }
                    //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                    var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                    tooltipElement.appendTo(container);
                }
            }, {
                //   command: [
                //    { name: "save", text: "", width: "10px", template: "<a ng-click='updateNumeric(this)' class=\'k-grid-save-changes k-item girdicons\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>" },
                //{ name: "cancel", text: "", width: "10px", template: "<a ng-click='cancelNumeric(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
                //{ name: "destroy", text: "", width: "10px", template: "<a  class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove  blue btn-xs-icon\'></div></a>" }], title: "Actions"
                command: ["edit"], width: "250px"
                , title: "Actions"
            }],
            dataBound: function (e) {
                var editeBtn = $(".k-grid-edit");
                var deleteBtn = $(".k-grid-delete");

                editeBtn.attr("title", "Edit");
                deleteBtn.attr("title", "Delete");

                if (e.sender._data.length > 0) {
                    var sort = e.sender._data[0].Sort;
                    if (sort != undefined) {
                        if (sort != "asc") {
                            if (e.sender.dataSource.sort()[0].dir != "desc") {
                                e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                            }
                        }
                    }
                }
                var grid = $("#PickListNumberValuesgrid").data("kendoGrid");
                var gridData = grid.dataSource.view();
                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    if ($rootScope.userRoleModifyPickListManagement == false) {
                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var editButton = $(currenRow).find(".k-grid-edit");
                        editButton.hide();
                    }
                    if ($rootScope.userRoleDeletePickListManagement == false) {

                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var deleteButton = $(currenRow).find(".k-grid-delete");
                        deleteButton.hide();
                    }

                }
                var grid = $("#PickListNumberValuesgrid").data("kendoGrid");
                $(grid.tbody).on("click", "td", function (e) {
                    var row = $(this).closest("tr");
                    var rowIdx = $("tr", grid.tbody).index(row);
                    var colIdx = $("td", row).index(this);
                    $scope.getRowIndex = rowIdx;

                });
            },
            edit: function (e) {

                var updateBtn = $(".k-grid-update");
                var cancelBtn = $(".k-grid-cancel");

                updateBtn.attr("title", "Update");
                cancelBtn.attr("title", "Cancel");

            }
        };
        $scope.GetPickListDateValues = {
            dataSource: $scope.GetPickListDateValuesDatasource,
            sortable: true,
            scrollable: true, filterable: true, autoBind: false,
            pageable: {
                numeric: false,
                previousNext: false,
                messages: {
                    display: "Total PickList Values : {2}"
                }
            }, editable: {
                mode: "inline",
                createAt: "bottom"
            },
            cancel: function (e) {
                setTimeout(function () {
                    var grid = $("#PickListDateValuesgrid").data("kendoGrid");
                    var gridData = grid.dataSource.view();

                    for (var i = 0; i < gridData.length; i++) {
                        var currentUid = gridData[i].uid;
                        if ($rootScope.userRoleModifyPickListManagement == false) {
                            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var editButton = $(currenRow).find(".k-grid-edit");
                            editButton.hide();
                        }
                        if ($rootScope.userRoleDeletePickListManagement == false) {

                            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var deleteButton = $(currenRow).find(".k-grid-delete");
                            deleteButton.hide();
                        }

                    }
                });
            },
            toolbar: [{ name: "create", text: "", template: "<button title=\'Add\' class=\'k-button k-grid-add k-item \' ng-show=\'userRoleAddPickListManagement\'>Add</button>" }
            ],
            // selectable: "row",
            columns: [{
                field: "ListItem", title: "Pick List Value", format: "{0:MM/dd/yyyy hh:mm tt}", editor: function (container, options) {
                    //create input element and add the validation attribute
                    $scope.name = '';
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'date and time') {
                        var input = $('<input kendo-date-time-picker name="' + options.field + '" k-interval=10 required="required" />');
                        input.appendTo(container);
                    }
                    //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                    var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                    tooltipElement.appendTo(container);
                }, filterable: {
                    ui: function (element) {
                        element.kendoDatePicker({
                            format: "{0:MM/dd/yyyy}"
                        });
                    }
                }
            }, {
                command: ["edit"], width: "250px"
                , title: "Actions"
                //   command: [
                //    { name: "save", text: "", width: "10px", template: "<a ng-click='updateDate(this)' class=\'k-grid-save-changes k-item girdicons\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>" },
                //{ name: "cancel", text: "", width: "10px", template: "<a ng-click='cancelDate(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
                //{ name: "destroy", text: "", width: "10px", template: "<a  class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove  blue btn-xs-icon\'></div></a>" }], title: "Actions"
            }],
            dataBound: function (e) {
                var editeBtn = $(".k-grid-edit");
                var deleteBtn = $(".k-grid-delete");

                editeBtn.attr("title", "Edit");
                deleteBtn.attr("title", "Delete");

                if (e.sender._data.length > 0) {
                    var sort = e.sender._data[0].Sort;
                    if (sort != undefined) {
                        if (sort != "asc") {
                            if (e.sender.dataSource.sort()[0].dir != "desc") {
                                e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                            }
                        }
                    }
                }
                var grid = $("#PickListDateValuesgrid").data("kendoGrid");
                var gridData = grid.dataSource.view();

                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    if ($rootScope.userRoleModifyPickListManagement == false) {
                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var editButton = $(currenRow).find(".k-grid-edit");
                        editButton.hide();
                    }
                    if ($rootScope.userRoleDeletePickListManagement == false) {

                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var deleteButton = $(currenRow).find(".k-grid-delete");
                        deleteButton.hide();
                    }

                }
                var grid = $("#PickListDateValuesgrid").data("kendoGrid");
                $(grid.tbody).on("click", "td", function (e) {
                    var row = $(this).closest("tr");
                    var rowIdx = $("tr", grid.tbody).index(row);
                    var colIdx = $("td", row).index(this);
                    $scope.getRowIndex = rowIdx;

                });
            },
            edit: function (e) {

                var updateBtn = $(".k-grid-update");
                var cancelBtn = $(".k-grid-cancel");

                updateBtn.attr("title", "Update");
                cancelBtn.attr("title", "Cancel");

            }
        };
        $scope.DeletePickList = function () {
            dataFactory.DeletePickList($scope.SelectedPickList).success(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + response + '.',
                    type: "info"
                });
                $scope.selecetedPLXMLData = "";
                $scope.SelectedPickList = "";
                $scope.PickListDataType.PICKLIST_DATA_TYPE = "";
                $("#PickListValues").hide();
                $("#PickListDateValues").hide();
                $("#PickListNumberValues").hide();
                $scope.PickListGroup.read();
            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + error + '.',
                    type: "info"
                });
            });
        };


        $scope.GetPickListValues = {
            dataSource: $scope.GetPickListValuesDatasource,
            sortable: true,
            scrollable: true, filterable: true, autoBind: false,
            pageable: {
                numeric: false,
                previousNext: false,
                messages: {
                    display: "Total PickList Values : {2}"
                }
            }, editable: {
                mode: "inline",
                createAt: "bottom"
            },
            cancel: function (e) {
                setTimeout(function () {
                    var grid = $("#PickListValuesgrid").data("kendoGrid");
                    var gridData = grid.dataSource.view();

                    for (var i = 0; i < gridData.length; i++) {
                        var currentUid = gridData[i].uid;
                        if ($rootScope.userRoleModifyPickListManagement == false) {
                            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var editButton = $(currenRow).find(".k-grid-edit");
                            editButton.hide();
                        }
                        if ($rootScope.userRoleDeletePickListManagement == false) {

                            var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                            var deleteButton = $(currenRow).find(".k-grid-delete");
                            deleteButton.hide();
                        }

                    }
                });
            },
            toolbar: [{ name: "create", text: "", template: "<button title=\'Add\' class=\'k-button k-grid-add k-item \' ng-show=\'userRoleAddPickListManagement\'>Add</button>" }


            ],
            selectable: "row",
            columns: [{
                field: "ListItem", title: "Pick List Value", editor: function (container, options) {
                    //create input element and add the validation attribute
                    $scope.name = '';
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'numeric') {
                        var input = $('<input name="' + options.field + '"  data-required-msg="Picklist value is mandatory." required="required"/>');
                        input.appendTo(container);
                        input.kendoNumericTextBox();
                    }
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'text') {
                        var input = $('<input name="' + options.field + '" type="text"  data-required-msg="Picklist value is mandatory." required="required"/>');
                        input.appendTo(container);
                    }
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'hyperlink') {
                        var input = $('<input name="' + options.field + '" type="text"   data-required-msg="Picklist value is mandatory."  onchange="TextBoxChanged(this)" id="link" required="required"/>');
                        input.appendTo(container);
                    }
                    //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                    var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                    tooltipElement.appendTo(container);
                }
            }, {
                //command: [
                //     { name: "save", text: "", width: "10px", template: "<a ng-click='update(this)' class=\'k-grid-save-changes k-item girdicons\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>" },
                // { name: "cancel", text: "", width: "10px", template: "<a ng-click='cancel(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
                //    { name: "destroy", text: "", width: "10px", template: "<a class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" }

                //], title: "Actions", width: "100px"
                command: ["edit"], width: "250px"
                    , title: "Actions"
            }],
            dataBound: function (e) {
                var editeBtn = $(".k-grid-edit");
                var deleteBtn = $(".k-grid-delete");

                editeBtn.attr("title", "Edit");
                deleteBtn.attr("title", "Delete");

                if (e.sender._data.length > 0) {
                    var sort = e.sender._data[0].Sort;
                    if (sort != undefined) {
                        if (sort != "asc") {
                            if (e.sender.dataSource.sort()[0].dir != "desc") {
                                e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                            }
                        }
                    }
                }

                var grid = $("#PickListValuesgrid").data("kendoGrid");
                var gridData = grid.dataSource.view();


                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    if ($rootScope.userRoleModifyPickListManagement == false) {
                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var editButton = $(currenRow).find(".k-grid-edit");
                        editButton.hide();
                    }
                    if ($rootScope.userRoleDeletePickListManagement == false) {

                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var deleteButton = $(currenRow).find(".k-grid-delete");
                        deleteButton.hide();
                    }

                }
                var grid = $("#PickListValuesgrid").data("kendoGrid");
                $(grid.tbody).on("click", "td", function (e) {
                    var row = $(this).closest("tr");
                    var rowIdx = $("tr", grid.tbody).index(row);
                    var colIdx = $("td", row).index(this);
                    $scope.getRowIndex = rowIdx;

                });
            },
            edit: function (e) {

                var updateBtn = $(".k-grid-update");
                var cancelBtn = $(".k-grid-cancel");

                updateBtn.attr("title", "Update");
                cancelBtn.attr("title", "Cancel");

            }
        };

        $scope.userRoleAddPickList = false;
        $scope.userRoleModifyPickList = false;
        $scope.userRoleDeletePickList = false;
        $scope.userRoleViewPickList = false;
        $scope.getUserRoleRightsPickList = function () {
            var id = 40205;

            dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddPickList = response[0].ACTION_ADD;
                    $scope.userRoleModifyPickList = response[0].ACTION_MODIFY;
                    $scope.userRoleDeletePickList = response[0].ACTION_REMOVE;
                    $scope.userRoleViewPickList = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.OpenPopupWindow = function (e) {
            $rootScope.paramValue = e.value;
            $scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
            $scope.winManageDrive.title("Asset Management");
            $scope.winManageDrive.center().open();
            $scope.driveMahementIsVisible = true;
            if (e.Name == "" || e.Name == undefined) {
                $rootScope.paramValue = "family";
                $scope.attribute = e;
            }
            else {

                $rootScope.paramValue = "family";
                $scope.attribute = e.Value;
            }
        }

        $rootScope.SaveFamilyFileSelection = function (fileName, path) {
            var extn = fileName.split(".");
            // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
            var FileFormat = ["rar", "zip"];
            if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
                $rootScope.categoryAttachPath = path;
                $scope.fmlyImgAttrChangeData.ATTR_VALUE = path;
                $scope.fmlyImgAttrChangeData.IMG_PATH = path;
                $scope.Family.FAMILYIMG_NAME = path;
                $scope.fmlyImgAttrChangeData.FAMILYIMG_NAME = path;
                document.getElementById($scope.attribute).value = path;
                $scope.selectedRow[$scope.attribute] = path;
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'This file type is not supported',
                    type: "info"
                });
            }
            $scope.winManageDrive.center().close();
            $scope.driveMahementIsVisible = false;

        }


        //-----------------------------Paging--------------------------//
        $scope.callFamilyGridPaging = function (pageno) {

            if (pageno != null && pageno <= $scope.totalProdPageCount && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {

                if (pageno == 'PREV' && $scope.ProdCurrentPageTest != 1) {
                    $scope.ProdCurrentPageTest = parseInt($scope.ProdCurrentPageTest) - 1;
                } else if (pageno == 'PREV' && $scope.ProdCurrentPageTest == 1) {
                    $scope.ProdCurrentPageTest = "1";
                } else if (pageno == 'NEXT') {

                    $("#cboProdPageCountUP").data("kendoDropDownList").text("");
                    $("#cboProdPageCountDown").data("kendoDropDownList").text("");
                    $scope.ProdCurrentPageTest = parseInt($scope.ProdCurrentPageTest) + 1;
                    if ($scope.ProdCurrentPageTest > $scope.totalProdPageCount) {
                        $scope.ProdCurrentPageTest = $scope.totalProdPageCount;
                    }
                } else {

                    //$("#cboProdPageCountUP").data("kendoDropDownList").text("");
                    //$("#cboProdPageCountDown").data("kendoDropDownList").text("");
                    $scope.ProdCurrentPageTest = pageno;
                }

                $rootScope.LoadProdData2($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId, $scope.group_id);

            }
        };


        $scope.callProductGridPagePerfamily = function (pageper) {

            if (pageper != null && pageper != "0") {
                $scope.ProdCountPerPageTest = pageper;
                $scope.ProdCurrentPageTest = "1";
                $rootScope.LoadProdData2($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId, $scope.group_id);
            }

        };

        $scope.multipletableData = [];
        $scope.tblDashBoardMultiple = new ngTableParams({ page: 1, count: 1000 },
            {
                //counts: [5, 10, 25, 50, 100], 
                autoBind: true,
                sorting: {
                    SORT: 'asc'     // initial sorting
                },
                total: function () { return $scope.multipletableData.length; },
                $scope: { $data: {} },
                getData: function ($defer, params) {

                    var filteredData = $scope.multipletableData;
                    var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) :
                      filteredData;
                    params.total(orderedData.length);
                    return $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                }
            });

        $rootScope.LoadProdData2 = function (catalogId, id, cat_id, group_id) {

            if (group_id == 0) {
                $scope.multipletableData = [];
                $scope.columnsMultipletable = [];
                $scope.tblDashBoardMultiple.reload();
            } else {
                $scope.sub_id = '0';
                $rootScope.sub_cat_id = cat_id;
                $rootScope.sub_fam_id = id;
                //dataFactory.getprodspecs(catalogId, id, $scope.displayID).success(function (response) {
                //document.getElementById("gridpaging" + $scope.ProdCountPerPage).className = "gridpaginghighlight";
                var ProdCount = [];

                dataFactory.getprodspecsWithoutAttributes(catalogId, id, true, cat_id).success(function (response) {
                    if (response.Data.Columns.length) {
                        var obj = jQuery.parseJSON(response.Data.Data);
                        var ProdEditPageloopcnt = Math.floor(obj.length / parseInt($scope.ProdCountPerPageTest));
                        if ((obj.length % parseInt($scope.ProdCountPerPageTest)) > 0) {
                            ProdEditPageloopcnt = ProdEditPageloopcnt + 1;
                        }
                        $scope.changeProdPage = "1";

                        for (var i = 1; i <= ProdEditPageloopcnt; i++) {
                            ProdCount.push(i);
                        }
                        noOfProd = $scope.ProdCountPerPageTest;
                        $scope.ProdPageCount = ProdCount;
                        if ($scope.ProdCurrentPageTest == "1") {
                            $scope.changeProdPage = 1;
                        } else {
                            $scope.changeProdPage = $scope.ProdCurrentPageTest;
                        }
                        $scope.ProdCurrentPageTest = $scope.changeProdPage;
                        $scope.totalProdPageCount = ProdEditPageloopcnt;
                        $('#cboProdPageCountUP').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageTest;
                        $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageTest;
                    }
                });
                dataFactory.multipleTableSpecs(catalogId, id, true, cat_id, $scope.ProdCurrentPageTest, $scope.ProdCountPerPageTest, $scope.sub_id, group_id).success(function (response) {
                    var obj = jQuery.parseJSON(response.Data.Data);
                    $('#cboProdPageCountUP').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageTest;
                    $('#cboProdPageCountDown').data("kendoDropDownList").span[0].innerHTML = $scope.ProdCurrentPageTest;
                    $scope.multipletableData = obj;
                    $scope.columnsMultipletable = response.Data.Columns;
                    $scope.tblDashBoardMultiple.reload();
                }).error(function (error) {
                    options.error(error);
                });
            }
        };

        $scope.callProductGridPagingTest = function (pageno) {
            if (pageno != null && pageno <= $scope.totalProdPageCount && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {
                if (pageno == 'PREV' && $scope.ProdCurrentPageTest != 1) {
                    $scope.ProdCurrentPageTest = parseInt($scope.ProdCurrentPageTest) - 1;
                } else if (pageno == 'PREV' && $scope.ProdCurrentPageTest == 1) {
                    $scope.ProdCurrentPageTest = "1";
                } else if (pageno == 'NEXT') {
                    $scope.ProdCurrentPageTest = parseInt($scope.ProdCurrentPageTest) + 1;
                    if ($scope.ProdCurrentPageTest > $scope.totalProdPageCount) {
                        $scope.ProdCurrentPageTest = $scope.totalProdPageCount;
                    }
                } else {
                    $scope.ProdCurrentPageTest = pageno;
                }

                $scope.LoadProdData1(2, "~293", "CAT4~293");
            }
        };
        $scope.LoadProdData1 = function (catalogId, id, cat_id) {
            $scope.sub_id = '0';
            //dataFactory.getprodspecs(catalogId, id, $scope.displayID).success(function (response) {
            //document.getElementById("gridpaging" + $scope.ProdCountPerPageTest).className = "gridpaginghighlight";
            var prodCount = [];
            dataFactory.getprodspecsWithoutAttributes(catalogId, id, true, cat_id).success(function (response) {
                if (response.Data.Columns.length) {
                    var obj = jQuery.parseJSON(response.Data.Data);
                    var prodEditPageloopcnt = Math.floor(obj.length / parseInt($scope.ProdCountPerPageTest));
                    if ((obj.length % parseInt($scope.ProdCountPerPageTest)) > 0) {
                        prodEditPageloopcnt = prodEditPageloopcnt + 1;
                    }
                    $scope.changeProdPage = "1";

                    for (var i = 1; i <= prodEditPageloopcnt; i++) {
                        prodCount.push(i);
                    }
                    $scope.ProdPageCount = prodCount;
                    if ($scope.ProdCurrentPageTest == "1") {
                        $scope.changeProdPage = 1;
                    } else {
                        $scope.changeProdPage = $scope.ProdCurrentPageTest;
                    }
                    $scope.ProdCurrentPageTest = $scope.changeProdPage;
                    $scope.totalProdPageCount = prodEditPageloopcnt;
                }
            });
            dataFactory.getprodspecs(catalogId, id, true, cat_id, $scope.ProdCurrentPageTest, $scope.ProdCountPerPageTest, $scope.sub_id).success(function (response) {
                var obj = jQuery.parseJSON(response.Data.Data);
                $scope.prodData = obj;
                $scope.columnsForAtt = response.Data.Columns;
                angular.forEach($scope.prodData, function (value) {
                    value.SORT = parseFloat(value.SORT);
                });
                // $scope.tblDashBoardTest.reload();

            }).error(function (error) {
                options.error(error);
            });

        };
        // Pdf
        $scope.OnlyDeleteFileUploadProduct = function () {

            var $el = $('#ProductFile');
            if ($el.val() != "") {
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();
            }

            var $el = $('#FamilyFile');
            if ($el.val() != "") {
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();
            }

        }

        $scope.$on("RefreshFileuploadValues", function () {

            $scope.OnlyDeleteFileUploadProduct();
        });



        $scope.OnlyDeleteFileUploadFamily = function () {
            var $el = $('#FamilyFile');
            if ($el.val() != "") {
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();
            }
        }

        $scope.deleteFamilyPdf = function () {

            var id = $scope.Family.FAMILY_ID;
            var catId = $rootScope.selecetedCatalogId;
            //$scope.undoFamilyTemplateData = [];
            $scope.SelectedFileForFamily = '';
            $scope.SelectedFileForFamilyTooltip = 'No file chosen';
            dataFactory.clearPdfXpressType('Family', id, catId).success(function (response) {

                if (response != null && response.m_Item1 != null) {
                    $scope.SelectedFileForUpload = response.m_Item1;

                    $scope.SelectedFileForFamily = response.m_Item1;
                    $scope.SelectedFileForFamilyTooltip = response.m_Item1;
                } else {
                    var $el = $('#FamilyFile');
                    if ($el.val() != "") {
                        $el.wrap('<form>').closest('form').get(0).reset();
                        $el.unwrap();
                    }
                }

            })
        }










        //$scope.SelectedFileForUpload = response;

        //$scope.getCategoryPdfExpress = '';


        //var $el = $('#chooseFile');
        //if ($el.val() != "") {
        //    $el.wrap('<form>').closest('form').get(0).reset();
        //    $el.unwrap();
        //}


        $scope.GetDefaultPdfTemplate = function () {
           
            var TYPE = 'FAMILY';
            var Id = $scope.Family.FAMILY_ID;
            var Catalog_id = $rootScope.selecetedCatalogId;

            dataFactory.getPdfXpressdefaultType(TYPE, Id, Catalog_id).success(function (response) {
                if (response != null && response != '0') {
                    $scope.SelectedFileForFamily = response;
                    $scope.SelectedFileForFamilyTooltip = response;
                }
                else {
                    $scope.SelectedFileForFamily = '';
                    $scope.SelectedFileForFamilyTooltip = 'No file chosen';
                }
                dataFactory.getPdfXpressdefaultType('Product', Id, Catalog_id).success(function (response) {
                    if (response != null && response != '0') {
                        $scope.SelectedFileForProduct = response;
                        $scope.SelectedFileForProductTooltip = response;
                    }
                    else {
                        $scope.SelectedFileForProduct = '';
                        $scope.SelectedFileForProductTooltip = 'No file chosen';
                    }
                    $rootScope.LoadFamilyData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
                });
            });
        }
        //$scope.GetDefaultPdfTemplateProduct = function () {

        //    var TYPE = 'FAMILY';
        //    var Id = $scope.Family.FAMILY_ID;
        //    var Catalog_id = $rootScope.selecetedCatalogId;

        //    dataFactory.getPdfXpressdefaultType(TYPE, Id, Catalog_id).success(function (response) {

        //        $scope.SelectedFileForFamily = response.substring(0, 12);

        //    });
        //}
        //-----------------------------End-----------------------------//
        $scope.OpenFamilyDefaultTab = function () {
            $('#opendesignerFAM').hide();
            $("#columnsetupfamily").hide();
            $('#hideFamilyAttributeGroup').show();
            $("#tabledesigner").hide();
            $("#divFmlyTab").show();
            $("#gridfamily").show();
            //------------------------------------------------------Undo flags--------------------------------------------------------//
            $rootScope.categoryUndo = false;
            $rootScope.familyUndo = true;
            $rootScope.productUndo = false;
            //------------------------------------------------------Undo flags--------------------------------------------------------//
            $scope.LoadFamilySpecsData();
            $scope.GetDefaultPdfTemplate();
            $scope.baseCopyFamilyFlag = "0";
            $rootScope.FamilyGroupNameforFamilyAssociate.read();
            $rootScope.LoadFamilyData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
            $scope.showUndoAllbutton = false;
            $scope.GetDefaultPdfTemplate();
            $scope.LoadFamilySpecsData();
            $rootScope.LoadFamilyData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
            $rootScope.workflowDataSource.read();
        }
        $rootScope.LoadFamilyData = function (catalogId, id, cat_id, packId) {
            $scope.searchedFamilyId = id;
            $scope.searchedCategoryId = cat_id;

            dataFactory.getFamilyspecification(catalogId, id, cat_id, packId).success(function (response) {
                var obj = jQuery.parseJSON(response.Data.Data);
                $scope.familyData = obj;


                var data = $scope.familyData[0];

                $scope.selectedRow = data;
                $scope.attributes = Object.keys(data);

                //--------------------------------PDF template Value------------------------------------//

                if ($scope.familyValues != undefined && ($scope.SelectedFileForFamily != "" && $scope.SelectedFileForFamily != undefined) && ($scope.SelectedFileForProduct != "" && $scope.SelectedFileForProduct != undefined)) {
                    $scope.familyValues["Familytemplate"] = angular.copy($scope.SelectedFileForFamily);
                    $scope.familyValues["Producttemplate"] = angular.copy($scope.SelectedFileForProduct);
                }





                $scope.selectedAttributeRow = JSON.parse(response.Data.Data);

                //for (var i = 0; i < details.length; i++) {
                //    if (details[i].FAMILY_ID == $scope.selectedAttributeRow[0].FAMILY_ID) {
                //                 //details.pop(details.pop[i]);
                //                details.splice(i, 1);
                //            }
                //        }
                $scope.famcolumnsForAtt = response.Data.Columns;
                $timeout(function () {

                    $rootScope.FamilyEdit();

                }, 2000);


            });

            blockUI.stop();
        };

        $scope.GetScrollId = function (e) {
            $scope.scrollId = e;
        }

        $scope.ClearScrollId = function () {
            $scope.scrollId = "";
        }

        $scope.setWidth = function () {
            var resizeheader = document.getElementById("resize_" + $scope.scrollId);
            var resizebody = document.getElementById("resize1_" + $scope.scrollId);
            style = window.getComputedStyle(resizeheader);
            wdt = style.getPropertyValue('width');
            resizebody.style.width = wdt;
        }

        $scope.clearImagePath = function (e) {
            //
            document.getElementById(e).value = "";
            $scope.selectedRow[e] = "";
        };




        //Filter based on Attribute pack start
        $scope.FamilyGroupNameChange = function (e) {

            $scope.SelectedFamilyGroupId = e.sender.value();
            $rootScope.LoadFamilyData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID, $scope.SelectedFamilyGroupId);

        }

        //Filter based on Attribute pack end

        // Attribute Pack start 

        $scope.familyAttributePackdataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "GROUP_ID", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    //
                    var selectAllFfamPack;
                    if ($("#selectAllFfamPack").prop("checked") == true) {
                        selectAllFfamPack = 1;
                    }
                    else if ($("#selectAllFfamPack").prop("checked") == false) {
                        selectAllFfamPack = 0;
                    }
                    dataFactory.GetFamilylevelPacks($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, options.data).success(function (response) {
                        if (selectAllFfamPack == 1) {
                            $('#selectAllFfamPack').prop('checked', false);
                        }
                        $scope.familyPackCount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });
                    } else {
                        $rootScope.items;
                        dataFactory.savecatalogallattrdetails($rootScope.selecetedCatalogId, $rootScope.items).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info",
                            });
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "GROUP_ID",
                    fields: {
                        GROUP_NAME: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });


        $scope.familylevelAttributePackOptions = {
            dataSource: $scope.familyAttributePackdataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: '<a ng-click="SaveFamilyAttributePack()" class="red k-button k-button-icontext k-grid-upload">Save</a>' },
             { name: "cancel", text: "", template: '<a ng-click="CancelFamilyAttributePackforFamily()" class="red k-button k-button-icontext k-grid-upload">Cancel</a>' }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllFfamPack' class='mc-checkbox' ng-click='selectAll($event)'/>" },

                { field: "GROUP_NAME", title: "Group Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };


        $scope.productAttributePackdataSource = new kendo.data.DataSource({
            //type: "json",
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "GROUP_ID", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllFProdpack;
                    if ($("#selectAllFProdpack").prop("checked") == true) {
                        selectAllFProdpack = 1;
                    }
                    else if ($("#selectAllFProdpack").prop("checked") == false) {
                        selectAllFProdpack = 0;
                    }
                    dataFactory.GetProductlevelPacks($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, options.data).success(function (response) {
                        if (selectAllFProdpack == 1) {
                            $('#selectAllFProdpack').prop('checked', false);
                        }
                        $scope.prodPackCount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });
                    } else {
                        $rootScope.items;
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, $rootScope.items).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info",
                            });
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "GROUP_ID",
                    fields: {
                        GROUP_NAME: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });


        $scope.productlevelAttributePackOptions = {
            dataSource: $scope.productAttributePackdataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: '<a ng-click="SaveProdAttributePack()" class="red k-button k-button-icontext k-grid-upload">Save</a>' },
             { name: "cancel", text: "", template: '<a ng-click="CancelProductAttributePackforFamily()" class="red k-button k-button-icontext k-grid-upload">Cancel</a>' }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllFProdpack' class='mc-checkbox' ng-click='selectAll1($event)'/>" },
                { field: "GROUP_NAME", title: "Group Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };

        $rootScope.items = [];
        $scope.LoadAttributePack = function () {
            // $("#AttributePackTab").show();
            $scope.familyAttributePackdataSource.read();
            $scope.productAttributePackdataSource.read();
            $timeout(function () {
                $("#AttributePackTab").show();
            }, 500);
        }

        // ********************************************************   Family attribute Pack     [START]

        var DeleteValues = [];


        $scope.updateSelection = function (e, id) {

            var grid = $(e.target).closest('[kendo-grid]').data("kendoGrid");
            var totalitems = grid.dataItems();

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);

                // logics - Start.
                $rootScope.items.push({
                    GROUP_ID: id.dataItem.GROUP_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    GROUP_NAME: id.dataItem.GROUP_NAME,
                    CATALOG_ID: $rootScope.selecetedCatalogId,
                    FAMILY_ID: $scope.Family.FAMILY_ID,
                    CATEGORY_ID: $scope.ActiveCategoryId,
                });

                var items = $rootScope.items;
                if (DeleteValues == "") {
                    DeleteValues = [];
                }

                if (e.currentTarget.checked == true) {
                    DeleteValues.push(id.dataItem.GROUP_ID);

                    if (totalitems.length == DeleteValues.length) {
                        $("#selectAllFfamPack").prop("checked", true);
                    }

                }
                if (e.currentTarget.checked == false) {
                    DeleteValues.pop();
                    $("#selectAllFfamPack").prop("checked", false);
                }

                if (DeleteValues.length == 0) {
                    $("#selectAllFfamPack").prop("checked", false);
                }

                //    End

            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };



        $scope.selectAll = function (ev) {

            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });

            if (ev.target.checked) {
                for (var i = 0; i < items.length; i++) {
                    DeleteValues.push(items[i].GROUP_ID);
                }
            } else {
                DeleteValues = [];
            }
        };


        // ********************************************************   Family attribute Pack     [END]

        // ********************************************************   Product attribute Pack     [START]

        var DeleteValues1 = [];


        $scope.updateSelection1 = function (e, id) {

            var grid = $(e.target).closest('[kendo-grid]').data("kendoGrid");
            var totalitems = grid.dataItems();

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);

                // logics - Start.
                $rootScope.items.push({
                    GROUP_ID: id.dataItem.GROUP_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    GROUP_NAME: id.dataItem.GROUP_NAME,
                    CATALOG_ID: $rootScope.selecetedCatalogId,
                    FAMILY_ID: $scope.Family.FAMILY_ID,
                    CATEGORY_ID: $scope.ActiveCategoryId,
                });

                var items = $rootScope.items;
                if (DeleteValues1 == "") {
                    DeleteValues1 = [];
                }

                if (e.currentTarget.checked == true) {
                    DeleteValues1.push(id.dataItem.GROUP_ID);

                    if (totalitems.length == DeleteValues1.length) {
                        $("#selectAllFProdpack").prop("checked", true);
                    }

                }
                if (e.currentTarget.checked == false) {
                    DeleteValues1.pop();
                    $("#selectAllFProdpack").prop("checked", false);
                }

                if (DeleteValues1.length == 0) {
                    $("#selectAllFProdpack").prop("checked", false);
                }

                //    End

            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };



        $scope.selectAll1 = function (ev) {

            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });

            if (ev.target.checked) {
                for (var i = 0; i < items.length; i++) {
                    DeleteValues1.push(items[i].GROUP_ID);
                }
            } else {
                DeleteValues1 = [];
            }
        };


        // ********************************************************   Product attribute Pack     [END]

        $scope.SaveProdAttributePack = function () {
            if ($rootScope.selecetedCatalogId === 1) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Master Catalog update is not permitted.',
                    type: "info"
                });
            } else {
                if ($scope.Family.FAMILY_ID != "" && $scope.Family.FAMILY_ID != undefined && $scope.Family.FAMILY_ID != '') {
                    if ($rootScope.items != undefined && $rootScope.items.length != 0) {
                        dataFactory.saveProdAttributePackforfamily($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $rootScope.items).success(function (response) {
                            $rootScope.items = [];
                            $('#selectAllFProdpack').prop('checked', false);
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info",
                            });
                            $scope.productAttributePackdataSource.read();
                            $rootScope.ProductGroupNameForFamily.read();
                            // options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                    else {

                    }
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please save or select product.',
                        type: "info",
                    });
                }
            }
        };

        $scope.SaveFamilyAttributePack = function () {
            if ($rootScope.selecetedCatalogId === 1) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Master Catalog update is not permitted.',
                    type: "info"
                });
            } else {
                if ($scope.Family.FAMILY_ID != "" && $scope.Family.FAMILY_ID != undefined && $scope.Family.FAMILY_ID != '') {
                    if ($rootScope.items != undefined && $rootScope.items.length != 0) {
                        dataFactory.saveFamilyAttributePackforfamily($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $rootScope.items).success(function (response) {
                            $rootScope.items = [];
                            $('#selectAllFfamPack').prop('checked', false);
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info",
                            });
                            $scope.familyAttributePackdataSource.read();
                            $rootScope.FamilyGroupNameforFamilyAssociate.read();
                            // options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });

                    }
                    else {

                    }
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please save or select product.',
                        type: "info",
                    });
                }
            }
        };

        $scope.CancelProductAttributePackforFamily = function () {
            $scope.productAttributePackdataSource.read();
        };

        $scope.CancelFamilyAttributePackforFamily = function () {
            $scope.familyAttributePackdataSource.read();
        };

        // Attribute pack end 



        //$scope.familysetWidth = function () {

        //    var resizeheader = document.getElementById("familyresize_" + $scope.scrollId);
        //    var resizebody = document.getElementById("familyresize1_" + $scope.scrollId);
        //    style = window.getComputedStyle(resizeheader);
        //    wdt = style.getPropertyValue('width');
        //    resizebody.style.width = wdt;
        //}
        $scope.tableDesignerExport = function (familyData, catalogId, categoryId) {
            dataFactory.TableDesignerExport($scope.Family.FAMILY_ID, $rootScope.selecetedCatalogId, $scope.Family.CATEGORY_ID).success(function (response) {
                if (response == "true") {
                    $scope.GetProdSpecsExport();
                }
            });
        }

        $scope.tableDesignerImportClick = function () {
            $("#divFmlyTab").hide();
            $("#tableDesignerImport").show();
            $('#tableDesignerImportss').hide();
        }

        $scope.selectFileforUploadRun_Family = function (file) {
            // var Family_Id = $scope.Family.FAMILY_ID;
            //   var Family_Name = $('#txtFAMILY_NAME').val();


            var Family_Id = $scope.Family.FAMILY_ID;
            var Family_Name = $('#txtFAMILY_NAME').val();

            if (Family_Id == undefined) {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Save the product,then select the product template.',
                    type: "error"
                });
                $scope.deleteFamilyPdf();
                return;
            }




            if (Family_Id != undefined && Family_Id != "") {
                $scope.SelectedFileForUpload = file[0];
                $scope.SelectedFileForFamily = file[0].name;
                $scope.SelectedFileForFamily = $scope.SelectedFileForFamily;
                $scope.SelectedFileForFamilyTooltip = file[0].name;
                $scope.$apply();
                $scope.$digest();
                //var Family_Name = $('familycategoryname').val();
                //var category_Name = $('txtFAMILY_NAME').val();
                //changes
                var category_Name = $rootScope.selectedCloneDetails;
                //var Family_Name = $rootScope.currentFamilyId;
                //var Family_Id = $rootScope.currentFamilyId;
                var Family_Id = $scope.Family.FAMILY_ID;
                var Family_Name = $('#txtFAMILY_NAME').val();
                $scope.$apply();
                $scope.$digest();
                var countList = 0;
                var listSelecteddata = category_Name.toString().split('~');
                var listSelected = category_Name.toString().split('~');

                angular.forEach(listSelected, function (value) {
                    angular.forEach(listSelecteddata, function (value1) {
                        if (value == value1) {
                            countList = countList + 1;
                        }
                    });
                });
                var count_List = 1;
                if (countList > listSelected.length)
                    countList = listSelected.length;
                if (listSelected[countList - 1].includes("CAT")) {
                    count_List = 1;
                } else {
                    count_List = 2;
                }

            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Save the product,then select the product template.',
                    type: "error"
                });
            }

            if (Family_Name == undefined || Family_Name == "") {
                var familyNameTextValue = $('#txtFAMILY_NAME').val();
                dataFactory.getNewFamilyID(familyNameTextValue, $scope.SelectedCatalogId, $localStorage.CategoryID).success(function (response) {
                    Family_Name = response;
                    if (response === 0) {
                        $scope.SelectedFileForFamily = null;
                        $scope.SelectedFileForFamilyTooltip = 'No file chosen';
                        $scope.SelectedFileForUpload = null;
                        var $el = $('#FamilyFile');
                        if ($el.val() != "") {
                            $el.wrap('<form>').closest('form').get(0).reset();
                            $el.unwrap();
                        }

                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please Save the product,then select the product template.',
                            type: "error"
                        });
                    }
                    else {
                        var Catalog_ID = $localStorage.getCatalogID;
                        dataFactory.setPdfXpressType("FAMILY", listSelected[0], Family_Id, "", Catalog_ID).success(function (response) {
                            dataFactory.getCatalogID($scope.SelectedCatalogId).success(function (response) {
                            });
                        });
                    }

                });
            }
            else {
                var Catalog_ID = $localStorage.getCatalogID;
                dataFactory.setPdfXpressType("FAMILY", listSelected[0], Family_Id, "", Catalog_ID).success(function (response) {
                    dataFactory.getCatalogID($scope.SelectedCatalogId).success(function (response) {
                    });
                });
            }

        };

        $scope.savePdfxpressToSession = function () {
            //var category_Name = $rootScope.selectedCloneDetails;
            var category_Name = $localStorage.CategoryID;
            var Family_Name = $rootScope.currentFamilyId;

            var countList = 0;
            var listSelecteddata = category_Name.split('~');
            var listSelected = category_Name.split('~');

            var index = category_Name.indexOf("~");
            var categoryId = category_Name.substring(0, index);
            if (categoryId == "") {
                categoryId = category_Name;
            }

            angular.forEach(listSelected, function (value) {
                angular.forEach(listSelecteddata, function (value1) {
                    if (value == value1) {
                        countList = countList + 1;
                    }
                });
            });
            var count_List = 1;
            if (categoryId.includes("CAT")) {
                count_List = 1;
            } else {
                count_List = 2;
            }

            let Catalog_id = $localStorage.getCatalogID;

            dataFactory.CheckTemplatePath_ProductItem("FAMILY", Family_Name, Catalog_id).success(function (response) {
                debugger;
                if (response == "Template found") {
                    var Catalog_ID = $localStorage.getCatalogID;
                    dataFactory.setPdfXpressType("FAMILY", categoryId, Family_Name, "", Catalog_ID).success(function (response) {
                        window.open("../Category/PdfPreviewFamily?countList=" + countList, '_blank');
                    });
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'PDFxPress Template is missing in the path. Kindly check it.',
                        type: "error"
                    });
                }

            });
           
        }

        $scope.ChechFileValid = function (file) {
            var isValid = false;
            if ($scope.SelectedFileForUpload != null) {
                if (file.name.contains(".mrt")) {
                    $scope.FileInvalidMessage = "";
                    isValid = true;
                }
                else {
                    $scope.FileInvalidMessage = "Selected file is Invalid. (only file type .mrt allowed)";
                }
            }
            else {
                $scope.FileInvalidMessage = "MRT Files required!";
            }
            $scope.IsFileValid = isValid;
        };

        //$scope.SaveFile = function () {

        //};
        $scope.openPDFPath = "";
        $scope.UploadFileFamily = function (file, saveData) {
            var formData = new FormData();
            formData.append("file", file);
            formData.append("CatalogId", $localStorage.getCatalogID)
            formData.append("TYPE", "Family");
            formData.append("ID", $scope.Family.FAMILY_ID);
            formData.append("SaveTemplateData", saveData)


            $http.post("/XpressCatalog/SaveFiles", formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function (d) {
                    $scope.openPDFPath = d;
                    //$.msgBox({
                    //    title: $localStorage.ProdcutTitle,
                    //    content: 'File upload successful.',
                    //    // type: "info"
                    //});

                    dataFactory.FindProjectType().success(function (response) {
                        if (response != null && response !== "Error") {
                            if (response.toLocaleLowerCase().contains("simple")) {
                                //     $("#opendesignerFAM").show();
                            } else {
                                //     $("#opendesignerFAM").show();
                            }
                        } else {
                            //   $("#opendesignerFAM").show();
                        }
                    }).error(function (error) {
                        //   $("#opendesignerFAM").show();
                    });
                })
                .error(function () {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'File upload failed, please try again.',
                        type: "error"
                    });

                });

            // return defer.promise;
        };
        $scope.FamilyPdfPreview = function () {
            //  $('#opendesignerFAM').show();
            var TYPE = 'FAMILY';
            var Id = $scope.Family.FAMILY_ID;
            var Catalog_id = $localStorage.getCatalogID;
            var CatalogID = Catalog_id;
           
            $scope.UploadFileFamily($scope.SelectedFileForUpload, false);
           
        // dataFactory.GetPdfXpressdefaultType(TYPE, Id, Catalog_id).success(function (response) {
            $timeout(function () {
            dataFactory.GetPdfXpressdefaultType_Family(TYPE, Id, CatalogID).success(function (response) {

                if (response != "") {
                    
                    $scope.SelectedFileForUpload = response;
                    $scope.UploadFile($scope.SelectedFileForUpload, false);
                    
                }
            });
            }, 2000);
            $scope.savePdfxpressToSession();

            $('#opendesignerFAM').hide();
            $scope.IsFormSubmitted = true;
            $scope.Message = "";
            // $scope.ChechFileValid($scope.SelectedFileForUpload);
           

        }
        $scope.closeOpentemplate = function () {
            $("#opendesignerFAM").hide();
        };

        $scope.exportFamilyPdf = function () {

            $scope.exportFileFormat = "pdf";

            if ($scope.exportFileFormat != undefined) {
                $http.post("../XpressCatalog/ExportResult?format=" + $scope.exportFileFormat).then(function (response) {
                    var windowlocation = window.location.origin;
                    var url = response.data;
                    $.ajax({
                        url: windowlocation,
                        success: function () {
                            window.open(url);
                        },
                        error: function () {
                            alert('does not exist in server location');
                        },
                    });
                });
            }
        }
        //------------------------------------ pdf Product

        $scope.selectFileforUploadRun_Product = function (file) {
            //var $el = $('#ProductFile');
            //if ($el.val() != "") {
            //    $el.wrap('<form>').closest('form').get(0).reset();
            //    $el.unwrap();
            //}
            var Family_Id = $scope.Family.FAMILY_ID;

            var Family_Name = $('#txtFAMILY_NAME').val();

            if (Family_Id == undefined) {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Save the product,then select the Item template.',
                    type: "error"
                });
                $scope.deleteProductPdf();
                return;
            }






            if (Family_Id != undefined || Family_Id != "") {
                if ($scope.currentProductId == null || $scope.currentProductId == '') {
                    $scope.currentProductId = 0;
                }
                $scope.SelectedFileForUploadProduct = file[0];
                $scope.SelectedFileForProduct = file[0].name;
                $scope.SelectedFileForProduct = $scope.SelectedFileForProduct;
                $scope.SelectedFileForProductTooltip = $scope.SelectedFileForProduct;
                $scope.$apply();
                $scope.$digest();
                var category_Id = $('#txt_CategoryID').val();
                var category_Name = $rootScope.selectedCloneDetails;
                // var Family_Id = $rootScope.currentFamilyId;
                var Family_Id = $scope.Family.FAMILY_ID;
                var Family_Name = $('#txtFAMILY_NAME').val();
                var countList = 0;
                var listSelecteddata = category_Name.toString().split('~');
                var listSelected = category_Name.toString().split('~');
                angular.forEach(listSelected, function (value) {
                    angular.forEach(listSelecteddata, function (value1) {
                        if (value == value1) {
                            countList = countList + 1;
                        }
                    });
                });

            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please Save the product,then select the item template.',
                    type: "error"
                });
            }
            if (Family_Name == undefined || Family_Name == "") {
                var familyNameTextValue = $('#txtFAMILY_NAME').val();
                dataFactory.getNewFamilyID(familyNameTextValue, $scope.SelectedCatalogId, $localStorage.CategoryID).success(function (response) {
                    Family_Name = response;
                    if (response === 0) {
                        $scope.SelectedFileForProduct = null;
                        $scope.SelectedFileForProductTooltip = 'No file chosen';
                        var $el = $('#ProductFile');
                        if ($el.val() != "") {
                            $el.wrap('<form>').closest('form').get(0).reset();
                            $el.unwrap();
                        }
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please Save the product,then select the Item template.',
                            type: "error"
                        });
                    }
                    else {
                        var Catalog_ID = $localStorage.getCatalogID;
                        dataFactory.setPdfXpressType("PRODUCT", listSelected[0], Family_Id, $scope.currentProductId, Catalog_ID).success(function (response) {
                            dataFactory.getCatalogID($scope.SelectedCatalogId).success(function (response) {

                            });
                        });
                    }

                });
            }
            else {
                var Catalog_ID = $localStorage.getCatalogID;
                dataFactory.setPdfXpressType("PRODUCT", listSelected[0], Family_Id, $scope.currentProductId, Catalog_ID).success(function (response) {
                    dataFactory.getCatalogID($scope.SelectedCatalogId).success(function (response) {

                    });
                });
            }

        };


        //Clear pdf express in default.

        $scope.deleteProductPdf = function () {
            debugger;
            var id = $scope.Family.FAMILY_ID;
            var catId = $localStorage.getCatalogID;
            $scope.SelectedFileForProduct = '';
            $scope.SelectedFileForProductTooltip = 'No file chosen';
            dataFactory.clearPdfXpressType("Product", id, catId).success(function (response) {
                // if (response == "Success") {

                $scope.SelectedFileForProduct = '';
                $scope.SelectedFileForProductTooltip = 'No file chosen';
                var $el = $('#ProductFile');
                if ($el.val() != "") {
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                }



                // }
            });


        }
        // End
        /////////////////////////////

        $scope.UploadFileProduct = function (file, saveData) {
            var formData = new FormData();
            formData.append("CatalogId", $localStorage.getCatalogID)
            formData.append("file", file);
            formData.append("TYPE", "Product");
            formData.append("SaveTemplateData", saveData);
            formData.append("ID", $scope.Family.FAMILY_ID);


            $http.post("/XpressCatalog/SaveFiles", formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function (d) {
                    $scope.openPDFPath = d;
                    //$.msgBox({
                    //    title: $localStorage.ProdcutTitle,
                    //    content: 'File upload successful.',
                    //    // type: "info"
                    //});

                    //dataFactory.FindProjectType().success(function (response) {
                    //    if (response !== "Error") {
                    //        if (response.toLocaleLowerCase().contains("simple")) {
                    //            //   $("#opendesignerProd").show();
                    //        } else {
                    //            //    $("#opendesignerProd").show();
                    //        }
                    //    } else {
                    //        //   $("#opendesignerProd").show();
                    //    }
                    //}).error(function (error) {
                    //    //     $("#opendesignerProd").show();
                    //});
                })
            //  return defer.promise;
        };


        //////////////////////////////
        //////////////////////////////

        $scope.ViewByHideProducts = function () {


            var familyId = 0;

            if ($rootScope.selecetedFamilyId == 0)
                familyId = $rootScope.currentFamilyId;
            else
                familyId = $rootScope.selecetedFamilyId



            dataFactory.viewByHideProducts($rootScope.selecetedCatalogId, familyId, $localStorage.CategoryID).success(function (response) {

                var viewByChangesButton = response.split("~");
                if (viewByChangesButton[0] == "true") {
                    if (viewByChangesButton[1] == 0) {

                        $scope.SpecificationButton = true;
                        

                        var classVal = $("#tagBtn").attr("class");
                        $("#tagBtn").addClass("active");
                        var classVal = $("#tagBtnItem").attr("class");
                        $("#tagBtnItem").addClass("active");
                    }
                    else {
                        $("#tagBtnItem").removeClass("active");
                        $("#tagBtn").removeClass("active");
                        $scope.SpecificationButton = false;
                    }

                    if (viewByChangesButton[2] == 0) {

                        $scope.ImageButton = true;
                        //mm22
                        var classVal = $("#imageBtn").attr("class");
                        $("#imageBtn").addClass("active");
                        var classVal = $("#imageBtnItem").attr("class");
                        $("#imageBtnItem").addClass("active");
                        ////mm22
                    }
                    else {
                        $("#imageBtnItem").removeClass("active");
                        $("#imageBtn").removeClass("active");
                        $scope.ImageButton = false;

                    }
                    if (viewByChangesButton[3] == 0) {

                        $scope.PriceButton = true;
                        //mm22
                        var classVal = $("#moneyBtn").attr("class");
                        $("#moneyBtn").addClass("active");
                        var classVal = $("#moneyBtnItem").attr("class");
                        $("#moneyBtnItem").addClass("active");
                        ////mm22
                    }
                    else {
                        $("#moneyBtn").removeClass("active");
                        $("#moneyBtnItem").removeClass("active");
                        $scope.PriceButton = false;

                    }
                    if (viewByChangesButton[4] == 0) {

                        $scope.KeyButton = true;
                        //mm22
                        var classVal = $("#keyBtn").attr("class");
                        $("#keyBtn").addClass("active");
                        var classVal = $("#keyBtnItem").attr("class");
                        $("#keyBtnItem").addClass("active");
                        ////mm22
                    }
                    else {
                        $("#keyBtn").removeClass("active");
                        $("#keyBtnItem").removeClass("active");
                        $scope.KeyButton = false;

                    }
                }

            });
        };

        ///Table Designer Faily Level Import
        $rootScope.SelectedFileForUploadnametabledesigner = "";

        $scope.selectFileforUpload = function (file) {
            $rootScope.SelectedFileForUploadnametabledesigner = file[0].name;
            $scope.SelectedFileForUpload = file[0];
            $scope.$apply(function () {
                $rootScope.SelectedFileForUploadnametabledesigner = file[0].name;
            });
        };

        $scope.ResetTableDesignerImport = function () {
            $rootScope.SelectedFileForUploadnametabledesigner = null;
            $scope.SelectedFileForUpload = null;
        }

        $scope.btnTableDesignerImport = function () {
            $scope.ResetTableDesignerImport();
            $("#divFmlyTab").show();
            $("#tableDesignerImport").hide();
        }

        $scope.SaveFile = function () {
            $scope.Message = "";
            if ($scope.SelectedFileForUpload !== null && $scope.SelectedFileForUpload !== undefined) {

                $scope.ChechFileValid1($scope.SelectedFileForUpload);
                $scope.UploadFile($scope.SelectedFileForUpload);

            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose a File.',
                    type: "info"
                });
            }
        };

        $scope.ChechFileValid1 = function (file) {
            var isValid = false;
            if ($scope.SelectedFileForUpload != null) {
                if (file.name.toLowerCase().contains(".xls") || file.name.toLowerCase().contains(".xlsx")) {// && file.size <= (512 * 2048)
                    $scope.FileInvalidMessage = "";
                    $scope.excelname = file.name;
                    isValid = true;
                }
                else {
                    $scope.FileInvalidMessage = "Selected file is Invalid. (only file type xls and xlsx is allowed)";
                }
            }
            else {
                $scope.FileInvalidMessage = "File required!";
            }
            $scope.IsFileValid = isValid;
        };


        $scope.UploadFile = function (file) {
            var formData = new FormData();
            formData.append("file", file);
            $http.post("/Import/SaveFiles?XMLName=" + $scope.newxmlname1 + "", formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                })
                .success(function (d) {
                    $scope.excelPath = d;
                    $scope.getSheetName();
                    $scope.FamilylevelTableDesignerImport();
                })
                .error(function () {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'File upload failed, please try again.',
                        type: "error"
                    });
                });
        };

        $scope.FamilylevelTableDesignerImport = function () {
            var excelSheetPath = $scope.excelPath;
            $('#tableDesignerImportss').show();
            $("#tableDesignerImport").hide();
        }

        $scope.BackTableDesignerImportPage = function () {
            $('#tableDesignerImportss').hide();
            $("#tableDesignerImport").show();
        }

        $scope.FinialTableDesignerImport = function () {
            dataFactory.FinishTableDesignerImport($scope.excelPath, $scope.sheetName).success(function (response) {
                return;
            });
        }

        $scope.getSheetName = function () {

            dataFactory.GetSheetName($scope.excelPath).success(function (response) {
                if (response != "false") {
                    $scope.sheetName = response;
                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Import failed please try again later',
                        type: "error"
                    });
                }

                return;
            });
        }

        $scope.$on("MyEvent", function (response) {
            $scope.SelectedFileForFamily = $localStorage.FamilyPdfXpressName;
            $scope.SelectedFileForFamilyTooltip = $localStorage.FamilyPdfXpressName;
            // $scope.SelectedFileForFamily = response;
        });


        $scope.UndoList = function (data, index) {
            $scope.index1 = index;
            $scope.fullDetils = data.familyWithoutAttributes;
            $scope.undoBaseAttributeValues = data;
            //-------------------Family and Product Template value------------------------//
            $scope.SelectedFileForFamily = data.familyWithoutAttributes.Familytemplate;
            $scope.SelectedFileForFamilyTooltip = data.familyWithoutAttributes.Familytemplate;
            if ($scope.SelectedFileForFamily == '')
                $scope.SelectedFileForFamilyTooltip = 'No file chosen';
            $scope.SelectedFileForProduct = data.familyWithoutAttributes.Producttemplate;
            $scope.SelectedFileForProductTooltip = data.familyWithoutAttributes.Producttemplate;
            if ($scope.SelectedFileForProduct == '')
                $scope.SelectedFileForProductTooltip = 'No file chosen';
            if ($scope.undoBaseAttributeValues.familyWithAttributes != '' && $scope.undoBaseAttributeValues.selectedFamilyRowAttributes != '') {
                $scope.Family = {
                    CATALOG_ID: $scope.fullDetils.CATALOG_ID,
                    FAMILY_ID: $scope.fullDetils.FAMILY_ID,
                    FAMILY_NAME: FAMILY_NAME = $scope.fullDetils.FAMILY_NAME,
                    CATEGORY_ID: $scope.fullDetils.CATEGORY_ID,
                    STATUS: $scope.fullDetils.STATUS,
                    PUBLISH: $scope.fullDetils.PUBLISH,
                    PUBLISH2PRINT: $scope.fullDetils.PUBLISH2PRINT,
                    PUBLISH2WEB: $scope.fullDetils.PUBLISH2WEB,
                    PUBLISH2PDF: $scope.fullDetils.PUBLISH2PDF,
                    PUBLISH2EXPORT: $scope.fullDetils.PUBLISH2EXPORT,
                    PUBLISH2PORTAL: $scope.fullDetils.PUBLISH2PORTAL,
                    PARENT_FAMILY_ID: $scope.fullDetils.PARENT_FAMILY_ID,
                    FAMILYIMG_NAME: $scope.fullDetils.FAMILYIMG_NAME,
                    FAMILYIMG_ATTRIBUTE: $scope.fullDetils.FAMILYIMG_ATTRIBUTE,
                    ATTRIBUTE_ID: $scope.fullDetils.ATTRIBUTE_ID,
                    OBJECT_NAME: $scope.fullDetils.OBJECT_NAME,
                    CATEGORY_NAME: $scope.fullDetils.CATEGORY_NAME
                };
                attributeWithout = [];
                $scope.selectedRow = $scope.undoBaseAttributeValues.familyWithAttributes;
                $scope.attributes = Object.keys($scope.undoBaseAttributeValues.familyWithAttributes);
                $scope.selectedFamilyRowAttributes = $scope.undoBaseAttributeValues.selectedFamilyRowAttributes;
                $scope.updateFamilyspecification = $scope.undoBaseAttributeValues.selectedFamilyRowAttributes;
                $scope.fromPageNo = 0;
                $scope.toPageNo = 15;
                angular.forEach($scope.attributes, function (value, key) {
                    if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2'))
                        attributeWithout.push(value);
                });
                if (attributeWithout.length < $scope.toPageNo) {
                    $scope.toPageNo = attributeWithout.length;
                }
                var attributestemp = [];
                for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                    attributestemp.push(attributeWithout[i]);
                }
                $scope.attributes = attributestemp;
                $scope.attrLength = $scope.attributes.length;
                $scope.FamSpecsCountPerPage = "15";
                $scope.FamSpecsCurrentPage = "1";
                $scope.loopCount = [];
                $scope.loopEditProdsCount = [];

                var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                if ((attributeWithout.length % parseInt($scope.FamSpecsCountPerPage)) > 0) {
                    loopcnt = loopcnt + 1;
                    loopEditProdsCnt = loopEditProdsCnt + 1;
                }
                $scope.FamSpecsPageCount = $scope.loopCount;
                $scope.totalSpecsFamPageCount = loopEditProdsCnt;

                for (var i = 0; i < loopcnt; i++) {
                    if (((i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)) < attributeWithout.length) {
                        $scope.loopCount.push({
                            PAGE_NO: (i + 1),
                            FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                            TO_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)
                        });
                    } else {
                        $scope.loopCount.push({
                            PAGE_NO: (i + 1),
                            FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                            TO_PAGE_NO: attributeWithout.length
                        });
                    }
                }
                var theString;
                $scope.selectedRowEdit = {};
                angular.copy($scope.undoBaseAttributeValues.familyWithAttributes, $scope.selectedRowEdit);
                for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                    if (attributeWithout[i].contains('__')) {
                        angular.forEach($scope.selectedFamilyRowAttributes, function (value, key) {
                            var theString = value.ATTRIBUTE_ID;
                            $scope.selectedFamilyRowDynamicAttributes[theString] = [];
                            $scope.selectedFamilyRowDynamicAttributes[theString] = value;

                            if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Num')) {

                                var numeric = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[0];
                                var decimal = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[1].split(')')[0];
                                var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                pattern = pattern.replace("numeric", numeric);
                                pattern = pattern.replace("decimal", decimal);
                                var reg = new RegExp(pattern);
                                var uimask = $scope.UIMask(numeric, decimal);
                                $scope.uimask[theString] = uimask;
                                $scope.attributePattern[theString] = reg;
                            } else if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                var maxlength = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(')')[0];
                                $scope.attributePattern[theString] = maxlength;
                                $scope.uimask[theString] = maxlength;
                            } else {
                                $scope.attributePattern[theString] = 524288;
                                $scope.uimask[theString] = 524288;
                            }

                        });
                    }
                }


            }


            for (var j = 0; j < $localStorage.savebaseProductAttributeRow.length; j++) {
                if ($localStorage.savebaseProductAttributeRow[j].productAttributes.FAMILY_ID == $scope.undoBaseAttributeValues.familyWithoutAttributes.FAMILY_ID) {
                    // $rootScope.undoProductFlag = "1";
                    //  $rootScope.SaveOrUpdateProductItemForUndo($localStorage.savebaseProductAttributeRow[j].productAttributes);
                    $rootScope.rowselectedEventForUndo($localStorage.savebaseProductAttributeRow[j].productAttributes.PRODUCT_ID, $localStorage.savebaseProductAttributeRow[j].productAttributes.FAMILY_ID, $localStorage.savebaseProductAttributeRow[j]);

                }
            }
            //var request = $scope.fullDetils;

            //dataFactory.UndoUpdateFamily($scope.fullDetils.CATALOG_ID, $scope.fullDetils.CATEGORY_ID, request).success(function (response) {
            //    if (response != null) {
            //        $timeout(function () {
            //            $scope.showFamily();
            //        }, 150);
            //        //  $rootScope.treeData.read();

            //    }
            //}).error(function (error) {
            //    options.error(error);
            //});

            //$rootScope.LoadFamilyData(data.CATALOG_ID, $scope.Family.FAMILY_ID, $scope.Family.CATEGORY_ID);
            //if ($scope.SelectedDataItem.ID !== "") {
            //    $scope.renderfamilyspecsgrid = true;
            //    $scope.renderfamilydescriptions = false;
            //    $scope.renderfamilyimages = false;
            //    $scope.renderrelatedfamily = false;
            //    $scope.renderclonedcategory = false;
            //    $scope.rendermultipletable = false;
            //    $rootScope.Family_Data = "Data";
            //    $scope.CategoryID = $scope.Family.CATEGORY_ID + '~' + $scope.Family.FAMILY_ID + '~!~~';
            //    dataFactory.GetDynamicFamilySpecs($scope.Family.FAMILY_ID, $scope.Family.CategoryID, data.CATALOG_ID).success(function (response) {
            //        if (response != null) {
            //            var obj = jQuery.parseJSON(response.Data.Data);
            //            if (obj.Columns.length > 0) {
            //                $scope.familySpecsData = obj.Data;
            //                $scope.columnsForFamilSpecs = obj.Columns;
            //                $scope.actiondisable = true;
            //            } else {
            //                $scope.familySpecsData = [];
            //                $scope.columnsForFamilSpecs = [];
            //                $scope.actiondisable = false;
            //            }
            //        }
            //    }).error(function (error) {
            //        options.error(error);
            //    });
            //}
            //else {
            //    $scope.renderfamilyspecsgrid = false;
            //}

            //var TYPE = 'FAMILY';
            //var Id = $scope.Family.FAMILY_ID;
            //var Catalog_id = data.CATALOG_ID;

            //dataFactory.getPdfXpressdefaultType(TYPE, Id, Catalog_id).success(function (response) {
            //    if (response != null) {
            //        $scope.SelectedFileForFamily = response.substring(0, 24);
            //        $scope.undoSelectedFileForFamily = $scope.SelectedFileForFamily;
            //    }
            //    else
            //        $scope.SelectedFileForFamily = '';

            //});
            $scope.enableUndo = true;
            $("#undoId").removeClass("first");
            $("#leftNavTreeViewKendoNavigator").data("kendoTreeView").collapse(".k-item");
            $scope.undoListFlag = "1";
            $scope.clickedCategoryId = angular.copy($rootScope.FamilyCATEGORY_ID);
            $rootScope.FamilyCATEGORY_ID = $scope.Family.CATEGORY_ID;

            // $scope.LoadFamilySpecsData();
            // $rootScope.LoadFamilyData($rootScope.selecetedCatalogId, $scope.Family.FAMILY_ID, $localStorage.CategoryID);
        }

        $scope.Undoclick = function () {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to Undo the changes to base value?",
                type: "confirm",
                buttons: [{ value: "Ok" }, { value: "Cancel" }],
                success: function (result) {
                    if (result === "Ok") {
                        $scope.index = $scope.index1;
                        $scope.undo = $scope.fullDetils;
                        $scope.modifyFlag = "0";
                        $rootScope.undoFamilyFlag = "1";
                        //family undo 
                        if ($scope.undoBaseAttributeValues.familyWithAttributes != '' && $scope.undoBaseAttributeValues.selectedFamilyRowAttributes != '') {
                            $scope.undoBeforeChangeAttributeRow = $scope.undoBaseAttributeValues.familyWithAttributes;
                            $scope.undoBeforeChangefields = $scope.undoBaseAttributeValues.familyWithoutAttributes;
                            $scope.undoBeforeChangeAttributeRow["FAMILY_NAME"] = $scope.undoBeforeChangefields.FAMILY_NAME;
                            //-------------------------------------------family product template values------------------------------------//
                            $scope.SelectedFileForFamily = $scope.undoBaseAttributeValues.familyWithoutAttributes.Familytemplate;
                            $scope.SelectedFileForFamilyTooltip = $scope.undoBaseAttributeValues.familyWithoutAttributes.Familytemplate;
                            $scope.SelectedFileForProduct = $scope.undoBaseAttributeValues.familyWithoutAttributes.Producttemplate;
                            $scope.SelectedFileForProductTooltip = $scope.undoBaseAttributeValues.familyWithoutAttributes.Producttemplate;

                            var request = $scope.undoBeforeChangeAttributeRow;
                            dataFactory.UndoUpdateFamily($scope.undoBeforeChangeAttributeRow.CATALOG_ID, $scope.undoBeforeChangefields.CATEGORY_ID, request).success(function (response) {
                                if (response != null) {
                                    $timeout(function () {
                                        $scope.showFamily();
                                    }, 150);
                                    //  $rootScope.treeData.read();

                                }
                            }).error(function (error) {
                                options.error(error);
                            });

                            $scope.Family = {
                                FAMILY_ID: $scope.undo.FAMILY_ID,
                                FAMILY_NAME: FAMILY_NAME = $scope.undo.FAMILY_NAME,
                                CATEGORY_ID: $scope.undo.CATEGORY_ID,
                                STATUS: $scope.undo.STATUS,
                                PUBLISH: $scope.undo.PUBLISH,
                                PUBLISH2PRINT: $scope.undo.PUBLISH2PRINT,
                                PUBLISH2WEB: $scope.undo.PUBLISH2WEB,
                                PUBLISH2PDF: $scope.undo.PUBLISH2PDF,
                                PUBLISH2EXPORT: $scope.undo.PUBLISH2EXPORT,
                                PUBLISH2PORTAL: $scope.undo.PUBLISH2PORTAL,
                                PARENT_FAMILY_ID: $scope.undo.PARENT_FAMILY_ID,
                                FAMILYIMG_NAME: $scope.undo.FAMILYIMG_NAME,
                                FAMILYIMG_ATTRIBUTE: $scope.undo.FAMILYIMG_ATTRIBUTE,
                                ATTRIBUTE_ID: $scope.undo.ATTRIBUTE_ID,
                                OBJECT_NAME: $scope.undo.OBJECT_NAME,
                                CATEGORY_NAME: $scope.undo.CATEGORY_NAME
                            };

                            if ($scope.SelectedFileForFamily == '' || $scope.SelectedFileForFamily == undefined) {
                                $scope.deleteFamilyPdf();
                            }
                            else if ($localStorage.undoFamilyTemplateData.length > 0) {
                                for (var i = 0; i < $localStorage.undoFamilyTemplateData.length; i++) {
                                    if (($scope.undo.FAMILY_ID.toString() == $localStorage.undoFamilyTemplateData[i].ASSIGN_TO) && ($localStorage.undoFamilyTemplateData[i].TEMPLATE_NAME == $scope.SelectedFileForFamily)) {
                                        dataFactory.undoFamilyDataSave($localStorage.undoFamilyTemplateData[i]).success(function (resFamUndo) {
                                        });
                                        break;
                                    }
                                }
                            }
                            if ($scope.SelectedFileForProduct == '' || $scope.SelectedFileForProduct == undefined) {
                                $scope.deleteProductPdf();
                            }
                            else if ($localStorage.undoProductTemplateData.length > 0) {
                                for (var i = 0; i < $localStorage.undoProductTemplateData.length; i++) {
                                    if (($scope.undo.FAMILY_ID.toString() == $localStorage.undoProductTemplateData[i].ASSIGN_TO) && ($localStorage.undoProductTemplateData[i].TEMPLATE_NAME == $scope.SelectedFileForProduct)) {
                                        dataFactory.undoFamilyDataSave($localStorage.undoProductTemplateData[i]).success(function (resFamUndo) {
                                        });
                                        break;
                                    }
                                }
                            }

                            //  $scope.SaveOrUpdateFamilSpecs($scope.undoBeforeChangeAttributeRow);
                            $scope.LoadFamilySpecsData();
                            $rootScope.LoadFamilyData($scope.undoBeforeChangeAttributeRow.CATALOG_ID, $scope.Family.FAMILY_ID, $scope.undoBeforeChangeAttributeRow.CATEGORY_ID);

                        }

                        //Undo Product value

                        for (var j = 0; j < $localStorage.savebaseProductAttributeRow.length; j++) {
                            if ($localStorage.savebaseProductAttributeRow[j].productAttributes.FAMILY_ID == $scope.undo.FAMILY_ID) {
                                $rootScope.undoProductFlag = "1";
                                $rootScope.SaveOrUpdateProductItemForUndo($localStorage.savebaseProductAttributeRow[j].productAttributes);
                                $rootScope.rowselectedEventForUndo($localStorage.savebaseProductAttributeRow[j].productAttributes.PRODUCT_ID, $localStorage.savebaseProductAttributeRow[j].productAttributes.FAMILY_ID, $localStorage.savebaseProductAttributeRow[j]);

                            }
                        }
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "Save successful.",
                            type: "info"
                        });


                        //var request = $scope.undoBeforeChangeAttributeRow;
                        //dataFactory.UndoUpdateFamily($scope.undoBeforeChangeAttributeRow.CATALOG_ID, $scope.undoBeforeChangeAttributeRow.CATEGORY_ID, request).success(function (response) {
                        //    if (response != null) {
                        //        $timeout(function () {
                        //            $scope.showFamily();
                        //        }, 150);
                        //      //  $rootScope.treeData.read();

                        //    }
                        //}).error(function (error) {
                        //    options.error(error);
                        //});
                        //$scope.Family = {
                        //    FAMILY_ID: $scope.undo.FAMILY_ID,
                        //    FAMILY_NAME: FAMILY_NAME = $scope.undo.FAMILY_NAME,
                        //    CATEGORY_ID: $scope.undo.CATEGORY_ID,
                        //    STATUS: $scope.undo.STATUS,
                        //    PUBLISH: $scope.undo.PUBLISH,
                        //    PUBLISH2PRINT: $scope.undo.PUBLISH2PRINT,
                        //    PUBLISH2WEB: $scope.undo.PUBLISH2WEB,
                        //    PUBLISH2PDF: $scope.undo.PUBLISH2PDF,
                        //    PUBLISH2EXPORT: $scope.undo.PUBLISH2EXPORT,
                        //    PUBLISH2PORTAL: $scope.undo.PUBLISH2PORTAL,
                        //    PARENT_FAMILY_ID: $scope.undo.PARENT_FAMILY_ID,
                        //    FAMILYIMG_NAME: $scope.undo.FAMILYIMG_NAME,
                        //    FAMILYIMG_ATTRIBUTE: $scope.undo.FAMILYIMG_ATTRIBUTE,
                        //    ATTRIBUTE_ID: $scope.undo.ATTRIBUTE_ID,
                        //    OBJECT_NAME: $scope.undo.OBJECT_NAME,
                        //    CATEGORY_NAME: $scope.undo.CATEGORY_NAME
                        //};
                        //  $scope.LoadFamilySpecsData();
                        //  $rootScope.LoadFamilyData($scope.undoBeforeChangeAttributeRow.CATALOG_ID, $scope.Family.FAMILY_ID, $scope.undoBeforeChangeAttributeRow.CATEGORY_ID);
                        $("#undoId").addClass("first");
                        $scope.undoClickFlag = "1";
                    }
                }
            });

        }

        $scope.removeUndoListValue = function () {
            var removeundofamily = [];
            removeundofamily = $localStorage.saveSelectedFamilyDetails;
            removeundofamily.splice($scope.removeIndex, 1);

            if ($scope.index == 0) {
                // $localStorage.saveDetails.push($scope.undo)
                $localStorage.saveDetails.push({ familyWithoutAttributes: $scope.undoBeforefamilyWithoutAttributes, familyWithAttributes: $scope.undoBeforefamilyWithAttributes, selectedFamilyRowAttributes: $scope.undoBeforeFamilyRowAttributes });
            }
            //for (var i = 0; i < details.length; i++) {
            //    if (details[i].FAMILY_ID == $scope.undoBeforeChangeAttributeRow.FAMILY_ID && details[i].FAMILY_NAME == $scope.undoBeforeChangeAttributeRow.FAMILY_NAME) {
            //        details.splice(i, 1);
            //    }
            //}
            $localStorage.saveSelectedFamilyDetails = removeundofamily;
            //details.push($scope.undoBeforeChangeAttributeRow);
            //$scope.selectedFamilyDetails = details;
        }

        $scope.btnUndoAll = function () {

        }

        $scope.OpenCategoryDefaultTab = function () {
            //------------------------------------------------------Undo flags--------------------------------------------------------//
            $rootScope.categoryUndo = true;
            $rootScope.familyUndo = false;
            $rootScope.productUndo = false;
            //------------------------------------------------------Undo flags--------------------------------------------------------//
        };

        $scope.$on("SendUndoProductGridDetails", function (ProductDetailsValue) {

            $scope.getUndoProductValue = $rootScope.ProductDetailsValue;
            $scope.getUndoProductValue["FAMILY_NAME"] = $rootScope.getClickedFamilyName;
            $scope.getbaseProductRowAttributes = $rootScope.baseProductRowAttributes;
            var productAddFlag = "0";
            var familyAddFlag = "0";

            if ($scope.detailslength > 5) {
                $localStorage.savebaseFamilyAttributeRow.splice(0, 1);
            }

            if ($localStorage.savebaseProductAttributeRow.length == 0) {
                $localStorage.savebaseProductAttributeRow.push({ productAttributes: $scope.getUndoProductValue, baseProductRowAttributes: $scope.getbaseProductRowAttributes });
            }
            else {
                for (var i = 0; i < $localStorage.savebaseProductAttributeRow.length; i++) {
                    if ($scope.getUndoProductValue.PRODUCT_ID == $localStorage.savebaseProductAttributeRow[i].productAttributes.PRODUCT_ID) {
                        productAddFlag = "1";
                    }
                }

                if (productAddFlag == "0") {
                    $localStorage.savebaseProductAttributeRow.push({ productAttributes: $scope.getUndoProductValue, baseProductRowAttributes: $scope.getbaseProductRowAttributes });
                }
            }

            if ($localStorage.savebaseFamilyAttributeRow.length == 0) {
                $localStorage.savebaseFamilyAttributeRow.push({ familyWithoutAttributes: $scope.getUndoProductValue, familyWithAttributes: '', selectedFamilyRowAttributes: '' });
                $rootScope.selectedFamilyDetails = $localStorage.savebaseFamilyAttributeRow;
            }
            else if ($localStorage.savebaseFamilyAttributeRow.length > 1) {
                for (var i = 0; i < $localStorage.savebaseFamilyAttributeRow.length; i++) {
                    if ($localStorage.savebaseFamilyAttributeRow[i].familyWithoutAttributes.FAMILY_ID == $scope.getUndoProductValue.FAMILY_ID) {
                        familyAddFlag = "1";

                    }
                }
                if (familyAddFlag == "0") {
                    $localStorage.savebaseFamilyAttributeRow.push({ familyWithoutAttributes: $scope.getUndoProductValue, familyWithAttributes: '', selectedFamilyRowAttributes: '' });
                    $rootScope.selectedFamilyDetails = $localStorage.savebaseFamilyAttributeRow;
                }

            }

            else {
                for (var j = 0; j < $localStorage.savebaseProductAttributeRow.length; j++) {
                    if ($scope.getUndoProductValue.PRODUCT_ID != $localStorage.savebaseProductAttributeRow[j].productAttributes.PRODUCT_ID) {
                        $localStorage.savebaseFamilyAttributeRow.push({ familyWithoutAttributes: $scope.getUndoProductValue, familyWithAttributes: '', selectedFamilyRowAttributes: '' });
                        $rootScope.selectedFamilyDetails = $localStorage.savebaseFamilyAttributeRow;
                    }
                }

            }


        });

        window.addEventListener('load', function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('#sampleProdgrid tbody').scroll(function (e) {
                /*
                Setting the thead left value to the negative valule of tbody.scrollLeft will make it track the movement
                of the tbody element. Setting an elements left value to that of the tbody.scrollLeft left makes it maintain
                it's relative position at the left of the table.
                */
                $('#sampleProdgrid thead').css("left", -$("#sampleProdgrid tbody").scrollLeft()); //fix the thead relative to the body scrolling
                $('#sampleProdgrid thead th:nth-child(-n+9)').css("left", $("#sampleProdgrid tbody").scrollLeft()); //fix the first cell of the header
                $('#sampleProdgrid tbody td:nth-child(-n+9)').css("left", $("#sampleProdgrid tbody").scrollLeft()); //fix the first column of tdbody
            });

        });

        $scope.$on("LoadFamilyGroupAssociate", function () {
            $rootScope.FamilyGroupNameforFamilyAssociate.read();
        });

        $scope.$on("ToClearProdFamData", function () {
            $scope.SelectedFileForFamily = "";
            $scope.SelectedFileForFamilyTooltip = 'No file chosen';
            $scope.SelectedFileForProduct = "";
            $scope.SelectedFileForProductTooltip = 'No file chosen';
        });


        ///////////--------------undo Product Only--------------------------------------------------///////////////////////////////////////////

        $scope.UndoProductclick = function () {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to Undo the changes to Item?",
                type: "confirm",
                buttons: [{ value: "Ok" }, { value: "Cancel" }],
                success: function (result) {
                    if (result === "Ok") {
                        $scope.undo = $scope.getProductDetails;
                        var index = $scope.getIndex;
                        $scope.setUndoProductFlag = 0;

                        if (index == 0) {
                            for (var i = 0; i < $localStorage.baseUndoOnlyProductDetails.length; i++) {
                                if ($scope.undo.PRODUCT_ID == $localStorage.baseUndoOnlyProductDetails[i].PRODUCT_ID) {
                                    $scope.undoProductGetProductDetails = $localStorage.baseUndoOnlyProductDetails[i];
                                    break;
                                }
                            }
                        }

                        else if (index > 0) {
                            for (var i = index - 1; i >= 0; i--) {
                                if ($scope.undo.PRODUCT_ID == $rootScope.showSelectedFamilyProductDetails[i].PRODUCT_ID) {
                                    $scope.undoProductGetProductDetails = $rootScope.showSelectedFamilyProductDetails[i];
                                    $scope.setUndoProductFlag = 1;
                                    break;
                                }
                            }

                            if ($scope.setUndoProductFlag == 0) {
                                for (var i = 0; i < $localStorage.baseUndoOnlyProductDetails.length; i++) {
                                    if ($scope.undo.PRODUCT_ID == $localStorage.baseUndoOnlyProductDetails[i].PRODUCT_ID) {
                                        $scope.undoProductGetProductDetails = $localStorage.baseUndoOnlyProductDetails[i];
                                    }
                                }
                            }
                        }
                        $rootScope.rowselectedEventForProductUndoOnly($scope.undoProductGetProductDetails["PRODUCT_ID"], $scope.undoProductGetProductDetails["FAMILY_ID"], $scope.undoProductGetProductDetails);
                        $("#undoId").addClass("first");
                    }
                }
            });

        }

        $scope.$on("SendOnlyUndoProductGridDetails", function (UndoProductDetailsValueOnly) {

            $rootScope.getOnlyUndoProductValue = $rootScope.UndoProductDetailsValueOnly;

            if ($localStorage.selectedListOnlyProductDetails.length == 0) {
                $scope.getItemValue = $rootScope.getOnlyUndoProductValue["ITEM#__OBJ__1__1__false__true"];
                $rootScope.getOnlyUndoProductValue["ItemHashValue"] = $scope.getItemValue;


                $localStorage.selectedListOnlyProductDetails.push($rootScope.getOnlyUndoProductValue);
                $rootScope.selectedProductDetails = $localStorage.selectedListOnlyProductDetails;
                $rootScope.showSelectedFamilyProductDetails = $localStorage.selectedListOnlyProductDetails;
            }
            else {
                $scope.getItemValue = $rootScope.getOnlyUndoProductValue["ITEM#__OBJ__1__1__false__true"];
                $rootScope.getOnlyUndoProductValue["ItemHashValue"] = $scope.getItemValue;


                $localStorage.selectedListOnlyProductDetails.push($rootScope.getOnlyUndoProductValue);
                if ($localStorage.selectedListOnlyProductDetails.length > 10) {
                    $localStorage.selectedListOnlyProductDetails.splice(0, 1);
                }
                $rootScope.selectedProductDetails = $localStorage.selectedListOnlyProductDetails;
                $rootScope.showSelectedFamilyProductDetails = [];
                for (var k = 0; k < $localStorage.selectedListOnlyProductDetails.length; k++) {
                    if ($rootScope.getOnlyUndoProductValue.FAMILY_ID == $localStorage.selectedListOnlyProductDetails[k].FAMILY_ID) {
                        $rootScope.showSelectedFamilyProductDetails.push($localStorage.selectedListOnlyProductDetails[k]);
                    }
                }
            }

        });


        $scope.UndoProductList = function (data, index) {
            $scope.getIndex = index;
            $scope.getProductDetails = $rootScope.showSelectedFamilyProductDetails[$scope.getIndex];
            $rootScope.undoProductFlag = "1";
            $rootScope.rowselectedEventForProductUndoOnly($scope.getProductDetails["PRODUCT_ID"], $scope.getProductDetails["FAMILY_ID"], $scope.getProductDetails);
            $scope.$apply();
            $scope.$digest();
        }

        ///////////--------------undo Product Only--------------------------------------------------///////////////////////////////////////////


        //----------------------------------------------------------New design changes for Family attribute setup page-----------------------//
        $scope.items = [];


        // logic - start
        $scope.checkPublish = function (e, id) {
            if (id.dataItem.PUBLISHED == 1) {
                id.dataItem.PUBLISHED = 0;


                id.dataItem.set("PUBLISHED", e.target.checked);
                $scope.items.push({
                    ISAvailable: true,
                    CATALOG_ID: $rootScope.selecetedCatalogId,
                    //CATEGORY_ID: $scope.ActiveCategoryId,
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    FAMILY_ID: $rootScope.currentFamilyId,
                    FLAG: "unpublish"
                });


            }

            else {



                if (id.dataItem.AVALIABLE == 0) {
                    id.dataItem.PUBLISHED = 0;
                    $("#chk_" + id.dataItem.ATTRIBUTE_ID).prop("checked", false);


                    id.dataItem.set("PUBLISHED", e.target.checked);
                    $scope.items.push({
                        ISAvailable: true,
                        CATALOG_ID: $rootScope.selecetedCatalogId,
                        //CATEGORY_ID: $scope.ActiveCategoryId,
                        ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                        FAMILY_ID: $rootScope.currentFamilyId,
                        FLAG: "unpublish"
                    });

                } else {
                    id.dataItem.PUBLISHED = 1;

                    id.dataItem.set("PUBLISHED", e.target.checked);
                    $scope.items.push({
                        ISAvailable: true,
                        CATALOG_ID: $rootScope.selecetedCatalogId,
                        //CATEGORY_ID: $scope.ActiveCategoryId,
                        ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                        FAMILY_ID: $rootScope.currentFamilyId,
                        FLAG: "publish"
                    });

                }
            }

        };



        $scope.checkAvailable = function (e, id) {
            if (id.dataItem.AVALIABLE == 1) {
                id.dataItem.AVALIABLE = 0;
                id.dataItem.PUBLISHED = 0;
                $("#chk_" + id.dataItem.ATTRIBUTE_ID).prop("checked", false);


                id.dataItem.set("PUBLISHED", e.target.checked);
                $scope.items.push({
                    ISAvailable: true,
                    CATALOG_ID: $rootScope.selecetedCatalogId,
                    //CATEGORY_ID: $scope.ActiveCategoryId,
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ATTRIBUTE_TYPE: id.dataItem.ATTRIBUTE_TYPE,
                    FAMILY_ID: $rootScope.currentFamilyId,
                    FLAG: "UnAvailable"
                });

                $scope.items.push({
                    ISAvailable: true,
                    CATALOG_ID: $rootScope.selecetedCatalogId,
                    //CATEGORY_ID: $scope.ActiveCategoryId,
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    FAMILY_ID: $rootScope.currentFamilyId,
                    FLAG: "unpublish"
                });


            }
            else {

                id.dataItem.set("PUBLISHED", e.target.checked);
                $scope.items.push({
                    ISAvailable: true,
                    CATALOG_ID: $rootScope.selecetedCatalogId,
                    //CATEGORY_ID: $scope.ActiveCategoryId,
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    FAMILY_ID: $rootScope.currentFamilyId,
                    ATTRIBUTE_TYPE: id.dataItem.ATTRIBUTE_TYPE,
                    FLAG: "Available"
                });

                id.dataItem.AVALIABLE = 1;
            }
        };


        //  LLogics End


        //$scope.checkPublish = function (e, id)
        //{
        //    if (e.target.checked) 
        //    {
        //        id.dataItem.set("PUBLISHED", e.target.checked);
        //        $scope.items.push({
        //            ISAvailable: true,
        //            CATALOG_ID: $rootScope.selecetedCatalogId,
        //            //CATEGORY_ID: $scope.ActiveCategoryId,
        //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
        //            FAMILY_ID: $rootScope.currentFamilyId,
        //            FLAG: "publish"
        //        });
        //    } else {
        //        id.dataItem.set("PUBLISHED", e.target.checked);
        //        $scope.items.push({
        //            ISAvailable: true,
        //            CATALOG_ID: $rootScope.selecetedCatalogId,
        //            //CATEGORY_ID: $scope.ActiveCategoryId,
        //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
        //            FAMILY_ID: $rootScope.currentFamilyId,
        //            FLAG: "unpublish"
        //        });
        //    }
        //};

        //$scope.uncheckPublish = function (e, id) {
        //    if (e.target.checked) {
        //        id.dataItem.set("PUBLISHED", e.target.checked);
        //        $scope.items.push({
        //            ISAvailable: true,
        //            CATALOG_ID: $rootScope.selecetedCatalogId,
        //            //CATEGORY_ID: $scope.ActiveCategoryId,
        //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
        //            FAMILY_ID: $rootScope.currentFamilyId,
        //            FLAG: "publish"
        //        });
        //    } else {
        //        id.dataItem.set("PUBLISHED", e.target.checked);
        //        $scope.items.push({
        //            ISAvailable: true,
        //            CATALOG_ID: $rootScope.selecetedCatalogId,
        //            //CATEGORY_ID: $scope.ActiveCategoryId,
        //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
        //            FAMILY_ID: $rootScope.currentFamilyId,
        //            FLAG: "unpublish"
        //        });
        //    }
        //};

        // For available attribute   - Start

        //$scope.checkPublishAvailable = function (e, id) {
        //    if (e.target.checked) {
        //        id.dataItem.set("PUBLISHED", e.target.checked);
        //        $scope.items.push({
        //            ISAvailable: true,
        //            CATALOG_ID: $rootScope.selecetedCatalogId,
        //            //CATEGORY_ID: $scope.ActiveCategoryId,
        //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
        //            FAMILY_ID: $rootScope.currentFamilyId,
        //            ATTRIBUTE_TYPE: id.dataItem.ATTRIBUTE_TYPE,
        //            FLAG: "Available"
        //        });
        //    } else {
        //        id.dataItem.set("PUBLISHED", e.target.checked);
        //        $scope.items.push({
        //            ISAvailable: true,
        //            CATALOG_ID: $rootScope.selecetedCatalogId,
        //            //CATEGORY_ID: $scope.ActiveCategoryId,
        //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
        //            ATTRIBUTE_TYPE: id.dataItem.ATTRIBUTE_TYPE,
        //            FAMILY_ID: $rootScope.currentFamilyId,
        //            FLAG: "UnAvailable"
        //        });
        //    }
        //}

        //$scope.uncheckPublishAvailable = function (e, id) {
        //    if (e.target.checked) {
        //        id.dataItem.set("PUBLISHED", e.target.checked);
        //        $scope.items.push({
        //            ISAvailable: true,
        //            CATALOG_ID: $rootScope.selecetedCatalogId,
        //            //CATEGORY_ID: $scope.ActiveCategoryId,
        //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
        //            ATTRIBUTE_TYPE: id.dataItem.ATTRIBUTE_TYPE,
        //            FAMILY_ID: $rootScope.currentFamilyId,
        //            FLAG: "Available"
        //        });
        //    } else {
        //        id.dataItem.set("PUBLISHED", e.target.checked);
        //        $scope.items.push({
        //            ISAvailable: true,
        //            CATALOG_ID: $rootScope.selecetedCatalogId,
        //            //CATEGORY_ID: $scope.ActiveCategoryId,
        //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
        //            ATTRIBUTE_TYPE: id.dataItem.ATTRIBUTE_TYPE,
        //            FAMILY_ID: $rootScope.currentFamilyId,
        //            FLAG: "UnAvailable"
        //        });
        //    }
        //}
        // For available attribute for - End
        //     Famiily Atribute Setup  - Start

        $scope.GetAllFamilyAttributesNew = new kendo.data.DataSource({
            type: "json",
            //serverFiltering: true,
            sortable: true,
            filterable: true,
            serverFiltering: false,
            // pageable: true,
            autoBind: false,
            //serverSorting: true,
            // pageSize: 5,
            sort: { field: "SORT_ORDER", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetAllFamilyAttributesNew($rootScope.selecetedCatalogId, $rootScope.currentFamilyId).success(function (response) {

                        // To chcek item#,style name is null [Mariya]
                        for (var i = 0 ; i < response.length ; i++) {

                            if (response[i]["STYLE_NAME"] == null) {
                                response[i]["STYLE_NAME"] = "";
                            }
                        }

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ISAvailable: {
                            type: "boolean"
                        },
                        CATALOG_ID: { editable: false },
                        CATEGORY_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        SORT_ORDER: { editable: false },
                        PUBLISH2PRINT: { editable: false },
                        PUBLISH2WEB: { editable: false },
                        PUBLISH2PDF: { editable: false },
                        PUBLISH2EXPORT: { editable: false },
                        PUBLISH2PORTAL: { editable: false },
                        PUBLISHED: { editable: false },
                        AVALIABLE: { editable: false },
                        STYLE_NAME: { editable: false },
                        ATTRIBUTE_DATATYPE: { editable: false }
                    }
                }
            }
        });

        //$("#newFamilyAttributeDesign .k-grid-content").css({
        //    "overflow-y": "scroll"
        //});

        $scope.newFamilyAttributeDesign = {

            dataSource: $scope.GetAllFamilyAttributesNew,
            filterable: { mode: "row" },

            //pageable: {
            //    alwaysVisible: false,
            //    pageSizes: [5, 10, 20, 100]
            //},

            scrollable: true, //editable: true, 
            // height: 329,
            autoBind: false,
            // selectable: "multiple",
            //filterable: true,
            dataBound: onDataBound,
            //serverSorting: true,
            sortable: true,
            columns:
                [
              //  { field: "AVALIABLE", title: "Avaliable", width: "9px", template: '<input type="checkbox" checked="checked"></input>' },

                { field: "AVAILABLE", title: "Select", width: "5px", template: '# if(AVALIABLE=="1") {#<input type="checkbox"  checked="checked"  ng-click="checkAvailable($event, this)"></input>#} else if(AVALIABLE=="0") {#<input type="checkbox"  ng-click="checkAvailable($event, this)"></input>#}#', filterable: false },
                { field: "PUBLISH", title: "Publish", width: "5px", template: '# if(PUBLISHED=="1") {#<input type="checkbox" id="chk_#=ATTRIBUTE_ID#" checked="checked"  ng-click="checkPublish($event, this)"></input>#} else if(PUBLISHED=="0") {#<input type="checkbox" id="chk_#=ATTRIBUTE_ID#" ng-click="checkPublish($event, this)"></input>#}#', filterable: false },


                //{ field: "AVALIABLE", title: "Select", width: "5px", template: '# if(AVALIABLE=="1") {#<input type="checkbox"  checked="checked"  ng-click="checkPublishAvailable($event, this)"></input>#} else if(AVALIABLE=="0") {#<input type="checkbox"   ng-click="uncheckPublishAvailable($event, this)"></input>#}       else {#<input type="checkbox" ng-click="avaliableToSelect($event, this)"#}#' },
                //{ field: "PUBLISHED", title: "Publish", width: "5px", template: '# if(PUBLISHED=="1" && AVALIABLE=="1") {#<input type="checkbox"  id="chk1_#=ATTRIBUTE_ID#" checked="checked"  ng-click="checkPublish($event, this)"></input>#} else if(PUBLISHED=="0" && AVALIABLE=="1") {#<input type="checkbox"  id="chk1_#=ATTRIBUTE_ID#" ng-click="uncheckPublish($event, this)"></input>#} else  if(PUBLISHED=="0" && AVALIABLE=="0") {#<input type="checkbox" id="chk1_#=ATTRIBUTE_ID#" disabled="disabled" ng-click="avaliableToSelect($event, this)"#}#' },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "20px", filterable: true, template: "<a title='#=ATTRIBUTE_NAME#' >#=ATTRIBUTE_NAME#</a>" },
                { field: "ATTRIBUTE_TYPE", title: "Type", width: "5px", template: '# if( ATTRIBUTE_TYPE=="11") {# <span title="Specification"><i class="fa fa-tags"></i><span># } else if(ATTRIBUTE_TYPE=="9") {#<span title="Image"><i class="fa fa-image"></i><span>#}  else if(ATTRIBUTE_TYPE=="12") {#<span title="Price"><i class="fa fa-dollar"></i><span>#}   else if(ATTRIBUTE_TYPE=="13") {#<span title="Key"><i class="fa fa-key"></i><span>#}      else if(ATTRIBUTE_TYPE=="7") {#<span title="Description"><i class="fa fa-life-ring"></i><span>#} #', filterable: false },
                { field: "ATTRIBUTE_DATATYPE", title: "Data Type", width: "7px" },
                { field: "STYLE_NAME", title: "Style Name", width: "10px", template: "<a title='#=STYLE_NAME#' >#=STYLE_NAME#</a>", filterable: false }
                //{ field: "PUBLISH2PRINT", title: "Print", width: "7px", template: '# if( PUBLISH2PRINT==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PRINT == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
                //{ field: "PUBLISH2WEB", title: "Web", width: "7px", template: '# if( PUBLISH2WEB==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2WEB == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
                //{ field: "PUBLISH2PDF", title: "PDF", width: "7px", template: '# if( PUBLISH2PDF==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PDF == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
                //{ field: "PUBLISH2EXPORT", title: "Export", width: "7px", template: '# if( PUBLISH2EXPORT==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2EXPORT == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
                //{ field: "PUBLISH2PORTAL", title: "Portal", width: "7px", template: '# if( PUBLISH2PORTAL==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PORTAL == false) {#<input type="checkbox" disabled="disabled"></input>#}#' }
                ],
            dataBound: function (e) {
                //debugger
                var columns = e.sender.columns;
                var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "PUBLISHED" + "]").index();
                var rows = e.sender.tbody.children();
                for (var j = 0; j < rows.length; j++) {
                    var row = $(rows[j]);
                    var dataItem = e.sender.dataItem(row);
                    var units = dataItem.get("PUBLISHED");
                    var publish = dataItem.get("PUBLISHED");
                    if (publish == 1 || publish == 0) {
                        row.addClass("selectable");
                    }
                }
            }

        };    

        $scope.sortableOptions = {
            filter: ".k-grid tr[data-uid]",
            hint: $.noop,
            cursor: "move",
            placeholder: function (element) {
                return element
                          .clone()
                          .removeAttr("uid")
                          .addClass("k-state-hover")
                          .css("opacity", 0.65);
            },
            container: ".k-grid tbody",
            change: function (e) {
                var grid = $("#newFamilyAttributeDesign").data("kendoGrid");
                //  var grid = $scope.myGrid
                dataItem = grid.dataSource.getByUid(e.item.data("uid"));

                grid.dataSource.remove(dataItem);
                grid.dataSource.insert(e.newIndex, dataItem);
                $scope.UnPublishFamilyAttributesSorting();
            }
        };

        $rootScope.LoaAttributeDataFamily = function () {
            $scope.GetAllFamilyAttributesNew.read();
        };

        $scope.displayHtmlPreview = function (e) {
            var gettextareaName = "textBoxValue" + e;
            $scope.gettextareaValue = $("textarea[name=" + gettextareaName + "]").val();
        };
        //$scope.showproductgrid = function () {
        //    debugger;
        //    if ($(".testfield").css("display") == "block")
        //    {
        //        $(".testfield").css("display", "none");
        //    }
        //    else
        //    {
        //        $(".testfield").css("display", "block");
        //    }
            
        //}

    }]);