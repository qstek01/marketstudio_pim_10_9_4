﻿'use strict';
LSApp.register.controller('appRolesController', ['$scope', '$window', '$routeParams', 'dataFactory', function ($scope, $window, $routeParams, dataFactory) {

    $scope.appRolesDatasource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                dataFactory.getAllRoles(options.data).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            }
        },      
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        pageSize: 5        
    });

    $scope.appRolesGridOptions = {
        dataSource: $scope.appRolesDatasource,
        autoBind: false,
        sortable: false,
        filterable: false,
        pageable: false,
        columns: [
            { field: "RoleName", title: "Role Name" }
            //{ title: "", template: "<a title='Edit' kendo-tooltip href='javascript:void(0);' ng-click='edit(this.dataItem)' class='glyphicon glyphicon-edit'></a>", width: "60px" }
        ]
    };


    $scope.init = function () {
        $scope.showListView = true;
        $scope.screenName = "Application Roles";
        $scope.addOrEditMode = "Add";
        //$scope.selectedRow = {};
        $scope.appRolesDatasource.read();
    };

    $scope.init();
}]);
