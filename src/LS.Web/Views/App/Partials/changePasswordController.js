﻿'use strict';
LSApp.register.controller('changePasswordController', ['$scope', '$window', '$routeParams', 'dataFactory', '$rootScope', '$localStorage', function ($scope, $window, $routeParams, dataFactory,$rootScope, $location, $localStorage) {
    
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

    $scope.save = function () {
        if ($scope.changePassword.$valid == false) {            
            $scope.formSubmitted = true;
            return;
        }
        if ($scope.selectedRow.NewPassword === $scope.selectedRow.ConfirmPassword) {
            dataFactory.changePassword($scope.selectedRow).success(function (response) {               
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + JSON.parse(response)+'.',
                    type: "info"
                });
                $scope.cancel();
                $scope.init();
            })
            .error(function (response) {
               // console.log(response);
            });
        }
        else {         
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Entered Password & Confirm Password do not match.',
                type: "error"
            });

            return;
        }
    };

    $scope.reset = function () {
        $scope.changePassword.$setPristine(); //clears form
        $scope.changePassword.$dirty = false;
        $scope.formSubmitted = false;
        $scope.selectedRow = {};
    };
    var baseUrl = $("base").first().attr("href");
    $scope.cancel = function () {
        $window.location.href = baseUrl;
    };

    $scope.init = function () {
        $scope.selectedRow = {};
    };

    $scope.init();
}]);