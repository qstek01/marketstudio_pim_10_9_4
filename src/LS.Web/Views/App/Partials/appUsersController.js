﻿'use strict';
LSApp.register.controller('appUsersController', ['$scope', '$window', '$routeParams', 'dataFactory', '$localStorage', function ($scope, $window, $routeParams, dataFactory, $localStorage) {

    $scope.pno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    $scope.regExPostalCode = /^[a-zA-Z0-9]{5,6}$/; // /^[0-9]{5,6}(?:-[0-9]{4})?$/;
    $scope.appUsersDatasource = new kendo.data.DataSource({
        transport: {
            read: function (options) {
                if ($scope.customerId > 0) {
                    if (!options.data.filter) {
                        var filter = { logic: "and", filters: [] };
                        filter.filters.push({ field: "CustomerId", operator: "eq", value: $scope.customerId });
                        options.data.filter = filter;
                    }
                }
                dataFactory.getUsers(options.data).success(function (response) {
                    options.success(response);
                }).error(function (response) {
                    options.success(response);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        },
        schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "UserId",
                fields: {
                    UserId: { type: "number" },
                    CustomerId: { type: "number" },
                    UserName: { type: "string" },
                    CustomerName: { type: "string" },
                    FullName: { type: "string" },
                    Title: { type: "string" },
                    FullAddress: { type: "string" },
                    Phone: { type: "string" },
                    Email: { type: "string" },
                    UserRole: { type: "string" },
                    Active: { type: "bool" }
                }
            }
        },
        serverPaging: false,
        serverSorting: false,
        serverFiltering: true,
        pageSize: 5,
        sort: {
            field: "UserId",
            dir: "asc"
        }
    });

    $scope.appUsersGridOptions = {
        dataSource: $scope.appUsersDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 },
        columns: [{ field: "UserId", title: "UserId", hidden: true },
            { field: "CustomerId", title: "CustomerId", hidden: true },
            { field: "UserName", title: "User Login Name", width: "100px" },
            { field: "CustomerName", title: "Customer Name", width: "100px" },
            { field: "FullName", title: "Name", width: "100px" },
            { field: "Title", title: "Title", width: "100px" },
            { field: "FullAddress", title: "Address", width: "100px" },
            { field: "Phone", title: "Phone", width: "100px" },
            { field: "Email", title: "Email", width: "100px" },
            { field: "UserRole", title: "UserRole", width: "100px" },
            { field: "Active", title: "IsActive", width: "70px", template: "<input type='checkbox' title='Edit' kendo-tooltip  #= Active ? 'checked=checked' : ''# disabled='disabled' class='checkbox'/>" },
            { title: "", template: "<a title='Edit' kendo-tooltip href='javascript:void(0);' ng-click='edit(this.dataItem)' class='glyphicon glyphicon-edit'></a>", width: "60px" }
        ]
    };
    
    $scope.add = function () {
        dataFactory.getAllRoles().success(function (response) {
            $scope.userRoleDatasource = response;                        
            $scope.showListView = false;
            $scope.addOrEditMode = "Add";
            $scope.IsAdd = true;
            $scope.selectedRow = {
                UserRoles: []
            };
        }).error(function (response) {
            //console.log(response);
        });
    };

    $scope.edit = function (dataItem) {
        dataFactory.getAllRoles().success(function (response) {
            $scope.userRoleDatasource = response;            
            $scope.showListView = false;
            $scope.addOrEditMode = "Edit";
            $scope.IsAdd = false;
            $scope.selectedRow = angular.copy(dataItem);
        }).error(function (response) {           
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + response + '.',
                type: "info"
            });
        });
    };


    $scope.toggleSelection = function toggleSelection(item) {
        var idx = $scope.selectedRow.UserRoles.indexOf(item);

        //For Radio Button - One User has one role - starts here
        $scope.selectedRow.UserRoles = new Array();
        $scope.selectedRow.UserRoles.push(item);
        //For Radio Button - One User has one role - ends here

        //For Checkbox Button - One User has many role - starts here
        // is currently selected
        //if (idx > -1) {
        //    $scope.selectedRow.UserRoles.splice(idx, 1);
        //}

        //    // is newly selected
        //else {
        //    $scope.selectedRow.UserRoles.push(item);
        //}
        //For Checkbox Button - One User has many role - ends here
    };

    $scope.save = function () {
        if ($scope.addEditAppUser.$valid == false) {
            if ($scope.selectedRow.UserRoles.length == 0) {             
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select role.',
                    //type: "info"
                });
            }
            $scope.formSubmitted = true;
            return;
        }
        if ($scope.IsAdd) {
            if ($scope.selectedRow.Password === $scope.selectedRow.ConfirmPassword) {
                dataFactory.addAppUser($scope.selectedRow).success(function (response) {                  
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + JSON.parse(response) + '.',
                        type: "info"
                    });
                    $scope.reset();
                    $scope.init();
                })
                .error(function (response) {
                    //console.log(response);
                });
            }
            else {              
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Entered Password & Confirm Password do not match.',
                    type: "error"
                });
                return;
            }
        }
        else {            
            dataFactory.updateAppUser($scope.selectedRow).success(function (response) {              
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + JSON.parse(response) + '.',
                    type: "info"
                });
                $scope.reset();
                $scope.init();
            })
            .error(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + response + '.',
                    type: "error"
                });
            });
        }
    };

    $scope.reset = function () {
        $scope.addEditAppUser.$setPristine(); //clears form
        $scope.addEditAppUser.$dirty = false;
        $scope.formSubmitted = false;
        $scope.selectedRow = {};
        $scope.screenName = "Application Users";
        $scope.addOrEditMode = "Add";
        $scope.IsAdd = true;

    };

    $scope.cancel = function () {
        $scope.showListView = true;
    };

    $scope.getCountries = function () {
        dataFactory.getCountries().success(function (response) {
            if (response) {
                $scope.countriesDataSource = response;
            }
        })
        .error(function (error) {
            //console.log('Unable to Get Countries');
        });
    };

    $scope.init = function () {
        $scope.customerId = parseInt($routeParams.customerId);
        $scope.showListView = true;
        $scope.screenName = "Users";
        $scope.addOrEditMode = "Add";
        $scope.IsAdd = true;
        $scope.selectedRow = {};
        $scope.appUsersDatasource.read();
        $scope.getCountries();
    };

    $scope.init();
}]);