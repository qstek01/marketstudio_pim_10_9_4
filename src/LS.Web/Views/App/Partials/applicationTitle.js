﻿LSApp.controller('applicationTitle', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', '$rootScope', '$localStorage', '$sce',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, $rootScope, $localStorage, $sce) {


        $scope.categoryTab = "Go To Category";
        $scope.familyTab = "Go To Product";
        $scope.productsTab = "Go To Items";
        //------------------------------------------Family Tab----------------------------------------------------
        //----------------Menu----------------------------------------------------------------------
        $scope.new = "Create a new Product/SubProduct";
        $scope.newFamily = "Create a new Product";
        $scope.newSubFamily = "Create a new SubProduct";
        $scope.familyPreview = "Product Preview";
        $scope.saveFamily = "Save Product";
        $scope.detachFamily = "Detach Product";
        $scope.deleteFamily = "Delete Product";
        $scope.familyAttributeSetUp = "Product Attributes setup";
        $scope.tableDesignerLayout = "Default Layout";
        $scope.createNewTableLayout = "Create a new Table Layout";
        $scope.deleteTableLayout = "Delete Table Layout";
        $scope.setDefaultLayout = "Set default Table Layout";
        $scope.familyTableDesigner = "Manage Product Table Layout";
        ////------------------Subtab Menu------------------------------------------------------------
        $scope.familySpecs = "Go To Product Specification";
        $scope.familyDescription = "Go To Product Description";
        $scope.relatedFamilies = "Go To Related Products";
        $scope.familyClones = "Go To Product Clones";
        $scope.familyImageAttach = "Go To Product Images & Attachments";
        $scope.familyAttributeGroups = "Go To Product Multiple Tables";
        //-----------------------Sub tab Buttons-----------------------------------------------------
        $scope.saveFamilyDescription = "Save Product Description";
        $scope.saveFamilyImage = "Save Product Image";
        $scope.clearFamilyImageAttach = "Clear Product Image Attachment";
        //--------------------------Multiple Table---------------------------------------------------
        $scope.multipleAttributeGroupingSetUp = "Manage Attribute Pack";
        $scope.multipleTableDesigner = "Manage Multiple Tables Designer";
        $scope.multipleTableRefresh = "Refresh Multiple Tables";
        $scope.multipleTableRemoveGroup = "Remove Multiple Tables";
        $scope.multipleTableEditGroup = "Edit Multiple Tables";
        $scope.multipleTableSaveGroup = "Save Multiple Tables";
        //--------------------------Multiple Table Designer------------------------------------------
        $scope.multipleTableSuperColumn = "Add a Super Column";
        $scope.multipleTablePreviewTD = "Preview Table Designer";
        $scope.multipleTableSaveTD = "Save Table Designer";
        $scope.multipleTableBackToGroup = "Back to Attribute Groups";
        //--------------------------Multiple Table edit------------------------------------------
        $scope.saveMultipleTableSpecs = "Save Multiple Table Specification";
        $scope.cancelMultipleTableSpecs = "Cancel Multiple Table Specification";
        //--------------------------Preview-----------------------------------------------------------
        $scope.familyPrint = "Print Product ";
        $scope.backToFamily = "Go Back to Product Page";
        //--------------------------Create Table Layout-----------------------------------------------
        $scope.createFamilyTableLayoutCreation = "Create Table Layout";
        $scope.cancelFamilyTableLayoutCreation = "Cancel Table Layout creation";
        //--------------------------Delete Table Layout-----------------------------------------------
        $scope.deleteFamilyTableLayoutDeletion = "Delete Product Table Structure";
        $scope.cancelFamilyTableLayoutDeletion = "Cancel Product Table Structure";
        //--------------------------Set Default Table Layout-----------------------------------------
        $scope.setDefaultFamilyTableLayout = "Set Default Table Layout Structure";
        $scope.cancelFamilyTableLayout = "Cancel Default Table Layout Structure";
        //--------------------------Family Table Designer--------------------------------------------
        $scope.familyTDSuperColumn = "Add a Super Column";
        $scope.familyTDPreview = "Preview Table Designer";
        $scope.familyTDSave = "Save Table Designer";
        $scope.familyTDBackGroup = "Back to Product";
        //--------------------------Products --------------------------------------------------------
        $scope.newProduct = "Create a new item";
        $scope.cutProducts = "Cut selected item(s)";
        $scope.copyProducts = "Copy selected item(s)";
        $scope.pasteProducts = "Paste selected item(s)";
        $scope.updateCommonValue = "Update common values for selected item";
        $scope.productsAttributeSetUp = "Item Attributes Setup";
        $scope.productsImport = "Import Item from a Spreadsheet";
        $scope.productsExport = "Export Item to a Spreadsheet";
        $scope.mainProductsTab = "Go To Main Item";
        $scope.subProductsTab = "Go To Sub Item";
        //------------------------- Products Import-----------------------------------------
        $scope.backToProducts = "Back to Items";
        $scope.loadImportFile = "Load Spreadsheet";
        $scope.importProducts = "Import Items from a Spreadsheet";
        //-------------------------Main Menu------------------------------------------------
        $scope.dashboard = "Dashboard";
        $scope.manageCatalogs = "Manage Catalogs";
        $scope.catalog = "Go To Catalog Setup";
        $scope.categories = "Go To Categories/Families/Item";
        $scope.invertedProducts = "Go To Item/SubProducts";
        $scope.attributes = "Go To Attributes Management";
        $scope.pickList = "Go To Picklists Management";
        $scope.suppliers = "Go To Suppliers Management";
        $scope.reference = "Go To Reference Tables";
        $scope.workflow = "Go To WorkFlow Management";
        $scope.publishCatalogs = "Publish Catalogs";
        $scope.portalCatalogs = "PDF Portal and Online Catalogs";
        $scope.import = "Import Data from a Spreadsheet";
        $scope.export = "Export Data to a Spreadsheet";
        $scope.pdfPublishing = "PDF Catalog Publishing";
        $scope.indesign = "Adobe InDesign Publishing";
        $scope.removeProject = "Remove a Project";
        $scope.wizard = "Attribute and Table Manager Wizard";
        $scope.globalAttributeWizard = "Manage Global Attributes";
        $scope.productTableManager = "Manage Product Tables";
        $scope.assets = "Manage Assets";
        $scope.resources = "MarketStudio Resources";
        $scope.reports = "Generate Reports";
        $scope.missingImageList = "Generate Missing Images List";
        $scope.catalogReports = "Generate Catalog Reports";
        $scope.summary = "Summary of all Catalogs";
        $scope.search = "Search using Keywords";
        $scope.ImageManage = "Goto File Managememt";
        $scope.logOff = "Sign Out";
        $scope.settings = "Application Settings";
        $scope.admin = "Application Administration";
        $scope.superAdmin = "Application Super Administration";
        $scope.preferences = "Set Application Preferences";
        $scope.recycleBin = "Recycle Bin";

        //Report Menus

        $scope.SupplierListName = "Supplier List";
        $scope.Familycreatedinlast7days = "Product created in last 7 days";
        $scope.Productcreatedinlast7days = "Item created in last 7 days";
        $scope.DuplicateItemnumber = "Duplicate Item number";
        //-----------------------------Catalog -------------------------------------------------------
        $scope.newCatalog = "Create a new Catalog";
        $scope.saveCatalog = "Save Catalog";
        $scope.deleteCatalog = "Delete Catalog and its Attributes";
        $scope.clearCatalog = "Cancel Catalog creation."
        $scope.catalogAttributes = "View Catalog Attributes";
        $scope.catalogAttributesTab = "View Catalog Attributes";
        $scope.catalogSortTab = "Sort Catalog Attributes";
        $scope.catalogFilterTab = "Filter Catalog Attributes";
        //----------------------------Catalog Sort-------------------------------------------------------
        $scope.categorySortTab = "Sort Catalog Attributes by Category";
        $scope.familySortTab = "Sort Catalog Attributes by Product";
        $scope.productsSortTab = "Sort Catalog Attributes by Items";
        $scope.sortCategories = "Sort Categories based on selection made above";
        $scope.sortFamilies = "Sort Products based on selection made above";
        $scope.sortProducts = "Sort Items based on selection made above";
        //----------------------------Catalog Filter-------------------------------------------------------
        $scope.familyFilterTab = "Filter Catalog by Product Attributes";
        $scope.productFilterTab = "Filter Catalog by Item Attributes";
        $scope.addFamilyFilter = "Add Product Filter";
        $scope.saveFamilyFilter = "Save Product Filter";
        $scope.removeFamilyFilter = "To Remove Product Filter";
        $scope.addProductFilter = "Add Item Filter";
        $scope.saveProductFilter = "Save Item Filter";
        $scope.removeProductFilter = "Remove Item Filter";
        //----------------------------Family Attribute Set up-------------------------------------------------------
        $scope.newFamilyAttributeSetup = "Add a New Product Attribute";
        $scope.saveFamilyAttributeSetup = "Save and Close";
        $scope.refreshFamilyAttributeSetup = "Refresh Product Attribute Management";
        $scope.cancelFamilyAttributeSetup = "Cancel Product Attribute Management";
        $scope.selectFamilyAttributes = "Select Product Attribute (s)";
        $scope.unselectFamilyAttributes = "UnSelect Product Attribute (s)";
        $scope.moveFamilyAttributesUp = "Move Attributes Up";
        $scope.moveFamilyAttributesDown = "Move Attributes Down";
        //----------------------------Product Attribute Set up-------------------------------------------------------
        $scope.newProductAttributeSetup = "Add a new Item Attribute";
        $scope.saveProductAttributeSetup = "Save and Close";
        $scope.refreshProductAttributeSetup = "Refresh Item Attribute Management";
        $scope.cancelProductAttributeSetup = "Cancel Item Attribute Management";
        $scope.selectProductAttributes = "Select Item Attribute (s)";
        $scope.unselectProductAttributes = "UnSelect Item Attribute (s)";
        $scope.moveProductAttributesUp = "Move Attributes Up";
        $scope.moveProductAttributesDown = "Move Attributes Down";
        //----------------------------Attribute-------------------------------------------------------
        $scope.addNewAttribute = "Add a New Attribute";
        $scope.saveNewAttribute = "Save a New Attribute";
        $scope.manageAttribute = "Manage Attributes";
        $scope.addPicklist = "Add Picklist Values";
        //----------------------------PickList-------------------------------------------------------
        $scope.addNewPickList = "Save a New Picklist";
        $scope.deletePickList = "Delete Picklist";
        //----------------------------Reference Table-------------------------------------------------------
        $scope.referenceTableMgmt = "Go To Reference Table Management";
        $scope.referenceTableValues = "Go To Reference Table Values";
        $scope.insertRow = "Insert a New Row in Reference Table";
        $scope.insertColumn = "Insert a New Column in Reference Table";
        $scope.saveReferenceTable = "Save Reference Table";
        $scope.refreshReferenceTable = "Refresh Reference Table";
        $scope.removeRow = "Remove Selected Row in Reference Table";
        $scope.removeColumn = "Remove Selected Column in Reference Table";
        $scope.saveReferenceTableMgmt = "Save Reference Table";
        $scope.refreshReferenceTableMgmt = "Refresh Reference Table";
        //----------------------------Supplier-------------------------------------------------------
        $scope.addNewSupplier = "Add a New Supplier";
        $scope.exportSupplier = "Export Supplier";
        $scope.refreshSupplierList = "Refresh Suppliers List";
        $scope.saveNewSupplier = "Save New Supplier";
        $scope.backToSupplierList = "Back to Suppliers List";
       
        //----------------------------Category-------------------------------------------------------
        $scope.saveCategory = "Save Category";
        $scope.previewCategory = "Preview Category";
        $scope.detachCategory = "Detach Category";
        $scope.deleteCategory = "Delete Category";
        $scope.clearCategoryImage = "Clear Category Image";
        //----------------------------Sub Products-------------------------------------------------------
        $scope.newSubProducts = "Create a new subproduct";
        $scope.cutSubProducts = "Cut selected SubProduct (s)";
        $scope.copySubProducts = "Copy selected SubProduct (s)";
        $scope.pasteSubProducts = "Paste selected SubProduct (s)";
        $scope.updateCommonValueSubProducts = "Update Common Values for select SubProducts";
        $scope.subProductsAttributeSetUp = "SubProducts Attributes Setup";
        //----------------------------Edit products and subproducts---------------------------------------
        $scope.searchProductSpecs = "Search using Keywords";
        $scope.saveProductSpecs = "Update Item Specifications";
        $scope.cancelProductSpecs = "Cancel Item Specifications";
        $scope.saveSubProductSpecs = "Save Item Specifications";
        $scope.cancelSubProductSpecs = "Cancel Item Specifications";
        //----------------------------Indesign ---------------------------------------
        $scope.newIndesignProject = "Create a New InDesign Project";
        $scope.showNavigator = "Show/Hide Navigator";
        $scope.AddFamiliestoIndesign = "Add Products for XML Generation";
        $scope.ExportFamiliestoIndesign = "Export";
        $scope.saveXmlFile = "Save XML File";
        $scope.cancelXml = "Cancel XML Generation";
        $scope.IndesignValidate = "Validate";
        $scope.IndesignReset = "Reset";
        $scope.IndesignCancel = "Cancel";
        $scope.Import = "Import";
        $scope.Back = "Back previous step";
        $scope.ImportProject = "Import";
        //----------------------------Indesign Tab--------------------------------------------------
        $scope.newProjectTab = "Create a New InDesign Project";
        $scope.openNewProject = "Open InDesign Project";
        //----------------------------PDF Xpress----------------------------------------------------
        $scope.newPDFProject = "Create a New PDF Project";
        $scope.designPDFTemplate = "Go To Design PDF Template";
        $scope.openPDFTemplate = "Go To Open PDF Template";
        $scope.runPDFTemplate = "Go To Run PDF Template";
        $scope.openCatalogPDF = "Go To Open PDF Catalog";
        $scope.downloadPDFTemplate = "Go To Download PDF Template";
        $scope.nextPDF = "Goto next page";
        $scope.cancelPDF = "Cancel PDFxPress";
        $scope.previousPDF = "Back to Previous Step";
        $scope.finishPDF = "Start PDFxPress template creation";
        //----------------------------Products / Subproducts Common Values----------------------------------------------------
        $scope.saveProductCommonValue = "Update Common Values for select Attribute";
        $scope.cancelProductCommonValue = "Cancel Common Values Update";
        $scope.saveSubProductCommonValue = "Update Common Values for select Attribute";
        $scope.cancelSubProductCommonValue = "Cancel Common Values Update";
        //----------------------------SubProduct Attribute Set up-------------------------------------------------------
        $scope.newSubProductAttributeSetup = "Add a new SubProduct Attribute";
        $scope.saveSubProductAttributeSetup = "Save and Close";
        $scope.refreshSubProductAttributeSetup = "Refresh SubProduct Attribute Management";
        $scope.cancelSubProductAttributeSetup = "Cancel SubProduct Attribute Management";
        $scope.selectSubProductAttributes = "Select SubProduct Attribute (s)";
        $scope.unselectSubProductAttributes = "UnSelect SubProduct Attribute (s)";
        $scope.moveSubProductAttributesUp = "Move Attributes Up";
        $scope.moveSubProductAttributesDown = "Move Attributes Down";
        //----------------------------Search-------------------------------------------------------
        $scope.searchItem = "Search using Keywords";
        $scope.cancelSearchItem = "Cancel Search Items";
        $scope.Refresh = "Refresh";
        $scope.backToResults = "Back To Search";
        $scope.resetSearchItemValue = "Reset Search Criteria";
        //-----------------------------Export--------------------------------------------------------------------------------
        $scope.exportHover = "Export Items to a Spreadsheet";
        $scope.exportOpenTemplate = "Open Export Template";
        $scope.exportDeleteTemplate = "Delete Import Template";
        $scope.exportLoad = "Load Spreadsheet";
        $scope.exportNext = "Go to Next Step";
        $scope.exportProduct = "Export Items to a Spreadsheet";
        $scope.exportBack = "Back to Previous Step";
        //-----------------------------QuickImport--------------------------------------------------------------------------------
        $scope.quickImportHover = "Quick Import Items from a Spreadsheet";
        $scope.quickImportOpenTemplate = "Open Quick Import Template";
        $scope.quickImportLoadFile = "Load Spreadsheet";
        $scope.quickImportFile = "Do a Quick Import from a Spreadsheet";
        $scope.quickImportSave = "Save Quick Import Template";
        //-----------------------------BulkImport--------------------------------------------------------------------------------
        $scope.bulkImportHover = "Bulk Import Items from a Spreadsheet";
        $scope.bulkImportOpenTemplate = "Open Bulk Import Template";
        $scope.bulkImportLoadFile = "Load Spreadsheet";
        $scope.bulkImport = "Do a Bulk Import from Spreadsheet";
        $scope.bulkImportSaveTemplate = "Save Bulk Import Template";
        //-----------------------------SubProductImport--------------------------------------------------------------------------------
        $scope.subProductImportHover = "Import SubProducts from a Spreadsheet";
        $scope.subproductOpenTemplate = "Open SubProducts Import Template";
        $scope.subProductLoadFile = "Load Spreadsheet";
        $scope.subProductImportFile = "Import  SubProducts from a Spreadsheet";
        $scope.subProductSaveTemplate = "Save SubProducts Import Template";
        $scope.subProductImport = "Import SubProducts from a Spreadsheet";
        //----------------------------Global Attribute Manager Tab-------------------------------------------------------
        $scope.globalAttributeAssociation = "Go To Attribute Association";
        $scope.globalColumnSorter = "Go To Column Sorter";
        $scope.globalCalculatedAttributes = "Go To Calculated Attributes";
        $scope.globalAttributeGrouping = "Product / Item Group";
        $scope.globalAttributeValue = "Go To Attribute Value";
        //----------------------------Attribute Association-------------------------------------------------------
        $scope.checkAllAttribute = "Check All Attributes to Associate";
        $scope.uncheckAllAttribute = "UnCheck All Attributes";
        $scope.saveProductFamilies = "Associate Selected Attribute(s) to Item Products";
        $scope.refreshProductFamilies = "Refresh Item Products";
        $scope.cancelProductFamilies = "Cancel Item Products Associate";
        $scope.saveSubProductFamilies = "Associate Selected Attribute(s) to SubProduct Products";
        $scope.refreshSubProductFamilies = "Refresh SubProduct Products";
        $scope.cancelSubProductFamilies = "Cancel SubProduct Products Associate";
        //----------------------------Column Sort-------------------------------------------------------
        $scope.saveColumnSortProductFamilies = "Column Sort for Select Attribute in Item Products";
        $scope.refreshColumnSortProductFamilies = "Refresh Item Products";
        $scope.cancelColumnSortProductFamilies = "Cancel Item Products Column Sort";
        $scope.saveColumnSortSubProductFamilies = "Column Sort for Select Attribute in SubProduct Products";
        $scope.refreshColumnSortSubProductFamilies = "Refresh SubProduct Products";
        $scope.cancelColumnSortSubProductFamilies = "Cancel SubProduct Products Column Sort";
        //----------------------------Calculated Attributes-------------------------------------------------------
        $scope.saveCalculateAttributes = "Execute Calculated Attributes for Item Products";
        $scope.refreshCalculateAttributes = "Refresh Product Products";
        $scope.cancelCalculateAttributes = "Cancel Calculated Attribute for Item Products";
        //----------------------------Attribute value-------------------------------------------------------
        $scope.processProductFamilies = "Apply Attribute Value for Select Item Products";
        $scope.processSubProductFamilies = "Apply Attribute Value for Select SubProduct Products";
        //----------------------------Product Table Manager-------------------------------------------------------
        $scope.nextPTM = "Go to Next Step";
        $scope.backPTM = "Back to Previous Step";
        $scope.finishPTM = "Execute";
        $scope.previewFamilyPTM = "Preview Products";
        //----------------------------Preferences-------------------------------------------------------
        $scope.applicationSettings = "Go To Application Settings";
        $scope.userSettings = "Go To User Settings";
        $scope.workflowStatus = "Go To WorkFlow Status";
        //----------------------------Application settings-------------------------------------------------------
        $scope.saveApplicationSettings = "Save Application Settings";
        //----------------------------User settings--------------------------------------------------------------
        $scope.saveUserSettings = "Save User Settings";
        //----------------------------Recycle Bin----------------------------------------------------------------
        $scope.restoreSelected = "Restore Items from Recycle Bin";
        $scope.removeItems = "Remove Items permanently from Recycle Bin";
        $scope.emptyItemsRecycleBin = "Remove all Items from Recycle Bin";
        $scope.refreshBin = "Refresh Recycle Bin";
        $scope.filterRecycleBin = "Apply Filter for selected Items";
        //----------------------------User Administration------------------------------------------------------
        $scope.profileAdmin = "Go To My Profile";
        $scope.usersAdmin = "Go To Users";
        $scope.rolesAdmin = "Go To Roles";
        $scope.workflowAdmin = "Go To WorkFlow";
        $scope.inviteAdmin = "Go To Invite";
        $scope.newuser = "Add a New User";
        $scope.saveNewuser = "Save New User with Details";
        $scope.cancelNewuser = "Cancel New User Creation";
        $scope.addNewRole = "Add a New Role";
        $scope.saveNewRole = "Save New Role";
        $scope.backRole = "Back To Previous Step";

        $scope.removeRoleValue = "Remove Role";
        $scope.userWorkFlow = "Save User WorkFlow Status";

        $scope.removeRole = "Remove Role";
        $scope.userWorkFlowDetail = "Save User WorkFlow Status";

        $scope.userInvitee = "Add a New Invitee";
        $scope.saveUserInvite = "Save Invite";    
        $scope.SaveAllRoleValue = "Save All";
        //----------------------------Super Admin------------------------------------------------------
        $scope.customerMgmt = "Go To Customer Management";
        $scope.planMgmt = "Go To Plan Management";
        $scope.generalSettings = "Go To Settings";
        //----------------------------Customer------------------------------------------------------
        $scope.newCustomer = "Add a New Customer";
        $scope.saveCustomerDetails = "Save Customer Details";
        $scope.refreshCustomerDetails = "Refresh Customer";
        $scope.cancelCustomerDetails = "Cancel Customer Creation";
        //----------------------------Plan Management------------------------------------------------------
        $scope.newPlan = "Add a New Plan";
        $scope.savePlanDetails = "Save Plan Details";
        $scope.refreshPlanDetails = "Reset Plan";
        $scope.cancelPlanDetails = "Cancel Plan Creation";
        //----------------------------General Settings------------------------------------------------------
        $scope.saveGeneralSettings = "Save Common Settings";
        //--------------------------------------------------------------------------------------------------------------------
        //----------------------------WebSync----------------------------------------------------------------
        $scope.Filterresulttitle = "Filter Results";
        $scope.Resetfiltertitle = "Reset Filter";
        $scope.Websynctitle = "WebSync";
        $scope.Logstitle = "Logs";

        //--------------------------------------------------------------------------------------------------------------------
        //--------------------Index PDF ---------------------------------------------------------------------------------
        $scope.Reset = "Reset the file";
        $scope.ContentNext = "Goto next page";
        $scope.Validation = "Validate sheet";
        $scope.BackValidation = "Back previous step";
        $scope.ImportContent = "Import the sheet";
        $scope.BackLog = "Back previous step";
        $scope.ExemptedReset = "Reset the file";
        $scope.ExemptedNext = "Goto next page";
        $scope.ExemptValidate = "Validate sheet";
        $scope.ExemptImport = "Import the sheet";
        $scope.ExemptBack = "Back previous step";
        //-------------------------------------------------------------------------------------------------------------

    }]);