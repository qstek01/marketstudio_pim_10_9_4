﻿
LSApp.controller("IndesignPopupController", ["$scope", "$window", "$location", "$routeParams", "dataFactory", '$http', '$localStorage', '$rootScope', '$compile',
    function ($scope, $window, $location, $routeParams, dataFactory, $http, $localStorage, $rootScope, $compile) {
        // --------------------------------------------------- Start Common Function --------------------------------------------------------------------------------

        $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerIDs = 0;
        }
        else {
            $scope.getCustomerIDs = $localStorage.getCustomerID;
            dataFactory.GetPreferencesDetails($scope.getCustomerIDs).success(function (response) {
                $scope.NewProject = response[0];
            });
        }

        function groupHeaderName(e) {

            if (e.value === 1) {
                return "Item Specifications (" + e.count + " items)";
            }
            else if (e.value === 3) {
                return "Item Image / Attachment (" + e.count + " items)";
            }
            else if (e.value === 4) {
                return "Item Price (" + e.count + " items)";
            } else if (e.value === 6) {
                return "Item Key (" + e.count + " items)";
            } else if (e.value === 7) {
                return "Product Description (" + e.count + " items)";
            } else if (e.value === 9) {
                return "Product Image / Attachment (" + e.count + " items)";
            } else if (e.value === 11) {
                return "Product Specifications (" + e.count + " items)";
            }
            else if (e.value === 12) {
                return "Product Price (" + e.count + " items)";
            }
            else if (e.value === 13) {
                return "Product Key (" + e.count + " items)";
            } else {
                return "Item Specifications (" + e.count + " items)";
            }
        }



        $scope.inDesignAttributeSetupClick = function () {
            //$scope.winInDesignAttributeSetup.refresh({ url: "../Views/App/Partials/inDesignAttributemanager.html" });
            //$scope.winInDesignAttributeSetup.center().open();
            //$scope.tableGroupHeaderValue = true;

        };

        $scope.inDesignGetAllAttributesdataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true, pageable: true, pageSize: 10,
            sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetAllInDesignAttributesfamily($scope.SelectedCatalogId, $scope.selectedfamilyid).success(function (response) {

                        if (response != null) {
                            for (var i = 0 ; response.length > i ; i++) {
                                if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                    response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                                }
                                if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                    response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                                }


                            }

                        }

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ISAvailable: { type: "boolean" },
                        CATALOG_ID: { editable: false },
                        FAMILY_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false }
                    }
                }
            },
            group: {
                field: "ATTRIBUTE_TYPE", aggregates: [
                   { field: "ATTRIBUTE_TYPE", aggregate: "count" }
                ]
            }

        });

        $scope.inDesignAvailableAttributes = {
            dataSource: $scope.inDesignGetAllAttributesdataSource,
            pageable: { buttonCount: 5 },
            columns: [
                { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>' },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" },
                {
                    field: "ATTRIBUTE_TYPE",
                    title: "Attribute Type",
                    hidden: true,
                    aggregates: ["count"],
                    groupHeaderTemplate: groupHeaderName
                }]
        };

        $scope.updateSelection = function (e, id) {
            id.dataItem.set("ISAvailable", true);
        };


        $scope.inDesignSelectedAttributesGriddataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true, pageSize: 5, pagable: true, serverPaging: true,
            //  pageable: true,
            // pageSize: 10,
            sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.GetAllInDesignAttributes($scope.SelectedCatalogId).success(function (response) {

                        if (response != null) {
                            for (var i = 0 ; response.length > i ; i++) {
                                if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
                                    response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
                                }
                                if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
                                    response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
                                }


                            }

                        }

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ISAvailable: { type: "boolean" },
                        CATALOG_ID: { editable: false },
                        FAMILY_ID: { editable: false },
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false }
                    }
                }
            }

        });

        $scope.inDesignSelectedAttributesGridOptions = {
            dataSource: $scope.inDesignSelectedAttributesGriddataSource,
            pageable: { buttonCount: 5 },
            //selectable: "multiple",
            columns: [
                { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforprodfamily" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>' },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px" }
            ]
        };
        $scope.updateSelection1 = function (e, id) {
            id.dataItem.set("ISAvailable", e.currentTarget.checked);
        };

        $scope.SaveSelectedAttributes = function () {
            var g = $scope.inDesignAllAttributesgrid.dataItems();
            angular.forEach(g, function (value, key) {
                if (value.ISAvailable) {
                    $scope.inDesignSelectedAttributesgrid.dataSource._data.push(value);
                    $scope.inDesignAllAttributesgrid.dataSource._data.remove(value);
                }
            });
        };
        $scope.DeleteSelectedAttributes = function () {
            var g = $scope.inDesignSelectedAttributesgrid.dataItems();
            angular.forEach(g, function (value, key) {
                if (value.ISAvailable) {
                    $scope.inDesignSelectedAttributesgrid.dataSource._data.remove(value);
                    $scope.inDesignAllAttributesgrid.dataSource._data.push(value);
                }
            });
        };

        $scope.btnGenerateInDesignXML = function () {
            if ($scope.SelectedprojectId == "" || $scope.SelectedprojectId == 0) {
                $scope.close = true;
                $scope.winInDesignAttributeSetup.close();
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select the Project and try again!',
                    type: "error"
                });
            }
            else {
                $scope.close = true;
                $scope.winInDesignAttributeSetup.close();

                var g = $scope.inDesignSelectedAttributesGriddataSource._data;
                if (g.length > 0) {
                    var df = $scope.selectedItem;
                    dataFactory.generateInDesignXMLs($scope.SelectedCatalogId, $scope.selectedItem, $scope.inDesignSelectedAttributesGriddataSource._data, $scope.flagallcreate, $scope.SelectedprojectId, $scope.NewProject.DisplayIDColumns).success(function (response) {
                        //$.msgBox({
                        //    title: $localStorage.ProdcutTitle,
                        //    content: '' + response + '.',
                        //    type: "info"
                        //});
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info",
                            success: function (result) {
                                debugger;
                                //Jothipriya
                                if (response == "XML generated successfully") {
                                    dataFactory.downloadXMLFilePath().success(function (responses) {
                                        let path1 = responses;
                                        let filename = $scope.selectedItem.XML_FILE_NAME + ".xml";
                                        window.open("DownloadIndesignXmlFile.ashx?Path=" + "\\" + path1 + "\\" + filename);
                                    });
                                }
                            }
                        });
                    }).error(function (error) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + error + '.',
                            type: "error"
                        });
                    });
                } else {
                    dataFactory.XmlDataFileswithoutAttr($scope.SelectedCatalogId, $scope.selectedItem, $scope.flagallcreate, $scope.SelectedprojectId, $scope.NewProject.DisplayIDColumns).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });
                    }).error(function (error) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + error + '.',
                            type: "error"
                        });
                    });
                }
            }
        };


        $scope.FileDownload = function (e) {
            $scope.bindworkflowforindesign.read();
            debugger;
            if ($scope.Indesign.STATUS_NAME == "APPROVE") {

                $scope.selectedItem = e.dataItem;
                var filename;
                var windowlocation = window.location.origin;

                if (e.dataItem.XML_FILE_NAME.toLowerCase().contains(".xml")) {
                    filename = e.dataItem.XML_FILE_NAME;
                    path1 = e.currentUserInfo.CustomerDetails.Comments;
                    //windowlocation = windowlocation + "/Content/InDesignXML/" + e.dataItem.XML_FILE_NAME;
                    window.open("DownloadIndesignXmlFile.ashx?Path=" + path1 + "\\Indesignxml\\" + filename);
                } else {
                    filename = e.dataItem.XML_FILE_NAME + ".xml";
                    path1 = e.currentUserInfo.CustomerDetails.Comments;
                    //windowlocation = windowlocation + "/Content/InDesignXML/" + e.dataItem.XML_FILE_NAME + ".xml";
                    window.open("DownloadIndesignXmlFile.ashx?Path=" + "\\" + path1 + "\\Indesignxml\\" + filename);
                }
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: ' Please! Make sure that PDF Publish and Workflow approval.',
                    type: "error"
                });
            }
        };

        $scope.btnGenerateInDesignXMLs = function () {
            //dataFactory.generateInDesignXML($scope.SelectedCatalogId, 'testcatalog', 145, 'test catalog', '144', 'DR-01-02', '1', '1897').success(function (response) {
            //    options.success(response);
            //    alert(response);
            //}).error(function (error) {
            //    options.error(error);
            //});
        };
        $scope.init = function () {

            $scope.selectedfamilyid = $localStorage.selectedfamilyid;

            if ($localStorage.getCatalogID === undefined) {
                $scope.SelectedCatalogId = 0;
            }
            else {
                $scope.SelectedCatalogId = $localStorage.getCatalogID;
                $scope.SelectedCatalogName = $localStorage.getCatalogName;
            }
            if ($localStorage.getCustomerID == undefined) {
                $scope.getCustomerID = 0;

            }
            else {
                $scope.getCustomerID = $localStorage.getCustomerID;
            }

            $("#divselectcatalog").show();
        };
        $scope.init();
    }]);
