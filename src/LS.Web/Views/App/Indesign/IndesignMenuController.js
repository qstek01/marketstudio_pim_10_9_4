﻿LSApp.controller('IndesignMenuController', ['$scope', '$rootScope', '$localStorage', function ($scope, $rootScope, $localStorage) {
    
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

    function createAutoFileName() {
        $scope.createAutoFileName();
    }
    function createnewfile() {
        $scope.createnewfile();
    }
    function createAllFiles() {
        $scope.createAllFiles();
    }

    function refreshXMLGrid() {
        $scope.refreshXMLGrid();
    }
    function refreshproject() {
        $scope.refreshproject();
    }
    if ($localStorage.EditMode == false)
    {
        $("#addNewFile").hide();
        $("#splitBtn").hide();
        $("#splitBtn1").hide();
    }
    //$("#addNewFile").ready({
    //    enable: false
    //});
    //$('#splitBtn').kendoButton({
    //    enable: $localStorage.EditMode
    //});
    //$('#splitBtn1').kendoButton({
    //    enable: $localStorage.EditMode
    //});
    $scope.indesigntoolbarOptions = {
        items: [{
            id:"addNewFile", type: "button", text: "Add New File", attributes: { "class": "red" }, click: createnewfile
        },
            //{
            //    id: "refresh", type: "button", text: "Refresh", attributes: { "class": "red" }, click: refreshXMLGrid
            //},
            {
                id: "addNewFile", type: "button", text: "Refresh", attributes: { "class": "red" }, click: refreshXMLGrid
            },
            {
                 type: "splitButton",
                text: "Options", attributes: { "class": "red" }, click: function () {
                    sample();
                },
                menuButtons: [
                    {
                    id:"splitBtn", text:"Automatic File Name", attributes: { "class": "red" }, click: function () {
                            createAutoFileName();
                        }
                    },
                    {
                        id:"splitBtn1", text: "Create all Catalog Data Files", attributes: { "class": "red" }, click: function () {
                            createAllFiles();
                        }
                    }//,
                    //{
                    //    text: "Generate InDesign Book File", attributes: { "class": "red" }, click: function () {
                    //        create();
                    //    }
                    //}
                ]
            },
            
            //,
            //{
            //    type: "button", text: "Copy XML", attributes: { "class": "red" }, click: onCopyXmlClick
            //},
            // {
            //     type: "button", text: "Move XML", attributes: { "class": "red" }, click: onMoveXmlClick
            // }

        ]
    };


    $scope.indesignprojecttoolbarOptions = {
        items: [{
            type: "button", text: "Refresh", attributes: { "class": "red" }, click: refreshproject
        }]
    };
}]);