﻿
LSApp.controller("IndesignController", ["$scope", "$window", "$location", "$timeout", "$routeParams", "$sce", "dataFactory", '$http', '$localStorage', '$rootScope', '$compile',
    function ($scope, $window, $location, $timeout, $routeParams, $sce, dataFactory, $http, $localStorage, $rootScope, $compile) {
        // --------------------------------------------------- Start Common Function --------------------------------------------------------------------------------
        $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
        $scope.showImport = true;
        $scope.Importvalidation = false;
        $scope.ImportvalidationResult = false;
        $scope.missing = false;
        $scope.newitem = false;
        $scope.duplicate = false;
        $scope.ViewLogIndesignvalidation = false;
        $scope.validation = false;
        $scope.TESTCHANGES = true;
        $scope.Errorlogoutputformat = "XLS";
        //InDesgin filters
        $rootScope.saveFilterAttributeValues = [];
        $scope.showFilterButton = true;
        $scope.showFilterText = false;
        $scope.clearFilterButton = true;
        $scope.IsAdmin = false;
        // $scope.Indesign.STATUS_NAME = '';
        //InDesgin filters

        if ($rootScope.clearFlag == "1") {
            $rootScope.clearFlag = "1"
        }
        else {
            $rootScope.clearFlag = "0";
        }
        $scope.Indesign = {
            PROJECT_NAME: "",
            COMMENTS: ""
        };
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerIDs = 0;
        }
        else {
            $scope.getCustomerIDs = $localStorage.getCustomerID;
            dataFactory.GetPreferencesDetails($scope.getCustomerIDs).success(function (response) {
                $scope.NewProject = response[0];
            });
        }

        $scope.printToCart = function (printSectionId) {
            var innerContents = document.getElementById(printSectionId).innerHTML;
            var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
            popupWinindow.document.open();
            popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
            popupWinindow.document.close();
        };
        $scope.Print = function () {
            $scope.printToCart("print_tab");
        };
        $scope.ProductPrint = function () {
            $scope.printToCart("ProductPreviewprint_tab");
        };
        $scope.SubProductPrint = function () {
            $scope.printToCart("SubProductPreviewprint_tab");
        };

        $scope.roleidcheck = function () {
            dataFactory.getAllRolesForIndesign().success(function (response) {
                $scope.IsAdmin = response;

            }).error(function (response) {
                //console.log(response);
            });
        };


        $scope.catalogDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCatalogDetails($scope.SelectedItem, $scope.getCustomerID).success(function (response) {

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.btnCreate = function () {

            dataFactory.Saveproject($scope.Indesign, $scope.SelectedCatalogId, $("#projecttype").val(), $scope.Indesign.STATUS_NAME).success(function (response) {
                if (response === 0 || response == -1) {
                    $("#divselectcatalog").show();
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Project Name already exists, please enter a different name for the Project.',
                        type: "error"
                    });

                } else {
                    $scope.Indesign.PROJECT_NAME = "";
                    $scope.projectId = response;
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Project created successfully.',
                        type: "info"
                    });
                    $scope.projectDataSource.read();
                }
            }).error(function (error) {
                options.error(error);
            });
        };

        var data = [{ type: "InDesign Catalog", value: "1" }
            //,{ type: "Export", value: "7" },
            //{ type: "PDF Catalog", value: "3" }
            //, { type: "HTML Catalog", value: "9" }
        ];

        // create DropDownList from input HTML element
        $("#projecttype").kendoDropDownList({
            dataTextField: "type",
            dataValueField: "value",
            dataSource: data,
            index: 0,
            change: function (e) {
                $scope.projectDataSource.read();
            }
        });

        $scope.SelectedCatalogName = "Sample";
        $scope.SelectedCatalogId = 0;
        $scope.SelectedprojectId = 0;
        $scope.SelectedprojectName = "";
        $scope.projectId = 0;

        $scope.catalogChange = function (e) {

            if (e.sender.value() !== "") {
                $scope.SelectedCatalogId = e.sender.value();
                $rootScope.selecetedCatalogId = e.sender.value();
                $scope.SelectedCatalogName = e.sender.text();
                $scope.projectDataSource.read();
                $scope.treeData.read();
                $scope.treeDataindesign.read();
                $scope.Indesign.PROJECT_NAME = "";
                $scope.SelectedprojectId = 0;
                $scope.SelectedrecordId = 0;
                $scope.GetIndesignFileValuesDatasource.read();
                $scope.GetIndesignCategoryValuesDatasource.read();
                $scope.projectMoveDataSource.read();
                $scope.projectCopyDataSource.read();

            }
        };

        $scope.projectChange = function (e) {
            if (e.sender.value() !== "") {
                $scope.SelectedrecordId = 0;
                $scope.SelectedprojectId = e.sender.value();
                $scope.SelectedprojectName = e.sender.text();
                $scope.GetIndesignFileValuesDatasource.read();
                $scope.workflowDataSourceIndesign.read();
                $scope.bindworkflowforindesign.read();
                $scope.projectMoveDataSource.read();
                $scope.projectCopyDataSource.read();
                $scope.treeData.read();
                $scope.treeDataindesign();
                $scope.clearInDesign();

            } else {
                $scope.SelectedprojectId = 0;
                $scope.SelectedprojectName = "";
                $scope.GetIndesignFileValuesDatasource.read();
                $scope.projectMoveDataSource.read();
                $scope.projectCopyDataSource.read();
                $scope.SelectedrecordId = 0;
                $scope.workflowDataSourceIndesign.read();
                $scope.bindworkflowforindesign.read();
                $scope.treeData.read();
                $scope.treeDataindesign();
            }

            if ($scope.SelectedprojectId != 0) {
                dataFactory.getProdjectDetails($scope.SelectedprojectId).success(function (response) {
                    if (response != null) {
                        var data = response[0];
                        $scope.Indesign.PROJECT_NAME = response;
                        $scope.bindworkflowforindesign.read();
                    }
                });
            }
            else {
                $scope.Indesign.PROJECT_NAME = "";
            }
        };



        $scope.projectDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetProjectDetails($scope.SelectedCatalogId, $("#projecttype").val()).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        $scope.projectMoveDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    if ($scope.SelectedprojectId == "") {
                        $scope.SelectedprojectId = 0;
                    }
                    dataFactory.GetMoveProjectDetails($scope.SelectedCatalogId, $("#projecttype").val(), $scope.SelectedprojectId).success(function (response) {
                        //  $scope.GetIndesignFileValuesDatasource.read();
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        $scope.projectCopyDataSource = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCopyProjectDetails($scope.SelectedCatalogId, $("#projecttype").val()).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.ClearAllFilter = function () {
            dataFactory.ClearAllFilter().success(function (response) {
            }).error(function (error) {
                options.error(error);
            });
            $scope.showFilterText = false;
            $('#showFilterId').addClass('ng-hide');
            $scope.clearFilterButton = true;
        }

        //----------------------------------------Delete project----------------
        $scope.DeletexmlProject = function (SelectedprojectId) {

            if ($scope.SelectedprojectName !== "") {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Are you sure want to Remove the Project?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {
                        if (result === "Yes") {
                            $http.post("../App/RemoveProjectFromDb?catlogId=" + $scope.SelectedCatalogId + "&selectedprojectId=" + $scope.SelectedprojectId + "&selectedprojectName=" + $scope.SelectedprojectName)
                                .success(function (msg) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + msg + '.',
                                        type: "info"
                                    });
                                    $scope.projectDataSource.read();
                                    $('#IndesignGrid').data('kendoGrid').dataSource.read();
                                    $('#IndesignGrid').data('kendoGrid').refresh();
                                    $('#IndesignCategoryGrid').data('kendoGrid').dataSource.read();
                                    $('#IndesignCategoryGrid').data('kendoGrid').refresh();
                                })
                                .error(function () {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'Please try again.',
                                        type: "error"
                                    });
                                });
                        }
                        else {
                            return;
                        }
                    }
                });


            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select one project.',
                    //type: "info"
                });
            }
        };
        //------------------------------------------------

        //------------------------------------------- Grid Functions -----------------------------------------//
        $scope.GetIndesignFileValuesDatasource = new kendo.data.DataSource({
            type: "json", sort: { field: "FILE_NO", dir: "asc" },
            serverFiltering: true, pageable: true, pageSize: 5, serverPaging: true,
            serverSorting: true,
            transport: {
                read: function (options) {
                    dataFactory.GetIndesignProjectValues($scope.SelectedprojectId, options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    debugger;
                    var BeforeEditXMLName = $rootScope.XmlFileNameBeforeEdit;
                    var AfterEditXMLName = options.data.XML_FILE_NAME;

                    if (BeforeEditXMLName == AfterEditXMLName) {
                        dataFactory.UpdateIndesignValues($scope.SelectedprojectId, options.data).success(function (response) {
                            options.success(response);
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Updated Successfully.',
                                type: "error"
                            });
                            $scope.GetIndesignFileValuesDatasource.read();
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                    else if (BeforeEditXMLName != AfterEditXMLName) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "If you rename the XML file name then you cannot update the existing InDesign document. Do you want to continue?",
                            type: "confirm",
                            buttons: [{ value: "Yes" }, { value: "No" }],
                            success: function (result) {
                                var bool = false;
                                if (result === "Yes") {
                                    dataFactory.UpdateIndesignValues($scope.SelectedprojectId, options.data).success(function (response) {
                                        options.success(response);
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: 'Updated Successfully.',
                                            type: "error"
                                        });
                                        $scope.GetIndesignFileValuesDatasource.read();
                                    }).error(function (error) {
                                        options.error(error);
                                    });
                                }
                            }
                        });
                    }
                },
                destroy: function (options) {
                    dataFactory.DeleteIndesignValues($scope.SelectedprojectId, options.data).success(function (response) {
                        options.success(response);
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Deleted successfully.',
                            type: "error"
                        });
                        $scope.GetIndesignFileValuesDatasource.read();
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                create: function (options) {
                    dataFactory.CreateIndesignValue($scope.SelectedprojectId, options.data).success(function (response) {

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                    return kendo.stringify(options);
                }
            }, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "PROJECT_ID",
                    fields: {
                        FILE_NO: { type: "number", editable: true },
                        RECORD_ID: { type: "number", editable: true },
                        XML_FILE_NAME: { editable: true },
                        XSD_FILE_NAME: { editable: true },
                        PAGE_FILE_NAME: { editable: true }, TEMPLATE_FILE_NAME: { editable: true }, NOTES: { editable: true }
                    }
                }
            }
        });
        $scope.copyProjectId = 0;
        $scope.moveProjectId = 0;
        $scope.projectCopyChange = function (e) {
            $scope.copyProjectId = e.sender.value();
            if ($scope.copyProjectId == '' || $scope.copyProjectId == 0) {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Select valid project.',
                    type: "error"
                });
            }
            else {
                if ($scope.SelectedrecordId !== 0 && $scope.SelectedrecordId !== "") {
                    dataFactory.copyXml($scope.SelectedrecordId, $scope.copyProjectId).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });
                        $scope.GetIndesignFileValuesDatasource.read();
                        $scope.GetIndesignCategoryValuesDatasource.read();
                        $scope.copyProjectId = 0;
                        $('#copyindesigndpn').data('kendoDropDownList').value(-1);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        };
        $scope.projectMoveChange = function (e) {
            $scope.moveProjectId = e.sender.value();
            if ($scope.moveProjectId == '' || $scope.moveProjectId == 0) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Select valid project.',
                    type: "error"
                });
            }
            else {
                if ($scope.SelectedrecordId !== 0) {
                    dataFactory.moveXml($scope.SelectedrecordId, $scope.moveProjectId).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + response + '.',
                            type: "info"
                        });
                        $scope.GetIndesignFileValuesDatasource.read();
                        $scope.GetIndesignCategoryValuesDatasource.read();
                        $scope.moveProjectId = 0;
                        // $scope.projectMoveDataSource.read();
                        $('#moveindesigndpn').data('kendoDropDownList').value(-1);
                    }).error(function (error) {
                        options.error(error);
                    });
                }

            }
        };
        $scope.catalogXmlFileName = '';
        $scope.getCatalogFileName = function () {
            $scope.winInDesignCatalogFileNameEditor.close();
            if ($scope.catalogXmlFileName != '') {
                if ($localStorage.SelectedprojectId != null) {
                    dataFactory.CreateIndesignValues($localStorage.SelectedprojectId, false, $scope.SelectedCatalogId, $scope.catalogXmlFileName).success(function (response) {
                        if (response == "Name already exists. Please enter a different name for the Group") {
                            $("#divselectcatalog").show();
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'File Name already exists, please enter a different name for the File.',
                                type: "error"
                            });

                        }
                        else {
                            $scope.SelectedprojectId = $localStorage.SelectedprojectId;
                            // $scope.GetIndesignFileValuesDatasource.read();
                            $('#IndesignGrid').data('kendoGrid').dataSource.read();
                            $('#IndesignGrid').data('kendoGrid').refresh();
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'File created successfully.',
                                type: "info"
                            });
                        }
                    }).error(function (error) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + error + '.',
                            type: "error"
                        });
                    });

                }
                $scope.catalogXmlFileName = '';

            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Enter File Name.',
                    // type: "info"
                });
            }
        };
        $scope.closeWindow = function () {
            $scope.winInDesignCatalogFileNameEditor.close();
        };
        $scope.createnewfile = function () {
            if ($scope.SelectedprojectId !== "" && $scope.SelectedprojectId !== 0) {
                if ($scope.tick) {
                    dataFactory.CreateIndesignValues($scope.SelectedprojectId, true, $scope.SelectedCatalogId, $scope.catalogXmlFileName).success(function (response) {
                        if (response == -1) {
                            $("#divselectcatalog").show();
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'File Name already exists, please enter a different name for the File.',
                                type: "error"
                            });

                        }
                        else {
                            $scope.GetIndesignFileValuesDatasource.read();
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'InDesign File created successfully.',
                                type: "info"
                            });
                        }
                    }).error(function (error) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + error + '.',
                            type: "error"
                        });
                    });
                } else {
                    $localStorage.SelectedprojectId = $scope.SelectedprojectId;
                    $scope.winInDesignCatalogFileNameEditor.center().open();

                }
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose one project and then create a file.',
                    type: "error"
                });
            }

        };
        $scope.FileNo = 0;
        $scope.ddlDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: function (options) {
                    dataFactory.GetIndesignFileValues($scope.SelectedprojectId, $scope.FileNo).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        $scope.FileDropDownEditor = function (container, options) {
            if (options.model.FILE_NO != "") {
                $scope.FileNo = 0;
            } else {
                $scope.FileNo = 1;
            }
            $scope.indesignFileNo = options.model.FILE_NO;
            $scope.ddlDataSource.read();
            var editor = $('<input kendo-drop-down-list required id="indesignPopupddl" ng-model="indesignFileNo" k-data-text-field="\'FILE_NO\'" k-data-value-field="\'FILE_NO\'" k-data-source="ddlDataSource" data-bind="value:' + options.field + '"/>')
                .appendTo(container);
            //alert(options.field);
            $compile(editor)($scope);
            editor.css("visibility", "visible");
        };
        $scope.GetIndesignValues = {
            dataSource: $scope.GetIndesignFileValuesDatasource,
            pageable: { buttonCount: 5 }, editable: "popup", sortable: true, scrollable: true, resizable: false,
            filterable: true,
            selectable: "row",
            columns: [
                {
                    field: "FILE_NO", title: "File No", editor: $scope.FileDropDownEditor, template: "#=FILE_NO#", width: "110px",
                    headerTemplate: '<span title="File No">File No</span>'
                },
                {
                    field: "", width: "30px", filterable: false, sortable: false, attributes: {
                        title: "Regenerated XML",
                        class: "#= XML_GENERATED_DATE == null? 'xmlGeneratedInActiveStatus' : XML_MODIFIED_DATE > XML_GENERATED_DATE ? 'xmlGeneratedInActiveStatus' : XML_GENERATED_DATE > FAMILY_MODIFED_DATE.MODIFIED_DATE  ? 'xmlGeneratedActiveStatus' : 'xmlGeneratedInActiveStatus' # "
                    },
                },
                { field: "XML_FILE_NAME", title: "XML Data File", width: "150px", headerTemplate: '<span title="XML Data File">XML Data File</span>' },
                //  {
                //      command: [{
                //          template: "<a class=\'k-item girdicons showdetails col-sm-10\' ng-click=\'showDetails(this)\' style=\'text-align:center;padding: 0;\'><div title=\'Create Data File\' class=\'glyphicon glyphicon-open blue btn-xs-icon\'></div></a>"
                //, editable: false
                //      }], width: "80px", title: "Create", headerTemplate: '<span  title="Create">Create</span>'
                //  },
                { field: "XSD_FILE_NAME", width: "100px", title: "Name", headerTemplate: '<span title="Name">Name</span>' },
                { field: "PAGE_FILE_NAME", width: "120px", title: "InDesign File", headerTemplate: '<span title="InDesign File">InDesign File</span>' },
                { field: "TEMPLATE_FILE_NAME", width: "150px", title: "Template File", headerTemplate: '<span title="Template File">Template File</span>' },
                { field: "NOTES", title: "Notes", width: "100px", headerTemplate: '<span title="Notes">Notes</span>' },
                {
                    command: [{ name: 'edit', text: "", width: "10px", template: "<a class=\'k-grid-edit k-item girdicons\'style=\'padding-right:10px\'><div title=\'Edit\' class=\'glyphicon glyphicon-edit blue btn-xs-icon\'></div></a>" },
                    { name: "destroy", text: "", width: "10px", template: "<a class=\'k-item girdicons\' ng-click=\'DeleteIndesignValues($event,this)\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" },
                    ],
                    title: "Actions", headerTemplate: '<span title="Actions">Actions</span>',
                    width: "80px",
                },
                //{
                //    command: [{
                //        template: "<a class=\'k-item girdicons col-sm-10\' ng-click=\'FileDownload(this)\' style=\'text-align:center;padding: 0;\'>" +
                //          "<div title=\'Download File\' class=\'glyphicon glyphicon-download-alt blue btn-xs-icon\'></div></a>", editable: false
                //    }], width: "80px", title: "Download",
                //    headerTemplate: '<span title="Download">Download</span>'
                //},
                //Naveen
                {
                    command: [{
                        template: "<label for='browse'><i class='fa fa-upload' aria-hidden='true' style='position: relative;left:35px;font-size:17px;'  ng-click=\'upLoadXmlFile(this)\' ></i></label>" +
                            "<div title=\'Upload File\'></div></a>", editable: false
                    }], width: "150px", title: "Upload File",
                    headerTemplate: '<span title="Upload File">Upload File</span>'
                },
                //Naveen
                //JO START
                {
                    command: [{
                        template: "<a class=\'k-item girdicons col-sm-10\' ng-click=\'showDetails(this)\' style=\'text-align:center;padding: 0;\'>" +
                            "<div title=\'Download File\' class=\'glyphicon glyphicon-download-alt blue btn-xs-icon\'></div></a>", editable: false
                    }], width: "150px", title: "Create & Download",
                    headerTemplate: '<span title="Create & Download">Create & Download</span>'
                },
                //JO END
                {
                    command: [{
                        template: "<a class=\'k-item girdicons showdetails col-sm-10\' title='Preview' ng-click=\'previewProject()\' style=\'text-align:center;padding: 0;\'> <div class=\'glyphicon glyphicon-eye-open blue btn-xs-icon\'></div></a>",

                        editable: false
                    }], title: "Preview", width: "80px"



                }],
            dataBound: function (e) {
                if (e.sender._data.length > 0) {

                    var row = this.tbody.find('tr:first');
                    this.select(row);
                    $scope.SelectedrecordId = e.sender._data[0].RECORD_ID;
                    $scope.filterSelectedRecordId = $scope.SelectedrecordId;
                    $rootScope.SelectedrecordId = $scope.SelectedrecordId;
                    $scope.GetIndesignCategoryValuesDatasource.read();
                } else {
                    $scope.SelectedrecordId = 0;
                    $scope.filterSelectedRecordId = 0;
                    $scope.GetIndesignCategoryValuesDatasource.read();
                }

            }, edit: function (e) {
                $(".showdetails").hide();
                $(".download").hide();
            },
            cancel: function (e) {
                $scope.GetIndesignFileValuesDatasource.read();
                // $scope.ddlDataSource.read();
                //      $scope.ddlDataSource.refresh();
                //alert("cancel");
            }
        };

        $scope.upLoadXmlFile = function (e) {
            debugger
            if ($scope.Indesign.STATUS_NAME == "APPROVE") {
                $scope.flagallcreate = false;
                $scope.selectedItem = e.dataItem;
                if (e.dataItem.XML_FILE_NAME !== "") {
                    if ($scope.GetIndesignCategoryValuesDatasource._data.length > 0) {
                        // popup
                        if ($scope.GetIndesignCategoryValuesDatasource._data[0].CreateXMLForSelectedAttributesToAllFamily == true) {
                            dataFactory.GetIndesignFamilyValues($scope.SelectedCatalogId, $scope.GetIndesignCategoryValuesDatasource._data).success(function (response) {
                                $scope.selectedfamilyid = response;
                                $localStorage.selectedfamilyid = $scope.selectedfamilyid;
                                if ($scope.selectedfamilyid == '0') {
                                    dataFactory.XmlDataFileswithoutAttr($scope.SelectedCatalogId, $scope.selectedItem, $scope.flagallcreate, $scope.SelectedprojectId, $scope.NewProject.DisplayIDColumns, $rootScope.saveFilterAttributeValues).success(function (responses) {

                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + response + '.',
                                            type: "info"
                                        });
                                    }).error(function (error) {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + error + '.',
                                            type: "error"
                                        });
                                    });
                                } else {
                                    $scope.winInDesignAttributeSetup.refresh({ url: "../Views/App/Partials/inDesignAttributemanager.html" });
                                    $scope.winInDesignAttributeSetup.center().open();
                                    $scope.tableGroupHeaderValue = true;
                                }
                            }).error(function (error) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + error + '.',
                                    type: "error"
                                });
                            });
                        }
                        else {
                            dataFactory.XmlDataFileswithoutAttr($scope.SelectedCatalogId, $scope.selectedItem, $scope.flagallcreate, $scope.SelectedprojectId, $scope.NewProject.DisplayIDColumns, $rootScope.saveFilterAttributeValues).success(function (response) {
                                //jo strat
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info",
                                    success: function (result) {
                                        //if (result == "Ok") {
                                        if (response == "XML generated successfully") {
                                            $scope.Fileupload(e);
                                        }
                                        else if (response == "XML generated successfully. You are about to reach the maximum memory") {
                                            $scope.Fileupload(e);
                                        }
                                    }
                                })
                                //$.msgBox({
                                //    title: $localStorage.ProdcutTitle,
                                //    content: '' + response + '.',
                                //    type: "info"
                                //});
                                //jo end
                            }).error(function (error) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + error + '.',
                                    type: "error"
                                });
                            });
                        }
                    } else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please add Products using the Navigator.',
                            type: "error"
                        });
                    }
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please enter a File Name.',
                        type: "error"
                    });
                }
            }
            else {
                debugger;
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: ' Please! Make sure that PDF Publish and Workflow approval.',
                    type: "error"
                });
            }
            //jo end
        }


        $("#IndesignGrid").on("click", "tr", function (e) {
            var grid = $("#IndesignGrid").data("kendoGrid");
            var selectedItem = grid.dataItem(grid.select());
            $scope.handleChange(selectedItem);
        });
        $scope.selectedItem = [];
        $scope.selectedfamilyid = "";

        $scope.showDetails = function (e) {
            debugger
            if ($scope.Indesign.STATUS_NAME == "APPROVE") {
                $scope.flagallcreate = false;
                $scope.selectedItem = e.dataItem;
                if (e.dataItem.XML_FILE_NAME !== "") {
                    if ($scope.GetIndesignCategoryValuesDatasource._data.length > 0) {
                        // popup
                        if ($scope.GetIndesignCategoryValuesDatasource._data[0].CreateXMLForSelectedAttributesToAllFamily == true) {
                            dataFactory.GetIndesignFamilyValues($scope.SelectedCatalogId, $scope.GetIndesignCategoryValuesDatasource._data).success(function (response) {
                                $scope.selectedfamilyid = response;
                                $localStorage.selectedfamilyid = $scope.selectedfamilyid;
                                if ($scope.selectedfamilyid == '0') {
                                    dataFactory.XmlDataFileswithoutAttr($scope.SelectedCatalogId, $scope.selectedItem, $scope.flagallcreate, $scope.SelectedprojectId, $scope.NewProject.DisplayIDColumns, $rootScope.saveFilterAttributeValues).success(function (responses) {

                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + response + '.',
                                            type: "info"
                                        });
                                    }).error(function (error) {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: '' + error + '.',
                                            type: "error"
                                        });
                                    });
                                } else {
                                    $scope.winInDesignAttributeSetup.refresh({ url: "../Views/App/Partials/inDesignAttributemanager.html" });
                                    $scope.winInDesignAttributeSetup.center().open();
                                    $scope.tableGroupHeaderValue = true;
                                }
                            }).error(function (error) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + error + '.',
                                    type: "error"
                                });
                            });
                        }
                        else {
                            dataFactory.XmlDataFileswithoutAttr($scope.SelectedCatalogId, $scope.selectedItem, $scope.flagallcreate, $scope.SelectedprojectId, $scope.NewProject.DisplayIDColumns, $rootScope.saveFilterAttributeValues).success(function (response) {
                                //jo strat
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info",
                                    success: function (result) {
                                        //if (result == "Ok") {
                                        if (response == "XML generated successfully") {
                                            $scope.FileDownload(e);
                                        }
                                        else if (response == "XML generated successfully. You are about to reach the maximum memory") {
                                            $scope.FileDownload(e);
                                        }
                                    }
                                })
                                //$.msgBox({
                                //    title: $localStorage.ProdcutTitle,
                                //    content: '' + response + '.',
                                //    type: "info"
                                //});
                                //jo end
                            }).error(function (error) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + error + '.',
                                    type: "error"
                                });
                            });
                        }
                    } else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please add Products using the Navigator.',
                            type: "error"
                        });
                    }
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please enter a File Name.',
                        type: "error"
                    });
                }
            }
            else {
                debugger;
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: ' Please! Make sure that PDF Publish and Workflow approval.',
                    type: "error"
                });
            }
            //jo end

            //$scope.GetIndesignFileValuesDatasource.read();
            //$scope.GetIndesignCategoryValuesDatasource.read();
            //$scope.bindworkflowforindesign.read();
        };
        $scope.Fileupload = function (e) {
            $scope.bindworkflowforindesign.read();
            debugger;
            if ($scope.Indesign.STATUS_NAME == "APPROVE") {

                $scope.selectedItem = e.dataItem;
                var filename;
                var windowlocation = window.location.origin;

                if (e.dataItem.XML_FILE_NAME.toLowerCase().contains(".xml")) {
                    filename = e.dataItem.XML_FILE_NAME;
                    path1 = e.currentUserInfo.CustomerDetails.Comments;
                    //windowlocation = windowlocation + "/Content/InDesignXML/" + e.dataItem.XML_FILE_NAME;
                    window.open("DownloadIndesignXmlFile.ashx?Path=" + path1 + "\\Indesignxml\\" + filename);
                    debugger;
                    //To refresh xml list data file
                    // $scope.GetIndesignFileValuesDatasource.read();
                } else {
                    filename = e.dataItem.XML_FILE_NAME + ".xml";
                    path1 = e.currentUserInfo.CustomerDetails.Comments;
                    //windowlocation = windowlocation + "/Content/InDesignXML/" + e.dataItem.XML_FILE_NAME + ".xml";
                    $http.post("/ImageManagement/SaveXMLFilesManageDrive?customerFolder=" + $scope.getCustomerFolder + "&Filename=" + filename + "", filename,
                          {
                              withCredentials: true,
                              headers: { 'Content-Type': undefined },
                              transformRequest: angular.identity

                          }).success(function (response) {
                              $.msgBox({
                                  title: $localStorage.ProdcutTitle,
                                  content: 'XML upload into server successfully.',
                                  type: ""
                              });
                          });
                    //To refresh xml list data file
                    // $scope.GetIndesignFileValuesDatasource.read();
                }
            }

            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: ' Please! Make sure that PDF Publish and Workflow approval.',
                    type: "error"
                });
            }
        };
        $scope.FileDownload = function (e) {
            $scope.bindworkflowforindesign.read();
            debugger;
            if ($scope.Indesign.STATUS_NAME == "APPROVE") {

                $scope.selectedItem = e.dataItem;
                var filename;
                var windowlocation = window.location.origin;

                if (e.dataItem.XML_FILE_NAME.toLowerCase().contains(".xml")) {
                    filename = e.dataItem.XML_FILE_NAME;
                    path1 = e.currentUserInfo.CustomerDetails.Comments;
                    //windowlocation = windowlocation + "/Content/InDesignXML/" + e.dataItem.XML_FILE_NAME;
                    window.open("DownloadIndesignXmlFile.ashx?Path=" + path1 + "\\Indesignxml\\" + filename);
                    debugger;
                    //To refresh xml list data file
                   // $scope.GetIndesignFileValuesDatasource.read();
                } else {
                    filename = e.dataItem.XML_FILE_NAME + ".xml";
                    path1 = e.currentUserInfo.CustomerDetails.Comments;
                    //windowlocation = windowlocation + "/Content/InDesignXML/" + e.dataItem.XML_FILE_NAME + ".xml";
                    window.open("DownloadIndesignXmlFile.ashx?Path=" + "\\" + path1 + "\\Indesignxml\\" + filename);
                    debugger;
                    //To refresh xml list data file
                   // $scope.GetIndesignFileValuesDatasource.read();
                }
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: ' Please! Make sure that PDF Publish and Workflow approval.',
                    type: "error"
                });
            }
        };

        $scope.refreshXMLGrid = function () {
            debugger;
            if ($scope.SelectedprojectId == 0) { }
            else {
                $scope.GetIndesignFileValuesDatasource.read();
            }
        };

        $scope.indesignsortChange = function (e) {
        };
        //------------------------------------------------------------------------------------------------------//
        //-------------------------------------------Category Grid Functions -----------------------------------------//
        $scope.SelectedrecordId = 0;
        $scope.OldSort = 0;
        $scope.NewSort = 0;
        $scope.handleChange = function (dataItem) {
            if (dataItem != undefined) {
                $rootScope.XmlFileNameBeforeEdit = dataItem.XML_FILE_NAME;
                $scope.SelectedrecordId = dataItem.RECORD_ID;
                $rootScope.SelectedrecordId = dataItem.RECORD_ID;
            }

            //$scope.GetIndesignCategoryValuesDatasource.read();
            $scope.treeData.read();

            dataFactory.GetIndesignFilterCategoryProjectValues($rootScope.SelectedrecordId, $rootScope.saveFilterAttributeValues).success(function (response) {
                //  $scope.SelectedrecordId = $scope.filterSelectedRecordId;
                // $scope.GetIndesignCategoryValuesDatasource.read();

                $("#IndesignCategoryGrid").data("kendoGrid").dataSource.read();
                $("#IndesignCategoryGrid").data("kendoGrid").refresh();
                $scope.showFilterText = false;
                $('#showFilterId').addClass('ng-hide');
                $scope.clearFilterButton = true;
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.GetIndesignCategoryValuesDatasource = new kendo.data.DataSource({
            sort: [{ field: "SORT_ORDER", dir: "asc" },
            { field: "ISAvailable", dir: "desc", }],
            // type: "json",
            // serverFiltering: true, pageable: true, pageSize: 5, serverPaging: true,
            // serverSorting: true, batch: false,

            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: false,
            autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.GetIndesignCategoryProjectValues($scope.SelectedrecordId, $rootScope.selecetedCatalogId, options.data).success(function (response) {
                        options.success(response);
                        if (response.Data.length > 0) {
                            $scope.showFilterButton = false;
                            $scope.clearFilterButton = false;
                        }
                        else {
                            $scope.showFilterButton = true;
                            $scope.clearFilterButton = true;

                        }

                        if ($rootScope.saveFilterAttributeValues.length == 0) {
                            $scope.clearFilterButton = true;

                        }
                        else {
                            $scope.clearFilterButton = false;
                        }
                        if ($rootScope.clearFlag == "1") {
                            $scope.clearFilterButton = false;
                        }
                        else if ($rootScope.clearFlag == "0" & $rootScope.saveFilterAttributeValues.length == 0) {
                            $scope.clearFilterButton = true;
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }, update: function (options) {
                    dataFactory.categorysortChange($scope.SelectedrecordId, options.data, $scope.GetIndesignCategoryValuesDatasource._data).success(function (response) {
                        options.success(response);
                        if (response.length > 0) {
                            $scope.showFilterButton = false;
                            $scope.clearFilterButton = false;
                        }
                        else {
                            $scope.showFilterButton = true;
                            $scope.clearFilterButton = true;
                        }
                        if ($rootScope.saveFilterAttributeValues.length == 0) {
                            $scope.clearFilterButton = true;
                        }
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update successful.',
                            type: "error"
                        });

                        $scope.GetIndesignCategoryValuesDatasource.read();
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                destroy: function (options) {
                    dataFactory.DeleteIndesignCategoryValues(options.data).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Deleted successfully.',
                            type: "error"
                        });

                        $scope.GetIndesignCategoryValuesDatasource.read();
                        options.success(response);
                        if (response.Data.length > 0) {
                            $scope.showFilterButton = false;
                            $scope.clearFilterButton = false;
                        }
                        else {
                            $scope.showFilterButton = true;
                            $scope.clearFilterButton = true;
                        }
                        if ($rootScope.saveFilterAttributeValues.length == 0) {
                            $scope.clearFilterButton = true;
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options, operation) {
                    if (operation !== "read" && options.models) {
                        return { models: kendo.stringify(options.models) };
                    }
                    return kendo.stringify(options);
                }
            }, schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "FAMILY_ID",
                    fields: {
                        //FAMILY_ID: { type: "number", editable: false },
                        //RECORD_ID: { type: "number", editable: false },
                        SORT_ORDER: { type: "number", editable: true },
                        CATEGORY_NAME: { enable: false },
                        CATEGORY_ID: { enable: false },
                        SECTION_NAME: { enable: false },
                        FAMILY_NAME: { enable: false }
                    }
                }
            }

        });

        $scope.ddlCategoryDataSource = new kendo.data.DataSource({
            type: "json",
            autobind: "false",
            transport: {
                read: function (options) {
                    dataFactory.GetIndesignCategoryValues($scope.SelectedrecordId).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.CategoryDropDownEditor = function (container, options) {
            $scope.ddlCategoryDataSource.read();
            var editor = $('<input kendo-drop-down-list required data-auto-bind="false" k-data-text-field="\'SORT_ORDER\'" k-data-value-field="\'SORT_ORDER\'" k-data-source="ddlCategoryDataSource" data-bind="value:' + options.field + '"/>')
                .appendTo(container);
            $compile(editor)($scope);
            editor.css("visibility", "visible");
        };

        //    $scope.GetIndesignCategoryValues = {
        //        dataSource: $scope.GetIndesignCategoryValuesDatasource,
        //        pageable: true,  sortable: true, scrollable: true,
        //        filterable: true,editable: {
        //            mode: "inline",

        //        },
        //        selectable: "row",
        //        columns: [{ field: "CATEGORY_NAME", title: "Category Name" },
        //{ field: "FAMILY_NAME", title: "Family Name" },
        //    };

        $scope.GetIndesignCategoryValues = {

            dataSource: $scope.GetIndesignCategoryValuesDatasource,

            //pageable: { buttonCount: 5 }, 
            editable: {

                mode: "popup",
                confirmation: function (e) {
                    return "Are you sure that you want to delete the product '" + e.FAMILY_NAME + "' in Page Organizer?";
                }
            }, sortable: true, scrollable: true,
            filterable: true,
            selectable: "row",
            columns: [

                {
                    field: "ISAvailable", title: "Select", nullable: true, filterable: false, sortable: false, width: "50px", attributes: { style: "text-align:center" }, template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="DeleteIndesignCategoryValuesforcheckbox($event, this)"></input>', headerAttributes: { style: "text-align:center" }, headerTemplate: "<input type='checkbox' title='select all' class='mc-checkbox' ng-click='selectAll($event)' />"
                },
                {
                    field: "", width: "30px", title: "Modified Family", filterable: false, sortable: false, attributes: {
                        class: "#=FAMILY_MODIFIED_DATE > XML_GENERATED_DATE ? 'modifedfamilyIndesign' : 'unPublishedfamilyIndesign' #"
                    },
                },

                { field: "SORT_ORDER", title: "Sort Order", width: "120px", editor: $scope.CategoryDropDownEditor, template: "#=SORT_ORDER#" },

                { field: "CATEGORY_NAME", title: "Category Name", },
                { field: "FAMILY_NAME", title: "Product Name" },
                {
                    command: [{ name: 'edit', text: "", width: "10px", template: "<a class=\'k-grid-edit k-item girdicons\'><div title=\'Edit\' class=\'glyphicon glyphicon-edit blue btn-xs-icon\'  disabled=\'disabled\'></div></a>" },
                    { name: "destroy", text: "", width: "10px", template: "<a class=\' k-item girdicons\' ng-click=\'DeleteIndesignCategoryValues($event,this)\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" },

                    { template: "<a class=\'k-item girdicons showdetails\' title='Preview' ng-click='previewFamily(this)'><div class=\'glyphicon glyphicon-eye-open blue btn-xs-icon\' ></div></a>" }],
                    title: "Actions",
                    width: "100px",

                }],

            toolbar: [
                {
                    name: "reset",
                    text: "Reset",
                    template: '<a ng-click="btnDeleteFamilies($event,this)"  class="red deleteButton k-button k-button-icontext k-grid-upload">Delete</a> <a ng-click="btnResetFamilies()" class="red resetButton k-button k-button-icontext k-grid-upload">Reset</a>',
                }],
            dataBound: function (e) {
            }, edit: function (e) {
                e.container.find(".k-edit-label:first").hide();
                e.container.find(".k-edit-field:first").hide();
                e.container.find(".k-edit-label:eq(1),.k-edit-field:eq(1)").hide();
            }
        };


        $scope.updateSelection = function (e, id) {
            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);

                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });

            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };

        var DeleteValues = [];

        $scope.selectAll = function (ev) {
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            if (DeleteValues == "") {
                DeleteValues = [];
            }

            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.FAMILY_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
            if (ev.currentTarget.checked == true) {
                for (var i = 0; i < items.length; i++) {

                    DeleteValues.push(items[i].FAMILY_ID);

                }
            }
            if (ev.currentTarget.checked == false) {
                for (var i = 0; i < items.length; i++) {

                    DeleteValues.pop(items[i].FAMILY_ID);

                }
            }
        };


        $scope.categorysortChange = function (e) {
            // alert(e.sender._old);
            //if (e.sender._initial !== "" && e.sender._old !== "") {
            //    $scope.OldSort = e.sender._initial;
            //    $scope.NewSort = e.sender._old;
            //    alert($scope.OldSort);
            //    alert($scope.NewSort);
            //    //var datas = e.sender.dataSource._data;
            //    //dataFactory.categorysortChange($scope.SelectedrecordId, e.sender._initial, e.sender._old, datas).success(function (response) {
            //    //    $scope.GetIndesignCategoryValuesDatasource.read();
            //    //}).error(function (error) {
            //    //    options.error(error);
            //    //});
            //}
        };

        $scope.refreshproject = function () {
            //  var datas = e.sender.dataSource._data;
            if ($scope.SelectedrecordId !== 0 && $scope.SelectedrecordId !== "undefined") {
                dataFactory.RefreshProject($scope.SelectedrecordId).success(function (response) {
                    $scope.GetIndesignCategoryValuesDatasource.read();
                }).error(function (error) {
                    options.error(error);
                });
            }
        };

        //------------------------------------------------------------------------------------------------------//

        $scope.treeData = new kendo.data.HierarchicalDataSource({
            type: "json",
            loadOnDemand: false,
            transport: {
                read: function (options) {
                    dataFactory.CategoriesForIndesign($scope.SelectedCatalogId, options.data.id, $scope.SelectedrecordId).success(function (response) {
                        options.success(response);
                        if (response.length == '0') {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Please! Make sure that PDF Publish and Workflow approval',
                                type: "info"
                            });
                        }
                        var treeview = $("#treeviewKendo1").data("kendoTreeView");
                        
                        $('#treeviewKendo1 ul li').each(function (i) {
                            var dataItem = treeview.dataItem(this);
                            if (dataItem.checked && dataItem.check) {
                                if (dataItem.XML_Genereted_Date != null && dataItem.Family_Modifed_Date != null) {
                                    if (dataItem.XML_Genereted_Date <= dataItem.Category_Modifed_Date) {
                                        $(this).find('.category').removeClass('category');
                                        $(this).find('.k-sprite').addClass('categorymodified');
                                        $(this).find('span.k-sprite.family.categorymodified').removeClass("categorymodified");
                                    }
                                }
                                if (dataItem.CATEGORY_ID.contains("~")) {
                                    if (dataItem.Family_Modifed_Date != null) {
                                        if (dataItem.XML_Genereted_Date <= dataItem.Family_Modifed_Date) {
                                            $(this).find('.category').removeClass('category');
                                            $(this).find('.k-sprite').addClass('categorymodified');
                                            $(this).find('span.k-sprite.family.categorymodified').removeClass("categorymodified");

                                            $(this).find('.family').addClass('familytest');
                                            $(this).find('.familytest').removeClass('family');
                                            $(this).find('.familytest').addClass('familymodified');
                                        }
                                    }
                                }
                                $(this).find('.k-checkbox-wrapper').addClass("first");
                            }
                        });

                    }).error(function (response) {
                        options.success(response);
                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    hasChildren: "hasChildren"
                }
            },

        });
        $scope.treeDataoptions = {
            checkboxes: {
                checkChildren: true,
            }, check: onCheck,
            dataSource: $scope.treeData
        };

        //---------------------------------------------------------------------------------------------------------------------------------------

        $scope.treeDataindesign = new kendo.data.HierarchicalDataSource({
            type: "json",
            loadOnDemand: true,
            transport: {
                read: function (options) {
                    dataFactory.CategoriesForIndesignfilter($scope.SelectedCatalogIdforindesign, options.data.id, $rootScope.SelectedrecordId).success(function (response) {
                        options.success(response);
                        //var treeview2 = $("#treeviewKendo11").data("kendoTreeView");
                        //$('#treeviewKendo11 ul li').each(function (i) {
                        //    var dataItem = treeview2.dataItem(this);
                        //    if (dataItem.checked && dataItem.check) {
                        //        $(this).find('.k-checkbox-wrapper').addclass("first");
                        //    }
                        //});

                    }).error(function (response) {
                        options.success(response);
                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    hasChildren: "hasChildren"
                }
            },

        });

        $scope.treeDataoptionsindesign = {
            checkboxes: {
                checkChildren: true,
            }, check: onCheckfilter,
            loadOnDemand: true,
            dataSource: $scope.treeDataindesign
        };
        //---------------------------------------------------------------------------------------------------------------------------------------

        // function that gathers IDs of checked nodes
        function checkedNodeIds(nodes, checkedNodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].checked) {
                    if (nodes[i].CATEGORY_ID.contains("~")) {
                        checkedNodes.push(nodes[i].parent().parent().CATEGORY_ID + "!" + nodes[i].CATEGORY_ID);
                    } else if (nodes[i].CATEGORY_ID && !nodes[i].check) {
                        checkedNodes.push(nodes[i].CATEGORY_ID);
                    }
                    else {
                        //checkedNodes.push(nodes[i].id);
                    }
                }

                if (nodes[i].hasChildren) {
                    checkedNodeIds(nodes[i].children.view(), checkedNodes);
                }
            }
        }
        var checkedNodes = [], message;
        // show checked node IDs on datasource change
        function onCheck() {
            checkedNodes = [];
            checkedNodeIds($scope.treeData._data, checkedNodes);

            if (checkedNodes.length > 0) {
                message = "IDs of checked nodes: " + checkedNodes.join(",");

            } else {
                message = "No nodes checked.";
            }
            //alert(message);
            //$("#result").html(message);
        }

        $scope.btnAddFamilies = function () {
            debugger
            var treeview = $("#treeviewKendo1").data("kendoTreeView");
            var items = $("#treeview .k-item input[type=checkbox]:checked").closest(".k-item");
            if ($scope.SelectedrecordId !== 0) {
                if (checkedNodes.length > 0) {
                    // message = "IDs of checked nodes: " + checkedNodes.join(",");
                    dataFactory.AddFamiliestoIndesign($scope.SelectedrecordId, $scope.SelectedCatalogId, checkedNodes).success(function (response) {
                        $scope.treeData.read();
                        debugger;
                        $scope.GetIndesignCategoryValuesDatasource.read();
                        //To refresh xml list data file
                        debugger;
                        //$scope.GetIndesignFileValuesDatasource.read();
                        if (response !== "") {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                } else {
                    //  message = "No nodes checked.";
                }
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Data File.',
                    type: "error"
                });
            }
            //To refresh xml list data file
            debugger;
            //$scope.GetIndesignFileValuesDatasource.read();
        };


        $scope.btnResetFamilies = function () {
            // $scope.treeData.read();

            if ($scope.SelectedrecordId !== 0) {
                dataFactory.ResetFamiliestoIndesign($scope.SelectedrecordId).success(function (response) {
                    //options.success(response);
                    $scope.treeData.read();
                    $scope.GetIndesignCategoryValuesDatasource.read();
                    if (response !== "") {
                        //$.msgBox({
                        //    title: $localStorage.ProdcutTitle,
                        //    content: 'Delete successful.',
                        //    type: "error"
                        //});
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Data File.',
                    type: "error"
                });
            }
        };



        $scope.btnDeleteFamilies = function () {
            // $scope.treeData.read();
            if ($scope.SelectedrecordId !== 0) {

                if (DeleteValues.length == 0) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select a product to delete.',
                        type: "info"
                    });

                }

                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Are you sure you want to delete the product?",
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }],
                        success: function (result) {
                            var bool = false;
                            if (result === "Yes") {
                                var getselectedValues = DeleteValues;
                                dataFactory.DeleteFamiliestoIndesign($scope.SelectedrecordId, DeleteValues).success(function (response) {
                                    //options.success(response);
                                    $scope.treeData.read();
                                    $scope.GetIndesignCategoryValuesDatasource.read();
                                    debugger;
                                    //To refresh xml list data file
                                    //$scope.GetIndesignFileValuesDatasource.read();
                                    if (response !== "") {
                                        $.msgBox({
                                            title: $localStorage.ProdcutTitle,
                                            content: 'Deleted successfully.',
                                            type: "error"
                                        });
                                        DeleteValues = [];
                                        $('.mc-checkbox').prop('checked', false);
                                    }

                                }).error(function (error) {
                                    options.error(error);
                                });
                            }

                        }
                    });
                }

            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Data File.',
                    type: "error"
                });
            }
        };



        $scope.previewFamily = function (e) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Product Preview may take some time to process.",
                type: "confirm",
                buttons: [{ value: "Ok" },
                { value: "Cancel" }
                ],
                success: function (result) {
                    if (result === "Ok") {

                        var grid = $("#IndesignCategoryGrid").data("kendoGrid");
                        var selectedItem = grid.dataItem(grid.select());

                        $scope.PreviewText = '';
                        if (!selectedItem.CATEGORY_ID.length) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Please select a Product.',
                                type: "error"
                            });
                        } else {


                            $("#tabledesigner").hide();
                            $("#EditMultipletable").hide();

                            $scope.previewFamilySpecsIndesign(selectedItem);
                        }
                    }
                    else {
                        return false;
                    }
                }
            });

        };

        $scope.trustAsHtml = function (string) {

            return $sce.trustAsHtml(string);
        };

        $scope.showFamily = function () {
            $("#familyPreview").hide();
            //$("#tabledesigner").hide();
            // $("#EditMultipletable").hide();
        };

        $scope.previewFamilySpecsIndesign = function (selectedItem) {

            $scope.Family.CATEGORY_ID = $rootScope.FamilyCATEGORY_ID;
            if (!selectedItem.CATEGORY_ID.length || selectedItem.FAMILY_ID == "") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Product.',
                    type: "error"
                });
            } else {
                $("#divFmlyTab").hide();
                $("#familyPreview").show();

                dataFactory.GetCsFamilyPreviewList(selectedItem.FAMILY_ID, selectedItem.CATEGORY_ID, $rootScope.selecetedCatalogId, $rootScope.FamilyLevelMultipletablePreview, $rootScope.EnableSubProduct).success(function (response) {
                    if (response != null) {

                        $scope.PreviewText = response.replace(/ITEM#/g, $localStorage.CatalogItemNumber);

                        $timeout(function () {

                            var divText = document.getElementById("dynamictableFmlyIndesign").outerHTML;
                            var myWindow = window.open('', '_blank', 'width=1600,height=700');
                            var doc = myWindow.document;
                            doc.open();
                            doc.write(divText);
                            doc.close();
                        }, 100);
                        $timeout(function () {
                            $scope.showFamily();
                        }, 150);

                        //$scope.PreviewText = response;
                    }
                }).error(function (error) {
                    options.error(error);
                });
                //        }

                //    }
                //}).error(function (error) {
                //    options.error(error);
                //});


            }

        };

        $scope.clearInDesign = function () {
            $rootScope.saveFilterAttributeValues = [];
            dataFactory.GetIndesignFilterCategoryProjectValues($scope.SelectedrecordId, $rootScope.saveFilterAttributeValues).success(function (response) {
                //$scope.SelectedrecordId = $scope.filterSelectedRecordId;
                // $scope.GetIndesignCategoryValuesDatasource.read();

                $("#IndesignCategoryGrid").data("kendoGrid").dataSource.read();
                $("#IndesignCategoryGrid").data("kendoGrid").refresh();
                $scope.showFilterText = false;
                $('#showFilterId').addClass('ng-hide');
                $scope.clearFilterButton = true;
            }).error(function (error) {
                options.error(error);
            });
            $scope.clearFilterBtnClicktreefilter();
            $rootScope.clearFlag = "0";
        }

        $scope.previewProject = function () {

            var j = 0;

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Preview may take some time to process.",
                type: "confirm",
                buttons: [{ value: "Ok" },
                { value: "Cancel" }
                ],
                success: function (result) {
                    if (result === "Ok") {
                        var CategoryId;
                        var familyId;
                        $scope.PreviewText = '';
                        $scope.PreviewChanges = '';
                        var len;
                        var grid = $("#IndesignGrid").data("kendoGrid");
                        var selectedItemProject = grid.dataItem(grid.select());
                        $scope.TESTCHANGES = false;
                        dataFactory.GetAllFamilyId(selectedItemProject.PROJECT_ID, selectedItemProject.RECORD_ID, selectedItemProject.FILE_NO).success(function (returnValue) {

                            if (returnValue != null && returnValue.length > 0) {
                                len = returnValue.length - 1;
                                var TEST;
                                for (var i = 0; i < returnValue.length; i++) {
                                    if (i == 0) {
                                        CategoryId = returnValue[i]
                                    }
                                    else {
                                        familyId = returnValue[i]
                                    }

                                    if (i != 0) {

                                        $scope.Family.CATEGORY_ID = $rootScope.FamilyCATEGORY_ID;
                                        if (!CategoryId || familyId == "") {
                                            $.msgBox({
                                                title: $localStorage.ProdcutTitle,
                                                content: 'Please select a Product.',
                                                type: "error"
                                            });
                                        } else {
                                            $("#divFmlyTab").hide();
                                            $("#familyPreview").show();

                                            dataFactory.GetCsFamilyPreviewList(familyId, CategoryId, $rootScope.selecetedCatalogId, $rootScope.FamilyLevelMultipletablePreview, $rootScope.EnableSubProduct).success(function (response) {
                                                if (response != null) {

                                                    if ($scope.PreviewChanges !== 'undefined')
                                                        $scope.PreviewChanges = $scope.PreviewChanges + response.replace(/ITEM#/g, $localStorage.CatalogItemNumber);

                                                    //   $scope.PreviewText = $scope.PreviewChanges;

                                                    j = j + 1;
                                                    if (j == len) {
                                                        $scope.TESTCHANGES = true;
                                                        $scope.PreviewText = $scope.PreviewChanges;
                                                        $timeout(function () {

                                                            var divText = document.getElementById("dynamictableFmlyIndesign").outerHTML;
                                                            var myWindow = window.open('', '_blank', 'width=1600,height=700');
                                                            var doc = myWindow.document;
                                                            doc.open();
                                                            doc.write(divText);
                                                            doc.close();
                                                        }, 100);
                                                        $timeout(function () {
                                                            $scope.showFamily();
                                                        }, 150);
                                                    }

                                                }
                                            }).error(function (error) {
                                                options.error(error);
                                            });

                                        }
                                    }

                                }
                                $("#divFmlyTab").hide();
                                $("#familyPreview").show();
                            }

                            else {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: "Please add products to the XML data file.",
                                    type: "info",
                                    buttons: [{ value: "Ok" }]
                                });
                            }

                        });

                    }
                    else {
                        return false;
                    }
                }
            });

        };

        $scope.tick = false;
        $scope.flagallcreate = false;
        $scope.createAllFiles = function () {
            $scope.flagallcreate = true;
            $scope.winInDesignAttributeSetup.refresh({ url: "../Views/App/Partials/inDesignAttributemanager.html" });
            $scope.winInDesignAttributeSetup.center().open();
        };

        $scope.DeleteIndesignValues = function (e, id) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to delete the selected Catalog Data File?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    var bool = false;
                    if (result === "Yes") {
                        bool = true;
                    }
                    if (bool === true) {
                        dataFactory.DeleteIndesignValues($scope.SelectedprojectId, id.dataItem).success(function (response) {
                            //options.success(response);

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Deleted successfully.',
                                type: "error"
                            });


                            $('#IndesignGrid').data('kendoGrid').dataSource.read();
                            $('#IndesignGrid').data('kendoGrid').refresh();
                        }).error(function (error) {
                            options.error(error);
                        });
                    };
                }
            });
        }

        $scope.DeleteIndesignCategoryValues = function (e, id) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to delete the selected Product?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    var bool = false;
                    if (result === "Yes") {
                        bool = true;
                    }
                    if (bool === true) {
                        dataFactory.DeleteIndesignCategoryValues(id.dataItem).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Deleted successfully.',
                                type: "error"
                            });
                            $scope.treeData.read();
                            $('#IndesignCategoryGrid').data('kendoGrid').dataSource.read();
                            $('#IndesignCategoryGrid').data('kendoGrid').refresh();
                            //options.success(response);

                            //To refresh xml list data file
                            debugger;
                            //$scope.GetIndesignFileValuesDatasource.read();
                        }).error(function (error) {
                            options.error(error);
                        });

                    };
                }
            });
        }

        $scope.DeleteIndesignCategoryValuesforcheckbox = function (e, id) {
            var grid = $(e.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            if (e.currentTarget.checked == true) {
                var a = id.dataItem.FAMILY_ID;
                DeleteValues.push(a);

            }

            if (e.currentTarget.checked == false) {
                for (var i = 0; i < DeleteValues.length; i++) {
                    var a = id.dataItem.FAMILY_ID;
                    var b = DeleteValues[i];

                    if (id.dataItem.FAMILY_ID == DeleteValues[i]) {
                        //DeleteValues.pop(DeleteValues[i]);
                        DeleteValues.splice(i, 1);

                    }


                }
            }

            if (DeleteValues.length == items.length) {
                $('.mc-checkbox').prop('checked', true);
            }
            else {
                $('.mc-checkbox').prop('checked', false);
            }
        }


        $scope.createAutoFileName = function () {
            if ($scope.SelectedprojectId != "") {


                $scope.tick = true;
                dataFactory.createAutoFileNameIndesign($scope.SelectedprojectId, $scope.SelectedCatalogId).success(function (response) {
                    $scope.GetIndesignFileValuesDatasource.read();
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                }).error(function (error) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + error + '.',
                        type: "info"
                    });
                });
            }
            else
            {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a indesgin project.',
                    // type: "info"
                });
            }

        };

        // InDesign Export and Import Start

        $scope.IndesignExportFormatType = '.XLS';

        $scope.exportInDesign = function () {
            if ($scope.SelectedprojectId == 0) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose a File.',
                    // type: "info"
                });
            }
            else {
                dataFactory.InDesigExport($scope.SelectedCatalogId, $rootScope.DisplayIdcolumns, $scope.IndesignExportFormatType, $scope.SelectedprojectId).success(function (response) {
                    window.open("DownloadFullExport.ashx?Path=" + response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        };
        $scope.IndesignImporthideshow = function () {
            $scope.showImport = false;
            $scope.Importvalidation = true;
        };

        $scope.selectFileforUpload = function (file) {
            debugger;
            $scope.enableReferenceImport = true;
            $scope.FileInvalidMessage = "";
            $rootScope.SelectedFileForUploadIndesign = file[0].name;
            $scope.SelectedFileForUpload = file[0];
            $scope.$apply(function () {
                $rootScope.SelectedFileForUploadIndesign = file[0].name;
            });
        };

        $scope.ChechFileValid = function (file) {

            var isValid = false;

            if ($scope.SelectedFileForUpload != null) {
                if (file.name.toLowerCase().endsWith('.xls') || file.nametoLowerCase().endsWith(".xlsx")) {// && file.size <= (512 * 2048)
                    $scope.FileInvalidMessage = "";
                    $scope.excelname = file.name;
                    isValid = true;
                }
                else {
                    $scope.FileInvalidMessage = "Selected file is Invalid. (only file type xls and xlsx is allowed)";
                    return false;
                }
            }
            else {
                $scope.FileInvalidMessage = "File required!";
                return false;
            }
            $scope.IsFileValid = isValid;
        };


        //Clear form 
        $scope.ClearForm = function () {
            $scope.FileDescription = "";
            angular.forEach(angular.element("input[type='file']"), function (inputElem) {
                angular.element(inputElem).val(null);
            });
            $scope.f1.$setPristine();
            $scope.IsFormSubmitted = false;
        }

        //Save File
        $scope.ValidateFile = function () {
            //$scope.Message = "";
            $scope.FileInvalidMessage = "";
            if ($scope.SelectedFileForUpload !== null) {
                $scope.ChechFileValid($scope.SelectedFileForUpload);
                if ($scope.IsFileValid) {
                    $scope.UploadFile($scope.SelectedFileForUpload, "Validate");
                    $scope.Importvalidation = false;
                    $scope.ImportvalidationResult = true;
                }
                else {
                    return false;
                }
            }
            else {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please choose a File.',
                    // type: "info"
                });
            }
        };
        $scope.ImportIndesignvalues = function () {
            $scope.UploadFile($scope.SelectedFileForUpload, "Import");
        }
        $scope.cancelImport = function () {
            $rootScope.SelectedFileForUploadIndesign = null;
            $rootScope.SelectedFileForUpload = null;
            $scope.showImport = true;
            $scope.Importvalidation = false;
            $scope.enableReferenceImport = false;
            $scope.ViewLogIndesignvalidation = false;
        }
        $scope.back = function () {
            $scope.ImportvalidationResult = false;
            $scope.Importvalidation = true;
            $scope.ViewLogIndesignvalidation = false;
        }
        $scope.backlog = function () {
            $rootScope.SelectedFileForUploadIndesign = null;
            $rootScope.SelectedFileForUpload = null;
            $scope.enableReferenceImport = false;
            $scope.ViewLogIndesignvalidation = false;
            $scope.showImport = false;
            $scope.Importvalidation = true;
        }
        $scope.ResetIndesignImport = function () {
            $scope.Importvalidation = true;
            $rootScope.SelectedFileForUploadIndesign = null;
            $rootScope.SelectedFileForUpload = null;
            $scope.showImport = false;
            $scope.ViewLogIndesignvalidation = false;
            $scope.showImport = false;
            $scope.Importvalidation = true;
            $scope.enableReferenceImport = false;
            var $el = $('#importfileIndesign');
            if ($el.val() != "") {
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();
            }

            if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
                $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        };
        $scope.Viewlog = function () {
            $scope.ViewLogIndesignvalidation = true;
            $scope.ImportvalidationResult = false;
            $scope.viewLogresultmissing;
            $scope.viewLogresultduplicate;
            $scope.viewLogresultnewitem;
            $scope.viewLogresult;
            //$scope.validation = false;
        }




        $scope.ResetReferenceImport = function () {
            $rootScope.SelectedFileForUploadReferenceTable = null;
            $rootScope.SelectedFileForUpload = null;
            $scope.enableReferenceImport = false;
            var $el = $('#importfile1reference');
            if ($el.val() != "") {
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();
            }

            if ($scope.excelPath != undefined && $scope.excelPath != "" && $scope.excelPath != null)
                $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        };



        $scope.newxmlname1 = "";
        $scope.excelPath = "";
        $scope.UploadFile = function (file, data) {
            var formData = new FormData();
            formData.append("file", file);
            if (data == "Validate") {
                //We can send more data to server using append         
                //var defer = $q.defer();
                $http.post("/Catalog/SaveFilesIndesgin?XMLName=" + $scope.newxmlname1 + "", formData,
                    {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                    .success(function (response) {
                        $scope.response = response;
                        var tempArr = [];
                        var missing = [];
                        var dupl = [];
                        var newitem = [];
                        if ($scope.response.length != 0) {
                            $scope.validation = false;
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Validation Unsuccessful.',
                                type: "error"
                            });
                            angular.forEach($scope.response, function (value, key) {
                                if (value == "Missing Column") {
                                    $scope.missing = true;
                                    tempArr.push(value);
                                }
                                else if (value == "New Item") {
                                    $scope.newitem = true;
                                    tempArr.push(value);
                                }
                                else if (value == "DuplicateItem") {
                                    $scope.duplicate = true;
                                    tempArr.push(value);
                                }
                                //tempArr.push(key + ': ' + value);
                            });
                            angular.forEach($scope.response, function (value, key) {
                                if (value.includes("Missing-")) {
                                    missing.push(value);
                                }
                                else if (value.includes("Dup-")) {
                                    dupl.push(value);
                                }
                                else if (value.includes("New-")) {
                                    newitem.push(value);
                                }
                            });
                            $scope.viewLogresultmissing = []; $scope.viewLogresultduplicate = []; $scope.viewLogresultnewitem = []; $scope.viewLogresult = [];
                            $scope.viewLogresultmissing = missing;
                            $scope.viewLogresultduplicate = dupl;
                            $scope.viewLogresultnewitem = newitem;
                            $scope.viewLogresult = tempArr;
                            //if ($scope.response[0] == "Missing Column")
                            //    $scope.missing = true;
                            //if ($scope.response[0] == "New Item" || $scope.response[1] == "New Item")
                            //    $scope.newitem = true;
                            //if ($scope.response[0] == "DuplicateItem" || $scope.response[1] == "DuplicateItem" || $scope.response[2] == "DuplicateItem")
                            //    $scope.duplicate = true;
                        }
                        else if ($scope.response.length == 0) {
                            $scope.validation = true;
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Validation Successful.',
                                type: "error"
                            });
                            //$.msgBox({
                            //    title: $localStorage.ProdcutTitle,
                            //    content: 'Import success',
                            //    type: "info"
                            //});
                            $scope.ImportvalidationResult = true;
                            // $scope.showImport = true;
                        }

                        return true;
                    })
                    .error(function () {
                        $scope.validation = false;
                        $scope.missing = false;
                        $scope.newitem = false;
                        $scope.duplicate = false;
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Import failed',
                            type: "error"
                        });
                    });
            }
            else if (data == "Import") {
                $http.post("/Catalog/ImportIndesgin?XMLName=" + $scope.newxmlname1 + "", formData,
                    {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                    .success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: response,
                            type: "info"
                        });
                        $scope.ImportvalidationResult = false;
                        $scope.showImport = true;
                        $('#IndesignGrid').data('kendoGrid').dataSource.read();
                        $('#IndesignGrid').data('kendoGrid').refresh();
                        $("#IndesignCategoryGrid").data("kendoGrid").dataSource.read();
                        $("#IndesignCategoryGrid").data("kendoGrid").refresh();
                        // $scope.GetIndesignCategoryValuesDatasource.read();
                        $scope.projectDataSource.read();
                    })
            }

        };
        $scope.export_log = function () {
            dataFactory.validationresult($scope.Errorlogoutputformat, $rootScope.SelectedFileForUploadIndesign).success(function (response) {
                window.open("DownloadFile.ashx?Path=" + response + "&importType=" + $scope.ImportType);
            }).error(function (error) {
                options.error(error);
            });
        };
        // InDesign Export and Import End

        $scope.init = function () {

            if ($localStorage.getCatalogID === undefined) {
                $scope.SelectedCatalogId = 0;
            }
            else {
                $scope.SelectedCatalogId = $localStorage.getCatalogID;
                $scope.SelectedCatalogName = $localStorage.getCatalogName;
            }
            if ($localStorage.getCustomerID == undefined) {
                $scope.getCustomerID = 0;

            }
            else {
                $scope.getCustomerID = $localStorage.getCustomerID;
            }

            //if ($scope.SelectedCatalogId == '') {
            //    $scope.SelectedCatalogId = $localStorage.getCatalogID;
            //}
            $scope.clearFilterButton = true;
            $scope.ClearAllFilter();
            $("#divselectcatalog").show();
            $scope.roleidcheck();
        };
        $scope.init();

        /////////////--------------------------------Start - InDesign attribute wise filter - Aswin kumar------------------------------//////

        $scope.FilterInDesignClick = function () {
            $scope.SelectedCatalogIdforindesign = $scope.SelectedCatalogId;
            $scope.InDesignFilterWindow.refresh({ url: "../Views/App/Partials/InDesignFilterPopup.html" });
            $scope.InDesignFilterWindow.center().open();
            $scope.applyProductFilterBtnDisabled = true;
            $scope.GetFilters1.read();
            $scope.GetFilters2.read();
            $scope.GetFilters3.read();
            $scope.GetFilters4.read();
            $scope.GetFilters5.read();
            $scope.treeDataindesign.read();
        }

        $scope.GetFilters1 = new kendo.data.DataSource({

            type: "json",
            serverFiltering: true,

            transport: {
                read: function (options) {
                    dataFactory.GetIndesignAllFiltersValue($scope.seletedAttribute1).success(function (response) {
                        if (response.length != 0) {
                            $scope.selectedFilters1 = response[0].FilterName;
                            $scope.usewildcards = response[0].FilterId;
                            if ($scope.Filterdatatype == "Date") {
                                $scope.typeInput = 'text';
                                //$scope.typeInput = false;


                                if ($scope.usewildcards == "14") {

                                    $scope.hidenValue = false;
                                    $scope.endDateHide = true;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = false;
                                }
                                else if ($scope.usewildcards != undefined) {
                                    $scope.hidenValue = true;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = false;
                                }
                                else {
                                    $scope.hidenSearchText = true;
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.numericTextHiden = false;
                                }
                            }
                            else if ($scope.Filterdatatype == "Number") {

                                $scope.typeInput = 'number';


                                //var clean = $scope.searchtxt.replace(/[^-0-9\.]/g, '');
                                //var negativeCheck = clean.split('-');
                                //var decimalCheck = clean.split('.');
                                if ($scope.usewildcards == "14") {

                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = true;
                                }
                                else if ($scope.usewildcards != undefined) {
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = true;
                                    $scope.numericTextHiden = false;
                                }
                                else {
                                    $scope.hidenSearchText = true;
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.numericTextHiden = false;
                                }
                            }
                            else {
                                $scope.typeInput = 'text';

                                $scope.hidenSearchText = true;
                                $scope.hidenValue = false;
                                $scope.endDateHide = false;
                                $scope.numericTextHiden = false;
                            }

                        }
                        else {
                            $scope.selectedFilters = "";
                            $scope.typeInput = 'text';

                        }

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });


        $scope.GetFilters2 = new kendo.data.DataSource({

            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetIndesignAllFiltersValue($scope.seletedAttribute2).success(function (response) {
                        if (response.length != 0) {
                            $scope.selectedFilters2 = response[0].FilterName;
                            $scope.usewildcards = response[0].FilterId;
                            if ($scope.Filterdatatype == "Date") {
                                $scope.typeInput = 'text';
                                //$scope.typeInput = false;


                                if ($scope.usewildcards == "14") {

                                    $scope.hidenValue = false;
                                    $scope.endDateHide = true;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = false;
                                }
                                else if ($scope.usewildcards != undefined) {
                                    $scope.hidenValue = true;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = false;
                                }
                                else {
                                    $scope.hidenSearchText = true;
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.numericTextHiden = false;
                                }
                            }
                            else if ($scope.Filterdatatype == "Number") {

                                $scope.typeInput = 'number';


                                //var clean = $scope.searchtxt.replace(/[^-0-9\.]/g, '');
                                //var negativeCheck = clean.split('-');
                                //var decimalCheck = clean.split('.');
                                if ($scope.usewildcards == "14") {

                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = true;
                                }
                                else if ($scope.usewildcards != undefined) {
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = true;
                                    $scope.numericTextHiden = false;
                                }
                                else {
                                    $scope.hidenSearchText = true;
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.numericTextHiden = false;
                                }
                            }
                            else {
                                $scope.typeInput = 'text';

                                $scope.hidenSearchText = true;
                                $scope.hidenValue = false;
                                $scope.endDateHide = false;
                                $scope.numericTextHiden = false;
                            }

                        }
                        else {
                            $scope.selectedFilters = "";
                            $scope.typeInput = 'text';

                        }

                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });



        $scope.GetFilters3 = new kendo.data.DataSource({

            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetIndesignAllFiltersValue($scope.seletedAttribute3).success(function (response) {
                        if (response.length != 0) {

                            $scope.selectedFilters3 = response[0].FilterName;
                            $scope.usewildcards = response[0].FilterId;
                            if ($scope.Filterdatatype == "Date") {
                                $scope.typeInput = 'text';
                                //$scope.typeInput = false;


                                if ($scope.usewildcards == "14") {

                                    $scope.hidenValue = false;
                                    $scope.endDateHide = true;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = false;
                                }
                                else if ($scope.usewildcards != undefined) {
                                    $scope.hidenValue = true;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = false;
                                }
                                else {
                                    $scope.hidenSearchText = true;
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.numericTextHiden = false;
                                }
                            }
                            else if ($scope.Filterdatatype == "Number") {

                                $scope.typeInput = 'number';


                                //var clean = $scope.searchtxt.replace(/[^-0-9\.]/g, '');
                                //var negativeCheck = clean.split('-');
                                //var decimalCheck = clean.split('.');
                                if ($scope.usewildcards == "14") {

                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = true;
                                }
                                else if ($scope.usewildcards != undefined) {
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = true;
                                    $scope.numericTextHiden = false;
                                }
                                else {
                                    $scope.hidenSearchText = true;
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.numericTextHiden = false;
                                }
                            }
                            else {
                                $scope.typeInput = 'text';

                                $scope.hidenSearchText = true;
                                $scope.hidenValue = false;
                                $scope.endDateHide = false;
                                $scope.numericTextHiden = false;
                            }

                        }
                        else {
                            $scope.selectedFilters = "";
                            $scope.typeInput = 'text';

                        }

                        options.success(response);
                        // $scope.retainKendoValues();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.GetFilters4 = new kendo.data.DataSource({

            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetIndesignAllFiltersValue($scope.seletedAttribute4).success(function (response) {
                        if (response.length != 0) {

                            $scope.selectedFilters4 = response[0].FilterName;
                            $scope.usewildcards = response[0].FilterId;
                            if ($scope.Filterdatatype == "Date") {
                                $scope.typeInput = 'text';
                                //$scope.typeInput = false;
                                if ($scope.usewildcards == "14") {
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = true;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = false;
                                }
                                else if ($scope.usewildcards != undefined) {
                                    $scope.hidenValue = true;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = false;
                                }
                                else {
                                    $scope.hidenSearchText = true;
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.numericTextHiden = false;
                                }
                            }
                            else if ($scope.Filterdatatype == "Number") {

                                $scope.typeInput = 'number';

                                if ($scope.usewildcards == "14") {

                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = true;
                                }
                                else if ($scope.usewildcards != undefined) {
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = true;
                                    $scope.numericTextHiden = false;
                                }
                                else {
                                    $scope.hidenSearchText = true;
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.numericTextHiden = false;
                                }
                            }
                            else {
                                $scope.typeInput = 'text';
                                $scope.hidenSearchText = true;
                                $scope.hidenValue = false;
                                $scope.endDateHide = false;
                                $scope.numericTextHiden = false;
                            }

                        }
                        else {
                            $scope.selectedFilters = "";
                            $scope.typeInput = 'text';
                        }
                        options.success(response);
                        // $scope.retainKendoValues();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.GetFilters5 = new kendo.data.DataSource({

            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetIndesignAllFiltersValue($scope.seletedAttribute5).success(function (response) {
                        if (response.length != 0) {

                            $scope.selectedFilters5 = response[0].FilterName;
                            $scope.usewildcards = response[0].FilterId;
                            if ($scope.Filterdatatype == "Date") {
                                $scope.typeInput = 'text';
                                //$scope.typeInput = false;


                                if ($scope.usewildcards == "14") {

                                    $scope.hidenValue = false;
                                    $scope.endDateHide = true;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = false;
                                }
                                else if ($scope.usewildcards != undefined) {
                                    $scope.hidenValue = true;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = false;
                                }
                                else {
                                    $scope.hidenSearchText = true;
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.numericTextHiden = false;
                                }
                            }
                            else if ($scope.Filterdatatype == "Number") {

                                $scope.typeInput = 'number';

                                if ($scope.usewildcards == "14") {

                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = false;
                                    $scope.numericTextHiden = true;
                                }
                                else if ($scope.usewildcards != undefined) {
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.hidenSearchText = true;
                                    $scope.numericTextHiden = false;
                                }
                                else {
                                    $scope.hidenSearchText = true;
                                    $scope.hidenValue = false;
                                    $scope.endDateHide = false;
                                    $scope.numericTextHiden = false;
                                }
                            }
                            else {
                                $scope.typeInput = 'text';
                                $scope.hidenSearchText = true;
                                $scope.hidenValue = false;
                                $scope.endDateHide = false;
                                $scope.numericTextHiden = false;
                            }

                        }
                        else {
                            $scope.selectedFilters = "";
                            $scope.typeInput = 'text';
                        }

                        options.success(response);
                        // $scope.retainKendoValues();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });


        $scope.IndesignProductAttributeChange1 = function (e) {
            $scope.seletedAttribute1 = e.sender.value();
            $scope.GetFilters1.read();


        }

        $scope.IndesignProductAttributeChange2 = function (e) {
            $scope.seletedAttribute2 = e.sender.value();
            $scope.GetFilters2.read();
        }


        $scope.IndesignProductAttributeChange3 = function (e) {
            $scope.seletedAttribute3 = e.sender.value();
            $scope.GetFilters3.read();
        }

        $scope.IndesignProductAttributeChange4 = function (e) {
            $scope.seletedAttribute4 = e.sender.value();
            $scope.GetFilters4.read();
        }

        $scope.IndesignProductAttributeChange5 = function (e) {
            $scope.seletedAttribute5 = e.sender.value();
            $scope.GetFilters5.read();
        }

        $scope.SelectItem1 = function (e) {
            $scope.filterValue1 = e;

        }

        $scope.SelectItem2 = function (e) {
            $scope.filterValue2 = e;
        }

        $scope.SelectItem3 = function (e) {
            $scope.filterValue3 = e;
        }

        $scope.SelectItem4 = function (e) {
            $scope.filterValue4 = e;
        }

        $scope.SelectItem5 = function (e) {
            $scope.filterValue5 = e;
        }

        $scope.searchTextKeyup1 = function (event) {
            $scope.searchText1 = event.currentTarget.value;
        }

        $scope.searchTextKeyup2 = function (event) {
            $scope.searchText2 = event.currentTarget.value;
        }

        $scope.searchTextKeyup3 = function (event) {
            $scope.searchText3 = event.currentTarget.value;
        }

        $scope.searchTextKeyup4 = function (event) {
            $scope.searchText4 = event.currentTarget.value;
        }

        $scope.searchTextKeyup5 = function (event) {
            $scope.searchText5 = event.currentTarget.value;
        }



        $scope.IndesignFilterAttributeTypeDataSource = new kendo.data.DataSource({
            type: "json",
            transport: {
                read: function (options) {
                    dataFactory.getProductAttributeType().success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });


        $scope.IndesignAttributeTypeChange1 = function (event) {
            $scope.productAttributeName1 = event.sender._old;
            $rootScope.productAttributeNameChange1 = $scope.productAttributeName1;
            $scope.flag1 = "0";

            if ($scope.productAttributeName1 == "Product Specfications") {
                $scope.productAttributeType = "1";
                $scope.flag1 = "1";
            }

            else if ($scope.productAttributeName1 == "Product Price") {
                $scope.productAttributeType = "4";
                $scope.flag1 = "1";
            }
            else if ($scope.productAttributeName1 == "Product Key") {
                $scope.productAttributeType = "6";
                $scope.flag1 = "1";
            }
            else {
                $scope.flag1 = "0";
            }
            $scope.disableEnableApplyBtn();

            $scope.IndesignFilterProductAttributeNameDataSource1 = new kendo.data.DataSource({
                type: "json",
                autoBind: false,
                sort: { field: "productAttributeProductFilterName", dir: "asc" },
                transport: {
                    read: function (options) {
                        dataFactory.getIndesignProductAttributeNames($scope.productAttributeType).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
            // $scope.retainKendoValues();
        }

        $scope.IndesignAttributeTypeChange2 = function (event) {
            $scope.productAttributeName2 = event.sender._old;
            $rootScope.productAttributeNameChange2 = $scope.productAttributeName2;
            $scope.flag2 = "0";

            if ($scope.productAttributeName2 == "Product Specfications") {
                $scope.productAttributeType = "1";
                $scope.flag2 = "1";
            }

            else if ($scope.productAttributeName2 == "Product Price") {
                $scope.productAttributeType = "4";
                $scope.flag2 = "1";
            }
            else if ($scope.productAttributeName2 == "Product Key") {
                $scope.productAttributeType = "6";
                $scope.flag2 = "1";
            }
            else {
                $scope.flag2 = "0";
            }

            $scope.disableEnableApplyBtn();

            $scope.IndesignFilterProductAttributeNameDataSource2 = new kendo.data.DataSource({
                type: "json",
                autoBind: false,
                sort: { field: "productAttributeProductFilterName", dir: "asc" },
                transport: {
                    read: function (options) {
                        dataFactory.getIndesignProductAttributeNames($scope.productAttributeType).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });


            //  $scope.retainKendoValues();
        }

        $scope.IndesignAttributeTypeChange3 = function (event) {
            $scope.productAttributeName3 = event.sender._old;
            $rootScope.productAttributeNameChange3 = $scope.productAttributeName3;
            $scope.flag3 = "0";

            if ($scope.productAttributeName3 == "Product Specfications") {
                $scope.productAttributeType = "1";
                $scope.flag3 = "1";
            }
            else if ($scope.productAttributeName3 == "Product Price") {
                $scope.productAttributeType = "4";
                $scope.flag3 = "1";
            }
            else if ($scope.productAttributeName3 == "Product Key") {
                $scope.productAttributeType = "6";
                $scope.flag3 = "1";
            }
            else {
                $scope.flag3 = "0";
            }
            $scope.disableEnableApplyBtn();

            $scope.IndesignFilterProductAttributeNameDataSource3 = new kendo.data.DataSource({
                type: "json",
                autoBind: false,
                sort: { field: "productAttributeProductFilterName", dir: "asc" },
                transport: {
                    read: function (options) {
                        dataFactory.getIndesignProductAttributeNames($scope.productAttributeType).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
            // $scope.retainKendoValues();
        }


        $scope.IndesignAttributeTypeChange4 = function (event) {
            $scope.productAttributeName4 = event.sender._old;
            $rootScope.productAttributeNameChange4 = $scope.productAttributeName4;
            $scope.flag4 = "0";

            if ($scope.productAttributeName4 == "Product Specfications") {
                $scope.productAttributeType = "1";
                $scope.flag4 = "1";
            }
            else if ($scope.productAttributeName4 == "Product Price") {
                $scope.productAttributeType = "4";
                $scope.flag4 = "1";
            }
            else if ($scope.productAttributeName4 == "Product Key") {
                $scope.productAttributeType = "6";
                $scope.flag4 = "1";
            }
            else {
                $scope.flag4 = "0";
            }
            $scope.disableEnableApplyBtn();

            $scope.IndesignFilterProductAttributeNameDataSource4 = new kendo.data.DataSource({
                type: "json",
                autoBind: false,
                sort: { field: "productAttributeProductFilterName", dir: "asc" },
                transport: {
                    read: function (options) {
                        dataFactory.getIndesignProductAttributeNames($scope.productAttributeType).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
            // $scope.retainKendoValues();
        }

        $scope.IndesignAttributeTypeChange5 = function (event) {
            $scope.productAttributeName5 = event.sender._old;
            $rootScope.productAttributeNameChange5 = $scope.productAttributeName5;
            $scope.flag5 = "0";

            if ($scope.productAttributeName5 == "Product Specfications") {
                $scope.productAttributeType = "1";
                $scope.flag5 = "1";
            }
            else if ($scope.productAttributeName5 == "Product Price") {
                $scope.productAttributeType = "4";
                $scope.flag5 = "1";
            }
            else if ($scope.productAttributeName5 == "Product Key") {
                $scope.productAttributeType = "6";
                $scope.flag5 = "1";
            }
            else {
                $scope.flag5 = "0";
            }
            $scope.disableEnableApplyBtn();

            $scope.IndesignFilterProductAttributeNameDataSource5 = new kendo.data.DataSource({
                type: "json",
                autoBind: false,
                sort: { field: "productAttributeProductFilterName", dir: "asc" },
                transport: {
                    read: function (options) {
                        dataFactory.getIndesignProductAttributeNames($scope.productAttributeType).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
            // $scope.retainKendoValues();
        }


        $scope.applyProductFilterBtnClick = function () {

            $scope.validateflag = "0";


            if ($scope.productAttributeName1 == "") {
                $scope.productAttributeName1 = undefined;
            }

            else if ($scope.productAttributeName2 == "") {
                $scope.productAttributeName2 = undefined;

            }
            else if ($scope.productAttributeName3 == "") {
                $scope.productAttributeName3 = undefined;

            }

            else if ($scope.productAttributeName4 == "") {
                $scope.productAttributeName4 = undefined;

            }
            else if ($scope.productAttributeName5 == "") {
                $scope.productAttributeName5 = undefined;

            }


            if ($scope.productAttributeName1 != undefined) {
                if ($scope.seletedAttribute1 == undefined || $scope.searchText1 == undefined) {
                    $scope.validateflag = "1";

                }

            }

            if ($scope.productAttributeName2 != undefined) {

                if ($scope.seletedAttribute2 == undefined || $scope.searchText2 == undefined) {

                    $scope.validateflag = "1";

                }
            }

            if ($scope.productAttributeName3 != undefined) {

                if ($scope.seletedAttribute3 == undefined || $scope.searchText3 == undefined) {

                    $scope.validateflag = "1";

                }
            }

            if ($scope.productAttributeName4 != undefined) {

                if ($scope.seletedAttribute4 == undefined || $scope.searchText4 == undefined) {

                    $scope.validateflag = "1";

                }
            }

            if ($scope.productAttributeName5 != undefined) {

                if ($scope.seletedAttribute5 == undefined || $scope.searchText5 == undefined) {

                    $scope.validateflag = "1";

                }
            }
            if ($scope.validateflag == "1") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please fill all the fields.',
                    type: "info"
                });
            }


            if ($scope.validateflag == "0") {


                if ($scope.filterValue1 == undefined) {
                    $scope.filterValue1 = $scope.selectedFilters1;
                }

                if ($scope.filterValue2 == undefined) {
                    $scope.filterValue2 = $scope.selectedFilters2;
                }

                if ($scope.filterValue3 == undefined) {
                    $scope.filterValue3 = $scope.selectedFilters3;
                }

                if ($scope.filterValue4 == undefined) {
                    $scope.filterValue4 = $scope.selectedFilters3;
                }

                if ($scope.filterValue5 == undefined) {
                    $scope.filterValue5 = $scope.selectedFilters3;
                }

                $rootScope.saveFilterAttributeValues.push({ productAttributes: $scope.seletedAttribute1, filterCondition: $scope.filterValue1, filterValue: $scope.searchText1 });
                $rootScope.saveFilterAttributeValues.push({ productAttributes: $scope.seletedAttribute2, filterCondition: $scope.filterValue2, filterValue: $scope.searchText2 });
                $rootScope.saveFilterAttributeValues.push({ productAttributes: $scope.seletedAttribute3, filterCondition: $scope.filterValue3, filterValue: $scope.searchText3 });
                $rootScope.saveFilterAttributeValues.push({ productAttributes: $scope.seletedAttribute4, filterCondition: $scope.filterValue4, filterValue: $scope.searchText4 });
                $rootScope.saveFilterAttributeValues.push({ productAttributes: $scope.seletedAttribute5, filterCondition: $scope.filterValue5, filterValue: $scope.searchText5 });
                $scope.InDesignFilterWindow.center().close();

                for (var i = 0; i < $rootScope.saveFilterAttributeValues.length; i++) {
                    if ($rootScope.saveFilterAttributeValues[i].productAttributes != undefined || $rootScope.saveFilterAttributeValues[i].filterValue != undefined) {
                        $scope.showFilterText = true;
                        $('#showFilterId').removeClass('ng-hide');
                        break;
                    }

                    else {
                        $scope.showFilterText = false;
                        $('#showFilterId').addClass('ng-hide');
                        break;
                    }
                }

            }


            if ($scope.showFilterText = true) {
                $scope.clearFilterButton = false;
            }
            else {
                $scope.clearFilterButton = true;
            }
            $scope.GetIndesignFilterCategoryProjectValues();

        }


        $scope.clearFilterBtnClick = function () {
            $scope.GetFilters1.read();
            $scope.GetFilters2.read();
            $scope.GetFilters3.read();
            $scope.GetFilters4.read();
            $scope.GetFilters5.read();

            $scope.searchText1 = "";
            $scope.searchText2 = "";
            $scope.searchText3 = "";
            $scope.searchText4 = "";
            $scope.searchText5 = "";

            $scope.cancelInDesign();
        }

        $scope.cancelFilterBtnClick = function () {
            $scope.InDesignFilterWindow.center().close();
        }

        $scope.Removefilter1BtnClick = function () {
            $scope.GetFilters1.read();
            $scope.searchText1 = "";
        }

        $scope.Removefilter2BtnClick = function () {
            $scope.GetFilters2.read();
            $scope.searchText2 = "";
        }

        $scope.Removefilter3BtnClick = function () {
            $scope.GetFilters3.read();
            $scope.searchText3 = "";
        }
        $scope.Removefilter4BtnClick = function () {
            $scope.GetFilters4.read();
            $scope.searchText4 = "";
        }

        $scope.Removefilter5BtnClick = function () {
            $scope.GetFilters5.read();
            $scope.searchText5 = "";
        }


        $scope.GetIndesignFilterCategoryProjectValues = function () {
            dataFactory.GetIndesignFilterCategoryProjectValues($scope.filterSelectedRecordId, $rootScope.saveFilterAttributeValues).success(function (response) {
                $scope.SelectedrecordId = $scope.filterSelectedRecordId;
                // $scope.GetIndesignCategoryValuesDatasource.read();

                $("#IndesignCategoryGrid").data("kendoGrid").dataSource.read();
                $("#IndesignCategoryGrid").data("kendoGrid").refresh();
            }).error(function (error) {
                options.error(error);
            });
        };



        $scope.disableEnableApplyBtn = function () {
            if ($scope.flag1 == undefined) {
                $scope.flag1 = "0";
            }

            else if ($scope.flag2 == undefined) {
                $scope.flag2 = "0"
            }

            else if ($scope.flag3 == undefined) {
                $scope.flag3 = "0"
            }

            else if ($scope.flag4 == undefined) {
                $scope.flag4 = "0"
            }

            else if ($scope.flag5 == undefined) {
                $scope.flag5 = "0"
            }

            if ($scope.flag1 == "0" && $scope.flag2 == "0" && $scope.flag3 == "0" && $scope.flag4 == "0" && $scope.flag5 == "0") {
                $scope.applyProductFilterBtnDisabled = true;
            }
            else {
                $scope.applyProductFilterBtnDisabled = false;//enable
            }
        }

        //----------------------------------------------------------------------------
        function onCheckfilter() {
            checkedNodes = [];
            checkedNodesforroot = [];
            checkedNodeIds($scope.treeDataindesign._data, checkedNodes);

            if (checkedNodes.length > 0) {
                message = "IDs of checked nodes: " + checkedNodes.join(",");

            } else {
                message = "No nodes checked.";
            }
        }



        $scope.applyProductFilterBtnClickfilter = function () {
            debugger
            //if ($scope.GetFilters1.read() != undefined || $scope.GetFilters2.read() != undefined || $scope.GetFilters3.read() != undefined || $scope.GetFilters4.read() != undefined || $scope.GetFilters5.read() != undefined)
            //{ $scope.clearFilterBtnClick(); }
            dataFactory.GetIndesignFilterCategoryProjectValuesfilter($rootScope.SelectedrecordId, $scope.SelectedCatalogIdforindesign, checkedNodes).success(function (response) {
                // $scope.SelectedrecordId = $scope.filterSelectedRecordId;
                // $scope.GetIndesignCategoryValuesDatasource.read();

                $("#IndesignCategoryGrid").data("kendoGrid").dataSource.read();
                $("#IndesignCategoryGrid").data("kendoGrid").refresh();
            }).error(function (error) {
                options.error(error);
            });


            if (checkedNodes[0] != null) {
                $scope.showFilterText = true;
                $('#showFilterId').removeClass('ng-hide');

            }

            else {
                $scope.showFilterText = false;
                $('#showFilterId').addClass('ng-hide');


            }


            if ($scope.showFilterText = true) {
                $scope.clearFilterButton = false;
                $rootScope.clearFlag = "1";
            }
            else {
                $scope.clearFilterButton = true;
                $rootScope.clearFlag = "0";
            }

            $scope.InDesignFilterWindow.center().close();
        }
        $scope.clearFilterBtnClicktreefilter = function () {


            $scope.treeDataindesign.read();
        }



        $scope.workflowitemChangeIndesign = function (e) {
            debugger;
            $scope.Indesign.STATUS_NAME = e.sender.value();
        };

        $scope.workflowDataSourceIndesign = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.getWorkflowforindesign($scope.SelectedprojectId).success(function (response) {
                        //CategoriesForIndesign
                        options.success(response);
                        $scope.Indesign.STATUS_NAME = response[0];

                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });


        $scope.bindworkflowforindesign = new kendo.data.DataSource({
            type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.bindWorkflowforindesign($scope.SelectedprojectId).success(function (response) {
                        
                        options.success(response);
                        //$scope.Indesign.STATUS_NAME = response;
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        //        $scope.indesignworkflowsave =  function() {
        //            datafactory.indesignworkflowsave($scope.SelectedprojectId,$scope.Indesign.STATUS_NAME).success(function (response) {
        //            options.success(response);
        //            }).error(function(error){
        //                options.error(error);
        //            });         
        //};

        $scope.indesignworkflowsave = function () {
            dataFactory.indesignworkflowsave($scope.SelectedprojectId, $scope.Indesign.STATUS_NAME).success(function (response) {
                if (response == "Saved.") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Saved successfully.',
                        type: "info",
                    });

                }
                $scope.workflowDataSourceIndesign.read();

            });
        }
        //------------------------------------------------------------------------------

        //$scope.retainKendoValues = function()
        //{
        //    $rootScope.retainproductAttributeName1 = $rootScope.productAttributeNameChange1;
        //    $rootScope.retainproductAttributeName2 = $rootScope.productAttributeNameChange2;
        //    $rootScope.retainproductAttributeName3 = $rootScope.productAttributeNameChange3;

        //    if ($rootScope.retainproductAttributeName1 == "")
        //    {
        //        $("#attributetypefilter1").data('kendo-drop-down-list').select(0);
        //    }
        //    if ($rootScope.retainproductAttributeName2 == "") {
        //        $("#attributetypefilter2").data('kendo-drop-down-list').select(0);
        //    }
        //    if ($rootScope.retainproductAttributeName3== "") {
        //        $("#attributetypefilter3").data('kendo-drop-down-list').select(0);
        //    }

        //    $("#attributetypefilter1").data('kendo-drop-down-list').value($rootScope.retainproductAttributeName1);
        //    $("#attributetypefilter2").data('kendo-drop-down-list').value($rootScope.retainproductAttributeName2);
        //    $("#attributetypefilter3").data('kendo-drop-down-list').value($rootScope.retainproductAttributeName3);
        //}

        /////////////--------------------------------End - InDesign attribute wise filter - Aswin kumar------------------------------//////


    }]);
