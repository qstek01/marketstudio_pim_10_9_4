LSApp.controller('catalogController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$filter', 'NgTableParams', '$compile', 'productService', '$localStorage', 'blockUI', '$rootScope',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $filter, NgTableParams, $compile, productService, $localStorage, blockUI, $rootScope) {
        // --------------------------------------------------- Start Catalog--------------------------------------------------------------------------------
        $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
        $rootScope.items = [];
        $scope.flag = 0;
        $scope.Catalog = {
            CATALOG_NAME: '',
            CATALOG_ID: 0,
            VERSION: '1.0',
            DESCRIPTION: 'Sample Catalog',
            FAMILY_FILTERS: '',
            PRODUCT_FILTERS: '',
            familyfilter: [],
            productfilter: [],
            DEFAULT_FAMILY: ''
        };
        $scope.cancelDeleteFlag = false;
        $scope.DEFAULT_FAMILY_NAME = '';
        $scope.selects = function (catalogid) {

            $http.get("../Catalog/GetCatalog?catalogId=" + catalogid).
              then(function (objcategoryDetails) {

                  $scope.Catalog = objcategoryDetails.data[0];
                  $("#showcatalogTab").show();
                  $scope.Catalog.DEFAULT_FAMILY = objcategoryDetails.data[0].DEFAULT_FAMILY;
                  $scope.DEFAULT_FAMILY = objcategoryDetails.data[0].DEFAULT_FAMILY;
                  if ($scope.newcatalogFlag === "1" && $scope.Catalog.CATALOG_ID != 0) {
                      $("#showcatalogTab").hide();

                      $scope.Catalog.CATALOG_NAME = "",
                     $scope.Catalog.VERSION = "",
                    $scope.Catalog.DESCRIPTION = "";
                      $scope.Catalog.DEFAULT_FAMILY = "";
                  }

                  $scope.itemsToAdd = objcategoryDetails.data[0].familyfilter;
                  $scope.prodItemsToAdd = objcategoryDetails.data[0].productfilter;
                  angular.forEach($scope.itemsToAdd, function (value, key) {

                      $scope.GetFamilyAttr(value, $scope.Catalog.CATALOG_ID);
                  });
                  angular.forEach($scope.prodItemsToAdd, function (value, key) {

                      $scope.GetProductAttr(value, $scope.Catalog.CATALOG_ID);
                  });

              });
        };
        $scope.GetFamilyAttr = function (attributeId, catalogid) {
            attributeId.Spec_List = new kendo.data.DataSource({
                //type: "json",
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        dataFactory.familyattrvalues(attributeId.Attribute_id, catalogid).success(function (response) {

                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
        };
        $scope.GetProductAttr = function (attributeId, catalogid) {

            attributeId.Spec_List = new kendo.data.DataSource({
                // type: "json",
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        dataFactory.productattrvalues(attributeId.Attribute_id, catalogid).success(function (response) {

                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
        };

        $scope.products = productService.getProducts();

        $scope.catalogDataSource = new kendo.data.DataSource({
            // type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCatalogDetails($scope.SelectedItem, $scope.getCustomerID).success(function (response) {
                        blockUI.start();
                        options.success(response);
                        blockUI.stop();
                        $scope.selectcatalogattr($scope.Catalog.CATALOG_ID);
                        $scope.defaultfamily();
                        //if ($scope.Catalog.CATALOG_ID != '')
                        //{ $scope.defaultfamily(); }
                        
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            cache: false
        });

        $scope.attrcount = 0;
        $scope.prodimageattrcount = 0;
        $scope.familydesccount = 0;
        $scope.prodpriceattrcount = 0;
        $scope.prodkeyattrcount = 0;
        $scope.familyimgcount = 0;
        $scope.familyattrcount = 0;
        $scope.familypricecount = 0;
        $scope.familykeycount = 0;
        $scope.catalogChange = function (e) {

            $scope.TempClearPdfTemplate();

            $scope.SelectedFileForUpload = undefined;
            $scope.Catalog.CATALOG_NAME = e.sender.text();
            if ($scope.Catalog.CATALOG_NAME == "--- New Catalog ---") {
                $scope.DEFAULT_FAMILY_NAME = 'Default Product';
                $("#new").show();
                $("#mirror").hide();
                $scope.Catalog.CATALOG_NAME = "";
                $scope.Catalog.DESCRIPTION = "";
                $scope.newcatalogFlag = "0";
                document.getElementById("newcatalog").disabled = true;
                $scope.cancelDeleteFlag = true;
                $scope.familyDataSource.read();
                $("#showcatalogTab").hide();
                $scope.SelectedFileForUploadnameSetup = "";
            }
            else if (e.sender.value() !== "") {
                $("#new").show();
                $("#mirror").hide();
                $scope.cancelDeleteFlag = false;
                $scope.newcatalogFlag = "0";
                document.getElementById("newcatalog").disabled = false;
                $localStorage.getCatalogID = e.sender.value();


                $scope.getCatalogId = e.sender.value();


                $scope.getDefaultPdfTemplate();
                $scope.selects(e.sender.value());
                $scope.attrcount = 0;
                $scope.prodimageattrcount = 0;
                $scope.familydesccount = 0;
                $scope.prodpriceattrcount = 0;
                $scope.prodkeyattrcount = 0;
                $scope.familyimgcount = 0;
                $scope.familyattrcount = 0;
                $scope.familypricecount = 0;
                $scope.familykeycount = 0;
                $scope.selectcatalogattr(e.sender.value());
                $scope.dataSource.read();
                $scope.treeData.read();
                $scope.catalogfamilyattrDataSource.read();
                $scope.catalogproductattrDataSource.read();
                $scope.familyDataSource.read();
                $scope.defaultfamily();
            }
            $scope.GetUserRoleRightsAll();
        };


        $scope.newcatalog = function () {

            $scope.TempClearPdfTemplate();


            $("#new").hide();
            $("#mirror").show();
            document.getElementById("newcatalog").disabled = true;
            // $scope.Catalog.CATALOG_ID = 0,
            $scope.Catalog.CATALOG_NAME = "",
            $scope.Catalog.VERSION = "",
            $scope.Catalog.DESCRIPTION = "";
            $("#showcatalogTab").hide();
            $scope.newcatalogFlag = "0";
            $scope.MirrorCatalog = false;
            $scope.cancelDeleteFlag = true;
            $scope.familyDataSource.read();
            $scope.MirrorCatalogclick(true);
            $scope.Catalog.CATALOG_NAME = "--- New Catalog ---";

            $scope.SelectedFileForUploadnameSetup = '';
            $scope.SelectedFileForUpload = undefined;

        };

        $scope.MirrorCatalog = false;
        $scope.MirrorCatalogclick = function ($event) {
            if ($event) {
                $scope.newcatalogFlag = "1";
                $("#showcatalogTab").hide();
                $scope.Catalog.CATALOG_NAME = "",
                 $scope.Catalog.VERSION = "",
                $scope.Catalog.DESCRIPTION = "";
            }
            else {
                $("#showcatalogTab").show();
                $scope.newcatalogFlag = "0";
                $scope.Catalog.CATALOG_NAME = $('#catalogddl').data('kendoDropDownList').text();
                $scope.selects($('#catalogddl').data('kendoDropDownList').value());
            }
        };
        //TO Save Catalog
        $scope.savecatalog = function (catalog) {
            // To convert the first letter to Capital;
            
            catalog.CATALOG_NAME = catalog.CATALOG_NAME.charAt(0).toUpperCase() + catalog.CATALOG_NAME.slice(1);

            if (catalog.CATALOG_NAME.trim() == "") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter a valid Name.',
                    type: "info"
                });
            }
            else {
                if ($scope.newcatalogFlag === "1" && $scope.Catalog.CATALOG_ID != 0) {
                    dataFactory.saveMirrorCatalogDetails(catalog).success(function (response) {
                        blockUI.start();
                        var responses = response;
                        if (responses.toString().contains("Catalog name already exists")) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + responses + '.',
                                type: "error"
                            });
                        }
                        else if (responses != "") {

                            $scope.flag = 0;
                            $scope.getCatalogId = responses;

                            // To save pdf xpress - start   // MVC
                            if ($scope.SelectedFileForUpload != undefined) {
                                $scope.UploadFile($scope.SelectedFileForUpload);
                            }
                            // End


                            $scope.cancelDeleteFlag = false;
                            $scope.catalogDataSource.read();
                            //$scope.Catalog.CATALOG_ID = responses;
                            $scope.selectcatalogattr(responses);
                            $scope.dataSource.read();
                            $scope.treeData.read();
                            $scope.catalogfamilyattrDataSource.read();
                            $scope.catalogproductattrDataSource.read();
                            $scope.productSpecificationsdataSource.read();
                            $scope.familyDataSource.read();
                            $scope.selects($scope.getCatalogId);
                            $scope.newcatalogFlag = "0";
                            $scope.MirrorCatalog = false;
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Catalog Cloned Successfully.',
                                type: "info"
                            });
                        }
                        blockUI.stop();

                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {
                    if (catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });

                    } else {
                        //if ($scope.newcatalogFlag == "0") {
                        //    catalog.DEFAULT_FAMILY = null;
                        //}
                        dataFactory.savecatalogdetails(catalog).success(function (response) {
                            blockUI.start();
                            var responses = response;
                            if (responses.toString().contains('Update completed') || responses.toString().contains('successful')) {
                                $scope.catalogDataSource.read();
                                $scope.newcatalogFlag = "0";
                                $scope.MirrorCatalog = false;

                                // To save pdf xpress - start   // MVC
                                if ($scope.SelectedFileForUpload != undefined) {
                                    $scope.UploadFile($scope.SelectedFileForUpload);
                                }
                                // End

                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Catalog details updated.',
                                    type: "info"
                                });
                            } else if (responses.toString().contains('Catalog name already exists')) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Catalog name already exists, please create a new name.',
                                    type: "info"
                                });
                            } else {

                                $scope.flag = 0;
                                $scope.getCatalogId = responses;

                                // To save pdf xpress - start   // MVC
                                if ($scope.SelectedFileForUpload != undefined) {
                                    $scope.UploadFile($scope.SelectedFileForUpload);
                                }
                                // End
                                //$scope.Catalog.CATALOG_ID = responses;
                                $scope.catalogDataSource.read();
                                //$scope.Catalog.CATALOG_ID = responses;
                                $scope.selectcatalogattr(responses);
                                $scope.dataSource.read();
                                $scope.treeData.read();
                                $scope.catalogfamilyattrDataSource.read();
                                $scope.catalogproductattrDataSource.read();
                                $scope.cancelDeleteFlag = false;
                                $scope.productSpecificationsdataSource.read();

                                $scope.familyDataSource.read();
                                $scope.selects($scope.getCatalogId);
                                $scope.newcatalogFlag = "0";
                                $scope.MirrorCatalog = false;
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Catalog created.',
                                    type: "info"
                                });
                            }

                            blockUI.stop();
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            }
        };
        $scope.deletecatalog = function (catalog) {

            if (catalog.CATALOG_ID === 0 || catalog.CATALOG_ID === "") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Catalog.',
                    type: "info"
                });
            } else if (catalog.CATALOG_ID === 1) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Master Catalog deletion is not permitted.',
                    type: "info"
                });
            } else {


                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Are you sure want to delete the Catalog?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {

                        if (result === "Yes") {
                            dataFactory.deletecatalogdetails(catalog).success(function (response) {
                                blockUI.start();
                                //   $scope.SelectedFileForUploadnameSetup = null;
                                $scope.catalogDataSource.read();
                                //$scope.familyDataSource.read();
                                //$scope.clearcatalog();
                                //$scope.selects($scope.getCatalogId);
                                $scope.newAfterDeletecatalog();


                                $scope.SelectedFileForUploadnameSetup = '';
                                $scope.SelectedFileForUpload = undefined;
                                $scope.MyFileUpload = 'No File Chosen';

                                var $el = $('#chooseFile');
                                if ($el.val() != "") {
                                    $el.wrap('<form>').closest('form').get(0).reset();
                                    $el.unwrap();
                                }






                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                                blockUI.stop();
                            }).error(function (error) {
                                options.error(error);
                            });

                        }
                        else {
                            return;
                            // $scope.catalogDataSource.read();
                        }


                    }
                });
            }

        };

        $scope.reset = function (catalog) {

            $scope.selects($scope.getCatalogId);
            $scope.Catalog.CATALOG_NAME = "";
            $scope.Catalog.VERSION = "";
            $scope.Catalog.DESCRIPTION = "";
            $scope.Catalog.DEFAULT_FAMILY = true;
        };

        // ---------------------------------------------------End Catalog--------------------------------------------------------------------------------


        //$scope.tableParams = [];
        // --------------------------------------------------- Start Attribute Tab--------------------------------------------------------------------------------
        $scope.selectcatalogattr = function (catalogid) {
            blockUI.start();
            $scope.attrcount = 0;
            $scope.prodimageattrcount = 0;
            $scope.familydesccount = 0;
            $scope.prodpriceattrcount = 0;
            $scope.prodkeyattrcount = 0;
            $scope.familyimgcount = 0;
            $scope.familyattrcount = 0;
            $scope.familypricecount = 0;
            $scope.familykeycount = 0;

            if (catalogid === "") {
                if ($scope.flag === '0') {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Please select Catalog to view its attributes.',
                        type: "info"
                    });
                }
            } else {

                $("#showcatalogTab").show();
                $scope.productSpecificationsdataSource.read();
                $scope.productPricedataSource.read();
                $scope.productImagedataSource.read();
                $scope.productKeydataSource.read();
                $scope.familyKeydataSource.read();
                $scope.familyAttributedataSource.read();
                $scope.familyPricedataSource.read();
                $scope.familyImagedataSource.read();
                $scope.FamilyDescriptiondataSource.read();
                $scope.CategorySpecsdataSource.read();
                $scope.CategoryImagedataSource.read();
                $scope.CategoryDescdataSource.read();
            }
            blockUI.stop();
        };


        //---------------------------------------------------Product Specifications-----------------------------------------------------------------------------

        // $scope.attrcount = 0;
        //$scope.prodimageattrcount = 0;

        $scope.productSpecificationsdataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_ID", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {

                    var selectAllprodSpec;
                    if ($("#selectAllchk").prop("checked") == true) {
                        selectAllprodSpec = 1;
                    }
                    else if ($("#selectAllchk").prop("checked") == false) {
                        selectAllprodSpec = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 1, options.data).success(function (response) {
                        $scope.CheckboxProductSpecifications = response.Total;
                        $scope.UnCheckboxProductSpecifications = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxProductSpecifications = $scope.UnCheckboxProductSpecifications + 1;
                            }
                        }
                        if ($scope.CheckboxProductSpecifications == $scope.UnCheckboxProductSpecifications) {
                            $("#selectAllchk").prop("checked", true);
                        }
                        else {
                            $("#selectAllchk").prop("checked", false);
                        }

                        if (selectAllprodSpec == 1) {
                            $('#selectAllchk').prop('checked', false);
                        }
                        $scope.attrcount = response.Total;
                        for (var i = 0 ; response.Data.length > i ; i++) {
                            if (response.Data[i].ATTRIBUTE_NAME == "ITEM#") {
                                response.Data[i].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                            }
                            if (response.Data[i].ATTRIBUTE_NAME == "SUBITEM#") {
                                response.Data[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                            }

                        }
                        $scope.catalogproductattrDataSource.read();
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {

                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });

                    } else {
                        $rootScope.items;
                        //   CheckBoxValues = JSON.parse(CheckBoxValues);
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, $rootScope.items).success(function (response) {
                            $('#selectAllchk').prop('checked', false);
                            //$rootScope.items = [];
                            if ($scope.CheckboxProductSpecifications == $scope.UnCheckboxProductSpecifications) {
                                $("#selectAllchk").prop("checked", true);
                            }
                            else {
                                $("#selectAllchk").prop("checked", false);
                            }

                            //  $scope.productSpecificationsdataSource.read();
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info",
                            });
                            $scope.catalogproductattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });

        $scope.productSpecificationsGridOptions = {
            dataSource: $scope.productSpecificationsdataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
             { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox" ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionPS($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllchk' class='mc-checkbox' ng-click='selectAllPS($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };



        $scope.updateSelectionPS = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                if (e.target.checked == false) {
                    $scope.UnCheckboxProductSpecifications = $scope.UnCheckboxProductSpecifications - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxProductSpecifications = $scope.UnCheckboxProductSpecifications + 1;
                }

                if ($scope.CheckboxProductSpecifications == $scope.UnCheckboxProductSpecifications) {
                    $("#selectAllchk").prop("checked", true);
                }
                else {
                    $("#selectAllchk").prop("checked", false);
                }
                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });



            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };


        $scope.selectAllPS = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxProductSpecifications = $scope.CheckboxProductSpecifications;
            }
            else {
                $scope.UnCheckboxProductSpecifications = 0;
            }
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };


        //Attribute pack start


        $scope.familyAttributePackdataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "GROUP_ID", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllfamPack;
                    if ($("#selectAllfamPack").prop("checked") == true) {
                        selectAllfamPack = 1;
                    }
                    else if ($("#selectAllfamPack").prop("checked") == false) {
                        selectAllfamPack = 0;
                    }
                    dataFactory.GetFamilyPacks($scope.Catalog.CATALOG_ID, options.data).success(function (response) {
                        if (selectAllfamPack == 1) {
                            $('#selectAllfamPack').prop('checked', false);
                        }
                        $scope.familyPackCount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "GROUP_ID",
                    fields: {
                        GROUP_NAME: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });


        $scope.familyAttributePackOptions = {
            dataSource: $scope.familyAttributePackdataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: '<a ng-click="SaveFamilyAttributePackforCatalog()" class="red k-button k-button-icontext k-grid-upload">Save</a>' },
             { name: "cancel", text: "", template: '<a ng-click="CancelFamilyAttributePackforCatalog()" class="red k-button k-button-icontext k-grid-upload">Cancel</a>' }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllfamPack' class='mc-checkbox' ng-click='selectAll1($event)'/>" },
                { field: "GROUP_NAME", title: "Group Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };

        $scope.productAttributePackdataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "GROUP_ID", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllProdPack;
                    if ($("#selectAllProdPack").prop("checked") == true) {
                        selectAllProdPack = 1;
                    }
                    else if ($("#selectAllProdPack").prop("checked") == false) {
                        selectAllProdPack = 0;
                    }
                    dataFactory.GetProductPacks($scope.Catalog.CATALOG_ID, options.data).success(function (response) {
                        if (selectAllProdPack == 1) {
                            $('#selectAllProdPack').prop('checked', false);
                        }
                        $scope.prodPackCount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "GROUP_ID",
                    fields: {
                        GROUP_NAME: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });


        $scope.productAttributePackOptions = {
            dataSource: $scope.productAttributePackdataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: '<a ng-click="SaveProductAttributePackforCatalog()" class="red k-button k-button-icontext k-grid-upload">Save</a>' },
             { name: "cancel", text: "", template: '<a ng-click="CancelProductAttributePackforCatalog()" class="red k-button k-button-icontext k-grid-upload">Cancel</a>' }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllProdPack' class='mc-checkbox' ng-click='selectAll1($event)'/>" },
                { field: "GROUP_NAME", title: "Group Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };

        $scope.SaveProductAttributePackforCatalog = function () {
            if ($scope.Catalog.CATALOG_ID === 1) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Master Catalog update is not permitted.',
                    type: "info"
                });
            } else {
                if ($rootScope.itemAttrPack != undefined && $rootScope.itemAttrPack.length != 0) {
                    dataFactory.SaveProductAttributePackforCatalog($scope.Catalog.CATALOG_ID, $rootScope.itemAttrPack).success(function (response) {
                        $rootScope.itemAttrPack = [];
                        $('#selectAllProdPack').prop('checked', false);
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Process completed.',
                            type: "info",
                        });
                        $scope.productAttributePackdataSource.read();
                        // options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {

                }
            }
        };

        $scope.SaveFamilyAttributePackforCatalog = function () {
            if ($scope.Catalog.CATALOG_ID === 1) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Master Catalog update is not permitted.',
                    type: "info"
                });
            } else {
                if ($rootScope.itemAttrPack != undefined && $rootScope.itemAttrPack.length != 0) {
                    dataFactory.SaveFamilyAttributePackforCatalog($scope.Catalog.CATALOG_ID, $rootScope.itemAttrPack).success(function (response) {
                        $rootScope.itemAttrPack = [];
                        $('#selectAllfamPack').prop('checked', false);
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Process completed.',
                            type: "info",
                        });
                        $scope.familyAttributePackdataSource.read();
                        // options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
                else {

                }
            }
        };

        $scope.CancelProductAttributePackforCatalog = function () {
            $scope.productAttributePackdataSource.read();
        };

        $scope.CancelFamilyAttributePackforCatalog = function () {
            $scope.familyAttributePackdataSource.read();
        };

        $rootScope.itemAttrPack = [];
        $scope.updateSelection1 = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                $rootScope.itemAttrPack.push({
                    GROUP_ID: id.dataItem.GROUP_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    GROUP_NAME: id.dataItem.GROUP_NAME,
                    CATALOG_ID: $scope.Catalog.CATALOG_ID,
                    // FAMILY_ID: $scope.Family.FAMILY_ID,
                    // CATEGORY_ID: $scope.ActiveCategoryId,
                });

            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };

        $scope.selectAll1 = function (ev) {

            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.itemAttrPack = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };


        //Attribute pack end

        //------------------------------------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Product Image------------------------------------------------------------------
        $scope.productImagedataSource = new kendo.data.DataSource({
            // type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {

                    var selectAllprodImg;
                    if ($("#selectAllProdImg").prop("checked") == true) {
                        selectAllprodImg = 1;
                    }
                    else if ($("#selectAllProdImg").prop("checked") == false) {
                        selectAllprodImg = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 3, options.data).success(function (response) {
                        $scope.CheckboxProductImage = response.Total;
                        $scope.UnCheckboxCheckboxProductImage = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxCheckboxProductImage = $scope.UnCheckboxCheckboxProductImage + 1;
                            }
                        }
                        if ($scope.CheckboxCheckboxProductImage == $scope.UnCheckboxCheckboxProductImage) {
                            $("#selectAllProdImg").prop("checked", true);
                        }
                        else {
                            $("#selectAllProdImg").prop("checked", false);
                        }
                        if (selectAllprodImg == 1) {
                            $('#selectAllProdImg').prop('checked', false);
                        }
                        $scope.prodimageattrcount = response.Total;
                        $scope.catalogproductattrDataSource.read();
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {

                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data.models).success(function (response) {
                            $('#selectAllProdImg').prop('checked', false);
                            if ($scope.CheckboxCheckboxProductImage == $scope.UnCheckboxCheckboxProductImage) {
                                $("#selectAllProdImg").prop("checked", true);
                            }
                            else {
                                $("#selectAllProdImg").prop("checked", false);
                            }
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info"
                            });
                            $scope.catalogproductattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.productImageGridOptions = {
            dataSource: $scope.productImagedataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
              { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
              { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionPI($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllProdImg' class='mc-checkbox' ng-click='selectAllPI($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };




        $scope.selectAllPI = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxCheckboxProductImage = $scope.CheckboxCheckboxProductImage;
            }
            else {
                $scope.UnCheckboxCheckboxProductImage = 0;
            }
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };

        $scope.updateSelectionPI = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                if (e.target.checked == false) {
                    $scope.UnCheckboxCheckboxProductImage = $scope.UnCheckboxCheckboxProductImage - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxCheckboxProductImage = $scope.UnCheckboxCheckboxProductImage + 1;
                }

                if ($scope.CheckboxCheckboxProductImage == $scope.UnCheckboxCheckboxProductImage) {
                    $("#selectAllProdImg").prop("checked", true);
                }
                else {
                    $("#selectAllProdImg").prop("checked", false);
                }
                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });

            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };


        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Product Price------------------------------------------------------------------
        $scope.productPricedataSource = new kendo.data.DataSource({
            // type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    //selectAllProdPrice
                    var selectAllProdPrice;
                    if ($("#selectAllProdPrice").prop("checked") == true) {
                        selectAllProdPrice = 1;
                    }
                    else if ($("#selectAllProdPrice").prop("checked") == false) {
                        selectAllProdPrice = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 4, options.data).success(function (response) {
                        $scope.CheckboxProductPrice = response.Total;
                        $scope.UnCheckboxProductPrice = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxProductPrice = $scope.UnCheckboxProductPrice + 1;
                            }
                        }
                        if ($scope.CheckboxProductPrice == $scope.UnCheckboxProductPrice) {
                            $("#selectAllProdPrice").prop("checked", true);
                        }
                        else {
                            $("#selectAllProdPrice").prop("checked", false);
                        }

                        if (selectAllProdPrice == 1) {
                            $('#selectAllProdPrice').prop('checked', false);
                        }
                        $scope.catalogproductattrDataSource.read();
                        $scope.prodpriceattrcount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });

                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data.models).success(function (response) {
                            $('#selectAllProdPrice').prop('checked', false);
                            if ($scope.CheckboxProductPrice == $scope.UnCheckboxProductPrice) {
                                $("#selectAllProdPrice").prop("checked", true);
                            }
                            else {
                                $("#selectAllProdPrice").prop("checked", false);
                            }
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info"
                            });
                            $scope.catalogproductattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.productPriceGridOptions = {
            dataSource: $scope.productPricedataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
             { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionPP($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllProdPrice' class='mc-checkbox' ng-click='selectAllPP($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };


        $scope.selectAllPP = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxProductPrice = $scope.CheckboxProductPrice;
            }
            else {
                $scope.UnCheckboxProductPrice = 0;
            }
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };


        $scope.updateSelectionPP = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                if (e.target.checked == false) {
                    $scope.UnCheckboxProductPrice = $scope.UnCheckboxProductPrice - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxProductPrice = $scope.UnCheckboxProductPrice + 1;
                }

                if ($scope.CheckboxProductPrice == $scope.UnCheckboxProductPrice) {
                    $("#selectAllProdPrice").prop("checked", true);
                }
                else {
                    $("#selectAllProdPrice").prop("checked", false);
                }
                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });
            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };






        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Product Key------------------------------------------------------------------
        $scope.productKeydataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllProdKey;
                    if ($("#selectAllProdKey").prop("checked") == true) {
                        selectAllProdKey = 1;
                    }
                    else if ($("#selectAllProdKey").prop("checked") == false) {
                        selectAllProdKey = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 6, options.data).success(function (response) {
                        $scope.CheckboxProductKey = response.Total;
                        $scope.UnCheckboxProductKey = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxProductKey = $scope.UnCheckboxProductKey + 1;
                            }
                        }
                        if ($scope.CheckboxProductKey == $scope.UnCheckboxProductKey) {
                            $("#selectAllProdKey").prop("checked", true);
                        }
                        else {
                            $("#selectAllProdKey").prop("checked", false);
                        }
                        if (selectAllProdKey == 1) {
                            $('#selectAllProdKey').prop('checked', false);
                        }
                        $scope.catalogproductattrDataSource.read();
                        $scope.prodkeyattrcount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data.models).success(function (response) {
                            $('#selectAllProdKey').prop('checked', false);
                            if ($scope.CheckboxProductKey == $scope.UnCheckboxProductKey) {
                                $("#selectAllProdKey").prop("checked", true);
                            }
                            else {
                                $("#selectAllProdKey").prop("checked", false);
                            }
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info"
                            });
                            $scope.catalogproductattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.productKeyGridOptions = {
            dataSource: $scope.productKeydataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
             { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionPK($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllProdKey' class='mc-checkbox' ng-click='selectAllPK($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };


        $scope.updateSelectionPK = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                if (e.target.checked == false) {
                    $scope.UnCheckboxProductKey = $scope.UnCheckboxProductKey - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxProductKey = $scope.UnCheckboxProductKey + 1;
                }

                if ($scope.CheckboxProductKey == $scope.UnCheckboxProductKey) {
                    $("#selectAllProdKey").prop("checked", true);
                }
                else {
                    $("#selectAllProdKey").prop("checked", false);
                }
                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });


            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };

        $scope.selectAllPK = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxProductKey = $scope.CheckboxProductKey;
            }
            else {
                $scope.UnCheckboxProductKey = 0;
            }
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };







        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Family Description------------------------------------------------------------------
        $scope.FamilyDescriptiondataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllFamDesc;
                    if ($("#selectAllFamDesc").prop("checked") == true) {
                        selectAllFamDesc = 1;
                    }
                    else if ($("#selectAllFamDesc").prop("checked") == false) {
                        selectAllFamDesc = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 7, options.data).success(function (response) {
                        $scope.CheckboxFamilyDescription = response.Total;
                        $scope.UnCheckboxFamilyDescription = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxFamilyDescription = $scope.UnCheckboxFamilyDescription + 1;
                            }
                        }
                        if ($scope.CheckboxFamilyDescription == $scope.UnCheckboxFamilyDescription) {
                            $("#selectAllFamDesc").prop("checked", true);
                        }
                        else {
                            $("#selectAllFamDesc").prop("checked", false);
                        }
                        if (selectAllFamDesc == 1) {
                            $('#selectAllFamDesc').prop('checked', false);
                        }
                        $scope.familydesccount = response.Total;
                        $scope.catalogfamilyattrDataSource.read();
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data.models).success(function (response) {
                            if ($scope.CheckboxFamilyDescription == $scope.UnCheckboxFamilyDescription) {
                                $("#selectAllFamDesc").prop("checked", true);
                            }
                            else {
                                $("#selectAllFamDesc").prop("checked", false);
                            }
                            $('#selectAllFamDesc').prop('checked', false);
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info"
                            });
                            $scope.catalogfamilyattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.FamilyDescriptionGridOptions = {
            dataSource: $scope.FamilyDescriptiondataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
             { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionFD($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllFamDesc' class='mc-checkbox' ng-click='selectAllFD($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };



        $scope.updateSelectionFD = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                if (e.target.checked == false) {
                    $scope.UnCheckboxFamilyDescription = $scope.UnCheckboxFamilyDescription - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxFamilyDescription = $scope.UnCheckboxFamilyDescription + 1;
                }

                if ($scope.CheckboxFamilyDescription == $scope.UnCheckboxFamilyDescription) {
                    $("#selectAllFamDesc").prop("checked", true);
                }
                else {
                    $("#selectAllFamDesc").prop("checked", false);
                }
                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });


            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };

        $scope.selectAllFD = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxFamilyDescription = $scope.CheckboxFamilyDescription;
            }
            else {
                $scope.UnCheckboxFamilyDescription = 0;
            }
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };



        //---------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------Family Image------------------------------------------------------------------
        $scope.familyImagedataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllFamImg;
                    if ($("#selectAllFamImg").prop("checked") == true) {
                        selectAllFamImg = 1;
                    }
                    else if ($("#selectAllFamImg").prop("checked") == false) {
                        selectAllFamImg = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 9, options.data).success(function (response) {
                        $scope.CheckboxFamilyImage = response.Total;
                        $scope.UnCheckboxFamilyImage = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxFamilyImage = $scope.UnCheckboxFamilyImage + 1;
                            }
                        }
                        if ($scope.CheckboxFamilyImage == $scope.UnCheckboxFamilyImage) {
                            $("#selectAllFamImg").prop("checked", true);
                        }
                        else {
                            $("#selectAllFamImg").prop("checked", false);
                        }
                        if (selectAllFamImg == 1) {
                            $('#selectAllFamImg').prop('checked', false);
                        }
                        $scope.familyimgcount = response.Total;
                        $scope.catalogfamilyattrDataSource.read();
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data.models).success(function (response) {
                            $('#selectAllFamImg').prop('checked', false);
                            if ($scope.CheckboxFamilyImage == $scope.UnCheckboxFamilyImage) {
                                $("#selectAllFamImg").prop("checked", true);
                            }
                            else {
                                $("#selectAllFamImg").prop("checked", false);
                            }
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info"
                            });
                            $scope.catalogfamilyattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.familyImageGridOptions = {
            dataSource: $scope.familyImagedataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
              { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
              { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionFI($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllFamImg' class='mc-checkbox' ng-click='selectAllFI($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };


        $scope.updateSelectionFI = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                if (e.target.checked == false) {
                    $scope.UnCheckboxFamilyImage = $scope.UnCheckboxFamilyImage - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxFamilyImage = $scope.UnCheckboxFamilyImage + 1;
                }

                if ($scope.CheckboxFamilyImage == $scope.UnCheckboxFamilyImage) {
                    $("#selectAllFamImg").prop("checked", true);
                }
                else {
                    $("#selectAllFamImg").prop("checked", false);
                }
                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });

            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };



        $scope.selectAllFI = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxFamilyImage = $scope.CheckboxFamilyImage;
            }
            else {
                $scope.UnCheckboxFamilyImage = 0;
            }
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };








        //---------------------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Family Price------------------------------------------------------------------

        $scope.familyPricedataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllFamPrice;
                    if ($("#selectAllFamPrice").prop("checked") == true) {
                        selectAllFamPrice = 1;
                    }
                    else if ($("#selectAllFamPrice").prop("checked") == false) {
                        selectAllFamPrice = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 12, options.data).success(function (response) {
                        $scope.CheckboxFamilyPrice = response.Total;
                        $scope.UnCheckboxFamilyPrice = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxFamilyPrice = $scope.UnCheckboxFamilyPrice + 1;
                            }
                        }
                        if ($scope.CheckboxFamilyPrice == $scope.UnCheckboxFamilyPrice) {
                            $("#selectAllFamPrice").prop("checked", true);
                        }
                        else {
                            $("#selectAllFamPrice").prop("checked", false);
                        }
                        if (selectAllFamPrice == 1) {
                            $('#selectAllFamPrice').prop('checked', false);
                        }
                        $scope.familypricecount = response.Total;
                        $scope.catalogfamilyattrDataSource.read();
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data.models).success(function (response) {
                            $('#selectAllFamPrice').prop('checked', false);
                            if ($scope.CheckboxFamilyPrice == $scope.UnCheckboxFamilyPrice) {
                                $("#selectAllFamPrice").prop("checked", true);
                            }
                            else {
                                $("#selectAllFamPrice").prop("checked", false);
                            }
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info"
                            });
                            $scope.catalogfamilyattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.familyPriceGridOptions = {
            dataSource: $scope.familyPricedataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
             { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionFP($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllFamPrice' class='mc-checkbox' ng-click='selectAllFP($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };


        $scope.updateSelectionFP = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);

                if (e.target.checked == false) {
                    $scope.UnCheckboxFamilyPrice = $scope.UnCheckboxFamilyPrice - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxFamilyPrice = $scope.UnCheckboxFamilyPrice + 1;
                }
                if ($scope.CheckboxFamilyPrice == $scope.UnCheckboxFamilyPrice) {
                    $("#selectAllFamPrice").prop("checked", true);
                }
                else {
                    $("#selectAllFamPrice").prop("checked", false);
                }
                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });

            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };



        $scope.selectAllFP = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxFamilyPrice = $scope.CheckboxFamilyPrice;
            }
            else {
                $scope.UnCheckboxFamilyPrice = 0;
            }
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };




        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Family Attribute------------------------------------------------------------------
        $scope.familyAttributedataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllFamAttr;
                    if ($("#selectAllFamAttr").prop("checked") == true) {
                        selectAllFamAttr = 1;
                    }
                    else if ($("#selectAllFamAttr").prop("checked") == false) {
                        selectAllFamAttr = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 11, options.data).success(function (response) {
                        $scope.CheckboxFamilyAttribute = response.Total;
                        $scope.UnCheckboxFamilyAttribute = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxFamilyAttribute = $scope.UnCheckboxFamilyAttribute + 1;
                            }
                        }
                        if ($scope.CheckboxFamilyAttribute == $scope.UnCheckboxFamilyAttribute) {
                            $("#selectAllFamAttr").prop("checked", true);
                        }
                        else {
                            $("#selectAllFamAttr").prop("checked", false);
                        }
                        if (selectAllFamAttr == 1) {
                            $('#selectAllFamAttr').prop('checked', false);
                        }
                        $scope.familyattrcount = response.Total;
                        $scope.catalogfamilyattrDataSource.read();
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });

                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data.models).success(function (response) {
                            $('#selectAllFamAttr').prop('checked', false);
                            if ($scope.CheckboxFamilyAttribute == $scope.UnCheckboxFamilyAttribute) {
                                $("#selectAllFamAttr").prop("checked", true);
                            }
                            else {
                                $("#selectAllFamAttr").prop("checked", false);
                            }
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info"
                            });
                            $scope.catalogfamilyattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }

        });
        $scope.familyAttributeGridOptions = {
            dataSource: $scope.familyAttributedataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
               { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
               { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionFA($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllFamAttr' class='mc-checkbox' ng-click='selectAllFA($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };



        $scope.updateSelectionFA = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                if (e.target.checked == false) {
                    $scope.UnCheckboxFamilyAttribute = $scope.UnCheckboxFamilyAttribute - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxFamilyAttribute = $scope.UnCheckboxFamilyAttribute + 1;
                }

                if ($scope.CheckboxFamilyAttribute == $scope.UnCheckboxFamilyAttribute) {
                    $("#selectAllFamAttr").prop("checked", true);
                }
                else {
                    $("#selectAllFamAttr").prop("checked", false);
                }

                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });
            }
            else {
                id.dataItem.set("ISAvailable", true);
            }
        };



        $scope.selectAllFA = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxFamilyAttribute = $scope.CheckboxFamilyAttribute;
            }
            else {
                $scope.UnCheckboxFamilyAttribute = 0;
            }
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };


        //---------------------------------------------------------------------------------------------------------------------

        //---------------------------------------------------Family Key------------------------------------------------------------------
        $scope.familyKeydataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_NAME", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllFamKey;
                    if ($("#selectAllFamKey").prop("checked") == true) {
                        selectAllFamKey = 1;
                    }
                    else if ($("#selectAllFamKey").prop("checked") == false) {
                        selectAllFamKey = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 13, options.data).success(function (response) {
                        $scope.CheckboxFamilyKey = response.Total;
                        $scope.UnCheckboxFamilyKey = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxFamilyKey = $scope.UnCheckboxFamilyKey + 1;
                            }
                        }
                        if ($scope.CheckboxFamilyKey == $scope.UnCheckboxFamilyKey) {
                            $("#selectAllFamKey").prop("checked", true);
                        }
                        else {
                            $("#selectAllFamKey").prop("checked", false);
                        }
                        if (selectAllFamKey == 1) {
                            $('#selectAllFamKey').prop('checked', false);
                        }
                        $scope.catalogfamilyattrDataSource.read();
                        $scope.familykeycount = response.Total;
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });
                    } else {
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, options.data.models).success(function (response) {
                            $('#selectAllFamKey').prop('checked', false);
                            if ($scope.CheckboxFamilyKey == $scope.UnCheckboxFamilyKey) {
                                $("#selectAllFamKey").prop("checked", true);
                            }
                            else {
                                $("#selectAllFamKey").prop("checked", false);
                            }
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info"
                            });
                            $scope.catalogfamilyattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });

        $scope.familyKeyGridOptions = {
            dataSource: $scope.familyKeydataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
               { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
               { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionFK($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllFamKey' class='mc-checkbox' ng-click='selectAllFK($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };

        $scope.updateSelectionFK = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                if (e.target.checked == false) {
                    $scope.UnCheckboxFamilyKey = $scope.UnCheckboxFamilyKey - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxFamilyKey = $scope.UnCheckboxFamilyKey + 1;
                }

                if ($scope.CheckboxFamilyKey == $scope.UnCheckboxFamilyKey) {
                    $("#selectAllFamKey").prop("checked", true);
                }
                else {
                    $("#selectAllFamKey").prop("checked", false);
                }
                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });
            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };


        $scope.selectAllFK = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxFamilyKey = $scope.CheckboxFamilyKey;
            }
            else {
                $scope.UnCheckboxFamilyKey = 0;
            }
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };



        //---------------------------------------------------------------------------------------------------------------------

        //$scope.loadproductprice = function () {
        //    $scope.AttributeType = 4;
        //    $scope.productPricedataSource.read();
        //};
        //$scope.loadproductimage = function () {
        //    $scope.AttributeType = 3;
        //    $scope.productImagedataSource.read();
        //};
        //$scope.loadproductkey = function () {
        //    $scope.AttributeType = 6;
        //    $scope.productKeydataSource.read();
        //};
        //$scope.loadfamilykey = function () {
        //    $scope.AttributeType = 13;
        //    $scope.familyKeydataSource.read();
        //};
        //$scope.loadfamilyattribute = function () {
        //    $scope.AttributeType = 11;
        //    $scope.familyAttributedataSource.read();
        //};
        //$scope.loadfamilyprice = function () {
        //    $scope.AttributeType = 12;
        //    $scope.familyPricedataSource.read();
        //};
        //$scope.loadfamilyimage = function () {
        //    $scope.AttributeType = 9;
        //    $scope.familyImagedataSource.read();
        //};
        //$scope.loadfamilydesc = function () {
        //    $scope.AttributeType = 7;
        //    $scope.familyImagedataSource.read();
        //};

        //$rootScope.items = [];
        $scope.updateSelection = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);

                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });

            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };

        $scope.selectAll = function (ev) {

            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };

        // ---------------------------------------------------End Attribute Tab--------------------------------------------------------------------------------

        // --------------------------------------------------- Start Category Sort Tab--------------------------------------------------------------------------------

        $scope.dataSource = new kendo.data.HierarchicalDataSource({
            //pageSize: 5,
            //type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCategorySortDetails("0", $scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                model: {
                    id: "CATEGORY_ID",
                    fields: {
                        CATEGORY_ID: { editable: false, nullable: true },
                        CATEGORY_SHORT: { editable: false, nullable: true },
                        CATEGORY_NAME: { editable: false, validation: { required: true } },
                        SORT_ORDER: { type: "number", validation: { required: true, min: 1 } },
                        PARENT_CATEGORY: { editable: false, nullable: true }
                    }
                }
            }
        });
        $scope.ParentCategoryID = '';
        $scope.ddlDataSource = new kendo.data.DataSource({
            //type: "json",
            transport: {
                read: function (options) {
                    dataFactory.GetCategorySortDetails($scope.ParentCategoryID, $scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });
        $scope.categoryDropDownEditor = function (container, options) {
            $scope.ParentCategoryID = options.model.PARENT_CATEGORY;

            $scope.ddlDataSource.read();
            var editor = $('<input kendo-drop-down-list required k-data-text-field="\'SORT_ORDER\'" k-data-value-field="\'SORT_ORDER\'"  k-on-change=\"categorysortChange(kendoEvent)\" k-data-source="ddlDataSource" data-bind="value:' + options.field + '"/>')
            .appendTo(container);

            $compile(editor)($scope);
            editor.css("visibility", "visible");
        };
        $scope.mainGridOptions = {
            loadOnDemand: false,
            dataSource: $scope.dataSource,
            //pageable: { buttonCount: 5 },
            selectable: "row",
            columns: [
              { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryDropDownEditor, template: "#=SORT_ORDER#" },
              { field: "CATEGORY_SHORT", title: "Category ID" },
              { field: "CATEGORY_NAME", title: "Category Name" }],

            editable: true, detailTemplate: kendo.template($("#template").html()),
            dataBound: function (e) {
                var count = 0;
                var sortData = $('#grid').data('kendo-grid')._data;
                angular.forEach(sortData, function (data) {
                    if ($('#grid').data('kendo-grid')._data[count].hasChildren == false) {
                        //if ($('#grid').find('.k-plus') != undefined && $('#grid').find('.k-plus').length > 0) {
                        // $('#grid').find('.k-plus')[count].style.display = "none";
                        var uid = $('#grid').data('kendo-grid').tbody.find("tr.k-master-row")[count].dataset.uid
                        $('#grid').data('kendo-grid').expandRow($('#grid').data('kendo-grid').tbody.find("tr[data-uid='" + uid + "']"));
                        // }
                    }
                    count = count + 1;
                });

                //this.expandRow(this.tbody.find("tr.k-master-row"));
                //this.expandRow(this.tbody.find("tr.k-master-row").first());
            }
        };
        $scope.mainGridOptionsWithoutSort = {
            loadOnDemand: false,
            dataSource: $scope.dataSource,
            pageable: { buttonCount: 5 },
            selectable: "row",
            columns: [
              { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryDropDownEditor, template: "#=SORT_ORDER#" },
              { field: "CATEGORY_SHORT", title: "Category ID" },
              { field: "CATEGORY_NAME", title: "Category Name" }],

            editable: false, detailTemplate: kendo.template($("#templatewithout").html()),
            dataBound: function () {
                var count = 0;
                var sortData = $('#grid2').data('kendo-grid')._data;
                angular.forEach(sortData, function (data) {
                    if ($('#grid2').data('kendo-grid')._data[count].hasChildren == false) {
                        var uid = $('#grid2').data('kendo-grid').tbody.find("tr.k-master-row")[count].dataset.uid
                        $('#grid2').data('kendo-grid').expandRow($('#grid2').data('kendo-grid').tbody.find("tr[data-uid='" + uid + "']"));
                        // }
                    }
                    count = count + 1;
                });
                //   this.expandRow(this.tbody.find("tr.k-master-row").first());
                //this.expandRow(this.tbody.find("tr.k-master-row"));
            }
        };

        $scope.detailGridOptions = function (dataItem) {
            return {
                dataSource: {
                    //type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.GetCategorySortDetails(dataItem.CATEGORY_ID, $scope.Catalog.CATALOG_ID).success(function (response) {
                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }, schema: {
                        model: {
                            id: "CATEGORY_ID",
                            fields: {
                                CATEGORY_ID: { editable: false, nullable: true },
                                CATEGORY_SHORT: { editable: false, nullable: true },
                                CATEGORY_NAME: { editable: false, validation: { required: true } },
                                SORT_ORDER: { type: "number", validation: { required: true, min: 1 } },
                                PARENT_CATEGORY: { editable: false, nullable: true }
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 5
                },
                scrollable: false,
                sortable: true,
                selectable: "row",
                pageable: { buttonCount: 5 },
                columns: [
        { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryDropDownEditor, template: "#=SORT_ORDER#" },
          { field: "CATEGORY_SHORT", title: "Category ID" },
          { field: "CATEGORY_NAME", title: "Category Name" }],
                editable: true, detailTemplate: kendo.template($("#template").html()),
                dataBound: function () {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#grid").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    }
                    else {

                        var count = 0;
                        var sortData = $('#grid').data('kendo-grid')._data;
                        angular.forEach(sortData, function (data) {
                            if ($('#grid').data('kendo-grid')._data[count].hasChildren == false) {
                                var uid = $('#grid').data('kendo-grid').tbody.find("tr.k-master-row")[count].dataset.uid
                                $('#grid').data('kendo-grid').expandRow($('#grid').data('kendo-grid').tbody.find("tr[data-uid='" + uid + "']"));
                            }
                            count = count + 1;
                        });
                        //this.expandRow(this.tbody.find("tr.k-master-row"));

                        //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                    }
                }
            };
        };


        $scope.detailGridOptionsWithoutSort = function (dataItem) {
            return {
                dataSource: {
                    // type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.GetCategorySortDetails(dataItem.CATEGORY_ID, $scope.Catalog.CATALOG_ID).success(function (response) {
                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }, schema: {
                        model: {
                            id: "CATEGORY_ID",
                            fields: {
                                CATEGORY_ID: { editable: false, nullable: true },
                                CATEGORY_SHORT: { editable: false, nullable: true },
                                CATEGORY_NAME: { editable: false, validation: { required: true } },
                                SORT_ORDER: { type: "number", validation: { required: true, min: 1 } },
                                PARENT_CATEGORY: { editable: false, nullable: true }
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 5
                },
                scrollable: false,
                sortable: true,
                selectable: "row",
                pageable: { buttonCount: 5 },
                columns: [
        { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryDropDownEditor, template: "#=SORT_ORDER#" },
          { field: "CATEGORY_SHORT", title: "Category ID" },
          { field: "CATEGORY_NAME", title: "Category Name" }],
                editable: false, detailTemplate: kendo.template($("#templatewithout").html()),
                dataBound: function () {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#grid2").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    }
                    else {

                        var count = 0;
                        var sortData = $('#grid2').data('kendo-grid')._data;
                        angular.forEach(sortData, function (data) {
                            if ($('#grid2').data('kendo-grid')._data[count].hasChildren == false) {
                                var uid = $('#grid2').data('kendo-grid').tbody.find("tr.k-master-row")[count].dataset.uid
                                $('#grid2').data('kendo-grid').expandRow($('#grid2').data('kendo-grid').tbody.find("tr[data-uid='" + uid + "']"));
                            }
                            count = count + 1;
                        });
                        //   this.expandRow(this.tbody.find("tr.k-master-row"));

                        //  this.expandRow(this.tbody.find("tr.k-master-row").first());
                    }
                }
            };
        };
        //}
        $scope.changecategory = [];

        $scope.exchangeRows = function (dataItem, e) {
            $scope.changecategory = dataItem;
        };

        $scope.categorysortChange = function () {
            dataFactory.savecategorysort($scope.changecategory, $scope.Catalog.CATALOG_ID, $scope.changecategory.PARENT_CATEGORY).success(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Update completed.',
                    type: "info"
                });
                $scope.dataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.sortcategoryorder = function () {
            if ($scope.categorynamesortvalue == undefined) {
                return;
            }
            dataFactory.savecategorysortasc($scope.categorynamesortvalue, $scope.Catalog.CATALOG_ID, $scope.categoryidvalue).success(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + response + '.',
                    type: "info"
                });
                $scope.dataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.categoryidvalue = 0;
        $("#categoryids").hide();
        $scope.logIt = function () {
            $("#categoryids").show();
        };

        $scope.hideIt = function () {
            $scope.categoryidvalue = 0;
            $("#categoryids").hide();
        };


        // --------------------------------------------------- End Category Sort Tab--------------------------------------------------------------------------------
        // --------------------------------------------------- Start Family Sort Tab--------------------------------------------------------------------------------
        $scope.treeData = new kendo.data.HierarchicalDataSource({
            //type: "json",
            loadOnDemand: false,
            transport: {
                read: function (options) {
                    dataFactory.GetCatalogCategorySortDetails($scope.Catalog.CATALOG_ID, options.data.id).success(function (response) {

                        options.success(response);
                    }).error(function (response) {
                        options.success(response);
                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    hasChildren: "hasChildren"
                }
            }
        });
        $scope.categoryID = "0";

        $scope.clickSelectedcategoryfamily = function (dataItem) {
            $scope.categoryID = dataItem.id;
            $scope.familysort(dataItem.id);
        };

        $scope.catalogfamilydataSource = new kendo.data.DataSource({
            //pageSize: 5,
            //type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCategoryfamilyDetails($scope.categoryID, $scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);

                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                model: {
                    id: "FAMILY_ID",
                    fields: {
                        FAMILY_ID: { editable: false, nullable: true },
                        CATEGORY_ID: { editable: false, nullable: true },
                        FAMILY_NAME: { editable: false, nullable: true },
                        SORT_ORDER: {
                            type: "number", validation: { required: true, min: 1 }
                        }
                    }
                }
            }
        });

        $scope.CategoryIDFamily = '';
        $scope.ddlCategoryDataSource = new kendo.data.DataSource({
            //type: "json",
            transport: {
                read: function (options) {
                    dataFactory.GetCategoryfamilyDetails($scope.CategoryIDFamily, $scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.categoryfamilyDropDownEditor = function (container, options) {
            $scope.CategoryIDFamily = options.model.CATEGORY_ID;
            $scope.ddlCategoryDataSource.read();
            var editor = $('<input kendo-drop-down-list required k-data-text-field="\'SORT_ORDER\'" k-data-value-field="\'SORT_ORDER\'" k-on-change=\"familysortChange(kendoEvent)\" k-data-source="ddlCategoryDataSource" data-bind="value:' + options.field + '"/>')
            .appendTo(container);
            $compile(editor)($scope);
            editor.css("visibility", "visible");
        };

        $scope.mainGridcategoryfamilyOptions = {
            dataSource: $scope.catalogfamilydataSource,
            //pageable: { buttonCount: 5 },
            selectable: "row",
            columns: [
                  { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryfamilyDropDownEditor, template: "#=SORT_ORDER#" },
              { field: "FAMILY_ID", title: "Product ID" },
              { field: "FAMILY_NAME", title: "Product Name" }],

            editable: true, detailTemplate: kendo.template($("#templates").html()),
            dataBound: function () {

                // this.expandRow(this.tbody.find("tr.k-master-row"));
                var count = 0;
                var sortData = $('#gridcategoryfamily').data('kendo-grid')._data;
                angular.forEach(sortData, function (data) {
                    if ($('#gridcategoryfamily').data('kendo-grid')._data[count].hasChildren == false) {
                        var uid = $('#gridcategoryfamily').data('kendo-grid').tbody.find("tr.k-master-row")[count].dataset.uid
                        $('#gridcategoryfamily').data('kendo-grid').expandRow($('#gridcategoryfamily').data('kendo-grid').tbody.find("tr[data-uid='" + uid + "']"));
                    }
                    count = count + 1;
                });

            }
        };

        $scope.mainGridcategoryfamilyOptionsWithoutSort = {
            dataSource: $scope.catalogfamilydataSource,
            pageable: { buttonCount: 5 },
            selectable: "row",
            columns: [
                  { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categoryfamilyDropDownEditor, template: "#=SORT_ORDER#" },
              { field: "FAMILY_ID", title: "Product ID" },
              { field: "FAMILY_NAME", title: "Product Name" }],

            editable: false, detailTemplate: kendo.template($("#withouttemplates").html()),
            dataBound: function () {

                //  this.expandRow(this.tbody.find("tr.k-master-row"));
                var count = 0;
                var sortData = $('#gridcategoryfamilyWithoutSort').data('kendo-grid')._data;
                angular.forEach(sortData, function (data) {
                    if ($('#gridcategoryfamilyWithoutSort').data('kendo-grid')._data[count].hasChildren == false) {
                        var uid = $('#gridcategoryfamilyWithoutSort').data('kendo-grid').tbody.find("tr.k-master-row")[count].dataset.uid
                        $('#gridcategoryfamilyWithoutSort').data('kendo-grid').expandRow($('#gridcategoryfamilyWithoutSort').data('kendo-grid').tbody.find("tr[data-uid='" + uid + "']"));
                    }
                    count = count + 1;
                });

            }
        };

        $scope.CategoryFamily = 0;
        $scope.ddlsubCategoryDataSource = new kendo.data.DataSource({
            // type: "json",
            transport: {
                read: function (options) {
                    dataFactory.GetCategorysubfamilyDetails($scope.CategoryFamily, $scope.CategoryIDFamily, $scope.Catalog.CATALOG_ID, $scope.CategoryFamily).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.categorysubfamilyDropDownEditor = function (container, options) {
            $scope.CategoryIDFamily = options.model.CATEGORY_ID;
            $scope.CategoryFamily = options.model.PARENT_FAMILY_ID;
            $scope.ddlsubCategoryDataSource.read();
            var editor = $('<input kendo-drop-down-list required k-data-text-field="\'SORT_ORDER\'" k-data-value-field="\'SORT_ORDER\'" k-on-change=\"subfamilysortChange(kendoEvent)\" k-data-source="ddlsubCategoryDataSource" data-bind="value:' + options.field + '"/>')
            .appendTo(container);
            $compile(editor)($scope);
            editor.css("visibility", "visible");
        };

        $scope.detailGridcategoryfamilyOptions = function (dataItem) {
            return {
                dataSource: {
                    //type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.GetCategorysubfamilyDetails(dataItem.FAMILY_ID, dataItem.CATEGORY_ID, $scope.Catalog.CATALOG_ID, dataItem.PARENT_FAMILY_ID).success(function (response) {
                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }, schema: {
                        model: {
                            id: "FAMILY_ID",
                            fields: {
                                FAMILY_ID: { editable: false, nullable: true },
                                CATEGORY_ID: { editable: false, nullable: true },
                                FAMILY_NAME: { editable: false, nullable: true },
                                SORT_ORDER: { type: "number", validation: { required: true, min: 1 } }
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 5
                },
                scrollable: false,
                sortable: true,
                selectable: "row",
                pageable: { buttonCount: 5 },
                columns: [
                            { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categorysubfamilyDropDownEditor, template: "#=SORT_ORDER#" },
                            { field: "FAMILY_ID", title: "Product ID" },
                            { field: "FAMILY_NAME", title: "Product Name" }
                ],
                editable: true, detailTemplate: kendo.template($("#templates").html()),
                dataBound: function (e) {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#gridcategoryfamily").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    }
                    else {

                        // this.expandRow(this.tbody.find("tr.k-master-row"));
                        var count = 0;
                        var sortData = $('#gridcategoryfamily1').data('kendo-grid')._data;
                        angular.forEach(sortData, function (data) {
                            if ($('#gridcategoryfamily1').data('kendo-grid')._data[count].hasChildren == false) {
                                var uid = $('#gridcategoryfamily1').data('kendo-grid').tbody.find("tr.k-master-row")[count].dataset.uid
                                $('#gridcategoryfamily1').data('kendo-grid').expandRow($('#gridcategoryfamily1').data('kendo-grid').tbody.find("tr[data-uid='" + uid + "']"));
                            }
                            count = count + 1;
                        });

                    }
                }
            };
        };

        $scope.detailGridcategoryfamilyOptionsWithoutSort = function (dataItem) {
            return {
                dataSource: {
                    //type: "json",
                    transport: {
                        read: function (options) {
                            dataFactory.GetCategorysubfamilyDetails(dataItem.FAMILY_ID, dataItem.CATEGORY_ID, $scope.Catalog.CATALOG_ID, dataItem.PARENT_FAMILY_ID).success(function (response) {
                                options.success(response);
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                    }, schema: {
                        model: {
                            id: "FAMILY_ID",
                            fields: {
                                FAMILY_ID: { editable: false, nullable: true },
                                CATEGORY_ID: { editable: false, nullable: true },
                                FAMILY_NAME: { editable: false, nullable: true },
                                SORT_ORDER: { type: "number", validation: { required: true, min: 1 } }
                            }
                        }
                    },
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    pageSize: 5
                },
                scrollable: false,
                sortable: true,
                selectable: "row",
                pageable: { buttonCount: 5 },
                columns: [
                            { field: "SORT_ORDER", title: "Sort Order", editor: $scope.categorysubfamilyDropDownEditor, template: "#=SORT_ORDER#" },
                            { field: "FAMILY_ID", title: "Product ID" },
                            { field: "FAMILY_NAME", title: "Product Name" }
                ],
                editable: false, detailTemplate: kendo.template($("#withouttemplates").html()),
                dataBound: function (e) {
                    if (this.dataSource.data().length === 0) {
                        var masterRow = this.element.closest("tr.k-detail-row").prev();
                        $("#gridcategoryfamilyWithoutSort").data("kendoGrid").collapseRow(masterRow);
                        masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                    }
                    else {
                        // this.expandRow(this.tbody.find("tr.k-master-row"));

                        var count = 0;
                        var sortData = $('#gridcategoryfamilyWithoutSort1').data('kendo-grid')._data;
                        angular.forEach(sortData, function (data) {
                            if ($('#gridcategoryfamilyWithoutSort1').data('kendo-grid')._data[count].hasChildren == false) {
                                var uid = $('#gridcategoryfamilyWithoutSort1').data('kendo-grid').tbody.find("tr.k-master-row")[count].dataset.uid
                                $('#gridcategoryfamilyWithoutSort1').data('kendo-grid').expandRow($('#gridcategoryfamilyWithoutSort1').data('kendo-grid').tbody.find("tr[data-uid='" + uid + "']"));
                            }
                            count = count + 1;
                        });

                    }
                }
            };
        };

        $scope.familysort = function (categoryID) {
            $scope.categoryID = categoryID;
            $scope.catalogfamilydataSource.read();
        };
        $scope.selectedAttrVal = '';
        $scope.onAttributeSelected = function (e) {
            $scope.selectedAttrVal = e.sender._old;
            if ($scope.selectedAttrVal == '') {
                $("#familyids").hide();
            }
            else {
                $("#familyids").show();
            }
        };
        $scope.newValue = function (value) {
            $scope.familynamesortval = value;
            $scope.selectedAttrVal = '--- Select Product Attribute ---';
            $("#familyids").hide();
        }
        $scope.catalogfamilyattrDataSource = new kendo.data.DataSource({
            //type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    blockUI.start();
                    dataFactory.GetCatalogfamilyattrlistDetails($scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);
                        blockUI.stop();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });

        $scope.changefamily = [];

        $scope.familyexchangeRows = function (dataItem, e) {
            $scope.changefamily = dataItem;
        };

        $scope.familysortChange = function (e) {
            dataFactory.savefamilysort($scope.changefamily, $scope.Catalog.CATALOG_ID, $scope.changefamily.PARENT_FAMILY_ID).success(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Product sort order update completed.',
                    type: "info"
                });
                $scope.catalogfamilydataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };
        $scope.subfamilysortChange = function (e) {
            dataFactory.savesubfamilysort($scope.changefamily, $scope.Catalog.CATALOG_ID, $scope.changefamily.PARENT_FAMILY_ID, $scope.changefamily.FAMILY_ID).success(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Subfamily sort order update completed.',
                    type: "info"
                });
                $scope.catalogfamilydataSource.read();
            }).error(function (error) {
                options.error(error);
            });
        };


        $scope.sortfamilyorder = function () {
            if ($scope.familynamesortvalue == undefined) {
                return;
            }
            var attributeid = $("#attributedropdown").val();
            dataFactory.savefamilysortasc($scope.familynamesortvalue, $scope.Catalog.CATALOG_ID, $scope.categoryID, attributeid, $scope.familyidvalue).success(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + response + '.',
                    type: "info"
                });
                $scope.catalogfamilydataSource.read();
                $("#familyids").hide();
            }).error(function (error) {
                options.error(error);
            });
        };


        // --------------------------------------------------- End Family Sort Tab--------------------------------------------------------------------------------
        // --------------------------------------------------- Start Product Sort Tab--------------------------------------------------------------------------------
        $scope.catalogproductattrDataSource = new kendo.data.DataSource({
            //type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    blockUI.start();
                    dataFactory.GetCatalogproductattrlistDetails($scope.Catalog.CATALOG_ID).success(function (response) {
                        for (var i = 0 ; response.length > i ; i++) {
                            if (response[i].ATTRIBUTE_NAME == "ITEM#") {
                                response[i].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                            }
                            if (response[i].ATTRIBUTE_NAME == "SUBITEM#") {
                                response[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                            }
                        }
                        options.success(response);
                        blockUI.stop();
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });


        //  

        $("#productattribute1").hide();
        $("#productattribute2").hide();
        $("#productattribute3").hide();
        $("#productattribute4").hide();

        $scope.productAttrSortValue = "asc";
        $scope.productAttrSortValue2 = "asc";
        $scope.productAttrSortValue3 = "asc";
        $scope.productAttrSortValue4 = "asc";
        $scope.productAttrSortValue5 = "asc";

        $scope.firstAttr = '0';
        $scope.secondAttr = '0';
        $scope.thirdAttr = '0';
        $scope.fourthAttr = '0';
        $scope.fifthAttr = '0';

        $scope.attributedrpchange = function (e) {
            $scope.firstAttr = e.sender._old;
            $("#productattribute1").show();
        };
        $scope.attributedrpchange1 = function (e) {
            $scope.secondAttr = e.sender._old;
            $("#productattribute2").show();
        };
        $scope.attributedrpchange2 = function (e) {
            $scope.thirdAttr = e.sender._old;
            $("#productattribute3").show();
        };
        $scope.attributedrpchange3 = function (e) {
            $scope.fourthAttr = e.sender._old;
            $("#productattribute4").show();
        };
        $scope.attributedrpchange4 = function (e) {
            $scope.fifthAttr = e.sender._old;
        };


        // --------------------------------------------------- End Product Sort Tab--------------------------------------------------------------------------------
        // --------------------------------------------------- Start Family Filter Tab--------------------------------------------------------------------------------
        var counter = 1;
        $scope.data = {
            fields: []
        };
        $scope.selectedattr = 0;

        //$scope.addField = function () {
        //    $scope.itemsToAdd = {$scope.items
        //        Attribute: 'name',
        //        Attribute_id: 0,
        //        Condition: 'NONE',
        //        Operator: '=',
        //        Value: 'NONE',
        //        Spec_List: []
        //    };
        //    if ($scope.addField.fields == null)
        //        $scope.addField.fields = [];
        //    //  if ($scope.getFamilyVal.Condition != "NONE")
        //    //  {
        //    $scope.data.fields.push($scope.itemsToAdd);
        //    // }
        //};
        $scope.familyattrChange = function (e, item) {
            $scope.selectedattr = e.sender.value();
            item.Spec_List = new kendo.data.DataSource({
                //type: "json",
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        dataFactory.familyattrvalues($scope.selectedattr, $scope.Catalog.CATALOG_ID).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
        };
        $scope.items = [];

        $scope.itemsToAdd = [{
            Attribute: 'name',
            Attribute_id: 0,
            Condition: 'NONE',
            Operator: '=',
            Value: 'NONE',
            Spec_List: []
        }];
        $scope.getItemValue = [];
        $scope.addNew = function () {
            if ($scope.itemsToAdd == null) {
                $scope.itemsToAdd = [{
                    Attribute: 'name',
                    Attribute_id: 0,
                    Condition: 'NONE',
                    Operator: '=',
                    Value: 'NONE',
                    Spec_List: []
                }];
            }
            angular.forEach($scope.itemsToAdd, function (value, key) {
                $scope.getItemValue = value;
            });
            $scope.filterCondition($scope.getItemValue);
        };
        $scope.filterCondition = function (itemVal) {
            if (itemVal.Condition != 'NONE') {
                $scope.itemsToAdd.push({
                    Attribute: '',
                    Attribute_id: '',
                    Condition: 'NONE',
                    Operator: '=',
                    Value: 'NONE',
                    Spec_List: []
                });
            }
        };
        $scope.Removefilter = function (itemaaa) {
            var i = -1;
            angular.forEach($scope.itemsToAdd, function (value, key) {
                // $scope.getItemValue = value;
                if (itemaaa == value) {
                    i = key;
                    // $scope.itemsToAdd.splice(itemaaa, 1);
                };
            });
            if (i != -1) {
                $scope.itemsToAdd.splice(i, 1);
            }
            if ($scope.itemsToAdd.length == 0) {
                $scope.itemsToAdd = null;
            }
        };

        // --------------------------------------------------- End Family Filter Tab--------------------------------------------------------------------------------

        // --------------------------------------------------- Start Product Filter Tab--------------------------------------------------------------------------------

        $scope.proddata = {
            fields: []
        };

        $scope.prodattrChange = function (e, field) {
            field.Spec_List = new kendo.data.DataSource({
                //type: "json",
                serverFiltering: true,
                transport: {
                    read: function (options) {
                        dataFactory.productattrvalues(e.sender.value(), $scope.Catalog.CATALOG_ID).success(function (response) {
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                }
            });
        };
        $scope.prodItemsToAdd = [{
            Attribute: 'name',
            Attribute_id: 0,
            Condition: 'NONE',
            Operator: '=',
            Value: 'NONE',
            Spec_List: []
        }];
        $scope.getProdValue = '';
        $scope.prodaddField = function () {
            if ($scope.prodItemsToAdd == null) {
                $scope.prodItemsToAdd = [{
                    Attribute: 'name',
                    Attribute_id: 0,
                    Condition: 'NONE',
                    Operator: '=',
                    Value: 'NONE',
                    Spec_List: []
                }];
            }
            angular.forEach($scope.prodItemsToAdd, function (value, key) {
                $scope.getProdValue = value;
            });
            $scope.filterProdCondition($scope.getProdValue.Condition);
        };
        $scope.filterProdCondition = function (itemProdVal) {
            if (itemProdVal != 'NONE') {
                $scope.prodItemsToAdd.push({
                    Attribute: '',
                    Attribute_id: '',
                    Condition: 'NONE',
                    Operator: '=',
                    Value: 'NONE',
                    Spec_List: []
                });
            }
        };


        $scope.familyitems = [{
            Attribute_id: "",
            Condition: "",
            Operator: "",
            Value: ""
        }];
        //$scope.prodaddField = function () {
        //    var item = {
        //        Attribute_id: "",
        //        Condition: "",
        //        Spec_Value: "",
        //        Spec_Condition: "",
        //        Spec_List: [],
        //    };

        //    if ($scope.prodaddField.fields == null)
        //        $scope.prodaddField.fields = [];
        //    $scope.proddata.fields.push(item);
        //};
        //$scope.proddata.fields
        $scope.productSaveField = function () {
            //  var sdte = $scope.proddata.fields;
            $scope.familyitems = [];
            angular.forEach($scope.prodItemsToAdd, function (value, key) {
                // familyitem = "";

                // familyitems.push(familyitem);
                $scope.familyitems.push({
                    Attribute_id: value.Attribute_id,
                    Condition: value.Condition,
                    Operator: value.Operator,
                    Value: value.Value
                });
            });
            // item = $scope.prodItemsToAdd;
            dataFactory.SaveProductFilters($scope.Catalog.CATALOG_ID, $scope.familyitems).success(function () {
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.familySaveField = function () {
            $scope.familyitems = [];
            angular.forEach($scope.itemsToAdd, function (value, key) {
                $scope.familyitems.push({
                    Attribute_id: value.Attribute_id,
                    Condition: value.Condition,
                    Operator: value.Operator,
                    Value: value.Value
                });
            });
            dataFactory.SaveFamilyFilters($scope.Catalog.CATALOG_ID, $scope.familyitems).success(function () {
            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.ProdRemovefilter = function (itemaaa) {
            var i = -1;
            angular.forEach($scope.prodItemsToAdd, function (value, key) {
                // $scope.getItemValue = value;
                if (itemaaa == value) {
                    i = key;
                    // $scope.itemsToAdd.splice(itemaaa, 1);
                };
            });
            if (i != -1) {
                $scope.prodItemsToAdd.splice(i, 1);
            }
            if ($scope.prodItemsToAdd.length == 0) {
                $scope.prodItemsToAdd = null;
            }
        };
        // --------------------------------------------------- End Product Filter Tab--------------------------------------------------------------------------------
        $scope.familyDataSource = new kendo.data.DataSource({
            //type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    if ($scope.Catalog.CATALOG_ID !== 0) {
                        dataFactory.GetCatalogFamily($scope.SelectedItem, $scope.Catalog.CATALOG_ID, $localStorage.getCustomerID).success(function (response) {
                            blockUI.start();
                            options.success(response);
                            if ($scope.DEFAULT_FAMILY != undefined) {
                                $scope.Catalog.DEFAULT_FAMILY = $scope.DEFAULT_FAMILY;
                            }

                            blockUI.stop();
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                    else {
                        dataFactory.GetCatalogFamily($scope.SelectedItem, 0, 0).success(function (response) {

                            blockUI.start();
                            options.success(response);
                            blockUI.stop();
                        }).error(function (error) {
                            options.error(error);
                        });

                    }
                }
            },
            cache: false
        });
        $scope.familyChange = function (e) {
            if (e.sender.value() !== "") {
                $scope.Catalog.DEFAULT_FAMILY = e.sender.value();
            }
        };

        $scope.userRoleAddCatalog = false;
        $scope.userRoleModifyCatalog = false;
        $scope.userRoleDeleteCatalog = false;
        $scope.userRoleViewCatalog = false;
        $scope.getUserRoleRightsCatalog = function () {
            var id = 40201;
            dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddCatalog = response[0].ACTION_ADD;
                    $scope.userRoleModifyCatalog = response[0].ACTION_MODIFY;
                    $scope.userRoleDeleteCatalog = response[0].ACTION_REMOVE;
                    $scope.userRoleViewCatalog = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.userRoleAddSort = false;
        $scope.userRoleModifySort = false;
        $scope.userRoleDeleteSort = false;
        $scope.userRoleViewSort = false;

        $scope.getUserRoleRightsSort = function () {
            var id = 4020102;
            dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddSort = response[0].ACTION_ADD;
                    $scope.userRoleModifySort = response[0].ACTION_MODIFY;
                    $scope.userRoleDeleteSort = response[0].ACTION_REMOVE;
                    $scope.userRoleViewSort = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        $scope.userRoleAddFilter = false;
        $scope.userRoleModifyFilter = false;
        $scope.userRoleDeleteFilter = false;
        $scope.userRoleViewFilter = false;

        $scope.getUserRoleRightsFilter = function () {
            var id = 4020103;
            dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddFilter = response[0].ACTION_ADD;
                    $scope.userRoleModifyFilter = response[0].ACTION_MODIFY;
                    $scope.userRoleDeleteFilter = response[0].ACTION_REMOVE;
                    $scope.userRoleViewFilter = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };


        $scope.userRoleAddCatalogAttribute = false;
        $scope.userRoleModifyCatalogAttribute = false;
        $scope.userRoleDeleteCatalogAttribute = false;
        $scope.userRoleViewCatalogAttribute = false;

        $scope.getUserRoleRightsCatalogAttribute = function () {
            var id = 4020101;
            dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
                if (response !== "" && response !== null) {
                    $scope.userRoleAddCatalogAttribute = response[0].ACTION_ADD;
                    $scope.userRoleModifyCatalogAttribute = response[0].ACTION_MODIFY;
                    $scope.userRoleDeleteCatalogAttribute = response[0].ACTION_REMOVE;
                    $scope.userRoleViewCatalogAttribute = response[0].ACTION_VIEW;
                }

            }).error(function (error) {
                options.error(error);
            });
        };

        //pdf
        $scope.getDefaultPdfTemplate = function () {
            var Cat_Id = $localStorage.getCatalogID;
            dataFactory.getPdfXpressdefaultTemplate(Cat_Id).success(function (response) {
                if (response != null && response != "Error") {
                    $scope.SelectedFileForUploadnameSetup = response.split('_')[1];
                    $scope.MyFileUpload = response.split('_')[1];
                    // $scope.SelectedFileForUploadnameSetup = response.substring(0, 18);
                }
                else {
                    $scope.SelectedFileForUploadnameSetup = '';
                    $scope.MyFileUpload = 'No File Chosen';
                }

            });
        }

        // pdf

        $scope.init = function () {
            $("#categoryids").hide();
            $("#familyids").hide();
            $("#mirror").hide();
            if ($localStorage.getCatalogID === undefined) {
                $scope.getCatalogId = 0;
                $scope.Catalog.CATALOG_ID = 0;
            }
            else {
                $scope.getCatalogId = $localStorage.getCatalogID;
                $scope.Catalog.CATALOG_ID = $scope.getCatalogId;
                $scope.selects($scope.getCatalogId);
                $("#showcatalogTab").hide();
            }
            if ($localStorage.getCustomerID == undefined) {
                $scope.getCustomerID = 0;

            }
            else {
                $scope.getCustomerID = $localStorage.getCustomerID;
            }
            $scope.getUserRoleRightsCatalog();
            $scope.getUserRoleRightsCatalogAttribute();
            $scope.getUserRoleRightsSort();
            $scope.getUserRoleRightsFilter();

            $scope.familyAttributePackdataSource.read();
            $scope.productAttributePackdataSource.read();
            $scope.getDefaultPdfTemplate();


        };
        $scope.checkboxes = { 'checked': false, items: {} };
        $scope.init();

        //function for clear catalog funcvtionality//
        $scope.clearcatalog = function (e) {

            e.CATALOG_NAME = "";
            e.DESCRIPTION = "";
        }


        $scope.cancelNewcatalog = function (e) {
            location.reload();
        }

        //page after delete a catalog//
        $scope.newAfterDeletecatalog = function () {

            $("#new").show();
            $("#mirror").hide();
            document.getElementById("newcatalog").disabled = true;
            $scope.Catalog.CATALOG_ID = 0,
            $scope.Catalog.CATALOG_NAME = "",
            $scope.Catalog.VERSION = "",
            $scope.Catalog.DESCRIPTION = "";
            $("#showcatalogTab").hide();
            $scope.newcatalogFlag = "0";
            //$scope.MirrorCatalog = false;
            $scope.cancelDeleteFlag = true;
            $scope.familyDataSource.read();
            //$scope.MirrorCatalogclick(true);
        }

        $scope.productsortorder = function () {

            if ($scope.selectedAttribute == "") {
                return;
            }
            if ($scope.GridValue.length > 0) {
                var familyId = "0";
                angular.forEach($scope.GridValue, function (value, key) {
                    familyId = familyId + "," + value.FAMILY_ID + ",";
                });
                dataFactory.saveProductSortOrder($scope.Catalog.CATALOG_ID, $scope.categoryID,$scope.firstAttr, $scope.secondAttr, $scope.thirdAttr, $scope.fourthAttr, $scope.fifthAttr,
                     $scope.productAttrSortValue, $scope.productAttrSortValue2, $scope.productAttrSortValue3, $scope.productAttrSortValue4, $scope.productAttrSortValue5,
                      familyId).success(function (response) {
                         $.msgBox({
                             title: $localStorage.ProdcutTitle,
                             content: '' + response + '.',
                             type: "info"
                         });
                     }).error(function (error) {
                         options.error(error);
                     });
            }
            else
                return;
        };

        //Product tree view
        $rootScope.treeDataProductSort = new kendo.data.HierarchicalDataSource({
            type: "json",
            loadOnDemand: false,
            autoBind: false,
            transport: {
                read: function (options) {
                    dataFactory.getAllCategories($scope.SelectedCatalogId, options.data.id, $localStorage.getCatalogID).success(function (response) {
                        blockUI.start();
                        options.success(response);
                        blockUI.stop();
                    }).error(function (response) {
                        options.success(response);
                    });
                }
            },
            schema: {
                model: {
                    id: "id",
                    hasChildren: "hasChildren"
                }
            }
        });
        $scope.GridValue = [];
        //PRODUCT SORT GRID DATA SOURCE//
        $scope.catalogfamilyProductdataSource = new kendo.data.DataSource({
            //pageSize: 5,
            //type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCategoryfamilyDetails($scope.categoryID, $scope.Catalog.CATALOG_ID).success(function (response) {
                        $scope.GridValue = response;
                        options.success(response);

                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                model: {
                    id: "FAMILY_ID",
                    fields: {
                        FAMILY_ID: { editable: false, nullable: true },
                        CATEGORY_ID: { editable: false, nullable: true },
                        FAMILY_NAME: { editable: false, nullable: true }
                    }
                }
            }
        });

        $scope.clickSelectedcategoryfamilyProduct = function (dataItem) {
            $scope.categoryID = dataItem.id;
            $scope.familysortProduct(dataItem.id);
        };

        $scope.familysortProduct = function (categoryID) {
            $scope.categoryID = categoryID;
            $scope.catalogfamilyProductdataSource.read();
        };

        //Product sort gridview//
        $scope.mainGridcategoryfamilyProductOptions = {
            dataSource: $scope.catalogfamilyProductdataSource,
            //pageable: { buttonCount: 5 },
            selectable: "row",
            columns: [
              { field: "FAMILY_ID", title: "Product ID" },
              { field: "FAMILY_NAME", title: "Product Name" }]
        };


        // Pdf Express - Start


        $scope.selectFileforUploadRun_SetUp = function (file) {

            $scope.SelectedFileForUpload = file[0];
            $scope.SelectedFileForCategory = file[0].name;
            $scope.SelectedFileForUploadnameSetup = file[0].name;
            $scope.MyFileUpload = file[0].name;




            var Cat_Id = $localStorage.getCatalogID;
            dataFactory.getPdfXpressdefaultTemplate(Cat_Id).success(function (response) {
                if (response != null && response != "Error") {

                }
            });

        };

        $scope.openPDFPath = "";
        $scope.UploadFile = function (file) {
            var formData = new FormData();
            formData.append("file", file);
            formData.append("CatalogId", $scope.getCatalogId);

            $http.post("/Catalog/SaveFiles", formData,
                {
                    withCredentials: true,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                }).success(function (d) {

                })
            // return defer.promise;
        };



        $scope.TempClearPdfTemplate = function () {
            $scope.SelectedFileForUploadnameSetup = '';
            $scope.SelectedFileForUpload = undefined;
            $scope.MyFileUpload = 'No File Chosen';

            var $el = $('#chooseFile');
            if ($el.val() != "") {
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();
            }
        }


        // Clear Pdf Template  - start

        $scope.ClearPdfTemplate = function () {
            var Cat_Id = $scope.getCatalogId;

            dataFactory.deletedefaultTemplate(Cat_Id).success(function (response) {
                if (response == 'Success') {
                    $scope.SelectedFileForUploadnameSetup = '';
                    $scope.SelectedFileForUpload = undefined;
                    $scope.MyFileUpload = 'No File Chosen';

                    var $el = $('#chooseFile');
                    if ($el.val() != "") {
                        $el.wrap('<form>').closest('form').get(0).reset();
                        $el.unwrap();
                    }
                }

            }).error(function (error) {
                options.error(error);
            });

        }


        // End 




        // Pdf Express - End


        // Category Level Attributes start

        //---------------------------------------- Category Specification ----------------------------------------
        $scope.CategorySpecsdataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_ID", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllCatSpecs;
                    if ($("#selectAllCatSpecs").prop("checked") == true) {
                        selectAllCatSpecs = 1;
                    }
                    else if ($("#selectAllCatSpecs").prop("checked") == false) {
                        selectAllCatSpecs = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 21, options.data).success(function (response) {
                        $scope.CheckboxCategorySpecification = response.Total;
                        $scope.UnCheckboxCategorySpecification = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxCategorySpecification = $scope.UnCheckboxCategorySpecification + 1;
                            }
                        }
                        if ($scope.CheckboxCategorySpecification == $scope.UncheckboxCategorySpecification) {
                            $("#selectAllCatSpecs").prop("checked", true);
                        }
                        else {
                            $("#selectAllCatSpecs").prop("checked", false);
                        }
                        if (selectAllCatSpecs == 1) {
                            $('#selectAllCatSpecs').prop('checked', false);
                        }
                        $scope.CatSpecscount = response.Total;
                        for (var i = 0 ; response.Data.length > i ; i++) {
                            if (response.Data[i].ATTRIBUTE_NAME == "ITEM#") {
                                response.Data[i].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                            }
                            if (response.Data[i].ATTRIBUTE_NAME == "SUBITEM#") {
                                response.Data[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                            }
                        }
                        $scope.catalogCategoryattrDataSource.read();
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {

                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });

                    } else {
                        $rootScope.items;
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, $rootScope.items).success(function (response) {
                            if ($scope.CheckboxCategorySpecification == $scope.UncheckboxCategorySpecification) {
                                $("#selectAllCatSpecs").prop("checked", true);
                            }
                            else {
                                $("#selectAllCatSpecs").prop("checked", false);
                            }
                            $('#selectAllCatSpecs').prop('checked', false);
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info",
                            });
                            $scope.catalogCategoryattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });

        $scope.CategorySpecGridOptions = {
            dataSource: $scope.CategorySpecsdataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
             { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox" ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionCS($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllCatSpecs' class='mc-checkbox' ng-click='selectAllCS($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };


        $scope.updateSelectionCS = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                if (e.target.checked == false) {
                    $scope.UnCheckboxCategorySpecification = $scope.UnCheckboxCategorySpecification - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxCategorySpecification = $scope.UnCheckboxCategorySpecification + 1;
                }

                if ($scope.CheckboxCategorySpecification == $scope.UncheckboxCategorySpecification) {
                    $("#selectAllCatSpecs").prop("checked", true);
                }
                else {
                    $("#selectAllCatSpecs").prop("checked", false);
                }
                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });

            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };


        $scope.selectAllCS = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxCategorySpecification = $scope.CheckboxCategorySpecification;
            }
            else {
                $scope.UnCheckboxCategorySpecification = 0;
            }
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };




        // ---------------------------------------------- Category Image and Attachment -----------------------------------------

        $scope.CategoryImagedataSource = new kendo.data.DataSource({
            //type: "json",
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_ID", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllCatImg;
                    if ($("#selectAllCatImg").prop("checked") == true) {
                        selectAllCatImg = 1;
                    }
                    else if ($("#selectAllCatImg").prop("checked") == false) {
                        selectAllCatImg = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 23, options.data).success(function (response) {
                        $scope.CheckboxCategoryImage = response.Total;
                        $scope.UnCheckboxCategoryImage = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxCategoryImage = $scope.UnCheckboxCategoryImage + 1;
                            }
                        }
                        if ($scope.CheckboxCategoryImage == $scope.UnCheckboxCategoryImage) {
                            $("#selectAllCatImg").prop("checked", true);
                        }
                        else {
                            $("#selectAllCatImg").prop("checked", false);
                        }
                        if (selectAllCatImg == 1) {
                            $('#selectAllCatImg').prop('checked', false);
                        }
                        $scope.CatImagecount = response.Total;
                        for (var i = 0 ; response.Data.length > i ; i++) {
                            if (response.Data[i].ATTRIBUTE_NAME == "ITEM#") {
                                response.Data[i].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                            }
                            if (response.Data[i].ATTRIBUTE_NAME == "SUBITEM#") {
                                response.Data[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                            }
                        }
                        $scope.catalogCategoryattrDataSource.read();
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {

                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });

                    } else {
                        $rootScope.items;
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, $rootScope.items).success(function (response) {
                            $('#selectAllCatImg').prop('checked', false);
                            if ($scope.CheckboxCategoryImage == $scope.UnCheckboxCategoryImage) {
                                $("#selectAllCatImg").prop("checked", true);
                            }
                            else {
                                $("#selectAllCatImg").prop("checked", false);
                            }
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info",
                            });
                            $scope.catalogCategoryattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });

        $scope.CategoryImageGridOptions = {
            dataSource: $scope.CategoryImagedataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
             { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox" ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionCI($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllCatImg' class='mc-checkbox' ng-click='selectAllCI($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };

        $scope.updateSelectionCI = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                if (e.target.checked == false) {
                    $scope.UnCheckboxCategoryImage = $scope.UnCheckboxCategoryImage - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxCategoryImage = $scope.UnCheckboxCategoryImage + 1;
                }

                if ($scope.CheckboxCategoryImage == $scope.UnCheckboxCategoryImage) {
                    $("#selectAllCatImg").prop("checked", true);
                }
                else {
                    $("#selectAllCatImg").prop("checked", false);
                }
                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });
            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };


        $scope.selectAllCI = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxCategoryImage = $scope.CheckboxCategoryImage;
            }
            else {
                $scope.UnCheckboxCategoryImage = 0;
            }
            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };

        // --------------------------------------------- Category Description ----------------------------------------------

        $scope.CategoryDescdataSource = new kendo.data.DataSource({
            sort: [{ field: "ISAvailable", dir: "desc" },
                { field: "ATTRIBUTE_ID", dir: "asc" }],
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true, editable: true,
            transport: {
                read: function (options) {
                    var selectAllCatDesc;
                    if ($("#selectAllCatDesc").prop("checked") == true) {
                        selectAllCatDesc = 1;
                    }
                    else if ($("#selectAllCatDesc").prop("checked") == false) {
                        selectAllCatDesc = 0;
                    }
                    dataFactory.GetAllCatalogattributesForAttribute($scope.Catalog.CATALOG_ID, 25, options.data).success(function (response) {
                        $scope.CheckboxCategoryDescription = response.Total;
                        $scope.UnCheckboxCategoryDescription = 0;
                        for (var i = 0; i < response.Total; i++) {
                            if (response.Data[i].ISAvailable == true) {
                                $scope.UnCheckboxCategoryDescription = $scope.UnCheckboxCategoryDescription + 1;
                            }
                        }
                        if ($scope.CheckboxCategoryDescription == $scope.UnCheckboxCategoryDescription) {
                            $("#selectAllCatDesc").prop("checked", true);
                        }
                        else {
                            $("#selectAllCatDesc").prop("checked", false);
                        }
                        if (selectAllCatDesc == 1) {
                            $('#selectAllCatDesc').prop('checked', false);
                        }
                        $scope.CatDesccount = response.Total;
                        for (var i = 0 ; response.Data.length > i ; i++) {
                            if (response.Data[i].ATTRIBUTE_NAME == "ITEM#") {
                                response.Data[i].ATTRIBUTE_NAME = $localStorage.CatalogItemNumber;
                            }
                            if (response.Data[i].ATTRIBUTE_NAME == "SUBITEM#") {
                                response.Data[i].ATTRIBUTE_NAME = $localStorage.CatalogSubItemNumber;
                            }
                        }
                        $scope.catalogCategoryattrDataSource.read();
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                    if ($scope.Catalog.CATALOG_ID === 1) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Master Catalog update is not permitted.',
                            type: "info"
                        });
                    } else {
                        $rootScope.items;
                        dataFactory.savecatalogallattrdetails($scope.Catalog.CATALOG_ID, $rootScope.items).success(function (response) {
                            $('#selectAllCatDesc').prop('checked', false);
                            if ($scope.CheckboxCategoryDescription == $scope.UnCheckboxCategoryDescription) {
                                $("#selectAllCatDesc").prop("checked", true);
                            }
                            else {
                                $("#selectAllCatDesc").prop("checked", false);
                            }
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Process completed.',
                                type: "info",
                            });
                            $scope.catalogCategoryattrDataSource.read();
                            options.success(response);
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            }, batch: true,
            schema: {
                data: "Data",
                total: "Total",
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: false },
                        ATTRIBUTE_TYPE: { editable: false },
                        ISAvailable: { editable: true, type: "boolean", sortable: false }
                    }
                }
            }
        });

        $scope.CategoryDescGridOptions = {
            dataSource: $scope.CategoryDescdataSource,
            sortable: true, scrollable: true, editable: true, autoBind: false,
            toolbar: [
             { name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'title='Save Selected Attributes'>Save</button>" },
             { name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'title='Cancel Attribute Selection'>Cancel</button>" }],
            filterable: true,
            selectable: "row",
            columns: [
                { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox" ng-disabled="!userRoleModifyCatalogAttribute" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelectionCD($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllCatDesc' class='mc-checkbox' ng-click='selectAllCD($event)'/>" },
                { field: "ATTRIBUTE_NAME", title: "Attribute Name" }
            ], dataBound: function () {
            },
            remove: function () {
                // e.model.ATTRIBUTE_ID
            }
        };

        $scope.catalogCategoryattrDataSource = new kendo.data.DataSource({
            //type: "json",
            serverFiltering: true,
            transport: {
                read: function (options) {
                    dataFactory.GetCatalogCategoryattrlistDetails($scope.Catalog.CATALOG_ID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }
        });


        $scope.updateSelectionCD = function (e, id) {

            if (id.dataItem.ATTRIBUTE_ID !== 1) {
                id.dataItem.set("ISAvailable", e.target.checked);
                if (e.target.checked == false) {
                    $scope.UnCheckboxCategoryDescription = $scope.UnCheckboxCategoryDescription - 1;
                }
                else if (e.target.checked == true) {
                    $scope.UnCheckboxCategoryDescription = $scope.UnCheckboxCategoryDescription + 1;
                }

                if ($scope.CheckboxCategoryDescription == $scope.UnCheckboxCategoryDescription) {
                    $("#selectAllCatDesc").prop("checked", true);
                }
                else {
                    $("#selectAllCatDesc").prop("checked", false);
                }
                $rootScope.items.push({
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    ISAvailable: id.dataItem.ISAvailable,
                    ATTRIBUTE_NAME: id.dataItem.ATTRIBUTE_NAME,
                    CATALOG_ID: id.dataItem.CATALOG_ID,
                    FAMILY_ID: id.dataItem.FAMILY_ID,
                    PRODUCT_ID: id.dataItem.PRODUCT_ID,
                });

            } else {
                id.dataItem.set("ISAvailable", true);
            }
        };



        $scope.selectAllCD = function (ev) {
            if (ev.target.checked == true) {
                $scope.UnCheckboxCategoryDescription = $scope.CheckboxCategoryDescription;
            }
            else {
                $scope.UnCheckboxCategoryDescription = 0;
            }

            var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
            var items = grid.dataItems();
            $rootScope.items = items;
            items.forEach(function (item) {
                if (item.ATTRIBUTE_ID !== 1) {
                    item.set("ISAvailable", ev.target.checked);
                }
            });
        };




        // Category Level Attributes end

        //File Upload

        $scope.loadSortOrder = function () {
            blockUI.start();
            $scope.catalogproductattrDataSource.read();
            $scope.catalogfamilyattrDataSource.read();
            $scope.catalogCategoryattrDataSource.read();
            blockUI.stop();
        };

        $scope.defaultfamily = function () {
           
            if ($scope.Catalog.CATALOG_ID != '') {
                dataFactory.getdefaultfamily($scope.Catalog.CATALOG_ID).success(function (response) {
                    if (response == "Default Family") {
                        $scope.DEFAULT_FAMILY_NAME = 'Default Product';
                    }
                    else {
                        $scope.DEFAULT_FAMILY_NAME = response;
                    }
                })
            }
            else {
                $scope.DEFAULT_FAMILY_NAME = 'Default Product';
            }
        };

    }]);

