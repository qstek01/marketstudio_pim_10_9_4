﻿/// <reference path="../Partials/ImageManagementPopup.html" />
/// <reference path="../Partials/ImageManagementPopup.html" />


LSApp.controller('categoryController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $rootScope, $localStorage) {

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;

    $scope.SelectedFileForCategory = '';

    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    $scope.onOff = true;
    $scope.categoryCustom = {
        CUSTOM_NUM_FIELD: {},
        CUSTOM_TEXT_FIELD: {}
    };
    $rootScope.attributesetup = false;
    $rootScope.attributepack = false;

    $scope.isNavigate = true;
    $scope.isSearch = false;
    // $("#gridcategory").show();
    $rootScope.items = [];
    $scope.Category = {
        CATEGORY_NAME: 'New Category',
        CATEGORY_ID: '',
        SHORT_DESC: '',
        PARENT_CATEGORY: '',
        PUBLISH: true,
        PUBLISH2PRINT: true,
        PUBLISH2WEB: true,
        PUBLISH2PDF: true,
        PUBLISH2EXPORT: true,
        PUBLISH2PORTAL: true,
        CUSTOM_TEXT_FIELD1: '',
        CUSTOM_TEXT_FIELD2: '',
        CUSTOM_TEXT_FIELD3: '',
        CUSTOM_NUM_FIELD1: '',
        CUSTOM_NUM_FIELD2: '',
        CUSTOM_NUM_FIELD3: '',
        IMAGE_FILE: '',
        IMAGE_FILE2: '',
        IMAGE_NAME: '',
        IMAGE_NAME2: '',
        PARENT_CATEGORYNAME: "",
        STATUS_NAME: '',
        IS_CLONE: false,
        CLONE_LOCK: false,
        CATEGORY_SHORT: 'New ID',
        CATEGORY_PARENT_SHORT: '0',
        CUSTOMER_ID: 1
    };

    $scope.ActiveCategoryId = '';

    $scope.CategoryImage = '';

    $scope.CatalogAssociation = {
        CATALOG_ID: '',
        CATALOG_NAME: '',
        CATEGORY_ID: ''
    };

    //---------------------------------------undo declaration-----------------------------------------//
    $scope.baseCatFlag = 0;
    $rootScope.categoryUndoList = [];

    if ($localStorage.categoryChangedListUndo != undefined && $localStorage.categoryChangedListUndo != []) {
        $rootScope.categoryUndoList = angular.copy($localStorage.categoryChangedListUndo);
    };
    //---------------------------------------undo declaration-----------------------------------------//

    $scope.$on("clearids", function (event) {
        if ($scope.Category.CUSTOM_TEXT_FIELD1 !== null && $scope.Category.CUSTOM_TEXT_FIELD1 != "") {
            $scope.categoryCustom.CUSTOM_TEXT_FIELD[1] = "";
        }
        if ($scope.Category.CUSTOM_TEXT_FIELD2 !== null && $scope.Category.CUSTOM_TEXT_FIELD2 != "") {
            $scope.categoryCustom.CUSTOM_TEXT_FIELD[2] = "";
        }
        if ($scope.Category.CUSTOM_TEXT_FIELD3 !== null && $scope.Category.CUSTOM_TEXT_FIELD3 != "") {
            $scope.categoryCustom.CUSTOM_TEXT_FIELD[3] = "";
        }
        if ($scope.Category.CUSTOM_NUM_FIELD1 !== null && $scope.Category.CUSTOM_NUM_FIELD1 != 0) {
            $scope.categoryCustom.CUSTOM_NUM_FIELD[1] = "";
        }
        if ($scope.Category.CUSTOM_NUM_FIELD2 !== null && $scope.Category.CUSTOM_NUM_FIELD2 != 0) {
            $scope.categoryCustom.CUSTOM_NUM_FIELD[2] = "";
        }
        if ($scope.Category.CUSTOM_NUM_FIELD3 !== null && $scope.Category.CUSTOM_NUM_FIELD3 != 0) {
            $scope.categoryCustom.CUSTOM_NUM_FIELD[3] = "";
        }
    });


    $scope.ModifyResults = function (data) {
        // $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;        
        if (data.length === 0) {
            $("#dynamictable").hide();
        }
        $("#familyEditor").hide();
        $("#recentmodifiedtab").show();
        $("#resultbtntab").hide();
    };


    //$scope.mainCategoryGridOptions = {
    //    dataSource: $scope.GetAllCategorydataSource,
    //    columns: [
    //        { field: "CATALOG_ID", title: "CATALOG_ID", width: "250px" },
    //        { field: "CATALOG_NAME", title: "CATALOG_NAME", width: "250px" },
    //        {
    //            field: "CATEGORY_ID",
    //            title: "CATEGORY_ID",
    //            hidden: true,
    //            aggregates: ["count"],
    //            groupHeaderTemplate: groupHeaderName
    //        }]
    //};
    //$scope.getallcategorydatasource = new kendo.data.datasource({
    //    type: "json",
    //    serverfiltering: true,  pagesize: 20,
    //    sort: { field: "catalog_id", dir: "asc" },
    //    transport: {
    //        read: function (options) {
    //            dataFactory.getcatalogassociation($scope.ActiveCategoryId).success(function (response) {
    //            //$http.get("../category/getcatalogassociation?categoryid=" + $scope.ActiveCategoryId).success(function (response) {
    //                options.success(response);
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //    }, schema: {
    //        model: {
    //            id: "catalog_id",
    //            fields: {
    //                catalog_name: { editable: false },
    //                category_name: { editable: false }
    //            }
    //        }
    //    },
    //    group: {
    //        field: "category_name", aggregates: [
    //           { field: "category_name", aggregate: "count" }
    //        ]
    //    }
    //});

    $rootScope.LoaAttributeData = function () {
        $scope.RefreshCategoryAttributes();
    };

    $scope.onSelect = function (e) {
        var message = $.map(e.files, function (file) { return file.name; }).join(", ");
        // $scope.CategoryImage = '~/Images/' + message;
        // alert(message);
        //kendoConsole.log("event :: select (" + message + ")");
    };
    $scope.onSelect1 = function (e) {
        var message = $.map(e.files, function (file) { return file.name; }).join(", ");
        // $scope.Category.IMAGE_FILE2 = '\\' + $scope.getCustomerCompanyName + '\\Images\\' + message;
        // $scope.$apply();
        // $scope.CategoryImage = '~/Images/' + message;
        // alert(message);
        //kendoConsole.log("event :: select (" + message + ")");
    };
    $scope.isImage = function (image) {

        if (image.split('.')[image.split('.').length - 1] == "eps" || image.split('.')[image.split('.').length - 1] == "psd" || image.split('.')[image.split('.').length - 1] == "tiff" || image.split('.')[image.split('.').length - 1].toLowerCase() == "tif") {
            $scope.imageInvalid = "Preview only available in Family/Category preview";
            return true;
        } else {
            return false;
        }
    };
    $scope.onSuccess = function (e) {
        // var someInfo = e.response.someInfo;
        if (e.operation === "remove") {
            $scope.Category.IMAGE_FILE = "";
        } else {

            var message = $.map(e.files, function (file) { return file.name; }).join(", ");
            if (message.indexOf('&') == -1 && message.indexOf('#') == -1 && message.indexOf('\'') == -1) {
                $scope.$apply(function () {
                    $scope.Category.IMAGE_FILE = '\\Images\\' + message;
                    //  alert($scope.Category.IMAGE_FILE);
                });
                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Image name contains invalid special characters( # & \' ).',
                    type: "error"
                });
                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            }

        }
    };
    $scope.onSuccess1 = function (e) {
        if (e.operation === "remove") {
            $scope.Category.IMAGE_FILE2 = "";
        } else {
            var message = $.map(e.files, function (file) { return file.name; }).join(", ");
            if (message.indexOf('&') == -1 && message.indexOf('#') == -1 && message.indexOf('\'') == -1) {
                $scope.$apply(function () {
                    $scope.Category.IMAGE_FILE2 = '\\Images\\' + message;
                });
                $(".k-upload-files").remove();
                $(".k-upload-status").remove();

            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Image name contains invalid special characters( # & \' ).',
                    type: "error"
                });
                $(".k-upload-files").remove();
                $(".k-upload-status").remove();
            }
            //$scope.Category.IMAGE_FILE2 = '\\' + $localStorage.getCustomerCompanyName + '\\Images\\' + message;
        }
    };
    //$scope.onFileSelect = function ($files) {
    //    $scope.progress = 0;
    //    fileReader.readAsDataUrl($scope.file, $scope)
    //        .then(function (result) {
    //            $scope.imageSrc = result;
    //        });
    //    //$files: an array of files selected, each file has name, size, and type.
    //    for (var i = 0; i < $files.length; i++) {
    //        var $file = $files[i];
    //        $http.uploadFile({
    //            url: 'Images', //upload.php script, node.js route, or servlet uplaod url)
    //            data: { myObj: $scope.myModelObj },
    //            file: $file
    //        }).then(function (data, status, headers, config) {
    //            // file is uploaded successfully
    //            //console.log(data);
    //        });
    //    }
    //};

    $scope.new = function () {
        $scope.$apply(function () {
            $scope.onOff = true;
            $scope.PlaceHolder_CATEGORY_NAME = "New Category";
            $scope.PlaceHolder_CATEGORY_ID = "New ID";
            $scope.Category.CATEGORY_NAME = "";
            $scope.Category.CATEGORY_SHORT = "";
            $scope.Category.SHORT_DESC = "";
            $scope.Category.PUBLISH = true;
            $scope.Category.PUBLISH2PRINT = true;
            $scope.Category.PUBLISH2WEB = true,
            $scope.Category.PUBLISH2PDF = true,
            $scope.Category.PUBLISH2EXPORT = true,
            $scope.Category.PUBLISH2PORTAL = true,
            $scope.Category.CUSTOM_TEXT_FIELD1 = "";
            $scope.Category.CUSTOM_TEXT_FIELD2 = "";
            $scope.Category.CUSTOM_TEXT_FIELD3 = "";
            $scope.Category.CUSTOM_NUM_FIELD1 = "";
            $scope.Category.CUSTOM_NUM_FIELD2 = "";
            $scope.Category.CUSTOM_NUM_FIELD3 = "";
            $scope.categoryCustom.CUSTOM_TEXT_FIELD[1] = "";
            $scope.categoryCustom.CUSTOM_TEXT_FIELD[2] = "";
            $scope.categoryCustom.CUSTOM_TEXT_FIELD[3] = "";
            $scope.categoryCustom.CUSTOM_NUM_FIELD[1] = "";
            $scope.categoryCustom.CUSTOM_NUM_FIELD[2] = "";
            $scope.categoryCustom.CUSTOM_NUM_FIELD[3] = "";
            $scope.Category.IMAGE_FILE = "";
            $scope.Category.IMAGE_FILE2 = "";
            $scope.Category.IMAGE_NAME = "";
            $scope.Category.IMAGE_NAME2 = "";
            $scope.Category.STATUS_NAME = "New";
        });
        $scope.ActiveCategoryId = 0;
        $scope.CatalogAssociationdatasource.read();
        $rootScope.selectedcategory_id = 0;
        $scope.NewCategoryId = 0;
        $scope.workflowDataSourceCategorylatest.read();
    };
    $scope.newcate = function () {
        $scope.onOff = true;
        $scope.Category.CATEGORY_NAME = "";
        $scope.Category.CATEGORY_SHORT = "";
        $scope.Category.SHORT_DESC = "";
        $scope.Category.PUBLISH = true;
        $scope.Category.PUBLISH2PRINT = true;
        $scope.Category.PUBLISH2WEB = true;
        $scope.Category.PUBLISH2PDF = true;
        $scope.Category.PUBLISH2EXPORT = true;
        $scope.Category.PUBLISH2PORTAL = true;
        $scope.Category.CUSTOM_TEXT_FIELD1 = "";
        $scope.Category.CUSTOM_TEXT_FIELD2 = "";
        $scope.Category.CUSTOM_TEXT_FIELD3 = "";
        $scope.Category.CUSTOM_NUM_FIELD1 = "";
        $scope.Category.CUSTOM_NUM_FIELD2 = "";
        $scope.Category.CUSTOM_NUM_FIELD3 = "";
        $scope.Category.IMAGE_FILE = "";
        $scope.Category.IMAGE_FILE2 = "";
        $scope.Category.IMAGE_NAME = "";
        $scope.Category.IMAGE_NAME2 = "";
        $scope.Category.STATUS_NAME = "New";
        $scope.ActiveCategoryId = 0;
        $scope.CatalogAssociationdatasource.read();
    };

    $scope.$on("NewCategoryIds", function (event) {
        debugger
        $("#accordion").accordion({ active: 0 });
        $scope.IsNewCategory = '1';
        $scope.newcate();
        document.getElementById("txt_CategoryID").disabled = false;
    });

    $scope.$on("New_Category_Id", function (event) {
        debugger
        $("#accordion").accordion({ active: 0 });
        $scope.IsNewCategory = '1';
        $scope.attributes = [];
        $scope.new();
        document.getElementById("txt_CategoryID").disabled = false;
        $("#FamilySpecsTab").hide();
        //$("#columnsetupcategory").hide();
    });
    $scope.$on("Active_Category_Id", function (event, selectedCategoryId) {

        //CloseDiv_TextEditor(this);
        //$(".k-upload-files.k-reset").find("li").remove();

        $scope.isNavigate = false;
        $scope.isSearch = true;
        $(".k-upload-files").remove();
        $(".k-upload-status").remove();
        $(".k-upload.k-header").addClass("k-upload-empty");
        $(".k-upload-button").removeClass("k-state-focused");
        document.getElementById("txt_CategoryID").disabled = true;
        $scope.IsNewCategory = '0';
        $scope.ActiveCategoryId = selectedCategoryId;
        $http.get("../Category/GetCategory?CategoryId=" + selectedCategoryId).
            then(function (categoryDetails) {
                $scope.onOff = true;
                $("#accordion").accordion({ active: 0 });
                $scope.Category = categoryDetails.data;
                if ($scope.Category.IMAGE_FILE === null) {
                    $scope.Category.IMAGE_FILE = "";
                }
                if ($scope.Category.IMAGE_FILE2 === null) {
                    $scope.Category.IMAGE_FILE2 = "";
                }
                if ($scope.Category.CUSTOM_TEXT_FIELD1 !== null && $scope.Category.CUSTOM_TEXT_FIELD1 != "") {
                    $scope.categoryCustom.CUSTOM_TEXT_FIELD[1] = $scope.Category.CUSTOM_TEXT_FIELD1;
                } else if ($scope.categoryCustom.CUSTOM_TEXT_FIELD[1] != undefined) {
                    $scope.categoryCustom.CUSTOM_TEXT_FIELD[1] = "";
                }
                if ($scope.Category.CUSTOM_TEXT_FIELD2 !== null && $scope.Category.CUSTOM_TEXT_FIELD2 != "") {
                    $scope.categoryCustom.CUSTOM_TEXT_FIELD[2] = $scope.Category.CUSTOM_TEXT_FIELD2;
                }
                else if ($scope.categoryCustom.CUSTOM_TEXT_FIELD[2] != undefined) {
                    $scope.categoryCustom.CUSTOM_TEXT_FIELD[2] = "";
                }
                if ($scope.Category.CUSTOM_TEXT_FIELD3 !== null && $scope.Category.CUSTOM_TEXT_FIELD3 != "") {
                    $scope.categoryCustom.CUSTOM_TEXT_FIELD[3] = $scope.Category.CUSTOM_TEXT_FIELD3;
                }
                else if ($scope.categoryCustom.CUSTOM_TEXT_FIELD[3] != undefined) {
                    $scope.categoryCustom.CUSTOM_TEXT_FIELD[3] = "";
                }

                if ($scope.Category.CUSTOM_NUM_FIELD1 !== null && $scope.Category.CUSTOM_NUM_FIELD1 != 0) {
                    $scope.categoryCustom.CUSTOM_NUM_FIELD[1] = $scope.Category.CUSTOM_NUM_FIELD1;

                }
                else if ($scope.categoryCustom.CUSTOM_NUM_FIELD[1] != undefined) {
                    $scope.categoryCustom.CUSTOM_NUM_FIELD[1] = "";
                }
                if ($scope.Category.CUSTOM_NUM_FIELD2 !== null && $scope.Category.CUSTOM_NUM_FIELD2 != 0) {
                    $scope.categoryCustom.CUSTOM_NUM_FIELD[2] = $scope.Category.CUSTOM_NUM_FIELD2;
                }
                else if ($scope.categoryCustom.CUSTOM_NUM_FIELD[2] != undefined) {
                    $scope.categoryCustom.CUSTOM_NUM_FIELD[2] = "";
                }
                if ($scope.Category.CUSTOM_NUM_FIELD3 !== null && $scope.Category.CUSTOM_NUM_FIELD3 != 0) {
                    $scope.categoryCustom.CUSTOM_NUM_FIELD[3] = $scope.Category.CUSTOM_NUM_FIELD3;
                }
                else if ($scope.categoryCustom.CUSTOM_NUM_FIELD[3] != undefined) {
                    $scope.categoryCustom.CUSTOM_NUM_FIELD[3] = "";
                }
                if (($scope.Category.CUSTOM_NUM_FIELD3 !== null && $scope.Category.CUSTOM_NUM_FIELD3 != 0) || ($scope.Category.CUSTOM_TEXT_FIELD3 !== null && $scope.Category.CUSTOM_TEXT_FIELD3 != "")) {
                    $scope.choices = [{ CUSTOM_TEXT_FIELD: 1, CUSTOM_NUM_FIELD: 1 },
                       { CUSTOM_TEXT_FIELD: 2, CUSTOM_NUM_FIELD: 2 },
                       { CUSTOM_TEXT_FIELD: 3, CUSTOM_NUM_FIELD: 3 }];
                }

                else if (($scope.Category.CUSTOM_NUM_FIELD2 !== null && $scope.Category.CUSTOM_NUM_FIELD2 != 0) || ($scope.Category.CUSTOM_TEXT_FIELD2 !== null && $scope.Category.CUSTOM_TEXT_FIELD2 != "")) {
                    $scope.choices = [{ CUSTOM_TEXT_FIELD: 1, CUSTOM_NUM_FIELD: 1 },
                       { CUSTOM_TEXT_FIELD: 2, CUSTOM_NUM_FIELD: 2 }];
                }
                else {
                    $scope.choices = [{ CUSTOM_TEXT_FIELD: 1, CUSTOM_NUM_FIELD: 1 }];
                }

                $rootScope.selecetedParentCategoryId = $scope.Category.PARENT_CATEGORY;
                if ($scope.Category.PARENT_CATEGORY === "0") {
                    $("#txt_ParentCategoryID").val("Root");
                }

                //-------------------------Undo functionality obtaining base value of the Category ----------------------//
                $scope.baseCatFlag = 1;
                //-------------------------Undo functionality obtaining base value of the Category ----------------------//      

                $rootScope.FamilyCATEGORY_ID = $scope.Category.CATEGORY_ID;
                //}
                $("#familycategoryname").val($scope.Category.CATEGORY_NAME);
                $scope.CatalogAssociationdatasource.read();
                $("#FamilySpecsTab").show();
                $scope.LoadCategorySpec($rootScope.selecetedCatalogId, selectedCategoryId);
                $("#attributePack").hide();
                $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
                $("#gridcategory").show();
                $("#columnsetupcategory").hide();
                $("#CategoryAttributePack").hide();
                $scope.categoryFamilyAttributePackdataSource.read();
                $scope.categoryProductAttributePackdataSource.read();
            });

        $scope.workflowDataSourceCategorylatest.read();
    });

    $scope.CatalogAssociationdatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, sort: { field: "CATALOG_ID", dir: "asc" },
        pageable: true,
        pageSize: 5,
        serverPaging: true,
        serverSorting: true, autoBind: false,
        transport: {
            read: function (options) {
                dataFactory.GetCatalogAssociation($scope.ActiveCategoryId, options.data).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        }, schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "CATALOG_ID",
                fields: {
                    CATALOG_ID: { type: "number", editable: false },
                    CATALOG_NAME: { editable: false },
                    CATEGORY_SHORT: { editable: false }
                }
            }
        }
    });

    $scope.ContextCategoryId = '';
    $scope.CatalogAssociationgrid = {
        dataSource: $scope.CatalogAssociationdatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 },
        columns: [
            { field: "CATALOG_ID", type: 'number', title: "Catalog ID", width: "100px" },
            { field: "CATALOG_NAME", title: "Catalog Name", width: "250px" },
           { field: "CATEGORY_SHORT", title: "Category ID", width: "250px" }]
    };

    $scope.$on("Context_Category_Id", function (event, contextCatId) {
        $scope.ContextCategoryId = contextCatId;
    });

    $scope.CutCategory = function () {

    };

    $scope.$on("DetachCateAttributes", function () {

        $scope.attributes = [];

    });

    // For New Detach Category
    $scope.Delete = function () {
        if ($scope.Category.CATEGORY_ID !== "") {
            if ($rootScope.SelectedNodeID == 0 || $rootScope.SelectedNodeID == "~" || $rootScope.SelectedNodeID == undefined) {
                $rootScope.SelectedNodeID = $scope.Category.CATEGORY_ID;
            }
            if ($rootScope.SelectedNodeID.includes('~!')) {
                $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
            }
            else if ($rootScope.SelectedNodeID.includes('~')) {
                $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
            }
            $rootScope.familyDeleteNavStatus = true;
            $rootScope.SelectedNodeIDPresist = 1;
            $rootScope.treeForCategory = true;
            dataFactory.ChkCategoryexists($rootScope.selecetedParentCategoryId, $scope.Category.CATEGORY_ID, $rootScope.selecetedCatalogId).success(function (responses) {
                if (responses == "true") {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Are you sure you want to remove Category Association?",
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }],
                        success: function (result) {
                            var bool = false;
                            if (result === "Yes") {
                                bool = true;
                            }
                            if (bool === true) {
                                $scope.deleteCategoryPdf();
                                dataFactory.DeleteCategory($rootScope.selecetedParentCategoryId, $scope.Category.CATEGORY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + response + '.',
                                        type: "info"
                                    });
                                    $scope.catalogDataSource.read();
                                    $rootScope.treeData.read();
                                    $scope.attributes = [];

                                    //$scope.treeData.read();
                                    $scope.Category = {
                                        CATEGORY_NAME: '',
                                        CATEGORY_SHORT: '',
                                        SHORT_DESC: '',
                                        PARENT_CATEGORY: '',
                                        PUBLISH: true,
                                        PUBLISH2PRINT: true,
                                        PUBLISH2WEB: true,
                                        PUBLISH2PDF: true,
                                        PUBLISH2EXPORT: true,
                                        PUBLISH2PORTAL: true,
                                        CUSTOM_TEXT_FIELD1: '',
                                        CUSTOM_TEXT_FIELD2: '',
                                        CUSTOM_TEXT_FIELD3: '',
                                        CUSTOM_NUM_FIELD1: '',
                                        CUSTOM_NUM_FIELD2: '',
                                        CUSTOM_NUM_FIELD3: '',
                                        IMAGE_FILE: '',
                                        IMAGE_FILE2: '',
                                        IMAGE_NAME: '',
                                        IMAGE_NAME2: '',
                                        PARENT_CATEGORYNAME: ""
                                    };

                                    document.getElementById("txt_CategoryID").disabled = false;
                                }).error(function (error) {
                                    options.error(error);
                                });

                            }
                        }
                    });
                } else if (responses == "false") {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "The selected category is available only in the working catalog, are you sure want to remove Category Association?",
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }],
                        success: function (result) {
                            var bool = false;
                            if (result === "Yes") {
                                bool = true;
                            }
                            if (bool === true) {
                                $scope.deleteCategoryPdf();
                                dataFactory.DeleteCategory($rootScope.selecetedParentCategoryId, $scope.Category.CATEGORY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + response + '.',
                                        type: "info"
                                    });
                                    $scope.catalogDataSource.read();
                                    $rootScope.treeData.read();
                                    $scope.attributes = [];

                                    //$scope.treeData.read();
                                    $scope.Category = {
                                        CATEGORY_NAME: '',
                                        CATEGORY_SHORT: '',
                                        SHORT_DESC: '',
                                        PARENT_CATEGORY: '',
                                        PUBLISH: true,
                                        PUBLISH2PRINT: true,
                                        PUBLISH2WEB: true,
                                        PUBLISH2PDF: true,
                                        PUBLISH2EXPORT: true,
                                        PUBLISH2PORTAL: true,
                                        CUSTOM_TEXT_FIELD1: '',
                                        CUSTOM_TEXT_FIELD2: '',
                                        CUSTOM_TEXT_FIELD3: '',
                                        CUSTOM_NUM_FIELD1: '',
                                        CUSTOM_NUM_FIELD2: '',
                                        CUSTOM_NUM_FIELD3: '',
                                        IMAGE_FILE: '',
                                        IMAGE_FILE2: '',
                                        IMAGE_NAME: '',
                                        IMAGE_NAME2: '',
                                        PARENT_CATEGORYNAME: ""
                                    };
                                    document.getElementById("txt_CategoryID").disabled = false;



                                }).error(function (error) {
                                    options.error(error);
                                });

                            }
                        }
                    });
                } else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "The selected category is available in another catalog, on removing association it will not appear in the Master Catalog, do you want to continue?",
                        type: "confirm",
                        buttons: [{ value: "Yes" }, { value: "No" }],
                        success: function (result) {
                            var bool = false;
                            if (result === "Yes") {
                                bool = true;
                            }

                            if (bool === true) {
                                $scope.deleteCategoryPdf();
                                dataFactory.DeleteCategory($rootScope.selecetedParentCategoryId, $scope.Category.CATEGORY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: '' + response + '.',
                                        type: "info"
                                    });
                                    $scope.catalogDataSource.read();
                                    $rootScope.treeData.read();
                                    $scope.attributes = [];
                                    //$scope.treeData.read();
                                    $scope.Category = {
                                        CATEGORY_NAME: '',
                                        CATEGORY_SHORT: '',
                                        SHORT_DESC: '',
                                        PARENT_CATEGORY: '',
                                        PUBLISH: true,
                                        PUBLISH2PRINT: true,
                                        PUBLISH2WEB: true,
                                        PUBLISH2PDF: true,
                                        PUBLISH2EXPORT: true,
                                        PUBLISH2PORTAL: true,
                                        CUSTOM_TEXT_FIELD1: '',
                                        CUSTOM_TEXT_FIELD2: '',
                                        CUSTOM_TEXT_FIELD3: '',
                                        CUSTOM_NUM_FIELD1: '',
                                        CUSTOM_NUM_FIELD2: '',
                                        CUSTOM_NUM_FIELD3: '',
                                        IMAGE_FILE: '',
                                        IMAGE_FILE2: '',
                                        IMAGE_NAME: '',
                                        IMAGE_NAME2: '',
                                        PARENT_CATEGORYNAME: ""
                                    };
                                    document.getElementById("txt_CategoryID").disabled = false;
                                }).error(function (error) {
                                    options.error(error);
                                });
                            }
                        }
                    });
                }
            }).error(function (error) {
                options.error(error);
            });

        }
    };
    $scope.DeleteFromMaster = function () {
        if ($scope.Category.CATEGORY_ID !== "") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to delete the Category?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    var bool = false;
                    if (result === "Yes") {
                        bool = true;
                    }
                    if (bool === true) {
                        dataFactory.DeleteCategoryFromMaster($rootScope.selecetedParentCategoryId, $scope.Category.CATEGORY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                            $scope.catalogDataSource.read();
                            $rootScope.treeData.read();
                            //$scope.treeData.read();
                            $scope.Category = {
                                CATEGORY_NAME: '',
                                CATEGORY_SHORT: '',
                                SHORT_DESC: '',
                                PARENT_CATEGORY: '',
                                PUBLISH: true,
                                PUBLISH2PRINT: true,
                                PUBLISH2WEB: true,
                                PUBLISH2PDF: true,
                                PUBLISH2EXPORT: true,
                                PUBLISH2PORTAL: true,
                                CUSTOM_TEXT_FIELD1: '',
                                CUSTOM_TEXT_FIELD2: '',
                                CUSTOM_TEXT_FIELD3: '',
                                CUSTOM_NUM_FIELD1: '',
                                CUSTOM_NUM_FIELD2: '',
                                CUSTOM_NUM_FIELD3: '',
                                IMAGE_FILE: '',
                                IMAGE_FILE2: '',
                                IMAGE_NAME: '',
                                IMAGE_NAME2: '',
                                PARENT_CATEGORYNAME: ""
                            };
                            document.getElementById("txt_CategoryID").disabled = false;
                        }).error(function (error) {
                            options.error(error);
                        });

                    }
                }
            });
        }
    };
    $scope.trashFromMaster = function () {
        if ($rootScope.newCat == true) {
            return;
        }
        $rootScope.newCat = false;
        if ($scope.Category.CATEGORY_ID !== "") {
            //if ($rootScope.SelectedNodeID == 0 || $rootScope.SelectedNodeID == "~" || $rootScope.SelectedNodeID == undefined) {
            //    $rootScope.SelectedNodeID = $scope.Category.CATEGORY_ID;
            //}
            if ($rootScope.SelectedNodeID.includes('~!')) {
                $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
            }
            else if ($rootScope.SelectedNodeID.includes('~')) {
                $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
            }

            $rootScope.treeForCategory = true;
            $rootScope.familyDeleteNavStatus = true;
            $rootScope.SelectedNodeIDPresist = 1;
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to Delete the Category?", //move into Recycle Bin
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {
                    var bool = false;
                    if (result === "Yes") {
                        bool = true;
                    }
                    if (bool === true) {
                        $scope.deleteCategoryPdf();
                        dataFactory.trashCategoryFromMaster($rootScope.selecetedParentCategoryId, $scope.Category.CATEGORY_ID, $rootScope.selecetedCatalogId).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                            $scope.catalogDataSource.read();
                            $rootScope.treeData.read();
                            //$scope.treeData.read();
                            $scope.attributes = [];
                            $scope.Category = {
                                CATEGORY_ID: '',
                                CATEGORY_NAME: '',
                                CATEGORY_SHORT: '',
                                SHORT_DESC: '',
                                PARENT_CATEGORY: '',
                                PUBLISH: true,
                                PUBLISH2PRINT: true,
                                PUBLISH2WEB: true,
                                PUBLISH2PDF: true,
                                PUBLISH2EXPORT: true,
                                PUBLISH2PORTAL: true,
                                CUSTOM_TEXT_FIELD1: '',
                                CUSTOM_TEXT_FIELD2: '',
                                CUSTOM_TEXT_FIELD3: '',
                                CUSTOM_NUM_FIELD1: '',
                                CUSTOM_NUM_FIELD2: '',
                                CUSTOM_NUM_FIELD3: '',
                                IMAGE_FILE: '',
                                IMAGE_FILE2: '',
                                IMAGE_NAME: '',
                                IMAGE_NAME2: '',
                                PARENT_CATEGORYNAME: ""
                            };
                            document.getElementById("txt_CategoryID").disabled = false;
                        }).error(function (error) {
                            options.error(error);
                        });

                    }
                }
            });
        }
    };
    $scope.IsNewCategory = '1';
    document.getElementById("txt_CategoryID").disabled = false;
    //$scope.PreviewCAT = function () {
    //   // $("#formDataView").hide();
    //    $("#previewDataView").show();
    //};

    $scope.ShowFormData = function () {
        $("#formDataView").show();
        $("#previewDataView").hide();
    };
    $scope.publish = function () {
        if (document.getElementById('chkPublish').checked === true) {
            $scope.Category.PUBLISH2WEB = true;
        } else {
            $scope.Category.PUBLISH2WEB = false;
        }
    };
    $scope.publishPdf = function () {
        if (document.getElementById('chkPublishToPdf').checked === true) {
            $scope.Category.PUBLISH2PDF = true;
        } else {
            $scope.Category.PUBLISH2PDF = false;
        }
    };
    $scope.publishExport = function () {
        if (document.getElementById('chkPublishToExport').checked === true) {
            $scope.Category.PUBLISH2EXPORT = true;
        } else {
            $scope.Category.PUBLISH2EXPORT = false;
        }
    };
    $scope.publishPortal = function () {
        if (document.getElementById('chkPublishToPrintToPortal').checked === true) {
            $scope.Category.PUBLISH2PORTAL = true;
        } else {
            $scope.Category.PUBLISH2PORTAL = false;
        }
    };
    $scope.publishToPrint = function () {
        if (document.getElementById('chkPublishToPrint').checked === true) {
            $scope.Category.PUBLISH2PRINT = true;
        } else {
            $scope.Category.PUBLISH2PRINT = false;
        }
    };
    $scope.clearCategoryImgAttach = function () {
        if ($scope.Category.CATEGORY_SHORT !== "") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure want to delete the Category Image?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {

                    if (result === "Yes") {

                        dataFactory.clearCategoryImgAttach($rootScope.selecetedParentCategoryId, $scope.Category).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                            $(".k-upload-files").remove();
                            $(".k-upload-status").remove();
                            $scope.catalogDataSource.read();
                            $rootScope.treeData.read();
                            $scope.Category.IMAGE_FILE = '';
                            $scope.Category.IMAGE_NAME = '';
                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                    else {
                        return;
                        //  $scope.catalogDataSource.read();
                    }
                }
            });

        }
    };
    $scope.clearCategoryImgAttach2 = function () {
        if ($scope.Category.CATEGORY_SHORT !== "") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure want to delete the Category Image?",
                type: "confirm",
                buttons: [{ value: "Yes" }, { value: "No" }],
                success: function (result) {

                    if (result === "Yes") {
                        dataFactory.clearCategoryImgAttach2($rootScope.selecetedParentCategoryId, $scope.Category).success(function (response) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                            $(".k-upload-files").remove();
                            $(".k-upload-status").remove();
                            $scope.catalogDataSource.read();
                            $rootScope.treeData.read();
                            $scope.Category.IMAGE_FILE2 = '';
                            $scope.Category.IMAGE_NAME2 = '';

                        }).error(function (error) {
                            options.error(error);
                        });

                    }
                    else {
                        return;
                        //  $scope.catalogDataSource.read();
                    }
                }
            });



        }
    };


    $scope.Create = function (selectedRow) {
        //$scope.NewCategoryId = selectedRow.CATEGORY_ID;
        var CheckPdfCreate = false;
        // $scope.saveCategorySessionForPdfXpress();
        //   $('#opendesigner').hide();
        //   $scope.IsFormSubmitted = true;
        //   $scope.Message = "";
        //   $scope.ChechFileValid($scope.SelectedFileForUpload);
        if ($scope.getCategoryPdfExpress == "" && $scope.undoCategoryClickFlag == "1") {
            var $el = $('#chooseFile');
            if ($el.val() != "") {
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();
            }
        }
        else if ($scope.SelectedFileForUpload != undefined) {
            CheckPdfCreate = true;
        }


        //if ($rootScope.SelectedNodeID == 0 || $rootScope.SelectedNodeID == "~" || $rootScope.SelectedNodeID == undefined) {
        //    $rootScope.SelectedNodeID = $scope.Category.CATEGORY_ID;
        //}
        //if ($rootScope.SelectedNodeID.includes('~!')) {
        //    $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~!')[0];
        //}
        //else if ($rootScope.SelectedNodeID.includes('~')) {
        //    $rootScope.familyDeleteNav = $rootScope.SelectedNodeID.split('~')[$rootScope.SelectedNodeID.split('~').length - 1];
        //}
        //$rootScope.familyDeleteNavStatus = true;
        //$rootScope.SelectedNodeIDPresist = 1;

        if ($rootScope.selecetedCatalogId !== 0) {
            $rootScope.IsPreview = false;
            //   $rootScope.dropDownTreeViewParnetCategoryDatasource.read();
            //    $rootScope.dropDownTreeViewDatasource.read();
            if ($scope.CUSTOM_NUM_FIELD1 == null || $scope.CUSTOM_NUM_FIELD1 == '') {
                $scope.CUSTOM_NUM_FIELD1 = '0';
            }
            if ($scope.categoryCustom.CUSTOM_TEXT_FIELD[1] != undefined) {
                $scope.Category.CUSTOM_TEXT_FIELD1 = $scope.categoryCustom.CUSTOM_TEXT_FIELD[1];
            }
            if ($scope.categoryCustom.CUSTOM_TEXT_FIELD[2] != undefined) {
                $scope.Category.CUSTOM_TEXT_FIELD2 = $scope.categoryCustom.CUSTOM_TEXT_FIELD[2];
            }
            if ($scope.categoryCustom.CUSTOM_TEXT_FIELD[3] != undefined) {
                $scope.Category.CUSTOM_TEXT_FIELD3 = $scope.categoryCustom.CUSTOM_TEXT_FIELD[3];
            }
            if ($scope.categoryCustom.CUSTOM_NUM_FIELD[1] != undefined) {
                $scope.Category.CUSTOM_NUM_FIELD1 = $scope.categoryCustom.CUSTOM_NUM_FIELD[1];
            }
            if ($scope.categoryCustom.CUSTOM_NUM_FIELD[2] != undefined) {
                $scope.Category.CUSTOM_NUM_FIELD2 = $scope.categoryCustom.CUSTOM_NUM_FIELD[2];
            }
            if ($scope.categoryCustom.CUSTOM_NUM_FIELD[3] != undefined) {
                $scope.Category.CUSTOM_NUM_FIELD3 = $scope.categoryCustom.CUSTOM_NUM_FIELD[3];
            }

            //row[key] =;


            // $scope.Category.SHORT_DESC = $("#txt_CategoryDesc")[0].nextElementSibling.innerHTML;
            if ($scope.IsNewCategory == '1') {
                if ($scope.Category.CATEGORY_SHORT !== "") {
                    //--------------------------------FOR WORKFLOW-------------------------------------------------------------//
                    $scope.NewCategoryId = "NEW";
                    //--------------------------------FOR WORKFLOW-------------------------------------------------------------//
                    dataFactory.CreateNewCategory($rootScope.selecetedParentCategoryId, $scope.Category, $rootScope.selecetedCatalogId, $scope.Category.STATUS_NAME).success(function (response) {
                        if (response.toLowerCase().contains("created successfully")) {
                            $rootScope.newCat = false;
                            var result = response.split(',');




                            if (CheckPdfCreate == true) {

                                $scope.getCategoryPdfExpress = $scope.SelectedFileForUpload.name;
                                $scope.getCategoryPdfExpressToolTip = $scope.SelectedFileForUpload.name;
                                // $rootScope.getCategoryPdfExpress = $scope.SelectedFileForCategory.substring(0, 25);


                                $scope.TempCategory = result[2];
                                $scope.UploadFile($scope.SelectedFileForUpload, true);
                            }



                            if (result.length > 0) {
                                if ($scope.Category.PARENT_CATEGORY == 'root') {
                                    $rootscope.selecetedParentCategoryId = '0';
                                }
                                // $rootScope.selecetedParentCategoryId = result[1];
                                if (result.length > 1) {
                                    $scope.Category.STATUS_NAME = result[2];
                                }
                                $scope.Category.CATEGORY_ID = result[1];
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + result[0] + '.',
                                    type: "info"
                                });
                            } else {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                            }
                        } else {
                            if (response == "Category ID already Exists.") {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: 'Category ID already exists.',
                                    type: "info"
                                });
                            }
                            else {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                            }
                        }
                        // $scope.catalogDataSource.read();
                        $rootScope.treeData.read();
                        $rootScope.getSelectedNode = $rootScope.selecetedParentCategoryId;
                        $scope.LoadCategorySpec($rootScope.selecetedCatalogId, "0");

                        //   $rootScope.dropDownTreeViewParnetCategoryDatasource.read();
                        //  $rootScope.dropDownTreeViewDatasource.read();
                        // $rootScope.previoustate();
                        if (response !== 'Category ID already Exists.') {
                            $scope.IsNewCategory = '0';

                        }
                        $scope.workflowDataSourceCategorylatest.read();


                    }).error(function (error) {
                        options.error(error);
                    });
                    $scope.workflowDataSourceCategorylatest.read();
                }
            }
            else if ($scope.IsNewCategory == '0') {



                //-----------------------------------------------------------------------------------------------


                if ($scope.SelectedFileForUpload != undefined && $scope.SelectedFileForUpload != "") {
                    $scope.UploadFile($scope.SelectedFileForUpload, true);
                }


                $scope.SelectedFileForUpload = '';


                //--------------------------------------------------------------------------------------------------



                if ($scope.undoCategoryClickFlag == "1") {
                    $scope.removeUndoListValue();
                    //   $scope.undoCategoryClickFlag ="0";

                }
                //--------------------------------FOR WORKFLOW-------------------------------------------------------------//
                $scope.NewCategoryId = "UPDATE";
                //--------------------------------FOR WORKFLOW-------------------------------------------------------------//
                dataFactory.UpdateCategory($rootScope.selecetedParentCategoryId, $scope.Category.STATUS_NAME, $scope.Category).success(function (response) {
                    //$rootScope.categoryChangedListUndo.push($scope.Category);
                    //var undoLength = $rootScope.categoryChangedListUndo.length - 1;
                    //angular.extend($rootScope.categoryChangedListUndo[undoLength], selectedRow);
                    $scope.undoCategorySaveFlag = "1";
                    $scope.SaveOrUpdateCategorySpecs(selectedRow);


                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + response + '.',
                        type: "info"
                    });
                    // $scope.catalogDataSource.read();
                    $rootScope.treeData.read();
                    // $rootScope.selecetedParentCategoryId = '0';
                    //$scope.Category = '';
                    //$rootScope.selecetedCatalogId = '';
                    $scope.workflowDataSourceCategorylatest.read();
                }).error(function (error) {
                    options.error(error);
                });
                $scope.workflowDataSourceCategorylatest.read();
                // $http.get("../Category/UpdateCategory?ID=" + $scope.Category.CATEGORY_ID + "&Name=" + $scope.Category.CATEGORY_NAME +
                //    "&Description=" + $scope.Category.SHORT_DESC + "&RootCat=" + $scope.Category.PARENT_CATEGORY + "&cnf1=" + $scope.Category.CUSTOM_NUM_FIELD1 +
                //    "&cnf2=" + $scope.Category.CUSTOM_NUM_FIELD2 + "&cnf3=" + $scope.Category.CUSTOM_NUM_FIELD3 + "&ctf1=" + $scope.Category.CUSTOM_TEXT_FIELD1 +
                //    "&ctf2=" + $scope.Category.CUSTOM_TEXT_FIELD2 + "&ctf3=" + $scope.Category.CUSTOM_TEXT_FIELD3 + "&img1=" + $scope.CategoryImage
                //    + "&publish=" + $scope.Category.PUBLISH + "&publish2print=" + $scope.Category.PUBLISH2PRINT).
                //then(function (data) {
                //    //alert(data);
                //    //$scope.Category = data;
                //});
            }

            $("#FamilySpecsTab").show();


        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please Select Catalog.',
                type: "error"
            });
        }

        $scope.workflowDataSourceCategorylatest.read();


    };


    // To check the defalut template  - Mariya vijayan

    /*  $scope.Encrypt = function () {
          var TYPE = 'Category';
           var Category_Id = $('#txt_CategoryID').val();
          var Id = $scope.Category.CATEGORY_ID;
          var Category_ID = Id;
          var Cat_Id = $localStorage.getCatalogID;
  
          dataFactory.getPdfXpressdefaultTemplate_Category(Category_ID).success(function (response) {
              if (response == null) {
                  dataFactory.getPdfXpressdefaultTemplate(Cat_Id).success(function (response) {
                      if (response == null) {
                          $scope.EncryptFunction();
                      }
                      else {
                          $scope.PreviewCategoryPdf();
                      }
                  });
              }
              else {
                  $scope.PreviewCategoryPdf();
              }
  
          });
      }*/
    $scope.Encrypt = function () {
        var TYPE = 'Category';
        var Id = $('#txt_CategoryID').val();
        var Category_Id = $scope.Category.CATEGORY_ID;
        var Cat_Id = $localStorage.getCatalogID;
        debugger;
        if (Category_Id != "") {
            dataFactory.getPdfXpressdefaultTemplate_Category(Cat_Id, Category_Id).success(function (response) {
                if (response == null) {
                    dataFactory.getPdfXpressdefaultTemplate(Cat_Id).success(function (response) {
                        if (response == null) {
                            // $scope.EncryptFunction();
                            $scope.showMessage();
                        }
                        else {
                            $scope.PreviewCategoryPdf();
                        }

                    });
                }
                else {
                    $scope.PreviewCategoryPdf();
                }

            });
        }
        else if (Category_Id == "") {
            debugger;
            dataFactory.getPdfXpressdefaultTemplate(Cat_Id).success(function (response) {
                if (response == null) {
                    // $scope.EncryptFunction();
                    $scope.showMessage();
                }
                else {
                    $scope.PreviewCategoryPdf();
                }

            });
        }
    }

    // End

    $scope.showMessage = function () {
        debugger;
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Please select PDFxPress template to preview category",
            type: "confirm",
            buttons: [{ value: "Ok" }
            ],
            success: function (result) {
                if (result === "Ok") {
                }
                else {

                    return false;
                }
            }

        });



    };


    $scope.EncryptFunction = function () {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Category Preview may take some time to process.",
            type: "confirm",
            buttons: [{ value: "Ok" },
                { value: "Cancel" }
            ],
            success: function (result) {
                if (result === "Ok") {

                    $http.get("../Category/GetCategory?CategoryId=" + $scope.Category.CATEGORY_ID).
              then(function (categoryDetails) {
                  $scope.Category = categoryDetails.data;
                  if ($scope.Category.Family_ID != 0) {
                      if ($scope.Category.PUBLISH2PRINT == true) {
                          var encryptcatalogId = $rootScope.selecetedCatalogId;
                          var encryptcategoryId = $scope.Category.CATEGORY_ID;
                          var encryptedcategoryid = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(encryptcategoryId), key,
                          { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });

                          var encryptedcatlogid = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(encryptcatalogId), key,
                            { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
                          window.open("../Category/Preview?catid=" + encryptedcategoryid + "&catalogid=" + encryptedcatlogid + "&userid=" + $localStorage.getCustomerID, '_blank');
                      }
                      else {
                          $.msgBox({
                              title: $localStorage.ProdcutTitle,
                              content: 'Category is not published to print, please publish the category to preview it.',
                              type: "error"
                          });
                      }
                  }
                  else {
                      $.msgBox({
                          title: $localStorage.ProdcutTitle,
                          content: 'No Family and Product in this category, please added Family And Product then category to preview it.',
                          type: "info"
                      });
                  }
              });
                    $scope.workflowDataSourceCategorylatest.read();
                }
                else {

                    return false;
                }
            }

        });



    };

    $scope.workflowDataSourceCategory = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getWorkflow().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.workflowitemChangeCategory = function (e) {
        $scope.Category.STATUS_NAME = e.sender.value();
    };
    $scope.workflowDataSourceCategorylatest = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getWorkflowdynamic($rootScope.selectedcategory_id, $scope.Category.STATUS_NAME, $scope.NewCategoryId, $scope.Category.CATEGORY_SHORT).success(function (response) {
                    options.success(response);

                    $scope.Category.STATUS_NAME = response[0];
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });


    $scope.workflowDataSourceCategory = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getWorkflow().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.workflowitemChangeCategory = function (e) {
        $scope.Category.STATUS_NAME = e.sender.value();
    };

    $scope.choices = [{ CUSTOM_TEXT_FIELD: 1, CUSTOM_NUM_FIELD: 1 }];

    $scope.addNewChoice = function () {

        var newItemNo = $scope.choices.length + 1;
        if (newItemNo == 2) {
            $scope.choices = [{ CUSTOM_TEXT_FIELD: 1, CUSTOM_NUM_FIELD: 1 },
                { CUSTOM_TEXT_FIELD: 2, CUSTOM_NUM_FIELD: 2 }];
        } else {
            $scope.choices = [{ CUSTOM_TEXT_FIELD: 1, CUSTOM_NUM_FIELD: 1 },
                { CUSTOM_TEXT_FIELD: 2, CUSTOM_NUM_FIELD: 2 },
                { CUSTOM_TEXT_FIELD: 3, CUSTOM_NUM_FIELD: 3 }];
        }
        ////  $scope.choices.push({ 'id': 'choice' + newItemNo });
        //if (newItemNo == 2) {
        //    $scope.choices = [{ CUSTOM_TEXT_FIELD1: $scope.Category.CUSTOM_TEXT_FIELD1, CUSTOM_NUM_FIELD1: $scope.Category.CUSTOM_NUM_FIELD1 },
        //        { CUSTOM_TEXT_FIELD2: $scope.Category.CUSTOM_TEXT_FIELD2, CUSTOM_NUM_FIELD2: $scope.Category.CUSTOM_NUM_FIELD2 }];
        //} else {
        //    $scope.choices = [{ CUSTOM_TEXT_FIELD1: $scope.Category.CUSTOM_TEXT_FIELD1, CUSTOM_NUM_FIELD1: $scope.Category.CUSTOM_NUM_FIELD1 },
        //        { CUSTOM_TEXT_FIELD2: $scope.Category.CUSTOM_TEXT_FIELD2, CUSTOM_NUM_FIELD2: $scope.Category.CUSTOM_NUM_FIELD2 },
        //        { CUSTOM_TEXT_FIELD3: $scope.Category.CUSTOM_TEXT_FIELD3, CUSTOM_NUM_FIELD3: $scope.Category.CUSTOM_NUM_FIELD3 }];
        //}
    };

    $scope.removeChoice = function () {

        var lastItem = $scope.choices.length - 1;

        if (lastItem !== 0) {
            $scope.categoryCustom.CUSTOM_TEXT_FIELD[lastItem + 1] = "";
            $scope.categoryCustom.CUSTOM_NUM_FIELD[lastItem + 1] = "";
            $scope.choices.splice(lastItem);
        }

    };


    $scope.isNum = function (evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Enter a Valid Data.',
                type: "error"
            });
            return false;
        }
        if (charCode == 13) {
            return false;
        }
        return true;
    }

    //$scope.userRoleAddCategory = false;
    //$scope.userRoleModifyCategory = false;
    //$scope.userRoleDeleteCategory = false;
    //$scope.userRoleViewCategory = false;

    //$scope.getUserRoleRightsCategory = function ()
    //{
    //    var id = 40202;
    //    dataFactory.getUserRoleRights(id).success(function (response) {
    //        if (response !== "" && response !== null) {
    //            $scope.userRoleAddCategory = response[0].ACTION_ADD;
    //            $scope.userRoleModifyCategory = response[0].ACTION_MODIFY;
    //            $scope.userRoleDeleteCategory = response[0].ACTION_REMOVE;
    //            $scope.userRoleViewCategory = response[0].ACTION_VIEW;
    //        }

    //    }).error(function (error) {
    //        options.error(error);
    //    });
    //};
    $scope.OpenPopupWindow = function (e) {
        $rootScope.paramValue = e.value;
        $scope.winManageDrive.refresh({ url: "../Views/App/Partials/ImageManagementPopup.html" });
        $scope.winManageDrive.title("Asset Management");
        $scope.winManageDrive.center().open();
        $scope.driveMahementIsVisible = true;
        if (e.Name == "" || e.Name == undefined) {
            $rootScope.paramValue = "category";

            $scope.attribute = e;
            $rootScope.Categoryattribute = e;
        }
        else {

            $rootScope.paramValue = "category";
            $scope.attribute = e.Value;
            $rootScope.Categoryattribute = e;
        }
    }
    $scope.clearImagePath = function (e) {
       
        document.getElementById(e).value = "";
        $rootScope.selectedCategoryRow[e] = "";
        $scope.selectedRow[e] = "";
    };




    $rootScope.SaveFileSelection = function (fileName, path) {
       
        var extn = fileName.split(".");
        // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
        var FileFormat = ["rar", "zip"];
        if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {
            $rootScope.categoryAttachPath = path;
            if ($rootScope.paramValue == "CategoryImage1") {
                $scope.Category.IMAGE_FILE = path.replace(/\\/g, "/");
            }
            if ($rootScope.paramValue == "CategoryImage2") {
                $scope.Category.IMAGE_FILE2 = path.replace(/\\/g, "/");
            }
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'This file type is not supported',
                type: "info"
            });
        }
        $scope.winManageDrive.center().close();
        $scope.driveMahementIsVisible = false;

    }


    $scope.init = function () {

        if ($localStorage.getCatalogID === undefined) {
            $rootScope.selecetedCatalogId = 0;
        } else {
            $rootScope.selecetedCatalogId = $localStorage.getCatalogID;
        }
        // $scope.getUserRoleRightsCategory();
        //$("#gridcategory").show();
        //    $("#columnsetupcategory").hide()

        $("#columnsetupcategory").hide();
        $("#CategoryAttributePack").hide();
    };
    $scope.init();

    /*-----Category New Attribute------*/

    /*-----Start Open Add New Attribute------*/




    $scope.SaveCategoryPublishAttributes = function () {

        dataFactory.SaveCategoryPublishAttributes($scope.GetAllCatalogCategoryAttributes._data, $localStorage.CategoryID, $rootScope.selecetedCatalogId).success(function (response) {
            if (response == "NotAvailable") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Category attributes supports only 12 attributes.',
                    type: "info"
                });
            }

            $scope.GetAllCatalogCategoryAttributes.read();
            $scope.GetAllSelectedCategoryAttributes.read();
        }).error(function (error) {
            options.error(error);
        });
    };


    $scope.CategorySetupAttributes = function () {
        // $scope.GetAllCatalogCategoryAttributes.read();
        $rootScope.attributesetup = true;
        $scope.GetAllCategoryAttributesNew.read();
        $scope.GetAllSelectedCategoryAttributes.read();
        $("#gridcategory").hide();
        $("#columnsetupcategory").show();
    }


    $scope.categoryNewAttributeSetupClick = function () {
        $scope.winFamilyNewAttributeSetup.refresh({ url: "../Views/App/Partials/categoryNewAttributeSetup.html" });
        $scope.winFamilyNewAttributeSetup.center().open();
        // $scope.tableGroupHeaderValue = true;
    };

    function groupHeaderName(e) {

        if (e.value === 21) {
            return "Category Specification (" + e.count + " items)";
        } else if (e.value === 23) {
            return "Category Image / Attachment (" + e.count + " items)";
        }
        else if (e.value === 24) {
            return "Category Price(" + e.count + " items)";
        }

        else {
            return "Category Description (" + e.count + " items)";
        }
    }
    $scope.GetAllCatalogCategoryAttributes = new kendo.data.DataSource({
        type: "json",
        pageable: true, pageSize: 5, autoBind: false,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetAllCatalogCategoryAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedCategoryId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
               { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });
    $scope.CategoryMainGridOptions = {
        dataSource: $scope.GetAllCatalogCategoryAttributes,
        pageable: { buttonCount: 5 }, autoBind: false,
        filterable: { mode: "row" },
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', filterable: false },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", filterable: true },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderName
            }
        ]
    };
    $scope.GetAllSelectedCategoryAttributes = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        pageable: true, autoBind: false,
        pageSize: 5, sort: { field: "SORT_ORDER", dir: "asc" },
        transport: {
            read: function (options) {
                //
                dataFactory.GetAllCategoryAttributes($rootScope.selecetedCatalogId, $rootScope.selecetedCategoryId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: {
                        type: "boolean"
                    },
                    CATALOG_ID: { editable: false },
                    CATEGORY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    SORT_ORDER: { editable: false }
                }
            }
        }

    });

    $scope.CategoryattrdataSource = {

        dataSource: $scope.GetAllSelectedCategoryAttributes,
        pageable: { buttonCount: 5 }, autoBind: false,
        //  selectable: "multiple",
        dataBound: onDataBound,
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforprodfamily" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px" }
        ]
    };

    var updownIndex = null;

    function onDataBound(e) {
        if (updownIndex != null) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {

                if (updownIndex == i) {
                    var isChecked = this.tbody.find("tr[data-uid='" + view[i].uid + "']").find(".chkbxforprodfamily").attr("checked");
                    if (isChecked) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .addClass("k-state-selected");
                    }

                }
            }

            updownIndex = null;
        }
    }

    $scope.updateSelection1 = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };

    $scope.DeleteCategoryPublishAttributes = function () {
        //var g = $scope.prodselectedgrid.select();
        //g.each(function (index, row) {
        //    var selectedItem = $scope.prodselectedgrid.dataItem(row);
        //    dataFactory.DeleteFamilyPublishAttributes(selectedItem, $localStorage.CategoryID).success(function (response) {
        //        $scope.familyGetAllCatalogattributesdataSource.read();
        //        $scope.familyprodfamilyattrdataSource.read();
        //    }).error(function (error) {
        //        options.error(error);
        //    });
        //});


        var g = $scope.CategoryselectedGrid.select();
        var data = [];
        for (var i = 0; i < g.length; i++) {
            var selectedItem = $scope.CategoryselectedGrid.dataItem(g[i]);
            data.push(selectedItem);
        }

        if (data == "") {
            data = $scope.getCategoryAttributeRow;
        }
        if (data != undefined) {
            dataFactory.DeleteCategoryPublishAttributes(data, $localStorage.CategoryID).success(function (response) {
                $scope.GetAllCatalogCategoryAttributes.read();
                $scope.GetAllSelectedCategoryAttributes.read();
            }).error(function (error) {
                options.error(error);
            });
        }
    };


    $scope.BtnCategoryMoveUpClick = function () {
        var g = $scope.CategoryselectedGrid.select();
        if (g.length === 1) {
            g.each(function (index, row) {
                var selectedItem = $scope.CategoryselectedGrid.dataItem(row);
                var data = $scope.CategoryselectedGrid.dataSource.data();
                var dataRows = $scope.CategoryselectedGrid.items();
                var selectedRowIndex = dataRows.index(g);
                //var sortOrder = data[selectedRowIndex - 1].SORT_ORDER;
                dataFactory.BtnCategoryMoveUpClick(selectedItem.SORT_ORDER - 1, selectedItem, $rootScope.selecetedCategoryId).success(function (response) {
                    $scope.GetAllCatalogCategoryAttributes.read();
                    $scope.GetAllSelectedCategoryAttributes.read();
                    updownIndex = selectedRowIndex - 1;
                }).error(function (error) {
                    options.error(error);
                });
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                type: "error"
            });
        }
    };



    $scope.BtnCategoryMoveDownClick = function () {

        var g = $scope.CategoryselectedGrid.select();
        if (g.length === 1) {
            g.each(function (index, row) {
                var selectedItem = $scope.CategoryselectedGrid.dataItem(row);
                var data = $scope.CategoryselectedGrid.dataSource.data();
                var dataRows = $scope.CategoryselectedGrid.items();
                var selectedRowIndex = dataRows.index(g);
                //  var sortOrder = data[selectedRowIndex + 1].SORT_ORDER;
                dataFactory.BtnCategoryMoveDownClick(selectedItem.SORT_ORDER + 1, selectedItem, $rootScope.selecetedCategoryId).success(function (response) {
                    $scope.GetAllCatalogCategoryAttributes.read();
                    $scope.GetAllSelectedCategoryAttributes.read();
                    updownIndex = selectedRowIndex + 1;
                }).error(function (error) {
                    options.error(error);
                });
            });
        } else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                type: "error"
            });


        }
    };


    $scope.UnPublishCategoryAttributes = function () {

        //-----------------------------------------new 6.6.22-------------------------------------------//
        // dataFactory.SaveCategoryPublishSortAttributes($localStorage.CategoryID, $rootScope.selecetedCatalogId, $scope.newCategoryAttributeDesign.dataSource._data).success(function (response) {
        $rootScope.attributesetup = false;
        $rootScope.attributepack = false;
        var CategoryAttributeCount = $scope.GetAllCategoryAttributesNew._data;
        var CategoryAttributelength = $scope.GetAllCategoryAttributesNew._data.length;
        var CategoryPublishCount, Count = "0";
        for (i = 0; i < CategoryAttributelength;) {
            if ($scope.GetAllCategoryAttributesNew._data[i].AVALIABLE == "1") {
                CategoryPublishCount = Count++;
                i++;
            }
            else if ($scope.GetAllCategoryAttributesNew._data[i].AVALIABLE !== "1") {
                i++;
            }
        }
        CategoryPublishCount = Count;
        if (CategoryPublishCount > 12) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Attribute cannot be Select or Published more than 12.',
                type: "error"
            });
        }
        else {
            dataFactory.UnPublishCategoryAttributes($scope.items, $localStorage.CategoryID, $rootScope.selecetedCatalogId, $scope.GetAllCategoryAttributesNew._data).success(function (response) {
                $scope.items = [];
                $scope.GetAllCategoryAttributesNew.read();
                var check = $scope.GetAllSelectedCategoryAttributes._total;

                // $scope.GetAllCatalogCategoryAttributes.read();
                $scope.GetAllSelectedCategoryAttributes.read();
                $scope.UnPublishCategoryAttributesFlag = "1";
                $scope.LoadCategorySpec($rootScope.selecetedCatalogId, $rootScope.selecetedCategoryId);
                $("#gridcategory").show();
                $("#columnsetupcategory").hide();
                //}
                //else
                //{
                //        $.msgBox({
                //            title: $localStorage.ProdcutTitle,
                //            content: 'Attribute cannot be Published more than 12.',
                //            type: "error"
                //        });
                //}
            }).error(function (error) {
                options.error(error);
            });
            //  });
        }
    };


    $scope.UnPublishCategoryAttributessorting = function () {
        //-----------------------------------------new 6.6.22 End-------------------------------------------//
        // dataFactory.SaveCategoryPublishSortAttributes($localStorage.CategoryID, $rootScope.selecetedCatalogId, $scope.newCategoryAttributeDesign.dataSource._data).success(function (response) {

        dataFactory.UnPublishCategoryAttributes($scope.items, $localStorage.CategoryID, $rootScope.selecetedCatalogId, $scope.GetAllCategoryAttributesNew._data).success(function (response) {
            $scope.items = [];
            $scope.GetAllCategoryAttributesNew.read();
            // $scope.GetAllCatalogCategoryAttributes.read();
            $scope.GetAllSelectedCategoryAttributes.read();
            $scope.UnPublishCategoryAttributesFlag = "1";
            $scope.LoadCategorySpec($rootScope.selecetedCatalogId, $rootScope.selecetedCategoryId);
            $("#gridcategory").hide();
            $("#columnsetupcategory").show();
            //new 6.6.22
            //$("#gridcategory").show();
            //$("#columnsetupcategory").hide();
        }).error(function (error) {
            options.error(error);
        });
        //  });

    };

    $scope.RefreshCategoryAttributes = function () {
        // $scope.GetAllCatalogCategoryAttributes.read();
        $scope.GetAllCategoryAttributesNew.read();
        $scope.GetAllSelectedCategoryAttributes.read();

    };


    $scope.CancelCategoryAttributes = function () {
        $rootScope.attributesetup = false;
        $scope.winFamilyAttributeSetup.close();
        $("#gridcategory").show();
        $("#columnsetupcategory").hide();
    };


    //dataFactory.getCategoryspecification($rootScope.selecetedCatalogId, $rootScope.selecetedCategoryId).success(function (response) {
    //    var obj = jQuery.parseJSON(response.Data.Data);
    //    $scope.CategoryData = obj;
    //    $scope.CategorycolumnsForAttributes = response.Data.Columns;
    //    $scope.Test($scope.CategorycolumnsForAttributes);
    //});


    $scope.LoadCategorySpec = function (CatalogID, CategoryID) {
        dataFactory.getCategoryspecification(CatalogID, CategoryID).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.CategoryData = obj;
            $scope.CategorycolumnsForAttributes = response.Data.Columns;

            if ($scope.undoCategoryClickFlag == "1") {
                $scope.getCategoryAttributeRow = [];
                $scope.filterOnlyCategorycolumnsForAttributes = [];

                $scope.filterOnlyCategorycolumnsForAttributes = angular.copy($scope.CategorycolumnsForAttributes);
                //users = $.grep($scope.filterOnlyCategorycolumnsForAttributes, function (el, idx) { return el.field == "CATALOG_ID" }, true)
                //users = $.grep($scope.filterOnlyCategorycolumnsForAttributes, function (el, idx) { return el.field == "CATEGORY_ID" }, true)
                for (var i = 0; i < $scope.CategorycolumnsForAttributes.length; i++) {
                    var obj = $scope.CategorycolumnsForAttributes[i];
                    var listToDelete = ["CATALOG_ID", "CATEGORY_ID"];
                    if (listToDelete[i] == obj.Caption) {
                        $scope.filterOnlyCategorycolumnsForAttributes.splice(0, 1);
                    }
                }

                var getProductIdList = [];
                for (var k = 0; k < $scope.selectedFamilyRowAttributes.length; k++) {
                    getProductIdList.push($scope.selectedFamilyRowAttributes[k].ATTRIBUTE_ID);
                }


                for (var i = 0; i < $scope.filterOnlyCategorycolumnsForAttributes.length; i++) {
                    var getTempCategoryAttributeRowObject = {};
                    getTempCategoryAttributeRowObject = { "CATALOG_ID": CatalogID, "CATEGORY_ID": CategoryID };
                    //    var a = $scope.CategorycolumnsForAttributes[2].Caption.split("__")[2];
                    var checkFlag = false;
                    for (var j = 0; j < $scope.selectedFamilyRowAttributes.length; j++) {
                        if ($scope.selectedFamilyRowAttributes[j].ATTRIBUTE_ID == $scope.filterOnlyCategorycolumnsForAttributes[i].Caption.split("__")[2]) {
                            checkFlag = true;
                        }
                    }
                    if (checkFlag == false) {
                        getTempCategoryAttributeRowObject["ATTRIBUTE_NAME"] = $scope.filterOnlyCategorycolumnsForAttributes[i].Caption.split('__')[0];
                        getTempCategoryAttributeRowObject["ATTRIBUTE_ID"] = $scope.filterOnlyCategorycolumnsForAttributes[i].Caption.split('__')[2];
                        getTempCategoryAttributeRowObject["ATTRIBUTE_TYPE"] = $scope.filterOnlyCategorycolumnsForAttributes[i].Caption.split('__')[3];
                        $scope.getCategoryAttributeRow.push(getTempCategoryAttributeRowObject);
                    }
                }
                $scope.DeleteCategoryPublishAttributes();
                $scope.undoCategoryClickFlag = "0";
                $scope.LoadCategorySpec($rootScope.selecetedCatalogId, $localStorage.CategoryID);
            }

            $scope.categoryNewEdit();
        });
    }

    $scope.attVal = {};

    $rootScope.SaveFileSelection = function (fileName, path) {
        
        var extn = fileName.split(".");
        $scope.attribute = $rootScope.Categoryattribute;
        $rootScope.selectedCategoryRow = $scope.selectedRow;
        // var imageFormat = ["JPEG/JFIF","JPEG 2000",'Exif',"TIFF","GIF","BMP","PNG","PPM","PNM","jpg","TIF","eps","psd"];
        var FileFormat = ["rar", "zip"];
        if (!FileFormat.includes(extn[1].toUpperCase()) && !FileFormat.includes(extn[1].toLowerCase())) {

            $rootScope.categoryAttachPath = path;
            $scope.fmlyImgAttrChangeData.ATTR_VALUE = path;
            $scope.fmlyImgAttrChangeData.IMG_PATH = path;
            $scope.Family.FAMILYIMG_NAME = path;
            $scope.fmlyImgAttrChangeData.FAMILYIMG_NAME = path;

            document.getElementById($scope.attribute).value = path;
            $rootScope.selectedCategoryRow[$scope.attribute] = path;


        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'This file type is not supported',
                type: "info"
            });
        }
        $scope.winManageDrive.center().close();
        $scope.driveMahementIsVisible = false;

    };



    /*-----END Open Add New Attribute------*/

    //$scope.selectedCategoryRowAttributes = [];
    //$scope.selectedCategoryRowDynamicAttributes = [];
    //$scope.attributePattern = {};
    //$scope.uimask = {};
    //$scope.UpdateCategorySpecs = function (data) {
    //    //$scope.selectedRow = angular.copy(data.Data);
    //    //$scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridItemEditPopup.html" });
    //    //$scope.winAddOrEditFamilySpecs.center().open();
    //    attributeWithout = [];
    //    if ($rootScope.selecetedCatalogId !== 0 && $rootScope.selecetedCategoryId !== 0) {

    //        dataFactory.GetCategoryAttributeDetails(data.Data.CATALOG_ID, $rootScope.selecetedCategoryId).success(function (response) {
    //            if (response != null) {

    //                $scope.selectedRow = data.Data;
    //                $scope.Categoryattributes = Object.keys(data.Data);
    //                $scope.selectedCategoryRowAttributes = response;
    //                $scope.updateFamilyspecification = data.Data;
    //                $scope.fromPageNo = 0;
    //                $scope.toPageNo = 15;
    //                angular.forEach($scope.Categoryattributes, function (value, key) {
    //                    if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2'))
    //                        attributeWithout.push(value);
    //                });
    //                if (attributeWithout.length < $scope.toPageNo) {
    //                    $scope.toPageNo = attributeWithout.length;
    //                }
    //                var attributestemp = [];
    //                for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
    //                    attributestemp.push(attributeWithout[i]);
    //                }
    //                $scope.Categoryattributes = attributestemp;
    //                $scope.attrLength = $scope.Categoryattributes.length;
    //                $scope.FamSpecsCountPerPage = "15";
    //                $scope.FamSpecsCurrentPage = "1";
    //                $scope.loopCount = [];
    //                $scope.loopEditProdsCount = [];

    //                var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
    //                var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
    //                if ((attributeWithout.length % parseInt($scope.FamSpecsCountPerPage)) > 0) {
    //                    loopcnt = loopcnt + 1;
    //                    loopEditProdsCnt = loopEditProdsCnt + 1;
    //                }
    //                $scope.FamSpecsPageCount = $scope.loopCount;
    //                $scope.totalSpecsFamPageCount = loopEditProdsCnt;

    //                for (var i = 0; i < loopcnt; i++) {
    //                    if (((i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)) < attributeWithout.length) {
    //                        $scope.loopCount.push({
    //                            PAGE_NO: (i + 1),
    //                            FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
    //                            TO_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)
    //                        });
    //                    } else {
    //                        $scope.loopCount.push({
    //                            PAGE_NO: (i + 1),
    //                            FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
    //                            TO_PAGE_NO: attributeWithout.length
    //                        });
    //                    }
    //                }
    //                var theString;
    //                $scope.selectedRowEdit = {};
    //                angular.copy(data.Data, $scope.selectedRowEdit);
    //                for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
    //                    if (attributeWithout[i].contains('__')) {
    //                        angular.forEach($scope.selectedCategoryRowAttributes, function (value, key) {
    //                            var theString = value.ATTRIBUTE_ID;
    //                            $scope.selectedCategoryRowDynamicAttributes[theString] = [];
    //                            $scope.selectedCategoryRowDynamicAttributes[theString] = value;

    //                            if ($scope.selectedCategoryRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Num')) {

    //                                var numeric = $scope.selectedCategoryRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[0];
    //                                var decimal = $scope.selectedCategoryRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[1].split(')')[0];
    //                                var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
    //                                pattern = pattern.replace("numeric", numeric);
    //                                pattern = pattern.replace("decimal", decimal);
    //                                var reg = new RegExp(pattern);
    //                                var uimask = $scope.UIMask(numeric, decimal);
    //                                $scope.uimask[theString] = uimask;
    //                                $scope.attributePattern[theString] = reg;
    //                            } else if ($scope.selectedCategoryRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Text(')) {
    //                                var maxlength = $scope.selectedCategoryRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(')')[0];
    //                                $scope.attributePattern[theString] = maxlength;
    //                                $scope.uimask[theString] = maxlength;
    //                            } else {
    //                                $scope.attributePattern[theString] = 524288;
    //                                $scope.uimask[theString] = 524288;
    //                            }

    //                        });
    //                    }
    //                }

    //                //  $scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridFamilySpecsEditPopup.html" });
    //                //   $scope.winAddOrEditFamilySpecs.center().open();


    //                $scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridCategorySpecsEditPopup.html" });
    //                $scope.winAddOrEditFamilySpecs.title("Category Specification");
    //                $scope.winAddOrEditFamilySpecs.center().open();
    //            }
    //        }).error(function (error) {
    //            options.error(error);
    //        });

    //    }
    //};

    $scope.selectedFamilyRowAttributes = [];
    $scope.selectedFamilyRowDynamicAttributes = [];
    $scope.attributePattern = {};
    $scope.uimask = {};
    $scope.UpdateCategorySpecs = function (data) {
       
        $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
        attributeWithout = [];
        if ($rootScope.selecetedCatalogId !== 0 && $rootScope.selecetedCategoryId !== 0) {
            dataFactory.GetCategoryAttributeDetails(data.Data.CATALOG_ID, $rootScope.selecetedCategoryId).success(function (response) {
                if (response != null) {
                    $scope.getCustomerCompanyName = $localStorage.getCustomerCompanyName;
                    $scope.selectedRow = data.Data;
                    $scope.attributes = Object.keys(data.Data);
                    $scope.selectedFamilyRowAttributes = response;
                    $scope.updateFamilyspecification = data.Data;
                    $scope.fromPageNo = 0;
                    $scope.toPageNo = 15;
                    angular.forEach($scope.attributes, function (value, key) {
                        if (value.includes("__") || value == 'PUBLISH' || value == 'SORT' || value == 'PUBLISH2PRINT' || value.includes('PUBLISH2'))
                            attributeWithout.push(value);
                    });
                    if (attributeWithout.length < $scope.toPageNo) {
                        $scope.toPageNo = attributeWithout.length;
                    }
                    var attributestemp = [];
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        attributestemp.push(attributeWithout[i]);
                    }
                    $scope.attributes = attributestemp;
                    $scope.attrLength = $scope.attributes.length;
                    $scope.FamSpecsCountPerPage = "15";
                    $scope.FamSpecsCurrentPage = "1";
                    $scope.loopCount = [];
                    $scope.loopEditProdsCount = [];

                    var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                    var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                    if ((attributeWithout.length % parseInt($scope.FamSpecsCountPerPage)) > 0) {
                        loopcnt = loopcnt + 1;
                        loopEditProdsCnt = loopEditProdsCnt + 1;
                    }
                    $scope.FamSpecsPageCount = $scope.loopCount;
                    $scope.totalSpecsFamPageCount = loopEditProdsCnt;

                    for (var i = 0; i < loopcnt; i++) {
                        if (((i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)) < attributeWithout.length) {
                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)
                            });
                        } else {
                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: attributeWithout.length
                            });
                        }
                    }
                    var theString;
                    $scope.selectedRowEdit = {};
                    angular.copy(data.Data, $scope.selectedRowEdit);
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].contains('__')) {
                            angular.forEach($scope.selectedFamilyRowAttributes, function (value, key) {
                                var theString = value.ATTRIBUTE_ID;
                                $scope.selectedFamilyRowDynamicAttributes[theString] = [];
                                $scope.selectedFamilyRowDynamicAttributes[theString] = value;

                                if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Num')) {

                                    var numeric = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[0];
                                    var decimal = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[1].split(')')[0];
                                    var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                    pattern = pattern.replace("numeric", numeric);
                                    pattern = pattern.replace("decimal", decimal);
                                    var reg = new RegExp(pattern);
                                    var uimask = $scope.UIMask(numeric, decimal);
                                    $scope.uimask[theString] = uimask;
                                    $scope.attributePattern[theString] = reg;
                                } else if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                    var maxlength = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(')')[0];
                                    $scope.attributePattern[theString] = maxlength;
                                    $scope.uimask[theString] = maxlength;
                                } else {
                                    $scope.attributePattern[theString] = 524288;
                                    $scope.uimask[theString] = 524288;
                                }


                            });
                        }
                    }

                    //  $scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridFamilySpecsEditPopup.html" });
                    //   $scope.winAddOrEditFamilySpecs.center().open();
                    //$scope.DATAFILE = 'File';

                    $scope.winAddOrEditCategorySpecs.refresh({ url: "../Views/App/Category/dynamicGridCategorySpecsEditPopup.html" });
                    $scope.winAddOrEditCategorySpecs.title("Category Specification");
                    $scope.winAddOrEditCategorySpecs.center().open();

                    // $scope.DATAFILE = 'File';
                }
            }).error(function (error) {
                options.error(error);
            });

        }
    };

    $scope.callFamilyGridPaging = function (pageno) {

        if (pageno != null && pageno <= $scope.totalProdPageCount && pageno != 0 || pageno == 'NEXT' || pageno == 'PREV') {

            if (pageno == 'PREV' && $scope.ProdCurrentPageTest != 1) {
                $scope.ProdCurrentPageTest = parseInt($scope.ProdCurrentPageTest) - 1;
            } else if (pageno == 'PREV' && $scope.ProdCurrentPageTest == 1) {
                $scope.ProdCurrentPageTest = "1";
            } else if (pageno == 'NEXT') {

                $("#cboProdPageCountUP").data("kendoDropDownList").text("");
                $("#cboProdPageCountDown").data("kendoDropDownList").text("");
                $scope.ProdCurrentPageTest = parseInt($scope.ProdCurrentPageTest) + 1;
                if ($scope.ProdCurrentPageTest > $scope.totalProdPageCount) {
                    $scope.ProdCurrentPageTest = $scope.totalProdPageCount;
                }
            } else {

                //$("#cboProdPageCountUP").data("kendoDropDownList").text("");
                //$("#cboProdPageCountDown").data("kendoDropDownList").text("");
                $scope.ProdCurrentPageTest = pageno;
            }

            $rootScope.LoadProdData2($rootScope.selecetedCatalogId, $rootScope.selecetedFamilyId, $rootScope.selecetedCategoryId, $scope.group_id);

        }
    };
    $scope.CancelCategorySpecs = function () {
        $scope.winAddOrEditCategorySpecs.close();
        //$scope.LoadFamilySpecsData();
    };

    $scope.SaveOrUpdateCategorySpecs = function (row) {
        var displayAttribures = "";
        var tt = $('#textboxvalue4');
        if ($scope.val_type === "true") {

            //angular.forEach(row, function (value, key) {
            //    if (key.contains("OBJ")) {
            //        //
            //        var sa = $scope.selectedFamilyRowDynamicAttributes[key.split('__')[2]];


            //        //
            //        if (value != null && $("#textboxvalue" + key.split('_')[4]).length != 0) {
            //            //
            //            var id = sa.ATTRIBUTE_ID;

            //            if ($("#textboxvalue" + id)[0].nextElementSibling.innerHTML != value && !$("#textboxvalue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textboxvalue" + id)[0].nextElementSibling.innerHTML != "<p><br></p>") {
            //                if (!$("#textboxvalue" + id)[0].nextElementSibling.innerHTML.includes("Value is too long") && !value.includes("Value is too long")) {

            //                    row[key] = $("#textboxvalue" + id)[0].nextElementSibling.innerHTML;
            //                }
            //                else {
            //                    row[key] = value;
            //                }

            //                if ($("#textboxvalue" + id)[0].nextElementSibling.innerHTML != value && !$("#textboxvalue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textboxvalue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
            //                    row[key] = "";
            //                }
            //            }
            //            if ($("#textboxvalue" + id)[0].nextElementSibling.innerHTML != value && !$("#textboxvalue" + id)[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textboxvalue" + id)[0].nextElementSibling.innerHTML == "<p><br></p>") {
            //                row[key] = "";
            //            }
            //        }
            //        else if ($("#textboxvalue" + key.split('_')[4]).length != 0 && $("#textboxvalue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "" && !$("#textboxvalue" + key.split('_')[4])[0].nextElementSibling.innerHTML.includes("Enter valid format") && $("#textboxvalue" + key.split('_')[4])[0].nextElementSibling.innerHTML != "<p><br></p>") {


            //            row[key] = $("#textboxvalue" + key.split('_')[4])[0].nextElementSibling.innerHTML;
            //        }
            //        if (sa != undefined) {
            //            //To check if the attribute is a value required field
            //            if (sa.VALUE_REQUIRED == true && (row[key] == '' || row[key] == null) || sa.ATTRIBUTE_ID == 1 && (row[key] == '' || row[key] == null)) {

            //                if (displayAttribures == '') {
            //                    displayAttribures = "The following attribute values are required,\n";
            //                }
            //                displayAttribures = displayAttribures + "\n\t" + key.split('__')[0];
            //            }
            //        }

            //    }

            //});
            if (displayAttribures == '') {
                $timeout(function () {
                    dataFactory.SaveCategorySpecs($rootScope.selecetedCatalogId, $localStorage.CategoryID, row).success(function (response) {

                        if (response != null) {


                            $timeout(function () {
                                $scope.showFamily();
                            }, 150);

                            //$scope.PreviewText = response;

                            $scope.winAddOrEditFamilySpecs.close();
                            $scope.LoadFamilySpecsData();

                            $scope.LoadCategorySpec($rootScope.selecetedCatalogId, $localStorage.CategoryID);
                            //$.msgBox({
                            //    title: $localStorage.ProdcutTitle,
                            //    content: 'Update completed.',
                            //    type: "info"
                            //});
                        }
                    }).error(function (error) {
                        options.error(error);
                    });
                }, 200);
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + displayAttribures + '.',
                    type: "info"
                });
            }

        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter Data using a valid format.',
                type: "info"
            });
        }



    };

    $scope.AttributePackforCategory = function () {
        $rootScope.attributepack = true;
        $("#gridcategory").show();
        $("#CategoryColumnSetUp").hide();
        $scope.categoryFamilyAttributePackdataSource.read();
        $scope.categoryProductAttributePackdataSource.read();
        $timeout(function () {
            $rootScope.attributepack = true;
            $("#CategoryAttributePack").show();
        }, 300);
    }


    $scope.categoryFamilyAttributePackdataSource = new kendo.data.DataSource({
        //type: "json",
        sort: [{ field: "ISAvailable", dir: "desc" },
            { field: "GROUP_ID", dir: "asc" }],
        serverFiltering: true,
        serverPaging: true,
        serverSorting: true, editable: true,
        transport: {
            read: function (options) {
                var selectAllCfamPack;
                if ($("#selectAllCfamPack").prop("checked") == true) {
                    selectAllCfamPack = 1;
                }
                else if ($("#selectAllCfamPack").prop("checked") == false) {
                    selectAllCfamPack = 0;
                }
                dataFactory.GetFamilyPacksforCategory($rootScope.selecetedCatalogId, $scope.ActiveCategoryId, options.data).success(function (response) {
                    if (selectAllCfamPack == 1) {
                        $('#selectAllCfamPack').prop('checked', false);
                    }
                    $scope.CatfamilyPackCount = response.Total;
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            update: function (options) {
                if ($scope.Catalog.CATALOG_ID === 1) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Master Catalog update is not permitted.',
                        type: "info"
                    });
                } else {
                    $rootScope.items;
                    dataFactory.savecatalogallattrdetails($rootScope.selecetedCatalogId, $rootScope.items).success(function (response) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Process completed.',
                            type: "info",
                        });
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        }, batch: true,
        schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "GROUP_ID",
                fields: {
                    GROUP_NAME: { editable: false },
                    ISAvailable: { editable: true, type: "boolean", sortable: false }
                }
            }
        }
    });


    $scope.CategoryfamilyAttributePackOptions = {
        dataSource: $scope.categoryFamilyAttributePackdataSource,
        sortable: true, scrollable: true, editable: true, autoBind: false,
        toolbar: [
         { name: "save", text: "", template: '<a ng-click="SaveSelectedFamilyAttributePack()" class="red k-button k-button-icontext k-grid-upload">Save</a>' },
         { name: "cancel", text: "", template: '<a ng-click="CancelFamilyAttributePackforCategory()" class="red k-button k-button-icontext k-grid-upload">Cancel</a>' }],
        filterable: true,
        selectable: "row",
        columns: [
            { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllCfamPack' class='mc-checkbox' ng-click='selectAll($event)'/>" },
            { field: "GROUP_NAME", title: "Group Name" }
        ], dataBound: function () {
        },
        remove: function () {
            // e.model.ATTRIBUTE_ID
        }
    };

    $scope.categoryProductAttributePackdataSource = new kendo.data.DataSource({
        //type: "json",
        sort: [{ field: "ISAvailable", dir: "desc" },
            { field: "GROUP_ID", dir: "asc" }],
        serverFiltering: true,
        serverPaging: true,
        serverSorting: true, editable: true,
        transport: {
            read: function (options) {
                var selectAllCprodPack;
                if ($("#selectAllCprodPack").prop("checked") == true) {
                    selectAllCprodPack = 1;
                }
                else if ($("#selectAllCprodPack").prop("checked") == false) {
                    selectAllCprodPack = 0;
                }
                dataFactory.GetProductPacksforCategory($rootScope.selecetedCatalogId, $scope.ActiveCategoryId, options.data).success(function (response) {
                    if (selectAllCprodPack == 1) {
                        $('#selectAllCprodPack').prop('checked', false);
                    }
                    $scope.CatprodPackCount = response.Total;
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options) {
                return kendo.stringify(options);
            }
        }, batch: true,
        schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "GROUP_ID",
                fields: {
                    GROUP_NAME: { editable: false },
                    ISAvailable: { editable: true, type: "boolean", sortable: false }
                }
            }
        }
    });


    $scope.CategoryproductAttributePackOptions = {
        dataSource: $scope.categoryProductAttributePackdataSource,
        sortable: true, scrollable: true, editable: true, autoBind: false,
        toolbar: [
         { name: "save", text: "", template: '<a ng-click="SaveSelectedProdAttributePack()" class="red k-button k-button-icontext k-grid-upload">Save</a>' },
         { name: "cancel", text: "", template: '<a ng-click="CancelProductAttributePackforCategory()" class="red k-button k-button-icontext k-grid-upload">Cancel</a>' }],
        filterable: true,
        selectable: "row",
        columns: [
            { field: "ISAvailable", title: "Select", width: 150, sortable: false, template: '<input type="checkbox"  class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', headerTemplate: "<input type='checkbox' title='select all' id='selectAllCprodPack'  class='mc-checkbox' ng-click='selectAll($event)'/>" },
            { field: "GROUP_NAME", title: "Group Name" }
        ], dataBound: function () {
        },
        remove: function () {
            // e.model.ATTRIBUTE_ID
        }
    };

    $scope.updateSelection = function (e, id) {

        if (id.dataItem.ATTRIBUTE_ID !== 1) {
            id.dataItem.set("ISAvailable", e.target.checked);

            $rootScope.items.push({
                GROUP_ID: id.dataItem.GROUP_ID,
                ISAvailable: id.dataItem.ISAvailable,
                GROUP_NAME: id.dataItem.GROUP_NAME,
                CATALOG_ID: $rootScope.selecetedCatalogId,
                // FAMILY_ID: id.dataItem.FAMILY_ID,
                CATEGORY_ID: $scope.ActiveCategoryId,
            });

        } else {
            id.dataItem.set("ISAvailable", true);
        }
    };


    $scope.selectAll = function (ev) {

        var grid = $(ev.target).closest('[kendo-grid]').data("kendoGrid");
        var items = grid.dataItems();
        $rootScope.items = items;
        items.forEach(function (item) {
            if (item.ATTRIBUTE_ID !== 1) {
                item.set("ISAvailable", ev.target.checked);
            }
        });
    };


    $scope.SaveSelectedProdAttributePack = function () {
        if ($rootScope.selecetedCatalogId === 1) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Master Catalog update is not permitted.',
                type: "info"
            });
        } else {
            if ($rootScope.items != undefined && $rootScope.items.length != 0) {
                dataFactory.saveProdAttributePackforcategory($rootScope.selecetedCatalogId, $scope.ActiveCategoryId, $rootScope.items).success(function (response) {
                    $rootScope.items = [];
                    $('#selectAllCprodPack').prop('checked', false);
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Process completed.',
                        type: "info",
                    });
                    $scope.categoryProductAttributePackdataSource.read();

                    // options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {

            }
        }
    };


    $scope.SaveSelectedFamilyAttributePack = function () {
        if ($rootScope.selecetedCatalogId === 1) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Master Catalog update is not permitted.',
                type: "info"
            });
        } else {
            if ($rootScope.items != undefined && $rootScope.items.length != 0) {
                dataFactory.saveFamilyAttributePackforcategory($rootScope.selecetedCatalogId, $scope.ActiveCategoryId, $rootScope.items).success(function (response) {
                    $rootScope.items = [];
                    $('#selectAllCfamPack').prop('checked', false);
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Process completed.',
                        type: "info",
                    });
                    $scope.categoryFamilyAttributePackdataSource.read();
                    // options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
            else {

            }
        }
    };

    $scope.CancelProductAttributePackforCategory = function () {
        
        $scope.categoryProductAttributePackdataSource.read();
        $rootScope.attributepack = false;
       
    };

    $scope.CancelFamilyAttributePackforCategory = function () {
        $scope.categoryFamilyAttributePackdataSource.read();
        $rootScope.attributepack = false;
        
    };

    //Attribute pack end


    $scope.init();

    // Pdf Express

    //-------------------------------------------------------------------------------
    //Upload mrt files for Open Template
    $scope.ChechFileValid = function (file) {
        var isValid = false;
        if ($scope.SelectedFileForUpload != null) {
            if (file.name.contains(".mrt")) {
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
            else {
                $scope.FileInvalidMessage = "Selected file is Invalid. (only file type .mrt allowed)";
            }
        }
        else {
            $scope.FileInvalidMessage = "MRT Files required!";
        }
        $scope.IsFileValid = isValid;
    };

    //$scope.SaveFile = function () {

    //};


    $scope.openPDFPath = "";
    $scope.UploadFile = function (file, saveData) {
       

        var categoryId = $('#txt_CategoryID').val();

        if (categoryId == undefined) {
            categoryId = $scope.TempCategory;
        }



        var formData = new FormData();
        formData.append("file", file);
        formData.append("TYPE", "Category");
        formData.append("ID", categoryId);
        formData.append("CatalogId", $localStorage.getCatalogID);
        formData.append("SaveTemplateData", saveData)
        $http.post("/XpressCatalog/SaveFiles", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            }).success(function (d) {
                $scope.openPDFPath = d;
                //$.msgBox({
                //    title: $localStorage.ProdcutTitle,
                //    content: 'File upload successful.',
                //    // type: "info"
                //});

                dataFactory.FindProjectType().success(function (response) {
                    if (response !== "Error") {
                        if (response.toLocaleLowerCase().contains("simple")) {
                            //    $("#opendesigner").show();
                        } else {
                            //    $("#opendesigner").show();
                        }
                    } else {
                        //   $("#opendesigner").show();
                    }
                }).error(function (error) {
                    // $("#opendesigner").show();
                });
            })
            .error(function () {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });

            });

        // return defer.promise;
    };



    //     --------------------------------------------------------------------------------


    $scope.saveCategorySessionForPdfXpress = function () {
        var category_Id = $('#txt_CategoryID').val();
        var Catalog_id = $localStorage.getCatalogID;
        //alert(category_Id);
        dataFactory.CheckTemplatePath("CATEGORY", category_Id, Catalog_id, "").success(function (response) {
            debugger;
            if (response == "Template found") {
                $scope.openCategoryTemplate();
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'PDFxPress Template is missing.',
                    type: "error"
                });
            }
       
        });
    }

    $scope.openCategoryTemplate = function () {
        debugger;
        var category_Id = $('#txt_CategoryID').val();
        var Catalog_ID = $localStorage.getCatalogID;
        dataFactory.setPdfXpressType("CATEGORY", category_Id, "", "", Catalog_ID).success(function (response) {
            window.open("../Category/PdfPreview?catid=" + category_Id, '_blank');
        });
    }

    //Jothipriya
    $scope.PreviewCategoryPdf = function () {
        var TYPE = 'Category';
        var Id = $('#txt_CategoryID').val();
        var Catalog_id = $localStorage.getCatalogID;

        dataFactory.getPdfXpressdefaultType(TYPE, Id).success(function (response) {
            // dataFactory.getPdfXpressdefaultType_Category(TYPE, Id).success(function (response) {
            if (response != "") {
                $scope.SelectedFileForUpload = response;
                $scope.UploadFile($scope.SelectedFileForUpload, false);
            }


        });


        $('#opendesigner').hide();
        $scope.IsFormSubmitted = true;
        $scope.Message = "";
        // $scope.ChechFileValid($scope.SelectedFileForUpload);


        // $scope.UploadFile($scope.SelectedFileForUpload, false);
        $scope.saveCategorySessionForPdfXpress();







    }
    function clearForm() {
        $scope.FileDescription = "";
        angular.forEach(angular.element("input[type='file']"), function (inputElem) {
            angular.element(inputElem).val(null);
        });
        $scope.f1.$setPristine();
        $scope.IsFormSubmitted = false;
    }

    $scope.closeOpentemplate = function () {
        $("#opendesigner").hide();
    };

    $scope.selectFileforUploadRun_Category = function (file) {
        $scope.SelectedFileForUpload = file[0];

        $scope.SelectedFileForCategory = file[0].name;
        $rootScope.getCategoryPdfExpress = file[0].name;
        $scope.getCategoryPdfExpress = file[0].name;
        $scope.getCategoryPdfExpressToolTip = file[0].name;
        $rootScope.getCategoryPdfExpressToolTip = file[0].name;
        var category_Id = $('#txt_CategoryID').val();
        //alert(category_Id);
        // $scope.getCatalog_id();
        var Catalog_ID = $localStorage.getCatalogID;

        dataFactory.setPdfXpressType("CATEGORY", category_Id, "", "", Catalog_ID).success(function (response) {
            dataFactory.getCatalogID($scope.SelectedCatalogId).success(function (response) {
            });

        });
    };
    // Its not working  - Mariya vijayan
    $scope.exportCategoryPdf = function () {

        $scope.exportFileFormat = "pdf";

        if ($scope.exportFileFormat != undefined) {
            $http.post("../XpressCatalog/ExportResult?format=" + $scope.exportFileFormat).then(function (response) {
                var windowlocation = window.location.origin;
                var url = response.data;
                $.ajax({
                    url: windowlocation,
                    success: function () {
                        window.open(url);
                    },
                    error: function () {
                        alert('does not exist in server location');
                    },
                });
            });
        }
    }
    // Clear the Value

    $scope.deleteCategoryPdf = function () {
        var id = $('#txt_CategoryID').val();
        var Id = $('#txt_CategoryID').val();
        var catId = $localStorage.getCatalogID;
        var Catalog_id = $localStorage.getCatalogID;
        var TYPE = "Category";

        if (id != null && id != undefined && id != "" && id != "undefined") {

            dataFactory.clearPdfXpressType("Category", id, catId).success(function (response) {
                if (response != "Error") {
                    dataFactory.getPdfXpressdefaultType(TYPE, Id, Catalog_id).success(function (response) {

                        if (response != "") {

                            $scope.SelectedFileForUpload = response;
                            $scope.getCategoryPdfExpress = '';
                            $scope.getCategoryPdfExpressToolTip = 'No file chosen';
                            var $el = $('#chooseFile');
                            if ($el.val() != "") {
                                $el.wrap('<form>').closest('form').get(0).reset();
                                $el.unwrap();
                            }

                        }


                    });
                    //  $scope.getCategoryPdfExpress = response.substring(0, 25);
                }
            });
        }
    }

    $scope.categoryNewEdit = function () {

        attributeWithout = [];
        $scope.attributes = [];
        if ($rootScope.selecetedCatalogId !== 0 && $rootScope.selecetedCategoryId !== 0) {
            dataFactory.GetCategoryAttributeDetails($rootScope.selecetedCatalogId, $rootScope.selecetedCategoryId).success(function (response) {
                if (response.m_Item1 != null) {
                    $scope.attributes = [];
                    attributeWithout = [];
                    $scope.selectedExternalAssetDriveURL = response.m_Item2;
                    $rootScope.selectedCategoryRow = angular.copy($scope.CategoryData[0]);
                    $scope.selectedRow = angular.copy($scope.CategoryData[0]);
                    $scope.updateproductspecs = angular.copy($scope.CategoryData[0]);
                    $scope.attributes = $scope.CategorycolumnsForAttributes;
                    $scope.selectedFamilyRowAttributes = response.m_Item1;
                    $scope.copyselectedRow = angular.copy($scope.CategoryData[0]);
                    $scope.copyattributes = angular.copy($scope.CategorycolumnsForAttributes);
                    $scope.copySelectedCategoryValue = angular.copy($scope.Category);
                    $scope.categoryTemplate = $scope.getCategoryPdfExpress;
                    $scope.copySelectedCategoryValue["categoryTemplateName"] = $scope.categoryTemplate;
                    if ($scope.baseCatFlag == 1) {
                        //-------------------------Undo functionality obtaining base value of the Category ----------------------//
                        if ($localStorage.baseCategoryValUndoList.length == 0) {
                            $localStorage.baseCategoryValUndoList.push({ category: $scope.copySelectedCategoryValue, selectedExternalAssetDriveURL: $scope.selectedExternalAssetDriveURL, selectedRow: $scope.copyselectedRow, attributes: $scope.copyattributes, selectedFamilyRowAttributes: $scope.selectedFamilyRowAttributes });
                        }
                        else if ($localStorage.baseCategoryValUndoList.length > 0) {
                            angular.forEach($localStorage.baseCategoryValUndoList, function (value) {
                                if (value.category.CATEGORY_ID == $scope.Category.CATEGORY_ID) {
                                    var indexValue = $localStorage.baseCategoryValUndoList.indexOf(value);
                                    $localStorage.baseCategoryValUndoList.splice(indexValue, 1);
                                    catCheckFlag = 1;
                                }
                            });
                            $localStorage.baseCategoryValUndoList.push({ category: $scope.copySelectedCategoryValue, selectedExternalAssetDriveURL: $scope.selectedExternalAssetDriveURL, selectedRow: $scope.copyselectedRow, attributes: $scope.copyattributes, selectedFamilyRowAttributes: $scope.selectedFamilyRowAttributes });
                        }
                        //-------------------------Undo functionality obtaining base value of the Category ----------------------// 
                        $scope.baseCatFlag = 0;
                    }
                    else if ($scope.UnPublishCategoryAttributesFlag != "1" && $scope.undoCategorySaveFlag == "1") {
                        $localStorage.categoryChangedListUndo.push({ category: $scope.copySelectedCategoryValue, selectedExternalAssetDriveURL: $scope.selectedExternalAssetDriveURL, selectedRow: $scope.copyselectedRow, attributes: $scope.copyattributes, selectedFamilyRowAttributes: $scope.selectedFamilyRowAttributes });
                        $rootScope.categoryUndoList = angular.copy($localStorage.categoryChangedListUndo);
                        $scope.undoCategorySaveFlag = "0";

                        if ($rootScope.categoryUndoList.length > 10) {
                            $localStorage.categoryChangedListUndo.splice(0, 1);
                        }
                        //var undoLength = $rootScope.categoryChangedListUndo.length - 1;
                        //angular.extend($rootScope.categoryChangedListUndo[undoLength], selectedRow);
                    }

                    $scope.fromPageNo = 0;
                    $scope.toPageNo = 15;
                    angular.forEach($scope.attributes, function (value, key) {
                        if (value.Caption.includes("__") || value.Caption == 'PUBLISH' || value.Caption == 'SORT' || value.Caption == 'PUBLISH2PRINT' || value.Caption.includes('PUBLISH2'))
                            attributeWithout.push(value);
                    });
                    if (attributeWithout.length < $scope.toPageNo) {
                        $scope.toPageNo = attributeWithout.length;
                    }
                    var attributestemp = [];
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        attributestemp.push(attributeWithout[i]);
                    }
                    $scope.attributes = attributestemp;
                    $scope.attrLength = $scope.attributes.length;
                    $scope.FamSpecsCountPerPage = "15";
                    $scope.FamSpecsCurrentPage = "1";
                    $scope.loopCount = [];
                    $scope.loopEditProdsCount = [];

                    var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                    var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                    if ((attributeWithout.length % parseInt($scope.FamSpecsCountPerPage)) > 0) {
                        loopcnt = loopcnt + 1;
                        loopEditProdsCnt = loopEditProdsCnt + 1;
                    }
                    $scope.FamSpecsPageCount = $scope.loopCount;
                    $scope.totalSpecsFamPageCount = loopEditProdsCnt;

                    for (var i = 0; i < loopcnt; i++) {
                        if (((i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)) < attributeWithout.length) {
                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)
                            });
                        } else {
                            $scope.loopCount.push({
                                PAGE_NO: (i + 1),
                                FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                                TO_PAGE_NO: attributeWithout.length
                            });
                        }
                    }
                    var theString;
                    $scope.selectedRowEdit = {};
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].Caption.contains('__')) {
                            angular.forEach($scope.selectedFamilyRowAttributes, function (value, key) {
                                var theString = value.ATTRIBUTE_ID;
                                $scope.selectedFamilyRowDynamicAttributes[theString] = [];
                                $scope.selectedFamilyRowDynamicAttributes[theString] = value;

                                if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Num')) {

                                    var numeric = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[0];
                                    var decimal = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[1].split(')')[0];
                                    var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                    pattern = pattern.replace("numeric", numeric);
                                    pattern = pattern.replace("decimal", decimal);
                                    var reg = new RegExp(pattern);
                                    var uimask = $scope.UIMask(numeric, decimal);
                                    $scope.uimask[theString] = uimask;
                                    $scope.attributePattern[theString] = reg;
                                } else if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                    var maxlength = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(')')[0];
                                    $scope.attributePattern[theString] = maxlength;
                                    $scope.uimask[theString] = maxlength;
                                } else {
                                    $scope.attributePattern[theString] = 524288;
                                    $scope.uimask[theString] = 524288;
                                }


                            });
                        }
                    }
                    $scope.groups = [];
                    var picklistNAme = "";
                    var attrId = "";
                    $scope.iterationCount = 0;
                    $scope.isUIReleased = 0;
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].Caption.contains("OBJ")) {
                            if (attributeWithout[i].Caption.split('__')[2] !== "0") {
                                var sa = $scope.selectedFamilyRowDynamicAttributes[attributeWithout[i].Caption.split('__')[2]];
                                if (sa.USE_PICKLIST) {
                                    $scope.iterationCount++;
                                }
                            }
                        }
                    }
                    // $rootScope.selectedRowDynamic = [];

                    $scope.iterationPicklistCount = 0;
                    for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                        if (attributeWithout[i].Caption.contains("OBJ")) {
                            if (attributeWithout[i].Caption.split('__')[2] !== "0") {
                                var sa = $scope.selectedFamilyRowDynamicAttributes[attributeWithout[i].Caption.split('__')[2]];
                                if (sa.USE_PICKLIST) {
                                    $scope.GetPickListDataCategory(sa.ATTRIBUTE_ID, sa.PICKLIST_NAME);
                                }
                            }
                        }
                    }

                    $scope.UnPublishCategoryAttributesFlag = "0";
                    $scope.$apply();
                    $scope.$digest();
                    //  $scope.winAddOrEditFamilySpecs.refresh({ url: "../Views/App/Partials/dynamicGridFamilySpecsEditPopup.html" });
                    //   $scope.winAddOrEditFamilySpecs.center().open();
                    //$scope.DATAFILE = 'File';                
                    // $scope.DATAFILE = 'File';
                }
            }).error(function (error) {
                options.error(error);
            });
        }
    }
    $scope.$on("ToClearTheData", function () {
        $scope.getCategoryPdfExpress = "";
        $scope.getCategoryPdfExpressToolTip = 'No file chosen';
    });

    $scope.$on("ToAddTheData", function () {
        $scope.getCategoryPdfExpress = $rootScope.getCategoryPdfExpress;
        $scope.getCategoryPdfExpressToolTip = $rootScope.getCategoryPdfExpress;

    });

    //-----------------------------------------------------------Category undo functions----------------------------------------------------//
    $rootScope.undoListSelectCat = function (data, index) {
        
        $scope.selectedUndoListIndex = index;
        $scope.Category = data.category;
        $scope.selectedExternalAssetDriveURL = data.selectedExternalAssetDriveURL;
        $rootScope.selectedCategoryRow = data.selectedRow;
        $scope.selectedRow = data.selectedRow;
        $scope.attributes = data.attributes;
        $scope.selectedFamilyRowAttributes = data.selectedFamilyRowAttributes;
        $scope.getCategoryPdfExpress = data.category.categoryTemplateName;
        $scope.getCategoryPdfExpressToolTip = data.category.categoryTemplateName;
        attributeWithout = [];
        $scope.fromPageNo = 0;
        $scope.toPageNo = 15;
        angular.forEach($scope.attributes, function (value, key) {
            if (value.Caption.includes("__") || value.Caption == 'PUBLISH' || value.Caption == 'SORT' || value.Caption == 'PUBLISH2PRINT' || value.Caption.includes('PUBLISH2'))
                attributeWithout.push(value);
        });
        if (attributeWithout.length < $scope.toPageNo) {
            $scope.toPageNo = attributeWithout.length;
        }
        var attributestemp = [];
        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
            attributestemp.push(attributeWithout[i]);
        }
        $scope.attributes = attributestemp;
        $scope.attrLength = $scope.attributes.length;
        $scope.FamSpecsCountPerPage = "15";
        $scope.FamSpecsCurrentPage = "1";
        $scope.loopCount = [];
        $scope.loopEditProdsCount = [];

        var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
        var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
        if ((attributeWithout.length % parseInt($scope.FamSpecsCountPerPage)) > 0) {
            loopcnt = loopcnt + 1;
            loopEditProdsCnt = loopEditProdsCnt + 1;
        }
        $scope.FamSpecsPageCount = $scope.loopCount;
        $scope.totalSpecsFamPageCount = loopEditProdsCnt;

        for (var i = 0; i < loopcnt; i++) {
            if (((i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)) < attributeWithout.length) {
                $scope.loopCount.push({
                    PAGE_NO: (i + 1),
                    FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                    TO_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)
                });
            } else {
                $scope.loopCount.push({
                    PAGE_NO: (i + 1),
                    FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                    TO_PAGE_NO: attributeWithout.length
                });
            }
        }
        var theString;
        $scope.selectedRowEdit = {};
        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
            if (attributeWithout[i].Caption.contains('__')) {
                angular.forEach($scope.selectedFamilyRowAttributes, function (value, key) {
                    var theString = value.ATTRIBUTE_ID;
                    $scope.selectedFamilyRowDynamicAttributes[theString] = [];
                    $scope.selectedFamilyRowDynamicAttributes[theString] = value;
                    if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Num')) {
                        var numeric = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[0];
                        var decimal = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[1].split(')')[0];
                        var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                        pattern = pattern.replace("numeric", numeric);
                        pattern = pattern.replace("decimal", decimal);
                        var reg = new RegExp(pattern);
                        var uimask = $scope.UIMask(numeric, decimal);
                        $scope.uimask[theString] = uimask;
                        $scope.attributePattern[theString] = reg;
                    } else if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Text(')) {
                        var maxlength = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(')')[0];
                        $scope.attributePattern[theString] = maxlength;
                        $scope.uimask[theString] = maxlength;
                    } else {
                        $scope.attributePattern[theString] = 524288;
                        $scope.uimask[theString] = 524288;
                    }
                });
            }
        }

    }

    $rootScope.UndoclickCat = function () {
      
        if ($scope.selectedUndoListIndex != undefined) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Are you sure you want to Undo the changes?",
                type: "confirm",
                buttons: [{ value: "Ok" }, { value: "Cancel" }],
                success: function (result) {
                    if (result === "Ok") {
                        $scope.undoCategoryClickFlag = "1";
                        var catCheck = 0;
                        attributeWithout = [];
                        var selectedUndoCategory = $localStorage.categoryChangedListUndo[$scope.selectedUndoListIndex];
                        if ($scope.selectedUndoListIndex == 0) {
                            angular.forEach($localStorage.baseCategoryValUndoList, function (baseVal) {
                                if (selectedUndoCategory.category.CATEGORY_ID == baseVal.category.CATEGORY_ID) {
                                    $scope.Category = baseVal.category;
                                    $scope.selectedExternalAssetDriveURL = baseVal.selectedExternalAssetDriveURL;
                                    $scope.selectedRow = baseVal.selectedRow;
                                    $scope.attributes = baseVal.attributes;
                                    $scope.selectedFamilyRowAttributes = baseVal.selectedFamilyRowAttributes;
                                    $scope.getCategoryPdfExpress = baseVal.category.categoryTemplateName;
                                    $scope.getCategoryPdfExpressToolTip = baseVal.category.categoryTemplateName;
                                }
                            });
                        }
                        else if ($scope.selectedUndoListIndex > 0) {
                            for (var i = $scope.selectedUndoListIndex - 1; i >= 0; i--) {
                                if ($localStorage.categoryChangedListUndo[i].category.CATEGORY_ID == selectedUndoCategory.category.CATEGORY_ID) {
                                    $scope.Category = $localStorage.categoryChangedListUndo[i].category;
                                    $scope.selectedExternalAssetDriveURL = $localStorage.categoryChangedListUndo[i].selectedExternalAssetDriveURL;
                                    $scope.selectedRow = $localStorage.categoryChangedListUndo[i].selectedRow;
                                    $scope.attributes = $localStorage.categoryChangedListUndo[i].attributes;
                                    $scope.selectedFamilyRowAttributes = $localStorage.categoryChangedListUndo[i].selectedFamilyRowAttributes;
                                    $scope.getCategoryPdfExpress = $localStorage.categoryChangedListUndo[i].category.categoryTemplateName;
                                    $scope.getCategoryPdfExpressToolTip = $localStorage.categoryChangedListUndo[i].category.categoryTemplateName;
                                    catCheck = 1;
                                    $scope.selectedUndoListIndex = i;
                                    break;
                                }
                            }
                            if (catCheck == 0) {
                                angular.forEach($localStorage.baseCategoryValUndoList, function (baseVal) {
                                    if (selectedUndoCategory.category.CATEGORY_ID == baseVal.category.CATEGORY_ID) {
                                        $scope.Category = baseVal.category;
                                        $scope.selectedExternalAssetDriveURL = baseVal.selectedExternalAssetDriveURL;
                                        $scope.selectedRow = baseVal.selectedRow;
                                        $scope.attributes = baseVal.attributes;
                                        $scope.selectedFamilyRowAttributes = baseVal.selectedFamilyRowAttributes;
                                        $scope.getCategoryPdfExpress = baseVal.category.categoryTemplateName;
                                        $scope.getCategoryPdfExpressToolTip = baseVal.category.categoryTemplateName;
                                    }
                                });
                            }
                        }

                        $scope.fromPageNo = 0;
                        $scope.toPageNo = 15;
                        angular.forEach($scope.attributes, function (value, key) {
                            if (value.Caption.includes("__") || value.Caption == 'PUBLISH' || value.Caption == 'SORT' || value.Caption == 'PUBLISH2PRINT' || value.Caption.includes('PUBLISH2'))
                                attributeWithout.push(value);
                        });
                        if (attributeWithout.length < $scope.toPageNo) {
                            $scope.toPageNo = attributeWithout.length;
                        }
                        var attributestemp = [];
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            attributestemp.push(attributeWithout[i]);
                        }
                        $scope.attributes = attributestemp;
                        $scope.attrLength = $scope.attributes.length;
                        $scope.FamSpecsCountPerPage = "15";
                        $scope.FamSpecsCurrentPage = "1";
                        $scope.loopCount = [];
                        $scope.loopEditProdsCount = [];

                        var loopcnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                        var loopEditProdsCnt = Math.floor(attributeWithout.length / parseInt($scope.FamSpecsCountPerPage));
                        if ((attributeWithout.length % parseInt($scope.FamSpecsCountPerPage)) > 0) {
                            loopcnt = loopcnt + 1;
                            loopEditProdsCnt = loopEditProdsCnt + 1;
                        }
                        $scope.FamSpecsPageCount = $scope.loopCount;
                        $scope.totalSpecsFamPageCount = loopEditProdsCnt;

                        for (var i = 0; i < loopcnt; i++) {
                            if (((i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)) < attributeWithout.length) {
                                $scope.loopCount.push({
                                    PAGE_NO: (i + 1),
                                    FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                                    TO_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + parseInt($scope.FamSpecsCountPerPage)
                                });
                            } else {
                                $scope.loopCount.push({
                                    PAGE_NO: (i + 1),
                                    FROM_PAGE_NO: (i * parseInt($scope.FamSpecsCountPerPage)) + 1,
                                    TO_PAGE_NO: attributeWithout.length
                                });
                            }
                        }
                        $scope.$apply();
                        $scope.$digest();
                        var theString;
                        $scope.selectedRowEdit = {};
                        for (var i = $scope.fromPageNo; i < $scope.toPageNo; i++) {
                            if (attributeWithout[i].Caption.contains('__')) {
                                angular.forEach($scope.selectedFamilyRowAttributes, function (value, key) {
                                    var theString = value.ATTRIBUTE_ID;
                                    $scope.selectedFamilyRowDynamicAttributes[theString] = [];
                                    $scope.selectedFamilyRowDynamicAttributes[theString] = value;
                                    if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Num')) {
                                        var numeric = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[0];
                                        var decimal = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(',')[1].split(')')[0];
                                        var pattern = "^\\d{0,numeric}(\\.\\d{0,decimal})?$";
                                        pattern = pattern.replace("numeric", numeric);
                                        pattern = pattern.replace("decimal", decimal);
                                        var reg = new RegExp(pattern);
                                        var uimask = $scope.UIMask(numeric, decimal);
                                        $scope.uimask[theString] = uimask;
                                        $scope.attributePattern[theString] = reg;
                                    } else if ($scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.contains('Text(')) {
                                        var maxlength = $scope.selectedFamilyRowDynamicAttributes[theString].ATTRIBUTE_DATATYPE.split('(')[1].split(')')[0];
                                        $scope.attributePattern[theString] = maxlength;
                                        $scope.uimask[theString] = maxlength;
                                    } else {
                                        $scope.attributePattern[theString] = 524288;
                                        $scope.uimask[theString] = 524288;
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }

    }

    $scope.removeUndoListValue = function () {
        var removeundofamily = [];
        removeundofamily = $rootScope.categoryUndoList;
        removeundofamily.splice($scope.selectedUndoListIndex, 1);
        $localStorage.categoryChangedListUndo.splice($scope.selectedUndoListIndex, 1);
        //if ($scope.index == 0) {
        //    details.push($scope.undo)
        //}
        //for (var i = 0; i < details.length; i++) {
        //    if (details[i].FAMILY_ID == $scope.undoBeforeChangeAttributeRow.FAMILY_ID && details[i].FAMILY_NAME == $scope.undoBeforeChangeAttributeRow.FAMILY_NAME) {
        //        details.splice(i, 1);
        //    }
        //}
        $rootScope.categoryUndoList = removeundofamily;
        //details.push($scope.undoBeforeChangeAttributeRow);
        //$scope.selectedFamilyDetails = details;
    }
    //-----------------------------------------------------------Category undo functions----------------------------------------------------//

    //----------------------------------------------------------New design changes for category attribute setup page-----------------------//
    $scope.items = [];



    // logic - start
    $scope.checkPublish = function (e, id) {
        if (id.dataItem.PUBLISHED == 1) {
            id.dataItem.PUBLISHED = 0;


            id.dataItem.set("PUBLISHED", e.target.checked);
            $scope.items.push({
                ISAvailable: true,
                CATALOG_ID: $rootScope.selecetedCatalogId,
                CATEGORY_ID: $scope.ActiveCategoryId,
                ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                FLAG: "unpublish"
            });


        }

        else {



            if (id.dataItem.AVALIABLE == 0) {
                id.dataItem.PUBLISHED = 0;
                $("#chk_" + id.dataItem.ATTRIBUTE_ID).prop("checked", false);


                id.dataItem.set("PUBLISHED", e.target.checked);
                $scope.items.push({
                    ISAvailable: true,
                    CATALOG_ID: $rootScope.selecetedCatalogId,
                    CATEGORY_ID: $scope.ActiveCategoryId,
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    FLAG: "unpublish"
                });

            } else {
                id.dataItem.PUBLISHED = 1;

                id.dataItem.set("PUBLISHED", e.target.checked);
                $scope.items.push({
                    ISAvailable: true,
                    CATALOG_ID: $rootScope.selecetedCatalogId,
                    CATEGORY_ID: $scope.ActiveCategoryId,
                    ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                    FLAG: "publish"
                });

            }
        }

    };



    $scope.checkAvailable = function (e, id) {
        if (id.dataItem.AVALIABLE == 1) {
            id.dataItem.AVALIABLE = 0;
            id.dataItem.PUBLISHED = 0;
            $("#chk_" + id.dataItem.ATTRIBUTE_ID).prop("checked", false);


            id.dataItem.set("PUBLISHED", e.target.checked);
            $scope.items.push({
                ISAvailable: true,
                CATALOG_ID: $rootScope.selecetedCatalogId,
                CATEGORY_ID: $scope.ActiveCategoryId,
                ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                FLAG: "UnAvailable"
            });

            $scope.items.push({
                ISAvailable: true,
                CATALOG_ID: $rootScope.selecetedCatalogId,
                CATEGORY_ID: $scope.ActiveCategoryId,
                ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                FLAG: "unpublish"
            });


        }
        else {

            id.dataItem.set("PUBLISHED", e.target.checked);
            $scope.items.push({
                ISAvailable: true,
                CATALOG_ID: $rootScope.selecetedCatalogId,
                CATEGORY_ID: $scope.ActiveCategoryId,
                ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
                FLAG: "Available"
            });

            id.dataItem.AVALIABLE = 1;
        }
    };













    //$scope.checkPublish = function (e, id) {
    //    if (e.target.checked) {
    //        id.dataItem.set("PUBLISHED", e.target.checked);
    //        $scope.items.push({
    //            ISAvailable: true,
    //            CATALOG_ID: $rootScope.selecetedCatalogId,
    //            CATEGORY_ID: $scope.ActiveCategoryId,
    //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
    //            FLAG: "publish"
    //        });
    //    } else {
    //        id.dataItem.set("PUBLISHED", e.target.checked);
    //        $scope.items.push({
    //            ISAvailable: true,
    //            CATALOG_ID: $rootScope.selecetedCatalogId,
    //            CATEGORY_ID: $scope.ActiveCategoryId,
    //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
    //            FLAG: "unpublish"
    //        });
    //    }

    //};

    //$scope.uncheckPublish = function (e, id) {

    //    if (e.target.checked) {
    //        id.dataItem.set("PUBLISHED", e.target.checked);
    //        $scope.items.push({
    //            ISAvailable: true,
    //            CATALOG_ID: $rootScope.selecetedCatalogId,
    //            CATEGORY_ID: $scope.ActiveCategoryId,
    //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
    //            FLAG: "publish"
    //        });
    //    } else {
    //        id.dataItem.set("PUBLISHED", e.target.checked);
    //        $scope.items.push({
    //            ISAvailable: true,
    //            CATALOG_ID: $rootScope.selecetedCatalogId,
    //            CATEGORY_ID: $scope.ActiveCategoryId,
    //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
    //            FLAG: "unpublish"
    //        });
    //    }

    //};

    //// For available attribute   - Start

    //$scope.checkPublishAvailable = function (e, id) {

    //    if (e.target.checked) {
    //        id.dataItem.set("PUBLISHED", e.target.checked);
    //        $scope.items.push({
    //            ISAvailable: true,
    //            CATALOG_ID: $rootScope.selecetedCatalogId,
    //            CATEGORY_ID: $scope.ActiveCategoryId,
    //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
    //            FLAG: "Available"
    //        });
    //    } else {
    //        id.dataItem.set("PUBLISHED", e.target.checked);
    //        $scope.items.push({
    //            ISAvailable: true,
    //            CATALOG_ID: $rootScope.selecetedCatalogId,
    //            CATEGORY_ID: $scope.ActiveCategoryId,
    //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
    //            FLAG: "UnAvailable"
    //        });
    //    }
    //}


    //$scope.uncheckPublishAvailable = function (e, id) {
    //    if (e.target.checked) {
    //        id.dataItem.set("PUBLISHED", e.target.checked);
    //        $scope.items.push({
    //            ISAvailable: true,
    //            CATALOG_ID: $rootScope.selecetedCatalogId,
    //            CATEGORY_ID: $scope.ActiveCategoryId,
    //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
    //            FLAG: "Available"
    //        });
    //    } else {
    //        id.dataItem.set("PUBLISHED", e.target.checked);
    //        $scope.items.push({
    //            ISAvailable: true,
    //            CATALOG_ID: $rootScope.selecetedCatalogId,
    //            CATEGORY_ID: $scope.ActiveCategoryId,
    //            ATTRIBUTE_ID: id.dataItem.ATTRIBUTE_ID,
    //            FLAG: "UnAvailable"
    //        });
    //    }
    //}
    //// For available attribute for - End


    $scope.GetAllCategoryAttributesNew = new kendo.data.DataSource({
        type: "json",
        //serverFiltering: true, 
        filterable: true,
        serverFiltering: false,
        //sortable: true,
        //new 6.6.22
        // pageable: true,
        autoBind: false,
        navigatable: true,
        //serverSorting: true,
        //pageSize: 5,
        sort: { field: "SORT_ORDER", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetAllCategoryAttributesNew($rootScope.selecetedCatalogId, $rootScope.selecetedCategoryId).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }

        },
        batch: true,
        schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: {
                        type: "boolean"
                    },
                    CATALOG_ID: { editable: false },
                    CATEGORY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    SORT_ORDER: { editable: false },
                    PUBLISH2PRINT: { editable: false },
                    PUBLISH2WEB: { editable: false },
                    PUBLISH2PDF: { editable: false },
                    PUBLISH2EXPORT: { editable: false },
                    PUBLISH2PORTAL: { editable: false },
                    PUBLISHED: { editable: false },
                    AVALIABLE: { editable: false },
                    STYLE_NAME: { editable: false },
                    ATTRIBUTE_DATATYPE: { editable: false }
                }
            }
        }
    });

    $("#newCategoryAttributeDesign .k-grid-content").css({
        "overflow-y": "scroll"
    });


    $scope.newCategoryAttributeDesign = {
        dataSource: $scope.GetAllCategoryAttributesNew,
        //pageable: {
        //    alwaysVisible: false,
        //    pageSizes: [5, 10, 20, 100]
        //},
        scrollable: true,
        //filterable: true,
        // editable: true,
        //height: 300,
        autoBind: false,
        filterable: { mode: "row" },
        //selectable: "multiple",
        dataBound: onDataBound, serverSorting: true, sortable: true,

        columns: [
          //  { field: "AVALIABLE", title: "Avaliable", width: "9px", template: '<input type="checkbox" checked="checked"></input>' },

                          { field: "AVAILABLE", title: "Select", width: "5px", template: '# if(AVALIABLE=="1") {#<input type="checkbox"  checked="checked"  ng-click="checkAvailable($event, this)"></input>#} else if(AVALIABLE=="0") {#<input type="checkbox"  ng-click="checkAvailable($event, this)"></input>#}#', filterable: false },
                { field: "PUBLISH", title: "Publish", width: "5px", template: '# if(PUBLISHED=="1") {#<input type="checkbox" id="chk_#=ATTRIBUTE_ID#" checked="checked"  ng-click="checkPublish($event, this)"></input>#} else if(PUBLISHED=="0") {#<input type="checkbox" id="chk_#=ATTRIBUTE_ID#" ng-click="checkPublish($event, this)"></input>#}#', filterable: false },

            //{ field: "AVALIABLE", title: "Select", width: "5px", template: '# if(AVALIABLE=="1") {#<input type="checkbox"  checked="checked"  ng-click="checkPublishAvailable($event, this)"></input>#} else if(AVALIABLE=="0") {#<input type="checkbox"   ng-click="uncheckPublishAvailable($event, this)"></input>#} else {#<input type="checkbox" ng-click="avaliableToSelect($event, this)"#}#' },
            //{ field: "PUBLISHED", title: "Publish", width: "5px", template: '# if(PUBLISHED=="1") {#<input type="checkbox"  checked="checked"  ng-click="checkPublish($event, this)"></input>#} else if(PUBLISHED=="0" && AVALIABLE=="1") {#<input type="checkbox"  ng-click="uncheckPublish($event, this)"></input>#} else {#<input type="checkbox" disabled="disabled" ng-click="avaliableToSelect($event, this)"#}#' },
           { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "20px", filterable: true, template: "<a title='#=ATTRIBUTE_NAME#' >#=ATTRIBUTE_NAME#</a>" },
            { field: "ATTRIBUTE_TYPE", title: "Type", width: "5px", template: '# if( ATTRIBUTE_TYPE=="21") {# <span title="Specification"><i class="fa fa-tags"></i><span># } else if(ATTRIBUTE_TYPE=="23") {#<span title="Image"><i class="fa fa-image"></i><span>#} else if(ATTRIBUTE_TYPE=="25") {#<span title="Description"><i class="fa fa-life-ring"></i><span>#} #', filterable: false },
            { field: "ATTRIBUTE_DATATYPE", title: "Data Type", width: "15px", filterable: false },
                          { field: "STYLE_NAME", title: "Style Name", width: "10px", template: "<a title='#=STYLE_NAME#' >#=STYLE_NAME#</a>", filterable: false }

            //{ field: "PUBLISH2PRINT", title: "Print", width: "7px", template: '# if( PUBLISH2PRINT==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PRINT == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
           // { field: "PUBLISH2WEB", title: "Web", width: "7px", template: '# if( PUBLISH2WEB==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2WEB == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
            //{ field: "PUBLISH2PDF", title: "PDF", width: "7px", template: '# if( PUBLISH2PDF==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PDF == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
           // { field: "PUBLISH2EXPORT", title: "Export", width: "7px", template: '# if( PUBLISH2EXPORT==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2EXPORT == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
           // { field: "PUBLISH2PORTAL", title: "Portal", width: "7px", template: '# if( PUBLISH2PORTAL==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PORTAL == false) {#<input type="checkbox" disabled="disabled"></input>#}#' }
           // { field: "STYLE_NAME", title: "Style Name", width: "10px" }
            //{ field: "PUBLISH2PRINT", title: "Print", width: "7px", template: '# if( PUBLISH2PRINT==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PRINT == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
            //{ field: "PUBLISH2WEB", title: "Web", width: "7px", template: '# if( PUBLISH2WEB==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2WEB == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
            //{ field: "PUBLISH2PDF", title: "PDF", width: "7px", template: '# if( PUBLISH2PDF==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PDF == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
            //{ field: "PUBLISH2EXPORT", title: "Export", width: "7px", template: '# if( PUBLISH2EXPORT==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2EXPORT == false) {#<input type="checkbox" disabled="disabled"></input>#}#' },
            //{ field: "PUBLISH2PORTAL", title: "Portal", width: "7px", template: '# if( PUBLISH2PORTAL==true) {#<input type="checkbox" checked="checked" disabled="disabled"></input># } else if(PUBLISH2PORTAL == false) {#<input type="checkbox" disabled="disabled"></input>#}#' }


        ],
        dataBound: function (e) {
            var columns = e.sender.columns;
            var columnIndex = this.wrapper.find(".k-grid-header [data-field=" + "PUBLISHED" + "]").index();

            var rows = e.sender.tbody.children();
            for (var j = 0; j < rows.length; j++) {
                var row = $(rows[j]);
                var dataItem = e.sender.dataItem(row);

                var units = dataItem.get("PUBLISHED");
                var publish = dataItem.get("PUBLISHED");

                if (publish == 1 || publish == 0) {
                    row.addClass("selectable");
                }

            }
        }
    };

    $scope.sortableOptions = {
        filter: ".k-grid tr[data-uid]",
        hint: $.noop,
        cursor: "move",
        placeholder: function (element) {
            return element
                      .clone()
                      .removeAttr("uid")
                      .addClass("k-state-hover")
                      .css("opacity", 0.65);
        },
        container: ".k-grid tbody",
        change: function (e) {
            var grid = $("#newCategoryAttributeDesign").data("kendoGrid");
            //  var grid = $scope.myGrid
            dataItem = grid.dataSource.getByUid(e.item.data("uid"));

            grid.dataSource.remove(dataItem);
            grid.dataSource.insert(e.newIndex, dataItem);
            //new 6.6.22
            $scope.UnPublishCategoryAttributessorting();
        }
    };

    $rootScope.LoaAttributeDataCategory = function () {
        $scope.GetAllCategoryAttributesNew.read();

    };
    //new 6.6.22
    $scope.datatypecheckcat = function (event, type, attribute) {
       
        var value = event.currentTarget.value;
        var datatype = type;
        var pattern = "";

        if (type !== "All Characters" && !type.contains("0-9") && !type.contains("uFFFF")) {
            pattern = /^[a-zA-Z ]+$/;
        }
        else {
            if (type.contains("uFFFF")) {
                //pattern = /^[ A-Za-z0-9_@./#&@$%^*!~*?+-]*$/;
            }
            else {
                pattern = /^[0-9a-zA-Z ]*$/;
            }
        }
        if (pattern !== "" && value.trim() !== "") {
            var id = event.currentTarget.id;
            if (!pattern.test(value)) {
                $scope.selectedRow[attribute.ColumnName] = "";
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please enter valid Data.',
                    type: "info"
                });

            }
        }
    };

    $scope.GetPickListDataCategory = function (attrName, picklistdata) {
        dataFactory.getPickListData(picklistdata).success(function (response) {
            $scope.selectedRowDynamic[attrName] = response;
            $scope.iterationPicklistCount++;
            if ($scope.iterationPicklistCount == $scope.iterationCount) {
                $timeout(function () {
                    $scope.selectedRow = {};
                    angular.copy($scope.updateproductspecs, $scope.selectedRow);
                }, 200);


            }
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.displayHtmlPreview = function (t) {
        var gettextareaName = "CattextBoxValue" + t;
        $scope.gettextareaValue = $("textarea[name=" + gettextareaName + "]").val();
    };


}]);

