﻿//LSApp.controller('PreferenceController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http) {
LSApp.controller('PreferenceController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$filter', '$compile','$localStorage','$rootScope',
    function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $filter, $compile, $localStorage, $rootScope) {

        $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
        

    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

    $scope.KendoThemes = {
        dataSource: {
            data: [
                { Text: "Black", value: "black" },
                { Text: "Blue Opal", value: "blueopal" },
                { Text: "Bootstrap", value: "bootstrap" },
                { Text: "Default", value: "default" },
                { Text: "HighContrast", value: "highcontrast" },
                { Text: "Metro", value: "metro" },
                { Text: "MetroBlack", value: "metroblack" },
                { Text: "Moonlight", value: "moonlight" },
                { Text: "Silver", value: "silver" },
                { Text: "Uniform", value: "uniform" }
            ]
        },
        dataTextField: "Text",
        dataValueField: "value"
    };
    $scope.Theme_Name = "";
    $http.get('../Preference/getmethod')
                        .then(function (data) {
                            $scope.Theme_Name = data.data;
                        });
    $scope.NewProject = {
        ErrorLogFilePath: { validation: { required: true } },
        Temporary_File_Path: '',
        TechSupportURL: '',
        WebcatSyncURL: '',
        WhiteboardWebserviceURL: '',
        DisplayIDColumns: false,
        AllowDuplicateItem_PartNum: false,
        FamilyLevelMultitablePreview: false,
        ShowCustomAPPS: false,
        ProductLevelMultitablePreview: false,
        EnableWorkFlow: false,
        SpellCheckerMode: '',
        // ---CatalogSettings--//
        CatalogFilePath: '',
        CatalogTemplatePath: '',
        CatalogXMLPath: '',
        Export_ImportPath: '',
        // ---Image Settings---//
        ImagePath: '',
        ImageConversionUtilityPath: '',
        WebCatImagePath: '',
        ReferenceTableImagePath: '',
        //---UserSettings--//
        DisplayCategoryIdinNavigator: false,
        DisplayFamilyandRelatedFamily: false,
        UploadImagesPDFForWebSync: false,
        CreateXMLForSelectedAttributesToAllFamily: false,
        DisplaySkuProductCountInAlert: true,
        SKUAlertPercentage: 90,
        Customer_MasterCatalog: 1,
        Subcatalog_Items: 0,
        AssetLimitPercentage: 90,
        EnableSubProduct: false,
        ImagePdfAttachemnt: false,
        ConverterType: '',
        //autoWebSync: false,
        
    };

    $scope.submit = function () {
        if (!$scope.myForm.$valid) {
            $scope.myForm.Filepath.$dirty = true;
        }
    };
    $scope.catalogDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetCatalogDetails($scope.SelectedItem, $localStorage.getCustomerID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        cache: false
    });
    $scope.customerCatalogChange = function (e) {
        if (e.sender.value() !== "") {
        }
        };
    $scope.KendoThemeChange = function (e) {
        //$scope.selectattributes(e.sender.value());
        var value = e.sender.value();
        $scope.ChangeTheme(value);
        $scope.selecteddrpdowntheme = value;
        $http.post('../Preference/ChangeTheme?SelectedTheme=' + $scope.selecteddrpdowntheme)
                        .then(function (data) {
                        });
        $scope.ChangeTheme(value);

    };

    $scope.function = function () {

    };

    $scope.ChangeTheme = function (skinName, animate) {
        window.localStorage.setItem('item_name', skinName);
        var doc = document,
        kendoLinks = $("link[href*='kendo.']", doc.getElementsByTagName("head")[0]),
        commonLink = kendoLinks.filter("[href*='kendo.common']"),
        skinLink = kendoLinks.filter(":not([href*='kendo.common'])"),
        href = location.href,
        skinRegex = /kendo\.\w+(\.min)?\.css/i,
        extension = skinLink.attr("rel") === "stylesheet" ? ".css" : ".less",
        url = commonLink.attr("href").replace(skinRegex, "kendo." + skinName + "$1" + extension),
        exampleElement = $("#example");

        function preloadStylesheet(file, callback) {
            var element = $("<link rel='stylesheet' media='print' href='" + file + "'").appendTo("head");

            setTimeout(function () {
                callback();
                element.remove();
            }, 100);
        }

        function replaceTheme() {
            var oldSkinName = $(doc).data("kendoSkin"),
            newLink;
            // if ($.browser.msie) {
            //     newLink = doc.createStyleSheet(url);
            //} else {
            newLink = skinLink.eq(0).clone().attr("href", url);
            newLink.insertBefore(skinLink[0]);
            //}
            skinLink.remove();
            $(doc.documentElement).removeClass("k-" + oldSkinName).addClass("k-" + skinName);
        }
        $scope.onSelect1 = function (e) {
         
        //    alert('asd');
        //var message = $.map(e.files, function (file) { return file.name; }).join(", ");
        //$scope.LogoFile= '\\Images\\' + message;
        // $scope.CategoryImage = '~/Images/' + message;
        // alert(message);
        //kendoConsole.log("event :: select (" + message + ")");
    };
    $scope.onSuccess1 = function (e) {
        // var someInfo = e.response.someInfo;
        //if (e.operation === "remove") {
        //    $scope.LogoFile = "";
        //} else {
        //    var message = $.map(e.files, function (file) { return file.name; }).join(", ");
        //    $scope.LogoFile = '\\Images\\' + message;
        //}
        //  alert(someInfo);
    };
        if (animate) {
            preloadStylesheet(url, replaceTheme);
        } else {
            replaceTheme();
        }
    };

    $("#files").kendoUpload({
        multiple: false,
        async: {
            saveUrl: "Preference/SaveLogo",     
            removeUrl: "Preference/RemoveBannerImage", 
            autoUpload: true,
            select: "onSelect1",
           
            
        },
       
    });
    $("#Bannerfiles").kendoUpload({
        multiple: false,
        async: {
            saveUrl: "Preference/SaveBanner",    
            removeUrl: "Preference/RemoveBannerImage", 
            autoUpload: true,
            select: "onSelect1",


        },

    });
    $scope.PreferenceOption = {
        dataSource: {
            data: [
                { Pref_Name: "Appletini.isl", id: 1 },
                 { Pref_Name: "Chamelon.isl", id: 2 },
                   { Pref_Name: "Claymation.isl", id: 3 },
                { Pref_Name: "CS5EnglishDefault.isl", id: 4 },
                { Pref_Name: "CS5EnglishGreen.isl", id: 5 },
                  { Pref_Name: "ElectricBlue.isl", id: 6 },
             { Pref_Name: "Harvest.isl", id: 7 },
               { Pref_Name: "Obsindian.isl", id: 8 },
               { Pref_Name: "Office2007Black.isl", id: 9 },
                { Pref_Name: "Office2007Silver.isl", id: 10 },

                   { Pref_Name: "Peach.isl", id: 11 },
               { Pref_Name: "Pear.isl", id: 12 },
             { Pref_Name: "RadioFlyer.isl", id: 13 },
            { Pref_Name: "RubberBlack.isl", id: 14 },
                { Pref_Name: "Trendy.isl", id: 15 }]
        },
        dataTextField: "Pref_Name",
        dataValueField: "id"

    };
    $scope.SpellCheckerOption = {
        dataSource: {
            data: [
                { List: "None", Spel_id: 1 },
                 { List: "Dialog on Validating", Spel_id: 2 },
                   { List: "As You Type", Spel_id: 3 },
                { List: "CS5EnglishDefault.isl", Spel_id: 4 },
                { List: "Dialog on Validating and As", Spel_id: 5 }
            ]

        },
        dataTextField: "List",
        dataValueField: "Spel_id"
    };

    $scope.Display_IDs = function (value) {
        $scope.Display_ID_Column = value;
    };

    $scope.Encrypt = function () {
        var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse("ES001"), key,
        { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });

        var encrypted_catlogid = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse("20"), key,
          { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });

        window.location = "../Category/Sample?catid=" + encrypted + "&catalogid=" + encrypted_catlogid;
    };
    $(document).keypress(function (e) {
        if (e.which == 13) {
            return false;
        }
    });

        // In this function for reset
    $scope.btnresetclick = function () {

        $scope.init();
    };

        //End
          $scope.convertchangeevent = function (e) {

        if ($scope.convertype == "Image Magic") {
            $scope.ConvertionType = "Image Magic";
        }
        else {
            $scope.ConvertionType = "ReaConverter";
        }
    };

    $scope.savecatalog = function ()
    {      
        var Theme_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(null), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        //var DisplayIDColumns_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.DisplayIDColumns), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var ErrorLogFilePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ErrorLogFilePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var Temporary_File_Path_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.Temporary_File_Path), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var TechSupportURL_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.TechSupportURL), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        //var AllowDuplicateItem_PartNum_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.AllowDuplicateItem_PartNum), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var WebcatSyncURL_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.WebcatSyncURL), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var SpelValue_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.SpelValue), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var WhiteboardWebserviceURL_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.WhiteboardWebserviceURL), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        //var ShowCustomAPPS_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ShowCustomAPPS), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        //var EnableWorkFlow_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.EnableWorkFlow), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        //var FamilyLevelMultitablePreview_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.FamilyLevelMultitablePreview), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        //var ProductLevelMultitablePreview_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ProductLevelMultitablePreview), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        //dataFactory.PreferecesApplicationSettings(Theme_Encrypt, DisplayIDColumns_Encrypt, ErrorLogFilePath_Encrypt, Temporary_File_Path_Encrypt, TechSupportURL_Encrypt, AllowDuplicateItem_PartNum_Encrypt, WebcatSyncURL_Encrypt, SpelValue_Encrypt, WhiteboardWebserviceURL_Encrypt, ShowCustomAPPS_Encrypt, EnableWorkFlow_Encrypt, FamilyLevelMultitablePreview_Encrypt, ProductLevelMultitablePreview_Encrypt).success(function (response) {
        if ($scope.NewProject.SubCatalog_Items == null)
        {
            $scope.NewProject.SubCatalog_Items = 0;
        }
        if ($scope.NewProject.SubCatalog_Items === true)
        {            
            $scope.NewProject.SubCatalog_Items = 1;
        }
        else if ($scope.NewProject.SubCatalog_Items === false) {
            $scope.NewProject.SubCatalog_Items = 0;
        }
        if ($scope.NewProject.Customer_MasterCatalog != null && $scope.NewProject.Customer_MasterCatalog != "" && $scope.ConvertionType != null) {
            
            if ($scope.webSyncFromDate == undefined) {
                var ts = new Date();
                $scope.webSyncFromDate = ts.toLocaleDateString();
            }
            if ($scope.NewProject.autoWebSync == undefined) {
                $scope.NewProject.autoWebSync = false;
            }
            if ($scope.NewProject.ImagePdfAttachemnt == undefined)
            {
                $scope.NewProject.ImagePdfAttachemnt = false;
            }
            if ($scope.NewProject.EnableSubProduct == undefined)
            {
                $scope.NewProject.EnableSubProduct = false;
            }
            if ($scope.ConverterType == undefined) {
                $scope.ConverterType = false;
            }
          
            dataFactory.PreferecesApplicationSettings(Theme_Encrypt, $scope.NewProject.DisplayIDColumns, ErrorLogFilePath_Encrypt, Temporary_File_Path_Encrypt, TechSupportURL_Encrypt, $scope.NewProject.AllowDuplicateItem_PartNum, WebcatSyncURL_Encrypt, SpelValue_Encrypt, WhiteboardWebserviceURL_Encrypt, $scope.NewProject.ShowCustomAPPS, $scope.NewProject.EnableWorkFlow, $scope.NewProject.FamilyLevelMultitablePreview, $scope.NewProject.ProductLevelMultitablePreview, $scope.getCustomerIDs, $scope.NewProject.DisplaySkuProductCountInAlert, $scope.NewProject.SKUAlertPercentage, $scope.NewProject.Customer_MasterCatalog, $scope.NewProject.SubCatalog_Items, $scope.defaultImagePath, $scope.NewProject.EnableSubProduct, $scope.CatalogItemNumber + "~" + $scope.CatalogSubItemNumber, $scope.NewProject.autoWebSync, $scope.webSyncFromDate, $scope.NewProject.ImagePdfAttachemnt, $scope.ConvertionType).success(function (response) // $scope.NewProject.autoWebSyncImagePdfAttachemnt
            {
                
                if ($scope.NewProject.SubCatalog_Items == null)
                {
                 $scope.NewProject.SubCatalog_Items = 0;
                }
            
                if (response === 1)
                {
                    // Here assign the coloumId value in to ["$rootScope.DisplayIdcolumns"]                      
                    $localStorage.CatalogItemNumber = $scope.CatalogItemNumber;
                    $localStorage.CatalogSubItemNumber = $scope.CatalogSubItemNumber;

                    $rootScope.CatalogItemNumber = $scope.CatalogItemNumber;
                    $rootScope.CatalogSubItemNumber = $scope.CatalogSubItemNumber;

                    $localStorage.DisplayIdBycoloumValue = $scope.NewProject.DisplayIDColumns;
                    $rootScope.DisplayIdcolumns = $scope.NewProject.DisplayIDColumns;                  

                    $rootScope.EnableWorkflow = $scope.NewProject.EnableWorkFlow;
                    $localStorage.EnableSubProductLocal = $scope.NewProject.ENABLESUBPRODUCT;
                    $rootScope.EnableSubProduct = $scope.NewProject.ENABLESUBPRODUCT;
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Saved successfully.',
                        type: "info"
                    });
                }
            }).error(function (error) {
                options.error(error);
            });
            $rootScope.EnableSubProduct = $scope.NewProject.ENABLESUBPRODUCT;
        }
        else
        {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Select default Master Catalog.',
                type: "info"
            });
        }
    };

    $scope.savecatalogsettings = function ()
    {
        var CatalogFilePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.CatalogFilePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var CatalogTemplatePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.CatalogTemplatePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var CatalogXMLPath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.CatalogXMLPath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var Export_ImportPath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.Export_ImportPath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });

        dataFactory.PreferecesCatalogSettings(CatalogFilePath_Encrypt, CatalogTemplatePath_Encrypt, CatalogXMLPath_Encrypt, Export_ImportPath_Encrypt, $scope.getCustomerIDs).success(function (response)
        {

        }).error(function (error)
        {
            options.error(error);
        });
    };

    $scope.saveImageSettings = function () {
        var ImagePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ImagePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var ImageConversionUtilityPath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ImageConversionUtilityPath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var WebCatImagePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.WebCatImagePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
        var ReferenceTableImagePath_Encrypt = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse($scope.NewProject.ReferenceTableImagePath), key, { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });

        dataFactory.PreferecesImageSettings(ImagePath_Encrypt, ImageConversionUtilityPath_Encrypt, WebCatImagePath_Encrypt, ReferenceTableImagePath_Encrypt, $scope.ImageSettings.Size1Width, $scope.ImageSettings.Size1Hht, $scope.ImageSettings.Size1Folder, $scope.ImageSettings.Size2Width, $scope.ImageSettings.Size2Hht, $scope.ImageSettings.Size2Folder, $scope.ImageSettings.Size3Width, $scope.ImageSettings.Size3Hht, $scope.ImageSettings.Size3Folder, $scope.ImageSettings.Size4Width, $scope.ImageSettings.Size4Hht, $scope.ImageSettings.Size4Folder, $scope.ImageSettings.Size5Width, $scope.ImageSettings.Size5Hht, $scope.ImageSettings.Size5Folder, $scope.ImageSettings.Size6Width, $scope.ImageSettings.Size6Hht, $scope.ImageSettings.Size6Folder, $scope.getCustomerIDs).success(function (response) {

        }).error(function (error) {
            options.error(error);
        });
    };
    $scope.saveUsersettings = function () {
        dataFactory.PrferencesaveUsersettings($scope.NewProject.DisplayCategoryIdinNavigator, $scope.NewProject.DisplayFamilyandRelatedFamily, $scope.NewProject.UploadImagesPDFForWebSync, $scope.NewProject.CreateXMLForSelectedAttributesToAllFamily, $scope.getCustomerIDs).success(function (response) {
            if (response == 1) {             
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Saved successfully.',
                    type: "info"
                });
            }

        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.Theme = "Appletini.isl";
    $scope.SpelValue = "None";
    $scope.catalogChange = function (e) {
        $scope.Theme = e.sender.text();

    };
    $scope.SpellChange = function (e) {

        $scope.SpelValue = e.sender.text();
    };

    $scope.select = function () {
        value: " ";
    };

    $scope.ImageSettings = {
        Size1Width: 0,
        Size1Hht: 0,
        Size1Folder: '',
        Size2Width: 0,
        Size2Hht: 0,
        Size2Folder: '',
        Size3Width: 0,
        Size3Hht: 0,
        Size3Folder: '',
        Size4Width: 0,
        Size4Hht: 0,
        Size4Folder: '',
        Size5Width: 0,
        Size5Hht: 0,
        Size5Folder: '',
        Size6Width: 0,
        Size6Hht: 0,
        Size6Folder: '',
    };
   
   // $scope.GetAllWorkflowdataSource.read();
    $scope.GetAllWorkflowdataSource = new kendo.data.DataSource({
        pageSize: 10,
        type: "json",
        serverFiltering: true,
        editable: true,
        serverPaging: true,
        serverSorting: true, pageable: true,
        transport: {
            read: function (options) {
                dataFactory.GetAllWorkFlowDataSourceValue(options.data).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },

            update: function (options) {
                dataFactory.UpdateWorkFlowData(options.data).success(function (response) {               
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Update completed.',
                        type: "info"
                    });
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
          
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
                return kendo.stringify(options);

            }
        },
    
        schema: {
            data: "Data",
            total: "Total",
                model: {
                    id: "STATUS_CODE",
                    fields: {
                        STATUS_NAME: { editable: true, nullable: true },
                        STATUS_DESCRIPTION: { editable: true, validation: { required: true } }
                    }
                }
            }
        
    });

    $scope.workflowGridOptions = {
        dataSource: $scope.GetAllWorkflowdataSource, autobind: true,
        sortable: true,
        scrollable: true,
        filterable: true,
        pageable: { buttonCount: 5 },
        autoBind: true,
        selectable: "row",
        editable: "inline",
        columns: [{ field: "STATUS_NAME", title: "Status Name" }, { field: "STATUS_DESCRIPTION", title: "Status Description" },
         { command: ["edit"], title: "&nbsp;" }],
        
        dataBound: function (e) {

        },
    };
    
    /**Check the WebApiConnection Start**/
    $scope.checkWebApiConnection = function () {
        var destinationUrl = $scope.NewProject.WebcatSyncURL + '\CheckWebApiConnection';
        $.get(destinationUrl, function (responseText) {
            if (responseText == true) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Connection Successful',
                    type: "info"
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Connection failed.Please verify connection url.',
                    type: "info"
                });
            }
        }).error(function (error) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Connection failed.Please verify connection url.',
                type: "info"
            });
        });;
    }
        /**End Connection**/

        ///-Get Role OF user
    $scope.getUserSuperAdmin = function () {

        dataFactory.getUserSuperAdmin().success(function (response) {
            if (response !== "" && response !== null) {
                if (response == "1") {
                    $("#idCatalogValues").show();
                    $("#autoWebSync").show();
                }
                else if (response == "2") {
                    $("#idCatalogValues").show();
                    $("#autoWebSync").show();
                }
                else {
                    $("#idCatalogValues").hide();
                    $("#autoWebSync").hide();
                    
                }
            }

        }).error(function (error) {
            options.error(error);
        });
    };
        ///

    $scope.init = function () {
 


        if ($localStorage.getCatalogID === undefined) {
            $scope.selecetedCatalogId = 0;
        } else {
            $scope.selecetedCatalogId = $localStorage.getCatalogID;
        }
        if ($localStorage.getCustomerID == undefined) {
            $scope.getCustomerIDs = 0;
        }
        else {
            $scope.getCustomerIDs = $localStorage.getCustomerID;
            dataFactory.GetPreferencesDetails($scope.getCustomerIDs).success(function (response) {
                $scope.NewProject = response[0];
            });
        }
        dataFactory.GetConverttypeDetails($localStorage.getCustomerID).success(function (response) {
            $scope.ImagepageConvertType = response;
            //options.success(response);
        }).error(function (error) {
            options.error(error);
        });
        $("#productpreview").hide();
        //$scope.GetAllWorkflowdataSource.read();
        $scope.getUserSuperAdmin();
    };

    $scope.ImageConvert = new kendo.data.DataSource({
        data: ['Image Magic', 'ReaConverter'],


    });
    $scope.ConvertionType = ''

    $scope.ImagepageConvertType = 'ReaConverter'
    $scope.init();
}]);