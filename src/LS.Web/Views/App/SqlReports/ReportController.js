﻿LSApp.controller('ReportController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', 'ngTableParams', '$filter', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, ngTableParams, $filter, $rootScope, $localStorage) {

    $scope.reportGrid = $localStorage.DisplayIdBycoloumValue;
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    $rootScope.ReportDetails = "";
    
    $rootScope.RepotsItemsDropdown = true;

   // alert($scope.reportGrid);
//Old
    //$scope.SubReportOptions = {
    //    dataSource: {
    //            data: [{ Sub_R_name1: "Category Listfrom MasterCatalog", Sub_id1: 1 },
    //            { Sub_R_name1: "Image Type Reports", Sub_id1: 2 },
    //            { Sub_R_name1: "Supplier List Report", Sub_id1: 3 },
    //            { Sub_R_name1: "Virtual Pages Report", Sub_id1: 5 },
    //            { Sub_R_name1: "Family Attribute Association with Catalog Report", Sub_id1: 1 },
    //            { Sub_R_name1: "Family and Products not selected in Catalog Report", Sub_id1: 2 },
    //            { Sub_R_name1: "Family Created in Last 7 days Report", Sub_id1: 3 },
    //            { Sub_R_name1: "Family Modified in Last 7 days Report", Sub_id1: 4 },
    //             { Sub_R_name1: "No Image Families Report", Sub_id1: 5 }, { Sub_R_name1: "Product Attribute Association with catalog Report", Sub_id1: 1 },
    //            { Sub_R_name1: "Products with multiple categories and catalog Report", Sub_id1: 2 },
    //            { Sub_R_name1: "Products Created in Last 7 days Report", Sub_id1: 3 },
    //            { Sub_R_name1: "Products Modified in Last 7 days Report", Sub_id1: 4 },
    //            { Sub_R_name1: "Total products Count Report", Sub_id1: 5 },
    //            { Sub_R_name1: "Unpublished product Report", Sub_id1: 6 },
    //         { Sub_R_name1: "Product that has Unpublished Columns and Values Report", Sub_id1: 67 },
    //         { Sub_R_name1: "All Attributes Report", Sub_id1: 1 },
    //        { Sub_R_name1: "All Products with No price Report", Sub_id1: 2 },
    //        { Sub_R_name1: "All Products with No Attributes Report", Sub_id1: 3 },
    //        { Sub_R_name1: "Catalog Attribute Association Report", Sub_id1: 4 },
    //        { Sub_R_name1: "Duplicate Catalog Item Number Report", Sub_id1: 5 }
    //        ]
    //    },
    //    dataTextField: "Sub_R_name1",
    //    dataValueField: "Sub_id1"
    //};
    //New
    $scope.SubReportOptions = {
        dataSource: {
            data: [{ Sub_R_name1: "Category Listfrom MasterCatalog", Sub_id1: 1 },
            { Sub_R_name1: "Image Type Reports", Sub_id1: 2 },
           // { Sub_R_name1: "Supplier List Report", Sub_id1: 3 },
            { Sub_R_name1: "Virtual Pages Report", Sub_id1: 5 },
            { Sub_R_name1: "Family Attribute Association with Catalog Report", Sub_id1: 1 },
            { Sub_R_name1: "Family and Products not selected in Catalog Report", Sub_id1: 2 },
           // { Sub_R_name1: "Family Created in Last 7 days Report", Sub_id1: 3 },
            { Sub_R_name1: "Family Modified in Last 7 days Report", Sub_id1: 4 },
             { Sub_R_name1: "No Image Families Report", Sub_id1: 5 },
             { Sub_R_name1: "Product Attribute Association with catalog Report", Sub_id1: 1 },
            { Sub_R_name1: "Products with multiple categories and catalog Report", Sub_id1: 2 },
           // { Sub_R_name1: "Products Created in Last 7 days Report", Sub_id1: 3 },
            { Sub_R_name1: "Products Modified in Last 7 days Report", Sub_id1: 4 },
            { Sub_R_name1: "Total products Count Report", Sub_id1: 5 },
            { Sub_R_name1: "Unpublished product Report", Sub_id1: 6 },
         { Sub_R_name1: "Product that has Unpublished Columns and Values Report", Sub_id1: 67 },
         { Sub_R_name1: "All Attributes Report", Sub_id1: 1 },
        { Sub_R_name1: "All Products with No price Report", Sub_id1: 2 },
        { Sub_R_name1: "All Products with No Attributes Report", Sub_id1: 3 },
        { Sub_R_name1: "Catalog Attribute Association Report", Sub_id1: 4 }
       // { Sub_R_name1: "Duplicate Catalog Item Number Report", Sub_id1: 5 }
            ]
        },
        dataTextField: "Sub_R_name1",
        dataValueField: "Sub_id1"
    };

    $scope.selects = "Category Listfrom MasterCatalog";
    $scope.mainGrid = [];
    $scope.catalogChange = function (e) {

        $scope.selects = e.sender.text();
        $scope.ImageTypeReportsoptionsDatasource.read();
        var grid = $("#gridSource").data("kendoGrid");
        grid.dataSource.query({
            page: 1,
            pageSize: 5
        });
    };
    $scope.prodData = [];
    $scope.columnsForAtt = [];


    //$scope.ImageTypeReportsoptionsDatasource = new kendo.data.DataSource({
    //    pageSize: 10,
    //    batch: false,
    //    serverPaging: false,
    //    serverSorting: false,
    //    serverFiltering: false,
    //    transport: {
    //        read: function (options) {
    //            dataFactory.GetALLReports($scope.SelectedCatalogId, $scope.selects).success(function (response) {
                    

    //                if ($scope.selects == "Product Attribute Association with catalog Report"
    //                    || $scope.selects == "Products with multiple categories and catalog Report"
    //                      || $scope.selects == "Products Modified in Last 7 days Report"
    //                      || $scope.selects == "Total products Count Report"
    //                      || $scope.selects == "Unpublished product Report"
    //                      || $scope.selects == "Product that has Unpublished Columns and Values Report"
    //                      || $scope.selects == "All Attributes Report"
    //                      || $scope.selects == "All Products with No price Report"
    //                      || $scope.selects == "All Products with No Attributes Report"
    //                      || $scope.selects == "Catalog Attribute Association Report"
    //                    )
    //                {
    //                    for (var i = 0 ; response.length > i ; i++) {
    //                        if (response[i]["ATTRIBUTE_NAME"] == "ITEM#") {
    //                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;
    //                        }
    //                        if (response[i]["ATTRIBUTE_NAME"] == "SUBITEM#") {
    //                            response[i]["ATTRIBUTE_NAME"] = $localStorage.CatalogSubItemNumber;
    //                        }
    //                    }
    //                }
                    
    //                //  dataFactory.GetRelatedfamilyDetails($scope.SelectedCatalogId, $scope.SelectedCatalogId, $scope.SelectedCatalogId, options.data).success(function (response) {
    //                options.success(response);

    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        },
    //        parameterMap: function (options) {
    //            return kendo.stringify(options);
    //        }
    //    }, schema: {
    //        data: function (data) { return data; },
    //        total: function (data) {
    //            return data.length;
    //        }
    //    }
    //});


    // When coloumid is false 
    $scope.reportGridoptionsFalse =
        {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: {
            buttonCount: 5
        }, selectable: true,

        columns: [
            { field: "CATALOG_NAME", title: "Catalog Name ", width: "180px" },
       { field: "CATEGORY_ID", title: "Category ID", width: "180px" },
        { field: "CATEGORY_NAME", title: "Category Name", width: "180px" }
        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            //if ($scope.DisplayIdcolumns == false) {
            //    this.hideColumn(0);
            //   // alert('false')
            //}
        },
        editable: false

    };
    // When coloumid is true
    $scope.reportGridoptionsTrue =
       {
           dataSource: $scope.ImageTypeReportsoptionsDatasource,
           autoBind: false,
           sortable: true,
           filterable: true,
           pageable: {
               buttonCount: 5
           }, selectable: true,

           columns: [{ field: "CATALOG_ID", title: "Catalog ID", width: "100px" },
               { field: "CATALOG_NAME", title: "Catalog Name ", width: "180px" },
          { field: "CATEGORY_ID", title: "Category ID", width: "180px" },
           { field: "CATEGORY_NAME", title: "Category Name", width: "180px" }
           ],
           dataBound: function (e) {
             //  alert('True')
           //    // When coloumid is false grid id also deleted. 
           //    if ($scope.DisplayIdcolumns == false) {
           //        this.hideColumn(0);
           //    }
           },
           editable: false

       };


    $scope.ImageTypeReportsoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: {
            buttonCount: 5
        }, selectable: true,

        columns: [{ field: "ObjectType", title: "OBJECT TYPE", width: "100px" }
        ],
        editable: false,
        norecords: {
            template: "No records found. "
        }
    };

    $scope.supplierlistoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }
        , selectable: true,


  columns: [{ field: "CATEGORY_SHORT", title: "Category ID", width: "100px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "100px" },
{ field: "SUPPLIER_NAME", title: "Supplier Name", width: "100px" }

        ],
  editable: false,
  dataBound: function (e) {
      // When coloumid is false grid id also deleted. 
      if ($scope.DisplayIdcolumns == false) {
          this.hideColumn(2);
          this.hideColumn(4);
      }
     
  },
        norecords: {
            template: "No records found. "
        }
    };


    $scope.virtuallistoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [{ field: "CATEGORY_ID", title: "Category ID", width: "150px" },
{ field: "PROJECT_ID", title: "Project ID", width: "130px" },
{ field: "PROJECT_NAME", title: "Project Name", width: "180px" },
{ field: "COMMENTS", title: "Comments", width: "130px" },
{ field: "RECORD_ID", title: "Record ID", width: "130px" },
{ field: "FILE_NO", title: "File No", width: "130px" },
{ field: "FAMILY_ID", title: "Family ID", width: "130px" },
{ field: "SORT_ORDER", title: "Sort Order", width: "130px" },
{ field: "PAGE_FILE_NAME", title: "Page File Name", width: "250px" },
{ field: "PAGE_FILE_PATH", title: "Page File Path", width: "250px" },
{ field: "PAGE_FILE_TYPE", title: "Page File Type", width: "250px" },
{ field: "NOTES", title: "Notes", width: "120px" },
{ field: "XML_FILE_NAME", title: "XML File Name", width: "250px" }


        ],

        dataBound: function () {

            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(1);
                this.hideColumn(4);
                this.hideColumn(6);
            }


            $(".k-header k-link").kendoTooltip({
                content: function (e) {

                    var cell = $(e.target);


                    var content = cell.text();
                    return content;
                }
            });
        },
        editable: false,
        norecords: {
            template: "No records found. "
        }
    };

    $scope.familyproductoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,

        columns: [
            { field: "CATALOG_ID", title: "Catalog ID", width: "130px" },
            { field: "CATALOG_NAME", title: "Catalog Name", width: "130px" },
{ field: "CATEGORY_ID", title: "Category ID", width: "150px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "130px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "130px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "130px" }

        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(0);
                this.hideColumn(4);
                this.hideColumn(6);
            }

        },

        editable: false
    };

    $scope.familycreatedoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [

{ field: "CATEGORY_ID", title: "Category ID", width: "100px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" }


        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(2);

            }

        },

        editable: false
    };
    $scope.familymodifiedoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,

        columns: [

{ field: "CATEGORY_ID", title: "Category ID", width: "100px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" }
        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(2);

            }

        },

        editable: false
    };
    $scope.noimageoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [

{ field: "CATALOG_ID", title: "Catalog ID", width: "100px" },
{ field: "CATALOG_NAME", title: "Catalog Name", width: "100px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" }



        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(0);
                this.hideColumn(2);

            }

        },
        editable: false
    };
    $scope.prodattriboptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [

{ field: "CATEGORY_ID", title: "Category ID", width: "130px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "130px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "150px" },
{ field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "180px" },
{ field: "ATTRIBUTE_TYPE", title: "Attribute Type", width: "180px" },
{ field: "ATTRIBUTE_ID", title: "Attribute ID", width: "130px" },
{ field: "ATTRIBUTE_CALC_FORMULA", title: "Attribute Calc Formula", width: "250px" },
{ field: "PICKLIST_NAME", title: "Picklist Name", width: "180px" }

        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(2);
                this.hideColumn(6);
            }

        },

        editable: false
    };

    $scope.prodmulticatoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [
            { field: "CATALOG_ID", title: "Catalog ID", width: "130px" },
            { field: "CATALOG_NAME", title: "Catalog Name", width: "130px" },
{ field: "CATEGORY_ID", title: "Category ID", width: "100px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "100px" }

        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(0);
                this.hideColumn(4);
                this.hideColumn(6);
            }

        },

        editable: false
    };
    $scope.prodcreatedoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,

        columns: [

{ field: "CATEGORY_ID", title: "Category ID", width: "100px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "100px" }

        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(2);
                this.hideColumn(4);

            }

        },

        editable: false
    };
    $scope.prodmodifiedoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [

{ field: "CATEGORY_ID", title: "Category ID", width: "100px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "100px" }

        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(2);
                this.hideColumn(4);

            }

        },

        editable: false
    };
    $scope.Totprodoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,

        columns: [

        { field: "PRODUCT_COUNT", title: "Product Count", width: "100px" }

        ],
        editable: false
    };

    $scope.unpublishoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [
            { field: "CATALOG_ID", title: "Catalog ID", width: "110px" },
            { field: "CATALOG_NAME", title: "Catalog Name", width: "140px" },
{ field: "CATEGORY_ID", title: "Category ID", width: "130px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "130px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "130px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "130px" }

        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(0);
                this.hideColumn(4);
                this.hideColumn(6);
            }

        },

        editable: true
    };
    $scope.unpublishcolumnsoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,

        columns: [
            { field: "CATALOG_ID", title: "Catalog ID", width: "100px" },
            { field: "CATALOG_NAME", title: "Catalog Name", width: "100px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "100px" },
{ field: "ATTRIBUTE_ID", title: "Attribute ID", width: "100px" },
{ field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "100px" }],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(0);
                this.hideColumn(2);
                this.hideColumn(4);
                this.hideColumn(5);
            }

        },

        editable: false
    };

    $scope.allattriboptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,

        columns: [
             { field: "ATTRIBUTE_ID", title: "Attribute ID", width: "130px" },
{ field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "150px" },
 { field: "ATTRIBUTE_TYPE", title: "Attribute Type", width: "150px" },
{ field: "CREATE_BY_DEFAULT", title: "Create By Default", width: "250px" },
{ field: "VALUE_REQUIRED", title: "Value Required", width: "200px" },
{ field: "STYLE_NAME", title: "Style Name", width: "150px" },
 { field: "STYLE_FORMAT", title: "Style Format", width: "150px" },
{ field: "DEFAULT_VALUE", title: "Default Value", width: "200px" },
{ field: "PUBLISH2PRINT", title: "Publish2Print", width: "150px" },
{ field: "PUBLISH2WEB", title: "Publish2Web", width: "150px" },
 { field: "PUBLISH2CDROM", title: "Publish2CDROM", width: "150px" },
{ field: "PUBLISH2ODP", title: "Publish2ODP", width: "150px" },
{ field: "ATTRIBUTE_DATATYPE", title: "Attribute Datatype", width: "250px" }
        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(0);

            }
        },

        editable: false
    };

    $scope.allprodoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,

        columns: [
            { field: "CATALOG_ID", title: "Catalog ID", width: "130px" },
            { field: "CATALOG_NAME", title: "Catalog Name", width: "150px" },

{ field: "FAMILY_ID", title: "Family ID", width: "130px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "130px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "130px" },
{ field: "ATTRIBUTE_ID", title: "Attribute ID", width: "130px" },
{ field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "150px" },
 { field: "NUMERIC_VALUE", title: "Numeric Value", width: "150px" }
        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(0);
                this.hideColumn(2);
                this.hideColumn(4);
                this.hideColumn(5);
            }

        },

        editable: false
    };
    $scope.noattriboptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [
            { field: "CATALOG_ID", title: "Catalog ID", width: "100px" },
            { field: "CATALOG_NAME", title: "Catalog Name", width: "100px" },

{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "100px" }

        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(0);
                this.hideColumn(2);
                this.hideColumn(4);
            }

        },

        editable: false
    };
    $scope.catattriboptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [
            { field: "CATALOG_ID", title: "Catalog ID", width: "100px" },
            { field: "CATALOG_NAME", title: "Catalog Name", width: "100px" },

{ field: "ATTRIBUTE_ID", title: "Attribute ID", width: "100px" },
{ field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "100px" }


        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(0);
                this.hideColumn(2);
            }

        },

        editable: false
    };
    $scope.familyattriboptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,


        columns: [{ field: "CATEGORY_ID", title: "Category ID", width: "180px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "150px" },
{ field: "ATTRIBUTE_TYPE", title: "Attribute Type", width: "150px" },
{ field: "ATTRIBUTE_ID", title: "Attribute ID", width: "150px" },
{ field: "ATTRIBUTE_CALC_FORMULA", title: "Attribute Calc Formula", width: "250px" },
{ field: "PICKLIST_NAME", title: "Picklist Name", width: "250px" }

        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(2);
                this.hideColumn(5);
            }

        },

        editable: false,
        norecords: {
            template: "No records found. "
        }
    };

    $scope.duplicateitemoptions = {
        dataSource: $scope.ImageTypeReportsoptionsDatasource,
        autoBind: false,
        sortable: true,
        filterable: true,
        pageable: { buttonCount: 5 }, selectable: true,

        columns: [

{ field: "CATEGORY_ID", title: "Category ID", width: "180px" },
{ field: "CATEGORY_NAME", title: "Category Name", width: "180px" },
{ field: "FAMILY_ID", title: "Family ID", width: "100px" },
{ field: "FAMILY_NAME", title: "Family Name", width: "100px" },
{ field: "PRODUCT_ID", title: "Product ID", width: "100px" },
        { field: "STRING_VALUE", title: "String Value", width: "100px" }
        ],
        dataBound: function (e) {
            // When coloumid is false grid id also deleted. 
            if ($scope.DisplayIdcolumns == false) {
                this.hideColumn(2);
                this.hideColumn(4);

            }

        },

        editable: false
    };
    $scope.init = function () {

        
        var ReportLogValue = sessionStorage.getItem("ReportLogValue");


        // To check the reportValue Title 
        switch(ReportLogValue.toString())
        {
            case "Supplier List Report":
                $rootScope.ReportDetails = "Supplier List";               
                break;
            case "Family Created in Last 7 days Report":
                $rootScope.ReportDetails = "Family created in last 7 days";
                break;
            case "Products Created in Last 7 days Report":
                $rootScope.ReportDetails = "Product created in last 7 days";
                break;
            case "Duplicate Catalog Item Number Report":
                $rootScope.ReportDetails = "Duplicate Item number";
                break;
            case "Category Listfrom MasterCatalog":
                $rootScope.ReportDetails = "All Reports";
                break;              
            default: $rootScope.ReportDetails = "All Reports";
        }

        if (ReportLogValue == "Category Listfrom MasterCatalog")
        {
           // $rootScope.ReportDetails = ReportLogValue;

            $rootScope.RepotsItemsDropdown = true;
            $scope.selects = ReportLogValue;
            $scope.ImageTypeReportsoptionsDatasource.read();
        }
        else 
        {
         //   $rootScope.ReportDetails = ReportLogValue;
            $rootScope.RepotsItemsDropdown = false;
            $scope.selects = ReportLogValue;
            $scope.ImageTypeReportsoptionsDatasource.read();

        }
       
      
        if ($localStorage.getCatalogID === undefined) {
            $scope.SelectedCatalogId = 0;
        }
        else {
            $scope.SelectedCatalogId = $localStorage.getCatalogID;
        }
        $scope.ImageTypeReportsoptionsDatasource.read();
    };
    $scope.init();

}]);



