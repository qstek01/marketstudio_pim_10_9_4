﻿LSApp.controller('SubProductnewattributeController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$localStorage', '$rootScope', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $localStorage,$rootScope) {

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    $scope.Attribute = {
        ATTRIBUTE_TYPE: 1, ATTRIBUTE_NAME: "", STYLE_NAME: "", ATTRIBUTE_DATATYPE: "Text", ATTRIBUTE_SIZE: "", ATTRIBUTE_DECIMAL: 0, ATTRIBUTE_DATAFORMAT: "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$",
        Prefix: "",
        Suffix: "",
        Condition: "",
        CustomValue: "",
        ApplyTo: "All",
        ApplyForNumericOnly: true, PUBLISH2PRINT: true, PUBLISH2WEB: true, PUBLISH2PORTAL: true, PUBLISH2PDF: true, PUBLISH2EXPORT: true
    };
    $scope.init = function () {
        $("#attributedataintegerSub").hide();
        $("#attributedatatextSub").show();
        $("#attributedatadateSub").hide();
        $("#attributedatahyperlinkSub").hide();
        $("#attributedatasizeSub").show();
        $("#attributedataintegersizeSub").hide();
        $("#attributedataemptysizeSub").hide();
        $("#txtsizeSub").show();
        $("#sizeSub").show();
        if ($localStorage.getCatalogID === undefined) {
            $scope.CATALOG_ID = 0;
        }
        else {
            $scope.CATALOG_ID = $localStorage.getCatalogID;
        }
    };
    $scope.init();
   
    $scope.NewAttrSaveSub = function (attribute) {
        
        if (attribute.ATTRIBUTE_NAME.trim() == "") {          
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a valid Name.',
                type: "error"
            });
        }
        else {
            
            dataFactory.savenewattribute(attribute, $scope.CATALOG_ID).success(function (response) {                           
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + response + '.',
                    type: "info"
                });
                $scope.Attribute.ATTRIBUTE_NAME = "";
                $scope.Attribute.STYLE_NAME = "";
                $scope.Attribute.ATTRIBUTE_SIZE = "";
                $scope.Attribute.ATTRIBUTE_DECIMAL = 0;
                $scope.Attribute.Prefix = "";
                $scope.Attribute.Suffix = "";
                $scope.Attribute.Condition = "";
                $scope.Attribute.CustomValue = "";
                $scope.Attribute.ApplyTo = "All";
                $scope.Attribute.ApplyForNumericOnly = true;
                $scope.Attribute.PUBLISH2PRINT = true;
                $scope.Attribute.PUBLISH2WEB = true;
                $scope.Attribute.PUBLISH2EXPORT = true;
                $scope.Attribute.PUBLISH2PDF = true;
                $scope.Attribute.PUBLISH2PORTAL = true;
            }).error(function (error) {
                options.error(error);
            });
        }
        $("#customvaluenew1").val("");
        $("#suffixnew1").val("");
        $("#prefixnewSub").val("");
        $("#attrapplytofirstnewSub").val("");
        $("#attrapplytoallnewSub").val("");
        $("#txt_attributedatasizeSub").val("");
        $("#txt_sizeSub").val("");
        $("#txt_stylenameSub1").val("");
       // $("#txt_attr_name1").val("");
        $scope.GetAllCatalogattributesdataSource.read();
        //$("#attrmaingrid").show();
        //$("#addnewattribute").hide();
    };

    var attributefulldata = [{
        attributetype: 1,
        attributetypename: 'Product Specifications'
    }, {
        attributetype: 3,
        attributetypename: 'Product Image / Attachment'
    }, {
        attributetype: 4,
        attributetypename: 'Product Price'
    },
        {
            attributetype: 6,
            attributetypename: 'Product Key'
        }, {
            attributetype: 7,
            attributetypename: 'Family Description'
        }, {
            attributetype: 9,
            attributetypename: 'Family Image / Attachment'
        },
        {
            attributetype: 11,
            attributetypename: 'Family Attribute'
        }, {
            attributetype: 12,
            attributetypename: 'Family Price'
        }, {
            attributetype: 13,
            attributetypename: 'Family Key'
        }];

    var attributefamilydata = [{ attributetypename: "Family Description", attributetype: "7" }, { attributetypename: "Family Image / Attachment", attributetype: "9" }, { attributetypename: "Family Attribute", attributetype: "11" }, { attributetypename: "Family Price", attributetype: "12" }, { attributetypename: "Family Key", attributetype: "13" }];
    var attributeproductdata = [{ attributetypename: "Product Specifications", attributetype: "1" }, { attributetypename: "Product Image / Attachment", attributetype: "3" }, { attributetypename: "Product Price", attributetype: "4" }, { attributetypename: "Product Key", attributetype: "6" }];
    $scope.attributeChange = function (e) {
        if (e.sender.text() === "Product Price" || e.sender.text() === "Family Price") {
            $("#attributedataintegerSub").show();
            $("#attributedatatextSub").hide();
            $("#attributedatadateSub").hide();
            $("#attributedatahyperlinkSub").hide();
            $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(^-?\d\d*$)";
            $scope.Attribute.ATTRIBUTE_DATATYPE = "Number";
            $scope.Attribute.ATTRIBUTE_SIZE = "";
            $("#attributedatasizeSub").hide();
            $("#attributedataintegersizeSub").show();
            $("#attributedataemptysizeSub").hide();
            $("#txtsizeSub").show();
            $("#sizeSub").show();
            
            $("#DatatypeddlSub1").hide();
            $("#DatatypetxtSub1").show();

            $("#datatxtSub11").val('Numbers(1,1.0,100)');
            $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(^-?\d\d*$)";
            $scope.Attribute.ATTRIBUTE_DATATYPE = "Number";
            $scope.Attribute.ATTRIBUTE_SIZE = "";
            $("#DataformatddlSub1").hide();
            $("#DataformattxtSub").show();
            $("#datatxtSub").val('Integer');
            $("#attrdataruleSub").show();
            $("#applytoSub").show();
            $("#prefixsuffixSub").show();
            $("#conditionSub").show();
            $("#applycustomtextSub").show();

        } else if (e.sender.text() === "Product Image / Attachment" || e.sender.text() === "Family Image / Attachment" || e.sender.text() === "Family Description") {
            $("#DatatypeddlSub1").hide();
            $("#DatatypetxtSub1").show();
            $("#datatxtSub11").val('Text');
            $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
            $scope.Attribute.ATTRIBUTE_DATATYPE = "Text";
            $scope.Attribute.ATTRIBUTE_SIZE = "";
            $("#DataformatddlSub1").hide();
            $("#DataformattxtSub").show();
            $("#datatxtSub").val('All Characters');
            $("#attrdataruleSub").hide();
            $("#applytoSub").hide();
            $("#prefixsuffixSub").hide();
            $("#conditionSub").hide();
            $("#applycustomtextSub").hide();
            $("#txtsizeSub").hide();
            $("#sizeSub").hide();
        } else {
            $("#DataformatddlSub1").show();
            $("#DatatypeddlSub1").show();
            $("#attributedataintegerSub").hide();
            $("#attributedatatextSub").show();
            $("#attributedatadateSub").hide();
            $("#attributedatahyperlinkSub").hide();
            $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
            $scope.Attribute.ATTRIBUTE_DATATYPE = "Text";
            $scope.Attribute.ATTRIBUTE_SIZE = "";
            $("#attributedatasizeSub").show();
            $("#attributedataintegersizeSub").hide();
            $("#attributedataemptysizeSub").hide();
            $("#txtsizeSub").show();
            $("#sizeSub").show();
            
            $("#DatatypetxtSub1").hide();
            $("#DataformattxtSub").hide();
            $("#attrdataruleSub").show();
            $("#applytoSub").show();
            $("#prefixsuffixSub").show();
            $("#conditionSub").show();
            $("#applycustomtextSub").show();
        }
    };
    $scope.attributetypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: attributefulldata
        }),
        change: function (e) {
            $scope.attributeChange(e);
        }
    };
    $scope.attributefamilytypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: attributefamilydata
        }),
        change: function (e) {
            $scope.attributeChange(e);
        }
    };
    $scope.attributeproducttypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: attributeproductdata
        }),
        change: function (e) {
            $scope.attributeChange(e);
        }
    };
    $scope.attributedatetypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: [{
                attributedatatype: "Text",
                attributedatatypename: 'Text'
            }, {
                attributedatatype: 'Number',
                attributedatatypename: 'Numbers(1,1.0,100)'
            }, {
                attributedatatype: 'Date and Time',
                attributedatatypename: 'Date and Time'
            },
            {
                attributedatatype: 'Hyperlink',
                attributedatatypename: 'HyperLink'
            }]
        }),
        change: function (e) {
            if (e.sender.text() === "Text") {
                $("#attributedatatextSub").show();
                $("#attributedataintegerSub").hide();
                $("#attributedatadateSub").hide();
                $("#attributedatahyperlinkSub").hide();
                $("#attributedatasizeSub").show();
                $("#attributedataintegersizeSub").hide();
                $("#attributedataemptysizeSub").hide();
                $("#txtsizeSub").show();
                $("#sizeSub").show();
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
                    $scope.Attribute.ATTRIBUTE_DATATYPE = "Text";
                });
                $("#DatatypetxtSub1").hide();
                $("#DataformattxtSub").hide();
            } else if (e.sender.text() === "Date and Time") {
                $("#attributedatatextSub").hide();
                $("#attributedataintegerSub").hide();
                $("#attributedatadateSub").show();
                $("#attributedatahyperlinkSub").hide();
              
                $("#attributedatasizeSub").hide();
                $("#attributedataintegersizeSub").hide();
                $("#attributedataemptysizeSub").hide();
                $("#txtsizeSub").hide();
                $("#sizeSub").hide();
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)";
                    $scope.Attribute.ATTRIBUTE_DATATYPE = "Date and Time";
                });
                $("#DatatypetxtSub1").hide();
                $("#DataformattxtSub").hide();
                $("#DataformatddlSub1").show();
            }
            else if (e.sender.text() === "HyperLink") {
                $("#attributedatatextSub").hide();
                $("#attributedataintegerSub").hide();
                $("#attributedatadateSub").hide();
                $("#attributedatahyperlinkSub").show();
                
                $("#attributedatasizeSub").hide();
                $("#attributedataintegersizeSub").hide();
                $("#attributedataemptysizeSub").hide();
                $("#txtsizeSub").hide();
                $("#sizeSub").hide();
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^(https?://)|^(HTTPS?://)|^(Https?://)?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+\\.)*([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\\.[a-zA-Z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+/?)$";
                    $scope.Attribute.ATTRIBUTE_DATATYPE = "Hyperlink";
                });
                $("#DatatypetxtSub1").hide();
                $("#DataformattxtSub").hide();
                $("#DataformatddlSub1").show();
            } else {
                $("#DataformatddlSub1").show();
                $("#DatatypeddlSub1").show();
                $("#attributedataintegerSub").show();
                $("#attributedatatextSub").hide();
                $("#attributedatadateSub").hide();
                $("#attributedatahyperlinkSub").hide();
                $("#attributedatasizeSub").hide();
                $("#attributedataintegersizeSub").show();
                $("#attributedataemptysizeSub").hide();
                $("#txtsizeSub").show();
                $("#sizeSub").show();
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(^-?\d\d*$)";
                    $scope.Attribute.ATTRIBUTE_DATATYPE = "Number";
                });
                $("#DatatypetxtSub1").hide();
                $("#DataformattxtSub").hide();
            }
        }
    };
    var intergerdata = [{
        attributedataformat: '(^-?\d\d*$)',
        attributedataformatname: 'Integer'
    }, {
        attributedataformat: '^-{0,1}?\d*\.{0,1}\d{0,6}$',
        attributedataformatname: 'Real Numbers'
    }];
    var textdata = [{
        attributedataformat: '^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$',
        attributedataformatname: 'All Characters'
    }, {
        attributedataformat: '^[ 0-9a-zA-Z\\r\\n]*$',
        attributedataformatname: 'Alpha Numeric'
    }, {
        attributedataformat: '^[ a-zA-Z\\r\\n]*$',
        attributedataformatname: 'Alphabets Only'
    }];
    var datedata = [{
        attributedataformat: '(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)',
        attributedataformatname: 'Short Format (dd/mm/yyyy)'
    }, {
        attributedataformat: '(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{3}(?:0[1-9]|[12]\\d|3[01]))(?=.{0}(?:0[1-9]|1[0-2]))(?!.{0}(?:0[2469]|11)-31)(?!.{0}02-30)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)...[13579])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)..[13579][048])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)..[02468][26])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.).[13579]00)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)[13579][048]00)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)[02468][26]00)',
        attributedataformatname: 'Short Format (mm/dd/yyyy)'
    }, {
        attributedataformat: '(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4}\\s\\d{2}(\\-|\\:)\\d\\d(\\-|\\:)\\d{2})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))',
        attributedataformatname: 'DateTime'
    }, {
        attributedataformat: '(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4}\\s\\d{2}(\\-|\\:)\\d\\d(\\-|\\:)\\d{2}\\s\\w{2})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))',
        attributedataformatname: 'Long Format'
    }, {
        attributedataformat: 'System default settings',
        attributedataformatname: 'System default'
    }];
    var hyperlinkdata = [{
        attributedataformat: "^(https?://)|^(HTTPS?://)|^(Https?://)?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+\\.)*([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\\.[a-zA-Z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+/?)$",
        attributedataformatname: 'Hyperlink'
    }];

    var sizeDecimal = [{
        Decimal: "0"
    },
    {
        Decimal: "1"
    },
    {
        Decimal: "2"
    },
    {
        Decimal: "3"
    },
        {
            Decimal: "4"
        },
        {
            Decimal: "5"
        },
        {
            Decimal: "6"
        }];
    $scope.attributedateintegerformatdatasource = {
        dataSource: new kendo.data.DataSource({
            data: intergerdata
        }),
        change: function (e) {
            var dropdownlist = $("#intdataSub").data("kendoDropDownList");
            if(e.sender.text()==="Real Numbers")
            {
                dropdownlist.text("1");
                $scope.Attribute.ATTRIBUTE_DECIMAL = 1;

            }
            else
            {
                dropdownlist.text("0");
                $scope.Attribute.ATTRIBUTE_DECIMAL = 0;
            }
        }
    };

    $scope.attributedateformattextdatasource = {
        dataSource: new kendo.data.DataSource({
            data: textdata
        }),
        change: function (e) {
            if (e.sender.text() === "All Characters") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
                });

            }
            else if (e.sender.text() === "Alphabets Only") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ a-zA-Z\\r\\n]*$";
                });
            }
            else if (e.sender.text() === "Alpha Numeric") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n]*$";
                });
            }
            else {
                // $scope.Attribute.ATTRIBUTE_DATAFORMAT = "Text";
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
                });
            }
        }
    };
    $scope.attributedateformatdatedatasource = {
        dataSource: new kendo.data.DataSource({
            data: datedata
        }),
        change: function (e) {
            if (e.sender.text() === "Short Format (dd/mm/yyyy)") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)";
                });
            }
            else if (e.sender.text() === "Short Format (mm/dd/yyyy)") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{3}(?:0[1-9]|[12]\\d|3[01]))(?=.{0}(?:0[1-9]|1[0-2]))(?!.{0}(?:0[2469]|11)-31)(?!.{0}02-30)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)...[13579])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)..[13579][048])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)..[02468][26])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.).[13579]00)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)[13579][048]00)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)[02468][26]00)";
                });
            }
            else if (e.sender.text() === "DateTime") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4}\\s\\d{2}(\\-|\\:)\\d\\d(\\-|\\:)\\d{2})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))";
                });
            }
            else if (e.sender.text() === "Long Format") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4}\\s\\d{2}(\\-|\\:)\\d\\d(\\-|\\:)\\d{2}\\s\\w{2})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))";
                });
            }
            else if (e.sender.text() === "System default") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "System default settings";
                });
            }
            else {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)";
                });
            }
        }
    };
    $scope.attributedateformathyperlinkdatasource = {
        dataSource: new kendo.data.DataSource({
            data: hyperlinkdata
        }),
        change: function (e) {
            $scope.$apply(function () {
                $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^(https?://)|^(HTTPS?://)|^(Https?://)?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+\\.)*([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\\.[a-zA-Z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+/?)$";
            });
        }
    };
    $scope.size = {
        dataSource: new kendo.data.DataSource({
            data: sizeDecimal
        }),
        change: function (e) {
            if (e.sender.text() != "0") {
                $("#DataformatddlSub1").hide();
                $("#DataformattxtSub").show();
                $("#datatxtSub").val('Real Numbers');
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^-{0,1}?\d*\.{0,1}\d{0,6}$";
                });
            }
            else {
                $("#datatxt1").val('Integer');
                $("#DataformatddlSub1").show();
                $("#DataformattxtSub").hide();
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(^-?\d\d*$)";
                });

            }
        }
    };


    $scope.CancelProductAttributeSub = function () {
         $scope.winSubProductNewAttributeSetup.close();
        
    };

}]);
