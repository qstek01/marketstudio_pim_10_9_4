﻿
LSApp.controller('attributeController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$localStorage', '$rootScope', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $localStorage, $rootScope) {


    function groupHeaderName(e) {

        if (e.value === 1) {
            return "Item Technical Specification (" + e.count + " items)";
        }
        else if (e.value === 3) {
            return "Item Image / Attachment (" + e.count + " items)";
        }
        else if (e.value === 4) {
            return "Item Price (" + e.count + " items)";
        } else if (e.value === 6) {
            return "Item Key (" + e.count + " items)";
        }
        else if (e.value === 7) {
            return "Product Description (" + e.count + " items)";
        }
        else if (e.value === 9) {
            return "Product Image / Attachment (" + e.count + " items)";
        } else if (e.value === 11) {
            return "Product Specifications (" + e.count + " items)";
        }
        else if (e.value === 12) {
            return "Product Price (" + e.count + " items)";
        }
        else if (e.value === 13) {
            return "Product Key (" + e.count + " items)";
        } else {
            return "Item Technical Specification (" + e.count + " items)";
        }
    }

    var packageTypefulldata = [{
        packagetype: 2,
        packagetypename: 'Item'
    }, {
        packagetype: 1,
        packagetypename: 'Product'
    }];

    // $scope.GroupeTypeAttr = 2;
    $scope.selectedtab = "";
    $('#button_save').hide();
    $("#showAttributeImportdiv").hide();
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

    var key = CryptoJS.enc.Utf8.parse('8080808080808080');
    var iv = CryptoJS.enc.Utf8.parse('8080808080808080');
    //$scope.tableParams = [];
    // --------------------------------------------------- Start Attribute Tab--------------------------------------------------------------------------------
    $scope.Attribute_type = '';



    $scope.userRoleDetachAttributeManagement = false;
    $scope.attrcount = 0;
    $scope.prodimageattrcount = 0;
    $scope.familydesccount = 0;
    $scope.prodpriceattrcount = 0;
    $scope.prodkeyattrcount = 0;
    $scope.familyimgcount = 0;
    $scope.familyattrcount = 0;
    $scope.familypricecount = 0;
    $scope.familykeycount = 0;
    // $scope.enableAttributeGrouping = false;
    $scope.associateAttrGrouping = false;
    // $scope.familyAndProduct = "Family";
    $scope.GroupeType = 2;


    $scope.RemoveAssociation = function (e, id) {
        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure you want to remove the Attribute association?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {
                    dataFactory.Deleteassociationattribute($scope.CATALOG_ID, id.dataItem.ATTRIBUTE_ID).success(function (response) {
                        if (id.dataItem.ATTRIBUTE_ID == 1 || id.dataItem.ATTRIBUTE_ID == 3) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Action not successful, you can't remove this association" + id.dataItem.ATTRIBUTE_NAME + ".",
                                type: "error"
                            });
                        } else {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Attribute association removed successfully.',
                                type: "info"
                            });
                        }
                    }).error(function (error) {
                    });
                };
            }
        });
    };

    $scope.DeleteAttribute = function (e, id) {

        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure you want to delete the selected Attribute?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {

                    dataFactory.Deleteattribute($scope.CATALOG_ID, id.dataItem).success(function (response) {

                        if (id.dataItem.ATTRIBUTE_ID == 1 || id.dataItem.ATTRIBUTE_ID == 3) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'You cannot delete the record of ' + id.dataItem.ATTRIBUTE_NAME + '.',
                                type: "info"
                            });
                        } else {
                            //$.msgBox({
                            //    title: $localStorage.ProdcutTitle,
                            //    content: "Are you sure you want to delete " + e.ATTRIBUTE_NAME + " record?",
                            //    Product deleted successfully
                            //    type: "info"
                            //});
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: "Attribute deleted successfully.",
                                type: "info"
                            });
                        }

                        $('#attrgrid').data('kendoGrid').dataSource.read();
                        $('#attrgrid').data('kendoGrid').refresh();
                        //$scope.AttrGroupGridOptions = {
                        //    toolbar: [
                        //        { template: kendo.template($("#template").html()) }
                        //    ],
                        //    dataSource: $scope.AttrGroupGridDataSource,
                        //    pageable: false,
                        //    selectable: "false",
                        //    columns: [{ field: "ATTRIBUTEGROUPNAME", title: "Attribute Group" }],
                        //    detailTemplate: kendo.template($("#newgrouptemplate").html()),
                        //    dataBound: function () {
                        //        //  this.expandRow(this.tbody.find("tr.k-master-row").first());

                        //    }

                        //};

                        //$scope.detailGridOptions(id.dataItem);
                        //$scope.detailGridOptions(id.dataItem)
                        //$scope.RefreshTable = function () {
                        //    if ($scope.Table.TABLE_ID !== 0) {
                        //        $scope.detailGridOptions.read();
                        //        $scope.detailGridOptions($scope.Table.TABLE_ID);
                        //    }
                        //};

                    }).error(function (error) {
                    });
                };
            }
        });
    };





    var sizeDecimal = [{
        Decimal: "0"
    },
    {
        Decimal: "1"
    },
    {
        Decimal: "2"
    },
    {
        Decimal: "3"
    },
    {
        Decimal: "4"
    },
    {
        Decimal: "5"
    },
    {
        Decimal: "6"
    }];
    $scope.size = {
        dataSource: new kendo.data.DataSource({
            data: sizeDecimal
        }),
        change: function (e) {

        }
    };

    $scope.getpicklistdatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (dataitem) {
                dataFactory.getpicklistall($scope.Attribute_type, $scope.AttrDatatype).success(function (response) {
                    dataitem.success(response);
                }).error(function (error) {
                    dataitem.error(error);
                });
            }

        }
    });

    $scope.IsCalculatedAttributeDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetICalSAttributeList($scope.SelectedItem).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    // ---------------------------------------------------End Attribute Tab--------------------------------------------------------------------------------
    $scope.GridIscalAttrlist = {
        dataSource: $scope.GetGridIscalAttrlistdataSource,
        scrollable: true,
        pageable: { buttonCount: 5 },
        pageSize: 7,
        filterable: { mode: "row" },
        selectable: "row",

        columns: [
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px" }]
    };
    $scope.GetGridIscalAttrlistdataSource = new kendo.data.DataSource({
        type: "json",
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        serverFiltering: true,
        serverPaging: true,
        serverSorting: true, editable: true, pageSize: 7,
        scrollable: true,
        pageable: { buttonCount: 5 },

        transport: {
            read:
                function (options) {

                    dataFactory.GetCalculatedattrList($scope.CATALOG_ID, $scope.Value_Attr_Type.datatype, $scope.Value_Attr_Type.ATTRIBUTE_DATATYPE, options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
        },
        parameterMap: function (options) {
            return kendo.stringify(options);
        }, schema: {

            data: "Data",
            total: "Total",
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ATTRIBUTE_NAME: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
               { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });


    $scope.toggle = function () {
        $scope.myVar = !$scope.myVar;
    };

    $scope.Iscalculatedattr_Save = function (val) {

        if ($scope.Value_Attr_Attrname.substr(-1) === "+" || $scope.Value_Attr_Attrname.substr(-1) === "-" || $scope.Value_Attr_Attrname.substr(-1) === "*" || $scope.Value_Attr_Attrname.substr(-1) === "/") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter valid Data.',
                type: "error"
            });
        }
        else {
            var encryptexpression = $scope.Value_Attr_Attrname;
            var encryptedexpressionname = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(encryptexpression), key,
    { keySize: 128 / 8, iv: iv, mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7 });
            dataFactory.Iscalculatedattr_Save_Click(encryptedexpressionname, $scope.Value_Attr_id, $scope.cal_flag).success(function (response) {
                $scope.winIsCalculatedAttribute.center().close();
            }).error(function (error) {
                options.error(error);
            });
        }
    };

    $scope.onSelection = function (kendoEvent) {
        if ($scope.Value_Attr_Attrname === null) {
            $scope.Value_Attr_Attrname = "";
        }
        var grid = kendoEvent.sender;
        var selectedData = grid.dataItem(grid.select());
        var selectedData1 = "[" + selectedData.ATTRIBUTE_NAME + "]";

        var selectedData2 = $scope.Value_Attr_Attrname;


        if ($scope.Value_Attr_Attrname == "") {
            var res = selectedData2.concat(selectedData1);
            $scope.Value_Attr_Attrname = res;
        } else {
            if ($scope.Value_Attr_Attrname.substr(-1) === "+" || $scope.Value_Attr_Attrname.substr(-1) === "-" || $scope.Value_Attr_Attrname.substr(-1) === "*" || $scope.Value_Attr_Attrname.substr(-1) === "/") {
                var res = selectedData2.concat(selectedData1);
                $scope.Value_Attr_Attrname = res;
            }
        }

    };
    $scope.Change = function (val) {
        if (val === "NumericValue") {

            $("#txt_cal_val").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                //if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //    //display error message
                //    //$("#errmsg").html("Digits Only").show().fadeOut("slow");
                //    return false;
                //}
            });
        }
        else {

        }
    };
    $scope.Adding = function () {
        var addingVal = $scope.Value_Attr_Attrname;
        if (addingVal.substr(-1) === "+" || addingVal.substr(-1) === "-" || addingVal.substr(-1) === "*" || addingVal.substr(-1) === "/") {
            $scope.Value_Attr_Attrname = res;
            $scope.Value_Attr_Attrname = addingVal;
        }
        else {
            var res = addingVal.concat("+");
            $scope.Value_Attr_Attrname = res;
        }

    };
    $scope.Minus = function () {
        var minusVal = $scope.Value_Attr_Attrname;
        if (minusVal.substr(-1) === "+" || minusVal.substr(-1) === "-" || minusVal.substr(-1) === "*" || minusVal.substr(-1) === "/") {

            var res = minusVal;
            $scope.Value_Attr_Attrname = res;
        }
        else {
            var res = minusVal.concat("-");
            $scope.Value_Attr_Attrname = res;

        }
    };
    $scope.Muti = function () {
        var mutiVal = $scope.Value_Attr_Attrname;
        if (mutiVal.substr(-1) === "+" || mutiVal.substr(-1) === "-" || mutiVal.substr(-1) === "*" || mutiVal.substr(-1) === "/") {

            var res = mutiVal;
            $scope.Value_Attr_Attrname = res;
        }
        else {
            var res = mutiVal.concat("*");
            $scope.Value_Attr_Attrname = res;
        }

    };
    $scope.Divide = function () {
        var divVal = $scope.Value_Attr_Attrname;
        if (divVal.substr(-1) === "+" || divVal.substr(-1) === "-" || divVal.substr(-1) === "*" || divVal.substr(-1) === "/") {
            var res = divVal;
            $scope.Value_Attr_Attrname = res;
        }
        else {
            var res = divVal.concat("/");
            $scope.Value_Attr_Attrname = res;
        }

    };

    $scope.Clear = function () {
        $scope.Value_Attr_Attrname = "";
    };

    $scope.userRoleAddAttributeManagement = false;
    $scope.userRoleModifyAttributeManagement = false;
    $scope.userRoleDeleteAttributeManagement = false;
    $scope.userRoleViewAttributeManagement = false;

    $scope.getUserRoleRightsAttributeManagement = function () {
        var id = 40204;
        dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
            if (response !== "" && response !== null) {

                $scope.userRoleAddAttributeManagement = response[0].ACTION_ADD;
                $scope.userRoleModifyAttributeManagement = response[0].ACTION_MODIFY;
                $scope.userRoleDeleteAttributeManagement = response[0].ACTION_REMOVE;
                $scope.userRoleViewAttributeManagement = response[0].ACTION_VIEW;
            }

        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.userRoleAddRelatedExpressionBuilder = false;

    $scope.getUserRoleRightsRelatedExpressionBuilder = function () {
        var id = 4020401;
        dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
            if (response !== "" && response !== null) {
                $scope.userRoleAddRelatedExpressionBuilder = response[0].ACTION_ADD;
                $scope.userRoleModifyRelatedExpressionBuilder = response[0].ACTION_MODIFY;
                $scope.userRoleDeleteRelatedExpressionBuilder = response[0].ACTION_REMOVE;
                $scope.userRoleViewRelatedExpressionBuilder = response[0].ACTION_VIEW;
            }

        }).error(function (error) {
            options.error(error);
        });
    };


    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $scope.CATALOG_ID = 0;
        }
        else {
            $scope.CATALOG_ID = $localStorage.getCatalogID;
        }
        $("#attrmaingrid").show();
        $(".familyProductattributeGrouping").hide();
        $("#picklistdata").hide();
        $scope.getUserRoleRightsAttributeManagement();
        $scope.getUserRoleRightsRelatedExpressionBuilder();
        $timeout(function () {
            $("#deleteEnablebtn").attr('disabled', 'disabled');
            if ($scope.GroupeType == "2")
                $scope.familyAndProduct = "Item";
            if ($scope.GroupeType == "1")
                $scope.familyAndProduct = "Product";
            $scope.groupNameDataSource.read();
            // $scope.familyGetAllattributesdataSource.read();
            var grid = $("#familyProductGrid").data("kendoGrid");
            grid.dataSource.read();
            grid.dataSource.page(1);
            // $scope.familyprodfamilyattrdataSource.read();
            var grid_Selected = $("#selectedAttributesId").data("kendoGrid");
            grid_Selected.dataSource.read();
            grid_Selected.dataSource.page(1);
        }, 2000);


    };

    $scope.Value_Attr_Type = {
        datatype: 1,
        ATTRIBUTE_DATATYPE: "Text",
        name: ""
    };
    $scope.isCalculatedChange = function (dataItem) {

        $route.Value_Attr_Attrname = "";
        $scope.selectcalattr_type = '';
        $scope.selectcalattrdata_type = '';
        $scope.cal_flag = 1;
        $scope.Value_Attr_id = dataItem.ATTRIBUTE_ID;
        $scope.Value_Attr_TypeValue_Attr_Attrname = dataItem.AttributeCalcFormula;
        $scope.selectcalattrdata_type = dataItem.ATTRIBUTE_DATATYPE;
        $scope.selectcalattr_type = dataItem.ATTRIBUTE_TYPE;
        $scope.Value_Attr_Attrname = dataItem.ATTRIBUTE_CALC_FORMULA;
        dataItem.dirty = true;
        if (dataItem.IS_CALCULATED) {
            if ($scope.Value_Attr_id === 1 || $scope.selectcalattr_type == '9' || $scope.selectcalattr_type == '3' || ($scope.selectcalattrdata_type.toLowerCase().indexOf("hyperlink")) > -1 || ($scope.selectcalattrdata_type.toLowerCase().indexOf("image")) > -1 || ($scope.selectcalattrdata_type.toLowerCase().indexOf("date")) > -1) {

                $("#is_cal_false").prop("checked", true);
                $("#is_cal_true").prop("checked", false);
                $("#is_cal_true").attr('disabled', true);
                $("#is_cal_false").prop("checked", true);
            }
            else {
                $("#is_cal_true").attr('disabled', false);
                $("#is_cal_false").attr('disabled', false);
                if ($scope.selectcalattr_type == '4' || $scope.selectcalattr_type == '12' || $scope.selectcalattr_type == '12' || $scope.selectcalattr_type == '6' && ($scope.selectcalattrdata_type.toLowerCase().indexOf("text")) == -1) {
                    $scope.Value_Attr_Type.datatype = dataItem.ATTRIBUTE_TYPE;
                    $scope.Value_Attr_Type.ATTRIBUTE_DATATYPE = dataItem.ATTRIBUTE_DATATYPE;
                    $scope.Value_Attr_Type_name = "NumericValue";
                    $("#txt_cal_val").on("keypress keyup blur", function (event) {
                        $(this).val($(this).val().replace(/[^\d].+/, ""));
                        if ((event.which < 48 || event.which > 57)) {
                            event.preventDefault();
                        }
                    });

                } else {
                    $scope.Value_Attr_Type.datatype = dataItem.ATTRIBUTE_TYPE;
                    $scope.Value_Attr_Type.ATTRIBUTE_DATATYPE = dataItem.ATTRIBUTE_DATATYPE;
                    var type = dataItem.ATTRIBUTE_DATATYPE;
                    if (type.toLowerCase().contains("num"))
                    { $scope.Value_Attr_Type_name = "NumericValue"; }
                    else
                    { $scope.Value_Attr_Type_name = "Non-NumericValue"; }


                }
                if ($scope.userRoleAddRelatedExpressionBuilder) {
                    $scope.winIsCalculatedAttribute.refresh({ url: "../Views/App/Partials/isCalculatedAttribute.html" });
                    $scope.winIsCalculatedAttribute.center().open();
                }

            }
        }
        else {
            if ($scope.Value_Attr_id === 1 || $scope.selectcalattr_type == '9' || $scope.selectcalattr_type == '3' || ($scope.selectcalattrdata_type.toLowerCase().indexOf("hyperlink")) > -1 || ($scope.selectcalattrdata_type.toLowerCase().indexOf("image")) > -1 || ($scope.selectcalattrdata_type.toLowerCase().indexOf("date")) > -1) {

                $("#is_cal_false").prop("checked", true);
                $("#is_cal_true").prop("checked", false);
                $("#is_cal_true").attr('disabled', true);
                $("#is_cal_false").attr('disabled', true);
                $("#txt_cal_val").on("keypress keyup blur", function (event) {
                    $(this).val($(this).val().replace(/[^\d].+/, ""));
                    if ((event.which < 48 || event.which > 57)) {
                        event.preventDefault();
                    }
                });
            }
            else {
                $("#is_cal_true").attr('disabled', false);
                $("#is_cal_false").attr('disabled', false);
                $scope.cal_flag = 0;
                $scope.Value_Attr_Attrname = "";
                $scope.Value_Attr_id = dataItem.ATTRIBUTE_ID;
                $scope.Iscalculatedattr_Save($scope.Value_Attr_Attrnam);
                if ($scope.Value_Attr_Attrname.trim() !== "") {
                    $("#attrgrid").closest(".k-grid").data("kendoGrid").dataSource.read();
                }
            }
        }
    };
    $scope.AttrDatatype = "";
    $scope.picklistTchange = function (e, datatype) {
        if (e === true) {
            $scope.AttrDatatype = datatype;
            $scope.getpicklistdatasource.read();
            $("#picklistdata").show();

        }
        else if (e === false) {
            $("#picklistdata").hide();
        }
    };
    $scope.decimalchange = function (e) {

    };

    var data = [{ type: "Date and Time", value: "Date and Time" }
   , { type: "Text", value: "Text" }

    ];
    $scope.DataTypedatasource = data;
    $scope.DateFormatChange = function (e) {
        if (e.sender.value() != "") {
            if (e.sender.value() == "Text") {
                $("#date_txt").val("All Characters");
            }
            else {
                $("#date_txt").val("Short Format (dd/mm/yyyy)");
            }
        }
    };
    var Hyperdata = [{ type: "Hyperlink", value: "Hyperlink" }
  , { type: "Text", value: "Text" }

    ];
    $scope.HyperTypedatasource = Hyperdata;
    $scope.HyperFormatChange = function (e) {
        if (e.sender.value() != "") {
            if (e.sender.value() == "Text") {
                $("#hyp_txt").val("All Characters");
            }
            else {
                $("#hyp_txt").val("Hyperlink");
            }
        }
    };

    $scope.DecimalChange = function (e, value) {
        if (value.sender.value() !== "0") {
            $("#num_txt").val("Real Numbers");
        }
        else {
            $("#num_txt").val("Integer");
        }
    };

    //----------------------For Pick List pop up new picklist 


    $scope.PickListDataType = {
        PICKLIST_DATA_TYPE: ''
    };

    $scope.NewPickListName = {
        PICKLIST_NAME: ''
    };
    $scope.userRoleModifyPickList = false;
    $scope.newPickList = false;
    $("#PickListValues").hide();
    $("#PickListDateValues").hide();
    $("#PickListNumberValues").hide();
    $scope.picklistChange = function (pkName) {

        if (pkName.sender != undefined) {
            $scope.SelectedPickList = pkName.sender._old;
        } else {
            $scope.SelectedPickList = $scope.ModifypicklistName;
        }
        if ($scope.SelectedPickList !== "") {
            $scope.userRoleModifyPickList = true;
            $scope.userRoleAddPickList = false;
            $scope.newPickList = true;
            $http.get("../PickList/LoadSelectedPickList?name=" + $scope.SelectedPickList).
                then(function (details) {
                    if (details.data.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'datetime') {
                        $("#drpDownDataType").data("kendoDropDownList").value("Date and Time");
                    } else {
                        $("#drpDownDataType").data("kendoDropDownList").value(details.data.PICKLIST_DATA_TYPE);
                    }
                    $scope.PickListDataType.PICKLIST_DATA_TYPE = details.data.PICKLIST_DATA_TYPE;
                    $scope.PicklistData.PICKLIST_DATA = details.data.PICKLIST_DATA;
                    $scope.selecetedPLXMLData = details.data.PICKLIST_DATA;
                    if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'datetime') {
                        var grid = $("#PickListDateValuesgrid").data("kendoGrid");
                        grid.dataSource.page(1);
                        $scope.GetPickListDateValuesDatasource.read();
                        $("#PickListValues").hide();
                        $("#PickListDateValues").show();
                        $("#PickListNumberValues").hide();
                    } else if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'numeric') {
                        var grid2 = $("#PickListNumberValuesgrid").data("kendoGrid");
                        grid2.dataSource.page(1);
                        $("#PickListValues").hide();
                        $("#PickListDateValues").hide();
                        $("#PickListNumberValues").show();
                        $scope.GetPickListNumericValuesDatasource.read();
                    } else {
                        var grid3 = $("#PickListValuesgrid").data("kendoGrid");
                        grid3.dataSource.page(1);
                        $("#PickListValues").show();
                        $("#PickListDateValues").hide();
                        $("#PickListNumberValues").hide();
                        $scope.GetPickListValuesDatasource.read();
                    }
                });
        } else {
            $("#PickListValues").hide();
            $("#PickListDateValues").hide();
            $("#PickListNumberValues").hide();
            $("#drpDownDataType").data("kendoDropDownList").value("Text");
            $scope.userRoleAddPickList = true;
            $scope.newPickList = false;
        }
    };
    $scope.PickListEntries = new kendo.data.ObservableArray([]);


    $scope.PickListGroup = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.getpicklistall('').success(function (response) {

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.addPickListValue = function () {

        $scope.PickListEntries.push({ Value: $scope.PicklistValue });
        $scope.PicklistValue = '';
    };

    $scope.gridColumns = [
    { field: "Value", title: "Value" }
    //{ field: "track", title: "Track" }
    ];

    $scope.addNewPicklist = function () {
        if ($scope.NewPickListName.PICKLIST_NAME.trim() == "") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a valid Name.',
                type: "info"
            });
        }
        else {
            if ($scope.SelectedPickListDataType !== "") {
                $http.get("../PickList/SaveNewPickList?Name=" + $scope.NewPickListName.PICKLIST_NAME.trim() + "&&Datatype=" + $scope.SelectedPickListDataType).
                    then(function (request) {
                        $scope.PickListGroup.read();
                        if (request.data != "")
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Saved successfully.',
                                type: "info"
                            });
                        else
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Name already exists, please enter a different name for the Picklist.',
                                type: "info"
                            });

                        $scope.NewPickListName.PICKLIST_NAME = "";
                    });
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Datatype.',
                    type: "error"
                });
            }
        }

    };
    $scope.PickListDefaultDatatype = [{
        id: 1,
        DATA_TYPE: "Text"
    },
    {
        id: 2,
        DATA_TYPE: "Numeric"
    },
    {
        id: 3,
        DATA_TYPE: "Hyperlink"
    },
    {
        id: 3,
        DATA_TYPE: "Date and Time"
    }];

    $scope.SelectedPickListDataType = "Text";
    $scope.SelectedPickList = "";

    $scope.DataTypeChanged = function (pkDatatype) {

        if (pkDatatype.sender._old === "Date and Time") {
            $scope.SelectedPickListDataType = "DATETIME";
        } else {
            $scope.SelectedPickListDataType = pkDatatype.sender._old;
        }
    };

    $scope.PicklistData = {
        PICKLIST_DATA: ''
    };




    $scope.selecetedPLXMLData = "";
    $scope.GetPickListValuesDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, editable: true, autoBind: false,
        serverPaging: false,
        serverSorting: true, pageable: false, sort: { field: "ListItem", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            update: function (options) {

                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data).success(function (response) {
                    options.success(response);
                    if (response == false) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Duplicate Picklist values are not allowed.',
                            type: "info"
                        });
                        $scope.GetPickListValuesDatasource.read();
                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update Successful.',
                            type: "info"
                        });
                        $scope.GetPickListValuesDatasource.read();
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }, destroy: function (options) {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Are you sure want to delete the PickList Value?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {
                        if (result === "Yes") {

                            dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data).success(function (response) {
                                options.success(response);
                                if (response == false) {

                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'The Picklist value is used in another place, please make sure before deleting.',
                                        //type: "error"
                                    });

                                }
                                else {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'The Picklist value is deleted successfully.',
                                        //type: "error"
                                    });

                                }
                                $scope.GetPickListValuesDatasource.read();
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                        else {

                            $scope.GetPickListValuesDatasource.read();
                        }


                    }
                });


            },
            create: function (options) {

                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data).success(function (response) {
                    options.success(response);
                    if (response == false) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Duplicate Picklist values are not allowed.',
                            type: "info"
                        });

                        $scope.GetPickListValuesDatasource.read();
                    }
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
            }
        }, batch: true, schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "ListItem",
                fields: {
                    ListItem: { editable: true }
                }
            }
        }

    });
    $scope.GetPickListDateValuesDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        editable: true,
        serverPaging: false,
        serverSorting: true, autoBind: false,
        pageable: false, sort: { field: "ListItem", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            update: function (options) {

                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                    options.success(response);
                    if (response == false) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Duplicate Picklist values are not allowed.',
                            type: "info"
                        });


                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Update Successful.',
                            type: "info"
                        });
                    }
                    $scope.GetPickListDateValuesDatasource.read();
                }).error(function (error) {
                    options.error(error);
                });

            },
            create: function (options) {
                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                    options.success(response);
                    if (response == false) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Duplicate Picklist values are not allowed.',
                            type: "info"
                        });
                        $scope.GetPickListDateValuesDatasource.read();
                    }
                }).error(function (error) {
                    options.error(error);
                });

            }, destroy: function (options) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Are you sure want to delete the PickList Value?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {
                        if (result === "Yes") {

                            dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListDateValuesDatasource._data).success(function (response) {
                                options.success(response);
                                if (response == false) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'The Picklist value is used in another place, please make sure before deleting.',
                                        //type: "error"
                                    });
                                }
                                else {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'The Picklist value is deleted successfully.',
                                        //type: "error"
                                    });

                                }
                                $scope.GetPickListValuesDatasource.read();
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                        else {

                            $scope.GetPickListValuesDatasource.read();
                        }


                    }
                });
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
            }
        }, batch: true, schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "ListItem",
                fields: {
                    ListItem: { editable: true, type: "date" }
                }
            }
        }

    });
    $scope.GetPickListNumericValuesDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        editable: true,
        serverPaging: false,
        serverSorting: true, autoBind: false,
        pageable: false, sort: { field: "ListItem", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetPickListValues($scope.SelectedPickList, options.data).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            update: function (options) {
                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                    options.success(response);
                    if (response == false) {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Duplicate Picklist values are not allowed.',
                            type: "info"
                        });
                        $scope.GetPickListNumericValuesDatasource.read();
                    }
                }).error(function (error) {
                    options.error(error);
                });

            },
            create: function (options) {
                dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListNumericValuesDatasource._data).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });

            }, destroy: function (options) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Are you sure want to delete the PickList Value?",
                    type: "confirm",
                    buttons: [{ value: "Yes" }, { value: "No" }],
                    success: function (result) {
                        if (result === "Yes") {

                            dataFactory.UpdateAllPickListValues($scope.SelectedPickList, $scope.PickListDataType.PICKLIST_DATA_TYPE, $scope.GetPickListValuesDatasource._data).success(function (response) {
                                options.success(response);
                                if (response == false) {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'The Picklist value is used in another place, please make sure before deleting.',
                                        //type: "error"
                                    });

                                }
                                else {
                                    $.msgBox({
                                        title: $localStorage.ProdcutTitle,
                                        content: 'The Picklist value is deleted successfully.',
                                        //type: "error"
                                    });

                                }
                                $scope.GetPickListValuesDatasource.read();
                            }).error(function (error) {
                                options.error(error);
                            });
                        }
                        else {

                            $scope.GetPickListValuesDatasource.read();
                        }


                    }
                });
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
            }
        }, batch: true, schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "ListItem",
                fields: {
                    ListItem: { editable: true, type: "number" }
                }
            }
        }

    });
    $scope.GetPickListNumberValues = {
        dataSource: $scope.GetPickListNumericValuesDatasource,
        sortable: true,
        scrollable: true, filterable: { mode: "row" }, autoBind: false,
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Total PickList Values : {2}"
            }
        }, editable: {
            mode: "inline",
            createAt: "bottom"
        },
        cancel: function (e) {
            setTimeout(function () {
                var grid = $("#PickListNumberValuesgrid").data("kendoGrid");
                var gridData = grid.dataSource.view();

                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    if ($rootScope.userRoleModifyPickListManagement == false) {
                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var editButton = $(currenRow).find(".k-grid-edit");
                        editButton.hide();
                    }
                    if ($rootScope.userRoleDeletePickListManagement == false) {

                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var deleteButton = $(currenRow).find(".k-grid-delete");
                        deleteButton.hide();
                    }

                }
            });
        },
        toolbar: [{ name: "create", text: "", template: "<button  title=\'Add\'  class=\'k-button k-grid-add k-item \' ng-show=\'userRoleAddPickListManagement\'>Add</button>" },

             //{ name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'>Save</button>" },
             //{ name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'>Cancel</button>" }
        ],
        //selectable: "row",
        columns: [{
            field: "ListItem", title: "Pick List Value", editor: function (container, options) {
                $scope.name = '';
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'numeric') {
                    var input = $('<input name="' + options.field + '" required="required"/>');
                    input.appendTo(container);
                    input.kendoNumericTextBox();
                }
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'text') {
                    var input = $('<input name="' + options.field + '" type="text" required="required"/>');
                    input.appendTo(container);
                }
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'hyperlink') {
                    var input = $('<input name="' + options.field + '" type="text"  onchange="TextBoxChanged(this)" id="link" required="required"/>');
                    input.appendTo(container);
                }
                //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                tooltipElement.appendTo(container);
            }
        }, {
            //   command: [
            //    { name: "save", text: "", width: "10px", template: "<a ng-click='updateNumeric(this)' class=\'k-grid-save-changes k-item girdicons\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>" },
            //{ name: "cancel", text: "", width: "10px", template: "<a ng-click='cancelNumeric(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
            //{ name: "destroy", text: "", width: "10px", template: "<a  class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove  blue btn-xs-icon\'></div></a>" }], title: "Actions"
            command: ["edit"], width: "250px"
                  , title: "Actions"
        }],
        dataBound: function (e) {
            var editeBtn = $(".k-grid-edit");
            var deleteBtn = $(".k-grid-delete");

            editeBtn.attr("title", "Edit");
            deleteBtn.attr("title", "Delete");

            if (e.sender._data.length > 0) {
                var sort = e.sender._data[0].Sort;
                if (sort != undefined) {
                    if (sort != "asc") {
                        if (e.sender.dataSource.sort()[0].dir != "desc") {
                            e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                        }
                    }
                }
            }
            var grid = $("#PickListNumberValuesgrid").data("kendoGrid");
            var gridData = grid.dataSource.view();

            for (var i = 0; i < gridData.length; i++) {
                var currentUid = gridData[i].uid;
                if ($rootScope.userRoleModifyPickListManagement == false) {
                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var editButton = $(currenRow).find(".k-grid-edit");
                    editButton.hide();
                }
                if ($rootScope.userRoleDeletePickListManagement == false) {

                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var deleteButton = $(currenRow).find(".k-grid-delete");
                    deleteButton.hide();
                }

            }
        },
        edit: function (e) {

            var updateBtn = $(".k-grid-update");
            var cancelBtn = $(".k-grid-cancel");

            updateBtn.attr("title", "Update");
            cancelBtn.attr("title", "Cancel");

        }
    };
    $scope.GetPickListDateValues = {
        dataSource: $scope.GetPickListDateValuesDatasource,
        sortable: true,
        scrollable: true, filterable: { mode: "row" }, autoBind: false,
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Total PickList Values : {2}"
            }
        }, editable: {
            mode: "inline",
            createAt: "bottom"
        },
        cancel: function (e) {
            setTimeout(function () {
                var grid = $("#PickListDateValuesgrid").data("kendoGrid");
                var gridData = grid.dataSource.view();

                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    if ($rootScope.userRoleModifyPickListManagement == false) {
                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var editButton = $(currenRow).find(".k-grid-edit");
                        editButton.hide();
                    }
                    if ($rootScope.userRoleDeletePickListManagement == false) {

                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var deleteButton = $(currenRow).find(".k-grid-delete");
                        deleteButton.hide();
                    }

                }
            });
        },
        toolbar: [{ name: "create", text: "", template: "<button title=\'Add\' class=\'k-button k-grid-add k-item \' ng-show=\'userRoleAddPickListManagement\'>Add</button>" },

          //{ name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'>Save</button>" },
          //{ name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'>Cancel</button>" }
        ],
        // selectable: "row",
        columns: [{
            field: "ListItem", title: "Pick List Value", format: "{0:dd/MM/yyyy hh:mm tt}", editor: function (container, options) {
                //create input element and add the validation attribute
                $scope.name = '';
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase().trim() === 'datetime') {
                    var input = $('<input kendo-date-time-picker name="' + options.field + '" k-interval=10  required="required" />');
                    input.appendTo(container);
                }
                //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                tooltipElement.appendTo(container);
            }, filterable: {
                ui: function (element) {
                    element.kendoDateTimePicker({
                        format: "{0:MM/dd/yyyy}",

                        interval: 15
                    });
                }
            }
        }, {
            command: ["edit"], width: "250px"
                , title: "Actions"
            //  command: [
            //    { name: "save", text: "", width: "10px", template: "<a ng-click='updateDate(this)' class=\'k-grid-save-changes k-item girdicons\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>"},
            //{ name: "cancel", text: "", width: "10px", template: "<a ng-click='cancelDate(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
            //{ name: "destroy", text: "", width: "10px", template: "<a  class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove  blue btn-xs-icon\'></div></a>" }], title: "Actions"
        }],
        dataBound: function (e) {
            var editeBtn = $(".k-grid-edit");
            var deleteBtn = $(".k-grid-delete");

            editeBtn.attr("title", "Edit");
            deleteBtn.attr("title", "Delete");

            if (e.sender._data.length > 0) {
                var sort = e.sender._data[0].Sort;
                if (sort != undefined) {
                    if (sort != "asc") {
                        if (e.sender.dataSource.sort()[0].dir != "desc") {
                            e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                        }
                    }
                }
            }
            var grid = $("#PickListDateValuesgrid").data("kendoGrid");
            var gridData = grid.dataSource.view();

            for (var i = 0; i < gridData.length; i++) {
                var currentUid = gridData[i].uid;
                if ($rootScope.userRoleModifyPickListManagement == false) {
                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var editButton = $(currenRow).find(".k-grid-edit");
                    editButton.hide();
                }
                if ($rootScope.userRoleDeletePickListManagement == false) {
                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var deleteButton = $(currenRow).find(".k-grid-delete");
                    deleteButton.hide();
                }
            }
        },
        edit: function (e) {
            var updateBtn = $(".k-grid-update");
            var cancelBtn = $(".k-grid-cancel");
            updateBtn.attr("title", "Update");
            cancelBtn.attr("title", "Cancel");
        }
    };
    $scope.DeletePickList = function () {
        dataFactory.DeletePickList($scope.SelectedPickList).success(function (response) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + response + '.',
                type: "info"
            });
            $scope.selecetedPLXMLData = "";
            $scope.SelectedPickList = "";
            $scope.PickListDataType.PICKLIST_DATA_TYPE = "";
            $("#PickListValues").hide();
            $("#PickListDateValues").hide();
            $("#PickListNumberValues").hide();
            $scope.PickListGroup.read();
        }).error(function (error) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: '' + error + '.',
                type: "info"
            });
        });
    };

    $scope.GetPickListValues = {

        dataSource: $scope.GetPickListValuesDatasource,
        sortable: true,
        scrollable: true, filterable: { mode: "row" }, autoBind: false,
        pageable: {
            numeric: false,
            previousNext: false,
            messages: {
                display: "Total PickList Values : {2}"
            }
        }, editable: {
            mode: "inline",
            createAt: "bottom"
        },
        cancel: function (e) {
            setTimeout(function () {
                var grid = $("#PickListValuesgrid").data("kendoGrid");
                var gridData = grid.dataSource.view();

                for (var i = 0; i < gridData.length; i++) {
                    var currentUid = gridData[i].uid;
                    if ($rootScope.userRoleModifyPickListManagement == false) {
                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var editButton = $(currenRow).find(".k-grid-edit");
                        editButton.hide();
                    }
                    if ($rootScope.userRoleDeletePickListManagement == false) {

                        var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                        var deleteButton = $(currenRow).find(".k-grid-delete");
                        deleteButton.hide();
                    }

                }
            });
        },
        toolbar: [{ name: "create", text: "", template: "<button title=\'Add\' class=\'k-button k-grid-add k-item \' ng-show=\'userRoleAddPickListManagement\'>Add</button>" },

          //{ name: "save", text: "", template: "<button class=\'k-button k-grid-save-changes k-item \'>Save</button>" },
          //{ name: "cancel", text: "", template: "<button class=\'k-button k-grid-cancel-changes k-item \'>Cancel</button>" }
        ],
        selectable: "row",
        columns: [{
            field: "ListItem", title: "Pick List Value", editor: function (container, options) {
                //create input element and add the validation attribute
                $scope.name = '';
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'numeric') {
                    var input = $('<input name="' + options.field + '"  data-required-msg="Picklist value is mandatory." required="required"/>');
                    input.appendTo(container);
                    input.kendoNumericTextBox();
                }
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'text') {
                    var input = $('<input name="' + options.field + '" type="text"  data-required-msg="Picklist value is mandatory." required="required"/>');
                    input.appendTo(container);
                }
                if ($scope.PickListDataType.PICKLIST_DATA_TYPE.toLowerCase() === 'hyperlink') {
                    var input = $('<input name="' + options.field + '" type="text"   data-required-msg="Picklist value is mandatory."  onchange="TextBoxChanged(this)" id="link" required="required"/>');
                    input.appendTo(container);
                }
                //create tooltipElement element, NOTE: data-for attribute should match editor's name attribute
                var tooltipElement = $('<span class="k-invalid-msg" data-for="' + options.field + '"></span>');
                tooltipElement.appendTo(container);
            }
        }, {
            //command: [
            //     { name: "save", text: "", width: "10px", template: "<a ng-click='update(this)' class=\'k-grid-save-changes k-item girdicons\'><div title=\'Save\' class=\'glyphicon glyphicon-floppy-save blue btn-xs-icon\'></div></a>" },
            // { name: "cancel", text: "", width: "10px", template: "<a ng-click='cancel(this)' class=\'k-grid-cancel-changes k-item girdicons\'><div title=\'Cancel\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></a>" },
            //    { name: "destroy", text: "", width: "10px", template: "<a class=\'k-grid-delete k-item girdicons\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" }

            //], title: "Actions", width: "100px"
            command: ["edit"], width: "250px"
                , title: "Actions"
        }],
        dataBound: function (e) {
            var editeBtn = $(".k-grid-edit");
            var deleteBtn = $(".k-grid-delete");

            editeBtn.attr("title", "Edit");
            deleteBtn.attr("title", "Delete");

            if (e.sender._data.length > 0) {
                var sort = e.sender._data[0].Sort;
                if (sort != undefined) {
                    if (sort != "asc") {
                        if (e.sender.dataSource.sort()[0].dir != "desc") {
                            e.sender.dataSource.sort({ field: "ListItem", dir: "desc" });
                        }
                    }
                }
            }
            var grid = $("#PickListValuesgrid").data("kendoGrid");
            var gridData = grid.dataSource.view();

            for (var i = 0; i < gridData.length; i++) {
                var currentUid = gridData[i].uid;
                if ($rootScope.userRoleModifyPickListManagement == false) {
                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var editButton = $(currenRow).find(".k-grid-edit");
                    editButton.hide();
                }
                if ($rootScope.userRoleDeletePickListManagement == false) {

                    var currenRow = grid.table.find("tr[data-uid='" + currentUid + "']");
                    var deleteButton = $(currenRow).find(".k-grid-delete");
                    deleteButton.hide();
                }

            }
        },
        edit: function (e) {

            var updateBtn = $(".k-grid-update");
            var cancelBtn = $(".k-grid-cancel");

            updateBtn.attr("title", "Update");
            cancelBtn.attr("title", "Cancel");

        }

    };

    $scope.userRoleAddPickList = false;
    $scope.userRoleModifyPickList = false;
    $scope.userRoleDeletePickList = false;
    $scope.userRoleViewPickList = false;

    $scope.getUserRoleRightsPickList = function () {
        var id = 40205;
        dataFactory.getUserRoleRights(id, $localStorage.getCatalogID).success(function (response) {
            if (response !== "" && response !== null) {
                $scope.userRoleAddPickList = response[0].ACTION_ADD;
                $scope.userRoleModifyPickList = response[0].ACTION_MODIFY;
                $scope.userRoleDeletePickList = response[0].ACTION_REMOVE;
                $scope.userRoleViewPickList = response[0].ACTION_VIEW;
            }

        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.PickListItems = function (data) {
        $("#win1").show();
        $scope.ModifypicklistNamepopup = data;
        $scope.ModifypicklistName = data;
        $scope.win1.center().open();
        if ($scope.ModifypicklistName != "" && $scope.ModifypicklistName != undefined && $scope.ModifypicklistName != null) {
            $scope.picklistChange(data);
        } else {
            $("#PickListValues").hide();
            $("#PickListDateValues").hide();
            $("#PickListNumberValues").hide();
            $scope.PickListDataType.PICKLIST_DATA_TYPE = "";
            $scope.ModifypicklistName = "";
        }

        if (!$("#manage")[0].className.contains("k-state-active")) {
            $("#newPicklist")[0].className = "k-item k-state-default k-last";
            $("#manage")[0].className = "k-state-active k-item k-tab-on-top k-state-default k-last";
            $("#tabstrips-2")[0].style.cssText = "display: block; opacity: 1;";
            $("#tabstrips-2")[0].className = "form-horizontal k-content k-state-active";
            $("#tabstrips-1")[0].style.cssText = "opacity: 0; display: none;";
        }
    };

    $scope.refreshlist = function () {
        $scope.getpicklistdatasource.read();

    };

    //-----------------------

    //$scope.AttrGroupGridDataSource = new kendo.data.DataSource({
    //    type: "json",
    //    serverFiltering: true,
    //    transport: {
    //        read: function (options) {
    //            dataFactory.GetAllattributesCount($scope.CATALOG_ID).success(function (response) {
    //                options.success(response);
    //            }).error(function (error) {
    //                options.error(error);
    //            });
    //        }
    //    }, schema: {
    //        model: {
    //            id: "ATTRIBUTETYPE",
    //            fields: {
    //                ATTRIBUTEGROUPNAME: { editable: false, nullable: true }
    //            }
    //        }
    //    }
    //});

    //$scope.AttrGroupGridOptions = {

    //    toolbar: [
    //        { template: kendo.template($("#template").html()) }
    //    ],
    //    dataSource: $scope.AttrGroupGridDataSource,
    //    pageable: false,
    //    selectable: "false",
    //    columns: [{ field: "ATTRIBUTEGROUPNAME", title: "Attribute Group" }],
    //    detailTemplate: kendo.template($("#newgrouptemplate").html()),
    //    dataBound: function () {
    //        //  this.expandRow(this.tbody.find("tr.k-master-row").first());

    //    }

    //};

    $scope.GetAllSupplierdataSource = new kendo.data.DataSource({
        // pageSize: 10,
        filterable: true,
        serverFiltering: false,
        type: "json",
        //serverFiltering: false,
        //editable: true,
        //serverPaging: true,
        //serverSorting: true, pageable: true,
        transport: {
            read: function (options) {

                dataFactory.GetAllattributesManager($scope.CATALOG_ID, $scope.ATTRIBUTETYPE).success(function (response) {
                    if (response.length > 0 && response[0]["ATTRIBUTE_ID"].toString() == "1")
                        response[0]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;

                    options.success(response);
                    // var CAPTION = options.data.CAPTION;
                }).error(function (error) {
                    options.error(error);
                });
            }
            , update: function (options) {

                if (options.data.ATTRIBUTE_NAME.match("__") || options.data.ATTRIBUTE_NAME.match("-") || options.data.ATTRIBUTE_NAME.match("&") || options.data.ATTRIBUTE_NAME.match("%") || options.data.ATTRIBUTE_NAME.match("[*+]")) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Attribute name should not contain special characters like (* & % + __ ).',
                        type: "error"
                    });
                }
                else {
                    if (options.data.STYLE_FORMAT !== null) {
                        options.data.STYLE_FORMAT = options.data.STYLE_FORMAT.text;
                    }

                    if (options.data.ATTRIBUTE_DATATYPE.toLowerCase().indexOf("date") > -1) {
                        var val = $("#date_txt").val();
                        if ((val.toLocaleLowerCase().indexOf("date") > -1) || (val.toLocaleLowerCase().indexOf("format"))) {
                            options.data.ATTRIBUTE_DATATYPE = "Date and Time";
                            options.data.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)";

                        } else {

                            options.data.ATTRIBUTE_DATATYPE = "Text";
                            options.data.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";

                        }

                    } else if (options.data.ATTRIBUTE_DATATYPE.toLowerCase().indexOf("hyp") > -1) {
                        var val = $("#date_txt").val();
                        if (val.toLocaleLowerCase().indexOf("hyp") > -1) {
                            options.data.ATTRIBUTE_DATATYPE = "Hyperlink";
                            options.data.ATTRIBUTE_DATAFORMAT = "^(https?://)|^(HTTPS?://)|^(Https?://)?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+\\.)*([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\\.[a-zA-Z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+/?)$";
                        } else {

                            options.data.ATTRIBUTE_DATATYPE = "Text";
                            options.data.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";

                        }

                    } else if (options.data.ATTRIBUTE_DATATYPE.toLowerCase().indexOf("num") > -1) {
                        if (options.data.DECIMAL === "0") {
                            options.data.ATTRIBUTE_DATAFORMAT = "(^-?\d\d*$)";
                        } else {
                            options.data.ATTRIBUTE_DATAFORMAT = "^-{0,1}?\\d*\\.{0,1}\\d{0,6}$";
                        }

                    }
                    if (options.data.USE_PICKLIST === true && options.data.PICKLIST_NAME === "") {

                        var uid = $(".k-edit-form-container").closest("[data-role=window]").data("uid");
                        var name = "usepicklist" + options.data.ATTRIBUTE_ID;
                        var model = $("#attrgrid").data("kendoGrid").dataSource.getByUid(uid);
                        model.set(name, false);
                        $scope.$apply(function () {
                            options.data.USE_PICKLIST = false;
                            model.USE_PICKLIST = false;
                        });
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please select a Picklist.',
                            type: "info"
                        });
                    } else {

                        dataFactory.UpdateAllattributes(options.data).success(function (response) {

                            if (response.trim() !== "") {

                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                                $('#attrgrid').data('kendoGrid').dataSource.read();
                                response = "";
                            } else {
                                //$("#attrgrid").closest(".k-grid").data("kendoGrid").dataSource.read();
                                //$('#attrgroupgrids').data('kendoGrid').dataSource.read();
                                //$('#attrgroupgrids').data('kendoGrid').refresh();
                                options.success(response);
                            }
                        }).error(function (error) {
                            options.error(error);
                        });
                        $("#attrgrid").closest(".k-grid").data("kendoGrid").dataSource.read();
                        //$("#attrgrid").closest(".k-grid").data("kendoGrid").dataSource.read();

                        ////  $('#attrgrid').data('kendoGrid').dataSource.read();
                        //$('#attrgrid').data('kendoGrid').refresh();
                    }
                }
            }


            //parameterMap: function (options, operation) {
            //    if (operation !== "read" && options.models) {
            //        return { models: kendo.stringify(options.models) };
            //    }
            //    return kendo.stringify(options);

            //}
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ATTRIBUTE_NAME: { editable: true },
                    // CAPTION: { editable: true },
                    ATTRIBUTE_TYPE: { editable: true },
                    ATTRIBUTE_ID: { editable: true, nullable: true },
                    CREATE_BY_DEFAULT: { editable: true },
                    VALUE_REQUIRED: { editable: true },
                    STYLE_NAME: { editable: true },
                    STYLE_FORMAT: { editable: true },
                    DEFAULT_VALUE: { editable: true },
                    PUBLISH2PRINT: { editable: true },
                    PUBLISH2WEB: { editable: true },
                    PUBLISH2CDROM: { editable: true },
                    PUBLISH2ODP: { editable: true },
                    USE_PICKLIST: { editable: true },
                    ATTRIBUTE_DATATYPE: { editable: true },
                    ATTRIBUTE_DATAFORMAT: { editable: true },
                    ATTRIBUTE_DATARULE: { editable: true },
                    UOM: { editable: true },
                    IS_CALCULATED: { editable: true },
                    ATTRIBUTE_CALC_FORMULA: { editable: true },
                    PICKLIST_NAME: { editable: true },
                    AllowEdit: { editable: true }
                }

            }
        },
    });
   

    $scope.gridOptions = {
        dataSource: $scope.GetAllSupplierdataSource,
       // selectable: "row",
        scrollable: true,
        autoBind: true,
        sortable: true,
        filterable: { mode: "row" },

        //groupable: false,
        //filterable: true,
        columns: [{ field: "ATTRIBUTE_ID", title: "ID", width: "50px", filterable: false },
           { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px",filterable: true },
           { field: "ATTRIBUTE_DATATYPE", title: "Data Type", width: "250px" },
           { field: "STYLE_NAME", title: "Style Name", width: "250px" },
           {
               headerTemplate: kendo.template("<div><center>Publish</center></div>"),
               columns: [
              { field: "PUBLISH2WEB", title: "Web", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2WEB ? "checked=checked" : "" #></input>', filterable: false },
              { field: "PUBLISH2PRINT", title: "Print", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2PRINT ? "checked=checked" : "" #></input>', filterable: false },
              { field: "PUBLISH2PDF", title: "PDF", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2PDF ? "checked=checked" : "" #></input>', filterable: false },
              //{ field: "PUBLISH2EXPORT", title: "Export", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2EXPORT ? "checked=checked" : "" #></input>', filterable: false },
              //{ field: "PUBLISH2PORTAL", title: "Portal", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2PORTAL ? "checked=checked" : "" #></input>', filterable: false }
               ]
           },
           {

               command: ["edit", "destroy"],
               command: [{ name: 'edit', text: "", width: "10px", template: "<a class=\'k-grid-edit k-item girdicons\' ng-show=\'userRoleModifyAttributeManagement\'><div title=\'Edit\' class=\'glyphicon glyphicon-edit blue btn-xs-icon\' ></div></a>" },
                   { name: "delete", text: "", width: "10px", template: "<a class=\'k-item girdicons\' ng-click=\'DeleteAttribute($event,this)\' ng-show=\'userRoleDeleteAttributeManagement\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" },
                   //Move to Recycle Bin
                   { template: '<a class=\'k-grid-detach k-item girdicons\' ng-show=\'userRoleDetachAttributeManagement\'  ng-click=\'RemoveAssociation($event,this)\'><div title=\'Detach\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></<a>' }],
               title: "Actions",
               //template: "#if(true){#<a>data</a>#}#",
               width: "100px",

           }],
        toolbar: [
               {
                   template: kendo.template($("#template").html())

               }
        ],

        editable: {
            mode: "popup",
            template: $("#popup_editor").html(), update: true
        },
        dataBound: function (e) {   
            if ($rootScope.DisplayIdcolumns == false) {
                this.hideColumn(0);
            }
        }
        //dataBound: function () {
        //    this.hideColumn(0);


        //}
        //  selectable: "row",
        //toolbar: ["create"]
    };
    
    $scope.AllAttributeOptions = {
        dataSource: {
            data: [
            { Sub_R_name1: "All Attributes", Sub_id1: 1 },
            { Sub_R_name1: "Item Specifications", Sub_id1: 2 },
            { Sub_R_name1: "Item Image/Attachment", Sub_id1: 5 },
            { Sub_R_name1: "Item Price", Sub_id1: 1 },
            { Sub_R_name1: "Item Key", Sub_id1: 2 },
            { Sub_R_name1: "Product Specifications", Sub_id1: 4 },
            { Sub_R_name1: "Product Image/Attachment", Sub_id1: 5 },
            { Sub_R_name1: "Product Price", Sub_id1: 1 },
            { Sub_R_name1: "Product Key", Sub_id1: 2 },
            { Sub_R_name1: "Product Description", Sub_id1: 4 },
            { Sub_R_name1: "Category Specifications", Sub_id1: 5 },
            { Sub_R_name1: "Category Image/Attachment", Sub_id1: 1 },
            { Sub_R_name1: "Category Description", Sub_id1: 2 }
            ]
        },
        dataTextField: "Sub_R_name1",
        dataValueField: "Sub_id1"
    };


    $scope.attributeChange = function (e) {
        
        $scope.selects = e.sender.text();
        switch ($scope.selects) {
            case 'Item Specifications':
                $scope.ATTRIBUTETYPE = 1;

                break;
            case 'Item Image/Attachment':
                $scope.ATTRIBUTETYPE = 3;

                break;
            case 'Item Price':
                $scope.ATTRIBUTETYPE = 4;
                break;
            case 'Item Key':
                $scope.ATTRIBUTETYPE = 6;
                break;
            case 'Product Specifications':
                $scope.ATTRIBUTETYPE = 11;
                break;
            case 'Product Image/Attachment':
                $scope.ATTRIBUTETYPE = 9;
                break;
            case 'Product Price':
                $scope.ATTRIBUTETYPE = 12;
                break;
            case 'Product Key':
                $scope.ATTRIBUTETYPE = 13;
                break;
            case 'Product Description':
                $scope.ATTRIBUTETYPE = 7;
                break;
            case 'Category Specifications':
                $scope.ATTRIBUTETYPE = 21;
                break;
            case 'Category Image/Attachment':
                $scope.ATTRIBUTETYPE = 23;
                break;
            case 'Category Description':
                $scope.ATTRIBUTETYPE = 25;
                break;

            default:
                $scope.ATTRIBUTETYPE = 0;


        }

        //$scope.ATTRIBUTETYPE = $scope.attributetype;

        //  $scope.detailGridOptions.read($scope.selects, $scope.attributetype);
        $scope.GetAllSupplierdataSource.read();

        //$scope.GetAllDataSource.read();

        // $scope.AllAttributeoptionsDatasource.read();
        //var grid = $("#attrgrid").data("kendoGrid");
        //grid.dataSource.query({
        //    page: 1,
        //    pageSize: 10
        //});
    };

    $scope.importAttributes = function () {
        $("#attrmaingrid").hide();
        $("#showAttributeImportdiv").show();
        $scope.showAttrImportdiv = true;


        $scope.AttrUploadDiv = true;
        // $scope.RoleUploadDiv = false;
        $scope.userbuttondiv = true;
        $("#Attrbuttonhide").show();
        //  $scope.rolebuttondiv = false;
        //   $("#Rolebuttonhide").hide();


        //  $scope.UserImportUploaddiv = true;


        //   $scope.RoleImportUploaddiv = false;
        // $scope.showImportRolediv = false;
        //  $("#showRoleImportdiv").hide();
        // $scope.RoleImportdiv = false;
        //$scope.showImportdiv = true;
        //   $scope.hideImportType = false;
        $scope.Importdiv = true;
        //$scope.UserImportdiv = true;
        $scope.hideImportType = true;
        $scope.importtype = "Attribute";
        $rootScope.importtypeValue = "Attribute";

        //  $("#userListViewGrid").hide();

        $scope.$emit("MyFunction");



    };

    $scope.exportAttributes = function () {
       
        dataFactory.GetAllAttributesExport($scope.CATALOG_ID, $scope.ATTRIBUTETYPE, $rootScope.DisplayIdcolumns).success(function (response) {

            if (response !== "" && response !== null && response.length != 0) {

                window.open("DownloadAttributesExport.ashx?");
                $.msgBox({
                    title: "CatalogStudio",
                    content: "Export completed successfully.",
                    type: "info"

                });
            }
            else {
                $.msgBox({
                    title: "CatalogStudio",
                    content: "Export cannot be completed.",
                    type: "info"

                });
            }

        }).error(function (error) {
            options.error(error);
        });
    };





    $scope.GetAllDataSource = function (dataItem) {
        return {
            type: "json",
            transport: {
                read: function (options) {

                    dataFactory.GetAllattributesManager($scope.CATALOG_ID, dataItem.ATTRIBUTETYPE).success(function (response) {
                        if (response.length > 0 && response[0]["ATTRIBUTE_ID"].toString() == "1")
                            response[0]["ATTRIBUTE_NAME"] = $localStorage.CatalogItemNumber;


                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                },
                update: function (options) {
                   

                    if (options.data.STYLE_FORMAT !== null) {
                        options.data.STYLE_FORMAT = options.data.STYLE_FORMAT.text;
                    }
                    if (options.data.ATTRIBUTE_DATATYPE.toLowerCase().indexOf("date") > -1) {
                        var val = $("#date_txt").val();
                        if ((val.toLocaleLowerCase().indexOf("date") > -1) || (val.toLocaleLowerCase().indexOf("format"))) {
                            options.data.ATTRIBUTE_DATATYPE = "Date and Time";
                            options.data.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)";

                        } else {

                            options.data.ATTRIBUTE_DATATYPE = "Text";
                            options.data.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";

                        }

                    } else if (options.data.ATTRIBUTE_DATATYPE.toLowerCase().indexOf("hyp") > -1) {
                        var val = $("#date_txt").val();
                        if (val.toLocaleLowerCase().indexOf("hyp") > -1) {
                            options.data.ATTRIBUTE_DATATYPE = "Hyperlink";
                            options.data.ATTRIBUTE_DATAFORMAT = "^(https?://)|^(HTTPS?://)|^(Https?://)?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+\\.)*([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\\.[a-zA-Z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+/?)$";
                        } else {

                            options.data.ATTRIBUTE_DATATYPE = "Text";
                            options.data.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";

                        }

                    } else if (options.data.ATTRIBUTE_DATATYPE.toLowerCase().indexOf("num") > -1) {
                        if (options.data.DECIMAL === "0") {
                            options.data.ATTRIBUTE_DATAFORMAT = "(^-?\d\d*$)";
                        } else {
                            options.data.ATTRIBUTE_DATAFORMAT = "^-{0,1}?\\d*\\.{0,1}\\d{0,6}$";
                        }

                    }
                    if (options.data.USE_PICKLIST === true && options.data.PICKLIST_NAME === "") {

                        var uid = $(".k-edit-form-container").closest("[data-role=window]").data("uid");
                        var name = "usepicklist" + options.data.ATTRIBUTE_ID;
                        var model = $("#attrgrid").data("kendoGrid").dataSource.getByUid(uid);
                        model.set(name, false);
                        $scope.$apply(function () {
                            options.data.USE_PICKLIST = false;
                            model.USE_PICKLIST = false;
                        });
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please select a Picklist.',
                            type: "info"
                        });
                    } else {

                        dataFactory.UpdateAllattributes(options.data).success(function (response) {

                            if (response.trim() !== "") {

                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: '' + response + '.',
                                    type: "info"
                                });
                                response = "";
                            } else {
                                //$("#attrgrid").closest(".k-grid").data("kendoGrid").dataSource.read();
                                //$('#attrgroupgrids').data('kendoGrid').dataSource.read();
                                //$('#attrgroupgrids').data('kendoGrid').refresh();
                                options.success(response);
                            }
                        }).error(function (error) {
                            options.error(error);
                        });
                        $("#attrgrid").closest(".k-grid").data("kendoGrid").dataSource.read();
                        //  $('#attrgroupgrids').data('kendoGrid').dataSource.read();
                        //    $('#attrgroupgrids').data('kendoGrid').refresh();
                    }
                },
                destroy: function (options) {

                    dataFactory.Deleteattribute($scope.CATALOG_ID, options.data).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });

                },
                parameterMap: function (options) {
                    return kendo.stringify(options);
                }
            },
            schema: {
                model: {
                    id: "ATTRIBUTE_ID",
                    fields: {
                        ATTRIBUTE_NAME: { editable: true },
                        ATTRIBUTE_TYPE: { editable: true },
                        ATTRIBUTE_ID: { editable: true, nullable: true },
                        CREATE_BY_DEFAULT: { editable: true },
                        VALUE_REQUIRED: { editable: true },
                        STYLE_NAME: { editable: true },
                        STYLE_FORMAT: { editable: true },
                        DEFAULT_VALUE: { editable: true },
                        PUBLISH2PRINT: { editable: true },
                        PUBLISH2WEB: { editable: true },
                        PUBLISH2CDROM: { editable: true },
                        PUBLISH2ODP: { editable: true },
                        USE_PICKLIST: { editable: true },
                        ATTRIBUTE_DATATYPE: { editable: true },
                        ATTRIBUTE_DATAFORMAT: { editable: true },
                        ATTRIBUTE_DATARULE: { editable: true },
                        UOM: { editable: true },
                        IS_CALCULATED: { editable: true },
                        ATTRIBUTE_CALC_FORMULA: { editable: true },
                        PICKLIST_NAME: { editable: true },
                        AllowEdit: { editable: true }
                    }

                }
            },
            serverPaging: false,
            serverSorting: false,
            serverFiltering: false
        };
    };




    $scope.columnsattrs = [{ field: "ATTRIBUTE_ID", title: "ID", filterable: false, },
        { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px" },
        { field: "ATTRIBUTE_DATATYPE", title: "Data Type", width: "250px" },
        { field: "STYLE_NAME", title: "Style Name", width: "250px" },
        {
            headerTemplate: kendo.template("<div><center>Publish</center></div>"),
            columns: [
        { field: "PUBLISH2WEB", title: "Web", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2WEB ? "checked=checked" : "" #></input>', filterable: false },
        { field: "PUBLISH2PRINT", title: "Print", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2PRINT ? "checked=checked" : "" #></input>', filterable: false },
            { field: "PUBLISH2PDF", title: "PDF", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2PDF ? "checked=checked" : "" #></input>', filterable: false },
     { field: "PUBLISH2EXPORT", title: "Export", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2EXPORT ? "checked=checked" : "" #></input>', filterable: false },
        { field: "PUBLISH2PORTAL", title: "Portal", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2PORTAL ? "checked=checked" : "" #></input>', filterable: false }
            ]
        },
        {
            command: [{ name: 'edit', text: "", width: "10px", template: "<a class=\'k-grid-edit k-item girdicons\' ng-show=\'userRoleModifyAttributeManagement\'><div title=\'Edit\' class=\'glyphicon glyphicon-edit blue btn-xs-icon\'></div></a>" },
                { name: "delete", text: "", width: "10px", template: "<a class=\'k-item girdicons\' ng-click=\'DeleteAttribute($event,this)\' ng-show=\'userRoleDeleteAttributeManagement\'><div title=\'Delete\' class=\'glyphicon glyphicon-remove blue btn-xs-icon\'></div></a>" },
                //Move to Recycle Bin
                { template: '<a class=\'k-grid-detach k-item girdicons\' ng-show=\'userRoleDetachAttributeManagement\'  ng-click=\'RemoveAssociation($event,this)\'><div title=\'Detach\' class=\'glyphicon glyphicon-remove-circle blue btn-xs-icon\'></div></<a>' }],
            title: "Actions",
            template: "#if(true){#<a>data</a>#}#",
            width: "100px",
        }];
    $scope.columnsattrswithoutedit = [{ field: "ATTRIBUTE_ID", title: "ID", filterable: false, },
        { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px" },
        { field: "ATTRIBUTE_DATATYPE", title: "Data Type" },
        { field: "STYLE_NAME", title: "Style Name" },
        { field: "PUBLISH2WEB", title: "Web", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2WEB ? "checked=checked" : "" #></input>', filterable: false },
        { field: "PUBLISH2PRINT", title: "Print", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2PRINT ? "checked=checked" : "" #></input>', filterable: false },
    { field: "PUBLISH2PDF", title: "PDF", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2PDF ? "checked=checked" : "" #></input>', filterable: false },
     { field: "PUBLISH2EXPORT", title: "Export", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2EXPORT ? "checked=checked" : "" #></input>', filterable: false },
        { field: "PUBLISH2PORTAL", title: "Portal", width: "60px", template: '<input type="checkbox" disabled="disabled" #= PUBLISH2PORTAL ? "checked=checked" : "" #></input>', filterable: false }];

    $scope.detailGridOptions = function (dataItem) {

        return {
            dataSource: $scope.GetAllDataSource(dataItem),
            scrollable: true,
            sortable: true,
            selectable: "row",
            filterable: { mode: "row" },
            pageable: false,
            columns: $scope.columnsattrs,
            editable: {
                mode: "popup",
                template: kendo.template($("#popup_editor").html()), update: dataItem.ALLOWEDIT,
                confirmation: function (e) {
                    if (e.ATTRIBUTE_ID == 1 || e.ATTRIBUTE_ID == 3) {
                        $("#attrgrid").closest(".k-grid").data("kendoGrid").dataSource.read();
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'You cannot delete the record of ' + e.ATTRIBUTE_NAME + '.',
                            type: "info"
                        });
                        return true;
                        //"You can't delete the record of " + e.ATTRIBUTE_NAME + ".";
                    } else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: "Are you sure you want to delete " + e.ATTRIBUTE_NAME + " record?",
                            type: "info"
                        });
                        return true;
                        //return "Are you sure you want to delete " + e.ATTRIBUTE_NAME + " record?";
                    }
                }
            },

            dataBound: function (e) {

                // When coloumid is false grid id also deleted. 
                if ($scope.DisplayIdcolumns == false) {
                    this.hideColumn(0);
                }

                $scope.attributeName = e;
                if (this.dataSource.data().length === 0) {
                    var masterRow = this.element.closest("tr.k-detail-row").prev();
                    $("#attrgroupgrids").data("kendoGrid").collapseRow(masterRow);
                    masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                }
                $scope.editableAttr = [];
                angular.forEach(e.sender._data, function (data) {
                    if (data.ALLOWEDIT == false) {
                        $("#attrgrid").data("kendoGrid").tbody.find("tr[data-uid='" + data.uid + "']")[0].cells[7].children[0].className = "k-grid-edit k-item girdicons ng-hide"
                        //  $("#attrgrid").data("kendoGrid").tbody.find("tr[data-uid='" + data.uid + "']")[0].cells[7].children[0].outerHTML=$("#attrgrid").data("kendoGrid").tbody.find("tr[data-uid='" + data.uid + "']")[0].cells[7].children[0].outerHTML.replace('userRoleModifyAttributeManagement', 'true' );
                        //$scope.editableAttr.push(data.ATTRIBUTE_NAME);
                    }
                })
            }
        };
    };

    $scope.detailGridOptionsWithoutEditDelete = function (dataItem) {

        return {
            dataSource: $scope.GetAllDataSource(dataItem),
            scrollable: false,
            sortable: true,
            selectable: "row",
            pageable: false,
            columns: $scope.columnsattrswithoutedit,
            editable: {
                mode: "popup",
                template: kendo.template($("#popup_editor").html()), update: true,
                confirmation: function (e) {
                    if (e.ATTRIBUTE_ID == 1 || e.ATTRIBUTE_ID == 3) {
                        $("#attrgrid").closest(".k-grid").data("kendoGrid").dataSource.read();
                        return "You can't delete the record of " + e.ATTRIBUTE_NAME + ".";

                    } else {

                        return "Are you sure you want to delete " + e.ATTRIBUTE_NAME + " record?";
                    }
                }
            },
            dataBound: function () {

                if (this.dataSource.data().length === 0) {
                    var masterRow = this.element.closest("tr.k-detail-row").prev();
                    $("#attrgroupgrids").data("kendoGrid").collapseRow(masterRow);
                    masterRow.find("td.k-hierarchy-cell .k-icon").removeClass();
                }
            }
        };
    };





    $scope.GetAllAttributeGroupdataSource = new kendo.data.DataSource({
        pageSize: 5,
        type: "json",
        serverFiltering: true,
        autoBind: true,
        pageable: true,
        transport: {
            read: function (options) {
                dataFactory.GetAllAttributegroups($rootScope.selecetedCatalogId, $scope.familyAndProduct, options.data).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            },
            parameterMap: function (options, operation) {
                if (operation !== "read" && options.models) {
                    return { models: kendo.stringify(options.models) };
                }
                return kendo.stringify(options);

            }
        }, schema: {
            data: "Data",
            total: "Total",
            model: {
                id: "GROUP_ID",
                fields: {
                    GROUP_NAME: { editable: true },
                    IS_FAMILY: { editable: true },
                }
            }
        }
    });


    $scope.mainGridOptionsAttributeGrouping = {
        dataSource: $scope.GetAllAttributeGroupdataSource, type: "json",
        sortable: true, scrollable: true, pageable: { buttonCount: 5 }, autoBind: true, serverFiltering: true,

        columns: [{ field: "GROUP_NAME", title: "Group Name" },
        {
            // command: ["edit"], title: "Action", 
            template: "<a title='Action' ng-click='editAttributePackaging(this)'><div class=\'k-icon k-edit\'></div></a>"
        }
        ],
        dataBound: function (e) {

        }
    };

    $scope.groupTypeChange = function (e) {
        $scope.familyAndProduct = e.sender.text();
        $scope.GetAllAttributeGroupdataSource.read();
    }

    $scope.handleChange = function (dataItem) {
        if (dataItem != undefined) {
            $scope.Group.GROUP_ID = dataItem.GROUP_ID;
        }
    };
    $scope.AddnewattributeClickPrevent = function () {
        return false;
    }
    $scope.AddnewattributeClick = function () {
        $('#button_save').show();
        $("#attrmaingrid").hide();
        $("#addnewattribute").show();
        $("#editAttributeGrouping").hide();
        $(".familyProductattributeGrouping").hide();
    };

    $scope.familyproductAttributeGruoping = function () {
        $('#button_save').hide();
        // $scope.enableAttributeGrouping = true;
        $(".familyProductattributeGrouping").show();
        $("#attrmaingrid").hide();
        $("#addnewattribute").hide();
        $("#editAttributeGrouping").hide();
    };

    $scope.editAttributePackaging = function (e) {
        //$scope.enableAttributeGrouping = false;
        $scope.enableEditAttributeGrouping = true;
        $scope.SelectedGroupId = e.dataItem["GROUP_ID"];
        if (e.dataItem["IS_FAMILY"] == "Product") {
            $scope.GroupeType = 1;
        }
        else if (e.dataItem["IS_FAMILY"] == "Item") {
            $scope.GroupeType = 2;
        }

        $("#attrmaingrid").hide();
        $("#addnewattribute").hide();
        $(".familyProductattributeGrouping").hide();
        $("#editAttributeGrouping").show();
    };



    $scope.groupNameDataSource12 = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetdropdownGroupName($scope.SelectedCatalogId, $scope.familyAndProduct).success(function (response) {
                    options.success(response);
                    $('#selectedGroupName').data("kendoDropDownList").span[0].innerHTML = $("#packageName").val();

                    var temp = $("#packageName").val();

                    for (var i = 0 ; i < response.length ; i++) {
                        if (response[i]["GROUP_NAME"] == temp) {
                            $scope.SelectedGroupId = response[i]["GROUP_ID"].toString();
                        }
                    }
                    $("#packageName").attr('disabled', 'disabled');



                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.callFunc = function () {
        $scope.groupNameDataSource12.read();
    }


    $scope.saveAttributeGrouping = function (e) {
        var btnVal = $("#saveAttributePackage").html();
        var attrGroupName = $("#packageName").val();
        if (btnVal == "Update") {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Updated successfully.",
                type: "info"
            });
        }
        else if (e.GroupName != 'undefined' && e.GroupName != null && e.GroupName != "" && e.GroupName != '' && $scope.familyAndProduct != 'undefined' && $scope.familyAndProduct != null && $scope.familyAndProduct != "" && $scope.familyAndProduct != '' && $("#packageName").val() != "") {
            $timeout(function () {
                var grid = $("#familyProductGrid").data("kendoGrid");
                grid.dataSource.read();
                grid.dataSource.page(1);
                var grid_Selected = $("#selectedAttributesId").data("kendoGrid");
                grid_Selected.dataSource.read();
                grid_Selected.dataSource.page(1);
                if (grid_Selected.dataSource._total > 0 && selectedAttributes == true) {


                    $("#packageName").attr('disabled', 'disabled');
                    $("#deleteEnablebtn").removeAttr('disabled');


                    $scope.groupNameDataSource.read();
                    $('#selectedGroupName').data("kendoDropDownList").span[0].innerHTML = $("#packageName").val();


                    $scope.callFunc();

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Saved successfully.",
                        type: "info"
                    });

                }
                else if (selectedAttributes != true) {
                    $scope.groupNameDataSource.read();
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Please move available attributes to selected attributes by clicking (-->)arrow.",
                        type: "info"
                    });

                }
                else {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Please select at least one attribute.",
                        type: "info"
                    });
                }
            }, 200);
        }
        else if (e.GroupName == 'undefined' || e.GroupName == null || e.GroupName == "" || e.GroupName == '') {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: "Please enter group name.",
                type: "info"
            });
        }

    };

    $scope.familyGetAllattributesdataSource = new kendo.data.DataSource({
        type: "json",
        //pageable: true,
        //pageSize: 10,
        autoBind: false,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetAllAttributesForGrouping($rootScope.selecetedCatalogId, $scope.SelectedGroupId, $scope.familyAndProduct).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: { type: "boolean" },
                    CATALOG_ID: { editable: false },
                    FAMILY_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false }
                }
            }
        },
        group: {
            field: "ATTRIBUTE_TYPE", aggregates: [
               { field: "ATTRIBUTE_TYPE", aggregate: "count" }
            ]
        }

    });


    $scope.familyProductGridOptions = {
        dataSource: $scope.familyGetAllattributesdataSource,
        //pageable: { buttonCount: 5 },
        autoBind: false,
        filterable: { mode: "row" },
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>', filterable: false },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", filterable: true },
            {
                field: "ATTRIBUTE_TYPE",
                title: "Attribute Type",
                hidden: true,
                aggregates: ["count"],
                groupHeaderTemplate: groupHeaderName
            }]
    };

    $scope.updateSelection = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);

    };


    $scope.familyprodfamilyattrdataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        //pageable: true,
        autoBind: false,
       // pageSize: 10,
        sort: { field: "SORT_ORDER", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetAttributesByGroupId($rootScope.selecetedCatalogId, $scope.SelectedGroupId, $scope.familyAndProduct).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },
        schema: {
            model: {
                id: "ATTRIBUTE_ID",
                fields: {
                    ISAvailable: {
                        type: "boolean"
                    },
                    CATALOG_ID: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    SORT_ORDER: { editable: false }
                }
            }
        }

    });



    $scope.familyProductselectedGridOptions = {

        dataSource: $scope.familyprodfamilyattrdataSource,
        //pageable: { buttonCount: 5 },
        autoBind: false,
        selectable: "multiple",
        dataBound: onDataBound,
        columns: [
            { field: "ISAvailable", title: "Select", width: "60px", template: '<input type="checkbox" class="chkbxforprodfamily" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection1($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "200px" }
        ]
    };


    $("#selectedAttributesId").click(function () {
        var grid = $("#selectedAttributesId").data("kendoGrid");
        var selectedItem = grid.dataItem(grid.select());
        if (selectedItem != null) {
            $scope.selectatt = selectedItem.ATTRIBUTE_ID;
        }
    });

    var updownIndex = null;

    function onDataBound(e) {
        if (updownIndex != null) {
            var view = this.dataSource.view();
            for (var i = 0; i < view.length; i++) {
                if (updownIndex == i) {
                    var isChecked = this.tbody.find("tr[data-uid='" + view[i].uid + "']").find(".chkbxforprodfamily").attr("checked");
                    if (isChecked) {
                        this.tbody.find("tr[data-uid='" + view[i].uid + "']")
                        .addClass("k-state-selected");
                    }

                }
            }

            updownIndex = null;
        }
    }

    $scope.updateSelection1 = function (e, id) {
        id.dataItem.set("ISAvailable", e.currentTarget.checked);
    };
    var selectedAttributes = "";

    $scope.SavePublishAttributes = function (e) {
        if ($scope.SelectedGroupId != undefined && $scope.SelectedGroupId != '' && $scope.SelectedGroupId != "" && $scope.SelectedGroupId != "0") {
            dataFactory.SaveAttributesPackaging($scope.familyGetAllattributesdataSource._data, $rootScope.selecetedCatalogId, $scope.SelectedGroupId, $scope.familyAndProduct).success(function (response) {
                //$scope.familyGetAllattributesdataSource.read();
                var grid = $("#familyProductGrid").data("kendoGrid");
                grid.dataSource.read();
                grid.dataSource.page(1);
                //$scope.familyprodfamilyattrdataSource.read();
                var grid_Selected = $("#selectedAttributesId").data("kendoGrid");
                grid_Selected.dataSource.read();
                grid_Selected.dataSource.page(1);
            }).error(function (error) {
                options.error(error);
            });
        }
        else if (e.GroupName != 'undefined' && e.GroupName != null && e.GroupName != "" && e.GroupName != '' && $scope.familyAndProduct != 'undefined' && $scope.familyAndProduct != null && $scope.familyAndProduct != "" && $scope.familyAndProduct != '' && $("#packageName").val() != "") {
            var as = $scope.familyGetAllattributesdataSource._data;
            for (var i = 0; i < $scope.familyGetAllattributesdataSource._data.length; i++) {
                if (as[i].ISAvailable == true) {
                    selectedAttributes = true;
                }
            }
            if (selectedAttributes == true) {
                dataFactory.saveAttributeGrouping($scope.SelectedCatalogId, e.GroupName, $scope.familyAndProduct, $scope.familyGetAllattributesdataSource._data).success(function (response) {
                    if (response !== "" && response !== "Name already exists. Please enter a different name for the Pack~0") {
                        var result = response.split('~');
                        $scope.GetAllAttributeGroupdataSource.read();
                        $scope.SelectedGroupId = 0;
                        $("#deleteEnablebtn").attr('disabled', 'disabled');
                        $timeout(function () {
                            $scope.SelectedGroupId = result[1];
                            var grid = $("#familyProductGrid").data("kendoGrid");
                            grid.dataSource.read();
                            grid.dataSource.page(1);
                            var grid_Selected = $("#selectedAttributesId").data("kendoGrid");
                            grid_Selected.dataSource.read();
                            grid_Selected.dataSource.page(1);
                        }, 200);
                    }
                    else
                    {
                         $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + response + '.',
                                type: "info"
                            });
                    }
                }).error(function (error) {
                    options.error(error);
                });
            } else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one attribute.',
                    type: "error"
                });
            }

        }
        else if (e.GroupName == 'undefined' || e.GroupName != null || e.GroupName != "" || e.GroupName != '') {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter group name.',
                type: "error"
            });
        }

    };

    $scope.DeletePublishAttributes = function () {
        if ($scope.SelectedGroupId != undefined && $scope.SelectedGroupId != '' && $scope.SelectedGroupId != "") {
            dataFactory.DeleteAttributesPackaging($scope.familyprodfamilyattrdataSource._data, $rootScope.selecetedCatalogId, $scope.SelectedGroupId, $scope.familyAndProduct).success(function (response) {

                // $scope.familyGetAllattributesdataSource.read();
                var grid = $("#familyProductGrid").data("kendoGrid");
                grid.dataSource.read();
                grid.dataSource.page(1);

                //$scope.familyprodfamilyattrdataSource.read();
                var grid_Selected = $("#selectedAttributesId").data("kendoGrid");
                grid_Selected.dataSource.read();
                grid_Selected.dataSource.page(1);

            }).error(function (error) {
                options.error(error);
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Group name.',
                type: "error"
            });
        }

    };




    $scope.BtnFamilyMoveUpClick = function () {
        if ($scope.selectatt != 'undefined' && $scope.selectatt != null && $scope.SelectedGroupId != undefined && $scope.SelectedGroupId != '' && $scope.SelectedGroupId != "") {
            dataFactory.BtnAttributeMoveUpClick($scope.selectatt, $scope.SelectedGroupId).success(function (response) {
                //  $scope.familyGetAllattributesdataSource.read();
                var grid = $("#familyProductGrid").data("kendoGrid");
                grid.dataSource.read();
                grid.dataSource.page(1);
                // $scope.familyprodfamilyattrdataSource.read();
                var grid_Selected = $("#selectedAttributesId").data("kendoGrid");
                grid_Selected.dataSource.read();
                grid_Selected.dataSource.page(1);
            }).error(function (error) {
                options.error(error);
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                type: "error"
            });
        }
    };


    $scope.BtnFamilyMoveDownClick = function () {
        if ($scope.selectatt != 'undefined' && $scope.selectatt != null && $scope.SelectedGroupId != undefined && $scope.SelectedGroupId != '' && $scope.SelectedGroupId != "") {
            dataFactory.BtnAttributeMoveDownClick($scope.selectatt, $scope.SelectedGroupId).success(function (response) {
                // $scope.familyGetAllattributesdataSource.read();
                var grid = $("#familyProductGrid").data("kendoGrid");
                grid.dataSource.read();
                grid.dataSource.page(1);
                // $scope.familyprodfamilyattrdataSource.read();
                var grid_Selected = $("#selectedAttributesId").data("kendoGrid");
                grid_Selected.dataSource.read();
                grid_Selected.dataSource.page(1);
            }).error(function (error) {
                options.error(error);
            });
        }
        else {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please select at least one Attribute.',
                type: "error"
            });
        }
    };


    $scope.groupNameDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.GetdropdownGroupName($scope.SelectedCatalogId, $scope.familyAndProduct).success(function (response) {
                    options.success(response);
                    for (var i = 0; i < response.length ; i++) {
                        if (response[i].GROUP_NAME === $("#packageName").val()) {
                            $scope.SelectedGroupId = response[i].GROUP_ID;
                            $("#selectedGroupName").data("kendoDropDownList").select(i + 1);
                        }
                    }
                    //if (response != null || response != undefined) {
                    //    $scope.SelectedGroupId = response[0].GROUP_ID;
                    //}

                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });


    $scope.groupNameChange = function (e) {
        // alert(e.sender.text());
        if (e.sender.text() != "----- New -----") {

            $scope.SelectedGroupId = e.sender.value();
            $("#packageName").val(e.sender.text());
            $("#packageName").attr('disabled', 'disabled');
            $("#deleteEnablebtn").removeAttr('disabled');
            $("#saveAttributePackage").html("Update");
            // $("#saveAttributePackage").attr('disabled', 'disabled');
        }
        else {
            $scope.SelectedGroupId = 0;
            $("#packageName").val("");
            $("#deleteEnablebtn").attr('disabled', 'disabled');
            $("#packageName").removeAttr('disabled');
            $("#saveAttributePackage").html("Save");
            // $("#saveAttributePackage").removeAttr('disabled');
        }
        $timeout(function () {
            //  $scope.familyGetAllattributesdataSource.read();
            var grid = $("#familyProductGrid").data("kendoGrid");
            grid.dataSource.read();
            grid.dataSource.page(1);
            // $scope.familyprodfamilyattrdataSource.read();
            var grid_Selected = $("#selectedAttributesId").data("kendoGrid");
            grid_Selected.dataSource.read();
            grid_Selected.dataSource.page(1);
        }, 200);

    }


    $scope.Packagetypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: packageTypefulldata
        })
    };



    $scope.DeleteGroupdetails = function () {
        var packname;

        var e = $scope.SelectedGroupId;

        packname = $("#packageName").val();

        if (packname != null && packname != undefined && packname != '' && packname != "")
            packname = $("#packageName").val();
        else
            packname = "this";

        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Are you sure you want to delete '" + packname + "' Group?",
            type: "confirm",
            buttons: [{ value: "Yes" }, { value: "No" }],
            success: function (result) {
                var bool = false;
                if (result === "Yes") {
                    bool = true;
                }
                if (bool === true) {

                    if (e != 'undefined' && e != null && e != "" && e != '') {
                        dataFactory.DeleteGroupdetails($scope.SelectedCatalogId, e).success(function (response) {
                            if (response != null) {
                                $.msgBox({
                                    title: $localStorage.ProdcutTitle,
                                    content: response + " .",
                                    type: "info"
                                });
                                $scope.SelectedGroupId = 0;
                                $scope.groupNameDataSource.read();
                                //  $scope.familyGetAllattributesdataSource.read();
                                var grid = $("#familyProductGrid").data("kendoGrid");
                                grid.dataSource.read();
                                grid.dataSource.page(1);
                                //$scope.familyprodfamilyattrdataSource.read();
                                var grid_Selected = $("#selectedAttributesId").data("kendoGrid");
                                grid_Selected.dataSource.read();
                                grid_Selected.dataSource.page(1);
                                $("#saveAttributePackage").html("Save");
                                $("#packageName").val("");
                                $("#packageName").removeAttr('disabled');
                                $("#deleteEnablebtn").attr('disabled', 'disabled');
                                // $("#saveAttributePackage").removeAttr('disabled');
                            }

                        }).error(function (error) {
                            options.error(error);
                        });
                    }
                    else {
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Please select a Group name.',
                            type: "error"
                        });
                    }
                };
            }
        });
    };


    //$scope.DeleteGroupdetails = function (e) {

    //    if (e != 'undefined' && e != null && e != "" && e != '') {
    //        dataFactory.DeleteGroupdetails($scope.SelectedCatalogId, e).success(function (response) {
    //            $scope.SelectedGroupId = 0;
    //            $scope.groupNameDataSource.read();
    //            $scope.familyGetAllattributesdataSource.read();
    //            $scope.familyprodfamilyattrdataSource.read();
    //            $("#packageName").val("");
    //            $("#packageName").removeAttr('disabled');
    //            $("#deleteEnablebtn").attr('disabled', 'disabled');
    //        }).error(function (error) {
    //            options.error(error);
    //        });
    //    }
    //    else {
    //        $.msgBox({
    //            title: $localStorage.ProdcutTitle,
    //            content: 'Please select a Pack name.',
    //            type: "error"
    //        });
    //    }
    //}





    $scope.ChangeGroupType = function (e) {



        //$scope.GetAllAttributeGroupdataSource.read();
        $scope.familyAndProduct = e.sender.text();
        $scope.GroupeType = e.sender.text();
        $scope.SelectedGroupId = 0;
        $("#packageName").val("");
        $("#packageName").removeAttr('disabled');
        // $("#saveAttributePackage").removeAttr('disabled');
        $scope.groupNameDataSource.read();
        $timeout(function () {
            //  $scope.familyGetAllattributesdataSource.read();
            var grid = $("#familyProductGrid").data("kendoGrid");
            grid.dataSource.read();
            grid.dataSource.page(1);
            // $scope.familyprodfamilyattrdataSource.read();
            var grid_Selected = $("#selectedAttributesId").data("kendoGrid");
            grid_Selected.dataSource.read();
            grid_Selected.dataSource.page(1);
        }, 500);
    }

    // For Global Attribute Manager in Attributes Menu Start

    $scope.NextBtnAttributeAssociation = function () {

        //$scope.enableAttributeGrouping = false;
        $scope.associateAttrGrouping = true;
        //   $scope.SelectedGroupId = e.dataItem["GROUP_ID"];
        $("#attrmaingrid").hide();
        $("#addnewattribute").hide();
        $(".familyProductattributeGrouping").hide();
        $("#associateAttributeGrouping").show();
    };

    $scope.backToAttributeGrouping = function () {

        //  $scope.enableAttributeGrouping = true;
        $("#attrmaingrid").hide();
        $("#addnewattribute").hide();
        $("#associateAttributeGrouping").hide();
        $(".familyProductattributeGrouping").show();
    };


    $scope.AttributeDataSource = [
    { id: 14, DATA_TYPE: "Item Group" },
    { id: 15, DATA_TYPE: "Product Group" },
    { id: 1, DATA_TYPE: "Item Specifications" },
    { id: 3, DATA_TYPE: "Item Image / Attachment" },
    { id: 4, DATA_TYPE: "Item Price" },
    { id: 6, DATA_TYPE: "Item key" },
    { id: 11, DATA_TYPE: "Product Specification" },
    { id: 9, DATA_TYPE: "Product Image / Attachment" },
    { id: 12, DATA_TYPE: "Product Price" },
    { id: 13, DATA_TYPE: "Product Key" },
    { id: 7, DATA_TYPE: "Product Description" }
    ];

    $scope.AttrtypeChange = function (e) {
        if (e.sender.value() != "") {
            $scope.selectcatalogattr(e.sender.value());
            //if (e.sender.value() == 14) {
            //    $scope.isAttributePackage = true;
            //    $timeout(function () {
            //        $scope.packageTypefulldata.read();
            //    }, 2000);
            //}
            if (e.sender.value() == 14 || e.sender.value() == 15) {
                $scope.isAttributePackage = true;
                var packType;
                if (e.sender.value() == 14) {
                    $scope.GroupeTypeName = "Item";
                }
                if (e.sender.value() == 15) {
                    $scope.GroupeTypeName = "Product";
                }
                //$timeout(function () {
                $scope.groupNameDataSourceAttr.read();
                // $scope.unselectallDisplay = false;
                //$("#chkselectall").removeAttr('disabled');
                $scope.unselectallDisplay = true;
                $("#chkselectall").attr('disabled', 'disabled');
                $("#addGroup").text("Add Group");
                $("#removeGroup").text("Remove Group");

                //}, 200);
            }
            else if (document.getElementById('rempub').checked === false && document.getElementById('remattr').checked === false) {
                $("#chkid1").prop("checked", true);
                $scope.isAttributePackage = false;

                $scope.IsCheck = true;
                $("#checkboxpadding").removeClass("disableBox");
                $scope.GroupeTypeName = "";
                $scope.unselectallDisplay = false;
                $("#chkselectall").removeAttr('disabled');
                $("#addGroup").text("Add selected attributes");
                $("#removeGroup").text("Remove selected attributes");
            }
            else {
                $("#chkid1").prop("checked", false);
                $scope.isAttributePackage = false;
                $scope.IsCheck = false;
                $("#checkboxpadding").removeClass("disableBox");
                $scope.GroupeTypeName = "";
                $scope.unselectallDisplay = false;
                $("#chkselectall").removeAttr('disabled');
                $("#addGroup").text("Add selected attributes");
                $("#removeGroup").text("Remove selected attributes");
            }
        } else {
            $scope.entities = [];
            if (e.sender.text() == "--- Select Attribute Type ---") {
                $scope.isAttributePackage = false;
            } else {

            }
        }
    };


    $scope.selectedAttributetype = 0;
    $scope.selectcatalogattr = function (Attributeid) {
        $scope.selectedAttributetype = Attributeid;
        if (document.getElementById('rempub').checked === false && document.getElementById('remattr').checked === false) {
            $("#chkid1").prop("checked", true);
            $scope.IsCheck = true;
        }
        else {
            $("#chkid1").prop("checked", false);
            $scope.IsCheck = false;
        }

        if (Attributeid == "") {
            Attributeid = 0;
        }
        $http.get("../Wizard/GetAllattributes?attributeid=" + Attributeid + "&catalogId=" + $scope.SelectedCatalogId).
            then(function (attributes) {
                $scope.entities = attributes.data;
            });

    };

    $scope.Packagetypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: packageTypefulldata
        })
    };


    $scope.groupNameDataSourceAttr = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                //if ($scope.GroupeTypeAttr == 1) {
                //    $scope.GroupeTypeName = "Family";
                //} else if ($scope.GroupeTypeAttr == 2) {
                //    $scope.GroupeTypeName = "Product";
                //}
                dataFactory.GetdropdownGroupName($scope.SelectedCatalogId, $scope.GroupeTypeName).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    $scope.unselectallDisplay = false;
    $("#chkselectall").removeAttr('disabled');
    $("#addGroup").text("Add selected attributes");
    $("#removeGroup").text("Remove selected attributes");

    $scope.groupNameChangeAttr = function (e) {
        if (e.sender.text() == "--- Select Group ---") {
            $scope.SelectedGroupIdAttr = 0;
        } else {
            $scope.SelectedGroupIdAttr = e.sender.value();
        }
        $scope.selectedAttributetype = 14;
        if (document.getElementById('rempub').checked === false && document.getElementById('remattr').checked === false) {
            $("#chkid1").prop("checked", true);
            $scope.IsCheck = true;
        }
        else {
            $("#chkid1").prop("checked", false);
            $scope.IsCheck = false;
        }
        $http.get("../Wizard/GetallAttributesByGroupId?groupId=" + $scope.SelectedGroupIdAttr + "&catalogId=" + $scope.SelectedCatalogId + "&packageType=" + $scope.GroupeTypeName).then(function (attributes) {
            if (attributes != null && attributes != undefined && attributes.data != null && attributes.data != undefined && attributes.data.length > 0) {
                $scope.entities = attributes.data;
                $timeout(function () {
                    $(".chk").each(function () {
                        $(this).prop("checked", true);
                    });
                    $("#checkboxpadding").addClass("disableBox");
                    $scope.unselectallDisplay = true;
                    $("#chkselectall").attr('disabled', 'disabled');
                    $("#addGroup").text("Add Group");
                    $("#removeGroup").text("Remove Group");
                }, 200);
            }
            else
                $scope.entities = [];
        });
    };


    $scope.ChangeGroupTypeAttr = function (e) {
        $scope.SelectedGroupIdAttr = 0;
        $scope.GroupeTypeAttr = e.sender.value();
        $http.get("../Wizard/GetallAttributesByGroupId?groupId=" + $scope.SelectedGroupIdAttr + "&catalogId=" + $scope.SelectedCatalogId + "&packageType=" + $scope.GroupeTypeAttr).then(function (attributes) {
            if (attributes.data.length > 0) {
                $scope.entities = attributes.data;
                $scope.groupNameDataSourceAttr.read();
            }
            else {
                $scope.entities = [];
                $scope.groupNameDataSourceAttr.read();
            }
        });
    }


    $scope.selectall = function (name) {

        $(".chk").each(function () {
            $(this).prop("checked", true);
        });
        if (document.getElementById('rempub').checked === false && document.getElementById('remattr').checked === false) {
            $("#chkid1").prop("checked", true);

            $scope.IsCheck = true;

        }
        else {
            $("#chkid1").prop("checked", false);

            $scope.IsCheck = false;

        }
    };


    $scope.unselectall = function (name) {

        $(".chk").each(function () {

            $(this).prop("checked", false);
        });
        if (document.getElementById('rempub').checked === false && document.getElementById('remattr').checked === false) {
            $("#chkid1").prop("checked", true);

            $scope.IsCheck = true;

        }
        else {
            $("#chkid1").prop("checked", false);

            $scope.IsCheck = false;

        }
    };

    $scope.check = function (selct) {
        if (document.getElementById('selattr').checked === true) {
            $("#remattr").prop("checked", false);
            document.getElementById("remattr").disabled = true;
            $("#rempub").prop("checked", false);
            document.getElementById("rempub").disabled = true;
            $("#addpub").prop("checked", false);
            document.getElementById("addpub").disabled = false;
        } else {
            document.getElementById("remattr").disabled = false;
            document.getElementById("rempub").disabled = false;
        }
        $("#chkid1").prop("checked", true);

        $scope.IsCheck = true;

    };

    $scope.remattrcheck = function (selct) {

        if (document.getElementById('remattr').checked === true) {
            $("#rempub").prop("checked", false);
            document.getElementById("rempub").disabled = true;
            $("#addpub").prop("checked", false);
            document.getElementById("addpub").disabled = true;
            $("#selattr").prop("checked", false);
        } else {
            document.getElementById("rempub").disabled = false;
            document.getElementById("addpub").disabled = false;
        }
        $("#chkid1").prop("checked", false);
    };

    $scope.rempubcheck = function (select) {

        if (document.getElementById('rempub').checked === true) {
            $("#addpub").prop("checked", false);
        }
        $("#chkid1").prop("checked", false);
    };

    $scope.addpubcheck = function (select) {
        if (document.getElementById('addpub').checked === true) {
            $("#rempub").prop("checked", false);
        }
        $("#chkid1").prop("checked", true);

        $scope.IsCheck = true;

    };

    $scope.selectedcatalog1 = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {

                dataFactory.getCategories($scope.SelectedCatalogId, options.data.id).success(function (response) {
                    options.success(response);
                    //$("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
                    //$("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
                }).error(function (response) {
                    options.success(response);
                });
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        checkboxes: {
            checkChildren: true,
        }

    });

    $scope.subproductcheckeditems = "";
    $scope.subproductdatasource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {


                if ($localStorage.EnableSubProductLocal == true) {
                    dataFactory.getCategoriessubProducts($scope.SelectedCatalogId, options.data.id).success(function (response) {
                        options.success(response);
                    }).error(function (response) {
                        options.success(response);
                    });
                }
                else {
                    $('#subproductfamily').hide();
                }
            },
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
        checkboxes: {
            checkChildren: true,
        }

    });

    $scope.subproducttreeOptionsCatalog = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            $scope.subproductcheckeditems = '';
            if (!e.node) {
                $scope.subproductschanges($scope.subproductdatasource);
            }
        }
    };



    $scope.treeOptionsCatalog = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            //console.log(e);
            $scope.checkeditems = '';
            if (!e.node) {
                $scope.selectedtab = "AttributeAssociation";
                $scope.attachChangeEvent();
            }
        }
    };

    $scope.treeOptionsCatalog_Sub = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            //console.log(e);
            $scope.checkeditems = '';
            if (!e.node) {
                $scope.selectedtab = "AttributeAssociationsub";
                $scope.attachChangeEvent();
            }
        }
    };


    $scope.subproducttreeOptionsCatalog = {
        checkboxes: {
            checkChildren: true,
        },
        loadOnDemand: false,
        dataBound: function (e) {
            $scope.subproductcheckeditems = '';
            if (!e.node) {
                $scope.subproductschanges($scope.subproductdatasource);
            }
        }
    };


    $scope.subproductschanges = function (dataSource) {
        $scope.subproductcheckeditems = '';
        // var dataSource = $scope.subproductdatasource;
        dataSource.bind("change", function (e) {
            var selectedNodes = 0;
            var checkedNodes = [];
            $scope.subproductcheckedNodeIds(dataSource.view(), checkedNodes);
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }

            $scope.subproductcheckeditems = checkedNodes.join(",");
        });

    };


    $scope.subproductcheckedNodeIds = function (nodes, checkedNodes) {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].checked) {
                checkedNodes.push(nodes[i].CATEGORY_ID);
            }
            if (nodes[i].hasChildren) {
                $scope.subproductcheckedNodeIds(nodes[i].children.view(), checkedNodes);
            }
        }
    };



    $scope.attachChangeEvent = function () {
        if ($scope.selectedtab != "" && $scope.selectedtab == "AttributeAssociation") {
            var dataSource = $scope.selectedcatalog1;

        }
        else if ($scope.selectedtab != "" && $scope.selectedtab == "ColumnSorter") {
            var dataSource = $scope.selectedcatalog1_sort;
        }
        else if ($scope.selectedtab != "" && $scope.selectedtab == "SelectedFamilyValue") {
            var dataSource = $scope.selectedcatalog_ForSelectedFamily;
        }
        else {
            var dataSource = $scope.selectedCalculatedcatalog1;
        }

        $scope.checkeditems = '';
        dataSource.bind("change", function (e) {
            var selectedNodes = 0;
            var checkedNodes = [];
            $scope.checkedNodeIds(dataSource.view(), checkedNodes);
            for (var i = 0; i < checkedNodes.length; i++) {
                var nd = checkedNodes[i];
                if (nd.checked) {
                    selectedNodes++;
                }
            }
            $scope.checkeditems = checkedNodes.join(",");
        });
    };


    $scope.CheckOrUncheckBox1 = false;
    $scope.CheckOrUncheck1 = function () {

        if (chkEnabled1.checked == true) {
            $scope.CheckOrUncheckBox1 = true;
        } else {
            $scope.CheckOrUncheckBox1 = false;
        }
        if ($scope.CheckOrUncheckBox1) {
            $('#treeviewKendo1 input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox1 = true;
        } else {
            $('#treeviewKendo1 input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox1 = false;
        }
    };


    $scope.ExpandOrCollapseBox1 = false;
    $scope.ExpandOrCollapse1 = function () {

        if (chkEnabled2.checked == true) {
            $scope.ExpandOrCollapseBox1 = true;
        } else {
            $scope.ExpandOrCollapseBox1 = false;
        }
        if ($scope.ExpandOrCollapseBox1) {
            $("#treeviewKendo1").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox1 = true;
        } else {
            $("#treeviewKendo1").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox1 = false;
        }
    };

    $scope.runscript = function (totest) {
        var flg = 0;
        var cnt = 0;
        var objpid = new Array();
        var objstrid = new String;
        $(".chk").each(function () {
            if ($(this).is(':checked')) {
                objstrid = objstrid + $(this).val() + ',';
                cnt = cnt + 1;
                flg = 1;
            }
        });
        // $scope.chkattrlist = objstrid;
        if ($scope.GroupeTypeName == "Product")
        {
            $scope.GroupeTypeName = "Family"
        }
        if ($scope.GroupeTypeName == "Item") {
            $scope.GroupeTypeName = "Product"
        }
        if ($scope.GroupeTypeName == "Family" || $scope.GroupeTypeName == "Product") {
            $scope.chkattrlist = '~' + $scope.SelectedGroupIdAttr;
            objstrid = '~' + $scope.SelectedGroupIdAttr;
        }
        else {
            $scope.chkattrlist = objstrid;
        }
        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        $scope.attrfamilyIds = objstrcatid;
        var objoption = new String;
        $(".chkoption").each(function () {
            if ($(this).is(':checked')) {
                objoption = objoption + ',' + $(this).val();
            }
        });
        objoption = objoption.substring(1);
        $scope.attroption = objoption;
        var sort = 0;
        if ($scope.chkattrlist != '' && $scope.chkattrlist != undefined && $scope.attroption != '' && $scope.checkeditems != '') {
            //$http.get("../Wizard/globalattribute?Attrid=" + objstrid + "&&familyid=" + objstrcatid + "&&Catalogid=" + $scope.seelectCatalogID + "&&Attributetype=" + $scope.selectedAttributetype + "&&option=" + objoption + "&&sort=" + sort)
            //    .success(function (response) {
            //        if (response != null) {
            //            alert(response);
            //        };
            //    });

            dataFactory.globalattribute(objstrid, $scope.checkeditems, $rootScope.selecetedCatalogId, $scope.selectedAttributetype, objoption, sort).success(function (response) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Saved successfully.',
                    type: "info"
                });

            }).error(function (error) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Save action not completed.',
                    type: "error"
                });
            });
        }
        else {
            if ($scope.selectedAttributetype == '' || $scope.selectedAttributetype == '--- Select Attribute type ---') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Attribute Type.',
                    type: "error"
                });
            }
            else if ($scope.chkattrlist == '' || $scope.chkattrlist == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one attribute.',
                    type: "error"
                });
            }

            else if ($scope.attroption == '' || $scope.attroption == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Option and continue.',
                    type: "error"
                });
            }
            else if ($scope.attrfamilyIds == '' || $scope.attrfamilyIds == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Item Product.',
                    type: "error"
                });
            }

        }
    };

    $scope.pagereload = function (name) {

        location.reload();
    };

    $scope.CheckOrUncheckBox1sub = false;
    $scope.CheckOrUncheck1sub = function () {

        if (chkEnabledsub1.checked == true) {
            $scope.CheckOrUncheckBox1sub = true;
        } else {
            $scope.CheckOrUncheckBox1sub = false;
        }
        if ($scope.CheckOrUncheckBox1sub) {
            $('#treeviewKendo1sub input[type="checkbox"]').prop('checked', true).trigger("change");
            $scope.CheckOrUncheckBox1sub = true;
        } else {
            $('#treeviewKendo1sub input[type="checkbox"]').prop('checked', false).trigger("change");
            $scope.CheckOrUncheckBox1sub = false;
        }
    };

    $scope.ExpandOrCollapseBox1sub = false;
    $scope.ExpandOrCollapse1sub = function () {

        if (chkEnabledsub2.checked == true) {
            $scope.ExpandOrCollapseBox1sub = true;
        } else {
            $scope.ExpandOrCollapseBox1sub = false;
        }
        if ($scope.ExpandOrCollapseBox1sub) {
            $("#treeviewKendo1sub").data("kendoTreeView").expand(".k-item");
            $scope.ExpandOrCollapseBox1sub = true;
        } else {
            $("#treeviewKendo1sub").data("kendoTreeView").collapse(".k-item");
            $scope.ExpandOrCollapseBox1sub = false;
        }
    };

    $scope.runscriptsubproducts = function (totest1) {

        var flg = 0;
        var cnt = 0;
        var objpid = new Array();
        var objstrid = new String;
        $(".chk").each(function () {
            if ($(this).is(':checked')) {
                objstrid = objstrid + $(this).val() + ',';
                cnt = cnt + 1;
                flg = 1;
            }
        });
        //  $scope.chkattrlist = objstrid;
        if ($scope.GroupeTypeName == "Family" || $scope.GroupeTypeName == "Product") {
            $scope.chkattrlist = '~' + $scope.SelectedGroupIdAttr;
            objstrid = '~' + $scope.SelectedGroupIdAttr;
        }
        else {
            $scope.chkattrlist = objstrid;
        }
        var objstrcatid = new String;
        var items = $scope.checkeditems;
        var temp = new Array();
        temp = items.split(',');
        for (var i = 0; i < temp.length; i++) {
            if (temp[i].indexOf('~') == 0) {
                objstrcatid = objstrcatid + ',' + temp[i].replace('~', '');
            }
        }
        objstrcatid = objstrcatid.substring(1);
        $scope.attrfamilyIds = objstrcatid;
        var objoption = new String;
        $(".chkoption").each(function () {
            if ($(this).is(':checked')) {
                objoption = objoption + ',' + $(this).val();
            }
        });
        objoption = objoption.substring(1);
        $scope.attroption = objoption;
        var sort = 0;

        if ($scope.chkattrlist != '' && $scope.chkattrlist != undefined && $scope.attroption != '' && $scope.subproductcheckeditems != '') {

            if ($scope.selectedAttributetype != 7 && $scope.selectedAttributetype != 9 && $scope.selectedAttributetype != 11 && $scope.selectedAttributetype != 13) {

                dataFactory.globalattributesub(objstrid, $scope.subproductcheckeditems, $rootScope.selecetedCatalogId, $scope.selectedAttributetype, objoption, sort).success(function (response) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Saved successfully.",
                        type: "info",
                        //autoClose: true
                    });

                }).error(function (error) {
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: "Save action not completed.",
                        type: "error",
                        ///autoClose: true
                    });
                });
            }
            else {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: "Product Level attributes cannot be selected.",
                    type: "info",
                    // autoClose:true
                });
            }

        }
        else {
            if ($scope.selectedAttributetype == '' || $scope.selectedAttributetype == '--- Select Attribute type ---') {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Attribute Type.',
                    type: "error"
                });
            }
            else if ($scope.chkattrlist == '' || $scope.chkattrlist == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select at least one attribute.',
                    type: "error"
                });
            }
            else if ($scope.attroption == '' || $scope.attroption == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select an Option and continue.',
                    type: "error"
                });
            }
            else if ($scope.attrfamilyIds == '' || $scope.attrfamilyIds == undefined) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Please select a Item Product.',
                    type: "error"
                });
            }

        }
    };

    // For Global Attribute Manager in Attributes Menu end

    $scope.attributesExportFormatType = ".XLS";

    $scope.exportAttributesListClick = function (attributesExportFormatType) {
        dataFactory.ExportAttributesList($rootScope.selecetedCatalogId, $rootScope.DisplayIdcolumns, attributesExportFormatType).success(function (response) {
            window.open("DownloadFullExport.ashx?Path=" + response);
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.TempAttributeDataSource = new kendo.data.HierarchicalDataSource({
        type: "json",
        loadOnDemand: false,
        transport: {
            read: function (options) {
                options.success($scope.TempAttributes);
            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        },
    });


    $scope.init();

}]);