﻿
LSApp.controller('calculateattributeController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$rootScope', '$localStorage', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $rootScope, $localStorage) {

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;


    $scope.GridIscalAttrlist = {
        dataSource: $scope.GetGridIscalAttrlistdataSource,
        pageable: { buttonCount: 5 },
        columns: [
            //{ field: "ISAvailable", title: "Select", template: '<input type="checkbox" class="chkbxforallattributes" #= ISAvailable ? "checked=checked" : "" #  ng-click="updateSelection($event, this)"></input>' },
            { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "250px" },
            {
                //field: "ATTRIBUTE_TYPE",
                //title: "Attribute Type",
                hidden: true,
                aggregates: ["count"],
                //groupHeaderTemplate: groupHeaderName
            }]
    };
    
    $scope.GetGridIscalAttrlistdataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, pageable: true, pageSize: 10,
        sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetCalculatedattrList($rootScope.CATALOG_ID, $rootScope.Value_Attr_Type.datatype, $rootScope.Value_Attr_Type.ATTRIBUTE_DATATYPE).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {

                id: "ATTRIBUTE_ID",
                fields: {
                    ATTRIBUTE_NAME: { editable: false }
                }
            }
        }
    });



    $scope.Iscalculatedattr_Save = function (val) {
        dataFactory.Iscalculatedattr_Save_Click($rootScope.Value_Attr_Attrname, $rootScope.Value_Attr_id).success(function (response) {
        }).error(function (error) {
            options.error(error);
        });
    };

    $scope.onSelection = function (kendoEvent) {
        var grid = kendoEvent.sender;
        var selectedData = grid.dataItem(grid.select());

        var selectedData1 = selectedData.ATTRIBUTE_NAME;
        var selectedData2 = $rootScope.Value_Attr_Attrname;
        var res = selectedData2.concat(selectedData1);
        $scope.Value_Attr_Attrname = res;
        //$scope.getAttributeName(selectedData.id);
        console.log(selectedData.id);

    };
    $scope.Adding = function () {
        var addingVal = $rootScope.Value_Attr_Attrname;
        var res = addingVal.concat("+");
        $scope.Value_Attr_Attrname = res;
    };
    $scope.Minus = function () {
        var minusVal = $rootScope.Value_Attr_Attrname;
        var res = minusVal.concat("-");
        $scope.Value_Attr_Attrname = res;
    };
    $scope.Muti = function () {
        var mutiVal = $rootScope.Value_Attr_Attrname;
        var res = mutiVal.concat("*");
        $scope.Value_Attr_Attrname = res;
    };
    $scope.Divide = function () {
        var divVal = $rootScope.Value_Attr_Attrname;
        var res = divVal.concat("/");
        $scope.Value_Attr_Attrname = res;
    };

    $scope.Clear = function () {
        $rootScope.Value_Attr_Attrname = "";
        //$scope.Value_Attr_Type = "";
        //$scope.Value_Attr_id = "";
    };

    $scope.init = function () {
        if ($localStorage.getCatalogID === undefined) {
            $rootScope.CATALOG_ID = 0;
        }
        else {
            $rootScope.CATALOG_ID = $localStorage.getCatalogID;
        }
    };

    $scope.init();
}]);
