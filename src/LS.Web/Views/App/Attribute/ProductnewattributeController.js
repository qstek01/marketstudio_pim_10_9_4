﻿
LSApp.controller('ProductnewattributeController', ['$scope', '$window', '$location', '$routeParams', 'dataFactory', '$q', '$timeout', '$route', '$http', '$localStorage', '$rootScope', function ($scope, $window, $location, $routeParams, dataFactory, $q, $timeout, $route, $http, $localStorage,$rootScope) {

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;

    //start


    $scope.IsDisabled = true;

    $scope.EnableDisable = function () {
        //If TextBox has value, the Button will be enabled else vice versa.
        $scope.IsDisabled = $scope.Attribute.ATTRIBUTE_NAME.length == 0;
    }

    // End





    $scope.Attribute = {
        ATTRIBUTE_TYPE: 1, ATTRIBUTE_NAME: "", STYLE_NAME: "", ATTRIBUTE_DATATYPE: "Text", ATTRIBUTE_SIZE: "", ATTRIBUTE_DECIMAL: 0, ATTRIBUTE_DATAFORMAT: "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$",
        Prefix: "",
        Suffix: "",
        Condition: "",
        CustomValue: "",
        ApplyTo: "All",
        ApplyForNumericOnly: true, PUBLISH2PRINT: true, PUBLISH2WEB: true, PUBLISH2PDF: true, PUBLISH2EXPORT: true, PUBLISH2PORTAL: true
    };
    $scope.init = function () {
        $("#attributedatainteger1").hide();
        $("#attributedatatext1").show();
        $("#attributedatadate1").hide();
        $("#attributedatahyperlink1").hide();
        $("#attributedatasize1").show();
        $("#attributedataintegersize1").hide();
        $("#attributedataemptysize1").hide();
        $("#txtsize1").show();
        $("#size1").show();
        if ($localStorage.getCatalogID === undefined) {
            $scope.CATALOG_ID = 0;
        }
        else {
            $scope.CATALOG_ID = $localStorage.getCatalogID;
        }
    };
    $scope.init();
   
    $scope.NewAttrSave = function (attribute) {
        
        var checkSystemAttributeName = (attribute.ATTRIBUTE_NAME.trim()).toUpperCase();
        if (attribute.ATTRIBUTE_NAME.trim() == "") {          
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a valid Name.',
                type: "error"
            });
        }
        if (checkSystemAttributeName.match("__") || checkSystemAttributeName.match("-") || checkSystemAttributeName.match("&") || checkSystemAttributeName.match("%") || checkSystemAttributeName.match("[*+]")) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Attribute name should not contain special characters like (* & % + __ ).',
                type: "error"
            });
        }
        else if (checkSystemAttributeName == "FAMILY_ID" || checkSystemAttributeName == "CATEGORY_ID" || checkSystemAttributeName == "CATALOG_ID" || checkSystemAttributeName == "PRODUCT_ID" ||
            checkSystemAttributeName == "FAMILYID" || checkSystemAttributeName == "CATEGORYID" || checkSystemAttributeName == "CATALOGID" || checkSystemAttributeName == "PRODUCTID" || checkSystemAttributeName == "ACTION" ||
            checkSystemAttributeName.contains("SUBCATID") || checkSystemAttributeName.contains("SUBCATNAME") || checkSystemAttributeName == "FAMILY_NAME" || checkSystemAttributeName == "CATEGORY_NAME" || checkSystemAttributeName == "CATALOG_NAME" || checkSystemAttributeName == "PRODUCT_NAME" ||
            checkSystemAttributeName == "FAMILYNAME" || checkSystemAttributeName == "CATEGORYNAME" || checkSystemAttributeName == "CATALOGNAME" || checkSystemAttributeName == "PRODUCTNAME" || checkSystemAttributeName.contains("'")) {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a valid Name.',
                type: "error"
            });
            return;

        }
        else {
            
            dataFactory.savenewattribute(attribute, $scope.CATALOG_ID).success(function (response) {                           
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + response + '.',
                    type: "info"
                });
                $scope.Attribute.ATTRIBUTE_NAME = "";
                $scope.Attribute.CAPTION = "";
                $scope.Attribute.STYLE_NAME = "";
                $scope.Attribute.ATTRIBUTE_SIZE = "";
                $scope.Attribute.ATTRIBUTE_DECIMAL = 0;
                $scope.Attribute.Prefix = "";
                $scope.Attribute.Suffix = "";
                $scope.Attribute.Condition = "";
                $scope.Attribute.CustomValue = "";
                $scope.Attribute.ApplyTo = "All";
                $scope.Attribute.ApplyForNumericOnly = true;
                $scope.Attribute.PUBLISH2PRINT = true;
                $scope.Attribute.PUBLISH2WEB = true;
                $scope.Attribute.PUBLISH2PDF = true;
                $scope.Attribute.PUBLISH2EXPORT = true;
                $scope.Attribute.PUBLISH2PORTAL = true;
            }).error(function (error) {
                options.error(error);
            });
        }
        $("#customvaluenew1").val("");
        $("#suffixnew1").val("");
        $("#prefixnew1").val("");
        $("#attrapplytofirstnew1").val("");
        $("#attrapplytoallnew1").val("");
        $("#txt_attributedatasize1").val("");
        $("#txt_size1").val("");
        $("#txt_stylename1").val("");
       // $("#txt_attr_name1").val("");
        $scope.GetAllCatalogattributesdataSource.read();
        $scope.GetAllProductAttributesNew.read();
        //$("#attrmaingrid").show();
        //$("#addnewattribute").hide();
    };

    var attributefulldata = [{
        attributetype: 1,
        attributetypename: 'Item Specifications'
    }, {
        attributetype: 3,
        attributetypename: 'Item Image / Attachment'
    }, {
        attributetype: 4,
        attributetypename: 'Item Price'
    },
        {
            attributetype: 6,
            attributetypename: 'Item Key'
        }, {
            attributetype: 7,
            attributetypename: 'Product Description'
        }, {
            attributetype: 9,
            attributetypename: 'Product Image / Attachment'
        },
        {
            attributetype: 11,
            attributetypename: 'Product Specifications'
        }, {
            attributetype: 12,
            attributetypename: 'Product Price'
        }, {
            attributetype: 13,
            attributetypename: 'Product Key'
        }];

    var attributefamilydata = [{ attributetypename: "Product Description", attributetype: "7" }, { attributetypename: "Product Image / Attachment", attributetype: "9" }, { attributetypename: "Product Specifications", attributetype: "11" }, { attributetypename: "Product Price", attributetype: "12" }, { attributetypename: "Product Key", attributetype: "13" }];
    var attributeproductdata = [{ attributetypename: "Item Specifications", attributetype: "1" }, { attributetypename: "Item Image / Attachment", attributetype: "3" }, { attributetypename: "Item Price", attributetype: "4" }, { attributetypename: "Item Key", attributetype: "6" }];
    $scope.attributeChange = function (e) {
        if (e.sender.text() === "Item Price" || e.sender.text() === "Product Price") {
            $("#attributedatainteger1").show();
            $("#attributedatatext1").hide();
            $("#attributedatadate1").hide();
            $("#attributedatahyperlink1").hide();
            $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(^-?\d\d*$)";
            $scope.Attribute.ATTRIBUTE_DATATYPE = "Number";
            $scope.Attribute.ATTRIBUTE_SIZE = "";
            $("#attributedatasize1").hide();
            $("#attributedataintegersize1").show();
            $("#attributedataemptysize1").hide();
            $("#txtsize1").show();
            $("#size1").show();
            
            $("#Datatypeddl1").hide();
            $("#Datatypetxt1").show();

            $("#datatxt11").val('Numbers(1,1.0,100)');
            $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(^-?\d\d*$)";
            $scope.Attribute.ATTRIBUTE_DATATYPE = "Number";
            $scope.Attribute.ATTRIBUTE_SIZE = "";
            $("#Dataformatddl1").hide();
            $("#Dataformattxt1").show();
            $("#datatxt1").val('Integer');
            $("#attrdatarule1").show();
            $("#applyto1").show();
            $("#prefixsuffix1").show();
            $("#condition1").show();
            $("#applycustomtext1").show();

        } else if (e.sender.text() === "Item Image / Attachment" || e.sender.text() === "Product Image / Attachment" || e.sender.text() === "Product Description") {
            $("#Datatypeddl1").hide();
            $("#Datatypetxt1").show();
            $("#datatxt11").val('Text');
            $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
            $scope.Attribute.ATTRIBUTE_DATATYPE = "Text";
            $scope.Attribute.ATTRIBUTE_SIZE = "";
            $("#Dataformatddl1").hide();
            $("#Dataformattxt1").show();
            $("#datatxt1").val('All Characters');
            $("#attrdatarule1").hide();
            $("#applyto1").hide();
            $("#prefixsuffix1").hide();
            $("#condition1").hide();
            $("#applycustomtext1").hide();
            $("#txtsize1").hide();
            $("#size1").hide();
        } else {
            $("#Dataformatddl1").show();
            $("#Datatypeddl1").show();
            $("#attributedatainteger1").hide();
            $("#attributedatatext1").show();
            $("#attributedatadate1").hide();
            $("#attributedatahyperlink1").hide();
            $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
            $scope.Attribute.ATTRIBUTE_DATATYPE = "Text";
            $scope.Attribute.ATTRIBUTE_SIZE = "";
            $("#attributedatasize1").show();
            $("#attributedataintegersize1").hide();
            $("#attributedataemptysize1").hide();
            $("#txtsize1").show();
            $("#size1").show();
            
            $("#Datatypetxt1").hide();
            $("#Dataformattxt1").hide();
            $("#attrdatarule1").show();
            $("#applyto1").show();
            $("#prefixsuffix1").show();
            $("#condition1").show();
            $("#applycustomtext1").show();
        }
    };
    $scope.attributetypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: attributefulldata
        }),
        change: function (e) {
            $scope.attributeChange(e);
        }
    };
    $scope.attributefamilytypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: attributefamilydata
        }),
        change: function (e) {
            $scope.attributeChange(e);
        }
    };
    $scope.attributeproducttypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: attributeproductdata
        }),
        change: function (e) {
            $scope.attributeChange(e);
        }
    };
    $scope.attributedatetypedatasource = {
        dataSource: new kendo.data.DataSource({
            data: [{
                attributedatatype: "Text",
                attributedatatypename: 'Text'
            }, {
                attributedatatype: 'Number',
                attributedatatypename: 'Numbers(1,1.0,100)'
            }, {
                attributedatatype: 'Date and Time',
                attributedatatypename: 'Date and Time'
            },
            {
                attributedatatype: 'Hyperlink',
                attributedatatypename: 'HyperLink'
            }]
        }),
        change: function (e) {
            if (e.sender.text() === "Text") {
                $("#attributedatatext1").show();
                $("#attributedatainteger1").hide();
                $("#attributedatadate1").hide();
                $("#attributedatahyperlink1").hide();
                $("#attributedatasize1").show();
                $("#attributedataintegersize1").hide();
                $("#attributedataemptysize1").hide();
                $("#txtsize1").show();
                $("#size1").show();
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
                    $scope.Attribute.ATTRIBUTE_DATATYPE = "Text";
                });
                $("#Datatypetxt1").hide();
                $("#Dataformattxt1").hide();
            } else if (e.sender.text() === "Date and Time") {
                $("#attributedatatext1").hide();
                $("#attributedatainteger1").hide();
                $("#attributedatadate1").show();
                $("#attributedatahyperlink1").hide();
              
                $("#attributedatasize1").hide();
                $("#attributedataintegersize1").hide();
                $("#attributedataemptysize1").hide();
                $("#txtsize1").hide();
                $("#size1").hide();
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)";
                    $scope.Attribute.ATTRIBUTE_DATATYPE = "Date and Time";
                });
                $("#Datatypetxt1").hide();
                $("#Dataformattxt1").hide();
                $("#Dataformatddl1").show();
            }
            else if (e.sender.text() === "HyperLink") {
                $("#attributedatatext1").hide();
                $("#attributedatainteger1").hide();
                $("#attributedatadate1").hide();
                $("#attributedatahyperlink1").show();
                
                $("#attributedatasize1").hide();
                $("#attributedataintegersize1").hide();
                $("#attributedataemptysize1").hide();
                $("#txtsize1").hide();
                $("#size1").hide();
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^(https?://)|^(HTTPS?://)|^(Https?://)?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+\\.)*([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\\.[a-zA-Z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+/?)$";
                    $scope.Attribute.ATTRIBUTE_DATATYPE = "Hyperlink";
                });
                $("#Datatypetxt1").hide();
                $("#Dataformattxt1").hide();
                $("#Dataformatddl1").show();
            } else {
                $("#Dataformatddl1").show();
                $("#Datatypeddl1").show();
                $("#attributedatainteger1").show();
                $("#attributedatatext1").hide();
                $("#attributedatadate1").hide();
                $("#attributedatahyperlink1").hide();
                $("#attributedatasize1").hide();
                $("#attributedataintegersize1").show();
                $("#attributedataemptysize1").hide();
                $("#txtsize1").show();
                $("#size1").show();
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(^-?\d\d*$)";
                    $scope.Attribute.ATTRIBUTE_DATATYPE = "Number";
                });
                $("#Datatypetxt1").hide();
                $("#Dataformattxt1").hide();
            }
        }
    };
    var intergerdata = [{
        attributedataformat: '(^-?\d\d*$)',
        attributedataformatname: 'Integer'
    }, {
        attributedataformat: '^-{0,1}?\d*\.{0,1}\d{0,6}$',
        attributedataformatname: 'Real Numbers'
    }];
    var textdata = [{
        attributedataformat: '^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$',
        attributedataformatname: 'All Characters'
    }, {
        attributedataformat: '^[ 0-9a-zA-Z\\r\\n]*$',
        attributedataformatname: 'Alpha Numeric'
    }, {
        attributedataformat: '^[ a-zA-Z\\r\\n]*$',
        attributedataformatname: 'Alphabets Only'
    }];
    var datedata = [{
        attributedataformat: '(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)',
        attributedataformatname: 'Short Format (dd/mm/yyyy)'
    }, {
        attributedataformat: '(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{3}(?:0[1-9]|[12]\\d|3[01]))(?=.{0}(?:0[1-9]|1[0-2]))(?!.{0}(?:0[2469]|11)-31)(?!.{0}02-30)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)...[13579])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)..[13579][048])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)..[02468][26])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.).[13579]00)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)[13579][048]00)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)[02468][26]00)',
        attributedataformatname: 'Short Format (mm/dd/yyyy)'
    }, {
        attributedataformat: '(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4}\\s\\d{2}(\\-|\\:)\\d\\d(\\-|\\:)\\d{2})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))',
        attributedataformatname: 'DateTime'
    }, {
        attributedataformat: '(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4}\\s\\d{2}(\\-|\\:)\\d\\d(\\-|\\:)\\d{2}\\s\\w{2})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))',
        attributedataformatname: 'Long Format'
    }, {
        attributedataformat: 'System default settings',
        attributedataformatname: 'System default'
    }];
    var hyperlinkdata = [{
        attributedataformat: "^(https?://)|^(HTTPS?://)|^(Https?://)?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+\\.)*([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\\.[a-zA-Z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+/?)$",
        attributedataformatname: 'Hyperlink'
    }];

    var sizeDecimal = [{
        Decimal: "0"
    },
    {
        Decimal: "1"
    },
    {
        Decimal: "2"
    },
    {
        Decimal: "3"
    },
        {
            Decimal: "4"
        },
        {
            Decimal: "5"
        },
        {
            Decimal: "6"
        }];
    $scope.attributedateintegerformatdatasource = {
        dataSource: new kendo.data.DataSource({
            data: intergerdata
        }),
        change: function (e) {
            var dropdownlist = $("#intdata1").data("kendoDropDownList");
            if(e.sender.text()==="Real Numbers")
            {
                dropdownlist.text("1");
                $scope.Attribute.ATTRIBUTE_DECIMAL = 1;

            }
            else
            {
                dropdownlist.text("0");
                $scope.Attribute.ATTRIBUTE_DECIMAL = 0;
            }
        }
    };

    $scope.attributedateformattextdatasource = {
        dataSource: new kendo.data.DataSource({
            data: textdata
        }),
        change: function (e) {
            if (e.sender.text() === "All Characters") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
                });

            }
            else if (e.sender.text() === "Alphabets Only") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ a-zA-Z\\r\\n]*$";
                });
            }
            else if (e.sender.text() === "Alpha Numeric") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n]*$";
                });
            }
            else {
                // $scope.Attribute.ATTRIBUTE_DATAFORMAT = "Text";
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^[ 0-9a-zA-Z\\r\\n\\x20-\\x7E\\u0000-\\uFFFF]*$";
                });
            }
        }
    };
    $scope.attributedateformatdatedatasource = {
        dataSource: new kendo.data.DataSource({
            data: datedata
        }),
        change: function (e) {
            if (e.sender.text() === "Short Format (dd/mm/yyyy)") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)";
                });
            }
            else if (e.sender.text() === "Short Format (mm/dd/yyyy)") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{3}(?:0[1-9]|[12]\\d|3[01]))(?=.{0}(?:0[1-9]|1[0-2]))(?!.{0}(?:0[2469]|11)-31)(?!.{0}02-30)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)...[13579])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)..[13579][048])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)..[02468][26])(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.).[13579]00)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)[13579][048]00)(?!02(\\-|\\/|\\.)29(\\-|\\/|\\.)[02468][26]00)";
                });
            }
            else if (e.sender.text() === "DateTime") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4}\\s\\d{2}(\\-|\\:)\\d\\d(\\-|\\:)\\d{2})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)(?=.{11}(?:0[0-9]|1[0-9]|2[03]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))";
                });
            }
            else if (e.sender.text() === "Long Format") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4}\\s\\d{2}(\\-|\\:)\\d\\d(\\-|\\:)\\d{2}\\s\\w{2})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)(?=.{11}(?:0[1-9]|1[0-2]))(?=.{14}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{17}(?:0[0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9]))(?=.{20}(?:[A|P][M]))";
                });
            }
            else if (e.sender.text() === "System default") {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "System default settings";
                });
            }
            else {
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(?=\\d\\d(\\-|\\/|\\.)\\d\\d(\\-|\\/|\\.)\\d{4})(?=.{0}(?:0[1-9]|[12]\\d|3[01]))(?=.{3}(?:0[1-9]|1[0-2]))(?!.{3}(?:0[2469]|11)-31)(?!.{3}02-30)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)...[13579])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[13579][048])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)..[02468][26])(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.).[13579]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[13579][048]00)(?!29(\\-|\\/|\\.)02(\\-|\\/|\\.)[02468][26]00)";
                });
            }
        }
    };
    $scope.attributedateformathyperlinkdatasource = {
        dataSource: new kendo.data.DataSource({
            data: hyperlinkdata
        }),
        change: function (e) {
            $scope.$apply(function () {
                $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^(https?://)|^(HTTPS?://)|^(Https?://)?(([0-9a-zA-Z_!~*'().&=+$%-]+: )?[0-9a-zA-Z_!~*'().&=+$%-]+@)?(([0-9]{1,3}\\.){3}[0-9]{1,3}|([0-9a-zA-Z_!~*'()-]+\\.)*([0-9a-zA-Z][0-9a-zA-Z-]{0,61})?[0-9a-zA-Z]\\.[a-zA-Z]{2,6})(:[0-9]{1,4})?((/?)|(/[0-9a-zA-Z_!~*'().;?:@&=+$,%#-]+)+/?)$";
            });
        }
    };
    $scope.size = {
        dataSource: new kendo.data.DataSource({
            data: sizeDecimal
        }),
        change: function (e) {
            if (e.sender.text() != "0") {
                $("#Dataformatddl1").hide();
                $("#Dataformattxt1").show();
                $("#datatxt1").val('Real Numbers');
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "^-{0,1}?\d*\.{0,1}\d{0,6}$";
                });
            }
            else {
                $("#datatxt1").val('Integer');
                $("#Dataformatddl1").show();
                $("#Dataformattxt1").hide();
                $scope.$apply(function () {
                    $scope.Attribute.ATTRIBUTE_DATAFORMAT = "(^-?\d\d*$)";
                });

            }
        }
    };


    $scope.CancelProductAttribute = function () {
        $scope.winProductNewAttributeSetup.close();
         $scope.winSubProductNewAttributeSetup.close();
        
    };

    $scope.omit_special_char = function (e, attribute_Name) {
     
        if (attribute_Name == "" || attribute_Name == '') {
            var valid = (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122);
            if (!valid) {
                e.preventDefault();
            }
        }
        else {
            var valid = (e.which >= 48 && e.which <= 57) || (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || (e.which == 45) || (e.which == 46) || (e.which == 95) || (e.which == 32);
            if (!valid) {
                e.preventDefault();
            }
        }
    }


}]);
