﻿LSApp.controller('SubproductImportController', ['$scope', 'dataFactory', '$http', '$compile', '$rootScope', 'ngTableParams', '$localStorage', function ($scope, dataFactory, $http, $compile, $rootScope, ngTableParams, $localStorage) {
    $scope.Message = "";
    $scope.FileInvalidMessage = "";
    $scope.SelectedFileForUpload = null;
    $scope.IsFormSubmitted = false;
    $scope.IsFileValid = false;
    $scope.IsFormValid = false;
    $scope.importSessionID = '';
    $("#subimportTableSheetSlectectionWindow").hide();
    $("#importbuttons").hide();
    $scope.backbuttonsub = false;
    $("#subimportTableSheetSlectectionWindowTemplate").hide();
    $("#subimportErrorWindowTemplate").hide();
    $("#subimportSuccessWindowTemplate").hide();
    $("#subimportErrorWindow").hide();
    $("#subimportSuccessWindow").hide();
    $("#subimportSuccessWindow1").hide();
    $("#Subimportpicklist").hide();
    $scope.displayimport = false;
    $scope.displayimportvalidate = true;
    $scope.displayexportbuttonsub = false;
    $scope.Errorlogoutputformatsub = "xls";
   
    
    //Form Validation
    $scope.$watch("f1.$valid", function (isValid) {
        $scope.IsFormValid = isValid;
    });
    
    $scope.SelectedValues = [];
    $scope.ChechFileValid = function (file) {
        var isValid = false;
        $scope.enableitemvalidationsub();
        if ($scope.SelectedFileForUpload != null) {
            if (file.name.contains(".xls") || file.name.contains(".xlsx")) {// && file.size <= (512 * 2048)
                $scope.FileInvalidMessage = "";
                isValid = true;
                
            }
            else {
               // $scope.FileInvalidMessage = "Selected file is Invalid. (only file type xls and xlsx is allowed)";
            }
        }
        else {
            $scope.FileInvalidMessage = "File required!";
        }
        $scope.IsFileValid = isValid;
    };


    $scope.export_logsub = function () {

        // alert("dssds");
        dataFactory.validationresult($scope.Errorlogoutputformatsub).success(function (response) {
            //$scope.passvalidation = response;
            window.open("DownloadFile.ashx?Path=" + response);


        }).error(function (error) {

            options.error(error);
        });
    };

    $scope.ChechFileValidTemplate = function (file) {
        var isValid = false;
        if ($scope.SelectedFileForUpload != null) {
            if (file.name.contains(".xml")) {// && file.size <= (512 * 2048)
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
            else {
                $scope.FileInvalidMessage = "Selected file is Invalid. (only file type xml)";
            }
        }
        else {
            $scope.FileInvalidMessage = "File required!";
        }
        $scope.IsFileValid = isValid;
    };
    $scope.SelectedFileForUploadnamesubproduct = "";
    //File Select event 
    $scope.selectFileforUpload = function (file) {
       
        $scope.SelectedFileForUpload = file[0];
        $scope.$apply(function () {
            $scope.SelectedFileForUploadnamesubproduct = file[0].name;
            $rootScope.SelectedFileForUploadnamemain = file[0].name;
        });
    };
    //----------------------------------------------------------------------------------------
    //Save File
    $scope.SaveFile = function () {
        $scope.IsFormSubmitted = true;
        $scope.Message = "";
        if ($scope.SelectedFileForUpload !== null) {
            $scope.ChechFileValid($scope.SelectedFileForUpload);
            $scope.UploadFile($scope.SelectedFileForUpload).then(function () {
                clearForm();
            }, function (e) {
            });
        }
        else
        {     
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please choose a File.',
                //type: "info"
            });
        }
    };
    //Save Template 
    $scope.SaveAsTemplate = function () {
        var windowlocation = window.location.origin;
        dataFactory.SaveAsTemplatePost($scope.allowDuplication, $scope.importExcelSheetDDSelectionValue, $scope.excelPath, $scope.prodImportData).success(function (response) {
            if (response.Data == "") {             
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File Name cannot contain Special Characters, please try a different combination without Special Characters and continue.',
                    type: "error"
                });
            } else {
                var filename = response.Data[0];
                windowlocation = windowlocation + "/Content/XMLTemplate/" + response.Data[1];
                window.open("DownloadImportTemplate.ashx?Path=" + response.Data[1]);
            }
        });
    };
    //Clear form 
    function clearForm() {
        $scope.FileDescription = "";
        angular.forEach(angular.element("input[type='file']"), function (inputElem) {
            angular.element(inputElem).val(null);
        });
        $scope.f1.$setPristine();
        $scope.IsFormSubmitted = false;
    }
    //var fac = {};
    //  fac.UploadFile = function (file) {
    $scope.newxmlname1 = "";
    $scope.excelPath = "";
    $scope.UploadFile = function(file) {
        var formData = new FormData();
        formData.append("file", file);
        //We can send more data to server using append         
        //var defer = $q.defer();
        $http.post("/Import/SaveFiles?XMLName=" + $scope.newxmlname1 + "", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function(d) {
                $scope.excelPath = d;
                $scope.ImportExcelSheetSelction.read();
                $("#subimportTableSheetSlectectionWindow").show();
                $("#importbuttons").show();
                $scope.backbuttonsub = true;
                $("#subimportTableSheetWindow").hide();
                $("#subimportSelectionTable").show();
                $("#advancedimport").show();
                

            })
            .error(function() {             
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });

            });
        return defer.promise;
    };    
    
    //Save Template
    $scope.SaveFileTemplate = function (e) {
        $scope.IsFormSubmitted = true;
        $scope.Message = "";
        if ($scope.SelectedFileForUpload !== null) {
            $scope.ChechFileValidTemplate($scope.SelectedFileForUpload);
            $scope.UploadFileTemplate($scope.SelectedFileForUpload).then(function () {
                ClearForm();
            }, function (e) {
            });
        }
        else {         
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please choose a File.',
               // type: "error"
            });
        }
    };
    
    $scope.UploadFileTemplate = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        //We can send more data to server using append         
        //var defer = $q.defer();
        $http.post("/Import/SaveFiles?XMLName=" + $scope.newxmlname1 + "", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {
                $scope.excelPath = d;
                loadXml();
                $("#subimportTableSheetSlectectionWindowTemplate").show();
                $("#importbuttons").show();
                $scope.backbuttonsub = true;
                //$("#subimportTableSheetWindowTemplate").hide();

            })
            .error(function () {            
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                     type: "error"
                });
            });
        return defer.promise;
    };
    
    function loadXml() {
        if ($scope.excelPath !== "") {
            dataFactory.LoadXmlFile($scope.excelPath, "Sub").success(function (importresult) {
                    $scope.importSessionID = importresult.Data.split("~", 2);   // kendo data source ;
                    if (importresult.Data.split("~", 1) == "Import Failed") {                      
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + importresult.Data.split("~", 1)+'.',
                            type: "info"
                        });
                      
                        $("#subimportErrorWindowTemplate").show();
                        $("#subimportSuccessWindowTemplate").hide();
                        $scope.importResultDatasource.read();
                    }
                    else if (importresult.Data.split("~", 1) == "SKU Exceed") {
                       
                        $("#subimportErrorWindowTemplate").hide();
                        $("#subimportSuccessWindowTemplate").hide();                    
                        $scope.importResultDatasource.read();
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'You have exceeded the Maximum No. of SKUs as per your Plan.',
                            type: "error"
                        });
                    }
                   
                    else {                        
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: '' + importresult.Data.split("~", 1)+'.',
                            type: "info"
                        });
                        $("#subimportErrorWindowTemplate").hide();
                        $("#subimportSuccessWindowTemplate").show();
                        $scope.importSuccessResultDatasource.read();
                    }

                    $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);  
                }).error(function (error) {                
                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'Import Failed, please try again.',
                        type: "error"
                    });
                    $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
                    options.error(error);
                });

                //$scope.excelPath = response;
                //$scope.importExcelSheetDDSelectionValue = "Sheet1";
                //$scope.SubproductsimportGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath);
        }
    }
    
    $scope.ImportExcelSheetSelction = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {
                dataFactory.SubproductsimportExcelSheetSelection($scope.excelPath).success(function (response) {
                    options.success(response);
                    if (response != null && response.length != 0) {
                        $scope.importSelectedvalue = response[0].TABLE_NAME;
                        $('#subimportSelectionTable').show();
                        $scope.importExcelSheetDDSelectionValue = $scope.importSelectedvalue;
                        $scope.SubproductsimportGridOptions($scope.importSelectedvalue, $scope.excelPath);
                    }
                 
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.importExcelSheetDDSelectionValue = "";
    $scope.importExcelSheetDDSelection = function (e) {
        $scope.importExcelSheetDDSelectionValue = e.sender.value();       
        if ($scope.importExcelSheetDDSelectionValue !== "") {
            $scope.SubproductsimportGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath);
        }
        else
        {
            $("#subimportSelectionTable").hide();
        }
    };
    //$scope.importExcelAttributeDDSelection = function (e) {
    //    $scope.importExcelAttributeDDSelectionValue = e.sender.value();
    //};
    $scope.cancelSubproduct = function () {
        $("#subimportTableSheetSlectectionWindowTemplate").hide();
        $("#subimportErrorWindowTemplate").hide();
        $("#subimportSuccessWindowTemplate").hide();
        $("#subimportTableSheetWindowTemplate").hide();
        
    };
    

    $scope.validatetResultDatasourcesub = new kendo.data.DataSource({
        type: "json", autobind: false,
        //serverFiltering: true, pageSize: 5, serverPaging: true,
        //serverSorting: true,
        sort: { field: "CATALOG_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                              
                dataFactory.ValidateImportResultssub($scope.importSessionIDsub).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "CATALOG_NAME",
                fields: {
                    CATALOG_NAME: { type: "string" },
                    CATEGORY_NAME: { type: "string" },
                    FAMILY_NAME: { type: "string" },
                    CATALOG_ITEM_NO: { type: "string" },
                    SUBITEM: { type: "string" }
                    

                }
            }
        }

    });

    $scope.importsubvalidationResultGridOptions = {
        dataSource: $scope.validatetResultDatasourcesub,
        //sortable: true,
        scrollable: true, resizable: true,
        //filterable: true,
        //pageable: true,
        autoBind: false,
        columns: [
            { field: "CATALOG_NAME", title: "CATALOG NAME", width: "200px" },
            { field: "CATEGORY_NAME", title: "CATEGORY NAME", width: "200px" },
            { field: "FAMILY_NAME", title: "FAMILY NAME", width: "200px" },
            { field: "CATALOG_ITEM_NO", title: "NEW ITEMS", width: "200px" },
            { field: "SUBITEM", title: "NEW SUBITEMS", width: "200px" }
           

        ]
    };
    $scope.ResetPage = function () {
        window.location.pathname = 'Import/ImportType';

    }
    $scope.Validatebacksub = function () {    

        $("#subimportSuccessWindow1").hide();
        $("#subimportTableSheetWindow").show();
        $("#advancedimport").hide();
        $scope.displayimportvalidate = false;
        $("#Subimportpicklist").hide();
        $scope.backbuttonsub = false;
        $scope.displayimport = false;
        var $el = $('#importfile-sub');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
       // $rootScope.SelectedFileForUploadnamemain = "";
        $scope.SelectedFileForUploadnamesubproduct = "";
        var dropdownlist = $("#familyimportsheetselectsub1").data("kendoDropDownList");
        dropdownlist.text(dropdownlist.options.optionLabel);
        dropdownlist.element.val(-1);
        dropdownlist.selectedIndex = -1;


    };

    $scope.subprodvalidation = function () {
       
        dataFactory.SubProductsfinishImport($scope.importExcelSheetDDSelectionValue, $scope.allowDuplication, $scope.excelPath, $scope.Errorlogoutputformatsub, $scope.prodImportData).success(function (importresult) {
            
            if (importresult.split("~", 1) == "validation Failed") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Validation failed. Please check log for details.',
                    type: "info"
                });
                $scope.importSessionIDsub = importresult.split("~", 2);
                $scope.validatetResultDatasourcesub.read();
                $scope.displayexportbuttonsub = true;
                $("#subimportSuccessWindow1").show();
                $scope.displayimportvalidate = false;
                $("#subimportSelectionTable").hide();                
                $("#subimportErrorWindow").hide();
                $("#subimportSuccessWindow").hide();
                $("#subimportErrorWindowTemplate").hide();
                $("#subimportSuccessWindowTemplate").hide();
                $("#advancedimport").hide();
                
                $scope.importResultDatasource.read();
              
            }          

            else {
                //$.msgBox({
                //    title: $localStorage.ProdcutTitle,
                //    content: 'Validation Success. No new items found.',
                //    type: "info"
                //});
                $scope.displayimport = true;
                $scope.displayimportvalidate = false;
           
            }

            
        }).error(function (error) {
            // $scope.loadingimg.center().close();       
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Import Failed, please try again.',
                type: "error"
            });
          
            options.error(error);
        });
    };

    $scope.enableitemvalidationsub = function () {
        dataFactory.enableitemvalidationsub().success(function (response) {
            //$scope.passvalidation = response;
            if (response == "true") {
                $scope.displayimport = false;
                $scope.displayimportvalidate = true;
            }
            else {
                $scope.displayimport = true;
                $scope.displayimportvalidate = false;
            }

        }).error(function (error) {

            options.error(error);
        });
    };
    $scope.finishImport = function () {
       // $scope.loadingimg.refresh({ url: "../views/app/partials/ImgLoading.html" });
        //   $scope.loadingimg.center().open();
        
        dataFactory.SubProductsfinishImport($scope.importExcelSheetDDSelectionValue, $scope.allowDuplication, $scope.excelPath, $scope.prodImportData).success(function (importresult) {
            // $scope.loadingimg.center().close();
            
                 $scope.importSessionID = importresult.split("~", 2);   // kendo data source ;
                 if (importresult.split("~", 1) == "Import Failed") {                
                     $.msgBox({
                         title: $localStorage.ProdcutTitle,
                         content: '' + importresult.split("~", 1) + '.',
                         type: "info"
                     });
                     $("#subimportErrorWindow").show();
                     $("#subimportSuccessWindow1").hide();
                     $("#subimportSuccessWindow").hide();
                     $("#subimportErrorWindowTemplate").show();
                     $("#subimportSuccessWindowTemplate").hide();
                     $scope.importResultDatasource.read();
                 }
                 else if(importresult.split("~", 1) == "SKU Exceed") {
                     $("#subimportErrorWindow").hide();
                     $("#subimportSuccessWindow").hide();
                     $("#subimportSuccessWindow1").hide();
                     $("#subimportErrorWindowTemplate").hide();
                     $("#subimportSuccessWindowTemplate").hide();                    
                     $.msgBox({
                         title: $localStorage.ProdcutTitle,
                         content: 'You have exceeded the Maximum No. of SKUs as per your Plan.',
                         type: "error"
                     });

                     $scope.importResultDatasource.read();
                 }
                 else if (importresult.contains("Import failed due to invalid Pick List value")) {
                     $.msgBox({
                         title: $localStorage.ProdcutTitle,
                         content: 'Import failed due to invalid Pick List value, please check the error log for details.',
                         type: "error"
                     });
                     // $scope.SelectedFileForUploadnamefamilyproduct = "";
                     $("#Subimportpicklist").show();
                     $scope.getFinishImportFailedpicklistResultsSub.read();

                 }
                 else {                 
                     $.msgBox({
                         title: $localStorage.ProdcutTitle,
                         content: '' + importresult.split("~", 1) + '.',
                         type: "info"
                     });
                     $("#subimportErrorWindow").hide();
                     $("#subimportSuccessWindow").show();
                     $("#subimportSuccessWindow1").hide();
                     $("#subimportErrorWindowTemplate").hide();
                     $("#subimportSuccessWindowTemplate").show();
                     $scope.importSuccessResultDatasource.read();
                 }

                 $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);  
        }).error(function (error) {
           // $scope.loadingimg.center().close();       
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Import Failed, please try again.',
                type: "error"
            });
            $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
               options.error(error);
           });
    };
    $scope.allowDuplication = "0";
    $scope.radioButtonYes = function () {
        $scope.allowDuplication = "1";
    };
    $scope.radioButtonNo = function () {
        $scope.allowDuplication = "0";
    };

    $scope.gridColumnsSub = [
        { field: "STATUS", title: "STATUS", width: "200px" },
        { field: "ITEM_NO", title: "ITEM_NO", width: "200px" },
        { field: "ATTRIBUTE_NAME", title: "ATTRIBUTE_NAME", width: "200px" },
        { field: "PICKLIST_VALUE", title: "PICKLIST_VALUE", width: "200px" }

    ];
    $scope.getFinishImportFailedpicklistResultsSub = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autoBind: false,
        scrollable: true,
        sort: { field: "STATUS", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportFailedpicklistResults($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    STATUS: { type: "string" },
                    ITEM_NO: { type: "string" },
                    ATTRIBUTE_NAME: { type: "string" },
                    PICKLIST_VALUE: { type: "string" }


                }
            }
        }

    });
    $("#subimportSelectionTable").hide();
    if ($scope.excelPath != "") {
        $scope.tblDashImportBoard = new ngTableParams({ page: 1, count: 5 },
            {counts: [],
                total: function () { return $scope.prodImportData.length; },
                getData: function ($defer, params) {
                    var filteredData = $scope.prodImportData;
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(filteredData, params.orderBy()) :
                        filteredData;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData);
                }
            });
    }
    $scope.SubproductsimportGridOptions = function (importExcelSheetDdSelectionValue, excelPath) {
        dataFactory.SubproductsGetImportSpecs(importExcelSheetDdSelectionValue, excelPath).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.prodImportData = obj.Data;
            $scope.columnsForImportAtt = obj.Columns;
        }).error(function (error) {
            options.error(error);
        });
        $("#subimportSelectionTable").show();
    };
  
    $scope.RadioCreateNew = function () {
        if ($scope.SelectedFileForUpload != null) {
            $scope.winNewxmlcreation.refresh({ url: "../views/app/partials/xmlcreation.html" });
            $scope.winNewxmlcreation.center().open();
        }
    };
    $scope.createXML = function() {
        $scope.newxmlname1 = $scope.newxmlname;
        $scope.winNewxmlcreation.center().close();
    };
    $scope.RadioUseSavedSettings = function() {
        if ($scope.SelectedFileForUpload != null) {
            $scope.winxmlupdation.refresh({ url: "../views/app/partials/xmlupdation.html" });
            $scope.winxmlupdation.center().open();
        }
    };
    $scope.AttributeDataSource1 = [{ Text: "Alpha-Numeric [Item Level]", value: 1 },
                 { Text: "Price / Numbers [Item Level]", value: 4 },
                 { Text: "Image / Attachment Path [Item Level]", value: 3 },
                 { Text: "Date-Time(MM/DD/YYYY) [Item Level]", value: 10 },
                 { Text: "Item Key Alpha-Numeric [Item Level]", value: 6 }];
    //{ Text: "-SYSTEM FIELD-", value: "SYSTEMFIELD" }];
    $scope.AttrType = "";
  $scope.selectedRow = null;
    $scope.LastIndex = "";

    $scope.importResultDatasource = new kendo.data.DataSource({
        type: "json",
        //serverFiltering: true, pageable: true, pageSize: 5, serverPaging: true,
        //serverSorting: true,
        sort: { field: "ErrorMessage", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportResults($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ErrorMessage",
                fields: {
                    ErrorMessage: { type: "string" },
                    ErrorProcedure: { type: "string" },
                    ErrorSeverity: { type: "string" },
                    ErrorState: { type: "string" },
                    ErrorNumber: { type: "string" },
                    ErrorLine: { type: "string" }
                }
            }
        }

    });

    $scope.importResultGridOptions = {
        dataSource: $scope.importResultDatasource, autoBind: false, resizable: true,
        //sortable: true, 
        scrollable: true,
        //filterable: true,
        //pageable: true
        columns: [
        { field: "ErrorMessage", title: "Error Message", width: "200px" },
        { field: "ErrorProcedure", title: "Error Procedure", width: "200px" },
        { field: "ErrorSeverity", title: "Error Severity", width: "200px" },
        { field: "ErrorState", title: "Error State", width: "200px" },
        { field: "ErrorNumber", title: "Error Number", width: "200px" },
        { field: "ErrorLine", title: "Error Line" }
        ]
    };

    $scope.importSuccessResultDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false,
        scrollable: true,
        sort: { field: "CATALOG_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishSubImportSuccessResults($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    CATALOG_NAME: { type: "string" },
                    SUB_PRODUCT_DETAILS: { type: "string" },
                    PRODUCT_DETAILS: { type: "string" },
                    STATUS: { type: "string" }
                }
            }
        }

    });

    $scope.importSuccessResultGridOptions = {
        dataSource: $scope.importSuccessResultDatasource,
        autoBind: false, resizable: true,
        sortable: true,
        pageable: false,
        serverPaging: false,
        serverSorting: false,
        scrollable: true,
        columns: [
        { field: "CATALOG_NAME", title: "CATALOG NAME", width: "200px" },
        { field: "PRODUCT_DETAILS", title: "PRODUCT DETAILS", width: "200px" },
            { field: "SUB_PRODUCT_DETAILS", title: "SUBPRODUCT DETAILS", width: "200px" },
        { field: "STATUS", title: "STATUS" }
        ]
    };
    return $scope.UploadFile;
}]);
