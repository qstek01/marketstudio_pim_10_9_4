﻿LSApp.controller('ImportController', ['$scope', 'dataFactory', '$http', '$compile', '$rootScope', 'ngTableParams', '$localStorage', '$rootScope', function ($scope, dataFactory, $http, $compile, $rootScope, ngTableParams, $localStorage, $rootScope) {
    $scope.Message = "";
    $scope.FileInvalidMessage = "";
    $scope.SelectedFileForUpload = null;
    $scope.IsFormSubmitted = false;
    $scope.IsFileValid = false;
    $scope.IsFormValid = false;
    $scope.importSessionID = '';
    $("#basicimportTableSheetSlectectionWindow").hide();
    $("#buttoncheck").hide();
    $("#importTableSheetSlectectionWindow").hide();
    $("#bulkimportTableSheetSlectectionWindow").hide();
    $("#importTableSheetSlectectionWindowTemplate").hide();
    $("#importErrorWindow").hide();
    $("#importSuccessWindow").hide();
    $("#bulkimportErrorWindow").hide();
    $("#bulkimportSuccessWindow").hide();
    $("#importErrorWindow1").hide();
    $("#importpicklist").hide();
    $scope.backbutton = false;
    $scope.displayexportbutton = false;
    $scope.importType = "Bulk";
    $scope.passvalidation = false;
    $scope.Errorlogoutputformat = "xls";
    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    //Form Validation
    $scope.$watch("f1.$valid", function (isValid) {
        $scope.IsFormValid = isValid;
    });
    $scope.SelectedValues = [];
    $scope.ChechFileValid = function (file) {
        var isValid = false;
        $scope.enablevalidation();
        if ($scope.SelectedFileForUpload != null) {
            if (file.name.contains(".xls") || file.name.contains(".xlsx")) {// && file.size <= (512 * 2048)
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
            else {
                //  $scope.FileInvalidMessage = "Selected file is Invalid. (only file type xls and xlsx is allowed)";
            }
        }
        else {
            $scope.FileInvalidMessage = "File required!";
        }
        $scope.IsFileValid = isValid;
    };

    $scope.Cancelfile_main = function () {
        $scope.SelectedFileForUpload = null;
        $scope.SelectedFileForUploadnamemainproduct = " ";
        $rootScope.SelectedFileForUploadnamemain = "";
        $('#importfile')[0].value = null;
    };
    $scope.Cancelfile = function () {
        $scope.SelectedFileForUpload = null;
        $scope.SelectedFileForUploadnamemainproduct = " ";
        $rootScope.SelectedFileForUploadnamemain = "";
        $('#importfile1')[0].value = null;
    };
    $scope.SelectedFileForUploadnamemainproduct = "";

    //File Select event 
    $scope.selectFileforUpload = function (file) {

        this.value = null;
        $scope.SelectedFileForUpload = file[0];
        $scope.$apply(function () {
            $scope.SelectedFileForUploadnamemainproduct = file[0].name;
            $rootScope.SelectedFileForUploadnamemain = file[0].name;
        });



    };



    $scope.ShowHide = function (data) {
        //If DIV is visible it will be hidden and vice versa.
        // $scope.checkedSub = data.checked;
        $scope.checked = data.checked;
    };

    //Save Template
    $scope.SaveFileTemplate = function (e) {
        $scope.IsFormSubmitted = true;
        $scope.importType = e.importType;
        $scope.Message = "";
        if ($scope.SelectedFileForUpload !== null) {
            $scope.ChechFileValidTemplate($scope.SelectedFileForUpload);
            $scope.UploadTemplate($scope.SelectedFileForUpload).then(function () {
                ClearForm();
            }, function (e) {
            });
        }
        else {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please choose a File.',
                //type: "info"
            });
        }
    };
    $scope.UploadTemplate = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        //We can send more data to server using append         
        //var defer = $q.defer();
        $http.post("/Import/SaveFiles?XMLName=" + $scope.newxmlname1 + "", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {
                $scope.TemplatePath = d; //d.split('\\')[d.split('\\').length - 1];
                //$scope.importExcelSheetSelction.read();
                if ($scope.importType == 'Bulk') {
                    $scope.BulkloadXml();
                    $("#bulkimportTableSheetSlectectionWindow").show();

                    $("#importTableSheetSlectectionWindowTemplate").hide();

                } else {

                    $scope.loadXml();
                    //$("#importTableSheetSlectectionWindowTemplate").show();
                    //$("#importTableSheetWindowTemplate").hide();
                }


            })
            .error(function () {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });
            });
        return defer.promise;
    };
    $scope.ChechFileValidTemplate = function (file) {
        var isValid = false;
        if ($scope.SelectedFileForUpload != null) {
            if (file.name.contains(".xml")) {// && file.size <= (512 * 2048)
                $scope.FileInvalidMessage = "";
                isValid = true;
            }
            else {
                $scope.FileInvalidMessage = "Selected file is Invalid. (only file type xml is allowed)";
            }
        }
        else {
            $scope.FileInvalidMessage = "File required!";
        }
        $scope.IsFileValid = isValid;
    };
    $scope.loadXml = function () {
        if ($scope.TemplatePath !== "") {
            dataFactory.LoadXmlFile($scope.TemplatePath, $scope.importType).success(function (response) {
                $scope.successREsultBasic = response.Data;
                $scope.SelectedFileForUpload = "";

                $("#importTableSheetWindowTemplate").hide();
                $("#basicImportSuccessWindowTemplate").show();
                $("#importTableSheetSlectectionWindowTemplate").show();
                $("#buttoncheck").show();
                $scope.backbutton = true;
                $("#importTableSheetWindowTemplate").show();

            });
        }
    };
    $scope.BulkloadXml = function () {
        if ($scope.TemplatePath !== "") {
            dataFactory.LoadXmlFile($scope.TemplatePath, $scope.importType).success(function (importresult) {
                $scope.importSessionID = importresult.Data.split("~", 2);
                if (importresult.Data.split("~", 1) == "Import Failed") {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + importresult.Data.split("~", 1) + '.',
                        type: "info"
                    });

                    $("#bulkimportErrorWindow").show();
                    $("#bulkimportSuccessWindow").hide();
                    $scope.importResultDatasource.read();
                }
                else if (importresult.Data.split("~", 1) == "SKU Exceed") {
                    $("#bulkimportErrorWindow").hide();
                    $("#bulkimportSuccessWindow").hide();
                    $scope.importResultDatasource.read();

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: 'You have exceeded the Maximum No. of SKUs as per your Plan.',
                        type: "info"
                    });
                }

                else {

                    $.msgBox({
                        title: $localStorage.ProdcutTitle,
                        content: '' + importresult.Data.split("~", 1) + '.',
                        type: "info"
                    });
                    $("#bulkimportErrorWindow").hide();
                    $("#bulkimportSuccessWindow").show();
                    $scope.importSuccessResultDatasource.read();
                }

                $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
            }).error(function (error) {
                // $scope.loadingimg.center().close();

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Import Failed, please try again.',
                    type: "error"
                });
                $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
                options.error(error);
            });
        }
    };



    //----------------------------------------------------------------------------------------
    //Save File
    $scope.SaveFile = function () {

        $scope.IsFormSubmitted = true;
        $scope.Message = "";
        if ($scope.SelectedFileForUpload !== null) {
            $scope.ChechFileValid($scope.SelectedFileForUpload);
            $scope.UploadFile($scope.SelectedFileForUpload).then(function () {
                ClearForm();
            }, function (e) {
            });
        }
        else {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please choose a File.',
                // type: "info"
            });
        }
    };
    $scope.SaveFileBasic = function () {
        $scope.IsFormSubmitted = true;
        $scope.Message = "";
        if ($scope.SelectedFileForUpload !== null) {
            $scope.ChechFileValid($scope.SelectedFileForUpload);
            $scope.UploadFileBasic($scope.SelectedFileForUpload).then(function () {
                ClearForm();
            }, function (e) {
            });
        }
        else {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please choose a File.',
                // type: "info"
            });
        }
    };
    //Save Basic Import File

    //Clear form 
    function ClearForm() {
        $scope.FileDescription = "";
        angular.forEach(angular.element("input[type='file']"), function (inputElem) {
            angular.element(inputElem).val(null);
        });
        $scope.f1.$setPristine();
        $scope.IsFormSubmitted = false;
    }
    //var fac = {};
    //  fac.UploadFile = function (file) {
    $("#basicImportSuccessWindow").hide();
    $("#basicImportSuccessWindowTemplate").hide();
    $scope.newxmlname1 = "";
    $scope.excelPath = "";

    $scope.UploadFile = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        //We can send more data to server using append         
        //var defer = $q.defer();
        $http.post("/Import/SaveFiles?XMLName=" + $scope.newxmlname1 + "", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {
                $scope.excelPath = d;
                $scope.importExcelSheetSelction.read();
                $("#basicimportTableSheetSlectectionWindow").show();
                $("#importTableSheetSlectectionWindow").show();
                $("#buttoncheck").show();
                $("#importTableSheetWindow").hide();
                $scope.backbutton = true;



            })
            .error(function () {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });
            });
        return defer.promise;
    };
    $scope.UploadFileBasic = function (file) {
        var formData = new FormData();
        formData.append("file", file);
        //We can send more data to server using append         
        //var defer = $q.defer();
        $http.post("/Import/SaveFiles?XMLName=" + $scope.newxmlname1 + "", formData,
            {
                withCredentials: true,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            })
            .success(function (d) {
                $scope.excelPath = d;
                $scope.importExcelSheetSelction.read();
                $("#basicimportTableSheetSlectectionWindow").show();
                $("#importTableSheetSlectectionWindow").hide();
                $("#buttoncheck").hide();
            })
            .error(function () {

                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File upload failed, please try again.',
                    type: "error"
                });
            });
        return defer.promise;
    };
    $scope.importExcelSheetSelction = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {

                dataFactory.importExcelSheetSelection($scope.excelPath).success(function (response) {
                    options.success(response);

                    if ($rootScope.EnableSubProduct == false) {
                        var subProdList = [];
                        $scope.subproduct = response[0].TABLE_NAME;
                        angular.forEach($scope.importExcelSheetSelction._data, function (val) {

                            if (val.TABLE_NAME.toString().toUpperCase().includes('SUBPRODUCTS')) {
                                $scope.importExcelSheetSelction._data.remove(val)
                            }
                        });
                    }

                    if (response != null && response.length != 0) {
                        $scope.importSelectedvalue = response[0].TABLE_NAME;
                        $('#importSelectionTable').show();
                        $scope.importExcelSheetDDSelectionValue = $scope.importSelectedvalue;
                        $scope.importGridOptions($scope.importSelectedvalue, $scope.excelPath);
                    }


                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.basicImportExcelSheetDDSelection = function (options) {

        $scope.basicImportExcelSheetDDSelectionvalue = options.sender.value();
        if ($scope.importExcelSheetDDSelectionbasicImportExcelSheetDDSelectionvalue !== "") {
            $scope.basicImportGridOptions($scope.basicImportExcelSheetDDSelectionvalue, $scope.excelPath);
        }
    };


    $scope.importExcelSheetDDSelectionValue = "";
    $scope.importExcelSheetDDSelection = function (e) {
        $scope.importExcelSheetDDSelectionValue = e.sender.value();
        //alert($scope.importExcelSheetDDSelectionValue);
        if ($scope.importExcelSheetDDSelectionValue !== "") {
            $scope.importGridOptions($scope.importExcelSheetDDSelectionValue, $scope.excelPath);
        }
        else {
            $("#importSelectionTable").hide();
        }
    };
    //$scope.importExcelAttributeDDSelection = function (e) {
    //    $scope.importExcelAttributeDDSelectionValue = e.sender.value();
    //};

    //---------Basic Import----------

    $scope.successREsultBasic = [];

    $scope.tblImportBasic = new ngTableParams({ page: 1, count: 25 },
    {
        counts: [5, 10, 25, 50, 100],
        sorting: {
            SORT: 'asc'     // initial sorting
        },
        total: function () { return $scope.successREsultBasic.length; },
        $scope: { $data: {} },
        getData: function ($defer, params) {

            var filteredData = $scope.successREsultBasic;
            var orderedData = filteredData;
            params.total(orderedData.length);
            return $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    });
    $scope.FinishImportBasic = function () {

        dataFactory.basicimportExcelSheetSelection($scope.allowDuplication, $scope.basicImportExcelSheetDDSelectionvalue, $scope.excelPath, $scope.basicprodImportData).success(function (response) {
            $scope.successREsultBasic = jQuery.parseJSON(response.Data);
            $scope.tblImportBasic.reload();
            $scope.tblImportBasic.$params.page = 1;
            $("#basicImportSuccessWindow").show();
            // $("#basicImportSuccessWindowTemplate").show();
        }).error(function (error) {
            options.error(error);
        });;


    };
    function FinishImportBasicTem() {

        //dataFactory.basicimportExcelSheetSelectionTemplate($scope.allowDuplication, $scope.basicImportExcelSheetDDSelectionvalue, $scope.excelPath, $scope.basicprodImportData).success(function (response) {
        //    $scope.successREsultBasic = response.Data;
        //    $("#basicImportSuccessWindow").show();
        //    $("#basicImportSuccessWindowTemplate").show();
        //}).error(function (error) {
        //    options.error(error);
        //});;


    };

    //-------Save As Template---------
    $scope.SaveAsTemplate = function () {
        var windowlocation = window.location.origin;
        $scope.importType = "";
        dataFactory.SaveAsTemplatePost($scope.allowDuplication, $scope.basicImportExcelSheetDDSelectionvalue, $scope.excelPath, $scope.basicprodImportData).success(function (response) {
            if (response.Data == "") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File Name cannot contain Special Characters, please try a different combination without Special Characters and continue.',
                    type: "error"
                });
            } else {
                var filename = response.Data[0];
                windowlocation = windowlocation + "/Content/XMLTemplate/" + response.Data[1];
                window.open("DownloadImportTemplate.ashx?Path=" + response.Data[1]);
            }
        });
    };
    $scope.BulkSaveAsTemplate = function () {
        var windowlocation = window.location.origin;
        $scope.importType = 'Bulk';
        dataFactory.SaveAsTemplatePost($scope.allowDuplication, $scope.importExcelSheetDDSelectionValue, $scope.excelPath, $scope.prodImportData).success(function (response) {
            if (response.Data == "") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'File Name cannot contain Special Characters, please try a different combination without Special Characters and continue.',
                    type: "error"
                });
            } else {
                var filename = response.Data[0];
                windowlocation = windowlocation + "/Content/XMLTemplate/" + response.Data[1];
                window.open("DownloadImportTemplate.ashx?Path=" + response.Data[1]);
            }
        });
    };
    $scope.finishImport = function () {

        $.msgBox({
            title: $localStorage.ProdcutTitle,
            content: "Import may take some time to Complete.",
            type: "confirm",
            buttons: [{ value: "Ok" },
                { value: "Cancel" }
            ],
            success: function (result) {
                if (result === "Ok") {
                    dataFactory.finishImport($scope.importExcelSheetDDSelectionValue, $scope.allowDuplication, $scope.excelPath, $scope.prodImportData).success(function (importresult) {
                        //    $scope.loadingimg.center().close();
                        $scope.importSessionID = importresult.split("~", 2);   // kendo data source ;
                        if (importresult.split("~", 1) == "Import Failed") {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: '' + importresult.split("~", 1) + '.',
                                type: "info"
                            });
                            $("#importErrorWindow").show();
                            $("#bulkimportErrorWindow").show();
                            $("#importSuccessWindow").hide();
                            $("#bulkimportSuccessWindow").hide();
                            $scope.importResultDatasource.read();
                        }
                        else if (importresult.split("~", 1) == "SKU Exceed") {
                            $("#importErrorWindow").hide();
                            $("#importSuccessWindow").hide();
                            $("#importSuccessWindow").hide();
                            $("#bulkimportSuccessWindow").hide();
                            $scope.importResultDatasource.read();

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'You have exceeded the Maximum No. of SKUs as per your Plan.',
                                type: "error"
                            });
                        }
                        else if (importresult.contains("Import failed due to invalid Pick List value")) {
                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Import failed due to invalid Pick List value, please check the error log for details.',
                                type: "error"
                            });
                            // $scope.SelectedFileForUploadnamefamilyproduct = "";
                            $("#importpicklist").show();
                            $scope.getFinishImportFailedpicklistResults1.read();

                        }
                        else {

                            $.msgBox({
                                title: $localStorage.ProdcutTitle,
                                content: 'Import successful.',
                                //content: ''+importresult.split("~", 1)+'',
                                type: "info"
                            });
                            $("#importErrorWindow").hide();
                            $("#importSuccessWindow").show();
                            // $("#importSuccessWindow").hide();
                            $("#bulkimportSuccessWindow").show();
                            $scope.importSuccessResultDatasource.read();
                        }

                        $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
                    }).error(function (error) {
                        // $scope.loadingimg.center().close();         
                        $.msgBox({
                            title: $localStorage.ProdcutTitle,
                            content: 'Import Failed, please try again.',
                            type: "error"
                        });
                        $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
                        options.error(error);
                    });
                }
                else {
                    return false;
                }
            }
        });
        $scope.gridColumns = [
            { field: "STATUS", title: "STATUS", width: "200px" },
            { field: "ITEM_NO", title: "ITEM_NO", width: "200px" },
            { field: "ATTRIBUTE_NAME", title: "ATTRIBUTE_NAME", width: "200px" },
            { field: "PICKLIST_VALUE", title: "PICKLIST_VALUE", width: "200px" }

        ];
        $scope.getFinishImportFailedpicklistResults1 = new kendo.data.DataSource({
            type: "json",
            serverFiltering: false, pageable: false, autoBind: false,
            scrollable: true,
            sort: { field: "STATUS", dir: "asc" },
            transport: {
                read: function (options) {
                    dataFactory.getFinishImportFailedpicklistResults($scope.importSessionID).success(function (response) {
                        options.success(response);
                    }).error(function (error) {
                        options.error(error);
                    });
                }
            }, schema: {
                model: {
                    //   id: "ATTRIBUTE_ID",
                    fields: {
                        STATUS: { type: "string" },
                        ITEM_NO: { type: "string" },
                        ATTRIBUTE_NAME: { type: "string" },
                        PICKLIST_VALUE: { type: "string" }


                    }
                }
            }

        });
        //  }
        //  $scope.loadingimg.refresh({ url: "../views/app/partials/ImgLoading.html" });
        // $scope.loadingimg.center().open();
        // $http.post("/Import/finishImport?SheetName=" + $scope.importExcelSheetDDSelectionValue + "&allowDuplicate=" + $scope.allowDuplication + "&excelPath=" + $scope.excelPath)
        // .success(function (importresult) {

        //     $scope.loadingimg.center().close();
        //     alert(importresult.split("~", 1));
        //     $scope.importSessionID = importresult.split("~", 2);   // kendo data source ;
        //     if (importresult.split("~", 1) == "Import Failed") {
        //         $("#importErrorWindow").show();
        //         $("#importSuccessWindow").hide();
        //         $scope.importResultDatasource.read();

        //         //alert("issue");
        //     }
        //     else {
        //         //alert("success");
        //         $("#importErrorWindow").hide();
        //         $("#importSuccessWindow").show();
        //         $scope.importSuccessResultDatasource.read();
        //     }

        //     $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);
        // })
        //.error(function () {
        //    $scope.loadingimg.center().close();
        //    alert("Import Failed. Please Try Again.");
        //    $http.post("/Import/DeleteFile?SheetPath=" + $scope.excelPath);




        //});
    };
    $scope.export_log = function () {

        // alert("dssds");
        dataFactory.validationresult($scope.Errorlogoutputformat).success(function (response) {
            //$scope.passvalidation = response;
            window.open("DownloadFile.ashx?Path=" + response);


        }).error(function (error) {

            options.error(error);
        });
    };
    $scope.ResetPage = function () {
        window.location.pathname = 'Import/ImportType';

    }
    $scope.Validateback = function () {
        $("#basicimportTableSheetSlectectionWindow").hide();
        $("#importTableSheetSlectectionWindow").hide();
        $("#buttoncheck").hide();
        $("#importErrorWindow1").hide();
        $("#importErrorWindow").hide();
        $("#importSuccessWindow").hide();
        $("#importpicklist").hide();

        $("#importTableSheetWindow").show();
        $scope.backbutton = false;
        var $el = $('#importfile1');
        $el.wrap('<form>').closest('form').get(0).reset();
        $el.unwrap();
        //$rootScope.SelectedFileForUploadnamemain = "";
        var dropdownlist = $("#familyimportsheetselectsub").data("kendoDropDownList");
        dropdownlist.text(dropdownlist.options.optionLabel);
        dropdownlist.element.val(-1);
        dropdownlist.selectedIndex = -1;


    };

    //Sheet validation before new item insertion//
    $scope.Validateimport = function () {
        dataFactory.Validateimport($scope.importExcelSheetDDSelectionValue, $scope.allowDuplication, $scope.excelPath, $scope.Errorlogoutputformat, $scope.prodImportData).success(function (importresult) {

            $scope.importSessionID = importresult.split("~", 2);   // kendo data source ;
            if (importresult.split("~", 1)[0].toLowerCase() == "validation failed") {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Validation failed. Please check log for details.',
                    type: "error"
                });
                $("#importSelectionTable").hide();
                $scope.displayexportbutton = true;

                $scope.passvalidation = false;
                $scope.validatetResultDatasource.read();
                $("#importErrorWindow1").show();
                $("#importErrorWindow").hide();
                $("#bulkimportErrorWindow").hide();
                $("#importSuccessWindow").hide();
                $("#bulkimportSuccessWindow").hide();
            }
            else if (importresult.split("~", 1)[0].toLowerCase() == "validation passed") {
                //$scope.passvalidation = true;
                $scope.passvalidationimport = true;
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: 'Validation passed.',
                    type: "error"
                });
            }

        }).error(function (error) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Validation failed, please try again.',
                type: "error"
            });

            options.error(error);
        });
    };


    //Validation of subcatalog items import//
    $scope.enablevalidation = function () {
        dataFactory.enableitemvalidation().success(function (response) {
            //$scope.passvalidation = response;
            if (response == "true") {
                $scope.passvalidationimport = false;
                $scope.passvalidation = true;
            }
            else {
                $scope.passvalidationimport = true;
                $scope.passvalidation = false;
            }

        }).error(function (error) {

            options.error(error);
        });
    };

    $scope.allowDuplication = "0";
    $scope.radioButtonYes = function () {
        $scope.allowDuplication = "1";
    };
    $scope.radioButtonNo = function () {
        $scope.allowDuplication = "0";
    };

    $scope.allowDuplication1 = "0";
    $scope.radioButtonYes1 = function () {
        $scope.allowDuplication1 = "1";
    };
    $scope.radioButtonNo1 = function () {
        $scope.allowDuplication1 = "0";
    };
    $("#importSelectionTable").hide();
    if ($scope.excelPath != "") {
        $scope.tblDashImportBoard = new ngTableParams({ page: 1, count: 5 },
            {
                counts: [],
                total: function () { return $scope.prodImportData.length; },
                getData: function ($defer, params) {
                    var filteredData = $scope.prodImportData;
                    var orderedData = params.sorting() ?
                        $filter('orderBy')(filteredData, params.orderBy()) :
                        filteredData;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData);
                }
            });
    }

    function Loadtable() {
        if ($scope.excelPath != "") {
            $scope.tblDashImportBoard = new ngTableParams({ page: 1, count: 5 },
                {
                    counts: [],
                    total: function () { return $scope.prodImportData.length; },
                    getData: function ($defer, params) {
                        var filteredData = $scope.prodImportData;
                        var orderedData = params.sorting() ?
                            $filter('orderBy')(filteredData, params.orderBy()) :
                            filteredData;
                        params.total(orderedData.length);
                        $defer.resolve(orderedData);
                    }
                });
        }
    }
    $("#basicImportSelectionTable").hide();
    if ($scope.excelPath !== "") {
        $scope.tblBasicDashImportBoard = new ngTableParams({ page: 1, count: 5 },
            {
                count: [],
                total: function () { return $scope.basicprodImportData.length; },
                getData: function ($defer, params) {
                    var filteredData = $scope.basicprodImportData;
                    var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;
                    params.total(orderedData.length);
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

                }
            });
    }
    $scope.importGridOptions = function (importExcelSheetDDSelectionValue, excelPath) {
        dataFactory.GetImportSpecs(importExcelSheetDDSelectionValue, excelPath).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);
            $scope.prodImportData = obj.Data;
            $scope.columnsForImportAtt = obj.Columns;
        }).error(function (error) {
            options.error(error);
        });
        $("#importSelectionTable").show();
    };
    $scope.basicImportGridOptions = function (basicImportExcelSheetDDSelectionValue, excelPath) {
        dataFactory.GetBasicImportSpecs(basicImportExcelSheetDDSelectionValue, excelPath).success(function (response) {
            var obj = jQuery.parseJSON(response.Data.Data);

            $scope.basicprodImportData = obj.Data;
            $scope.basiccolumnsForImportAtt = obj.Columns;

        }).error(function (error) {
            option.error(error);
        });
        $("#basicImportSelectionTable").show();
    };
    //$scope.importExcelAttributeSelection = {
    //    dataSource: {
    //        data: [
    //            { Text: "Alpha-Numeric [Product Level]", value: "Alpha-Numeric" },
    //            { Text: "Price / Numbers [Product Level]", value: "Price/Numbers" },
    //            { Text: "Image / Attachment Path [Product Level]", value: "Image/AttachmentPath" },
    //            { Text: "Date-Time(MM/DD/YYYY) [Product Level]", value: "Date-Time" },
    //            { Text: "Product Key Alpha-Numeric [Product Level]", value: "ProductKeyAlpha-Numeric" },
    //            { Text: "Alpha-Numeric [Family Level]", value: "Alpha-Numeric" },
    //            { Text: "Image / Attachment Path [Family Level]", value: "Image/AttachmentPath" },
    //            { Text: "Family-Attribute [Family Level]", value: "Family-Attribute" },
    //            { Text: "Family-Price [Family Level]", value: "Family-Price" },
    //            { Text: "Family-Key [Family Level]", value: "Family-Key" },
    //            { Text: "-SYSTEM FIELD-", value: "SYSTEMFIELD" }
    //        ]
    //    },
    //    dataTextField: "Text",
    //    dataValueField: "value"
    //};
    $scope.RadioCreateNew = function () {
        if ($scope.SelectedFileForUpload != null) {
            $scope.winNewxmlcreation.refresh({ url: "../views/app/partials/xmlcreation.html" });
            $scope.winNewxmlcreation.center().open();
        }
    };
    $scope.createXML = function () {
        $scope.newxmlname1 = $scope.newxmlname;
        $scope.winNewxmlcreation.center().close();
    }
    $scope.RadioUseSavedSettings = function () {
        if ($scope.SelectedFileForUpload != null) {
            $scope.winxmlupdation.refresh({ url: "../views/app/partials/xmlupdation.html" });
            $scope.winxmlupdation.center().open();
        }
    }
    $scope.AttributeDataSource1 = [
    //{ id: 1, DATA_TYPE: "Product Technical Specification" },
    //{ id: 3, DATA_TYPE: "Product Image / Attachment" },
    //{ id: 4, DATA_TYPE: "Product Price" },
    //{ id: 6, DATA_TYPE: "Product key" },
    //{ id: 7, DATA_TYPE: "Family Description" },
    //{ id: 9, DATA_TYPE: "Family Image / Attachment" },
    //{ id: 11, DATA_TYPE: "Family Attribute" },
    //{ id: 12, DATA_TYPE: "Family Price" },
    //{ id: 13, DATA_TYPE: "Family Key" }
    {
        Text: "Alpha-Numeric [Item Level]", value: "Alpha-Numeric",
        value: 1
    },
                {
                    Text: "Price / Numbers [Item Level]", value: "Price/Numbers",
                    value: 4
                },
                {
                    Text: "Image / Attachment Path [Item Level]", value: "Image/AttachmentPath",
                    value: 3
                },
                {
                    Text: "Date-Time(MM/DD/YYYY) [Item Level]", value: "Date-Time",
                    value: 10
                },
                {
                    Text: "Item Key Alpha-Numeric [Item Level]", value: "ProductKeyAlpha-Numeric",
                    value: 6
                },
                {
                    Text: "Alpha-Numeric [Product Level]", value: "Alpha-Numeric",
                    value: 7
                },
                {
                    Text: "Image / Attachment Path [Product Level]", value: "Image/AttachmentPath",
                    value: 9
                },
                {
                    Text: "Product-Attribute [Product Level]", value: "Family-Attribute",
                    value: 11
                },
                {
                    Text: "Product-Price [Product Level]", value: "Family-Price",
                    value: 12
                },
                {
                    Text: "Product-Key [Product Level]", value: "Family-Key",
                    value: 13
                },
                { Text: "-SYSTEM FIELD-", value: "SYSTEMFIELD" }
    ];


    $scope.importResultDatasource = new kendo.data.DataSource({
        type: "json", autobind: false,
        //serverFiltering: true, pageSize: 5, serverPaging: true,
        //serverSorting: true,
        sort: { field: "ErrorMessage", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportResults($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "ErrorMessage",
                fields: {
                    ErrorMessage: { type: "string" },
                    ErrorProcedure: { type: "string" },
                    ErrorSeverity: { type: "string" },
                    ErrorState: { type: "string" },
                    ErrorNumber: { type: "string" },
                    ErrorLine: { type: "string" }
                }
            }
        }

    });

    $scope.validatetResultDatasource = new kendo.data.DataSource({
        type: "json", autobind: false,
        //serverFiltering: true, pageSize: 5, serverPaging: true,
        //serverSorting: true,
        sort: { field: "CATALOG_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.validateImportResults($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                id: "CATALOG_NAME",
                fields: {
                    CATALOG_NAME: { type: "string" },
                    CATEGORY_NAME: { type: "string" },
                    FAMILY_NAME: { type: "string" },
                    CATALOG_ITEM_NO: { type: "string" }

                }
            }
        }

    });

    $scope.importResultGridOptionsvalidation = {
        dataSource: $scope.validatetResultDatasource,
        //sortable: true,
        scrollable: true, resizable: true,
        //filterable: true,
        //pageable: true,
        autoBind: false,
        columns: [
            { field: "CATALOG_NAME", title: "CATALOG NAME", width: "200px" },
            { field: "CATEGORY_NAME", title: "CATEGORY NAME", width: "200px" },
            { field: "FAMILY_NAME", title: "FAMILY NAME", width: "200px" },
            { field: "CATALOG_ITEM_NO", title: "NEW ITEMS", width: "200px" }

        ]
    };

    $scope.importResultGridOptions = {
        dataSource: $scope.importResultDatasource,
        //sortable: true,
        scrollable: true, resizable: true,
        //filterable: true,
        //pageable: true,
        autoBind: false,
        columns: [
        { field: "ErrorMessage", title: "Error Message", width: "200px" },
        { field: "ErrorProcedure", title: "Error Procedure", width: "200px" },
        { field: "ErrorSeverity", title: "Error Severity", width: "200px" },
        { field: "ErrorState", title: "Error State", width: "200px" },
        { field: "ErrorNumber", title: "Error Number", width: "200px" },
        { field: "ErrorLine", title: "Error Line" }
        ]
    };

    $scope.importSuccessResultDatasource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: false, pageable: false, autobind: false,
        scrollable: true,
        sort: { field: "CATALOG_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishImportSuccessResults($scope.importSessionID).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            model: {
                //   id: "ATTRIBUTE_ID",
                fields: {
                    CATALOG_NAME: { type: "string" },
                    CATEGORY_DETAILS: { type: "string" },
                    FAMILY_DETAILS: { type: "string" },
                    SUBFAMILY_DETAILS: { type: "string" },
                    PRODUCT_DETAILS: { type: "string" },
                    STATUS: { type: "string" }
                }
            }
        }

    });


    $scope.BasicimportSuccessResultDatasource = new kendo.data.DataSource({
        type: "json",
        //serverFiltering: false, pageable: false, autobind: false,
        //scrollable: true,
        //sort: { field: "CATALOG_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.getFinishBasicImportSuccessResults().success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        },

    });

    $scope.importSuccessResultGridOptions = {
        dataSource: $scope.importSuccessResultDatasource,
        autoBind: false, resizable: true,
        sortable: true,
        pageable: false,
        serverPaging: false,
        serverSorting: false,
        scrollable: true,
        columns: [
        { field: "CATALOG_NAME", title: "CATALOG NAME", width: "200px" },
        { field: "CATEGORY_DETAILS", title: "CATEGORY DETAILS", width: "200px" },
        { field: "FAMILY_DETAILS", title: "FAMILY DETAILS", width: "200px" },
        { field: "SUBFAMILY_DETAILS", title: "SUBFAMILY DETAILS", width: "200px" },
        { field: "PRODUCT_DETAILS", title: "PRODUCT DETAILS", width: "200px" },
        { field: "STATUS", title: "STATUS" }
        ]
    };
    $scope.AttrType = "";
    //$scope.AttrtypeChange1 = function (e) {
    //    $scope.AttrType = $scope.AttrType + "," + e.sender._old;
    //};

    $scope.selectedRow = null;
    $scope.LastIndex = "";
    //$scope.setClickedRow = function (index, data,type) {
    //    if ($scope.selectedRow !== index) {
    //        var values = data;
    //        //$scope.SelectedValues.push({ ExcelColumn: data[index].ExcelColumn, CatalogField: data[index].CatalogField, SelectedToImport: data[index].SelectedToImport, IsSystemField: data[index].IsSystemField, FieldType: data[index].FieldType });
    //        $scope.SelectedString = $scope.SelectedString + "^" + data[index].ExcelColumn + "~" + data[index].SelectedToImport;
    //        $scope.selectedRow = index;
    //    }
    //}





    $scope.validatefile = function () {

        dataFactory.basicimportExcelSheetSelection($scope.allowDuplication, $scope.basicImportExcelSheetDDSelectionvalue, $scope.excelPath, $scope.basicprodImportData).success(function (response) {
            $scope.successREsultBasic = jQuery.parseJSON(response.Data);
            $scope.tblImportBasic.reload();
            $scope.tblImportBasic.$params.page = 1;
            $("#basicImportSuccessWindow").show();
            // $("#basicImportSuccessWindowTemplate").show();
        }).error(function (error) {
            options.error(error);
        });;


    };
    return $scope.UploadFile;
}]);
