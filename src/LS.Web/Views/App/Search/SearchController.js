﻿
LSApp.controller("SearchController", ["$scope", "$window", "$location", "$routeParams", "dataFactory", '$rootScope', '$localStorage', '$http',
function ($scope, $window, $location, $routeParams, dataFactory, $rootScope, $localStorage, $http) {
    // --------------------------------------------------- Start Common Function --------------------------------------------------------------------------------

    $rootScope.EnableSubProduct = $localStorage.EnableSubProductLocal;
    $scope.searchtxt = "";
    $scope.CategoryID = "";
    $scope.catalogIdAttr = 0;
    $scope.getCustomerIDs = 0;
    if ($localStorage.getCatalogID === undefined) {
        $scope.catalogIdAttr = 0;
        $scope.catalogIDD = 0;
    } else {
        $scope.catalogIdAttr = $localStorage.getCatalogID;
        $scope.Catalog_id = $localStorage.getCatalogID;
        $scope.getCustomerIDs = $localStorage.getCustomerID;
    }

    $scope.itemnumberonly = false;
    $scope.subitemnumberonly = false;
    $scope.usewildcards = false;
    $scope.categorydetails = true;

    $scope.productdetails = false;
    $scope.subproductdetails = false;
    $scope.familydetails = false;
    $scope.globalattributes = false;
    $scope.checksubpro = "subprod";
    $scope.searchEmptyResult = false;



    $scope.SearchcatalogDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true,
        transport: {
            read: function (options) {

                dataFactory.GetCatalogDetailsforSearchCustomers($scope.catalogIdAttr, $scope.getCustomerIDs).success(function (response) {
                    //dataFactory.GetCatalogDetails($scope.catalogIdAttr, $scope.getCustomerIDs).success(function (response) {
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });

    // $scope.selectedOption = 3;

    $scope.seletedAttribute = "All";
    $scope.catalogIdAttrDataSource = new kendo.data.DataSource({
        type: "json",
        serverFiltering: true, sort: { field: "ATTRIBUTE_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.GetAllattributes($scope.catalogIdAttr).success(function (response) {
                    options.success(response);
                    if ($scope.productdetails == true || $scope.subproductdetails == true) {
                        if (response.length > 1) {
                            $scope.seletedAttribute = response[1].ATTRIBUTE_NAME;
                        }

                    }
                    else
                    {
                        $scope.seletedAttribute = "";
                    }

                }).error(function (error) {
                    options.error(error);
                });
            }
        }
    });
    $scope.selectcatalogattr = function (catalogid) {

        $scope.catalogIdAttr = catalogid;
        if (catalogid == 0 || catalogid != "") {
            $("#categorydisable").show();
            $("#categorylbldisable").show();
            $scope.catalogIdAttrDataSource.read();
            $rootScope.dropDownCategorySearchDatasource.read();
        } else {
            $("#categorydisable").hide();
            $("#categorylbldisable").hide();

        }
    };
    $rootScope.Dashboards = true;
    $scope.itemnumberclick = function ($event) {
       if ($event) {
            $scope.categorydetails = false;
            $scope.productdetails = false;
            $scope.subproductdetails = false;
            $scope.familydetails = false;
            $scope.subitemnumberonly = false;
            $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        }
    };
    $scope.wildCard = function (e) {
        if (e.usewildcards == false) {
            return true;
        }
        if ($scope.categorydetails == true || $scope.familydetails == true || $scope.productdetails == true || $scope.subproductdetails == true) {
            $scope.usewildcards = true;
            $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        }
        else if ($scope.subitemnumberonly == false && $scope.globalattributes == false && $scope.itemnumberonly == false) {
            $scope.categorydetails = true
            $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        }
    };
    $scope.subitemnumberclick = function ($event) {
        if ($event) {
            $scope.categorydetails = false;
            $scope.productdetails = false;
            $scope.subproductdetails = false;
            $scope.familydetails = false;
            $scope.itemnumberonly = false;
            $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        }
    };

    //$scope.dropDownCategoryDatasource = new kendo.data.HierarchicalDataSource({
    //    type: "json",
    //    transport: {
    //        read: function (options) {
    //            dataFactory.GetCatalogCategorySortDetails($scope.catalogIdAttr, options.data.id).success(function (response) {
    //                options.success(response);
    //            }).error(function (response) {
    //                options.success(response);
    //            });
    //        }
    //    },
    //    schema: {
    //        model: {
    //            id: "id",
    //            hasChildren: "hasChildren"
    //        }
    //    }
    //});

    //if ($("#CategoryTree").val() !== undefined) {
    //    $scope.dropDownCategory = $("#CategoryTree").kendoExtDropDownTreeView({
    //        treeview: {
    //            dataSource: $scope.dropDownCategoryDatasource,
    //            dataTextField: "CATEGORY_NAME",
    //            dataValueField: "id",
    //            loadOnDemand: false,
    //            select: onfamilySelect
    //        }
    //    }).data("kendoExtDropDownTreeView");

    //    $scope.dropDownCategory.treeview().expand(".k-item");
    //}

    $("#searchid").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
            // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });


    $scope.categorydetailsclick = function ($event) {

        //if ($event) {
        $scope.itemnumberonly = false;
        $scope.subitemnumberonly = false;
        $scope.globalattributes = false;
        // }

        $scope.productdetails = false;
        $scope.subproductdetails = false;
        $scope.familydetails = false;
        $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        if ($scope.usewildcards == false) {
            return true;
        }
        if ($scope.categorydetails == true || $scope.familydetails == true || $scope.productdetails == true || $scope.subproductdetails == true) {
            $scope.usewildcards = true;
        }
        else {
            $scope.usewildcards = false;
        }
        if ($scope.categorydetails == true) {

        }
    };

    $scope.familydetailsclick = function ($event) {
        // if ($event) {

        $scope.itemnumberonly = false;
        $scope.subitemnumberonly = false;
        $scope.globalattributes = false;
        //}
        $scope.categorydetails = false;
        $scope.productdetails = false;
        $scope.subproductdetails = false;
        $scope.familydetails = true;
        //$('#myTreeView').data('kendoTreeView').remove();
        //categoryBind();
        $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        if ($scope.usewildcards == false) {
            return true;
        }
        if ($scope.categorydetails == true || $scope.familydetails == true || $scope.productdetails == true || $scope.subproductdetails == true) {
            $scope.usewildcards = true;
        }
        else {
            $scope.usewildcards = false;
        }

    };

    $scope.productdetailsclick = function ($event) {
        //  if ($event) {
        $scope.itemnumberonly = false;
        $scope.subitemnumberonly = false;
        $scope.globalattributes = false;
        // }
        $scope.categorydetails = false;
        $scope.productdetails = true;
        $scope.subproductdetails = false;
        $scope.familydetails = false;
        $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        if ($scope.usewildcards == false) {
            return true;
        }
        if ($scope.categorydetails == true || $scope.familydetails == true || $scope.productdetails == true || $scope.subproductdetails == true) {
            $scope.usewildcards = true;
        }
        else {
            $scope.usewildcards = false;
        }
    };
    $scope.subproductdetailsclick = function ($event) {
        // if ($event) {
        $scope.itemnumberonly = false;
        $scope.subitemnumberonly = false;
        $scope.globalattributes = false;
        // }
        $scope.categorydetails = false;
        $scope.productdetails = false;
        $scope.subproductdetails = true;
        $scope.familydetails = false;
        $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        if ($scope.usewildcards == false) {
            return true;
        }
        if ($scope.categorydetails == true || $scope.familydetails == true || $scope.productdetails == true || $scope.subproductdetails == true) {
            $scope.usewildcards = true;
        }
        else {
            $scope.usewildcards = false;
        }
    };
    $scope.globalattributesclick = function ($event, val) {
        if (!$event && val) {
            $scope.itemnumberonly = true;
            $scope.subitemnumberonly = true;
            $scope.categorydetails = false;
            $scope.productdetails = false;
            $scope.subproductdetails = false;
            $scope.familydetails = false;
            $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        }
        else if ($event && val) {
            $scope.itemnumberonly = true;
            $scope.subitemnumberonly = true;
            $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        }
        else {
            $scope.itemnumberonly = false;
            $scope.subitemnumberonly = false;
            $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        }
    };

    $scope.catalogChange = function (e) {
        var catalId = e.sender.value();
        if (catalId == '') {
            catalId = 0;
        }
        $scope.catalogIDD = catalId;
        $scope.selectcatalogattr(catalId);
        $rootScope.selecetedCatalogId = catalId;
        $localStorage.getCatalogID = $rootScope.selecetedCatalogId;
        $scope.GetUserRoleRightsAll();
        // alert(e.sender.text());
    };
    $scope.attributeChange = function (e) {
        $scope.seletedAttribute = e.sender.value();
        // alert(e.sender.text());
    };
    // $scope.categorydetails = true;
    // $scope.productdetails = true;
    //  $scope.familydetails = true;
    // --------------------------------------------------- End Common Function --------------------------------------------------------------------------------
    // --------------------------------------------------- Start button clicks events--------------------------------------------------------------------------------

    $scope.ModifyResults = function (data) {
        if (data.length === 0) {
            $("#dynamictable").hide();
        }
        $("#familyEditor").hide();
        $("#searchtab").show();
        $("#resultbtntab").hide();
    };

    $scope.enterKeyPressedSearch = function (keyEvent) {
        if (keyEvent.keyCode == 13) {
            $scope.btnsearchClick();
        }
    };
    $scope.btnsearchClick = function () {
        
        //if ($scope.CategoryID === '') {

        //    $.msgBox({
        //        title: $localStorage.ProdcutTitle,
        //        content: 'Please select atleast one category.',
        //        type: "error"
        //    });
        //    return false;
        //}
        if ($scope.catalogIdAttr === '1') {

            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please change the working catalog.',
                type: "error"
            });
            return false;
        }
        if ($scope.searchtxt == "" || $scope.searchtxt == undefined) {
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'Please enter a value in the Search text.',
                type: "error"
            });
            return false;
        }
        $scope.productData = [];
        if ($scope.CategoryID !== "") {
            $scope.Option = '1';
        }
        else
            $scope.Option = '2';
        //$scope.isProductSearchResultsNull = true;
        //$scope.isSubProductSearchResultsNull = true;
        //$scope.isFamilySearchResultsNull = true;
        //$scope.isCategorySearchResultsNull = true;
        $("#divSubItemSearch").hide();

        if ($scope.searchid !== "" && $scope.searchid !== 0) {
            if ($scope.searchtxt !== "" && $scope.searchtxt != undefined)
                $scope.searchtxt = "";
        }

        if ($scope.searchtxt !== "" && $scope.searchtxt != undefined && $scope.itemnumberonly && !$scope.globalattributes) {
            if ($scope.searchid == "" || $scope.searchid == undefined)
                $scope.searchid = 0;
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();
            $("#divSubProductSearch").hide();

            $("#divSubItemSearch").hide();
            $("#dynamictable").hide();
            $scope.GetItemSearchResultsdataSource.read();
        }
        else if ($scope.searchtxt !== "" && $scope.searchtxt != undefined && $scope.itemnumberonly && $scope.globalattributes) {
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();
            $("#divSubProductSearch").hide();

            $("#divSubItemSearch").hide();
            $scope.GetItemSearchResultsdataSource.read();
            $('#dynamictable').show();
            dataFactory.SearchWithGlobalAttributes($scope.catalogIdAttr, $scope.searchtxt, "").success(function (response) {
                var obj = jQuery.parseJSON(response.Data.Data);
                $scope.productData = obj.Data;
                $scope.columns = obj.Columns;
            }).error(function (error) {
                options.error(error);
            });

        }
        else if ($scope.searchtxt !== "" && $scope.searchtxt != undefined && $scope.subitemnumberonly && !$scope.globalattributes) {
            if ($scope.searchid == "" || $scope.searchid == undefined)
                $scope.searchid = 0;
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();
            $("#divSubProductSearch").hide();
            $("#divItemSearch").hide();

            $("#dynamictable").hide();
            $scope.GetSubItemSearchResultsdataSource.read();
        }
        else if ($scope.searchtxt !== "" && $scope.searchtxt != undefined && $scope.subitemnumberonly && $scope.globalattributes && $scope.checksubpro == "subprod") {
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();
            $("#divSubProductSearch").hide();
            $("#divItemSearch").hide();

            $scope.GetSubItemSearchResultsdataSource.read();
            $('#dynamictable').show();
            dataFactory.SearchWithGlobalAttributes($scope.catalogIdAttr, $scope.searchtxt, $scope.checksubpro).success(function (response) {
                var obj = jQuery.parseJSON(response.Data.Data);
                $scope.productData = obj.Data;
                $scope.columns = obj.Columns;
            }).error(function (error) {
                options.error(error);
            });

        }

        else if ($scope.searchtxt !== "" && $scope.searchtxt != undefined && !$scope.itemnumberonly && $scope.categorydetails && !$scope.familydetails && !$scope.productdetails && !$scope.subproductdetails) {
            if ($scope.searchid == "" || $scope.searchid == undefined)
                $scope.searchid = 0;
            $("#divItemSearch").hide();
            $("#divSubItemSearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();

            $("#dynamictable").hide();
            $scope.GetcategorySearchGridresultsdataSource.read();
        } else if ($scope.searchtxt !== "" && $scope.searchtxt != undefined && !$scope.itemnumberonly && !$scope.categorydetails && $scope.familydetails && !$scope.productdetails && !$scope.subproductdetails) {
            if ($scope.searchid == "" || $scope.searchid == undefined)
                $scope.searchid = 0;
            $("#divItemSearch").hide();
            $("#divSubItemSearch").hide();
            $("#divCategorySearch").hide();
            $("#divProductSearch").hide();

            $("#dynamictable").hide();
            $scope.GetFamilySearchGridresultsdataSource.read();
        }
        else if ($scope.searchid !== "" && $scope.searchid !== 0 && !$scope.itemnumberonly && !$scope.categorydetails && $scope.familydetails && !$scope.productdetails && !$scope.subproductdetails) {
            if ($scope.searchtxt == undefined)
                $scope.searchtxt = "";
            $("#divItemSearch").hide();
            $("#divSubItemSearch").hide();
            $("#divCategorySearch").hide();
            $("#divProductSearch").hide();
            //$("#divFamilySearch").show();
            $("#dynamictable").hide();
            $scope.GetFamilySearchGridresultsdataSource.read();

        } else if ($scope.searchtxt !== "" && $scope.searchtxt != undefined && !$scope.itemnumberonly && !$scope.categorydetails && !$scope.familydetails && !$scope.subproductdetails && $scope.productdetails) {
            if ($scope.searchid == "" || $scope.searchid == undefined)
                $scope.searchid = 0;
            $("#divItemSearch").hide();
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            //$("#divProductSearch").show();
            $("#divSubProductSearch").hide();
            $("#dynamictable").hide();
            $scope.GetProductSearchGridresultsdataSource.read();
        }
        else if ($scope.searchtxt !== "" && $scope.searchtxt != undefined && !$scope.itemnumberonly && !$scope.categorydetails && !$scope.familydetails && !$scope.productdetails && $scope.subproductdetails) {
            if ($scope.searchid == "" || $scope.searchid == undefined)
                $scope.searchid = 0;
            $("#divItemSearch").hide();
            $("#divSubItemSearch").hide();
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();

            $("#dynamictable").hide();
            $scope.GetSubProductSearchGridresultsdataSource.read();

        }

        else if ($scope.searchid !== "" && $scope.searchid !== 0 && !$scope.itemnumberonly && !$scope.categorydetails && !$scope.familydetails && !$scope.subproductdetails && $scope.productdetails) {
            if ($scope.searchtxt == undefined)
                $scope.searchtxt = "";
            $("#divItemSearch").hide();
            $("#divSubItemSearch").hide();
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            //$("#divProductSearch").show();
            $("#divSubProductSearch").hide();
            $("#dynamictable").hide();
            $scope.GetProductSearchGridresultsdataSource.read();

        }
        else if ($scope.searchid !== "" && $scope.searchid !== 0 && !$scope.itemnumberonly && !$scope.categorydetails && !$scope.familydetails && !$scope.productdetails && $scope.subproductdetails) {
            if ($scope.searchtxt == undefined)
                $scope.searchtxt = "";
            $("#divItemSearch").hide();
            $("#divSubItemSearch").hide();
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();
            //$("#divSubProductSearch").show();
            $("#dynamictable").hide();
            $scope.GetSubProductSearchGridresultsdataSource.read();
        }
        else if ((($scope.searchtxt !== "" && $scope.searchtxt != undefined) || ($scope.searchid !== 0 && $scope.searchid !== "")) && !$scope.itemnumberonly && ($scope.categorydetails || $scope.familydetails || $scope.productdetails || $scope.subproductdetails)) {
            if ($scope.searchid == "" || $scope.searchid == undefined)
                $scope.searchid = 0;
            if ($scope.searchtxt == undefined)
                $scope.searchtxt = "";
            $("#divItemSearch").hide();
            $("#divSubItemSearch").hide();
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();
            $("#divSubProductSearch").hide();
            $("#dynamictable").hide();

            if ($scope.categorydetails) {

                if (($scope.searchid == 0 || $scope.searchid == "")) {
                    //$("#divCategorySearch").show();
                    $scope.GetcategorySearchGridresultsdataSource.read();
                }
            }

            if ($scope.familydetails) {
                //$("#divFamilySearch").show();
                $scope.GetFamilySearchGridresultsdataSource.read();

            }
            if ($scope.productdetails) {
                //$("#divProductSearch").show();
                $("#divSubItemSearch").hide();
                $scope.GetProductSearchGridresultsdataSource.read();

            }
            if ($scope.subproductdetails) {
                //$("#divSubProductSearch").show();
                $("#divSubItemSearch").hide();
                $scope.GetSubProductSearchGridresultsdataSource.read();
            }
        } else {
            $("#divItemSearch").hide();
            $("#divSubItemSearch").hide();
            $("#divCategorySearch").hide();
            $("#divFamilySearch").hide();
            $("#divProductSearch").hide();
            $("#divSubProductSearch").hide();
            $("#dynamictable").hide();
            $.msgBox({
                title: $localStorage.ProdcutTitle,
                content: 'No results found, please try using different combinations.',

                //type: "info"
            });

        }

       
    };
    
    $scope.btncancelclick = function () {
        
        $scope.searchtxt = "";
        $scope.itemnumberonly = false;
        $scope.subitemnumberonly = false;
        $scope.usewildcards = true;
        $scope.categorydetails = true;
        $scope.productdetails = false;
        $scope.subproductdetails = false;
        $scope.familydetails = false;
        $("#divItemSearch").hide();
        $("#divSubItemSearch").hide();
        $("#divCategorySearch").hide();
        $("#divFamilySearch").hide();
        $("#divProductSearch").hide();
        $("#divSubProductSearch").hide();
        $("#dynamictable").hide();
        $scope.selectcatalogattr($rootScope.selecetedCatalogId);
        //$rootScope.dropDownCategoryDatasource.read();
    };
    $scope.display = false;
    $scope.mouseleave = function (val) {
        var val = $("#searchid").val();
        if (val !== "0" && val !== "") {
            //$("#SearchCategoryTree").hide();
            $("#categorydisable").hide();
            $("#categorylbldisable").hide();
        }
        else {
            //  $("#SearchCategoryTree").show();
            $("#categorydisable").show();
            $("#categorylbldisable").show();
        }
    };
    $scope.blur = function () {
        $("#searchid").val(0);
        // $("#SearchCategoryTree").show();
        $("#categorydisable").show();
        $("#categorylbldisable").show();
    };
    // --------------------------------------------------- End button clicks event --------------------------------------------------------------------------------

    // ---------------------------------------------------Start Item Search Results--------------------------------------------------------------------------------

    $scope.GetItemSearchResultsdataSource = new kendo.data.DataSource({
        pageSize: 5,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        sort: { field: "FAMILY_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                dataFactory.SearchResults($scope.searchtxt, $scope.usewildcards, $scope.catalogIdAttr, $scope.CategoryID).success(function (response) {
                    if (response.length == 0) {
                        $scope.searchEmptyResult = true;
                        options.success(response);
                    }
                    else {
                        $("#divItemSearch").show();

                        options.success(response);
                        $scope.searchEmptyResult = false;
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FAMILY_ID",
                fields: {
                    STRING_VALUE: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    PRODUCT_ID: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    OBJECT_TYPE: { editable: false },
                    OBJECT_NAME: { editable: false },
                    FAMILY_ID: { editable: false },
                    FAMILY_NAME: { editable: false },
                    SUBFAMILY_NAME: { editable: false },
                    CATEGORY_ID: { editable: false },
                    CATEGORY_NAME: { editable: false },
                    SUBCATNAME_L1: { editable: false },
                    SUBCATNAME_L2: { editable: false },
                    SUBCATNAME_L3: { editable: false },
                    CATALOG_ID: { editable: false },
                    CATALOG_NAME: { editable: false },
                    CREATED_USER: { editable: false },
                    CREATED_DATE: { editable: false, type: "date" },
                    MODIFIED_USER: { editable: false },
                    MODIFIED_DATE: { editable: false, type: "date" }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    //type: "info"
                });


            }
        }
    });
    $scope.GetSubItemSearchResultsdataSource = new kendo.data.DataSource({

        pageSize: 5,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        sort: { field: "FAMILY_NAME", dir: "asc" },
        transport: {
            read: function (options) {

                if ($scope.catalogIdAttr == "") {
                    $scope.catalogIdAttr = 0;
                }
                dataFactory.SubSearchResults($scope.searchtxt, $scope.usewildcards, $scope.catalogIdAttr, $scope.CategoryID).success(function (response) {
                    if (response.length == 0) {
                        $scope.searchEmptyResult = true;
                    }
                    else {
                        $("#divSubItemSearch").show();
                        options.success(response);

                        $scope.searchEmptyResult = false;
                    }
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FAMILY_ID",
                fields: {
                    STRING_VALUE: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    PRODUCT_ID: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    OBJECT_TYPE: { editable: false },
                    OBJECT_NAME: { editable: false },
                    FAMILY_ID: { editable: false },
                    FAMILY_NAME: { editable: false },
                    SUBFAMILY_NAME: { editable: false },
                    CATEGORY_ID: { editable: false },
                    CATEGORY_NAME: { editable: false },
                    SUBCATNAME_L1: { editable: false },
                    SUBCATNAME_L2: { editable: false },
                    SUBCATNAME_L3: { editable: false },
                    CATALOG_ID: { editable: false },
                    CATALOG_NAME: { editable: false },
                    CREATED_USER: { editable: false },
                    CREATED_DATE: { editable: false, type: "date" },
                    MODIFIED_USER: { editable: false },
                    MODIFIED_DATE: { editable: false, type: "date" }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });
            }
        }
    });

    $scope.itemSearchGridresultsColumns = [
         { field: "STRING_VALUE", title: "String Value", locked: 'true', template: "<a class='k-link' href='javascript:void(0);' ng-click='ngclkproductid(this)'>#=STRING_VALUE#</a>", width: "180px" },
         { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "180px" },
           { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
             { field: "SUBCATNAME_L1", title: "SubCatName L1", width: "180px" },
              { field: "SUBCATNAME_L2", title: "SubCatName L2", width: "180px" },
              { field: "SUBCATNAME_L3", title: "SubCatName L3", width: "180px" },
         { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkCategoryid(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
         { field: "FAMILY_NAME", title: "Family Name", template: "<a title='Family Name' class='k-link' href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=FAMILY_NAME#</a>", width: "180px" },
         { field: "CREATED_USER", title: "Created User", width: "180px" },
         { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } },
         { field: "MODIFIED_USER", title: "Modified User", width: "180px" },
         { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } }];
    $scope.subitemSearchGridresultsColumns = [
        { field: "STRING_VALUE", title: "String Value", locked: "true", template: "<a class='k-link' href='javascript:void(0);' ng-click='ngclkSubProductid(this)'>#=STRING_VALUE#</a>", width: "180px" },
        { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "180px" },
        { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkCategoryid(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
        { field: "FAMILY_NAME", title: "Family Name", template: "<a title='Family Name' class='k-link' href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=FAMILY_NAME#</a>", width: "180px" },
        { field: "CATALOG_ITEM_NO", title: "ITEM#", template: "<a class='k-link' href='javascript:void(0);' >#=CATALOG_ITEM_NO#</a>", width: "180px" },
        { field: "CREATED_USER", title: "Created User", width: "180px" },
        { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } },
        { field: "MODIFIED_USER", title: "Modified User", width: "180px" },
        { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } }];

    $scope.ngclkproductid = function (e) {

        $("#familyEditor").show();
        $("#searchtab").hide();
        $("#resultbtntab").show();
        $rootScope.clickSelectedItems(e.dataItem, true, false);
        var tabstripSp = $("#productgridtabstrip").data("kendoTabStrip");
        var myTabSp = tabstripSp.tabGroup.children("li").eq(0);
        tabstripSp.enable(myTabSp);
        tabstripSp.select(myTabSp);
        $rootScope.inverted = '';
        if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1327px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1327px !important;");
        }
        if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
            $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1430px; !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1430px !important;");
        }

        if (window.screen.availWidth >= 1920 && window.screen.availWidth <= 2100) {
            $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1645px; !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1645px !important;");
        }

        if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
            $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1430px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1430px !important;");

        }
        if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
            $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

        }


        /*Sub Product*/

        if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1270px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1270px !important;");
        }
        if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");
        }

        if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1440px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1440px !important;");

        }
        if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

        }
    };

    $scope.ngclkfamilyid = function (e) {
        $("#familyEditor").show();
        $("#searchtab").hide();
        $("#resultbtntab").show();
        e.dataItem.id = e.dataItem.FAMILY_ID;
        $rootScope.clickSelectedItems(e.dataItem, true, true);
        var tabstripSp = $("#productgridtabstrip").data("kendoTabStrip");
        var myTabSp = tabstripSp.tabGroup.children("li").eq(0);
        tabstripSp.enable(myTabSp);
        tabstripSp.select(myTabSp);
        
    };
    $scope.ngclkSubProductid = function (e) {
        
        $("#familyEditor").show();
        $("#searchtab").hide();
        $("#resultbtntab").show();
        e.dataItem.id = e.dataItem.FAMILY_ID;
        
        $rootScope.clickSelectedItemsInverted(e.dataItem, true);
        var tabstripSp = $("#productgridtabstrip").data("kendoTabStrip");
        var myTabSp = tabstripSp.tabGroup.children("li").eq(1);
        tabstripSp.enable(myTabSp);
        tabstripSp.select(myTabSp);
        $rootScope.inverted = '';
        if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1270px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1270px !important;");

        }
        if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
            $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1220px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1220px !important;");
        }

        if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
            $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1430px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1430px !important;");

        }
        if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
            $("#sampleProdgrid").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
            $("#sampleProdgrid tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

        }


        /*Sub Product*/

        if (window.screen.availWidth >= 1400 && window.screen.availWidth <= 1490) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1270px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1270px !important;");
       
        }
        if (window.screen.availWidth >= 1900 && window.screen.availWidth <= 2100) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");
        }

        if (window.screen.availWidth >= 1500 && window.screen.availWidth <= 1600) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1300px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1300px !important;");

        }
        if (window.screen.availWidth >= 1360 && window.screen.availWidth < 1400) {
            $("#samplegridss").css("cssText", "margin-top: 20px; margin-bottom: 40px; position: relative !important; width: 950px !important; overflow: hidden !important; border-collapse: collapse !important;max-width: 1195px !important;");
            $("#samplegridss tbody").css("cssText", "margin-left: -2px;max-width: 1195px !important;");

        }
    };


    $scope.ngclkCategory = function (e) {
        $("#familyEditor").show();
        $("#searchtab").hide();
        $("#resultbtntab").show();
        e.dataItem.id = e.dataItem.CATEGORY_ID;
        $rootScope.clickSelectedItem(e.dataItem);
    };

    $scope.ngclkCategoryid = function (e) {
        $("#familyEditor").show();
        $("#searchtab").hide();
        $("#resultbtntab").show();
        e.dataItem.id = e.dataItem.CATEGORY_ID;
        $rootScope.clickSelectedItem(e.dataItem);
    };

    $scope.navigate = function (e) {
        $("#familyEditor").show();
        $("#searchtab").hide();
        $("#resultbtntab").show();
        e.id = e.FAMILY_ID;
        $rootScope.clickSelectedItems(e, true, true);
        var tabstripSp = $("#productgridtabstrip").data("kendoTabStrip");
        var myTabSp = tabstripSp.tabGroup.children("li").eq(0);
        tabstripSp.enable(myTabSp);
        tabstripSp.select(myTabSp);
    };
    // --------------------------------------------------- End Item Search Results -----------------------------------------------------------------------------------

    // ---------------------------------------------------Start Category Search Results--------------------------------------------------------------------------------

    $scope.GetcategorySearchGridresultsdataSource = new kendo.data.DataSource({
        pageSize: 5,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        sort: { field: "CATEGORY_NAME", dir: "asc" },
        transport: {
            read: function (options) {

                dataFactory.GetCategorySearchResults($scope.searchtxt, $scope.usewildcards, $scope.catalogIdAttr, $scope.CategoryID, $scope.Option).success(function (response) {
                    if (response.length != 0) {
                        $scope.isCategorySearchResultsNull = false;
                        $scope.searchEmptyResult = false;
                        $("#divCategorySearch").show();
                    }
                    else {
                        $scope.isCategorySearchResultsNull = true;
                        $scope.searchEmptyResult = true;
                    }
                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });
            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "CATEGORY_ID",
                fields: {
                    CATALOG_ID: { editable: false }
                    , CATALOG_NAME: { editable: false }
                    , CATEGORY_ID: { editable: false }
                    , CATEGORY_NAME: { editable: false }
                    , PARENT_CATEGORY: { editable: false }
                    , SHORT_DESC: { editable: false }
                    , IMAGE_FILE: { editable: false }
                    , IMAGE_TYPE: { editable: false }
                    , IMAGE_NAME: { editable: false }
                    , IMAGE_NAME2: { editable: false }
                    , IMAGE_FILE2: { editable: false }
                    , IMAGE_TYPE2: { editable: false }
                    , CUSTOM_NUM_FIELD1: { editable: false }
                    , CUSTOM_NUM_FIELD2: { editable: false }
                    , CUSTOM_NUM_FIELD3: { editable: false }
                    , CUSTOM_TEXT_FIELD1: { editable: false }
                    , CUSTOM_TEXT_FIELD2: { editable: false }
                    , CUSTOM_TEXT_FIELD3: { editable: false }
                    , CREATED_USER: { editable: false }
                    , CREATED_DATE: { editable: false, type: "date" }
                    , MODIFIED_USER: { editable: false }
                    , MODIFIED_DATE: { editable: false, type: "date" }
                    , PUBLISH: { editable: false }
                    , PUBLISH2PRINT: { editable: false }
                    , PUBLISH2CD: { editable: false }
                    , WORKFLOW_STATUS: { editable: false }
                    , WORKFLOW_COMMENTS: { editable: false }
                    , CLONE_LOCK: { editable: false }
                    , IS_CLONE: { editable: false }
                    , CATEGORY_SHORT: { editable: false }
                    , CUSTOMER_ID: { editable: false }
                    , CATEGORY_PARENT_SHORT: { editable: false }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    //type: "info"
                });
            }
        },
        change: function (e) {
            if ($scope.isCategorySearchResultsNull)
                $("#divCategorySearch").hide();
        }
    });



    $scope.categorySearchGridresultsColumns = [
        { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
        { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkCategory(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
        { field: "CATEGORY_PARENT_SHORT", title: "Parent Category", width: "180px" },
        { field: "SHORT_DESC", title: "Short description", width: "180px" },
        { field: "IMAGE_FILE", title: "Image File", width: "180px" },
        { field: "IMAGE_NAME", title: "Image Name", width: "180px" },
        { field: "IMAGE_NAME2", title: "Image Name2", width: "180px" },
        { field: "IMAGE_FILE2", title: "Image File2", width: "180px" },
        { field: "CUSTOM_NUM_FIELD1", title: "Custom Num Field1", width: "180px" },
        { field: "CUSTOM_NUM_FIELD2", title: "Custom Num Field2", width: "180px" },
        { field: "CUSTOM_NUM_FIELD3", title: "Custom Num Field3", width: "180px" },
        { field: "CUSTOM_TEXT_FIELD1", title: "Custom Text Field1", width: "180px" },
        { field: "CUSTOM_TEXT_FIELD2", title: "Custom Text Field2", width: "180px" },
        { field: "CUSTOM_TEXT_FIELD3", title: "Custom Text Field3", width: "180px" },
          { field: "CREATED_USER", title: "Created User", width: "180px" },
         { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } },
         { field: "MODIFIED_USER", title: "Modified User", width: "180px" },
         { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } },
        { field: "PUBLISH", title: "Publish", width: "180px" },
        { field: "PUBLISH2PRINT", title: "Publish To Print", width: "180px" },
        { field: "PUBLISH2CD", title: "Publish To CD", width: "180px" },
        { field: "WORKFLOW_STATUS", title: "Workflow Status", width: "180px" },
        { field: "WORKFLOW_COMMENTS", title: "Workflow Comments", width: "180px" },
        { field: "CLONE_LOCK", title: "Clone Lock", width: "180px" },
        { field: "IS_CLONE", title: "IS Clone", width: "180px" }];

    // ---------------------------------------------------End Category Search Results--------------------------------------------------------------------------------

    // ---------------------------------------------------Start Family Search Results--------------------------------------------------------------------------------


    $scope.searchEndtxt = '';

    $scope.GetFamilySearchGridresultsdataSource = new kendo.data.DataSource({
        pageSize: 5,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        sort: { field: "CATEGORY_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                if ($scope.seletedAttribute == "") {
                    $scope.seletedAttribute = "All";
                }
                dataFactory.GetFamilySearchResults($scope.searchtxt, $scope.searchid, $scope.usewildcards, $scope.catalogIdAttr, $scope.seletedAttribute, $scope.CategoryID,$scope.searchEndtxt).success(function (response) {
                    if (response.length != 0) {
                        $("#divFamilySearch").show();
                        $scope.isFamilySearchResultsNull = false;
                        $scope.searchEmptyResult = false;
                    }
                    else {
                        $scope.isFamilySearchResultsNull = true;
                        $scope.searchEmptyResult = true;
                    }
                    options.success(response);

                }).error(function (error) {
                    options.error(error);
                });

            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FAMILY_ID",
                fields: {
                    CATALOG_ID: { editable: false }, CATALOG_NAME: { editable: false }, CATEGORY_ID: { editable: false },
                    CATEGORY_NAME: { editable: false }, FAMILY_ID: { editable: false }, FAMILY_NAME: { editable: false },
                    STATUS: { editable: false }, ATTRIBUTE_ID: { editable: false }, ATTRIBUTE_NAME: { editable: false },
                    STRING_VALUE: { editable: false }, ATTRIBUTE_TYPE: { editable: false }, NUMERIC_VALUE: { editable: false },
                    OBJECT_NAME: { editable: false }, OBJECT_TYPE: { editable: false }, CREATED_USER: { editable: false },
                    CREATED_DATE: { editable: false, type: "date" }, MODIFIED_USER: { editable: false }, MODIFIED_DATE: { editable: false, type: "date" }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });
            }
        },
        change: function (e) {
            if ($scope.isFamilySearchResultsNull)
                $("#divFamilySearch").hide();
        }
    });

    $scope.familySearchGridresultsColumns = [
        { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
         { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkCategoryid(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
         { field: "FAMILY_NAME", title: "Family Name", template: "<a title='Family Name' class='k-link'  href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=FAMILY_NAME#</a>", width: "180px" },
         { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "180px" },
         { field: "STRING_VALUE", title: "String Value", width: "180px" },
         { field: "NUMERIC_VALUE", title: "Numeric Value", width: "180px" },
         { field: "STATUS", title: "Status", width: "180px" },
         { field: "CREATED_USER", title: "Created User", width: "180px" },
         { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } },
         { field: "MODIFIED_USER", title: "Modified User", width: "180px" },
         { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } }];


    // ---------------------------------------------------End Family Search Results--------------------------------------------------------------------------------
    // ---------------------------------------------------Start Product Search Results--------------------------------------------------------------------------------

    $scope.GetProductSearchGridresultsdataSource = new kendo.data.DataSource({
        pageSize: 5,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        sort: { field: "CATEGORY_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                if ($scope.seletedAttribute == "") {
                    $scope.seletedAttribute = "All";
                }
                dataFactory.GetProductSearchResults($scope.searchtxt, $scope.searchid, $scope.usewildcards, $scope.catalogIdAttr, $scope.seletedAttribute, $scope.CategoryID).success(function (response) {
                    if (response.length != 0) {
                        $("#divProductSearch").show();
                        $scope.searchEmptyResult = false;
                        $scope.isProductSearchResultsNull = false;
                    }
                    else {
                        $scope.searchEmptyResult = true;
                        $scope.isProductSearchResultsNull = true;
                    }

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });

            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FAMILY_ID",
                fields: {
                    STRING_VALUE: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    PRODUCT_ID: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    OBJECT_TYPE: { editable: false },
                    OBJECT_NAME: { editable: false },
                    FAMILY_ID: { editable: false },
                    FAMILY_NAME: { editable: false },
                    SUBFAMILY_NAME: { editable: false },
                    CATEGORY_ID: { editable: false },
                    CATEGORY_NAME: { editable: false },
                    SUBCATNAME_L1: { editable: false },
                    SUBCATNAME_L2: { editable: false },
                    SUBCATNAME_L3: { editable: false },
                    CATALOG_ID: { editable: false },
                    CATALOG_NAME: { editable: false },
                    CREATED_USER: { editable: false },
                    CREATED_DATE: { editable: false, type: "date" },
                    MODIFIED_USER: { editable: false },
                    MODIFIED_DATE: { editable: false, type: "date" }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });
            }

        },
        change: function (e) {
            if ($scope.isProductSearchResultsNull)
                $("#divProductSearch").hide();
        }
    });

    $scope.GetSubProductSearchGridresultsdataSource = new kendo.data.DataSource({
        pageSize: 5,
        batch: false,
        serverPaging: false,
        serverSorting: false,
        serverFiltering: false,
        sort: { field: "CATEGORY_NAME", dir: "asc" },
        transport: {
            read: function (options) {
                if ($scope.seletedAttribute == "") {
                    $scope.seletedAttribute = "All";
                }
                dataFactory.GetSubProductSearchResults($scope.searchtxt, $scope.searchid, $scope.usewildcards, $scope.catalogIdAttr, $scope.seletedAttribute, $scope.CategoryID).success(function (response) {
                    if (response.length != 0) {
                        $scope.searchEmptyResult = false;
                        $scope.isSubProductSearchResultsNull = false;
                        $("#divSubProductSearch").show();
                    }
                    else {
                        $scope.searchEmptyResult = true;
                        $scope.isSubProductSearchResultsNull = true;
                    }

                    options.success(response);
                }).error(function (error) {
                    options.error(error);
                });

            }
        }, schema: {
            data: function (data) { return data; },
            total: function (data) {
                return data.length;
            },
            model: {
                id: "FAMILY_ID",
                fields: {
                    STRING_VALUE: { editable: false },
                    ATTRIBUTE_NAME: { editable: false },
                    PRODUCT_ID: { editable: false },
                    ATTRIBUTE_TYPE: { editable: false },
                    OBJECT_TYPE: { editable: false },
                    OBJECT_NAME: { editable: false },
                    FAMILY_ID: { editable: false },
                    FAMILY_NAME: { editable: false },
                    SUBFAMILY_NAME: { editable: false },
                    CATEGORY_ID: { editable: false },
                    CATEGORY_NAME: { editable: false },
                    SUBCATNAME_L1: { editable: false },
                    SUBCATNAME_L2: { editable: false },
                    SUBCATNAME_L3: { editable: false },
                    CATALOG_ID: { editable: false },
                    CATALOG_NAME: { editable: false },
                    CREATED_USER: { editable: false },
                    CREATED_DATE: { editable: false, type: "date" },
                    MODIFIED_USER: { editable: false },
                    MODIFIED_DATE: { editable: false, type: "date" }
                }
            }, error: function (e) {
                $.msgBox({
                    title: $localStorage.ProdcutTitle,
                    content: '' + e.xhr.responseText + '.',
                    type: "error"
                });
            }

        },
        change: function (e) {
            if ($scope.isSubProductSearchResultsNull)
                $("#divSubProductSearch").hide();
        }
    });

    $scope.productSearchGridresultsColumns = [
        { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
        { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkCategoryid(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
         { field: "FAMILY_NAME", title: "Family Name", template: "<a title='Family Name' class='k-link' href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=FAMILY_NAME#</a>", width: "180px" },
         { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "180px" },
         { field: "STRING_VALUE", title: "String Value", template: "<a title='Product Specification' class='k-link' href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=STRING_VALUE#</a>", width: "180px" },
         { field: "NUMERIC_VALUE", title: "Numeric Value", width: "180px" },
         { field: "STATUS", title: "Status", width: "180px" },
         { field: "CREATED_USER", title: "Created User", width: "180px" },
         { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } },
         { field: "MODIFIED_USER", title: "Modified User", width: "180px" },
         { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } }];

    $scope.subproductSearchGridresultsColumns = [
       { field: "CATALOG_NAME", title: "Catalog Name", width: "180px" },
       { field: "CATEGORY_NAME", title: "Category Name", template: "<a title='Category Name' class='k-link' href='javascript:void(0);' ng-click='ngclkCategoryid(this)'>#=CATEGORY_NAME#</a>", width: "180px" },
        { field: "FAMILY_NAME", title: "Family Name", template: "<a title='Family Name' class='k-link' href='javascript:void(0);' ng-click='ngclkfamilyid(this)'>#=FAMILY_NAME#</a>", width: "180px" },
        { field: "CATALOG_ITEM_NO", title: "ITEM#", template: "<a class='k-link' href='javascript:void(0);' >#=CATALOG_ITEM_NO#</a>", width: "180px" },
        { field: "ATTRIBUTE_NAME", title: "Attribute Name", width: "180px" },
        { field: "STRING_VALUE", title: "String Value", template: "<a title='SubProducts Specification' class='k-link' href='javascript:void(0);' ng-click='ngclkSubProductid(this)'>#=STRING_VALUE#</a>", width: "180px" },
        { field: "NUMERIC_VALUE", title: "Numeric Value", width: "180px" },
        { field: "STATUS", title: "Status", width: "180px" },
        { field: "CREATED_USER", title: "Created User", width: "180px" },
        { field: "CREATED_DATE", title: "Created Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } },
        { field: "MODIFIED_USER", title: "Modified User", width: "180px" },
        { field: "MODIFIED_DATE", title: "Modified Date", width: "180px", format: "{0:MM/dd/yyyy hh:mm tt}", filterable: { ui: "datetimepicker" } }];

    // ---------------------------------------------------End Product Search Results--------------------------------------------------------------------------------

    $rootScope.dropDownCategorySearchDatasource = new kendo.data.HierarchicalDataSource({
        type: "json",
        transport: {
            read: function (options) {

                dataFactory.GetCategoriesForSearch($scope.catalogIdAttr, options.data.id).success(function (response) {

                    var CatID = 0;
                    if ($scope.categorydetails == true) {
                        //$scope.dataItemCat = response;
                        //for (catId = 0; CatID <= $scope.dataItemCat.length; CatID++)
                        //{
                        //    $scope.dataItemCat.splice(catId, 1);
                        //}

                        $("#SearchCategoryTree").addClass("disabledbutton");
                    }
                    else {
                        $("#SearchCategoryTree").removeClass("disabledbutton");

                    }

                    options.success(response);


                }).error(function (response) {
                    options.success(response);
                });
            }
        },
        schema: {
            model: {
                id: "id",
                hasChildren: "hasChildren"
            }
        }
    });

    if ($("#SearchCategoryTree").val() !== undefined) {
        $rootScope.SearchCategoryTree = $("#SearchCategoryTree").kendoExtDropDownTreeView({

            treeview: {
                dataSource: $rootScope.dropDownCategorySearchDatasource,
                dataTextField: "CATEGORY_NAME",
                dataValueField: "id",
                loadOnDemand: true,
                select: onSelect,
                selectable: true,
                dataBound: onDataBound


            }

        }).data("kendoExtDropDownTreeView");


        //var selected;
        //var treeView = $rootScope.SearchCategoryTree;
        ////treeView.expand($treePath);

        //// Gotta make both calls...
        //treeView.select(selected);

        //treeView.trigger('select', { node: selected });

        //   $rootScope.dropDownParnetCategoryTreeView.treeview().expand(".k-item");
    }
    function categoryBind() {
        $rootScope.SearchCategoryTree = $("#SearchCategoryTree").kendoExtDropDownTreeView({
            treeview: {
                dataSource: $rootScope.dropDownCategorySearchDatasource,
                dataTextField: "CATEGORY_NAME",
                dataValueField: "id",
                loadOnDemand: true,
                select: onSelect,
                selectable: true,
                dataBound: onDataBound
            }
        }).data("kendoExtDropDownTreeView");
    };
    var i = 0;
    function onDataBound(e) {
        //var tree = $("#SearchCategoryTree").data("kendoExtDropDownTreeView");
        //var firstNode = $("#SearchCategoryTree").find('.k-first');
        //tree.select(firstNode);
        //tree.select(firstNode);
        //var dataItem = this.dataItem(e.sender.root.children()[0]);

        if ($scope.categorydetails == true) {
            $('#SearchCategoryTree').children()[0].children[0].children[0].innerText = "";

            return null;
        }
        var treeview = $("#SearchCategoryTree").data("kendoExtDropDownTreeView");
        treeview.treeview().findByUid($("#SearchCategoryTree").find('li')[0].attributes[2].value);
        var selectitem = treeview.treeview().findByUid($("#SearchCategoryTree").find('li')[0].attributes[2].value);
        treeview.treeview().select(selectitem);
        $('#SearchCategoryTree').children()[0].children[0].children[0].attributes[0].value = "off"
        $('#SearchCategoryTree').children()[0].children[0].children[0].innerText = selectitem[0].outerText;
        $scope.CategoryID = treeview.treeview().dataSource._data[0].id;

    }
    function onSelect(e) {

        var dataItem = this.dataItem(e.sender._clickTarget);
        $scope.CategoryID = dataItem.id;
    };
    //--------To get EnableSubProduct value from preference page-------
    $rootScope.getEnableSubProduct = function () {
        //$scope.CustomerID = $rootScope.currentUser.CustomerDetails.CustomerId;
        dataFactory.getEnableSubProduct().success(function (response) {
            
            $rootScope.EnableSubProduct = response;
            $localStorage.EnableSubProductLocal = response;

        })
    };


    $scope.init = function () {
        $("#divItemSearch").hide();
        $("#divCategorySearch").hide();
        $("#divFamilySearch").hide();
        $("#divProductSearch").hide();
        $("#divSubProductSearch").hide();
        $("#familyEditor").hide();
        $("#searchtab").show();
        $("#resultbtntab").hide();
        $("#dynamictable").hide();
        $("#divSubItemSearch").hide();
        $scope.productData = [];
        $rootScope.SubProdInvert = false;
        $rootScope.btnSubProdInvert = false;
        $rootScope.invertedproductsshow = false;
        $rootScope.invertedproductsbutton = false;
        $rootScope.invertedproductsbutton1 = false;
        $rootScope.getEnableSubProduct();
    };
    $scope.init();
}]);
